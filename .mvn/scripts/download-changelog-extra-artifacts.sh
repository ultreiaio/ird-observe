#!/usr/bin/env bash

source $(which ultreiaio-common)

execute_maven "-N -e gitlab:download-extra-artifacts -Dgitlab.verbose -Dplugin.version.gitlab=1.0.20-SNAPSHOT -Dgitlab.milestone=8.0-RC -Dgitlab.versions=8.0-RC-1,8.0-RC-2,8.0-RC-3,8.0-RC-4,8.0-RC-5,8.0-RC-6,8.0-RC-7,8.0.0-RC-8" "Download extract artifacts for v8.0-RC"
execute_maven "-N -e gitlab:download-extra-artifacts -Dgitlab.verbose -Dplugin.version.gitlab=1.0.20-SNAPSHOT -Dgitlab.milestone=9.0.0-RC -Dgitlab.versions=9.0.0-RC-1,9.0.0-RC-2,9.0.0-RC-3,9.0.0-RC-4,9.0.0-RC-5,9.0.0-RC-6,9.0.0-RC-7,9.0.0-RC-8,9.0.0-RC-8.1,9.0.0-RC-9,9.0.0-RC-10,9.0.0-RC-11,9.0.0-RC-12,9.0.0-RC-13,9.0.0-RC-14,9.0.0-RC-15,9.0.0-RC-16,9.0.0-RC-17,9.0.0-RC-18,9.0.0-RC-19,9.0.0-RC-20,9.0.0-RC-21,9.0.0-RC-22,9.0.0-RC-23" "Download extract artifacts for v9.0.0-RC"
