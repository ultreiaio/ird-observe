#!/usr/bin/env bash

source $(which ultreiaio-common)

execute_maven "-N -Pupdate-site-versions" "Update site versions"

execute_maven "scm-publish:publish-scm" "Publish site"
