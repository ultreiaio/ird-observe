#!/usr/bin/env bash

source $(which ultreiaio-common)

execute_maven "-N -Pdelete-tck" "Delete tck cache"