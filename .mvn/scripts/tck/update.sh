#!/usr/bin/env bash

source $(which ultreiaio-common)

execute_maven "clean install -e -am -rf :model -pl :core-persistence-migration" "Build to core-persistence-migration"
execute_maven "-DperformRelease -pl :core-persistence-migration -Pupdate-tck" "Migrates and deploy tck"