# ObServe changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2024-12-30 14:03.

## Version [9.4.0-M2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/289)
# Maintenance évolutive et corrective sur la 9.4.0

**Closed at 2024-12-30.**


### Download
* [Client (observe-9.4.0-M2-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.4.0-M2/observe-9.4.0-M2-client.zip)
* [Serveur (observe-9.4.0-M2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.4.0-M2/observe-9.4.0-M2.war)
* [Serveur (observe-9.4.0-M2-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.4.0-M2/observe-9.4.0-M2-server.zip)


### Issues
  * [[Type::Anomalie 2951]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2951) **L&#39;option d&#39;édition du référentiel d&#39;une base locale nécessite un rechargement manuel de la source de données** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2952]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2952) **Problème dans la consultation des données de référence Objet flottant matériel** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2953]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2953) **Toute petite imperfection sur le formulaire PS / Lots de marché local** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2956]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2956) **Souci de l&#39;UI configuration sur les options de type liste** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2960]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2960) **L&#39;UI de synchro de marées pose un problème sous Windows uniquement** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2961]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2961) **2 mappings AVDTH ASSOC non fonctionnels** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2949]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2949) **Bornes de longueurs d&#39;orins en ftm** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2950]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2950) **LL/observations/capture : Champ numéro au virage devrait être désactivé en saisie groupée** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2954]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2954) **Amélioration possible sur l&#39;UI PS / Logbook / Echantillon / minus10weight et plus10weight** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2957]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2957) **Libellés sur l&#39;UI consolidation** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2958]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2958) **A propos des listes d&#39;espèces pour les calculs de consolidation** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2959]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2959) **Peaufinage sur la migration des LOT_COM AVDTH** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)

## Version [9.4.0-M1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/286)

**Closed at 2024-10-01.**

### Download
* [Client (observe-9.4.0-M1-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.4.0-M1/observe-9.4.0-M1-client.zip)
* [Serveur (observe-9.4.0-M1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.4.0-M1/observe-9.4.0-M1.war)
* [Serveur (observe-9.4.0-M1-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.4.0-M1/observe-9.4.0-M1-server.zip)

### Issues
  * [[Type::Evolution 2603]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2603) **Améliorer la reconnexion à un serveur distant utilisé comme connexion d’un assistant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2669]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2669) **Automatiser la calcul des sample.minus10Weight et sample.plus10Weight par sommation des lots du plan de cuves** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2818]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2818) **Ajouter 2 tables de référence et 2 références à ces tables dans Vessel** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2893]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2893) **Labels à améliorer** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2909]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2909) **LL observations/captures : ajout de champs Reférence Échantillon** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2914]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2914) **Utilisation du convertisseur d&#39;unité sur la définition détaillée de palangre et décimales** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2931]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2931) **Ajouter une liste d&#39;espèce dédiée à la consolidation des sampleActivity.weightedWeight et l&#39;utiliser dans l&#39;assistant de consolidation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2932]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2932) **3 listes déroulantes à ajouter sur formulaire LL / observations / captures (références déjà existantes)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2942]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2942) **Il devrait être possible de saisir un échantillon PS logbook sans poids -10/+10/total de sorte à bénéficier ensuite du calcul automatique** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.3.9](https://gitlab.com/ultreiaio/ird-observe/-/milestones/288)

**Closed at 2024-09-27.**

### Download
* [Client (observe-9.3.9-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.9/observe-9.3.9-client.zip)
* [Serveur (observe-9.3.9.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.9/observe-9.3.9.war)
* [Serveur (observe-9.3.9-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.9/observe-9.3.9-server.zip)

### Issues
  * [[Type::Anomalie 2944]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2944) **Problème de validation sur le formulaire Échantillon (PS - Marché local)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.3.8](https://gitlab.com/ultreiaio/ird-observe/-/milestones/287)

**Closed at 2024-09-26.**

### Download
* [Client (observe-9.3.8-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.8/observe-9.3.8-client.zip)
* [Serveur (observe-9.3.8.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.8/observe-9.3.8.war)
* [Serveur (observe-9.3.8-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.8/observe-9.3.8-server.zip)

### Issues
  * [[Type::Anomalie 2939]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2939) **Erreur lors de la création d&#39;une route PS observations** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2940]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2940) **Erreur systématique lors de la création d&#39;activité PS observation** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2941]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2941) **Erreur de la saisie des échantillons espèce PS logbooks** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2943]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2943) **Impossible de saisir les Echantillons espèce en PS logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.3.7](https://gitlab.com/ultreiaio/ird-observe/-/milestones/285)

**Closed at 2024-09-18.**

### Download
* [Client (observe-9.3.7-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.7/observe-9.3.7-client.zip)
* [Serveur (observe-9.3.7.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.7/observe-9.3.7.war)
* [Serveur (observe-9.3.7-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.7/observe-9.3.7-server.zip)

### Issues
  * [[Type::Anomalie 2917]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2917) **Faille sur l&#39;UI de remplacement de code de référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2925]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2925) **Perte dans l&#39;éditeur du type de mesure et de pesée lors de la sélection d&#39;une ligne fraîchement crée dans le tableau des données d&#39;échantillons** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2927]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2927) **Le changement de format des éditeurs de température ou de longueur altère l&#39;état «modifié» du formulaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2928]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2928) **Le changement de format des éditeurs de coordonnées ou de longueur altère l&#39;état «modifié» du formulaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1203]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1203) **Tableaux de saisie (captures, échantillons, équipements...) - Classement en cliquant sur les en-têtes de colonnes** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2594]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2594) **Suppression de la technologie XWorks (utilisé pour la validation)** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2734]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2734) **La liste des activités associées à un échantillon pourrait être déduite du plan de cuves, et inversément** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2816]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2816) **Fonction de changement de programme en masse** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2826]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2826) **Permettre le renommage, la suppression, éventuellement l&#39;édition des présélections de FOB** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2834]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2834) **Sauvegarde des préférences utilisateur sur le classement des listes : gestion dans l&#39;UI** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2875]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2875) **A propos des dates d&#39;activités sur la carte LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2900]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2900) **Possibilité de n&#39;effectuer la recherche dans une liste déroulante que sur l&#39;un des champs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2901]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2901) **Permettre à l’utilisateur de sauvegarder les unités choisies (longueur, température et coordonnées)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2903]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2903) **LL observations / logbook : mise à jour auto de la date de fin de marée en fonction des dates activités et calées (comme en PS)** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2904]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2904) **LL observations / logbook : Revue des contrôles sur la cohérences des dates sur les calées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2906]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2906) **LL observations/composition détaillée : amélioration de la réactivité du formulaire en mode édition** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2907]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2907) **LL observations/captures : réorganisation du formulaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2918]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2918) **Amélioration des poids +10/-10 et total des échantillons lors de l&#39;import AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2923]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2923) **Mettre à null le commentaire Trip.logbookComment si celui ci n&#39;est constitué que d&#39;espaces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2924]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2924) **Ajout de nouveaux critères dans la configuration de l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2926]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2926) **Amélioration de la conservation du paramétrage des UI** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2929]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2929) **Rendre configurable la liste des espèces à inclure dans la consolidation du sampleActivity.weightedWeight** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2930]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2930) **Ajout des supply et canneurs dans les listes de bateaux sur marées PS** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2934]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2934) **Certains forms acceptent des dates supérieures à la date du jour** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2936]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2936) **Ajout dans la configuration du client de l&#39;option déjéà existante instance.server.checkServerVersion (onglet expert)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.3.6](https://gitlab.com/ultreiaio/ird-observe/-/milestones/283)

**Closed at 2024-07-03.**

### Download
* [Client (observe-9.3.6-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.6/observe-9.3.6-client.zip)
* [Serveur (observe-9.3.6.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.6/observe-9.3.6.war)
* [Serveur (observe-9.3.6-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.6/observe-9.3.6-server.zip)

### Issues
  * [[Type::Anomalie 2887]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2887) **En synchro avancée de données, la modification de la configuration des arbres de navigation ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2889]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2889) **Les transcodages ASSOC ne sont plus totalement respectés par l&#39;importeur AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2890]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2890) **Sur synchro avancée de données, on ne peut plus changer le classement de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2891]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2891) **L&#39;utilisation du bouton de création d&#39;un FOB brut (sans présélection) ne fonctionne plus en mode serveur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2896]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2896) **Une règle de migration AVDTH ASSOC non respectée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2899]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2899) **Amélioration du code de mise à jour sql pour les associations lors de remplacement de référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2915]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2915) **Un utilisateur Windows a toujours un souci avec les chemins lors du démarrage en 9.3.5** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2888]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2888) **En synchro avancée de données, amélioration de la gestion des droits des bases utilisées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2892]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2892) **Empêcher l &#39;assistant synchro données avancées de poursuivre si les 2 bases n&#39;ont pas un droit saisisseur au minimum** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2905]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2905) **Sur le formulaire Schéma détaillé de palangre, onglet &quot;Composition&quot;, tableau &quot;Paniers&quot;, préciser l&#39;unité dans les en-têtes des colonnes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2908]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2908) **LL observations/captures : ajout de champs dans le tableau récapitulatif** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2910]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2910) **Dans le cas d&#39;une opération activité 34, toujours ajouter le matériaux balise seule sur le FOB** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2911]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2911) **Ultime peaufinage sur traitement d&#39;une OPERA 34 (retrait de balise seule)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2912]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2912) **Externaliser les bases avdth utilisées dans les tests** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Documentation 2894]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2894) **Mini erreur dans la doc du migrateur AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Documentation 2897]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2897) **Précisions à ajouter dans la documentation de migration AVDTH** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Documentation 2916]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2916) **Micro correction dans la doc du migrateur AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.3.5](https://gitlab.com/ultreiaio/ird-observe/-/milestones/282)

**Closed at 2024-05-22.**

### Download
* [Client (observe-9.3.5-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.5/observe-9.3.5-client.zip)
* [Serveur (observe-9.3.5.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.5/observe-9.3.5.war)
* [Serveur (observe-9.3.5-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.5/observe-9.3.5-server.zip)

### Issues
  * [[Type::Anomalie 2884]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2884) **Le système de conservation du dernier chemin utilisé pour une sauvegarde empêche le logiciel de redémarrer si le volume utilisé n&#39;existe plus** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2885]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2885) **Le répertoire à utiliser pour les sauvegardes n&#39;est pas mis à jour dans la configuration lors d&#39;un sauvegarde pendant un nouvel import** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2886]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2886) **Insertion de set LL logbook sans capture** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.3.4](https://gitlab.com/ultreiaio/ird-observe/-/milestones/280)

**Closed at 2024-05-13.**

### Download
* [Client (observe-9.3.4-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.4/observe-9.3.4-client.zip)
* [Serveur (observe-9.3.4.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.4/observe-9.3.4.war)
* [Serveur (observe-9.3.4-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.4/observe-9.3.4-server.zip)

### Issues
  * [[Type::Anomalie 2882]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2882) **Il semble que le référentiel local ne se mette plus à jour lors d&#39;une synchro simple de référentiel** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2883]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2883) **Muavaise gestion des lignes sur la carte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.3.3](https://gitlab.com/ultreiaio/ird-observe/-/milestones/279)

**Closed at 2024-05-02.**

### Download
* [Client (observe-9.3.3-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.3/observe-9.3.3-client.zip)
* [Serveur (observe-9.3.3.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.3/observe-9.3.3.war)
* [Serveur (observe-9.3.3-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.3/observe-9.3.3-server.zip)

### Issues
  * [[Type::Anomalie 2881]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2881) **ERROR migration encore** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.3.2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/278)

**Closed at 2024-05-02.**

### Download
* [Client (observe-9.3.2-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.2/observe-9.3.2-client.zip)
* [Serveur (observe-9.3.2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.2/observe-9.3.2.war)
* [Serveur (observe-9.3.2-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.2/observe-9.3.2-server.zip)

### Issues
  * [[Type::Anomalie 2880]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2880) **migration : ERROR: column objectoperation contains null values** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.3.1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/277)

**Closed at 2024-04-30.**

### Download
* [Client (observe-9.3.1-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.1/observe-9.3.1-client.zip)
* [Serveur (observe-9.3.1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.1/observe-9.3.1.war)
* [Serveur (observe-9.3.1-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.1/observe-9.3.1-server.zip)

### Issues
  * [[Type::Anomalie 2868]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2868) **Lorsque l&#39;on crée une marée dans un programme depuis l&#39;arbre, le programme en question n&#39;est plus sélectionné par défaut** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2872]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2872) **Impossible d&#39;effacer une marée (PS ou LL) en mode serveur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2873]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2873) **La sauvegarde en sql.gz ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2876]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2876) **Le form PS logbook a perdu sa valeur Data quality par défaut lorsque la marée est créée par le programme logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2877]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2877) **Il est possible de sauver un FOB sans lui avoir donné de type d&#39;opération, et cela plante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2869]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2869) **En mode classement par bateau, il est possible de créer une marée PS avec un bateau LL, de plus avec un bateau désactivé** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2871]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2871) **Augmenter la valeur d&#39;un contrôle de formulaire LL Logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2874]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2874) **Encore une borne de poids à augmenter, sur PS localmarket batch** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2879]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2879) **Suppression des valeurs balise pays et bateau si l&#39;appartenance à désactiver ces champs** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.3.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/271)

**Closed at 2024-04-25.**

### Download
* [Client (observe-9.3.0-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.0/observe-9.3.0-client.zip)
* [Serveur (observe-9.3.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.0/observe-9.3.0.war)
* [Serveur (observe-9.3.0-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.3.0/observe-9.3.0-server.zip)

### Issues
  * [[Type::Anomalie 2700]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2700) **Certains ajouts ou certaines MAJ de champs en synchro avancée de référentiel sont sans effet** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2748]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2748) **En prévision du peaufinage des forms Balise, déterminer comment gérer la migration des données existantes : cas appartenance &#61; &#39;A ce bateau&#39;** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2796]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2796) **L&#39;application insère parfois des floatingobjectpart avec whenarriving&#61;NULL &amp; whenleaving&#61;NULL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2804]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2804) **Les assistants d&#39;ouverture ou sauvergade de sql.gz ne retrouvent jamais le dernier répertoire utilisé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2810]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2810) **LL observations, en sélection multiple d&#39;activités, la fonction &#39;Rouvrir la donnée de type activité&#39; ne devrait pas être disponible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2813]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2813) **En prévision du peaufinage des forms Balise, déterminer comment gérer la migration des données existantes : cas appartenance &#61; &#39;A un bateau inconnu&#39;** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2817]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2817) **Le service init.information de l&#39;API retourne les informations sur la base cible, alors qu&#39;on ne devrait que retourner son nom** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2819]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2819) **Depuis la v9, les fichiers de log ouverts en UTF-8 présentent un défaut d&#39;affichage** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2835]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2835) **Exception lors de la modification des statuts d&#39;une marée** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2840]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2840) **Erreur retournée si envoie de marée logbook LL avec un coup de pêche sans captures** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2843]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2843) **Erreur sur transaction et plantage de Tomcat sur consolidation en mode serveur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2854]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2854) **Perte de connection au serveur lors d&#39;envoie massif de marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2857]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2857) **La valeur du champs ps_observation.Catch.well a des fois la valeur vide, à la place de la valeur nulle** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2860]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2860) **Alerte InsecureRecursiveDeleteException lors du lancement de la 9.3** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2862]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2862) **Sur LL logbooks, global composition, le bouton &#39;Save&#39; est disponible dans des situations où il ne devrait pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2864]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2864) **Les reports LL logbook ne fonctionnent pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2866]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2866) **Impossible de rouvrir une activité PS logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1962]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1962) **Séparer le fichier des reports en plusieurs fichiers** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2044]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2044) **Peaufinage du form Balise : disponibilité des champs Appartenance, Pays et Navire** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2182]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2182) **Ajouter le défilement année par année sur les calendriers** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2363]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2363) **Ajouter une option pour afficher/masquer la trace entre les activités de la carte** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2549]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2549) **Mettre en exergue sur la carte les calées qui ont été échantillonnées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2599]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2599) **Rendre le fichier de définition des rapports multilingue** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2605]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2605) **Mettre à jour la libraire apache http-client** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2663]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2663) **Contrôle de validation position si opération sur objet !&#61; perte/fin transmission/fin balise** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2746]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2746) **Suppression de références en masse** (Thanks to ) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2755]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2755) **Sur les tableaux de synthèse, ajouter un export HTML** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2805]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2805) **Ajouter une option pour voir le nombre de points dans la légende** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2806]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2806) **Pouvoir sélectionner/déselectionner les lignes et les points dans la carte depuis la légende** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2811]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2811) **Définir les règles de mise en forme conditionnelle sur les tableaux de synthèse** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2812]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2812) **Normalisation des dates et heures dans les rapports** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2814]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2814) **VesselActivity sur création d&#39;activity par l&#39;importeur AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2822]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2822) **Amélioration de l&#39;interface des rapports** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2827]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2827) **Extraire toutes les requêtes sql (et les ajouter au niveau du modèle)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2828]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2828) **Ajout d&#39;une option avancée pour ne pas consolider les données sur un assistant de reporting** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2831]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2831) **Pouvoir rejourer l&#39;assistant d&#39;appairement des activités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2836]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2836) **Agrandissement de popup** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2839]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2839) **Lors d&#39;exports massifs de données, le token expire rapidement et fait échouer la suite du processus** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2844]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2844) **Consolidation du modèle de persistence sur les données de type décimales** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2848]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2848) **Tableaux de synthèse à intégrer** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2849]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2849) **Optimisation du rapport Livre de bord - Vérification des plan de cuves** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2850]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2850) **Optimisation du rapport Livre de bord - Plan de cuves** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2851]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2851) **Optimisation du rapport Observations - Utilisation des FOB, tableau simplifiée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2852]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2852) **Optimisation du rapport Observations - Utilisation des FOB, tableau détaillé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2853]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2853) **Amélioration de la taille des colonnes du tableau des résultats d&#39;un rapport** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2855]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2855) **Ajout des rapports sur les FOB PS - Logbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2856]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2856) **Nouveau rapport PS / Observations &amp; Logbooks / Captures par Zone FPA, espèce et devenir** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2863]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2863) **Augmenter la valeur max d&#39;un contrôle sur form PS landing** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2867]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2867) **Quelques libellés anglais, sur le fil** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2807]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2807) **Supprimer le module server-configuration-tool (il ne sert pas)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.2.3](https://gitlab.com/ultreiaio/ird-observe/-/milestones/276)

**Closed at 2024-02-15.**

### Download
* [Client (observe-9.2.3-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.3/observe-9.2.3-client.zip)
* [Serveur (observe-9.2.3.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.3/observe-9.2.3.war)
* [Serveur (observe-9.2.3-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.3/observe-9.2.3-server.zip)

### Issues
  * [[Type::Anomalie 2837]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2837) **Sur PS / Observations / Calée, les contrôles sur les dates/heures ne fonctionnent plus correctement** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2838]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2838) **En 9.2.2, impossible de créer une marée en mode serveur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.2.2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/273)

**Closed at 2024-01-22.**

### Download
* [Client (observe-9.2.2-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.2/observe-9.2.2-client.zip)
* [Serveur (observe-9.2.2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.2/observe-9.2.2.war)
* [Serveur (observe-9.2.2-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.2/observe-9.2.2-server.zip)

### Issues
  * [[Type::Anomalie 2825]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2825) **Sur l&#39;écran de définition Non déclenchement du bouton Enregistrer** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2832]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2832) **Impossibilité de déplacer une activité PS observation sur une autre route en mode serveur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.2.1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/272)

**Closed at 2023-11-14.**

### Download
* [Client (observe-9.2.1-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.1/observe-9.2.1-client.zip)
* [Serveur (observe-9.2.1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.1/observe-9.2.1.war)
* [Serveur (observe-9.2.1-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.1/observe-9.2.1-server.zip)

### Issues
  * [[Type::Anomalie 2798]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2798) **Souci de décalage de lignes sur le rapport Vérification des plans de cuve** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2800]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2800) **Erreur de migration sur bases AZTI locales** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2801]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2801) **Erreur migration 9.1 vers 9.2 de la base centrale AZTI** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2797]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2797) **Les fichiers s&#39;accumulent dans ressources/temp/sql-service jusqu&#39;à saturation** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2802]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2802) **Précisions sur les déductions AVDTH ASSOC vers FloatingObject et ObjectMaterial** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2803]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2803) **Ajout d&#39;un code à transcoder OPERA vers VesselActivity** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.2.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/269)

**Closed at 2023-10-09.**

### Download
* [Client (observe-9.2.0-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.0/observe-9.2.0-client.zip)
* [Serveur (observe-9.2.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.0/observe-9.2.0.war)
* [Serveur (observe-9.2.0-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.2.0/observe-9.2.0-server.zip)

### Issues
  * [[Type::Anomalie 2761]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2761) **Validation d&#39;une activité 13 sans heure** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2768]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2768) **On ne voit pas les toolTip dans les arbres des assistants de synchronisation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2769]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2769) **La valeur de configuration instance.security.key n&#39;est pas bien enregistrée même si elle est présente dans les fichiers de configuration commun (/var/local/observe-server/.conf/9.x)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2774]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2774) **En mode indexé, lorsque l&#39;on remonte une activité, les activités qui lui succédaient sont incrémentées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2777]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2777) **Sur les écrans LL-Observation qui renseigne des avançons, le champs TimeSinceContact est initialement accéssible alors que cela est conditionné par le champ Timer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2778]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2778) **Sur LL-Observation Capture, dans l&#39;onglet Avançon le champ Horodatage de montée à bord n&#39;est jamais accessible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2779]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2779) **La valeur null du champs de type time est altérée lors de l&#39;ouverture des formulaires** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2786]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2786) **Lors de la suppression d&#39;une donnée, il faut mettre à jour le champ lastUpdateDate sur les entités qui remontent jusqu&#39;à la marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2792]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2792) **Une incompatibilité empêche l&#39;ouverture de l&#39;application sous Java 21** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2793]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2793) **L&#39;UI équipement n&#39;interdit pas d&#39;associer à un équipement une caractéristique qui ne lui est pas destinée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2211]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2211) **Revoir la validation pour produire un résultat en forme d&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2215]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2215) **Améliorer le rapport de validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2449]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2449) **Nouveaux flags allowSet et fpaZoneMode sur les types d&#39;activités bateau** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2497]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2497) **Filtrage des devenirs pour les données observation et logbooks PS et LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2498]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2498) **Filtrage des types d&#39;activité bateau pour les données observation et logbooks PS et LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2706]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2706) **Améliorer l&#39;identification des enregistrements dans l&#39;UI de validation batch** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2707]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2707) **Faire évoluer l&#39;UI équipement du bateau pour autoriser la gestion de listes de référence** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2708]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2708) **Ajout de deux nouveaux champs et listes déroulantes sur le formulaire bonnes pratiques de remise à l&#39;eau** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2714]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2714) **ll_observation.Catch.count devrait être NOT NULL** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2725]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2725) **Contrôler la syntaxe des id balises par une expression régulière propre à chaque modèle de balise** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2729]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2729) **En PS / logbook / Activité, voir comment mieux gérer l&#39;ajout d&#39;une activité dont l&#39;horaire précéde celui de la dernière activité saisie (souci lié au numéro d&#39;activité auto incrémenté)** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2740]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2740) **En PS / logbook, ajouter 2 champs previousFpaZone et nextFpaZone** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2750]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2750) **Effectuer un différentiel sur l&#39;UI de synchro de marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2762]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2762) **Normalisation des drapeaux sur référentiels qui permettent de filtrer la liste des référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2763]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2763) **Rajouter un type Date (simple et sans time zone) dans GearCharacteristicType** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2767]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2767) **Ne plus afficher le label L&#39;opération requière des corrections sur les interfaces de certains assistants** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2771]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2771) **Sur les validateur de type taille/poids d&#39;une espèce, rajouter dans le message la valeur incriminée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2772]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2772) **Ne pas autoriser l&#39;utilisation d&#39;une caractéristique plus d&#39;une fois sur le formulaire équipement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2773]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2773) **Mauvaise récupération des heures dans l&#39;API publique** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2776]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2776) **Suppression des JSlider sur les éditeurs de temps** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2780]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2780) **Lors de la migration, il faut réorganiser les numéros d&#39;ordre des activités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2781]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2781) **Ajout de règles de validation sur les champ zone FPA sur les formulaires Activités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2782]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2782) **Ne pas autoriser la recherche dans les arbres (via l&#39;API consacrée dans JTree)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2783]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2783) **Ajouter une rège de validation sur les routes pour vérifier que les numéros d&#39;ordre suivent bien une séquence partant de 1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2784]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2784) **Sur la suppression d&#39;un activité, il faut recalculer les numéros d&#39;ordre sur les activités restantes de la route** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2785]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2785) **Sur le déplacement d&#39;un activité, il faut recalculer les numéros d&#39;ordre des activités sur la route origine et la route destination du déplacement.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2787]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2787) **Ajout de caractéristiques autorisées sur des équipements** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2788]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2788) **Supprimer les valeurs &#96;&#96;&#96;None&#96;&#96;&#96; des identifiants de balises via la migration classique.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2794]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2794) **Elargir l&#39;affichage du panneau Caractéristiques (d&#39;un équipement)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2791]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2791) **Problème de connexion au serveur IEO** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Documentation 2775]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2775) **Suppression du site nommé latest** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.1.6](https://gitlab.com/ultreiaio/ird-observe/-/milestones/270)

**Closed at 2023-07-15.**

### Download
* [Client (observe-9.1.6-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.6/observe-9.1.6-client.zip)
* [Serveur (observe-9.1.6.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.6/observe-9.1.6.war)
* [Serveur (observe-9.1.6-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.6/observe-9.1.6-server.zip)

### Issues
  * [[Type::Anomalie 2747]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2747) **Séquence du focus sur PS / Logbook / Activité** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2752]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2752) **Sur l&#39;UI référentiel Conditionnement, le champ poids moyen n&#39;est pas visible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2754]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2754) **Erreur de calcul sur les tonnages du tableau de synthèse &#39;Vérification des plans de cuve&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2757]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2757) **Problème d&#39;accès aux fonctions postgis** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2759]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2759) **L&#39;action voir les utilisations du référentiel ne doit pas être accessible si l&#39;utilisateur ne peut pas voir les données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2723]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2723) **Améliorer le comportement de l&#39;UI traduction** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2749]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2749) **Sur enregistrement d&#39;une nouvelle donnée, déplacement non souhaité de l&#39;arbre vers la droite** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2753]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2753) **Sur le tableau d&#39;affichage des lots de marché local, afficher le champ date** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2758]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2758) **Amélioration du feedback en mode serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.1.5](https://gitlab.com/ultreiaio/ird-observe/-/milestones/268)

**Closed at 2023-06-25.**

### Download
* [Client (observe-9.1.5-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.5/observe-9.1.5-client.zip)
* [Serveur (observe-9.1.5.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.5/observe-9.1.5.war)
* [Serveur (observe-9.1.5-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.5/observe-9.1.5-server.zip)

### Issues
  * [[Type::Anomalie 2713]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2713) **Affichage des types date sur l&#39;UI de synchro avancée de référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2718]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2718) **Rendre visible les heures de début et de fin de chaque sous-échantillon et faciliter leur saisie** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2724]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2724) **Le rôle saisisseur ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2727]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2727) **Une erreur de validation sur ps_localMarket.SurveyPart.proportion** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2730]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2730) **En PS / Logbook / Activité, la direction du vent se trouve dans le modèle de données mais pas sur le formulaire** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2731]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2731) **En PS / logbook / Activité, la validation sur les heures / numéro d&#39;incrémant n&#39;est pas correcte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2733]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2733) **Sur PS / Logbook / Echantillon, améliorer le défilement de la liste de saisie des fréquences de taille** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2735]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2735) **Le fichier de configuration du serveur n&#39;est pas consistent** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2719]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2719) **Borne max de poids sur le formulaire Lots de faux-poisson** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2720]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2720) **Le champs &#39;position d&#39;origine&#39; pourrait être désactivé si la case &#39;Donnée corrigée&#39; n&#39;est pas cochée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2728]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2728) **Pour les champs du modèle de persistance de type date, forcer le type réel via la tag value idoine** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2732]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2732) **Sur PS / Logbook / Echantillon et Plan de cuves, afficher le ne numéro d&#39;activité dans la liste déroulante des activités** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2736]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2736) **Ne pas regénérer le fichier de configuration des logs sur le serveur si celui contient déjà la bonne version** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2741]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2741) **Modification d&#39;un libellé sur Capture logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2745]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2745) **Amélioration des triggers postgis** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.1.4](https://gitlab.com/ultreiaio/ird-observe/-/milestones/267)

**Closed at 2023-06-01.**

### Download
* [Client (observe-9.1.4-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.4/observe-9.1.4-client.zip)
* [Serveur (observe-9.1.4.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.4/observe-9.1.4.war)
* [Serveur (observe-9.1.4-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.4/observe-9.1.4-server.zip)

### Issues
  * [[Type::Anomalie 2705]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2705) **Les résultats des utilisations d&#39;un référentiel peuvent être améliorés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2709]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2709) **Assistant sauvegarde qui ne zippe pas les bases** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2712]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2712) **Table de versionnage non présente dans certaines sauvegardes (depuis la version 9.0.X jusqu&#39;à la 9.1.3)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2702]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2702) **Permettre le remapping de code, même sans avoir à demander une désactivation ou une suppression de référence** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.1.3](https://gitlab.com/ultreiaio/ird-observe/-/milestones/266)

**Closed at 2023-05-20.**

### Download
* [Client (observe-9.1.3-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.3/observe-9.1.3-client.zip)
* [Serveur (observe-9.1.3.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.3/observe-9.1.3.war)
* [Serveur (observe-9.1.3-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.3/observe-9.1.3-server.zip)

### Issues
  * [[Type::Anomalie 2692]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2692) **Non déclenchement du bouton Enregistrer sur le form PS / observation / Set** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2696]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2696) **Label manquant sur cette popup** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2698]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2698) **Améliorer un retour d&#39;erreur sur le client, en mode serveur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2701]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2701) **Erreur en consultation des utilisations d&#39;un référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2703]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2703) **Assistant traduction, le bouton Export (du fichier) ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2704]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2704) **Dans l&#39;arbre de sélection de marée, si une marée est en cours d&#39;édition, son nœud parent n&#39;est pas bien identifié (il devrait être en gras)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2693]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2693) **Tant que la fonction corbeille de l&#39;UI de synchro avancée de référentiel n&#39;est pas 100% fiable, masquer les corbeilles** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2694]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2694) **On aimerait voir cet espace supprimé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2699]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2699) **Mapping du code &#39;avarie&#39; dans l&#39;importeur AVDTH non optimum** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.1.2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/265)

**Closed at 2023-05-12.**

### Download
* [Client (observe-9.1.2-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.2/observe-9.1.2-client.zip)
* [Serveur (observe-9.1.2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.2/observe-9.1.2.war)
* [Serveur (observe-9.1.2-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.2/observe-9.1.2-server.zip)

### Issues
  * [[Type::Anomalie 2688]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2688) **En LL / logbook / Opération de pêche, les boutons de recopie des caractéristiques des opérations précédentes sont indisponibles** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2687]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2687) **Dans le calcul des nombres estimés (PS observation), appliquer un arrondi standard** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2689]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2689) **Dans la consolidation des poids pondérés des cuves-calées, il faut effectuer un filtre sur une liste d&#39;espèces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.1.1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/264)

**Closed at 2023-05-10.**

### Download
* [Client (observe-9.1.1-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.1/observe-9.1.1-client.zip)
* [Serveur (observe-9.1.1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.1/observe-9.1.1.war)
* [Serveur (observe-9.1.1-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.1/observe-9.1.1-server.zip)

### Issues
  * [[Type::Anomalie 2680]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2680) **Erreur en sortie de form lors du renommage d&#39;un programme** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2681]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2681) **Impossible de saisir des captures LL observations en mode individuel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2683]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2683) **Sur les formulaires de composition globale de la palangre, les validations au niveau de chaque onglet de ces composantes essaye de valider la calée parent** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2686]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2686) **L&#39;accessibilité du champs temps depuis le déclanchement  n&#39;est pas correcte dans certains cas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2502]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2502) **Libellés UI sous Calée** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Evolution 2676]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2676) **Le marqueur ll_observations.catch.depredated prend trois valeurs, alors que les données sont en oui ou non** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2682]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2682) **Le champs ll_logbook.Catch.hookWhenDiscarded n&#39;est pas utilisé dans le formulaire dédié, que faire ?** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2685]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2685) **Passer certains éditeurs de booléen dans les formulaires pour gérer la nullité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.1.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/262)

**Closed at 2023-04-27.**

### Download
* [Client (observe-9.1.0-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.0/observe-9.1.0-client.zip)
* [Serveur (observe-9.1.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.0/observe-9.1.0.war)
* [Serveur (observe-9.1.0-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.0/observe-9.1.0-server.zip)

### Issues
  * [[Type::Anomalie 2667]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2667) **Un marée sur laquelle l&#39;assistant d&#39;appariement bloque** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2671]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2671) **Etat par défaut du bouton radio Saisie groupée / Saisie individuelle sur capture LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2677]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2677) **Souci de migration v9 d&#39;un commentaire** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2053]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2053) **Automatiser le calcul de la pondération de la calée dans la cuve échantillonnée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2666]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2666) **Sur un code AVDTH ASSOC inopportun, l&#39;importeur bloque avec une exception** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2668]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2668) **Petites améliorations sur la gestion des numéros de sous-échantillons** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2672]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2672) **Remplir le champs Vessel.cfrId (utilisé pour akado)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2674]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2674) **Dans les tableaux, remplacer les champs vides pour les nombres de valeur 0 par 0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2675]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2675) **Ajouter une colonne poids total (en t) sur le tableau des cuves calées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.1.0-RC-2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/259)

**Closed at 2023-04-01.**

### Download
* [Client (observe-9.1.0-RC-2-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.0-RC-2/observe-9.1.0-RC-2-client.zip)
* [Serveur (observe-9.1.0-RC-2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.0-RC-2/observe-9.1.0-RC-2.war)
* [Serveur (observe-9.1.0-RC-2-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.0-RC-2/observe-9.1.0-RC-2-server.zip)

### Issues
  * [[Type::Anomalie 2649]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2649) **Configuration de l&#39;arbre impossible, commandes grisées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2650]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2650) **topiaid non conforme dans common.lastupdatedate** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2646]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2646) **Erreur de nommage d&#39;un champ** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2647]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2647) **Ajout de champs sur Vessel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2655]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2655) **Vérifier les mappings de REF_OBSERVED_SYSTEM** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2659]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2659) **Ajout de deux champs dans le référentiel FpaZone (pour AKaDo)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.1.0-RC-1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/243)

**Closed at 2023-03-13.**

### Download
* [Client (observe-9.1.0-RC-1-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.0-RC-1/observe-9.1.0-RC-1-client.zip)
* [Serveur (observe-9.1.0-RC-1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.0-RC-1/observe-9.1.0-RC-1.war)
* [Serveur (observe-9.1.0-RC-1-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.1.0-RC-1/observe-9.1.0-RC-1-server.zip)

### Issues
  * [[Type::Anomalie 2546]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2546) **Quelques inexactitudes sur la persistence (découvert en voulant migrer vers Hibernate 6)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2218]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2218) **tms_version va-t-elle rester dans le schéma public ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2435]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2435) **Renommer l&#39;entité  common.LengthMeasureMethod en common.SizeMeasureMethod** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2478]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2478) **Réusinage du plan de cuve** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2600]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2600) **Nouvelle référence &#39;Refueling&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2602]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2602) **Table ll_common.weightdeterminationmethod inutile ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2612]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2612) **Gestion des FAD sur OPERA AVDTH non mappées vers VesselActivity avec allowFad&#61;TRUE** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2617]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2617) **Ajout de ps_common.TransmittingBuoyOperation 99** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2618]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2618) **Ajout de ps_landing.Destination 28 et 29** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2620]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2620) **Nettoyage de la table LastUpdateDate** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2628]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2628) **Sur les captures, suppression de la méthode de mesure de poids si le poids n&#39;est pas déclaré, ou qu&#39;il est calculé dans l&#39;action de consolidation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.0.31](https://gitlab.com/ultreiaio/ird-observe/-/milestones/263)

**Closed at *In progress*.**


### Issues
No issue.

## Version [9.0.30](https://gitlab.com/ultreiaio/ird-observe/-/milestones/261)

**Closed at 2023-04-01.**

### Download
* [Client (observe-9.0.30-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.30/observe-9.0.30-client.zip)
* [Serveur (observe-9.0.30.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.30/observe-9.0.30.war)
* [Serveur (observe-9.0.30-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.30/observe-9.0.30-server.zip)

### Issues
  * [[Type::Anomalie 2656]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2656) **Erreur sur déplacement de données d&#39;observation (la marée cible n&#39;est pas bien rafrachie dans l&#39;arbre de navigation)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2657]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2657) **Lorsque l&#39;on passe un type de données d&#39;un statut &#39;Réalisé&#39; à &#39;Non réalisé&#39;, il faudrait certainement mettre le programme de rattachement à null** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2658]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2658) **Lorsque l&#39;on réinitialise le programme d&#39;une marée, il faudrait certainement rafraichir l&#39;arbre** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2660]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2660) **Appariement des activités à la loupe** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2661]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2661) **Erreur d&#39;interprétation du référentiel AVDTH FP_TYP_COND** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2664]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2664) **Non déclenchement des recopies de méta-données lors de déplacement (impliquant une possible modification des méta-données de la marée)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2665]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2665) **Sur le nœud Observations embarquées, il est possible de tout déplacer même si le formulaire n&#39;est pas accessible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.0.29](https://gitlab.com/ultreiaio/ird-observe/-/milestones/260)

**Closed at 2023-03-17.**

### Download
* [Client (observe-9.0.29-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.29/observe-9.0.29-client.zip)
* [Serveur (observe-9.0.29.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.29/observe-9.0.29.war)
* [Serveur (observe-9.0.29-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.29/observe-9.0.29-server.zip)

### Issues
  * [[Type::Anomalie 2648]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2648) **Champ Vessel.comment non visible sur l&#39;UI** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2651]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2651) **Erreur Calendar.SetTime lors de l&#39;export d&#39;une base locale vers la base centrale** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2653]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2653) **Impossible de changer la date d&#39;une route** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.28](https://gitlab.com/ultreiaio/ird-observe/-/milestones/258)

**Closed at 2023-03-13.**

### Download
* [Client (observe-9.0.28-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.28/observe-9.0.28-client.zip)
* [Serveur (observe-9.0.28.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.28/observe-9.0.28.war)
* [Serveur (observe-9.0.28-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.28/observe-9.0.28-server.zip)

### Issues
  * [[Type::Anomalie 2638]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2638) **Messages en double dans le log de l&#39;assistant d&#39;export** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2639]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2639) **Pouvoir tester l&#39;ouverture d&#39;une base 9.1 avec une application 9.0.28** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2640]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2640) **Imperfection sur le rapport Observations - Dénombrement des captures selon le type d&#39;association, filtrées par groupe et mode (rejeté/conservé)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2641]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2641) **Problème de sérialisation sur le retour de la consolidation en mode serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2643]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2643) **La supression d&#39;un panier du schéma de palangre supprime toute la section** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2644]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2644) **Dernières améliorations sur l&#39;APi de consolidation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 2642]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2642) **Ajout d&#39;une documentation sur la consolidation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.0.27](https://gitlab.com/ultreiaio/ird-observe/-/milestones/257)

**Closed at 2023-03-09.**

### Download
* [Client (observe-9.0.27-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.27/observe-9.0.27-client.zip)
* [Serveur (observe-9.0.27.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.27/observe-9.0.27.war)
* [Serveur (observe-9.0.27-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.27/observe-9.0.27-server.zip)

### Issues
  * [[Type::Anomalie 2635]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2635) **Erreur si on quitte un formulaire (avec des modifications) pour aller vers un assistant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2636]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2636) **Problème sur les arrondis de nombres décimaux** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2637]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2637) **Validation - Amélioration du message sur une détection d&#39;entrée non unique dans une collection (on ajoute désormais le rendu de l&#39;objet validé)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.0.26](https://gitlab.com/ultreiaio/ird-observe/-/milestones/256)

**Closed at 2023-03-08.**

### Download
* [Client (observe-9.0.26-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.26/observe-9.0.26-client.zip)
* [Serveur (observe-9.0.26.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.26/observe-9.0.26.war)
* [Serveur (observe-9.0.26-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.26/observe-9.0.26-server.zip)

### Issues
  * [[Type::Anomalie 2629]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2629) **Revue de la précision lors des calcul de RTP et RTT** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2630]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2630) **Revue du code de l&#39;action de consolidation et amélioration de la sortie de l&#39;action** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2631]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2631) **Ajouter des options dans la configuration du client pour configurer l&#39;action de consolidation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2632]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2632) **Problème lors de la connexion à des bases distantes (la version du modèle de la première base testée est conservée ensuite comme version de modèle requis)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2633]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2633) **Supprimer de la génération des Dto de méthodes non utilisées (pour les propriétés à multiplicité n)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.0.25](https://gitlab.com/ultreiaio/ird-observe/-/milestones/255)

**Closed at 2023-02-14.**

### Download
* [Client (observe-9.0.25-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.25/observe-9.0.25-client.zip)
* [Serveur (observe-9.0.25.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.25/observe-9.0.25.war)
* [Serveur (observe-9.0.25-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.25/observe-9.0.25-server.zip)

### Issues
  * [[Type::Anomalie 2616]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2616) **Retirer observe.exe qui ne fonctionne pas et induit les utilisateurs en erreur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2625]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2625) **Champ manquant sur un formulaire échantillons** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2622]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2622) **[UI REFERENTIELS] Envisager un auto-trim droite et gauche sur les champs alphanumériques** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2623]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2623) **Amélioration de l&#39;éditeur des matériaux de DCP** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2627]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2627) **Sur le formulaire Captures, le champs méthode de mesure de poids doit être accessible uniquement si le poids est renseigné et non calculé** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)

## Version [9.0.24](https://gitlab.com/ultreiaio/ird-observe/-/milestones/254)

**Closed at 2023-01-25.**

### Download
* [Client (observe-9.0.24-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.24/observe-9.0.24-client.zip)
* [Serveur (observe-9.0.24.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.24/observe-9.0.24.war)
* [Serveur (observe-9.0.24-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.24/observe-9.0.24-server.zip)

### Issues
  * [[Type::Anomalie 2573]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2573) **Revoir le calcul et la mise à jour du type de banc de la calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2615]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2615) **Procédure de calcul qui ne détecte pas les données modifiées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2457]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2457) **La création d&#39;un objet sur une activité n&#39;ajoute pas le système observé 20/FOB comme attendu** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2485]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2485) **Revue de migration de Activity.schoolType** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2570]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2570) **Gestion du flag AVDTH F_DCP_ECO** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2575]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2575) **Revue de la migration des DCP depuis AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2610]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2610) **Revue du remplissage de activity.setCount** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2499]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2499) **Interrogations sur la localisation de certaines tables de ps_common** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2596]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2596) **Réduire la taille du livrable server-zip** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Documentation 2597]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2597) **Documentation de l&#39;outil de reporting** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.0.23](https://gitlab.com/ultreiaio/ird-observe/-/milestones/253)

**Closed at 2023-01-05.**

### Download
* [Client (observe-9.0.23-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.23/observe-9.0.23-client.zip)
* [Serveur (observe-9.0.23.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.23/observe-9.0.23.war)
* [Serveur (observe-9.0.23-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.23/observe-9.0.23-server.zip)

### Issues
  * [[Type::Anomalie 2579]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2579) **Impossible de créer une route en 9.0.22** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2580]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2580) **Lors de la suppression gloable d&#39;une capture utilisée sur le formulaire espèces sensibles, la demande de confirmation n&#39;est pas proposée (mais elle l&#39;est bien si l&#39;espèce est utilisée dans un échantillon)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2581]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2581) **Imperfection de la validation sur une activité de pêche** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2582]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2582) **Après création d&#39;une activité, le nœud captures n&#39;apparaît pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2584]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2584) **Les activités ne sont pas disponibles lors de la création d&#39;un échantillon (mais disponible en mode mis à jour)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2585]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2585) **Problème sur la sauvegarde d&#39;un échantillon (si on lui associe des activités)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2587]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2587) **Si on quitte le formulaire marée modifié, aucune alerte n&#39;est proposée pour enregistrer les modifications** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2589]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2589) **Sur un formulaire tableau modifié, la demande confirmation est demandée deux fois si la première fois, on dit annuler** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2591]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2591) **Mauvaise persistence des lots au niveau des sondages** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2592]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2592) **Sur le formulaire lot, le champs commentaire n&#39;est pas lié à un commentaire de la marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2577]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2577) **Améliorer les variables dans les rapports pour proposer les variables suivantes en filtrant sur la variable précédente** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2578]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2578) **Revoir la syntaxe des rapports** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2586]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2586) **Amélioration du texte d&#39;information quand on ne peut pas encore remplir le marché local** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2590]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2590) **Ajout sur le formulaire d&#39;une information sur la saisie possible ou non du marché local à partir du port d&#39;arrivée sélectionné** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2595]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2595) **Améliorations de libellés de l&#39;arbre** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)

## Version [9.0.22](https://gitlab.com/ultreiaio/ird-observe/-/milestones/252)

**Closed at 2022-12-12.**

### Download
* [Client (observe-9.0.22-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.22/observe-9.0.22-client.zip)
* [Serveur (observe-9.0.22.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.22/observe-9.0.22.war)
* [Serveur (observe-9.0.22-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.22/observe-9.0.22-server.zip)

### Issues
  * [[Type::Anomalie 2551]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2551) **Rapport LB - Vérification croisée des débarquements, colonnes cuves vides erronées** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2571]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2571) **Une BD sql.gz qui génère une corruption de données systématique - Notre espoir de comprendre le problème ?** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2574]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2574) **Lors de la sauvegarde d&#39;une activité, l&#39;arbre est replié mais surtout on perd sur ces fils les statistiques** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2543]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2543) **Migration IEO** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2550]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2550) **Lorsque que l&#39;on utilise la fonction Précédent de l&#39;assistants rapports, il est possible de lancer les rapports sur des marées non calculées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2552]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2552) **Optimisation des rapports Captures de thons selon le type d&#39;association et Rejets de thons selon le type d&#39;association** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2553]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2553) **Rapport Observations - Dénombrement des captures par devenir, filtrés par groupe** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2554]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2554) **Besoin d&#39;un rapport Observations fournissant les captures par devenir, en poids** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2555]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2555) **Besoin d&#39;un rapport Observations fournissant les poids par raisons de rejets** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2568]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2568) **Gestion des codes AVDTH 40 et 41 par le migrateur AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2569]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2569) **Gestion de la propriété de la balise (F_PROP_BALISE) en situation de migration AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.21](https://gitlab.com/ultreiaio/ird-observe/-/milestones/251)

**Closed at 2022-11-30.**

### Download
* [Client (observe-9.0.21-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.21/observe-9.0.21-client.zip)
* [Serveur (observe-9.0.21.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.21/observe-9.0.21.war)
* [Serveur (observe-9.0.21-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.21/observe-9.0.21-server.zip)

### Issues
  * [[Type::Anomalie 2544]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2544) **Problème d&#39;ouverture de base de donnée (centrale), lié à l&#39;état précédent de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2545]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2545) **Bug sur report Observations - Dénombrement des captures par devenir, filtrés par groupe (sur une base postgresql)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2547]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2547) **Le temps de pêche est doublé par rapport à sa valeur AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2548]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2548) **Gestion des associations (Observed Systems) par la migration AVDTH** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)

## Version [9.0.20](https://gitlab.com/ultreiaio/ird-observe/-/milestones/250)

**Closed at 2022-11-25.**

### Download
* [Client (observe-9.0.20-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.20/observe-9.0.20-client.zip)
* [Serveur (observe-9.0.20.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.20/observe-9.0.20.war)
* [Serveur (observe-9.0.20-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.20/observe-9.0.20-server.zip)

### Issues
No issue.

## Version [9.0.19](https://gitlab.com/ultreiaio/ird-observe/-/milestones/249)

**Closed at 2022-11-23.**

### Download
* [Client (observe-9.0.19-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.19/observe-9.0.19-client.zip)
* [Serveur (observe-9.0.19.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.19/observe-9.0.19.war)
* [Serveur (observe-9.0.19-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.19/observe-9.0.19-server.zip)

### Issues
  * [[Type::Anomalie 2541]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2541) **Erreur d&#39;affichage du form PS/Observations/Echantillons** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.18](https://gitlab.com/ultreiaio/ird-observe/-/milestones/248)

**Closed at 2022-11-23.**

### Download
* [Client (observe-9.0.18-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.18/observe-9.0.18-client.zip)
* [Serveur (observe-9.0.18.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.18/observe-9.0.18.war)
* [Serveur (observe-9.0.18-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.18/observe-9.0.18-server.zip)

### Issues
  * [[Type::Anomalie 2540]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2540) **Vérifier que les quadrants océans sont bien initialisés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2537]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2537) **Migration AZTI** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2538]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2538) **Restreindre les devenirs possibles des échantillons aux devenirs de l&#39;espèce saisis dans les captures** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2539]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2539) **Replay Sample migration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.0.17](https://gitlab.com/ultreiaio/ird-observe/-/milestones/245)

**Closed at 2022-11-16.**

### Download
* [Client (observe-9.0.17-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.17/observe-9.0.17-client.zip)
* [Serveur (observe-9.0.17.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.17/observe-9.0.17.war)
* [Serveur (observe-9.0.17-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.17/observe-9.0.17-server.zip)

### Issues
  * [[Type::Anomalie 2448]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2448) **Dans l&#39;UI de recherche, les dates ne sont pas ordonnées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2496]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2496) **Sur l&#39;UI de gestion des listes d&#39;affichage d&#39;espèces, un contrôle récalcitrant sur espèces désactivées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2503]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2503) **Sur échantillons PS observations saisis par lots, migrés vers 9.0, valeurs étranges sur le champ poids individuel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2507]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2507) **La vérification avant sauvegarde d&#39;un référentiel désactivé n&#39;est plus en place** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2508]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2508) **Pouvoir de nouveau éditer le référentiel d&#39;une base locale en utilisant l&#39;option de configuration déjà existante** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2510]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2510) **Dans l&#39;import AVDTH, les longitudes extrêmes &gt;90° ou &gt;100° semblent mal gérées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2511]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2511) **Pas mal de positions géographiques sont mises à NULL/NULL par l&#39;importeur AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2528]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2528) **Disparition des TargetCatch avec discarded&#61;FALSE dans la nouvelle migration V9** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2531]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2531) **Adaptation requises dans les rapports PS observations suite au regroupement de TargetCatch et NonTargetCatch** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2534]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2534) **Rapport Dénombrement des captures accessoires et devenir, filtrées par groupe** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1544]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1544) **[UI REFERENTIELS] Envisager un auto-trim droite et gauche sur les champs alphanumériques** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2459]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2459) **En insertion de marée par le service web, les nouveaux topiaid générés ont un nouveau format. Est-il voulu et fiable ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2492]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2492) **Ajout une contrainte de non nullité sur PS_OBS_Catch.speciesFate** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2504]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2504) **Souci d&#39;unicité sur le formPS observations / captures** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2505]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2505) **Générer les validateurs sur l&#39;unicité des clef métier de collections** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2512]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2512) **Labellisation du référentiel &#39;Type de données des propriétés/matériaux de FOB** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2513]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2513) **Amélioration de libellés du formulaire captures** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2515]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2515) **Replay de la migration V9 captures** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2526]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2526) **Adaptation de la validation et du formulaire Captures suite à la nouvelle migration v9** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2527]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2527) **Décoration des devenirs sur formulaire captures** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2529]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2529) **Nettoyage par migration de tous les champs texte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2530]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2530) **Rapports Captures/Rejets selon le type d&#39;association** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2532]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2532) **Rapport Liste des captures de faune accessoire selon le type de banc, filtrées par groupe** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2533]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2533) **Rapports Distributions de tailles** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2535]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2535) **Ordonner les colonnes du tableau captures selon l&#39;ordre des champs sur le formulaire** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2480]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2480) **Les touches de recourcis F3, F4, F5... exigent que le focus soit déjà sur leur panel pour fonctionner** (Thanks to ) (Reported by Pascal Cauquil)
  * [[Type::Documentation 2432]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2432) **Quel nom de rôle recherche l&#39;assistant sécurité pour attribuer le nouveau rôle &#39;Opérateur de données&#39; ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.16](https://gitlab.com/ultreiaio/ird-observe/-/milestones/242)

**Closed at 2022-10-24.**

### Download
* [Client (observe-9.0.16-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.16/observe-9.0.16-client.zip)
* [Serveur (observe-9.0.16.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.16/observe-9.0.16.war)
* [Serveur (observe-9.0.16-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.16/observe-9.0.16-server.zip)

### Issues
  * [[Type::Anomalie 2483]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2483) **Création relation taille-poids impossible - champ espèce est grisé** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 2484]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2484) **Le schooltype devrait être obligatoire en cas d&#39;activité PS logbook de type 6 - Pêche** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2486]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2486) **problème de sauvegarde des échantillons ps logbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2487]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2487) **L&#39;ajout d&#39;un référentiel ne met pas à jour l&#39;arbre de navigation** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2494]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2494) **Formulaire équipements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2488]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2488) **Quadrants des océans Méditerranée et Multiples non initialisés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2489]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2489) **Meilleur gestion des raccourcis clavier dans le gestionnaire de connexions et du focus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2491]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2491) **On a pu créer une capture sans devenir** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2493]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2493) **Lorsque l&#39;on veut créer une nouvelle donnée, on fait **F3**, on peut alors s&#39;affranchir de choisir quoi créer si une seule possibilité existe (cela evite un clic)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.0.15](https://gitlab.com/ultreiaio/ird-observe/-/milestones/241)

**Closed at 2022-10-18.**

### Download
* [Client (observe-9.0.15-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.15/observe-9.0.15-client.zip)
* [Serveur (observe-9.0.15.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.15/observe-9.0.15.war)
* [Serveur (observe-9.0.15-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.15/observe-9.0.15-server.zip)

### Issues
  * [[Type::Anomalie 2476]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2476) **L&#39;enregistrement du schéma détaillé de palangre plante après modification si des captures (ou des TDR) lui sont associés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.14](https://gitlab.com/ultreiaio/ird-observe/-/milestones/240)

**Closed at 2022-10-18.**

### Download
* [Client (observe-9.0.14-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.14/observe-9.0.14-client.zip)
* [Serveur (observe-9.0.14.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.14/observe-9.0.14.war)
* [Serveur (observe-9.0.14-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.14/observe-9.0.14-server.zip)

### Issues
  * [[Type::Anomalie 2460]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2460) **Le form composition détaillée LL observations ne fonctionne plus en mode serveur uniquement** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2461]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2461) **Libellés des heures jalon des calées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2462]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2462) **Clés de libellés non traduites** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2463]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2463) **Lorsqu&#39;un template de composition détaillé LL est invalide, l&#39;UI permet malgré tout d&#39;exécuter la génération, qui produit une erreur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2464]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2464) **Imperfection d&#39;affichage dans la liste déroulante des templates de génération LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2465]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2465) **Validateur de template de génération LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2468]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2468) **Problème JSON en consultation mode serveur du schéma détaillé de palangre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2469]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2469) **Mesures sur captures LL observations non affichées dans l&#39;UI** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2473]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2473) **Restaurer la possibilité d&#39;afficher les relations taille-poids et taille-taille par code FAO** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2451]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2451) **Passer le flag saisie en &#39;non autorisée&#39; sur le statut de collecte &#39;999 - Inconnu&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2466]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2466) **Ajout d&#39;une information sur la syntaxe des templates LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2467]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2467) **Rajouter le bouton de réinitialisation sur le champs heure de début de coulissage** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2474]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2474) **Agrandir la zone de texte Source sur les référentiels taille-poids et taille-taille** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.13](https://gitlab.com/ultreiaio/ird-observe/-/milestones/239)

**Closed at 2022-10-11.**

### Download
* [Client (observe-9.0.13-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.13/observe-9.0.13-client.zip)
* [Serveur (observe-9.0.13.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.13/observe-9.0.13.war)
* [Serveur (observe-9.0.13-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.13/observe-9.0.13-server.zip)

### Issues
  * [[Type::Anomalie 2458]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2458) **Le formulaire échantillon plante à l&#39;enregistrement** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.12](https://gitlab.com/ultreiaio/ird-observe/-/milestones/238)

**Closed at 2022-10-10.**

### Download
* [Client (observe-9.0.12-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.12/observe-9.0.12-client.zip)
* [Serveur (observe-9.0.12.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.12/observe-9.0.12.war)
* [Serveur (observe-9.0.12-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.12/observe-9.0.12-server.zip)

### Issues
  * [[Type::Anomalie 2453]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2453) **Dysfonctionnement du formulaire échantillon, saisie des échantillons impossible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2454]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2454) **observe.exe ne fonctionne pas, le supprimer ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2456]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2456) **Lorsque l&#39;on crée une nouvelle activité en séquence d&#39;une autre, le form vide se positionne sur le dernier onglet utilisé sur l&#39;activité précédente** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2455]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2455) **Sur une capture, exiger le champ origine de l&#39;information** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.11](https://gitlab.com/ultreiaio/ird-observe/-/milestones/237)

**Closed at 2022-10-06.**

### Download
* [Client (observe-9.0.11-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.11/observe-9.0.11-client.zip)
* [Serveur (observe-9.0.11.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.11/observe-9.0.11.war)
* [Serveur (observe-9.0.11-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.11/observe-9.0.11-server.zip)

### Issues
  * [[Type::Anomalie 2445]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2445) **Problème d&#39;import AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2447]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2447) **Un référentiel présent indiqué comme absent par l&#39;importeur AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2446]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2446) **Ajout de références ObservedSystem** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.10](https://gitlab.com/ultreiaio/ird-observe/-/milestones/236)

**Closed at 2022-10-02.**

### Download
* [Client (observe-9.0.10-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.10/observe-9.0.10-client.zip)
* [Serveur (observe-9.0.10.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.10/observe-9.0.10.war)
* [Serveur (observe-9.0.10-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.10/observe-9.0.10-server.zip)

### Issues
  * [[Type::Anomalie 2072]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2072) **Importeur AVDTH - bouton** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2438]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2438) **La synchro simple de référentiel ne fonctionne toujours pas en 9.0.9** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2439]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2439) **Une fois la base locale fermée, on ne peut plus la rouvrir (la base vient juste d&#39;être créé depuis un référentiel distant)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2440]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2440) **NPE sur la création d&#39;une nouvelle base centrale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2441]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2441) **Si on change la modalité d&#39;une marée, il se peut que l&#39;interface graphique plante** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2444]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2444) **Problème dans la synchronisation simple de référentiels - les désactivations sont executées après les suppressions...** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2442]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2442) **Supprimer vesselActivity 28 via la migration qui a été ajouté en 8.1 pour rien... + correction du nouveau système observé 110** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2443]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2443) **Revue de la génération des validateurs (ils sont désormais dans les sources du projet)** (Thanks to ) (Reported by Tony CHEMIT)

## Version [9.0.9](https://gitlab.com/ultreiaio/ird-observe/-/milestones/235)

**Closed at 2022-10-01.**

### Download
* [Client (observe-9.0.9-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.9/observe-9.0.9-client.zip)
* [Serveur (observe-9.0.9.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.9/observe-9.0.9.war)
* [Serveur (observe-9.0.9-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.9/observe-9.0.9-server.zip)

### Issues
  * [[Type::Evolution 2429]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2429) **Migration AVDTH des activités de type &#39;14 - Chavire la poche&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2437]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2437) **Normalisation des dates dans l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.0.8](https://gitlab.com/ultreiaio/ird-observe/-/milestones/234)

**Closed at 2022-09-30.**

### Download
* [Client (observe-9.0.8-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.8/observe-9.0.8-client.zip)
* [Serveur (observe-9.0.8.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.8/observe-9.0.8.war)
* [Serveur (observe-9.0.8-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.8/observe-9.0.8-server.zip)

### Issues
  * [[Type::Anomalie 2431]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2431) **L&#39;assistant sécurité 9.0.7 ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2433]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2433) **La synchronisation simple de la base locale ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2434]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2434) **L&#39;importeur AVDTH plante en fin de processus** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2436]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2436) **Souci d&#39;affichage (probablement d&#39;index) dans les échantillons PS après migration V9** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.7](https://gitlab.com/ultreiaio/ird-observe/-/milestones/232)

**Closed at 2022-08-23.**

### Download
* [Client (observe-9.0.7-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.7/observe-9.0.7-client.zip)
* [Serveur (observe-9.0.7.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.7/observe-9.0.7.war)
* [Serveur (observe-9.0.7-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.7/observe-9.0.7-server.zip)

### Issues
  * [[Type::Anomalie 2324]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2324) **Ouverture de base impossible selon configuration précédente de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2327]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2327) **Clé de libellé non traduite en anglais sur la page de synchrod e la synchro avancée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2340]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2340) **La taille de la fenêtre de configuration de l&#39;arbre de navigation tronque les boutons du bas si on passe du module PS vers LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2381]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2381) **Connection manager, bases distantes, champs non visibles sous Windows** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2399]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2399) **Problème SSL - en attente - ne s&#39;est pas reproduit** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2411]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2411) **HResponseErrorException / InvalidAuthenticationTokenException en 9.0.6 en quittant l&#39;application** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2412]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2412) **Certains services sont encore en GET alors qu&#39;ils devraient être en POST** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2415]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2415) **Erreur lors du rechargement de la source si token expiré** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2416]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2416) **La boîte &#39;A propos&#39; ne fontionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2419]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2419) **Erreur de migration de l&#39;espèce AVDTH 8 de CAPT_ELEM vers ps_logbook.catch** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2420]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2420) **Statut de la doc de mapping des espèces AVDTH/ObServe** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2423]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2423) **Erreur calcul des données sur V9** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 2427]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2427) **Erreur de type de mesure sur les échantillons AVDTH migrés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2428]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2428) **Problème lors d&#39;un déplacement de toutes les données d&#39;une marée Senne** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1248]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1248) **Mise à jour du module cartographie avec les nouvelles données** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1751]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1751) **Mettre un sablier lors de l&#39;ouverture si le nœud a beaucoup de données à charger.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1866]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1866) **Remplir les données depuis AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2104]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2104) **On pourrait avoir un service de génération de topiaid** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2223]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2223) **Ajouter une action nouvelle marée dans l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2225]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2225) **Bien gérer le repositionnement dans l&#39;arbre de navigation d&#39;une marée suite à une sauvegarde si le critère a été modifié** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2232]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2232) **Dans l&#39;UI des profils de connexion, trimmer les espaces** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2259]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2259) **Dans l&#39;assistant rapport, ajouter un export CSV** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2293]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2293) **Il serait intéressant de permettre l&#39;absence de base déclarée par défaut** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2343]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2343) **Sur les données LL port et associées, rajouter 2 nouveaux champs data entry operators** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2345]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2345) **Ajouter un champ position sur le formulaire Landing LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2366]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2366) **UI de gestion des associations espèce/océan pas disponible sur Océan** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2370]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2370) **Il faudrait un type d&#39;accès en écriture mais qui empêche la modification des références** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2378]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2378) **Convertisseur de longueur m/km/nm/ftm** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2395]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2395) **Titre de l&#39;appli** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2409]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2409) **Aucune barre de progression** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2410]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2410) **Ordonancement par défaut des 3 combos section, basket, avançon sur les observations** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2417]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2417) **Improve statistics API** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2418]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2418) **Contournements des soucis mémoire de l&#39;importeru AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2421]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2421) **Traitement de RAV** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2422]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2422) **Il faudrait mettre à jour le script qui crée les colonnes postgis pour chaque champ lat/lon** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2425]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2425) **RAV*, quelques détails** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2426]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2426) **Avoir des retours d&#39;expérience possibles sur les actions de maintenance de bases distantes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [9.0.6](https://gitlab.com/ultreiaio/ird-observe/-/milestones/231)

**Closed at 2022-06-20.**

### Download
* [Client (observe-9.0.6-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.6/observe-9.0.6-client.zip)
* [Serveur (observe-9.0.6.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.6/observe-9.0.6.war)
* [Serveur (observe-9.0.6-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.6/observe-9.0.6-server.zip)

### Issues
  * [[Type::Anomalie 2005]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2005) **Erreur d&#39;ajout de trip ( methode POST)** (Thanks to Tony CHEMIT) (Reported by adelphe n&#39;goran)
  * [[Type::Anomalie 2283]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2283) **Nom du fichier de log constant sur le serveur quelque soit la version installée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2335]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2335) **En cas de perte de connexion, le message est un peu violent** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2400]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2400) **Toujours un petit problème d&#39;accessibilité de l&#39;action Enregistrer sur les formulaires de type tableau** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2402]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2402) **Exception** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2403]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2403) **Erreur en quittant le gestionnaire de connexions** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2404]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2404) **INDEX en doublons ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2405]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2405) **Erreur sur les commentaires du référentiel Type de balise sélectionné** (Thanks to adelphe n&#39;goran) (Reported by adelphe n&#39;goran)
  * [[Type::Anomalie 2407]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2407) **Revue et correction du cache de contextes de persistance** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2408]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2408) **Avec java 17 les erreurs sont mal remontée depuis le serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2406]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2406) **Préfixes des statistiques dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.5](https://gitlab.com/ultreiaio/ird-observe/-/milestones/230)

**Closed at 2022-06-12.**

### Download
* [Client (observe-9.0.5-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.5/observe-9.0.5-client.zip)
* [Serveur (observe-9.0.5.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.5/observe-9.0.5.war)
* [Serveur (observe-9.0.5-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.5/observe-9.0.5-server.zip)

### Issues
  * [[Type::Anomalie 2398]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2398) **Exception sur consultation des UI referentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.4](https://gitlab.com/ultreiaio/ird-observe/-/milestones/229)

**Closed at 2022-06-10.**

### Download
* [Client (observe-9.0.4-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.4/observe-9.0.4-client.zip)
* [Serveur (observe-9.0.4.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.4/observe-9.0.4.war)
* [Serveur (observe-9.0.4-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.4/observe-9.0.4-server.zip)

### Issues
  * [[Type::Anomalie 2396]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2396) **Exceptions au premier démarrage sur une nouvelle source de données** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2397]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2397) **Exceptions liée aux nouvelles statistiques** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.3](https://gitlab.com/ultreiaio/ird-observe/-/milestones/228)

**Closed at 2022-06-08.**

### Download
* [Client (observe-9.0.3-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.3/observe-9.0.3-client.zip)
* [Serveur (observe-9.0.3.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.3/observe-9.0.3.war)
* [Serveur (observe-9.0.3-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.3/observe-9.0.3-server.zip)

### Issues
  * [[Type::Anomalie 2394]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2394) **Plantage de la migration 9.0 (avec l&#39;appli 9.0.2)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/227)

**Closed at 2022-06-06.**

### Download
* [Client (observe-9.0.2-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.2/observe-9.0.2-client.zip)
* [Serveur (observe-9.0.2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.2/observe-9.0.2.war)
* [Serveur (observe-9.0.2-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.2/observe-9.0.2-server.zip)

### Issues
  * [[Type::Anomalie 2241]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2241) **Petit souci de mise en page sur le form FOB matériaux** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2285]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2285) **Erreurs de validation retournées par le service web, dont on ne trouve pas la cause dans le content** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2369]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2369) **Erreur sur form LL Logbook / sample SpeciesDto.getMinLength() et InvalidAuthenticationTokenException** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2371]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2371) **Le message d&#39;erreur de proportion sur les Baits n&#39;apparaît pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2376]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2376) **Sur certains forms à double bouton &#39;Save&#39; le bouton &#39;Save&#39; en base est disponible alors que le tableau n&#39;est pas encore enregistrer, ce qui fait planter** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2379]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2379) **Clic sur &#39;Save&#39; avant clic sur &#39;Insert&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2384]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2384) **Lors de création d&#39;une marée PS, les forms de saisie ne sont pas rendus accessibles même si les status de saisie ont été correctement initialisés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2386]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2386) **PS / Logbook / Catch, déclenchement perfectible du bouton &#39;Save&#39;** (Thanks to ) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2344]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2344) **Ajouter des stats permettant de mieux repérer les samples attachés aux activités** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2372]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2372) **En anglais, afficher les unités dans les libellés sur les écrans global composition** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2373]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2373) **Sur les global composition du logbook, rendre les proportions optionnelles** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2375]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2375) **En anglais, afficher les unités sur LL / Logbook / Activity (+ libellé)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2377]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2377) **En anglais, afficher les unités sur Set / Characteristics** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2382]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2382) **Petit renommage sur le form LL / Logbook / Landing** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2383]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2383) **Petits renommages sur le form LL / Logbook /** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2385]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2385) **PS / Logbooks / Capture, le champ poids ne doit pas être exigé si le nombre est saisi** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2387]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2387) **Ajouter le bouton &#39;Coller la coordonnée d&#39;origine&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2388]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2388) **Trois rafinements sur l&#39;expresion régulière par défaut de cuves des bateaux** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/226)

**Closed at 2022-05-19.**

### Download
* [Client (observe-9.0.1-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.1/observe-9.0.1-client.zip)
* [Serveur (observe-9.0.1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.1/observe-9.0.1.war)
* [Serveur (observe-9.0.1-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.1/observe-9.0.1-server.zip)

### Issues
  * [[Type::Anomalie 2334]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2334) **Comportement de la carte, activités masquées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2337]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2337) **Comportement de l&#39;assistant connexion après un première échact de connexion** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2351]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2351) **Sur le form trip LL, le bouton &#39;Save&#39; ne détecte pas bien tous les événements de changements de valeurs** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2356]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2356) **Exception sur form LL logbook / Global Composition / Branchline** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2357]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2357) **Sur Global composition / Hook, the bouton &#39;Save&#39; n&#39;est pas activé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2358]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2358) **Global composition / Floatlines, Bouton &#39;Save&#39; non déclenché sur mise à jour d&#39;une valeur du tableau** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2361]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2361) **Sur le form Sample (attaché à une activité), on peut cliquer sur &#39;Save&#39; prématurément, ce qui le fait planter** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2364]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2364) **Quandrant de Méditérannée et Multiples non initialisés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2333]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2333) **Rajouter sur l&#39;arbre des critères de classement relatifs aux bateaux** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2338]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2338) **Comportement de l&#39;arbre quand à l&#39;affichage des références** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2339]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2339) **Amélioration des identifiants générés lors d&#39;un CREATE ou UPDATE depuis le service public** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2342]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2342) **Sur l&#39;UI, sur les traductions anglaises, changer tous les &#39;Harbour&#39; en &#39;Port&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2346]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2346) **Renommer le noeud Landing de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2348]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2348) **Questions autour de la création d&#39;un échantillon directement depuis le noeud Activity** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2350]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2350) **Une coquille s&#39;est glisée dans le code de migration V9** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2352]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2352) **Augmenter le nombre total d&#39;hameçons** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2353]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2353) **Petites améliorations sur l&#39;onglet de choix des espèces ciblées par la marée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2354]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2354) **Nombre maximum de lightsticks à augmenter** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2355]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2355) **Bouton de recopie de valeurs d&#39;une activité à l&#39;autre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2359]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2359) **Valeurs maxi sur Branchline et Floatline** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2365]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2365) **Libellé LL / Set à renommer en Fishing Operation** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2367]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2367) **Vérifier quel critère est utilisé pour filtrer la liste des bateaux disponibles sur Trip** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2368]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2368) **Mise en page et traductions dans l&#39;UI de réglage de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/222)

**Closed at 2022-05-09.**

### Download
* [Client (observe-9.0.0-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0/observe-9.0.0-client.zip)
* [Serveur (observe-9.0.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0/observe-9.0.0.war)
* [Serveur (observe-9.0.0-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0/observe-9.0.0-server.zip)

### Issues
  * [[Type::Anomalie 2330]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2330) **Le gestionnaire de connexion directe a une petite imperfection** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2242]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2242) **Définition des rapports LL simples** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2295]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2295) **Sur échantillons PS, un contrôle de validation imparfait** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2332]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2332) **Nouveau critère de classement pour l&#39;arbre : type de bateau** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [9.0.0-RC](https://gitlab.com/ultreiaio/ird-observe/-/milestones/181)

**Closed at 2022-05-08.**

### Download
* [Application (observe-9.0.0-RC-31-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-31/observe-9.0.0-RC-31.zip)
* [Serveur (observe-9.0.0-RC-31-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-31/observe-9.0.0-RC-31.war)
* [Serveur (war) (observe-9.0.0-RC-31.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-31/observe-9.0.0-RC-31.war)
* [Application (observe-9.0.0-RC-30-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-30/observe-9.0.0-RC-30.zip)
* [Serveur (observe-9.0.0-RC-30-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-30/observe-9.0.0-RC-30.war)
* [Serveur (war) (observe-9.0.0-RC-30.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-30/observe-9.0.0-RC-30.war)
* [Application (observe-9.0.0-RC-29-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-29/observe-9.0.0-RC-29.zip)
* [Serveur (observe-9.0.0-RC-29-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-29/observe-9.0.0-RC-29.war)
* [Serveur (war) (observe-9.0.0-RC-29.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-29/observe-9.0.0-RC-29.war)
* [Application (observe-9.0.0-RC-28-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-28/observe-9.0.0-RC-28.zip)
* [Serveur (observe-9.0.0-RC-28-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-28/observe-9.0.0-RC-28.war)
* [Serveur (war) (observe-9.0.0-RC-28.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-28/observe-9.0.0-RC-28.war)
* [Application (observe-9.0.0-RC-27-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-27/observe-9.0.0-RC-27.zip)
* [Serveur (observe-9.0.0-RC-27-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-27/observe-9.0.0-RC-27.war)
* [Serveur (war) (observe-9.0.0-RC-27.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-27/observe-9.0.0-RC-27.war)
* [Application (observe-9.0.0-RC-26-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-26/observe-9.0.0-RC-26.zip)
* [Serveur (observe-9.0.0-RC-26-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-26/observe-9.0.0-RC-26.war)
* [Serveur (war) (observe-9.0.0-RC-26.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-26/observe-9.0.0-RC-26.war)
* [Application (observe-9.0.0-RC-25-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-25/observe-9.0.0-RC-25.zip)
* [Serveur (observe-9.0.0-RC-25-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-25/observe-9.0.0-RC-25.war)
* [Serveur (war) (observe-9.0.0-RC-25.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-25/observe-9.0.0-RC-25.war)
* [Application (observe-9.0.0-RC-24-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-24/observe-9.0.0-RC-24.zip)
* [Serveur (observe-9.0.0-RC-24-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-24/observe-9.0.0-RC-24.war)
* [Serveur (war) (observe-9.0.0-RC-24.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-24/observe-9.0.0-RC-24.war)
* [Application (observe-9.0.0-RC-23-client.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-23/observe-9.0.0-RC-23.zip)
* [Serveur (observe-9.0.0-RC-23-server.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-23/observe-9.0.0-RC-23.war)
* [Serveur (war) (observe-9.0.0-RC-23.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-23/observe-9.0.0-RC-23.war)
* [Application (observe-9.0.0-RC-22.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-22/observe-9.0.0-RC-22.zip)
* [Serveur (observe-9.0.0-RC-22.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-22/observe-9.0.0-RC-22.war)
* [Serveur documentation (observe-9.0.0-RC-22-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-22/observe-9.0.0-RC-22-doc.zip)
* [Application (observe-9.0.0-RC-21.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-21/observe-9.0.0-RC-21.zip)
* [Serveur (observe-9.0.0-RC-21.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-21/observe-9.0.0-RC-21.war)
* [Serveur documentation (observe-9.0.0-RC-21-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-21/observe-9.0.0-RC-21-doc.zip)
* [Application (observe-9.0.0-RC-20.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-20/observe-9.0.0-RC-20.zip)
* [Serveur (observe-9.0.0-RC-20.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-20/observe-9.0.0-RC-20.war)
* [Serveur documentation (observe-9.0.0-RC-20-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-20/observe-9.0.0-RC-20-doc.zip)
* [Application (observe-9.0.0-RC-19.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-19/observe-9.0.0-RC-19.zip)
* [Serveur (observe-9.0.0-RC-19.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-19/observe-9.0.0-RC-19.war)
* [Serveur documentation (observe-9.0.0-RC-19-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-19/observe-9.0.0-RC-19-doc.zip)
* [Application (observe-9.0.0-RC-18.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-18/observe-9.0.0-RC-18.zip)
* [Serveur (observe-9.0.0-RC-18.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-18/observe-9.0.0-RC-18.war)
* [Serveur documentation (observe-9.0.0-RC-18-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-18/observe-9.0.0-RC-18-doc.zip)
* [Application (observe-9.0.0-RC-17.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-17/observe-9.0.0-RC-17.zip)
* [Serveur (observe-9.0.0-RC-17.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-17/observe-9.0.0-RC-17.war)
* [Serveur documentation (observe-9.0.0-RC-17-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-17/observe-9.0.0-RC-17-doc.zip)
* [Application (observe-9.0.0-RC-16.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-16/observe-9.0.0-RC-16.zip)
* [Serveur (observe-9.0.0-RC-16.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-16/observe-9.0.0-RC-16.war)
* [Serveur documentation (observe-9.0.0-RC-16-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-16/observe-9.0.0-RC-16-doc.zip)
* [Application (observe-9.0.0-RC-15.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-15/observe-9.0.0-RC-15.zip)
* [Serveur (observe-9.0.0-RC-15.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-15/observe-9.0.0-RC-15.war)
* [Serveur documentation (observe-9.0.0-RC-15-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-15/observe-9.0.0-RC-15-doc.zip)
* [Application (observe-9.0.0-RC-14.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-14/observe-9.0.0-RC-14.zip)
* [Serveur (observe-9.0.0-RC-14.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-14/observe-9.0.0-RC-14.war)
* [Serveur documentation (observe-9.0.0-RC-14-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-14/observe-9.0.0-RC-14-doc.zip)
* [Application (observe-9.0.0-RC-13.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-13/observe-9.0.0-RC-13.zip)
* [Serveur (observe-9.0.0-RC-13.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-13/observe-9.0.0-RC-13.war)
* [Serveur documentation (observe-9.0.0-RC-13-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-13/observe-9.0.0-RC-13-doc.zip)
* [Application (observe-9.0.0-RC-12.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-12/observe-9.0.0-RC-12.zip)
* [Serveur (observe-9.0.0-RC-12.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-12/observe-9.0.0-RC-12.war)
* [Serveur documentation (observe-9.0.0-RC-12-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-12/observe-9.0.0-RC-12-doc.zip)
* [Application (observe-9.0.0-RC-11.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-11/observe-9.0.0-RC-11.zip)
* [Serveur (observe-9.0.0-RC-11.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-11/observe-9.0.0-RC-11.war)
* [Serveur documentation (observe-9.0.0-RC-11-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-11/observe-9.0.0-RC-11-doc.zip)
* [Application (observe-9.0.0-RC-10.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-10/observe-9.0.0-RC-10.zip)
* [Serveur (observe-9.0.0-RC-10.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-10/observe-9.0.0-RC-10.war)
* [Serveur documentation (observe-9.0.0-RC-10-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-10/observe-9.0.0-RC-10-doc.zip)
* [Application (observe-9.0.0-RC-9.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-9/observe-9.0.0-RC-9.zip)
* [Serveur (observe-9.0.0-RC-9.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-9/observe-9.0.0-RC-9.war)
* [Serveur documentation (observe-9.0.0-RC-9-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-9/observe-9.0.0-RC-9-doc.zip)
* [Application (observe-9.0.0-RC-8.1.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-8.1/observe-9.0.0-RC-8.1.zip)
* [Serveur (observe-9.0.0-RC-8.1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-8.1/observe-9.0.0-RC-8.1.war)
* [Serveur documentation (observe-9.0.0-RC-8.1-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-8.1/observe-9.0.0-RC-8.1-doc.zip)
* [Application (observe-9.0.0-RC-8.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-8/observe-9.0.0-RC-8.zip)
* [Serveur (observe-9.0.0-RC-8.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-8/observe-9.0.0-RC-8.war)
* [Serveur documentation (observe-9.0.0-RC-8-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-8/observe-9.0.0-RC-8-doc.zip)
* [Application (observe-9.0.0-RC-7.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-7/observe-9.0.0-RC-7.zip)
* [Serveur (observe-9.0.0-RC-7.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-7/observe-9.0.0-RC-7.war)
* [Serveur documentation (observe-9.0.0-RC-7-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-7/observe-9.0.0-RC-7-doc.zip)
* [Application (observe-9.0.0-RC-6.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-6/observe-9.0.0-RC-6.zip)
* [Serveur (observe-9.0.0-RC-6.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-6/observe-9.0.0-RC-6.war)
* [Serveur documentation (observe-9.0.0-RC-6-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-6/observe-9.0.0-RC-6-doc.zip)
* [Application (observe-9.0.0-RC-5.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-5/observe-9.0.0-RC-5.zip)
* [Serveur (observe-9.0.0-RC-5.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-5/observe-9.0.0-RC-5.war)
* [Serveur documentation (observe-9.0.0-RC-5-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-5/observe-9.0.0-RC-5-doc.zip)
* [Application (observe-9.0.0-RC-4.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-4/observe-9.0.0-RC-4.zip)
* [Serveur (observe-9.0.0-RC-4.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-4/observe-9.0.0-RC-4.war)
* [Serveur documentation (observe-9.0.0-RC-4-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-4/observe-9.0.0-RC-4-doc.zip)
* [Application (observe-9.0.0-RC-3.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-3/observe-9.0.0-RC-3.zip)
* [Serveur (observe-9.0.0-RC-3.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-3/observe-9.0.0-RC-3.war)
* [Serveur documentation (observe-9.0.0-RC-3-doc.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-3/observe-9.0.0-RC-3-doc.zip)
* [Application (observe-9.0.0-RC-2.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-2/observe-9.0.0-RC-2.zip)
* [Serveur (observe-9.0.0-RC-2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-2/observe-9.0.0-RC-2.war)
* [Application (observe-9.0.0-RC-1.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-1/observe-9.0.0-RC-1.zip)
* [Serveur (observe-9.0.0-RC-1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/9.0.0-RC-1/observe-9.0.0-RC-1.war)

### Issues
  * [[Type::Anomalie 629]](https://gitlab.com/ultreiaio/ird-observe/-/issues/629) **[SERVEUR WEB] Exceptions probablement lorsque la connexion est ponctuellement coupée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 886]](https://gitlab.com/ultreiaio/ird-observe/-/issues/886) **Problème d&#39;exports de marées LL en v5.4** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1378]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1378) **[V9] Contrôle de validité sur date de route** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1382]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1382) **[V9] PS Trip / Débarquement : gestion des catégories de poids** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1589]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1589) **Impossible ouverture LL observation captures** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1832]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1832) **Erreur lors de l&#39;effacement d&#39;une base v9 par la v8** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1834]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1834) **Marée mal décorée (bateau absent)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1836]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1836) **Orthographe libellé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1837]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1837) **Erreur lors de l&#39;enregistrement de la mise à jour d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1838]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1838) **Sauvargade d&#39;une marée vers base sql.gz impossible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1839]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1839) **Nom d&#39;observateur abusivement exigé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1840]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1840) **Marché local : conseil à donner lorsque la liste des conditionnements est vide** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1841]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1841) **Libellés de conditionnements absents** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1852]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1852) **Dans l&#39;opération de conslidation PS, on voit des marées LL dans la sélection des données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1853]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1853) **Dans la configuration de l&#39;arbre de navigation, on ne pvoit pas les états sélectionnés à une première ouverture** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1854]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1854) **Les libellés des traductions sur les écrans de référentiels ont disparus :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1855]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1855) **Le bouton supprimer n&#39;est pas au bon endroit dans les formulaires de référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1856]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1856) **L&#39;écran des usages d&#39;un référentiel ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1858]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1858) **Les remplacements ne sont pas effectués sur une désactivation de référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1859]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1859) **Les dates de dernière mises à jour ne sont pas augmentées lors d&#39;un remplacement de référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1867]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1867) **Impossible d&#39;enregistrer le formulaire Marée LL avec section livre de bord et données associées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1868]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1868) **Impossible d&#39;accéder au formulaire Composition détaillée en LL observations** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1893]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1893) **Problème d&#39;enregistrement des échantilllons senne au port** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1895]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1895) **Problème d&#39;enregistrement sur les sondages de marché local** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1904]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1904) **Le menu **actions** reste accessible lorsque l&#39;application est occupée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1907]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1907) **Le filtre sur l&#39;arbre de navigation n&#39;est pas bien appliqué** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1908]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1908) **Après application du filtre de navigation, si l&#39;ancienne sélection prote sur un nœud programme qui n&#39;est plus présent, il est réajouté, ce qui n&#39;est pas correct** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1909]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1909) **Bien géré le fait que l&#39;heure d&#39;une activité LogBook est optionnelle** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1913]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1913) **Souci de mise en page sur le générateur de retour d&#39;expérience** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1915]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1915) **Utilisation d&#39;un login de type référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1916]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1916) **Impossible de générer une base locale vierge depuis le service web avec un compte de type référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1917]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1917) **Import AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1919]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1919) **Pas de marées à exporter ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1923]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1923) **Fenêtre des messages non visible sur form ps.logbooks.samples** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1934]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1934) **Pour un export, les marées LL ne sont plus proposées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1936]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1936) **Ultimate bug sur un GET** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1938]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1938) **Mauvais comportement des paramètres **config.loadReferential** et **config.recursive**** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1942]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1942) **Mises aux points sur le nouveau système de saisie de données** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1945]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1945) **Marées non trouvées lors de l&#39;import AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1948]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1948) **Les erreurs lors de l&#39;extraction et import AVDTH sont silencieuses** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1950]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1950) **Certaines méthodes de services doivent être passée en POST** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1951]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1951) **Impossible de générer une nouvelle base locale depuis un serveur** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1952]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1952) **Bug sur migration 7 -&gt; 9 RC9** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1953]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1953) **Buf sur génération de base locale RC9** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1958]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1958) **Erreur lors de l&#39;affichage de l&#39;A propos** (Thanks to Tony CHEMIT) (Reported by Julien Lebranchu)
  * [[Type::Anomalie 1960]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1960) **Problème de modélisation des SampleSpecies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1973]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1973) **Dysfonctionnements sur le form logbooks FOB** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1981]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1981) **Bug synchro référentiel simple** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1982]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1982) **Bug sur suppression de programme (vide) et &#39;voir toutes les utilisations de ce référentiel&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1995]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1995) **Il n&#39;est toujours pas possible de synchroniser simple un référentiel local avec un login distant de type référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1996]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1996) **La synchronisation simple de référentiel ne synchronise pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1998]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1998) **La synchro avancée de référentiel montre des différences qui ne devraient pas être là** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2003]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2003) **La synchro simple de marées ne marche pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2004]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2004) **L&#39;UI Confirmation pour insérer les référentiels manquants ne rentre pas dans l&#39;écran** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2010]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2010) **Problème mapping ACTIVITE.C_TYP_OBJET vers ObservedSystem** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2015]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2015) **Problème lors de l&#39;export, cinématique avec la mise à jour de référentiel en cause.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2020]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2020) **ll_logbook.set.haulingendtimestamp NOT NULL ?** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2021]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2021) **Un programme essaye toujours de se charger dans l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2023]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2023) **Pouvoir changer la taille du formulaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2028]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2028) **Lecture des messages longs** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2029]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2029) **Ordre du code sql généré incorrect sur la synchronisation simple et code généré en double** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2037]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2037) **Le classement alphabétique des bateaux semble ne pas fonctionner** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2048]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2048) **Petit défaut d&#39;affichage sur Marché local** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2050]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2050) **Nouvelle marée : onglet focusé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2055]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2055) **Erreur sur ouverture des forms objet flottant en mode serveur, sur les logbooks et les observations** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2061]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2061) **Exception sur modification des dates d&#39;une marée PS existante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2062]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2062) **Souci de validation sur form FP Sondage** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2063]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2063) **Activités/Changer de marée ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2071]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2071) **On peut créer un programme sans sélectionner aucun des types de données qu&#39;il peut recevoir** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2074]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2074) **Ne pas supprimer les fichers temporaires à chaque lancement de l&#39;application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2079]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2079) **Interdire l&#39;utilisation des systèmes observés désactivés** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2083]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2083) **Mauvais enregistrement matériaux au départ lors de l&#39;import AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2086]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2086) **Voir pourquoi le parsing des poids min/max sur les catégories n&#39;a pas bien fonctionné** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2087]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2087) **Si des activités sont liées aux échantillons ou bien au plan de cuves et qu&#39;elles ne sont pas de type pêche, on ne les voit pas dans les formulaires associés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2088]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2088) **Par défaut l&#39;assistant report ne cherche pas les reports au bon endroit** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2089]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2089) **Aucun report ne fonctionne en mode serveur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2107]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2107) **Le chagement manuel de topiaid ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2110]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2110) **Souci sur requête WS sur Person** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2112]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2112) **Les champs homeid ne sont pas toujours éditables sur les UI référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2115]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2115) **Problème de migration V9 après avoir mis à jour notre référentiel Ports** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2116]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2116) **Affichage de l&#39;arbre, souci sur le décompte des items** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2117]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2117) **Souci dans la doc RC18** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2118]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2118) **Problème de transcodage des codes types activités PS logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2119]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2119) **La consultation des FOB provoque une erreur, en observations et logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2120]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2120) **Correction de quelques incohérences sur l&#39;API public** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2121]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2121) **Souci de requêtage WS sur critère Booléen (ici, sur Person)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2123]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2123) **Il manque des orber-by dans le modèle de persistence** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2126]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2126) **La migration RC19 n&#39;identifie pas la pré existence de références qu&#39;elle insère** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2127]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2127) **En mode serveur les échantillons LL logbook rattachés à une activité retournent une erreur JSON** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2129]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2129) **Il faut consolider le code de migration concernant les relations many-to-many** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2137]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2137) **OptimisticLockException suite à requête PUT avortée** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2138]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2138) **Séparation du référentiel Programme pour chaque domaine (ps_common et ll_common)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2139]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2139) **Problème pour ouvrir les assistants gestion avancée des données et du référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2140]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2140) **Jeton d&#39;authentification expiré lors d&#39;une longue opération distante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2142]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2142) **Erreur TopiaQueryException sur service PUT pour modifier une marée existante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2147]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2147) **Le client ObServe ne démarre plus depuis Java 17 (peut-être 15 ou 16)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2165]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2165) **La fermeture des routes ne fonctionne pas bien** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2166]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2166) **Le menu navigation affiche trop de nœuds** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2167]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2167) **Gestion des cas où le nœud de groupement voulu pour une sélection dans l&#39;arbre de navigation n&#39;existe pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2172]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2172) **Amélioration de la gestion de la transaction sur un service en erreur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2173]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2173) **Correction des lastUpdateDate non mis à jour sur les formulaires de type tableau** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2178]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2178) **Souci lors de la configuration de l&#39;arbre 1 avec base vierge** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2180]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2180) **Souci lors de la configuration de l&#39;arbre 2** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2181]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2181) **Soucis lors de la configuration de l&#39;arbre 3 avec base vierge** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2183]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2183) **Création d&#39;un programme : exiger &#39;Observations&#39; ou &#39;Livre de bord&#39;** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2188]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2188) **Import AVDTH ne devrait pas être visible en mode serveur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2189]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2189) **Impossible d&#39;annuler une édition si un référentiel est désactivé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2190]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2190) **Exception sur chargement de l&#39;arbre (mode serveur) PS / Par bateaux** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2192]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2192) **Web service public RC24 ne fonctionne pas ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2195]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2195) **Message d&#39;erreur d&#39;insertion de Trip** (Thanks to Tony CHEMIT) (Reported by adelphe n&#39;goran)
  * [[Type::Anomalie 2196]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2196) **Erreur sur utilisation de PUT pour MAJ une marée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2197]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2197) **Bouton &quot;Créer la base locale&quot; ne marche pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2198]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2198) **La génération d&#39;une base locale ne marche pas :(:(:(** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2199]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2199) **Impossible d&#39;enregistrer une base :(:(:(** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2201]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2201) **Le changement de l&#39;heure ne fire pas le bouton Enregistrer** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2202]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2202) **Le changement d&#39;heure n&#39;est pas enregistré** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2205]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2205) **Les trois formulaires Calée ont perdu le reset** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2210]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2210) **Les messages de validation sont mal encodés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2213]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2213) **Correction sur les actions du comportements lors du retour sur onglet précédent après une action** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2217]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2217) **Corriger le nombre d&#39;étapes dans l&#39;export de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2219]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2219) **Plein d&#39;échantillons, comptage de l&#39;arbre : 0** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2220]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2220) **Problème de décoration de l&#39;arbre  de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2221]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2221) **Amélioration du mécanisme de remplacement de marée suite à un petit soucis lors de l&#39;export de données (détection de faux positifs dans le script d&#39;insertion)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2222]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2222) **Si on utilise un favori sur source serveur, le nom de la base optionel n&#39;est pas renseigné dans son éditeur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2227]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2227) **Corrections sur l&#39;appairement d&#39;activités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2228]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2228) **Enfin avoir les erreur de WebMotion!** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2236]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2236) **Problème de chargement de source de données sous Windows ET SOUS LINUX** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2237]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2237) **Sur un DELETE avec un topiaid de Trip non existant, le service retourne un codde de succès 200** (Thanks to Tony CHEMIT) (Reported by adelphe n&#39;goran)
  * [[Type::Anomalie 2256]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2256) **Arbre - option &quot;regrouper par période temporelle&quot;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2282]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2282) **Problème lors des déplacements, changement de programme intempestif de la marée cible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2284]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2284) **Erreur non explicitée s&#39;il manque l&#39;information de base par défaut** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2287]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2287) **Statistique bizarre sur plan de cuves** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2291]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2291) **Problème performance sur l&#39;écran Plan de cuve** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2292]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2292) **Sur écran échantillon espèce (livre de bord PS), si on change l&#39;ordre dans le tableau, les fréquences ne sont plus bien rattachés à la bonne ligne** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2298]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2298) **En anglais, boîte de dialogue illisible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2301]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2301) **Synchro avancée référentiels ne fonctionne pas si la base de gauche est un serveur distant** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2311]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2311) **Exception sur saisie de coordonnées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2315]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2315) **Erreur sur déplacement d&#39;un échantillon de la marée vers une de ses activités** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2316]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2316) **Erreur sur déplacement d&#39;échantillon d&#39;activité à activité** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2317]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2317) **Déplacement d&#39;échantillon d&#39;une activité vers sa propre marée semble impossible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2321]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2321) **Amélioration de l&#39;affichage des nœuds feuilles** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 123]](https://gitlab.com/ultreiaio/ird-observe/-/issues/123) **Uniformiser les topiaid entre ObServe et t3+** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 236]](https://gitlab.com/ultreiaio/ird-observe/-/issues/236) **Permettre de copier-coller facilement le contenu de la fenêtre &quot;Erreur...&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1085]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1085) **Generate navigation API** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1107]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1107) **Déplacer le fichier observe-client.conf dans le .observe** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1199]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1199) **[V9] Sur le formulaire PS/Observations/Captures faune associée, champ &quot;Origine de l&#39;information&quot;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1246]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1246) **Nouveaux raccourcis de navigation dans l’arbre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1250]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1250) **Ré usinage du formulaire marée PS et des sous-formulaires associés - Organisation hiérarchique des données senne** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1252]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1252) **Réorganisation des données d’observation PS - Re localisation dans la hiérarchie** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1254]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1254) **Réorganisation des données d’observation PS - Réorganisation des captures et échantillons de cible et bycatch observés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1255]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1255) **Insertion du livre de bord - Formulaire route** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1256]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1256) **Insertion du livre de bord - Formulaire activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1257]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1257) **Insertion du livre de bord - Formulaire calée (captures et rejets)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1258]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1258) **Insertion du livre de bord - Formulaire FOB et sous formulaire balise(s)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1259]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1259) **Insertion des lots de débarquements** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1260]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1260) **Insertion du plan de cuves** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1261]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1261) **Insertion de l’échantillonnage au port** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1262]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1262) **Insertion du suivi du marché local** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1297]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1297) **[LOGBOOK PS] Position bateau vs position balise** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1300]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1300) **[PS] Contenu des onglets spécifiques observations et logbooks et le formulaire marée** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1347]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1347) **Symboles de carte pour V9** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1375]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1375) **Sur le logbook captures (V9), permettre d&#39;utiliser des espèces sans catégorie commerciale** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1376]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1376) **[V9] l&#39;Observateur devrait être optionnel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1377]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1377) **[V9] Gestion des dates de route par défaut** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1379]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1379) **[V9] Synthèse des erreurs sur libellés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1381]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1381) **[V9] Revue des contrôles sur Trip PS** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1475]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1475) **Logique de fonctionnement des fichiers de configuration** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1511]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1511) **Stockage en base locale de la date de dernière mise à jour du référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1578]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1578) **Amélioration des messages d&#39;exceptions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1594]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1594) **Deplacer le réferentiel WeightCategory vers le schema ps_logbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1595]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1595) **Supprimer les vieilles migrations ?** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1599]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1599) **Introduction d&#39;un nouvel éditeur spécialisé pour renseigner un identifiant de cuve** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1601]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1601) **Bornes numériques sur tables de catégories de poids** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1603]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1603) **[PS][logbooks] Liste de référence des systèmes observés à utiliser** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1604]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1604) **[PS][Logbook] Supprimer le code océan du formulaire activité** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1606]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1606) **Remplir les référentiels depuis AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1607]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1607) **Amélioration de la modélisation des types de conditionnement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1608]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1608) **[PS] Constitution de la liste de référence VesselActivity** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1609]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1609) **Migration des DCP d&#39;AVDTH vers ObServe et son modèle FOB** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1610]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1610) **Amélioration du rendu visuel d&#39;une activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1614]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1614) **Liste d&#39;espèces accesssoires utilisées dans les lots commerciaux** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1615]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1615) **[PS] Renommage onglets** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1617]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1617) **[PS][Logbook] Réinitialisation/mise à null de l&#39;heure d&#39;activité impossible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1618]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1618) **[PS][Logbook] Rajout d&#39;un champ** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1619]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1619) **[PS][Observations] rajouter un champ horodatage** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1630]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1630) **[PS] Libellé activé par défaut pour &quot;Pays&quot; et &quot;Navire&quot; (plutôt que code) dans Objet flottant/Balise** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Evolution 1668]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1668) **Extraire les configurations sauvegardés de sources de données dans des fichiers séparé** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1699]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1699) **Générer les usines de services** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1744]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1744) **Quelques ajouts de champs sur la V9** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1753]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1753) **Acquisition des échantillons** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1773]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1773) **S&#39;assurer qu&#39;il y aura une date sur les landings PS** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1791]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1791) **Revoir les actions à réaliser lors d&#39;un déplacement de données** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1793]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1793) **Réduire la taille des fichiers livrables** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1796]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1796) **[PS][Logbook] Champ nombre capturés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1803]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1803) **Ajout des référentiels PS_LOGBOOK** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1805]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1805) **Ajout des référentiels PS_LANDING** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1806]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1806) **Ajout des référentiels PS_LOCALMARKET** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1808]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1808) **Ajout des données PS_LOGBOOK** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1809]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1809) **Ajout des données PS_LANDING** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1810]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1810) **Ajout des données PS_LOCALMARKET** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1812]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1812) **Modification des données PS_OBSERVATION** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1813]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1813) **Modification du formulaire Trip** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1825]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1825) **Ajouter une action pour pouvoir récupérer le jeton d&#39;authentification en mode server** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1826]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1826) **Ajouter deux options par défaut sur le serveur pour avoir des langues par défaut** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1827]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1827) **Mise en place de services publiques pour accéder/modifier les données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1828]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1828) **Amélioration du rendu des référentiels dans l&#39;abre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1829]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1829) **Mise en place du marché local** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1830]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1830) **Mise en place d&#39;une nouvelle API pour les arbres (sélection et navigation)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1842]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1842) **Mise en place de la documentation des api public** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1846]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1846) **Remplir la documentation avec tous les types de données possibles** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1847]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1847) **Ajouter les nouvelles données dans les tck** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1848]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1848) **Mise en place de la validation sur les API public** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1849]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1849) **Mise en place des filtres d&#39;accès sur le type d&#39;API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1850]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1850) **Amélioration de la session utilisateur du server** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1851]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1851) **Amélioration de la mise en place su serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1857]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1857) **Afficher les erreurs techniques dans les logs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1860]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1860) **Amélioration de la validation dans l&#39;application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1861]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1861) **Allégement du temps de chargement de l&#39;arbre de navigation lors d&#39;un changement de configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1862]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1862) **Migration des données Logbook depuis AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1863]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1863) **Amélioration des paramètres de configuration des requêtes GET** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1864]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1864) **Mapping associations AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1865]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1865) **Faire apparaître le top niveau &quot;FOB&quot; dans l&#39;UI des matériaux des FOB** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1869]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1869) **Ajout flag traité/rejeté sur le form lot de débarquement** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1870]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1870) **Migration des TargetCatch de la v7/8 vers Catch de la v9 : Méthode d&#39;aquisition** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1871]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1871) **Migration des TargetCatch de la v7/8 vers Catch de la v9 : De la catégorie au poids moyen** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1872]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1872) **Amélioration des tables PS de qualification de l&#39;origine des données** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1873]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1873) **Mapping des zones FPA AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1875]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1875) **Mapping des Océans AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1876]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1876) **Mapping des catégories de poids landing AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1877]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1877) **Mapping des cuves AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1878]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1878) **Ajout d&#39;un nouveau référentiel WellFate** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1879]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1879) **Migration des catégories de poids dans les plans de cuve** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1880]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1880) **Mapping des références AVDTH CUVE.C_DEST** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1883]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1883) **Captures logbook avec espèce de type Appât** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1884]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1884) **Migration des données Débarquement depuis AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1885]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1885) **Usinage des catégories des landings dans ObServe** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1886]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1886) **Ajout de bornes min et max sur les catégories de poids** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1887]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1887) **Ajout d&#39;un nouvel attribut **Person.psSampler**** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1888]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1888) **Synthèse des catégories de poids actuellement en vigueur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1890]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1890) **Déporter la regex de contrôle de la syntaxe des cuves dans la configuration de l&#39;application** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1891]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1891) **Petite modification sur Vessel (puissance moteur)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1892]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1892) **Ajout de date sur lot de marché local** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1894]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1894) **Produire un résultat de l&#39;import avec quelques statistiques** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1901]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1901) **Echantillonneurs des Seychelles** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1902]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1902) **Mettre en place une nouvelle action dans le menu Source de données pour importer une base AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1905]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1905) **Table MAREE - Champs MAREE.F_ENQ et MAREE.C_ZONE_GEO** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1910]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1910) **Table ACTIVITE - champs non pris en compte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1914]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1914) **Migration du référentiel Wind** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1918]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1918) **Libellé à mettre à jour dans le menu Actions** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1920]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1920) **A propos du service web public** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1921]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1921) **Amélioration de la gestion des positions corrigées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1922]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1922) **Préférences de configuration : nom d&#39;un des onglets** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1924]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1924) **Tables de référence autour des cuves** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1925]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1925) **Mise en place du Status de collecte sur la marée (protocole d&#39;échantillonnage)** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1926]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1926) **Simplification de l&#39;import AVDTH en supprimant la première étape** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1928]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1928) **Consolidation des contraintes de nullité dans le modèle de persistance** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1930]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1930) **Supprimer observationsAvailability, logbookAvailability et activityAvailability** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1937]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1937) **Ajout de nouveaux services pour récupérer l&#39;ensemble du référentiel** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1940]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1940) **Oh la la le formulaire surchargé !** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1943]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1943) **Info type de validation sur service Information** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1946]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1946) **Changement de la notation pointée vers une notation souligné bas pour les noms de filtre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1949]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1949) **Améliorer l&#39;opération d&#39;export de données (la partie calcul de référentiels manquants)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1954]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1954) **Sur l&#39;import avdth, pouvoir utiliser les programmes désactivés.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1956]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1956) **Assainissement de la configuration de l&#39;import avdth** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1957]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1957) **Assainir les paramètres des services d&#39;UI** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1959]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1959) **Critères de groupement pour le nouvel arbre** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1961]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1961) **Renommage Size en SampleMeasure** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1963]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1963) **Rajouter un programme logbook sur la marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1964]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1964) **Supprimer l&#39;action Reallocation du programme d&#39;une marée.** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1965]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1965) **Contraindre la date de débarquement FP** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1967]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1967) **Revoir la modélisation des échantillons de marché local** (Thanks to ) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1968]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1968) **Appellation des actions de création sur échantillons de marché local** (Thanks to ) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1969]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1969) **Définition des reports PS logbooks tabulaires** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1972]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1972) **Amélioration des éditeurs inline** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1975]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1975) **Mise en place de la documentation de la migration AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1976]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1976) **Amélioration de la documentation des configurations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1978]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1978) **Ajouter une info &#39;type d&#39;échantillon&#39; sur les échantillons de marché local** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1979]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1979) **Revoir l&#39;ordre d&#39;affichage des matériaux de DCP pour gérer le cas où &#39;lon a plus de 9 fils sous un nœud** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1983]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1983) **Ajout d&#39;un feedback sur les actions longues** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1984]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1984) **Forcer le désenregistrement des driver sur le serveur à la fermeture de l&#39;application web** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1985]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1985) **Suppression de ps_localmarket.Sample.totalCount** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1986]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1986) **affichage des actions sur les tableaux inlines dans son titre  à droite dans une toolbar et suppression alors des menus contextuels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1987]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1987) **Stabilisation des éditeurs inlines** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1988]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1988) **Passage en UTF8 du fichier de reports** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1989]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1989) **Quelques améliorations sur l&#39;action de rapport** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1990]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1990) **Renommer une référence** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1991]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1991) **Algorithme d&#39;estimation du poids des batch de localmarket** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1994]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1994) **Origine de ces libellés ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1997]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1997) **Problème de données AVDTH / OA / Marché local sondage** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1999]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1999) **Revoir la synchronisation de référentiel et y intégrer la détection de référentiels à copier lors d&#39;un export** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2001]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2001) **Revue de la modélisation ps logbook Sample** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2007]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2007) **Ne pas exposer le paramètre validationMode dans les services** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2011]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2011) **Revoir les conditions avant suppression des données (avec lien externe)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2013]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2013) **Amélioration de la migration des référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2014]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2014) **Mauvais tri des marées et routes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2017]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2017) **Raffinement de l&#39;import des associations AVDTH 22 et 24 (épaves balisées)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2018]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2018) **null value in column &quot;wellregex&quot; violates not-null constraint** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2022]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2022) **Ne plus afficher par défaut le slider dans les éditeurs de temps** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2024]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2024) **Rendre triable les échantillons PS Logbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2025]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2025) **Amélioration des doubles lists** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2026]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2026) **Logs de l&#39;import AVDTH** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2027]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2027) **ll_landing.landingpart.weight NOT NULL ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2030]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2030) **Que faire des référentiels plus à jour dans la base locale lors d&#39;une synchronisation de référentiel simple** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2031]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2031) **Amélioration interface de l&#39;import AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2033]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2033) **Revue de l&#39;affichage des indicateurs dans l&#39;arbre de navigation** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2034]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2034) **Répertoires visibles dans les fenêtres de sélection de dossier/fichier** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2035]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2035) **Afficher le type bateau sur le liste du gestionnaire bateaux** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2036]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2036) **Visibilité des types de bateaux** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2038]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2038) **Amélioration d&#39;un libellé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2039]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2039) **Ajout de personnes** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2040]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2040) **Ajout de 2 colonnes dates sur Vessel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2041]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2041) **Note d&#39;information sur le form Activité** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2042]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2042) **Petites améliorations sur la migration V9 des zones FPA** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2043]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2043) **Organisation du form PS/Logbook/Activité** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2045]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2045) **Libellé form plan de cuves** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2046]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2046) **Peaufinage validation du form PS logbook échantillon** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2051]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2051) **Supprimer ces flags sur Program** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2052]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2052) **flags Program.observation(s) et Program.logbook(s)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2057]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2057) **Amélioration des éditeurs de type combo-box** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2058]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2058) **Affichage des nombres de noeuds dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2059]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2059) **Contrôle total marché local** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2067]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2067) **Reconnexion** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2068]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2068) **Synthèse des espèces et catégories de poids dans l&#39;import AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2069]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2069) **sampleActivity.weightedWeight, sample.smallsWeight, sample.bigsWeight et sample.totalWeight** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2070]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2070) **Création des routes et activités requises lorsqu&#39;elles sont absentes** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2073]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2073) **Codes espèces manquants dans la base de production** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2075]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2075) **Alléger la décoration des espèces** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2076]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2076) **Positionner sur le bouton choisir un fichier ou un répertoire sur les écrans requis** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2078]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2078) **Passer activity.latitude/longitude en NULL possible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2080]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2080) **Activité PS logbook : Préremplir &#39;Nombre d&#39;opération&#39; avec 1** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2081]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2081) **Distinguer Perte de transmission et Fin d&#39;utilisation de balise** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2082]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2082) **Retirer le contrôle sur le quandrant en fonction de l&#39;océan** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2084]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2084) **Amélioration des pré-configuration de DCP** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2085]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2085) **Remplir les codes de weightmeasuremethod** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2090]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2090) **Petite amélioration des formulaires sur les paquetages de référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2092]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2092) **La migration AVDTH met &#39;Peson&#39; au lieu de &#39;Estimation Visuelle&#39; sur les captures logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2093]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2093) **Mise en conformité des couleurs sur les nœuds de l&#39;arbre de navigation sur les référentiels.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2094]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2094) **Remonter l&#39;action supprimer dans les formulaire de type tableau dans la popup Actions supplémentaires** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2097]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2097) **La migration AVDTH devrait initialiser le saisisseur de données logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2098]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2098) **Ajout de 3 codes dans le référentiel vesselActivity : la bonne méthode** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2100]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2100) **Amélioration si besoin des suppressions de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2101]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2101) **Réorganisation de l&#39;arbre de navigation pour mieux représenter les paquetages** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2102]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2102) **Amélioration de l&#39;action déplacer des données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2111]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2111) **Quelques ajouts au référentiel par la migration V7 -&gt; V8** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2113]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2113) **Modélisation activity &lt;-&gt; sample sur LL logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2114]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2114) **Consolidation des référentiels (ajout not-null sur code et les validations not-null et unicité sur les cas pertinents)** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2125]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2125) **Modifier des codes de références ajoutées par la migration V7/8/9** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2131]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2131) **Amélioration de l&#39;API pour supporter le JSON-Encoded POST/PUT data** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2132]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2132) **Review get API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2133]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2133) **Ajout d&#39;un ordre naturel sur les requêtes GetAll et GetSome** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2134]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2134) **Permettre d&#39;avoir plusieurs instances du serveur tournant sur des configurations différentes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2135]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2135) **Utilisation d&#39;un seul fichier server.yml pour la configuration du serveur (remplace databases.yml et users.yml)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2155]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2155) **Amélioration de l&#39;interface graphique des source de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2156]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2156) **Amélioration de l&#39;API publique (GetSome)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2159]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2159) **Revue de l&#39;interface graphique pour les déplacements simples dont le parent est une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2160]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2160) **Ajouter la notion d&#39;obligatoire sur les critères de groupements** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2161]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2161) **Ajouter dans les arbres une zone où afficher les statistiques sur la source de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2162]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2162) **Lors de la création d&#39;une nouvelle marée, pré-renseigner si le critère de groupement est qualitatif, sa valeur dans le formulaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2163]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2163) **Ne pas renseigner Trip.logbookProgram lors de la migration v7 → v9** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2164]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2164) **Le raccourci F5 ne fonctionne pas dans les formulaires si l&#39;éditeur qui a le focus est un éditeur de date** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2168]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2168) **Enlever la configuration du server instance.session.max.size** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2169]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2169) **Vérifier comment est utilisé le timeout sur la connexion server** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2170]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2170) **Revoir le démarrage du serveur web** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2171]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2171) **Définir comment mettre à jour une marée via l&#39;API publique** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2175]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2175) **Générer les script sql pour la suppression/recopie de toute entité (comme déjà fait pour les marée)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2184]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2184) **On pourrait empêcher de configurer l&#39;arbre sans référentiels ni marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2186]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2186) **Libellé d&#39;affichage des marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2193]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2193) **Création du programme LL FINSS ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2194]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2194) **Le classement des bateaux pourrait se faire sur les noms** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2203]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2203) **Contrôle sur l&#39;heure d&#39;activité sur le WS public en create et update** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2204]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2204) **Ne plus retourner le contenu en erreur de validation dans l&#39;API publique** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2206]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2206) **Amélioration de la documentation de l&#39;API publique pour voir les types de date utilisés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2207]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2207) **Amélioration des validateurs sur collection** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2224]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2224) **Améloration sur l&#39;écran Marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2226]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2226) **Revue flux de données PS** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2231]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2231) **Amélioration de la fin de la synchronisation simple de référentiel** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2235]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2235) **Soucis de sélection multiple sur déplacement de données** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2238]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2238) **Sur échantillonnage PS, permettre l&#39;absence d&#39;activités associées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2239]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2239) **Entrer les identités de l&#39;équipe de Dakar** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2243]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2243) **Rapport LL : liste détaillée des captures** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2247]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2247) **Liste des relations FK à inverser** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2255]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2255) **Souci sur importation d&#39;une base espagnole** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2257]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2257) **Amélioration du calcul des référentiels manquants** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2258]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2258) **Base IEO ouverture en V9 à tester** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2260]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2260) **Des FloattingObjectMaterial à rajouter dans le référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2262]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2262) **Tooltip non pertinente** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2263]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2263) **Stats dans la nouvelle présentation de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2264]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2264) **Normaliser la présentation des statistiques dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2265]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2265) **Mise en page des rapports** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2267]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2267) **Déplacement des routes d’observation depuis le nœud ‘Observations’** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2268]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2268) **Lors d’un déplacement de données vers une autre marée, seules les marées du même programme sont proposées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2269]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2269) **Déplacement d’une marée dans un autre programme** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2270]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2270) **Amélioration de la sélection des marées dans les interfaces graphiques de déplacement.** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2271]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2271) **Déplacement de routes d’observation : gestion incomplète** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2274]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2274) **Déplacement du marché local : gestion incomplète** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2278]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2278) **Lisibilité des erreurs retournées par le service web** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2279]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2279) **Amélioration de l&#39;écran marée - onglet Livre de bords** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2281]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2281) **Libellé sur UI déplacements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2286]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2286) **Dans le gestionnaire de connexions, l&#39;onglet serveurs pourrait être le premier** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2296]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2296) **Libellés anglais dans l&#39;arbre à corriger** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2297]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2297) **Libellés français dans l&#39;arbre à corriger** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2299]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2299) **Libellés anglais appartenance balise à corriger** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2300]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2300) **Libellés anglais sur form PS logbook Capture** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2302]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2302) **Labels anglais sur les référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2310]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2310) **Rendre les associations océan/quandrants configurables** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2312]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2312) **Forme de filage par défaut sur LL logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2313]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2313) **Validation perfectible sur les dates d&#39;opération de pêche** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2314]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2314) **Contrôle sur les espèces de la marée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2318]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2318) **Form débarquement, déplacer un champ** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2319]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2319) **Enlever la poussière de WebMotion** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2326]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2326) **Intégrer cette référence ll_landing.dataSource dans la migration 9.0.0** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2328]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2328) **Standardiser l&#39;affichage des statistiques LL dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2329]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2329) **Renforcer le modèle par des clés d&#39;unicité sur certaines clés étrangères** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1653]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1653) **Il manque des boutons reset sur les éditeurs de dates** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1941]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1941) **Comment configurer le type d&#39;accès d&#39;un compte utilisateur ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1993]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1993) **Migration 7 vers 9 RC13 bloque** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2122]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2122) **Le service ps/logbook/SetSuccessStatus n&#39;est pas documenté** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2208]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2208) **Synchro simple du référentiel, associations espèce/océan** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2252]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2252) **Vérifier le bon fonctionnement du observe.exe** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2261]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2261) **Défaut de libellés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2280]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2280) **Eprouver le dump AZTI** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Tâche 2290]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2290) **Eprouver le dump IEO** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Tâche 2320]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2320) **Calée orpheline de la base IRD** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Documentation 2158]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2158) **Amélioration de la documentation de l&#39;API publique** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Documentation 2233]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2233) **Documentation /data/ps/common/Trip/Create incomplète** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [8.0.10](https://gitlab.com/ultreiaio/ird-observe/-/milestones/187)

**Closed at 2021-04-16.**

### Download
* [Application (observe-8.0.10.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.10/observe-8.0.10.zip)
* [Serveur (observe-8.0.10.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.10/observe-8.0.10.war)

### Issues
  * [[Type::Anomalie 1823]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1823) **postgresql/update.sh ne démarre pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [8.0.9](https://gitlab.com/ultreiaio/ird-observe/-/milestones/185)

**Closed at 2021-03-24.**

### Download
* [Application (observe-8.0.9.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.9/observe-8.0.9.zip)
* [Serveur (observe-8.0.9.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.9/observe-8.0.9.war)

### Issues
  * [[Type::Anomalie 1380]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1380) **[V9] Lorsque l&#39;on éditer un champ et que l&#39;on enregistre sur PS trip/onglet livre de bord, le focus se déplace sur l&#39;onglet caractéristiques** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1821]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1821) **Problème de chargement de la carte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1628]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1628) **[LL][Landing] Renommer table compagnies** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1811]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1811) **Remove BranchlineService** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1816]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1816) **Petits ajustements sur le gestionnaire de connection** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1818]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1818) **Réduire la taille de l&#39;archive d&#39;un retour d&#39;expérience** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1819]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1819) **Amélioration de la représentation des référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [8.0.8](https://gitlab.com/ultreiaio/ird-observe/-/milestones/184)

**Closed at 2021-03-15.**

### Download
* [Application (observe-8.0.8.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.8/observe-8.0.8.zip)
* [Serveur (observe-8.0.8.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.8/observe-8.0.8.war)

### Issues
  * [[Type::Anomalie 837]](https://gitlab.com/ultreiaio/ird-observe/-/issues/837) **Problème d&#39;UI lors de l&#39;enregistrement d&#39;un profil de connexion direct** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1802]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1802) **Corrections du rendu visuel des heures de création et modification incorrect** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1495]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1495) **Suite à la création d&#39;un route, sur le form route, on pourrait avoir un bouton &#39;Créer une activité&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1612]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1612) **Mutualiser la liste ObjectMaterial pour les observations et les logbooks** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1801]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1801) **Générer de nouveaux identifiants sur tous les objets d&#39;une grappe de données lors d&#39;un déplacement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1804]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1804) **Déplacement du référentiel **ObservedSystem** to **ps_common**** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [8.0.7](https://gitlab.com/ultreiaio/ird-observe/-/milestones/183)

**Closed at 2021-03-12.**

### Download
* [Application (observe-8.0.7.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.7/observe-8.0.7.zip)
* [Serveur (observe-8.0.7.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.7/observe-8.0.7.war)

### Issues
  * [[Type::Anomalie 1795]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1795) **Mauvaise génération d&#39;une sauvegarde si toutes les données sont sélectionnées** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1752]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1752) **Remplacer les raccourcis clavier Ctrl+L et Ctrl+R par des raccourcis compatible MacOs** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1794]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1794) **Rendre les déplacements impossible si les parents incriminés sont obsolètes.** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1797]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1797) **Réusinage de la configuration d&#39;une source de données et des templates associées** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1798]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1798) **Revue de l&#39;opération longue de validation** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1799]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1799) **Bloquer l&#39;import d&#39;une marée si elle contient des identifiants déjà utilisés dans la base cible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1800]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1800) **Amélioration des éditeurs inline** (Thanks to ) (Reported by Tony CHEMIT)

## Version [8.0.6](https://gitlab.com/ultreiaio/ird-observe/-/milestones/182)

**Closed at 2021-02-19.**

### Download
* [Application (observe-8.0.6.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.6/observe-8.0.6.zip)
* [Serveur (observe-8.0.6.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.6/observe-8.0.6.war)

### Issues
  * [[Type::Anomalie 1780]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1780) **Problème de sauvegardes des échantillons** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1781]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1781) **Mauvaise disposition de l&#39;encadré des espèces et quelques améliorations sur les deux écrans** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1782]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1782) **On en voit plus les DCP!** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1783]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1783) **Impossible d&#39;importer des données en mode avancé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1786]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1786) **Déplacement de marées pas effectif** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1787]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1787) **Amélioration de la boite de dialogue de suppression** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1788]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1788) **Revoir l&#39;ordre des actions dans la popup **Actions**** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1789]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1789) **Problème de contexte d&#39;édition lors de déplacement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1785]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1785) **Finaliser les tests automatisés de services** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1790]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1790) **Amélioration de l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1792]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1792) **Amélioration du rendu des coordonnées** (Thanks to ) (Reported by Tony CHEMIT)

## Version [8.0.5](https://gitlab.com/ultreiaio/ird-observe/-/milestones/179)

**Closed at 2021-02-07.**

### Download
* [Application (observe-8.0.5.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.5/observe-8.0.5.zip)
* [Serveur (observe-8.0.5.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.5/observe-8.0.5.war)

### Issues
  * [[Type::Anomalie 1593]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1593) **Figer tous les lastUpdate et topiaCreateDate dans les scripts de migration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1763]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1763) **Dysfonctionnement de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1764]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1764) **Micro bizarrerie sur le modèle 8.0** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1769]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1769) **Tailles d&#39;icônes différentes sur le même niveau de hiérarchie** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1770]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1770) **Petit défaut d&#39;affichage d&#39;une liste à choix multiple** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1776]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1776) **Problème de persistence de trois relations** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1777]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1777) **Problème lors du chargement d&#39;une sauvegarde** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1778]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1778) **Problème d&#39;accessibilité sur le formulaire avançon** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1779]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1779) **Les références sur les marées ne contiennent plus les données techniques** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 795]](https://gitlab.com/ultreiaio/ird-observe/-/issues/795) **Generate services tests** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1717]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1717) **Disponibilité du sélecteur DMS / DMD / DD** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1747]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1747) **Sélection multiple de marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1765]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1765) **Suppression en cascade des captures conservées** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1766]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1766) **Suppression en cascade des captures rejetées** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1767]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1767) **Suppression en cascade de la faune accessoire** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1768]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1768) **Suppression en cade des libérations de capture accessoire** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1771]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1771) **Problème de sauvegarde libération espèce sensible** (Thanks to ) (Reported by Tony CHEMIT)

## Version [8.0.4](https://gitlab.com/ultreiaio/ird-observe/-/milestones/178)

**Closed at 2021-01-19.**

### Download
* [Application (observe-8.0.4.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.4/observe-8.0.4.zip)
* [Serveur (observe-8.0.4.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.4/observe-8.0.4.war)

### Issues
  * [[Type::Anomalie 1762]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1762) **Problème de digit TargetLength.weight à 1 au lieu de 3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [8.0.3](https://gitlab.com/ultreiaio/ird-observe/-/milestones/176)

**Closed at 2021-01-18.**

### Download
* [Application (observe-8.0.3.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.3/observe-8.0.3.zip)
* [Serveur (observe-8.0.3.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.3/observe-8.0.3.war)

### Issues
  * [[Type::Anomalie 813]](https://gitlab.com/ultreiaio/ird-observe/-/issues/813) **Le focus n&#39;est pas bien positionné sur le formulaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1637]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1637) **Remontée des messages d&#39;erreur serveur vers le client** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1697]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1697) **On en voit pas les activités observées disponibles** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1700]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1700) **Il manque des logs sur le serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1701]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1701) **Impossible d&#39;ouvrir la configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1714]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1714) **Validateur needcomment récalcitrant** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1715]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1715) **LL Trip : erreur d&#39;enregistrement** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1718]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1718) **La dernière SNAPSHOT 8.0.3 semble instable** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1719]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1719) **La carto ne tolère pas le &#39;Port à corriger&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1720]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1720) **La dernière SNAPSHOT 8.0.3 semble instable 2** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1723]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1723) **Détail ergonomique sur l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1725]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1725) **Problème si on supprime l&#39;unique commentaire d&#39;une commentaire, ça ne fonctionne pas bien!** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1726]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1726) **Les marées sont mal classées dans la fenêtre de droite** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1728]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1728) **Erreur sur création de marée LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1730]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1730) **Erreur sur validateur &#39;numéro au virage&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1731]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1731) **Consultation du tableau de captures** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1732]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1732) **Bouton &#39;infos techniques&#39; sur formulaire captures** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1739]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1739) **Consultation du tableau des débarquements lorsque le formulaire est verrouillé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1741]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1741) **On arrive à vider la sélection sur un tableau alors qu&#39;on ne devrait pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1746]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1746) **Widget horodatage sans bouton de réinitialisation** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1748]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1748) **L&#39;export de marées vers sql.gz ne fontionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1759]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1759) **Focus lors du dévérouillage LL/observations/Composition détaillée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1695]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1695) **l&#39;information nombre total d&#39;opération de pêches sur le formulaire marée me semble un peu faible ?** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1698]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1698) **Génération du code final des services local** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1702]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1702) **Configuration de la cartographie** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1704]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1704) **Ajouts au référentiel VesseType** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1705]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1705) **Peaufinage de la table ll_common.onboardprocessing** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1706]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1706) **Peaufinage de la table ll_common.catchefate** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1707]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1707) **Mise en page de l&#39;arbre sur le référentiel manageur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1708]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1708) **Peaufinage de la table common.country** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1709]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1709) **Améliorer les performances lors du changement de l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1711]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1711) **Peaufinage de la table ll_common.triptype** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1712]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1712) **Peaufinage de la table common.program** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1721]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1721) **Meilleure disposition pour l&#39;onglet carte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1727]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1727) **LL Logbook capture : validation formulaire** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1729]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1729) **Gestion du champ &#39;numéro au virage&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1733]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1733) **Peaufinage de la table ll_common.onboardprocessing** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1738]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1738) **Rechargement de la source de données** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1740]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1740) **Verrouillage/déverouillage des formulaires** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1745]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1745) **Affichage des coordonnées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1750]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1750) **Optimisation de l&#39;arbre de sélection et navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1664]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1664) **Il reste quelques optimisations à réaliser sur la couche de persistence** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1665]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1665) **Valider tous les écrans du modèle LL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [8.0.2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/175)
Merry Christmas

**Closed at 2020-12-24.**

### Download
* [Application (observe-8.0.2.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.2/observe-8.0.2.zip)
* [Serveur (observe-8.0.2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.2/observe-8.0.2.war)

### Issues
  * [[Type::Anomalie 1642]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1642) **Comportement des combobox** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1666]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1666) **Impossible d&#39;ouvrir les bases de données** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1667]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1667) **Petit problème erratique sur l&#39;ouverture de nœud dans l&#39;arbre** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1670]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1670) **Si on annule le changement d&#39;une source de donnée, la source précédente reste connectée mais on ne voit pas l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1672]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1672) **[LL] Bug sur création d&#39;activité logbook LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1673]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1673) **[LL] Ajout données d&#39;observation** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1675]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1675) **[LL][Logbook] Quadrant** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1677]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1677) **[LL][Logbooks] Pêche / Onglet cyalumes** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1678]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1678) **[LL][Logbooks] F5 inopérant (sur MacOS)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1680]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1680) **[LL][Logbooks] Form Composition globale ne s&#39;affiche pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1681]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1681) **[LL][Logbooks] Impossible d&#39;afficher le form Echantillons** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1682]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1682) **[LL][Logbooks] Impossible d&#39;afficher le form Débarquements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1683]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1683) **Il semble manquer des index sur des relations many-to-one (par exemple Tdr → Section)** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1685]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1685) **Gestion de la nullité de certaines clef étrangère** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1690]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1690) **À la sortie de la configuration (avec un appel de rechargement d&#39;interface graphique), la base est masquée et innacessible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1691]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1691) **Aucun ordre n&#39;est utilisé lors de la synchro simple de référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1640]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1640) **Lisibilité de la carte - police de la date** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1656]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1656) **Ajouter un onglet dans la configuration pour tout ce qui concerne l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1657]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1657) **Revoir le bouton qui permet de voir la base chargée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1661]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1661) **Passage en java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1671]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1671) **Lors d&#39;une action d&#39;import d&#39;une sauvegarde, la source de donnée courante est fermée avant même que l&#39;utilisateur est validé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1674]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1674) **Peux-ton afficher ici le racourcis F2 qui permet de basculer lecture/mise à jour ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1686]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1686) **Meilleur intégration de la précision sur les champs numériques** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1687]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1687) **Génération du type précis pour les champs textes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1689]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1689) **Ajouter un onglet cartographie dans la configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1693]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1693) **Nettoyage des fichiers du client** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1694]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1694) **Sur les éditeurs de date simple utiliser le même raccourci pour ouvrir la popup de date que pour une liste à sélection multiple (↓)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1696]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1696) **Commencer les raccourcis claviers pour changer d&#39;onglet à partir de **shift F1** et plus **shift F5**** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1663]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1663) **Revoir le code de gestion des échantillons** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [8.0.1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/173)

**Closed at 2020-12-14.**

### Download
* [Application (observe-8.0.1.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.1/observe-8.0.1.zip)
* [Serveur (observe-8.0.1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.1/observe-8.0.1.war)

### Issues
  * [[Type::Anomalie 1588]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1588) **Impossible d&#39;enregister une caléee ll observation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1596]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1596) **La reprise sur erreur en mode serveur doit être revue** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1598]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1598) **Lors de l&#39;ouverture de l&#39;application, si la base locale est déjà occupée par une autre application,  message erroné** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1620]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1620) **Champs avec needcomment&#61;true** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1632]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1632) **[LL][Logbook] landingpart.landing devrait être une FK** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1633]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1633) **[LL][Logbook] ll_landing.landingpart contient 3 champs certainement en trop** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1636]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1636) **Nombre de marées par programme non affiché dans l&#39;arbre, avant clic sur chaque programme** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1638]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1638) **Reconnaissance des paramètres de sécurité** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1639]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1639) **Suppression d&#39;entrée en synchro avancée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1641]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1641) **&#39;+&#39; / Nouvelle route non à propos** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1644]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1644) **CTROL+F8 non fonctionnel (sous Mac)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1645]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1645) **Calée : racourcis inopportuns** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1647]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1647) **Bug sur suppression estimation banc** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1648]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1648) **Boutons de navigation devraient avoir disparu** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1649]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1649) **Les listes déroulantes permettent de chosir des entrées de référentiel désactivées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 495]](https://gitlab.com/ultreiaio/ird-observe/-/issues/495) **[Référentiel] Corriger un nom de champ** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 974]](https://gitlab.com/ultreiaio/ird-observe/-/issues/974) **Ajouter une option de configuration permettant de s&#39;affranchir des ouvertures/clôtures** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1249]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1249) **Amélioration des fonctionnalités de synchronisation avancées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1512]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1512) **Gestion du versionning des marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1592]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1592) **Auto-chargement des rôles pour les utilisateurs de la base ciblées dans les assistants de création/mise à jour** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1605]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1605) **Nouveau texte de popup pour le cas &#39;Vous devez mettre à jour le form marée avant de pouvoir saisir le livre de bord&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1613]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1613) **Ajouter un nouveau bouton sur le formulaire d&#39;une ligne sélectionnée dans un écran de type table créer/enregister ET nouveau** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1626]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1626) **Manque le champ lightsticksperbasket** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1627]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1627) **[LL][Logbooks] table ll_landing.datasource à compléter** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1629]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1629) **[LL][Logbooks] Ajout d&#39;un champ** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1643]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1643) **Déploiement du calendrier** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1646]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1646) **Méthodes de mesure par défaut** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1658]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1658) **Revoir les clefs d&#39;unicité sur la base** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1532]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1532) **Remplacer le moteur de scripting utilisé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1611]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1611) **Revoir si on doit laisser le lien Catch.set si la capture est associée à un élément de la ligne (section, basket, branchline)** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1623]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1623) **Optimisation de la migration 8.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [8.0.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/149)

**Closed at 2020-08-10.**

### Download
* [Application (observe-8.0.0.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.0/observe-8.0.0.zip)
* [Serveur (observe-8.0.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.0/observe-8.0.0.war)

### Issues
  * [[Type::Anomalie 1428]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1428) **[V8] Bug sur clic sur programme** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1576]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1576) **L&#39;option afficher/cacher la légende semble ne pas fonctionner** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1580]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1580) **Défauts d&#39;affichage dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1581]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1581) **Les rapports FOB ne fonctionnent pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 202]](https://gitlab.com/ultreiaio/ird-observe/-/issues/202) **En édition d&#39;un formulaire existant, si pas de modifications, permettre de clôturer même si la validation ne passe pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 576]](https://gitlab.com/ultreiaio/ird-observe/-/issues/576) **[UI][PS] Confirmation de non saisie de balise sur DCP** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1545]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1545) **[ASSITANTS SYNCHRO AVANCEE] Cosmétique écran de connexions** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1556]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1556) **Améliorer la transition vers l&#39;onglet de sélection de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1579]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1579) **Ajout contrôle de borne 0 à 100 sur la proportion déprédaté sur une capture ll logbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1582]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1582) **Amélioration de la représentation des sonnées de données dans les actions longues** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1566]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1566) **Ne plus rendre DecoratorService comme un Composant applicatif** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [8.0-RC](https://gitlab.com/ultreiaio/ird-observe/-/milestones/151)

**Closed at 2020-07-08.**

### Download
* [Application (observe-8.0.0-RC-8.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.0-RC-8/observe-8.0.0-RC-8.zip)
* [Serveur (observe-8.0.0-RC-8.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0.0-RC-8/observe-8.0.0-RC-8.war)
* [Application (observe-8.0-RC-7.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-7/observe-8.0-RC-7.zip)
* [Serveur (observe-8.0-RC-7.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-7/observe-8.0-RC-7.war)
* [Application (observe-8.0-RC-6.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-6/observe-8.0-RC-6.zip)
* [Serveur (observe-8.0-RC-6.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-6/observe-8.0-RC-6.war)
* [Application (observe-8.0-RC-5.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-5/observe-8.0-RC-5.zip)
* [Serveur (observe-8.0-RC-5.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-5/observe-8.0-RC-5.war)
* [Application (observe-8.0-RC-4.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-4/observe-8.0-RC-4.zip)
* [Serveur (observe-8.0-RC-4.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-4/observe-8.0-RC-4.war)
* [Application (observe-8.0-RC-3.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-3/observe-8.0-RC-3.zip)
* [Serveur (observe-8.0-RC-3.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-3/observe-8.0-RC-3.war)
* [Application (observe-8.0-RC-2.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-2/observe-8.0-RC-2.zip)
* [Serveur (observe-8.0-RC-2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-2/observe-8.0-RC-2.war)
* [Application (observe-8.0-RC-1.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-1/observe-8.0-RC-1.zip)
* [Serveur (observe-8.0-RC-1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/8.0-RC-1/observe-8.0-RC-1.war)

### Issues
  * [[Type::Anomalie 513]](https://gitlab.com/ultreiaio/ird-observe/-/issues/513) **Deux soucis sur les requêtes de synthèse** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 538]](https://gitlab.com/ultreiaio/ird-observe/-/issues/538) **[REFERENTIEL] Clé étrangère manquante sur le champ vessel.fleetcountry** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 540]](https://gitlab.com/ultreiaio/ird-observe/-/issues/540) **Corruption de la base à la ré ouverture** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 670]](https://gitlab.com/ultreiaio/ird-observe/-/issues/670) **Exception en saisie LL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 731]](https://gitlab.com/ultreiaio/ird-observe/-/issues/731) **Bug lors de la saisie d&#39;une calée PS** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 736]](https://gitlab.com/ultreiaio/ird-observe/-/issues/736) **Petits défauts d&#39;affichage dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 738]](https://gitlab.com/ultreiaio/ird-observe/-/issues/738) **[LL] bug association panier avec TDR** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 876]](https://gitlab.com/ultreiaio/ird-observe/-/issues/876) **Problèmes de stabilité en 32 bits** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1272]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1272) **Ecran marée LL et PS - Aucun navire dans la liste déroulante** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1273]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1273) **Impossible de remplir le champ espèces** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1275]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1275) **Editeurs d&#39;url non actifs** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1276]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1276) **Incohérence date de début date de fin** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1277]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1277) **Création d&#39;équipement impossible** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1279]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1279) **Impossible d&#39;éditer nouveau DCP** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1281]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1281) **Pas d&#39;accès à l&#39;activité de fin de veille** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1283]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1283) **Erreur création activité** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1284]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1284) **Pas de warning malgré erreur dans les messages** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1285]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1285) **Pas de vérification de vitesses entre 2 activités** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1286]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1286) **Débarquements ne fonctionnent pas** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1287]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1287) **Echantillonsne fonctionnent pas** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1288]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1288) **Activité d&#39;observation associée** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1289]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1289) **pb traduction pop up activité observée associée** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1291]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1291) **Pb arbre de navigation - traduction - débarquement/échantillon** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1292]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1292) **[PS] Opérations sur balises non traduites** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1295]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1295) **[PS] Pb traduction Echantillon nespece accessoire** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1296]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1296) **[LL] Pas d&#39;alertes dans les onglets** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1302]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1302) **L&#39;asssitant connexion propose des raccourcis sur des touches qui n&#39;existent pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1305]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1305) **Groupe espèce obligatoire en modification mais pas en création** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1306]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1306) **Pas d&#39;accès aux informations techniques immédiatement après la création d&#39;un référentiel** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1307]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1307) **Erreur lors de la suppression d&#39;un référentiel espèces** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1309]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1309) **impossible de supprimer le référentiel dans systèmes observés** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1311]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1311) **Les objets en édition ne sont pas bien restitués lors d&#39;un redémarrage de l&#39;application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1312]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1312) **[LL] Certains formulaires ont des  onglets avec le raccourci clavier écrit deux fois** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1316]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1316) **[LL Composition détaillée] Sur le formulaire Brancheline deux champs non conditionnés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1317]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1317) **[LL Composition détaillée] Sur l&#39;écran Brancheline le label Temps depuis déclanchement dupliqué** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1318]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1318) **[LL Composition détaillée] Validateur en erreur sur le formulaire Brancheline** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1320]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1320) **[PS Route] Lors de la sauvegarde d&#39;une route le nœud Marée n&#39;est pas mis à jour** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1326]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1326) **[PS FOB Buoys] Revoir cet écran** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1327]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1327) **[LL] Programmes non traduits dans l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1330]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1330) **N° de versions mentionnés par l&#39;assistant migration** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1359]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1359) **Impossible de créer une activité LL logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1361]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1361) **Impossible de supprimer une activité LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1362]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1362) **[LL] Activité logbook : petits ajustements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1370]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1370) **[UI][REFERENTIEL] L&#39;icône &#39;voir toutes les utilisations de ce référentiel&#39; ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1371]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1371) **[UI][REFERENTIEL] Icône &#39;Toutes les clés métier de ce référentiel&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1372]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1372) **[UI][REFERENTIEL] [Mode Serveur] La suppression de tous les référentiels ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1391]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1391) **Impossible de lancer l&#39;opération d&#39;apparairment d&#39;activités LL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1392]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1392) **Répertoire des rapports à générer disparait si on annule le changement du répertoire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1393]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1393) **Labels non traduits sur l&#39;écran de validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1394]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1394) **Erreur lors de la sélection des référentiels dans l&#39;écran de validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1395]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1395) **on ne voit pas le nom des variables dans l&#39;écran de tableaux de synthèses** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1396]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1396) **Rapport LL ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1398]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1398) **Bouton &#39;Supprimer les marées par lot&#39; ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1400]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1400) **Génération de rapport - Modèle LL -Exception** (Thanks to Tony CHEMIT) (Reported by ezanno cedric)
  * [[Type::Anomalie 1403]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1403) **[PS] Affichage bizarre sur formulaire Calée/captures non cible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1404]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1404) **[LL] Impossible d&#39;afficher le formulaire TDR** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1405]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1405) **[LL] Logbooks - formulaire opération de pêche** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1415]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1415) **Bad extra zone layout in ContentTableUI** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1421]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1421) **Impossible de démarrer le mode H2 serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1422]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1422) **[LL] Problème sur le contrôle de fin de marée en mode modification** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1423]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1423) **L&#39;action de replier le slider d&#39;un éditeur de temps ne replie plus les autres éditeurs du même type sur un formulaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1426]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1426) **[V8] Form marée, espèces ciblées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1446]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1446) **Migration de base locale 7.3.0 vers 8RC5 échoue** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1447]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1447) **Reprise après corruption de base locale** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1451]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1451) **[SFA] LL / Logbooks / Captures** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1466]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1466) **Revoir la gestion du focus sur les formulaires à onglets, ne pas changer d&#39;onglet après une sauvegarde** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1467]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1467) **Problème de rafraîchissement sur les formulaire de type tableau** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1470]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1470) **Les températures ne sont pas bien restituées...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1476]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1476) **Absence du gestionnaire de connexions** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1478]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1478) **Fichiers de traductions manquants** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1486]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1486) **Impossible d&#39;éditer un référentiel :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1496]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1496) **[PS] Les listes de sélection d&#39;espèces cible sont vides, celle des espèces accessoires partielle** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1497]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1497) **Disfonctionnement de raccourcis** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1498]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1498) **[PS] Souci de validation sur form Marée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1499]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1499) **Un programme est marqué ouvert mais pas de marée ouverte à l&#39;intérieur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1500]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1500) **Réouverture simplifiée d&#39;item : l&#39;arbre de navigation est mal rafraîchit** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1501]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1501) **Imperfections sur création de nouveau FOB en utilisant les préconfigurations** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1503]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1503) **Enregistrement de FOB impossible si une balise est présente** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1504]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1504) **FOB enregistré non disponible immédiatement dans la popup de sélection d&#39;objet prédéfini** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1505]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1505) **Enregistrement de FOB -&gt; chagement d&#39;onglet intempestif** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1506]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1506) **Classement Calée / FOBs dans l&#39;arbre** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1508]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1508) **[PS] Calée / estimation banc** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1515]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1515) **Racourcis non fonctinonels** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1516]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1516) **L&#39;ajout d&#39;une caractéristique d&#39;équipement plante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1518]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1518) **MAJ+TAB fonctionne moyennement sur 8 RC7, comme sur 7.6.2** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1519]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1519) **LL Observations / Echec d&#39;enregistrement d&#39;une capture** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1524]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1524) **Widget horodatage dans un état instable** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1525]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1525) **[LL][Logbook] opération de pêche / Numéro : réaction inatendue à la touche &#39;d&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1527]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1527) **[LL][Logbook] opération de pêche / décimaux : gestion de la virgule** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1537]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1537) **[LL] Appairement des activités : cosmétique** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1540]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1540) **Impossible d&#39;afficher le &#96;À propos&#96;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1543]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1543) **[SYNCHRO AVANCEE MAREES] Barre de progression ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1547]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1547) **[CALCULS] L&#39;assistant calculs bloque après la sélection des marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1549]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1549) **L&#39;export de marées central vers local plante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1550]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1550) **La modification de la sélection du composant comboBox ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1555]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1555) **Amélioration de l&#39;onglet Sauvegarde (Actions longues)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1570]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1570) **Les actions de changement d&#39;ordre sur les tableaux ordonné ne fonctionnent plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 294]](https://gitlab.com/ultreiaio/ird-observe/-/issues/294) **Mise à jour du référentiel Stomach fullness** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 628]](https://gitlab.com/ultreiaio/ird-observe/-/issues/628) **Plusieurs marées sur les tableaux de synthèse** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 645]](https://gitlab.com/ultreiaio/ird-observe/-/issues/645) **[A PRECISER] Consignation de la méthode de mesure** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 784]](https://gitlab.com/ultreiaio/ird-observe/-/issues/784) **[PS] Faciliter le changement d&#39;espèce sur les lignes capture/échantillon/relâche** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 844]](https://gitlab.com/ultreiaio/ird-observe/-/issues/844) **Lorsque rien n&#39;est ouvert, permettre d&#39;ouvrir directement la feuille** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 915]](https://gitlab.com/ultreiaio/ird-observe/-/issues/915) **Ajout de règles de validation de niveau 1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 918]](https://gitlab.com/ultreiaio/ird-observe/-/issues/918) **Déplacement de groupes de données** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 970]](https://gitlab.com/ultreiaio/ird-observe/-/issues/970) **Mutualiser les topiaid t3 et ObServe** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1021]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1021) **[PS] Balise émettrice -&gt; Lier le champ appartenance (ownership) à une table de référence** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1066]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1066) **Improve usage guis** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1122]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1122) **[LL] Fournir le contenu de la table LL Compagnies destinataires des débarquements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1130]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1130) **[LL] Créer une liste d&#39;affichage d&#39;espèces propre aux captures, débarquements et échantillons LL Logbooks** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1155]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1155) **[PS] Permettre la modification de l&#39;espèce sur les formulaires déjà saisis** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1172]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1172) **Ajouter un générateur d&#39;id sur LL-Trip** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1182]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1182) **Corrections dans le référentiel (requises pour migration FINSS)** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1185]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1185) **[V8][DATA MODEL] Ajout de champs** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1212]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1212) **Le nom du fichier de sauvegarde par défaut pourrait être modernisé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1231]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1231) **Déploiement par JNLP ?** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1235]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1235) **Navigation : pertinence de la calée dans le menu ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1238]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1238) **Nouveau logo** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1239]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1239) **Improve select tree** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1267]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1267) **[PS] FOB / Balises : ajouter un champ &quot;bateau propriétaire&quot; guidé par la table vessel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1293]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1293) **Choix de nouvelles couleurs pour les symboles de la carte LL** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1299]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1299) **[V8][V9] Pour l&#39;algorithme d&#39;appariement d&#39;activités (LL comme PS), décider du comportement vis-à-vis de l&#39;absence d&#39;heures sur les activités (cas très fréquent sur les logbooks)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1313]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1313) **Revue de certains raccourcis clavier** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1314]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1314) **[SYNCHRO AVANCEE] Trier les référentiels dans ordre alphabétique dans les arbres** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1319]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1319) **[LL Composition détaillée] Quelques améliorations d&#39;ergonomies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1321]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1321) **[PS FOB] Mise en place des nouvelles templates de FOB** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1322]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1322) **[PS] Transformation de deux énumérations en référentiels** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1333]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1333) **[PS FOB] Intégration du nœud Balise en tant qu&#39;onglet du formulaire FOB** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1337]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1337) **[PS] Suppression de 2 champs** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1343]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1343) **[CARTE] Symboles de cartes pour V8** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1348]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1348) **[PS FOB] Déplacement de deux référentiels vers ps_common** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1349]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1349) **Renommer champs vessel.iattcid et vessel.ctoiid** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1358]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1358) **Création de nouvelles icônes de navigation** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1360]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1360) **Assouplir un contrôle de saisie** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1365]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1365) **[LL][LOGBOOKS] Plage de taille d&#39;un échantillon** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1366]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1366) **[LL][LOGBOOKS] Echantillons, ajouter l&#39;unité °** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1368]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1368) **[CARTO] Petits ajustement sur la carte** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1369]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1369) **[SERVEUR] observe-server.conf / observeweb.host** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1385]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1385) **[V8 ou V9] Passer l&#39;heure de fin de coulissage en optionnel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1386]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1386) **Lors d&#39;un &#39;Rechargement de la source&#39; ou d&#39;un appel aux filtres d&#39;affichage de l&#39;arbre, afficher une barre de progression** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1419]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1419) **Améliorer le dépliage dans l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1420]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1420) **[V8] Problèmes d&#39;affichage de formulaires** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1425]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1425) **[V8] Il ne devrait pas être possible de créer une marée dans un programme désactivé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1439]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1439) **[SYNCHRO AVANCEE REFERENTIEL] Visualisation du nom des bases en présence** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1448]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1448) **[SFA] Questions liste d&#39;espèces cibles LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1449]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1449) **[SFA] LL/Logbook/Trips/activity date by default** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1450]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1450) **[SFA] Assouplissement de contrôles sur LL Logbooks** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1452]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1452) **[SFA] LL / Logbooks / Samples : mode de saisie par défaut** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1453]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1453) **[SFA] LL / Logbooks / Samples : modification d&#39;espèce** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1454]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1454) **[SFA] Système de recopie de champs lors de la création d&#39;une nouvelle activité de pêche** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1464]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1464) **Amélioration des logs serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1465]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1465) **[SYNCHRO AVANCEE DATA] Visualisation du nom des bases en présence** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1468]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1468) **Suppression de l&#39;onglet résumé sur les assistants** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1471]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1471) **Utilisation de filtres intelligents pour l&#39;appairement des activités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1477]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1477) **Dans le gestionnaire de connexions, un bouton &#39;dupliquer le profil&#39; serait pratique** (Thanks to ) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1479]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1479) **Quelques améliorations dans le gestionnaire de connexion** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1480]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1480) **L&#39;écran de gestion des connexions pourrait être présenté en partage horizontal** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1481]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1481) **Les assistants create, update, update-security (et drop ?) devraient exiger que le compte fourni soit superuser** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1509]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1509) **Libellés référentiels** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1510]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1510) **Ajout d&#39;une méthode de mesure de poids par défaut sur Estimations du banc** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1517]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1517) **Carte** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1519]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1519) **LL Observations / Echec d&#39;enregistrement d&#39;une capture** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1520]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1520) **LL Observations / Capture / déprédation : libellé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1521]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1521) **Renommer un onglet sur form Marée LL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1522]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1522) **[LL] Form marée / Coche des cases &#39;Données logbook&#39; et &#39;Données d&#39;observation&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1523]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1523) **[LL][Logbooks] Besoin de pouvoir mettre les horodatages à NULL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1525]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1525) **[LL][Logbook] opération de pêche / Numéro : réaction inatendue à la touche &#39;d&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1526]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1526) **[LL][Logbook] opération de pêche / libellé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1531]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1531) **[LL][LOGBOOKS] Activité : champ vitesse vent superflu** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1534]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1534) **[LL][Logbooks] Form Opération de pêche / Capture : champs actifs en fonction du mode de saisie** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1535]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1535) **[LL][Echantillon logbook] Cosmétique échantillon dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1536]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1536) **[LL][Echantillon logbook] Cosmétique** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1537]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1537) **[LL] Appairement des activités : cosmétique** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1538]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1538) **[LL] Appairement des activités : assistant** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1551]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1551) **[LL Logbook Activity] Revoir finement l&#39;activité observée associée à une activité logbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1554]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1554) **[WEB SERVICE] Se passer du paramètre observeweb.host** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1557]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1557) **Ajouter une option pour ne pas afficher la légende dans la carte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1560]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1560) **Élimination des points non effectifs dans la carte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1152]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1152) **S&#39;assurer de la compatibilité de ObServe 8 avec Java 10** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1330]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1330) **N° de versions mentionnés par l&#39;assistant migration** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1417]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1417) **Revoir les barres de progressions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1493]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1493) **Comportement de l&#39;ouverture des programmes** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1508]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1508) **[PS] Calée / estimation banc** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1553]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1553) **Bascule entre langues** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [8.0-alpha](https://gitlab.com/ultreiaio/ird-observe/-/milestones/150)

**Closed at 2018-12-04.**


### Issues
  * [[Type::Anomalie 1105]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1105) **[V8-A3] Ouverture (migration) de bases locales V5 impossible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1106]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1106) **[V8-A3] Après migration d&#39;une base centrale, impossible de l&#39;ouvrir avec l&#39;application** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1108]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1108) **[V8] L&#39;enregistrement d&#39;une activité logbook LL échoue lorsque l&#39;on tente des créer des activités logbook dans une marée issue d&#39;une marée d&#39;observation saisie en V5** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1109]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1109) **[V8] L&#39;accès au formulaire Echantillons logbooks LL ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1111]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1111) **[V8-A3] Ne peut pas créer d&#39;échantillon LL Logbook dans cette base** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1112]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1112) **Some data (in table forms) order is not deterministic** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1113]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1113) **Fix some coordinates validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1117]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1117) **[LL] Correction de libellés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 578]](https://gitlab.com/ultreiaio/ird-observe/-/issues/578) **[LL] Implanter le formulaire logbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 782]](https://gitlab.com/ultreiaio/ird-observe/-/issues/782) **[LL][PS] Ajouter un flag qualité sur les marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 889]](https://gitlab.com/ultreiaio/ird-observe/-/issues/889) **Ajout de nouvelles entrées dans le référentiel observe_common.VesselType** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 890]](https://gitlab.com/ultreiaio/ird-observe/-/issues/890) **Modification du référentiel Person** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 891]](https://gitlab.com/ultreiaio/ird-observe/-/issues/891) **Ajout de nouvelles entrées au référentiel observe_longline.VesselActivity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 892]](https://gitlab.com/ultreiaio/ird-observe/-/issues/892) **Modification du référentiel Wind** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 893]](https://gitlab.com/ultreiaio/ird-observe/-/issues/893) **Ajout du référentiel observe_longline.LogbookTripType** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 894]](https://gitlab.com/ultreiaio/ird-observe/-/issues/894) **Ajout du référentiel observe_longline.DataSource** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 896]](https://gitlab.com/ultreiaio/ird-observe/-/issues/896) **Ajout du référentiel observe_common.Compagnies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 897]](https://gitlab.com/ultreiaio/ird-observe/-/issues/897) **Ajout du référentiel observe_longline.OnBoardProcessing** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 898]](https://gitlab.com/ultreiaio/ird-observe/-/issues/898) **Ajout du référentiel observe_longline.Conservation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 899]](https://gitlab.com/ultreiaio/ird-observe/-/issues/899) **Ajout du référentiel observe_common.WeightMeasureMethod** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 900]](https://gitlab.com/ultreiaio/ird-observe/-/issues/900) **Ajout du référentiel observe_common.LengthMeasureMethod** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 901]](https://gitlab.com/ultreiaio/ird-observe/-/issues/901) **Amélioration du composant de cartographie** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 902]](https://gitlab.com/ultreiaio/ird-observe/-/issues/902) **Visualisation des données du livre de bord sur la carte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 903]](https://gitlab.com/ultreiaio/ird-observe/-/issues/903) **Renommage des « données observateurs » du modèle Palangre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 904]](https://gitlab.com/ultreiaio/ird-observe/-/issues/904) **Réusinage de la donnée observe_longline.Trip** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 905]](https://gitlab.com/ultreiaio/ird-observe/-/issues/905) **Adaptation de l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 906]](https://gitlab.com/ultreiaio/ird-observe/-/issues/906) **Amélioration de l&#39;éditeur de température** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 907]](https://gitlab.com/ultreiaio/ird-observe/-/issues/907) **Ajout de la donnée observe_longline.ActivityLogbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 908]](https://gitlab.com/ultreiaio/ird-observe/-/issues/908) **Nouvelle action « Apparier les activités logbook/observations »** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 909]](https://gitlab.com/ultreiaio/ird-observe/-/issues/909) **Ajout de la donnée observe_longline.SetLogbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 910]](https://gitlab.com/ultreiaio/ird-observe/-/issues/910) **Définition de la composition globale de la ligne pour les logbooks** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 911]](https://gitlab.com/ultreiaio/ird-observe/-/issues/911) **Ajout de la donnée observe_longline.CatchLogbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 912]](https://gitlab.com/ultreiaio/ird-observe/-/issues/912) **Ajout de la donnée observe_longline.Landing** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 913]](https://gitlab.com/ultreiaio/ird-observe/-/issues/913) **Ajout de la donnée observe_longline.SampleLogbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 914]](https://gitlab.com/ultreiaio/ird-observe/-/issues/914) **Gestion des transbordements** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 916]](https://gitlab.com/ultreiaio/ird-observe/-/issues/916) **Tableaux de synthèse** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 917]](https://gitlab.com/ultreiaio/ird-observe/-/issues/917) **Gestion des méthodes de mesure sur les formulaires existants** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 919]](https://gitlab.com/ultreiaio/ird-observe/-/issues/919) **Renommage du référentiel observe_longline.TripType** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 920]](https://gitlab.com/ultreiaio/ird-observe/-/issues/920) **Ajout du référentiel observe_longline.WeightCategory** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 921]](https://gitlab.com/ultreiaio/ird-observe/-/issues/921) **Ajout du référentiel observe_longline.WeightDeterminationMethod** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 922]](https://gitlab.com/ultreiaio/ird-observe/-/issues/922) **Ajoute le référentiel observe_common.DataQuality sur les données existantes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 950]](https://gitlab.com/ultreiaio/ird-observe/-/issues/950) **Renommage de la classe seine.WeightCategory en seine.WeightCategorySeine** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 951]](https://gitlab.com/ultreiaio/ird-observe/-/issues/951) **Ajout du référentiel observe_longline.TripType** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1078]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1078) **Improve navigation popup actions** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1084]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1084) **Improve Move API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1096]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1096) **Improve Open/Close API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1115]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1115) **[LL] Les positions géographiques de fin de filage, début et fin de virage ne doivent pas être obligatoires** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1118]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1118) **[LL] Activité : Petits ajustements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1120]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1120) **[LL] Rendre fonctionnels les boutons &quot;Déplacer échantillon&quot;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1121]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1121) **[LL] Débarquements : petits ajustements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1124]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1124) **[LL] Marée : Petits ajustements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1125]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1125) **[LL] Opération de pêche : recopie de propriétés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1126]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1126) **[LL] Opération de pêche : petits ajustements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1128]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1128) **[LL] Composition globale de la palangre : petits ajustements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1129]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1129) **[LL] Capture : petits ajustements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1131]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1131) **[LL][PS] Dans le widget liste déroulante d&#39;espèces, après le code FAO et le non scientifique, ajouter le nom commun (vernaculaire) dans la langue courante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1132]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1132) **[LL] Echantillons : Petits ajustements** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1133]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1133) **[LL][PS] Améliorer le widget position** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1135]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1135) **[LL] Marée : redondance des listes Type de marée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1137]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1137) **[LL] Débarquements : petits ajustements 2** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1138]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1138) **Internationaliser le référentiel port** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1150]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1150) **Calcul de l&#39;activité d&#39;observation sur l&#39;écran de l&#39;activité logbook** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1162]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1162) **En mode de compatibilité &quot;référentiels obsolètes&quot;, les rendre utilisable en saisie** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1166]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1166) **[V8][DATA MODEL] Retours sur le modèle V8 alpha** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1181]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1181) **Ajouter un champ vessel.lloydid** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1183]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1183) **[V8][DATA MODEL] Transformer Vessel.fleetcountry (int4) en topiaid** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.14](https://gitlab.com/ultreiaio/ird-observe/-/milestones/233)

**Closed at 2022-07-29.**

### Download
* [Application (observe-7.6.14.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.14/observe-7.6.14.zip)
* [Serveur (observe-7.6.14.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.14/observe-7.6.14.war)

### Issues
  * [[Type::Anomalie 1669]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1669) **[SYNCHRO][7.6.6][V8] L&#39;algo de synchro simple bloque sur des cas qu&#39;il devrait certainement pouvoir gérer** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1749]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1749) **Problème de synchro simple sur Country** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2392]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2392) **Erreur dans l&#39;identification des bases de droite et gauche dans la synchro avancée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2393]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2393) **Error sur synchro avancée de marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2414]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2414) **Impossible de mettre à jour le référentiel (méthode simple) de ces 2 bases locales 7.6 (avec la 7.6.13) pour cause de violation de clé étrangère** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2424]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2424) **Faux positifs détectés dans la synchronisation de référentiels sur les associations multiples** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2149]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2149) **Nommage des fichiers de log** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2413]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2413) **Moteur de scripting non trouvé en ouvrant ObServe 7.6.13 avec Java 17** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.13](https://gitlab.com/ultreiaio/ird-observe/-/milestones/223)

**Closed at 2022-05-02.**

### Download
* [Application (observe-7.6.13.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.13/observe-7.6.13.zip)
* [Serveur (observe-7.6.13.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.13/observe-7.6.13.war)

### Issues
  * [[Type::Evolution 2309]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2309) **Problème de quadrant** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.12](https://gitlab.com/ultreiaio/ird-observe/-/milestones/218)

**Closed at 2022-04-30.**

### Download
* [Application (observe-7.6.12.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.12/observe-7.6.12.zip)
* [Serveur (observe-7.6.12.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.12/observe-7.6.12.war)

### Issues
  * [[Type::Anomalie 1635]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1635) **StomachFullness** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2152]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2152) **Boutons de cadrage sur la carte 7.6.11** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2177]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2177) **Référentiel / Systèmes Observés / &#39;Voir toutes les utilisations de ce référentiel&#39; ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2200]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2200) **Petit défaut de mise en page sur 7.6.11** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2214]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2214) **Impossible de créer une base centrale vierge, quelque soit la méthode appliquée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2303]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2303) **Besoin d&#39;aide sur une base à intégrer rapidement** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 2307]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2307) **Impossible d&#39;exporter toutes les marées d&#39;une centrale en local en 7.6.11** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2209]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2209) **Mise à jour de dépendences** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2306]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2306) **Amélioration de la synchronisation simple de référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 2305]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2305) **Souci d&#39;import de la base AZTI dans cette base centrale** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.11](https://gitlab.com/ultreiaio/ird-observe/-/milestones/214)

**Closed at 2022-01-06.**

### Download
* [Application (observe-7.6.11.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.11/observe-7.6.11.zip)
* [Serveur (observe-7.6.11.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.11/observe-7.6.11.war)

### Issues
  * [[Type::Evolution 2151]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2151) **Update to log4j 2.27.1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [7.6.10](https://gitlab.com/ultreiaio/ird-observe/-/milestones/213)

**Closed at 2021-12-24.**

### Download
* [Application (observe-7.6.10.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.10/observe-7.6.10.zip)
* [Serveur (observe-7.6.10.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.10/observe-7.6.10.war)

### Issues
  * [[Type::Evolution 2148]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2148) **Suppression des paramètres de configuration sur le serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [7.6.9](https://gitlab.com/ultreiaio/ird-observe/-/milestones/212)

**Closed at 2021-12-19.**

### Download
* [Application (observe-7.6.9.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.9/observe-7.6.9.zip)
* [Serveur (observe-7.6.9.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.9/observe-7.6.9.war)

### Issues
  * [[Type::Evolution 2146]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2146) **update log4j to 2.17.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [7.6.8](https://gitlab.com/ultreiaio/ird-observe/-/milestones/180)

**Closed at 2021-12-17.**


### Issues
  * [[Type::Anomalie 1356]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1356) **[CALCULS] Une marée fait planter la procédure de calcul en mode serveur uniquement** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1784]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1784) **Problème sur la gestion des référentiels manquant lors d&#39;un import** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2012]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2012) **L&#39;écran de gestion du référentiel Port n&#39;enregistre pas les coordonnées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2143]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2143) **Mise à jour de librairies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2144]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2144) **Amélioration configuration serveur minimale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [7.6.7](https://gitlab.com/ultreiaio/ird-observe/-/milestones/174)

**Closed at 2021-01-20.**

### Download
* [Application (observe-7.6.7.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.7/observe-7.6.7.zip)
* [Serveur (observe-7.6.7.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.7/observe-7.6.7.war)

### Issues
  * [[Type::Anomalie 1755]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1755) **Problème d&#39;enregistrement sur avançon depuis le formulaire capture** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1756]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1756) **Problème dans la gestion des onglets du formulaire &#39;Avançon&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1757]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1757) **Souci de gestion sur 12h du champ &#39;Temps depuis déclenchement&#39;** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1625]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1625) **[LL][Observations] Horodatage hook timers par défaut** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Evolution 1754]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1754) **Découplage case &#39;Horloge&#39; et champ &#39;Horodatage de montée à bord&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.6](https://gitlab.com/ultreiaio/ird-observe/-/milestones/170)

**Closed at 2020-11-01.**

### Download
* [Application (observe-7.6.6.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.6/observe-7.6.6.zip)
* [Serveur (observe-7.6.6.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.6/observe-7.6.6.war)

### Issues
  * [[Type::Anomalie 1624]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1624) **[LL] Perte des données saisies &quot;catchhealthstatus&quot; et &quot;discardhealthstatus&quot; dans table &quot;catch&quot; depuis le passage à 7.6.2** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1631]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1631) **[PS] Champ &quot;type de mensuration&quot; ne se met pas à jour** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)

## Version [7.6.5](https://gitlab.com/ultreiaio/ird-observe/-/milestones/169)

**Closed at 2020-08-10.**

### Download
* [Application (observe-7.6.5.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.5/observe-7.6.5.zip)
* [Serveur (observe-7.6.5.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.5/observe-7.6.5.war)

### Issues
  * [[Type::Anomalie 927]](https://gitlab.com/ultreiaio/ird-observe/-/issues/927) **Penser à tester le comportement des large objects sur la V7** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 942]](https://gitlab.com/ultreiaio/ird-observe/-/issues/942) **Plantage base en sortie de l&#39;outil de traduction** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1584]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1584) **[UI] Correction des libellés des tables de référence du schéma observe.longline dans Observe** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1585]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1585) **Listes à choix multiples et mode permissif sur référentiels désactivés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1587]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1587) **traductions manquantes (UI validation)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 887]](https://gitlab.com/ultreiaio/ird-observe/-/issues/887) **Ajouter une fonction de suppression en masse de marées/routes/activités** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1583]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1583) **[PS] Contrôle de la saisie des espèces dans faune accessoire par systèmes observés (cas requin baleine)** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)

## Version [7.6.4](https://gitlab.com/ultreiaio/ird-observe/-/milestones/166)

**Closed at 2020-07-08.**

### Download
* [Application (observe-7.6.4.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.4/observe-7.6.4.zip)
* [Serveur (observe-7.6.4.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.4/observe-7.6.4.war)

### Issues
  * [[Type::Anomalie 1533]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1533) **Deux comportements étranges sur l&#39;assistant tableaux de synthèse** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1552]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1552) **[PS][Observations] Activité : contrôle trop strict** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1558]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1558) **[LL] Onglet Composition détaillée / Détail avançon / Hameçon et appât non consultable** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1561]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1561) **[7.6.3] Rappel d&#39;espèce / catégorie** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1562]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1562) **[7.6.3][PS] Les thonidés sont disponibles dans la liste déroulante du form Espèces accessoires, alors qu&#39;ils ne sont pas dans la display liste correspondante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1563]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1563) **Correction des composants graphiques numériques et sélection** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1567]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1567) **les programmes ne sont pas triés dans le même ordre dans l&#39;arbre de navigation et dans la formulaire liste des marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1568]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1568) **le reset reste actif sur les éditeurs de nombres même si ceci ne sont pas actif** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1569]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1569) **le reset des combo box ne fonctionne pas bien (alors que l&#39;action associée Ctrl+D est ok)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1571]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1571) **Problème de traduction sur les validations taille/poids des espèces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1572]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1572) **Deux coquilles dans les validations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1573]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1573) **Revoir la gestion des validateurs taille-poids d&#39;espèces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1575]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1575) **[PS] Pratiques de remises à l&#39;eau : le mode de libération n&#39;est pas supprimé si on change d&#39;espèce** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1564]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1564) **[PS] Pratiques de remise à l&#39;eau, cétacés non trouvés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1565]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1565) **Pouvoir changer l&#39;espèce d&#39;un enregistrement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1529]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1529) **Requêtes de synthèse PS** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1574]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1574) **Amélioration des logs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [7.6.3](https://gitlab.com/ultreiaio/ird-observe/-/milestones/165)

**Closed at 2020-06-22.**

### Download
* [Application (observe-7.6.3.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.3/observe-7.6.3.zip)
* [Serveur (observe-7.6.3.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.3/observe-7.6.3.war)

### Issues
  * [[Type::Anomalie 1494]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1494) **LL / Opération de pêche / Composition globale / Avançons / Longueur bas de ligne : ce champ était facultatif et est devenu obligatoire** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1530]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1530) **[LL] Activité, Capture, Enregistreur de profondeur Le raccourci clavier MAJ+TAB (focus arrière) ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1528]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1528) **[PS] Rappel de la dernière espèce utilisée dans la saisie des échantillons non cible** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)

## Version [7.6.2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/164)

**Closed at 2020-05-26.**

### Download
* [Application (observe-7.6.2.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.2/observe-7.6.2.zip)
* [Serveur (observe-7.6.2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.2/observe-7.6.2.war)

### Issues
  * [[Type::Anomalie 1492]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1492) **Migration des trip.comment vers trip.homeid incorrecte** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/163)

**Closed at 2020-05-23.**

### Download
* [Application (observe-7.6.1.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.1/observe-7.6.1.zip)
* [Serveur (observe-7.6.1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.1/observe-7.6.1.war)

### Issues
  * [[Type::Evolution 1490]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1490) **Nom et version de l&#39;application Web** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/162)

**Closed at 2020-05-21.**

### Download
* [Application (observe-7.6.0.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.0/observe-7.6.0.zip)
* [Serveur (observe-7.6.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.0/observe-7.6.0.war)

### Issues
  * [[Type::Anomalie 1271]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1271) **[SYNCHRO SIMPLE] Le référentiel SPECIESGROUP_SPECIESGROUPRELEASEMODE n&#39;est pas mis à jour** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1432]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1432) **[SYNCHRO MAREES] L&#39;assistant demande un remplacement de code manquant, même si celui-ci n&#39;est pas utilisé par les données** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1485]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1485) **Version de base dans tms_version** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1487]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1487) **Ne pas sauvegarder automatiquement la base locale lors d&#39;une action longue** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1482]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1482) **La suppression d&#39;une marée pourrait être permise même si le formulaire n&#39;est pas parfaitement conforme** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1484]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1484) **Synchro simple des listes d&#39;espèce** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

