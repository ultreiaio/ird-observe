# Install commands

To be able to use some usefull commands, please run this unique command. It will create a **~/ultreiaio** directory
with some nice bash scripts.

```
wget -q -O - https://gitlab.com/ultreiaio/pom/raw/develop/bin/install.sh | bash
```

# Perform full release with no stage

```
bash ~/ultreiaio/close-milestone.sh
bash ~/ultreiaio/create-stage.sh
bash ~/ultreiaio/close-and-release-stage.sh
bash ~/ultreiaio/update-changelog.sh
bash ~/ultreiaio/create-milestone.sh
```

# Create Release stage

```
bash ~/ultreiaio/close-milestone.sh
bash ~/ultreiaio/create-stage.sh
bash ~/ultreiaio/drop-stage.sh
bash ~/ultreiaio/update-staging-changelog.sh
bash ~/ultreiaio/create-milestone.sh
```

# Release stage

``` 
bash ~/ultreiaio/release-stage.sh
```

# Drop stage

```
bash ~/ultreiaio/drop-stage.sh
```

# Regenerate changelog

``` 
bash ~/ultreiaio/update-changelog.sh
```

# Regenerate staging changelog

``` 
bash ~/ultreiaio/update-staging-changelog.sh
```

# Generate site

``` 
mvn clean verify site -DperformRelease scm-publish:publish-scm
```

# Deploy latest demo

``` 
bash ~/ultreiaio/execute-maven.sh '-Padd-git-commit-id-to-project-version -N'
bash ~/ultreiaio/execute-maven.sh 'install -am -pl application-web -DskipTests -Pdeploy-demo -Dclassifier=latest'
```


# Add a referential

Example for *seine.ObjectMaterialType* on version *6.901*

* [persistence] edit persistence/src/main/models/Observe.model
* [persistence] add migration script persistence/src/main/resources/db/migration/6.901/xx_add_referenial-common.sql
* [persistence] register this script in migrationResolver fr.ird.observe.entities.migration.DataSourceMigrationForVersion_7_0
* [persistence] register type in fr.ird.observe.entities.Entities
* [services] edit services/src/main/models/Observe.model
* build `mvn clean install -DskipTests`

will fails in services

* [services] register in fr.ird.observe.dto.referential.ReferentialHelper
* [services-topia] add Binder fr.ird.observe.binder.referential.seine.ObjectMaterialTypeEntityDtoBinder
* [services-topia] implements new method in fr.ird.observe.services.binder.EntityBindersInitializerOld
* [services] implements new method in fr.ird.observe.dto.ObserveDtoInitializer
* [services] register in fr.ird.observe.dto.decoration.DecoratorService.modelInitializer class
* [services] add i18n key observe.type.objectMaterialType in application-swing-decoration i18n bundle
* [services] add i18n key observe.type.objectMaterialTypes in application-swing-decoration i18n bundle
* [validation] add swing validation in validation/src/main/resources/fr/ird/observe/services/dto/referential
* [client] add swing referential editor

* build `mvn clean install -DskipTests`

* migrate db test `mvn -Pupdate-test-dbs -pl services-topia`
* [tests] register in fr.ird.observe.test.ObserveFixtures (count depends on your migration script)
* [tests] increments lastupdatedate count in fr.ird.observe.test.ObserveFixtures 
* [validation] fix test fr.ird.observe.client.validation.BeanValidatorDetectorTest
* [services-topia] fix test fr.ird.observe.services.local.service.actions.validate.ValidateServiceLocalTest
* build `mvn clean install`

