# ObServe

[![Maven Central status](https://img.shields.io/maven-central/v/fr.ird.observe/observe.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22fr.ird.observe%22%20AND%20a%3A%22observe%22)
![Build Status](https://gitlab.com/ultreiaio/ird-observe/badges/develop/pipeline.svg)
[![The GNU General Public License, Version 3.0](https://img.shields.io/badge/license-GPL3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.txt)

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/ird-observe/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/ird-observe)

# Demo

* [Demo (latest)](https://demo.ultreia.io/observe-latest)
* [Demo (5.x)](https://demo.ultreia.io/observe-5.3.5)
* [Demo (6.x)](https://demo.ultreia.io/observe-6.0-RC-1)

# Community

* [Mailing-list (Devel)](http://list.forge.codelutin.com/cgi-bin/mailman/listinfo/observe-devel)
* [Mailing-list (Commits)](http://list.forge.codelutin.com/cgi-bin/mailman/listinfo/observe-commits)
* [Contact](mailto:dev@tchemit.fr)
