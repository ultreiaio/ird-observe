description = ClientConfig.description

[option applicationName]
description = application.config.option.build.application.name
key = build.application.name
type = string
defaultValue = @@{model.name}
final = true
transient = true

[option applicationOrganisationName]
description = application.config.option.build.application.organisation.name
key = build.application.organisation.name
type = string
defaultValue = @@{project.organization.name}
final = true
transient = true

[option applicationOrganisationUrl]
description = application.config.option.build.application.organisation.url
key = build.application.organisation.url
type = url
defaultValue = @@{project.organization.url}
final = true
transient = true

[option applicationSiteUrl]
description = application.config.option.build.application.site.url
key = build.application.site.url
type = url
defaultValue = https://ultreiaio.gitlab.io/ird-observe/
final = true
transient = true

[option buildDate]
description = application.config.option.build.date
key = build.date
type = date
defaultValue = @@{buildDate}
final = true
transient = true

[option buildNumber]
description = application.config.option.build.number
key = build.number
type = string
defaultValue = @@{buildNumber}
final = true
transient = true

[option buildVersion]
description = application.config.option.build.version
key = build.version
type = version
defaultValue = @@{project.version}
final = true
transient = true

[option buildMajorVersion]
description = application.config.option.build.version.major
key = build.version.major
type = version
defaultValue = @@{major}
final = true
transient = true

[option modelVersion]
description = application.config.option.build.version.persistence
key = build.version.persistence
type = version
defaultValue = @@{persistence.model.version}
final = true
transient = true

[option remoteDataSourceConfigurationsFile]
description = observe.config.remoteDataSourceConfigurations.description
key = common.ui.file.remoteDataSourceConfigurations
type = file
defaultValue = ${instance.data.directory}/config/server-preset.json
final = true
transient = true

[option serverDataSourceConfigurationsFile]
description = observe.config.serverDataSourceConfigurations.description
key = common.ui.file.serverDataSourceConfigurations
type = file
defaultValue = ${instance.data.directory}/config/remote-preset.json
final = true
transient = true

[option avdthDirectory]
description = observe.config.avdthDirectory.description
key = instance.avdth.directory
type = file
defaultValue = ${instance.data.directory}/backup

[option avdthForceImport]
description = observe.config.avdth.forceImport
key = instance.avdth.forceImport
type = Boolean
defaultValue = false

[option backupAtClose]
description = observe.config.backup.atClose
key = instance.backup.atClose
type = Boolean
defaultValue = true

[option backupDelay]
description = observe.config.backup.delay
key = instance.backup.delay
type = Integer
defaultValue = 30

[option backupDirectory]
description = observe.config.defaultBackupDirectory.description
key = instance.backup.directory
type = file
defaultValue = ${instance.data.directory}/backup

[option backupsDirectory]
description = observe.config.backupsDirectory.description
key = instance.backups.directory
type = file
defaultValue = ${instance.data.directory}/backup
final = true
transient = true

[option backupsFile]
description = observe.config.backupsFile.description
key = instance.backup.file
type = file
defaultValue = ${instance.data.directory}/backups.json
final = true
transient = true

[option backupUse]
description = observe.config.backup.use
key = instance.backup.use
type = Boolean
defaultValue = true

[option backupFilesTimeout]
description = observe.config.client.backupFiles.timeout.description
key = instance.config.client.backupFilesTimeout
type = Integer
defaultValue = 2160

[option feedbackFilesTimeout]
description = observe.config.client.feedbackFiles.timeout.description
key = instance.config.client.feedbackFilesTimeout
type = Integer
defaultValue = 720

[option logFilesTimeout]
description = observe.config.client.logFiles.timeout.description
key = instance.config.client.logFilesTimeout
type = Integer
defaultValue = 720

[option temporaryFilesTimeout]
description = observe.config.client.temporaryFiles.timeout.description
key = instance.config.client.temporaryFilesTimeout
type = Integer
defaultValue = 120

[option dataDirectory]
description = observe.config.defaultDataDirectory.description
key = instance.data.directory
type = file
defaultValue = ${user.home}/.observe
final = true
transient = true

[option defaultDataSourceConnectMode]
description = observe.config.defaultDataSourceConnectMode
key = instance.db.defaultDataSourceConnectMode
type = fr.ird.observe.datasource.configuration.DataSourceConnectMode
defaultValue = LOCAL

[option defaultDataSourceCreateMode]
description = observe.config.defaultDataSourceCreateMode
key = instance.db.defaultDataSourceCreateMode
type = fr.ird.observe.datasource.configuration.DataSourceCreateMode
defaultValue = IMPORT_EXTERNAL_DUMP

[option dbDirectory]
description = observe.config.defaultLocalDbDirectory.description
key = instance.db.directory
type = file
defaultValue = ${instance.data.directory}/db
final = true
transient = true

[option h2CanEditReferential]
description = observe.config.h2.can.editReferential.description
key = instance.db.h2.canEditReferential
type = Boolean
defaultValue = false

[option h2CanMigrate]
description = observe.config.h2.can.migrate.description
key = instance.db.h2.canMigrate
type = Boolean
defaultValue = true

[option h2Password]
description = observe.config.h2.password.description
key = instance.db.h2.password
type = string
defaultValue = sa
final = true
transient = true

[option h2ServerPort]
description = observe.config.h2.serverPort.description
key = instance.db.h2.serverPort
type = Integer
defaultValue = 9093

[option h2Login]
description = observe.config.h2.login.description
key = instance.db.h2.username
type = string
defaultValue = sa
final = true
transient = true

[option initialDbDump]
description = observe.config.defaultInitialDbDump.description
key = instance.db.initial.db.dump
type = file
defaultValue = ${instance.data.directory}/initial-database.sql.gz
final = true
transient = true

[option dbLocale]
description = observe.config.db.locale
key = instance.db.locale
type = locale
defaultValue = fr_FR

[option obstunaCanMigrate]
description = observe.config.pg.can.migrate.description
key = instance.db.pg.canMigrate
type = Boolean
defaultValue = false

[option showSql]
description = observe.config.showSql
key = instance.db.showSql
type = Boolean
defaultValue = false

[option dcpPresetsDirectory]
description = observe.config.dcpPresetsDirectory.description
key = instance.dcp.presets.directory
type = file
defaultValue = ${instance.resources.directory}/dcp-presets
final = true
transient = true

[option feedBackDirectory]
description = observe.config.feedBackDirectory.description
key = instance.feedBack.directory
type = file
defaultValue = ${instance.data.directory}/feedback
final = true
transient = true

[option simplifiedObjectTypeSpecializedRulesFile]
description = observe.config.simplifiedObjectTypeSpecializedRules.description
key = instance.file.simplifiedObjectTypeSpecializedRules
type = file
defaultValue = ${instance.resources.directory}/observe-specialized-fad-rules.properties
final = true
transient = true

[option httpTimeout]
description = observe.config.httpTimeout.description
key = instance.http.timeout
type = int
defaultValue = 30000

[option i18nDirectory]
description = observe.config.defaultI18nDirectory.description
key = instance.i18n.directory
type = file
defaultValue = ${instance.resources.directory}/i18n
final = true
transient = true

[option importDirectory]
description = observe.config.importDirectory.description
key = instance.import.directory
type = file
defaultValue = ${instance.data.directory}/backup

[option logDirectory]
description = observe.config.defaultLogDbDirectory.description
key = instance.log.directory
type = file
defaultValue = ${instance.data.directory}/log
final = true
transient = true

[option logConfigurationFile]
description = observe.config.logConfigurationFile.description
key = instance.logConfigurationFile
type = file
defaultValue = ${instance.resources.directory}/observe-log4j2.xml
transient = true

[option useJvmKeyStore]
description = observe.config.useJvmKeyStore.description
key = instance.useJvmKeyStore
type = Boolean
defaultValue = false

[option longlineActivityPairingMaxDistance]
description = observe.config.longlineActivityPairing.maxDistance
key = instance.longlineActivityPairing.maxDistance
type = long
defaultValue = 50

[option longlineActivityPairingMaxTime]
description = observe.config.longlineActivityPairing.maxTime
key = instance.longlineActivityPairing.maxTime
type = long
defaultValue = 30

[option longlineLandingPartDefaultWeightMeasureMethod]
description = observe.config.longlineLandingPart.defaultWeightMeasureMethod
key = instance.longlineLandingPart.defaultWeightMeasureMethod
type = string
defaultValue = fr.ird.referential.common.WeightMeasureMethod#666#02

[option mapDirectory]
description = observe.config.defaultMapDirectory.description
key = instance.map.directory
type = file
defaultValue = ${instance.resources.directory}/map
final = true
transient = true

[option reportDirectory]
description = observe.config.defaultReportDirectory.description
key = instance.report.directory
type = file
defaultValue = ${instance.resources.directory}/report

[option resourcesDirectory]
description = observe.config.defaultResourcesDirectory.description
key = instance.resources.directory
type = file
defaultValue = ${instance.data.directory}/resources-${version}
final = true
transient = true

[option checkServerVersion]
description = observe.config.checkServerVersion.description
key = instance.server.checkServerVersion
type = Boolean
defaultValue = true

[option swingSessionFile]
description = observe.config.swingSessionFile.description
key = instance.swingSessionFile
type = file
defaultValue = ${instance.resources.directory}/config/ObserveSwingSession.ui.xml
final = true
transient = true

[option temporaryDirectory]
description = observe.config.temporaryDirectory.description
key = instance.temporary.directory
type = file
defaultValue = ${instance.data.directory}/tmp
final = true
transient = true

[option exportDirectory]
description = observe.config.exportDirectory.description
key = instance.export.directory
type = file
defaultValue = ${instance.data.directory}/export
final = true
transient = true

[option navigationEditModelFile]
description = observe.config.ui.tree.edit.nodes
key = instance.ui.file.tree.edit.nodes
type = file
defaultValue = ${instance.resources.directory}/config/edit-model.json
final = true
transient = true

[option navigationLastSelectedPath]
description = observe.config.ui.tree.last.selected.path
key = instance.ui.file.tree.last.selected.path
type = String[]

[option validationFactoryName]
description = observe.config.validation.factoryName
key = instance.validation.factoryName
type = string
defaultValue = default

[option validationLengthWeightEnable]
description = observe.config.validation.lengthWeightEnable
key = instance.validation.lengthWeightEnable
type = Boolean
defaultValue = true

[option validationReportDirectory]
description = observe.config.defaultValidationReportDirectory.description
key = instance.validation.report.directory
type = file
defaultValue = ${instance.data.directory}/validation-report
final = true
transient = true

[option validationSpeedEnable]
description = observe.config.validation.speedEnable
key = instance.validation.speedEnable
type = Boolean
defaultValue = true

[option validationSpeedMaxValue]
description = observe.config.validation.speedMaxValue
key = instance.validation.speedMaxValue
type = Float
defaultValue = 30.0

[option validationUseDisabledReferential]
description = observe.config.validation.useDisabledReferential
key = instance.validation.useDisabledReferential
type = Boolean
defaultValue = false

[option consolidationFailIfLengthWeightParameterNotFound]
description = observe.config.consolidation.failIfLengthWeightParameterNotFound
key = instance.consolidation.failIfLengthWeightParameterNotFound
type = Boolean
defaultValue = false

[option consolidationFailIfLengthLengthParameterNotFound]
description = observe.config.consolidation.failIfLengthLengthParameterNotFound
key = instance.consolidation.failIfLengthLengthParameterNotFound
type = Boolean
defaultValue = false

[option consolidationSpeciesListForLogbookSampleActivityWeightedWeight]
description = observe.config.consolidation.speciesListForLogbookSampleActivityWeightedWeight
key = instance.consolidation.speciesListForLogbookSampleActivityWeightedWeight
type = String
defaultValue = fr.ird.referential.common.SpeciesList#1464000000000#100

[option consolidationSpeciesListForLogbookSampleWeights]
description = observe.config.consolidation.speciesListForLogbookSampleWeights
key = instance.consolidation.speciesListForLogbookSampleWeights
type = String
defaultValue = fr.ird.referential.common.SpeciesList#1464000000000#101

[option sizeMeasureTypeSeineObservationTargetSampleId]
description = observe.config.sizeMeasureType.seine.observation.targetSample
key = referential.sizeMeasureType.seine.observation.targetSample
type = String[]
defaultValue = fr.ird.referential.common.SizeMeasureType#1433499466774#0.529249255312607,fr.ird.referential.common.SizeMeasureType#1433499465700#0.0902433863375336

[option seineLocalmarketSampleSizeMeasureTypeId]
description = observe.config.sizeMeasureType.seine.localmarket.sample
key = referential.sizeMeasureType.type.seine.localmarket.sample
type = String[]
defaultValue = fr.ird.referential.common.SizeMeasureType#1433499466774#0.529249255312607,fr.ird.referential.common.SizeMeasureType#1433499465700#0.0902433863375336,fr.ird.referential.common.SizeMeasureType#1433499466532#0.844473292818293

[option seineLogbookSampleSizeMeasureTypeId]
description = observe.config.sizeMeasureType.seine.logbook.sample
key = referential.sizeMeasureType.type.seine.logbook.sample
type = String[]
defaultValue = fr.ird.referential.common.SizeMeasureType#1433499466774#0.529249255312607,fr.ird.referential.common.SizeMeasureType#1433499465700#0.0902433863375336,fr.ird.referential.common.SizeMeasureType#1433499466532#0.844473292818293

[option speciesListLonglineCommonTripId]
description = observe.config.speciesList.longline.common.trip
key = referential.speciesList.longline.common.trip
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1239832675370#0.7

[option speciesListLonglineDepredatorId]
description = observe.config.speciesList.longline.depredator
key = referential.speciesList.longline.depredator
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1239832675370#0.5

[option speciesListLonglineLandingId]
description = observe.config.speciesList.longline.landing
key = referential.speciesList.longline.landing
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1239832675370#0.8

[option speciesListLonglineLogbookCatchId]
description = observe.config.speciesList.longline.logbook.catch
key = referential.speciesList.longline.logbook.catch
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1239832675370#0.8

[option speciesListLonglineLogbookSampleId]
description = observe.config.speciesList.longline.logbook.sample
key = referential.speciesList.longline.logbook.sample
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1239832675370#0.8

[option speciesListLonglineObservationCatchId]
description = observe.config.speciesList.longline.observation.catch
key = referential.speciesList.longline.observation.catch
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1239832675370#0.3

[option speciesListLonglineObservationEncounterId]
description = observe.config.speciesList.longline.observation.encounter
key = referential.speciesList.longline.observation.encounter
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1239832675370#0.4

[option speciesListSeineLandingId]
description = observe.config.speciesList.seine.landing
key = referential.speciesList.seine.landing
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1464000000000#0.10

[option speciesListSeineLocalmarketId]
description = observe.config.speciesList.seine.localmarket
key = referential.speciesList.seine.localmarket
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1464000000000#0.9

[option speciesListSeineLogbookCatchId]
description = observe.config.speciesList.seine.logbook.catch
key = referential.speciesList.seine.logbook.catch
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1464000000000#0.9

[option speciesListSeineLogbookWellPlanId]
description = observe.config.speciesList.seine.logbook.wellPlan
key = referential.speciesList.seine.logbook.wellplan
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1464000000000#0.9

[option speciesListSeineObservationCatchId]
description = observe.config.speciesList.seine.observation.catch
key = referential.speciesList.seine.observation.catch
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1464000000000#0.9

[option speciesListSeineObservationObjectObservedSpeciesId]
description = observe.config.speciesList.seine.observation.objectObservedSpecies
key = referential.speciesList.seine.observation.objectObservedSpecies
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1239832675370#0.2

[option speciesListSeineObservationObjectSchoolEstimateId]
description = observe.config.speciesList.seine.observation.objectSchoolEstimate
key = referential.speciesList.seine.observation.objectSchoolEstimate
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1239832675370#0.1

[option speciesListSeineObservationSchoolEstimateId]
description = observe.config.speciesList.seine.observation.schoolEstimate
key = referential.speciesList.seine.observation.schoolEstimate
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1239832675370#0.1

[option speciesListSeineSampleId]
description = observe.config.speciesList.seine.sample
key = referential.speciesList.seine.sample
type = string
defaultValue = fr.ird.referential.common.SpeciesList#1464000000000#0.9

[option vesselTypeLonglineLandingId]
description = observe.config.vessel.type.longline.landing
key = referential.vesselType.longline.landing
type = String[]
defaultValue = fr.ird.referential.common.VesselType#1308149641588#0.8724092935671164,fr.ird.referential.common.VesselType#1308149718650#0.791946073811996

[option autoPopupNumberEditor]
description = observe.config.ui.autoPopupNumberEditor
key = ui.autoPopupNumberEditor
type = Boolean
defaultValue = false

[option autoSelectText]
description = observe.config.ui.autoSelectText
key = ui.autoSelectText
type = Boolean
defaultValue = true

[option blockStateColor]
description = observe.config.ui.blockStateColor
key = ui.blockState.color
type = color
defaultValue = java.awt.Color[r=50,g=50,b=50]

[option busyStateColor]
description = observe.config.ui.busyStateColor
key = ui.busyState.color
type = color
defaultValue = java.awt.Color[r=50,g=50,b=50]

[option changeSynchroSrc]
description = observe.config.ui.changeSynchroSrc
key = ui.changeSynchroSrc
type = Boolean
defaultValue = false

[option coordinateFormat]
description = observe.config.coordinate.format
key = ui.coordinate.format
type = org.nuiton.jaxx.widgets.gis.CoordinateFormat
defaultValue = dmd

[option floatingObjectMaterialErrorColor]
description = observe.config.ui.dcp.error.color
key = ui.dcp.error.color
type = color
defaultValue = java.awt.Color[r=255,g=100,b=100]

[option floatingObjectMaterialNotEditableColor]
description = observe.config.ui.dcp.not.editable.color
key = ui.dcp.not.editable.color
type = color
defaultValue = java.awt.Color[r=193,g=250,b=250]

[option focusBorderColor]
description = observe.config.ui.focusBorderColor
key = ui.focusBorder.color
type = color
defaultValue = java.awt.Color[r=64,g=64,b=64]

[option loadLocalStorage]
description = observe.config.ui.loadLocalStorage
key = ui.loadLocalStorage
type = Boolean
defaultValue = true

[option locale]
description = observe.config.ui.locale
key = ui.locale
type = locale
defaultValue = fr_FR

[option mapBackgroundColor]
description = observe.config.map.background.description
key = ui.map.background.color
type = color
defaultValue = java.awt.Color[r=87,g=200,b=255]

[option mapDateFormat]
description = observe.config.map.dateFormat
key = ui.map.date.format
type = string
defaultValue = MM-dd

[option mapLayer1Color]
description = observe.config.map.layer1.color
key = ui.map.layer1.color
type = color
defaultValue = java.awt.Color[r=251,g=233,b=215]

[option mapLayer1]
description = observe.config.map.layer1.description
key = ui.map.layer1.path
type = file
defaultValue = ${instance.map.directory}/shapeFiles/continents/GSHHS_l_L1.shp

[option mapLayer10]
description = observe.config.map.layer10.description
key = ui.map.layer10.path
type = file

[option mapLayer2Color]
description = observe.config.map.layer2.color
key = ui.map.layer2.color
type = color
defaultValue = java.awt.Color[r=245,g=245,b=255]

[option mapLayer2]
description = observe.config.map.layer2.description
key = ui.map.layer2.path
type = file
defaultValue = ${instance.map.directory}/shapeFiles/continents/GSHHS_l_L6.shp

[option mapLayer3Color]
description = observe.config.map.layer3.color
key = ui.map.layer3.color
type = color
defaultValue = java.awt.Color[r=127,g=183,b=255]

[option mapLayer3]
description = observe.config.map.layer3.description
key = ui.map.layer3.path
type = file
defaultValue = ${instance.map.directory}/shapeFiles/lakesAndSeas/GSHHS_l_L2.shp

[option mapLayer4Color]
description = observe.config.map.layer4.color
key = ui.map.layer4.color
type = color
defaultValue = java.awt.Color[r=251,g=233,b=215]

[option mapLayer4]
description = observe.config.map.layer4.description
key = ui.map.layer4.path
type = file
defaultValue = ${instance.map.directory}/shapeFiles/borders/WDBII_border_l_L1.shp

[option mapLayer5Color]
description = observe.config.map.layer5.color
key = ui.map.layer5.color
type = color
defaultValue = java.awt.Color[r=130,g=244,b=249]

[option mapLayer5]
description = observe.config.map.layer5.description
key = ui.map.layer5.path
type = file
defaultValue = ${instance.map.directory}/shapeFiles/zee/eez_v11_lowres.shp

[option mapLayer6]
description = observe.config.map.layer6.description
key = ui.map.layer6.path
type = file

[option mapLayer7]
description = observe.config.map.layer7.description
key = ui.map.layer7.path
type = file

[option mapLayer8]
description = observe.config.map.layer8.description
key = ui.map.layer8.path
type = file

[option mapLayer9]
description = observe.config.map.layer9.description
key = ui.map.layer9.path
type = file

[option mapLlStyleLogbookLineHaulingColor]
description = observe.config.map.ll.style.logbook.line.hauling.color
key = ui.map.ll.style.logbook.line.hauling.color
type = color
defaultValue = java.awt.Color[r=0,g=255,b=255]

[option mapLlStyleLogbookLineSettingColor]
description = observe.config.map.ll.style.logbook.line.setting.color
key = ui.map.ll.style.logbook.line.setting.color
type = color
defaultValue = java.awt.Color[r=255,g=0,b=0]

[option mapLlStyleLogbookLineTripColor]
description = observe.config.map.ll.style.logbook.line.trip.color
key = ui.map.ll.style.logbook.line.trip.color
type = color
defaultValue = java.awt.Color[r=34,g=34,b=34]

[option mapLlStyleLogbookPointColor]
description = observe.config.map.ll.style.logbook.point.color
key = ui.map.ll.style.logbook.point.color
type = color
defaultValue = java.awt.Color[r=255,g=0,b=0]

[option mapLlStyleObservationsLineHaulingColor]
description = observe.config.map.ll.style.observations.line.hauling.color
key = ui.map.ll.style.observations.line.hauling.color
type = color
defaultValue = java.awt.Color[r=255,g=255,b=0]

[option mapLlStyleObservationsLineSettingColor]
description = observe.config.map.ll.style.observations.line.setting.color
key = ui.map.ll.style.observations.line.setting.color
type = color
defaultValue = java.awt.Color[r=0,g=255,b=0]

[option mapLlStyleObservationsLineTripColor]
description = observe.config.map.ll.style.observations.line.trip.color
key = ui.map.ll.style.observations.line.trip.color
type = color
defaultValue = java.awt.Color[r=0,g=255,b=0]

[option mapLlStyleObservationsPointColor]
description = observe.config.map.ll.style.observations.point.color
key = ui.map.ll.style.observations.point.color
type = color
defaultValue = java.awt.Color[r=0,g=255,b=0]

[option mapLlStyleFile]
description = observe.config.map.ll.style.description
key = ui.map.ll.style.path
type = file
defaultValue = ${instance.map.directory}/ll-style.xml
final = true

[option mapPsStyleLogbookLineColor]
description = observe.config.map.ps.style.logbook.line.color
key = ui.map.ps.style.logbook.line.color
type = color
defaultValue = java.awt.Color[r=255,g=0,b=255]

[option mapPsStyleLogbookPointColor]
description = observe.config.map.ps.style.logbook.point.color
key = ui.map.ps.style.logbook.point.color
type = color
defaultValue = java.awt.Color[r=255,g=0,b=0]

[option mapPsStyleLogbookWithSamplingPointColor]
description = observe.config.map.ps.style.logbook.withSampling.point.color
key = ui.map.ps.style.logbook.withSampling.point.color
type = color
defaultValue = java.awt.Color[r=255,g=255,b=0]

[option mapPsStyleLogbookTextColor]
description = observe.config.map.ps.style.logbook.text.color
key = ui.map.ps.style.logbook.text.color
type = color
defaultValue = java.awt.Color[r=0,g=0,b=0]

[option mapPsStyleLogbookTextSize]
description = observe.config.map.ps.style.logbook.text.size
key = ui.map.ps.style.logbook.text.size
type = Integer
defaultValue = 14

[option mapPsStyleLogbookTransmittingBuoyLostPointColor]
description = observe.config.map.ps.style.logbook.transmitting.buoy.lost.point.color
key = ui.map.ps.style.logbook.transmitting.buoy.lost.point.color
type = color
defaultValue = java.awt.Color[r=122,g=17,b=135]

[option mapPsStyleObservationsLineColor]
description = observe.config.map.ps.style.observations.line.color
key = ui.map.ps.style.observations.line.color
type = color
defaultValue = java.awt.Color[r=255,g=0,b=0]

[option mapPsStyleObservationsPointColor]
description = observe.config.map.ps.style.observations.point.color
key = ui.map.ps.style.observations.point.color
type = color
defaultValue = java.awt.Color[r=0,g=255,b=0]

[option mapPsStyleObservationsTextColor]
description = observe.config.map.ps.style.observations.text.color
key = ui.map.ps.style.observations.text.color
type = color
defaultValue = java.awt.Color[r=0,g=0,b=0]

[option mapPsStyleObservationsTextSize]
description = observe.config.map.ps.style.observations.text.size
key = ui.map.ps.style.observations.text.size
type = Integer
defaultValue = 14

[option mapPsStyleFile]
description = observe.config.map.ps.style.description
key = ui.map.ps.style.path
type = file
defaultValue = ${instance.map.directory}/ps-style.xml
final = true

[option navigationIconsPath]
description = observe.config.navigationIconsPath.description
key = ui.navigation.icons.path
type = string
defaultValue = navigation/default
final = true
transient = true

[option noFocusBorderColor]
description = observe.config.ui.noFocusBorderColor
key = ui.noFocusBorderColor.color
type = color
defaultValue = java.awt.Color[r=192,g=192,b=192]

[option psLogbookWellPlanCanCreateActivity]
description = observe.config.ui.psLogbookWellPlanCanCreateActivity
key = ui.psWellPlanCanCreateActivity
type = Boolean
defaultValue = false

[option seineBycatchObservedSystem]
description = observe.config.ui.seineBycatchObservedSystem.description
key = ui.seineBycatchObservedSystem
type = fr.ird.observe.dto.validation.SeineBycatchObservedSystemConfig
defaultValue = {\"fr.ird.referential.common.Species#1239832684290#0.04680507324710936\": [\"fr.ird.referential.ps.common.ObservedSystem#0#1.0\",\"fr.ird.referential.ps.common.ObservedSystem#0#1.1\",\"fr.ird.referential.ps.common.ObservedSystem#1239832686428#0.9217864901728908\"]}

[option showMnemonic]
description = observe.config.ui.showMnemonic
key = ui.showMnemonic
type = Boolean
defaultValue = true

[option showDataSourceOptionalActions]
description = observe.config.ui.showDataSourceOptionalActions
key = ui.showDataSourceOptionalActions
type = Boolean
defaultValue = false

[option showNumberEditorButton]
description = observe.config.ui.showNumberEditorButton
key = ui.showNumberEditorButton
type = Boolean
defaultValue = true

[option storeRemoteStorage]
description = observe.config.ui.storeRemoteStorage
key = ui.storeRemoteStorage
type = Boolean
defaultValue = true

[option tableEmptyRowColor]
description = observe.config.ui.table.empty.row.color
key = ui.table.empty.row.color
type = color
defaultValue = java.awt.Color[r=255,g=255,b=0]

[option temperatureFormat]
description = observe.config.temperature.format
key = ui.temperature.format
type = org.nuiton.jaxx.widgets.temperature.TemperatureFormat
defaultValue = C

[option nauticalLengthFormat]
description = observe.config.nauticalLength.format
key = ui.nauticalLength.format
type = io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthFormat
defaultValue = M

[option showSensibleCriteriaInTripBulkChanges]
description = observe.config.showSensibleCriteriaInTripBulkChanges
key = ui.showSensibleCriteriaInTripBulkChanges
type = Boolean
defaultValue = false

[option treeGroupByName]
description = observe.config.ui.tree.groupByName
key = ui.tree.config.groupByName
type = String
defaultValue = dataPsCommonTripGroupByObservationsProgram

[option treeGroupByFlavor]
description = observe.config.ui.tree.groupByFlavor
key = ui.tree.config.groupByFlavor
type = String
defaultValue = year

[option treeModuleName]
description = observe.config.ui.tree.moduleName
key = ui.tree.config.moduleName
type = String
defaultValue = ps

[option treeLoadReferential]
description = observe.config.ui.tree.loadReferential
key = ui.tree.config.loadReferential
type = Boolean
defaultValue = true

[option treeLoadData]
description = observe.config.ui.tree.loadData
key = ui.tree.config.loadData
type = Boolean
defaultValue = true

[option treeLoadDisabledGroupBy]
description = observe.config.ui.tree.loadDisabledGroupBy
key = ui.tree.config.loadDisabledGroupBy
type = Boolean
defaultValue = true

[option treeLoadEmptyGroupBy]
description = observe.config.ui.tree.loadEmptyGroupBy
key = ui.tree.config.loadEmptyGroupBy
type = Boolean
defaultValue = true

[option treeLoadNullGroupBy]
description = observe.config.ui.tree.loadNullGroupBy
key = ui.tree.config.loadNullGroupBy
type = Boolean
defaultValue = true

[option treeLoadTemporalGroupBy]
description = observe.config.ui.tree.loadTemporalGroupBy
key = ui.tree.config.loadTemporalGroupBy
type = Boolean
defaultValue = true

[option treeNodeDisabledColor]
description = observe.config.ui.tree.node.disabled.color
key = ui.tree.config.node.disabled.color
type = color
defaultValue = java.awt.Color[r=128,g=128,b=128]

[option treeNodeEmptyColor]
description = observe.config.ui.tree.node.empty.color
key = ui.tree.config.node.empty.color
type = color
defaultValue = java.awt.Color[r=255,g=175,b=175]

[option treeNodeUnloadedColor]
description = observe.config.ui.tree.node.unloaded.color
key = ui.tree.config.node.unloaded.color
type = color
defaultValue = java.awt.Color[r=144,g=144,b=241]

[option treeNodeUnsavedColor]
description = observe.config.ui.tree.node.unsaved.color
key = ui.tree.config.node.unsaved.color
type = color
defaultValue = java.awt.Color[r=255,g=0,b=0]

[option skipConsolidateStepForReport]
description = observe.config.ui.actions.report.skipConsolidateStep
key = ui.actions.report.skipConsolidateStep
type = Boolean
defaultValue = false

[action adminUi]
description = observe.ui.action.commandline.launch.admin.ui
action = fr.ird.observe.client.ObserveCLAction#launchAdminUI
aliases = -a,--admin

[action configureUi]
description = observe.ui.action.commandline.configure.ui
action = fr.ird.observe.client.ObserveCLAction#configure
aliases = -c,--configure

[action createId]
description = observe.ui.action.commandline.create.id
action = fr.ird.observe.client.ObserveCLAction#createId
aliases = --create-id

[action h2ServerMode]
description = observe.ui.action.commandline.launch.h2.server.mode
action = fr.ird.observe.client.ObserveCLAction#launchH2ServerMode
aliases = --h2-server

[action help]
description = observe.ui.action.commandline.help
action = fr.ird.observe.client.ObserveCLAction#help
aliases = -h,--help

[action helpUi]
description = observe.ui.action.commandline.help.ui
action = fr.ird.observe.client.ObserveCLAction#helpUI
aliases = --help-ui

[action noMainUi]
description = observe.ui.action.commandline.disable.main.ui
action = fr.ird.observe.client.ObserveCLAction#disableMainUI
aliases = -n,--no-main

[action remoteAdminUi]
description = observe.ui.action.commandline.launch.remote.admin.ui
action = fr.ird.observe.client.ObserveCLAction#launchRemoteAdminUI
aliases = -r,--remote-admin

[action serverAdminUi]
description = observe.ui.action.commandline.launch.server.admin.ui
action = fr.ird.observe.client.ObserveCLAction#launchServerAdminUI
aliases = -s,--server-admin

[action useJmx]
description = observe.ui.action.commandline.use.jmx
action = fr.ird.observe.client.ObserveCLAction#useJMX
aliases = --jmx

