<#--
 #%L
 ObServe Client :: Configuration
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<h3>ObServe</h3>
<a href="http://www.ird.fr/informatique-scientifique/projets/observe/">Système d'Information,
  d'Observation et de Suivi des pêches thonières tropicales de surface.</a>
<hr/>
<p>
  Aplicación de introducción de datos de observadores y consulta de los mismos desde una base <i>Obstuna</i>.
</p>
<p>
  El proyecto se inició en 2008 por la unidad US 007-OSIRIS de <a href="http://www.ird.fr">l'IRD</a> en el marco
  de un
  <a href="http://www.ird.fr/informatique-scientifique/soutien/spirales/anciens_projets/affiche_projet.php?code=2008.11">project
    spirale</a>.</p>
<p>
  Ha siso realizado por la sociedad <a href="https://ultreia.io">Ultreia.io</a> en 2009.
</p>
<br/>
<hr/>
<p>
 Para más información, puede visistar la <a href="https://ultreiaio.gitlab.io/ird-observe">site du projet</a>.
</p>

<h4>Versión de software</h4>
<dl>
  <dt>Versión</dt>
  <dd>${buildVersion}</dd>
  <dt>Fecha</dt>
  <dd>${buildDate?string('dd.MM.yyyy HH:mm:ss')}</dd>
  <dt>Número de registro</dt>
  <dd><a href="https://gitlab.com/ultreiaio/ird-observe/-/commit/${buildNumber}">${buildNumber}</a></dd>
</dl>

</body>
</html>
