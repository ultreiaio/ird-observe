<#--
 #%L
 ObServe Client :: Configuration
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<h3>ObServe</h3>
<a href="http://www.ird.fr/informatique-scientifique/projets/observe/">Système informatique de gestion de données
  d'observations embarquées de pêche à la senne et à la palangre.</a>
<hr/>
<p>
  Ce projet a été initiée par l'IRD en 2009. Les contributeurs financiers sont :
</p>
<ul>
  <li>Institut de Recherche pour le Développement (IRD)</li>
  <li>Fond Européen pour les Affaires Maritimes et la Pêche (FEAMP)</li>
  <li>Seychelles Fishing Authority (SFA)</li>
  <li>AZTI Tecnalia</li>
  <li>Orthongel</li>
</ul>
<p>
  Il a été réalisé par la société <a href="https://ultreia.io">Ultreia.io</a> depuis 2009.
</p>
<br/>
<hr/>
<p>
  Pour plus d'informations, vous pouvez visiter le <a href="https://ultreiaio.gitlab.io/ird-observe">site du projet</a>.
</p>

<h4>Version du locigiel</h4>
<dl>
  <dt>Version</dt>
  <dd>${buildVersion}</dd>
  <dt>Date</dt>
  <dd>${buildDate?string('dd.MM.yyyy HH:mm:ss')}</dd>
  <dt>Numéro de build</dt>
  <dd><a href="https://gitlab.com/ultreiaio/ird-observe/-/commit/${buildNumber}">${buildNumber}</a></dd>
</dl>

</body>
</html>
