/*
 * #%L
 * ObServe Client :: Configuration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.configuration;

import com.google.common.io.MoreFiles;
import com.google.common.io.RecursiveDeleteOption;
import com.google.gson.Gson;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeSpecializedRules;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import fr.ird.observe.datasource.security.WithPermission;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.data.TripMapConfig;
import fr.ird.observe.dto.data.TripMapContentBuilder;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;
import fr.ird.observe.dto.presets.RemoteDataSourceConfiguration;
import fr.ird.observe.dto.presets.ServerDataSourceConfiguration;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.navigation.tree.ToolkitTreeLoadingConfig;
import fr.ird.observe.navigation.tree.TreeConfig;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.navigation.tree.selection.SelectionTreeConfig;
import fr.ird.observe.report.definition.ReportDefinitionsBuilder;
import fr.ird.observe.services.ObserveServiceInitializerConfig;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import fr.ird.observe.spi.json.guava.ImmutableListAdapter;
import io.ultreia.java4all.application.context.spi.ApplicationComponentInstantiateStrategy;
import io.ultreia.java4all.application.context.spi.GenerateApplicationComponent;
import io.ultreia.java4all.application.template.TemplateGeneratorConfig;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.config.ApplicationConfig;
import io.ultreia.java4all.config.ArgumentsParserException;
import io.ultreia.java4all.config.ConfigHelper;
import io.ultreia.java4all.config.ConfigResource;
import io.ultreia.java4all.config.clean.CleanTemporaryFilesTaskConfiguration;
import io.ultreia.java4all.config.spi.ApplicationConfigInit;
import io.ultreia.java4all.config.spi.ApplicationConfigScope;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.boot.DefaultI18nBootLoader;
import io.ultreia.java4all.i18n.runtime.boot.UserI18nBootLoader;
import io.ultreia.java4all.i18n.spi.I18nTemplateDefinition;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.SortedProperties;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import io.ultreia.java4all.validation.impl.java.NuitonValidatorProviderFactoryImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.application.ApplicationRunner;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;
import org.nuiton.jaxx.widgets.temperature.TemperatureFormat;

import javax.swing.ImageIcon;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static io.ultreia.java4all.i18n.I18n.t;
import static org.nuiton.jaxx.runtime.swing.SwingUtil.ICON_PREFIX;

/**
 * La configuration de l'application.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
@GenerateApplicationComponent(name = "ObServe client config", hints = {TripMapConfig.class, TemplateGeneratorConfig.class, CleanTemporaryFilesTaskConfiguration.class}, instantiateStrategy = ApplicationComponentInstantiateStrategy.SUPPLIER, instantiateSupplier = ClientConfigFinder.class)
@GenerateTemplate(template = "about.ftl")
@GenerateJavaBeanDefinition
public class ClientConfig extends GeneratedClientConfig implements TripMapConfig, TemplateGeneratorConfig, JavaBean, CleanTemporaryFilesTaskConfiguration {

    public static final String DB_NAME = "obstuna";
    public static final List<ClientConfigOption> MAP_LAYERS = List.of(ClientConfigOption.MAP_LAYER1, ClientConfigOption.MAP_LAYER2, ClientConfigOption.MAP_LAYER3, ClientConfigOption.MAP_LAYER4, ClientConfigOption.MAP_LAYER5, ClientConfigOption.MAP_LAYER6, ClientConfigOption.MAP_LAYER7, ClientConfigOption.MAP_LAYER8, ClientConfigOption.MAP_LAYER9, ClientConfigOption.MAP_LAYER10);
    public static final String APPLICATION_NAME = "observe-client";
    private static final Logger log = LogManager.getLogger(ClientConfig.class);
    /**
     * le pattern du fichier de sauvegarde d'une base locale
     */
    private static final String BACKUP_DB_PATTERN = "observe-v%1$s-%2$tF--%2$tk-%2$tM-%2$tS.sql.gz";
    private static final String FEED_BACK_PATTERN = "observe-v%1$s-feedback-%2$s_%3$s.zip";
    private static final String APPLICATION_VERSION = "application.version";
    private static final String VERSION = "version";

    private static final String DEFAULT_OBSERVE_SWING_CONFIGURATION_FILENAME = APPLICATION_NAME + ".conf";
    /**
     * Liste des options qu'on ne peut pas sauvegarder (les mots de passes,
     * les options d'admin pour les simples utilisateurs,...).
     *
     * @since 1.5
     */
    private final Set<String> doNotSave = Set.of(
//            ClientConfigOption.OBSTUNA_PASSWORD.getKey(),
            ClientConfigOption.H2_PASSWORD.getKey(), ClientConfigOption.OBSTUNA_CAN_MIGRATE.getKey());

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm z");
    /**
     * un drapeau pour savoir s'il faut lancer l'interface graphique. Cette
     * valeur peut être programmées lors des actions.
     */
    private boolean displayMainUI = true;
    /**
     * drapeau pour savoir si une base locale existe
     */
    private boolean localStorageExist;
    /**
     * drapeau pour savoir si le dump initial a ete chargée depuis la base
     * centrale
     */
    private boolean initialDumpExist;
    /**
     * drapeau pour savoir si on peut utiliser des ui dans l'environnement.
     * <p>
     * Par défaut, on suppose qu'on peut utiliser l'environnement graphique et
     * si on désactive explicitement ou si pas d'environnement graphique
     * trouvé.
     */
    private boolean canUseUI = true;
    /**
     * La version de l'application
     */
    private Version version;
    /**
     * Texte du copyright (calculé dynamiquement).
     */
    private String copyrightText;
    private List<ServerDataSourceConfiguration> serverDataSourceConfigurationList;
    private List<RemoteDataSourceConfiguration> remoteDataSourceConfigurationList;

    private DtoGsonSupplier gsonSupplier;
    private ReferentialLocale referentialLocale;
    private SimplifiedObjectTypeSpecializedRules simplifiedObjectTypeSpecializedRules;
    private Set<TripMapContentBuilder> tripMapContentBuilders;
    private File feedBackDirectoryFile;
    /**
     * Lazy helper class that manages all java bean operations.
     *
     * @see #javaBeanDefinition()
     */
    private transient JavaBeanDefinition javaBeanDefinition;
    private transient ObserveServiceInitializerConfig serviceInitializerConfig;

    private ClientConfig(ApplicationConfigInit init) {
        super(init.setConfigFileName(DEFAULT_OBSERVE_SWING_CONFIGURATION_FILENAME).setExtraConfigDirectory("${instance.data.directory}"));
        NuitonValidatorProviders.setDefaultFactoryName(NuitonValidatorProviderFactoryImpl.PROVIDER_NAME);
    }

    public static ClientConfig forTest() {
        ClientConfig clientConfig = new ClientConfig(ApplicationConfigInit.forAllScopesWithout(ApplicationConfigScope.HOME, ApplicationConfigScope.ENV, ApplicationConfigScope.SYSTEM));
        clientConfig.setBackupUse(false);
        return clientConfig;
    }

    public static void initI18n(ClientConfig config) {

        I18n.close();

        Path i18nDirectory = config.getI18nDirectory().toPath();

        if (config.isVersionSnapshot() && Files.exists(i18nDirectory)) {
            // always regenerate i18n
            try (Stream<Path> walk = Files.walk(i18nDirectory)) {
                walk.sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
            } catch (IOException e) {
                throw new IllegalStateException("Can't delete i18n directory: " + i18nDirectory, e);
            }

        }
        UserI18nBootLoader i18nInitializer = new UserI18nBootLoader(i18nDirectory, new DefaultI18nBootLoader(I18nConfiguration.createDefaultConfiguration()));
        long t00 = System.nanoTime();

        Locale locale = config.getLocale();

        I18n.init(i18nInitializer, locale);

        log.debug(String.format("i18n language : %s", locale));
        log.debug(String.format("i18n loading time : %s", Strings.convertTime(t00, System.nanoTime())));
    }

    public static DecoratorService getDecoratorService() {
        return getDecoratorService(null);
    }

    public static DecoratorService getDecoratorService(ReferentialLocale referentialLocale) {
        Optional<Object> optional = Optional.ofNullable(UIManager.getDefaults().get("DecoratorService"));
        if (optional.isPresent()) {
            return (DecoratorService) optional.get();
        }
        if (referentialLocale == null) {
            referentialLocale = ReferentialLocale.FR;
        }
        initDecoratorService(referentialLocale);
        return getDecoratorService(referentialLocale);
    }

    public static void initDecoratorService(ReferentialLocale referentialLocale) {
        UIManager.getDefaults().put("DecoratorService", (UIDefaults.LazyValue) table -> new DecoratorService(referentialLocale));
    }

    public static String getColorToHex(Color color) {
        return String.format("#%02X%02X%02X", color.getRed(), color.getGreen(), color.getBlue());
    }

    public static ClientConfig forRunner(ApplicationRunner runner) throws Exception {
        ClientConfig config = new ClientConfig(ApplicationConfigInit.forAllScopesWithout(ApplicationConfigScope.HOME, ApplicationConfigScope.ENV, ApplicationConfigScope.SYSTEM));
        config.initConfig(runner.getArgs());
        if (log.isInfoEnabled()) {
            String message = config.getConfigurationDescription();
            log.info(message);
        }
        return config;
    }

    @Override
    protected ApplicationConfig createApplicationConfig(ApplicationConfigInit init) {
        return new ApplicationConfig(init) {

            @Override
            public void save(File file, boolean forceAll, String... excludeKeys) {
                ClientConfig.this.saveForUser();
            }
        };
    }

    public NavigationTreeConfig getNavigationConfig(WithPermission permissions) {
        NavigationTreeConfig result = new NavigationTreeConfig();
        fillTreeConfig(result, permissions);
        return result;
    }

    public SelectionTreeConfig getSelectionConfig(WithPermission permissions) {
        SelectionTreeConfig result = new SelectionTreeConfig();
        fillTreeConfig(result, permissions);
        return result;
    }

    public void saveTreeConfig(ToolkitTreeLoadingConfig newConfig) {
        setTreeModuleName(newConfig.getModuleName());
        setTreeGroupByName(newConfig.getGroupByName());
        setTreeGroupByFlavor(newConfig.getGroupByFlavor());
        setTreeLoadData(newConfig.isLoadData());
        setTreeLoadReferential(newConfig.isLoadReferential());
        setTreeLoadDisabledGroupBy(newConfig.isLoadDisabledGroupBy());
        setTreeLoadEmptyGroupBy(newConfig.isLoadEmptyGroupBy());
        setTreeLoadNullGroupBy(newConfig.isLoadNullGroupBy());
        setTreeLoadTemporalGroupBy(newConfig.isLoadTemporalGroupBy());
        log.info(String.format("Save navigation tree config: %s", newConfig));
        saveForUser();
    }

    private void fillTreeConfig(TreeConfig result, WithPermission permissions) {
        result.setModuleName(getTreeModuleName());
        result.setGroupByName(getTreeGroupByName());
        result.setGroupByFlavor(getTreeGroupByFlavor());
        result.setLoadDisabledGroupBy(isTreeLoadDisabledGroupBy());
        result.setLoadEmptyGroupBy(isTreeLoadEmptyGroupBy());
        result.setLoadNullGroupBy(isTreeLoadNullGroupBy());
        result.setLoadTemporalGroupBy(isTreeLoadTemporalGroupBy());
        if (permissions != null) {
            boolean canLoadData = permissions.canReadData();
            result.setCanLoadData(canLoadData);
            boolean canLoadReferential = permissions.canReadReferential();
            result.setCanLoadReferential(canLoadReferential);
            result.setLoadData(canLoadData && isTreeLoadData());
            result.setLoadReferential(canLoadReferential && isTreeLoadReferential());
        } else {
            result.setLoadData(isTreeLoadData());
            result.setLoadReferential(isTreeLoadReferential());
        }
    }

    public File initLog() {
        File logFile = getLogConfigurationFile();
        log.info(String.format("Loading user log file at %s", logFile));

        ObserveUtil.loadLogConfiguration(ClientResources.LOG_CONFIGURATION_FILE, null, logFile.toPath(), this);
        return logFile;
    }

    public void initUIConfiguration() {
        // prepare ui look&feel and load ui properties
        try {
            SwingUtil.initNimbusLoookAndFeel();
        } catch (Exception e) {
            // could not find nimbus look-and-feel
            log.warn(t("observe.warning.nimbus.landf", e));
        } catch (Throwable e) {
            log.warn(t("observe.warning.no.ui", e));
            // no ui
            setCanUseUI(false);
        }

        if (isCanUseUI()) {
            // load ui configurations
            loadUIConfig();
        }
    }

    public void reloadEmbeddedDcpPresets() throws IOException {
        File dcpPresetsDirectory = getDcpPresetsDirectory();
        if (dcpPresetsDirectory.exists()) {
            MoreFiles.deleteDirectoryContents(dcpPresetsDirectory.toPath());
        }
        dcpPresetsDirectory = createDirectory(ClientConfigOption.DCP_PRESETS_DIRECTORY);
        unzipToDirectory(ClientResources.DCP_PRESETS, ClientConfigOption.RESOURCES_DIRECTORY, t("observe.runner.copy.default.dcp.file", dcpPresetsDirectory));
    }

    public void initUserDirectories() throws IOException {

        // 1 - user data directory

        File dataDirectory = createDirectory(ClientConfigOption.DATA_DIRECTORY);
        log.debug("user data directory : {}", dataDirectory);
        createParentDirectory(ClientConfigOption.DB_DIRECTORY);
        createParentDirectory(ClientConfigOption.INITIAL_DB_DUMP);

        File extraConfigFile = get().getExtraConfigFile();
        if (Files.notExists(extraConfigFile.toPath())) {
            ClientResources.CONFIG.copyResource(extraConfigFile);
            log.info(String.format("Generate empty configuration file to: %s", extraConfigFile));
        }

        // 2 - tmp directory

        createDirectory(ClientConfigOption.TEMPORARY_DIRECTORY);

        // 3 - backup / import / avdth directory

        boolean saveConfiguration = initBackupDirectory();
        saveConfiguration |= initImportDirectory();
        saveConfiguration |= initAvdthDirectory();
        if (saveConfiguration) {
            saveForUser();
        }

        // 4 - resources directory

        File resourcesDirectory = createDirectory(ClientConfigOption.RESOURCES_DIRECTORY);
        log.debug("user resource data directory : {}", resourcesDirectory);

        // 5 - resources report

        File reportDirectory = createDirectory(ClientConfigOption.REPORT_DIRECTORY);
        boolean versionSnapshot = this.isVersionSnapshot();
        File file = reportDirectory.toPath().resolve("default").toFile();
        if (versionSnapshot || !file.exists()) {
            log.info(t("observe.runner.copy.default.report.file", file));

            if (file.exists()) {
                log.info("Delete report directory: {}", file);
                MoreFiles.deleteDirectoryContents(file.toPath(), RecursiveDeleteOption.ALLOW_INSECURE);
            }
            List<String> locations = ClientResources.REPORT_LIST.readLines();
            for (String location : locations) {
                String reportLocation = ReportDefinitionsBuilder.reportLocation(location);
                URL resource = getClass().getClassLoader().getResource(reportLocation);
                Path reportPath = reportDirectory.toPath().resolve(location);
                createDirectory(reportPath.getParent().toFile());
                log.info("Copy report from {} to {}", reportLocation, reportPath);
                Files.writeString(reportPath, ObserveUtil.loadResourceContent(resource));
            }
        }

        // 6 - validation report directory

        createDirectory(ClientConfigOption.VALIDATION_REPORT_DIRECTORY);

        // 7 - resources shapeFiles

        File mapDirectory = createDirectory(ClientConfigOption.MAP_DIRECTORY);
        unzipToDirectory(ClientResources.mapLayers, ClientConfigOption.RESOURCES_DIRECTORY, t("observe.runner.copy.default.map.file", mapDirectory));
        for (TripMapContentBuilder tripMapContentBuilder : getTripMapContentBuilders()) {
            String styleFileName = tripMapContentBuilder.getStyleFileName();
            Path path = mapDirectory.toPath().resolve(styleFileName);
            if (versionSnapshot || Files.notExists(path)) {
                ConfigResource resource = new ConfigResource("/map/" + styleFileName);
                String message = t("observe.runner.copy.default.map.file", path);
                log.info(message);
                resource.copyResource(path.toFile());
            }
        }

        // 8 - resources Dcp-presets

        File dcpPresetsDirectory = getDcpPresetsDirectory();
        if (!dcpPresetsDirectory.exists()) {
            reloadEmbeddedDcpPresets();
        }

        // 9 - DCP

        File dcpFile = ClientResources.dcp.getFile(resourcesDirectory);
        if (!dcpFile.exists()) {
            String message = t("observe.runner.copy.default.dcp.file", dcpFile);
            log.info(message);
            ClientResources.dcp.copyResource(dcpFile);
        }

        // 10 - export directory
        createDirectory(ClientConfigOption.EXPORT_DIRECTORY);

        // 11 - Add custom keystore (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2791)

        Path keystore = resourcesDirectory.toPath().resolve("observe.jks");
        if (Files.notExists(keystore)) {
            String message = t("observe.runner.copy.embedded.keystore.file", keystore);
            log.info(message);
            ClientResources.KEYSTORE.copyResource(keystore.toFile());
        }

        if (!isUseJvmKeyStore()) {
            String message = t("observe.runner.use.embedded.keystore.file", keystore);
            log.info(message);
            System.setProperty("javax.net.ssl.trustStore", keystore.toString());
            System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
        } else {
            System.clearProperty("javax.net.ssl.trustStore");
            System.clearProperty("javax.net.ssl.trustStorePassword");
        }

    }

    public void detectLocalDataBase() {
        boolean hasLocalStorage = new File(getLocalDBDirectory(), DB_NAME).exists();
        setLocalStorageExist(hasLocalStorage);
        if (!hasLocalStorage) {
            log.info(t("observe.init.no.local.db.detected", getLocalDBDirectory()));
        }
        boolean hasInitialDb = getInitialDbDump().exists();
        setInitialDumpExist(hasInitialDb);
        if (!hasInitialDb) {
            log.info(t("observe.init.no.initial.dump.detected", getInitialDbDump()));
        }
    }

    @Override
    public Supplier<Gson> getGsonSupplier() {
        return gsonSupplier == null ? gsonSupplier = new DtoGsonSupplier(true) : gsonSupplier;
    }

    private boolean initBackupDirectory() {
        String defaultDirectory = get().replaceRecursiveOptions(ClientConfigOption.BACKUP_DIRECTORY.getDefaultValue());
        String currentDirectory = getBackupDirectory().getAbsolutePath();
        if (defaultDirectory.equals(currentDirectory)) {
            createDirectory(ClientConfigOption.BACKUP_DIRECTORY);
        } else {
            if (Files.notExists(getBackupDirectory().toPath())) {
                log.warn("Setting back backup directory to {} (since previous location does not exist any longer ({}).", defaultDirectory, currentDirectory);
                setBackupDirectory(new File(defaultDirectory));
                createDirectory(ClientConfigOption.BACKUP_DIRECTORY);
                return true;
            }
        }
        return false;
    }

    private boolean initImportDirectory() {
        String defaultDirectory = get().replaceRecursiveOptions(ClientConfigOption.IMPORT_DIRECTORY.getDefaultValue());
        String currentDirectory = getImportDirectory().getAbsolutePath();
        if (!defaultDirectory.equals(currentDirectory) && Files.notExists(getImportDirectory().toPath())) {
            log.warn("Setting back import directory to {} (since previous location does not exist any longer ({}).", defaultDirectory, currentDirectory);
            setImportDirectory(new File(defaultDirectory));
            return true;
        }
        return false;
    }

    private boolean initAvdthDirectory() {
        String defaultDirectory = get().replaceRecursiveOptions(ClientConfigOption.AVDTH_DIRECTORY.getDefaultValue());
        String currentDirectory = getAvdthDirectory().getAbsolutePath();
        if (!defaultDirectory.equals(currentDirectory) && Files.notExists(getImportDirectory().toPath())) {
            log.warn("Setting back avdth directory to {} (since previous location does not exist any longer ({}).", defaultDirectory, currentDirectory);
            setAvdthDirectory(new File(defaultDirectory));
            return true;
        }
        return false;
    }

    private void removeSnapshotFromVersion() {
        Version version = Objects.requireNonNull(getBuildVersion());
        if (version.isSnapshot()) {
            version = Version.removeSnapshot(version);
        }
        setVersion(version);
        get().setDefaultOption(VERSION, version.getVersion());
        get().setDefaultOption(APPLICATION_VERSION, version.getVersion());
    }

    public void initConfig(String... args) throws ArgumentsParserException {
        ApplicationConfig applicationConfig = get();
        applicationConfig.parse(args);

        File userConfigFile = applicationConfig.getUserConfigFile();
        if (userConfigFile.exists() && (Files.notExists(getServerDataSourceConfigurationsFile().toPath()) || Files.notExists(getRemoteDataSourceConfigurationsFile().toPath()))) {
            Properties old = new Properties();
            try (BufferedReader reader = Files.newBufferedReader(userConfigFile.toPath())) {
                old.load(reader);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            {
                String content = old.getProperty("serverDataSourceConfigurations");
                if (content != null && Files.notExists(getServerDataSourceConfigurationsFile().toPath())) {
                    log.info("Migrates serverDataSourceConfigurations to: {}", getServerDataSourceConfigurationsFile().toPath());
                    ServerDataSourceConfiguration[] data = getGsonSupplier().get().fromJson(content, ServerDataSourceConfiguration[].class);
                    setServerDataSourceConfigurations(Arrays.asList(data));
                }
            }
            {
                String content = old.getProperty("remoteDataSourceConfigurations");
                if (content != null && Files.notExists(getRemoteDataSourceConfigurationsFile().toPath())) {
                    log.info("Migrates remoteDataSourceConfigurations to: {}", getRemoteDataSourceConfigurationsFile().toPath());
                    RemoteDataSourceConfiguration[] data = getGsonSupplier().get().fromJson(content, RemoteDataSourceConfiguration[].class);
                    setRemoteDataSourceConfigurations(Arrays.asList(data));
                }
            }
        }
        removeSnapshotFromVersion();
        serviceInitializerConfig = null;
    }

    public FloatingObjectPresetsStorage newFloatingObjectPresetsManager() {
        return new FloatingObjectPresetsStorage(getDcpPresetsDirectory().toPath());
    }

    @Override
    public Set<TripMapContentBuilder> getTripMapContentBuilders() {
        if (tripMapContentBuilders == null) {
            tripMapContentBuilders = new LinkedHashSet<>();
            for (TripMapContentBuilder dtoModel : ServiceLoader.load(TripMapContentBuilder.class)) {
                tripMapContentBuilders.add(dtoModel);
            }
        }
        return tripMapContentBuilders;
    }

    @Override
    public File getTemplatesDirectory() {
        return new File(getI18nDirectory(), I18nTemplateDefinition.PATH);
    }

    @Override
    public DataSourceConnectMode getDefaultDataSourceConnectMode() {
        return DataSourceConnectMode.valueOf(get().getOption(ClientConfigOption.DEFAULT_DATA_SOURCE_CONNECT_MODE.getKey()));
    }

    @Override
    public DataSourceCreateMode getDefaultDataSourceCreateMode() {
        return DataSourceCreateMode.valueOf(get().getOption(ClientConfigOption.DEFAULT_DATA_SOURCE_CREATE_MODE.getKey()));
    }

    @Override
    public CoordinateFormat getCoordinateFormat() {
        return CoordinateFormat.valueOf(get().getOption(ClientConfigOption.COORDINATE_FORMAT.getKey()));
    }

    @Override
    public TemperatureFormat getTemperatureFormat() {
        return get().getOption(TemperatureFormat.class, ClientConfigOption.TEMPERATURE_FORMAT.getKey());
    }

    public ReferentialLocale getReferentialLocale() {
        if (referentialLocale == null) {
            referentialLocale = ReferentialLocale.valueOf(getDbLocale());
        }
        return referentialLocale;
    }

    @Override
    public void setLocale(Locale locale) {
        super.setLocale(locale);
        serviceInitializerConfig = null;
    }

    @Override
    public void setDbLocale(Locale dbLocale) {
        super.setDbLocale(dbLocale);
        referentialLocale = null;
        serviceInitializerConfig = null;
    }

    @Override
    public void setHttpTimeout(int httpTimeout) {
        super.setHttpTimeout(httpTimeout);
        serviceInitializerConfig = null;
    }

    public String getCopyrightText() {
        if (copyrightText == null) {
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int year = calendar.get(Calendar.YEAR);
            copyrightText = "Version " + getVersion() + " IRD @ 2008-" + year;
        }
        return copyrightText;
    }

    public Version getVersion() {
        return version;
    }

    private void setVersion(Version version) {
        this.version = version;
    }

    public File getLocalDBDirectory() {
        return getDbDirectory();
    }

    public boolean isDisplayMainUI() {
        return displayMainUI;
    }

    public void setDisplayMainUI(boolean b) {
        displayMainUI = b;
    }

    public boolean isCanUseUI() {
        return canUseUI;
    }

    public void setCanUseUI(boolean canUseUI) {
        this.canUseUI = canUseUI;
        if (!canUseUI) {
            // won't start main ui
            setDisplayMainUI(false);
        }
    }

    public boolean isLocalStorageExist() {
        return localStorageExist;
    }

    public void setLocalStorageExist(boolean newValue) {
        localStorageExist = newValue;
        // always force propagation
//        firePropertyChange(PROPERTY_LOCAL_STORAGE_EXIST, newValue);
    }

    public boolean isInitialDumpExist() {
        return initialDumpExist;
    }

    public void setInitialDumpExist(boolean newValue) {
        initialDumpExist = newValue;
        // always force propagation
//        firePropertyChange(PROPERTY_INITIAL_DUMP_EXIST, newValue);
    }

    @Override
    public List<File> getMapLayerFiles() {
        List<File> layers = new LinkedList<>();

        for (ClientConfigOption layerOption : MAP_LAYERS) {
            File layerFile = get().getOptionAsFile(layerOption.getKey());
            if (layerFile != null && layerFile.exists()) {
                layers.add(layerFile);
            }
        }
        return layers;
    }

    @Override
    public Map<String, String> getVariables(TripMapContentBuilder builder) {
        Map<String, String> replaceMapping = new LinkedHashMap<>();
        for (String variableName : builder.getVariableNames()) {
            Object variable = get(variableName);
            if (variable == null) {
                continue;
            }
            if (variable instanceof Color) {
                variable = getColorToHex((Color) variable);
            } else {
                variable = String.valueOf(variable);
            }
            replaceMapping.put(variableName, variable.toString());
        }
        return replaceMapping;
    }

    public File newBackupDataFile() {
        return new File(getBackupDirectory(), String.format(BACKUP_DB_PATTERN, getBuildVersion().toString().replaceAll("\\.", "_"), new Date()));
    }

    public File newAutomaticBackupDataFile() {
        return new File(getBackupsDirectory(), String.format(BACKUP_DB_PATTERN, getBuildVersion().toString().replaceAll("\\.", "_"), new Date()));
    }

    public File getFeedBackDirectoryFile() {
        if (feedBackDirectoryFile == null) {
            Date now = new Date();
            String day = String.format("%1$tF", now);
            feedBackDirectoryFile = getFeedBackDirectory().toPath().resolve(day).toFile();
            log.info("Create Feedback directory: {}", feedBackDirectoryFile);
        }
        return feedBackDirectoryFile;
    }

    public File newFeedBackFile() {
        File directory = getFeedBackDirectoryFile();
        Date now = new Date();
        String day = String.format("%1$tF", now);
        String time = String.format("%1$tk-%1$tM-%1$tS", now);
        return new File(directory, String.format(FEED_BACK_PATTERN, getBuildVersion().toString().replaceAll("\\.", "_"), day, time));
    }

    @Override
    public Map<Path, Integer> getTemporaryDirectoriesAndTimeout() {
        return Map.of(getTemporaryDirectory().toPath(), getTemporaryFilesTimeout(), getLogDirectory().toPath(), getLogFilesTimeout(), getFeedBackDirectoryFile().toPath(), getFeedbackFilesTimeout(), getBackupsDirectory().toPath(), getBackupFilesTimeout());
    }

    public void updateBackupDirectory(File backupFile) {
        setBackupDirectory(backupFile.getParentFile());
        saveForUser();
    }

    public void saveForUser() {
        File file = get().getExtraConfigFile();
        log.info(t("observe.ui.message.save.configuration", file));
        ConfigHelper.save(get(), file, doNotSave, ClientResources.CONFIG, options());
    }

    public Project getNavigationEditModel() {
        return loadJson(getNavigationEditModelFile().toPath(), Project.class);
    }

    public void setNavigationEditModel(Project navigationEditModel) {
        storeJson(getNavigationEditModelFile().toPath(), navigationEditModel);
    }

    public int getNavigationEditModelCount() {
        try {
            Project model = getNavigationEditModel();
            return model == null ? 0 : model.count();
        } catch (Exception e) {
            log.error("Can't load navigation edit model", e);
            return 0;
        }
    }

    public List<RemoteDataSourceConfiguration> getRemoteDataSourceConfigurations() {
        return loadJsonList(getRemoteDataSourceConfigurationsFile().toPath(), RemoteDataSourceConfiguration.class);
    }

    public void setRemoteDataSourceConfigurations(List<RemoteDataSourceConfiguration> configurations) {
        storeJsonList(getRemoteDataSourceConfigurationsFile().toPath(), configurations);
        remoteDataSourceConfigurationList = null;
    }

    public void addRemoteDataSourceConfiguration(RemoteDataSourceConfiguration configuration) {
        List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations = getRemoteDataSourceConfigurations();
        remoteDataSourceConfigurations.add(configuration);
        remoteDataSourceConfigurations.sort(Comparator.comparing(RemoteDataSourceConfiguration::getName));
        setRemoteDataSourceConfigurations(remoteDataSourceConfigurations);
    }

    public void updateRemoteDataSourceConfiguration(RemoteDataSourceConfiguration configuration) {
        List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations = getRemoteDataSourceConfigurations();
        remoteDataSourceConfigurations.remove(configuration);
        remoteDataSourceConfigurations.add(configuration);
        remoteDataSourceConfigurations.sort(Comparator.comparing(RemoteDataSourceConfiguration::getName));
        setRemoteDataSourceConfigurations(remoteDataSourceConfigurations);
    }

    public void removeRemoteDataSourceConfiguration(RemoteDataSourceConfiguration configuration) {
        List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations = getRemoteDataSourceConfigurations();
        remoteDataSourceConfigurations.remove(configuration);
        setRemoteDataSourceConfigurations(remoteDataSourceConfigurations);
    }

    public List<RemoteDataSourceConfiguration> getRemoteDataSourceConfigurationList() {
        if (remoteDataSourceConfigurationList == null) {
            List<RemoteDataSourceConfiguration> configurations = getRemoteDataSourceConfigurations();
            configurations.sort(Comparator.comparing(RemoteDataSourceConfiguration::getName));
            remoteDataSourceConfigurationList = configurations;
        }
        return remoteDataSourceConfigurationList;
    }

    public List<ServerDataSourceConfiguration> getServerDataSourceConfigurationList() {
        if (serverDataSourceConfigurationList == null) {
            List<ServerDataSourceConfiguration> configurations = getServerDataSourceConfigurations();
            configurations.sort(Comparator.comparing(ServerDataSourceConfiguration::getName));
            serverDataSourceConfigurationList = configurations;
        }
        return serverDataSourceConfigurationList;
    }

    public List<ServerDataSourceConfiguration> getServerDataSourceConfigurations() {
        return loadJsonList(getServerDataSourceConfigurationsFile().toPath(), ServerDataSourceConfiguration.class);
    }

    public void setServerDataSourceConfigurations(List<ServerDataSourceConfiguration> configurations) {
        storeJsonList(getServerDataSourceConfigurationsFile().toPath(), configurations);
        serverDataSourceConfigurationList = null;
    }

    public void addServerDataSourceConfiguration(ServerDataSourceConfiguration configuration) {
        List<ServerDataSourceConfiguration> remoteDataSourceConfigurations = getServerDataSourceConfigurations();
        remoteDataSourceConfigurations.add(configuration);
        remoteDataSourceConfigurations.sort(Comparator.comparing(ServerDataSourceConfiguration::getName));
        setServerDataSourceConfigurations(remoteDataSourceConfigurations);
    }

    public void updateServerDataSourceConfiguration(ServerDataSourceConfiguration configuration) {
        List<ServerDataSourceConfiguration> remoteDataSourceConfigurations = getServerDataSourceConfigurations();
        remoteDataSourceConfigurations.remove(configuration);
        remoteDataSourceConfigurations.add(configuration);
        remoteDataSourceConfigurations.sort(Comparator.comparing(ServerDataSourceConfiguration::getName));
        setServerDataSourceConfigurations(remoteDataSourceConfigurations);
    }

    public void removeServerDataSourceConfiguration(ServerDataSourceConfiguration configuration) {
        List<ServerDataSourceConfiguration> serverDataSourceConfigurations = getServerDataSourceConfigurations();
        serverDataSourceConfigurations.remove(configuration);
        setServerDataSourceConfigurations(serverDataSourceConfigurations);
    }

    @Override
    public Date getBuildDate() {
        String dateStr = get().getOption(ClientConfigOption.BUILD_DATE.getKey());
        try {
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isVersionSnapshot() {
        return getBuildVersion().isSnapshot();
    }

    public Properties read(Properties resource) {
        SortedProperties result = new SortedProperties();
        for (String propertyName : resource.stringPropertyNames()) {
            result.put(propertyName, get().replaceRecursiveOptions(resource.getProperty(propertyName)));
        }
        return result;
    }

    public void loadUIConfig() {
        File dir = getResourcesDirectory();
        loadUIConfigFile(ClientResources.ui, dir);
        loadUIConfigFile(ClientResources.ui_navigation_common, dir);
        loadUIConfigFile(ClientResources.ui_navigation_ps, dir);
        loadUIConfigFile(ClientResources.ui_navigation_ll, dir);
    }

    public void loadUIManager() {
        UIDefaults defaults = UIManager.getDefaults();
        defaults.put("BlockingLayerUI.busyColor", getBusyStateColor());
        defaults.put("BlockingLayerUI.blockColor", getBlockStateColor());
        for (ClientConfigOption option : ClientConfigOption.values()) {
            if (option.getType() == Color.class) {
                String key = option.getKey();
                Color color = get().getOptionAsColor(key);
                log.info(String.format("Register color: %s → %s", key, color));
                defaults.put(key, color);
            }
        }
    }

    public void loadUIConfigFile(ConfigResource clientResources, File resourceDirectory) {
        File file = clientResources.getFile(resourceDirectory);
        if (!file.exists() || isVersionSnapshot()) {

            String message = t("observe.runner.copy.default.ui.file", file);
            log.info(message);
            clientResources.copyResource(file);
        }

        log.info(t("observe.runner.loading.ui.configuration", file));
        try {
            Properties p = read(ConfigResource.load(file));
            SwingUtil.loadUIConfig(p);
            SwingUtil.loadUIConfig(p, entry -> {
                String name = entry.getKey().toString();
                if (name.startsWith("icon.action.content-")) {
                    String iconKey = name.substring(ICON_PREFIX.length());
                    addScaledIcon(iconKey, 16);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("could not load ui configuration: " + file, e);
        }
    }

    public void addScaledIcon(String name, int size) {
        ImageIcon icon = (ImageIcon) Objects.requireNonNull(UIManager.getIcon(name), String.format("Can't find icon: %s", name));
        BufferedImage resizedImg = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = resizedImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(icon.getImage(), 0, 0, size, size, null);
        g2.dispose();
        icon = new ImageIcon(resizedImg);
        String key = name + "-" + size;
//        try {
//            File file = new File(String.format("%s/%s.png", "ird-observe/client/core/src/main/resources/icons/navigation/default/actions", key.replace("actions.content-", "")));
//            ImageIO.write(resizedImg, "png", file);
//        } catch (IOException e) {
//            throw new IllegalStateException(e);
//        }
        UIManager.put(key, icon);
    }

    @Override
    public JavaBeanDefinition javaBeanDefinition() {
        return javaBeanDefinition == null ? javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(getClass()).orElseThrow(() -> new NullPointerException("Can't find JavaBeanDefinition for " + getClass())) : javaBeanDefinition;
    }

    @Override
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
    }

    @Override
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
    }

    public SimplifiedObjectTypeSpecializedRules getSimplifiedObjectTypeSpecializedRules() {
        if (simplifiedObjectTypeSpecializedRules == null) {
            this.simplifiedObjectTypeSpecializedRules = SimplifiedObjectTypeSpecializedRules.of(getSimplifiedObjectTypeSpecializedRulesFile());
        }
        return simplifiedObjectTypeSpecializedRules;
    }

    public ObserveServiceInitializerConfig getServiceInitializerConfig() {
        if (serviceInitializerConfig == null) {
            serviceInitializerConfig = new ObserveServiceInitializerConfig(getLocale(), getReferentialLocale(), getTemporaryDirectory(), getHttpTimeout(), getModelVersion(), getBuildVersion());
        }
        return serviceInitializerConfig;
    }

    protected <T> T loadJson(Path path, Class<T> type) {
        if (Files.notExists(path)) {
            return null;
        }
        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            return getGsonSupplier().get().fromJson(bufferedReader, type);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    protected <T> List<T> loadJsonList(Path path, Class<T> type) {
        if (Files.notExists(path)) {
            return new ArrayList<>();
        }
        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            return getGsonSupplier().get().fromJson(bufferedReader, ImmutableListAdapter.listOf(type));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    protected <T> void storeJson(Path path, T data) {
        try {
            if (Files.notExists(path.getParent())) {
                Files.createDirectories(path.getParent());
            }
            String content = getGsonSupplier().get().toJson(data);
            Files.writeString(path, content);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    protected <T> void storeJsonList(Path path, List<T> data) {
        try {
            if (Files.notExists(path.getParent())) {
                Files.createDirectories(path.getParent());
            }
            String content = getGsonSupplier().get().toJson(data);
            Files.writeString(path, content);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public ValidationRequestConfiguration toValidationRequestConfiguration() {
        ValidationRequestConfiguration configuration = new ValidationRequestConfiguration();
        configuration.setValidationSpeedMaxValue(getValidationSpeedMaxValue());
        configuration.setValidationSpeedEnable(getValidationSpeedEnable());
        configuration.setValidationLengthWeightEnable(getValidationLengthWeightEnable());
        configuration.setValidationUseDisabledReferential(getValidationUseDisabledReferential());
        configuration.setSeineBycatchObservedSystemConfig(getSeineBycatchObservedSystem());
        return configuration;
    }

    //////////////////////////////////////////////////
    // Toutes les étapes d'actions
    //////////////////////////////////////////////////

    public enum Step {
        AfterInit, BeforeExit
    }
}
