package fr.ird.observe.client.configuration;

/*-
 * #%L
 * ObServe Client :: Configuration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.definition.ReportDefinitionsBuilder;
import io.ultreia.java4all.config.ConfigResource;

/**
 * Created on 07/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public final class ClientResources {
    public static final ConfigResource CONFIG = new ConfigResource("/META-INF/configuration/observe-client.conf");
    public static final ConfigResource ui = new ConfigResource("/observe-ui.properties");
    public static final ConfigResource ui_navigation_common = new ConfigResource("/observe-ui-navigation-common.properties");
    public static final ConfigResource ui_navigation_ps = new ConfigResource("/observe-ui-navigation-ps.properties");
    public static final ConfigResource ui_navigation_ll = new ConfigResource("/observe-ui-navigation-ll.properties");
    public static final ConfigResource REPORT_LIST = new ConfigResource(ReportDefinitionsBuilder.REPORT_LIST_LOCATION);
    public static final ConfigResource dcp = new ConfigResource("/observe-specialized-fad-rules.properties");
    public static final ConfigResource LOG_CONFIGURATION_FILE = new ConfigResource("/observe-log4j2.xml");
    public static final ConfigResource DCP_PRESETS = new ConfigResource("/dcp-presets.zip");
    public static final ConfigResource mapLayers = new ConfigResource("/map.zip");
    public static final ConfigResource KEYSTORE = new ConfigResource("/observe.jks");
}
