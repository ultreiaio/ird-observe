package fr.ird.observe.client.configuration;

/*-
 * #%L
 * ObServe Client :: Configuration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ArrayListMultimap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.ird.observe.dto.validation.SeineBycatchObservedSystemConfig;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import org.apache.commons.beanutils.ConversionException;
import org.nuiton.converter.NuitonConverter;

/**
 * Created by tchemit on 03/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("rawtypes")
@AutoService(NuitonConverter.class)
public class SeineBycatchObservedSystemConfigConverter implements NuitonConverter<SeineBycatchObservedSystemConfig> {

    // serialize
    private final Gson gson = new DtoGsonSupplier(false).get();

    @Override
    public Class<SeineBycatchObservedSystemConfig> getType() {
        return SeineBycatchObservedSystemConfig.class;
    }

    @Override
    public <T> T convert(Class<T> aClass, Object value) {
        if (value == null) {
            throw new ConversionException(
                    String.format("No value specified for converter %s", this));
        }
        Object result;
        if (String.class.equals(aClass)) {
            if (isEnabled(value.getClass())) {
                result = gson.toJson(((SeineBycatchObservedSystemConfig) value).getData());
                return aClass.cast(result);
            }
        }
        if (isEnabled(aClass)) {

            if (isEnabled(value.getClass())) {
                return aClass.cast(value);
            }
            if (value instanceof String) {
                result = new SeineBycatchObservedSystemConfig();
                ((SeineBycatchObservedSystemConfig) result).setData(gson.fromJson((String) value, new TypeToken<ArrayListMultimap<String, String>>() {

                }.getType()));
                return aClass.cast(result);
            }
        }
        throw new ConversionException(
                String.format("No converter found for type %2$s and objet '%1$s'", aClass.getName(), value));
    }

    protected boolean isEnabled(Class<?> aClass) {
        return getType().equals(aClass);
    }

}
