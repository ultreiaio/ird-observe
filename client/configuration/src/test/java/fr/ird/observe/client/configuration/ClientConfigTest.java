package fr.ird.observe.client.configuration;

/*-
 * #%L
 * ObServe Client :: Configuration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;
import fr.ird.observe.dto.validation.SeineBycatchObservedSystemConfig;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.test.ToolkitFixtures;
import io.ultreia.java4all.config.ArgumentsParserException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 03/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ClientConfigTest {

    public static final int EXPECTED_VESSEL_TYPE_LONGLINE_LANDING = 2;
    public static final int EXPECTED_VESSEL_TYPE_LONGLINE_TRIP = 5;
    private static File testDirectory;
    private ClientConfig config;

    @BeforeClass
    public static void beforeClass() {
        testDirectory = ToolkitFixtures.getTestBasedir(ClientConfigTest.class).toPath().getParent().resolve(ClientConfigTest.class.getName()).toFile();
    }

    @Before
    public void setUp() throws ArgumentsParserException {
        config = ClientConfig.forTest();
        config.setDataDirectory(testDirectory);
        config.initConfig();
    }

    @Test
    public void testSeineBycatchObservedSystemConfig() {
        SeineBycatchObservedSystemConfig seineBycatchObservedSystem = Objects.requireNonNull(config.getSeineBycatchObservedSystem());
        {
            Collection<String> requiredObservedSystemBySpeciesId = seineBycatchObservedSystem.getRequiredObservedSystemBySpeciesId("fr.ird.referential.common.Species#1239832684290#0.04680507324710936");
            Assert.assertNotNull(requiredObservedSystemBySpeciesId);
            Assert.assertEquals(3, requiredObservedSystemBySpeciesId.size());
            Iterator<String> iterator = requiredObservedSystemBySpeciesId.iterator();
            Assert.assertEquals("fr.ird.referential.ps.common.ObservedSystem#0#1.0", iterator.next());
            Assert.assertEquals("fr.ird.referential.ps.common.ObservedSystem#0#1.1", iterator.next());
            Assert.assertEquals("fr.ird.referential.ps.common.ObservedSystem#1239832686428#0.9217864901728908", iterator.next());
        }
        {
            Collection<String> requiredObservedSystemBySpeciesId = seineBycatchObservedSystem.getRequiredObservedSystemBySpeciesId("fr.ird.referential.common.Species#1239832684290#0.04680507324710936_fake");
            Assert.assertNotNull(requiredObservedSystemBySpeciesId);
            Assert.assertEquals(0, requiredObservedSystemBySpeciesId.size());
        }
    }

    @Test
    public void testNavigationEditModel() {
        Project navigationEditModel = config.getNavigationEditModel();
        Assert.assertNull(navigationEditModel);
    }

    @Test
    public void testGetVesselTypeLonglineLandingId() {
        String[] value = config.getVesselTypeLonglineLandingId();
        Assert.assertNotNull(value);
        Assert.assertEquals(EXPECTED_VESSEL_TYPE_LONGLINE_LANDING, value.length);

        List<String> list = config.getVesselTypeLonglineLandingIdList();
        Assert.assertNotNull(list);
        Assert.assertEquals(EXPECTED_VESSEL_TYPE_LONGLINE_LANDING, list.size());
    }

    @Test
    public void testDcpPresets() throws IOException {
        FloatingObjectPresetsStorage presetsManager = config.newFloatingObjectPresetsManager();

        File dcpPresetsDirectory = config.createDirectory(ClientConfigOption.DCP_PRESETS_DIRECTORY);
        config.unzipToDirectory(ClientResources.DCP_PRESETS, ClientConfigOption.RESOURCES_DIRECTORY, t("observe.runner.copy.default.dcp.file", dcpPresetsDirectory));

        Assert.assertNotNull(presetsManager);
        Assert.assertEquals(EXPECTED_VESSEL_TYPE_LONGLINE_TRIP, presetsManager.size());
        presetsManager.setPresetsMap(Collections.emptyMap());
        Assert.assertEquals(0, presetsManager.size());
        presetsManager.reset();
        Assert.assertEquals(EXPECTED_VESSEL_TYPE_LONGLINE_TRIP, presetsManager.size());
    }
}
