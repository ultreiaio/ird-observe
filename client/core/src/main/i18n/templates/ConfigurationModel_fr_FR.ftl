<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.config.form.RemoteConfigurationModel" -->
<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<h2>Résultat de la connexion</h2>
<#if .data_model.connexionUntested>
    <p>
        <strong>La connexion n'a jamais été validée ou a été modifiée depuis le dernier test de connexion.</strong>
    </p>
<#elseif .data_model.connexionFailed>
    <p>
        <strong>La connexion a échouée.</strong>
    </p>
    <ul>
        <li>${.data_model.connexionStatusError}</li>
    </ul>
<#elseif .data_model.connexionSuccess>
    <p>
        <strong>La connexion est établie.</strong>
    </p>
    <hr/>
    <h3>Informations sur la connexion</h3>
    <#import "ObserveDataSourceConfiguration_fr_FR.ftl" as configurationMacros>
    <@configurationMacros.dataSourceConfiguration .data_model.configuration />
    <@configurationMacros.dataSourceInformationRights .data_model.dataSourceInformation />
</#if>
</body>
</html>
