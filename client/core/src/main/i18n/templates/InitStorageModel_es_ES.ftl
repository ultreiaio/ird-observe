<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.api.InitStorageModel" -->
<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>

<h3>Importar una base de datos</h3>
<#if .data_model.error??>
    <p>
        The local database could not be loaded.
    </p>
    <hr/>
    <code style="font-size: 10px">
        ${.data_model.error.message}
    </code>
<#else>
    <p>
        La base local no existe (ruta ${.data_model.localDb.absolutePath}).
    </p>
</#if>
<hr/>
<p>
    Ahora puede:
</p>

<ul>
    <#if .data_model.withBackup>
        <li>Utilizar la última copia de seguridad automática (${.data_model.backupDate})</li>
    </#if>
    <li>Crear una base local nueva</li>
    <li>Conectar a una base remota</li>
</ul>

</body>
</html>
