<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.api.InitStorageModel" -->
<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>

<h3>Chargement d'une source de donnée</h3>

<#if .data_model.error??>
    <p>
        La base locale n'a pas pu être chargée.
    </p>
    <hr/>
    <code style="font-size: 10px">
        ${.data_model.error.message}
    </code>
<#else>
    <p>
        La base locale n'existe pas (emplacement ${.data_model.localDb.absolutePath}).
    </p>
</#if>
<hr/>
<p>
    Vous pouvez à ce stade
</p>

<ul>
    <#if .data_model.withBackup>
        <li>utiliser la dernière sauvegarde automatique (${.data_model.backupDate})</li>
    </#if>
    <li>créer la base locale</li>
    <li>vous connecter à une base distante.</li>
</ul>

</body>
</html>
