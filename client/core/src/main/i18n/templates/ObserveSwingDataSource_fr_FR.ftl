<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.api.ObserveSwingDataSource" -->
<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<#if .data_model?? && .data_model.label??>
    <#import "ObserveDataSourceConfiguration_fr_FR.ftl" as configurationMacros>
    <h3>${.data_model.label}</h3>
    <@configurationMacros.dataSourceConfiguration .data_model.configuration />
    <#if .data_model.dataSourceInformation??>
        <@configurationMacros.dataSourceInformationRights .data_model.dataSourceInformation />
        <h4>Dernières opérations réalisées</h4>
        <ul>
            <li><strong>Mise à jour simple de référentiel : </strong>
                ${.data_model.simpleReferentialSynchronisationLastUpdate!"Pas encore réalisé"}</li>
            <li><strong>Mise à jour avancée de référentiel : </strong>
                ${.data_model.advancedReferentialSynchronisationLastUpdate!"Pas encore réalisé"}</li>
        </ul>
    </#if>
<#else>
    Aucune source de données chargée
</#if>
</body>
</html>
