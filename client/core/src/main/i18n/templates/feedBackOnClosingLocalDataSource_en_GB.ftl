<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>

<h3>${errorTitle}</h3>
<p><b>${errorMessage!""}</b></p>

<hr/>
<p>At this point, we can produce a feed back archive with the local data source and any stuff to be able to fix the
    problem.</p>

<br/>
<p>The archive will be generated at: ${feedBackFile}</p>

<br/>
<hr/>
You can now :

<ul>
    <li><b>Cancel feed back generate</b> <i>(this will stop the data source opening process)</i></li>
    <li><b>Generate feed back</b> <i>(this will remove local data source and continue the data source opening
            process)</i>
    </li>
</ul>
</body>
</html>
