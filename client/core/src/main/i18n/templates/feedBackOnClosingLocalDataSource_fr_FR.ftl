<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<h3>${errorTitle}</h3>
<p><b>${errorMessage!""}</b></p>

<hr/>
<p>À ce stade, vous pouvez générer un retour d'expérience sous forme d'archive qui contiendra les données nécessaires
    pour que nous puissions examiner le problème).</p>

<br/>
<p>L'archive sera générée ici: ${feedBackFile}</p>

<br/>

Vous pouvez :

<ul>
    <li><b>Annuler la génération du retour d'expérience</b>
        <i>(cela annulera la suite de l'opération d'ouverture de source de données)</i>
    </li>
    <li><b>Generer le retour d'expérience</b> <i>(cela supprimera la base locale et poursuivra l'opération d'ouverture
            de
            source de données)</i>
    </li>
</ul>
</body>
</html>
