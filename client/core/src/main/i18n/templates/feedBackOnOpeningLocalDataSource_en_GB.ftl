<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.api.action.OpenLocalDataSourceFeedBackModel" -->
<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<h3>${errorTitle}</h3>
<p><b>${errorMessage!""}</b></p>
<#if databaseLocked>
    <p style="color: red;font-style: italic;font-weight: bold">
        Local database seems to be locked by another application, please check this before continue...
    </p>
</#if>

<hr/>
<p>At this point, we can produce a feed back archive with the local data source and any stuff to be able to fix the
    problem.</p>

<br/>

<p>The archive will be generated at: ${feedBackFile}</p>

<br/>
<hr/>
<p>You can now :</p>
<dl>
    <dt>Cancel feed back generate</dt>
    <dd>this will not removed local data source</dd>
    <dt>Generate feed back</dt>
    <dd>
        <p><b>this will remove local data source</b> and you will be able to repeat the data source opening process</p>
    </dd>
    <dt>Continue</dt>
    <dd>
        <p><b>this will remove local data source</b> and you will be able to repeat the data source opening process</p>
    </dd>
</dl>
</body>
</html>
