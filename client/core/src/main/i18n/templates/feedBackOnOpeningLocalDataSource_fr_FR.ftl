<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.api.action.OpenLocalDataSourceFeedBackModel" -->
<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<h3>${errorTitle}</h3>
<p><b>${errorMessage!""}</b></p>

<#if databaseLocked>
    <p style="color: red;font-style: italic;font-weight: bold">
        La base locale semble utilisée par une autre application, vérifier cela avant de continuer...
    </p>

</#if>
<hr/>

<br/>

<p>À ce stade, vous pouvez générer un retour d'expérience sous forme d'archive qui contiendra les données nécessaires
    pour que nous puissions examiner le problème.</p>

<br/>
<p>L'archive sera générée ici: ${feedBackFile}</p>

<br/>
<p>Le retour d'expérience est accessible via le menu <b>Fichier &gt; Retour d'expérience</b></p>
<p>
    Vous pouvez :
</p>
<dl>
    <#if .data_model.destroyLocalDatasource>
        <dt>Annuler</dt>
        <dd>Pour annuler l'opération de création de la base locale, celle-ci ne sera pas supprimée et restera en
            l'état
        </dd>
    </#if>
    <dt>Générer un retour d'expérience</dt>
    <dd>
        <#if .data_model.destroyLocalDatasource>
            <p>Pour générer un retour d'expérience, <b>la base locale sera alors supprimée</b>.</p>
            <p>Vous pourrez ensuite créer une nouvelle base locale.</p>
        <#else>
            <p>Pour générer un retour d'expérience.</p>
        </#if>
    </dd>
    <dt>Continuer</dt>
    <dd>
        <#if .data_model.destroyLocalDatasource>
            <p>Continuer, <b>la base locale sera alors supprimée</b>.</p>
            <p>Vous pourrez ensuite créer une nouvelle base locale.</p>
        <#else>
            <p>Continuer, et fermer la dase.</p>
        </#if>
    </dd>
</dl>
</body>
</html>
