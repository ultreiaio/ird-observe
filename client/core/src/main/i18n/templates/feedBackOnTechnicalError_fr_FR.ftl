<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<h3>${errorTitle}</h3>
<p><b>${errorMessage!""}</b></p>
<hr/>
<br/>
<p>À ce stade, vous pouvez générer un retour d'expérience sous forme d'archive qui contiendra les données nécessaires
    pour que nous puissions examiner le problème.</p>

<br/>
<p>L'archive sera générée ici: ${feedBackFile}</p>

<br/>
<p>Le retour d'expérience est accessible via le menu <b>Fichier &gt; Retour d'expérience</b></p>
<p>
    Vous pouvez :
</p>
<dl>
    <dt>Générer un retour d'expérience</dt>
    <dd>
        <p>Pour générer un retour d'expérience, et continuer.</p>
    </dd>
    <dt>Continuer</dt>
    <dd>
        <p>Continuer, sans rien faire.</p>
    </dd>
</dl>
</body>
</html>
