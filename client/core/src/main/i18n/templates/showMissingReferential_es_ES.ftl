<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.api.data.DataManager" -->
<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<p>
    There is some referential used in data source <i>${.data_model.dataSources.left().label}</i> not present in target data source
    <i>${.data_model.dataSources.right().label}</i> and required for data import.
</p>
<p>
    To continue data import, you have to <strong>Confirm insertion</strong> of those referential in target data source.
</p>
</body>
</html>
