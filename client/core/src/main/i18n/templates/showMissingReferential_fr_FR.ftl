<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.api.data.DataManager" -->
<#--
 #%L
 ObServe Client :: Core
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<p>
    Il existe des référentiels dans la source de données <i>${.data_model.dataSources.left().label}</i> non présents dans la source de
    données cible <i>${.data_model.dataSources.right().label}</i> et nécessaires pour l'import de données.
</p>
<p>
    Pour continuer l'import des données, vous devez <strong>Confirmer l'insertion</strong> de ces référentiels dans la
    source de données cible.
</p>
</body>
</html>
