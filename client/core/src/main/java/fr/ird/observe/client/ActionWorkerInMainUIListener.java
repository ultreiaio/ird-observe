package fr.ird.observe.client;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.application.ActionWorker;
import org.nuiton.jaxx.runtime.swing.application.event.ActionExecutorEvent;
import org.nuiton.jaxx.runtime.swing.application.event.ActionExecutorListener;

public class ActionWorkerInMainUIListener implements ActionExecutorListener, WithClientUIContextApi {

    private final ErrorHandler errorHandler;

    public ActionWorkerInMainUIListener(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    @Override
    public void actionStart(ActionExecutorEvent event) {
        ActionWorker<?, ?> source = event.getWorker();
        getBusyModel().addTask("Start task: " + source.getActionLabel());
    }

    @Override
    public void actionFail(ActionExecutorEvent event) {
        ActionWorker<?, ?> source = event.getWorker();
        Exception error = source.getError();
        setUiStatus("Action [" + source.getActionLabel() + "] arrêté à cause d'un erreur " + error.getMessage());
        errorHandler.createFeedback(error);
    }

    @Override
    public void actionCancel(ActionExecutorEvent event) {
        ActionWorker<?, ?> source = event.getWorker();
        setUiStatus("Action [" + source.getActionLabel() + "] annulée");
    }

    @Override
    public void actionEnd(ActionExecutorEvent event) {
        ActionWorker<?, ?> source = event.getWorker();
        setUiStatus("Action [" + source.getActionLabel() + "] terminée.");
    }

    @Override
    public void actionDone(ActionExecutorEvent event) {
        getBusyModel().popTask();
    }
}
