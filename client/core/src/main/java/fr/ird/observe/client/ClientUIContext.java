package fr.ird.observe.client;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.h2.backup.BackupsManager;
import fr.ird.observe.client.datasource.h2.backup.LocalDatabaseBackupTimer;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.client.main.MainUIModel;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.body.NoBodyContentComponent;
import fr.ird.observe.client.main.callback.ObserveUICallbackManager;
import fr.ird.observe.client.main.focus.MainUIFocusModel;
import fr.ird.observe.client.util.action.ObserveExecutorService;
import fr.ird.observe.client.util.session.ObserveSwingSessionHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;
import fr.ird.observe.navigation.id.IdProjectManager;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServiceMainFactory;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.application.context.spi.GenerateApplicationComponent;
import io.ultreia.java4all.config.clean.CleanTemporaryFilesTask;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.runtime.swing.application.ActionExecutor;

import javax.swing.SwingUtilities;
import java.awt.Component;
import java.io.Closeable;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;

@GenerateApplicationComponent(name = "ObServe Client UI Context", dependencies = ClientConfig.class)
public class ClientUIContext implements Closeable, ClientUIContextApi {

    private static final Logger log = LogManager.getLogger(ClientUIContext.class);

    private final ClientConfig clientConfig;
    private final ObserveSwingSessionHelper observeSwingSessionHelper;
    private final ObserveActionExecutor actionExecutor;
    private final FloatingObjectPresetsStorage floatingObjectPresetsManager;
    private final Project editModel;
    private final Project selectModel;
    private final IdProjectManager idProjectModelManager;
    private final ObserveServiceMainFactory serviceFactory;
    private final ObserveDataSourcesManager dataSourcesManager;
    private final BackupsManager backupsManager;
    private final LocalDatabaseBackupTimer localDatabaseBackupTimer;
    private final BusyModel busyModel;
    private final ClientValidationContext clientValidationContext;
    private final NuitonValidatorProvider validationProvider;
    private ObserveMainUI mainUI;

    private ObserveUICallbackManager uiCallbackManager;
    private Timer deleteTemporaryFilesTimer;

    public ClientUIContext(ClientConfig clientConfig) {
        ClientConfig.initDecoratorService(clientConfig.getReferentialLocale());
        this.clientConfig = clientConfig;
        this.observeSwingSessionHelper = new ObserveSwingSessionHelper(clientConfig.getSwingSessionFile());
        this.floatingObjectPresetsManager = clientConfig.newFloatingObjectPresetsManager();
        this.serviceFactory = new ObserveServiceMainFactory();
        this.editModel = new Project();
        this.selectModel = new Project();
        this.idProjectModelManager = new IdProjectManager(editModel);
        this.dataSourcesManager = new ObserveDataSourcesManager(clientConfig, serviceFactory, getDecoratorService(), selectModel, editModel);
        this.backupsManager = new BackupsManager(clientConfig, dataSourcesManager);
        this.localDatabaseBackupTimer = new LocalDatabaseBackupTimer(clientConfig, backupsManager, dataSourcesManager);
        this.actionExecutor = new ObserveActionExecutor(new ObserveExecutorService());
        this.busyModel = new BusyModel();
        this.clientValidationContext = new ClientValidationContext(this);
        this.validationProvider = NuitonValidatorProviders.newProvider(NuitonValidatorProviders.getDefaultFactoryName(), clientValidationContext);

    }

    @Override
    public void initDeleteTemporaryFilesTimer() {
        if (deleteTemporaryFilesTimer != null) {
            closeDeleteTemporaryFilesTimer();
        }
        deleteTemporaryFilesTimer = CleanTemporaryFilesTask.create(getClientConfig());
    }

    @Override
    public ClientValidationContext getClientValidationContext() {
        return clientValidationContext;
    }

    @Override
    public NuitonValidatorProvider getValidationProvider() {
        return validationProvider;
    }

    @Override
    public ObserveServiceMainFactory getServiceFactory() {
        return serviceFactory;
    }

    @Override
    public DecoratorService getDecoratorService() {
        return ClientConfig.getDecoratorService();
    }

    @Override
    public ClientConfig getClientConfig() {
        return clientConfig;
    }

    @Override
    public ObserveSwingSessionHelper getObserveSwingSessionHelper() {
        return observeSwingSessionHelper;
    }

    @Override
    public FloatingObjectPresetsStorage getFloatingObjectPresetsManager() {
        return floatingObjectPresetsManager;
    }

    @Override
    public Project getObserveEditModel() {
        return editModel;
    }

    @Override
    public IdProjectManager getObserveIdModelManager() {
        return idProjectModelManager;
    }

    @Override
    public Project getObserveSelectModel() {
        return selectModel;
    }

    @Override
    public ObserveDataSourcesManager getDataSourcesManager() {
        return dataSourcesManager;
    }

    @Override
    public LocalDatabaseBackupTimer getLocalDatabaseBackupTimer() {
        return localDatabaseBackupTimer;
    }

    @Override
    public BackupsManager getBackupsManager() {
        return backupsManager;
    }

    public ObserveUICallbackManager getUiCallbackManager() {
        return uiCallbackManager;
    }

    @Override
    public ActionExecutor getActionExecutor() {
        return actionExecutor;
    }

    @Override
    public void setUiStatus(String status) {
        ObserveMainUI mainUI = getMainUI();
        if (mainUI != null) {
            mainUI.getStatus().setStatus(status);
        } else {
            log.info(status);
        }
    }

    @Override
    public ObserveMainUI getMainUI() {
        return mainUI;
    }

    @Override
    public void setMainUI(ObserveMainUI mainUI) {
        this.mainUI = Objects.requireNonNull(mainUI);
        uiCallbackManager = new ObserveUICallbackManager(mainUI);
    }

    @Override
    public void removeMainUI() {
        if (mainUI != null) {
            mainUI.getModel().getFocusModel().uninstall();
        }
        this.mainUI = null;
        if (uiCallbackManager != null) {
            uiCallbackManager.close();
        }
    }

    @Override
    public void blockFocus() {
        if (mainUI != null) {
            getFocusModel().block();
        }
    }

    @Override
    public void unblockFocus() {
        if (mainUI != null) {
            getFocusModel().unblock();
        }
    }

    @Override
    public MainUIFocusModel getFocusModel() {
        return mainUI == null ? null : mainUI.getModel().getFocusModel();
    }

    @Override
    public void setFocusZone(String zoneName) {
        Objects.requireNonNull(getFocusModel(), "No focus model").setFocusOwnerZone(zoneName);
    }

    @Override
    public void setFocusZoneOwner(Component focusOwner) {
        Objects.requireNonNull(getFocusModel(), "No focus model").setZoneOwnerFocusOwner(focusOwner);
    }

    @Override
    public void dirtyFocusZone(String zoneName) {
        Objects.requireNonNull(getFocusModel(), "No focus model").dirtyFocusOwnerZone(zoneName);
    }

    /**
     * Methode pour initialiser l'interface graphique principale sans l'afficher.
     *
     * @param context le context applicatif
     * @param config  la configuration a utiliser
     * @return l'ui instancie et initialisée mais non visible encore
     */
    @Override
    public ObserveMainUI initUI(ApplicationContext context, ClientConfig config) {

        DecoratorService decoratorService = getDecoratorService();

        Locale currentLocale = I18n.getDefaultLocale();
        Locale configurationLocale = config.getLocale();
        if (!configurationLocale.equals(currentLocale)) {
            log.info("re-init I18n with locale " + configurationLocale);
            I18n.setDefaultLocale(configurationLocale);
        }
        if (!config.getDbLocale().equals(decoratorService.getReferentialLocale().getLocale())) {
            log.info("re-init db with locale " + config.getDbLocale());
            decoratorService.setReferentialLocale(config.getReferentialLocale());
        }

        config.loadUIManager();
        JAXXInitialContext tx = new JAXXInitialContext();
        tx.add(context);

        return new ObserveMainUI(tx);
    }

    @Override
    public void setMainUIVisible(ObserveMainUI ui, boolean replace) {

        if (ui.getMainUIBodyContentManager().getCurrentBody() == null) {
            ui.changeBodyContent(NoBodyContentComponent.class);
        }
        // affichage de l'interface graphique
        SwingUtilities.invokeLater(() -> ui.setVisible(true));
        getObserveSwingSessionHelper().addComponent(ui, replace);
    }

    @Override
    public MainUIModel getMainUIModel() {
        return mainUI == null ? null : mainUI.getModel();
    }

    @Override
    public BusyModel getBusyModel() {
        return busyModel;
    }

    @Override
    public void addSimpleAction(String message, Runnable action) {
        getBusyModel().addTask(message);
        try {
            action.run();
        } finally {
            getBusyModel().popTask();
            setUiStatus(message);
        }
    }

    @Override
    public void close() {
        closeDeleteTemporaryFilesTimer();
        List<Closeable> closeableList = new LinkedList<>();
        closeableList.add(localDatabaseBackupTimer);
        closeableList.add(clientValidationContext);
        closeableList.add(backupsManager);
        closeableList.add(dataSourcesManager);
        closeableList.add(serviceFactory);
        closeableList.add(observeSwingSessionHelper);
        closeableList.add(actionExecutor);
        for (Closeable closeable : closeableList) {
            try {
                closeable.close();
            } catch (IOException e) {
                log.error("Could not close " + closeable, e);
            }
        }
        mainUI = null;
    }

    @Override
    public void closeDeleteTemporaryFilesTimer() {
        if (deleteTemporaryFilesTimer != null) {
            try {
                deleteTemporaryFilesTimer.cancel();
            } catch (Exception e) {
                log.error("Could not terminates delete temporary files timer...", e);
            }
        }
    }
}
