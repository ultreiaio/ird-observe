package fr.ird.observe.client;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.h2.backup.BackupsManager;
import fr.ird.observe.client.datasource.h2.backup.LocalDatabaseBackupTimer;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.client.main.MainUIModel;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.callback.ObserveUICallbackManager;
import fr.ird.observe.client.main.focus.MainUIFocusModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.session.ObserveSwingSessionHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;
import fr.ird.observe.navigation.id.IdProjectManager;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServiceMainFactory;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.application.ActionExecutor;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.util.Objects;

import static javax.swing.JOptionPane.CLOSED_OPTION;
import static javax.swing.JOptionPane.VALUE_PROPERTY;

/**
 * Created on 11/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface ClientUIContextApi {

    default void displayInfo(String text) {
        JFrame ui = getMainUI();
        if (ui == null) {
            JOptionPane.showMessageDialog(null, text);
        } else {
            setUiStatus(text);
        }
    }

    default void displayWarning(String title, String text) {
        JFrame ui = getMainUI();
        JOptionPane.showMessageDialog(ui, text, title, JOptionPane.WARNING_MESSAGE);
    }

    default int askToUser(String title,
                          Object message,
                          int typeMessage,
                          Object[] options,
                          int defaultOption) {
        JFrame ui = getMainUI();
        return askToUser(
                Objects.requireNonNull(ui),
                title,
                message,
                typeMessage,
                options,
                defaultOption
        );
    }

    default int askToUser(JOptionPane pane,
                          String title,
                          Object[] options) {

        JFrame mainUI = getMainUI();
        JDialog dialog = new JDialog(mainUI, true);
        dialog.setTitle(title);

        Container contentPane = dialog.getContentPane();

        contentPane.setLayout(new BorderLayout());
        contentPane.add(pane, BorderLayout.CENTER);
        if (!Objects.equals(true, pane.getClientProperty(UIHelper.NO_PACK))) {
            dialog.pack();
        } else {
            dialog.setSize(contentPane.getPreferredSize());
            if (Objects.equals(true, pane.getClientProperty(UIHelper.FIX_SIZE))) {
//                dialog.setResizable(false);
                dialog.setMinimumSize(pane.getPreferredSize());
                dialog.setPreferredSize(pane.getPreferredSize());
            }
        }
        SwingUtil.center(mainUI, dialog);

        final PropertyChangeListener listener = event -> {
            // Let the defaultCloseOperation handle the closing
            // if the user closed the window without selecting a button
            // (newValue = null in that case).  Otherwise, close the dialog.
            if (dialog.isVisible() && event.getSource() == pane &&
                    (event.getPropertyName().equals(VALUE_PROPERTY)) &&
                    event.getNewValue() != null &&
                    event.getNewValue() != JOptionPane.UNINITIALIZED_VALUE) {
                dialog.setVisible(false);
            }
        };

        WindowAdapter adapter = new WindowAdapter() {
            private boolean gotFocus = false;

            @Override
            public void windowClosing(WindowEvent we) {
                pane.setValue(null);
            }

            @Override
            public void windowClosed(WindowEvent e) {
                pane.removePropertyChangeListener(listener);
                dialog.getContentPane().removeAll();
            }

            @Override
            public void windowGainedFocus(WindowEvent we) {
                // Once window gets focus, set initial focus
                if (!gotFocus) {
                    pane.selectInitialValue();
                    gotFocus = true;
                }
            }
        };
        dialog.addWindowListener(adapter);
        dialog.addWindowFocusListener(adapter);
        dialog.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent ce) {
                // reset value to ensure closing works properly
                pane.setValue(JOptionPane.UNINITIALIZED_VALUE);
            }

            @Override
            public void componentHidden(ComponentEvent e) {
                SwingUtilities.invokeLater(ObserveUtil::cleanMemory);
            }
        });

        pane.addPropertyChangeListener(listener);

        dialog.setVisible(true);
        Object selectedValue = pane.getValue();

        if (selectedValue == null)
            return CLOSED_OPTION;
        for (int counter = 0, maxCounter = options.length;
             counter < maxCounter; counter++) {
            if (options[counter].equals(selectedValue))
                return counter;
        }
        return CLOSED_OPTION;
    }

    default int askToUser(Component parent,
                          String title,
                          Object message,
                          int typeMessage,
                          Object[] options,
                          int defaultOption) {
        if (parent == null) {
            parent = getMainUI();
        }
        return JOptionPane.showOptionDialog(
                parent,
                message,
                title,
                JOptionPane.DEFAULT_OPTION,
                typeMessage,
                null,
                options,
                options[defaultOption]
        );
    }

    void initDeleteTemporaryFilesTimer();

    default ObserveBusinessProject getBusinessProject() {
        return ObserveBusinessProject.get();
    }

    ObserveServiceMainFactory getServiceFactory();

    ClientValidationContext getClientValidationContext();

    NuitonValidatorProvider getValidationProvider();

    DecoratorService getDecoratorService();

    ClientConfig getClientConfig();

    ObserveSwingSessionHelper getObserveSwingSessionHelper();

    FloatingObjectPresetsStorage getFloatingObjectPresetsManager();

    Project getObserveEditModel();

    Project getObserveSelectModel();

    IdProjectManager getObserveIdModelManager();

    ObserveDataSourcesManager getDataSourcesManager();

    LocalDatabaseBackupTimer getLocalDatabaseBackupTimer();

    BackupsManager getBackupsManager();

    ObserveUICallbackManager getUiCallbackManager();

    ActionExecutor getActionExecutor();

    void setUiStatus(String status);

    ObserveMainUI getMainUI();

    void setMainUI(ObserveMainUI mainUI);

    void removeMainUI();

    void blockFocus();

    void unblockFocus();

    MainUIFocusModel getFocusModel();

    void setFocusZone(String zoneName);

    void setFocusZoneOwner(Component focusOwner);

    void dirtyFocusZone(String zoneName);

    ObserveMainUI initUI(ApplicationContext context, ClientConfig config);

    void setMainUIVisible(ObserveMainUI ui, boolean replace);

    MainUIModel getMainUIModel();

    BusyModel getBusyModel();

    void addSimpleAction(String message, Runnable action);

    void closeDeleteTemporaryFilesTimer();
}
