package fr.ird.observe.client;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.action.FeedBackBuilder;
import fr.ird.observe.client.datasource.api.action.TechnicalErrorFeedBackBuilderModel;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.server.security.InvalidAuthenticationTokenException;
import io.ultreia.java4all.http.HResponseError;
import io.ultreia.java4all.http.HResponseErrorException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Container;
import java.lang.reflect.InvocationTargetException;

/**
 * To manage error.
 * <p>
 * Created on 26/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ErrorHandler implements Thread.UncaughtExceptionHandler, WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(ErrorHandler.class);

    protected abstract void onInvalidAuthenticationTokenException();

    private Container container;

    public Container getContainer() {
        return container;
    }

    public void setContainer(Container container) {
        this.container = container;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        StackTraceElement[] stackTrace = e.getStackTrace();
        if (stackTrace.length > 0 && stackTrace[0].getClassName().startsWith("javax.swing.plaf")) {
            //FIXME
            log.error("Swing error", e);
            return;
        }
        if (e instanceof InvalidAuthenticationTokenException || containsExceptionInStack(e, InvalidAuthenticationTokenException.class)) {
            onInvalidAuthenticationTokenException();
            return;
        }
        if (e instanceof Exception) {
            if (e instanceof InvocationTargetException) {
                createFeedback((Exception) e.getCause());
                return;
            }
            Throwable cause = e.getCause();
            if (cause instanceof InvocationTargetException) {
                createFeedback((Exception) cause.getCause());
                return;
            }
            createFeedback((Exception) e);
            return;
        }
        UIHelper.handlingError(e);
    }

    public void createFeedback(Exception e) {
        TechnicalErrorFeedBackBuilderModel model = new TechnicalErrorFeedBackBuilderModel(getClientConfig(), e);
        Container ui = container;
        if (ui == null) {
            ObserveMainUI mainUI = getMainUI();
            if (mainUI == null) {
                log.error("Error in feed-back with no ui...", e);
                return;
            }
            ui = mainUI;
        }
        FeedBackBuilder feedBackBuilder = new FeedBackBuilder(model, ui);
        try {
            feedBackBuilder.run();
        } catch (Exception exception) {
            log.error("Error in feed-back...", e);
        }
    }

    public static <E extends Throwable> boolean containsExceptionInStack(Throwable e, Class<E> type) {
        if (e == null) {
            return false;
        }
        if (type.isInstance(e)) {
            return true;
        }
        if (e instanceof HResponseErrorException) {
            HResponseError error = ((HResponseErrorException) e).getError();
            Class<?> exceptionType = error.getExceptionType();
            return type.equals(exceptionType);
        }
        while (e.getCause() != null) {
            e = e.getCause();
            if (type.isInstance(e)) {
                return true;
            }
        }
        return false;
    }


    protected <E extends Throwable> boolean containsExceptionInStack2(Throwable e, Class<E> type) {
        if (e == null) {
            return false;
        }
        if (type.isInstance(e)) {
            return true;
        }
        if (e instanceof HResponseErrorException) {
            return containsExceptionInStack(((HResponseErrorException) e).getError().getException(), type);
        }
        while (e.getCause() != null) {
            e = e.getCause();
            if (type.isInstance(e)) {
                return true;
            }
        }
        return false;
    }
}
