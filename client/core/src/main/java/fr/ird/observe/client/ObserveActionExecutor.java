package fr.ird.observe.client;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.action.ObserveExecutorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.application.ActionExecutor;
import org.nuiton.jaxx.runtime.swing.application.ActionWorker;
import org.nuiton.jaxx.runtime.swing.application.event.LogListener;

import java.io.Closeable;
import java.util.Objects;

public class ObserveActionExecutor extends ActionExecutor implements Closeable {

    private static final Logger log = LogManager.getLogger(ObserveActionExecutor.class);

    private final ObserveExecutorService executorService;

    public ObserveActionExecutor(ObserveExecutorService executorService) {
        this.executorService = Objects.requireNonNull(executorService);
        addActionExecutorListener(new LogListener(log));
        addActionExecutorListener(new ApplicationContextClosedListener());
    }

    @Override
    protected void executeWorker(String actionLabel, ActionWorker<?, ?> worker) {
        log.info(String.format("Launch ObServe worker [%s] register...", actionLabel));
        executorService.execute(worker);
        log.info(String.format("Launch ObServe worker [%s] running....", actionLabel));
    }

    @Override
    public void close() {
        terminatesAndWaits();
    }

}
