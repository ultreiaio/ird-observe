/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client;

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.configuration.ClientConfigFinder;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.ObserveServiceMainFactory;
import io.ultreia.java4all.application.context.ApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;
import java.util.Optional;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Le contexte de l'application.
 * <p>
 * On définit ici toutes les entrées du contexte.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ObserveSwingApplicationContext extends ApplicationContext implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(ObserveSwingApplicationContext.class);
    private static ObserveDataSourceConfiguration dataSourceConfigurationToReload;

    // We put here the i18n keys from the client-runner module, otherwise we need yet another module before observe-i18n
    // to get all i18n stuff...
    static {
        n("observe.runner.action.showConfig.title");
        n("observe.runner.initStorage.choice.createLocalStorage");
        n("observe.runner.initStorage.choice.doNothing");
        n("observe.runner.initStorage.choice.loadLastAutomaticBackup");
        n("observe.runner.initStorage.choice.useRemoteStorage");
        n("observe.runner.initStorage.local.db.error");
        n("observe.runner.initStorage.done");
        n("observe.runner.message.help.usage");
        n("observe.runner.config.loaded");
        n("observe.runner.context.loaded");
        n("observe.runner.i18n.loaded");
        n("observe.runner.init");
        n("observe.runner.load.database");
        n("observe.runner.loaded");
        n("observe.runner.quit.without.ui");
        n("observe.runner.start");
        n("observe.runner.ui.loaded");
        n("observe.runner.user.directories.loaded");
        n("observe.runner.initStorage.label.local");
        n("observe.runner.initStorage.title.create.local.db");
        n("observe.runner.title.error.dialog");
        n("observe.runner.initStorage.title.load.remote.db");
        n("observe.runner.initStorage.title.no.local.db.found");

    }

    public static ObserveSwingApplicationContext init(ClientConfig config) {
        ClientConfigFinder.set(config);
        return new ObserveSwingApplicationContext();
    }

    public static ObserveSwingApplicationContext init(ClientConfig config, Class<?> excludeComponent) {
        ClientConfigFinder.set(config);
        return new ObserveSwingApplicationContext(excludeComponent);
    }

    public static void setDataSourceToReload(ObserveDataSourceConfiguration dataSourceToReload) {
        ObserveSwingApplicationContext.dataSourceConfigurationToReload = dataSourceToReload;
    }

    public static Optional<ObserveDataSourceConfiguration> getDataSourceToReload() {
        return Optional.ofNullable(dataSourceConfigurationToReload);
    }

    public ObserveSwingApplicationContext(Class<?> excludeComponent) {
        super(Objects.requireNonNull(excludeComponent));
        //FIXME Je ne comprends pas pourquoi cela ne fonctionne plus.
//        config.get().setOption("user.home", SystemUtils.USER_HOME);

        ObserveServiceMainFactory serviceMainFactory = getServiceFactory();
        log.info(String.format("Initialize services factory: %s", serviceMainFactory));
        log.info("Initialize delete temporary files task.");
        initDeleteTemporaryFilesTimer();
    }

    public ObserveSwingApplicationContext() {
        super();
        //FIXME Je ne comprends pas pourquoi cela ne fonctionne plus.
//        config.get().setOption("user.home", SystemUtils.USER_HOME);

        ObserveServiceMainFactory serviceMainFactory = getServiceFactory();
        log.info(String.format("Initialize services factory: %s", serviceMainFactory));
        log.info("Initialize delete temporary files task.");
        initDeleteTemporaryFilesTimer();
    }
}
