package fr.ird.observe.client;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.h2.backup.BackupsManager;
import fr.ird.observe.client.datasource.h2.backup.LocalDatabaseBackupTimer;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.client.main.MainUIModel;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.callback.ObserveUICallbackManager;
import fr.ird.observe.client.main.focus.MainUIFocusModel;
import fr.ird.observe.client.util.session.ObserveSwingSessionHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;
import fr.ird.observe.navigation.id.IdProjectManager;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServiceMainFactory;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import org.nuiton.jaxx.runtime.swing.application.ActionExecutor;

import java.awt.Component;

/**
 * Created on 11/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface WithClientUIContextApi extends ClientUIContextApi {

    @Override
    default void initDeleteTemporaryFilesTimer() {
        ClientUIContextApplicationComponent.value().initDeleteTemporaryFilesTimer();
    }

    @Override
    default ObserveServiceMainFactory getServiceFactory() {
        return ClientUIContextApplicationComponent.value().getServiceFactory();
    }

    @Override
    default ClientValidationContext getClientValidationContext() {
        return ClientUIContextApplicationComponent.value().getClientValidationContext();
    }

    @Override
    default NuitonValidatorProvider getValidationProvider() {
        return ClientUIContextApplicationComponent.value().getValidationProvider();
    }

    @Override
    default DecoratorService getDecoratorService() {
        return ClientUIContextApplicationComponent.value().getDecoratorService();
    }

    @Override
    default ClientConfig getClientConfig() {
        return ClientUIContextApplicationComponent.value().getClientConfig();
    }

    @Override
    default ObserveSwingSessionHelper getObserveSwingSessionHelper() {
        return ClientUIContextApplicationComponent.value().getObserveSwingSessionHelper();
    }

    @Override
    default FloatingObjectPresetsStorage getFloatingObjectPresetsManager() {
        return ClientUIContextApplicationComponent.value().getFloatingObjectPresetsManager();
    }

    @Override
    default Project getObserveEditModel() {
        return ClientUIContextApplicationComponent.value().getObserveEditModel();
    }

    @Override
    default Project getObserveSelectModel() {
        return ClientUIContextApplicationComponent.value().getObserveSelectModel();
    }

    @Override
    default IdProjectManager getObserveIdModelManager() {
        return ClientUIContextApplicationComponent.value().getObserveIdModelManager();
    }

    @Override
    default ObserveDataSourcesManager getDataSourcesManager() {
        return ClientUIContextApplicationComponent.value().getDataSourcesManager();
    }

    @Override
    default LocalDatabaseBackupTimer getLocalDatabaseBackupTimer() {
        return ClientUIContextApplicationComponent.value().getLocalDatabaseBackupTimer();
    }

    @Override
    default BackupsManager getBackupsManager() {
        return ClientUIContextApplicationComponent.value().getBackupsManager();
    }

    @Override
    default ObserveUICallbackManager getUiCallbackManager() {
        return ClientUIContextApplicationComponent.value().getUiCallbackManager();
    }

    @Override
    default ActionExecutor getActionExecutor() {
        return ClientUIContextApplicationComponent.value().getActionExecutor();
    }

    @Override
    default void setUiStatus(String status) {
        ClientUIContextApplicationComponent.value().setUiStatus(status);
    }

    @Override
    default ObserveMainUI getMainUI() {
        return ClientUIContextApplicationComponent.value().getMainUI();
    }

    @Override
    default void setMainUI(ObserveMainUI mainUI) {
        ClientUIContextApplicationComponent.value().setMainUI(mainUI);
    }

    @Override
    default void removeMainUI() {
        ClientUIContextApplicationComponent.value().removeMainUI();
    }

    @Override
    default void blockFocus() {
        ClientUIContextApplicationComponent.value().blockFocus();
    }

    @Override
    default void unblockFocus() {
        ClientUIContextApplicationComponent.value().unblockFocus();
    }

    @Override
    default MainUIFocusModel getFocusModel() {
        return ClientUIContextApplicationComponent.value().getFocusModel();
    }

    @Override
    default void setFocusZone(String zoneName) {
        ClientUIContextApplicationComponent.value().setFocusZone(zoneName);
    }

    @Override
    default void setFocusZoneOwner(Component focusOwner) {
        ClientUIContextApplicationComponent.value().setFocusZoneOwner(focusOwner);
    }

    @Override
    default void dirtyFocusZone(String zoneName) {
        ClientUIContextApplicationComponent.value().dirtyFocusZone(zoneName);
    }

    @Override
    default ObserveMainUI initUI(ApplicationContext context, ClientConfig config) {
        return ClientUIContextApplicationComponent.value().initUI(context, config);
    }

    @Override
    default void setMainUIVisible(ObserveMainUI ui, boolean replace) {
        ClientUIContextApplicationComponent.value().setMainUIVisible(ui, replace);
    }

    @Override
    default MainUIModel getMainUIModel() {
        return ClientUIContextApplicationComponent.value().getMainUIModel();
    }

    @Override
    default BusyModel getBusyModel() {
        return ClientUIContextApplicationComponent.value().getBusyModel();
    }

    @Override
    default void addSimpleAction(String message, Runnable action) {
        ClientUIContextApplicationComponent.value().addSimpleAction(message, action);
    }

    @Override
    default void closeDeleteTemporaryFilesTimer() {
        ClientUIContextApplicationComponent.value().closeDeleteTemporaryFilesTimer();
    }

}
