package fr.ird.observe.client.datasource.api;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;

/**
 * Created on 09/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class DataSourceReferenceProvider {

    private final ObserveSwingDataSource dataSource;

    public DataSourceReferenceProvider(ObserveSwingDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @SuppressWarnings("unchecked")
    public final <R extends DtoReference> R getReference(Class<R> referenceType, String id, String classifier) {
        if (DataDtoReference.class.isAssignableFrom(referenceType)) {
            @SuppressWarnings({"unchecked", "rawtypes"}) R reference = (R) getDataReference((Class) referenceType, id, classifier);
            return reference;
        }
        if (ReferentialDtoReference.class.isAssignableFrom(referenceType)) {
            @SuppressWarnings({"unchecked", "rawtypes"}) R reference = (R) getReferentialReference((Class) referenceType, id);
            return reference;
        }
        throw new IllegalStateException("Can't manage type: " + referenceType);
    }

    private <R extends ReferentialDtoReference> R getReferentialReference(Class<R> referenceType, String id) {
        if (id == null) {
            return dataSource.getReferenceService().createReferential(referenceType);
        }
        return dataSource.getReferenceService().loadReferential(referenceType, id);
    }

    private <R extends DataDtoReference> R getDataReference(Class<R> referenceType, String id, String classifier) {
        if (id == null) {
            return dataSource.getReferenceService().createData(referenceType);
        }
        return dataSource.getReferenceService().loadData(referenceType, id, classifier);
    }
}
