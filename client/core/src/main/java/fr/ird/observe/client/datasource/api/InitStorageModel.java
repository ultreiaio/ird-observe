package fr.ird.observe.client.datasource.api;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.h2.backup.BackupStorage;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Optional;

@GenerateTemplate(template = "InitStorageModel.ftl")
public class InitStorageModel {

    private final File localDb;
    private final BackupStorage lastAutomaticBackup;
    private final SimpleDateFormat simpleDateFormat;
    private final Exception error;

    public InitStorageModel(File localDb, BackupStorage lastAutomaticBackup, Exception error) {
        this.localDb = localDb;
        this.lastAutomaticBackup = lastAutomaticBackup;
        this.simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
        this.error = error;
    }

    public Exception getError() {
        return error;
    }

    public File getLocalDb() {
        return localDb;
    }

    public boolean isWithBackup() {
        return lastAutomaticBackup != null;
    }

    public String getBackupDate() {
        return Optional.ofNullable(lastAutomaticBackup).map(l -> simpleDateFormat.format(l.getDate())).orElse("");
    }
}
