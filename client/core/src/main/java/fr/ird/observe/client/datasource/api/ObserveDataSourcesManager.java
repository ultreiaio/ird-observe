package fr.ird.observe.client.datasource.api;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceEvent;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceListenerAdapter;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.server.security.InvalidAuthenticationTokenException;
import fr.ird.observe.services.ObserveServiceMainFactory;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

/**
 * Permet de gérer les différentes data sources utilisées dans l'application.
 * <p>
 * Created on 09/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
@GenerateJavaBeanDefinition
public class ObserveDataSourcesManager extends AbstractJavaBean implements Closeable {

    private static final Logger log = LogManager.getLogger(ObserveDataSourcesManager.class);
    public static final String PROPERTY_MAIN_DATA_SOURCE = "mainDataSource";
    private final List<ObserveSwingDataSource> dataSources = new LinkedList<>();
    private final ClientConfig config;
    private final ObserveServiceMainFactory serviceFactory;
    private final Project selectModel;
    private final Project editModel;
    private final DecoratorService decoratorService;
    private ObserveSwingDataSource dataSource;

    public ObserveDataSourcesManager(ClientConfig config, ObserveServiceMainFactory serviceFactory, DecoratorService decoratorService, Project selectModel, Project editModel) {
        this.config = Objects.requireNonNull(config);
        this.serviceFactory = Objects.requireNonNull(serviceFactory);
        this.decoratorService = decoratorService;
        this.selectModel = Objects.requireNonNull(selectModel);
        this.editModel = Objects.requireNonNull(editModel);
    }

    /**
     * Create a new data source for the locale data base.
     * <p>
     * <b>Note:</b> Data base must exist!
     *
     * @return new local data source not opened
     */
    public ObserveSwingDataSource newLocalDataSource() {
        ObserveDataSourceConfigurationTopiaH2 dataSourceConfigurationH2 = newH2DataSourceConfiguration(I18n.n("observe.runner.initStorage.label.local"));
        if (!dataSourceConfigurationH2.getDatabaseFile().exists()) {
            throw new IllegalStateException(String.format("local base must exist, when using this method (%s)", dataSourceConfigurationH2.getDirectory()));
        }
        return newDataSource(dataSourceConfigurationH2);
    }

    public ObserveDataSourceConfigurationTopiaH2 newH2DataSourceConfiguration(String label) {
        File dbDirectory = new File(config.getLocalDBDirectory(), ClientConfig.DB_NAME);
        return ObserveDataSourceConfigurationTopiaH2.create(
                label,
                dbDirectory,
                ClientConfig.DB_NAME,
                config.getH2Login(),
                config.getH2Password().toCharArray(),
                config.getModelVersion()
        );
    }

    public ObserveSwingDataSource newTemporaryH2DataSource(String label) {
        File tmpDirectory = config.getTemporaryDirectory();
        File dbDirectory = new File(tmpDirectory, ClientConfig.DB_NAME + UUID.randomUUID());
        return new ObserveSwingDataSource(config,
                                          serviceFactory,
                                          decoratorService, ObserveDataSourceConfigurationTopiaH2.create(
                label,
                dbDirectory,
                ClientConfig.DB_NAME,
                config.getH2Login(),
                config.getH2Password().toCharArray(),
                config.getModelVersion()
        ));
    }

    public ObserveDataSourceConfigurationTopiaPG newPGDataSourceConfiguration(String label) {
        return ObserveDataSourceConfigurationTopiaPG.create(
                label,
                null,
                null,
                new char[0],
                false,
                config.getModelVersion()
        );
    }

    public ObserveDataSourceConfigurationRest newRestDataSourceConfiguration(String label) {
        return ObserveDataSourceConfigurationRest.create(
                label,
                null,
                null,
                new char[0],
                null,
                config.getModelVersion()
        );
    }

    public ObserveSwingDataSource getMainDataSource() {
        return dataSource;
    }

    public void setMainDataSource(ObserveSwingDataSource dataSource) {
        this.dataSource = dataSource;
        firePropertyChange(PROPERTY_MAIN_DATA_SOURCE, dataSource);
    }

    public Optional<ObserveSwingDataSource> getOptionalMainDataSource() {
        return Optional.ofNullable(dataSource);
    }

    public ObserveSwingDataSource newSimpleDataSource(ObserveDataSourceConfiguration configuration) {
        return newDataSource(configuration);
    }

    public ObserveSwingDataSource newDataSource(ObserveDataSourceConfiguration configuration) {
        ObserveSwingDataSource dataSource = new ObserveSwingDataSource(config, serviceFactory, decoratorService, configuration);
        addInternalListener(dataSource);
        return dataSource;
    }

    public ObserveSwingDataSource newDataSource(ObserveDataSourceConfigurationTopiaH2 configuration, SqlScript dump) {
        ObserveSwingDataSource dataSource = new ObserveSwingDataSource(config, serviceFactory, decoratorService, configuration) {
            @Override
            public void open() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException {
                createFromDump(dump);
            }
        };
        addInternalListener(dataSource);
        return dataSource;
    }

    void addInternalListener(ObserveSwingDataSource dataSource) {
        dataSource.addObserveSwingDataSourceListener(new ObserveSwingDataSourceListenerAdapter() {

            @Override
            public void onOpened(ObserveSwingDataSourceEvent event) {
                super.onOpened(event);
                ObserveSwingDataSource dataSource = event.getSource();
                dataSources.add(dataSource);
                log.info(String.format("Data source opened : %s (%d data sources open)", dataSource.getConfiguration(), dataSources.size()));
            }

            @Override
            public void onClosed(ObserveSwingDataSourceEvent event) {
                super.onClosed(event);
                ObserveSwingDataSource dataSource = event.getSource();
                dataSources.remove(dataSource);
                if (Objects.equals(dataSource, getMainDataSource())) {
                    log.info(String.format("Remove main data source: %s", dataSource));
                    setMainDataSource(null);
                }
                log.info(String.format("Data source closed : %s (%d data sources open)", dataSource.getConfiguration(), dataSources.size()));
            }
        });
    }

    @Override
    public void close() {

        // close all opened datasource
        for (ObserveSwingDataSource dataSource : new ArrayList<>(dataSources)) {
            log.info("Closing dataSource : " + dataSource.getConnection());
            try {
                dataSource.close();
            } catch (InvalidAuthenticationTokenException e) {
                // If token is expired, no error message
                log.error("Tried to close session, but session was already closed by the server. Continuing...");
            } catch (Exception e) {

                log.error("Could not close data source: " + dataSource, e);
            }
        }
        setMainDataSource(null);
    }

    public void saveNavigationToConfig() {
        log.info("Store navigation model to file.");
        config.setNavigationEditModel(editModel);
    }

    public void saveSelectedPath(String[] paths) {
        log.warn("Will save selected path:\n\t" + String.join("\n\t", paths));
        config.setNavigationLastSelectedPath(paths);
        //FIXME We should save only at close time...
        config.saveForUser();
    }

    public void clear() {
        log.info("Clear navigation models.");
        selectModel.clearModel();
        editModel.clearModel();
        // will save last selected path
        config.saveForUser();
    }
}
