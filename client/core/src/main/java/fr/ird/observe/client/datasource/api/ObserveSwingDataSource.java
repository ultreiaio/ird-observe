package fr.ird.observe.client.datasource.api;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceEvent;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceListener;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfigurationAndConnection;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfigurationAndConnectionDto;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.datasource.request.CreateDatabaseRequest;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.datasource.security.WithPermission;
import fr.ird.observe.datasource.security.model.DataSourceUserDto;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.ObserveTopiaConfigurationFactory;
import fr.ird.observe.navigation.id.IdAggregateModel;
import fr.ird.observe.navigation.id.IdModel;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.tree.selection.SelectionTreeConfig;
import fr.ird.observe.server.security.SecurityExceptionSupport;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.ObserveServiceMainFactory;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.ObserveServicesProviderImpl;
import fr.ird.observe.services.service.AnonymousService;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.services.service.referential.ObserveReferentialCache;
import fr.ird.observe.services.service.referential.ReferentialService;
import fr.ird.observe.services.service.referential.SynchronizeService;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcHelperH2;

import javax.swing.BoundedRangeModel;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.event.EventListenerList;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
@GenerateTemplate(template = "ObserveSwingDataSource.ftl")
public class ObserveSwingDataSource extends AbstractJavaBean implements ObserveServicesProvider, ObserveDataSourceConfigurationAndConnection, WithClientUIContextApi, WithPermission {

    private static final Logger log = LogManager.getLogger(ObserveSwingDataSource.class);

    private final EventListenerList listenerList;
    private final ClientConfig config;
    private final ObserveDataSourceConfigurationAndConnection configurationAndConnection;
    private final ObserveReferentialCache referentialCache;
    private final DataSourceReferenceProvider referenceProvider;
    private final DecoratorService decoratorService;
    private final Icon icon;
    private final ObserveServicesProviderImpl servicesProvider;
    //    private final Map<Class<?>, ObserveService> cache;
    private ProgressionModel progressModel;
    // connexion expired, but data source not closed
    private boolean expired;
    // at least one commit was performed on this data source
    private boolean modified;
    // connected user is owner of data source
    private boolean owner;
    // connected user is super-user of data source
    private boolean superUser;

    public static void doCloseSource(ObserveSwingDataSource source) {
        if (source != null && source.isOpen()) {
            source.close();
        }
    }

    public static ObserveSwingDataSource doOpenSource(ObserveSwingDataSource source) {
        if (source != null && !source.isOpen()) {
            try {
                source.open();
                return source;
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return source;
    }

    public ObserveSwingDataSource(ClientConfig config, ObserveServiceMainFactory serviceFactory, DecoratorService decoratorService, ObserveDataSourceConfiguration configuration) {
        this.config = Objects.requireNonNull(config);
        this.configurationAndConnection = new ObserveDataSourceConfigurationAndConnectionDto(Objects.requireNonNull(configuration), null);
        this.listenerList = new EventListenerList();
        this.decoratorService = decoratorService;
        this.referentialCache = new ObserveReferentialCache(ObserveBusinessProject.get(), this.decoratorService);
        this.referenceProvider = new DataSourceReferenceProvider(this);
        this.expired = false;
        this.icon = configuration.getConnectMode().getIcon();
        //FIXME Not sure a cache here is good, any leak inside a service will stay here...
//        this.cache = new HashMap<>();
        setProgressModel(new ProgressionModel());
        this.servicesProvider = new ObserveServicesProviderImpl(serviceFactory, this::createServiceInitializer);
    }

    @Override
    public ObserveDataSourceConfiguration getConfiguration() {
        return configurationAndConnection.getConfiguration();
    }

    @Override
    public ObserveDataSourceConnection getConnection() {
        return configurationAndConnection.getConnection();
    }

    @Override
    public void setConfiguration(ObserveDataSourceConfiguration configuration) {
        configurationAndConnection.setConfiguration(configuration);
    }

    @Override
    public void setConnection(ObserveDataSourceConnection connection) {
        if (connection != null && isLocal() && config.isH2CanEditReferential()) {
            // let's change permission to be able to edit referential
            connection = connection.toPermission(Permission.ALL);
        }
        configurationAndConnection.setConnection(connection);
    }

    @Override
    public <S extends ObserveService> S getService(Class<S> serviceType) {
//        @SuppressWarnings("unchecked") S result = (S) cache.get(serviceType);
//        if (result == null) {
//            result = servicesProvider.getService(serviceType);
//            cache.put(serviceType, result);
//        }
//        return result;
        return servicesProvider.getService(serviceType);
    }

    protected ObserveServiceInitializer createServiceInitializer() {
        getConfiguration().setTraceSql(config.isShowSql());
        return new ObserveServiceInitializer(config.getServiceInitializerConfig(), getConfiguration(), getConnection());
    }

    public ObserveDataSourceInformation getDataSourceInformation() {
        return configurationAndConnection.withConnection() ? getConnection().getDataSourceInformation() : null;
    }

    public Icon getIcon() {
        return icon;
    }

    public String getLabel() {
        return getConfiguration().getLabel();
    }

    @Override
    public Permission getCredentials() {
        return configurationAndConnection.withConnection() ? getConnection().getCredentials() : null;
    }

    public Version getVersion() {
        Version result = null;
        if (configurationAndConnection.withConnection()) {
            result = configurationAndConnection.getConnection().getDataSourceInformation().getVersion();
        }
        return result;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public void open() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException {
        checkIsNotOpen();
        fireNewMessage(t("observe.ui.datasource.storage.message.opening", getLabel()));
        fireOpening();
        AnonymousService dataSourceService = getAnonymousService();
        setConnection(dataSourceService.open(getConfiguration()));
        fireNewMessage(t("observe.ui.datasource.storage.message.opened", getLabel()));
        fireOpened();
    }

    public void createEmpty()
            throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException,
            DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        checkIsNotOpen();
        fireNewMessage(t("observe.ui.datasource.storage.message.creating", getLabel()));
        fireOpening();
        AnonymousService dataSourceService = getAnonymousService();
        setConnection(dataSourceService.createEmpty(getConfiguration()));
        fireNewMessage(t("observe.ui.datasource.storage.message.created", getLabel()));
        fireOpened();
    }

    public void createFromDump(SqlScript dump)
            throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException,
            DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        checkIsNotOpen();
        fireNewMessage(t("observe.ui.datasource.storage.message.creating", getLabel()));
        fireOpening();
        AnonymousService dataSourceService = getAnonymousService();
        setConnection(dataSourceService.createFromDump(getConfiguration(), dump));
        fireNewMessage(t("observe.ui.datasource.storage.message.created", getLabel()));
        fireOpened();
    }

    public void createFromImport(SqlScript referential, SqlScript data)
            throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException,
            DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        checkIsNotOpen();
        fireNewMessage(t("observe.ui.datasource.storage.message.creating", getLabel()));
        fireOpening();
        AnonymousService dataSourceService = getAnonymousService();
        setConnection(dataSourceService.createFromImport(getConfiguration(), referential, data));
        fireNewMessage(t("observe.ui.datasource.storage.message.created", getLabel()));
        fireOpened();
    }

    public SqlScript extract(boolean pg, boolean loadSchema, Set<String> importDataIds) {
        checkIsOpen();
        DataSourceService dataSourceService = getDataSourceService();
        CreateDatabaseRequest.Builder request = CreateDatabaseRequest.builder(pg, config.getModelVersion());
        if (loadSchema) {
            request.addGeneratedSchema().addVersionTable().addStandaloneTables();
        }
        if (importDataIds != null) {
            Multimap<String, String> idsByType = ArrayListMultimap.create();
            for (String importDataId : importDataIds) {
                //FIXME
                String className = importDataId.contains(".ll.") ? "fr.ird.observe.entities.data.ll.common.Trip" : "fr.ird.observe.entities.data.ps.common.Trip";
                idsByType.put(className, importDataId);
            }
            for (Map.Entry<String, Collection<String>> entry : idsByType.asMap().entrySet()) {
                String type = entry.getKey();
                Set<String> dataIds = Set.copyOf(entry.getValue());
                log.info(String.format("Add %d data of type: %s", dataIds.size(), type));
                request.dataIdsToAdd(type, dataIds);
            }
        }
        return dataSourceService.produceCreateSqlScript(request.build());
    }

    @Override
    public void close() {
        if (!configurationAndConnection.withConnection()) {
            return;
        }
        checkIsOpen();
        fireNewMessage(t("observe.ui.datasource.storage.message.closing", getLabel()));
        fireClosing();
        referentialCache.close();
        try {
            // si la connection a expirée la source a deja été fermée pas le serveur
            if (!expired) {
                DataSourceService dataSourceService = getDataSourceService();
                try {
                    dataSourceService.close();
                } catch (SecurityExceptionSupport e) {
                    // ignore any security exception from server
                }
            }
        } finally {
//            cache.clear();
            ObserveUtil.cleanMemory();
            setConnection(null);
            expired = false;
            fireNewMessage(t("observe.ui.datasource.storage.message.closed", getLabel()));
            fireClosed();
        }
    }

    public void destroy() {
        checkIsNotOpen();
        fireNewMessage(t("observe.ui.datasource.storage.message.destroying", getLabel()));
        try {
            destroy0();
        } finally {
            ObserveUtil.cleanMemory();
            fireNewMessage(t("observe.ui.datasource.storage.message.destroyed", getLabel()));
        }
    }

    public Set<DataSourceUserDto> getUsers() {
        checkIsNotOpen();
        return getAnonymousService().getUsers(getConfiguration());
    }

    public void applySecurity(Set<DataSourceUserDto> users) {
        checkIsNotOpen();
        getAnonymousService().applySecurity(getConfiguration(), users);
    }

    public SelectionTreeConfig newSelectionTreeConfig() {
        return getClientConfig().getSelectionConfig(this);
    }

    //FIXME: We should design a new object to open and create databases, in that way we should be able to apply
    //FIXME: the same logic for opening any data sources, means in data source service we do not call exactly the same
    //FIXME: code while importing a database for example, we SHOULD always use the same logic (ask use to migrate)
    public void migrateData(ObserveDataSourceInformation dataSourceInformation, Version targetVersion) {

        checkIsNotOpen();

        Version dbVersion = dataSourceInformation.getVersion();
        if (!dataSourceInformation.getMigrations().isEmpty()) {

            if (dbVersion.before(dataSourceInformation.getMinimumVersion())) {

                JOptionPane.showMessageDialog(
                        null,
                        t("observe.ui.datasource.storage.migrate.not.possible.before.version.3.0.message", targetVersion, dbVersion),
                        t("observe.ui.datasource.storage.migrate.not.possible.before.version.3.0.title", targetVersion),
                        JOptionPane.WARNING_MESSAGE);

            } else {

                int answer = JOptionPane.showConfirmDialog(
                        null,
                        t("observe.ui.datasource.storage.migrate.askUser.message", dbVersion, targetVersion, dataSourceInformation.getMigrations()),
                        t("observe.ui.datasource.storage.migrate.askUser.title", targetVersion),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);

                if (answer == JOptionPane.YES_OPTION) {
                    log.info(String.format("Migrate data source %s in %s to %s", getLabel(), dbVersion, targetVersion));
                    getAnonymousService().migrateData(getConfiguration());
                }
            }
        }

    }

    public boolean isOwner() {
        return owner;
    }

    public void migrateDataIfPossible(ObserveDataSourceInformation dataSourceInformation, Version targetVersion) {
        checkIsNotOpen();
        Version dbVersion = dataSourceInformation.getVersion();
        if (!dataSourceInformation.getMigrations().isEmpty()) {
            if (dbVersion.before(dataSourceInformation.getMinimumVersion())) {
                log.warn(t("observe.ui.datasource.storage.migrate.not.possible.before.version.3.0.message", targetVersion, dbVersion));
            } else {
                log.info(t("observe.ui.datasource.storage.migrate.askUser.message", dbVersion, targetVersion, dataSourceInformation.getMigrations()));
                log.info("Migrate data source " + getLabel() + " in " + dbVersion + " to " + targetVersion);
                getAnonymousService().migrateData(getConfiguration());
            }
        }
    }

    public <D extends BusinessDto> Map<Class<? extends ReferentialDtoReference>, ReferentialDtoReferenceSet<?>> updateReferentialReferenceSetsCache(Class<D> dtoType) {
        ReferentialService referentialService = getReferentialService();
        return referentialCache.loadReferenceSets(referentialService, dtoType);
    }

    public <R extends ReferentialDtoReference> ReferentialDtoReferenceSet<R> getReferentialReferenceSet(Class<R> type, String... ids) {
        checkIsOpen();
        ReferentialService referentialService = getReferentialService();
        ReferentialDtoReferenceSet<R> result = referentialCache.getReferentialReferenceSet(referentialService, type);
        if (ids.length > 0) {
            result = result.subset(ids);
        }
        return result;
    }

    public <R extends ReferentialDtoReference> Set<R> getReferentialReferences(Class<R> type, String... ids) {
        ReferentialDtoReferenceSet<R> referentialReferenceSet = getReferentialReferenceSet(type, ids);
        return referentialReferenceSet.toSet();
    }

    public boolean isOpen() {
        return configurationAndConnection.withConnection();
    }

    public ObserveDataSourceInformation checkCanConnect(boolean canBeEmpty) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        checkIsNotOpen();
        AnonymousService dataSourceService = getAnonymousService();
        ObserveDataSourceInformation dataSourceInformation = canBeEmpty ? dataSourceService.checkCanConnectOrBeEmpty(getConfiguration()) : dataSourceService.checkCanConnect(getConfiguration());
        setOwner(dataSourceInformation.isOwner());
        setSuperUser(dataSourceInformation.isSuperUser());
        return dataSourceInformation;
    }

    public DataSourceReferenceProvider getReferenceProvider() {
        return referenceProvider;
    }

    public boolean isLocal() {
        return getConfiguration().isLocal();
    }

    public boolean isRemote() {
        return getConfiguration().isRemote();
    }

    public boolean isServer() {
        return getConfiguration().isServer();
    }

    public boolean isSuperUser() {
        return superUser;
    }

    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }

    public String getSimpleReferentialSynchronisationLastUpdate() {
        Date lastUpdateDate = simpleReferentialSynchronisationLastUpdate();
        if (lastUpdateDate != null) {
            return String.format("%1$tF %1$tT", lastUpdateDate);
        }
        return null;
    }

    public String getAdvancedReferentialSynchronisationLastUpdate() {
        Date lastUpdateDate = advancedReferentialSynchronisationLastUpdate();
        if (lastUpdateDate != null) {
            return String.format("%1$tF %1$tT", lastUpdateDate);
        }
        return null;
    }

    protected Date simpleReferentialSynchronisationLastUpdate() {
        return getDataSourceService().getLastUpdateDate(SynchronizeService.SIMPLE_REFERENTIAL_SYNCHRONISATION);
    }

    protected Date advancedReferentialSynchronisationLastUpdate() {
        return getDataSourceService().getLastUpdateDate(SynchronizeService.ADVANCED_REFERENTIAL_SYNCHRONISATION);
    }

    /**
     * Effectue une sauvegarde de la base locale vers le fichier choisi.
     *
     * @param dst le fichier de sauvegarde
     */
    public void backupLocalDatabase(Path dst) {
        Objects.requireNonNull(dst, "file where to backup can not be null");
        log.info(String.format("Do backup of %s into: %s", this, dst));
        try {
            Files.deleteIfExists(dst);
            CreateDatabaseRequest request = CreateDatabaseRequest.builder(false, config.getModelVersion()).addGeneratedSchema().addVersionTable().addStandaloneTables().addAllData().build();
            SqlScript dataDump = getDataSourceService().produceCreateSqlScript(request);
            dataDump.copyAndCompress(dst);
        } catch (Exception e) {
            throw new ObserveSwingTechnicalException(e);
        }
    }

    public void addObserveSwingDataSourceListener(ObserveSwingDataSourceListener listener) {
        listenerList.add(ObserveSwingDataSourceListener.class, listener);
    }

    public void removeObserveSwingDataSourceListener(ObserveSwingDataSourceListener listener) {
        log.info("removing listener " + listener);
        listenerList.remove(ObserveSwingDataSourceListener.class, listener);
    }

    public ObserveSwingDataSourceListener[] getObserveSwingDataSourceListener() {
        return listenerList.getListeners(ObserveSwingDataSourceListener.class);
    }

    private void fireNewMessage(String message) {
        fireNewMessage(message, ObserveSwingDataSourceEvent.MessageLevel.INFO);
    }

    private void fireNewMessage(String message, @SuppressWarnings("SameParameterValue") ObserveSwingDataSourceEvent.MessageLevel level) {
        ObserveSwingDataSourceEvent evt = new ObserveSwingDataSourceEvent(this, message, level);
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            listener.onNewMessage(evt);
        }
    }

    public boolean isExpired() {
        return expired;
    }

    public void expired() {
        this.expired = true;
    }

    private void fireOpening() {
        ObserveSwingDataSourceEvent evt = new ObserveSwingDataSourceEvent(this);
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            listener.onOpening(evt);
        }
    }

    private void fireOpened() {
        ObserveSwingDataSourceEvent evt = new ObserveSwingDataSourceEvent(this);
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            listener.onOpened(evt);
        }
    }

    private void fireClosing() {
        ObserveSwingDataSourceEvent evt = new ObserveSwingDataSourceEvent(this);
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            listener.onClosing(evt);
        }
    }

    private void fireClosed() {
        ObserveSwingDataSourceEvent evt = new ObserveSwingDataSourceEvent(this);
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            listener.onClosed(evt);
        }
    }

    private void checkIsOpen() {
        if (!isOpen()) {
            throw new IllegalStateException("Connection is not open");
        }
    }

    private void checkIsNotOpen() {
        if (isOpen()) {
            throw new IllegalStateException("Connection is open");
        }
    }

    public Map<Class<? extends ReferentialDto>, Set<ToolkitIdLabel>> getReferentialMap(Map<Class<? extends ReferentialDto>, Set<String>> referentialIds) {
        Map<Class<? extends ReferentialDto>, Set<ToolkitIdLabel>> result = new LinkedHashMap<>();
        for (Class<? extends ReferentialDto> dtoType : referentialIds.keySet()) {
            Set<String> ids = referentialIds.get(dtoType);
            Class<ReferentialDtoReference> referenceType = ObserveBusinessProject.get().getMapping().getReferenceType(dtoType);
            Set<ReferentialDtoReference> references = getReferentialReferenceSet(referenceType).subSet(ids).collect(Collectors.toSet());
            decoratorService.installDecorator(referenceType, references.stream());
            Set<ToolkitIdLabel> labels = references.stream().map(ReferentialDtoReference::toLabel).collect(Collectors.toSet());
            decoratorService.installToolkitIdLabelDecorator(dtoType, labels.stream());
            result.computeIfAbsent(dtoType, k -> new LinkedHashSet<>()).addAll(labels);
        }
        return result;
    }

    public ProgressionModel getProgressModel() {
        return progressModel;
    }

    public void setProgressModel(ProgressionModel progressModel) {
        this.progressModel = progressModel;
    }

    public boolean getOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }

    public void sanitizeIds(BoundedRangeModel progressModel, IdAggregateModel navigationSelectModel) {
        try {
            for (IdModel navigationModel : navigationSelectModel.getModels()) {
                for (IdNode<?> node : navigationModel.getNodesWithIds()) {
                    if (node.isEnabled()) {
                        Set<String> safeIds = getDataSourceService().retainExistingIds(Collections.singleton(node.getId()));
                        if (safeIds.isEmpty()) {
                            node.setId(null);
                        }
                    }
                    progressModel.setValue(progressModel.getValue() + 1);
                }
            }
        } catch (Exception e) {
            log.error("Could not sanitizeIds", e);
        }
    }

    public void sanitize(BoundedRangeModel progressModel, FloatingObjectPresetsStorage presets) {
        int size = presets.size();
        log.info("Will sanitize {} preset(s).", size);
        Map<Path, FloatingObjectPreset> map = presets.getPresetsMap();
        Map<Path, FloatingObjectPreset> result = new TreeMap<>(map);
        Iterator<Map.Entry<Path, FloatingObjectPreset>> iterator = result.entrySet().iterator();
        while (iterator.hasNext()) {
            sanitize(progressModel, iterator);
        }
        presets.setPresetsMap(result);
        log.info("Found {} safe preset(s).", presets.size());
    }

    private void sanitize(BoundedRangeModel progressModel, Iterator<Map.Entry<Path, FloatingObjectPreset>> iterator) {
        FloatingObjectPreset preset = iterator.next().getValue();
        log.info("Will sanitize preset: {}.", preset.getLabel(config.getReferentialLocale()));
        Set<String> ids = preset.getIds();
        Set<String> resultIds = this.getDataSourceService().retainExistingIds(ids);
        if (resultIds.size() < ids.size()) {
            iterator.remove();
            Set<String> missingIds = new TreeSet<>(ids);
            missingIds.removeAll(resultIds);
            log.warn("Remove preset: {} ({} id(s) not found in database).", preset.getLabel(config.getReferentialLocale()), missingIds);
        }
        progressModel.setValue(progressModel.getValue() + 1);
    }

    public void setCanMigrate(ClientConfig appConfig) {
        if (isLocal()) {
            ObserveDataSourceConfigurationTopiaH2 h2Config = (ObserveDataSourceConfigurationTopiaH2) getConfiguration();
            h2Config.setCanMigrate(appConfig.isH2CanMigrate());
        } else if (isRemote()) {
            ObserveDataSourceConfigurationTopiaPG pgConfig = (ObserveDataSourceConfigurationTopiaPG) getConfiguration();
            pgConfig.setCanMigrate(appConfig.isObstunaCanMigrate());
        }
    }

    public String getSummaryText() {
        return ObserveSwingDataSourceTemplate.generate(this);
    }

    public void destroy0() {
        if (!isLocal()) {
            throw new IllegalStateException("CAN NOT DESTROY A NONE LOCAL DATABASE");
        }
        ObserveDataSourceConfigurationTopiaH2 dataSourceConfigurationH2 = (ObserveDataSourceConfigurationTopiaH2) getConfiguration();
        File databaseFile = dataSourceConfigurationH2.getDatabaseFile();
        if (!databaseFile.delete()) {
            throw new IllegalStateException("could not delete " + databaseFile);
        }
    }

    public String getLabelWithUrl() {
        String txt;
        switch (getConfiguration().getConnectMode()) {
            case LOCAL:
                return getLabel();
            case SERVER: {
                txt = getConfiguration().getUrl();
                ObserveDataSourceConfigurationRest configuration = (ObserveDataSourceConfigurationRest) getConfiguration();
                Optional<String> optionalDatabaseName = Optional.ofNullable(configuration.getDatabaseName());
                txt += " - " + t("observe.ui.datasource.storage.server.dataBase.name") + " " + (optionalDatabaseName.orElse(t("observe.ui.datasource.storage.server.default.dataBase")));
            }
            break;
            case REMOTE:
                txt = getConfiguration().getUrl();
                break;
            default:
                throw new IllegalStateException(String.format("Can't have a such mode: %s", getConfiguration().getConnectMode()));
        }
        return String.format("%s (%s)", getLabel(), txt);
    }

    public void backup(File file) {
        if (!isLocal()) {
            throw new IllegalStateException("Cant backup a none H2 database.");
        }
        TopiaConfiguration configuration = ObserveTopiaConfigurationFactory.create(getConfiguration());
        JdbcHelperH2 jdbcHelperH2 = new JdbcHelperH2(configuration);
        jdbcHelperH2.backup(file, true);
    }
}
