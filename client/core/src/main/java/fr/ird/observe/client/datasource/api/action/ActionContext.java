package fr.ird.observe.client.datasource.api.action;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.dto.ProgressionModel;

import java.util.Objects;

public class ActionContext {

    private final ProgressionModel progressModel;
    private final ClientConfig config;

    public ActionContext(ProgressionModel progressModel, ClientConfig config) {
        this.progressModel = Objects.requireNonNull(progressModel);
        this.config = Objects.requireNonNull(config);
    }

    public void incrementsProgress() {
        progressModel.increments();
    }

    public ProgressionModel getProgressModel() {
        return progressModel;
    }

    public ClientConfig getConfig() {
        return config;
    }

    protected void initStepsCount(boolean firstAction, int stepsCount) {
        if (firstAction) {
            progressModel.setValue(0);
            progressModel.setMaximum(stepsCount);
        } else {
            progressModel.setMaximum(progressModel.getMaximum() + stepsCount);
        }
    }

}
