package fr.ird.observe.client.datasource.api.action;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

public abstract class ActionStep<A extends ActionContext> {

    protected final A actionContext;
    private final ActionStepInputState inputState;
    private final boolean needFeedBackOnFail;
    private ActionStepOutputState outputState;
    private Exception outputError;
    private int computedSteps;
    private int consumedSteps;

    protected ActionStep(A actionContext, boolean pending, boolean needFeedBackOnFail) {
        this.actionContext = actionContext;
        this.inputState = pending ? ActionStepInputState.PENDING : ActionStepInputState.SKIP;
        this.needFeedBackOnFail = needFeedBackOnFail;
    }

    protected abstract int computeStepCount0();

    public abstract String getErrorTitle();

    protected abstract boolean skip(ActionWithSteps<A> mainAction);

    protected abstract void doAction0(ActionWithSteps<A> mainAction) throws Exception;

    public boolean isNeedFeedBackOnFail() {
        return needFeedBackOnFail;
    }

    public void incrementsProgress() {
        consumedSteps++;
        actionContext.incrementsProgress();
    }

    public void doAction(ActionWithSteps<A> mainAction) {
        if (skip(mainAction)) {
            onSkip();
            return;
        }
        try {
            doAction0(mainAction);
            onDone();
        } catch (Exception e) {
            onError(e);
        }
    }

    public ActionStepInputState getInputState() {
        return inputState;
    }

    public ActionStepOutputState getOutputState() {
        return outputState;
    }

    public void setOutputState(ActionStepOutputState outputState) {
        this.outputState = outputState;
    }

    public boolean notFail() {
        return outputState == null || outputState.notFail();
    }

    public boolean fail() {
        return outputState != null && outputState.fail();
    }

    public Exception getOutputError() {
        return outputError;
    }

    public void setOutputError(Exception outputError) {
        this.outputError = outputError;
    }

    protected int computeStepCount() {
        return this.computedSteps = computeStepCount0();
    }

    protected void onEnd() {
        while (consumedSteps < computedSteps) {
            incrementsProgress();
        }
    }

    protected void onSkip() {
        setOutputState(ActionStepOutputState.SKIP);
        onEnd();
    }

    protected void onDone() {
        setOutputState(ActionStepOutputState.DONE);
        onEnd();
    }

    protected void onError(Exception error) {
        setOutputState(ActionStepOutputState.FAILED);
        setOutputError(Objects.requireNonNull(error));
        onEnd();
    }

    protected boolean skipIfPreviousFail(ActionWithSteps<A> mainAction) {
        if (inputState.skip()) {
            // If we are on skip state, then nothing to do, so previous states are not used
            return true;
        }
        // No fail before, nothing was changed from previous steps,
        return mainAction.fail();
    }

}
