package fr.ird.observe.client.datasource.api.action;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.util.UIHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class ActionWithSteps<A extends ActionContext> implements Runnable {

    private static final Logger log = LogManager.getLogger(ActionWithSteps.class);

    private final A actionContext;
    private final List<ActionStep<A>> previousFailedSteps;
    private final Iterator<? extends ActionStep<A>> stepsIterator;
    private ActionStep<A> currentStep;
    private ActionStepOutputState outputState;

    public ActionWithSteps(A actionContext, boolean firstAction) {
        this.actionContext = Objects.requireNonNull(actionContext);
        List<? extends ActionStep<A>> steps = createSteps(actionContext);
        this.stepsIterator = steps.iterator();
        this.previousFailedSteps = new ArrayList<>(steps.size());
        int stepsCount = 0;
        for (ActionStep<A> step : steps) {
            stepsCount += step.computeStepCount();
        }
        log.info(String.format("Found %d steps on %s.", stepsCount, getClass().getName()));
        actionContext.initStepsCount(firstAction, stepsCount);
    }

    protected abstract List<? extends ActionStep<A>> createSteps(A actionContext);

    protected abstract FeedBackBuilderModel createFeedBackModel(ClientConfig config, ActionStep<A> stepsFailed);

    @Override
    public final void run() {
        while (stepsIterator.hasNext()) {
            doStepAction();
            onStepFinished();
        }
        List<ActionStep<A>> stepsFailed = previousFailedSteps.stream().filter(ActionStep::isNeedFeedBackOnFail).collect(Collectors.toList());
        if (stepsFailed.isEmpty()) {
            outputState = ActionStepOutputState.DONE;
        } else {
            try {
                doFeedBack(stepsFailed.get(0));
                outputState = ActionStepOutputState.DONE;
            } catch (Exception e) {
                outputState = ActionStepOutputState.FAILED;
                UIHelper.handlingError(e);
            }
        }
    }

    protected void doFeedBack(ActionStep<A> stepsFailed) throws Exception {
        log.info(String.format("Do feed back after errors... on steps: %s", stepsFailed));
        FeedBackBuilderModel builderModel = createFeedBackModel(getContext().getConfig(), stepsFailed);
        FeedBackBuilder feedBackBuilder = new FeedBackBuilder(builderModel);
        feedBackBuilder.run();
    }

    public boolean isStarted() {
        return currentStep != null;
    }

    public boolean isFinished() {
        return isStarted() && !stepsIterator.hasNext();
    }

    public boolean notFail() {
        return outputState == null || outputState.notFail();
    }

    public boolean fail() {
        return outputState != null && outputState.fail();
    }

    public A getContext() {
        return actionContext;
    }

    protected final void doStepAction() {
        if (isFinished()) {
            throw new IllegalStateException("No more step to consume");
        }
        currentStep = stepsIterator.next();
        log.info(String.format("New step to consume: %s", currentStep));

        currentStep.doAction(this);
    }

    protected void onStepFinished() {
        if (isStarted()) {
            // Make sure step was consumed
            if (currentStep.getOutputState() == null) {
                throw new IllegalStateException(String.format("Step %s cas not consumed", currentStep));
            }
            log.info(String.format("Current step %s was consumed: %s", currentStep, currentStep.getOutputState()));
            if (currentStep.fail()) {
                previousFailedSteps.add(currentStep);
                outputState = ActionStepOutputState.FAILED;
            } else if (notFail()) {
                outputState = ActionStepOutputState.DONE;
            }
        }
    }
}
