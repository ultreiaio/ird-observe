package fr.ird.observe.client.datasource.api.action;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;

import static io.ultreia.java4all.i18n.I18n.t;

@SuppressWarnings("unused")
@GenerateTemplate(template = "feedBackOnClosingLocalDataSource.ftl")
public class CloseLocalDataSourceFeedBackModel extends FeedBackBuilderModel {

    public CloseLocalDataSourceFeedBackModel(ClientConfig config, ActionStep<? extends ActionContext> stepsFailed) {
        super(config, stepsFailed, t("observe.error.storage.could.not.close.local.data.source.title"));
    }

    @Override
    protected boolean destroyLocalDatasource() {
        return true;
    }

    @Override
    protected boolean needCancel() {
        return true;
    }

    @Override
    protected String generateText() {
        return CloseLocalDataSourceFeedBackModelTemplate.generate(this);
    }

}
