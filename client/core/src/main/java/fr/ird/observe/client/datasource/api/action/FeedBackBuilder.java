package fr.ird.observe.client.datasource.api.action;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.main.MainUIModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.UIInitHelper;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkEvent;
import java.awt.Container;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static io.ultreia.java4all.i18n.I18n.t;

public class FeedBackBuilder implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(FeedBackBuilder.class);

    private final FeedBackBuilderModel model;
    private final Container container;

    public FeedBackBuilder(FeedBackBuilderModel model) {
        this.container = Objects.requireNonNull(getMainUI());
        this.model = Objects.requireNonNull(model);
    }

    public FeedBackBuilder(FeedBackBuilderModel model, Container container) {
        this.container = Objects.requireNonNull(container);
        this.model = Objects.requireNonNull(model);
    }

    public void run() throws Exception {

        Object[] options = model.needCancel() ? new Object[]{t("observe.ui.choice.continue"), t("observe.ui.choice.generate.feedback"), t("observe.ui.choice.cancel")} : new Object[]{t("observe.ui.choice.continue"), t("observe.ui.choice.generate.feedback")};
        JComponent message = model.getMessage();
        SwingUtilities.invokeLater(() -> {
            try {
                model.prepareRender(message, container);
            } catch (Exception e) {
                log.error(e);
            }
        });
        int response = UIHelper.askUser(container,
                                        model.getTitle(),
                                        message,
                                        JOptionPane.ERROR_MESSAGE,
                                        options, 0);
        boolean createFeedBack = true;
        switch (response) {
            case JOptionPane.CLOSED_OPTION:
            case 2:
                // cancel operation
                log.warn("User cancel feed back creation.");
                return;
            case 0:
                // continue without feed back
                createFeedBack = false;
                break;
        }
        File feedBackFile = model.getFeedBackFile();
        try {
            ClientConfig config = model.getConfig();
            if (createFeedBack) {
                log.info(String.format("Create feed back at: %s", feedBackFile));
                createFeedBack(config, feedBackFile);
            }
            if (model.destroyLocalDatasource()) {
                setLocalStorageDoesNotExist(config);
                destroyLocalDataSource(config);
            }
            if (createFeedBack) {
                informUser(feedBackFile);
            }
        } catch (Exception e) {
            log.error("Could not create feed back", e);
            throw e;
        }
    }

    private void informUser(File feedBackFile) {
        setUiStatus(t("observe.ui.datasource.storage.feedback.generated.title", feedBackFile));
        MainUIModel mainUIModel = getMainUIModel();
        if (mainUIModel != null) {
            mainUIModel.setOpenLastFeedBackEnabled(true);
            return;
        }
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            JTextPane textPane = new JTextPane();

            String text = String.format("<html><body>%1$s</body></html>", t("observe.ui.datasource.storage.feedback.generated.title", String.format("<a href=\"%1$s\">%1$s</a>", feedBackFile)));
            textPane.setText(text);
            UIInitHelper.initInformationPane(textPane, text);

            textPane.addHyperlinkListener(e -> {
                if (e.getEventType() != HyperlinkEvent.EventType.ACTIVATED) {
                    return;
                }
                try {
                    desktop.browse(feedBackFile.getParentFile().toURI());
                } catch (IOException ignored) {
                }

            });
            JOptionPane.showMessageDialog(container, textPane, t("observe.ui.action.open.last.feedback"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void createFeedBack(ClientConfig config, File feedBackFile) throws IOException {
        if (Files.notExists(feedBackFile.toPath().getParent())) {
            Files.createDirectories(feedBackFile.toPath().getParent());
        }
        String version = config.getVersion().toString();
        try (ZipOutputStream outputStream = new ZipOutputStream(new FileOutputStream(feedBackFile))) {
            copyDirectoryToZip(config.getDbDirectory().toPath(), outputStream);
            copyDirectoryToZip(config.getResourcesDirectory().toPath(), outputStream);
            copyDirectoryToZip(config.getLogDirectory().toPath(), p -> p.toFile().getName().contains(version), outputStream);
            Path userConfigFile = config.get().getExtraConfigFile().toPath();
            copyFileToZip(userConfigFile.getParent(), userConfigFile, outputStream);
        }
    }

    private void destroyLocalDataSource(ClientConfig config) throws IOException {
        File dataSourceDirectory = config.getDbDirectory();
        FileUtils.deleteDirectory(dataSourceDirectory);
    }

    private void copyDirectoryToZip(Path rootFile, ZipOutputStream zipOutputStream) throws IOException {
        copyDirectoryToZip(rootFile, t -> !t.getParent().getParent().toFile().getName().equals("shapeFiles"), zipOutputStream);
    }

    private void copyDirectoryToZip(Path rootFile, Predicate<Path> predicate, ZipOutputStream zipOutputStream) throws IOException {
        if (Files.notExists(rootFile)) {
            return;
        }
        Path rootFileParent = rootFile.getParent();
        Files.walk(rootFile).filter(Files::isRegularFile).filter(predicate).forEach(p -> copyFileToZip(rootFileParent, p, zipOutputStream));
    }

    private void copyFileToZip(Path rootFile, Path file, ZipOutputStream zipOutputStream) {

        String entryName = rootFile.relativize(file).toString();
        try {
            zipOutputStream.putNextEntry(new ZipEntry(entryName));
            Files.copy(file, zipOutputStream);
        } catch (IOException e) {
            String message = String.format("Could not copy file %s to zip entry %S", file, entryName);
            log.error(message);
            throw new IllegalStateException(message, e);
        }
    }

    private void setLocalStorageDoesNotExist(ClientConfig config) {
        config.setLocalStorageExist(false);
        config.saveForUser();
    }

}
