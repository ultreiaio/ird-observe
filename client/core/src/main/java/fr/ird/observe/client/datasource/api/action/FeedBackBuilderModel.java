package fr.ird.observe.client.datasource.api.action;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.ObserveUtil;
import io.ultreia.java4all.http.HResponseErrorException;
import io.ultreia.java4all.i18n.I18n;
import org.nuiton.jaxx.runtime.context.JAXXContextEntryDef;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.widgets.hidor.HidorButton;
import org.nuiton.jaxx.widgets.hidor.HidorButtonHandler;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.Container;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created on 23/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class FeedBackBuilderModel {
    private final ClientConfig config;
    private final ActionStep<?> actionStepInError;
    private final File feedBackFile;
    private final String title;
    private Container container;

    protected FeedBackBuilderModel(ClientConfig config, ActionStep<?> actionStepInError, String title) {
        this.config = Objects.requireNonNull(config);
        this.actionStepInError = Objects.requireNonNull(actionStepInError);
        this.feedBackFile = config.newFeedBackFile();
        this.title = title;
    }

    protected abstract String generateText();

    public String getTitle() {
        return title;
    }

    public Exception getError() {
        return actionStepInError.getOutputError();
    }

    public String getErrorMessage() {
        String message = getError().getMessage();
        return message == null ? null : message.length() < 150 ? message : message.substring(0, 150) + "...";
    }

    public String getErrorTitle() {
        return actionStepInError.getErrorTitle();
    }

    public ClientConfig getConfig() {
        return config;
    }

    public File getFeedBackFile() {
        return feedBackFile;
    }

    public String getText() {
        return generateText();
    }

    protected abstract boolean destroyLocalDatasource();

    protected abstract boolean needCancel();

    protected JComponent getMessage() {
        JLabel text = new JLabel(getText());
        JTextArea error = new JTextArea(getErrorStack());
        error.setEditable(false);
        error.setFont(error.getFont().deriveFont(8.0f));
        error.setRows(25);
        error.setColumns(80);
        error.setFocusable(true);
        JScrollPane errorScrollPane = new JScrollPane(error);
        errorScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        errorScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        JPanel errorPanel = new JPanel(new BorderLayout());
//        errorPanel.setMinimumSize(new Dimension(300, 400));
//        errorPanel.setMaximumSize(new Dimension(600, 400));
        JAXXContextEntryDef<Object> afterOpen = UIHelper.newContextEntryDef(HidorButtonHandler.AFTER_OPEN, Consumer.class);

        HidorButton comp = new HidorButton(new JAXXInitialContext().add(afterOpen, (Consumer<HidorButton>) o -> SwingUtilities.invokeLater(() -> {
//            errorPanel.revalidate();
            JDialog dialog = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, o);
            if (dialog != null) {
                dialog.pack();
                UIHelper.center(container, dialog);
            }
        })));
        comp.setShowText(I18n.t("observe.ui.error.show.error"));
        comp.setShowTip(I18n.t("observe.ui.error.show.error"));
        comp.putClientProperty("showIcon", UIManager.getIcon("action.go-detail"));
        comp.setHideIcon(UIManager.getIcon("action.go-detail"));
        comp.setHideText(I18n.t("observe.ui.error.hide.error"));
        comp.setHideTip(I18n.t("observe.ui.error.hide.error"));
        comp.setTarget(errorScrollPane);
        comp.setTargetVisible(false);
        errorPanel.add(comp, BorderLayout.WEST);
        errorPanel.add(errorScrollPane, BorderLayout.CENTER);
        JPanel p = new JPanel(new BorderLayout());
        p.add(text, BorderLayout.NORTH);
        p.add(errorPanel, BorderLayout.CENTER);
        return p;
    }

    public void prepareRender(JComponent message, Container container) {
        this.container = container;
    }


    public String getErrorStack() {
        StringWriter w = new StringWriter();
        Exception error = getError();
        try (PrintWriter writer = new PrintWriter(w)) {
            error.printStackTrace(writer);
        }
        if (error instanceof HResponseErrorException) {
            w.append("\n-------------------------------------------------------------------------------------------\n");
            String prettyFormat = ObserveUtil.toPrettyFormat(((HResponseErrorException) error).getError().getMessage());
            w.append(prettyFormat);
            w.append("\n-------------------------------------------------------------------------------------------\n");
        }
        return w.toString();
    }
}
