package fr.ird.observe.client.datasource.api.action;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;

import static io.ultreia.java4all.i18n.I18n.t;

@SuppressWarnings("unused")
@GenerateTemplate(template = "feedBackOnOpeningLocalDataSource.ftl")
public class OpenLocalDataSourceFeedBackModel extends FeedBackBuilderModel {

    private final boolean destroyLocalDatasource;

    public OpenLocalDataSourceFeedBackModel(ClientConfig config, ActionStep<? extends ActionContext> stepsFailed, boolean destroyLocalDatasource) {
        super(config, stepsFailed, t("observe.error.storage.could.not.open.data.source.title"));
        this.destroyLocalDatasource = destroyLocalDatasource;
    }

    public boolean isDestroyLocalDatasource() {
        return destroyLocalDatasource;
    }

    @Override
    protected boolean destroyLocalDatasource() {
        return destroyLocalDatasource;
    }

    @Override
    protected boolean needCancel() {
        return true;
    }

    @Override
    protected String generateText() {
        return OpenLocalDataSourceFeedBackModelTemplate.generate(this);
    }

    public boolean isDatabaseLocked() {
        return getError() instanceof DatabaseConnexionNotAuthorizedException;
    }
}

