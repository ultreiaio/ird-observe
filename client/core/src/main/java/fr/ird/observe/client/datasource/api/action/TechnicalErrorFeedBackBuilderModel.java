package fr.ird.observe.client.datasource.api.action;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.ProgressionModel;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.http.HResponseErrorException;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created on 26/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("unused")
@GenerateTemplate(template = "feedBackOnTechnicalError.ftl")
public class TechnicalErrorFeedBackBuilderModel extends FeedBackBuilderModel {

    private static final Logger log = LogManager.getLogger(TechnicalErrorFeedBackBuilderModel.class);

    static class TechnicalActionStep extends ActionStep<ActionContext> {

        protected TechnicalActionStep(ActionContext actionContext, boolean pending, boolean needFeedBackOnFail) {
            super(actionContext, pending, needFeedBackOnFail);
        }

        @Override
        protected int computeStepCount0() {
            return 0;
        }

        @Override
        public String getErrorTitle() {
            return I18n.t("errorUI.message");
        }

        @Override
        protected boolean skip(ActionWithSteps<ActionContext> mainAction) {
            return false;
        }

        @Override
        protected void doAction0(ActionWithSteps<ActionContext> mainAction) {
        }
    }

    static TechnicalActionStep createStep(ClientConfig config, Exception error) {
        ActionContext context = new ActionContext(new ProgressionModel(), config);
        TechnicalActionStep step = new TechnicalActionStep(context, false, true);
        step.setOutputError(error);
        StringBuilder message = new StringBuilder("A technical error report");
        if (error instanceof HResponseErrorException) {
            message.append("\n-------------------------------------------------------------------------------------------\n");
            String prettyFormat = ObserveUtil.toPrettyFormat(((HResponseErrorException) error).getError().getMessage());
            message.append(prettyFormat);
            message.append("\n-------------------------------------------------------------------------------------------\n");
        }
        log.error(message.toString(), error);
        step.setOutputState(ActionStepOutputState.FAILED);
        return step;
    }

    public TechnicalErrorFeedBackBuilderModel(ClientConfig config, Exception error) {
        super(config, createStep(config, error), I18n.t("errorUI.title"));
    }

    @Override
    protected boolean needCancel() {
        return false;
    }

    @Override
    protected boolean destroyLocalDatasource() {
        return false;
    }

    @Override
    protected String generateText() {
        return TechnicalErrorFeedBackBuilderModelTemplate.generate(this);
    }
}
