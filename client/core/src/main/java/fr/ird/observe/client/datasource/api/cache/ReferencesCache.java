package fr.ird.observe.client.datasource.api.cache;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.util.init.DefaultUIInitializer;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.form.FormDefinition;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.DtoReferenceCollection;
import fr.ird.observe.dto.reference.DtoReferenceDefinition;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceDefinition;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.list.DoubleList;
import io.ultreia.java4all.jaxx.widgets.list.ListHeader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

/**
 * Created by tchemit on 29/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferencesCache {

    private static final Logger log = LogManager.getLogger(ReferencesCache.class);
    /**
     * Registered filters.
     */
    private final Map<String, ReferencesFilter<?>> referentialFilters = new TreeMap<>();
    /**
     * All data references sets.
     */
    private final Map<String, DataDtoReferenceSet<?>> dataReferenceSetsByPropertyName = new TreeMap<>();
    /**
     * Data source used to get data.
     */
    private final ObserveSwingDataSource dataSource;
    /**
     * Flag to show or hide disabled referential references while filtering them.
     */
    private final boolean hideDisabledReferential;
    /**
     * All referential references sets.
     */
    private final Map<String, ReferentialDtoReferenceSet<?>> referentialReferenceSetsByPropertyName = new TreeMap<>();
    /**
     * ComboBoxes linked to this cache.
     */
    private List<FilterableComboBox<?>> comboBoxes;
    /**
     * ListHeaders linked to this cache.
     */
    private List<ListHeader<?>> ListHeaders;
    /**
     * Filterable double lists linked to this cache.
     */
    private List<DoubleList<?>> filterableDoubleLists;

    public ReferencesCache(ObserveSwingDataSource dataSource, boolean hideDisabledReferential) {
        this.dataSource = Objects.requireNonNull(dataSource);
        this.hideDisabledReferential = hideDisabledReferential;
    }

    public void addReferentialFilter(String propertyName, ReferencesFilter<?> filter) {
        referentialFilters.put(propertyName, filter);
    }

    public boolean isHideDisabledReferential() {
        return hideDisabledReferential;
    }

    public void registerComponents(DefaultUIInitializerResult initializerResult) {
        comboBoxes = initializerResult.getComboBoxes();
        ListHeaders = initializerResult.getListHeaders();
        filterableDoubleLists = initializerResult.getDoubleLists();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public void loadReferentialReferenceSetsInModel(Form<?> form) {
        Optional<FormDefinition<BusinessDto>> optionalFormDefinition = ObserveBusinessProject.get().getOptionalFormDefinition(form.getType());
        if (optionalFormDefinition.isEmpty()) {
            return;
        }
        FormDefinition<BusinessDto> formDefinition = optionalFormDefinition.get();
        Class<? extends BusinessDto> dtoType = formDefinition.getType();
        BusinessDto object = form.getObject();
        Map<String, ReferentialDtoReferenceSet<?>> modelReferentialReferenceSets = new LinkedHashMap<>();
        log.debug("Update referential reference sets for: " + dtoType);
        // mettre à jour le cache de référentiel
        Map<Class<? extends ReferentialDtoReference>, ReferentialDtoReferenceSet<?>> referentialReferenceSetsByType = dataSource.updateReferentialReferenceSetsCache(dtoType);
        // calculer les listes de référentiels à utiliser dans le modèle
        for (Map.Entry<String, ReferentialDtoReferenceDefinition<?, ?>> entry : formDefinition.getProperties().entrySet()) {
            String propertyName = entry.getKey();
            ReferentialDtoReferenceDefinition definition = entry.getValue();
            ReferentialDtoReferenceSet<?> referentialReferenceSet = referentialReferenceSetsByType.get(definition.getType());
            ReferentialDtoReferenceSet<?> filteredReferentialReferenceSet = filterReferentialReferenceSet(propertyName, definition, object, referentialReferenceSet);
            modelReferentialReferenceSets.put(propertyName, filteredReferentialReferenceSet);
        }
        referentialReferenceSetsByPropertyName.clear();
        referentialReferenceSetsByPropertyName.putAll(modelReferentialReferenceSets);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public void loadReferentialReferenceSetsInModel(FormDefinition<?> formDefinition, boolean clear) {
        Class<? extends BusinessDto> dtoType = formDefinition.getType();
        Map<String, ReferentialDtoReferenceSet<?>> modelReferentialReferenceSets = new LinkedHashMap<>();
        Set<String> existingProperties;
        if (!clear) {
            // keep existing sets
            modelReferentialReferenceSets.putAll(referentialReferenceSetsByPropertyName);
            existingProperties = referentialReferenceSetsByPropertyName.keySet();
        } else {
            existingProperties = Collections.emptySet();
        }
        log.debug("Update referential reference sets for: " + dtoType);
        // mettre à jour le cache de référentiel
        Map<Class<? extends ReferentialDtoReference>, ReferentialDtoReferenceSet<?>> referentialReferenceSetsByType = dataSource.updateReferentialReferenceSetsCache(dtoType);
        // calculer les listes de référentiels à utiliser dans le modèle
        for (Map.Entry<String, ReferentialDtoReferenceDefinition<?, ?>> entry : formDefinition.getProperties().entrySet()) {
            String propertyName = entry.getKey();
            if (existingProperties.contains(propertyName)) {
                // already in cache
                continue;
            }
            ReferentialDtoReferenceDefinition definition = entry.getValue();
            ReferentialDtoReferenceSet<?> referentialReferenceSet = referentialReferenceSetsByType.get(definition.getType());
            ReferentialDtoReferenceSet<?> filteredReferentialReferenceSet = filterReferentialReferenceSet(propertyName, definition, referentialReferenceSet);
            modelReferentialReferenceSets.put(propertyName, filteredReferentialReferenceSet);
        }
        referentialReferenceSetsByPropertyName.clear();
        referentialReferenceSetsByPropertyName.putAll(modelReferentialReferenceSets);
    }

    public <R extends DataDtoReference> Optional<Set<R>> tryToGetDataReferenceSet(String propertyName) {
        DataDtoReferenceSet<R> referenceSet = getDataReferenceSet(propertyName);
        Set<R> references = null;
        if (referenceSet != null) {
            references = referenceSet.toSet();
        }
        return Optional.ofNullable(references);
    }

    public <R extends ReferentialDtoReference> Optional<Set<R>> tryToGetReferentialReferenceSet(String propertyName) {
        ReferentialDtoReferenceSet<R> referenceSet = getReferentialReferenceSet(propertyName);
        Set<R> references = null;
        if (referenceSet != null) {
            references = referenceSet.toSet();
        }
        return Optional.ofNullable(references);
    }

    public <R extends ReferentialDtoReference> List<R> getReferentialReferences(String name) {
        ReferentialDtoReferenceSet<R> referentialReferenceSet = getReferentialReferenceSet(name);
        return new LinkedList<>(referentialReferenceSet.toSet());
    }

    public <R extends DataDtoReference> List<R> getDataReferences(String name) {
        DataDtoReferenceSet<R> referentialReferenceSet = getDataReferenceSet(name);
        return new LinkedList<>(referentialReferenceSet.toSet());
    }

    public <R extends ReferentialDtoReference> Optional<R> tryGetReferentialReferenceById(String name, String id) {
        ReferentialDtoReferenceSet<R> referenceSet = getReferentialReferenceSet(name);
        return referenceSet.tryGetReferenceById(id);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public void updateUiWithReferenceSetsFromModel(Form<?> form) {
        comboBoxes.forEach(c -> {
            Class dtoClass = c.getBeanType();
            if (ReferentialDtoReference.class.isAssignableFrom(dtoClass)) {
                updateReferentialFilterableComboBox(dtoClass, form, (FilterableComboBox) c);
            } else {
                updateDataFilterableComboBox(dtoClass, form, (FilterableComboBox) c);
            }
        });
        ListHeaders.forEach(c -> {
            Class dtoClass = c.getBeanType();
            if (ReferentialDtoReference.class.isAssignableFrom(dtoClass)) {
                updateReferentialListHeader(dtoClass, form, (ListHeader) c);
            } else {
                updateDataListHeader(dtoClass, form, (ListHeader) c);
            }
        });
        filterableDoubleLists.forEach(c -> {
            Class dtoClass = c.getBeanType();
            if (ReferentialDtoReference.class.isAssignableFrom(dtoClass)) {
                updateReferentialFilterableDoubleList(form, (DoubleList) c);
            } else {
                updateDataFilterableDoubleList(form, (DoubleList) c);
            }
        });
    }

    public void destroy() {
        dataReferenceSetsByPropertyName.clear();
        referentialReferenceSetsByPropertyName.clear();
    }

    public ObserveSwingDataSource getDataSource() {
        return dataSource;
    }

    private <R extends ReferentialDtoReference> ReferentialDtoReferenceSet<R> filterReferentialReferenceSet(String propertyName,
                                                                                                            DtoReferenceDefinition<?, R> propertyDefinition,
                                                                                                            ReferentialDtoReferenceSet<R> incomingReferentialReferenceSet) {

        Class<?> dtoType = propertyDefinition.getDtoType();

        List<R> incomingReferences = new LinkedList<>(incomingReferentialReferenceSet.toList());

        log.debug(String.format("Filter referential references (type %s - property %s), original size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));

        if (hideDisabledReferential && !propertyName.equals(FormDefinition.REFERENTIAL_LIST_HEADER)) {
            incomingReferences = DtoReferenceCollection.filterEnabled(incomingReferences);
            log.debug(String.format("Filter referential references (type %s - property %s), without disabled size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
        }

        @SuppressWarnings("unchecked")
        ReferencesFilter<R> referencesFilter = (ReferencesFilter<R>) referentialFilters.get(propertyName);
        if (referencesFilter != null) {
            incomingReferences = referencesFilter.filter(null, incomingReferences);
            log.debug(String.format("Filter referential references (type %s - property %s), with specific filter size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
        }
        return ReferentialDtoReferenceSet.of(incomingReferentialReferenceSet.getType(), incomingReferences, incomingReferentialReferenceSet.getLastUpdate());
    }


    private <R extends ReferentialDtoReference> ReferentialDtoReferenceSet<R> filterReferentialReferenceSet(String propertyName,
                                                                                                            DtoReferenceDefinition<?, R> propertyDefinition,
                                                                                                            BusinessDto object,
                                                                                                            ReferentialDtoReferenceSet<R> incomingReferentialReferenceSet) {

        Class<?> dtoType = propertyDefinition.getDtoType();

        List<R> incomingReferences = new LinkedList<>(incomingReferentialReferenceSet.toList());

        log.debug(String.format("Filter referential references (type %s - property %s), original size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));

        if (hideDisabledReferential && !propertyName.equals(FormDefinition.REFERENTIAL_LIST_HEADER)) {
            incomingReferences = DtoReferenceCollection.filterEnabled(incomingReferences);
            log.debug(String.format("Filter referential references (type %s - property %s), without disabled size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
        }

        @SuppressWarnings("unchecked")
        ReferencesFilter<R> referencesFilter = (ReferencesFilter<R>) referentialFilters.get(propertyName);
        if (referencesFilter != null) {
            incomingReferences = referencesFilter.filter(object, incomingReferences);
            log.debug(String.format("Filter referential references (type %s - property %s), with specific filter size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
            //FIXME Maybe we could find a better way to not do this twice?
            if (hideDisabledReferential && !propertyName.equals(FormDefinition.REFERENTIAL_LIST_HEADER)) {
                incomingReferences = DtoReferenceCollection.filterEnabled(incomingReferences);
                log.debug(String.format("Filter referential references (type %s - property %s), without disabled size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
            }
        }
        return ReferentialDtoReferenceSet.of(incomingReferentialReferenceSet.getType(), incomingReferences, incomingReferentialReferenceSet.getLastUpdate());
    }

    @SuppressWarnings("unchecked")
    public <R extends ReferentialDtoReference> ReferentialDtoReferenceSet<R> getReferentialReferenceSet(String propertyName) {
        ReferentialDtoReferenceSet<R> referenceSet = (ReferentialDtoReferenceSet<R>) referentialReferenceSetsByPropertyName.get(propertyName);
        Objects.requireNonNull(referenceSet, "Could not find referentialReferenceSet named " + propertyName);
        return referenceSet;
    }

    @SuppressWarnings("unchecked")
    private <R extends DataDtoReference> DataDtoReferenceSet<R> getDataReferenceSet(String propertyName) {
        DataDtoReferenceSet<R> referenceSet = (DataDtoReferenceSet<R>) dataReferenceSetsByPropertyName.get(propertyName);
        Objects.requireNonNull(referenceSet, "Could not find dataReferenceSet named " + propertyName);
        return referenceSet;
    }

    public <R extends DataDtoReference> void setDataReferenceSet(String propertyName, DataDtoReferenceSet<R> set) {
        dataReferenceSetsByPropertyName.put(Objects.requireNonNull(propertyName), Objects.requireNonNull(set));
    }

    private <R extends DataDtoReference> void updateDataFilterableDoubleList(Form<?> form, DoubleList<R> list) {
        Boolean noLoad = (Boolean) list.getClientProperty(DefaultUIInitializer.CLIENT_PROPERTY_LIST_NO_LOAD);
        List<R> data;
        if (Objects.equals(true, noLoad) || form == null) {
            data = Collections.emptyList();
        } else {
            data = getDataReferences(list.getModel().getProperty());
        }
        //FIXME A finir (bien vérifier que la sélection n'est plus dans l'univers)
        List<R> selected = list.getModel().getSelected();
        list.setUniverse(data);
        list.setSelected(selected);
        list.putClientProperty("data", data);
    }

    private <R extends ReferentialDtoReference> void updateReferentialFilterableDoubleList(Form<?> form, DoubleList<R> list) {
        Boolean forceLoadComboBox = (Boolean) list.getClientProperty(DefaultUIInitializer.CLIENT_PROPERTY_FORCE_LOAD);
        List<R> data;
        if (!Objects.equals(true, forceLoadComboBox) && form == null) {
            data = Collections.emptyList();
        } else {
            data = getReferentialReferences(list.getModel().getProperty());
        }
        //FIXME A finir (bien vérifier que la sélection n'est plus dans l'univers)
        List<R> selected = list.getModel().getSelected();
        list.setUniverse(data);
        list.setSelected(selected);
        list.putClientProperty("data", data);
    }

    private <R extends ReferentialDtoReference> void updateReferentialListHeader(Class<R> dtoClass, Form<?> form, ListHeader<R> list) {
        Boolean noLoad = (Boolean) list.getClientProperty(DefaultUIInitializer.CLIENT_PROPERTY_LIST_NO_LOAD);
        List<R> data;
        String propertyName = list.getName();
        if (!FormDefinition.REFERENTIAL_LIST_HEADER.equals(propertyName) && (Objects.equals(true, noLoad) || form == null)) {
            log.debug(String.format("Skip loading of ListHeader [%s-%s] (listNoLoad property found or form is null)", dtoClass.getSimpleName(), propertyName));
            data = Collections.emptyList();
        } else {
            data = getReferentialReferences(propertyName);
        }
        list.setData(data);
        list.putClientProperty("data", data);
        list.getList().setListData(new Vector<>(data));
    }

    private <R extends DataDtoReference> void updateDataListHeader(Class<R> dtoClass, Form<?> form, ListHeader<R> list) {
        Boolean noLoad = (Boolean) list.getClientProperty(DefaultUIInitializer.CLIENT_PROPERTY_LIST_NO_LOAD);
        List<R> data;
        String propertyName = list.getName();
        if (Objects.equals(true, noLoad) || form == null) {
            log.debug(String.format("Skip loading of ListHeader [%s-%s] (listNoLoad property found or form is null)", dtoClass.getSimpleName(), propertyName));
            data = Collections.emptyList();
        } else {
            data = getDataReferences(propertyName);
        }
        list.setData(data);
        list.putClientProperty("data", data);
    }

    private <R extends ReferentialDtoReference> void updateReferentialFilterableComboBox(Class<R> dtoClass, Form<?> form, FilterableComboBox<R> comboBox) {
        Boolean noLoad = (Boolean) comboBox.getClientProperty(DefaultUIInitializer.CLIENT_PROPERTY_LIST_NO_LOAD);
        Boolean forceLoadComboBox = (Boolean) comboBox.getClientProperty(DefaultUIInitializer.CLIENT_PROPERTY_FORCE_LOAD);
        String propertyName = comboBox.getConfig().getProperty();
        List<R> data;
        if (!Objects.equals(true, forceLoadComboBox) && (Objects.equals(true, noLoad) || form == null)) {
            log.debug(String.format("Skip loading of comboBox [%s-%s] (listNoLoad property found or form is null)", dtoClass.getSimpleName(), propertyName));
            data = Collections.emptyList();
        } else {
            Optional<Set<R>> optionalReferenceSetDto = tryToGetReferentialReferenceSet(propertyName);
            if (optionalReferenceSetDto.isPresent()) {
                Set<R> references = optionalReferenceSetDto.get();
                data = new ArrayList<>(references);
            } else {
                data = Collections.emptyList();
            }
        }
        log.debug(String.format("comboBox [%s-%s] : %d", dtoClass.getSimpleName(), propertyName, data.size()));
        comboBox.setData(data);
    }

    private <R extends DataDtoReference> void updateDataFilterableComboBox(Class<R> dtoClass, Form<?> form, FilterableComboBox<R> comboBox) {
        Boolean noLoad = (Boolean) comboBox.getClientProperty(DefaultUIInitializer.CLIENT_PROPERTY_LIST_NO_LOAD);
        String propertyName = comboBox.getConfig().getProperty();
        List<R> data;
        if (Objects.equals(true, noLoad) || form == null) {
            if (form != null) {
                return;
            }
            log.debug(String.format("Skip loading of comboBox [%s-%s] (listNoLoad property found or form is null)", dtoClass.getSimpleName(), propertyName));
            data = Collections.emptyList();
        } else {
            Optional<Set<R>> optionalReferenceSetDto = tryToGetDataReferenceSet(propertyName);
            if (optionalReferenceSetDto.isPresent()) {
                Set<R> references = optionalReferenceSetDto.get();
                data = new ArrayList<>(references);
            } else {
                data = Collections.emptyList();
            }
        }
        log.debug(String.format("entity comboBox [%s-%s] : %d", dtoClass.getSimpleName(), propertyName, data.size()));
        comboBox.setData(data);
    }

    public <D extends BusinessDto> void loadExtraReferentialReferenceSetsInModel(Class<D> dtoType) {
        Optional<FormDefinition<D>> optionalFormDefinition = ObserveBusinessProject.get().getOptionalFormDefinition(dtoType);
        optionalFormDefinition.ifPresent(formDefinition -> loadReferentialReferenceSetsInModel(formDefinition, false));
    }
}
