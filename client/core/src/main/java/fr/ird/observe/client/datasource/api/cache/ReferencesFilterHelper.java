package fr.ird.observe.client.datasource.api.cache;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.common.SizeMeasureTypeReference;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created on 12/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ReferencesFilterHelper {

    public static ReferencesFilter<SpeciesReference> newLlSpeciesList(ObserveServicesProvider servicesProvider, Project observeSelectModel, String speciesListId) {
        return (bean, incomingReferences) -> {
            String tripLonglineId = observeSelectModel.getLl().getCommonTrip().getId();
            return servicesProvider.getLlCommonTripService().getSpeciesByListAndTrip(tripLonglineId, speciesListId).toList();
        };
    }

    public static ReferencesFilter<SpeciesReference> newPsSpeciesList(ObserveServicesProvider servicesProvider, Project observeSelectModel, String speciesListId) {
        return (bean, incomingReferences) -> {
            String tripLonglineId = observeSelectModel.getPs().getCommonTrip().getId();
            return servicesProvider.getPsCommonTripService().getSpeciesByListAndTrip(tripLonglineId, speciesListId).toList();
        };
    }
//
//    public static ReferencesFilter<SpeciesReference> newSpeciesList(ObserveServicesProvider servicesProvider, String speciesListId) {
//        return (bean, incomingReferences) -> {
//            Form<SpeciesListDto> speciesListDtoForm = servicesProvider.getReferentialService().loadForm(SpeciesListDto.class, speciesListId);
//            SpeciesListDto speciesListDto = speciesListDtoForm.getObject();
//            Set<String> speciesIds = speciesListDto.getSpecies().stream().map(ReferentialDtoReference::getId).collect(Collectors.toSet());
//            return ToolkitId.filterContains(incomingReferences, speciesIds);
//        };
//    }
//

    public static <R extends ReferentialDtoReference> ReferencesFilter<R> newPredicateList(Predicate<R> predicate) {
        return (bean, incomingReferences) -> incomingReferences.stream().filter(predicate).collect(Collectors.toList());
    }

    public static ReferencesFilter<VesselReference> newSubVesselList(String... ids) {
        return (bean, incomingReferences) -> incomingReferences.stream().filter(e -> {
            List<String> idList = Arrays.asList(ids);
            return idList.contains(e.getVesselType().getId());
        }).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public static <D extends IdDto, R extends DtoReference> ReferencesFilter<R> newSubList(Function<D, Set<String>> ids) {
        return (bean, incomingReferences) -> ToolkitId.filterContains(incomingReferences, ids.apply((D) bean));
    }

    public static ReferencesFilter<SizeMeasureTypeReference> newSubSizeMeasureList(String... ids) {
        return (bean, incomingReferences) -> ToolkitId.filterContains(incomingReferences, ids);
    }
}
