package fr.ird.observe.client.datasource.api.data;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNodeBean;

import java.util.function.BiFunction;
import java.util.stream.Stream;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 09/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class CopyDataTask extends DataTaskSupport {

    private final boolean dataExistOnOpposite;
    private final ToolkitIdLabel data;
    private final String prefix;

    public static Stream<CopyDataTask> of(TaskSide taskSide, SelectionTreeModel treeModel) {
        BiFunction<String, SelectionTreeNodeBean, CopyDataTask> taskFactory = (k, v) -> new CopyDataTask(taskSide, k, v.toLabel(), v.existsOnOtherSide());
        return treeModel.streamOnSelectedDataByParent(k -> k.toLabel().toString(), taskFactory);
    }

    protected CopyDataTask(TaskSide taskSide, String prefix, ToolkitIdLabel data, boolean dataExistOnOpposite) {
        super(taskSide, taskSide.getCopyLabelKey(), taskSide.getCopyIcon());
        this.dataExistOnOpposite = dataExistOnOpposite;
        this.data = data;
        this.prefix = prefix;
    }

    public final ToolkitIdLabel getData() {
        return data;
    }

    public final String getPrefix() {
        return prefix;
    }

    @Override
    public final String getLabel() {
        return t(super.getLabel(), prefix, data);
    }

    @Override
    public int stepCount() {
        return 4;
    }

    public boolean isDataExistOnOpposite() {
        return dataExistOnOpposite;
    }

}
