package fr.ird.observe.client.datasource.api.data;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.usage.UsageForDisplayUIHandler;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.data.DataManagementService;
import fr.ird.observe.services.service.data.DeleteDataRequest;
import fr.ird.observe.services.service.data.DeleteDataResult;
import fr.ird.observe.services.service.data.ExportDataRequest;
import fr.ird.observe.services.service.data.ExportDataResult;
import fr.ird.observe.services.service.data.ImportDataRequest;
import fr.ird.observe.services.service.data.ImportDataResult;
import fr.ird.observe.services.service.data.MissingReferentialRequest;
import fr.ird.observe.services.service.data.MissingReferentialResult;
import fr.ird.observe.services.service.referential.ReferentialIds;
import fr.ird.observe.services.service.referential.ReferentialService;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.SingletonSupplier;
import io.ultreia.java4all.util.TwoSideContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Hi-level object to perform business data management (says: delete some trip, copy some trip, insert missing referential, ...)
 * <p>
 * Created on 10/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
@GenerateTemplate(template = "showMissingReferential.ftl")
public class DataManager {
    private static final Logger log = LogManager.getLogger(DataManager.class);
    /**
     * Data sources by side.
     */
    private final TwoSideContext<TaskSide, ObserveSwingDataSource> dataSources;
    /**
     * {@link DataManagementService} supplier by side.
     */
    private final TwoSideContext<TaskSide, Supplier<DataManagementService>> dataManagementServices;
    /**
     * Progression model.
     */
    private final ProgressionModel progressModel;

    public DataManager(ProgressionModel progressModel, ObserveSwingDataSource leftSource, ObserveSwingDataSource rightSource) {
        this.dataSources = TwoSideContext.of(leftSource, rightSource);
        this.dataManagementServices = dataSources.apply(s -> SingletonSupplier.of(s::getDataManagementService));
        this.progressModel = Objects.requireNonNull(progressModel);
    }

    public String getMessage() {
        return DataManagerTemplate.generate(this);
    }

    public void consume(Collection<? extends DataTaskSupport> tasks) {
        for (DataTaskSupport task : tasks) {
            if (task instanceof CopyDataTask) {
                consume((CopyDataTask) task);
                continue;
            }
            if (task instanceof DeleteDataTask) {
                consume((DeleteDataTask) task);
                continue;
            }
            if (task instanceof InsertMissingReferentialTask) {
                consume((InsertMissingReferentialTask) task);
                continue;
            }
            throw new IllegalStateException(String.format("Can't consume the task: %s", task));
        }
    }

    public TwoSideContext<TaskSide, ObserveSwingDataSource> getDataSources() {
        return dataSources;
    }

    void consume(CopyDataTask task) {
        @SuppressWarnings("resource") ObserveSwingDataSource incomingSource = dataSources.onSameSide(task.getTaskSide());
        @SuppressWarnings("resource") ObserveSwingDataSource targetSource = dataSources.onOppositeSide(task.getTaskSide());
        DataManagementService incomingSourceDataManagementService = dataManagementServices.onSameSide(task.getTaskSide()).get();
        DataManagementService targetSourceDataManagementService = dataManagementServices.onOppositeSide(task.getTaskSide()).get();
        String tripId = task.getData().getTopiaId();
        String incomingSourceLabel = incomingSource.getLabel();
        String targetSourceLabel = targetSource.getLabel();
        progressModel.setMessage(t("observe.ui.datasource.editor.actions.data.copy", targetSourceLabel, task.getPrefix(), task.getData()));
        ExportDataRequest exportDataRequest = new ExportDataRequest(!targetSource.isLocal(), tripId);
        ExportDataResult exportDataResult = incomingSourceDataManagementService.exportData(exportDataRequest);

        String message = sendLogResultMessage(n("observe.ui.datasource.editor.actions.data.export.result"),
                                              incomingSourceLabel, task.getPrefix(), task.getData(), exportDataResult.getTime());
        log.info(message);
        ImportDataRequest importDataRequest = new ImportDataRequest(exportDataResult);
        log.info("Use import script: " + importDataRequest.getSqlContent().getLocation());
        ImportDataResult importDataResult = targetSourceDataManagementService.importData(importDataRequest);

        if (importDataResult.isDeleted()) {
            message = sendLogResultMessage(n("observe.ui.datasource.editor.actions.data.delete.result"), targetSourceLabel, task.getPrefix(), task.getData(), importDataResult.getDeleteTime());
            log.info(message);
        } else {
            progressModel.increments();
        }
        if (importDataResult.isImported()) {
            message = sendLogResultMessage(n("observe.ui.datasource.editor.actions.data.import.result"), targetSourceLabel, task.getPrefix(), task.getData(), importDataResult.getImportTime());
            log.info(message);
        } else {
            progressModel.increments();
        }
        if (targetSource.isLocal()) {
            targetSource.setModified(true);
        }
    }

    void consume(InsertMissingReferentialTask task) {
        @SuppressWarnings("resource") ObserveSwingDataSource incomingSource = dataSources.onSameSide(task.getTaskSide());
        @SuppressWarnings("resource") ObserveSwingDataSource targetSource = dataSources.onOppositeSide(task.getTaskSide());
        progressModel.increments();

        String targetSourceLabel = targetSource.getLabel();

        ReferentialService targetReferentialService = targetSource.getReferentialService();
        ReferentialIds targetSourceReferential = targetReferentialService.getReferentialIds();
        MissingReferentialRequest missingReferentialRequest = MissingReferentialRequest.of(targetSourceReferential.getIds(), task.getIdsToCopy().toArray(new String[0]));
        MissingReferentialResult missingReferentialResult = incomingSource.getRootOpenableService().computeMissingReferential(task.getDataType(), missingReferentialRequest);
        progressModel.increments();

        if (missingReferentialResult == null) {
            progressModel.increments();
            return;
        }

        // there is some referential to add to target source

        Map<Class<? extends ReferentialDto>, Set<ToolkitIdLabel>> usages = incomingSource.getReferentialMap(missingReferentialResult.getMissingIds());

        boolean response = UsageForDisplayUIHandler.showMissingReferential(this.getMessage(), usages, targetSourceLabel);

        if (!response) {
            progressModel.increments();
            throw new UserCancelException();
        }

        StringBuilder parameters = new StringBuilder();

        for (Class<? extends ReferentialDto> key : usages.keySet()) {
            Set<ToolkitIdLabel> references = usages.get(key);
            String type = t(I18nDecoratorHelper.getType(key));
            String labels = references.stream().map(Object::toString).collect(Collectors.joining("\n\t * "));
            parameters.append(t("observe.ui.datasource.editor.actions.data.import.add.missing.referential.for.type", targetSourceLabel, references.size(), type, labels));
            parameters.append("\n");
        }

        progressModel.setMessage(t("observe.ui.datasource.editor.actions.data.import.add.missing.referential", targetSourceLabel, parameters.toString()));

        targetReferentialService.insertMissingReferential(missingReferentialResult.getSqlCode());
    }

    void consume(DeleteDataTask task) {
        @SuppressWarnings("resource") ObserveSwingDataSource targetSource = dataSources.onSameSide(task.getTaskSide());
        DataManagementService targetSourceDataManagementService = dataManagementServices.onSameSide(task.getTaskSide()).get();
        String targetSourceLabel = targetSource.getLabel();
        String dataId = task.getData().getTopiaId();

        progressModel.setMessage(t("observe.ui.datasource.editor.actions.data.delete", targetSourceLabel, task.getPrefix(), task.getData()));
        DeleteDataRequest deleteDataRequest = new DeleteDataRequest(dataId);
        DeleteDataResult deleteDataResult = targetSourceDataManagementService.deleteData(deleteDataRequest);

        String message = sendLogResultMessage(n("observe.ui.datasource.editor.actions.data.delete.result"),
                                              targetSourceLabel, task.getPrefix(), task.getData(), deleteDataResult.getTime());
        log.info(message);

        if (targetSource.isLocal()) {
            targetSource.setModified(true);
        }
    }

    String sendLogResultMessage(String i18nKey,
                                String sourceLabel,
                                String prefix,
                                ToolkitIdLabel data,
                                long time) {
        String timeStr = Strings.convertTime(time);
        String message = Strings.leftPad(timeStr, 20) + " - " + t(i18nKey, sourceLabel, prefix, data);
        progressModel.setMessage(message);
        return message;
    }
}
