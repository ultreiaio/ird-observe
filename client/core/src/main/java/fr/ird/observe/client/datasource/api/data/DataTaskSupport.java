package fr.ird.observe.client.datasource.api.data;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;
import java.util.Objects;

/**
 * Created on 03/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public abstract class DataTaskSupport {

    private final TaskSide taskSide;
    private final String label;
    private final Icon icon;

    protected DataTaskSupport(TaskSide taskSide, String label, Icon iconName) {
        this.taskSide = Objects.requireNonNull(taskSide);
        this.label = Objects.requireNonNull(label);
        this.icon = Objects.requireNonNull(iconName);
    }

    public final TaskSide getTaskSide() {
        return taskSide;
    }

    public final Icon getIcon() {
        return icon;
    }

    public String getLabel() {
        return label;
    }

    public abstract int stepCount();

}
