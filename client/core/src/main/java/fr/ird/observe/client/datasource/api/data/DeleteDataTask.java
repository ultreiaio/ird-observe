package fr.ird.observe.client.datasource.api.data;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNodeBean;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 09/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class DeleteDataTask extends DataTaskSupport {

    private final String prefix;
    private final ToolkitIdLabel data;

    public static Stream<DeleteDataTask> of(TaskSide taskSide, SelectionTreeModel treeModel) {
        BiFunction<String, SelectionTreeNodeBean, DeleteDataTask> taskFactory = (k, v) -> new DeleteDataTask(taskSide, k, v.toLabel());
        return treeModel.streamOnSelectedDataByParent(k -> k.toLabel().toString(), taskFactory);
    }

    protected DeleteDataTask(TaskSide taskSide, String prefix, ToolkitIdLabel data) {
        super(taskSide, taskSide.getDeleteLabelKey(), taskSide.getDeleteIcon());
        this.prefix = Objects.requireNonNull(prefix);
        this.data = Objects.requireNonNull(data);
    }

    @Override
    public int stepCount() {
        return 2;
    }

    @Override
    public String getLabel() {
        return t(super.getLabel(), prefix, data);
    }

    public String getPrefix() {
        return prefix;
    }

    public ToolkitIdLabel getData() {
        return data;
    }
}
