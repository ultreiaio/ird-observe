package fr.ird.observe.client.datasource.api.data;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.RootOpenableDto;
import io.ultreia.java4all.i18n.I18n;

import java.util.Collection;
import java.util.Objects;

/**
 * Created on 12/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class InsertMissingReferentialTask extends DataTaskSupport {

    private final Class<? extends RootOpenableDto> dataType;
    private final Collection<String> idsToCopy;

    public static InsertMissingReferentialTask of(TaskSide taskSide, Class<? extends RootOpenableDto> dataType, Collection<String> idsToCopy) {
        return new InsertMissingReferentialTask(taskSide, dataType, idsToCopy);
    }

    protected InsertMissingReferentialTask(TaskSide taskSide, Class<? extends RootOpenableDto> dataType, Collection<String> idsToCopy) {
        super(taskSide, taskSide.getCopyLabelKey(), taskSide.getCopyIcon());
        this.dataType = dataType;
        this.idsToCopy = Objects.requireNonNull(idsToCopy);
    }

    @Override
    public String getLabel() {
        return I18n.t(super.getLabel());
    }

    @Override
    public int stepCount() {
        return 3;
    }

    public Class<? extends RootOpenableDto> getDataType() {
        return dataType;
    }

    public Collection<String> getIdsToCopy() {
        return idsToCopy;
    }

}
