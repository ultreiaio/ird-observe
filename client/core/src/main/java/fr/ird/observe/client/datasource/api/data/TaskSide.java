package fr.ird.observe.client.datasource.api.data;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.TwoSide;

import javax.swing.Icon;

/**
 * Created on 12/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public enum TaskSide implements TwoSide {
    /**
     * The task begins on left side.
     *
     * <ul>
     *     <li>For a copy, it means copy from left → right.</li>
     *     <li>For a delete, it means delete from left.</li>
     * </ul>
     */
    FROM_LEFT("copyToRight",
              "deleteFromLeft",
              "insertMissingReferentialToRight",
              I18n.n("observe.ui.datasource.editor.actions.data.copyToRight"),
              I18n.n("observe.ui.datasource.editor.actions.data.deleteFromLeft"),
              I18n.n("observe.ui.datasource.editor.actions.data.insertMissingReferentialToRight"),
              I18n.n("observe.ui.datasource.editor.actions.data.copyToRight.tip"),
              I18n.n("observe.ui.datasource.editor.actions.data.deleteFromLeft.tip")
    ),
    /**
     * The task begins on right side.
     *
     * <ul>
     *     <li>For a copy, it means copy from right → left.</li>
     *     <li>For a delete, it means delete from right.</li>
     * </ul>
     */
    FROM_RIGHT("copyToLeft",
               "deleteFromRight",
               "insertMissingReferentialToLeft",
               I18n.n("observe.ui.datasource.editor.actions.data.copyToLeft"),
               I18n.n("observe.ui.datasource.editor.actions.data.deleteFromRight"),
               I18n.n("observe.ui.datasource.editor.actions.data.insertMissingReferentialToLeft"),
               I18n.n("observe.ui.datasource.editor.actions.data.copyToLeft.tip"),
               I18n.n("observe.ui.datasource.editor.actions.data.deleteFromRight.tip")
    );

    private final String copyIconName;
    private final String deleteIconName;
    private final String insertMissingReferentialIconName;

    private final String copyLabelKey;
    private final String deleteLabelKey;
    private final String insertMissingReferentialLabelKey;
    private final String copyTipKey;
    private final String deleteTipKey;

    TaskSide(String copyIconName,
             String deleteIconName,
             String insertMissingReferentialIconName,
             String copyLabelKey,
             String deleteLabelKey,
             String insertMissingReferentialLabelKey,
             String copyTipKey,
             String deleteTipKey) {
        this.copyIconName = copyIconName;
        this.deleteIconName = deleteIconName;
        this.insertMissingReferentialIconName = insertMissingReferentialIconName;
        this.copyLabelKey = copyLabelKey;
        this.deleteLabelKey = deleteLabelKey;
        this.insertMissingReferentialLabelKey = insertMissingReferentialLabelKey;
        this.copyTipKey = copyTipKey;
        this.deleteTipKey = deleteTipKey;
    }

    public String getCopyIconName() {
        return copyIconName;
    }

    public String getDeleteIconName() {
        return deleteIconName;
    }

    public String getInsertMissingReferentialIconName() {
        return insertMissingReferentialIconName;
    }

    public String getCopyLabelKey() {
        return copyLabelKey;
    }

    public String getDeleteLabelKey() {
        return deleteLabelKey;
    }

    public String getInsertMissingReferentialLabelKey() {
        return insertMissingReferentialLabelKey;
    }

    public String getCopyTipKey() {
        return copyTipKey;
    }

    public String getDeleteTipKey() {
        return deleteTipKey;
    }

    @Override
    public boolean onLeft() {
        return TaskSide.FROM_LEFT == this;
    }

    public Icon getCopyIcon() {
        return UIHelper.getUIManagerActionIcon(getCopyIconName());
    }

    public Icon getDeleteIcon() {
        return UIHelper.getUIManagerActionIcon(getDeleteIconName());
    }

    public Icon getInsertMissingReferentialIcon() {
        return UIHelper.getUIManagerActionIcon(getInsertMissingReferentialIconName());
    }
}
