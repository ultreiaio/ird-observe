/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.api.event;

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;

import java.util.EventListener;

/**
 * Le contrat d'un listener sur une source de données.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public interface ObserveSwingDataSourceListener extends EventListener {


    /**
     * Déclanché avant l'ouverture d'un service (i.e au debut de la méthode
     * {@link ObserveSwingDataSource#open()}).
     *
     * @param event l'evenement de pre-ouverture
     */
    void onOpening(ObserveSwingDataSourceEvent event);

    /**
     * Déclanché apres une ouverture d'un service (i.e à la fin de la méthode
     * {@link ObserveSwingDataSource#open()}).
     *
     * @param event l'evenement d'ouverture
     */
    void onOpened(ObserveSwingDataSourceEvent event);

    /**
     * Déclanché avant la fermeture d'un service (i.e au avant la méthode {@link
     * ObserveSwingDataSource#close()}).
     *
     * @param event l'evenement de pre-fermeture
     */
    void onClosing(ObserveSwingDataSourceEvent event);

    /**
     * Déclanché apres la fermeture d'un service (i.e apres de la méthode {@link
     * ObserveSwingDataSource#close()}).
     *
     * @param event l'evenement de fermeture
     */
    void onClosed(ObserveSwingDataSourceEvent event);

    /**
     * Déclanché pour envoyer un nouveau message.
     *
     * @param event l'evenement de message
     */
    void onNewMessage(ObserveSwingDataSourceEvent event);
}
