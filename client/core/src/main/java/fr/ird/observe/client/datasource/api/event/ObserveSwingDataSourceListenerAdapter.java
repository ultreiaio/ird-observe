/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.api.event;

/**
 * Un adapteur de {@link ObserveSwingDataSourceListener}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ObserveSwingDataSourceListenerAdapter implements ObserveSwingDataSourceListener {

    @Override
    public void onOpening(ObserveSwingDataSourceEvent event) {
        // ne fait rien
    }

    @Override
    public void onOpened(ObserveSwingDataSourceEvent event) {
        // ne fait rien
    }

    @Override
    public void onClosing(ObserveSwingDataSourceEvent event) {
        // ne fait rien
    }

    @Override
    public void onClosed(ObserveSwingDataSourceEvent event) {
        // ne fait rien
    }

    @Override
    public void onNewMessage(ObserveSwingDataSourceEvent event) {
        // ne fait rien
    }
}
