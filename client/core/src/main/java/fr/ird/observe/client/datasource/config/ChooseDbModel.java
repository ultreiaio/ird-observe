package fr.ird.observe.client.datasource.config;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.io.File;
import java.util.Objects;

/**
 * Created on 28/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
@GenerateJavaBeanDefinition
public class ChooseDbModel extends AbstractJavaBean implements WithClientUIContextApi {

    public static final String REFERENTIEL_IMPORT_MODE_PROPERTY_NAME = "referentielImportMode";
    public static final String DATA_IMPORT_MODE_PROPERTY_NAME = "dataImportMode";

    public static final String EDIT_LOCAL_CONFIG_PROPERTY_NAME = "editLocalConfig";
    public static final String EDIT_REMOTE_CONFIG_PROPERTY_NAME = "editRemoteConfig";
    public static final String EDIT_SERVER_CONFIG_PROPERTY_NAME = "editServerConfig";
    public static final String CAN_MIGRATE_PROPERTY_NAME = "canMigrate";
    public static final String LOCAL_STORAGE_EXIST_PROPERTY_NAME = "localStorageExist";
    public static final String INITIAL_DB_DUMP_PROPERTY_NAME = "initialDbDump";
    public static final String VALIDATE_PROPERTY_NAME = "validate";
    /**
     * Define usage of a data source.
     * <p>
     * If not null, then use a data source, otherwise create a new data source.
     */
    protected final DataSourceInitModel initModel;
    /**
     * Local data source create mode (for referential import only).
     *
     * @since 3.0
     */
    protected DataSourceCreateMode referentielImportMode;
    /**
     * Local data source create mode (for data import only).
     *
     * @since 3.6
     */
    protected DataSourceCreateMode dataImportMode;
    /**
     * Is local storage exist?
     */
    protected boolean localStorageExist;
    /**
     * Initial db dump (optional).
     */
    protected File initialDbDump;

    /**
     * Is can migrate?
     */
    protected boolean canMigrate;
    /**
     * Is using server config?
     */
    protected boolean editServerConfig;
    /**
     * Is using local config?
     */
    protected boolean editLocalConfig;
    /**
     * Is using remote config?
     */
    protected boolean editRemoteConfig;

    public ChooseDbModel() {
        ClientConfig config = getClientConfig();
        DataSourceConnectMode defaultDataSourceConnectMode = config.getDefaultDataSourceConnectMode();
        DataSourceCreateMode defaultDataSourceCreateMode = config.getDefaultDataSourceCreateMode();
        this.initModel = new DataSourceInitModel();
        initModel.setInitMode(DataSourceInitMode.CONNECT);
        initModel.setConnectMode(defaultDataSourceConnectMode);
        initModel.setCreateMode(defaultDataSourceCreateMode);

        boolean localStorageExist = config.isLocalStorageExist();
        setLocalStorageExist(localStorageExist);
        setInitialDbDump(config.getInitialDbDump());
        if (!localStorageExist) {
            // on force a ne pas pouvoir utiliser la base locale
            initModel.getAuthorizedConnectModes().remove(DataSourceConnectMode.LOCAL);
        }
    }

    public void copyTo(ChooseDbModel dst) {
        dst.localStorageExist = isLocalStorageExist();
        getInitModel().copyTo(dst.initModel);
        dst.initialDbDump = getInitialDbDump();
        dst.dataImportMode = getDataImportMode();
        dst.referentielImportMode = getReferentielImportMode();
        dst.editLocalConfig = isEditLocalConfig();
        dst.editRemoteConfig = isEditRemoteConfig();
        dst.editServerConfig = isEditServerConfig();
        dst.canMigrate = isCanMigrate();
    }

    public void start() {

        firePropertyChange(CAN_MIGRATE_PROPERTY_NAME, isCanMigrate());
        firePropertyChange(LOCAL_STORAGE_EXIST_PROPERTY_NAME, isLocalStorageExist());
        firePropertyChange(INITIAL_DB_DUMP_PROPERTY_NAME, getInitialDbDump());

        getInitModel().start();

        firePropertyChange(EDIT_REMOTE_CONFIG_PROPERTY_NAME, isEditRemoteConfig());
        firePropertyChange(EDIT_SERVER_CONFIG_PROPERTY_NAME, isEditServerConfig());
        firePropertyChange(EDIT_LOCAL_CONFIG_PROPERTY_NAME, isEditLocalConfig());

    }

    public DataSourceInitModel getInitModel() {
        return initModel;
    }

    public boolean isCanMigrate() {
        return canMigrate;
    }

    public DataSourceCreateMode getReferentielImportMode() {
        return referentielImportMode;
    }

    public DataSourceCreateMode getDataImportMode() {
        return dataImportMode;
    }

    public boolean isEditLocalConfig() {
        return editLocalConfig;
    }

    public boolean isEditRemoteConfig() {
        return editRemoteConfig;
    }

    public boolean isEditServerConfig() {
        return editServerConfig;
    }

    public boolean isLocalStorageExist() {
        return localStorageExist;
    }

    public File getInitialDbDump() {
        return initialDbDump;
    }

    public boolean isImportReferential() {
        return referentielImportMode != null && DataSourceCreateMode.EMPTY != referentielImportMode;
    }

    public boolean isImportData() {
        return dataImportMode != null && DataSourceCreateMode.EMPTY != dataImportMode;
    }

    public void setReferentielImportMode(DataSourceCreateMode referentielImportMode) {
        DataSourceCreateMode oldValue = getReferentielImportMode();
        if (Objects.equals(oldValue, referentielImportMode)) {
            return;
        }
        this.referentielImportMode = referentielImportMode;
        firePropertyChange(REFERENTIEL_IMPORT_MODE_PROPERTY_NAME, oldValue, referentielImportMode);
    }

    public void setDataImportMode(DataSourceCreateMode dataImportMode) {
        DataSourceCreateMode oldValue = getDataImportMode();
        if (Objects.equals(oldValue, dataImportMode)) {
            return;
        }
        this.dataImportMode = dataImportMode;
        firePropertyChange(DATA_IMPORT_MODE_PROPERTY_NAME, oldValue, dataImportMode);
    }

    public void setCanMigrate(boolean canMigrate) {
        boolean oldValue = isCanMigrate();
        this.canMigrate = canMigrate;
        firePropertyChange(CAN_MIGRATE_PROPERTY_NAME, oldValue, canMigrate);
    }

    public void setLocalStorageExist(boolean localStorageExist) {
        boolean oldValue = isLocalStorageExist();
        if (Objects.equals(oldValue, localStorageExist)) {
            return;
        }
        this.localStorageExist = localStorageExist;
        firePropertyChange(LOCAL_STORAGE_EXIST_PROPERTY_NAME, oldValue, localStorageExist);
    }

    public void setInitialDbDump(File initialDbDump) {
        File oldValue = getInitialDbDump();
        this.initialDbDump = initialDbDump;
        firePropertyChange(INITIAL_DB_DUMP_PROPERTY_NAME, oldValue, initialDbDump);
    }

    public DataSourceInitMode getMode() {
        return initModel.getInitMode();
    }

    public boolean doImportReferential() {
        return referentielImportMode != null && DataSourceCreateMode.EMPTY != referentielImportMode;
    }

    public boolean doImportData() {
        return dataImportMode != null && DataSourceCreateMode.EMPTY != dataImportMode;
    }

    public void initModes(ClientConfig config) {
        adaptInitModel(config);
        updateEditConfig();
    }

    protected boolean computeCanMigrate() {
        if (initModel.isOnConnectMode() && initModel.isOnConnectModeLocal()) {
            // using local database, always accept migration
            return true;
        }
        if (initModel.isOnCreateMode()) {
            // while creating local db, authorize migration of imported dump
            return initModel.isOnCreateModeImportExternalDump() || initModel.isOnCreateModeImportInternalDump();
        }
        // all other cases does not accept any migration
        return false;
    }

    protected void adaptInitModel(ClientConfig config) {
    }

    public void setInitMode(DataSourceInitMode initMode) {
        getInitModel().setInitMode(initMode);
        updateEditConfig();
    }

    public void setConnectMode(DataSourceConnectMode connectMode) {
        getInitModel().setConnectMode(connectMode);
        updateEditConfig();
    }

    public void setCreateMode(DataSourceCreateMode createMode) {
        getInitModel().setCreateMode(createMode);
        updateEditConfig();
    }

    public void updateEditConfig() {

        boolean oldValueRemote = isEditRemoteConfig();
        boolean oldValueServer = isEditServerConfig();
        boolean oldValueLocal = isEditLocalConfig();
        boolean oldValueCanMigrate = isCanMigrate();

        editLocalConfig = initModel.isOnConnectModeLocal();
        firePropertyChange(EDIT_LOCAL_CONFIG_PROPERTY_NAME, oldValueLocal, editLocalConfig);

        editRemoteConfig = initModel.isOnConnectModeRemote() || initModel.isOnCreateMode() && initModel.isOnCreateModeImportRemoteStorage();
        firePropertyChange(EDIT_REMOTE_CONFIG_PROPERTY_NAME, oldValueRemote, editRemoteConfig);

        editServerConfig = initModel.isOnConnectModeServer() || initModel.isOnCreateMode() && initModel.isOnCreateModeImportServerStorage();
        firePropertyChange(EDIT_SERVER_CONFIG_PROPERTY_NAME, oldValueServer, editServerConfig);

        this.canMigrate = computeCanMigrate();
        firePropertyChange(CAN_MIGRATE_PROPERTY_NAME, oldValueCanMigrate, canMigrate);
    }

    public boolean validate() {
        return initModel.isValid();
    }
}
