package fr.ird.observe.client.datasource.config;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;

import javax.swing.Icon;
import javax.swing.UIManager;

/**
 * Describes status of a connexion.
 * <p>
 * Created on 01/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.6
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_LABEL_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_DESCRIPTION_PATTERN)
})
public enum ConnexionStatus {
    /**
     * When connexion is untested, or was modified since last check.
     */
    UNTESTED,
    /**
     * When connexion is successful.
     */
    SUCCESS,
    /**
     * When connexion failed.
     */
    FAILED;

    private final String iconPath;
    private Icon icon;

    ConnexionStatus() {
        this.iconPath = "action.connexion-status-" + name().toLowerCase();
    }

    public Icon getIcon() {
        if (icon == null) {
            icon = UIManager.getIcon(iconPath);
        }
        return icon;
    }

    public String getLabel() {
        return ConnexionStatusI18n.getLabel(this);
    }

    public String getDescription() {
        return ConnexionStatusI18n.getDescription(this);
    }

}
