package fr.ird.observe.client.datasource.config;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.EnumSet;
import java.util.Objects;

/**
 * Describes a data source init mode, says :
 * <ul>
 *  <li>should we connect to a data source? if so of which type?
 * <li>should we create a new data source? if so from what?
 * </ul>
 * Created on 01/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.6
 */
@GenerateTemplate(template = "DataSourceInitModel.ftl")
@GenerateJavaBeanDefinition
public class DataSourceInitModel extends AbstractJavaBean {

    public static final String INIT_MODE_PROPERTY_NAME = "initMode";
    public static final String CONNECT_MODE_PROPERTY_NAME = "connectMode";
    public static final String CREATE_MODE_PROPERTY_NAME = "createMode";
    public static final String DESCRIPTION_PROPERTY_NAME = "description";
    public static final String ON_CONNECT_MODE_PROPERTY_NAME = "onConnectMode";
    public static final String ON_CONNECT_MODE_LOCAL_PROPERTY_NAME = "onConnectModeLocal";
    public static final String ON_CONNECT_MODE_REMOTE_PROPERTY_NAME = "onConnectModeRemote";
    public static final String ON_CONNECT_MODE_SERVER_PROPERTY_NAME = "onConnectModeServer";
    public static final String ON_CREATE_MODE_PROPERTY_NAME = "onCreateMode";
    public static final String ON_CREATE_MODE_EMPTY_PROPERTY_NAME = "onCreateModeEmpty";
    public static final String ON_CREATE_MODE_IMPORT_EXTERNAL_DUMP_PROPERTY_NAME = "onCreateModeImportExternalDump";
    public static final String ON_CREATE_MODE_IMPORT_INTERNAL_DUMP_PROPERTY_NAME = "onCreateModeImportInternalDump";
    public static final String ON_CREATE_MODE_IMPORT_LOCAL_STORAGE_PROPERTY_NAME = "onCreateModeImportLocalStorage";
    public static final String ON_CREATE_MODE_IMPORT_REMOTE_STORAGE_PROPERTY_NAME = "onCreateModeImportRemoteStorage";
    public static final String ON_CREATE_MODE_IMPORT_SERVER_STORAGE_PROPERTY_NAME = "onCreateModeImportServerStorage";
    public static final String CAN_CONNECT_MODE_PROPERTY_NAME = "canConnectMode";
    public static final String CAN_CONNECT_MODE_LOCAL_PROPERTY_NAME = "canConnectModeLocal";
    public static final String CAN_CONNECT_MODE_REMOTE_PROPERTY_NAME = "canConnectModeRemote";
    public static final String CAN_CONNECT_MODE_SERVER_PROPERTY_NAME = "canConnectModeServer";
    public static final String CAN_CREATE_MODE_PROPERTY_NAME = "canCreateMode";
    public static final String VALID_PROPERTY_NAME = "valid";
    private EnumSet<DataSourceInitMode> authorizedInitModes;
    private EnumSet<DataSourceConnectMode> authorizedConnectModes;
    private EnumSet<DataSourceCreateMode> authorizedCreateModes;
    /**
     * Init mode to use.
     */
    private DataSourceInitMode initMode;
    /**
     * Connect mode to use if {@link #initMode} equals {@link DataSourceInitMode#CONNECT}.
     */
    private DataSourceConnectMode connectMode;
    /**
     * Create mode to use if {@link #initMode} equals {@link DataSourceInitMode#CREATE}.
     */
    private DataSourceCreateMode createMode;
    /**
     * Computed description of the model, once if is valid.
     */
    private transient String description;

    /**
     * Internal state set to {@code true} when {@link #start()} method is invoked.
     */
    private transient boolean init = false;

    public DataSourceInitModel() {
        // by default authorized all init modes.
        this.authorizedInitModes = EnumSet.allOf(DataSourceInitMode.class);
        // by default authorized all connect modes.
        this.authorizedConnectModes = EnumSet.allOf(DataSourceConnectMode.class);
        // by default authorized all create modes.
        this.authorizedCreateModes = EnumSet.allOf(DataSourceCreateMode.class);
    }

    public boolean isValid() {
        if (initMode == null || !authorizedInitModes.contains(initMode)) {
            return false;
        }
        switch (initMode) {
            case CONNECT:
                return connectMode != null && authorizedConnectModes.contains(connectMode);
            case CREATE:
                return createMode != null && authorizedCreateModes.contains(createMode);
            default:
                return false;
        }
    }

    public void copyTo(DataSourceInitModel dst) {
        dst.initMode = getInitMode();
        dst.connectMode = getConnectMode();
        dst.createMode = getCreateMode();
        dst.authorizedInitModes = getAuthorizedInitModes();
        dst.authorizedConnectModes = getAuthorizedConnectModes();
        dst.authorizedCreateModes = getAuthorizedCreateModes();
        dst.description = null;
    }

    public void start() {
        init = true;
        firePropertyChange(INIT_MODE_PROPERTY_NAME, getInitMode());
        firePropertyChange(ON_CONNECT_MODE_PROPERTY_NAME, isOnConnectMode());
        firePropertyChange(ON_CREATE_MODE_PROPERTY_NAME, isOnCreateMode());

        firePropertyChange(CONNECT_MODE_PROPERTY_NAME, getConnectMode());
        firePropertyChange(ON_CONNECT_MODE_LOCAL_PROPERTY_NAME, isOnConnectModeLocal());
        firePropertyChange(ON_CONNECT_MODE_REMOTE_PROPERTY_NAME, isOnConnectModeRemote());
        firePropertyChange(ON_CONNECT_MODE_SERVER_PROPERTY_NAME, isOnConnectModeServer());

        firePropertyChange(CREATE_MODE_PROPERTY_NAME, getCreateMode());
        firePropertyChange(ON_CREATE_MODE_EMPTY_PROPERTY_NAME, isOnCreateModeEmpty());
        firePropertyChange(ON_CREATE_MODE_IMPORT_INTERNAL_DUMP_PROPERTY_NAME, isOnCreateModeImportInternalDump());
        firePropertyChange(ON_CREATE_MODE_IMPORT_EXTERNAL_DUMP_PROPERTY_NAME, isOnCreateModeImportExternalDump());
        firePropertyChange(ON_CREATE_MODE_IMPORT_LOCAL_STORAGE_PROPERTY_NAME, isOnCreateModeImportLocalStorage());
        firePropertyChange(ON_CREATE_MODE_IMPORT_REMOTE_STORAGE_PROPERTY_NAME, isOnCreateModeImportRemoteStorage());
        firePropertyChange(ON_CREATE_MODE_IMPORT_SERVER_STORAGE_PROPERTY_NAME, isOnCreateModeImportServerStorage());

        firePropertyChange(CAN_CONNECT_MODE_PROPERTY_NAME, isCanConnectMode());
        firePropertyChange(CAN_CONNECT_MODE_LOCAL_PROPERTY_NAME, isCanConnectModeLocal());
        firePropertyChange(CAN_CONNECT_MODE_REMOTE_PROPERTY_NAME, isCanConnectModeRemote());
        firePropertyChange(CAN_CONNECT_MODE_SERVER_PROPERTY_NAME, isCanConnectModeServer());
        firePropertyChange(CAN_CREATE_MODE_PROPERTY_NAME, isCanCreateMode());
        fireValid();
        fireDescription();
    }

    public boolean isCanConnectMode() {
        return authorizedInitModes.contains(DataSourceInitMode.CONNECT);
    }

    public boolean isCanCreateMode() {
        return authorizedInitModes.contains(DataSourceInitMode.CREATE);
    }

    public boolean isCanConnectModeLocal() {
        return authorizedConnectModes.contains(DataSourceConnectMode.LOCAL);
    }

    public boolean isCanConnectModeRemote() {
        return authorizedConnectModes.contains(DataSourceConnectMode.REMOTE);
    }

    public boolean isCanConnectModeServer() {
        return authorizedConnectModes.contains(DataSourceConnectMode.SERVER);
    }

    public EnumSet<DataSourceInitMode> getAuthorizedInitModes() {
        return authorizedInitModes;
    }

    public void setAuthorizedInitModes(EnumSet<DataSourceInitMode> authorizedInitModes) {
        this.authorizedInitModes = Objects.requireNonNull(authorizedInitModes);
    }

    public EnumSet<DataSourceConnectMode> getAuthorizedConnectModes() {
        return authorizedConnectModes;
    }

    public void setAuthorizedConnectModes(EnumSet<DataSourceConnectMode> authorizedConnectModes) {
        this.authorizedConnectModes = Objects.requireNonNull(authorizedConnectModes);
    }

    public EnumSet<DataSourceCreateMode> getAuthorizedCreateModes() {
        return authorizedCreateModes;
    }

    public void setAuthorizedCreateModes(EnumSet<DataSourceCreateMode> authorizedCreateModes) {
        this.authorizedCreateModes = Objects.requireNonNull(authorizedCreateModes);
    }

    public DataSourceInitMode getInitMode() {
        return initMode;
    }

    public void setInitMode(DataSourceInitMode initMode) {
        DataSourceInitMode oldValue = getInitMode();
        this.initMode = initMode;
        if (!init) {
            return;
        }
        firePropertyChange(INIT_MODE_PROPERTY_NAME, oldValue, initMode);
        firePropertyChange(ON_CONNECT_MODE_PROPERTY_NAME, oldValue == DataSourceInitMode.CONNECT, isOnConnectMode());
        firePropertyChange(ON_CREATE_MODE_PROPERTY_NAME, oldValue == DataSourceInitMode.CREATE, isOnCreateMode());
        fireValid();
        fireDescription();
    }

    public DataSourceConnectMode getConnectMode() {
        return connectMode;
    }

    public void setConnectMode(DataSourceConnectMode connectMode) {
        DataSourceConnectMode oldValue = getConnectMode();
        this.connectMode = connectMode;
        if (!init) {
            return;
        }
        firePropertyChange(CONNECT_MODE_PROPERTY_NAME, oldValue, connectMode);
        firePropertyChange(ON_CONNECT_MODE_LOCAL_PROPERTY_NAME, oldValue == DataSourceConnectMode.LOCAL, isOnConnectModeLocal());
        firePropertyChange(ON_CONNECT_MODE_REMOTE_PROPERTY_NAME, oldValue == DataSourceConnectMode.REMOTE, isOnConnectModeRemote());
        firePropertyChange(ON_CONNECT_MODE_SERVER_PROPERTY_NAME, oldValue == DataSourceConnectMode.SERVER, isOnConnectModeServer());
        fireValid();
        fireDescription();
    }

    public DataSourceCreateMode getCreateMode() {
        return createMode;
    }

    public void setCreateMode(DataSourceCreateMode createMode) {
        DataSourceCreateMode oldValue = getCreateMode();
        this.createMode = createMode;
        if (!init) {
            return;
        }
        firePropertyChange(CREATE_MODE_PROPERTY_NAME, oldValue, createMode);
        firePropertyChange(ON_CREATE_MODE_EMPTY_PROPERTY_NAME, oldValue == DataSourceCreateMode.EMPTY, isOnCreateModeEmpty());
        firePropertyChange(ON_CREATE_MODE_IMPORT_INTERNAL_DUMP_PROPERTY_NAME, oldValue == DataSourceCreateMode.IMPORT_INTERNAL_DUMP, isOnCreateModeImportInternalDump());
        firePropertyChange(ON_CREATE_MODE_IMPORT_EXTERNAL_DUMP_PROPERTY_NAME, oldValue == DataSourceCreateMode.IMPORT_EXTERNAL_DUMP, isOnCreateModeImportExternalDump());
        firePropertyChange(ON_CREATE_MODE_IMPORT_LOCAL_STORAGE_PROPERTY_NAME, oldValue == DataSourceCreateMode.IMPORT_LOCAL_STORAGE, isOnCreateModeImportLocalStorage());
        firePropertyChange(ON_CREATE_MODE_IMPORT_REMOTE_STORAGE_PROPERTY_NAME, oldValue == DataSourceCreateMode.IMPORT_REMOTE_STORAGE, isOnCreateModeImportRemoteStorage());
        firePropertyChange(ON_CREATE_MODE_IMPORT_SERVER_STORAGE_PROPERTY_NAME, oldValue == DataSourceCreateMode.IMPORT_SERVER_STORAGE, isOnCreateModeImportServerStorage());
        fireValid();
        fireDescription();
    }

    public String getDescription() {
        if (init && description == null) {
            description = DataSourceInitModelTemplate.generate(this);
        }
        return description;

    }

    public boolean isOnConnectMode() {
        return Objects.equals(initMode, DataSourceInitMode.CONNECT);
    }

    public boolean isOnConnectModeLocal() {
        return Objects.equals(connectMode, DataSourceConnectMode.LOCAL);
    }

    public boolean isOnConnectModeRemote() {
        return Objects.equals(connectMode, DataSourceConnectMode.REMOTE);
    }

    public boolean isOnConnectModeServer() {
        return Objects.equals(connectMode, DataSourceConnectMode.SERVER);
    }

    public boolean isOnCreateMode() {
        return Objects.equals(initMode, DataSourceInitMode.CREATE);
    }

    public boolean isOnCreateModeImportServerStorage() {
        return Objects.equals(createMode, DataSourceCreateMode.IMPORT_SERVER_STORAGE);
    }

    public boolean isOnCreateModeImportRemoteStorage() {
        return Objects.equals(createMode, DataSourceCreateMode.IMPORT_REMOTE_STORAGE);
    }

    public boolean isOnCreateModeImportLocalStorage() {
        return Objects.equals(createMode, DataSourceCreateMode.IMPORT_LOCAL_STORAGE);
    }

    public boolean isOnCreateModeImportExternalDump() {
        return Objects.equals(createMode, DataSourceCreateMode.IMPORT_EXTERNAL_DUMP);
    }

    public boolean isOnCreateModeImportInternalDump() {
        return Objects.equals(createMode, DataSourceCreateMode.IMPORT_INTERNAL_DUMP);
    }

    public boolean isOnCreateModeEmpty() {
        return Objects.equals(createMode, DataSourceCreateMode.EMPTY);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataSourceInitModel)) return false;
        DataSourceInitModel that = (DataSourceInitModel) o;
        return Objects.equals(initMode, that.initMode) && Objects.equals(connectMode, that.connectMode) && Objects.equals(createMode, that.createMode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(initMode, connectMode, createMode);
    }

    protected void fireDescription() {
        String oldDescription = getDescription();
        description = null;
        firePropertyChange(DESCRIPTION_PROPERTY_NAME, oldDescription, getDescription());
    }

    protected void fireValid() {
        firePropertyChange(VALID_PROPERTY_NAME, isValid());
    }
}
