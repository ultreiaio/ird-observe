package fr.ird.observe.client.datasource.config.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.RemoteConfig;
import fr.ird.observe.client.datasource.config.form.RemoteConfigurationModel;
import fr.ird.observe.dto.presets.RemoteDataSourceConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 24/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
public class RemoteConfigSaveConfiguration extends SaveConfigurationSupport<RemoteConfig> {
    private static final Logger log = LogManager.getLogger(RemoteConfigSaveConfiguration.class);

    @Override
    protected void doActionPerformed(ActionEvent e, RemoteConfig ui) {

        RemoteConfigurationModel model = ui.getModel();

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0, 2));

        JTextField question = new JTextField();
        panel.add(new JLabel(t("observe.ui.datasource.storage.remote.configuration.name")));
        panel.add(question);

        panel.add(new JLabel(t("observe.ui.datasource.storage.remote.url")));
        JTextField url = new JTextField(model.getJdbcUrl());
        url.setEditable(false);
        panel.add(url);

        panel.add(new JLabel(t("observe.ui.datasource.storage.remote.login")));
        JTextField login = new JTextField(model.getUsername());
        login.setEditable(false);
        panel.add(login);

        panel.add(new JLabel(t("observe.ui.datasource.storage.remote.password")));
        JTextField password = new JTextField(new String(model.getPassword()));
        password.setEditable(false);
        panel.add(password);

        JCheckBox databaseName = new JCheckBox(t("observe.ui.datasource.storage.remote.useSll"), model.isUseSsl());
        databaseName.setEnabled(false);
        panel.add(databaseName);

        question.setPreferredSize(new Dimension(300, 30));
        int response = askToUser(ui, t("observe.ui.datasource.storage.remote.configuration.save.title"), panel, JOptionPane.QUESTION_MESSAGE, new String[]{
                t("observe.ui.action.save"),
                t("observe.ui.action.cancel")
        }, 0);
        String configurationName = question.getText().trim();

        if (response != 0 || configurationName.isEmpty()) {
            log.info("User skip save of configuration");
            return;
        }
        log.info("Will add remote configuration: " + configurationName);

        RemoteDataSourceConfiguration configuration = ui.getModel().toPreset(configurationName);
        getClientConfig().addRemoteDataSourceConfiguration(configuration);
        RemoteConfigToggleConfigurations.addConfiguration(ui, configuration, ui.getConfigurationsPopup().getSubElements().length + 2);
    }
}
