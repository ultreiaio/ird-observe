package fr.ird.observe.client.datasource.config.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.RemoteConfig;
import fr.ird.observe.dto.presets.RemoteDataSourceConfiguration;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Created on 23/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
public class RemoteConfigToggleConfigurations extends ToggleConfigurationsSupport<RemoteConfig> {

    public static void addConfiguration(RemoteConfig ui, RemoteDataSourceConfiguration configuration, int position) {
        RemoteConfigUseConfiguration action = new RemoteConfigUseConfiguration(RemoteConfigUseConfiguration.class.getName() + position, configuration);
        JMenuItem editor = new JMenuItem();
        RemoteConfigUseConfiguration.init(ui, editor, action);
    }

    public RemoteConfigToggleConfigurations() {
        super(I18n.t("observe.ui.datasource.storage.remoteConfiguration.presets"));
    }

    @Override
    protected void preparePopup(JPopupMenu menu) {
        int position = 0;
        menu.removeAll();
        for (RemoteDataSourceConfiguration configuration : getClientConfig().getRemoteDataSourceConfigurationList()) {
            addConfiguration(ui, configuration, position++);
        }
    }
}
