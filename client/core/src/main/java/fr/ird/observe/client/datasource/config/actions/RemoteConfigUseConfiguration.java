package fr.ird.observe.client.datasource.config.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.RemoteConfig;
import fr.ird.observe.client.datasource.config.form.RemoteConfigurationModel;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.dto.presets.RemoteDataSourceConfiguration;

import java.util.Objects;

/**
 * Created on 23/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
public class RemoteConfigUseConfiguration extends UseConfigurationSupport<RemoteConfig> {
    private final RemoteDataSourceConfiguration configuration;

    public RemoteConfigUseConfiguration(String name, RemoteDataSourceConfiguration configuration) {
        super(name, configuration.getName());
        this.configuration = Objects.requireNonNull(configuration);
        setIcon(DataSourceConnectMode.REMOTE.getIcon());
    }

    @Override
    public void toModel(RemoteConfig ui) {
        RemoteConfigurationModel model = ui.getModel();
        model.fromPreset(configuration);
    }
}
