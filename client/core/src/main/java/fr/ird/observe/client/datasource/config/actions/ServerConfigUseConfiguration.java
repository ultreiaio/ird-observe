package fr.ird.observe.client.datasource.config.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.ServerConfig;
import fr.ird.observe.client.datasource.config.form.ServerConfigurationModel;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.dto.presets.ServerDataSourceConfiguration;

/**
 * Created on 18/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ServerConfigUseConfiguration extends UseConfigurationSupport<ServerConfig> {
    private final ServerDataSourceConfiguration configuration;

    public ServerConfigUseConfiguration(String name, ServerDataSourceConfiguration configuration) {
        super(name, configuration.getName());
        this.configuration = configuration;
        setIcon(DataSourceConnectMode.SERVER.getIcon());
    }

    @Override
    public void toModel(ServerConfig ui) {
        ServerConfigurationModel model = ui.getModel();
        model.fromPreset(configuration);
    }
}
