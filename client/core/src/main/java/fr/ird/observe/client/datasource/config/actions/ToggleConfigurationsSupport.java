package fr.ird.observe.client.datasource.config.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.ConfigSupport;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.event.ActionEvent;

/**
 * Created on 23/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
public abstract class ToggleConfigurationsSupport<U extends ConfigSupport> extends ConfigSupportAction<U> {

    public ToggleConfigurationsSupport(String label) {
        super(label, label, "add-preset", ObserveKeyStrokesSupport.KEY_STROKE_CONFIGURE_TOGGLE_CONFIGURATIONS);
    }

    protected abstract void preparePopup(JPopupMenu menu);

    @Override
    protected ActionMap getActionMap(U ui) {
        return ui.getActionMap();
    }

    @Override
    protected InputMap getInputMap(U u, int inputMapCondition) {
        return super.getInputMap(u, inputMapCondition);
    }

    @Override
    protected int getInputMapCondition() {
        return JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT;
    }

    @Override
    public void init() {
        super.init();
        preparePopup(ui.getConfigurationsPopup());
        ui.getConfigurationsPopup().addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                editor.setSelected(false);
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
                editor.setSelected(false);
            }
        });
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        editor.setSelected(true);
        SwingUtilities.invokeLater(() -> {
            JComponent c = ui.getToggleConfigurations();
            JPopupMenu p = ui.getConfigurationsPopup();
            MenuAction.preparePopup(p, c, false);
        });
    }

}
