package fr.ird.observe.client.datasource.config.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.config.ConfigSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created on 23/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
public abstract class UseConfigurationSupport<U extends ConfigSupport> extends ConfigSupportAction<U> implements WithClientUIContextApi, PresetMenuAction {

    private static final Logger log = LogManager.getLogger(UseConfigurationSupport.class);

    public UseConfigurationSupport(String name, String label) {
        super(name, label, label, null, null);
    }

    public abstract void toModel(U ui);

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {

        if (!((JMenuItem) getEditor()).isArmed()) {
            return;
        }

        toModel(ui);

        SwingUtilities.invokeLater(() -> {
            boolean testConnexion = ui.getModel().testConnexion(getDataSourceFunction());
            log.info(String.format("Test connexion result: %s", testConnexion));
            ui.getModel().fireUsePreset(testConnexion);
        });
    }
}
