package fr.ird.observe.client.datasource.config.form;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.config.ConnexionStatus;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.services.service.AnonymousService;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.util.Version;

import javax.swing.Icon;
import java.awt.Color;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 24/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
@GenerateTemplate(template = "ConfigurationModel.ftl")
public abstract class ConfigurationModel extends AbstractJavaBean implements WithClientUIContextApi {
    public static final String PASSWORD_PROPERTY_NAME = "password";
    public static final String LABEL_PROPERTY_NAME = "label";
    public static final String CONNEXION_STATUS_PROPERTY_NAME = "connexionStatus";
    public static final String DATASOURCE_INFORMATION_PROPERTY_NAME = "dataSourceInformation";
    public static final String USE_PRESET_PROPERTY_NAME = "usePreset";
    public static final String CAN_MIGRATE_PROPERTY_NAME = "canMigrate";
    public static final String CONNEXION_STATUS_ERROR_PROPERTY_NAME = "connexionStatusError";
    public static final String CONNEXION_ERROR_PROPERTY_NAME = "connexionError";
    public static final String CONNEXION_UNTESTED_PROPERTY_NAME = "connexionUntested";
    public static final String CONNEXION_FAILED_PROPERTY_NAME = "connexionFailed";
    public static final String CONNEXION_SUCCESS_PROPERTY_NAME = "connexionSuccess";
    public static final String CONNEXION_STATUS_LABEL_PROPERTY_NAME = "connexionStatusLabel";
    public static final String CONNEXION_STATUS_TEXT_PROPERTY_NAME = "connexionStatusText";
    public static final String CONNEXION_STATUS_ICON_PROPERTY_NAME = "connexionStatusIcon";
    public static final String CONNEXION_STATUS_COLOR_PROPERTY_NAME = "connexionStatusColor";
    public static final String CAN_CREATE_DATABASE_PROPERTY_NAME = "canCreateDatabase";
    public static final String SYNTAX_VALID_PROPERTY_NAME = "syntaxValid";

    /**
     * Delegate config.
     */
    private final ObserveDataSourceConfiguration configuration;
    /**
     * Last connexion status text.
     */
    protected String connexionStatusText;
    /**
     * Connexion status when a test remote was run.
     */
    private ConnexionStatus connexionStatus = ConnexionStatus.UNTESTED;
    /**
     * Connexion status error (if any!).
     */
    private String connexionStatusError;
    /**
     * Connexion status error (if any!).
     */
    private Throwable connexionError;
    /**
     * Data source information.
     **/
    private ObserveDataSourceInformation dataSourceInformation;
    /**
     * Can migrate?
     */
    private boolean canMigrate;
    /**
     * Can create database?
     */
    private boolean canCreateDatabase;
    /**
     * Is configuration syntax is valid?
     */
    private boolean syntaxValid;
    /**
     * FIXME Add this in DataSourceConnectMode
     * Cache of colors indexed by connexion status.
     */
    private Map<ConnexionStatus, Color> colors;

    protected ConfigurationModel(ObserveDataSourceConfiguration configuration) {
        this.configuration = Objects.requireNonNull(configuration);
    }

    protected abstract boolean testSyntax();

    public void fromConfig(ObserveDataSourceConfiguration config) {
        String oldLabel = getLabel();
        getConfiguration().setLabel(config == null ? null : config.getLabel());
        getConfiguration().setModelVersion(config == null ? null : config.getModelVersion());
        firePropertyChange(LABEL_PROPERTY_NAME, oldLabel, getLabel());
    }

    public DataSourceConnectMode getDataSourceType() {
        return configuration.getConnectMode();
    }

    /**
     * @param dataSourceFunction to get new data source
     * @return {@code true} if connexion was tested and is valid.
     */
    public final boolean testConnexion(Function<ObserveDataSourceConfiguration, ObserveSwingDataSource> dataSourceFunction) {
        clearStatus();
        if (!isSyntaxValid() || isConnexionFailed()) {
            return false;
        }
        ObserveSwingDataSource dataSource = dataSourceFunction.apply(getConfiguration());
        try {
            ObserveDataSourceInformation dataSourceInformation = test(dataSource);
            testDataSourceInformation(dataSourceInformation);
            if (!isConnexionFailed()) {
                setConnexionStatus(ConnexionStatus.SUCCESS);
            }
        } catch (Throwable e) {
            onConnexionError(e);
        } finally {
            if (dataSource.isOpen()) {
                dataSource.close();
            }
        }
        return isConnexionSuccess();
    }

    public boolean isSyntaxValid() {
        return syntaxValid;
    }

    public void setSyntaxValid(boolean syntaxValid) {
        boolean oldValue = isSyntaxValid();
        this.syntaxValid = syntaxValid;
        firePropertyChange(SYNTAX_VALID_PROPERTY_NAME, oldValue, syntaxValid);
    }

    public boolean isConnexionSuccess() {
        return getConnexionStatus() == ConnexionStatus.SUCCESS;
    }

    public boolean isConnexionFailed() {
        return getConnexionStatus() == ConnexionStatus.FAILED;
    }

    public boolean isConnexionUntested() {
        return getConnexionStatus() == ConnexionStatus.UNTESTED;
    }

    public String getLabel() {
        return getConfiguration().getLabel();
    }

    public Version getModelVersion() {
        return getClientConfig().getModelVersion();
    }

    public Color getConnexionStatusColor() {
        return connexionStatus == null ? null : getColors().get(connexionStatus);
    }

    public Icon getConnexionStatusIcon() {
        return connexionStatus == null ? null : connexionStatus.getIcon();
    }

    public String getConnexionStatusLabel() {
        return connexionStatus == null ? null : connexionStatus.getLabel();
    }

    public ConnexionStatus getConnexionStatus() {
        return connexionStatus;
    }

    public String getConnexionStatusText() {
        if (connexionStatusText == null) {
            try {
                this.connexionStatusText = ConfigurationModelTemplate.generate(this);
            } catch (Exception e) {
                StringWriter out = new StringWriter(4048);
                try (PrintWriter writer = new PrintWriter(out)) {
                    e.printStackTrace(writer);
                    out.flush();
                }
                connexionStatusText = out.toString();
            }
        }
        return connexionStatusText;
    }

    public void setConnexionStatus(ConnexionStatus connexionStatus) {
        Color oldValueColor = getConnexionStatusColor();
        Icon oldValueIcon = getConnexionStatusIcon();
        String oldValueLabel = getConnexionStatusLabel();
        ConnexionStatus oldValue = getConnexionStatus();
        boolean oldSuccess = isConnexionSuccess();
        boolean oldFailed = isConnexionFailed();
        boolean oldUntested = isConnexionUntested();
        this.connexionStatus = connexionStatus;
        this.connexionStatusText = null;
        firePropertyChange(CONNEXION_STATUS_PROPERTY_NAME, oldValue, connexionStatus);
        firePropertyChange(CONNEXION_STATUS_COLOR_PROPERTY_NAME, oldValueColor, getConnexionStatusColor());
        firePropertyChange(CONNEXION_STATUS_ICON_PROPERTY_NAME, oldValueIcon, getConnexionStatusIcon());
        firePropertyChange(CONNEXION_STATUS_TEXT_PROPERTY_NAME, null, getConnexionStatusText());
        firePropertyChange(CONNEXION_STATUS_LABEL_PROPERTY_NAME, oldValueLabel, getConnexionStatusLabel());
        firePropertyChange(CONNEXION_SUCCESS_PROPERTY_NAME, oldSuccess, isConnexionSuccess());
        firePropertyChange(CONNEXION_FAILED_PROPERTY_NAME, oldFailed, isConnexionFailed());
        firePropertyChange(CONNEXION_UNTESTED_PROPERTY_NAME, oldUntested, isConnexionUntested());
    }

    public boolean isCanCreateDatabase() {
        return canCreateDatabase;
    }

    public void setCanCreateDatabase(boolean canCreateDatabase) {
        boolean oldValue = isCanCreateDatabase();
        this.canCreateDatabase = canCreateDatabase;
        firePropertyChange(CAN_CREATE_DATABASE_PROPERTY_NAME, oldValue, canCreateDatabase);
    }

    public String getConnexionStatusError() {
        return connexionStatusError;
    }

    public void setConnexionStatusError(String connexionStatusError) {
        String oldValue = getConnexionStatusError();
        this.connexionStatusError = connexionStatusError;
        firePropertyChange(CONNEXION_STATUS_ERROR_PROPERTY_NAME, oldValue, connexionStatusError);
    }

    public Throwable getConnexionError() {
        return connexionError;
    }

    public void setConnexionError(Throwable connexionError) {
        Throwable oldValue = getConnexionError();
        this.connexionError = connexionError;
        firePropertyChange(CONNEXION_ERROR_PROPERTY_NAME, oldValue, connexionError);
        setConnexionStatusError(connexionError == null ? null : connexionError.getMessage() == null ? connexionError.toString() : connexionError.getMessage());
    }

    public ObserveDataSourceInformation getDataSourceInformation() {
        return dataSourceInformation;
    }

    public void setDataSourceInformation(ObserveDataSourceInformation dataSourceInformation) {
        ObserveDataSourceInformation oldValue = getDataSourceInformation();
        this.dataSourceInformation = dataSourceInformation;
        //FIXME Find out why we did this before ?
        //FIXME the configuration should never be altered by information
//        if (dataSourceInformation != null) {
//            getConfiguration().setModelVersion(dataSourceInformation.getVersion());
//        }
        firePropertyChange(DATASOURCE_INFORMATION_PROPERTY_NAME, oldValue, dataSourceInformation);
    }

    public boolean isCanMigrate() {
        return canMigrate;
    }

    public void setCanMigrate(boolean canMigrate) {
        boolean oldValue = isCanMigrate();
        this.canMigrate = canMigrate;
        firePropertyChange(CAN_MIGRATE_PROPERTY_NAME, oldValue, canMigrate);
    }

    /**
     * Use this as soon as the configuration has been modified.
     */
    protected void clearStatus() {
        setDataSourceInformation(null);
        setConnexionError(null);
        setConnexionStatus(ConnexionStatus.UNTESTED);
        setSyntaxValid(testSyntax());
    }

    protected void onConnexionError(Throwable e) {
        if (e instanceof UndeclaredThrowableException) {
            e = ((UndeclaredThrowableException) e).getUndeclaredThrowable();
        }
        setConnexionError(e);
        setConnexionStatus(ConnexionStatus.FAILED);
    }

    public ObserveDataSourceConfiguration getConfiguration() {
        return configuration;
    }

    protected final ObserveDataSourceInformation test(ObserveSwingDataSource dataSource) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        if (isConnexionFailed()) {
            return null;
        }
        testServerVersion(dataSource);
        testModelVersion(dataSource);
        testCanConnect(dataSource);
        if (isConnexionFailed()) {
            return null;
        }
        return Objects.requireNonNull(getDataSourceInformation());

    }

    protected final void testCanConnect(ObserveSwingDataSource dataSource) {
        if (isConnexionFailed()) {
            return;
        }
        try {
            boolean canCreateDatabase = isCanCreateDatabase();
            ObserveDataSourceInformation dataSourceInformation = dataSource.checkCanConnect(canCreateDatabase);
            setDataSourceInformation(dataSourceInformation);
        } catch (Exception e) {
            setDataSourceInformation(null);
            onConnexionError(e);
        }
    }

    public void testServerVersion(ObserveSwingDataSource dataSource) {
        if (isConnexionFailed()) {
            return;
        }
        AnonymousService pingService = dataSource.getAnonymousService();
        Version clientVersion = getClientConfig().getVersion();
        Version serverVersion = pingService.getServerVersion();
        if (clientVersion.isSnapshot()) {
            clientVersion = Version.removeSnapshot(clientVersion);
        }
        if (getClientConfig().isCheckServerVersion() && !Objects.equals(serverVersion, clientVersion)) {
            setConnexionStatusError(t("observe.ui.datasource.storage.error.serverVersionMismatch", serverVersion, clientVersion));
            setConnexionStatus(ConnexionStatus.FAILED);
        }
    }

    public void testModelVersion(ObserveSwingDataSource dataSource) {
        if (isConnexionFailed()) {
            return;
        }
        AnonymousService pingService = dataSource.getAnonymousService();
        Version clientModelVersion = getModelVersion();
        Version modelServerVersion = pingService.getModelVersion();
        if (!Objects.equals(clientModelVersion, modelServerVersion)) {
            setConnexionStatusError(t("observe.ui.datasource.storage.error.serverVersionModelMismatch", modelServerVersion, clientModelVersion));
            setConnexionStatus(ConnexionStatus.FAILED);
        }
    }

    protected void testDataSourceInformation(ObserveDataSourceInformation dataSourceInformation) {
        if (isConnexionFailed()) {
            return;
        }
        if (dataSourceInformation == null) {
            return;
        }
        boolean canCreateDatabase = isCanCreateDatabase();
        Version versionDataSource = dataSourceInformation.getVersion();
        // en mise a jour de la base on ne test pas la version
        if (!isCanMigrate() && !canCreateDatabase && !getModelVersion().equals(versionDataSource)) {
            setConnexionStatusError(t("observe.ui.datasource.storage.error.dbVersionMismatch", versionDataSource, getModelVersion()));
            setConnexionStatus(ConnexionStatus.FAILED);
        }
    }

    protected void testsIsSuperUser(ObserveDataSourceInformation dataSourceInformation) {
        if (dataSourceInformation != null && !dataSourceInformation.isSuperUser()) {
            setConnexionStatusError(t("observe.ui.datasource.storage.error.user.not.superUser"));
            setConnexionStatus(ConnexionStatus.FAILED);
        }
    }

    protected void testIsOwner(ObserveDataSourceInformation dataSourceInformation) {
        if (dataSourceInformation != null && !dataSourceInformation.isOwner()) {
            setConnexionStatusError(t("observe.ui.datasource.storage.error.user.not.owner"));
            setConnexionStatus(ConnexionStatus.FAILED);
        }
    }

    public Map<ConnexionStatus, Color> getColors() {
        if (colors == null) {
            colors = Map.of(
                    ConnexionStatus.UNTESTED, Color.GRAY,
                    ConnexionStatus.FAILED, Color.RED,
                    ConnexionStatus.SUCCESS, Color.GREEN);
        }
        return colors;
    }

    /**
     * To fire {@link #USE_PRESET_PROPERTY_NAME} property when a preset was used (this is used by wizard by update ui).
     *
     * @param testConnexion {@code true} if connection is successful.
     */
    public void fireUsePreset(boolean testConnexion) {
        firePropertyChange(USE_PRESET_PROPERTY_NAME, testConnexion);
    }

    public void copyTo(ConfigurationModel dst) {
        dst.connexionError = getConnexionError();
        dst.connexionStatus = getConnexionStatus();
        dst.dataSourceInformation = getDataSourceInformation();
        dst.canMigrate = isCanMigrate();
        dst.canCreateDatabase = isCanCreateDatabase();
    }
}

