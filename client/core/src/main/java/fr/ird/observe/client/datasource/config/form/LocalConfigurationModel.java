package fr.ird.observe.client.datasource.config.form;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.io.File;

/**
 * Created on 28/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
@GenerateJavaBeanDefinition
public class LocalConfigurationModel extends ConfigurationModel {
    public static final String DB_NAME_PROPERTY_NAME = "dbName";
    public static final String USERNAME_PROPERTY_NAME = "username";
    public static final String DIRECTORY_PROPERTY_NAME = "directory";
    public static final String DATABASE_FILE_PROPERTY_NAME = "databaseFile";

    public LocalConfigurationModel() {
        super(new ObserveDataSourceConfigurationTopiaH2());
    }

    @Override
    public ObserveDataSourceConfigurationTopiaH2 getConfiguration() {
        return (ObserveDataSourceConfigurationTopiaH2) super.getConfiguration();
    }

    public void fromConfig(ObserveDataSourceConfigurationTopiaH2 config) {
        File oldDirectory = getDirectory();
        String oldUsername = getUsername();
        char[] oldPassword = getPassword();
        String oldDbName = getDbName();
        super.fromConfig(config);
        getConfiguration().setLogin(config == null ? null : config.getLogin());
        getConfiguration().setPassword(config == null ? null : config.getPassword());
        getConfiguration().setDbName(config == null ? null : config.getDbName());
        getConfiguration().setDirectory(config == null ? null : config.getDirectory());
        firePropertyChange(USERNAME_PROPERTY_NAME, oldUsername, getUsername());
        firePropertyChange(PASSWORD_PROPERTY_NAME, oldPassword, getPassword());
        firePropertyChange(DIRECTORY_PROPERTY_NAME, oldDirectory, getDirectory());
        firePropertyChange(DATABASE_FILE_PROPERTY_NAME, oldDirectory, getDirectory());
        firePropertyChange(DB_NAME_PROPERTY_NAME, oldDbName, getDbName());
    }

    @Override
    protected boolean testSyntax() {
        return getDirectory() != null
                && getDbName() != null
                && getPassword() != null
                && getUsername() != null;
    }

    @Override
    public void testServerVersion(ObserveSwingDataSource dataSource) {
        //FIXME:Service why we do not have a such implementation for local service?
        //super.testServerVersion(dataSource);
    }

    @Override
    public void testModelVersion(ObserveSwingDataSource dataSource) {
        //FIXME:Service why we do not have a such implementation for local service?
        //super.testModelVersion(dataSource);
    }

    @Override
    public void copyTo(ConfigurationModel dst) {
        ((LocalConfigurationModel) dst).fromConfig(getConfiguration());
        super.copyTo(dst);
    }

    public String getDbName() {
        return getConfiguration().getDbName();
    }

    public void setDbName(String dbName) {
        String oldValue = getDbName();
        File oldDatabaseFile = getDatabaseFile();
        getConfiguration().setDbName(dbName);
        firePropertyChange(DB_NAME_PROPERTY_NAME, oldValue, dbName);
        firePropertyChange(DATABASE_FILE_PROPERTY_NAME, oldDatabaseFile, getDatabaseFile());
        clearStatus();
    }

    public File getDirectory() {
        return getConfiguration().getDirectory();
    }

    public void setDirectory(File directory) {
        File oldValue = getDirectory();
        File oldDatabaseFile = getDatabaseFile();
        getConfiguration().setDirectory(directory);
        firePropertyChange(DIRECTORY_PROPERTY_NAME, oldValue, directory);
        firePropertyChange(DATABASE_FILE_PROPERTY_NAME, oldDatabaseFile, getDatabaseFile());
        clearStatus();
    }

    public String getUsername() {
        return getConfiguration().getLogin();
    }

    public void setUsername(String username) {
        String oldValue = getUsername();
        getConfiguration().setLogin(username);
        firePropertyChange(USERNAME_PROPERTY_NAME, oldValue, username);
        clearStatus();
    }

    public char[] getPassword() {
        return getConfiguration().getPassword();
    }

    public void setPassword(char[] password) {
        char[] oldValue = getPassword();
        getConfiguration().setPassword(password);
        firePropertyChange(PASSWORD_PROPERTY_NAME, oldValue, password);
        clearStatus();
    }

    public File getDatabaseFile() {
        return getConfiguration().getDatabaseFile();
    }
}
