package fr.ird.observe.client.datasource.config.form;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.dto.presets.RemoteDataSourceConfiguration;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created on 23/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
@GenerateJavaBeanDefinition
public class RemoteConfigurationModel extends ConfigurationModel {
    public static final String JDBC_URL_PROPERTY_NAME = "jdbcUrl";
    public static final String USERNAME_PROPERTY_NAME = "username";
    public static final String USE_SLL_PROPERTY_NAME = "useSll";
    private static final Logger log = LogManager.getLogger(RemoteConfigurationModel.class);

    public RemoteConfigurationModel() {
        super(new ObserveDataSourceConfigurationTopiaPG());
    }

    public void fromConfig(ObserveDataSourceConfigurationTopiaPG config) {
        String oldJdbcUrl = getJdbcUrl();
        String oldUsername = getUsername();
        char[] oldPassword = getPassword();
        boolean oldUseSsl = isUseSsl();
        super.fromConfig(config);
        getConfiguration().setUrl(config == null ? null : config.getUrl());
        getConfiguration().setLogin(config == null ? null : config.getLogin());
        getConfiguration().setPassword(config == null ? null : config.getPassword());
        getConfiguration().setUseSsl(config != null && config.isUseSsl());
        firePropertyChange(JDBC_URL_PROPERTY_NAME, oldJdbcUrl, getJdbcUrl());
        firePropertyChange(USERNAME_PROPERTY_NAME, oldUsername, getUsername());
        firePropertyChange(PASSWORD_PROPERTY_NAME, oldPassword, getPassword());
        firePropertyChange(USE_SLL_PROPERTY_NAME, oldUseSsl, isUseSsl());
    }

    public void fromPreset(RemoteDataSourceConfiguration configuration) {
        log.info(String.format("Use configuration: %s", configuration.getName()));
        String oldJdbcUrl = getJdbcUrl();
        String oldUsername = getUsername();
        char[] oldPassword = getPassword();
        boolean oldUseSsl = isUseSsl();
        getConfiguration().setUrl(configuration.getUrl());
        getConfiguration().setLogin(configuration.getLogin());
        getConfiguration().setPassword(configuration.getPassword().toCharArray());
        getConfiguration().setUseSsl(configuration.isUseSsl());
        firePropertyChange(JDBC_URL_PROPERTY_NAME, oldJdbcUrl, getJdbcUrl());
        firePropertyChange(USERNAME_PROPERTY_NAME, oldUsername, getUsername());
        firePropertyChange(PASSWORD_PROPERTY_NAME, oldPassword, getPassword());
        firePropertyChange(USE_SLL_PROPERTY_NAME, oldUseSsl, isUseSsl());
        clearStatus();
    }

    public RemoteDataSourceConfiguration toPreset(String configurationName) {
        RemoteDataSourceConfiguration configuration = new RemoteDataSourceConfiguration();
        configuration.setName(configurationName);
        configuration.setUrl(getJdbcUrl());
        configuration.setLogin(getUsername());
        configuration.setPassword(new String(getPassword()));
        configuration.setUseSsl(isUseSsl());
        return configuration;
    }

    @Override
    protected boolean testSyntax() {
        return getJdbcUrl() != null
                && getUsername() != null
                && getPassword() != null;
    }

    @Override
    public void testServerVersion(ObserveSwingDataSource dataSource) {
        //FIXME:Service why we do not have a such implementation for local service?
        //super.testServerVersion(dataSource);
    }

    @Override
    public void testModelVersion(ObserveSwingDataSource dataSource) {
        //FIXME:Service why we do not have a such implementation for local service?
        //super.testModelVersion(dataSource);
    }

    @Override
    public ObserveDataSourceConfigurationTopiaPG getConfiguration() {
        return (ObserveDataSourceConfigurationTopiaPG) super.getConfiguration();
    }

    public String getJdbcUrl() {
        return getConfiguration().getUrl();
    }

    public void setJdbcUrl(String jdbcUrl) {
        String oldValue = getJdbcUrl();
        getConfiguration().setUrl(jdbcUrl);
        firePropertyChange(JDBC_URL_PROPERTY_NAME, oldValue, jdbcUrl);
        clearStatus();
    }

    public String getUsername() {
        return getConfiguration().getLogin();
    }

    public void setUsername(String username) {
        String oldValue = getUsername();
        getConfiguration().setLogin(username);
        firePropertyChange(USERNAME_PROPERTY_NAME, oldValue, username);
        clearStatus();
    }

    public char[] getPassword() {
        return getConfiguration().getPassword();
    }

    public void setPassword(char[] password) {
        char[] oldValue = getPassword();
        getConfiguration().setPassword(password);
        firePropertyChange(PASSWORD_PROPERTY_NAME, oldValue, password);
        clearStatus();
    }

    public boolean isUseSsl() {
        return getConfiguration().isUseSsl();
    }

    public void setUseSsl(boolean useSsl) {
        boolean oldValue = isUseSsl();
        getConfiguration().setUseSsl(useSsl);
        firePropertyChange(USE_SLL_PROPERTY_NAME, oldValue, useSsl);
        clearStatus();
    }

    @Override
    public void copyTo(ConfigurationModel dst) {
        ((RemoteConfigurationModel) dst).fromConfig(getConfiguration());
        super.copyTo(dst);
    }
}
