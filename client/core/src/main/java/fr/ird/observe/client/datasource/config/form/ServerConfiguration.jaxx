<!--
  #%L
  ObServe Client :: Core
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<JPanel id="serverConfigTop" layout='{new BorderLayout()}'>
  <import>
    org.nuiton.jaxx.widgets.text.NormalTextEditor
    org.nuiton.jaxx.widgets.text.PasswordEditor
  </import>

  <ServerConfigurationModel id='model' initializer='getContextValue(ServerConfigurationModel.class)'/>
  <Table id="configPanel" beanScope="model">
    <row>
      <cell anchor='west'>
        <JLabel id="serverUrlLabel" text='observe.ui.datasource.storage.remote.url'/>
      </cell>
      <cell weightx='1' fill="both" anchor='east'>
        <NormalTextEditor id="serverUrl"/>
      </cell>
    </row>
    <row>
      <cell anchor='west'>
        <JLabel id="loginLabel" text='observe.ui.datasource.storage.remote.login'/>
      </cell>
      <cell weightx='1' fill="both" anchor='east'>
        <NormalTextEditor id="login"/>
      </cell>
    </row>
    <row>
      <cell anchor='west'>
        <JLabel id="passwordLabel" text='observe.ui.datasource.storage.remote.password'/>
      </cell>
      <cell weightx='1' fill="both" anchor='east'>
        <PasswordEditor id="password"/>
      </cell>
    </row>
    <row>
      <cell anchor='west'>
        <JLabel id="databaseNameLabel" text='observe.ui.datasource.storage.server.dataBase'/>
      </cell>
      <cell weightx='1' fill="both" anchor='east'>
        <NormalTextEditor id="databaseName"/>
      </cell>
    </row>
  </Table>

</JPanel>
