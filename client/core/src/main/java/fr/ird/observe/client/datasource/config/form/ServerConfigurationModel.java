package fr.ird.observe.client.datasource.config.form;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.ConnexionStatus;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.dto.presets.ServerDataSourceConfiguration;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created on 23/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
@GenerateJavaBeanDefinition
public class ServerConfigurationModel extends ConfigurationModel {
    public static final String LOGIN_PROPERTY_NAME = "login";
    public static final String SERVER_URL_PROPERTY_NAME = "serverUrl";
    public static final String DATABASE_NAME_PROPERTY_NAME = "databaseName";
    private static final Logger log = LogManager.getLogger(ServerConfigurationModel.class);

    public ServerConfigurationModel() {
        super(new ObserveDataSourceConfigurationRest());
    }

    public void fromConfig(ObserveDataSourceConfigurationRest config) {
        String oldServerUrl = getServerUrl();
        String oldLogin = getLogin();
        char[] oldPassword = getPassword();
        String oldDatabaseName = getDatabaseName();
        super.fromConfig(config);
        getConfiguration().setUrl(config == null ? null : config.getUrl());
        getConfiguration().setLogin(config == null ? null : config.getLogin());
        getConfiguration().setPassword(config == null ? null : config.getPassword());
        getConfiguration().setDatabaseName(config == null ? null : config.getDatabaseName());
        firePropertyChange(SERVER_URL_PROPERTY_NAME, oldServerUrl, getServerUrl());
        firePropertyChange(LOGIN_PROPERTY_NAME, oldLogin, getLogin());
        firePropertyChange(PASSWORD_PROPERTY_NAME, oldPassword, getPassword());
        firePropertyChange(DATABASE_NAME_PROPERTY_NAME, oldDatabaseName, getDatabaseName());
    }

    public void fromPreset(ServerDataSourceConfiguration configuration) {
        log.info(String.format("Use configuration: %s", configuration));
        String oldServerUrl = getServerUrl();
        String oldLogin = getLogin();
        char[] oldPassword = getPassword();
        String oldOptionalDatabaseName = getDatabaseName();
        getConfiguration().setUrl(configuration.getUrl());
        getConfiguration().setLogin(configuration.getLogin());
        getConfiguration().setPassword(configuration.getPassword().toCharArray());
        getConfiguration().setDatabaseName(configuration.getDatabaseName());
        firePropertyChange(SERVER_URL_PROPERTY_NAME, oldServerUrl, getServerUrl());
        firePropertyChange(LOGIN_PROPERTY_NAME, oldLogin, getLogin());
        firePropertyChange(PASSWORD_PROPERTY_NAME, oldPassword, getPassword());
        firePropertyChange(DATABASE_NAME_PROPERTY_NAME, oldOptionalDatabaseName, getDatabaseName());
        clearStatus();
    }

    public ServerDataSourceConfiguration toPreset(String configurationName) {
        ServerDataSourceConfiguration configuration = new ServerDataSourceConfiguration();
        configuration.setName(configurationName);
        configuration.setUrl(getServerUrl());
        configuration.setLogin(getLogin());
        configuration.setPassword(new String(getPassword()));
        configuration.setDatabaseName(getDatabaseName());
        return configuration;
    }

    @Override
    public ObserveDataSourceConfigurationRest getConfiguration() {
        return (ObserveDataSourceConfigurationRest) super.getConfiguration();
    }

    @Override
    protected boolean testSyntax() {
        String serverUrl = getConfiguration().getUrl();
        if (getServerUrl() == null || getLogin() == null || getPassword() == null) {
            return false;
        }
        try {
            new URL(serverUrl);
        } catch (MalformedURLException e) {
            setConnexionStatusError(I18n.t("observe.ui.datasource.storage.error.badUrl", serverUrl));
            setConnexionStatus(ConnexionStatus.FAILED);
            return false;
        }
        return true;
    }

    public String getServerUrl() {
        return getConfiguration().getUrl();
    }

    public void setServerUrl(String serverUrl) {
        String oldValue = getServerUrl();
        getConfiguration().setUrl(serverUrl);
        firePropertyChange(SERVER_URL_PROPERTY_NAME, oldValue, serverUrl);
        clearStatus();
    }

    public String getLogin() {
        return getConfiguration().getLogin();
    }

    public void setLogin(String login) {
        String oldValue = getLogin();
        getConfiguration().setLogin(login);
        firePropertyChange(LOGIN_PROPERTY_NAME, oldValue, login);
        clearStatus();
    }

    public char[] getPassword() {
        return getConfiguration().getPassword();
    }

    public void setPassword(char[] password) {
        char[] oldValue = getPassword();
        getConfiguration().setPassword(password);
        firePropertyChange(PASSWORD_PROPERTY_NAME, oldValue, password);
        clearStatus();
    }

    public String getDatabaseName() {
        return getConfiguration().getDatabaseName();
    }

    public void setDatabaseName(String optionalDatabaseName) {
        String oldValue = getDatabaseName();
        getConfiguration().setDatabaseName(optionalDatabaseName);
        firePropertyChange(DATABASE_NAME_PROPERTY_NAME, oldValue, optionalDatabaseName);
        clearStatus();
    }

    @Override
    public void copyTo(ConfigurationModel dst) {
        ((ServerConfigurationModel) dst).fromConfig(getConfiguration());
        super.copyTo(dst);
    }
}
