package fr.ird.observe.client.datasource.h2.backup;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

/**
 * Created on 19/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AutomaticLocalDatabaseBackupTask extends LocalDatabaseBackupTaskSupport {

    private static final Logger log = LogManager.getLogger(AutomaticLocalDatabaseBackupTask.class);

    public AutomaticLocalDatabaseBackupTask(ClientConfig config, BackupsManager backupsManager, ObserveDataSourcesManager dataSourcesManager) {
        super(config, backupsManager, dataSourcesManager);
    }

    @Override
    public void run() {

        if (!config.isBackupUse()) {
            log.info("Skip, auto backup is not activate.");
            return;
        }
        if (!config.isLocalStorageExist()) {
            log.info("Skip, no local database found.");
            return;
        }

        if (canBackup()) {
            File file = doBackup();
            backupsManager.addAutomaticBackup(file.toPath());
        }

    }

}
