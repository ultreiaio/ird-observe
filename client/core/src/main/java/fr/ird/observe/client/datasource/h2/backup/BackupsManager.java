package fr.ird.observe.client.datasource.h2.backup;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceEvent;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceListenerAdapter;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import io.ultreia.java4all.util.json.adapters.DateAdapter;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 23/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BackupsManager implements Closeable {

    private static final Logger log = LogManager.getLogger(BackupsManager.class);

    private static final Pattern AUTOMATIC_BACKUP_FILENAME_PATTERN = Pattern.compile("obs.+-([0-9-]+)\\.sql\\.gz");

    private final ObserveDataSourcesManager dataSourcesManager;
    private final Path storePath;
    private final List<BackupStorage> backups;
    private final BlockingDeque<BackupStorage> backupsToCheck = new LinkedBlockingDeque<>();
    private final Gson gson;
    private boolean run = true;
    private final Thread checkBackups = new Thread("Check-Backups") {
        @Override
        public void run() {

            while (run) {
                log.info("Waiting for next backup to check");
                try {
                    BackupStorage backupStorage = backupsToCheck.takeFirst();
                    if (run) {
                        addBackup(backupStorage);
                    }
                } catch (InterruptedException e) {
                    log.error(e);
                }
            }

        }

        private void addBackup(BackupStorage backupStorage) {
            log.info("Will check backup " + backupStorage.getFile());

            try {

                try (ObserveSwingDataSource dataSource = dataSourcesManager.newTemporaryH2DataSource("check-" + backupStorage.getName())) {
                    dataSource.addObserveSwingDataSourceListener(new ObserveSwingDataSourceListenerAdapter() {
                        @Override
                        public void onClosed(ObserveSwingDataSourceEvent event) {
                            dataSource.destroy();
                        }
                    });
                    dataSource.createFromDump(SqlScript.of(backupStorage.getFile().toURI()));
                }
                backupStorage.setVerified(true);
                log.info("Add sane backup from " + backupStorage.getFile());
                backups.add(backupStorage);
                storeBackups();
            } catch (Exception e) {
                log.error("Could not check backup " + backupStorage.getFile(), e);
            }

        }

    };

    public BackupsManager(ClientConfig config, ObserveDataSourcesManager dataSourcesManager) {
        this.storePath = config.getBackupsFile().toPath();
        this.dataSourcesManager = dataSourcesManager;
        this.gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateAdapter()).create();
        try {
            List<BackupStorage> backups;
            if (config.isBackupUse() && Files.exists(storePath)) {
                try (BufferedReader reader = Files.newBufferedReader(storePath, StandardCharsets.UTF_8)) {
                    Type typeOfT = new TypeToken<List<BackupStorage>>() {
                    }.getType();

                    try {
                        backups = gson.fromJson(reader, typeOfT);
                    } catch (Exception e) {
                        log.error(String.format("Could not load %s file.", storePath), e);
                        backups = new LinkedList<>();
                    }
                    log.info(String.format("Loaded %d backups from %s", backups.size(), storePath));
                }
            } else {
                backups = new LinkedList<>();
            }
            backups.removeIf(backupStorage -> !backupStorage.exists());
            this.backups = backups;
        } catch (IOException e) {
            throw new ObserveSwingTechnicalException("Can't init backups manager", e);
        }
    }

    public void start() {
        log.info("Starts Backups manager");
        this.checkBackups.start();
        Iterator<BackupStorage> iterator = backups.iterator();
        List<BackupStorage> toCheck = new LinkedList<>();
        while (iterator.hasNext()) {
            BackupStorage backupStorage = iterator.next();
            if (!backupStorage.isVerified()) {
                iterator.remove();
                toCheck.add(backupStorage);
            }
        }
        for (BackupStorage backupStorage : toCheck) {
            addAutomaticBackup(backupStorage.getFile().toPath());
        }
    }

    public List<BackupStorage> getAutomaticBackups() {
        return backups.stream().filter(BackupStorage::isAutomatic).sorted(Comparator.comparing(BackupStorage::getDate).reversed()).collect(Collectors.toList());
    }

    public void addAutomaticBackup(Path backupPath) {
        Matcher matcher = AUTOMATIC_BACKUP_FILENAME_PATTERN.matcher(backupPath.toFile().getName());
        if (!matcher.matches()) {
            throw new IllegalStateException("backup does not match the pattern.");
        }
        BackupStorage backupStorage = new BackupStorage();
        backupStorage.setDate(new Date());
        backupStorage.setFile(backupPath.toFile());
        backupStorage.setAutomatic(true);
        backupStorage.setVerified(false);
        backupStorage.setName(matcher.group(1));
        backupsToCheck.add(backupStorage);
    }

    @Override
    public void close() {

        run = false;

        try {
            log.debug("Waiting for " + checkBackups + " to end");
            // add a fake backup storage to end the main thread
            backupsToCheck.add(new BackupStorage());
            checkBackups.join();
            log.debug("Closed " + checkBackups);
        } catch (InterruptedException e) {
            throw new ObserveSwingTechnicalException(e);
        }

    }

    private void storeBackups() throws IOException {
        log.info(String.format("Store %d backups to %s", backups.size(), storePath));
        try (BufferedWriter writer = Files.newBufferedWriter(storePath, StandardCharsets.UTF_8)) {
            gson.toJson(backups, writer);
        }
    }

}
