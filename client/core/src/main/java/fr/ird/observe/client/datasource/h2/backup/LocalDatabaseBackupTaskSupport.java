package fr.ird.observe.client.datasource.h2.backup;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.ClientUIContextApplicationComponent;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Date;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 19/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class LocalDatabaseBackupTaskSupport implements Runnable {

    private static final Logger log = LogManager.getLogger(LocalDatabaseBackupTaskSupport.class);
    final ClientConfig config;
    final BackupsManager backupsManager;
    final ObserveDataSourcesManager dataSourcesManager;

    protected LocalDatabaseBackupTaskSupport(ClientConfig config, BackupsManager backupsManager, ObserveDataSourcesManager dataSourcesManager) {
        this.config = config;
        this.backupsManager = backupsManager;
        this.dataSourcesManager = dataSourcesManager;
    }

    protected boolean canBackup() {
        ObserveSwingDataSource mainDataSource = dataSourcesManager.getMainDataSource();
        if (mainDataSource == null) {
            log.info("Skip, no database open.");
            return false;
        }
        if (!mainDataSource.isLocal()) {
            log.info("Skip, open database is not local.");
            return false;
        }

        if (!mainDataSource.isModified()) {
            log.info("Skip, open database was not modified since last backup.");
            return false;
        }
        return true;
    }

    protected File doBackup() {
        File file = config.newAutomaticBackupDataFile();
        String startMessage = t("observe.ui.datasource.backup.start", new Date());

        ClientUIContextApplicationComponent.value().setUiStatus(startMessage);
        log.info(startMessage + " - " + file);
        long t0 = System.nanoTime();
        ClientUIContextApplicationComponent.value().getBusyModel().addTask(startMessage + " - " + file);
        ObserveSwingDataSource mainDataSource = dataSourcesManager.getMainDataSource();
        try {
            mainDataSource.backup(file);
        } finally {
            String endMessage = t("observe.ui.datasource.backup.done", new Date(), Strings.convertTime(System.nanoTime() - t0));
            ClientUIContextApplicationComponent.value().setUiStatus(endMessage);
            log.info(endMessage + " - " + file);
            ClientUIContextApplicationComponent.value().getBusyModel().popTask();
            mainDataSource.setModified(false);
        }
        return file;
    }
}
