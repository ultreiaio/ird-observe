package fr.ird.observe.client.datasource.h2.backup;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class LocalDatabaseBackupTimer extends ScheduledThreadPoolExecutor implements Closeable {

    private static final Logger log = LogManager.getLogger(LocalDatabaseBackupTimer.class);

    private final ClientConfig clientConfig;
    private final BackupsManager backupsManager;
    private final ObserveDataSourcesManager dataSourcesManager;

    public LocalDatabaseBackupTimer(ClientConfig clientConfig, BackupsManager backupsManager, ObserveDataSourcesManager dataSourcesManager) {
        super(1);
        this.clientConfig = clientConfig;
        this.backupsManager = backupsManager;
        this.dataSourcesManager = dataSourcesManager;
    }

    public void start() {
        scheduleAtFixedRate(new AutomaticLocalDatabaseBackupTask(clientConfig, backupsManager, dataSourcesManager), 1, clientConfig.getBackupDelay(), TimeUnit.MINUTES);
    }

    @Override
    public void close() {

        try {
            awaitTermination(0, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            log.error(e);
        }

        if (clientConfig.isBackupAtClose()) {
            new AtCloseApplicationLocalDatabaseBackupTask(clientConfig, backupsManager, dataSourcesManager).run();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // who cares ?
            }
        }
    }
}
