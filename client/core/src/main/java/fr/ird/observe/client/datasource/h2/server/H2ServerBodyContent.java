package fr.ird.observe.client.datasource.h2.server;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.body.HideBodyContentNotAcceptedException;
import fr.ird.observe.client.main.body.MainUIBodyContent;
import org.h2.tools.Server;

import java.io.File;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
@SuppressWarnings("rawtypes")
@AutoService(MainUIBodyContent.class)
public class H2ServerBodyContent extends MainUIBodyContent<H2ServerUI> implements WithClientUIContextApi {

    private static final String H2_SERVER_URL_PATTERN = "jdbc:h2:%s/%s/obstuna";

    private Server server;

    public H2ServerBodyContent() {
        super(2, H2ServerUI.class, H2ServerUI::new);
    }

    @Override
    public void show(ObserveMainUI mainUI) {
        H2ServerUI ui = get();

        ClientConfig config = getClientConfig();
        File dbDirectory = new File(config.getLocalDBDirectory(), "obstuna");
        String h2Login = config.getH2Login();
        String h2Password = config.getH2Password();


        String url = String.format(H2_SERVER_URL_PATTERN,
                                   server.getURL(),
                                   dbDirectory.getAbsolutePath());


        String text = t("observe.ui.message.server.info", dbDirectory);


        ui.getServerModeInfo().setText(text);

        ui.getServerModeURL().setText(url);
        ui.getServerModeLogin().setText(h2Login);
        ui.getServerModePassword().setText(h2Password);

        // On mémorise l'instance du server dans le contexte applicatif afin de pouvoir la récupérer plus tard,
        // par exemple lorsque l'on souhaitera arrêter le server.
        ui.getModel().setServer(server);

        super.show(mainUI);
    }

    @Override
    public void hide(ObserveMainUI mainUI) throws HideBodyContentNotAcceptedException {
        super.hide(mainUI);
        server = null;
    }

    public void setServer(Server server) {
        this.server = server;
    }
}
