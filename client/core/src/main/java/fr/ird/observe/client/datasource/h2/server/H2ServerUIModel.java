package fr.ird.observe.client.datasource.h2.server;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.h2.tools.Server;

@GenerateJavaBeanDefinition
public class H2ServerUIModel extends AbstractJavaBean {

    private Server h2WebServer;
    private Server server;

    public Server getH2WebServer() {
        return h2WebServer;
    }

    public void setH2WebServer(Server h2WebServer) {
        boolean wasServerRunning = isH2WebServerRunning();
        this.h2WebServer = h2WebServer;
        firePropertyChange("h2WebServerRunning", wasServerRunning, isH2WebServerRunning());
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public boolean isH2WebServerRunning() {
        return h2WebServer != null;
    }

}
