package fr.ird.observe.client.datasource.h2.server.actions;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.h2.server.H2ServerUI;
import fr.ird.observe.client.datasource.h2.server.H2ServerUIModel;
import fr.ird.observe.client.main.body.NoBodyContentComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.tools.Server;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class StopServerMode extends H2ServerUIActionSupport {

    private static final Logger log = LogManager.getLogger(StopServerMode.class);

    public StopServerMode() {
        super(t("observe.ui.action.stop.server.mode"), t("observe.ui.action.stop.server.mode.tip"), "db-stop-server", 'Q');
    }

    @Override
    protected void doActionPerformed(ActionEvent e, H2ServerUI ui) {

        log.info("Will stop server mode...");
        Server server = ui.getModel().getServer();
        if (server != null) {
            server.stop();
        }
        H2ServerUIModel uiModel = ui.getModel();
        uiModel.setServer(null);

        if (uiModel.isH2WebServerRunning()) {

            // stop also the h2 web server
            StopH2WebServer.launchStopH2WebServer(ui);
        }
        getMainUI().changeBodyContent(NoBodyContentComponent.class);
    }
}
