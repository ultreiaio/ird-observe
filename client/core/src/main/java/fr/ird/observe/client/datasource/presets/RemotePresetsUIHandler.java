package fr.ird.observe.client.datasource.presets;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.dto.presets.RemoteDataSourceConfiguration;
import fr.ird.observe.dto.presets.ServerDataSourceConfiguration;
import io.ultreia.java4all.decoration.Decorator;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.renderer.DecoratorListCellRenderer;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import java.awt.CardLayout;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * Created on 20/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class RemotePresetsUIHandler implements UIHandler<RemotePresetsUI>, WithClientUIContextApi {

    private RemotePresetsUI ui;

    @Override
    public void beforeInit(RemotePresetsUI ui) {
        this.ui = ui;
        RemotePresetsUIModel model = new RemotePresetsUIModel();
        ClientConfig config = getClientConfig();
        model.setServerDataSourceConfigurations(config.getServerDataSourceConfigurationList());
        model.setRemoteDataSourceConfigurations(config.getRemoteDataSourceConfigurationList());
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(RemotePresetsUI ui) {

        initRemotePanel(ui);
        initServerPanel(ui);

        ui.getTabs().addChangeListener(this::onTabChanged);

        onTabChanged(null);
    }

    void setServerModified(KeyEvent event) {
        if (event.isActionKey()) {
            return;
        }
        ui.getModel().setServerModified(true);
    }

    void setRemoteModified(KeyEvent event) {
        if (event.isActionKey()) {
            return;
        }
        ui.getModel().setRemoteModified(true);
    }

    private void onTabChanged(ChangeEvent e) {
        JTabbedPane tabbedPane = e == null ? ui.getTabs() : (JTabbedPane) e.getSource();
        int selectedIndex = tabbedPane.getSelectedIndex();
        JComponent toFocus = ui.getCreateAction();
        switch (selectedIndex) {
            case 0:
                if (ui.getServerForm().isVisible()) {
                    toFocus = ui.getServerName();
                }
                break;
            case 1:
                if (ui.getRemoteForm().isVisible()) {
                    toFocus = ui.getRemoteName();
                }
                break;
        }
        SwingUtilities.invokeLater(toFocus::requestFocusInWindow);
    }

    private void initRemotePanel(RemotePresetsUI ui) {
        Decorator remoteDecorator = getDecoratorService().getDecoratorByType(RemoteDataSourceConfiguration.class, RemoteDataSourceConfiguration.WITH_URL);

        ui.getRemoteConfigurations().setCellRenderer(new DecoratorListCellRenderer<>(remoteDecorator));
        DefaultListModel<RemoteDataSourceConfiguration> remoteModel = new DefaultListModel<>();
        RemotePresetsUIModel model = ui.getModel();
        model.getRemoteDataSourceConfigurations().forEach(remoteModel::addElement);

        ui.getRemoteConfigurations().setModel(remoteModel);

        ui.getRemoteConfigurations().addListSelectionListener(evt -> {

            if (ui.getRemoteConfigurations().isSelectionEmpty()) {
                model.setRemoteDataSourceConfiguration(null);
                model.setRemoteModified(false);
            } else {
                model.setRemoteDataSourceConfiguration(ui.getRemoteConfigurations().getSelectedValue());
                ui.getRemoteName().requestFocusInWindow();
            }
        });

        model.addPropertyChangeListener("remoteDataSourceConfigurations", evt -> {

            @SuppressWarnings("unchecked") List<RemoteDataSourceConfiguration> newValue = (List<RemoteDataSourceConfiguration>) evt.getNewValue();

            DefaultListModel<RemoteDataSourceConfiguration> listModel = (DefaultListModel<RemoteDataSourceConfiguration>) ui.getRemoteConfigurations().getModel();
            listModel.clear();
            for (RemoteDataSourceConfiguration remoteDataSourceConfiguration : newValue) {
                listModel.addElement(remoteDataSourceConfiguration);
            }

        });

        model.addPropertyChangeListener("remoteDataSourceConfiguration", evt -> {

            RemoteDataSourceConfiguration oldValue = (RemoteDataSourceConfiguration) evt.getOldValue();
            if (model.getRemoteDataSourceConfiguration() == null) {
                ((CardLayout) ui.getRemoteContentPanel().getLayout()).show(ui.getRemoteContentPanel(), "empty");
                ui.getCreateAction().requestFocusInWindow();
            } else {
                if (oldValue == null) {
                    ((CardLayout) ui.getRemoteContentPanel().getLayout()).show(ui.getRemoteContentPanel(), "form");
                }
                model.setRemoteModified(false);
            }
            SwingUtilities.invokeLater(ui::repaint);
        });
        model.addPropertyChangeListener("remoteCreateMode", evt -> {
            JPanel remoteActions = ui.getRemoteActions();
            if ((Boolean) evt.getNewValue()) {
                remoteActions.remove(ui.getResetRemoteAction());
                remoteActions.remove(ui.getDeleteRemoteAction());
                remoteActions.remove(ui.getDuplicateRemoteAction());
                remoteActions.add(ui.getCancelRemoteAction(), 0);
            } else {
                remoteActions.add(ui.getResetRemoteAction(), 0);
                remoteActions.add(ui.getDeleteRemoteAction(), 2);
                remoteActions.add(ui.getDuplicateRemoteAction());
                remoteActions.remove(ui.getCancelRemoteAction());
            }
        });
        ui.getRemoteActions().remove(0);

        if (ui.getRemoteConfigurations().getModel().getSize() > 0) {
            ui.getRemoteConfigurations().setSelectedIndex(0);
        }
    }

    private void initServerPanel(RemotePresetsUI ui) {
        RemotePresetsUIModel model = ui.getModel();
        Decorator serverDecorator = getDecoratorService().getDecoratorByType(ServerDataSourceConfiguration.class, ServerDataSourceConfiguration.WITH_URL);
        ui.getServerConfigurations().setCellRenderer(new DecoratorListCellRenderer<>(serverDecorator));
        DefaultListModel<ServerDataSourceConfiguration> ServerModel = new DefaultListModel<>();
        model.getServerDataSourceConfigurations().forEach(ServerModel::addElement);

        ui.getServerConfigurations().setModel(ServerModel);

        ui.getServerConfigurations().addListSelectionListener(evt -> {
            if (ui.getServerConfigurations().isSelectionEmpty()) {
                model.setServerDataSourceConfiguration(null);
                model.setServerModified(false);
            } else {
                model.setServerDataSourceConfiguration(ui.getServerConfigurations().getSelectedValue());
                ui.getServerName().requestFocusInWindow();
            }
        });

        model.addPropertyChangeListener("serverDataSourceConfigurations", evt -> {

            @SuppressWarnings("unchecked") List<ServerDataSourceConfiguration> newValue = (List<ServerDataSourceConfiguration>) evt.getNewValue();

            DefaultListModel<ServerDataSourceConfiguration> listModel = (DefaultListModel<ServerDataSourceConfiguration>) ui.getServerConfigurations().getModel();
            listModel.clear();
            for (ServerDataSourceConfiguration ServerDataSourceConfiguration : newValue) {
                listModel.addElement(ServerDataSourceConfiguration);
            }

        });

        model.addPropertyChangeListener("serverDataSourceConfiguration", evt -> {

            ServerDataSourceConfiguration oldValue = (ServerDataSourceConfiguration) evt.getOldValue();

            if (model.getServerDataSourceConfiguration() == null) {
                ((CardLayout) ui.getServerContentPanel().getLayout()).show(ui.getServerContentPanel(), "empty");
                ui.getCreateAction().requestFocusInWindow();
            } else {
                if (oldValue == null) {
                    ((CardLayout) ui.getServerContentPanel().getLayout()).show(ui.getServerContentPanel(), "form");
                }
                model.setServerModified(false);
            }
            SwingUtilities.invokeLater(ui::repaint);
        });

        model.addPropertyChangeListener("serverCreateMode", evt -> {
            JPanel serverActions = ui.getServerActions();
            if ((Boolean) evt.getNewValue()) {
                serverActions.remove(ui.getResetServerAction());
                serverActions.remove(ui.getDeleteServerAction());
                serverActions.remove(ui.getDuplicateServerAction());
                serverActions.add(ui.getCancelServerAction(), 0);
            } else {

                serverActions.add(ui.getResetServerAction(), 0);
                serverActions.add(ui.getDeleteServerAction(), 2);
                serverActions.add(ui.getDuplicateServerAction());
                serverActions.remove(ui.getCancelServerAction());
            }
        });

        ui.getServerActions().remove(0);
        if (ui.getServerConfigurations().getModel().getSize() > 0) {
            ui.getServerConfigurations().setSelectedIndex(0);
        }
    }
}
