package fr.ird.observe.client.datasource.presets;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.presets.RemoteDataSourceConfiguration;
import fr.ird.observe.dto.presets.ServerDataSourceConfiguration;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.List;

/**
 * Created on 20/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
@GenerateJavaBeanDefinition
public class RemotePresetsUIModel extends AbstractJavaBean {

    private List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations;
    private RemoteDataSourceConfiguration remoteDataSourceConfiguration;
    private ServerDataSourceConfiguration serverDataSourceConfiguration;
    private List<ServerDataSourceConfiguration> serverDataSourceConfigurations;

    private boolean remoteModified;
    private boolean serverModified;
    private boolean remoteCreateMode;
    private boolean serverCreateMode;

    public List<RemoteDataSourceConfiguration> getRemoteDataSourceConfigurations() {
        return remoteDataSourceConfigurations;
    }

    public void setRemoteDataSourceConfigurations(List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations) {
        this.remoteDataSourceConfigurations = remoteDataSourceConfigurations;
        firePropertyChange("remoteDataSourceConfigurations", null, remoteDataSourceConfigurations);
    }

    public List<ServerDataSourceConfiguration> getServerDataSourceConfigurations() {
        return serverDataSourceConfigurations;
    }

    public void setServerDataSourceConfigurations(List<ServerDataSourceConfiguration> serverDataSourceConfigurations) {
        this.serverDataSourceConfigurations = serverDataSourceConfigurations;
        firePropertyChange("serverDataSourceConfigurations", null, serverDataSourceConfigurations);
    }

    public RemoteDataSourceConfiguration getRemoteDataSourceConfiguration() {
        return remoteDataSourceConfiguration;
    }

    public void setRemoteDataSourceConfiguration(RemoteDataSourceConfiguration remoteDataSourceConfiguration) {
        RemoteDataSourceConfiguration oldValue = getRemoteDataSourceConfiguration();
        this.remoteDataSourceConfiguration = remoteDataSourceConfiguration;
        firePropertyChange("remoteDataSourceConfiguration", oldValue, remoteDataSourceConfiguration);
    }

    public ServerDataSourceConfiguration getServerDataSourceConfiguration() {
        return serverDataSourceConfiguration;
    }

    public void setServerDataSourceConfiguration(ServerDataSourceConfiguration serverDataSourceConfiguration) {
        ServerDataSourceConfiguration oldValue = getServerDataSourceConfiguration();
        this.serverDataSourceConfiguration = serverDataSourceConfiguration;
        firePropertyChange("serverDataSourceConfiguration", oldValue, serverDataSourceConfiguration);
    }

    public boolean isRemoteModified() {
        return remoteModified;
    }

    public void setRemoteModified(boolean remoteModified) {
        this.remoteModified = remoteModified;
        firePropertyChange("remoteModified", null, remoteModified);
    }

    public boolean isServerModified() {
        return serverModified;
    }

    public void setServerModified(boolean serverModified) {
        this.serverModified = serverModified;
        firePropertyChange("serverModified", null, serverModified);
    }

    public boolean isRemoteCreateMode() {
        return remoteCreateMode;
    }

    public void setRemoteCreateMode(boolean remoteCreateMode) {
        this.remoteCreateMode = remoteCreateMode;
        firePropertyChange("remoteCreateMode", null, remoteCreateMode);
    }

    public boolean isServerCreateMode() {
        return serverCreateMode;
    }

    public void setServerCreateMode(boolean serverCreateMode) {
        this.serverCreateMode = serverCreateMode;
        firePropertyChange("serverCreateMode", null, serverCreateMode);
    }
}
