package fr.ird.observe.client.datasource.presets.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.presets.RemotePresetsUI;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.dto.presets.ServerDataSourceConfiguration;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created by tchemit on 14/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CancelServer extends ServerActionSupport {

    public CancelServer() {
        super(n("observe.ui.action.cancel"), n("observe.ui.action.cancel.server.tip"), "cancel", ObserveKeyStrokesSupport.KEY_STROKE_RESET);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, RemotePresetsUI ui) {

        ServerDataSourceConfiguration configuration = ui.getModel().getServerDataSourceConfiguration();

        List<ServerDataSourceConfiguration> configurations = new ArrayList<>(ui.getModel().getServerDataSourceConfigurations());
        configurations.remove(configuration);
        ui.getModel().setServerDataSourceConfigurations(configurations);
        getClientConfig().removeServerDataSourceConfiguration(configuration);
        ui.getModel().setServerDataSourceConfiguration(null);
        ui.getModel().setServerCreateMode(false);
    }
}
