package fr.ird.observe.client.datasource.presets.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.presets.RemotePresetsUI;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.dto.presets.ServerDataSourceConfiguration;
import io.ultreia.java4all.decoration.Decorator;

import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 17/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DeleteServer extends ServerActionSupport {

    public DeleteServer() {
        super(n("observe.ui.action.delete"), n("observe.ui.action.delete.server.tip"), "delete", ObserveKeyStrokesSupport.KEY_STROKE_DELETE_DATA_GLOBAL);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, RemotePresetsUI ui) {

        Decorator decorator = getDecoratorService().getDecoratorByType(ServerDataSourceConfiguration.class);
        ServerDataSourceConfiguration configuration = ui.getModel().getServerDataSourceConfiguration();

        int response = askToUser(t("observe.ui.datasource.storage.presets.delete.title"),
                               t("observe.ui.datasource.storage.presets.delete.message", decorator.decorate(configuration)),
                               JOptionPane.QUESTION_MESSAGE,
                               new Object[]{t("observe.ui.action.delete"), t("observe.ui.action.cancel")},
                               0
        );
        boolean delete = response == 0;

        if (delete) {

            List<ServerDataSourceConfiguration> configurations = new ArrayList<>(ui.getModel().getServerDataSourceConfigurations());
            configurations.remove(configuration);
            ui.getModel().setServerDataSourceConfigurations(configurations);
            getClientConfig().removeServerDataSourceConfiguration(configuration);
            ui.getModel().setServerDataSourceConfiguration(null);
            int newConfiguration = configurations.isEmpty() ? -1 : 0;
            ui.getServerConfigurations().setSelectedIndex(newConfiguration);
        }

    }


}
