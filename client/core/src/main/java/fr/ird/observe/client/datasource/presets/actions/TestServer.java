package fr.ird.observe.client.datasource.presets.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.presets.RemotePresetsUI;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.service.AnonymousService;
import io.ultreia.java4all.util.Version;

import java.awt.event.ActionEvent;
import java.net.MalformedURLException;
import java.net.URL;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 17/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TestServer extends ServerActionSupport {

    public TestServer() {
        super(n("observe.ui.action.test.server"), n("observe.ui.action.test.server.tip"), "connect_creating", 'V');
    }

    @Override
    protected void doActionPerformed(ActionEvent event, RemotePresetsUI ui) {

        String connexionStatusError = null;
        ObserveDataSourceConfigurationRest config = new ObserveDataSourceConfigurationRest();
        String url = ui.getServerUrl().getText().trim();
        try {
            new URL(url);
        } catch (MalformedURLException e) {
            connexionStatusError = t("observe.ui.datasource.storage.error.badUrl", url);
        }
        config.setUrl(url);
        config.setLogin(ui.getServerLogin().getText().trim());
        config.setPassword(ui.getServerPassword().getText().trim().toCharArray());
        String databaseName = ui.getServerDataBase().getText().trim();
        config.setDatabaseName(databaseName.isEmpty() ? null : databaseName);

        ClientConfig applicationConfig = getClientConfig();
        Version applicationVersion = applicationConfig.getVersion();
        Version modelVersion = applicationConfig.getModelVersion();
        ObserveSwingDataSource dataSource = getDataSourcesManager().newDataSource(config);
        try {
            AnonymousService pingService = dataSource.getAnonymousService();
            if (applicationConfig.isCheckServerVersion()) {
                Version serverVersion = pingService.getServerVersion();
                if (!applicationVersion.equals(serverVersion)) {
                    connexionStatusError = t("observe.ui.datasource.storage.error.serverVersionMismatch", serverVersion, applicationVersion);
                }
            }
            if (connexionStatusError == null) {
                Version modelServerVersion = pingService.getModelVersion();
                if (!modelVersion.equals(modelServerVersion)) {
                    connexionStatusError = t("observe.ui.datasource.storage.error.serverVersionModelMismatch", modelServerVersion, modelVersion);
                }
            }
            if (connexionStatusError == null) {
                ObserveDataSourceInformation dataSourceInformation = dataSource.checkCanConnect(false);
                Version versionDataSource = dataSourceInformation.getVersion();
                // en mise a jour de la base on ne test pas la version
                if (!modelVersion.equals(versionDataSource)) {
                    connexionStatusError = t("observe.ui.datasource.storage.error.dbVersionMismatch", versionDataSource, modelVersion);
                }
            }
        } catch (Exception e) {
            connexionStatusError = e.getMessage();
            if (connexionStatusError == null || connexionStatusError.isEmpty()) {
                connexionStatusError = e.getClass().getName();
            }
        } finally {
            try {
                if (dataSource.isOpen()) {
                    dataSource.close();
                }
            } finally {
                if (connexionStatusError == null) {
                    displayInfo(t("observe.ui.datasource.storage.config.test.title"), t("observe.ui.datasource.storage.config.test.message.success"));
                } else {
                    displayWarning(t("observe.ui.datasource.storage.config.test.title"), t("observe.ui.datasource.storage.config.test.message.failure", connexionStatusError));
                }
            }
        }
    }
}
