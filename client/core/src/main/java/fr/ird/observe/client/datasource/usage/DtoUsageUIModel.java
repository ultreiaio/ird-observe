package fr.ird.observe.client.datasource.usage;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 11/02/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class DtoUsageUIModel<D extends BusinessDto> {

    private final Class<D> dtoType;
    private final SingletonSupplier<Collection<ToolkitIdLabel>> dataSupplier;
    private final DecoratorService decoratorService;

    public DtoUsageUIModel(Class<D> dtoType, SingletonSupplier<Collection<ToolkitIdLabel>> dataSupplier, DecoratorService decoratorService) {
        this.dtoType = dtoType;
        this.dataSupplier = dataSupplier;
        this.decoratorService = decoratorService;
    }

    public Collection<ToolkitIdLabel> decorate(Collection<ToolkitIdLabel> collection) {
        decoratorService.installToolkitIdLabelDecorator(dtoType, collection.stream());
        return collection;
    }

    public Class<D> getDtoType() {
        return dtoType;
    }

    public Collection<ToolkitIdLabel> getData() {
        return decorate(dataSupplier.get());
    }

    public List<String> getDataText() {
        return getData().stream().map(Object::toString).sorted().collect(Collectors.toList());
    }

    public boolean isLoaded() {
        return dataSupplier.withValue();
    }
}
