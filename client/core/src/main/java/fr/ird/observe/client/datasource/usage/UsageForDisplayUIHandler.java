package fr.ird.observe.client.datasource.usage;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.UsageCountWithLabel;
import fr.ird.observe.services.service.data.OpenableService;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.util.SingletonSupplier;

import javax.swing.JOptionPane;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 16/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1
 */
public class UsageForDisplayUIHandler extends UsageUIHandlerSupport<UsageForDisplayUI> {

    public static <O extends OpenableDto> void showCompositionDataUsages(OpenableService usageService,
                                                                         Class<O> dtoType,
                                                                         Collection<? extends ToolkitIdDtoBean> request,
                                                                         UsageCount usages) {
        String type = ObserveBusinessProject.get().getLongTitle(dtoType);
        String message = t("observe.ui.message.show.data.compositions.usages", type);
        String title = t("observe.ui.title.show.data.compositions.usage");
        showDataUsages(message, title, usageService, dtoType, request, usages,true);
    }

    public static <O extends OpenableDto> void showAggregationDataUsages(OpenableService usageService,
                                                                         Class<O> dtoType,
                                                                         Collection<? extends ToolkitIdDtoBean> request,
                                                                         UsageCount usages) {
        String type = ObserveBusinessProject.get().getLongTitle(dtoType);
        String message = t("observe.ui.message.show.data.aggregations.usages", type);
        String title = t("observe.ui.title.show.data.aggregations.usage");
        showDataUsages(message, title, usageService, dtoType, request, usages, false);
    }

    static <O extends OpenableDto> void showDataUsages(String message,
                                                       String title,
                                                       OpenableService usageService,
                                                       Class<O> dtoType,
                                                       Collection<? extends ToolkitIdDtoBean> request,
                                                       UsageCount usages,
                                                       boolean mandatory) {

        UsageCountWithLabel realUsages = new UsageCountWithLabel(I18n.getDefaultLocale(), ObserveBusinessProject.get(), usages);
        UsagesGetter getter = new UsagesGetter() {
            @Override
            public UsageCountWithLabel getCount() {
                return realUsages;
            }

            @Override
            public <D extends BusinessDto> SingletonSupplier<Collection<ToolkitIdLabel>> getUsages(Class<D> targetType) {
                if (mandatory) {
                    return SingletonSupplier.of(() -> usageService.getMandatoryDependencies(dtoType, request.stream().map(ToolkitId::getId).collect(Collectors.toSet()), targetType));
                }
                return SingletonSupplier.of(() -> usageService.getOptionalDependencies(dtoType, request.stream().map(ToolkitId::getId).collect(Collectors.toSet()), targetType));
            }
        };
        UsageForDisplayUI usagesUI = UsageForDisplayUI.build(message, getter);
        usagesUI.getUsagesPanel().setBorder(null);
        Object[] options = {t("observe.ui.choice.quit")};
        JOptionPane pane = new JOptionPane(usagesUI, JOptionPane.INFORMATION_MESSAGE,
                                           JOptionPane.DEFAULT_OPTION, null,
                                           options, options[0]);

        usagesUI.getHandler().setUISize(pane);
        usagesUI.getHandler().askToUser(pane, title, options);
    }

    public static boolean showMissingReferential(String message,
                                                 Map<Class<? extends ReferentialDto>, Set<ToolkitIdLabel>> usages,
                                                 String targetSourceLabel) {
        UsageCount count = new UsageCount();
        for (Class<? extends ReferentialDto> dtoType : usages.keySet()) {
            count.put(dtoType, (long) usages.get(dtoType).size());
        }
        UsageCountWithLabel realUsages = new UsageCountWithLabel(I18n.getDefaultLocale(), ObserveBusinessProject.get(), count);

        UsagesGetter getter = new UsagesGetter() {
            @Override
            public UsageCountWithLabel getCount() {
                return realUsages;
            }

            @SuppressWarnings("unchecked")
            @Override
            public <D extends BusinessDto> SingletonSupplier<Collection<ToolkitIdLabel>> getUsages(Class<D> dtoType) {
                return SingletonSupplier.of(() -> usages.get((Class<? extends ReferentialDto>) dtoType));
            }
        };
        UsageForDisplayUI usagesUI = UsageForDisplayUI.build(message, getter);

        String[] options = {
                t("observe.ui.choice.confirm.insert"),
                t("observe.ui.choice.cancel")};
        JOptionPane pane = new JOptionPane(usagesUI, JOptionPane.WARNING_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, options[0]);

        usagesUI.getHandler().setUISize(pane);
        String title = t("observe.ui.datasource.editor.actions.data.import.title.require.add.missing.referential", targetSourceLabel);
        int response = usagesUI.getHandler().askToUser(pane, title, options);
        return response == 0;

    }

    @Override
    protected UsagePanel getUsages() {
        return ui.getUsagesPanel();
    }

    @Override
    protected FilterableComboBox<ToolkitIdLabel> getReplace() {
        return null;
    }

}
