package fr.ird.observe.client.datasource.usage;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.util.DisabledItemSelectionModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.services.service.UsageCountWithLabel;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.util.SingletonSupplier;
import org.jdesktop.swingx.JXTitledPanel;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 16/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1
 */
public abstract class UsageUIHandlerSupport<U extends JXTitledPanel & JAXXObject> implements UIHandler<U>, WithClientUIContextApi {

    protected U ui;

    public static JButton findButton(Container c, String text) {

        for (Component component : c.getComponents()) {
            if (component instanceof JButton) {
                if (text.equals(((JButton) component).getText())) {
                    return (JButton) component;
                }
                continue;
            }
            if (component instanceof Container) {
                JButton button = findButton((Container) component, text);
                if (button != null) {
                    return button;
                }
            }
        }
        return null;
    }

    protected abstract UsagePanel getUsages();

    protected abstract FilterableComboBox<ToolkitIdLabel> getReplace();

    @Override
    public void beforeInit(U ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(U ui) {

        JList<? super String> l = getUsages().getDataList();
        l.setSelectionModel(new DisabledItemSelectionModel());

        String message = ui.getContextValue(String.class);
        ui.setTitle(message);
        UsagesGetter usagesGetter = Objects.requireNonNull(ui.getContextValue(UsagesGetter.class));
        DecoratorService decoratorService = getDecoratorService();
        if (usagesGetter.isEmpty()) {
            getUsages().removeAll();
            getUsages().add(new JLabel(t("observe.ui.message.no.usage.for.entity")));
        } else {

            ButtonGroup buttonGroup = new ButtonGroup();
            UsageCountWithLabel usages = usagesGetter.getCount();
            for (Class<? extends BusinessDto> dtoType : usages.keySet()) {
                buildUsagePanel(buttonGroup, dtoType, usages, usagesGetter, decoratorService);
            }
            JToggleButton component = (JToggleButton) getUsages().getUsagesCount().getComponent(0);
            component.doClick();
        }

        FilterableComboBox<ToolkitIdLabel> comboBox = getReplace();
        if (comboBox != null) {
            @SuppressWarnings("unchecked") List<ToolkitIdLabel> references = ui.getContextValue(List.class);
            Class<?> dtoType = ui.getContextValue(Class.class);
            comboBox.setBeanType(ToolkitIdLabel.class);
            comboBox.setI18nPrefix("observe.common.");
            comboBox.setShowDecorator(false);
            Decorator decorator = decoratorService.getToolkitIdLabelDecoratorByType(dtoType);
            comboBox.init(decorator, references);
        }
    }

    public void attachToOptionPane(JOptionPane pane, String message) {
        JButton jButton = findButton(pane, message);
        Objects.requireNonNull(jButton);
        jButton.setEnabled(false);
        ui.addPropertyChangeListener("canApply", evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            jButton.setEnabled(Objects.equals(true, newValue));
        });
    }

    public void setUISize(JOptionPane pane) {
        Dimension size = getMainUI().getSize();
        Dimension thisSize = new Dimension((int) size.getWidth() - 50, (int) size.getHeight() - 50);
        pane.setPreferredSize(thisSize);
        pane.putClientProperty(UIHelper.NO_PACK, true);
        pane.putClientProperty(UIHelper.FIX_SIZE, true);
    }

    private <D extends BusinessDto> void buildUsagePanel(ButtonGroup buttonGroup, Class<D> dtoType, UsageCountWithLabel usages, UsagesGetter usagesGetter, DecoratorService decoratorService) {
        SingletonSupplier<Collection<ToolkitIdLabel>> factory = usagesGetter.getUsages(dtoType);
        String showText = usages.getLabelWithCount(dtoType);

        DtoUsageUIModel<D> model = new DtoUsageUIModel<>(dtoType, factory, decoratorService);
        JToggleButton button = new JToggleButton();
        button.setText(showText);
        button.getModel().setGroup(buttonGroup);
        button.addActionListener(e -> {
            if (button.isSelected()) {
                List<String> rrs = model.getDataText();
                getUsages().getDataList().setListData(rrs.toArray(new String[0]));
            }
        });
        getUsages().getUsagesCount().add(button);
    }

}
