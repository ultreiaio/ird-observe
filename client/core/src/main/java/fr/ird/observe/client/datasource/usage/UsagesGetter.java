package fr.ird.observe.client.datasource.usage;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.services.service.UsageCountWithLabel;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.Collection;

/**
 * Created on 10/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public interface UsagesGetter {

    UsageCountWithLabel getCount();

    <D extends BusinessDto> SingletonSupplier<Collection<ToolkitIdLabel>> getUsages(Class<D> dtoType);

    default boolean isEmpty() {
        return getCount().isEmpty();
    }
}
