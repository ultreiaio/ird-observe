package fr.ird.observe.client.datasource.validation;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.ClientUIContextApi;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.dto.referential.common.SpeciesDto;
import fr.ird.observe.dto.referential.ps.common.ObservedSystemReference;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditorModel;
import io.ultreia.java4all.jaxx.widgets.validation.JaxxWidgetsValidationContext;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditorModel;

import java.io.Closeable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */

public class ClientValidationContext extends GeneratedClientValidationContext implements Closeable, WithClientUIContextApi, JaxxWidgetsValidationContext {

    private final Map<String, CoordinatesEditor> coordinatesEditors;
    private final Map<String, TemperatureEditorModel> temperatureEditorModels;
    private final Map<String, NauticalLengthEditorModel> nauticalLengthEditorModels;
    private final ObserveDataSourcesManager dataSourcesManager;

    public ClientValidationContext(ClientUIContextApi clientUIContext) {
        super(clientUIContext.getClientConfig().toValidationRequestConfiguration(), clientUIContext.getDecoratorService(), clientUIContext.getObserveSelectModel());
        this.dataSourcesManager = clientUIContext.getDataSourcesManager();
        this.temperatureEditorModels = new TreeMap<>();
        this.nauticalLengthEditorModels = new TreeMap<>();
        this.coordinatesEditors = new TreeMap<>();
    }

    @Override
    public fr.ird.observe.services.service.data.ll.common.TripService getLlCommonTripService() {
        return getServicesProvider().getLlCommonTripService();
    }

    @Override
    public fr.ird.observe.services.service.data.ps.common.TripService getPsCommonTripService() {
        return getServicesProvider().getPsCommonTripService();
    }

    @Override
    public final ObserveServicesProvider getServicesProvider() {
        return dataSourcesManager.getMainDataSource();
    }

    @Override
    protected Function<String, SpeciesDto> speciesFunction() {
        return id -> getServicesProvider().getReferentialService().loadDto(SpeciesDto.class, id);
    }

    @Override
    protected Supplier<List<ObservedSystemReference>> psCommonObservedSystemSupplier() {
        return () -> getServicesProvider().getReferentialService().getReferenceSet(ObservedSystemReference.class, null).toList();
    }

    @Override
    protected Supplier<List<String>> currentLlLogbookCatchSpeciesIdSupplier() {
        return () -> {
            TripDto dto = getCurrentLlCommonTrip();
            return dto == null ? List.of() : getServicesProvider().getLlCommonTripService().getLogbookCatchSpeciesIds(dto.getId());
        };
    }

    @Override
    public void reset() {
        super.reset();
        coordinatesEditors.clear();
        temperatureEditorModels.clear();
        nauticalLengthEditorModels.clear();
    }

    @Override
    public Map<String, CoordinatesEditor> getCoordinatesEditors() {
        return coordinatesEditors;
    }


    @Override
    public Map<String, TemperatureEditorModel> getTemperatureEditorModels() {
        return temperatureEditorModels;
    }


    @Override
    public Map<String, NauticalLengthEditorModel> getNauticalLengthEditorModels() {
        return nauticalLengthEditorModels;
    }
}
