package fr.ird.observe.client.datasource.validation;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableModel;

import javax.swing.JComponent;
import java.util.List;

/**
 * Created on 07/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentMessageTableModel extends SwingValidatorMessageTableModel {

    private static final long serialVersionUID = 1L;

    @Override
    public void addMessages(SwingValidator<?> validator, String fieldName, NuitonValidatorScope scope, String... messages) {
        if (validator != null && validator.getFieldRepresentation(fieldName) == null) {
            //FIXME Move this to jaxx, in fact we are receiving messages from this validator but with no ui associated
            //FIXME Improve generated validation files to be able to deal with context
            return;
        }
        super.addMessages(validator, fieldName, scope, messages);
    }

    public List<SwingValidatorMessage> getData() {
        return data;
    }

    @Override
    protected void addMessages(SwingValidator<?> validator, String fieldName, NuitonValidatorScope scope, boolean sort, String... messages) {
        if (validator != null && validator.getFieldRepresentation(fieldName) == null) {
            //FIXME Move this to jaxx, in fact we are receiving messages from this validator but with no ui associated
            //FIXME Improve generated validation files to be able to deal with context
            return;
        }
        super.addMessages(validator, fieldName, scope, sort, messages);
    }

    @Override
    public void removeMessages(JComponent editor, NuitonValidatorScope scope) {
        if (scope == null) {
            // on supprime tout sans aucun calcul
            int nb = getRowCount();
            if (nb > 0) {
                data.clear();
                fireTableRowsDeleted(0, nb - 1);
            }
        } else {
            super.removeMessages(editor, scope);
        }
    }
}
