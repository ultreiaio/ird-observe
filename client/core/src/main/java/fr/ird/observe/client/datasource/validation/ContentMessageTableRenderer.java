package fr.ird.observe.client.datasource.validation;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducer;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableRenderer;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Component;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 12/13/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class ContentMessageTableRenderer extends SwingValidatorMessageTableRenderer {

    private static final long serialVersionUID = 1L;

    private final BeanPropertyI18nKeyProducer labelsBuilder;
    private final Color defaultBackgroundColor;
    private final Color alternateBackgroundColor;

    public ContentMessageTableRenderer() {
        labelsBuilder = I18nDecoratorHelper.get().getDefaultLabelsBuilder();
        defaultBackgroundColor = UIManager.getColor("List.background");
//        alternateBackgroundColor = new Color(242, 242, 242);
        alternateBackgroundColor = UIManager.getColor("Table.alternateRowColor");
    }

    @Override
    public String getFieldName(JTable table, String value, int row) {
        Objects.requireNonNull(value, "Value can not be null!");
        ContentMessageTableModel tableModel = (ContentMessageTableModel) table.getModel();
        SwingValidatorMessage model = tableModel.getRow(row);
        JComponent editor = model.getEditor();
        if (editor != null) {
            String validatorLabel = (String) editor.getClientProperty("validatorLabel");
            if (validatorLabel != null) {
//                log.warn("Using deprecated validatorLabel : " + validatorLabel);
                return t(validatorLabel);
            }
            Boolean doNotTranslateFieldName = (Boolean) editor.getClientProperty("doNotTranslateFieldName");
            if (Objects.equals(true, doNotTranslateFieldName)) {
                return value;
            }
        }
        if (value.startsWith("observe.") || model.getValidator() == null) {
            return t(value);
        }
        return t(labelsBuilder.getI18nPropertyKey(model.getValidator().getType(), value));
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component result = super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
        if (!isSelected) {
            if (row % 2 == 1) {
                result.setBackground(alternateBackgroundColor);
            } else {
                result.setBackground(defaultBackgroundColor);
            }
            ContentMessageTableModel model = (ContentMessageTableModel) table.getModel();
            NuitonValidatorScope scope = (NuitonValidatorScope) (column == 0 ? value : model.getValueAt(row, 0));
            Color textColor = scope == NuitonValidatorScope.WARNING ? Color.RED : Color.BLACK;
            result.setForeground(textColor);
        }
        return result;
    }
}
