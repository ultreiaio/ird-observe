/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.validation;

import fr.ird.observe.client.ClientUIContext;
import fr.ird.observe.client.ClientUIContextApplicationComponent;
import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.nuiton.jaxx.validator.swing.SwingValidator;

/**
 * Une surcharge du validateur swing offert par jaxx pour pouvoir ajouter dans
 * la stack le context (pour faire de la validation sur le context de
 * données d'un niveau supérieur (valider une marée à partir d'une route par
 * exemple).
 *
 * @param <B> le type d'objet a valider
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ObserveSwingValidator<B> extends SwingValidator<B> {

    private ObserveSwingValidator(NuitonValidatorProvider provider, Class<B> type, String context, NuitonValidatorScope... scopes) {
        super(provider, type, context, scopes);
    }

    public static <B> ObserveSwingValidator<B> newValidator(Class<B> type, String context, NuitonValidatorScope... scopes) {
        @SuppressWarnings("resource") ClientUIContext clientUIContext = ClientUIContextApplicationComponent.value();
        return new ObserveSwingValidator<>(clientUIContext.getValidationProvider(), type, context, scopes);
    }

}
