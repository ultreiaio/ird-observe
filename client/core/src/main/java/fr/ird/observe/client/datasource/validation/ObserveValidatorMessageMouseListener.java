package fr.ird.observe.client.datasource.validation;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.main.focus.FocusDispatcher;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableMouseListener;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.Objects;

/**
 * Created on 06/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveValidatorMessageMouseListener extends SwingValidatorMessageTableMouseListener {
    FocusDispatcher focusDispatcher = new FocusDispatcher();

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {

            SwingValidatorMessage entry = getSelectedMessage(e);
            if (entry == null) {
                // no entry found
                return;
            }
            pcs.firePropertyChange(HIGHLIGHT_ERROR_PROPERTY, null, entry);
            JComponent component = entry.getEditor();
            if (component == null) {
                return;
            }
            if (component instanceof CoordinatesEditor) {
                CoordinatesEditor editor = (CoordinatesEditor) component;
                String field = entry.getField();
                if (field.toLowerCase().contains("quadrant")) {
                    component= editor.getQuadrant1();
                } else if (field.toLowerCase().contains("latitude")) {
                    switch(editor.getModel().getFormat()) {
                        case dd:
                            component = editor.getLatitudeDd().getEditor();
                            break;
                        case dms:
                            component = editor.getLatitudeDms().getEditor();
                            break;
                        case dmd:
                            component = editor.getLatitudeDmd().getEditor();
                            break;
                    }
                } else if (field.toLowerCase().contains("longitude")) {
                    switch(editor.getModel().getFormat()) {
                        case dd:
                            component = editor.getLongitudeDd().getEditor();
                            break;
                        case dms:
                            component = editor.getLongitudeDms().getEditor();
                            break;
                        case dmd:
                            component = editor.getLongitudeDmd().getEditor();
                            break;
                    }
                }
            }
            JComponent realFocusComponent = focusDispatcher.dispatchFocus(component);
            if (realFocusComponent == null) {
                return;
            }
            if (!Objects.equals(component, realFocusComponent)) {
                component = realFocusComponent;
            }
            doFocus(component);
        }
    }

    protected void doFocus(JComponent component) {
        if (!component.isShowing()) {
            JTabbedPane ancestor = (JTabbedPane) SwingUtilities.getAncestorOfClass(JTabbedPane.class, component);
            if (ancestor != null) {
                int index = 0;
                for (Component ancestorComponent : ancestor.getComponents()) {
                    if (SwingUtilities.isDescendingFrom(component, ancestorComponent)) {
                        ancestor.setSelectedIndex(index);
                        break;
                    }
                    index++;
                }
            }
        }
        component.requestFocusInWindow();
    }
}
