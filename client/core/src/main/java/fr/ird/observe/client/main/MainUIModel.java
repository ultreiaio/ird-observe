package fr.ird.observe.client.main;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.main.focus.MainUIFocusModel;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.awt.KeyboardFocusManager;
import java.util.Locale;
import java.util.Objects;

@GenerateJavaBeanDefinition
public class MainUIModel extends AbstractJavaBean {
    /**
     * Busy model.
     */
    private final BusyModel busyModel;
    /**
     * Focus model.
     */
    private final MainUIFocusModel focusModel;
    private final ClientConfig clientConfig;
    /**
     * Is file menu enabled?
     */
    private boolean fileEnabled;
    /**
     * Is a feed back was generated in this session.
     */
    private boolean openLastFeedBackEnabled;
    /**
     * Is storage menu enabled?
     */
    private boolean storageEnabled;
    /**
     * Is actions menu is enabled?
     */
    private boolean actionsEnabled;
    /**
     * Is config menu enabled?
     */
    private boolean configEnabled;
    /**
     * Is change language to french menu enabled?
     */
    private boolean changeLanguageToFrenchEnabled;
    /**
     * Is change language to english menu enabled?
     */
    private boolean changeLanguageToEnglishEnabled;
    /**
     * Is change language to spanish menu enabled?
     */
    private boolean changeLanguageToSpanishEnabled;
    /**
     * Is model in dev mode? (means we should have more controls in gui)
     */
    private boolean devMode;
    /**
     * Is model adjusting? (used internally in the model to avoid re-entrant code while event propagation)
     */
    private boolean adjusting;

    public static boolean acceptBodyType(Class<?> mode, String... modes) {
        return acceptBodyType(mode, true, modes);
    }

    public static boolean acceptBodyType(Class<?> mode, boolean condition, String... modes) {
        if (condition && mode != null) {
            for (String m : modes) {
                if (m.equals(mode.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public MainUIModel(BusyModel busyModel, ClientConfig clientConfig) {
        this.focusModel = new MainUIFocusModel(this.busyModel = Objects.requireNonNull(busyModel), KeyboardFocusManager.getCurrentKeyboardFocusManager());
        this.clientConfig = clientConfig;
        setDevMode(clientConfig.getBuildVersion().isSnapshot());
    }

    public ClientConfig getClientConfig() {
        return clientConfig;
    }

    public boolean isOpenLastFeedBackEnabled() {
        return openLastFeedBackEnabled;
    }

    public void setOpenLastFeedBackEnabled(boolean openLastFeedBackEnabled) {
        boolean oldValue = isOpenLastFeedBackEnabled();
        this.openLastFeedBackEnabled = openLastFeedBackEnabled;
        firePropertyChange("openLastFeedBackEnabled", oldValue, openLastFeedBackEnabled);
    }

    public boolean isActionsEnabled() {
        return actionsEnabled;
    }

    public void setActionsEnabled(boolean actionsEnabled) {
        this.actionsEnabled = actionsEnabled;
        firePropertyChange("actionsEnabled", null, actionsEnabled);
    }

    public boolean isStorageEnabled() {
        return storageEnabled;
    }

    public void setStorageEnabled(boolean storageEnabled) {
        this.storageEnabled = storageEnabled;
        firePropertyChange("storageEnabled", null, storageEnabled);
    }

    public boolean isConfigEnabled() {
        return configEnabled;
    }

    public void setConfigEnabled(boolean configEnabled) {
        this.configEnabled = configEnabled;
        firePropertyChange("configEnabled", null, configEnabled);
    }

    public boolean isFileEnabled() {
        return fileEnabled;
    }

    public void setFileEnabled(boolean fileEnabled) {
        this.fileEnabled = fileEnabled;
        firePropertyChange("fileEnabled", null, fileEnabled);
    }

    public boolean isChangeLanguageToFrenchEnabled() {
        return changeLanguageToFrenchEnabled;
    }

    public void setChangeLanguageToFrenchEnabled(boolean changeApplicationLanguageToFrenchEnabled) {
        this.changeLanguageToFrenchEnabled = changeApplicationLanguageToFrenchEnabled;
        firePropertyChange("changeLanguageToFrenchEnabled", null, changeApplicationLanguageToFrenchEnabled);
    }

    public boolean isChangeLanguageToEnglishEnabled() {
        return changeLanguageToEnglishEnabled;
    }

    public void setChangeLanguageToEnglishEnabled(boolean changeApplicationLanguageToEnglishEnabled) {
        this.changeLanguageToEnglishEnabled = changeApplicationLanguageToEnglishEnabled;
        firePropertyChange("changeLanguageToEnglishEnabled", null, changeApplicationLanguageToEnglishEnabled);
    }

    public boolean isChangeLanguageToSpanishEnabled() {
        return changeLanguageToSpanishEnabled;
    }

    public void setChangeLanguageToSpanishEnabled(boolean changeApplicationLanguageToSpanishEnabled) {
        this.changeLanguageToSpanishEnabled = changeApplicationLanguageToSpanishEnabled;
        firePropertyChange("changeLanguageToSpanishEnabled", null, changeApplicationLanguageToSpanishEnabled);
    }

    public boolean isDevMode() {
        return devMode;
    }

    public void setDevMode(boolean devMode) {
        this.devMode = devMode;
        firePropertyChange("devMode", null, devMode);
    }

    public boolean acceptLocale(Locale l, String expected) {
        return l == null || !l.toString().equals(expected);
    }

    public boolean isAdjusting() {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    public MainUIFocusModel getFocusModel() {
        return focusModel;
    }

    public BusyModel getBusyModel() {
        return busyModel;
    }
}
