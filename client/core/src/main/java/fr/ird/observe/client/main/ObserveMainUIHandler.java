package fr.ird.observe.client.main;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.main.body.MainUIBodyContentManager;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.spi.ui.BusyLayerUI;
import fr.ird.observe.spi.ui.BusyModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.action.JAXXObjectActionSupport;
import org.nuiton.jaxx.widgets.error.ErrorDialogUI;

import javax.swing.JComponent;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Locale;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class ObserveMainUIHandler implements UIHandler<ObserveMainUI>, WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(ObserveMainUIHandler.class);
    protected ObserveMainUI ui;

    @Override
    public void beforeInit(ObserveMainUI ui) {
        this.ui = ui;
        BusyModel busyModel = getBusyModel();
        ClientConfig clientConfig = getClientConfig();
        MainUIModel model = new MainUIModel(busyModel, clientConfig);
        model.addPropertyChangeListener(evt -> {
            if (model.isAdjusting()) {
                return;
            }
            log.debug(String.format("Main ui model property %s changed, reload main ui model.", evt.getPropertyName()));
            reload(ui.getModel(), clientConfig, busyModel);
        });

        ui.setContextValue(busyModel);
        ui.setContextValue(model);
        ui.setContextValue(clientConfig);
        ui.setContextValue(new MainUIBodyContentManager(ui));
        if (log.isDebugEnabled()) {
            ui.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    log.debug(String.format("Main frame size: %d-%d (min: %d-%d)", ui.getSize().width, ui.getSize().height, ui.getMinimumSize().width, ui.getMinimumSize().height));
                }
            });
        }
    }

    @Override
    public void afterInit(ObserveMainUI ui) {
        UIHelper.addApplicationIcon(ui);
        setMainUI(ui);
        ui.getMainUIBodyContentManager().install();
        MainUIModel model = ui.getModel();


        ClientConfig config = getClientConfig();
        String title = t("observe.ui.title.welcome.admin") + (" " + config.getVersion());
        ui.setTitle(title);

        ErrorDialogUI.init(ui);

        BusyModel busyModel = getBusyModel();
        JAXXObjectActionSupport.makeActionsEnabledOnlyIfMenuItemParentIsOpened(ui.getJMenuBar());

        ObserveKeyStrokesSupport.addKeyStrokeFromMnemonic(ui);

        ui.getStatus().init();

        // init blocking layer associated with the busy model
        BusyLayerUI.create(ui, busyModel);

        ui.getMenu().setEnabled(!busyModel.isBusy());

        reload(model, getClientConfig(), busyModel);

        busyModel.addPropertyChangeListener(BusyModel.BUSY_PROPERTY_NAME, evt -> reload(model, config, busyModel));

        ui.getMenuLanguage().disabled();
        ui.getMenuHelp().addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent e) {
                ui.getMenuLanguage().enabled();
            }

            @Override
            public void menuDeselected(MenuEvent e) {
                ui.getMenuLanguage().disabled();
            }

            @Override
            public void menuCanceled(MenuEvent e) {
                ui.getMenuLanguage().disabled();
            }
        });
    }

    public void destroy() {
        ui.getMainUIBodyContentManager().close();
        BusyLayerUI.close(ui);
        UIHelper.destroy(ui);
        ErrorDialogUI.init(null);
        removeMainUI();
    }

    public void reload(MainUIModel model, ClientConfig config, BusyModel busyModel) {

        log.debug(String.format("Reload ui model: %s", model));

        model.setAdjusting(true);

        try {

            reload0(config, model, busyModel);

            //FIXME ???
//            setBusy(isBusy());
        } finally {
            model.setAdjusting(false);
        }
    }

    void reload0(ClientConfig config, MainUIModel model, BusyModel busyModel) {
        boolean notBusy = !busyModel.isBusy();
        Locale applicationLocale = config.getLocale();

        model.setChangeLanguageToEnglishEnabled(model.acceptLocale(applicationLocale, "en_GB"));
        model.setChangeLanguageToFrenchEnabled(model.acceptLocale(applicationLocale, "fr_FR"));
        model.setChangeLanguageToSpanishEnabled(model.acceptLocale(applicationLocale, "es_ES"));
        //FIXME:BodyContent Trouver un moyen de dispatcher cela aux bons endroit via un listener qui permet de modifier ce model...
        Class<? extends JComponent> bodyContentType = ui.getMainUIBodyContentManager().getCurrentBodyType();
        model.setActionsEnabled(MainUIModel.acceptBodyType(bodyContentType, notBusy, "fr.ird.observe.client.datasource.editor.api.DataSourceEditor", "fr.ird.observe.client.main.body.NoBodyContentComponent"));
        model.setFileEnabled(MainUIModel.acceptBodyType(bodyContentType, notBusy, "fr.ird.observe.client.main.body.NoBodyContentComponent", "fr.ird.observe.client.datasource.editor.api.DataSourceEditor"));
        model.setConfigEnabled(MainUIModel.acceptBodyType(bodyContentType, notBusy, "fr.ird.observe.client.datasource.editor.api.DataSourceEditor", "fr.ird.observe.client.main.body.NoBodyContentComponent"));
    }

}
