package fr.ird.observe.client.main.actions;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.body.NoBodyContentComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.util.Locale;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public abstract class ChangeLanguageActionSupport extends MainUIActionSupport {

    private static final Logger log = LogManager.getLogger(ChangeLanguageActionSupport.class);
    private final Locale newLocale;

    ChangeLanguageActionSupport(Locale newLocale, String name, String description, char mnemonic) {
        super(name, description, "i18n-" + newLocale.getLanguage(), mnemonic);
        this.newLocale = newLocale;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ObserveMainUI ui) {

        log.info("ObServe changing application language...");
        boolean canContinue = ui.changeBodyContent(NoBodyContentComponent.class);
//        boolean canContinue = ContentUIManagerApplicationComponent.value().closeSelectedContentUI();
        if (canContinue) {
            ClientConfig config = getClientConfig();

            config.setLocale(newLocale);
            config.setDbLocale(newLocale);
            config.saveForUser();
            getUiCallbackManager().run("application");
        }

    }

}
