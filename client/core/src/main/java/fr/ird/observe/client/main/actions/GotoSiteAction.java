package fr.ird.observe.client.main.actions;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.net.URL;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public class GotoSiteAction extends MainUIActionSupport {

    private static final Logger log = LogManager.getLogger(GotoSiteAction.class);

    public GotoSiteAction() {
        super(t("observe.ui.action.site"), t("observe.ui.action.site.tip"), "site", 'S');
    }

    @Override
    protected void doActionPerformed(ActionEvent event, ObserveMainUI ui) {

        ClientConfig config = getClientConfig();

        URL siteURL = config.getApplicationSiteUrl();

        displayInfo(t("observe.ui.message.goto.site", siteURL));

        log.info("goto " + siteURL);
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(siteURL.toURI());
            } catch (Exception ex) {
                throw new ObserveSwingTechnicalException(ex);
            }
        }
    }

}
