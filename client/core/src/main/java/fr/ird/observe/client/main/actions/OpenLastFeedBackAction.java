package fr.ird.observe.client.main.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.util.UIHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.io.File;
import java.nio.file.Files;

import static io.ultreia.java4all.i18n.I18n.t;

public class OpenLastFeedBackAction extends MainUIActionSupport {

    private static final Logger log = LogManager.getLogger(OpenLastFeedBackAction.class);

    public OpenLastFeedBackAction() {
        super(t("observe.ui.action.open.last.feedback"), t("observe.ui.action.open.last.feedback.tip"), "about", 'F');
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        return super.canExecuteAction(e) && getUi().getModel().isOpenLastFeedBackEnabled();
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ObserveMainUI mainUI) {
        File feedBackDirectoryFile = getClientConfig().getFeedBackDirectoryFile();
        if (Files.notExists(feedBackDirectoryFile.toPath())) {
            log.warn(String.format("Feedback directory: %s does not exist.", feedBackDirectoryFile));
        } else {
            UIHelper.openLink(feedBackDirectoryFile.toURI());
        }
    }
}
