package fr.ird.observe.client.main.actions;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.ObserveSwingApplicationContext;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.main.body.NoBodyContentComponent;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.server.security.InvalidAuthenticationTokenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.application.ApplicationRunner;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public class ReloadApplicationAction extends CloseApplicationAction {

    private static final Logger log = LogManager.getLogger(ReloadApplicationAction.class);

    public ReloadApplicationAction() {
        super(t("observe.ui.action.restart.application"), t("observe.ui.action.restart.application.tip"), "application-reload", 'R');
    }

    @Override
    public void run() {
        log.info("ObServe reloading...");

        boolean canContinue = ui.changeBodyContent(NoBodyContentComponent.class);
        if (!canContinue) {
            return;
        }

        ObserveSwingDataSource mainDataSource = getDataSourcesManager().getMainDataSource();
        if (mainDataSource != null) {
            ObserveDataSourceConfiguration mainDataSourceConfiguration = mainDataSource.getConfiguration();
            if (mainDataSource.isOpen()) {
                try {
                    mainDataSource.close();
                } catch (InvalidAuthenticationTokenException e) {
                    // If token is expired, then still will try to reopen this data source after all
                } catch (Exception e) {
                    UIHelper.handlingError(e);
                    // In this case, won't try to reopen it
                    mainDataSourceConfiguration = null;
                }
            }
            if (getClientConfig().isLoadLocalStorage()) {
                ObserveSwingApplicationContext.setDataSourceToReload(mainDataSourceConfiguration);
            }
        }
        ApplicationRunner.getRunner().setReload(true);

        super.run();
    }
}
