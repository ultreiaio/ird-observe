package fr.ird.observe.client.main.actions;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.body.NoBodyContentComponent;
import io.ultreia.java4all.i18n.I18n;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class ReloadResourcesAction extends MainUIActionSupport {

    private static final Logger log = LogManager.getLogger(ReloadResourcesAction.class);

    public ReloadResourcesAction() {
        super(t("observe.ui.action.reloadResources"), t("observe.ui.action.reloadResources.tip"), "application-reload", 'R');
    }

    @Override
    protected void doActionPerformed(ActionEvent event, ObserveMainUI ui) {
        log.info("ObServe reloading resources...");
        boolean canContinue = ui.changeBodyContent(NoBodyContentComponent.class);
        if (canContinue) {
            ClientConfig config = getClientConfig();
            File directory = config.getResourcesDirectory();
            log.info(t("observe.ui.message.delete.directory", directory));
            // suppression du répertoire
            try {
                FileUtils.deleteDirectory(directory);
            } catch (IOException e) {
                log.error("Could not delete resources directory: " + directory, e);
            }
            I18n.reload();
            getClientConfig().loadUIConfig();
            getUiCallbackManager().run("ui");
        }
    }
}
