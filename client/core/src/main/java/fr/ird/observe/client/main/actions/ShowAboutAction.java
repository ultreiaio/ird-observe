package fr.ird.observe.client.main.actions;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.configuration.ClientConfigTemplate;
import fr.ird.observe.client.main.ObserveMainUI;
import io.ultreia.java4all.config.ConfigResource;
import org.nuiton.jaxx.widgets.about.AboutUI;
import org.nuiton.jaxx.widgets.about.AboutUIBuilder;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class ShowAboutAction extends MainUIActionSupport {

    /**
     * Retourne l'icone demandee.
     *
     * @param name le nom de l'icone
     * @return Retourne l'icon demande ou null s'il n'est pas trouvé
     */
    public static ImageIcon getIcon(String name) {
        return new ImageIcon(Objects.requireNonNull(ShowAboutAction.class.getResource(name)));
    }

    public ShowAboutAction() {
        super(t("observe.ui.action.about"), t("observe.ui.action.about.tip"), "about", 'A');
    }

    @Override
    protected void doActionPerformed(ActionEvent event, ObserveMainUI ui) {

        ClientConfig clientConfig = getClientConfig();
        String name = ClientConfig.APPLICATION_NAME;

        String changelog = String.join("\n", new ConfigResource("/META-INF/" + name + "-CHANGELOG.html").readLines());
        String configuration = String.join("\n", new ConfigResource("/META-INF/configuration/" + name + ".html").readLines());

        String aboutContent = ClientConfigTemplate.generate(clientConfig);

        AboutUI about = AboutUIBuilder.builder(ui)
                .setIconPath("/icons/logo.png")
                .setTitle(t("observe.ui.title.about"))
                .setBottomText(clientConfig.getCopyrightText())
                .addAboutTab(aboutContent, true)
                .addDefaultLicenseTab(name, false)
                .addDefaultThirdPartyTab(name, false)
                .addTab(t("aboutframe.changelog"), changelog, true)
                .addTab(t("aboutframe.configuration"), configuration, true)
                .build();

        // tweak top panel
        JPanel topPanel = about.getTopPanel();
        topPanel.removeAll();
        JPanel topPanel2 = new JPanel();
        JPanel topPanel3 = new JPanel();
        topPanel.setLayout(new BorderLayout());
        topPanel2.setLayout(new GridLayout(1, 0));
        topPanel3.setLayout(new GridLayout(1, 0));
        topPanel.add(topPanel2, BorderLayout.CENTER);
        topPanel.add(topPanel3, BorderLayout.SOUTH);

        int width = (int) (about.getOwner().getWidth() * 0.9);
        int height = (int) (about.getOwner().getHeight() * 0.9) / 6;

        topPanel2.add(new JLabel(getIcon("/icons/logo_obs7.png")));
        topPanel2.add(new JLabel(getIcon("/icons/logo_ird.png")));

        topPanel3.add(new JLabel(getImage("/icons/logo_sfa.jpg", width / 4, height)));
        topPanel3.add(new JLabel(getImage("/icons/logo_cro.jpg", width / 4, height)));
        topPanel3.add(new JLabel(getImage("/icons/logo_feamp.png", width / 4, height)));

        about.display();

    }

    private ImageIcon getImage(String image, int width, int height) {
        return new ImageIcon(getIcon(image).getImage().getScaledInstance(width, height, Image.SCALE_DEFAULT));
    }
}
