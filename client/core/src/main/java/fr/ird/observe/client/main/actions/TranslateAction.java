package fr.ird.observe.client.main.actions;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.main.ObserveMainUI;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.editor.model.Project;
import io.ultreia.java4all.i18n.editor.ui.ProjectUI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 09/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TranslateAction extends MainUIActionSupport {

    private static final Logger log = LogManager.getLogger(TranslateAction.class);

    public TranslateAction() {
        super(t("observe.ui.action.translate"), t("observe.ui.action.translate.tip"), "translate", 'T');
    }

    @Override
    protected void doActionPerformed(ActionEvent event, ObserveMainUI ui) {

        ClientConfig config = getClientConfig();
        File i18nDirectory = config.getI18nDirectory();

        Project model = new Project(I18n.getLanguageProvider(), i18nDirectory.toPath());
        ProjectUI projectUI = new ProjectUI(ui, model);
        projectUI.toggleFilter();

        projectUI.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                if (model.getLastExportDate() != null) {
                    log.info("Found existing i18n export, will reload i18n");
                    displayInfo(t("observe.ui.title.reload.i18n"), t("observe.ui.message.reload.i18n"));
                    getUiCallbackManager().run("application");
                }
            }
        });
    }
}
