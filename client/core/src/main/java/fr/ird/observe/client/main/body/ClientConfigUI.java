package fr.ird.observe.client.main.body;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.configuration.ClientConfigOption;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.callback.ObserveUICallback;
import fr.ird.observe.client.main.callback.ObserveUICallbackManager;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.referential.common.SpeciesListReference;
import io.ultreia.java4all.config.spi.ConfigOptionDef;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.nuiton.jaxx.widgets.config.ConfigCategoryUI;
import org.nuiton.jaxx.widgets.config.ConfigUI;
import org.nuiton.jaxx.widgets.config.ConfigUIHelper;
import org.nuiton.jaxx.widgets.config.model.ConfigUIModelBuilder;
import org.nuiton.jaxx.widgets.config.model.MainCallBackFinalizer;

import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JTable;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.Map;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class ClientConfigUI extends JXTitledPanel implements WithClientUIContextApi {

    private final ObserveMainUI mainUI;

    @SuppressWarnings({"UnusedReturnValue", "WeakerAccess"})
    public static class ObserveConfigUIBuilder extends ConfigUIHelper {

        private final SpeciesListTableCellEditor speciesListTableCellEditor;

        private final SpeciesListsTableCellRenderer speciesListsTableCellRenderer;

        protected ObserveConfigUIBuilder(ClientConfig config,
                                         SpeciesListTableCellEditor speciesListTableCellEditor,
                                         SpeciesListsTableCellRenderer speciesListsTableCellRenderer) {
            super(config, config.get(), config.get().getExtraConfigFile());
            this.speciesListTableCellEditor = speciesListTableCellEditor;
            this.speciesListsTableCellRenderer = speciesListsTableCellRenderer;
        }

        @Override
        public ConfigUIModelBuilder addOption(ConfigOptionDef def) throws IllegalStateException, NullPointerException {
            return super.addOption(def).setOptionShortLabel(def.getDescription());
        }

        public ConfigUIModelBuilder registerCallBack(ObserveUICallback callback) {
            String name = callback.getName();
            String description = callback.getLabel();
            Icon icon = callback.getIcon();
            registerCallBack(name, description, icon, callback);

            return modelBuilder;

        }

        protected ConfigUIModelBuilder addSpeciesListOption(ClientConfigOption option) {
            return addOption(option).setOptionEditor(speciesListTableCellEditor).setOptionRenderer(speciesListsTableCellRenderer);
        }

    }

    protected static class SpeciesListsTableCellRenderer extends DefaultTableRenderer {

        private static final long serialVersionUID = 1L;

        private final Map<String, SpeciesListReference> entityMap;

        //FIXME Decorators, let's object deal with it and never ask at this level... Just do a toString()...
        private final Decorator decorator;

        public SpeciesListsTableCellRenderer(Map<String, SpeciesListReference> entityMap, Decorator decorator) {
            this.entityMap = entityMap;
            this.decorator = decorator;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {
            String speciesListId = String.valueOf(value);
            SpeciesListReference speciesList = entityMap.get(speciesListId);
            return super.getTableCellRendererComponent(table, decorator.decorate(speciesList), isSelected, hasFocus, row, column);

        }
    }

    protected static class SpeciesListTableCellEditor extends DefaultCellEditor {

        private static final long serialVersionUID = 1L;

        private final Map<String, SpeciesListReference> entityMap;

        protected SpeciesListTableCellEditor(Collection<SpeciesListReference> entities,
                                             Map<String, SpeciesListReference> entityMap,
                                             Decorator decorator) {

            super(new JComboBox<>());

            this.entityMap = entityMap;
            FilterableComboBox<SpeciesListReference> component = UIHelper.newFilterableComboBox(
                    SpeciesListReference.class,
                    decorator,
                    new ArrayList<>(entities));
            component.setShowReset(true);

            setClickCountToStart(1);

            editorComponent = component;
            delegate = new DefaultCellEditor.EditorDelegate() {

                private static final long serialVersionUID = 1L;

                @Override
                public void setValue(Object value) {
                    if (value instanceof String) {
                        value = SpeciesListTableCellEditor.this.entityMap.get(String.valueOf(value));
                    }
                    if (value==null) {
                        return;
                    }
                    component.setSelectedItem((SpeciesListReference) value);
                }

                @Override
                public Object getCellEditorValue() {
                    SpeciesListReference selectedItem = component.getModel().getSelectedItem();
                    return selectedItem == null ? null : selectedItem.getId();
                }

                @Override
                public boolean shouldSelectCell(EventObject anEvent) {
                    if (anEvent instanceof MouseEvent) {
                        MouseEvent e = (MouseEvent) anEvent;
                        return e.getID() != MouseEvent.MOUSE_DRAGGED;
                    }
                    return true;
                }

                @Override
                public boolean stopCellEditing() {
                    if (component.getModel().getSelectedItem()==null) {
                         super.cancelCellEditing();
                        return true;
                    }
                    if (component.getConfig().isEditable()) {
                        // Commit edited value.
                        component.getCombobox().actionPerformed(new ActionEvent(SpeciesListTableCellEditor.this, 0, ""));
                    }
                    return super.stopCellEditing();
                }
            };
        }
    }

    public ClientConfigUI(ObserveSwingDataSource dataSource) {
        super(t("observe.ui.action.configuration"));
        this.mainUI = getMainUI();

        ObserveConfigUIBuilder helper = buildUI(dataSource);
        helper.setCloseAction(this::quit);
        ConfigUI configUI = helper.buildUI(mainUI, "observe.config.category.application");

        configUI.getQuit().setMnemonic('Q');
        int tabCount = configUI.getCategories().getTabCount();
        for (int i = 0; i < tabCount; i++) {
            ConfigCategoryUI categoryUI = (ConfigCategoryUI) configUI.getCategories().getComponentAt(i);
            categoryUI.getSave().setMnemonic('E');
            categoryUI.getReset().setMnemonic('A');
            ObserveKeyStrokesSupport.addKeyStrokeFromMnemonic(categoryUI);
        }
        ObserveKeyStrokesSupport.addKeyStrokeFromMnemonic(configUI);
        // si on ne met pas ça, à la fermeture la fenetre principale est fermée
        configUI.getHandler().setTopContainer(this);
        configUI.getHandler().setConfigCallBackUICustomizer(callBackUI -> {
            callBackUI.getGo().setMnemonic('A');
            callBackUI.getGo().setText(t("observe.ui.action.apply"));
            callBackUI.getGo().setToolTipText(t("observe.ui.action.apply"));
            ObserveKeyStrokesSupport.addKeyStrokeFromMnemonic(callBackUI.getGo());
        });
        setContentContainer(configUI);
    }

    private void quit() {
        mainUI.changeToPreviousBodyContent();
    }

    private ObserveConfigUIBuilder buildUI(ObserveSwingDataSource dataSource) {
        SpeciesListTableCellEditor editor = null;
        SpeciesListsTableCellRenderer renderer = null;
        if (dataSource != null && dataSource.isOpen()) {
            Set<SpeciesListReference> speciesLists = dataSource.getReferentialReferences(SpeciesListReference.class);
            Map<String, SpeciesListReference> speciesListMap = ToolkitId.uniqueIndex(speciesLists);
            //FIXME Decorators, let's object deal with it and never ask at this level... Just do a toString()...
            DecoratorService decoratorService = getDecoratorService();
            Decorator referenceDecorator = decoratorService.getDecoratorByType(SpeciesListReference.class);
            editor = new SpeciesListTableCellEditor(speciesLists, speciesListMap, referenceDecorator);
            renderer = new SpeciesListsTableCellRenderer(speciesListMap, referenceDecorator);
        }
        ClientConfig config = getClientConfig();

        ObserveConfigUIBuilder helper = new ObserveConfigUIBuilder(config, editor, renderer);

        ObserveUICallbackManager uiCallbackManager = getUiCallbackManager();
        uiCallbackManager.getCallbacks().forEach(helper::registerCallBack);

        helper.setFinalizer(new MainCallBackFinalizer(uiCallbackManager.getCallback("application").getLabel()));

        //FIXME:BodyContent This is not working any longer???
//        MainUIBodyContent<?> currentBody = getClientUIContext().getMainUI().getMainUIBodyContentManager().getCurrentBody();
//        if (currentBody.get() instanceof DataSourceEditor) {
//            helper.setCloseAction(((DataSourceEditor)currentBody.get()).getContentUIManager()::restartEdit);
//        }

        addApplicationOptions(helper, dataSource);
        addUiOptions(helper);
        addNavigationOptions(helper);
        addMapOptions(helper);
        addDataOptions(helper, dataSource);
        addExpertOptions(helper);
        addTechnicalOptions(helper);
        return helper;
    }

    private void addApplicationOptions(ObserveConfigUIBuilder helper, ObserveSwingDataSource dataSource) {

        helper.addCategory(n("observe.config.category.application"),
                           n("observe.config.category.application.description"),
                           "application");

        helper.addOption(ClientConfigOption.VALIDATION_SPEED_ENABLE);
        helper.addOption(ClientConfigOption.VALIDATION_SPEED_MAX_VALUE);
        helper.addOption(ClientConfigOption.VALIDATION_LENGTH_WEIGHT_ENABLE);
        helper.addOption(ClientConfigOption.VALIDATION_USE_DISABLED_REFERENTIAL);
        helper.addOption(ClientConfigOption.CONSOLIDATION_FAIL_IF_LENGTH_WEIGHT_PARAMETER_NOT_FOUND);
        helper.addOption(ClientConfigOption.CONSOLIDATION_FAIL_IF_LENGTH_LENGTH_PARAMETER_NOT_FOUND);
        if (dataSource != null && dataSource.isOpen()) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2931
            helper.addSpeciesListOption(ClientConfigOption.CONSOLIDATION_SPECIES_LIST_FOR_LOGBOOK_SAMPLE_ACTIVITY_WEIGHTED_WEIGHT);
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2669
            helper.addSpeciesListOption(ClientConfigOption.CONSOLIDATION_SPECIES_LIST_FOR_LOGBOOK_SAMPLE_WEIGHTS);
        }

    }

    private void addUiOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.ui"),
                           n("observe.config.category.ui.description"),
                           "ui");

        helper.addOption(ClientConfigOption.SHOW_DATA_SOURCE_OPTIONAL_ACTIONS);
        helper.addOption(ClientConfigOption.SHOW_NUMBER_EDITOR_BUTTON);
        helper.addOption(ClientConfigOption.AUTO_POPUP_NUMBER_EDITOR);
        helper.addOption(ClientConfigOption.AUTO_SELECT_TEXT);
        helper.addOption(ClientConfigOption.SHOW_MNEMONIC);
        helper.addOption(ClientConfigOption.COORDINATE_FORMAT);

        helper.addOption(ClientConfigOption.BUSY_STATE_COLOR);
        helper.addOption(ClientConfigOption.BLOCK_STATE_COLOR);
        helper.addOption(ClientConfigOption.FOCUS_BORDER_COLOR);
        helper.addOption(ClientConfigOption.NO_FOCUS_BORDER_COLOR);
        helper.addOption(ClientConfigOption.TABLE_EMPTY_ROW_COLOR);
        helper.addOption(ClientConfigOption.FLOATING_OBJECT_MATERIAL_ERROR_COLOR);
        helper.addOption(ClientConfigOption.FLOATING_OBJECT_MATERIAL_NOT_EDITABLE_COLOR);

        helper.addOption(ClientConfigOption.DEFAULT_DATA_SOURCE_CONNECT_MODE);
        helper.addOption(ClientConfigOption.DEFAULT_DATA_SOURCE_CREATE_MODE);
        helper.addOption(ClientConfigOption.PS_LOGBOOK_WELL_PLAN_CAN_CREATE_ACTIVITY);
    }

    private void addMapOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.map"),
                           n("observe.config.category.map.description"),
                           "ui");

        helper.addOption(ClientConfigOption.MAP_BACKGROUND_COLOR);
        helper.addOption(ClientConfigOption.MAP_LAYER1);
        helper.addOption(ClientConfigOption.MAP_LAYER1_COLOR);
        helper.addOption(ClientConfigOption.MAP_LAYER2);
        helper.addOption(ClientConfigOption.MAP_LAYER2_COLOR);
        helper.addOption(ClientConfigOption.MAP_LAYER3);
        helper.addOption(ClientConfigOption.MAP_LAYER3_COLOR);
        helper.addOption(ClientConfigOption.MAP_LAYER4);
        helper.addOption(ClientConfigOption.MAP_LAYER4_COLOR);
        helper.addOption(ClientConfigOption.MAP_LAYER5);
        helper.addOption(ClientConfigOption.MAP_LAYER5_COLOR);
        helper.addOption(ClientConfigOption.MAP_LAYER6);
        helper.addOption(ClientConfigOption.MAP_LAYER7);
        helper.addOption(ClientConfigOption.MAP_LAYER8);
        helper.addOption(ClientConfigOption.MAP_LAYER9);
        helper.addOption(ClientConfigOption.MAP_LAYER10);
        helper.addOption(ClientConfigOption.MAP_DATE_FORMAT);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_FILE);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_OBSERVATIONS_TEXT_COLOR);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_OBSERVATIONS_TEXT_SIZE);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_OBSERVATIONS_LINE_COLOR);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_OBSERVATIONS_POINT_COLOR);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_LOGBOOK_TEXT_COLOR);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_LOGBOOK_TEXT_SIZE);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_LOGBOOK_LINE_COLOR);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_LOGBOOK_POINT_COLOR);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_LOGBOOK_WITH_SAMPLING_POINT_COLOR);
        helper.addOption(ClientConfigOption.MAP_PS_STYLE_LOGBOOK_TRANSMITTING_BUOY_LOST_POINT_COLOR);
        helper.addOption(ClientConfigOption.MAP_LL_STYLE_FILE);
        helper.addOption(ClientConfigOption.MAP_LL_STYLE_OBSERVATIONS_LINE_TRIP_COLOR);
        helper.addOption(ClientConfigOption.MAP_LL_STYLE_OBSERVATIONS_LINE_SETTING_COLOR);
        helper.addOption(ClientConfigOption.MAP_LL_STYLE_OBSERVATIONS_LINE_HAULING_COLOR);
        helper.addOption(ClientConfigOption.MAP_LL_STYLE_OBSERVATIONS_POINT_COLOR);
        helper.addOption(ClientConfigOption.MAP_LL_STYLE_LOGBOOK_LINE_TRIP_COLOR);
        helper.addOption(ClientConfigOption.MAP_LL_STYLE_LOGBOOK_LINE_SETTING_COLOR);
        helper.addOption(ClientConfigOption.MAP_LL_STYLE_LOGBOOK_LINE_HAULING_COLOR);
    }

    private void addDataOptions(ObserveConfigUIBuilder helper, ObserveSwingDataSource dataSource) {

        if (dataSource != null && dataSource.isOpen()) {

            helper.addCategory(n("observe.config.category.speciesList.seine"),
                               n("observe.config.category.speciesList.seine.description"),
                               "ui");

            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_SEINE_OBSERVATION_CATCH_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_SEINE_OBSERVATION_SCHOOL_ESTIMATE_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_SEINE_OBSERVATION_OBJECT_SCHOOL_ESTIMATE_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_SEINE_OBSERVATION_OBJECT_OBSERVED_SPECIES_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_SEINE_LOGBOOK_CATCH_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_SEINE_LOGBOOK_WELL_PLAN_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_SEINE_LOCALMARKET_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_SEINE_LANDING_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_SEINE_SAMPLE_ID);

            helper.addCategory(n("observe.config.category.speciesList.longline"),
                               n("observe.config.category.speciesList.longline.description"),
                               "ui");

            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_LONGLINE_COMMON_TRIP_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_LONGLINE_DEPREDATOR_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_LONGLINE_OBSERVATION_CATCH_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_LONGLINE_OBSERVATION_ENCOUNTER_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_LONGLINE_LOGBOOK_CATCH_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_LONGLINE_LOGBOOK_SAMPLE_ID);
            helper.addSpeciesListOption(ClientConfigOption.SPECIES_LIST_LONGLINE_LANDING_ID);

            //FIXME:Referential Add others config (vessel type, size measure, speciesGroup, ...)
        }
    }

    private void addExpertOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.expert"),
                           n("observe.config.category.expert.description"));

        helper.addOption(ClientConfigOption.LOAD_LOCAL_STORAGE).setOptionCallBack("application");

        helper.addOption(ClientConfigOption.BACKUP_USE).setOptionCallBack("application");
        helper.addOption(ClientConfigOption.BACKUP_DELAY).setOptionCallBack("application");
        helper.addOption(ClientConfigOption.BACKUP_AT_CLOSE).setOptionCallBack("application");

        helper.addOption(ClientConfigOption.REPORT_DIRECTORY).setOptionCallBack("application");

        helper.addOption(ClientConfigOption.TEMPORARY_FILES_TIMEOUT).setOptionCallBack("application");
        helper.addOption(ClientConfigOption.LOG_FILES_TIMEOUT).setOptionCallBack("application");
        helper.addOption(ClientConfigOption.FEEDBACK_FILES_TIMEOUT).setOptionCallBack("application");
        helper.addOption(ClientConfigOption.BACKUP_FILES_TIMEOUT).setOptionCallBack("application");

        helper.addOption(ClientConfigOption.CHANGE_SYNCHRO_SRC).setOptionCallBack("application");
        helper.addOption(ClientConfigOption.H2_CAN_MIGRATE).setOptionCallBack("application");
        helper.addOption(ClientConfigOption.OBSTUNA_CAN_MIGRATE).setOptionCallBack("application");
        helper.addOption(ClientConfigOption.H2_SERVER_PORT).setOptionCallBack("application");
        helper.addOption(ClientConfigOption.H2_CAN_EDIT_REFERENTIAL).setOptionCallBack("db");
        helper.addOption(ClientConfigOption.AVDTH_FORCE_IMPORT).setOptionCallBack("ui");

        helper.addOption(ClientConfigOption.SHOW_SQL);
        helper.addOption(ClientConfigOption.USE_JVM_KEY_STORE);
        helper.addOption(ClientConfigOption.SKIP_CONSOLIDATE_STEP_FOR_REPORT).setOptionCallBack("ui");
        helper.addOption(ClientConfigOption.SHOW_SENSIBLE_CRITERIA_IN_TRIP_BULK_CHANGES).setOptionCallBack("ui");
        helper.addOption(ClientConfigOption.CHECK_SERVER_VERSION).setOptionCallBack("ui");

    }

    private void addTechnicalOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.technical"),
                           n("observe.config.category.technical.description"));

        helper.addOption(ClientConfigOption.DATA_DIRECTORY);
        helper.addOption(ClientConfigOption.DB_DIRECTORY);
        helper.addOption(ClientConfigOption.BACKUP_DIRECTORY);
        helper.addOption(ClientConfigOption.EXPORT_DIRECTORY);
        helper.addOption(ClientConfigOption.LOG_DIRECTORY);
        helper.addOption(ClientConfigOption.IMPORT_DIRECTORY);
        helper.addOption(ClientConfigOption.TEMPORARY_DIRECTORY);
        helper.addOption(ClientConfigOption.VALIDATION_REPORT_DIRECTORY);
        helper.addOption(ClientConfigOption.RESOURCES_DIRECTORY);
        helper.addOption(ClientConfigOption.AVDTH_DIRECTORY);
        helper.addOption(ClientConfigOption.SWING_SESSION_FILE);
        helper.addOption(ClientConfigOption.SIMPLIFIED_OBJECT_TYPE_SPECIALIZED_RULES_FILE);

        helper.addOption(ClientConfigOption.H2_LOGIN);
        helper.addOption(ClientConfigOption.H2_PASSWORD);

    }

    private void addNavigationOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.ui.navigation"),
                           n("observe.config.category.ui.navigation.description"),
                           "ui");

        helper.addOption(ClientConfigOption.TREE_LOAD_DATA);
        helper.addOption(ClientConfigOption.TREE_LOAD_REFERENTIAL);
        helper.addOption(ClientConfigOption.TREE_MODULE_NAME);
        helper.addOption(ClientConfigOption.TREE_GROUP_BY_NAME);
        helper.addOption(ClientConfigOption.TREE_GROUP_BY_FLAVOR);
        helper.addOption(ClientConfigOption.TREE_LOAD_EMPTY_GROUP_BY);
        helper.addOption(ClientConfigOption.TREE_LOAD_DISABLED_GROUP_BY);
        helper.addOption(ClientConfigOption.TREE_LOAD_NULL_GROUP_BY);
        helper.addOption(ClientConfigOption.TREE_LOAD_TEMPORAL_GROUP_BY);
        helper.addOption(ClientConfigOption.TREE_NODE_DISABLED_COLOR);
        helper.addOption(ClientConfigOption.TREE_NODE_UNSAVED_COLOR);
        helper.addOption(ClientConfigOption.TREE_NODE_UNLOADED_COLOR);
        helper.addOption(ClientConfigOption.TREE_NODE_EMPTY_COLOR);
    }
}
