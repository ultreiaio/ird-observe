package fr.ird.observe.client.main.body;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.main.ObserveMainUI;
import io.ultreia.java4all.util.SingletonSupplier;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.io.Closeable;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Describe a main ui body content.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public abstract class MainUIBodyContent<B extends JComponent> implements Supplier<B>, Closeable {

    private final int order;
    private final Class<B> type;
    private SingletonSupplier<B> supplier;
    private boolean uninstalled;

    protected MainUIBodyContent(int order, Class<B> type) {
        this.order = order;
        this.type = Objects.requireNonNull(type);
    }

    protected MainUIBodyContent(int order, Class<B> type, Supplier<B> supplier) {
        this(order, type);
        this.supplier = SingletonSupplier.of(Objects.requireNonNull(supplier));
    }

    /**
     * Install body content once when ui is created.
     * <p>
     * This will NOT display the body content, but just prepare stuff around it (like menus).
     *
     * @param mainUI ui where to install
     */
    public void install(ObserveMainUI mainUI) {
        // by default nothing de do here
    }

    /**
     * Uninstall body content once when ui is destroying.
     *
     * @param mainUI ui where to uninstall
     */
    public void uninstall(ObserveMainUI mainUI) {
        // by default nothing de do here
        uninstalled = true;
    }

    /**
     * To show the body content in the given main ui.
     *
     * @param mainUI ui where to display content
     */
    public void show(ObserveMainUI mainUI) {
        // create content body
        B contentBody = get();
        // then attach it to main ui
        mainUI.getBody().add(contentBody, BorderLayout.CENTER);
        // repaint main ui
        SwingUtilities.invokeLater(mainUI::revalidate);
    }

    /**
     * Test if we can quit this body content.
     * <p>
     * This method is called before hiding a body content.
     *
     * @param mainUI ui to test
     * @return #{@code true} if we can quit this body content, {@code false} otherwise
     */
    public boolean canQuit(ObserveMainUI mainUI) {
        return true;
    }

    /**
     * To hide the body content in the given main ui.
     *
     * @param mainUI ui where to hide content
     * @throws HideBodyContentNotAcceptedException if could not hide current body content
     */
    public void hide(ObserveMainUI mainUI) throws HideBodyContentNotAcceptedException {
        if (!canQuit(mainUI)) {
            throw new HideBodyContentNotAcceptedException("Can't hide current body content: " + this);
        }
        // remove component from main ui
        hideSafe(mainUI);
        // repaint main ui
        SwingUtilities.invokeLater(mainUI::revalidate);
        // remove also supplier value
        supplier.clear();
    }

    /**
     * To hide the body content once we are sure we can hide it.
     *
     * @param mainUI ui where to hide content
     */
    protected void hideSafe(ObserveMainUI mainUI) {
        mainUI.getBody().removeAll();
    }

    /**
     * when ui was recreated, make what you can to reload this content.
     *
     * @param mainUI new main ui
     */
    public void reloadContent(ObserveMainUI mainUI) {

    }

    public void setSupplier(SingletonSupplier<B> supplier) {
        this.supplier = Objects.requireNonNull(supplier);
    }

    /**
     * @return unique content order (it will be used to initialize main ui).
     */
    public int order() {
        return order;
    }


    public Class<B> type() {
        return type;
    }

    @Override
    public B get() {
        return supplier.get();
    }

    @Override
    public void close() {
        if (supplier != null) {
            supplier.clear();
        }
    }

    protected boolean isAlive() {
        return !uninstalled;
    }

    @Override
    public String toString() {
        return String.format("%s{order=%d, type=%s}", super.toString(), order, type.getName());
    }

    protected boolean withValue() {
        return supplier != null && supplier.withValue();
    }
}
