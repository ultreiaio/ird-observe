package fr.ird.observe.client.main.body;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.dto.ObserveUtil;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JComponent;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
@GenerateJavaBeanDefinition
public class MainUIBodyContentManager extends AbstractJavaBean {

    private static final Logger log = LogManager.getLogger(MainUIBodyContentManager.class);

    /**
     * List of know bodies ordered by their order.
     *
     * @see MainUIBodyContent#order()
     */
    private final List<MainUIBodyContent<?>> orderedBodies;
    /**
     * Dictionary of know bodies indexed by their type.
     *
     * @see MainUIBodyContent#type()
     */
    private final Map<Class<?>, MainUIBodyContent<?>> bodies;
    /**
     * Current body displayed in gui.
     */
    private MainUIBodyContent<?> currentBody;
    /**
     * Previous body displayed in gui.
     */
    private MainUIBodyContent<?> previousBody;
    /**
     * Main ui where to attach body contents.
     */
    private ObserveMainUI mainUI;

    public MainUIBodyContentManager(ObserveMainUI mainUI) {
        this.mainUI = mainUI;
        Map<Class<?>, MainUIBodyContent<?>> bodies = new LinkedHashMap<>();
        List<MainUIBodyContent<?>> orderedBodies = new LinkedList<>();
        Set<Integer> orders = new TreeSet<>();

        @SuppressWarnings({"unchecked", "rawtypes"}) ServiceLoader<MainUIBodyContent<?>> mainUIBodyContents = (ServiceLoader) ServiceLoader.load(MainUIBodyContent.class);
        mainUIBodyContents.reload();
        for (MainUIBodyContent<?> body : mainUIBodyContents) {
            log.info(String.format("Detected main ui body: %s", body));
            bodies.put(body.type(), body);
            if (!orders.add(body.order())) {
                throw new IllegalStateException(String.format("Already found a body content with same order: %s, can't add body content: %s", body, orderedBodies.stream().filter(b -> b.order() == body.order()).findFirst().orElse(null)));
            }
            orderedBodies.add(body);
        }
        log.info(String.format("Found %d main ui bodies.", bodies.size()));
        this.orderedBodies = Collections.unmodifiableList(orderedBodies.stream().sorted(Comparator.comparing(MainUIBodyContent::order)).collect(Collectors.toList()));
        this.bodies = Collections.unmodifiableMap(bodies);
    }

    public MainUIBodyContent<?> getCurrentBody() {
        return currentBody;
    }

    public void setCurrentBody(MainUIBodyContent<?> currentBody) {
        MainUIBodyContent<?> oldValue = getCurrentBody();
        this.currentBody = currentBody;
        firePropertyChange("currentBody", oldValue, currentBody);
    }

    /**
     * To install bodies on main ui.
     * <p>
     * This should be done each time main ui is created.
     */
    public void install() {
        for (MainUIBodyContent<?> body : orderedBodies) {
            log.info(String.format("Initialize body content: %s", body));
            body.install(mainUI);
        }
    }

    /**
     * To close bodies on main ui.
     * <p>
     * this should be done each time main ui is closed.
     */
    public void close() {
        for (MainUIBodyContent<?> body : orderedBodies) {
            log.info(String.format("Close body content: %s", body));
            body.uninstall(mainUI);
            body.close();
        }
    }

    public <B extends JComponent> boolean changeCurrentBody(Class<B> type) {
        if (currentBody != null) {
            log.info(String.format("Hide body content: %s", currentBody));
            try {
                currentBody.hide(mainUI);
                ObserveUtil.cleanMemory();
                previousBody = currentBody;
            } catch (HideBodyContentNotAcceptedException e) {
                log.error(String.format("Could not hide body content: %s", currentBody), e);
                // this cancel change of body
                return false;
            }
        }
        currentBody = getBody(type);
        log.info(String.format("Show body content: %s", currentBody));
        currentBody.show(mainUI);
        ObserveUtil.cleanMemory();
        return true;
    }

    public <B extends JComponent> void changeCurrentBody(MainUIBodyContent<B> type) {
        if (currentBody != null) {
            log.info(String.format("Hide body content: %s", currentBody));
            try {
                currentBody.hide(mainUI);
                ObserveUtil.cleanMemory();
                previousBody = currentBody;
            } catch (HideBodyContentNotAcceptedException e) {
                log.error(String.format("Could not hide body content: %s", currentBody), e);
                // this cancel change of body
                return;
            }
        }
        currentBody = type;
        log.info(String.format("Show body content: %s", currentBody));
        currentBody.reloadContent(mainUI);
        ObserveUtil.cleanMemory();
    }

    public <B extends JComponent> MainUIBodyContent<B> getBody(Class<B> type) {
        @SuppressWarnings("unchecked")
        MainUIBodyContent<B> mainUIBodyContent = (MainUIBodyContent<B>) bodies.get(Objects.requireNonNull(type));
        return Objects.requireNonNull(mainUIBodyContent, "Can't find body with type: " + type.getName());
    }

    public <B extends JComponent, C extends MainUIBodyContent<B>> C getBodyTyped(Class<B> type, Class<C> contentType) {
        @SuppressWarnings("unchecked")
        MainUIBodyContent<B> mainUIBodyContent = (MainUIBodyContent<B>) bodies.get(Objects.requireNonNull(type));
        return contentType.cast(Objects.requireNonNull(mainUIBodyContent, "Can't find body with type: " + type.getName()));
    }

    public Class<? extends JComponent> getCurrentBodyType() {
        return currentBody == null ? null : currentBody.type();
    }

    public void changeToPreviousBodyContent() {
        if (previousBody == null) {
            changeCurrentBody(NoBodyContentComponent.class);
        } else {
            changeCurrentBody(previousBody);
        }
    }

    public MainUIBodyContent<?> getPreviousBody() {
        return previousBody;
    }
}
