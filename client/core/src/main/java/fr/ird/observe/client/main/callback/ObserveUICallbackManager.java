package fr.ird.observe.client.main.callback;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.main.ObserveMainUI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public class ObserveUICallbackManager implements Closeable {

    private static final Logger log = LogManager.getLogger(ObserveUICallbackManager.class);

    private final Map<String, ObserveUICallback> callbacks;

    public ObserveUICallbackManager(ObserveMainUI mainUI) {
        this.callbacks = new LinkedHashMap<>();
        for (ObserveUICallback callback : ServiceLoader.load(ObserveUICallback.class)) {
            callbacks.put(callback.getName(), callback);
            log.info(String.format("Init ui call back: %s", callback.getName()));
            callback.init(mainUI);
        }
        log.info(String.format("Found %d main ui callbacks", this.callbacks.size()));
    }

    public Collection<ObserveUICallback> getCallbacks() {
        return callbacks.values();
    }

    public ObserveUICallback getCallback(String name) {
        return callbacks.get(name);
    }

    public void run(String name) {
        getCallback(name).run();
    }

    @Override
    public void close() {
        callbacks.values().forEach(ObserveUICallback::close);
    }
}
