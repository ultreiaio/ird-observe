package fr.ird.observe.client.main.callback;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.body.ClientConfigUI;
import fr.ird.observe.client.main.body.MainUIBodyContent;
import fr.ird.observe.client.main.body.NoBodyContentComponent;
import io.ultreia.java4all.application.context.ApplicationContext;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.Icon;
import javax.swing.JComponent;
import java.util.Objects;
import java.util.Optional;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
@AutoService(ObserveUICallback.class)
public class ReloadUiCallback implements ObserveUICallback, WithClientUIContextApi {

    private ObserveMainUI ui;

    @Override
    public String getName() {
        return "ui";
    }

    @Override
    public String getLabel() {
        return n("observe.ui.action.reload.ui");
    }

    @Override
    public Icon getIcon() {
        return SwingUtil.getUIManagerActionIcon("ui-reload");
    }

    @Override
    public void run() {

        ClientConfig config = getClientConfig();

        ApplicationContext rootContext = ApplicationContext.get();

        // scan main ui

        //FIXME:BodyContent, cela doit être délégué au body content concerné
        Class<? extends JComponent> bodyContent = NoBodyContentComponent.class;
        MainUIBodyContent<?> previousBody = null;
        if (ui != null) {
            // Keep a reference on ui instance since it will be reset in close method
            // FIXME Should use a closing state in application to improve this
            ObserveMainUI ui = this.ui;
            previousBody = ui.getMainUIBodyContentManager().getPreviousBody();
            Optional<ObserveSwingDataSource> mainDataSource = getDataSourcesManager().getOptionalMainDataSource();
            if (mainDataSource.isPresent() && previousBody != null) {
                bodyContent = previousBody.type();
            } else {
                previousBody = null;
                bodyContent = ui.getMainUIBodyContentManager().getCurrentBodyType();
                if (ClientConfigUI.class.equals(bodyContent)) {
                    bodyContent = NoBodyContentComponent.class;
                }
            }
            ui.dispose();
            System.runFinalization();
        }

        ui = initUI(rootContext, config);
        if (previousBody != null) {
            MainUIBodyContent<? extends JComponent> body = ui.getMainUIBodyContentManager().getBody(bodyContent);
            body.reloadContent(ui);
        } else {
            ui.changeBodyContent(bodyContent);
        }

        // show ui
        setMainUIVisible(ui, true);
    }

    @Override
    public void init(ObserveMainUI mainUI) {
        this.ui = Objects.requireNonNull(mainUI);
    }

    @Override
    public void close() {
        this.ui = null;
    }
}
