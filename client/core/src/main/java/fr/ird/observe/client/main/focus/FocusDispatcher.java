package fr.ird.observe.client.main.focus;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditor;
import io.ultreia.java4all.jaxx.widgets.list.DoubleList;
import org.jdesktop.jxlayer.JXLayer;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;
import org.nuiton.jaxx.widgets.datetime.DateEditor;
import org.nuiton.jaxx.widgets.datetime.DateTimeEditor;
import org.nuiton.jaxx.widgets.datetime.TimeEditor;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.number.NumberEditor;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditor;
import org.nuiton.jaxx.widgets.text.BigTextEditor;
import org.nuiton.jaxx.widgets.text.NormalTextEditor;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import java.awt.Component;

/**
 * To dispatch focus to correct component on a rich editor.
 * <p>
 * Created on 06/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class FocusDispatcher {

    public JComponent dispatchFocus(JComponent source) {
        if (source == null) {
            return null;
        }
        if (source instanceof FilterableComboBox<?>) {
            return ((FilterableComboBox<?>) source).getCombobox();
        }
        if (source instanceof NormalTextEditor) {
            return ((NormalTextEditor) source).getTextEditor();
        }
        if (source instanceof BigTextEditor) {
            return ((BigTextEditor) source).getTextEditor();
        }
        if (source instanceof DateEditor) {
            return ((DateEditor) source).getDateEditor().getEditor();
        }
        if (source instanceof DateTimeEditor) {
            if (((DateTimeEditor) source).getModel().isDateEditable()) {
                return ((DateTimeEditor) source).getDayDateEditor().getEditor();
            }
            return (JComponent) ((DateTimeEditor) source).getHourEditor().getEditor().getComponent(0);
        }
        if (source instanceof TimeEditor) {
            return (JComponent) ((TimeEditor) source).getHourEditor().getEditor().getComponent(0);
        }
        if (source instanceof TemperatureEditor) {
            return ((TemperatureEditor) source).getEditor().getTextField();
        }
        if (source instanceof NauticalLengthEditor) {
            return ((NauticalLengthEditor) source).getEditor().getTextField();
        }
        if (source instanceof NumberEditor) {
            return ((NumberEditor) source).getTextField();
        }
        if (source instanceof CoordinatesEditor) {
            return ((CoordinatesEditor) source).getQuadrant1();
        }
        if (source instanceof DoubleList) {
            return ((DoubleList<?>) source).getUniverseList();
        }
        if (source instanceof JScrollPane) {
            return (JComponent) ((JScrollPane) source).getViewport().getView();
        }
        if (source instanceof JMenuItem) {
            JMenuItem item = (JMenuItem) source;
            JToggleButton toggleButton = (JToggleButton) item.getClientProperty("toggleButton");
            if (toggleButton == null) {
                return source;
            }
            Action action = item.getAction();
            if (action instanceof MenuAction) {
                MenuAction action1 = (MenuAction) action;
                JPopupMenu popupMenu = action1.getPopupMenu();
                toggleButton.doClick();
                SwingUtilities.invokeLater(() -> UIHelper.select(popupMenu, item));
                return null;
            }
        }
        if (source instanceof JPanel) {
            JPanel panel = (JPanel) source;
            int max = panel.getComponentCount();
            for (int i = 0; i < max; i++) {
                Component component = panel.getComponent(i);

                if (component instanceof JXLayer<?>) {
                    component = ((JXLayer<?>) component).getView();
                }
                if (component instanceof JComponent) {
                    JComponent result = dispatchFocus((JComponent) component);
                    if (result != null && result.isEnabled()) {
                        return result;
                    }
                }
            }
        }
        return source;
    }
}
