package fr.ird.observe.client.main.focus;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.ui.BusyModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTableHeader;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JScrollBar;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import java.awt.Component;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 10/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public class MainUIFocusModel extends UIFocusModel implements PropertyChangeListener {

    public static final String PROPERTY_FOCUS_OWNER_ZONE = "focusOwnerZone";
    public static final String PROPERTY_FOCUS_OWNER = "focusOwner";
    private static final Logger log = LogManager.getLogger(MainUIFocusModel.class);
    protected final Set<Class<?>> acceptedClassesInBlockingLayer;
    private final BusyModel busyModel;
    private final KeyboardFocusManager keyboardFocusManager;
    private final MainUIKeyEventDispatcher dispatcher;
    private final Map<String, MainUIFocusZone<?>> zones;
    private String focusOwnerZone;

    public MainUIFocusModel(BusyModel busyModel, KeyboardFocusManager keyboardFocusManager) {
        this.busyModel = Objects.requireNonNull(busyModel);
        this.keyboardFocusManager = Objects.requireNonNull(keyboardFocusManager);
        this.acceptedClassesInBlockingLayer = new LinkedHashSet<>();
        this.acceptedClassesInBlockingLayer.add(JTable.class);
//        this.acceptedClassesInBlockingLayer.add(JList.class);
        this.acceptedClassesInBlockingLayer.add(JScrollBar.class);
        this.acceptedClassesInBlockingLayer.add(JTabbedPane.class);
        this.acceptedClassesInBlockingLayer.add(JXTableHeader.class);

        this.dispatcher = new MainUIKeyEventDispatcher();
        this.zones = new TreeMap<>();
        install();
    }

    public Set<Class<?>> getAcceptedClassesInBlockingLayer() {
        return acceptedClassesInBlockingLayer;
    }

    public void addAcceptedClassesInBlockingLayer(Class<?> type) {
        acceptedClassesInBlockingLayer.add(type);
    }

    public void removeAcceptedClassesInBlockingLayer(Class<?> type) {
        acceptedClassesInBlockingLayer.remove(type);
    }

    @Override
    protected void install() {
        keyboardFocusManager.addKeyEventDispatcher(dispatcher);
        keyboardFocusManager.addPropertyChangeListener(PROPERTY_FOCUS_OWNER, dispatcher);
        busyModel.addPropertyChangeListener(BusyModel.BUSY_PROPERTY_NAME, this);
        addPropertyChangeListener(PROPERTY_FOCUS_OWNER_ZONE, this);
        addPropertyChangeListener(PROPERTY_BLOCK, this);
    }

    @Override
    public void uninstall() {
        new LinkedHashSet<>(zones.keySet()).forEach(this::removeZone);
        keyboardFocusManager.removeKeyEventDispatcher(dispatcher);
        keyboardFocusManager.removePropertyChangeListener(PROPERTY_FOCUS_OWNER, dispatcher);
        busyModel.removePropertyChangeListener(BusyModel.BUSY_PROPERTY_NAME, this);
        removePropertyChangeListener(PROPERTY_FOCUS_OWNER_ZONE, this);
        removePropertyChangeListener(PROPERTY_BLOCK, this);
    }

    public void addZone(MainUIFocusZone<?> zone) {
        zones.put(Objects.requireNonNull(zone).getName(), zone);
        zone.install();
    }

    public void removeZone(String zoneName) {
        MainUIFocusZone<?> remove = zones.remove(zoneName);
        if (remove != null) {
            remove.uninstall();
        }
    }

    public String getFocusOwnerZone() {
        return focusOwnerZone;
    }

    public void setFocusOwnerZone(String focusOwnerZone) {
        String oldValue = getFocusOwnerZone();
        this.focusOwnerZone = focusOwnerZone;
        firePropertyChange(PROPERTY_FOCUS_OWNER_ZONE, oldValue, focusOwnerZone);
    }

    public KeyboardFocusManager getKeyboardFocusManager() {
        return keyboardFocusManager;
    }

    public void dirtyFocusOwnerZone(String zoneName) {
        MainUIFocusZone<?> zone = zones.get(zoneName);
        if (zone != null) {
            zone.dirty();
            if (zone.isOwner()) {
                zone.refreshFocus();
            }
        }
    }

    public void refreshZoneFocus(String zoneName) {
        MainUIFocusZone<?> zone = zones.get(zoneName);
        if (zone != null) {
            if (zone.isOwner()) {
                zone.refreshFocus();
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case BusyModel.BUSY_PROPERTY_NAME:
                boolean busy = (boolean) evt.getNewValue();
                if (busy) {
                    block();
                } else {
                    unblock();
                }
                break;
            case PROPERTY_BLOCK:
                //propagate to zones
                boolean block = (boolean) evt.getNewValue();
                if (block) {
                    zones.values().forEach(MainUIFocusZone::block);
                } else {
                    zones.values().forEach(MainUIFocusZone::unblock);
                }
                break;
            case PROPERTY_FOCUS_OWNER_ZONE:
                String oldZoneName = (String) evt.getOldValue();
                String newZoneName = (String) evt.getNewValue();
                if (oldZoneName != null) {
                    MainUIFocusZone<?> oldZone = zones.get(oldZoneName);
                    if (oldZone != null) {
                        oldZone.lostFocus();
                    }
                }
                if (newZoneName != null) {
                    MainUIFocusZone<?> newZone = zones.get(newZoneName);
                    if (newZone != null) {
                        newZone.acquireFocus();
                    }
                }
                break;
        }
    }

    public void setZoneOwnerFocusOwner(Component finalFocusOwner) {
        String focusOwnerZone = getFocusOwnerZone();
        if (focusOwnerZone != null) {
            MainUIFocusZone<?> focusZone = zones.get(focusOwnerZone);
            if (focusZone != null) {
                focusZone.setComponent(finalFocusOwner);
            }
        }
    }

    class MainUIKeyEventDispatcher implements KeyEventDispatcher, PropertyChangeListener {

        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            Window focusedWindow = getKeyboardFocusManager().getFocusedWindow();
            if (focusedWindow instanceof JDialog) {
                return false;
            }
            return busyModel.isBusy();
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (PROPERTY_FOCUS_OWNER.equals(evt.getPropertyName())) {
                Object newValue = evt.getNewValue();
                log.info(String.format("New focus owner - (%s)", newValue));
                if (newValue instanceof JTextComponent && !(newValue instanceof JTextArea) && !(newValue instanceof JEditorPane)) {
                    JTextComponent jTextComponent = (JTextComponent) newValue;
                    log.info(String.format("Auto-select content of %s", jTextComponent));
                    SwingUtilities.invokeLater(jTextComponent::selectAll);
                }
            }
        }
    }
}
