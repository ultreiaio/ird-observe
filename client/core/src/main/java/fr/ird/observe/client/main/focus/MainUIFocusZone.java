package fr.ird.observe.client.main.focus;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.JComponent;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Objects;
import java.util.Set;

/**
 * To define a zone in application
 * <p>
 * Created on 10/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class MainUIFocusZone<C extends JComponent> extends UIFocusModel implements PropertyChangeListener {

    public static final String PROPERTY_COMPONENT = "component";
    public static final String PROPERTY_OWNER = "owner";
    private static final Logger log = LogManager.getLogger(MainUIFocusZone.class);
    /**
     * Layer to control focus on this zone.
     */
    protected final MainUIFocusZoneLayer<C> layer;
    /**
     * Zone name.
     */
    private final String name;
    /**
     * Main model.
     */
    private final MainUIFocusModel model;
    /**
     * Container of the zone.
     */
    private final C container;
    /**
     * Component with focus in this zone.
     * <p>
     * As soon as the zone in entered, we will ask to set this focus.
     */
    private Component component;
    /**
     * Is the zone has the focus?
     */
    private boolean owner;
    /**
     * Flag to recompute the component.
     */
    private boolean dirty;
    /**
     * Flag to avoid re-entrant cod, while processing focus adjustments.
     */
    private boolean focusAdjusting;

    public MainUIFocusZone(String name, MainUIFocusModel model, C container) {
        this.name = Objects.requireNonNull(name);
        this.model = Objects.requireNonNull(model);
        this.container = Objects.requireNonNull(container);
        this.layer = new MainUIFocusZoneLayer<>(this);
    }

    protected abstract Component computeFocusOwner(C container, Component previousFocusOwner);

    @Override
    protected void install() {
        Objects.requireNonNull(SwingUtil.getLayer(container)).setUI(layer);
        addPropertyChangeListener(PROPERTY_OWNER, this);
    }

    @Override
    protected void uninstall() {
        removePropertyChangeListener(PROPERTY_OWNER, this);
        Objects.requireNonNull(SwingUtil.getLayer(container)).setUI(null);
    }

    public String getName() {
        return name;
    }

    public MainUIFocusModel getModel() {
        return model;
    }

    public final C getContainer() {
        return container;
    }

    public void requestFocusInWindow() {
        if (isFocusAdjusting()) {
            return;
        }
        if (isBlock()) {
            return;
        }
        block();
        focusAdjusting();
        try {
            if (component == null || isDirty()) {
                log.info(String.format("Zone %s, dirty, will compute new focus (previous component: %s", name, component));
                Component newComponent = computeFocusOwner(container, component);
                setComponent(newComponent);
            }
            dirty = false;
            if (component != null) {
                component.requestFocusInWindow();
            }
        } finally {
            focusNotAdjusting();
            unblock();
        }
    }

    public final Component getComponent() {
        return component;
    }

    public final void setComponent(Component component) {
        log.info(String.format("Zone %s, set new focus component: %s", name, component));
        Component oldValue = getComponent();
        this.component = component;
        firePropertyChange(PROPERTY_COMPONENT, oldValue, component);
    }

    public final boolean isOwner() {
        return owner;
    }

    public Set<Class<?>> getAcceptedClassesInBlockingLayer() {
        return getModel().getAcceptedClassesInBlockingLayer();
    }

    public void lostFocus() {
        setOwner(false);
    }

    public void acquireFocus() {
        setOwner(true);
    }

    public void refreshFocus() {
        layer.refreshFocusOwner();
    }

    protected void setOwner(boolean owner) {
        boolean oldValue = isOwner();
        this.owner = owner;
        firePropertyChange(PROPERTY_OWNER, oldValue, owner);
    }

    public final boolean isDirty() {
        return dirty;
    }

    public boolean isFocusAdjusting() {
        return focusAdjusting;
    }

    public void focusAdjusting() {
        focusAdjusting = true;
    }

    public void focusNotAdjusting() {
        focusAdjusting = false;
    }

    public final void dirty() {
        dirty = true;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (PROPERTY_OWNER.equals(evt.getPropertyName())) {
            layer.updateFocusOwner((Boolean) evt.getNewValue());
        }
    }
}
