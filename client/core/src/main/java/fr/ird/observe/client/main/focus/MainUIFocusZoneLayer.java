package fr.ird.observe.client.main.focus;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;
import org.jdesktop.jxlayer.plaf.LayerUI;
import org.nuiton.jaxx.runtime.swing.BlockingLayerUI;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Objects;

/**
 * The layzer that box the zone to manage properly the zone.
 * <p>
 * Created on 10/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public class MainUIFocusZoneLayer<C extends JComponent> extends AbstractLayerUI<C> implements WithClientUIContextApi {
    private static final Logger log = LogManager.getLogger(MainUIFocusZoneLayer.class);
    private final MainUIFocusZone<C> zone;
    private Border noFocusBorder;
    private Border focusBorder;


    public MainUIFocusZoneLayer(MainUIFocusZone<C> zone) {
        this.zone = Objects.requireNonNull(zone);
    }

    @Override
    protected void processMouseEvent(MouseEvent e, JXLayer<? extends C> l) {
        if (zone.isBlock()) {
            return;
        }
        if (e.getID() == MouseEvent.MOUSE_ENTERED) {
            if (!zone.isOwner()) {
                log.info(String.format("Zone %s - Welcome in, will requestFocusInWindows", zone.getName()));
                zone.getModel().setFocusOwnerZone(zone.getName());
            }
        }
    }

    @Override
    protected void processKeyEvent(KeyEvent e, JXLayer<? extends C> l) {
        super.processKeyEvent(e, l);
    }

    @Override
    protected void processFocusEvent(FocusEvent e, JXLayer<? extends C> l) {
        if (zone.isBlock()) {
            return;
        }
        if (zone.isOwner()) {
            return;
        }
        switch (e.getCause()) {
            case TRAVERSAL:
            case TRAVERSAL_UP:
            case TRAVERSAL_DOWN:
            case TRAVERSAL_FORWARD:
            case TRAVERSAL_BACKWARD:

                // check if we are in a danger zone
                Component component = e.getComponent();
                if (component != null && !zone.getAcceptedClassesInBlockingLayer().contains(component.getClass())) {
                    BlockingLayerUI blockingLayerUI = canExecutionActionFromLayer(component);
                    if (blockingLayerUI != null && blockingLayerUI.isBlock()) {
                        Component oppositeComponent = e.getOppositeComponent();
                        zone.focusAdjusting();
                        try {
                            log.info(String.format("Zone %s - Reject focus event (will restore opposite focus component: %s)", zone.getName(), oppositeComponent));
                            oppositeComponent.requestFocusInWindow();
                        } finally {
                            zone.focusNotAdjusting();
                        }
                    }
                }
                break;
        }
    }

    protected BlockingLayerUI canExecutionActionFromLayer(Component editor) {
        if (editor == null) {
            return null;
        }
        JXLayer<?> ancestor = (JXLayer<?>) SwingUtilities.getAncestorOfClass(JXLayer.class, editor);
        if (ancestor == null) {
            return null;
        }
        LayerUI<?> ui = ancestor.getUI();
        if (ui instanceof BlockingLayerUI) {
            return (BlockingLayerUI) ui;
        }
        return canExecutionActionFromLayer(ancestor);
    }

    private Border getFocusBorder() {
        if (focusBorder == null) {
            focusBorder = new LineBorder(getClientConfig().getFocusBorderColor(), 3, true);
        }
        return focusBorder;
    }

    private Border getNoFocusBorder() {
        if (noFocusBorder == null) {
            noFocusBorder = new LineBorder(getClientConfig().getNoFocusBorderColor(), 3, true);
        }
        return noFocusBorder;
    }

    public void updateFocusOwner(boolean newValue) {
        if (zone.isFocusAdjusting()) {
            return;
        }
        if (newValue) {
            log.info(String.format("Zone %s - acquire focus", zone.getName()));
            zone.getContainer().setBorder(getFocusBorder());
            SwingUtilities.invokeLater(zone::requestFocusInWindow);
        } else {
            log.info(String.format("Zone %s - lost focus", zone.getName()));
            zone.getContainer().setBorder(getNoFocusBorder());
        }
    }

    public void refreshFocusOwner() {
        if (zone.isFocusAdjusting()) {
            return;
        }
        log.info(String.format("Zone %s - update focus", zone.getName()));
        SwingUtilities.invokeLater(zone::requestFocusInWindow);
    }
}
