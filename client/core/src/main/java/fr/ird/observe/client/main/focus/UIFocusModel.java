package fr.ird.observe.client.main.focus;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;

/**
 * Created on 10/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public abstract class UIFocusModel extends AbstractJavaBean {

    public static final String PROPERTY_BLOCK = "block";
    /**
     * Internal states to count hits of blocking focus.
     * <p>
     * The focus is blocked until the value is zero.
     */
    private int blockCount;

    protected abstract void install();

    protected abstract void uninstall();

    public final boolean isBlock() {
        return blockCount > 0;
    }

    public final void block() {
        setBlockCount(blockCount + 1);
    }

    public final void unblock() {
        setBlockCount(blockCount - 1);
    }

    protected int getBlockCount() {
        return blockCount;
    }

    protected synchronized void setBlockCount(int blockCount) {
        boolean oldBlock = isBlock();
        this.blockCount = blockCount;
        firePropertyChange(PROPERTY_BLOCK, oldBlock, isBlock());
    }

}
