package fr.ird.observe.client.util;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import org.nuiton.jaxx.runtime.bean.BeanScopeAware;

import javax.swing.JLabel;
import java.util.Objects;

/**
 * TODO Move this in jaxx
 * Created on 14/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class BeanLabel extends JLabel implements BeanScopeAware {

    JavaBean bean;

    @Override
    public Object getBean() {
        return bean;
    }

    @Override
    public void setBean(Object bean) {
        this.bean = (JavaBean) Objects.requireNonNull(bean);
    }
}
