package fr.ird.observe.client.util;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.referential.common.OceanDto;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.Icon;

/**
 * Created on 20/07/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class DtoIconHelper {

    private static final Logger log = LogManager.getLogger(DtoIconHelper.class);

    public static Icon getIcon(Class<? extends BusinessDto> dtoType) {
        return getIcon(dtoType, false);
    }

    public static Icon getIcon(String dtoType) {
        return getIcon(dtoType, false);
    }

    public static Icon getIcon(Class<? extends BusinessDto> dtoType, boolean small) {
        return getIcon(dtoType, "", small);
    }


    public static Icon getIcon(Class<? extends BusinessDto> dtoType, String suffix, boolean small) {
        return getIcon(ObserveBusinessProject.get().getDtoSimplifiedName(dtoType) + suffix, small);
    }

    public static Icon getReferentialHome(boolean small) {
        Pair<BusinessModule, BusinessSubModule> load = ObserveBusinessProject.get().load(OceanDto.class);
        String layout = ObserveBusinessProject.getLayout(load.getKey(), load.getValue(), false, "Home");
        return getIcon(I18n.t(layout), small);
    }

    public static Icon getDataHome(boolean small) {
        Pair<BusinessModule, BusinessSubModule> load = ObserveBusinessProject.get().load(OceanDto.class);
        String layout = ObserveBusinessProject.getLayout(load.getKey(), load.getValue(), true, "Home");
        return getIcon(I18n.t(layout), small);
    }

    public static Icon getReferentialHome(BusinessModule module, BusinessSubModule subModule, boolean small) {
        String layout = ObserveBusinessProject.getLayout(module, subModule, false, "Home");
        return getIcon(I18n.t(layout), small);
    }

    public static String getIconPath(String iconPathPrefix, boolean small) {
        String iconPath = iconPathPrefix;
        if (!iconPath.startsWith("navigation.")) {
            iconPath = "navigation." + iconPath;
        }
        if (small) {
            iconPath += "-small";
        }
        return iconPath;
    }

    public static Icon getIcon(String iconPathPrefix, boolean small) {
        String iconPath = getIconPath(iconPathPrefix, small);
        Icon icon = UIHelper.getUIManagerIcon(iconPath);
        if (icon == null) {
            log.warn(String.format("Can't find icon: '%s'", iconPath));
        }
        return icon;
    }
}
