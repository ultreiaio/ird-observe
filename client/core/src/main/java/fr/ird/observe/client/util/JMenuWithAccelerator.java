package fr.ird.observe.client.util;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class JMenuWithAccelerator extends JMenu {

    private static final Logger log = LogManager.getLogger(JMenuWithAccelerator.class);

    private KeyStroke accelerator;
    private boolean accessible;

    @Override
    public KeyStroke getAccelerator() {
        return accessible ? accelerator : null;
    }

    public void enabled() {
        KeyStroke oldValue = getAccelerator();
        accessible = true;
        repaint();
        revalidate();
        firePropertyChange("accelerator", oldValue, getAccelerator());
    }

    public void disabled() {
        KeyStroke oldValue = getAccelerator();
        accessible = false;
        firePropertyChange("accelerator", oldValue, getAccelerator());
    }

    @Override
    public void setAccelerator(KeyStroke keyStroke) {
        KeyStroke oldAccelerator = accelerator;
        this.accelerator = keyStroke;
        repaint();
        revalidate();
        firePropertyChange("accelerator", oldAccelerator, accelerator);
    }

    @Override
    public void setSelected(boolean b) {
        super.setSelected(b);
        if (b) {
            log.debug(String.format("%s is now selected", getName()));
            SwingUtilities.invokeLater(() -> ((JMenuItem) getMenuComponent(0)).setSelected(true));
        }
    }
}
