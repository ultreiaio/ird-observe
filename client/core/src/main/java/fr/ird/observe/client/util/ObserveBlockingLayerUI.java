package fr.ird.observe.client.util;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.jxlayer.JXLayer;
import org.nuiton.jaxx.runtime.swing.BlockingLayerUI;

import javax.swing.JComponent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;

/**
 * Created on 09/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ObserveBlockingLayerUI extends BlockingLayerUI {

    private static final Logger log = LogManager.getLogger(ObserveBlockingLayerUI.class);

    @Override
    protected final void processMouseWheelEvent(MouseWheelEvent e, JXLayer<? extends JComponent> l) {
        // never block that type of events
    }

    @Override
    protected void processKeyEvent(KeyEvent e, JXLayer<? extends JComponent> l) {
        log.debug(e + " → " + l.getView());
        super.processKeyEvent(e, l);
    }
}
