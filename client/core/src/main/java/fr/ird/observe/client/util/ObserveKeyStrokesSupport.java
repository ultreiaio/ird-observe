package fr.ird.observe.client.util;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfigApplicationComponent;
import io.ultreia.java4all.lang.Strings;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.AbstractButton;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.LinkedHashSet;
import java.util.Set;

import static javax.swing.Action.ACCELERATOR_KEY;

/**
 * Created on 23/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class ObserveKeyStrokesSupport {

    public static final KeyStroke KEY_STROKE_RELOAD_DEFAULT_CONFIGURATION = KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.ALT_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK);
    public static final KeyStroke KEY_STROKE_ESCAPE = KeyStroke.getKeyStroke("pressed ESCAPE");
    public static final KeyStroke KEY_STROKE_ENTER = KeyStroke.getKeyStroke("pressed ENTER");
    public static final KeyStroke KEY_STROKE_SPACE = KeyStroke.getKeyStroke("pressed SPACE");
    public static final KeyStroke KEY_STROKE_ALT_ENTER = KeyStroke.getKeyStroke("alt pressed ENTER");
    public static final KeyStroke KEY_STROKE_CONFIGURE_SAVE_CONFIGURATION = KeyStroke.getKeyStroke("pressed F5");
    public static final KeyStroke KEY_STROKE_STORAGE_TEST_CONNEXION = KeyStroke.getKeyStroke("pressed F2");
    public static final KeyStroke KEY_STROKE_RESET = KeyStroke.getKeyStroke("pressed F4");

    public static final KeyStroke KEY_STROKE_CONFIGURE_TOGGLE_CONFIGURATIONS = KeyStroke.getKeyStroke("pressed F1");
    public static final KeyStroke KEY_STROKE_INSERT_CONFIGURE = KeyStroke.getKeyStroke("pressed F3");
    public static final KeyStroke KEY_STROKE_RESET_DATA = KeyStroke.getKeyStroke("pressed F4");
    public static final KeyStroke KEY_STROKE_SAVE_DATA = KeyStroke.getKeyStroke("pressed F5");
    public static final KeyStroke KEY_STROKE_DELETE_DATA_GLOBAL = KeyStroke.getKeyStroke("pressed F6");
    public static final KeyStroke KEY_STROKE_DUPLICATE = KeyStroke.getKeyStroke("pressed F7");


    public static String keyStrokeToStr(KeyStroke actionKey) {
        String result = "";
        if (actionKey != null) {
            result = " (" + Strings.removeStart(actionKey.toString().replace("pressed", "+"), "+ ") + ")";
        }
        return result;
    }

    public static void addKeyStroke(AbstractButton component, KeyStroke actionKey) {

        boolean showMnemonic = isShowMnemonic();
        if (showMnemonic) {
            String actionStr = keyStrokeToStr(actionKey);
            if (component.getText() != null && !component.getText().contains(actionStr)) {
                String text = component.getText() + actionStr;
                component.setText(text);
            }
            if (component.getToolTipText() != null && !component.getToolTipText().contains(actionStr)) {
                String tip = component.getToolTipText() + actionStr;
                component.setToolTipText(tip);
            }
        }
    }

    protected static boolean isShowMnemonic() {
        return ClientConfigApplicationComponent.value().isShowMnemonic();
    }

    public static String suffixTextWithKeyStroke(String text, KeyStroke keyStroke) {
        boolean showMnemonic = isShowMnemonic();
        if (showMnemonic) {
            text += keyStrokeToStr(keyStroke);
        }
        return text;
    }

    public static void addKeyStroke2(AbstractButton editor, KeyStroke keyStroke) {
        String tip = (String) editor.getClientProperty("toolTipText");
        boolean showMnemonic = isShowMnemonic();
        if (tip != null) {
            if (keyStroke != null && showMnemonic) {
                tip += keyStrokeToStr(keyStroke);
            }
            editor.setToolTipText(tip);
        }

        String text = (String) editor.getClientProperty("text");
        if (text != null) {
            if (keyStroke != null && showMnemonic && !(editor instanceof JMenuItem)) {
                text += keyStrokeToStr(keyStroke);
            }
            editor.setText(text);
        }
    }

    public static void addKeyStrokeFromMnemonic(AbstractButton editor) {
        int mnemonic = editor.getMnemonic();
        if (mnemonic > 0) {
            if (isShowMnemonic()) {
                String accelerator = " (Alt + " + (char) mnemonic + ")";
                if (editor.getText() != null) {
                    editor.setText(editor.getText() + accelerator);
                }
                if (editor.getToolTipText() != null) {
                    editor.setToolTipText(editor.getToolTipText() + accelerator);
                }
            }
        }
    }

    public static void addKeyStrokeFromMnemonic(JAXXObject jaxxObject) {
        Set<JAXXObject> done = new LinkedHashSet<>();
        addKeyStrokeFromMnemonic(jaxxObject, done);
    }

    protected static void addKeyStrokeFromMnemonic(JAXXObject jaxxObject, Set<JAXXObject> done) {

        if (done.contains(jaxxObject)) {
            return;
        }
        done.add(jaxxObject);

        jaxxObject.get$objectMap().values().stream().filter(o -> !(o instanceof JMenuItem) && o instanceof AbstractButton).forEach(
                o -> {
                    AbstractButton b = (AbstractButton) o;
                    if (b.getAction() == null || b.getAction().getValue(ACCELERATOR_KEY) == null) {
                        addKeyStrokeFromMnemonic((AbstractButton) o);
                    }
                }
        );

        jaxxObject.get$objectMap().values().stream().filter(o -> o instanceof JAXXObject).forEach(
                o -> addKeyStrokeFromMnemonic((JAXXObject) o, done)
        );
    }

    public static KeyStroke getFunctionKeyStroke(int position) {
        if (position < (12) + 1) {
            return KeyStroke.getKeyStroke("F" + position);
        }
        if (position < (12 * 2) + 1) {
            return KeyStroke.getKeyStroke("shift pressed F" + (position - 12));
        }
        if (position < (12 * 3) + 1) {
            return KeyStroke.getKeyStroke("ctrl pressed F" + (position - 24));
        }
        if (position < (12 * 4) + 1) {
            return KeyStroke.getKeyStroke("shift ctrl pressed F" + (position - 36));
        }
        return null;
    }

    public static String removeKeyStroke(String text) {
        int lastIndex = text.lastIndexOf('(');
        if (lastIndex > -1) {
            text = text.substring(0, lastIndex - 1).trim();
        }
        return text;
    }
}
