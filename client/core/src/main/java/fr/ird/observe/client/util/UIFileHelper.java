package fr.ird.observe.client.util;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.file.JaxxFileChooser;

import javax.swing.JOptionPane;
import java.awt.Component;
import java.io.File;
import java.util.Arrays;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Helper around files ui actions for user.
 * <p>
 * Created on 09/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class UIFileHelper {
    private static final Logger log = LogManager.getLogger(UIFileHelper.class);

    /**
     * Choisir un fichier via un sélecteur graphique de fichiers.
     *
     * @param parent        le component swing appelant la boite de dialogue
     * @param title         le titre du dialogue de sélection
     * @param buttonLabel   le label du bouton d'acceptation
     * @param incoming      le fichier de base à utiliser
     * @param mainExtension l'extension principale (à rajouter sur le nom du fichier si besoin)
     * @param filters       les filtres + descriptions sur le sélecteur de fichiers
     * @return le fichier choisi ou le fichier incoming si l'opération a été annulée
     */
    public static File chooseFile(Component parent, String title, String buttonLabel, File incoming, String mainExtension, String... filters) {

        JaxxFileChooser.ToLoadFile toLoadFile = JaxxFileChooser.forLoadingFile()
                .setParent(parent)
                .setTitle(title)
                .setApprovalText(buttonLabel)
                .setPatternOrDescriptionFilters(Arrays.asList(filters));

        File parentDirectoryIfExist = getParentDirectoryIfExist(incoming);
        if (parentDirectoryIfExist != null) {
            toLoadFile.setStartDirectory(parentDirectoryIfExist);
        }
        File file = toLoadFile.choose();

        log.debug(title + " : " + file);
        return file == null ? incoming : ensureFileExtension(file, mainExtension);
    }

    /**
     * Choisir un répertoire via un sélecteur graphique de fichiers.
     *
     * @param parent      le component swing appelant la boite de dialogue
     * @param title       le titre de la boite de dialogue de sélection
     * @param buttonLabel le label de l'action d'acceptation
     * @param incoming    le fichier de base à utiliser
     * @return le répertoire choisi ou le répertoire incoming si l'opération a été annulée
     */
    public static File chooseDirectory(Component parent, String title, String buttonLabel, File incoming) {

        JaxxFileChooser.ToLoadDirectory toLoadDirectory = JaxxFileChooser.forLoadingDirectory()
                .setParent(parent)
                .setTitle(title)
                .setApprovalText(buttonLabel);
        File parentDirectoryIfExist = getParentDirectoryIfExist(incoming);
        if (parentDirectoryIfExist != null) {
            toLoadDirectory.setStartDirectory(parentDirectoryIfExist);
        }
        File file = toLoadDirectory.choose();
        log.debug(title + " : " + file);
        return file;
    }

    public static File ensureFileExtension(File file, String extension) {
        if (extension != null && !file.getName().toLowerCase().endsWith(extension)) {
            file = file.toPath().getParent().resolve(file.getName() + extension).toFile();
        }
        return file;
    }

    private static File getParentDirectoryIfExist(File incoming) {
        if (incoming != null) {
            File basedir;
            if (incoming.isFile()) {
                basedir = incoming.getParentFile();
            } else {
                basedir = incoming;
            }
            if (basedir.exists()) {
                return basedir;
            }
        }
        return null;
    }

    public static boolean confirmOverwriteFileIfExist(Component parent, File file) {
        boolean write = true;
        if (file.exists()) {
            write = SwingUtil.askUser(
                    parent,
                    t("observe.Common.saveFile.overwrite.title"),
                    t("observe.Common.saveFile.overwrite"),
                    JOptionPane.OK_CANCEL_OPTION,
                    new Object[]{
                            t("observe.Common.saveFile.overwrite.ok"),
                            t("observe.Common.saveFile.overwrite.cancel")},
                    1) == 0;
        }
        return write;
    }
}
