/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.util;

import fr.ird.observe.client.ClientUIContextApplicationComponent;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdLabel;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.choice.BooleanTableCellRenderer;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxCellEditor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.renderer.DecoratorTableCellRenderer;
import org.nuiton.jaxx.widgets.error.ErrorDialogUI;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;
import static javax.swing.JOptionPane.CLOSED_OPTION;
import static javax.swing.JOptionPane.VALUE_PROPERTY;

/**
 * Une classe d'aide pour les ui
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class UIHelper extends SwingUtil {

    public static final PropertyChangeListener LOG_PROPERTY_CHANGE_LISTENER = new LogPropertyChanged();
    public static final String NO_PACK = "NoPack";
    public static final String FIX_SIZE = "FixSize";

    static private final Logger log = LogManager.getLogger(UIHelper.class);

    public static class LogPropertyChanged implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String name = evt.getPropertyName();
            Object oldValue = evt.getOldValue();
            Object newValue = evt.getNewValue();
            log.debug(evt.getSource() + " - Property [" + name + "] has changed from  " + oldValue + " to " + newValue);
        }
    }

    public static String getContentActionIconKey(String action) {
        return String.format("content-%s-16", action);
    }

    public static Icon getContentActionIcon(String action) {
        return getUIManagerActionIcon(getContentActionIconKey(action));
    }

    public static void addApplicationIcon(Window window) {
        window.setIconImage(createImageIcon("logo.png").getImage());
    }

    public static DecoratorTableCellRenderer newDecorateTableCellRenderer(TableCellRenderer renderer, Class<?> type) {
        return newDecorateTableCellRenderer(renderer, type, null);
    }

    public static DecoratorTableCellRenderer newDecorateTableCellRenderer(TableCellRenderer renderer, Class<?> type, String context) {
        Objects.requireNonNull(renderer);
        Objects.requireNonNull(type);
        Decorator decorator = ClientUIContextApplicationComponent.value().getDecoratorService().getDecoratorByType(type, context);
        Objects.requireNonNull(decorator, "cant find decorator for " + type.getName() + " - " + context);
        return new DecoratorTableCellRenderer(renderer, decorator);
    }

    //FIXME Move this to jaxx
    public static TableCellRenderer newStringTableCellRenderer(
            final TableCellRenderer renderer,
            final int length,
            final boolean tooltip) {

        return (table, value, isSelected, hasFocus, row, column) -> {

            String originalValue = null;
            String val2 = null;
            if (value != null) {
                originalValue = value.toString();
                val2 = value.toString();
                if (val2.length() > length) {
                    val2 = val2.substring(0, length - 3) + "...";
                }
            }

            JComponent comp = (JComponent) renderer.getTableCellRendererComponent(
                    table,
                    val2,
                    isSelected,
                    hasFocus,
                    row,
                    column
            );
            if (tooltip) {
                comp.setToolTipText(originalValue);
            }
            return comp;
        };
    }

    /**
     * Copy to clipBoard the content of the given text.
     *
     * @param text text to copy to clipboard
     * @since 2.0
     */
    public static void copyToClipBoard(String text) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        log.debug(String.format("Put in clipboard :\n%s", text));
        StringSelection selection = new StringSelection(text);
        clipboard.setContents(selection, selection);
    }

    public static void stopEditing(JTable table) {
        TableCellEditor cellEditor = table.getCellEditor();
        if (cellEditor != null) {
            cellEditor.stopCellEditing();
        }
    }

    public static void cancelEditing(JTable table) {
        TableCellEditor cellEditor = table.getCellEditor();
        if (cellEditor != null) {
            cellEditor.cancelCellEditing();
        }
    }

    public static TableCellRenderer newBooleanTableCellRenderer(TableCellRenderer renderer) {
        return new BooleanTableCellRenderer(renderer);
    }

    public static <O extends JavaBean> FilterableComboBox<O> newFilterableComboBox(Class<O> type, Decorator decorator, List<O> data) {
        FilterableComboBox<O> editor = new FilterableComboBox<>();
        editor.setI18nPrefix("observe.common.");
        String entityLabel = I18nDecoratorHelper.getType(type);
        editor.setPopupTitleText(t("observe.data.Data.type", entityLabel));
        editor.setBeanType(type);
        editor.init(decorator, data);
        return editor;
    }

    public static FilterableComboBox<ToolkitIdLabel> newToolkitIdLabelFilterableComboBox(Class<?> type, Decorator decorator, List<ToolkitIdLabel> data) {
        FilterableComboBox<ToolkitIdLabel> editor = new FilterableComboBox<>();
        editor.setI18nPrefix("observe.common.");
        String entityLabel = I18nDecoratorHelper.getType(type);
        editor.setPopupTitleText(t("observe.data.Data.type", entityLabel));
        editor.setBeanType(ToolkitIdLabel.class);
        editor.init(decorator, data);
        return editor;
    }

    public static void handlingError(Throwable e) {
        log.error(e.getMessage(), e);
        //FIXME Improve this in JAXX
        ErrorDialogUI.showError((Exception) e);
    }

    public static JAXXInitialContext initialContext(JAXXObject ui, Object model) {
        return new JAXXInitialContext().add(model).add(ui);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <B extends JavaBean> FilterableComboBoxCellEditor newCellEditor(List<B> data, Decorator decorator) {
        FilterableComboBoxCellEditor cellEditor = newCellEditor((Class<B>) decorator.definition().type(), decorator);
        cellEditor.getComponent().setData((List) data);
        return cellEditor;
    }

    public static <B extends JavaBean> FilterableComboBoxCellEditor newCellEditor(Class<B> dataType, Decorator decorator) {
        FilterableComboBox<B> editor = initCellEditor(dataType, decorator);
        return new FilterableComboBoxCellEditor(editor);
    }

    public static <B extends JavaBean> FilterableComboBox<B> initCellEditor(Class<B> dataType, Decorator decorator) {
        FilterableComboBox<B> editor = new FilterableComboBox<>();
        editor.setI18nPrefix("observe.common.");
        String entityLabel = I18nDecoratorHelper.getType(dataType);
        editor.setPopupTitleText(t("observe.data.Data.type", entityLabel));
        editor.setBeanType(dataType);
        editor.setMinimumSize(new Dimension(0, 22));
        editor.getConfig().setShowReset(true);
        editor.init(decorator, Collections.emptyList());
        return editor;
    }

    //FIXME Move this to MenuAction in JAXX
    public static void select(JPopupMenu popupMenu, JMenuItem item) {
        int componentCount = popupMenu.getComponentCount();
        if (componentCount == 0) {
            return;
        }
        MenuElement menuElement = null;
        for (MenuElement subElement : popupMenu.getSubElements()) {
            if (item.equals(subElement.getComponent())) {
                menuElement = subElement;
                break;
            }
        }
        if (menuElement != null) {
            MenuSelectionManager.defaultManager().setSelectedPath(new MenuElement[]{popupMenu, menuElement});
        }

    }

    public static int askUser(Frame ui, JOptionPane pane, Dimension minimumDimension, String title, Object[] options) {

        JDialog dialog = new JDialog(ui, true);
        dialog.setMinimumSize(minimumDimension);
        dialog.setTitle(title);
        dialog.setResizable(false);

        Container contentPane = dialog.getContentPane();

        contentPane.setLayout(new BorderLayout());
        contentPane.add(pane, BorderLayout.CENTER);

        dialog.pack();
        dialog.setLocationRelativeTo(ui);

        final PropertyChangeListener listener = event -> {
            // Let the defaultCloseOperation handle the closing
            // if the user closed the window without selecting a button
            // (newValue = null in that case).  Otherwise, closeData the dialog.
            if (dialog.isVisible() && event.getSource() == pane &&
                    (event.getPropertyName().equals(VALUE_PROPERTY)) &&
                    event.getNewValue() != null &&
                    event.getNewValue() != JOptionPane.UNINITIALIZED_VALUE) {
                dialog.setVisible(false);
            }
        };

        WindowAdapter adapter = new WindowAdapter() {
            private boolean gotFocus = false;

            public void windowClosing(WindowEvent we) {
                pane.setValue(null);
            }

            public void windowClosed(WindowEvent e) {
                pane.removePropertyChangeListener(listener);
                dialog.getContentPane().removeAll();
            }

            public void windowGainedFocus(WindowEvent we) {
                // Once window gets focus, set initial focus
                if (!gotFocus) {
                    pane.selectInitialValue();
                    gotFocus = true;
                }
            }
        };
        dialog.addWindowListener(adapter);
        dialog.addWindowFocusListener(adapter);
        dialog.addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent ce) {
                // reset value to ensure closing works properly
                pane.setValue(JOptionPane.UNINITIALIZED_VALUE);
            }
        });

        pane.addPropertyChangeListener(listener);

        dialog.setVisible(true);
        Object selectedValue = pane.getValue();

        if (selectedValue == null)
            return CLOSED_OPTION;
        for (int counter = 0, maxCounter = options.length;
             counter < maxCounter; counter++) {
            if (options[counter].equals(selectedValue))
                return counter;
        }
        return CLOSED_OPTION;
    }
}
