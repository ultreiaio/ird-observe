package fr.ird.observe.client.util.action;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ObserveExecutorService extends ThreadPoolExecutor {

    public ObserveExecutorService() {
        super(5, 10, 10L, TimeUnit.MINUTES, new LinkedBlockingQueue<>(), new ThreadFactory() {
            final ThreadFactory defaultFactory = Executors.defaultThreadFactory();

            public Thread newThread(@SuppressWarnings("NullableProblems") Runnable r) {
                Thread thread = defaultFactory.newThread(r);
                thread.setName("ActionWorker-" + thread.getName());
                thread.setDaemon(true);
                return thread;
            }


        });

        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdownNow));
    }
}
