package fr.ird.observe.client.util.init;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.navigation.tree.JXTreeWithNoSearch;
import io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox;
import io.ultreia.java4all.jaxx.widgets.combobox.BeanEnumEditor;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.list.DoubleList;
import io.ultreia.java4all.jaxx.widgets.list.ListHeader;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.init.UIInitializerSupport;
import org.nuiton.jaxx.widgets.datetime.DateEditor;
import org.nuiton.jaxx.widgets.datetime.DateTimeEditor;
import org.nuiton.jaxx.widgets.datetime.TimeEditor;
import org.nuiton.jaxx.widgets.datetime.UnlimitedTimeEditor;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.number.NumberEditor;
import org.nuiton.jaxx.widgets.text.BigTextEditor;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JSplitPane;
import javax.swing.JTree;

public class DefaultUIInitializer<UI extends JComponent & JAXXObject> extends UIInitializerSupport<UI, DefaultUIInitializerContext<UI>> implements WithClientUIContextApi {

    public static final String CLIENT_PROPERTY_FORCE_LOAD = "forceLoad";
    public static final String CLIENT_PROPERTY_LIST_NO_LOAD = "listNoLoad";
    public static final String MAIN_TABBED_PANE = "mainTabbedPane";
    public static final String SUB_TABBED_PANE = "subTabbedPane";

    public static final Class<?>[] MANAGED_TYPES = new Class<?>[]{
            JComponent.class,
            BeanCheckBox.class,
            AbstractButton.class,
            NumberEditor.class,
            FilterableComboBox.class,
            TimeEditor.class,
            DateTimeEditor.class,
            CoordinatesEditor.class,
            BeanEnumEditor.class,
            JLabel.class,
            JTree.class
    };
    protected final UI ui;
    protected final DecoratorService decoratorService;

    public static <UI extends JComponent & JAXXObject> void doInit(UI ui) {
        new DefaultUIInitializer<>(ui).initUI();
    }

    public DefaultUIInitializer(UI ui, Class<?>... types) {
        super(ui, types);
        this.ui = ui;
        this.decoratorService = getDecoratorService();
    }

    public DefaultUIInitializer(UI ui) {
        super(ui, MANAGED_TYPES);
        this.ui = ui;
        this.decoratorService = getDecoratorService();
    }

    @Override
    protected DefaultUIInitializerContext<UI> createIInitializerContext(UI ui, Class<?>... types) {
        return new DefaultUIInitializerContext<>(ui, types);
    }

    @Override
    protected DefaultUIInitializerResult build(DefaultUIInitializerContext<UI> initializerContext) {
        return new DefaultUIInitializerResult(initializerContext);
    }

    @Override
    protected void initUI(DefaultUIInitializerContext<UI> initializerContext) {
        initializerContext
                .startFirstPass()
                .onSubComponents(JComponent.class, this::init)
                .onComponents(BeanCheckBox.class, this::init)
                .onSubComponents(AbstractButton.class, this::init)
                .onComponents(NumberEditor.class, this::init)
                .onComponents(FilterableComboBox.class, this::init)
                .onComponents(TimeEditor.class, this::init)
                .onComponents(UnlimitedTimeEditor.class, this::init)
                .onComponents(DateEditor.class, this::init)
                .onComponents(DateTimeEditor.class, this::init)
                .onComponents(CoordinatesEditor.class, this::init)
                .onComponents(BeanEnumEditor.class, this::init)
                .onComponents(JLabel.class, this::init)
                .onComponents(JSplitPane.class, this::init)
                .onComponents(JTree.class, this::init)
                .onComponents(JXTreeWithNoSearch.class, this::init)
                .startSecondPass();
    }

    private void init(JTree editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    private void init(JLabel editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(ui, editor);
    }

    private void init(JSplitPane editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(AbstractButton editor) {
        initializerContext.checkFirstPass();
    }

    protected void init(BeanCheckBox editor) {
        initializerContext.checkFirstPass();
        editor.init();
    }

    @SuppressWarnings("rawtypes")
    protected void init(FilterableComboBox editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(ListHeader<?> editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(DoubleList<?> editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(BigTextEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(TimeEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(UnlimitedTimeEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(NumberEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor, getClientConfig().isAutoPopupNumberEditor(), getClientConfig().isShowNumberEditorButton());
    }

    protected void init(DateTimeEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(DateEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(CoordinatesEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor, getClientConfig().getCoordinateFormat());
    }

    protected void init(BeanEnumEditor<?> editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(JComponent editor) {
        initializerContext.checkFirstPass();
    }

}

