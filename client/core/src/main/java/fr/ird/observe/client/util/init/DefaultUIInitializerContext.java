package fr.ird.observe.client.util.init;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.init.UIInitializerContext;
import org.nuiton.jaxx.validator.JAXXValidator;
import org.nuiton.jaxx.validator.swing.tab.JTabbedPaneValidator;

import javax.swing.JTabbedPane;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Context to collect components at init time.
 * <p>
 * Created on 09/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class DefaultUIInitializerContext<U extends JAXXObject> extends UIInitializerContext<U> {

    private final Map<String, JAXXValidator> extraTabUIs;
    protected JTabbedPaneValidator tabbedPaneValidator;
    protected JTabbedPaneValidator subTabbedPaneValidator;

    public DefaultUIInitializerContext(U ui, Class<?>... types) {
        super(ui, types);
        this.extraTabUIs = new LinkedHashMap<>();
    }

    public void addExtraTabUi(String editor, JAXXValidator validator) {
        extraTabUIs.put(editor, validator);
    }

    public Map<String, JAXXValidator> getExtraTabUIs() {
        return extraTabUIs;
    }

    public JTabbedPaneValidator getTabbedPaneValidator() {
        return tabbedPaneValidator;
    }

    public JTabbedPaneValidator getSubTabbedPaneValidator() {
        return subTabbedPaneValidator;
    }

    public void setTabbedPaneValidator(JTabbedPane tabbedPane) {
        this.tabbedPaneValidator = JTabbedPaneValidator.builder((JAXXValidator) getUi(), tabbedPane.getName()).addScope(NuitonValidatorScope.ERROR, NuitonValidatorScope.WARNING).addExtraTab(getExtraTabUIs()).build();
    }

    public void setSubTabbedPaneValidator(JTabbedPane tabbedPane) {
        this.subTabbedPaneValidator = JTabbedPaneValidator.builder((JAXXValidator) getUi(), tabbedPane.getName()).addScope(NuitonValidatorScope.ERROR, NuitonValidatorScope.WARNING).build();
    }

    @Override
    public UIInitializerContext<U> startFirstPass() {
        super.startFirstPass();
        // Url
        addDoNotBlockComponentId("openLink");
        // FilterableComboBox, DoubleList, ListHeader
        addDoNotBlockComponentId("displayDecorator");
        // TripMap
        addDoNotBlockComponentId("observeMapPane");
        addDoNotBlockComponentId("zoomIt");
        addDoNotBlockComponentId("zoomPlus");
        addDoNotBlockComponentId("zoomMinus");
        addDoNotBlockComponentId("exportPng");
        addDoNotBlockComponentId("toggleConfigure");
        // TemperatureEditor
        addDoNotBlockComponentId("cFormat");
        addDoNotBlockComponentId("fFormat");
        // CoordinateEditor
        addDoNotBlockComponentId("ddFormat");
        addDoNotBlockComponentId("dmdFormat");
        addDoNotBlockComponentId("dmsFormat");
        // Nautical length
        addDoNotBlockComponentId("FTM");
        addDoNotBlockComponentId("M");
        addDoNotBlockComponentId("KM");
        addDoNotBlockComponentId("NM");
        return this;
    }
}
