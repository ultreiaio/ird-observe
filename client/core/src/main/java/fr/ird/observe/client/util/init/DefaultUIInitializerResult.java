package fr.ird.observe.client.util.init;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditor;
import io.ultreia.java4all.jaxx.widgets.list.DoubleList;
import io.ultreia.java4all.jaxx.widgets.list.ListHeader;
import org.nuiton.jaxx.runtime.init.UIInitializerResult;
import org.nuiton.jaxx.validator.swing.tab.JTabbedPaneValidator;
import org.nuiton.jaxx.widgets.datetime.DateTimeEditor;
import org.nuiton.jaxx.widgets.datetime.TimeEditor;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditor;

import java.util.List;

/**
 * Result of init.
 * <p>
 * Created on 10/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class DefaultUIInitializerResult extends UIInitializerResult {

    private final JTabbedPaneValidator tabbedPaneValidator;
    private final JTabbedPaneValidator subTabbedPaneValidator;

    public DefaultUIInitializerResult(DefaultUIInitializerContext<?> context) {
        super(context);
        this.tabbedPaneValidator = context.getTabbedPaneValidator();
        this.subTabbedPaneValidator = context.getSubTabbedPaneValidator();
    }

    public JTabbedPaneValidator getTabbedPaneValidator() {
        return tabbedPaneValidator;
    }

    public JTabbedPaneValidator getSubTabbedPaneValidator() {
        return subTabbedPaneValidator;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<FilterableComboBox<?>> getComboBoxes() {
        return (List) getComponentsList(FilterableComboBox.class);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<ListHeader<?>> getListHeaders() {
        return (List) getComponentsList(ListHeader.class);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<DoubleList<?>> getDoubleLists() {
        return (List) getComponentsList(DoubleList.class);
    }

    public List<CoordinatesEditor> getCoordinateEditors() {
        return getComponentsList(CoordinatesEditor.class);
    }

    public List<DateTimeEditor> getDateTimeEditors() {
        return getComponentsList(DateTimeEditor.class);
    }

    public List<TimeEditor> getTimeEditors() {
        return getComponentsList(TimeEditor.class);
    }

    public List<TemperatureEditor> getTemperatureEditors() {
        return getComponentsList(TemperatureEditor.class);
    }

    public List<NauticalLengthEditor> getNauticalLengthEditors() {
        return getComponentsList(NauticalLengthEditor.class);
    }

}
