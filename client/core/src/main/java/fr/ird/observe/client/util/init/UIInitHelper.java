package fr.ird.observe.client.util.init;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.validation.ContentMessageTableRenderer;
import fr.ird.observe.client.datasource.validation.ObserveValidatorMessageMouseListener;
import io.ultreia.java4all.jaxx.widgets.combobox.BeanEnumEditor;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditor;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditorConfig;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthFormat;
import io.ultreia.java4all.jaxx.widgets.list.DoubleList;
import io.ultreia.java4all.jaxx.widgets.list.ListHeader;
import io.ultreia.java4all.lang.Strings;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.painter.Painter;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableMouseListener;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.nuiton.jaxx.widgets.datetime.DateEditor;
import org.nuiton.jaxx.widgets.datetime.DateTimeEditor;
import org.nuiton.jaxx.widgets.datetime.TimeEditor;
import org.nuiton.jaxx.widgets.datetime.UnlimitedTimeEditor;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.number.NumberEditor;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditor;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditorConfig;
import org.nuiton.jaxx.widgets.temperature.TemperatureFormat;
import org.nuiton.jaxx.widgets.text.BigTextEditor;
import org.nuiton.jaxx.widgets.text.NormalTextEditor;
import org.nuiton.jaxx.widgets.text.UrlEditor;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.UIDefaults;
import javax.swing.table.TableColumn;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static io.ultreia.java4all.i18n.I18n.t;
import static org.nuiton.jaxx.runtime.init.UIInitializerSupport.bindFromBean;

/**
 * To manage init of our ui.
 * <p>
 * Created on 09/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class UIInitHelper {

    /**
     * Components which must NOT be blocked by any layer in ui.
     *
     * @since 2.0
     */
    public static final String[] ACCEPTABLE_COMPONENTS = {
            // show progress ui in access import
            "showProgressButton",
            // copy global progression of resume admin tab to clipboard
            "globalProgressionCopyToClipBoard",
            // copy progression of any admin tab to clipboard
            "copyToClipBoard",
            "fr.ird.observe.client.ui.admin.actions.CopyToClipBoard"
    };
    public static final Class<?>[] ACCEPTABLE_COMPONENTS_TYPE = {
            JScrollBar.class, JTabbedPane.class, JTable.class
    };
    public static final String DECORATOR_CLASSIFIER = "decoratorClassifier";

    public static void init(JScrollPane selectedTreePane, JTree tree) {

        // customize tree selection colors
        UIDefaults defaults = new UIDefaults();

        Painter<JComponent> painter = (g, c, w, h) -> g.fillRect(0, 0, w, h);
        defaults.put("Tree:TreeCell[Enabled+Selected].backgroundPainter", painter);
        defaults.put("Tree:TreeCell[Enabled+Focused].backgroundPainter", painter);
        defaults.put("Tree:TreeCell[Focused+Selected].backgroundPainter", painter);

        tree.putClientProperty("Nimbus.Overrides", defaults);
        // Fix http://forge.codelutin.com/issues/2781
        selectedTreePane.getViewport().setBackground(Color.WHITE);
    }

    public static void init(JSplitPane editor) {
        InputMap inputMap = editor.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "none");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0), "none");
    }

    public static void init(JTabbedPane editor) {
        InputMap inputMap = editor.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke("ctrl pressed TAB"), "none");
        inputMap.put(KeyStroke.getKeyStroke("shift ctrl pressed TAB"), "none");
        inputMap.put(KeyStroke.getKeyStroke("ctrl pressed UP"), "none");
        inputMap.put(KeyStroke.getKeyStroke("ctrl pressed KP_UP"), "none");

        inputMap = editor.getInputMap(JComponent.WHEN_FOCUSED);
        inputMap.put(KeyStroke.getKeyStroke("ctrl pressed KP_UP"), "none");
        inputMap.put(KeyStroke.getKeyStroke("ctrl pressed DOWN"), "none");
        inputMap.put(KeyStroke.getKeyStroke("pressed SPACE"), "none");

        editor.putClientProperty("notBlocking", true);
    }

    public static void init(JTable editor) {
        InputMap inputMap = editor.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke("pressed UP"), "none");
        inputMap.put(KeyStroke.getKeyStroke("pressed PAGE_UP"), "none");
        inputMap.put(KeyStroke.getKeyStroke("pressed DOWN"), "none");
        inputMap.put(KeyStroke.getKeyStroke("pressed PAGE_DOWN"), "none");
        inputMap.put(KeyStroke.getKeyStroke("pressed LEFT"), "none");
        inputMap.put(KeyStroke.getKeyStroke("pressed RIGHT"), "none");
        inputMap.put(KeyStroke.getKeyStroke("pressed F2"), "none");
        inputMap.put(KeyStroke.getKeyStroke("pressed F8"), "none");
    }

    public static void init(JTree editor) {
        editor.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0), "none");
    }

    public static void init(JScrollPane editor) {
        editor.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "none");
        editor.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "none");
    }

    public static void init(JToolBar editor) {
        editor.setBorderPainted(false);
        editor.setFloatable(false);
        editor.setOpaque(false);
    }

    public static void init(BigTextEditor editor) {
        editor.setMinimumSize(new Dimension(50, 5 * 24));
        editor.getTextEditor().setRows(4);
        editor.setResetTip(t("observe.ui.action.reset"));
        editor.setInfoLabelTip(t("observe.ui.textArea.tip"));
        editor.init();
    }

    public static void init(NormalTextEditor editor) {
        editor.setResetTip(t("observe.ui.action.reset"));
        editor.init();
    }

    public static void init(UrlEditor editor) {
        editor.setResetTip(t("observe.ui.action.reset"));
        editor.init();
    }

    public static void init(JAXXObject ui, TemperatureEditor editor, boolean autoPopupNumberEditor, boolean showPopupButton, TemperatureFormat defaultFormat) {
        NumberEditor numberEditor = editor.getEditor();
        numberEditor.setShowReset(true);
        numberEditor.setAutoPopup(autoPopupNumberEditor);
        numberEditor.setShowPopupButton(showPopupButton);
        String propertyName = editor.getName();
        JLabel label = (JLabel) ui.getObjectById(propertyName + "Label");
        Objects.requireNonNull(label, "can't find label for temperature editor " + editor);
        editor.setConfig(new TemperatureEditorConfig(defaultFormat, label.getText(), propertyName));
        editor.init(label);
    }

    public static void init(JAXXObject ui, NauticalLengthEditor editor, boolean autoPopupNumberEditor, boolean showPopupButton, NauticalLengthFormat defaultFormat) {
        NumberEditor numberEditor = editor.getEditor();
        numberEditor.setShowReset(true);
        numberEditor.setAutoPopup(autoPopupNumberEditor);
        numberEditor.setShowPopupButton(showPopupButton);
        String propertyName = editor.getName();
        JLabel label = (JLabel) ui.getObjectById(propertyName + "Label");
        Objects.requireNonNull(label, "can't find label for nautical length editor " + editor);
        editor.setConfig(new NauticalLengthEditorConfig(defaultFormat, label.getText(), propertyName));
        editor.init(label);

    }

    public static void init(NumberEditor editor, boolean autoPopupNumberEditor, boolean showPopupButton) {
        editor.setShowReset(true);
        editor.setAutoPopup(autoPopupNumberEditor);
        editor.setShowPopupButton(showPopupButton);
        editor.init();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void init(DoubleList editor) {
        if (Strings.isEmpty(editor.getModel().getProperty())) {
            editor.setProperty(editor.getName());
        }
        editor.setI18nPrefix("observe.common.");
        editor.setShowDecorator(true);
        editor.setShowReset(true);
        editor.setShowSelectPopupEnabled(false);
        editor.setFilterable(false);
        editor.setShowListLabel(true);
        editor.setAutoSortSelectedList(true);
        editor.putClientProperty("notBlocking", true);
        bindFromBean(editor, editor.getModel().getProperty(), e -> e.getModel().getSelected(), (e, v) -> editor.setSelected(v));

    }

    public static void init(ListHeader<?> editor) {
        editor.setI18nPrefix("observe.common.");
    }

    public static void init(JAXXObject ui, JLabel editor) {
        String editorName = editor.getName();
        if (editorName.endsWith("Label")) {
            editorName = Strings.removeEnd(editorName, "Label");
            Object objectById = ui.getObjectById(editorName);
            if (objectById instanceof JComponent) {
                editor.setLabelFor((Component) objectById);
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static void init(FilterableComboBox editor) {
        editor.setI18nPrefix("observe.common.");
        editor.setShowReset(true);
//        editor.setMinimumSize(new Dimension(0, 24));
        if (Strings.isEmpty(editor.getConfig().getProperty())) {
            editor.setProperty(editor.getName());
        }
        String property = editor.getConfig().getProperty();
        bindFromBean(editor, property, e -> e.getModel().getSelectedItem(), (e, v) -> editor.setSelectedItem(v));

    }

    public static void init(TimeEditor editor) {
        editor.init();
    }

    public static void init(UnlimitedTimeEditor editor) {
        editor.init();
        editor.getSliderHidorToolBar().setVisible(false);
    }

    public static void init(DateTimeEditor editor) {
        editor.setDateFormat("dd/MM/yyyy");
        editor.init();
        editor.getDateEditor().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "none");
        init(editor.getDayDateEditor(), editor.getModel()::getDayDate, editor.getModel()::setDayDate);
    }

    public static void init(DateEditor editor) {
        editor.setDateFormat("dd/MM/yyyy");
        editor.init();
        init(editor.getDateEditor(), editor.getModel()::getDate, editor.getModel()::setDate);
    }

    public static void init(DateEditor editor, Locale locale) {
        editor.setLocale(locale);
        editor.init();
        init(editor.getDateEditor(), editor.getModel()::getDate, editor.getModel()::setDate);
    }

    //FIXME Move back to JAXX
    private static void init(JXDatePicker picker, Supplier<Date> getter, Consumer<Date> setter) {
        picker.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "none");
        picker.getEditor().setFocusLostBehavior(JFormattedTextField.COMMIT);
        PropertyChangeListener listener = e -> {
            Date date = picker.getDate();
            Date oldDate = getter.get();
            if (!Objects.equals(oldDate, date)) {
                setter.accept(date);
            }
        };
        picker.addPropertyChangeListener("date", listener);
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2182
        picker.getMonthView().setZoomable(true);
    }

    public static void init(CoordinatesEditor editor, CoordinateFormat coordinateFormat) {
        editor.setFormat(coordinateFormat);
//        editor.setFormat(org.nuiton.jaxx.widgets.gis.CoordinateFormat.dms);
        editor.setDisplayZeroWhenNull(false);
        editor.setFillWithZero(true);
        editor.setShowGlobalResetButton(true);
        editor.init();
    }

    public static <B extends Enum<B>> void init(BeanEnumEditor<B> editor) {
        editor.init(Enum::toString);
    }

    public static void initErrorTable(JTable errorTable) {
        errorTable.setAutoCreateRowSorter(true);
        SwingValidatorUtil.installUI(errorTable, new ContentMessageTableRenderer());
        errorTable.getTableHeader().setReorderingAllowed(false);
        errorTable.setRowSorter(null);
        errorTable.setCellSelectionEnabled(false);
        errorTable.setRowSelectionAllowed(true);
        errorTable.setFillsViewportHeight(true);
        errorTable.setRowHeight(20);
        errorTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        TableColumn column = errorTable.getColumnModel().getColumn(1);
        column.setPreferredWidth(150);
        column.setMinWidth(100);
        column = errorTable.getColumnModel().getColumn(2);
        column.setMinWidth(400);
        errorTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        SwingValidatorMessageTableMouseListener listener = SwingValidatorUtil.getErrorTableMouseListener(errorTable);
        errorTable.removeMouseListener(listener);
        listener = new ObserveValidatorMessageMouseListener();
        errorTable.addMouseListener(listener);
    }

    public static void setBinding(JComponent editor, KeyStroke[] keyStrokes, InputMap newInputMap, ActionMap newActionMap) {
        InputMap inputMap = editor.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = editor.getActionMap();
        for (KeyStroke key : keyStrokes) {
            Object actionKey = newInputMap.get(key);
            Action action = newActionMap.get(actionKey);
            if (action == null) {
                inputMap.put(key, "none");
            } else {
                inputMap.put(key, actionKey);
                actionMap.put(key, action);
            }
        }
    }

    public static void cleanInputMap(JComponent editor, KeyStroke[] keyStrokes) {
        InputMap inputMap = editor.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        Arrays.stream(keyStrokes).forEach(uiActionKeyStroke -> inputMap.put(uiActionKeyStroke, "none"));
    }

    public static void setAction(AbstractButton editor, Action action) {
        String text = editor.getText();
        String toolTipText = editor.getToolTipText();
        editor.setAction(action);
        if (text != null) {
            editor.setText(text);
        }
        if (toolTipText != null) {
            editor.setToolTipText(toolTipText);
        }
    }

    public static void initInformationPane(JTextPane comp, String message) {
        comp.setFont(comp.getFont().deriveFont(11f));
        comp.setContentType("text/html");
        comp.setEditable(false);
        comp.setFocusable(false);
        comp.setText(message);
    }
}
