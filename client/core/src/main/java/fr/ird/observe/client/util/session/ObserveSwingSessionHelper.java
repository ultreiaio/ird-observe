package fr.ird.observe.client.util.session;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.session.FilterableComboBoxState;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditor;
import io.ultreia.java4all.jaxx.widgets.length.nautical.session.NauticalLengthEditorState;
import io.ultreia.java4all.jaxx.widgets.list.DoubleList;
import io.ultreia.java4all.jaxx.widgets.list.ListHeader;
import io.ultreia.java4all.jaxx.widgets.list.session.DoubleListState;
import io.ultreia.java4all.jaxx.widgets.list.session.ListHeaderState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.session.SwingSession;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.gis.absolute.session.CoordinatesEditorState;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditor;
import org.nuiton.jaxx.widgets.temperature.session.TemperatureEditorState;

import java.awt.Component;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;

/**
 * Created on 07/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ObserveSwingSessionHelper implements Closeable {

    private static final Logger log = LogManager.getLogger(ObserveSwingSessionHelper.class);

    private final SwingSession session;

    public ObserveSwingSessionHelper(File file) {
        //FIXME Add additional states (auto detect from JaxxUIResourcesProvider ?)
        session = new SwingSession(file,
                                   false,
                                   Map.of(FilterableComboBox.class, new FilterableComboBoxState(),
                                          DoubleList.class, new DoubleListState(),
                                          ListHeader.class, new ListHeaderState(),
                                          TemperatureEditor.class, new TemperatureEditorState(),
                                          NauticalLengthEditor.class, new NauticalLengthEditorState(),
                                          CoordinatesEditor.class, new CoordinatesEditorState()));
    }

    public SwingSession getSession() {
        return session;
    }

    public void addComponent(Component c, boolean replace) {
        session.add(c, replace);
    }

    public void save() {
        try {
            if (Files.notExists(session.getFile().toPath())) {
                Files.createDirectories(session.getFile().toPath().getParent());
            }
            session.save();
        } catch (IOException e) {
            log.error("Could not save swing session", e);
        }
    }

    @Override
    public void close() {
        save();
    }
}
