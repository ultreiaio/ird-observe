package fr.ird.observe.client.util.table;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.InlineDataDto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * A cache of {@link EditableList} indexed by an integer
 * Created on 17/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class EditableListCache<E extends BusinessDto & InlineDataDto> {

    /**
     * Dto type.
     */
    private final Class<E> dtoType;
    /**
     * Cache of data list per row.
     * <p>
     * Typical usage is having an inline table inside a ContentTableUI:
     * <p>
     * Each row of the ContentTableUI will get an entry here for his inline data rows.
     */
    private final Map<Integer, EditableList<E>> cacheByRow;

    public EditableListCache(Class<E> dtoType) {
        this.dtoType = dtoType;
        this.cacheByRow = new TreeMap<>();
    }

    public List<E> get(int rowIndex) {
        if (rowIndex == -1) {
            return Collections.emptyList();
        }
        EditableList<E> list = cacheByRow.get(rowIndex);
        return list == null ? null : list.getData();
    }

    public void put(int editingRow, List<E> data) {
        EditableList<E> editableList = getNotExistingEditableList(editingRow);
        // Get a copy of the list (to avoid to edit the content of the list)
        List<E> original = copyList(data);
        editableList.setOriginal(original);
        editableList.setData(data);
        cacheByRow.put(editingRow, editableList);
    }

    public void update(int editingRow, List<E> data) {
        EditableList<E> editableList = getExistingEditableList(editingRow);
        editableList.setData(data);
    }

    public void replace(int editingRow, List<E> data) {
        EditableList<E> editableList = getExistingEditableList(editingRow);
        List<E> original = copyList(data);
        editableList.setOriginal(original);
        editableList.setData(data);
    }

    public List<E> reset(int rowToReset) {
        EditableList<E> editableList = getExistingEditableList(rowToReset);
        // restore a copy of original data
        List<E> newData = copyList(editableList.getOriginal());
        editableList.setData(newData);
        return newData;
    }

    public void remove(int rowToDelete) {
        cacheByRow.remove(rowToDelete);
        // decrements all row indexes after the deleted one
        List<Integer> rows = new ArrayList<>(cacheByRow.keySet());
        Collections.sort(rows);
        for (Integer row : rows) {
            if (row > rowToDelete) {
                // set now to row -1
                EditableList<E> remove = cacheByRow.remove(row);
                cacheByRow.put(row - 1, remove);
            }
        }
    }

    public void moveTo(int row1, int row2) {
        EditableList<E> list1 = cacheByRow.remove(row1);
        EditableList<E> list2 = cacheByRow.remove(row2);
        if (list1 != null) {
            cacheByRow.put(row2, list1);
        }
        if (list2 != null) {
            cacheByRow.put(row1, list2);
        }
    }

    public void clear() {
        cacheByRow.clear();
    }

    protected E createNewRow(E incoming, Date now) {
        E result = BusinessDto.newDto(dtoType, now);
        incoming.copy(result);
        return result;
    }

    protected List<E> copyList(List<E> data) {
        Date now = new Date();
        return data.stream().map(e -> createNewRow(e, now)).collect(Collectors.toList());
    }

    protected EditableList<E> getExistingEditableList(int rowIndex) {
        EditableList<E> editableList = cacheByRow.get(rowIndex);
        if (editableList == null) {
            throw new IllegalStateException(String.format("No list found for row: %d", rowIndex));
        }
        return editableList;
    }

    protected EditableList<E> getNotExistingEditableList(int rowIndex) {
        EditableList<E> editableList = cacheByRow.get(rowIndex);
        if (editableList != null) {
            throw new IllegalStateException("Cant have a list for row: " + rowIndex);
        }
        return new EditableList<>();
    }

}
