package fr.ird.observe.client.util.table;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.validation.ObserveSwingValidator;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.data.InlineDataDto;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.validator.swing.SwingValidator;

import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 12/3/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public abstract class EditableTableModel<E extends InlineDataDto> extends AbstractTableModel {

    public static final String EMPTY_PROPERTY = "empty";
    public static final String MODIFIED_PROPERTY = "modified";
    public static final String EDITABLE_PROPERTY = "editable";
    public static final String VALID_PROPERTY = "valid";
    public static final String SELECTED_ROW_INDEX_PROPERTY = "selectedRowIndex";
    public static final String SELECTED_ROW_PROPERTY = "selectedRow";
    public static final String SELECTION_EMPTY_PROPERTY = "selectionEmpty";
    public static final String ROW_COUNT_PROPERTY = "rowCount";

    private static final Logger log = LogManager.getLogger(EditableTableModel.class);
    private static final long serialVersionUID = 1L;
    /**
     * Data in table model.
     */
    private final List<E> data = new ArrayList<>();
    /**
     * Cache of valid states.
     */
    private final Map<Integer, NuitonValidatorScope> rowValidStates = new TreeMap<>();
    /**
     * To fire property changes.
     */
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    /**
     * Internal row validator.
     */
    private final SwingValidator<E> internalRowValidator;
    /**
     * Is can add new row?
     */
    private final boolean canAddRow;
    /**
     * Type of data in model.
     */
    private final Class<E> type;
    /**
     * Ui editing row validator.
     */
    private SwingValidator<E> rowValidator;
    /**
     * Is model editable?
     */
    private boolean editable;
    /**
     * Is model valid?
     */
    private boolean valid;
    /**
     * Is model modified?
     */
    private boolean modified;
    /**
     * Can we move rows?
     */
    private boolean canMoveRows;
    /**
     * Selected row index.
     */
    private int selectedRowIndex = -1; /* -1 = pas de selection */
    /**
     * Is selection adjusting?
     */
    private boolean selectionIsAdjusting;
    /**
     * To decorate a row.
     */
    private Decorator rowDecorator;
    /**
     * Selection model.
     */
    private transient ListSelectionModel selectionModel;

    protected EditableTableModel(boolean canAddRow) {
        this.canAddRow = canAddRow;
        type = ObserveUtil.getType(this, 0);
        internalRowValidator = ObserveSwingValidator.newValidator(type, "update", NuitonValidatorScope.values());
    }

    protected abstract E createNewRow();

    public Class<E> type() {
        return type;
    }

    public boolean isCanCreateNewRow(int rowIndex) {
        boolean canCreateNewRow = canAddRow && isEditable();
        if (canCreateNewRow) {
            if (rowIndex == -1) {
                return true;
            }
            E row = getData(rowIndex);
            canCreateNewRow = row != null && !row.isDataEmpty() && isRowValid(rowIndex);
        }
        return canCreateNewRow;
    }

    public final boolean isRowValid(int rowIndex) {
        if (rowIndex == selectedRowIndex && rowValidator != null) {
            return rowValidator.isValid();
        }
        NuitonValidatorScope scope = rowValidStates.get(rowIndex);
        return scope == null || scope.ordinal() > NuitonValidatorScope.ERROR.ordinal();
    }

    public final boolean isRowEmpty(int rowIndex) {
        E data = getData(rowIndex);
        return data != null && data.isDataEmpty();
    }

    protected final boolean computeRowValid(E row) {
        if (row == null || row.isDataEmpty()) {
            return true;
        }
        internalRowValidator.setBean(row);
        try {
            return internalRowValidator.isValid();
        } finally {
            internalRowValidator.setBean(null);
        }
    }

    protected final E createNewRow(E incoming) {
        E result = createNewRow();
        incoming.copy(result);
        return result;
    }

    public final boolean isRowNotEmpty(E row) {
        return !row.isDataEmpty();
    }

    public final boolean isCanAddRow() {
        return canAddRow;
    }

    public final List<E> getNotEmptyData() {
        List<E> result = new ArrayList<>();
        for (E row : data) {
            if (isRowNotEmpty(row)) {
                result.add(row);
            }
        }
        return result;
    }

    public final List<E> getValidData() {
        List<E> result = new ArrayList<>();
        int index = 0;
        for (E row : data) {
            if (isRowNotEmpty(row) && isRowValid(index++)) {
                result.add(row);
            }
        }
        return result;
    }

    public final List<E> getData() {
        return data;
    }

    public void setData(List<E> data) {
        this.data.clear();
        this.data.addAll(data);
        addDecorator(this.data);
        if (isCanAddEmptyRow()) {
            // add an empty row (no fire and select: will be done just below)
            addNewRow(0, false);
        }
        // reset previous selection
        selectedRowIndex = -1;
        fireTableDataChanged();
        // on sélectionne la première ligne (ou supprime la selection si plus de donnes)
        setSelectedRowIndex(isEmpty() ? -1 : 0, true);
        fireEmpty();
        fireRowCount(-1);
    }

    public final E getData(int rowIndex) {
        return rowIndex >= data.size() ? null : data.get(rowIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return editable;
    }

    @Override
    public final int getRowCount() {
        return data.size();
    }

    public final boolean isEditable() {
        return editable;
    }

    public final void setEditable(boolean editable) {
        boolean oldValue = this.editable;
        this.editable = editable;
        firePropertyChange(EDITABLE_PROPERTY, oldValue, editable);
    }

    public boolean isCanMoveRows() {
        return canMoveRows;
    }

    public void setCanMoveRows(boolean canMoveRows) {
        this.canMoveRows = canMoveRows;
    }

    public void removeData(int selectedRow) {
        int rowCount = getRowCount();
        this.data.remove(selectedRow);
        fireTableRowsDeleted(selectedRow, selectedRow);
        // on sélectionne la ligne précédente (ou supprime la selection si plus de donnes)
        setSelectedRowIndex(isEmpty() ? -1 : selectedRow - 1, true);
        fireEmpty();
        rowValidStates.remove(selectedRow);
        fireRowCount(rowCount);
        setModified(true);
    }

    public void removeSelectedRow() {
        // get selected row
        int selectedRowIndex1 = getSelectedRowIndex();
        // clear selection (to avoid any trouble while trying to access previous selected values which does not exists
        // any long in data list)
        setSelectedRowIndex(-1, true);
        // safe remove data
        removeData(selectedRowIndex1);
    }

    public E addNewRow() {
        int row = getRowCount();
        return addNewRow(row, true);
    }

    public final E insert(boolean before) {
        return before ? insertBeforeSelectedRow() : insertAfterSelectedRow();
    }

    public E insertBeforeSelectedRow() {
        int currentRow = getSelectedRowIndex();
        setSelectedRowIndex(-1); // clear selection
        int insertRow = currentRow;
        if (insertRow < 0) {
            insertRow = 0;
        }
        log.info(String.format("Insert before selected row: %d :: %d", currentRow, insertRow));
        return addNewRow(insertRow, true);
    }

    public E insertAfterSelectedRow() {
        int currentRow = getSelectedRowIndex();
        setSelectedRowIndex(-1); // clear selection
        int insertRow = currentRow == getRowCount() ? currentRow : currentRow + 1;
        log.info(String.format("Insert after selected row: %d :: %d", currentRow, insertRow));
        return addNewRow(insertRow, true);
    }

    public final E addNewRow(int row, boolean fireAndSelectNewRow) {
        ensureEditable();
        int rowCount = getRowCount();
        // on est autorise a ajouter une nouvelle entrée
        E bean = createNewRow();
        addDecorator(bean);
        if (row == rowCount) {
            // add new row
            data.add(bean);
        } else {
            // insert new row (but not at the end)
            data.add(row, bean);
        }
        if (fireAndSelectNewRow) {
            fireTableRowsInserted(row, row);
            fireEmpty();
            // la nouvelle ligne est celle en cours d'édition
            setSelectedRowIndex(row, true);
        }
        computeValidState(row);
        fireEmpty();
        fireRowCount(rowCount);
        //FIXME Need to review this
        setModified(fireAndSelectNewRow);
        return bean;
    }

    public final void moveTop(int selectedRow) {
        moveTo(selectedRow, 0);
    }

    public final void moveBottom(int selectedRow) {
        moveTo(selectedRow, getRowCount() - 1);
    }

    public final void moveUp(int selectedRow) {
        moveTo(selectedRow, selectedRow - 1);
    }

    public final void moveDown(int selectedRow) {
        moveTo(selectedRow, selectedRow + 1);
    }

    protected void moveTo(int selectedRow, int newSelectedRow) {
        E remove = data.remove(selectedRow);
        data.add(newSelectedRow, remove);
        fireTableRowsUpdated(newSelectedRow, selectedRow);
        setSelectedRowIndex(newSelectedRow, true);
        setModified(true);
    }

    public void clear() {
        int rowCount = getRowCount();
        setSelectedRowIndex(-1);
        setData(Collections.emptyList());
        if (rowValidator != null) {
            rowValidator.setBean(null);
        }
        validate();
        setModified(false);
        fireEmpty();
        fireRowCount(rowCount);
    }

    public final boolean isEmpty() {
        return getRowCount() == 0;
    }

    public final boolean isSelectionEmpty() {
        return getSelectedRowIndex() == -1;
    }

    public final E getSelectedRow() {
        return isSelectionEmpty() ? null : getData(getSelectedRowIndex());
    }

    public final int getSelectedRowIndex() {
        return selectedRowIndex;
    }

    public final void setSelectedRowIndex(int selectedRowIndex) {
        setSelectedRowIndex(selectedRowIndex, false);
    }

    public final void setSelectedRowIndex(int selectedRowIndex, boolean pushToSelectionModel) {
        if (!selectionIsAdjusting) {
            selectionIsAdjusting = true;
            try {
                int oldSelectedRowIndex = getSelectedRowIndex();
                //boolean oldSelectionEmpty = isSelectionEmpty();
                E oldSelectedRow = getSelectedRow();
                this.selectedRowIndex = selectedRowIndex;
                if (pushToSelectionModel && selectionModel != null) {
                    selectionModel.setSelectionInterval(selectedRowIndex, selectedRowIndex);
                }
                firePropertyChange(SELECTED_ROW_INDEX_PROPERTY, oldSelectedRowIndex, selectedRowIndex);
                firePropertyChange(SELECTION_EMPTY_PROPERTY, null /* to force fire */, isSelectionEmpty());
                firePropertyChange(SELECTED_ROW_PROPERTY, oldSelectedRow, getSelectedRow());
            } finally {
                selectionIsAdjusting = false;
            }
        }
    }

    @Override
    public final void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        E row = getData().get(rowIndex);
        boolean modified = setValueAt0(row, aValue, rowIndex, columnIndex);
        if (modified) {
            fireTableCellUpdated(rowIndex, columnIndex);
        }
        setModified(modified);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        E row = getData().get(rowIndex);
        return getValueAt0(row, rowIndex, columnIndex);
    }

    public abstract Object getValueAt0(E row, int rowIndex, int columnIndex);

    public abstract boolean setValueAt0(E row, Object aValue, int rowIndex, int columnIndex);

    public final boolean isModified() {
        return modified;
    }

    public final void setModified(boolean modified) {
        this.modified = modified;
        firePropertyChange(MODIFIED_PROPERTY, null, modified);
    }

    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public final void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public final void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    public final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    public final void fireEmpty() {
        firePropertyChange(EMPTY_PROPERTY, null, isEmpty());
    }

    public final void fireRowCount(int oldValue) {
        firePropertyChange(ROW_COUNT_PROPERTY, oldValue, getRowCount());
    }

    public final void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public final boolean isValid() {
        return valid;
    }

    public final void setValid(boolean valid) {
        this.valid = valid;
        firePropertyChange(VALID_PROPERTY, null, isValid());
    }

    public final void validate() {
        if (!isEditable()) {
            return;
        }
        log.debug("Recompute valid - state");
        boolean newValidValue = computeValidState();
        log.info(String.format("Recompute valid - state: %s", newValidValue));
        setValid(newValidValue);
    }

    protected final void ensureEditable() throws IllegalStateException {
        if (!editable) {
            throw new IllegalStateException("can not edit this model since it is marked as none editable " + this);
        }
    }

    protected boolean computeValidState() {
        boolean newValidValue = true;
        rowValidStates.clear();
        int rowCount = getRowCount();
        for (int i = 0; i < rowCount; i++) {
            boolean rowValid = computeValidState(i);
            if (!rowValid) {
                newValidValue = false;
            }
        }
        return newValidValue;
    }

    protected boolean computeValidState(int rowIndex) {
        boolean newValidValue = true;
        E row = getData(rowIndex);
        boolean rowValid = row == null || computeRowValid(row);
        if (!rowValid) {
            rowValidStates.put(rowIndex, NuitonValidatorScope.ERROR);
            newValidValue = false;
        } else {
            rowValidStates.remove(rowIndex);
        }
        return newValidValue;
    }

    public void onModifiedChanged(SwingValidator<?> validator, Boolean newValue) {
        if (newValue) {
            // modify the validator, since this is the best way to prevent table edit form actions
            // that something was modified on the form
            validator.setChanged(true);
        }
        // recompute table model valid state
        validate();
    }

    public void setSelectionModel(ListSelectionModel selectionModel) {
        this.selectionModel = selectionModel;
    }

    public void setRowValidator(SwingValidator<E> rowValidator) {
        this.rowValidator = rowValidator;
    }

    public SwingValidator<E> getRowValidator() {
        return rowValidator;
    }

    public final Decorator getRowDecorator() {
        return rowDecorator;
    }

    public final void setRowDecorator(Decorator rowDecorator) {
        this.rowDecorator = rowDecorator;
    }

    public final void addDecorator(E row) {
        if (rowDecorator != null) {
            row.registerDecorator(rowDecorator);
        }
    }

    public final void addDecorator(Collection<E> rows) {
        if (rowDecorator != null) {
            rows.forEach(r -> r.registerDecorator(rowDecorator));
        }
    }

    /**
     * FIXME We should stop trying to add a default empty row when data is empty, user must do it, not we...
     *
     * @return {@code true} if can add a new empty row if data is empty.
     */
    protected boolean isCanAddEmptyRow() {
        return this.data.isEmpty() && editable && canAddRow;
    }

}
