package fr.ird.observe.client.util.table;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.InlineDataDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JTable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.10
 */
public abstract class EditableTableModelWithCache<E extends BusinessDto & InlineDataDto> extends EditableTableModel<E> {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger(EditableTableModelWithCache.class);
    /**
     * Cache of data list per row.
     * <p>
     * Typical usage is having an inline table inside a ContentTableUI:
     * <p>
     * Each row of the ContentTableUI will get an entry here for his inline data rows.
     */
    private final EditableListCache<E> cache;
    /**
     * To get and set data list to external world.
     */
    private final EditableListProperty<E> listProperty;

    protected EditableTableModelWithCache(EditableListProperty<E> listProperty) {
        super(true);
        this.listProperty = listProperty;
        this.cache = new EditableListCache<>(type());
    }

    public final EditableListCache<E> cache() {
        return cache;
    }

    public final EditableListProperty<E> listProperty() {
        return listProperty;
    }

    public final List<E> getCacheForRow(int rowIndex) {
        return cache.get(rowIndex);
    }

    public final void storeInCacheForRowIfSelected(int editingRow) {
        if (!isEditable()) {
            return;
        }
        if (editingRow > -1) {
            // store valid data for the selected row
            List<E> data = getValidData();
            cache.update(editingRow, data);
        }
    }

    public void onBeforeResetRow(String prefix, int editingRow, JTable table) {
        if (!isEditable()) {
            return;
        }
        log.info(String.format("%sBefore reset edit bean at row: %d", prefix, editingRow));
        UIHelper.cancelEditing(table);
        List<E> newData = cache.reset(editingRow);
        setData(newData);
        validate();
    }

    public final void onAfterLoadRowBeanToEdit(String prefix, int editingRow, boolean newRow) {
        // load data from cache
        List<E> data = getCacheForRow(editingRow);
        if (data == null) {
            log.info(String.format("%s init data for row %d", prefix, editingRow));
            // first time coming on this row
            if (newRow) {
                // create mode: just init with empty list
                data = Collections.emptyList();
                log.info(String.format("%s create mode, use an empty list", prefix));
            } else {
                // updating mode: loading from db
                data = new ArrayList<>(listProperty.get());
                log.info(String.format("%s Loaded data for row %d: %d", prefix, editingRow, data.size()));
            }
            // init size measures
            cache.put(editingRow, data);
        } else {
            log.info(String.format("%s Using existing data for row %d : %d", prefix, editingRow, data.size()));
        }
        if (isEditable()) {
            // only set back to bean if editable (used for bean validation)
            listProperty.set(new LinkedList<>(data));
        }
        setData(data);
        setModified(false);
    }

    public final void onBeforeUpdateRowFromEditBean(int editingRow, boolean newRow) {
        if (!isEditable()) {
            return;
        }
        List<E> data = getValidData();
        if (newRow) {
            cache.replace(editingRow, data);
        } else {
            cache.update(editingRow, data);
        }
        listProperty.set(data);
    }

    public final void removeRow(int rowToDelete) {
        cache.remove(rowToDelete);
    }

    public final void onMoveTo(int selectedRow, int newSelectedRow) {
        if (!isEditable()) {
            return;
        }
        cache.moveTo(selectedRow, newSelectedRow);
    }

    @Override
    public final void clear() {
        cache.clear();
        super.clear();
    }

    public final void simpleClear() {
        super.clear();
    }
}
