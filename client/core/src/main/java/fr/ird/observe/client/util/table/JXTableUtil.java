package fr.ird.observe.client.util.table;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.ClientUIContextApplicationComponent;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.renderer.DecoratorTableRenderer;
import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.StringValue;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.Objects;
import java.util.function.Consumer;

import static io.ultreia.java4all.i18n.I18n.getDefaultLocale;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 18/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class JXTableUtil {

    public static void initRenderers(JXTable table, TableCellRenderer... renderers) {
        TableColumnModel columnModel = table.getColumnModel();
        Enumeration<TableColumn> columns = columnModel.getColumns();
        for (TableCellRenderer renderer : renderers) {
            columns.nextElement().setCellRenderer(renderer);
        }
    }

    public static void initEditors(JXTable table, TableCellEditor... renderers) {
        TableColumnModel columnModel = table.getColumnModel();
        Enumeration<TableColumn> columns = columnModel.getColumns();
        for (TableCellEditor editor : renderers) {
            columns.nextElement().setCellEditor(editor);
        }
    }

    public static void setMinimumHeight(int height, JXTable table, JScrollPane scroller) {
        Dimension dim = table.getPreferredScrollableViewportSize();
        dim.height = height;
        table.setPreferredScrollableViewportSize(dim);
        table.setMinimumSize(dim);
        Dimension dimHeader = table.getTableHeader().getPreferredSize();
        dim.height += dimHeader.height;
        scroller.setMinimumSize(dim);
    }

    public static void setI18nTableHeaderRenderer(JXTable table, Class<?> type, String... properties) {
        String[] all = new String[properties.length * 2];
        for (int i = 0; i < properties.length; i++) {
            String property = properties[i];
            all[2 * i] = I18nDecoratorHelper.getPropertyI18nKey(type, property + ".short");
            all[2 * i + 1] = I18nDecoratorHelper.getPropertyI18nKey(type, property);
        }
        UIHelper.setI18nTableHeaderRenderer(table, all);
    }

    public static <T extends Decorated> TableCellRenderer newDecoratedRenderer(Class<T> referenceType) {
        return newDecoratedRenderer(referenceType, null);
    }

    public static <T extends Decorated> TableCellRenderer newDecoratedRenderer(Class<T> referenceType, String context) {
        Objects.requireNonNull(referenceType);
        Decorator decorator = ClientUIContextApplicationComponent.value().getDecoratorService().getDecoratorByType(referenceType, context);
        Objects.requireNonNull(decorator, "cant find decorator for " + referenceType.getName());
        return newDecoratedRenderer(decorator);
    }

    public static TableCellRenderer newDecoratedRenderer(Decorator decorator) {
        return new DecoratorTableRenderer(decorator);
    }

    public static TableCellRenderer newEmptyNumberTableCellRenderer() {
        return newEmptyNumberTableCellRenderer(null);
    }

    public static TableCellRenderer newEmptyNumberTableCellRenderer(Consumer<JComponent> extraConsumer) {
        StringValue sv = value -> {
            if (value == null) {
                return null;
            }
            Number reference = (Number) value;
            String result = reference.toString();
//            if (reference.doubleValue() == 0) {
//                return null;
//            }
            if (reference instanceof Integer) {
                return result;
            }
            if (result.contains("E-")) {
                result = String.format("%.04f", reference.doubleValue()).replace(",", ".");
            }
            return result;
        };
        return getDefaultTableRenderer(sv, true, extraConsumer);
    }

    public static TableCellRenderer newTimeTableCellRenderer() {
        StringValue sv = value -> {
            if (value == null) {
                return null;
            }
            Date reference = (Date) value;
            return I18nDecoratorHelper.getTimeLabel(getDefaultLocale(), reference);
        };
        return getDefaultTableRenderer(sv, true);
    }
    public static TableCellRenderer newTimestampTableCellRenderer() {
        StringValue sv = value -> {
            if (value == null) {
                return null;
            }
            Date reference = (Date) value;
            return I18nDecoratorHelper.getTimestampLabel(getDefaultLocale(), reference);
        };
        return getDefaultTableRenderer(sv, true);
    }
    public static TableCellRenderer newDateTableCellRenderer() {
        StringValue sv = value -> {
            if (value == null) {
                return null;
            }
            Date reference = (Date) value;
            return I18nDecoratorHelper.getDateLabel(getDefaultLocale(), reference);
        };
        return getDefaultTableRenderer(sv, true);
    }

    public static TableCellRenderer newBooleanTableCellRenderer2() {
        StringValue sv = value -> {
            if (value == null) {
                return t("boolean.null");
            }
            Boolean reference = (Boolean) value;
            return reference ? t("boolean.true") : t("boolean.false");
        };
        return getDefaultTableRenderer(sv, true);
    }

    public static TableCellRenderer newStringTableCellRenderer(int length) {
        StringValue sv = value -> {
            if (value == null) {
                return null;
            }
            String reference = (String) value;
            return reference.length() > length ? reference.substring(0, length - 3) + "..." : reference;
        };
        return getDefaultTableRenderer(sv, false);
    }

    public static TableCellRenderer newCollectionTableCellRenderer() {
        StringValue sv = value -> {
            if (value == null) {
                return null;
            }
            Collection<?> reference = (Collection<?>) value;
            return reference.size() + "";
        };
        return getDefaultTableRenderer(sv, false);
    }

    public static <E extends Enum<E>> TableCellRenderer newEnumTableCellRenderer(Class<E> enumClass) {
        final EnumSet<E> enumValues = EnumSet.allOf(enumClass);
        StringValue sv = value -> {
            if (value == null) {
                return null;
            }
            if (enumClass.isAssignableFrom(value.getClass())) {
                return value.toString();
            }
            if (value instanceof Number) {
                Number number = (Number) value;
                return enumValues.stream().filter(e -> e.ordinal() == number.intValue()).findAny().map(Object::toString).orElse(null);
            }
            if (value instanceof String) {
                String value1 = (String) value;
                return enumValues.stream().filter(e -> e.name().equals(value1)).findAny().map(Object::toString).orElse(null);
            }
            return null;
        };
        return getDefaultTableRenderer(sv, true);
    }

    public static TableCellEditor newFloat2ColumnEditor() {
        @SuppressWarnings("unchecked") NumberCellEditor<Float> editor = (NumberCellEditor<Float>) NumberCellEditor.newFloatColumnEditor();
        editor.getNumberEditor().setSelectAllTextOnError(true);
        editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
        editor.getNumberEditor().setNumberPattern(SwingUtil.DECIMAL2_PATTERN);
        return editor;
    }

    private static DefaultTableRenderer getDefaultTableRenderer(StringValue sv, boolean useStringValue) {
        return getDefaultTableRenderer(sv, useStringValue, null);
    }

    private static DefaultTableRenderer getDefaultTableRenderer(StringValue sv, boolean useStringValue, Consumer<JComponent> extraConsumer) {
        return new DefaultTableRenderer(sv) {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                JComponent tableCellRendererComponent = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                String toolTipText;
                if (useStringValue) {
                    toolTipText = sv.getString(value);
                } else {
                    if (value == null) {
                        toolTipText = null;
                    } else {
                        if (value instanceof Collection) {
                            Collection<?> collection = (Collection<?>) value;
                            if (collection.isEmpty()) {
                                toolTipText = null;
                            } else {
                                StringBuilder sb = new StringBuilder("<html><body><ul style='font-size:8px; padding: 0;margin: 0;'>");
                                for (Object o : collection) {
                                    sb.append("<li>").append(o);
                                }
                                toolTipText = sb.toString();
                            }
                        } else {
                            toolTipText = String.valueOf(value);
                        }
                    }
                }
                tableCellRendererComponent.setToolTipText(toolTipText);
                if (extraConsumer != null) {
                    extraConsumer.accept(tableCellRendererComponent);
                }
                return tableCellRendererComponent;
            }
        };
    }

}
