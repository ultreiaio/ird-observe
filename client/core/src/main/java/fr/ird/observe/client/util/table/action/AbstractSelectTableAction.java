package fr.ird.observe.client.util.table.action;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableTableModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.JTables;

import javax.swing.AbstractAction;
import javax.swing.JTable;

/**
 * Abstract action to select a cell in a table.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public abstract class AbstractSelectTableAction<M extends EditableTableModel<?>> extends AbstractAction {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LogManager.getLogger(AbstractSelectTableAction.class);
    protected final JTable table;
    private final M model;

    public AbstractSelectTableAction(M model, JTable table) {
        this.model = model;
        this.table = table;
    }

    public abstract void install();

    protected void doSelectCell(int rowIndex, int columnIndex) {
        log.debug(String.format("Will select cell at %s", getCellCoordinate(rowIndex, columnIndex)));
        UIHelper.stopEditing(table);
        JTables.doSelectCell(table, rowIndex, columnIndex);
    }

    protected int getSelectedRow() {
        return table.getSelectedRow();
    }

    protected int getSelectedColumn() {
        return table.getSelectedColumn();
    }

    protected int getRowCount() {
        return table.getRowCount();
    }

    protected int getColumnCount() {
        return table.getColumnCount();
    }

    protected boolean isCellEditable(int rowIndex, int columnIndex) {
        return rowIndex > -1 && columnIndex > -1 && table.isCellEditable(rowIndex, columnIndex);
    }

    protected boolean isCreateNewRow(int rowIndex) {
        boolean canCreateNewRow = model.isEditable() && model.isCanAddRow();
        if (canCreateNewRow) {
            if (rowIndex == -1 && model.isEmpty()) {
                // model is empty, we surely can add a new row
                return true;
            }
            // ask model if we can add a new row
            canCreateNewRow = model.isCanCreateNewRow(rowIndex);
        }
        return canCreateNewRow;
    }

    protected String getCellCoordinate(int rowIndex, int columnIndex) {
        return String.format(" [%d, %d]", rowIndex, columnIndex);
    }

    protected void addNewRow() {
        model.addNewRow();
    }
}
