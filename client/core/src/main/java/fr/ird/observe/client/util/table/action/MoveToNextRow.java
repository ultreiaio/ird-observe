package fr.ird.observe.client.util.table.action;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableTableModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

/**
 * Action to select next editable row in a table.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public class MoveToNextRow<M extends EditableTableModel<?>> extends AbstractSelectTableAction<M> {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LogManager.getLogger(MoveToNextRow.class);

    public static <M extends EditableTableModel<?>> void install(M model, JTable table) {
        new MoveToNextRow<>(model, table).install();
    }

    protected MoveToNextRow(M model, JTable table) {
        super(model, table);
    }

    @Override
    public void install() {
        InputMap inputMap = table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = table.getActionMap();
        String actionBindingName = getClass().getName();
        inputMap.put(KeyStroke.getKeyStroke("pressed DOWN"), actionBindingName);
        actionMap.put(actionBindingName, this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int currentRow = getSelectedRow();
        int currentColumn = getSelectedColumn();
        log.debug(String.format("Move to next row editable cell %s", getCellCoordinate(currentRow, currentColumn)));
        UIHelper.stopEditing(table);
        int rowCount = getRowCount();
        if (currentRow < rowCount) {
            // go to next cell
            currentRow++;
            boolean canSelect = true;
            // select next cell
            if (currentRow >= rowCount) {
                log.debug("No next row");
                if (isCreateNewRow(currentRow - 1)) {
                    // create a new row in model
                    addNewRow();
                } else {
                    canSelect = false;
                }
            }
            if (canSelect) {
                doSelectCell(currentRow, currentColumn);
            }
        }
    }
}
