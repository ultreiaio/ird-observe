package fr.ird.observe.client.util.table.popup;

/*
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.table.EditableTableModel;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableModel;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Objects;

/**
 * Created on 12/12/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.10
 */
public abstract class AutoSelectRowAndShowPopupActionSupport {

    private static final Logger log = LogManager.getLogger(AutoSelectRowAndShowPopupActionSupport.class);

    private final JTable table;
    private final JPopupMenu popup;

    protected AutoSelectRowAndShowPopupActionSupport(JTable table) {
        this(table, new JPopupMenu());
    }

    protected AutoSelectRowAndShowPopupActionSupport(JTable table, JPopupMenu popup) {
        this.table = Objects.requireNonNull(table);
        this.popup = Objects.requireNonNull(popup);

        KeyAdapter keyAdapter = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                openRowMenu(e);
            }
        };
        this.table.addKeyListener(keyAdapter);

        MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                autoSelectRowInTable(e);
            }
        };
        this.table.addMouseListener(mouseAdapter);

        JScrollPane parent = (JScrollPane) table.getParent().getParent();
        Objects.requireNonNull(parent).addMouseListener(mouseAdapter);
        parent.addKeyListener(keyAdapter);
    }

    protected abstract void beforeOpenPopup(int modelRowIndex, int modelColumnIndex);

    public JPopupMenu getPopup() {
        return popup;
    }

    public JTable getTable() {
        return table;
    }

    protected boolean canShowPopup(TableModel model) {
        if (model instanceof EditableTableModel<?>) {
            return ((EditableTableModel<?>) model).isEditable();
        }
        return true;
    }

    private void autoSelectRowInTable(MouseEvent e) {
        if (!table.isEnabled()) {
            return;
        }
        boolean rightClick = SwingUtilities.isRightMouseButton(e);

        if (rightClick || SwingUtilities.isLeftMouseButton(e)) {

            // get the coordinates of the mouse click
            Point p = e.getPoint();

            int[] selectedRows = table.getSelectedRows();
            int[] selectedColumns = table.getSelectedColumns();

            // get the row index at this point
            int rowIndex = table.rowAtPoint(p);

            // get the column index at this point
            int columnIndex = table.columnAtPoint(p);

            log.debug(String.format("At point [%s] found Row %d, Column %d", p, rowIndex, columnIndex));

            boolean canContinue = stopEdit();
            if (!canContinue) {
                return;
            }
            TableModel model = table.getModel();
            canContinue = canShowPopup(model);
            if (!canContinue) {
                return;
            }
            if (rowIndex != -1 && columnIndex != -1) {

                // select row (could empty selection)
                if (!ArrayUtils.contains(selectedRows, rowIndex)) {
                    // set selection
                    table.setRowSelectionInterval(rowIndex, rowIndex);
                }

                // select column (could empty selection)
                if (!ArrayUtils.contains(selectedColumns, columnIndex)) {
                    table.setColumnSelectionInterval(columnIndex, columnIndex);
                }
            }
            if (rightClick) {
                showPopup(rowIndex, columnIndex, p);
            }
        }
    }

    private void openRowMenu(KeyEvent e) {
        if (!table.isEnabled()) {
            return;
        }
        if (KeyEvent.VK_CONTEXT_MENU == e.getKeyCode()) {

            // get the lowest selected row
            int[] selectedRows = table.getSelectedRows();
            int lowestRow = -1;
            for (int row : selectedRows) {
                lowestRow = Math.max(lowestRow, row);
            }
            // get the selected column
            int selectedColumn = table.getSelectedColumn();
            Rectangle r = table.getCellRect(lowestRow, selectedColumn, true);

            // get the point in the middle lower of the cell
            Point p = new Point(r.x + r.width / 2, r.y + r.height);

            log.debug(String.format("Row %d found t point [%s]", lowestRow, p));
            boolean canContinue = stopEdit();
            if (canContinue) {
                showPopup(lowestRow, selectedColumn, p);
            }
        }
    }

    private boolean stopEdit() {
        if (table.isEditing()) {

            // stop editing
            try {
                boolean stopEdit = table.getCellEditor().stopCellEditing();
                if (!stopEdit) {
                    log.warn("Could not stop edit cell...");
                    return false;
                }
            } catch (Exception e) {
                log.error("Can't stop edit, but still will do it...", e);
                table.removeEditor();
                return true;
            }
        }
        return true;
    }

    private void showPopup(int row, int column, Point p) {
        // use now model coordinate
        int rowIndex = row == -1 ? -1 : table.convertRowIndexToModel(row);
        int columnIndex = column == -1 ? -1 : table.convertColumnIndexToModel(column);
        beforeOpenPopup(rowIndex, columnIndex);
        popup.show(table, p.x, p.y);
    }

}
