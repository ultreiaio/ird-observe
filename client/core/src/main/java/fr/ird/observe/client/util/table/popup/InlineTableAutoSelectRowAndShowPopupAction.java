package fr.ird.observe.client.util.table.popup;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.table.EditableTableModel;
import fr.ird.observe.dto.data.InlineDataDto;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

/**
 * Created on 15/03/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class InlineTableAutoSelectRowAndShowPopupAction<E extends InlineDataDto, M extends EditableTableModel<E>> extends AutoSelectRowAndShowPopupActionSupport {

    private final JMenuItem addWidget;
    private final JMenuItem deleteWidget;
    private final M tableModel;

    public InlineTableAutoSelectRowAndShowPopupAction(JTable table,
                                                      M tableModel,
                                                      JPopupMenu popup,
                                                      JMenuItem addWidget,
                                                      JMenuItem deleteWidget) {
        super(table, popup);
        this.addWidget = addWidget;
        this.deleteWidget = deleteWidget;
        this.tableModel = tableModel;
    }

    @Override
    protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {
        boolean editable = tableModel.isEditable();
        boolean canAdd = editable && tableModel.isValid();
        boolean canDelete = editable && !tableModel.isSelectionEmpty();
        if (canAdd) {
            if (!tableModel.isEmpty()) {
                canAdd = tableModel.isCanCreateNewRow(tableModel.getRowCount() - 1);
            }
        }
        addWidget.setEnabled(canAdd);
        deleteWidget.setEnabled(canDelete);
    }

}
