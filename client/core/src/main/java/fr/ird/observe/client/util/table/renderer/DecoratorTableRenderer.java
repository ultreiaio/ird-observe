package fr.ird.observe.client.util.table.renderer;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.LabelProvider;
import org.jdesktop.swingx.renderer.StringValue;

import javax.swing.JComponent;
import javax.swing.JTable;
import java.awt.Component;

/**
 * Created at 11/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class DecoratorTableRenderer extends DefaultTableRenderer {

    public DecoratorTableRenderer(Decorator decorator) {
        super(new DecoratorLabelProvider(decorator));
    }

    @Override
    public DecoratorLabelProvider getComponentProvider() {
        return (DecoratorLabelProvider) super.getComponentProvider();
    }
    public Decorator getDecorator() {
        return getComponentProvider().getStringValue().getDecorator();
    }
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JComponent tableCellRendererComponent = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        String toolTipText = getString(value);
        tableCellRendererComponent.setToolTipText(toolTipText);
        return tableCellRendererComponent;
    }

    public static class DecoratorStringValue implements StringValue {

        private final Decorator decorator;

        private DecoratorStringValue(Decorator decorator) {
            this.decorator = decorator;
        }

        @Override
        public String getString(Object value) {
            if (value == null) {
                return null;
            }
            Decorated reference = (Decorated) value;
            reference.registerDecorator(decorator);
            return reference.decorate();
        }

        public Decorator getDecorator() {
            return decorator;
        }
    }

    public static class DecoratorLabelProvider extends LabelProvider {


        public DecoratorLabelProvider(Decorator decorator) {
            super(new DecoratorStringValue(decorator));
        }

        @Override
        public DecoratorStringValue getStringValue() {
            return (DecoratorStringValue) super.getStringValue();
        }
    }
}
