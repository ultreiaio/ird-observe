package fr.ird.observe.client.util.treetable;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.EventObject;

/**
 * Created by tchemit on 05/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.0
 */
public class JRadioButtonCellEditor extends AbstractCellEditor implements TableCellEditor {

    private JRadioButton editorComponent;
    private EditorDelegate delegate;

    public JRadioButtonCellEditor() {
        editorComponent = new JRadioButton();
        editorComponent.setHorizontalAlignment(SwingConstants.CENTER);
        delegate = new EditorDelegate();
        editorComponent.addActionListener(delegate);
        editorComponent.setRequestFocusEnabled(false);
    }

    public Component getComponent() {
        return editorComponent;
    }

    @Override
    public Object getCellEditorValue() {
        return delegate.getCellEditorValue();
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return delegate.isCellEditable(anEvent);
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return delegate.shouldSelectCell(anEvent);
    }

    @Override
    public boolean stopCellEditing() {
        return delegate.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        delegate.cancelCellEditing();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected,
                                                 int row, int column) {
        delegate.setValue(value);
        //in order to avoid a "flashing" effect when clicking a checkbox
        //in a table, it is important for the editor to have as a border
        //the same border that the renderer has, and have as the background
        //the same color as the renderer has. This is primarily only
        //needed for JCheckBox since this editor doesn't fill all the
        //visual space of the table cell, unlike a text field.
        TableCellRenderer renderer = table.getCellRenderer(row, column);
        Component c = renderer.getTableCellRendererComponent(table, value, isSelected, true, row, column);
        if (c != null) {
            editorComponent.setOpaque(true);
            editorComponent.setBackground(c.getBackground());
            if (c instanceof JComponent) {
                editorComponent.setBorder(((JComponent) c).getBorder());
            }
        } else {
            editorComponent.setOpaque(false);
        }
        return editorComponent;
    }

    protected class EditorDelegate implements ActionListener, ItemListener, Serializable {

        boolean getCellEditorValue() {
            return editorComponent.isSelected();
        }

        public void setValue(Object value) {

            boolean selected = false;
            if (value instanceof Boolean) {
                selected = (Boolean) value;
            } else if (value instanceof String) {
                selected = value.equals("true");
            }
            editorComponent.setSelected(selected);
        }

        public boolean isCellEditable(EventObject anEvent) {
            return !(anEvent instanceof MouseEvent) || ((MouseEvent) anEvent).getClickCount() > 0;
        }

        boolean shouldSelectCell(@SuppressWarnings("unused") EventObject ignored) {
            return true;
        }

        boolean stopCellEditing() {
            fireEditingStopped();
            return true;
        }

        void cancelCellEditing() {
            fireEditingCanceled();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.stopCellEditing();
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            this.stopCellEditing();
        }
    }

}
