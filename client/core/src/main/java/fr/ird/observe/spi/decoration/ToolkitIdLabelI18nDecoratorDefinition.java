package fr.ird.observe.spi.decoration;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.decoration.ToolkitIdLabelDecoratorDefinition;
import io.ultreia.java4all.decoration.Decorator;
import org.nuiton.jaxx.runtime.i18n.I18nDecoratorDefinition;

/**
 * Created on 27/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@AutoService(I18nDecoratorDefinition.class)
public class ToolkitIdLabelI18nDecoratorDefinition implements I18nDecoratorDefinition {
    @Override
    public boolean accept(Decorator decorator) {
        return ToolkitIdLabelDecoratorDefinition.class.equals(decorator.definition().getClass());
    }

    @Override
    public Class<?> getDecoratorType(Decorator decorator) {
        if (accept(decorator)) {
            return ((ToolkitIdLabelDecoratorDefinition) decorator.definition()).dtoType();
        }
        return null;
    }
}
