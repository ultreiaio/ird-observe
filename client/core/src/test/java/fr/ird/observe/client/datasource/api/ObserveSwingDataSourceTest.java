package fr.ird.observe.client.datasource.api;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.ClientUIContextApi;
import fr.ird.observe.client.datasource.config.form.ConfigurationModel;
import fr.ird.observe.client.datasource.config.form.LocalConfigurationModel;
import fr.ird.observe.client.datasource.config.form.RemoteConfigurationModel;
import fr.ird.observe.client.datasource.config.form.ServerConfigurationModel;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.test.ObserveFixtures;
import io.ultreia.java4all.util.Version;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created on 06/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
@RunWith(Parameterized.class)
public class ObserveSwingDataSourceTest {

    @ClassRule
    public static final TemplateClassResource CLASS_RULE = new TemplateClassResource();
    @Parameterized.Parameter
    public Locale locale;
    private ClientUIContextApi applicationContext;
    private ObserveDataSourceInformation dataSourceInformation;

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Iterable<Locale> data() {
        return CLASS_RULE.data();
    }

    @Before
    public void setUp() {
        applicationContext = CLASS_RULE.getApplicationContext();
        CLASS_RULE.setLocale(locale);
        dataSourceInformation = new ObserveDataSourceInformation(Permission.ALL,
                                                                 true,
                                                                 true,
                                                                 Version.VZERO,
                                                                 applicationContext.getClientConfig().getModelVersion(),
                                                                 List.of());
    }

    @Test
    public void generateLocal() {
        LocalConfigurationModel model = new LocalConfigurationModel();

        model.setDirectory(new File("Here"));
        model.setUsername("username");
        model.setPassword("password".toCharArray());
        model.setDbName("databaseName");
        ObserveDataSourceConfigurationTopiaH2 configuration = model.getConfiguration();
        configuration.setLabel("TaiseLocal");

        ObserveDataSourceConnection connection = configuration.toConnection("Token", dataSourceInformation);
        generate(model, connection);
    }

    @Test
    public void generateRemote() {
        RemoteConfigurationModel model = new RemoteConfigurationModel();

        model.setJdbcUrl("jdbcUrl");
        model.setUsername("username");
        model.setPassword("password".toCharArray());
        model.setUseSsl(true);
        ObserveDataSourceConfigurationTopiaPG configuration = model.getConfiguration();
        configuration.setLabel("TaiseRemote");
        ObserveDataSourceConnection connection = configuration.toConnection("Token", dataSourceInformation);
        generate(model, connection);
    }

    @Test
    public void generateServer() {


        ServerConfigurationModel model = new ServerConfigurationModel();
        model.setServerUrl("https://serverUrl");
        model.setLogin("login");
        model.setPassword("password".toCharArray());
        model.setDatabaseName("optionalDatabaseName");
        ObserveDataSourceConfiguration configuration = model.getConfiguration();
        configuration.setLabel("TaiseServer");
        ObserveDataSourceConnection connection = configuration.toConnection("Token", dataSourceInformation);
        generate(model, connection);
    }

    private void generate(ConfigurationModel configuration, ObserveDataSourceConnection connection) {

        ObserveSwingDataSource dataSource = new ObserveSwingDataSource(
                applicationContext.getClientConfig(),
                applicationContext.getServiceFactory(),
                applicationContext.getDecoratorService(),
                configuration.getConfiguration()) {
            @Override
            protected Date simpleReferentialSynchronisationLastUpdate() {
                return ObserveFixtures.DATE;
            }

            @Override
            protected Date advancedReferentialSynchronisationLastUpdate() {
                return null;
            }
        };
        assertGenerate(dataSource);
        dataSource.setConnection(connection);
        assertGenerate(dataSource);
    }

    private void assertGenerate(ObserveSwingDataSource dataSource) {
        String expected = ObserveSwingDataSourceTemplate.generate(null);
        Assert.assertNotNull(expected);
        expected = ObserveSwingDataSourceTemplate.generate(dataSource);
        String actual = dataSource.getSummaryText();
        Assert.assertNotNull(expected);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected, actual);
        System.out.println(expected);
    }
}
