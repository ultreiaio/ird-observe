package fr.ird.observe.client.datasource.api;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.ClientUIContextApi;
import fr.ird.observe.client.ObserveSwingApplicationContext;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.configuration.ClientResources;
import fr.ird.observe.test.TestConfig;
import fr.ird.observe.test.ToolkitFixtures;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.config.ArgumentsParserException;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.File;
import java.util.List;
import java.util.Locale;

/**
 * Created on 25/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
public class TemplateClassResource implements TestRule {
    private static ClientConfig config;
    private ObserveSwingApplicationContext applicationContext;

    public Iterable<Locale> data() {
        return List.of(Locale.FRANCE, Locale.UK, new Locale("es", "ES"));
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                before(description);
                try {
                    base.evaluate();
                } finally {
                    after(description);
                }
            }
        };
    }

    private void before(Description description) throws ArgumentsParserException {
        if (ApplicationContext.isInit()) {
            ApplicationContext.get().close();
        }
        File testDirectory = ToolkitFixtures.getTestBasedir(description.getTestClass()).toPath().getParent().resolve(description.getTestClass().getName()).toFile();
        config = ClientConfig.forTest();
        config.setDataDirectory(testDirectory);
        config.initConfig();
        ClientConfig.initI18n(config);
        File dir = config.getResourcesDirectory();
        config.loadUIConfigFile(ClientResources.ui, dir);
        applicationContext = ObserveSwingApplicationContext.init(config, TestConfig.class);
    }

    private void after(@SuppressWarnings("unused") Description description) {
        if (ApplicationContext.isInit()) {
            ApplicationContext.get().close();
        }
    }

    public void setLocale(Locale locale) {
        config.setLocale(locale);
    }

    public ClientUIContextApi getApplicationContext() {
        return applicationContext.getComponentValue(ClientUIContextApi.class);
    }
}
