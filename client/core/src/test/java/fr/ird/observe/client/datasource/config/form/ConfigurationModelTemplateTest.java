package fr.ird.observe.client.datasource.config.form;

/*-
 * #%L
 * ObServe Client :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.TemplateClassResource;
import fr.ird.observe.client.datasource.config.ConnexionStatus;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.security.Permission;
import io.ultreia.java4all.util.Version;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.util.List;
import java.util.Locale;

/**
 * Created on 25/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
@RunWith(Parameterized.class)
public class ConfigurationModelTemplateTest {

    @ClassRule
    public static final TemplateClassResource CLASS_RULE = new TemplateClassResource();

    @Parameterized.Parameter
    public Locale locale;

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Iterable<Locale> data() {
        return CLASS_RULE.data();
    }

    @Before
    public void setUp() {
        CLASS_RULE.setLocale(locale);
    }

    @Test
    public void generateLocal() {
        LocalConfigurationModel model = new LocalConfigurationModel();
        generate(model, ConnexionStatus.UNTESTED);

        model.setConnexionError(new NullPointerException("NPE"));
        generate(model, ConnexionStatus.FAILED);

        model.setDirectory(new File("Here"));
        model.setUsername("username");
        model.setPassword("password".toCharArray());
        model.setDbName("databaseName");
        model.setDataSourceInformation(new ObserveDataSourceInformation(Permission.ALL,
                                                                        true,
                                                                        true,
                                                                        Version.VZERO,
                                                                        model.getModelVersion(),
                                                                        List.of()));
        generate(model, ConnexionStatus.SUCCESS);
    }

    @Test
    public void generateRemote() {
        RemoteConfigurationModel model = new RemoteConfigurationModel();
        generate(model, ConnexionStatus.UNTESTED);

        model.setConnexionError(new NullPointerException("NPE"));
        generate(model, ConnexionStatus.FAILED);

        model.setJdbcUrl("jdbcUrl");
        model.setUsername("username");
        model.setPassword("password".toCharArray());
        model.setUseSsl(true);
        model.setDataSourceInformation(new ObserveDataSourceInformation(Permission.ALL,
                                                                        true,
                                                                        true,
                                                                        Version.VZERO,
                                                                        model.getModelVersion(),
                                                                        List.of()));
        generate(model, ConnexionStatus.SUCCESS);
    }

    @Test
    public void generateServer() {
        ServerConfigurationModel model = new ServerConfigurationModel();
        generate(model, ConnexionStatus.UNTESTED);

        model.setConnexionError(new NullPointerException("NPE"));
        generate(model, ConnexionStatus.FAILED);

        model.setServerUrl("serverUrl");
        model.setLogin("login");
        model.setPassword("password".toCharArray());
        model.setDatabaseName("optionalDatabaseName");
        model.setDataSourceInformation(new ObserveDataSourceInformation(Permission.ALL,
                                                                        true,
                                                                        true,
                                                                        Version.VZERO,
                                                                        model.getModelVersion(),
                                                                        List.of()));
        generate(model, ConnexionStatus.SUCCESS);
    }

    private void generate(ConfigurationModel model, ConnexionStatus status) {
        model.setConnexionStatus(status);
        String expected = ConfigurationModelTemplate.generate(model);
        String actual = model.getConnexionStatusText();
        Assert.assertNotNull(expected);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected, actual);
        System.out.println(expected);
    }
}
