# Abstract

This module exposes the long actions of the client application:

  * referential synchronisation (simple and advanced)
  * data synchronisation (simple and advanced)
  * consolidation
  * pairing
  * report