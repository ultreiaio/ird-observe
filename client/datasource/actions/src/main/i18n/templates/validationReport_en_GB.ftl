<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.actions.validate.ValidateModel" -->
<#--
 #%L
 ObServe Client :: DataSource :: Actions
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<!doctype html>
<html lang="en">
<#macro displayNode node>
<#-- @ftlvariable name="node" type="fr.ird.observe.client.datasource.actions.validate.tree.ValidationNode" -->
    <#assign withChildren = node.childCount != 0 />
    <#assign withMessages = node.messagesCount != 0 />
    <li>
        <span id="${node.id}" class="<#if withChildren>caret<#else>leaf</#if>"></span>
        <span class="datum">${node.label}</span> <#if node.id?starts_with("fr.ird.referential") || node.id?starts_with("fr.ird.data") > - <span
                class="topiaId">${node.id}</span></#if>
        <#if withChildren || withMessages>
            <ul class="nested">
                <#if withMessages>
                    <#assign messages = node.messagesByLabel />
                    <table>
                        <#list messages as fieldName, fieldMessages>
                            <tr>
                                <td style="width:25%">${fieldName}</td>
                                <td>
                                    <ul class="messages">
                                        <#list fieldMessages as message>
                                            <li class="<#if message.scope.name() == "ERROR">error<#else>warning</#if>">${message.message}</li>
                                        </#list>
                                    </ul>
                                </td>
                            </tr>
                        </#list>
                    </table>
                </#if>
                <#if withChildren>
                    <#list node.getChildren() as childNode>
                        <@displayNode childNode/>
                    </#list>
                </#if>
            </ul>
        </#if>
    </li>
</#macro>
<head>
    <meta charset="UTF-8">
    <meta lang="en">
    <meta content="text/html">
    <title>Validation report</title>
    <style>
        #result {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #result ul {
            list-style-type: none;
        }

        .caret {
            cursor: pointer;
            user-select: none;
        }

        .caret::before {
            content: "\1f5c0";
            color: #027BFF;
            display: inline-block;
            margin-right: 6px;
        }

        .caretSymbol::before {
            content: "\1f5c0";
            color: #027BFF;
            display: inline-block;
            margin-right: 6px;
        }
        .leaf {
            margin-top: 6px;
        }

        .leaf::before {
            content: "\25CB";
            color: #027BFF;
            display: inline-block;
            margin-right: 6px;
        }

        .caret-down::before {
            content: "\1f5c1";
            color: #027BFF;
        }

        #result .nested {
            display: none;
        }

        #result .active {
            display: block;
        }

        .datum {

        }

        table {
            border-collapse: collapse;
            width: 90%;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        td, th {
            border: 1px solid #027BFF;
            text-align: left;
            padding: 4px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .error:before {
            padding-right: 8px;
            content: "\26D4";
        }

        .warning:before {
            padding-right: 8px;
            content: "\26A0";
            font-style: normal;
        }

        .information::before {
            content: "\1F6C8";
            color: #027BFF;
            display: inline-block;
            margin-right: 6px;
        }

        .topiaId {
            -moz-user-select: all;
            -webkit-user-select: all;
            -ms-user-select: all;
            user-select: all;
        }

        ul.messages {
            list-style-type: none;
        }

        .action {
            cursor: pointer;
            user-select: none;
        }
    </style>
</head>
<body>
<h1>Validation report</h1>

<h2>Configuration</h2>

<ul>
    <li>Validation report generated at ${.data_model.now}</li>
    <li>Type of data to validate: <#if .data_model.modelMode.name()=="DATA">Business data<#else>Referential</#if></li>
    <li>Validation scope(s):
        <#list .data_model.scopes as scope>
            <span class="<#if scope.name() == "ERROR">error<#else>warning</#if>">${scope.label}</span>
        </#list>
    </li>
    <li>Max speed value used in activities speed validation: ${.data_model.validationSpeedMaxValue}</li>
    <li>Force validation of activities speed: ${.data_model.validationSpeedEnable?string("yes", "no")}</li>
    <li>Force validation of species length max: ${.data_model.validationLengthWeightEnable?string("yes", "no")}</li>
    <li>To bypass validation of disabled referential (display them as warnings): ${.data_model.validationUseDisabledReferential?string("yes", "no")}</li>
    <li>Dictionary of required observed systems per species on senne set: ...</li>
</ul>

<h2>Results</h2>
<h3>Notes</h3>
<div class="information">To expand a node, click on icon <span class="caretSymbol"></span> (Combination with key <b>Shift</b> will also expand his inner sub-tree).</div>
<div class="information">To collapse a note, click on icon <span class="caret-down"></span> (Combination with key <b>Shift</b> will also collapse his inner sub-tree).</div>
<div class="information">A click on a id will select it automatically.</div>
<br/>
<div class="information">To expand all, click on the following icon: <span id="expandAll" class="caretSymbol action"></span>.</div>
<div class="information">To collapse all, click on the following icon: <span id="collapseAll" class="caret-down action"></span>.</div>
<#assign rootNode = .data_model.rootNode />
<ul id="result">
    <#list rootNode.getChildren() as node>
        <@displayNode node/>
        <br/>
    </#list>
</ul>

<script>
    function expand(node) {
        node.classList.add("caret-down");
        node.parentElement.querySelector(".nested").classList.add("active");
    }

    function collapse(node) {
        // console.info("will collapse child " + node.id);
        node.classList.remove("caret-down");
        node.parentElement.querySelector(".nested").classList.remove("active");
    }

    function toggle(expanded, node) {
        if (expanded) {
            collapse(node);
        } else {
            expand(node);
        }
    }

    for (const caret of document.getElementsByClassName("caret")) {
        caret.addEventListener("click", function (event) {
            let expanded = this.classList.contains("caret-down");
            toggle(expanded, this);

            if (event.shiftKey) {
                for (const child of this.parentElement.querySelector(".nested").getElementsByClassName("caret")) {
                    toggle(expanded, child);
                }
            }
        });
    }
    let collapseAll = document.getElementById("collapseAll");
    collapseAll.addEventListener("click", function (event) {
        for (const caret of document.getElementsByClassName("caret")) {
            collapse(caret);
        }
    });
    let expandAll = document.getElementById("expandAll");
    expandAll.addEventListener("click", function (event) {
        for (const caret of document.getElementsByClassName("caret")) {
            expand(caret);
        }
    });
    <#if .data_model.modelMode.name() == "REFERENTIEL">
    expandAll.click();
    </#if>
    for (const caret of document.getElementsByClassName("leaf")) {
        let element = caret.parentElement.querySelector(".nested");
        if (element != null) {
            element.classList.toggle("active");
        }
    }
</script>
</body>
</html>
