/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions;

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.actions.config.ConfigModel;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.referential.ReferentialService;
import fr.ird.observe.services.service.referential.SynchronizeEngine;
import fr.ird.observe.services.service.referential.differential.DifferentialModelBuilder;
import fr.ird.observe.services.service.referential.differential.ObserveDifferentialMetaModel;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.util.LeftOrRightContext;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardExtStepModel;

/**
 * Le modèle de base pour les actions.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public abstract class AdminActionModel extends WizardExtStepModel<AdminStep> implements JavaBean, WithClientUIContextApi {

    private final ProgressionModel progressModel = new ProgressionModel();
    /**
     * Lazy helper class that manages all java bean operations.
     *
     * @see #javaBeanDefinition()
     */
    private transient JavaBeanDefinition javaBeanDefinition;

    protected AdminActionModel(AdminStep objectOperation) {
        super(objectOperation);
    }

    @Override
    public final JavaBeanDefinition javaBeanDefinition() {
        return javaBeanDefinition == null
                ? javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(getClass()).orElseThrow(() -> new NullPointerException("Can't find JavaBeanDefinition for " + getClass()))
                : javaBeanDefinition;
    }

    /**
     * Test if the step model is valid.
     *
     * @param uiModel global ui model
     * @return {@code true} if this model is valid (means his step is valid), {@code false} otherwise.
     */
    public abstract boolean validate(AdminUIModel uiModel);

    public abstract void start(AdminUIModel uiModel);

    /**
     * Test if the config for this step is valid.
     *
     * @param uiModel global ui model
     * @return {@code true} if the config of this step is valid (means in the config tab), {@code false} otherwise.
     */
    public boolean validateConfig(AdminUIModel uiModel) {

        ConfigModel configModel = uiModel.getConfigModel();

        if (uiModel.isNeedLeftDataSource()) {
            DataSourceSelectorModel source = configModel.getLeftSourceModel();
            if (!source.validateExt()) {
                return false;
            }
        }
        if (uiModel.isNeedRightDataSource()) {
            DataSourceSelectorModel source = configModel.getRightSourceModel();
            return source.validateExt();
        }
        return true;
    }

    public ProgressionModel getProgressModel() {
        return progressModel;
    }

    public ProgressionModel initProgressModel(int max) {
        progressModel.setMaximum(max);
        progressModel.setValue(0);
        return progressModel;
    }


    protected DifferentialModelBuilder newDifferentialModelBuilder(LeftOrRightContext<ObserveSwingDataSource> sources) {
        return ReferentialService.createDifferentialModelBuilder(getClientConfig().getReferentialLocale().getLocale(), ObserveDifferentialMetaModel::get, sources.apply(ObserveServicesProvider::getReferentialService));
    }

    protected SynchronizeEngine newReferentialSynchronizeEngine(LeftOrRightContext<ObserveSwingDataSource> sources) {
        return new SynchronizeEngine(getClientConfig().getTemporaryDirectory().toPath(), sources.apply(ObserveSwingDataSource::getSynchronizeService));
    }
}
