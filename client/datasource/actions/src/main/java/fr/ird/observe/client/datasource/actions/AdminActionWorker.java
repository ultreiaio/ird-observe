/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions;

import org.nuiton.jaxx.runtime.swing.application.ActionWorker;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.util.concurrent.Callable;

/**
 * Un worker pour les opération longues d'administration.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class AdminActionWorker extends ActionWorker<WizardState, String> {

    protected final AdminTabUIHandler<?> handler;
    private final Runnable atTheEnd;

    protected AdminActionWorker(AdminTabUIHandler<?> handler, String actionLabel, Runnable atTheEnd) {
        super(actionLabel);
        this.handler = handler;
        this.atTheEnd = atTheEnd;
    }

    public static AdminActionWorker newWorker(AdminTabUIHandler<?> handler, String actionLabel, Callable<WizardState> target,Runnable atTheEnd) {
        AdminActionWorker adminActionWorker = new AdminActionWorker(handler, actionLabel,atTheEnd);
        adminActionWorker.setTarget(target);
        return adminActionWorker;
    }

    @Override
    protected void done() {
        try {
            super.done();
        } finally {
            if (atTheEnd!=null) {
                atTheEnd.run();
            }
        }
    }

    public AdminTabUIHandler<?> getHandler() {
        return handler;
    }
}
