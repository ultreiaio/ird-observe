package fr.ird.observe.client.datasource.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.application.ActionWorker;
import org.nuiton.jaxx.runtime.swing.application.event.ActionExecutorEvent;
import org.nuiton.jaxx.runtime.swing.application.event.ActionExecutorListener;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

public class AdminActionWorkerListener implements ActionExecutorListener {

    private static final Logger log = LogManager.getLogger(AdminActionWorkerListener.class);

    @Override
    public void actionStart(ActionExecutorEvent event) {
        ActionWorker<?, ?> source = event.getWorker();
        if (source instanceof AdminActionWorker) {
            AdminActionWorker admin = (AdminActionWorker) source;
            AdminUIModel model = admin.getHandler().getModel();
            model.setBusy(true);
            model.setStepState(WizardState.RUNNING);
        }
    }

    @Override
    public void actionFail(ActionExecutorEvent event) {
        ActionWorker<?, ?> source = event.getWorker();
        Exception error = source.getError();
        if (source instanceof AdminActionWorker) {
            AdminActionWorker admin = (AdminActionWorker) source;
            AdminUIModel model = admin.getHandler().getModel();
            model.getStepModel(model.getOperation()).setError(error);
            model.setStepState(WizardState.FAILED);
            event.consume();
        }
    }

    @Override
    public void actionCancel(ActionExecutorEvent event) {
        ActionWorker<?, ?> source = event.getWorker();
        if (source instanceof AdminActionWorker) {
            AdminActionWorker admin = (AdminActionWorker) source;
            admin.getHandler().getModel().setStepState(WizardState.CANCELED);
            event.consume();
        }
    }

    @Override
    public void actionEnd(ActionExecutorEvent event) {
        ActionWorker<?, ?> source = event.getWorker();
        if (source instanceof AdminActionWorker) {
            AdminActionWorker admin = (AdminActionWorker) source;
            WizardState state;
            try {
                state = admin.get();
                admin.getHandler().getModel().setStepState(state);
            } catch (Exception e) {
                log.error("Could not retrieve data from worker " + admin, e);
            }
            event.consume();
        }
    }

    @Override
    public void actionDone(ActionExecutorEvent event) {
        ActionWorker<?, ?> source = event.getWorker();
        if (source instanceof AdminActionWorker) {
            AdminActionWorker admin = (AdminActionWorker) source;
            admin.getHandler().getModel().setBusy(false);
        }
    }
}
