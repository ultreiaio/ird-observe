/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions;

import fr.ird.observe.client.datasource.actions.config.ConfigUI;
import fr.ird.observe.client.datasource.actions.config.SelectDataModel;
import fr.ird.observe.client.datasource.actions.config.SelectDataUI;
import fr.ird.observe.client.datasource.actions.consolidate.ConsolidateModel;
import fr.ird.observe.client.datasource.actions.consolidate.ConsolidateUI;
import fr.ird.observe.client.datasource.actions.export.ExportModel;
import fr.ird.observe.client.datasource.actions.export.ExportUI;
import fr.ird.observe.client.datasource.actions.pairing.ActivityPairingModel;
import fr.ird.observe.client.datasource.actions.pairing.ActivityPairingUI;
import fr.ird.observe.client.datasource.actions.report.ReportModel;
import fr.ird.observe.client.datasource.actions.report.ReportUI;
import fr.ird.observe.client.datasource.actions.save.SaveLocalModel;
import fr.ird.observe.client.datasource.actions.save.SaveLocalUI;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroUI;
import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.SynchronizeModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.SynchronizeUI;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchroUI;
import fr.ird.observe.client.datasource.actions.validate.ValidateModel;
import fr.ird.observe.client.datasource.actions.validate.ValidateUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardExtStep;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardExtUI;

import javax.swing.ImageIcon;
import java.util.EnumSet;
import java.util.function.Function;
import java.util.function.Supplier;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Pour caractériser les étapes (correspond aux onglets de l'ui).
 * <p>
 * Chaque constante représente un onglet de l'ui et est responsable de
 * l'instanciation des différents objets associés à un onglet, à savoir :
 * <ul>
 * <li>le model {@link #newModel()}</li>
 * <li>l'ui {@link #newUI(WizardExtUI)} </li>
 * </ul>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION, pattern = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION_PATTERN)
})
public enum AdminStep implements WizardExtStep {

    CONFIG(
            null,
            null,
            ConfigUI.class,
            ConfigUI::new),
    SELECT_DATA(
            n("observe.constant.AdminStep.SELECT_DATA"),
            n("observe.constant.AdminStep.SELECT_DATA.description"),
            "synchronizeReferentiel",
            n("observe.constant.AdminStep.SELECT_DATA"),
            n("observe.constant.AdminStep.SELECT_DATA.description"),
            SelectDataModel.class,
            SelectDataModel::new,
            SelectDataUI.class,
            SelectDataUI::new,
            'S'),
    SYNCHRONIZE(
            n("observe.ui.datasource.editor.actions.synchro.referential.legacy"),
            n("observe.ui.datasource.editor.actions.synchro.referential.legacy.tip"),
            "synchronizeReferentiel",
            n("observe.ui.datasource.editor.actions.synchro.referential.legacy"),
            n("observe.ui.datasource.editor.actions.synchro.referential.legacy.description"),
            SynchronizeModel.class,
            SynchronizeModel::new,
            SynchronizeUI.class,
            SynchronizeUI::new,
            'M',
            DataSourceConnectMode.LOCAL,
            DataSourceConnectMode.REMOTE,
            DataSourceConnectMode.SERVER),
    DATA_SYNCHRONIZE(
            n("observe.ui.datasource.editor.actions.synchro.data.title"),
            n("observe.ui.datasource.editor.actions.synchro.data.title.tip"),
            "dataSynchronize",
            n("observe.ui.datasource.editor.actions.synchro.data"),
            n("observe.ui.datasource.editor.actions.synchro.data.description"),
            DataSynchroModel.class,
            DataSynchroModel::new,
            DataSynchroUI.class,
            DataSynchroUI::new,
            'G',
            DataSourceConnectMode.LOCAL,
            DataSourceConnectMode.REMOTE,
            DataSourceConnectMode.SERVER),
    REFERENTIAL_SYNCHRONIZE(
            n("observe.ui.datasource.editor.actions.synchro.referential.title"),
            n("observe.ui.datasource.editor.actions.synchro.referential.title.tip"),
            "referentialSynchronize",
            n("observe.ui.datasource.editor.actions.synchro.referential"),
            n("observe.ui.datasource.editor.actions.synchro.referential.description"),
            ReferentialSynchroModel.class,
            ReferentialSynchroModel::new,
            ReferentialSynchroUI.class,
            ReferentialSynchroUI::new,
            'R',
            DataSourceConnectMode.LOCAL,
            DataSourceConnectMode.REMOTE,
            DataSourceConnectMode.SERVER),
    VALIDATE(
            n("observe.ui.datasource.editor.actions.validate.title"),
            n("observe.ui.datasource.editor.actions.validate.title.tip"),
            "validate",
            n("observe.ui.datasource.editor.actions.validate"),
            n("observe.ui.datasource.editor.actions.validate.description"),
            ValidateModel.class,
            ValidateModel::new,
            ValidateUI.class,
            ValidateUI::new,
            'V',
            DataSourceConnectMode.LOCAL,
            DataSourceConnectMode.REMOTE,
            DataSourceConnectMode.SERVER),
    CONSOLIDATE(
            n("observe.ui.datasource.editor.actions.consolidate.title"),
            n("observe.ui.datasource.editor.actions.consolidate.title.tip"),
            "consolidate",
            n("observe.ui.datasource.editor.actions.consolidate"),
            n("observe.ui.datasource.editor.actions.consolidate.description"),
            ConsolidateModel.class,
            ConsolidateModel::new,
            ConsolidateUI.class,
            ConsolidateUI::new,
            'C',
            DataSourceConnectMode.LOCAL,
            DataSourceConnectMode.REMOTE,
            DataSourceConnectMode.SERVER),
    ACTIVITY_PAIRING(
            n("observe.ui.datasource.editor.actions.pairing.title"),
            n("observe.ui.datasource.editor.actions.pairing.tip"),
            "consolidate",
            n("observe.ui.datasource.editor.actions.activity.pairing"),
            n("observe.ui.datasource.editor.actions.pairing.description"),
            ActivityPairingModel.class,
            ActivityPairingModel::new,
            ActivityPairingUI.class,
            ActivityPairingUI::new,
            'P',
            DataSourceConnectMode.LOCAL,
            DataSourceConnectMode.REMOTE,
            DataSourceConnectMode.SERVER),
    REPORT(
            n("observe.ui.datasource.editor.actions.report.title"),
            n("observe.ui.datasource.editor.actions.report.title.tip"),
            "report",
            n("observe.ui.datasource.editor.actions.report"),
            n("observe.ui.datasource.editor.actions.report.description"),
            ReportModel.class,
            ReportModel::new,
            ReportUI.class,
            ReportUI::new,
            'S',
            DataSourceConnectMode.LOCAL,
            DataSourceConnectMode.REMOTE,
            DataSourceConnectMode.SERVER),

    SAVE_LOCAL(
            null,
            null,
            "save",
            n("observe.ui.datasource.editor.actions.saveLocal"),
            n("observe.ui.datasource.editor.actions.saveLocal.description"),
            SaveLocalModel.class,
            SaveLocalModel::new,
            SaveLocalUI.class,
            SaveLocalUI::new,
            null),
    EXPORT_DATA(
            n("observe.ui.datasource.editor.actions.exportData.title"),
            n("observe.ui.datasource.editor.actions.exportData.title.tip"),
            "remote-export",
            n("observe.ui.datasource.editor.actions.exportData"),
            n("observe.ui.datasource.editor.actions.exportData.description"),
            ExportModel.class,
            ExportModel::new,
            ExportUI.class,
            ExportUI::new,
            'E',
            DataSourceConnectMode.LOCAL,
            DataSourceConnectMode.REMOTE,
            DataSourceConnectMode.SERVER);

    private static final EnumSet<AdminStep> REQUIRE_RIGHT_DATASOURCE = EnumSet.of(
            SYNCHRONIZE, DATA_SYNCHRONIZE, REFERENTIAL_SYNCHRONIZE, EXPORT_DATA
    );

    private static final EnumSet<AdminStep> REQUIRE_SAVE = EnumSet.of(
            SYNCHRONIZE
    );
    private static final EnumSet<AdminStep> REQUIRE_SELECT = EnumSet.of(
            VALIDATE, CONSOLIDATE, ACTIVITY_PAIRING, REPORT, EXPORT_DATA
    );
    private final Class<? extends AdminActionModel> modelClass;
    private final Supplier<AdminActionModel> modelFactory;
    private final Class<? extends AdminTabUI> uiClass;
    private final Function<AdminUI, AdminTabUI> uiFactory;
    private final String iconName;
    private final String title;
    private final String titleTip;
    private final String operationLabel;
    private final Character mnemonic;
    private final String operationDescription;
    private final DataSourceConnectMode[] dataSourceConnectModes;
    private transient ImageIcon icon;

    public static EnumSet<AdminStep> getOperations() {
        EnumSet<AdminStep> operations = EnumSet.noneOf(AdminStep.class);
        for (AdminStep step : AdminStep.values()) {
            if (step.isOperation()) {
                operations.add(step);
            }
        }
        return operations;
    }

    public static AdminStep valueOfIgnoreCase(String value) {
        for (AdminStep step : AdminStep.values()) {
            if (step.name().equalsIgnoreCase(value)) {
                return step;
            }
        }
        return null;
    }

    AdminStep(Class<? extends AdminActionModel> modelClass,
              Supplier<AdminActionModel> modelFactory,
              Class<? extends AdminTabUI> uiClass,
              Function<AdminUI, AdminTabUI> uiFactory) {
        this(null,
             null,
             null,
             null,
             null,
             modelClass,
             modelFactory,
             uiClass,
             uiFactory,
             null
        );
    }

    AdminStep(String title,
              String titleTip,
              String iconName,
              String operationLabel,
              String operationDescription,
              Class<? extends AdminActionModel> modelClass,
              Supplier<AdminActionModel> modelFactory,
              Class<? extends AdminTabUI> uiClass,
              Function<AdminUI, AdminTabUI> uiFactory,
              Character mnemonic,
              DataSourceConnectMode... dataSourceConnectModes) {
        this.title = title;
        this.titleTip = titleTip;
        this.iconName = iconName;
        this.mnemonic = mnemonic;
        this.operationDescription = operationDescription;
        this.operationLabel = operationLabel;
        this.modelClass = modelClass;
        this.modelFactory = modelFactory;
        this.uiClass = uiClass;
        this.uiFactory = uiFactory;
        this.dataSourceConnectModes = dataSourceConnectModes;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleTip() {
        return titleTip;
    }

    public String getIconName() {
        return iconName;
    }

    public ImageIcon getIcon() {
        if (iconName != null && icon == null) {
            icon = (ImageIcon) UIHelper.getUIManagerActionIcon(iconName);
        }
        return icon;
    }

    @Override
    public String getLabel() {
        return AdminStepI18n.getLabel(this);
    }

    @Override
    public String getDescription() {
        return AdminStepI18n.getDescription(this);
    }

    @Override
    public String getOperationDescription() {
        return operationDescription;
    }

    @Override
    public String getOperationLabel() {
        return operationLabel;
    }

    @Override
    public boolean isOperation() {
        return operationLabel != null;
    }

    @Override
    public boolean isConfig() {
        return this == CONFIG;
    }

    public boolean isNeedSelect() {
        return REQUIRE_SELECT.contains(this);
    }

    @Override
    public Class<? extends AdminActionModel> getModelClass() {
        return modelClass;
    }

    @Override
    public AdminActionModel newModel() {
        if (!isOperation()) {
            throw new IllegalStateException("The current step [" + this + "] is not an operation.");
        }
        try {
            return modelFactory.get();
        } catch (Exception e) {
            throw new IllegalStateException("Could not init model : " + getModelClass(), e);
        }
    }

    @Override
    public Class<? extends AdminTabUI> getUiClass() {
        return uiClass;
    }

    @Override
    public AdminTabUI newUI(WizardExtUI<?, ?> ui) {
        try {
            return uiFactory.apply((AdminUI) ui);
        } catch (Exception e) {
            throw new IllegalStateException("Could not init ui " + this, e);
        }
    }

    /**
     * Is this step is the new step and come from a previous step (in ordinal order) ?
     *
     * @param oldStep the step before
     * @return {@code true} if new step is this step and old step has a smaller ordinal, {@code false} otherwise
     */
    public boolean forwardToThisStep(AdminStep oldStep) {
        return oldStep != null && oldStep.ordinal() < ordinal();
    }

    public boolean isNeedReferentiel() {
        return REQUIRE_RIGHT_DATASOURCE.contains(this);
    }

    public boolean isNeedSave() {
        return REQUIRE_SAVE.contains(this);
    }

    /**
     * Is this step is the new step and come from a greater step (in ordinal order) ?
     *
     * @param oldStep the step before
     * @return {@code true} if new step is this step and old step has a greater ordinal, {@code false} otherwise
     */
    public boolean rewindToThisStep(AdminStep oldStep) {
        return oldStep != null && oldStep.ordinal() > ordinal();
    }

    public DataSourceConnectMode[] getDataSourceConnectModes() {
        return dataSourceConnectModes;
    }

    public Character getMnemonic() {
        return mnemonic;
    }

    public boolean hasIncomingDataSourceConnectModes() {
        return dataSourceConnectModes.length > 0;
    }
}
