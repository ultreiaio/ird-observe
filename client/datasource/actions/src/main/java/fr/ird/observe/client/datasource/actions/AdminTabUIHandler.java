/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions;

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.actions.config.ConfigUI;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class AdminTabUIHandler<U extends AdminTabUI> implements WithClientUIContextApi {

    public static final String ADMIN_TAB_UI = "synchroParent";

    protected U ui;
    protected AdminUI parentUI;
    private JButton autoStart;
    private JButton autoNext;

    protected void onComingFromPreviousStep(boolean pending) {
        if (pending && autoStart != null) {
            SwingUtilities.invokeLater(autoStart::doClick);
        }
    }

    protected void onComingFromNextStep(AdminStep oldStep) {
        // by default nothing to do
    }

    public void beforeInit(U ui) {
        this.ui = ui;
        this.parentUI = ui.getContextValue(AdminUI.class, "parent");
        ui.setContextValue(ui, ADMIN_TAB_UI);
    }

    public void afterInit(U ui) {
        AdminUIModel model = ui.getModel();
        AdminStep step = ui.getStep();
        model.addPropertyChangeListener(AdminUIModel.STEP_PROPERTY_NAME, e -> ui.getProgress().setString(getProgressString(model.getStepIndex(model.getStep()), model.getSteps().size())));
        model.addPropertyChangeListener(AdminUIModel.STEPS_PROPERTY_NAME, e -> ui.getProgress().setString(getProgressString(model.getStepIndex(model.getStep()), model.getSteps().size())));

        String stepLabel = step.getLabel();
        ui.getRUNNING_label().setText(t("observe.ui.datasource.editor.actions.operation.message.running", stepLabel));
        ui.SUCCESSED_label.setText(t("observe.ui.datasource.editor.actions.operation.message.success", stepLabel));
        ui.NEED_FIX_label.setText(t("observe.ui.datasource.editor.actions.operation.message.needFix", stepLabel));
        ui.CANCELED_label.setText(t("observe.ui.datasource.editor.actions.operation.message.canceled", stepLabel));
        ui.FAILED_label.setText(t("observe.ui.datasource.editor.actions.operation.message.failed", stepLabel));
        ui.progression.setVisible(true);
        ui.progressionTop.setVisible(true);
        ConfigUI configUI = ConfigUI.get(parentUI);
        initConfig(configUI);
    }

    protected final void setAutoStart(JButton autoStart) {
        this.autoStart = Objects.requireNonNull(autoStart);
    }

    protected final void setAutoNext(JButton autoNext) {
        this.autoNext = Objects.requireNonNull(autoNext);
    }

    protected void initConfig(ConfigUI configUI) {
    }

    public U getUi() {
        return ui;
    }

    public AdminUIModel getModel() {
        return ui.getModel();
    }

    public final void updateState(WizardState newState) {
        if (newState == null) {
            newState = WizardState.PENDING;
        }
        ui.getContentLayout().show(ui.getContent(), newState.name());
        switch (newState) {
            case PENDING:
                onStateChangeToPending();
                break;
            case RUNNING:
                onStateChangeToRunning();
                break;
            case SUCCESSED:
                onStateChangeToSuccess();
                break;
            case FAILED:
                onStateChangeToFail();
                break;
            case NEED_FIX:
                onStateChangeToNeedFix();
                break;
            case CANCELED:
                onStateChangeToCancel();
                break;
        }
    }

    protected void onStateChangeToPending() {
        ui.description.setText(t(ui.getStep().getDescription()));
    }

    protected void onStateChangeToRunning() {
        ui.RUNNING_progressionPane.getViewport().setView(ui.progression);
        ui.RUNNING_progressionPane.setColumnHeaderView(ui.progressionTop);
    }

    protected void onStateChangeToSuccess() {
        ui.SUCCESSED_progressionPane.getViewport().setView(ui.progression);
        ui.SUCCESSED_progressionPane.setColumnHeaderView(ui.progressionTop);
        if (autoNext != null) {
            SwingUtilities.invokeLater(autoNext::doClick);
        }
    }

    protected void onStateChangeToFail() {
        Exception e = ui.getStepModel().getError();
        StringWriter w = new StringWriter();
        e.printStackTrace(new PrintWriter(w));
        addMessage(w.toString());
        ui.FAILED_progressionPane.getViewport().setView(ui.progression);
        ui.FAILED_progressionPane.setColumnHeaderView(ui.progressionTop);
    }

    protected void onStateChangeToNeedFix() {
    }

    protected void onStateChangeToCancel() {
        ui.CANCELED_progressionPane.getViewport().setView(ui.progression);
        ui.CANCELED_progressionPane.setColumnHeaderView(ui.progressionTop);
    }

    public String getProgressString(int currentStep, int nbStep) {
        String txt = "";
        AdminStep step = ui.getStep();
        if (step != null) {
            txt = n("observe.ui.datasource.storage.step.label");
            txt = t(txt, currentStep + 1, nbStep, step.getLabel());
        }
        return txt;
    }

    public void addMessage(String text) {
        JTextArea progression = ui.getProgression();
        progression.append(text + "\n");
        progression.setCaretPosition(progression.getDocument().getLength());
    }

    protected void hideFixedPanelLabel(AdminTabUI ui) {
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2767
        // FIXME Maybe we should use a simple JPanel instead?
        ui.getNEED_FIX_panel().removeAll();
        ui.getNEED_FIX_panel().add(ui.getNEED_FIX_content(), new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, 10, 1, new Insets(3, 3, 3, 3), 0, 0));
    }

    public void destroy() {

    }

    protected void onComingFromNextStepAdjust() {
        AdminUIModel model = ui.getModel();
        if (model.containsOperation(AdminStep.VALIDATE) || model.containsOperation(AdminStep.ACTIVITY_PAIRING) ||
                (model.containsOperation(AdminStep.CONSOLIDATE) && !model.containsOperation(AdminStep.EXPORT_DATA))) {
            if (model.getStepState(AdminStep.CONSOLIDATE) == WizardState.SUCCESSED) {
                // set back to pending consolidate step
                model.setStepState(AdminStep.CONSOLIDATE, WizardState.PENDING);
            } else if (model.getStepState(AdminStep.VALIDATE) == WizardState.NEED_FIX || model.getStepState(AdminStep.VALIDATE) == WizardState.SUCCESSED) {
                // set back to pending validate step
                model.setStepState(AdminStep.VALIDATE, WizardState.PENDING);
                // reset report model
                model.getValidateModel().reset();
            } else if (model.getStepState(AdminStep.ACTIVITY_PAIRING) == WizardState.NEED_FIX || model.getStepState(AdminStep.ACTIVITY_PAIRING) == WizardState.SUCCESSED) {
                // set back to pending activity pairing step
                model.setStepState(AdminStep.ACTIVITY_PAIRING, WizardState.PENDING);
            }
        }
    }
}
