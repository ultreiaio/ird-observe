package fr.ird.observe.client.datasource.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.actions.actions.LaunchAdminAction;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditorBodyContent;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.body.HideBodyContentNotAcceptedException;
import fr.ird.observe.client.main.body.MainUIBodyContent;
import fr.ird.observe.client.main.body.NoBodyContentComponent;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.dto.ObserveUtil;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.runtime.context.JAXXContextEntryDef;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
@SuppressWarnings("rawtypes")
@AutoService(MainUIBodyContent.class)
public class AdminUIBodyContent extends MainUIBodyContent<AdminUI> implements WithClientUIContextApi {

    /**
     * the jaxx context entry to store the apply action
     */
    public static final JAXXContextEntryDef<Runnable> APPLY_DEF = JAXXUtil.newContextEntryDef("apply", Runnable.class);
    /**
     * the jaxx context entry to store the cancel action
     */
    public static final JAXXContextEntryDef<Runnable> CANCEL_DEF = JAXXUtil.newContextEntryDef("cancel", Runnable.class);
    private static final Logger log = LogManager.getLogger(AdminUIBodyContent.class);

    public AdminUIBodyContent() {
        super(3, AdminUI.class);
        setSupplier(SingletonSupplier.of(createSupplier()));
    }

    @Override
    public void install(ObserveMainUI mainUI) {
        super.install(mainUI);
        JMenu menu = new JMenu(I18n.t("observe.ui.menu.actions"));
        menu.setMnemonic('A');
        addMenuAction(mainUI, menu, AdminStep.SYNCHRONIZE);
        addMenuAction(mainUI, menu, AdminStep.REFERENTIAL_SYNCHRONIZE);
        menu.addSeparator();
        addMenuAction(mainUI, menu, AdminStep.EXPORT_DATA);
        addMenuAction(mainUI, menu, AdminStep.DATA_SYNCHRONIZE);
        menu.addSeparator();
        addMenuAction(mainUI, menu, AdminStep.CONSOLIDATE);
        addMenuAction(mainUI, menu, AdminStep.ACTIVITY_PAIRING);
        addMenuAction(mainUI, menu, AdminStep.VALIDATE);
        addMenuAction(mainUI, menu, AdminStep.REPORT);
        JMenuBar menuBar = mainUI.getMenu();
        menuBar.add(menu, 2);
        mainUI.getModel().addPropertyChangeListener("actionsEnabled", evt -> menu.setEnabled((Boolean) evt.getNewValue()));
    }

    protected void addMenuAction(ObserveMainUI mainUI, JMenu menu, AdminStep step) {
        LaunchAdminAction action = new LaunchAdminAction(step);
        JMenuItem menuItem = new JMenuItem();
        LaunchAdminAction.init(mainUI, menuItem, action);
        menu.add(menuItem);
    }

    @Override
    public void show(ObserveMainUI mainUI) {
        super.show(mainUI);

        AdminUI ui = get();

        AdminUIModel adminUIModel = ui.getModel();
        mainUI.setContextValue(adminUIModel);

        adminUIModel.addOperation(adminUIModel.getAdminStep());
        adminUIModel.updateStepUniverse();

        ui.start();
        // refresh selected tab (otherwise it does NOT display (white screen...)
        SwingUtilities.invokeLater(() -> {
            ui.getTabs().getSelectedComponent().validate();
            ui.getTabs().getSelectedComponent().requestFocusInWindow();
        });
    }

    @Override
    public void hide(ObserveMainUI mainUI) throws HideBodyContentNotAcceptedException {

        AdminUI ui = get();
        AdminUIModel model = ui.getModel();
        model.destroy();
        ui.destroy();
        ui.dispose();

        //FIXME:BodyContent find a way to start mainUIBodyContent in a simplified ui JDialog
//        if (!(parent instanceof ObserveMainUI)) {
//            JDialog dialog = (JDialog) parent;
//            log.info("dispose ui!");
//            dialog.setVisible(false);
//            dialog.dispose();
//            log.info("After dispose.");
//            ApplicationContext.get().releaseLock();
//            return;
//        }

        mainUI.removeContextValue(model.getClass());

        ObserveUtil.cleanMemory();

        super.hide(mainUI);
    }

    private Supplier<AdminUI> createSupplier() {
        return () -> {
            ObserveMainUI mainUI = getMainUI();
            AdminStep adminStep = mainUI.getContextValue(AdminStep.class);
            AdminUIModel model = new AdminUIModel(Objects.requireNonNull(adminStep));
            JAXXInitialContext uiContext = UIHelper.initialContext(mainUI, model);
            //FIXME:BodyContent Review jaxx to remove this...
            // apply action
            uiContext.add(APPLY_DEF, () -> doClose(false));
            // cancel action
            uiContext.add(CANCEL_DEF, () -> doClose(true));
            return new AdminUI(uiContext);
        };
    }

    private void doClose(boolean wasCanceled) {
        log.info(String.format("%s, was canceled ? %s", this, wasCanceled));
        AdminUI ui = get();
        AdminUIModel model = ui.getModel();
        ObserveSwingDataSource.doCloseSource(model.getConfigModel().getRightSourceModel().getSource());
        ObserveSwingDataSource.doCloseSource(model.getConfigModel().getLeftSourceModel().getSource());
        ObserveMainUI mainUI = getMainUI();
        ObserveDataSourceConfiguration sourceConfiguration = model.getConfigModel().getPreviousSourceConfiguration();
        if (sourceConfiguration == null) {
            // no previous data source
            mainUI.changeBodyContent(NoBodyContentComponent.class);
            return;
        }
        // reopen data source
        ObserveSwingDataSource source = getDataSourcesManager().newDataSource(sourceConfiguration);
        // attach it to ui
        DataSourceEditorBodyContent dataSourceEditorBodyContent = mainUI.getMainUIBodyContentManager().getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class);
        // load it
        try {
            dataSourceEditorBodyContent.loadStorage(mainUI, source);
        } catch (Exception e) {
            throw new ObserveSwingTechnicalException(e);
        }
    }
}
