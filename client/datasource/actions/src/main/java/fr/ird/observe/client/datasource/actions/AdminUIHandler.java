/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions;

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.util.UIHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.wizard.BusyChangeListener;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardExtUtil;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.Date;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class AdminUIHandler implements UIHandler<AdminUI>, WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(AdminUIHandler.class);

    private AdminUI ui;
    private Component parent;

    @Override
    public void beforeInit(AdminUI ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(AdminUI ui) {

        // installation du dispatcher de modifications du modele
        WizardExtUtil.installWizardUIListeners(ui);
        WizardExtUtil.addTraceLogListener(log, ui.getModel());

        AdminUIModel model = ui.getModel();
        parent = getParent(ui);
        BusyChangeListener listener = new BusyChangeListener(parent) {

            @Override
            protected void setBusy(Component ui) {
                log.debug("Busy for component " + ui);
                if (parent instanceof ObserveMainUI) {
                    ((ObserveMainUI) parent).getBusyModel().addTask("Busy from admin task");
                    super.setBusy(parent);
                }
                super.setBusy(ui);
            }

            @Override
            protected void setUnBusy(Component ui) {
                log.debug("UnBusy for component " + ui);
                if (parent instanceof ObserveMainUI) {
                    ((ObserveMainUI) parent).getBusyModel().popTask();
                    super.setUnBusy(parent);
                }
                super.setUnBusy(ui);
            }
        };
        UIHelper.setLayerUI(ui.getTabs(), ui.getTabBusyBlockLayerUI());
        listener.setBlockingUI(ui.getTabBusyBlockLayerUI());
        model.addPropertyChangeListener(AdminUIModel.BUSY_PROPERTY_NAME, listener);

        ui.setTitle(t(model.getAdminStep().getTitle()));
        ui.setToolTipText(t(model.getAdminStep().getTitleTip()));
    }

    public AdminStep getSelectedStep() {
        int index = ui.getTabs().getSelectedIndex();
        AdminTabUI c = null;
        if (index > -1) {
            c = (AdminTabUI) ui.getTabs().getComponentAt(index);
        }
        return c == null ? null : c.getStep();
    }

    public AdminTabUI getStepUI(AdminStep step) {
        if (step != null) {
            return (AdminTabUI) ui.getObjectById(step.name());
        }
        return null;
    }

    public AdminTabUI getStepUI(int stepIndex) {
        if (stepIndex > ui.getTabs().getTabCount()) {
            return null;
        }
        return (AdminTabUI) ui.getTabs().getComponentAt(stepIndex);
    }

    public AdminTabUI getSelectedStepUI() {
        AdminStep step = getSelectedStep();
        return getStepUI(step);
    }

    public void onWasInit() {
        log.info(String.format("%s model was init at %s", ui.getName(), new Date()));
    }

    public void onWasStarted() {
        log.info(String.format("%s model was started at %s", ui.getName(), new Date()));
    }

    public void onStepsChanged(AdminStep[] steps) {

        log.debug("Will use these steps : " + Arrays.toString(steps));

        ui.getModel().setValueAdjusting(true);


        // on supprime tous les onglets
        while (ui.tabs.getTabCount() > 1) {

            int index = ui.tabs.getTabCount() - 1;
            log.debug("remove step : " + index);
            ui.tabs.remove(index);
        }

        // on recree les onglets
        for (AdminStep step : steps) {
            AdminTabUI c = ui.getStepUI(step);
            log.debug(String.format("StepUI [%s] : %s", step, c));
            if (c == null) {

                log.debug(String.format("Will init step [%s]", step));

                c = step.newUI(ui);

                log.debug(String.format("Created tabUI [%s]", c.getName()));

                ui.get$objectMap().put(step.name(), c);

                log.debug(String.format("Will init tabUI [%s]", c.getName()));

                JComponent content = c.getContent();
                UIHelper.setLayerUI(content, ui.getBusyBlockLayerUI());
            }

            String title = step.getLabel();
            String tip = step.getDescription();
            ui.tabs.addTab(title, null, c, tip);
        }

        ui.getModel().setValueAdjusting(false);
    }

    public void onStepChanged(AdminStep oldStep, AdminStep newStep) {
        log.trace(newStep);
        AdminTabUI c = ui.getStepUI(newStep);

        AdminUIModel model = ui.getModel();

        if (!model.isWasDone()) {
            if (newStep.forwardToThisStep(oldStep)) {
                c.onComingFromPreviousStep(model.getStepState(newStep) == WizardState.PENDING);
            } else if (newStep.rewindToThisStep(oldStep)) {
                c.onComingFromNextStep(oldStep);
            }
        }
        // select new tab
        //FIXME Is this necessary?
        int index = ui.tabs.indexOfComponent(c);
        if (index > -1 && ui.tabs.getSelectedIndex() != index) {
            ui.tabs.setSelectedIndex(index);
        }
        SwingUtilities.invokeLater(() -> ui.getTabs().getSelectedComponent().requestFocusInWindow());

    }

    /**
     * Call back lorsque l'état du modèle a changé.
     * <p>
     * Ici, on va rendre accessible (ou pas) les onglets selon l'état du modèle.
     *
     * @param newState le nouvel état.
     */
    public void onModelStateChanged(WizardState newState) {
        log.debug(newState);

        if (ui.getModel().isWasDone()) {
            return;
        }

        AdminTabUI selected = ui.getSelectedStepUI();
        boolean busy = WizardExtUtil.acceptStates(newState, WizardState.RUNNING);
        UIHelper.TabbedPaneIterator<Component> itr = UIHelper.newTabbedPaneIterator(ui.getTabs());
        if (busy) {
            log.debug("Busy state, can only access selected step : " + selected);
            // seul l'onglet courant est accessible
            while (itr.hasNext()) {
                Component tab = itr.next();
                ui.tabs.setEnabledAt(itr.getIndex() - 1, tab.equals(selected));
            }
            return;
        }

        if (selected != null && (selected.getStep().isConfig() || selected.getStep() == AdminStep.SELECT_DATA)) {

            log.debug("Selected step is config : " + selected.getStep());
            while (itr.hasNext()) {
                int index = itr.getIndex();
                Component tab = itr.next();
                AdminTabUI tabUI = (AdminTabUI) tab;
                AdminStep synchroStep = tabUI.getStep();
                if (synchroStep.isConfig() || (synchroStep == AdminStep.SELECT_DATA && ui.getModel().getStepState(synchroStep) == WizardState.SUCCESSED)) {
                    log.debug("Test config panel : " + synchroStep);
                    boolean valid = ui.getModel().validate(synchroStep);
                    if (valid) {
                        log.debug("step " + synchroStep + " is valid");
                        ui.tabs.setIconAt(index, getTabIcon(WizardState.SUCCESSED));
                    } else {
                        log.debug("step " + synchroStep + " is not valid!");
                        ui.tabs.setIconAt(index, getTabIcon(WizardState.FAILED));
                    }
                }
            }
            itr = UIHelper.newTabbedPaneIterator(ui.getTabs());
        }

        boolean[] accessibleSteps = ui.getModel().getAccessibleSteps();
        log.debug("Accessible step : " + Arrays.toString(accessibleSteps));
        for (; itr.hasNext(); itr.next()) {
            int index = itr.getIndex();
            ui.tabs.setEnabledAt(index, accessibleSteps[index]);
        }
    }

    public void onOperationStateChanged(AdminStep step, WizardState newState) {
        log.debug(step + " - " + newState);

        // mettre a jour l'onglet
        AdminTabUI selected = ui.getStepUI(step);
        int index = ui.tabs.indexOfComponent(selected);
        if (index > -1 && !selected.getStep().isConfig()) {
            ui.tabs.setIconAt(index, getTabIcon(newState));
        }
        if (selected != null && step == selected.getStep()) {
            selected.getHandler().updateState(newState);
        }
    }

    public void start() {

        try {
            // on démarre le modèle
            ui.getModel().startModel(ui);
            ui.onWasInit();

            // affichage ui
            ui.setVisible(true);

        } finally {

            ui.getModel().setBusy(false);
        }
    }

    public void destroy() {
        ui.getModel().destroy();
        log.debug("destroy ui " + ui.getName());
        UIHelper.TabbedPaneIterator<Component> itr = UIHelper.newTabbedPaneIterator(ui.getTabs());
        while (itr.hasNext()) {
            AdminTabUI tab = (AdminTabUI) itr.next();
            log.debug("destroy ui " + tab.getName());
            tab.destroy();
        }
        UIHelper.destroy(ui);
    }

    public void dispose() {
        destroy();
    }

    public Icon getTabIcon(WizardState state) {
        Icon i = null;
        if (state != null) {
            String key = "wizard-state-" + state.name().toLowerCase();
            i = UIHelper.getUIManagerActionIcon(key);
        }
        return i;
    }

    protected Component getParent(AdminUI ui) {
        ObserveMainUI mainUI = getMainUI();
        ImageIcon icon = ui.getModel().getAdminStep().getIcon();
        if (mainUI == null) {
            // pas de fenêtre détectée
            // on encapsule l'interface dans un dialog

            JDialog dialog = new JDialog();
            dialog.setSize(1024, 780);

            if (icon != null) {
                dialog.setIconImage(icon.getImage());
            }
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setAlwaysOnTop(true);
            dialog.setModal(true);
            dialog.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosed(WindowEvent e) {
                    log.info("closing dialog " + e.getWindow().getName());
                    if (ui.getModel().getModelState() != WizardState.CANCELED) {
                        log.info("cancel panel from dialog !" + e.getWindow().getName());
                        ui.getCancelAction().doClick();
                    }
                }
            });
            return dialog;
        } else {
            return mainUI;
        }
    }

}
