/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions;

import fr.ird.observe.client.datasource.actions.config.ConfigModel;
import fr.ird.observe.client.datasource.actions.config.ConfigUI;
import fr.ird.observe.client.datasource.actions.config.SelectDataModel;
import fr.ird.observe.client.datasource.actions.consolidate.ConsolidateModel;
import fr.ird.observe.client.datasource.actions.export.ExportModel;
import fr.ird.observe.client.datasource.actions.pairing.ActivityPairingModel;
import fr.ird.observe.client.datasource.actions.report.ReportModel;
import fr.ird.observe.client.datasource.actions.save.SaveLocalModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.SynchronizeModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchroModel;
import fr.ird.observe.client.datasource.actions.validate.ValidateModel;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelector;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardExtModel;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Le modele de l'ui pour effectuer des opérations de synchro et validation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class AdminUIModel extends WizardExtModel<AdminStep> {

    private static final Logger log = LogManager.getLogger(AdminUIModel.class);
    public static final String PROPERTY_WAS_DONE = "wasDone";

    /**
     * Config model.
     */
    private final ConfigModel configModel = new ConfigModel();

    /**
     * Select data model.
     */
    private final SelectDataModel selectDataModel = new SelectDataModel();
    /**
     * Main admin step (this should be an operation).
     */
    private final AdminStep adminStep;
    /**
     * Available incoming db mode for any data source configured here.
     */
    private final EnumSet<DataSourceConnectMode> availableIncomingModes;
    /**
     * Flag to know if operations are done.
     * <p>
     * With this flag, we will be able to not disable tab and restart a new action.
     */
    protected boolean wasDone;
    /**
     * Flag to know when model is init.
     * <p>
     * Init is done at the end of method {@link #startModel(AdminUI)}.
     */
    private boolean init;

    public AdminUIModel(AdminStep adminStep) {
        super(AdminStep.class);
        this.adminStep = Objects.requireNonNull(adminStep);
        this.availableIncomingModes = EnumSet.noneOf(DataSourceConnectMode.class);
        log.debug(String.format("model [%s] is instantiated.", this));
    }

    public AdminStep getAdminStep() {
        return adminStep;
    }

    @Override
    public AdminActionModel getStepModel(AdminStep operation) {
        return (AdminActionModel) super.getStepModel(operation);
    }

    @Override
    public void firePropertyChange(String propertyName, Object newValue) {
        super.firePropertyChange(propertyName, newValue);
    }

    public boolean isInit() {
        return init;
    }

    public void setInit(boolean init) {
        this.init = init;
    }

    public ConfigModel getConfigModel() {
        return configModel;
    }

    public SelectDataModel getSelectDataModel() {
        return selectDataModel;
    }

    public ValidateModel getValidateModel() {
        return (ValidateModel) getStepModel(AdminStep.VALIDATE);
    }

    public SynchronizeModel getSynchronizeReferentielModel() {
        return (SynchronizeModel) getStepModel(AdminStep.SYNCHRONIZE);
    }

    public ExportModel getExportModel() {
        return (ExportModel) getStepModel(AdminStep.EXPORT_DATA);
    }

    public ReportModel getReportModel() {
        return (ReportModel) getStepModel(AdminStep.REPORT);
    }

    public ConsolidateModel getConsolidateModel() {
        return (ConsolidateModel) getStepModel(AdminStep.CONSOLIDATE);
    }

    public ActivityPairingModel getActivityPairingModel() {
        return (ActivityPairingModel) getStepModel(AdminStep.ACTIVITY_PAIRING);
    }

    public SaveLocalModel getSaveLocalModel() {
        return (SaveLocalModel) getStepModel(AdminStep.SAVE_LOCAL);
    }

    public DataSynchroModel getDataSynchroModel() {
        return (DataSynchroModel) getStepModel(AdminStep.DATA_SYNCHRONIZE);
    }

    public ReferentialSynchroModel getReferentialSynchroModel() {
        return (ReferentialSynchroModel) getStepModel(AdminStep.REFERENTIAL_SYNCHRONIZE);
    }

    public void startModel(AdminUI ui) {

        if (init) {
            throw new IllegalStateException(String.format("Can't init twice the model: %s", this));
        }
        availableIncomingModes.clear();

        // demarrage du modèle : on fixe ici une fois pour toute les liste
        // des onglets visibles
        start();

        // preparation de l'ui (création des onglets visibles)
//        ui.blockOperations();

        log.info("enables steps = " + steps);
        log.info("enables operations = " + operations);

        getConfigModel().start(this);

        getSelectDataModel().start(this);

        if (containsOperation(AdminStep.SAVE_LOCAL)) {
            getSaveLocalModel().start(this);
        }
        if (containsOperation(AdminStep.VALIDATE)) {
            getValidateModel().start(this);
        }
        if (containsOperation(AdminStep.REPORT)) {
            getReportModel().start(this);
        }
        if (containsOperation(AdminStep.CONSOLIDATE)) {
            getConsolidateModel().start(this);
        }
        if (containsOperation(AdminStep.ACTIVITY_PAIRING)) {
            getActivityPairingModel().start(this);
        }
        if (containsOperation(AdminStep.DATA_SYNCHRONIZE)) {
            getDataSynchroModel().start(this);
        }
        if (containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE)) {
            getReferentialSynchroModel().start(this);
        }
        if (containsOperation(AdminStep.SYNCHRONIZE)) {
            getSynchronizeReferentielModel().start(this);
        }
        if (containsOperation(AdminStep.EXPORT_DATA)) {
            getExportModel().start(this);
        }

        boolean needLeftDataSource = isNeedLeftDataSource();
        boolean needRightDataSource = isNeedRightDataSource();
        boolean needBothDataSources = needLeftDataSource && needRightDataSource;
        if (needLeftDataSource) {
            configModel.getLeftSourceModel().addPropertyChangeListener(DataSourceSelectorModel.DATA_SOURCE_CHANGED_PROPERTY_NAME, e -> {
                if (!isInit()) {
                    return;
                }
                if (needBothDataSources) {
                    onDataSourcesChanged(ui);
                } else {
                    onLeftSourceValidChanged(ui, true);
                }
            });
        }
        if (needRightDataSource) {
            configModel.getRightSourceModel().addPropertyChangeListener(DataSourceSelectorModel.DATA_SOURCE_CHANGED_PROPERTY_NAME, e -> {
                if (!isInit()) {
                    return;
                }
                if (needBothDataSources) {
                    onDataSourcesChanged(ui);
                } else {
                    onRightSourceValidChanged(ui, true);
                }
            });
        }
        log.info("End of start...");

        init = true;
        // on revalide le modèle (tout est prêt)
        validate();

        if (needLeftDataSource) {
            onLeftSourceValidChanged(ui, true);
        }
    }

    public void onDataSourcesChanged(AdminUI ui) {
        onLeftSourceValidChanged(ui, false);
        onRightSourceValidChanged(ui, true);
    }

    public void onLeftSourceValidChanged(AdminUI ui, boolean validate) {
        DataSourceSelector sourceConfig = ConfigUI.get(ui).getLeftSourceConfig();
        sourceConfig.onDatasourceChanged();
        if (validate) {
            configModel.doValidate(this);
            validate();
        }
    }

    public void onRightSourceValidChanged(AdminUI ui, boolean validate) {
        DataSourceSelector sourceConfig = ConfigUI.get(ui).getRightSourceConfig();
        sourceConfig.onDatasourceChanged();
        if (validate) {
            configModel.doValidate(this);
            validate();
        }
    }

    public boolean needSelect() {
        if (WizardState.CANCELED == getModelState()) {
            return false;
        }
        Set<AdminStep> operations = getOperations();
        for (AdminStep operation : operations) {
            if (operation.isNeedSelect()) {
                return true;
            }
        }
        return false;
    }

    public boolean isValidStep() {
        return validStep;
    }

    /**
     * Pour savoir si on a besoin d'une base source pour l'opération.
     * <p>
     * Actuellement, seul l'opération d'import acces n'a pas besoin d'une telle
     * base.
     *
     * @return {@code true} si au moins une des opérations a besoin d'une source
     * d'entrée, {@code false} autrement.
     * @since 2.0
     */
    public boolean isNeedLeftDataSource() {
        for (AdminStep op : getOperations()) {
            if (op.hasIncomingDataSourceConnectModes()) {

                // l'operation requiere une base en entree
                return true;
            }
        }

        // aucune operation ne requiere pas de base en entree
        return false;
    }

    /**
     * Pour savoir si on a besoin d'une base de référence pour l'opération.
     *
     * @return {@code true} si au moins une des opérations a besoin d'une source
     * de référence, {@code false} autrement.
     * @since 2.0
     */
    public boolean isNeedRightDataSource() {
        for (AdminStep op : getOperations()) {
            if (op.isNeedReferentiel()) {

                // l'operation requiere une base de référence
                return true;
            }
        }

        // aucune operation ne requiere pas de base de référence
        return false;
    }

    /**
     * Construit l'ensemble des modes disponibles pour la base en entrée.
     *
     * @return l'ensemble des modes disponibles.
     */
    public EnumSet<DataSourceConnectMode> getIncomingDataSourceConnectMode() {
        EnumSet<DataSourceConnectMode> result = EnumSet.noneOf(DataSourceConnectMode.class);
        getOperations().stream().filter(AdminStep::hasIncomingDataSourceConnectModes).forEach(op -> result.addAll(Arrays.asList(op.getDataSourceConnectModes())));
        return result;
    }

    @Override
    public void destroy() {
        getConfigModel().destroy();
        super.destroy();
    }

    @Override
    public void cancel() {
        super.cancel();
        // on affiche l'onglet de resume si requis
        AdminStep newStep = getSteps().get(getSteps().size() - 1);
        log.debug("Operation canceled, will go to final step " + newStep);
        gotoStep(newStep);
    }

    @Override
    public AdminUIModel addOperation(AdminStep step) {
        if (step.isNeedSelect()) {
            getOperations().add(AdminStep.SELECT_DATA);
        }
        if (AdminStep.EXPORT_DATA == step) {

            // pour exporter les données utilisateurs
            // l'opération de synchronisation de référentiel est nécessaire
            getOperations().add(AdminStep.SYNCHRONIZE);

            // l'opération de calcul des données est aussi obligatoire
            getOperations().add(AdminStep.CONSOLIDATE);
        }

        if (AdminStep.REPORT == step) {

            // l'opération de calcul des données est obligatoire
            getOperations().add(AdminStep.CONSOLIDATE);
        }
        return (AdminUIModel) super.addOperation(step);
    }

    @Override
    public void removeOperation(AdminStep step) {
        if (AdminStep.SYNCHRONIZE == step) {

            // pour exporter les données utilisateurs
            // l'opération de synchronisation de référentiel est nécessaire
            getOperations().remove(AdminStep.EXPORT_DATA);
        }

        if (AdminStep.CONSOLIDATE == step) {

            // pour exporter les données utilisateurs ou effectuer des reports
            // l'opération de calcul des données est nécessaire
            getOperations().remove(AdminStep.EXPORT_DATA);
            getOperations().remove(AdminStep.REPORT);
        }

        super.removeOperation(step);
    }

    @Override
    public boolean validate(AdminStep step) {

        if (!isInit()) {
            log.debug("not init");
            return false;
        }

        boolean validate = super.validate(step) && !getOperations().isEmpty();
        if (!validate) {
            log.debug("not valid from generic states...");
            return false;
        }

        switch (step) {
            case CONFIG:
                return getConfigModel().validate(this);
            case SELECT_DATA:
                return getSelectDataModel().validate(this);
            case VALIDATE:
                return getValidateModel().validate(this);
            case EXPORT_DATA:
                return getExportModel().validate(this);
            case CONSOLIDATE:
                return getConsolidateModel().validate(this);
            case ACTIVITY_PAIRING:
                return getActivityPairingModel().validate(this);
            case REPORT:
                return getReportModel().validate(this);
            case SYNCHRONIZE:
                return getSynchronizeReferentielModel().validate(this);
            case SAVE_LOCAL:
                return getSaveLocalModel().validate(this);
        }
        return true;
    }

    @Override
    protected AdminStep[] updateStepUniverse() {

        List<AdminStep> universe = new ArrayList<>();

        // toujours l'onglet de configuration des opérations
        universe.add(AdminStep.CONFIG);

        if (!operations.isEmpty()) {

            if (needSelect()) {

                // ajout de l'onglet de selection des donnees
                universe.add(AdminStep.SELECT_DATA);
            }

            if (containsOperation(AdminStep.DATA_SYNCHRONIZE)) {

                universe.add(AdminStep.DATA_SYNCHRONIZE);
            }

            if (containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE)) {

                universe.add(AdminStep.REFERENTIAL_SYNCHRONIZE);
            }

            if (containsOperation(AdminStep.SYNCHRONIZE)) {

                // ajout de l'onglet de resolution des entites obsoletes
                universe.add(AdminStep.SYNCHRONIZE);
            }

            if (containsOperation(AdminStep.VALIDATE)) {

                // ajout de l'onglet de validation
                universe.add(AdminStep.VALIDATE);
            }

            if (containsOperation(AdminStep.CONSOLIDATE)) {

                // ajout de l'onglet de consolidation
                universe.add(AdminStep.CONSOLIDATE);
            }

            if (containsOperation(AdminStep.ACTIVITY_PAIRING)) {

                // ajout de l'onglet d'appairement
                universe.add(AdminStep.ACTIVITY_PAIRING);
            }

            if (containsOperation(AdminStep.REPORT)) {

                universe.add(AdminStep.REPORT);
            }

            updateSaveLocalOperation();

            if (containsOperation(AdminStep.SAVE_LOCAL)) {

                // ajout de l'onglet de sauvegarde de la base locale
                universe.add(AdminStep.SAVE_LOCAL);
            }

            if (containsOperation(AdminStep.EXPORT_DATA)) {

                // ajout de l'onglet de sauvegarde de la base distante
                universe.add(AdminStep.EXPORT_DATA);
            }
        }
        return universe.toArray(new AdminStep[0]);
    }

    private void updateSaveLocalOperation() {
        boolean shouldAdd = false;
        for (AdminStep s : operations) {
            if (s.isNeedSave()) {
                shouldAdd = true;
                break;
            }
        }
        if (shouldAdd) {
            // ajout de l'opération
            operations.add(AdminStep.SAVE_LOCAL);
        } else {
            // on doit supprimer l'opération
            operations.remove(AdminStep.SAVE_LOCAL);
        }
    }

    public EnumSet<DataSourceConnectMode> getAvailableIncomingModes() {
        return availableIncomingModes;
    }

    public boolean isWasDone() {
        return wasDone;
    }

    public void setWasDone(boolean wasDone) {
        boolean oldValue = this.wasDone;
        this.wasDone = wasDone;
        firePropertyChange(PROPERTY_WAS_DONE, oldValue, wasDone);
    }
}
