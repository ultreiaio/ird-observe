package fr.ird.observe.client.datasource.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;

import javax.swing.KeyStroke;

/**
 * Created on 03/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ObserveKeyStrokesActions extends ObserveKeyStrokesEditorApi {

    public static final KeyStroke KEY_STROKE_DATA_SYNCHRO_COPY_LEFT = KeyStroke.getKeyStroke("pressed F4");
    public static final KeyStroke KEY_STROKE_DATA_SYNCHRO_DELETE_LEFT = KeyStroke.getKeyStroke("pressed F5");

    public static final KeyStroke KEY_STROKE_DATA_SYNCHRO_COPY_RIGHT = KeyStroke.getKeyStroke("pressed F9");
    public static final KeyStroke KEY_STROKE_DATA_SYNCHRO_DELETE_RIGHT = KeyStroke.getKeyStroke("pressed F10");
    public static final KeyStroke KEY_STROKE_EXPORT_POPUP = KeyStroke.getKeyStroke("pressed F10");
    public static final KeyStroke KEY_STROKE_EXPORT_CLIPBOARD = KeyStroke.getKeyStroke("pressed F2");
    public static final KeyStroke KEY_STROKE_EXPORT_CSV = KeyStroke.getKeyStroke("pressed F3");
    public static final KeyStroke KEY_STROKE_EXPORT_HTML = KeyStroke.getKeyStroke("pressed F4");

    public static final KeyStroke KEY_STROKE_EXIT = KeyStroke.getKeyStroke("alt pressed F");
}
