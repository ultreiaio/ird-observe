package fr.ird.observe.client.datasource.actions.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.actions.AdminActionWorker;
import fr.ird.observe.client.datasource.actions.AdminTabUI;
import fr.ird.observe.client.datasource.actions.AdminUI;
import fr.ird.observe.dto.ProgressionModel;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.TimeLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.concurrent.Callable;

import static io.ultreia.java4all.i18n.I18n.t;

public abstract class AdminTabUIActionSupport<U extends AdminTabUI> extends JComponentActionSupport<U> implements WithClientUIContextApi {
    private static final Logger log = LogManager.getLogger(AdminTabUIActionSupport.class);
    /**
     * To listen progression model messages (to add them in action messages area).
     */
    private final PropertyChangeListener progressionModelListener;

    public AdminTabUIActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
        progressionModelListener = evt -> {
            String message = (String) evt.getNewValue();
            addMessage(message);
        };
    }

    public AdminTabUIActionSupport(String actionCommandKey, String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(actionCommandKey, label, shortDescription, actionIcon, acceleratorKey);
        progressionModelListener = evt -> {
            String message = (String) evt.getNewValue();
            addMessage(message);
        };
    }

    public AdminTabUIActionSupport(String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
        progressionModelListener = evt -> {
            String message = (String) evt.getNewValue();
            addMessage(message);
        };
    }

    public AdminTabUIActionSupport(String actionCommandKey, String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(actionCommandKey, label, shortDescription, actionIcon, acceleratorKey);
        progressionModelListener = evt -> {
            String message = (String) evt.getNewValue();
            addMessage(message);
        };
    }

    protected void addAdminWorkerWithNoProgress(String label, Callable<WizardState> callable) {
        AdminActionWorker worker = AdminActionWorker.newWorker(ui.getHandler(), label, callable, null);
        getActionExecutor().addAction(worker.getActionLabel(), worker);
    }

    protected void addAdminWorker(String label, Callable<WizardState> callable) {
        installProgressBar();
        AdminActionWorker worker = AdminActionWorker.newWorker(ui.getHandler(), label, callable, this::unInstallProgressBar);
        getActionExecutor().addAction(worker.getActionLabel(), worker);
    }

    public String sendMessage(String message) {
        addMessage(message);
        return message;
    }

    protected AdminUI getParentUI() {
        return ui.getContextValue(AdminUI.class, "parent");
    }

    public void addMessage(String text) {
        JTextArea progression = ui.getProgression();
        progression.append(text + "\n");
        progression.setCaretPosition(progression.getDocument().getLength());
    }

    protected void installProgressBar() {
        ProgressionModel model = ui.getStepModel().getProgressModel();
        model.installUI(ui.getProgressBar(), false);
        model.removePropertyChangeListener(ProgressionModel.PROPERTY_MESSAGE, progressionModelListener);
        model.addPropertyChangeListener(ProgressionModel.PROPERTY_MESSAGE, progressionModelListener);
    }

    protected void unInstallProgressBar() {
        ProgressionModel model = ui.getStepModel().getProgressModel();
        model.uninstallUI(ui.getProgressBar());
        model.removePropertyChangeListener(ProgressionModel.PROPERTY_MESSAGE, progressionModelListener);
    }

    protected void onEndAction(long t00, int stepCount, ProgressionModel progressModel) {
        if (progressModel != null) {
            progressModel.increments();
        }
        sendMessage(t("observe.ui.datasource.editor.actions.operation.message.done", new Date(), Strings.convertTime(TimeLog.getTime() - t00)));
        if (progressModel != null) {
            log.info(String.format("Expected count: %d - final step count: %d", stepCount, progressModel.getValue()));
        }
    }
}
