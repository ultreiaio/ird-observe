package fr.ird.observe.client.datasource.actions.actions;

/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUI;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.actions.MainUIActionSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class LaunchAdminAction extends MainUIActionSupport {

    private static final Logger log = LogManager.getLogger(LaunchAdminAction.class);

    private final AdminStep action;

    public LaunchAdminAction(AdminStep action) {
        super(t(Objects.requireNonNull(action).getOperationLabel()), t(action.getOperationDescription()), action.getIconName(), action.getMnemonic());
        this.action = action;
    }

    //FIXME:BodyContent No more used, since we will check to leave editor while hiding it
//    @Override
//    protected boolean canExecuteAction(ActionEvent e) {
//        boolean canExecuteAction = super.canExecuteAction(e);
//        if (canExecuteAction) {
//            canExecuteAction = getContentUIManager().closeSelectedContentUI();
//        }
//        return canExecuteAction;
//    }

    @Override
    protected void doActionPerformed(ActionEvent e, ObserveMainUI ui) {
        log.info(String.format("Will start admin action: %s", action.getLabel()));
        ui.setContextValue(action);
        try {
            ui.changeBodyContent(AdminUI.class);
        } finally {
            ui.removeContextValue(AdminStep.class);
        }
    }
}
