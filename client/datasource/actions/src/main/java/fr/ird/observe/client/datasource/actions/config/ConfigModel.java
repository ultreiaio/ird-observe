package fr.ird.observe.client.datasource.actions.config;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.PropertyChangeListener;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 28/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ConfigModel extends AdminActionModel {

    private static final PropertyChangeListener LOG_PROPERTY_CHANGE_LISTENER = new UIHelper.LogPropertyChanged();

    private static final Logger log = LogManager.getLogger(ConfigModel.class);

    /**
     * la configuration de la base source
     */
    private final DataSourceSelectorModel leftSourceModel;

    /**
     * la configuration de la base central
     */
    private final DataSourceSelectorModel rightSourceModel;

    /**
     * la configuration de la source de données en cours d'utilisation avant de faire une action longue.
     */
    private ObserveDataSourceConfiguration previousSourceConfiguration;

    public ConfigModel() {
        super(AdminStep.CONFIG);
        leftSourceModel = new DataSourceSelectorModel(ObserveKeyStrokesActions.KEY_STROKE_CONFIGURE_LOCAL_SOURCE) {
            @Override
            public boolean validateExt() {
                boolean isValid = super.validateExt();
                if (isValid && rightSourceModel.isValid()) {
                    isValid = validateNotSameDataSources();
                    if (!isValid) {
                        setValidationMessage(I18n.t("observe.ui.datasource.actions.config.data.sources.equals"));
                        return false;
                    }
                }
                return isValid;
            }
        };

        rightSourceModel = new DataSourceSelectorModel(ObserveKeyStrokesActions.KEY_STROKE_CONFIGURE_REMOTE_SOURCE) {
            @Override
            public boolean validateExt() {
                boolean isValid = super.validateExt();
                if (isValid && leftSourceModel.isValid()) {
                    isValid = validateNotSameDataSources();
                    if (!isValid) {
                        setValidationMessage(I18n.t("observe.ui.datasource.actions.config.data.sources.equals"));
                        return false;
                    }
                }
                return isValid;
            }
        };
        leftSourceModel.setSourceLabel(I18n.t("observe.ui.datasource.storage.config.source.storage"));
    }

    @Override
    public boolean validate(AdminUIModel uiModel) {

        // validate common configuration (data source validity)
        boolean validate = validateConfig(uiModel);

        if (!validate) {
            return false;
        }

        for (AdminStep operation : uiModel.getOperations()) {
            AdminActionModel stepModel = uiModel.getStepModel(operation);
            validate = stepModel.validateConfig(uiModel);
            if (!validate) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void start(AdminUIModel uiModel) {

        // on positionne la source courante de l'application comme service
        // entrant (on se sert de la configuration de ce service s'il existe
        // pour la source locale).
        Optional<ObserveSwingDataSource> optionalPreviousSource = getDataSourcesManager().getOptionalMainDataSource();
        setPreviousSourceConfiguration(optionalPreviousSource.map(ObserveSwingDataSource::getConfiguration).orElse(null));

        // avant le démarrage du wizard, on ferme toujours la source
        // en cours d'utilisation

        ObserveDataSourceInformation previousSourceInformation = null;
        if (optionalPreviousSource.isPresent()) {
            ObserveSwingDataSource dataSource = optionalPreviousSource.get();
            previousSourceInformation = dataSource.getDataSourceInformation();
            log.debug(String.format("Close previous source %s", dataSource.getLabel()));
            ObserveSwingDataSource.doCloseSource(dataSource);
        }
        startLeftSourceModel(uiModel, previousSourceInformation);
        startRightSourceModel(uiModel);
    }

    @Override
    public void destroy() {
        super.destroy();
        leftSourceModel.destroy();
        rightSourceModel.destroy();
    }

    private void startLeftSourceModel(AdminUIModel uiModel, ObserveDataSourceInformation previousSourceInfo) {

        if (!uiModel.isNeedLeftDataSource()) {
            return;
        }

        ObserveDataSourceConfiguration previousSourceConfiguration = getPreviousSourceConfiguration();

        ChooseDbModel chooseDb = leftSourceModel.getChooseDb();

        ObserveDataSourceConfiguration previousSourceConfig = null;

        EnumSet<DataSourceConnectMode> authorizedModes = uiModel.getIncomingDataSourceConnectMode();
        EnumSet<DataSourceConnectMode> modes = EnumSet.noneOf(DataSourceConnectMode.class);

        if (authorizedModes.contains(DataSourceConnectMode.LOCAL)) {
            // ce mode est disponible uniquement si une base locale existe
            if (chooseDb.isLocalStorageExist()) {
                modes.add(DataSourceConnectMode.LOCAL);
            }
        }
        if (authorizedModes.contains(DataSourceConnectMode.REMOTE)) {
            modes.add(DataSourceConnectMode.REMOTE);
        }
        if (authorizedModes.contains(DataSourceConnectMode.SERVER)) {
            modes.add(DataSourceConnectMode.SERVER);
        }
        if (previousSourceConfiguration != null) {
            try {
                previousSourceConfig = previousSourceConfiguration.clone();
//                previousSourceInfo = previousSource.getInformation();
            } catch (CloneNotSupportedException e) {
                log.error("con not clone previous data configuration", e);
            }

            if (previousSourceConfiguration.isRemote()) {
                if (!modes.contains(DataSourceConnectMode.REMOTE)) {
                    // pas autorise a utiliser cette source en entree
                    previousSourceConfiguration = null;
                }
            } else if (previousSourceConfiguration.isServer()) {
                if (!modes.contains(DataSourceConnectMode.SERVER)) {
                    // pas autorise a utiliser cette source en entree
                    previousSourceConfiguration = null;
                }
            } else if (previousSourceConfiguration.isLocal()) {
                if (!modes.contains(DataSourceConnectMode.LOCAL)) {
                    // pas autorise a utiliser cette source en entree
                    previousSourceConfiguration = null;
                }
            }
        }
        uiModel.getAvailableIncomingModes().addAll(modes);

        DataSourceInitModel initModel = chooseDb.getInitModel();
        initModel.getAuthorizedInitModes().remove(DataSourceInitMode.CREATE);
        initModel.setAuthorizedConnectModes(modes);
        initModel.setAuthorizedCreateModes(EnumSet.noneOf(DataSourceCreateMode.class));

        if (previousSourceConfiguration == null) {
            // on initialise le modèle de la source locale à partir du service entrant (s'il existe)
            leftSourceModel.init(getClientConfig());

        } else {
            // on initialise le modèle de la source locale à partir du service entrant (s'il existe)
            leftSourceModel.initFromPreviousConfig(previousSourceConfig, previousSourceInfo);
        }
        leftSourceModel.start();

        if (log.isDebugEnabled()) {
            leftSourceModel.removePropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
            leftSourceModel.addPropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
        }
    }

    private void startRightSourceModel(AdminUIModel uiModel) {

        if (!uiModel.isNeedRightDataSource()) {
            return;
        }

        // par default, on tente d'utiliser le serveur distance
        rightSourceModel.init(getClientConfig());
        ChooseDbModel chooseDb = rightSourceModel.getChooseDb();
        DataSourceInitModel initModel = chooseDb.getInitModel();
        boolean canUseLocalSource = uiModel.getOperations().contains(AdminStep.DATA_SYNCHRONIZE)
                || uiModel.getOperations().contains(AdminStep.SYNCHRONIZE)
                || uiModel.getOperations().contains(AdminStep.REFERENTIAL_SYNCHRONIZE);
        initModel.getAuthorizedInitModes().remove(DataSourceInitMode.CREATE);
        if (!canUseLocalSource) {
            initModel.getAuthorizedConnectModes().remove(DataSourceConnectMode.LOCAL);
        }
        chooseDb.setInitMode(DataSourceInitMode.CONNECT);
        chooseDb.setConnectMode(DataSourceConnectMode.SERVER);
        rightSourceModel.start();
        if (log.isDebugEnabled()) {
            rightSourceModel.removePropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
            rightSourceModel.addPropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
        }
    }

    public boolean validateNotSameDataSources() {
        return !Objects.equals(leftSourceModel.getLabelWithUrl(), rightSourceModel.getLabelWithUrl());
    }

    public DataSourceSelectorModel getLeftSourceModel() {
        return leftSourceModel;
    }

    public DataSourceSelectorModel getRightSourceModel() {
        return rightSourceModel;
    }

    public ObserveDataSourceConfiguration getPreviousSourceConfiguration() {
        return previousSourceConfiguration;
    }

    private void setPreviousSourceConfiguration(ObserveDataSourceConfiguration previousSource) {
        this.previousSourceConfiguration = previousSource;
    }

    public void doValidate(AdminUIModel uiModel) {
        boolean valid = validate(uiModel);
        firePropertyChange(AdminUIModel.VALID_STEP_PROPERTY_NAME, valid);
    }
}
