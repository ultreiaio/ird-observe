<!--
  #%L
  ObServe Client :: DataSource :: Actions
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.actions.AdminTabUI>

  <import>
    fr.ird.observe.client.datasource.actions.AdminStep
    fr.ird.observe.client.datasource.actions.AdminTabUI
    fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelector
    fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel

    java.awt.Color
    static io.ultreia.java4all.i18n.I18n.t
  </import>
  <script><![CDATA[
public static ConfigUI get(AdminUI ui) {
    return get(ui, AdminStep.CONFIG);
}
]]>
  </script>

  <ConfigModel id='stepModel' initializer='getModel().getConfigModel()'/>
  <DataSourceSelectorModel id='leftSourceModel'/>
  <DataSourceSelectorModel id='rightSourceModel'/>

  <JPanel id='PENDING_content' layout="{new BorderLayout()}">
    <JPanel id='config' constraints='BorderLayout.CENTER' decorator='boxed' layout="{new BorderLayout()}">
      <JPanel id="configLabelPanel" constraints='BorderLayout.NORTH'>
        <JLabel id="configLabel"/>
      </JPanel>
      <Table fill='both' weightx='1' insets='0' constraints='BorderLayout.CENTER'>
        <row>
          <cell fill='both' weightx='1'>
            <JPanel id='operations' layout="{new GridLayout(0, 1, 3, 3)}"/>
          </cell>
        </row>
        <!-- configuration de la source en entree -->
        <row>
          <cell fill='both'>
            <DataSourceSelector id="leftSourceConfig" initializer="DataSourceSelector.create(this, getLeftSourceModel())"/>
          </cell>
        </row>
        <!-- configuration de la source de référence -->
        <row>
          <cell fill='both'>
            <DataSourceSelector id="rightSourceConfig" initializer="DataSourceSelector.create(this, getRightSourceModel())"/>
          </cell>
        </row>
        <!-- configuration supplémentaire (fournit par les opérations) -->
        <row>
          <cell fill='both' weightx='1'>
            <JPanel id="extraConfig" layout="{new BorderLayout()}"/>
          </cell>
        </row>
        <row>
          <cell fill='both' weightx='1'>
            <JPanel id="extraConfig2" layout="{new BorderLayout()}"/>
          </cell>
        </row>
        <row>
          <cell fill='both' weightx='1' weighty='1'>
            <JPanel/>
          </cell>
        </row>
      </Table>
    </JPanel>
  </JPanel>

</fr.ird.observe.client.datasource.actions.AdminTabUI>
