/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.config;

import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Font;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ConfigUIHandler extends AdminTabUIHandler<ConfigUI> implements UIHandler<ConfigUI> {

    private static final Logger log = LogManager.getLogger(ConfigUIHandler.class);

    @Override
    public void afterInit(ConfigUI ui) {
        ui.getLeftSourceConfig().getSourceInfoLabel().setVisible(false);
        DefaultUIInitializer.doInit(ui);
        super.afterInit(ui);
        UIHelper.setLayerUI(ui.getConfig(), parentUI.getConfigBlockLayerUI());
        UIHelper.setLayerUI(ui.getContent(), null);
        AdminUIModel model = ui.getModel();
        model.getConfigModel().addPropertyChangeListener(AdminUIModel.VALID_STEP_PROPERTY_NAME, e -> {
            if (model.isInit()) {
                return;
            }
            model.validate();
        });
        model.addPropertyChangeListener(AdminUIModel.OPERATIONS_PROPERTY_NAME, e -> blockOperations());
    }

    @Override
    protected void onComingFromNextStep(AdminStep oldStep) {
        onComingFromNextStepAdjust();
        super.onComingFromNextStep(oldStep);
    }

    private void blockOperations() {
        AdminUIModel model = ui.getModel();
        if (model.isInit()) {
            return;
        }
        model.setValueAdjusting(true);
        try {
            JPanel operations = ui.getOperations();
            operations.removeAll();
            for (AdminStep operation : model.getOperations()) {
                if (AdminStep.SELECT_DATA == operation) {
                    continue;
                }
                log.info("Add operation: " + operation);
                JLabel label = new JLabel(operation.getLabel());
                label.setFont(label.getFont().deriveFont(Font.ITALIC).deriveFont(13f));
//                label.setMinimumSize(new Dimension(100, 30));
                label.setIcon(operation.getIcon());
                label.setIconTextGap(12);
                label.setToolTipText(operation.getDescription());
                operations.add(label);
            }
        } finally {
            model.setValueAdjusting(false);
        }
    }
}
