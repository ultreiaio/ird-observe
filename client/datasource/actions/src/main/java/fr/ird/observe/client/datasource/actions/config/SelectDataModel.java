package fr.ird.observe.client.datasource.actions.config;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.validate.ValidateModel;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePane;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePaneHandler;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.navigation.tree.selection.SelectionTreeConfig;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * FIXME Replace usage of reference by ToolkitIdLabel
 * Created on 28/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectDataModel extends AdminActionModel {

    private static final Logger log = LogManager.getLogger(SelectDataModel.class);

    private SelectionTreeModel selectionDataModel;

    public SelectDataModel() {
        super(AdminStep.SELECT_DATA);
    }

    public SelectionTreeConfig getTreeConfig() {
        return getSelectionDataModel().getConfig();
    }

    @Override
    public void start(AdminUIModel uiModel) {

        if (!uiModel.needSelect()) {
            return;
        }

        getSelectionDataModel().addPropertyChangeListener(SelectionTreeModel.SELECTED_COUNT, evt -> {
            SelectionTreeModel source = (SelectionTreeModel) evt.getSource();
            log.debug(String.format("selection data model [%s] changed on %s, new value = %s", source, evt.getPropertyName(), evt.getNewValue()));
            uiModel.validate();
            log.debug(String.format("nb selected export data = %d", source.getSelectedCount()));
        });
    }

    @Override
    public boolean validate(AdminUIModel uiModel) {

        boolean validate = true;
        boolean notEmpty = !selectionDataModel.isSelectionEmpty();
        if (uiModel.containsOperation(AdminStep.VALIDATE)) {
            validate = uiModel.validate(AdminStep.CONFIG) && notEmpty;
        }
        if (uiModel.containsOperation(AdminStep.EXPORT_DATA)) {
            validate &= uiModel.validate(AdminStep.CONFIG) && notEmpty;
        }
        if (uiModel.containsOperation(AdminStep.CONSOLIDATE)) {
            validate &= uiModel.validate(AdminStep.CONFIG) && notEmpty;
        }
        if (uiModel.containsOperation(AdminStep.ACTIVITY_PAIRING)) {
            validate &= uiModel.validate(AdminStep.CONFIG) && notEmpty;
        }
        if (uiModel.containsOperation(AdminStep.REPORT)) {
            validate &= uiModel.validate(AdminStep.CONFIG) && notEmpty;
        }
        return validate;
    }

    public SelectionTreeModel getSelectionDataModel() {
        return selectionDataModel;
    }

    void setSelectionDataModel(SelectionTreeModel selectionDataModel) {
        this.selectionDataModel = selectionDataModel;
    }

    public boolean initSelectionModel(SelectionTreePane selectTreePane, AdminUIModel uiModel, boolean init) {
        boolean selectAllData = true;
        boolean loadData = true;
        boolean loadReferential = true;
        boolean useOpenData = false;
        boolean doExport = uiModel.containsOperation(AdminStep.EXPORT_DATA);
        if (doExport) {
            loadReferential = false;
            useOpenData = true;
        }
        if (uiModel.containsOperation(AdminStep.REPORT)) {
            loadReferential = false;
        }
        if (uiModel.containsOperation(AdminStep.VALIDATE)) {
            useOpenData = true;
            ValidateModel validateModel = uiModel.getValidateModel();
            switch (validateModel.getModelMode()) {
                case DATA:
                    loadReferential = false;
                    break;
                case REFERENTIEL:
                    loadData = false;
                    break;
                default:
                    break;
            }
        }
        if (uiModel.containsOperation(AdminStep.CONSOLIDATE)) {
            useOpenData = true;
            loadReferential = false;
        }
        if (uiModel.containsOperation(AdminStep.ACTIVITY_PAIRING)) {
            useOpenData = true;
            loadReferential = false;
            selectAllData = false;
        }
        try (ObserveSwingDataSource dataSource = uiModel.getConfigModel().getLeftSourceModel().getSafeSource(true)) {
            SelectionTreeModel selectionDataModel = getSelectionDataModel();
            SelectionTreeConfig config = getTreeConfig();
            if (init) {
                SelectionTreeConfig newConfig = dataSource.newSelectionTreeConfig();
                newConfig.setLoadData(loadData);
                newConfig.setLoadReferential(loadReferential);
                newConfig.setUseOpenData(useOpenData);
                // do not show empty group by
                newConfig.setLoadEmptyGroupBy(false);
                // need to see all data
                newConfig.setLoadNullGroupBy(true);
                // need to see all data
                newConfig.setLoadDisabledGroupBy(true);

                config.init(newConfig);
                Project navigationEditModel = getClientConfig().getNavigationEditModel();
                if (navigationEditModel != null) {
                    List<String> openIds = navigationEditModel.getIds();
                    selectionDataModel.setEditIds(openIds);
                }
            } else {
                config.setLoadData(loadData);
                config.setLoadReferential(loadReferential);
                config.setUseOpenData(useOpenData);
            }
            SelectionTreePaneHandler.initDataSource(selectTreePane, dataSource);
            selectionDataModel.populate(dataSource.getNavigationService()::loadSelectionRoot);
            if (uiModel.containsOperation(AdminStep.EXPORT_DATA)) {
                try (ObserveSwingDataSource safeCentralSource = uiModel.getConfigModel().getRightSourceModel().getSafeSource(true)) {
                    String modelType = config.getModuleName();
                    List<String> existingTrip;
                    switch (modelType) {
                        case "ps":
                            existingTrip = new ArrayList<>(safeCentralSource.getPsCommonTripService().getAllTripIds());
                            break;
                        case "ll":
                            existingTrip = new ArrayList<>(safeCentralSource.getLlCommonTripService().getAllTripIds());
                            break;
                        default:
                            throw new IllegalStateException(String.format("ModelType %s not managed", modelType));
                    }
                    selectionDataModel.augmentsExistIds(existingTrip);
                }
            }
            boolean local = dataSource.isLocal();
            if (local && selectAllData) {
                selectionDataModel.selectAll();
            }
            return local;
        }
    }

}
