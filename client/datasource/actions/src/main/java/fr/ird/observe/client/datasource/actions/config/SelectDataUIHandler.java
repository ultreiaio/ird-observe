package fr.ird.observe.client.datasource.actions.config;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUIHandler;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePane;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePaneHandler;
import fr.ird.observe.client.datasource.editor.api.selection.actions.SelectUnselect;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.navigation.tree.selection.SelectionTree;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.SwingUtilities;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.function.Consumer;

/**
 * Created on 28/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectDataUIHandler extends AdminTabUIHandler<SelectDataUI> implements UIHandler<SelectDataUI> {

    public static void loadSelectData(SelectDataUI ui) {
        AdminUIModel model = ui.getModel();
        // To avoid multiple init
        boolean init = ui.getClientProperty("__init") == null;
        SelectionTreePane treePane = ui.getSelectTreePane();
        SelectionTree tree = treePane.getTree();
        if (init) {
            ui.putClientProperty("__init", true);
        } else {
            tree.clearSelection();
        }
        boolean isLocal = ui.getModel().getSelectDataModel().initSelectionModel(treePane, model, init);
        SelectionTreePaneHandler.updateStatistics(treePane);
        if (tree.getModel().isNotEmpty()) {
            tree.setSelectionRow(0);
        }
        if (isLocal) {
            SwingUtilities.invokeLater(tree::expandAll);
        }
        SwingUtilities.invokeLater(tree::requestFocusInWindow);
    }

    @Override
    public void registerActions(SelectDataUI ui) {
        SelectUnselect.installUI(ui.getSelectTreePane());
        Consumer<TreeConfigUI> init = u -> {
            if (u.getBean().isLoadData()) {
                TreeConfigUIHandler.hideOptions(u);
            } else {
                TreeConfigUIHandler.hideOptions(u);
                TreeConfigUIHandler.hideData(u);
            }
        };
        Consumer<TreeConfigUI> apply = u -> loadSelectData(ui);
        SelectionTreePaneHandler.init(ui.getSelectTreePane(), init, apply);
    }

    @Override
    public void afterInit(SelectDataUI ui) {
        super.afterInit(ui);
        setAutoStart(ui.getSelectData());
        SelectionTree tree = ui.getSelectTreePane().getTree();
        getModel().getSelectDataModel().setSelectionDataModel(tree.getModel());
        getModel().addPropertyChangeListener(AdminUIModel.PROPERTY_WAS_DONE, evt -> {
            UIHelper.setLayerUI(ui.getContent(), null);
            if (UIHelper.isLayered(ui.getSelectTreePane())) {
                UIHelper.setLayerUI(ui.getSelectTreePane(), parentUI.getConfigBlockLayerUI());
            }
        });
    }

    @Override
    protected void onStateChangeToSuccess() {
        super.onStateChangeToSuccess();
        ui.getSUCCESSED_panel().removeAll();
        ui.getSUCCESSED_panel().add(SwingUtil.boxComponentWithJxLayer(ui.getSelectTreePane()), new GridBagConstraints(0, 0, 1, 1, 1.0, 1, 10, 1, new Insets(3, 3, 3, 3), 0, 0));
        SwingUtilities.invokeLater(() -> ui.getSelectTreePane().getTree().requestFocusInWindow());
    }

    @Override
    protected void onComingFromNextStep(AdminStep oldStep) {
        onComingFromNextStepAdjust();
        super.onComingFromNextStep(oldStep);
    }

}
