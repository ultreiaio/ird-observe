package fr.ird.observe.client.datasource.actions.config.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.actions.AdminTabUIActionSupport;
import fr.ird.observe.client.datasource.actions.config.SelectDataUI;
import fr.ird.observe.client.datasource.actions.config.SelectDataUIHandler;
import fr.ird.observe.navigation.tree.selection.SelectionTree;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

public class SelectData extends AdminTabUIActionSupport<SelectDataUI> implements Runnable {

    public SelectData() {
        super(null, null, "wizard-start", 'D');
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        setText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        setTooltipText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        super.defaultInit(inputMap, actionMap);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SelectDataUI ui) {
        run();
    }

    @Override
    public void run() {
        if (getParentUI().getConfigBlockLayerUI().isBlock()) {
            return;
        }
        addAdminWorkerWithNoProgress(ui.getSelectData().getToolTipText(), this::run0);
    }

    private WizardState run0() {
        AdminUIModel model = ui.getModel();
        WizardState stepState = model.getStepState(ui.getStep());
        if (stepState != WizardState.PENDING) {
            return stepState;
        }
        SelectionTree selectTree = ui.getSelectTreePane().getTree();
        selectTree.clearSelection();

        SelectDataUIHandler.loadSelectData(ui);
        return WizardState.SUCCESSED;
    }
}
