/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.consolidate;

import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.dto.referential.common.SpeciesListReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.util.Set;
import java.util.TreeSet;

/**
 * Modele pour preparer une validation de donnees d'une base.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class ConsolidateModel extends AdminActionModel {

    private static final Logger log = LogManager.getLogger(ConsolidateModel.class);
    /**
     * To track trip that has already been consolidated (they won't be again processed).
     */
    private final Set<String> alreadyDoneTripIds;
    /**
     * Data source to use for operation.
     */
    private ObserveSwingDataSource source;
    private boolean consolidationFailIfLengthWeightParameterNotFound;
    private boolean consolidationFailIfLengthLengthParameterNotFound;
    private SpeciesListReference speciesListForLogbookSampleActivityWeightedWeight;
    private SpeciesListReference speciesListForLogbookSampleWeights;
    private boolean skipForReport;

    public ConsolidateModel() {
        super(AdminStep.CONSOLIDATE);
        this.alreadyDoneTripIds = new TreeSet<>();
    }

    public ObserveSwingDataSource getSource() {
        return source;
    }

    public void setSource(ObserveSwingDataSource source) {
        this.source = source;
    }

    @Override
    public void destroy() {
        super.destroy();
        source = null;
    }

    public void start(AdminUIModel uiModel) {
        addPropertyChangeListener(evt -> {
            ConsolidateModel source = (ConsolidateModel) evt.getSource();
            log.debug(String.format("consolidate model [%s] changed on %s, new value = %s", source, evt.getPropertyName(), evt.getNewValue()));
            uiModel.validate();
        });

        DataSourceSelectorModel sourceModel = uiModel.getConfigModel().getLeftSourceModel();
        sourceModel.setRequiredReadOnReferential(true);
        sourceModel.setRequiredReadOnData(true);
        sourceModel.setRequiredWriteOnData(true);

        setConsolidationFailIfLengthWeightParameterNotFound(getClientConfig().isConsolidationFailIfLengthWeightParameterNotFound());
        setConsolidationFailIfLengthLengthParameterNotFound(getClientConfig().isConsolidationFailIfLengthLengthParameterNotFound());
    }

    @Override
    public boolean validate(AdminUIModel uiModel) {
        return uiModel.validate(AdminStep.SELECT_DATA) && uiModel.getStepState(step) == WizardState.SUCCESSED;
    }

    @Override
    public boolean validateConfig(AdminUIModel uiModel) {
        return super.validateConfig(uiModel)
                && getSpeciesListForLogbookSampleActivityWeightedWeight() != null
                && getSpeciesListForLogbookSampleWeights() != null;
    }

    public boolean checkIfTripAlreadyProcessed(String tripId) {
        return alreadyDoneTripIds.contains(tripId);
    }

    public void setTripProcessed(String tripId) {
        alreadyDoneTripIds.add(tripId);
    }

    public boolean isConsolidationFailIfLengthWeightParameterNotFound() {
        return consolidationFailIfLengthWeightParameterNotFound;
    }

    public void setConsolidationFailIfLengthWeightParameterNotFound(boolean consolidationFailIfLengthWeightParameterNotFound) {
        boolean oldValue = this.consolidationFailIfLengthWeightParameterNotFound;
        this.consolidationFailIfLengthWeightParameterNotFound = consolidationFailIfLengthWeightParameterNotFound;
        firePropertyChange("consolidationFailIfLengthWeightParameterNotFound", oldValue, consolidationFailIfLengthWeightParameterNotFound);
    }

    public boolean isConsolidationFailIfLengthLengthParameterNotFound() {
        return consolidationFailIfLengthLengthParameterNotFound;
    }

    public void setConsolidationFailIfLengthLengthParameterNotFound(boolean consolidationFailIfLengthLengthParameterNotFound) {
        boolean oldValue = this.consolidationFailIfLengthLengthParameterNotFound;
        this.consolidationFailIfLengthLengthParameterNotFound = consolidationFailIfLengthLengthParameterNotFound;
        firePropertyChange("consolidationFailIfLengthLengthParameterNotFound", oldValue, consolidationFailIfLengthLengthParameterNotFound);
    }

    public SpeciesListReference getSpeciesListForLogbookSampleActivityWeightedWeight() {
        return speciesListForLogbookSampleActivityWeightedWeight;
    }

    public void setSpeciesListForLogbookSampleActivityWeightedWeight(SpeciesListReference speciesListForLogbookSampleActivityWeightedWeight) {
        SpeciesListReference oldValue = this.speciesListForLogbookSampleActivityWeightedWeight;
        this.speciesListForLogbookSampleActivityWeightedWeight = speciesListForLogbookSampleActivityWeightedWeight;
        firePropertyChange("speciesListForLogbookSampleActivityWeightedWeight", oldValue, speciesListForLogbookSampleActivityWeightedWeight);
    }

    public String getSpeciesListForLogbookSampleActivityWeightedWeightId() {
        return speciesListForLogbookSampleActivityWeightedWeight == null ? null : speciesListForLogbookSampleActivityWeightedWeight.getId();
    }

    public SpeciesListReference getSpeciesListForLogbookSampleWeights() {
        return speciesListForLogbookSampleWeights;
    }

    public void setSpeciesListForLogbookSampleWeights(SpeciesListReference speciesListForLogbookSampleWeights) {
        SpeciesListReference oldValue = this.speciesListForLogbookSampleWeights;
        this.speciesListForLogbookSampleWeights = speciesListForLogbookSampleWeights;
        firePropertyChange("speciesListForLogbookSampleWeights", oldValue, speciesListForLogbookSampleWeights);
    }

    public String getSpeciesListForLogbookSampleWeightsId() {
        return speciesListForLogbookSampleWeights == null ? null : speciesListForLogbookSampleWeights.getId();
    }

    public boolean isSkipForReport() {
        return skipForReport;
    }

    public void setSkipForReport(boolean skipForReport) {
        this.skipForReport = skipForReport;
    }
}
