/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.consolidate;

import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.config.ConfigUI;
import fr.ird.observe.client.datasource.actions.consolidate.actions.SelectConsolidationConfigFlag;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.dto.referential.common.SpeciesListReference;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class ConsolidateUIHandler extends AdminTabUIHandler<ConsolidateUI> implements UIHandler<ConsolidateUI> {
    private static final Logger log = LogManager.getLogger(ConsolidateUIHandler.class);

    @Override
    public void afterInit(ConsolidateUI ui) {
        super.afterInit(ui);
        setAutoStart(ui.getStart());
        setAutoNext(parentUI.getNextStepAction());
    }

    @Override
    protected void initConfig(ConfigUI configUI) {
        super.initConfig(configUI);
        configUI.getLeftSourceConfig().getSourceInfoLabel().setText(t("observe.ui.action.config.left.datasource.required.write.data"));
        ConsolidateConfigUI extraConfig = new ConsolidateConfigUI(UIHelper.initialContext(configUI, this));
        SelectConsolidationConfigFlag.init(extraConfig, extraConfig.getConsolidationFailIfLengthWeightParameterNotFound(), new SelectConsolidationConfigFlag("consolidationFailIfLengthWeightParameterNotFound", ConsolidateModel::isConsolidationFailIfLengthWeightParameterNotFound, ConsolidateModel::setConsolidationFailIfLengthWeightParameterNotFound, 5));
        SelectConsolidationConfigFlag.init(extraConfig, extraConfig.getConsolidationFailIfLengthLengthParameterNotFound(), new SelectConsolidationConfigFlag("consolidationFailIfLengthLengthParameterNotFound", ConsolidateModel::isConsolidationFailIfLengthLengthParameterNotFound, ConsolidateModel::setConsolidationFailIfLengthLengthParameterNotFound, 6));
        getModel().getConfigModel().getLeftSourceModel().addPropertyChangeListener(DataSourceSelectorModel.VALID_PROPERTY_NAME, evt -> onDataSourceValidChanged(extraConfig, (boolean) evt.getNewValue()));
        Decorator decorator = getDecoratorService().getDecoratorByType(SpeciesListReference.class);
        {
            FilterableComboBox<SpeciesListReference> editor = extraConfig.getSpeciesListForLogbookSampleActivityWeightedWeight();
            UIInitHelper.init(editor);
            editor.init(decorator, Collections.emptyList());
        }
        {
            FilterableComboBox<SpeciesListReference> editor = extraConfig.getSpeciesListForLogbookSampleWeights();
            UIInitHelper.init(editor);
            editor.init(decorator, Collections.emptyList());
        }
        configUI.getExtraConfig().add(extraConfig);
    }

    private void onDataSourceValidChanged(ConsolidateConfigUI extraConfig, boolean dataSourceValid) {
        log.info("Data source valid: {}", dataSourceValid);
        List<SpeciesListReference> data;
        if (dataSourceValid) {
            try (ObserveSwingDataSource dataSource = getModel().getConfigModel().getLeftSourceModel().getSafeSource(true)) {
                data = dataSource.getReferentialService().getReferenceSet(SpeciesListReference.class, null).toArrayList();
            }
        } else {
            data = Collections.emptyList();
        }
        initSpeciesList(extraConfig.getSpeciesListForLogbookSampleActivityWeightedWeight(),
                        dataSourceValid,
                        data,
                        () -> Optional.ofNullable(getModel().getConsolidateModel().getSpeciesListForLogbookSampleActivityWeightedWeightId()).orElse(getClientConfig().getConsolidationSpeciesListForLogbookSampleActivityWeightedWeight()),
                        ConsolidateModel::setSpeciesListForLogbookSampleActivityWeightedWeight);
        initSpeciesList(extraConfig.getSpeciesListForLogbookSampleWeights(),
                        dataSourceValid,
                        data,
                        () -> Optional.ofNullable(getModel().getConsolidateModel().getSpeciesListForLogbookSampleWeightsId()).orElse(getClientConfig().getConsolidationSpeciesListForLogbookSampleWeights()),
                        ConsolidateModel::setSpeciesListForLogbookSampleWeights);
    }

    private void initSpeciesList(FilterableComboBox<SpeciesListReference> editor,
                                 boolean dataSourceValid,
                                 List<SpeciesListReference> data,
                                 Supplier<String> speciesListIdSupplier,
                                 BiConsumer<ConsolidateModel, SpeciesListReference> speciesListConsumer) {
        editor.setData(data);
        editor.setEnabled(dataSourceValid);
        SpeciesListReference speciesListReference = null;
        if (dataSourceValid) {
            String speciesListId = speciesListIdSupplier.get();
            speciesListReference = data.stream().filter(sl -> Objects.equals(sl.getId(), speciesListId)).findFirst().orElse(null);
        }
        speciesListConsumer.accept(getModel().getConsolidateModel(), speciesListReference);
    }
}
