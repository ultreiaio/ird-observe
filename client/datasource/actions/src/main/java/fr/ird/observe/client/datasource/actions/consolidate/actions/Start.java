package fr.ird.observe.client.datasource.actions.consolidate.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.actions.AdminTabUIActionSupport;
import fr.ird.observe.client.datasource.actions.consolidate.ConsolidateModel;
import fr.ird.observe.client.datasource.actions.consolidate.ConsolidateUI;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.consolidation.data.ps.common.TripConsolidateRequest;
import fr.ird.observe.consolidation.data.ps.common.TripConsolidateResult;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeSpecializedRules;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.data.ps.localmarket.BatchDto;
import fr.ird.observe.dto.data.ps.logbook.SampleActivityDto;
import fr.ird.observe.dto.data.ps.logbook.SampleDto;
import fr.ird.observe.dto.data.ps.observation.CatchDto;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;
import fr.ird.observe.dto.data.ps.observation.SampleMeasureDto;
import fr.ird.observe.dto.data.ps.observation.SetDto;
import fr.ird.observe.dto.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.services.service.data.ps.ConsolidateDataService;
import io.ultreia.java4all.bean.monitor.JavaBeanPropertyModification;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.TimeLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static io.ultreia.java4all.i18n.I18n.t;

public class Start extends AdminTabUIActionSupport<ConsolidateUI> {

    private static final Logger log = LogManager.getLogger(Start.class);

    public Start() {
        super(null, null, "wizard-start", 'D');
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        setText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        setTooltipText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        super.defaultInit(inputMap, actionMap);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ConsolidateUI ui) {
        addAdminWorker(ui.getStart().getToolTipText(), this::doAction);
    }

    private WizardState doAction() {

        log.debug(this);

        ConsolidateModel stepModel = ui.getStepModel();
        AdminUIModel model = ui.getModel();
        if (stepModel.isSkipForReport()) {
            return WizardState.SUCCESSED;
        }
        stepModel.setSource(model.getConfigModel().getLeftSourceModel().getSafeSource(false));

        Set<String> tripIds = model.getSelectDataModel().getSelectionDataModel().getSelectedDataIds();

        int stepCount = tripIds.size() + 3;
        long t000 = TimeLog.getTime();
        ProgressionModel progressModel = stepModel.initProgressModel(stepCount);
        try (ObserveSwingDataSource dataSource = ObserveSwingDataSource.doOpenSource(stepModel.getSource())) {
            progressModel.increments();
            ConsolidateDataService consolidateDataService = dataSource.getPsConsolidateDataService();

            AtomicInteger index = new AtomicInteger();
            int nbTrips = tripIds.size();

            SimplifiedObjectTypeSpecializedRules simplifiedObjectTypeSpecializedRules = getClientConfig().getSimplifiedObjectTypeSpecializedRules();

            Set<TripConsolidateResult> results = new LinkedHashSet<>();
            long t00 = TimeLog.getTime();
            for (String tripId : tripIds) {
                long t0 = TimeLog.getTime();
                try {
                    consolidateTrip(tripId,
                                    index,
                                    nbTrips,
                                    stepModel,
                                    simplifiedObjectTypeSpecializedRules,
                                    consolidateDataService,
                                    progressModel,
                                    results);
                } finally {
                    ObserveUtil.cleanMemory();
                    String message = t("observe.ui.datasource.editor.actions.consolidate.end.trip", index, nbTrips, tripId, Strings.convertTime(TimeLog.getTime() - t0));
                    log.info(message);
                    sendMessage(message);
                    sendMessage("------------------------");
                }
            }
            progressModel.increments();
            if (results.isEmpty()) {
                sendMessage(t("observe.ui.datasource.editor.actions.consolidate.message.noChanges"));
            } else {
                sendMessage(t("observe.ui.datasource.editor.actions.consolidate.message.save.changes", results.size()));
                dataSource.setModified(true);
            }
            sendMessage(t("observe.ui.datasource.editor.actions.consolidate.message.operation.done", new Date(), Strings.convertTime(TimeLog.getTime() - t00)));
        }
        onEndAction(t000, stepCount, progressModel);
        return WizardState.SUCCESSED;
    }

    void consolidateTrip(String tripId,
                         AtomicInteger index,
                         int nbTrips,
                         ConsolidateModel stepModel,
                         SimplifiedObjectTypeSpecializedRules simplifiedObjectTypeSpecializedRules,
                         ConsolidateDataService consolidateDataService,
                         ProgressionModel progressModel,
                         Set<TripConsolidateResult> results) {
        progressModel.increments();

        if (stepModel.checkIfTripAlreadyProcessed(tripId)) {
            // this one has already been processed
            sendMessage(t("observe.ui.datasource.editor.actions.consolidate.skip.trip", index.incrementAndGet(), nbTrips, tripId));
            return;
        }
        sendMessage(t("observe.ui.datasource.editor.actions.consolidate.start.trip", index.incrementAndGet(), nbTrips, tripId));

        // mark trip processed
        stepModel.setTripProcessed(tripId);

        if (TripAware.isLonglineId(tripId)) {
            // can't consolidate LL trips...
            return;
        }
        TripConsolidateRequest request = createTripConsolidateRequest(tripId, stepModel);
        TripConsolidateResult result = consolidateDataService.consolidateTrip(simplifiedObjectTypeSpecializedRules, request);
        if (result != null) {
            printResult(result);
            if (result.withModifications()) {
                results.add(result);
            }
        }
    }

    private TripConsolidateRequest createTripConsolidateRequest(String tripId, ConsolidateModel stepModel) {
        TripConsolidateRequest request = new TripConsolidateRequest();
        request.setTripId(tripId);
        request.setFailIfLengthWeightParameterNotFound(stepModel.isConsolidationFailIfLengthWeightParameterNotFound());
        request.setFailIfLengthLengthParameterNotFound(stepModel.isConsolidationFailIfLengthLengthParameterNotFound());
        request.setSpeciesListForLogbookSampleActivityWeightedWeight(stepModel.getSpeciesListForLogbookSampleActivityWeightedWeightId());
        request.setSpeciesListForLogbookSampleWeights(stepModel.getSpeciesListForLogbookSampleWeightsId());
        return request;
    }

    void printResult(TripConsolidateResult result) {
        String tripSeineLabel = result.getTripLabel();
        if (result.withModifications()) {
            sendMessage(t("observe.ui.datasource.editor.actions.consolidate.message.trip.modification", result.getTripId(), tripSeineLabel));
        }
        if (result.withWarnings()) {
            sendMessage(t("observe.ui.datasource.editor.actions.consolidate.message.trip.warning", result.getTripId(), tripSeineLabel));
        }
        result.getActivityObservationResults().forEach(this::printResult);
        result.getActivityLogbookResults().forEach(this::printResult);
        result.getLogbookSampleResults().forEach(this::printResult);
        result.getLocalmarketBatchResults().forEach(batchResult -> printResult(BatchDto.class, batchResult));
    }

    void printResult(fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult activityResult) {
        if (activityResult.withModifications()) {
            sendMessage("  " + t("observe.ui.datasource.editor.actions.consolidate.message.observation.activity.modification", activityResult.getActivityId(), activityResult.getActivityLabel()));
        }
        if (activityResult.withWarnings()) {
            sendMessage("  " + t("observe.ui.datasource.editor.actions.consolidate.message.observation.activity.warning", activityResult.getActivityId(), activityResult.getActivityLabel()));
        }
        printResult(SetDto.class, activityResult.getSetModifications());
        activityResult.getFloatingObjectModifications().forEach(modification -> printResult(FloatingObjectDto.class, modification));
        activityResult.getCatchModifications().forEach(modification -> printResult(CatchDto.class, modification));
        activityResult.getSampleMeasureModifications().forEach(modification -> printResult(SampleMeasureDto.class, modification));
    }

    void printResult(fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateResult activityResult) {
        if (activityResult.withModifications()) {
            sendMessage("  " + t("observe.ui.datasource.editor.actions.consolidate.message.logbook.activity.modification", activityResult.getActivityId(), activityResult.getActivityLabel()));
        }
        if (activityResult.withWarnings()) {
            sendMessage("  " + t("observe.ui.datasource.editor.actions.consolidate.message.logbook.activity.warning", activityResult.getActivityId(), activityResult.getActivityLabel()));
        }
        activityResult.getFloatingObjectModifications().forEach(modification -> printResult(FloatingObjectDto.class, modification));
    }

    void printResult(fr.ird.observe.consolidation.data.ps.logbook.SampleConsolidateResult sampleResult) {
        if (sampleResult.withModifications()) {
            sendMessage("  " + t("observe.ui.datasource.editor.actions.consolidate.message.logbook.sample.modification", sampleResult.getSampleId(), sampleResult.getSampleLabel()));
        }
        if (sampleResult.withWarnings()) {
            sendMessage("  " + t("observe.ui.datasource.editor.actions.consolidate.message.logbook.sample.warning", sampleResult.getSampleId(), sampleResult.getSampleLabel()));
        }
        printResult(SampleDto.class, sampleResult.getModifications());
        sampleResult.getSampleActivityModifications().forEach(modification -> printResult(SampleActivityDto.class, modification));
    }

    private void printResult(Class<? extends BusinessDto> dataType, ToolkitIdModifications modifications) {
        if (modifications == null) {
            return;
        }
        String type = I18n.t(ObserveI18nDecoratorHelper.getTypeKey(dataType));
        if (modifications.withModifications()) {
            sendMessage("    " + t("observe.ui.datasource.editor.actions.consolidate.message.modifications.modification", modifications.modificationsCount(), type, modifications.getId(), modifications.getLabel()));
            for (JavaBeanPropertyModification modification : modifications.getModifications()) {
                String property = I18n.t(ObserveI18nDecoratorHelper.getPropertyI18nKey(dataType, modification.getPropertyName()));
                String message = I18n.t("observe.ui.datasource.editor.actions.consolidate.message.modifications.property",
                                        property,
                                        AtomicConsolidateAction.decorateValue(modification.getOldValue()),
                                        AtomicConsolidateAction.decorateValue(modification.getNewValue()));
                sendMessage("      - " + message);
            }
        }
        if (modifications.withWarnings()) {
            sendMessage("    " + t("observe.ui.datasource.editor.actions.consolidate.message.modifications.warning", modifications.warningsCount(), type, modifications.getId(), modifications.getLabel()));
            for (String warning : modifications.getWarnings()) {
                sendMessage("      - " + warning);
            }
        }
    }
}
