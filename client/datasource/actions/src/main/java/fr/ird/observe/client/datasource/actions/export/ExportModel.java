/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.export;

import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.data.CopyDataTask;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.util.ArrayList;
import java.util.List;

/**
 * Le modèle d'une opération d'export de données observers.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ExportModel extends AdminActionModel {

    /**
     * Index of data to export.
     */
    protected int[] exportDataSelectedIndex;
    /**
     * Data we can export.
     */
    protected List<CopyDataTask> data;
    protected ObserveSwingDataSource source;
    protected ObserveSwingDataSource centralSource;

    public ExportModel() {
        super(AdminStep.EXPORT_DATA);
    }

    public void start(AdminUIModel uiModel) {
        DataSourceSelectorModel leftSourceModel = uiModel.getConfigModel().getLeftSourceModel();
        leftSourceModel.setRequiredReadOnReferential(true);
        leftSourceModel.setRequiredReadOnData(true);

        DataSourceSelectorModel rightSourceModel = uiModel.getConfigModel().getRightSourceModel();
        rightSourceModel.setRequiredReadOnReferential(true);
        rightSourceModel.setRequiredReadOnData(true);
        rightSourceModel.setRequiredWriteOnData(true);
    }

    @Override
    public boolean validate(AdminUIModel uiModel) {
        return uiModel.validate(AdminStep.SELECT_DATA) && uiModel.getStepState(step) == WizardState.SUCCESSED;
    }

    @Override
    public void destroy() {
        super.destroy();
        exportDataSelectedIndex = null;
        data = null;
        centralSource = null;
        source = null;
    }

    public List<CopyDataTask> getData() {
        return data;
    }

    public void setData(List<CopyDataTask> data) {
        this.data = data;
    }

    public void setExportDataSelectedIndex(int... exportDataSelectedIndex) {
        this.exportDataSelectedIndex = exportDataSelectedIndex;
    }

    public ObserveSwingDataSource getSource() {
        return source;
    }

    public void setSource(ObserveSwingDataSource source) {
        this.source = source;
    }

    public ObserveSwingDataSource getCentralSource() {
        return centralSource;
    }

    public void setCentralSource(ObserveSwingDataSource centralSource) {
        this.centralSource = centralSource;
    }

    public List<CopyDataTask> getTasks() {
        List<CopyDataTask> entries = new ArrayList<>();
        for (int index : exportDataSelectedIndex) {
            entries.add(data.get(index));
        }
        return entries;
    }
}
