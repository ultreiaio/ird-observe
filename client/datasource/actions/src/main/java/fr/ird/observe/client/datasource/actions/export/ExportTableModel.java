/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.export;

import fr.ird.observe.client.datasource.api.data.CopyDataTask;
import fr.ird.observe.dto.ToolkitIdLabel;

import javax.swing.table.AbstractTableModel;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ExportTableModel extends AbstractTableModel {

    protected static final Class<?>[] COLUMN_CLASSES = {
            Boolean.class,
            String.class,  // data prefix
            ToolkitIdLabel.class,  // data
            Boolean.class
    };

    private static final long serialVersionUID = 1L;
    protected final Set<Integer> selected;
    protected CopyDataTask[] data;
    protected boolean selectAll;

    public ExportTableModel() {
        selected = new HashSet<>();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        // can only edit first column (select or not data)
        return columnIndex == 0;
    }

    public int[] getSelected() {
        int[] result = new int[selected.size()];
        int i = 0;
        for (Integer index : selected) {
            result[i++] = index;
        }
        return result;
    }

    public boolean hasSelection() {
        return !selected.isEmpty();
    }

    /**
     * Initialise le modèle.
     *
     * @param data les entrées du modèles (program-maree-existe à distance)
     */
    public void init(List<CopyDataTask> data) {

        this.data = data.toArray(new CopyDataTask[0]);

        selected.clear();
        // par defaut, on selectionne toutes les references
        setSelectAll(true);
    }

    public void clear() {
        data = null;
    }

    @Override
    public int getRowCount() {
        return data == null ? 0 : data.length;
    }

    @Override
    public int getColumnCount() {
        return COLUMN_CLASSES.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        CopyDataTask task = data == null ? null : data[rowIndex];
        if (task != null) {
            switch (columnIndex) {
                case 0:
                    value = selected.contains(rowIndex);
                    break;
                case 1:
                    value = task.getPrefix();
                    break;
                case 2:
                    value = task.getData();
                    break;
                case 3:
                    value = task.isDataExistOnOpposite();
                    break;
                default:
                    throw new IllegalStateException("can not get value for row " + rowIndex + ", col " + columnIndex);
            }
        }
        return value;
    }

    public boolean isSelectAll() {
        return selectAll;
    }

    public void setSelectAll(boolean selectAll) {
        this.selectAll = selectAll;
        if (selectAll) {
            for (int i = 0, max = getRowCount(); i < max; i++) {
                selected.add(i);
            }
        } else {
            selected.clear();
        }
        fireTableDataChanged();
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            Boolean value = (Boolean) aValue;
            if (value) {
                selected.add(rowIndex);
                if (selected.size() == getRowCount()) {
                    selectAll = true;
                }
            } else {
                selected.remove(rowIndex);
                if (selected.isEmpty()) {
                    selectAll = false;
                }
            }
            fireTableCellUpdated(rowIndex, columnIndex);
        }

        // no edit for others columns
    }

}
