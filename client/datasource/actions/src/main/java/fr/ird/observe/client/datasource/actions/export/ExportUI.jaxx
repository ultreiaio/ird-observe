<!--
  #%L
  ObServe Client :: DataSource :: Actions
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.actions.AdminTabUI>
  <import>
    fr.ird.observe.client.datasource.actions.AdminUI
    fr.ird.observe.client.datasource.actions.AdminStep
  </import>

  <ExportModel id='stepModel' initializer='getModel().getExportModel()'/>
  <ExportTableModel id='tableModel' onTableChanged='start.setEnabled(tableModel.hasSelection())'/>

  <script><![CDATA[
public static ExportUI get(AdminUI ui) {
    return get(ui, AdminStep.EXPORT_DATA);
}

@Override
public void destroy() {
    super.destroy();
    tableModel.clear();
}
]]>
  </script>

  <JPanel id='PENDING_content' layout="{new BorderLayout()}">
    <Table constraints='BorderLayout.CENTER' fill='both' weightx='1' weighty='1'>
      <row>
        <cell>
          <JButton id="prepare"/>
        </cell>
      </row>
    </Table>
  </JPanel>

  <JPanel id='NEED_FIX_content' layout="{new BorderLayout()}">
    <JScrollPane id='tablePane' constraints='BorderLayout.CENTER'>
      <JTable id='table'/>
    </JScrollPane>
    <JPanel constraints='BorderLayout.SOUTH' layout='{new BorderLayout()}'>
      <JButton id='start' constraints='BorderLayout.CENTER'/>
    </JPanel>
  </JPanel>

</fr.ird.observe.client.datasource.actions.AdminTabUI>
