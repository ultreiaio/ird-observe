/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.export;

import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.config.ConfigUI;
import fr.ird.observe.client.util.UIHelper;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.editor.MyDefaultCellEditor;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ExportUIHandler extends AdminTabUIHandler<ExportUI> implements UIHandler<ExportUI> {

    @Override
    public void afterInit(ExportUI ui) {
        super.afterInit(ui);
        setAutoStart(ui.getPrepare());
        final JTable table4 = ui.getTable();
        table4.setRowHeight(24);

        UIHelper.fixTableColumnWidth(table4, 0, 20);
        UIHelper.fixTableColumnWidth(table4, 3, 20);

        DefaultTableCellRenderer renderer5 = new DefaultTableCellRenderer();

        UIHelper.setI18nTableHeaderRenderer(
                table4,
                n("observe.ui.datasource.editor.actions.exportData.table.selected"),
                n("observe.ui.datasource.editor.actions.exportData.table.selected.tip"),
                n("observe.ui.datasource.editor.actions.exportData.table.program.label"),
                n("observe.ui.datasource.editor.actions.exportData.table.program.label.tip"),
                n("observe.ui.datasource.editor.actions.exportData.table.trip.label"),
                n("observe.ui.datasource.editor.actions.exportData.table.trip.label.tip"),
                n("observe.ui.datasource.editor.actions.exportData.table.exist.label"),
                n("observe.ui.datasource.editor.actions.exportData.table.exist.label.tip"));

        UIHelper.setTableColumnRenderer(table4, 0, UIHelper.newBooleanTableCellRenderer(renderer5));
        UIHelper.setTableColumnRenderer(table4, 1, renderer5);
        UIHelper.setTableColumnRenderer(table4, 2, renderer5);
        UIHelper.setTableColumnRenderer(table4, 3, UIHelper.newBooleanTableCellRenderer(renderer5));
        UIHelper.setTableColumnEditor(table4, 0, MyDefaultCellEditor.newBooleanEditor(false));

        // to select all -unselect all from table header
        table4.getTableHeader().addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                int colIndex = table4.getTableHeader().columnAtPoint(e.getPoint());
                colIndex = table4.convertColumnIndexToModel(colIndex);
                if (colIndex == 0) {
                    ExportTableModel model = (ExportTableModel) table4.getModel();
                    boolean oldValue = model.isSelectAll();
                    // toggle selectAll
                    model.setSelectAll(!oldValue);
                }
            }
        });
    }

    @Override
    protected void initConfig(ConfigUI configUI) {
        super.initConfig(configUI);
        configUI.getRightSourceConfig().getSourceInfoLabel().setText(t("observe.ui.action.config.export.required.write.data"));
        configUI.getLeftSourceModel().setSourceLabel(t("observe.ui.datasource.storage.config.exportSource.storage"));
        configUI.getRightSourceModel().setSourceLabel(t("observe.ui.datasource.storage.config.exportTarget.storage"));
    }

    @Override
    protected void onStateChangeToNeedFix() {
        super.onStateChangeToNeedFix();
        // update table model
        ui.tableModel.init(ui.getStepModel().getData());
    }

}
