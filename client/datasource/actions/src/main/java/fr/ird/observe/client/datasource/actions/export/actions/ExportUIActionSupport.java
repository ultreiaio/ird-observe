package fr.ird.observe.client.datasource.actions.export.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.actions.AdminTabUIActionSupport;
import fr.ird.observe.client.datasource.actions.export.ExportUI;

import javax.swing.KeyStroke;

public abstract class ExportUIActionSupport extends AdminTabUIActionSupport<ExportUI> {
    public ExportUIActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    public ExportUIActionSupport(String actionCommandKey, String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(actionCommandKey, label, shortDescription, actionIcon, acceleratorKey);
    }

    public ExportUIActionSupport(String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    public ExportUIActionSupport(String actionCommandKey, String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(actionCommandKey, label, shortDescription, actionIcon, acceleratorKey);
    }
}
