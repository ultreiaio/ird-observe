package fr.ird.observe.client.datasource.actions.export.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.config.ConfigModel;
import fr.ird.observe.client.datasource.actions.export.ExportModel;
import fr.ird.observe.client.datasource.actions.export.ExportUI;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.data.CopyDataTask;
import fr.ird.observe.client.datasource.api.data.TaskSide;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

public class Prepare extends ExportUIActionSupport {

    public Prepare() {
        super(null, null, "wizard-start", 'R');
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        setText(t("observe.ui.datasource.editor.actions.synchro.prepare.operation", t(ui.getStep().getOperationLabel())));
        setTooltipText(t("observe.ui.datasource.editor.actions.synchro.prepare.operation", t(ui.getStep().getOperationLabel())));
        super.defaultInit(inputMap, actionMap);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ExportUI ui) {
        addAdminWorker(ui.getPrepare().getToolTipText(), this::doPrepareAction0);
    }

    private WizardState doPrepareAction0() {

        AdminUIModel model = ui.getModel();
        List<CopyDataTask> data = CopyDataTask.of(TaskSide.FROM_LEFT, model.getSelectDataModel().getSelectionDataModel()).collect(Collectors.toList());

        ExportModel stepModel = model.getExportModel();
        if (data.isEmpty()) {
            throw new IllegalStateException("Can't export if no data selected");
        }

        ConfigModel configModel = model.getConfigModel();
        ObserveSwingDataSource targetSource = configModel.getRightSourceModel().getSafeSourceNotOpened();
        ObserveSwingDataSource incomingSource = configModel.getLeftSourceModel().getSafeSourceNotOpened();

        stepModel.setData(data);
        stepModel.setCentralSource(targetSource);
        stepModel.setSource(incomingSource);
        sendMessage(t("observe.ui.datasource.editor.actions.exportData.message.prepare.data"));
        sendMessage(t("observe.ui.datasource.editor.actions.exportData.message.operation.needFix", new Date()));
        ui.getModel().setWasDone(true);
        // user can now select real data to export
        return WizardState.NEED_FIX;
    }
}
