package fr.ird.observe.client.datasource.actions.export.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.export.ExportModel;
import fr.ird.observe.client.datasource.actions.export.ExportUI;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.data.CopyDataTask;
import fr.ird.observe.client.datasource.api.data.DataManager;
import fr.ird.observe.dto.ProgressionModel;
import io.ultreia.java4all.util.TimeLog;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.t;

public class Start extends ExportUIActionSupport {

    public Start() {
        super(null, null, "save", 'S');
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        setText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        setTooltipText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        super.defaultInit(inputMap, actionMap);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ExportUI ui) {
        int[] rows = ui.getTableModel().getSelected();
        ui.getModel().getExportModel().setExportDataSelectedIndex(rows);
        addAdminWorker(ui.getStart().getToolTipText(), this::doStartAction);
    }

    private WizardState doStartAction() {

        AdminUIModel model = ui.getModel();
        ExportModel stepModel = model.getExportModel();
        List<CopyDataTask> tasks = stepModel.getTasks();
        if (tasks.isEmpty()) {
            throw new IllegalStateException("Can't export no trip...");
        }

        long t00 = TimeLog.getTime();
        int stepCount = 4 + tasks.stream().mapToInt(CopyDataTask::stepCount).sum();
        ProgressionModel progressModel = stepModel.initProgressModel(stepCount);
        try (ObserveSwingDataSource localDataSource = ObserveSwingDataSource.doOpenSource(stepModel.getSource())) {
            progressModel.increments();
            try (ObserveSwingDataSource centralDataSource = ObserveSwingDataSource.doOpenSource(stepModel.getCentralSource())) {
                progressModel.increments();
                DataManager dataManager = new DataManager(progressModel, localDataSource, centralDataSource);
                dataManager.consume(tasks);
            }
            progressModel.increments();
        }
        onEndAction(t00, stepCount, progressModel);
        return WizardState.SUCCESSED;
    }
}
