package fr.ird.observe.client.datasource.actions.pairing;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.pairing.tree.PairingTreeTableModelSupport;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.util.List;

/**
 * Created by tchemit on 20/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ActivityPairingModel extends AdminActionModel {

    private static final Logger log = LogManager.getLogger(ActivityPairingModel.class);
    private PairingTreeTableModelSupport<?, ?, ?> treeTableModel;
    private ObserveSwingDataSource source;
    private boolean modified;

    public ActivityPairingModel() {
        super(AdminStep.ACTIVITY_PAIRING);
    }

    @Override
    public void start(AdminUIModel uiModel) {
        addPropertyChangeListener(evt -> {
            ActivityPairingModel source = (ActivityPairingModel) evt.getSource();
            log.debug(String.format("activities longline pairing model [%s] changed on %s, new value = %s", source, evt.getPropertyName(), evt.getNewValue()));
            uiModel.validate();
        });
        DataSourceSelectorModel sourceModel = uiModel.getConfigModel().getLeftSourceModel();
        sourceModel.setRequiredReadOnReferential(true);
        sourceModel.setRequiredReadOnData(true);
        sourceModel.setRequiredWriteOnData(true);
    }

    @Override
    public boolean validate(AdminUIModel uiModel) {
        return uiModel.validate(AdminStep.SELECT_DATA) && uiModel.getStepState(step) == WizardState.SUCCESSED;
    }

    @Override
    public void destroy() {
        super.destroy();
        source = null;
    }

    public ObserveSwingDataSource getSource() {
        return source;
    }

    public void setSource(ObserveSwingDataSource source) {
        this.source = source;
    }

    public PairingTreeTableModelSupport<?, ?, ?> getTreeTableModel() {
        return treeTableModel;
    }

    public void setTreeTableModel(PairingTreeTableModelSupport<?, ?, ?> treeTableModel) {
        this.treeTableModel = treeTableModel;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        boolean oldValue = isModified();
        this.modified = modified;
        firePropertyChange("modified", oldValue, modified);
    }

    public int getSelectedCount() {
        return treeTableModel == null ? 0 : treeTableModel.getSelectedCount();
    }

    @SuppressWarnings("unused")
    public void setSelectedCount(int selectedCount) {
        // not used
    }

    public void recomputeSelectedValues() {
        PairingTreeTableModelSupport<?, ?, ?> treeTableModel = getTreeTableModel();
        if (treeTableModel != null) {
            List<?> oldValue = treeTableModel.getSelectedNodes();
            int oldCountValue = treeTableModel.getSelectedCount();

            treeTableModel.recomputeSelectedValues();
            int selectedCount = treeTableModel.getSelectedCount();
            List<?> selectedNodes = treeTableModel.getSelectedNodes();
            firePropertyChange("selectedNodes", oldValue, selectedNodes);
            firePropertyChange("selectedCount", oldCountValue, selectedCount);
            log.debug("Selected count: " + getSelectedCount());
            setModified(true);
        }
    }
}
