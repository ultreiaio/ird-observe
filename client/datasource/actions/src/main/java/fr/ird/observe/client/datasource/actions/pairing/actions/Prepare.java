package fr.ird.observe.client.datasource.actions.pairing.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.config.SelectDataModel;
import fr.ird.observe.client.datasource.actions.pairing.ActivityPairingModel;
import fr.ird.observe.client.datasource.actions.pairing.ActivityPairingUI;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.ToolkitIdLabel;
import io.ultreia.java4all.util.TimeLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static io.ultreia.java4all.i18n.I18n.t;

public class Prepare extends ActivityPairingUIActionSupport {
    private static final Logger log = LogManager.getLogger(Prepare.class);

    public Prepare() {
        super(null, null, "wizard-start", 'R');
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        setText(t("observe.ui.datasource.editor.actions.synchro.prepare.operation", t(ui.getStep().getOperationLabel())));
        setTooltipText(t("observe.ui.datasource.editor.actions.synchro.prepare.operation", t(ui.getStep().getOperationLabel())));
        super.defaultInit(inputMap, actionMap);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ActivityPairingUI ui) {
        addAdminWorker(ui.getPrepare().getToolTipText(), this::doPrepareAction0);
    }

    private WizardState doPrepareAction0() {
        log.debug(this);
        long t00 = TimeLog.getTime();
        ActivityPairingModel stepModel = ui.getStepModel();
        AdminUIModel model = ui.getModel();
        stepModel.setSource(model.getConfigModel().getLeftSourceModel().getSafeSource(false));

        SelectDataModel selectDataModel = model.getSelectDataModel();
        Map<String, List<ToolkitIdLabel>> selectedDataByProgram = selectDataModel.getSelectionDataModel().getSelectedDataByParent();

        int stepCount = 3 + selectedDataByProgram.values().stream().mapToInt(Collection::size).sum();
        ProgressionModel progressModel = stepModel.initProgressModel(stepCount);
        try (ObserveSwingDataSource source = ObserveSwingDataSource.doOpenSource(stepModel.getSource())) {
            progressModel.increments();
            String modelType = selectDataModel.getTreeConfig().getModuleName();
            switch (modelType) {
                case "ps": {
                    fr.ird.observe.client.datasource.actions.pairing.ps.node.RootNode rootNode = fr.ird.observe.client.datasource.actions.pairing.ps.RootNodeBuilder.create(this, source.getPsActivityPairingService(), progressModel, selectedDataByProgram, getDecoratorService());
                    new fr.ird.observe.client.datasource.actions.pairing.ps.PairingTreeTable(rootNode).initTable(stepModel, ui.getTableScroll());
                }
                break;
                case "ll": {
                    fr.ird.observe.client.datasource.actions.pairing.ll.node.RootNode rootNode = fr.ird.observe.client.datasource.actions.pairing.ll.RootNodeBuilder.create(this, source.getLlActivityPairingService(), progressModel, selectedDataByProgram, getDecoratorService());
                    new fr.ird.observe.client.datasource.actions.pairing.ll.PairingTreeTable(rootNode).initTable(stepModel, ui.getTableScroll());
                }
                break;
            }
            progressModel.increments();
        }
        stepModel.recomputeSelectedValues();
        onEndAction(t00, stepCount, progressModel);
        return WizardState.NEED_FIX;
    }
}
