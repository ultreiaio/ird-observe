package fr.ird.observe.client.datasource.actions.pairing.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.pairing.ActivityPairingModel;
import fr.ird.observe.client.datasource.actions.pairing.ActivityPairingUI;
import fr.ird.observe.client.datasource.actions.pairing.tree.PairingTreeTableModelSupport;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.data.pairing.ApplyPairingRequest;
import fr.ird.observe.services.service.data.ActivityPairingService;
import io.ultreia.java4all.util.TimeLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;
import java.util.Date;

import static io.ultreia.java4all.i18n.I18n.t;

public class Start extends ActivityPairingUIActionSupport {

    private static final Logger log = LogManager.getLogger(Start.class);

    public Start() {
        super(null, null, "save", 'S');
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        setText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        setTooltipText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        super.defaultInit(inputMap, actionMap);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ActivityPairingUI ui) {
        addAdminWorker(ui.getStart().getToolTipText(), this::doStartAction0);
    }

    private WizardState doStartAction0() {
        log.debug(this);
        long t00 = TimeLog.getTime();
        int stepCount = 3;
        ActivityPairingModel stepModel = ui.getStepModel();
        PairingTreeTableModelSupport<?, ?, ?> treeTableModel = stepModel.getTreeTableModel();
        ApplyPairingRequest request = treeTableModel.computeRequest();

        ProgressionModel progressModel = stepModel.initProgressModel(stepCount);

        try (ObserveSwingDataSource dataSource = ObserveSwingDataSource.doOpenSource(stepModel.getSource())) {
            progressModel.increments();
            ActivityPairingService<?, ?, ?> service = treeTableModel.getService(dataSource);
            service.applyPairing(request);
            log.info(sendMessage(t("observe.ui.datasource.editor.actions.pairing.message.apply", request.size())));
            progressModel.increments();
            dataSource.setModified(true);
            log.info(sendMessage(t("observe.ui.datasource.editor.actions.pairing.message.operation.done", new Date())));
        }
        onEndAction(t00, stepCount, progressModel);
        return WizardState.SUCCESSED;
    }
}
