package fr.ird.observe.client.datasource.actions.pairing.ll;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.pairing.ll.node.ActivityNode;
import fr.ird.observe.client.datasource.actions.pairing.ll.node.RootNode;
import fr.ird.observe.client.datasource.actions.pairing.tree.PairingTreeTableModelSupport;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.data.ll.pairing.ActivityPairingResult;
import fr.ird.observe.dto.data.ll.pairing.ActivityPairingResultItem;
import fr.ird.observe.services.service.data.ll.ActivityPairingService;

import java.util.Comparator;
import java.util.Objects;

/**
 * Created by tchemit on 20/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class PairingTreeTableModel extends PairingTreeTableModelSupport<ActivityPairingResultItem, ActivityNode, RootNode> {

    public PairingTreeTableModel(RootNode rootNode) {
        super(Objects.requireNonNull(rootNode));
    }

    @Override
    public ActivityPairingService getService(ObserveSwingDataSource source) {
        return source.getLlActivityPairingService();
    }

    @Override
    public Comparator<ActivityPairingResultItem> getComparator() {
        return ActivityPairingResult.COMPARATOR;
    }
}
