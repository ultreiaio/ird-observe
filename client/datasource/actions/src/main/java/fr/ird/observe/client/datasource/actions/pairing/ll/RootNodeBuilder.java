package fr.ird.observe.client.datasource.actions.pairing.ll;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.pairing.actions.Prepare;
import fr.ird.observe.client.datasource.actions.pairing.ll.node.ActivityNode;
import fr.ird.observe.client.datasource.actions.pairing.ll.node.RootNode;
import fr.ird.observe.client.datasource.actions.pairing.ll.node.TripNode;
import fr.ird.observe.client.datasource.actions.pairing.tree.node.GroupByNode;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.ll.logbook.ActivityReference;
import fr.ird.observe.dto.data.ll.pairing.ActivityPairingResult;
import fr.ird.observe.dto.data.ll.pairing.ActivityPairingResultItem;
import fr.ird.observe.dto.data.ll.pairing.TripActivitiesPairingResult;
import fr.ird.observe.services.service.data.ll.ActivityPairingService;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 21/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class RootNodeBuilder {
    private static final Logger log = LogManager.getLogger(RootNodeBuilder.class);

    private final RootNode rootNode;
    private final Decorator activityLogbookDecorator;
    private final Decorator activityObservationDecorator;
    private final Decorator itemDecorator;
    private GroupByNode groupByNode;
    private TripNode tripNode;

    public static RootNode create(Prepare action,
                                  ActivityPairingService service,
                                  ProgressionModel progressModel,
                                  Map<String, List<ToolkitIdLabel>> selectedDataByProgram,
                                  DecoratorService decoratorService) {
        //FIXME Build the result tree model from the selectionModel
        //FIXME In that way, we just follow the same structure, no need to create new type of nodes :)
        //FIXME Introduce new states for SelectionTreeNodeBean
        RootNodeBuilder builder = new RootNodeBuilder(decoratorService);

        int nbProgram = selectedDataByProgram.size();
        int programIndex = 0;

        log.info(action.sendMessage(""));
        log.info(action.sendMessage(String.format("Found %d groupBy to process.", nbProgram)));

        for (Map.Entry<String, List<ToolkitIdLabel>> entry : selectedDataByProgram.entrySet()) {
            String groupBy = entry.getKey();
            builder.addGroupBy(groupBy);

            List<ToolkitIdLabel> trips = entry.getValue();
            int nbTrip = trips.size();
            long t0 = System.nanoTime();
            log.info(action.sendMessage(String.format("[GroupBy %d/%d] Computing activity pairing for groupBy: %s (%d trips)", ++programIndex, nbProgram, groupBy, nbTrip)));

            int tripIndex = 0;
            for (ToolkitIdLabel trip : trips) {
                long t1 = System.nanoTime();
                log.info(action.sendMessage(String.format("[GroupBy %d/%d - Trip %d/%d] Computing activity pairing for trip: %s", programIndex, nbProgram, ++tripIndex, nbTrip, trip)));
                TripActivitiesPairingResult tripActivitiesPairingResult = service.computePairing(trip.getTopiaId());
                boolean withResult = tripActivitiesPairingResult != null;
                String duration = Strings.convertTime(t1, System.nanoTime());
                log.info(action.sendMessage(String.format("[GroupBy %d/%d - Trip %d/%d] Found %d logbook activities (duration: %s).", programIndex, nbProgram, tripIndex, nbTrip, withResult? tripActivitiesPairingResult.getItems().size():0, duration)));
                if (withResult) {
                    builder.addTrip(trip, tripActivitiesPairingResult);
                }

                progressModel.increments();
            }
            String duration = Strings.convertTime(t0, System.nanoTime());
            log.info(action.sendMessage(String.format("[GroupBy %d/%d] Computing activity pairing for groupBy: %s (%d trips) done (duration: %s)", programIndex, nbProgram, groupBy, nbTrip, duration)));
        }
        return builder.build();
    }

    public RootNodeBuilder(DecoratorService decoratorService) {
        activityLogbookDecorator = decoratorService.getDecoratorByType(ActivityReference.class);
        activityObservationDecorator = decoratorService.getDecoratorByType(fr.ird.observe.dto.data.ll.observation.ActivityReference.class);
        itemDecorator = decoratorService.getDecoratorByType(ActivityPairingResultItem.class);
        rootNode = new RootNode();
    }

    public RootNode build() {
        flushGroupByNode();
        return rootNode;
    }

    public void addGroupBy(String groupBy) {
        flushGroupByNode();
        groupByNode = new GroupByNode(groupBy);
    }

    public void addTrip(ToolkitIdLabel trip, TripActivitiesPairingResult tripActivitiesPairingResult) {
        flushTripNode();
        tripNode = new TripNode(trip);
        for (ActivityPairingResult activityPairingResult : Objects.requireNonNull(tripActivitiesPairingResult).getItems()) {
            addActivity(activityPairingResult);
        }
    }

    public void addActivity(ActivityPairingResult activityPairingResult) {
        decorate(activityPairingResult);
        if (!activityPairingResult.getItems().isEmpty() || activityPairingResult.getSelectedRelatedObservedActivity() != null) {
            tripNode.add(new ActivityNode(activityPairingResult));
        }
    }

    protected void flushGroupByNode() {
        if (groupByNode != null) {
            flushTripNode();
            if (groupByNode.getChildCount() > 0) {
                rootNode.add(groupByNode);
            }
            groupByNode = null;
        }
    }

    protected void flushTripNode() {
        if (tripNode != null) {
            if (tripNode.getChildCount() > 0) {
                groupByNode.add(tripNode);
            }
            tripNode = null;
        }
    }

    protected void decorate(ActivityPairingResult activityPairingResult) {
        List<ActivityPairingResultItem> items = activityPairingResult.getItems();
        items.forEach(this::decorate);
        decorate(activityPairingResult.getSelectedRelatedObservedActivity());
        activityPairingResult.getActivityLogbook().registerDecorator(activityLogbookDecorator);
    }

    protected void decorate(ActivityPairingResultItem item) {
        if (item != null) {
            item.getObservationActivity().registerDecorator(activityObservationDecorator);
            item.registerDecorator(itemDecorator);
        }
    }
}
