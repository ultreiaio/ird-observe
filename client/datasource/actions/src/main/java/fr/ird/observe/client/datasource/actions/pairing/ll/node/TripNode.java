package fr.ird.observe.client.datasource.actions.pairing.ll.node;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.pairing.tree.node.NodeSupport;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.ll.common.TripDto;

import javax.swing.Icon;
import java.util.Objects;

/**
 * Created by tchemit on 20/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TripNode extends NodeSupport {
    private final static Icon ICON = DtoIconHelper.getIcon(TripDto.class);

    public TripNode(ToolkitIdLabel trip) {
        super(Objects.requireNonNull(trip), true);
    }

    @Override
    public Icon getIcon() {
        return ICON;
    }
}
