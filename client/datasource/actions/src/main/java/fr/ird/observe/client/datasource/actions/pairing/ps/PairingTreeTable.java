package fr.ird.observe.client.datasource.actions.pairing.ps;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.actions.pairing.ActivityPairingModel;
import fr.ird.observe.client.datasource.actions.pairing.ps.node.ActivityNode;
import fr.ird.observe.client.datasource.actions.pairing.ps.node.RootNode;
import fr.ird.observe.client.datasource.actions.pairing.tree.PairingTreeTableCellEditor;
import fr.ird.observe.client.datasource.actions.pairing.tree.PairingTreeTableCellRenderer;
import fr.ird.observe.client.datasource.actions.pairing.tree.PairingTreeTableSupport;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ps.pairing.ActivityPairingResultItem;

import javax.swing.JScrollPane;
import java.util.Objects;

/**
 * Created on 20/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class PairingTreeTable extends PairingTreeTableSupport<ActivityPairingResultItem, ActivityNode, RootNode, PairingTreeTableModel> implements WithClientUIContextApi {

    public PairingTreeTable(RootNode rootNode) {
        super(new PairingTreeTableModel(Objects.requireNonNull(rootNode)));
    }

    @Override
    public void initTable(ActivityPairingModel model, JScrollPane tableScroll) {
        DecoratorService decoratorService = getDecoratorService();
        setDefaultRenderer(Object.class, new PairingTreeTableCellRenderer<>(ActivityNode.class, this));
        setDefaultEditor(Object.class, new PairingTreeTableCellEditor<>(ActivityPairingResultItem.class, ActivityNode.class, this, decoratorService));
        super.initTable(model, tableScroll);
    }

}
