package fr.ird.observe.client.datasource.actions.pairing.ps.node;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.pairing.tree.node.ActivityNodeSupport;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.dto.data.ps.observation.ActivityDto;
import fr.ird.observe.dto.data.ps.pairing.ActivityPairingResult;
import fr.ird.observe.dto.data.ps.pairing.ActivityPairingResultItem;

import javax.swing.Icon;
import java.util.Objects;

/**
 * Created on 20/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ActivityNode extends ActivityNodeSupport<fr.ird.observe.dto.data.ps.logbook.ActivityReference, fr.ird.observe.dto.data.ps.observation.ActivityReference, ActivityPairingResultItem, ActivityPairingResult> {
    private final static Icon ICON = DtoIconHelper.getIcon(ActivityDto.class);

    public ActivityNode(ActivityPairingResult userObject) {
        super(Objects.requireNonNull(userObject));
    }

    @Override
    public Icon getIcon() {
        return ICON;
    }

}
