package fr.ird.observe.client.datasource.actions.pairing.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.pairing.tree.node.ActivityNodeSupport;
import fr.ird.observe.client.datasource.actions.pairing.tree.node.NodeSupport;
import fr.ird.observe.client.datasource.actions.pairing.tree.node.RootNodeSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.pairing.ActivityPairingResultItemSupport;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxCellEditor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTreeTable;

import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.util.EventObject;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 20/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class PairingTreeTableCellEditor<I extends ActivityPairingResultItemSupport<?>, S extends ActivityNodeSupport<?, ?, I, ?>, R extends RootNodeSupport<S>, M extends PairingTreeTableModelSupport<I, S, R>> implements TableCellEditor {
    private static final Logger log = LogManager.getLogger(PairingTreeTableCellEditor.class);

    private final TableCellEditor booleanEditor;
    private final FilterableComboBoxCellEditor pairingEditor;
    private final Set<TableCellEditor> editors;
    private final M model;
    private final Class<S> nodeType;
    private TableCellEditor editor;

    public PairingTreeTableCellEditor(Class<I> type, Class<S> nodeType, PairingTreeTableSupport<I, S, R, M> table, DecoratorService decoratorService) {
        this.nodeType = Objects.requireNonNull(nodeType);
        this.model = Objects.requireNonNull(table).getTreeTableModel();
        Decorator decorator = decoratorService.getDecoratorByType(Objects.requireNonNull(type));
        this.editors = new LinkedHashSet<>();
        editors.add(this.booleanEditor = table.getDefaultEditor(Boolean.class));
        editors.add(this.pairingEditor = UIHelper.newCellEditor(type, decorator));
    }

    @Override
    public final Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        NodeSupport node = (NodeSupport) ((JXTreeTable) table).getPathForRow(row).getLastPathComponent();
        Objects.requireNonNull(node);
        if (column == 1) {
            if (Objects.equals(nodeType, node.getClass())) {
                @SuppressWarnings("unchecked") FilterableComboBox<I> component = (FilterableComboBox<I>) pairingEditor.getComponent();
                @SuppressWarnings("unchecked") S editNode = (S) node;
                List<I> availableActivities = model.getAvailableActivities(editNode);
                I selectedValue = editNode.getSelectedValue();
                if (!availableActivities.contains(selectedValue)) {
                    selectedValue = null;
                }
                log.info(String.format("Will use %d candidate(s) (selected value? %s).", availableActivities.size(), selectedValue));
                component.setData(availableActivities);
                component.setSelectedItem(selectedValue);
                editor = pairingEditor;
            }
        } else if (column == 2) {
            editor = booleanEditor;
        } else {
            throw new IllegalStateException("Can't manage type: " + node);
        }
        return editor.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    @Override
    public final Object getCellEditorValue() {
        return editor == null ? null : editor.getCellEditorValue();
    }

    @Override
    public final boolean isCellEditable(EventObject anEvent) {
        return editors.stream().anyMatch(e -> e.isCellEditable(anEvent));
    }

    @Override
    public final boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public final boolean stopCellEditing() {
        return editor != null && editor.stopCellEditing();
    }

    @Override
    public final void cancelCellEditing() {
        if (editor != null) {
            editor.cancelCellEditing();
        }
    }

    @Override
    public final void addCellEditorListener(CellEditorListener l) {
        editors.forEach(e -> e.addCellEditorListener(l));
    }

    @Override
    public final void removeCellEditorListener(CellEditorListener l) {
        editors.forEach(e -> e.removeCellEditorListener(l));
    }

}

