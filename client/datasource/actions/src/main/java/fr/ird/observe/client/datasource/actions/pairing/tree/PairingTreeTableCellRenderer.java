package fr.ird.observe.client.datasource.actions.pairing.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.pairing.tree.node.ActivityNodeSupport;
import fr.ird.observe.client.datasource.actions.pairing.tree.node.NodeSupport;
import fr.ird.observe.client.datasource.actions.pairing.tree.node.RootNodeSupport;
import fr.ird.observe.dto.data.pairing.ActivityPairingResultItemSupport;
import org.jdesktop.swingx.JXTreeTable;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.util.Objects;

/**
 * Created on 20/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class PairingTreeTableCellRenderer<I extends ActivityPairingResultItemSupport<?>, S extends ActivityNodeSupport<?, ?, I, ?>, R extends RootNodeSupport<S>, M extends PairingTreeTableModelSupport<I, S, R>> implements TableCellRenderer {

    // Render anything else
    protected final TableCellRenderer objectRenderer;
    // Render booleans (inclusive)
    private final TableCellRenderer booleanInclusiveRenderer;
    private final Class<S> nodeType;

    public PairingTreeTableCellRenderer(Class<S> nodeType, PairingTreeTableSupport<I, S, R, M> table) {
        // register as a default renderer to get correct integration with ui
        this.booleanInclusiveRenderer = table.getDefaultRenderer(Boolean.class);
        this.objectRenderer = table.getDefaultRenderer(Object.class);
        this.nodeType = Objects.requireNonNull(nodeType);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        NodeSupport node = (NodeSupport) ((JXTreeTable) table).getPathForRow(row).getLastPathComponent();
        Objects.requireNonNull(node);
        switch (column) {
            case 0:
                return renderSelected(node, table, value, isSelected, hasFocus, row, column);
            case 1:
                Object newValue = value;
                if (Objects.equals(nodeType, node.getClass())) {
                    @SuppressWarnings("unchecked") S editNode = (S) node;
                    I selectedValue = editNode.getSelectedValue();
                    newValue = selectedValue == null ? "" : selectedValue.toString();
                }
                Component component = objectRenderer.getTableCellRendererComponent(table, newValue, isSelected, hasFocus, row, column);
                component.setEnabled(node.isSelected());
                return component;
            case 2:
                return renderBoolean(table, value, isSelected, hasFocus, row, column);
            default:
                throw new IllegalStateException("Can't happen");
        }
    }

    protected Component renderSelected(NodeSupport node, JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Object newValue = value == null ? null : Boolean.valueOf(String.valueOf(value));
        Component component = objectRenderer.getTableCellRendererComponent(table, newValue, isSelected, hasFocus, row, column);
        component.setEnabled(node.isSelected());
        return component;
    }

    protected Component renderBoolean(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Object newValue = value == null ? null : Boolean.valueOf(String.valueOf(value));
        Component component = booleanInclusiveRenderer.getTableCellRendererComponent(table, newValue, isSelected, hasFocus, row, column);
        component.setEnabled(true);
        return component;
    }
}
