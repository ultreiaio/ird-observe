package fr.ird.observe.client.datasource.actions.pairing.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.pairing.tree.node.ActivityNodeSupport;
import fr.ird.observe.client.datasource.actions.pairing.tree.node.RootNodeSupport;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.data.pairing.ActivityPairingResultItemSupport;
import fr.ird.observe.dto.data.pairing.ApplyPairingRequest;
import fr.ird.observe.services.service.data.ActivityPairingService;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created on 20/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class PairingTreeTableModelSupport<I extends ActivityPairingResultItemSupport<?>, S extends ActivityNodeSupport<?, ?, I, ?>, R extends RootNodeSupport<S>> extends DefaultTreeTableModel {

    private final int columnCount;
    private List<S> selectedNodes;
    private boolean selectAll = true;

    public PairingTreeTableModelSupport(R rootNode) {
        super(Objects.requireNonNull(rootNode));
        this.columnCount = 3;
    }

    @Override
    public int getColumnCount() {
        return columnCount;
    }

    public abstract ActivityPairingService<?, ?, ?> getService(ObserveSwingDataSource source);

    public abstract Comparator<I> getComparator();

    public List<I> getAvailableActivities(S selectedNode) {
        List<S> selectedNodes1 = getSelectedNodes();
        List<S> selectedNodes = new LinkedList<>();
        if (selectedNodes1 != null) {
            selectedNodes.addAll(selectedNodes1);
        }
        selectedNodes.remove(selectedNode);
        Set<I> usedActivities = selectedNodes.stream().map(ActivityNodeSupport::getSelectedValue).collect(Collectors.toSet());
        Set<I> availableActivities = new LinkedHashSet<>(selectedNode.getItems());
        availableActivities.removeAll(usedActivities);
        if (selectedNode.getSelectedRelatedObservedActivity() != null) {
            // force the default value
            availableActivities.add(selectedNode.getSelectedRelatedObservedActivity());
        }
        List<I> result = new ArrayList<>(availableActivities);
        result.sort(getComparator());
        return result;
    }

    public ApplyPairingRequest computeRequest() {
        TreeMap<String, String> request = new TreeMap<>();
        for (S o : getSelectedNodes()) {
            o.addToRequest(request);
        }
        return new ApplyPairingRequest(request);
    }

    @Override
    public boolean isCellEditable(Object node, int column) {
        return column == 2 || (column == 1 && node instanceof ActivityNodeSupport && (boolean) getValueAt(node, 2));
    }

    public int getSelectedCount() {
        return selectedNodes == null ? 0 : selectedNodes.size();
    }

    @SuppressWarnings("unused")
    public void setSelectedCount(int selectedCount) {
        // not used
    }

    public List<S> getSelectedNodes() {
        return selectedNodes;
    }

    public void setSelectedNodes(List<S> selectedNodes) {
        this.selectedNodes = selectedNodes;
    }

    public void recomputeSelectedValues() {
        R root = getRoot();
        List<S> selectedNodes = root.getSelectedNodes();
        setSelectedNodes(selectedNodes);
    }

    @Override
    public final Class<?> getColumnClass(int column) {
        if (column < columnCount - 2) {
            return String.class;
        }
        return Object.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final R getRoot() {
        return (R) super.getRoot();
    }

    public final void toggleSelectAll() {
        setSelectAll(!isSelectAll());
    }

    public final boolean isSelectAll() {
        return selectAll;
    }

    public final void setSelectAll(boolean selectAll) {
        this.selectAll = selectAll;
        getRoot().applySelected(selectAll);
    }
}

