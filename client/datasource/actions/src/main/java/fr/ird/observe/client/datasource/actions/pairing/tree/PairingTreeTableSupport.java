package fr.ird.observe.client.datasource.actions.pairing.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.actions.pairing.ActivityPairingModel;
import fr.ird.observe.client.datasource.actions.pairing.tree.node.ActivityNodeSupport;
import fr.ird.observe.client.datasource.actions.pairing.tree.node.RootNodeSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.pairing.ActivityPairingResultItemSupport;
import io.ultreia.java4all.i18n.I18n;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.table.ColumnFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created on 20/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class PairingTreeTableSupport<I extends ActivityPairingResultItemSupport<?>, S extends ActivityNodeSupport<?, ?, I, ?>, R extends RootNodeSupport<S>, M extends PairingTreeTableModelSupport<I, S, R>> extends JXTreeTable implements WithClientUIContextApi {

    static class FilterIconHeaderRenderer4 implements TableCellRenderer {
        private final Icon url = UIHelper.getUIManagerIcon("checkbox.empty");
        private final Icon url2 = UIHelper.getUIManagerIcon("checkbox.full");
        private final PairingTreeTableModelSupport<?, ?, ?> treeTableModel;

        public FilterIconHeaderRenderer4(PairingTreeTableModelSupport<?, ?, ?> treeTableModel) {

            this.treeTableModel = treeTableModel;
        }

        @Override
        public Component getTableCellRendererComponent(
                JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            DefaultTableCellRenderer r = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
            Component component = r.getTableCellRendererComponent(table, "", isSelected, hasFocus, row, column);
            r.setIcon(treeTableModel.isSelectAll() ? url2 : url);
            r.setHorizontalTextPosition(SwingConstants.LEFT);
            return component;
        }
    }

    public PairingTreeTableSupport(M treeModel) {
        super(treeModel);
        setTreeCellRenderer(new PairingTreeTableTreeCellRenderer());
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setCellSelectionEnabled(true);
        setRootVisible(false);
        setRowHeight(26);
        getTableHeader().setReorderingAllowed(false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public final M getTreeTableModel() {
        return (M) super.getTreeTableModel();
    }

    public final void openTable(R rootNode) {

        getTreeTableModel().setRoot(rootNode);

        expandAll();

        // auto expand nodes when selected
        addTreeSelectionListener(e -> {
            int selectedRow = getSelectedRow();
            if (selectedRow != -1) {
                if (!isExpanded(selectedRow)) {
                    SwingUtilities.invokeLater(() -> expandRow(selectedRow));
                }
            }
        });
        SwingUtilities.invokeLater(this::packAll);
        rootNode.setValueAt(true, 2);
    }

    public void initTable(ActivityPairingModel model, JScrollPane tableScroll) {
        model.setTreeTableModel(getTreeTableModel());
        tableScroll.setViewportView(this);
        getTreeTableModel().addTreeModelListener(new TreeModelListener() {
            @Override
            public void treeNodesChanged(TreeModelEvent e) {
                model.recomputeSelectedValues();
            }

            @Override
            public void treeNodesInserted(TreeModelEvent e) {
            }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
            }

            @Override
            public void treeStructureChanged(TreeModelEvent e) {
            }
        });
        setColumnFactory(new ColumnFactory() {
            @Override
            public void configureColumnWidths(JXTable table, TableColumnExt columnExt) {
                if (table.getColumn(0).equals(columnExt)) {
                    columnExt.setTitle(I18n.t("observe.ui.datasource.editor.actions.pairing.table.logbook.activity"));
                    columnExt.setToolTipText(I18n.t("observe.ui.datasource.editor.actions.pairing.table.logbook.activity"));
                    return;
                }
                if (table.getColumn(1).equals(columnExt)) {
                    columnExt.setTitle(I18n.t("observe.ui.datasource.editor.actions.pairing.table.observation.activity"));
                    columnExt.setToolTipText(I18n.t("observe.ui.datasource.editor.actions.pairing.table.observation.activity"));
                    return;
                }
                if (table.getColumn(2).equals(columnExt)) {
                    columnExt.setPreferredWidth(30);
                    columnExt.setMinWidth(30);
                    columnExt.setMaxWidth(30);
                    columnExt.setToolTipText(I18n.t("observe.ui.datasource.editor.actions.pairing.table.selected.tip"));
                    columnExt.setHeaderRenderer(new FilterIconHeaderRenderer4(getTreeTableModel()));
                    return;
                }
                super.configureColumnWidths(table, columnExt);
                columnExt.setMinWidth(columnExt.getPreferredWidth());
            }
        });
        getTableHeader().addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() < 2) {
                    return;
                }
                JTableHeader source = (JTableHeader) e.getSource();
                int colIndex = source.columnAtPoint(e.getPoint());
                colIndex = convertColumnIndexToModel(colIndex);
                if (colIndex == 2) {
                    getTreeTableModel().toggleSelectAll();
                    model.recomputeSelectedValues();
                    SwingUtilities.invokeLater(() -> {
                        repaint();
                        getTableHeader().repaint();
                    });
                }
            }
        });
        openTable(getTreeTableModel().getRoot());
    }
}
