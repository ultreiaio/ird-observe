package fr.ird.observe.client.datasource.actions.pairing.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.pairing.tree.node.NodeSupport;
import org.jdesktop.swingx.tree.DefaultXTreeCellRenderer;

import javax.swing.Icon;
import javax.swing.JTree;
import java.awt.Component;

/**
 * Created on 20/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class PairingTreeTableTreeCellRenderer extends DefaultXTreeCellRenderer {

    public PairingTreeTableTreeCellRenderer() {
        setLeafIcon(null);
        setOpenIcon(null);
        setClosedIcon(null);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        NodeSupport node = (NodeSupport) value;
        boolean enabled = node.isSelected();
        value = node.toString();
        Icon icon = node.getIcon();
        Component treeCellRendererComponent = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        setIcon(icon);
        treeCellRendererComponent.setEnabled(enabled);
        return treeCellRendererComponent;
    }
}
