package fr.ird.observe.client.datasource.actions.pairing.tree.node;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ActivityAware;
import fr.ird.observe.dto.data.pairing.ActivityPairingResultItemSupport;
import fr.ird.observe.dto.data.pairing.ActivityPairingResultSupport;

import java.util.List;
import java.util.Map;

/**
 * Created on 20/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ActivityNodeSupport<ALogbook extends ActivityAware, AObservation extends ActivityAware, I extends ActivityPairingResultItemSupport<AObservation>, R extends ActivityPairingResultSupport<ALogbook, AObservation, I>> extends NodeSupport {
    protected I selectedValue;

    public ActivityNodeSupport(R userObject) {
        super(userObject, false);
        this.selectedValue = getSelectedRelatedObservedActivity();
        if (selectedValue == null && !getItems().isEmpty()) {
            I activityPairingResultItem = getItems().get(0);
            setValueAt(activityPairingResultItem, 1);
        }
    }

    public final I getSelectedRelatedObservedActivity() {
        return getUserObject().getSelectedRelatedObservedActivity();
    }

    public final List<I> getItems() {
        return getUserObject().getItems();
    }

    @SuppressWarnings("unchecked")
    public final R getUserObject() {
        return (R) super.getUserObject();
    }

    @SuppressWarnings("unchecked")
    @Override
    public final void setSelectedValue(Object value) {
        selectedValue = (I) value;
    }

    @Override
    public String toString() {
        return getUserObject().getActivityLogbook().toString();
    }

    public final I getSelectedValue() {
        return selectedValue;
    }

    public final void addToRequest(Map<String, String> request) {
        I selectedValue = getSelectedValue();
        request.put(getUserObject().getActivityLogbook().getId(), selectedValue == null ? "" : getUserObject().getActivityObservation(selectedValue).getId());
    }

}
