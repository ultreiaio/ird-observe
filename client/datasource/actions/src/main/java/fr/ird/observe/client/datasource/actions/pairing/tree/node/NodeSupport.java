package fr.ird.observe.client.datasource.actions.pairing.tree.node;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;

import javax.swing.Icon;
import java.util.List;

/**
 * Created on 24/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public abstract class NodeSupport extends AbstractMutableTreeTableNode {

    private boolean selected;

    public NodeSupport(Object userObject, boolean allowsChildren) {
        super(userObject, allowsChildren);
    }

    public Icon getIcon() {
        return null;
    }

    public Object getSelectedValue() {
        return null;
    }

    public void setSelectedValue(Object value) {
    }

    public final boolean isSelected() {
        return selected;
    }

    public final void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public final Object getValueAt(int column) {
        switch (column) {
            case 0:
                return getUserObject();
            case 1:
                return getSelectedValue();
            case 2:
                return isSelected();
        }
        return null;
    }

    @Override
    public final void setValueAt(Object aValue, int column) {
        switch (column) {
            case 1:
                setSelectedValue(aValue);
                break;
            case 2:
                applySelected((Boolean) aValue);
                break;
        }
    }

    @Override
    public final int getColumnCount() {
        return 3;
    }

    public final void applySelected(boolean selected) {
        setSelected(selected);
        for (MutableTreeTableNode child : children) {
            ((NodeSupport) child).applySelected(selected);
        }
    }

    protected final void collectSelectedNodes(List<NodeSupport> result) {
        if (!isSelected()) {
            return;
        }
        if (getSelectedValue() != null) {
            result.add(this);
            return;
        }
        for (MutableTreeTableNode child : children) {
            ((NodeSupport) child).collectSelectedNodes(result);
        }
    }
}
