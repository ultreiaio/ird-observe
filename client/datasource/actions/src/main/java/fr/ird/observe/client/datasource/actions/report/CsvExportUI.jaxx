<!--
  #%L
  ObServe Client :: DataSource :: Actions
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<Table id="reportConfig">
  <import>
    java.io.File
    javax.swing.Box
    javax.swing.BoxLayout
    java.awt.Dimension

    static fr.ird.observe.client.util.UIHelper.getStringValue
  </import>
  <ReportModel id='stepModel' initializer='getContextValue(ReportModel.class)'/>
  <row>
    <cell anchor="west">
      <JLabel id="directoryLabel"/>
    </cell>
    <cell weightx='1' fill="horizontal">
      <JTextField id='directoryText' onKeyReleased='getHandler().changeDirectory(new File(((JTextField)event.getSource()).getText()))'/>
    </cell>
    <cell anchor="east">
      <JButton id="chooseExportCsvDirectory"/>
    </cell>
  </row>
  <row>
    <cell anchor="west">
      <JLabel id="fileLabel"/>
    </cell>
    <cell weightx='1' fill="horizontal" columns="2">
      <JTextField id='filenameText' onKeyReleased='getHandler().changeFilename(((JTextField)event.getSource()).getText())'/>
    </cell>
  </row>
  <row>
    <cell columns="3" anchor="west" fill="both">
      <JPanel id='actions' layout="{new BoxLayout(actions, BoxLayout.Y_AXIS)}">
        <JPanel id="cancelMessagePanel" layout="{new GridLayout(0, 1)}">
          <JSeparator constructorParams="JSeparator.HORIZONTAL"/>
          <JLabel id="cancelMessage" styleClass="information italic"/>
          <Component initializer="Box.createRigidArea(new Dimension(10, 5))"/>
        </JPanel>
      </JPanel>
    </cell>
  </row>
</Table>
