/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.report;

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.config.SelectDataModel;
import fr.ird.observe.client.datasource.actions.report.actions.ExportToClipboard;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.navigation.tree.selection.SelectionTreeConfig;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportColumnRenderersParameters;
import fr.ird.observe.report.ReportVariable;
import fr.ird.observe.report.definition.DefaultReportDefinitionsBuilder;
import fr.ird.observe.report.definition.ReportDefinition;
import fr.ird.observe.report.html.HtmlExportModel;
import fr.ird.observe.services.service.ReportService;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.util.matrix.DataMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTable;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Le modèle utilisé pour la fonctionnalité de génération de rapport.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
@SuppressWarnings("unused")
@GenerateJavaBeanDefinition
public class ReportModel extends AdminActionModel {
    public static final String EXPORT_CSV_FILE_PROPERTY_NAME = "exportCsvFile";
    public static final String EXPORT_CSV_FILE_NAME_PROPERTY_NAME = "exportCsvFileName";
    public static final String SELECTED_REPORT_PROPERTY_NAME = "selectedReport";
    public static final String SELECTED_REPORT_DEFINITION_PROPERTY_NAME = "selectedReportDefinition";
    public static final String REPORTS_PROPERTY_NAME = "reports";
    public static final String REPORT_FILE_PROPERTY_NAME = "reportFile";
    public static final String COPY_ROW_HEADERS_PROPERTY_NAME = "copyRowHeaders";
    public static final String COPY_COLUMN_HEADERS_PROPERTY_NAME = "copyColumnHeaders";
    public static final String AUTO_COPY_TO_CLIPBOARD_PROPERTY_NAME = "autoCopyToClipboard";
    public static final String VARIABLES_PROPERTY_NAME = "variables";
    public static final String VALID_PROPERTY_NAME = "valid";
    private static final TimeLog timeLog = new TimeLog(ReportModel.class, 500, 1000);
    private static final Logger log = LogManager.getLogger(ReportModel.class);
    private final Map<String, ReportColumnRenderersParameters> reportRenderers;
    /**
     * Report file location.
     */
    private File reportFile;
    /**
     * All reports loaded from report file.
     */
    private List<ReportDefinition> allReports;
    /**
     * Selected data model.
     */
    private SelectDataModel selectDataModel;
    /**
     * Reports usable from the selected data.
     */
    private List<ReportDefinition> reports;
    /**
     * Optional selected report.
     */
    private Report selectedReport;
    /**
     * Result model when selectedReport was executed.
     */
    private ResultTableModel resultModel;
    /**
     * A flag to auto copy result ot clipboard.
     */
    private boolean autoCopyToClipboard;
    /**
     * Is the selected report was be executed?
     * <p>
     * If value is {@code true} then the report is executed.
     * <p>
     * The special {@code null} value is used internal to make sure valid bean property is always executed
     */
    private Boolean valid;
    /**
     * Copy row headers to result?
     */
    private boolean copyRowHeaders = true;
    /**
     * Copy columns headers to result?
     */
    private boolean copyColumnHeaders = true;
    /**
     * Where to do optional csv export.
     */
    private File exportCsvFile = new File("");
    /**
     * Internal state to block if value is {@code true} variables components to execute some listeners.
     */
    private boolean populateVariableAdjusting;
    /**
     * Cache of variables components indexed by variable name.
     */
    private Map<String, FilterableComboBox<?>> variableComponents;
    /**
     * Main ui model.
     */
    private AdminUIModel uiModel;

    public ReportModel() {
        super(AdminStep.REPORT);
        // when report file location changed, let's reload all reports and reports
        addPropertyChangeListener(REPORT_FILE_PROPERTY_NAME, evt -> updateReports());
        reportRenderers = new TreeMap<>();
    }

    public File getExportCsvFile() {
        return exportCsvFile;
    }

    public void setExportCsvFile(File exportCsvFile) {
        File oldValue = getExportCsvFile();
        String oldBackupFileName = getExportCsvFileName();
        this.exportCsvFile = exportCsvFile;
        firePropertyChange(EXPORT_CSV_FILE_PROPERTY_NAME, oldValue, exportCsvFile);
        firePropertyChange(EXPORT_CSV_FILE_NAME_PROPERTY_NAME, oldBackupFileName, getExportCsvFileName());
    }

    public boolean isPopulateVariableAdjusting() {
        return populateVariableAdjusting;
    }

    public void setPopulateVariableAdjusting(boolean populateVariableAdjusting) {
        this.populateVariableAdjusting = populateVariableAdjusting;
    }

    public String getExportCsvFileName() {
        return ObserveUtil.removeCsvExtension(exportCsvFile.getName());
    }

    @Override
    public void start(AdminUIModel uiModel) {

        this.uiModel = uiModel;
        DataSourceSelectorModel sourceModel = uiModel.getConfigModel().getLeftSourceModel();
        sourceModel.setRequiredReadOnReferential(true);
        sourceModel.setRequiredReadOnData(true);

        selectDataModel = uiModel.getSelectDataModel();
        selectDataModel.getTreeConfig().addPropertyChangeListener(SelectionTreeConfig.MODULE_NAME, evt -> updateReports());
        ClientConfig config = getClientConfig();

        File reportFile = config.getReportDirectory();

        if (reportFile.exists()) {
            log.info(String.format("Will use default report directory : %s", reportFile));
        } else {
            log.warn(String.format("Default report directory %s does not exists.", reportFile));
        }
        setReportFile(reportFile);

        addPropertyChangeListener(evt -> {
            ReportModel source = (ReportModel) evt.getSource();
            log.debug(String.format("report model [%s] changed on %s, new value = %s", source, evt.getPropertyName(), evt.getNewValue()));
            uiModel.validate();
        });

        PropertyChangeListener listenerSelectedDataForReport = evt -> {
            // la modification de la sélection entraine la suppression d'un report sélectionné
            if (getSelectedReport() != null) {
                setSelectedReport(null);
            }
        };
        selectDataModel.getSelectionDataModel().addPropertyChangeListener(SelectionTreeModel.SELECTED_COUNT, listenerSelectedDataForReport);
        updateReports();
        uiModel.getConsolidateModel().setSkipForReport(getClientConfig().isSkipConsolidateStepForReport());
    }

    @Override
    public boolean validate(AdminUIModel uiModel) {
        // pour acceder a l'onglet des report, il faut que
        // l'onglet de sélection des données soit ok
        ReportModel reportModel = uiModel.getReportModel();
        List<ReportDefinition> reports = reportModel.getReports();
        return uiModel.validate(AdminStep.CONSOLIDATE) && reports != null && !reports.isEmpty();
    }

    @Override
    public boolean validateConfig(AdminUIModel uiModel) {
        boolean valid = super.validateConfig(uiModel);
        if (!valid) {
            return false;
        }
        ReportModel reportModel = uiModel.getReportModel();
        File reportFile = reportModel.getReportFile();
        return reportFile != null && reportFile.exists();
    }

    @Override
    public void destroy() {
        super.destroy();
        reportRenderers.clear();
        resultModel = null;
        reports = null;
    }

    public List<ReportDefinition> loadReports(Path resource) throws IOException {
        return DefaultReportDefinitionsBuilder.build(resource);
    }

    public File getReportFile() {
        return reportFile;
    }

    public void setReportFile(File reportFile) {
        Object old = this.reportFile;
        this.reportFile = reportFile;
        firePropertyChange(REPORT_FILE_PROPERTY_NAME, old, reportFile);
    }

    public boolean isAutoCopyToClipboard() {
        return autoCopyToClipboard;
    }

    public void setAutoCopyToClipboard(boolean autoCopyToClipboard) {
        boolean oldValue = this.autoCopyToClipboard;
        this.autoCopyToClipboard = autoCopyToClipboard;
        firePropertyChange(AUTO_COPY_TO_CLIPBOARD_PROPERTY_NAME, oldValue, autoCopyToClipboard);
    }

    public boolean isCopyRowHeaders() {
        return copyRowHeaders;
    }

    public void setCopyRowHeaders(boolean copyRowHeaders) {
        boolean oldValue = this.copyRowHeaders;
        this.copyRowHeaders = copyRowHeaders;
        firePropertyChange(COPY_ROW_HEADERS_PROPERTY_NAME, oldValue, copyRowHeaders);
    }

    public boolean isCopyColumnHeaders() {
        return copyColumnHeaders;
    }

    public void setCopyColumnHeaders(boolean copyColumnHeaders) {
        boolean oldValue = this.copyColumnHeaders;
        this.copyColumnHeaders = copyColumnHeaders;
        firePropertyChange(COPY_COLUMN_HEADERS_PROPERTY_NAME, oldValue, copyColumnHeaders);
    }

    public ResultTableModel getResultModel() {
        if (resultModel == null) {
            resultModel = new ResultTableModel();
        }
        return resultModel;
    }

    public boolean isInit() {
        return reports != null;
    }

    public List<ReportDefinition> getAllReports() {
        if (allReports == null) {
            if (reportFile == null || Files.notExists(reportFile.toPath())) {
                log.warn("No report file, no reports loaded.");
                allReports = Collections.emptyList();
            } else {
                try {
                    allReports = loadReports(reportFile.toPath());
                    log.debug(String.format("Add loaded %d report(s).", allReports.size()));
                } catch (IOException e) {
                    throw new IllegalStateException(
                            String.format("Could not load reports definition file (%s).", reportFile), e);
                }
            }
        }
        return allReports;
    }

    public List<ReportDefinition> getReports() {
        return reports;
    }

    public Report getSelectedReport() {
        return selectedReport;
    }

    public void setSelectedReport(Report selectedReport) {
        // invalidate model
        valid = null;
        Object old = getSelectedReport();
        Object oldDefinition = getSelectedReportDefinition();
        this.selectedReport = selectedReport;
        firePropertyChange(SELECTED_REPORT_PROPERTY_NAME, old, selectedReport);
        firePropertyChange(SELECTED_REPORT_DEFINITION_PROPERTY_NAME, oldDefinition, getSelectedReportDefinition());
    }

    public ReportDefinition getSelectedReportDefinition() {
        return selectedReport == null ? null : selectedReport.definition();
    }

    public boolean isValid() {
        return valid != null && valid;
    }

    public void setValid(boolean valid) {
        Boolean old = this.valid;
        this.valid = valid;
        firePropertyChange(VALID_PROPERTY_NAME, old, valid);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public void addVariable(String name, Object value) {
        ReportVariable variable = selectedReport.getVariable(name);
        variable.setSelectedValue(value);
        // invalidate model
        valid = null;
        firePropertyChange(VARIABLES_PROPERTY_NAME, null, name);
    }

    public void updateReports() {
        setSelectedReport(null);
        allReports = null;
        if (reportFile == null) {
            return;
        }
        if (selectDataModel == null) {
            return;
        }
        String modelType = selectDataModel.getTreeConfig().getModuleName();
        if (modelType != null) {
            reports = getAllReports().stream().filter(r -> Objects.equals(modelType, r.getModelType().toLowerCase())).collect(Collectors.toList());
        } else {
            reports = getAllReports();
        }
        firePropertyChange(REPORTS_PROPERTY_NAME, reports);
        log.info(String.format("Detects %d report(s).", reports.size()));
    }

    public File newExportCsvFile() {
        return new File(getClientConfig().getExportDirectory(), String.format("export-%1$s--%2$tF--%2$tk-%2$tM-%2$tS.csv", Objects.requireNonNull(getSelectedReport()).getId().replaceAll("\\.", "_"), new Date()));
    }

    public Path newExportHtmlDirectory() {
        return getClientConfig().getExportDirectory().toPath().resolve(String.format("export-%1$s--%2$tF--%2$tk-%2$tM-%2$tS", Objects.requireNonNull(getSelectedReport()).getId().replaceAll("\\.", "_"), new Date()));
    }

    void onReportsChanged(ReportUI ui, List<ReportDefinition> newValue) {
        log.debug(String.format("New reports : %s", newValue));
        newValue.sort(Comparator.comparing(ReportDefinition::getName));
        // Add a first null entry at first position)
        newValue.add(0, null);
        // reloading the content of the report combo-box
        UIHelper.fillComboBox(ui.getReportSelector(), newValue, null);
    }

    void updateSelectedReportFromEvent(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            JComboBox<?> source = (JComboBox<?>) event.getSource();
            updateSelectedReport((ReportDefinition) source.getSelectedItem());
        }
    }

    void updateSelectedReport(ReportDefinition reportDefinition) {
        uiModel.setBusy(true);
        try {
            log.info(String.format("New selected report definition : %s", reportDefinition));
            setSelectedReport(reportDefinition == null ? null : reportDefinition.toReport());
        } finally {
            uiModel.setBusy(false);
        }
    }

    void onSelectedReportChanged(ReportUI ui, Runnable reValidateTabUI, Report oldReport, Report report) {
        log.info(String.format("New selected report [%s]", report));
        uiModel.setBusy(true);
        try {

            // clean result model
            clearResult(ui);
            ui.getResultTable().setHighlighters();
            ObserveUtil.cleanMemory();
            if (oldReport != null) {
                log.info("Unload old selected report: {}", oldReport.getName());
                oldReport.unload();
            }
            if (report != null) {
                log.info("Unload new selected report: {}", report.getName());
                report.unload();

                initColumnRendererParameters(ui, report);
                initVariables(ui, report);
            }
            // revalidate tab ui
            SwingUtilities.invokeLater(reValidateTabUI);
            onVariablesChanges(null);

        } finally {
            uiModel.setBusy(false);
        }
    }

    void onValidChanged(ReportUI ui, boolean valid) {
        log.info("valid state changed to " + valid);
        if (!valid) {
            // clear result
            clearResult(ui);
            return;
        }
        uiModel.setBusy(true);
        try {
            Report report = getSelectedReport();
            Set<String> tripIds = selectDataModel.getSelectionDataModel().getSelectedDataIds();
            log.info(String.format("Build result for report [%s] on %s trip(s)", report.getName(), tripIds.size()));

            long startTime = TimeLog.getTime();

            ObserveSwingDataSource dataSource = uiModel.getConfigModel().getLeftSourceModel().getSafeSource(true);

            ReportService reportService = dataSource.getReportService();
            DataMatrix data = reportService.executeReport(report, tripIds);
            timeLog.log(startTime, "execute", report.getName());
            log.info(String.format("Result to display:\n%s", data.getDimension()));

            if (log.isDebugEnabled()) {
                log.debug(String.format("Result to display:\n%s", data.getClipboardContent(true, true, false, '\t')));
            }

            // populate result model
            getResultModel().populate(getClientConfig().getReferentialLocale(), report, data);

            int rowCount = getResultModel().getRowCount();
            String message;
            switch (rowCount) {
                case 0:
                    message = I18n.t("observe.ui.datasource.editor.actions.report.resultCountEmpty");
                    break;
                case 1:
                    message = I18n.t("observe.ui.datasource.editor.actions.report.resultCountOne");
                    break;
                default:
                    message = I18n.t("observe.ui.datasource.editor.actions.report.resultCount", rowCount);

            }
            ui.getConfigurationPane().setTitle(message);
            // update clipboard if necessary
            if (isAutoCopyToClipboard()) {
                ExportToClipboard.copyReportToClipBoard(report, this);
            }
            // add column renderers
            JXTable resultTable = ui.getResultTable();
            resultTable.setHighlighters();
            ReportColumnRenderersParameters reportColumnRenderersParameters = reportRenderers.get(report.definition().getId());
            if (reportColumnRenderersParameters != null) {
                reportColumnRenderersParameters.consumeColumnRenderersSwing(resultTable);
            }
            resizeColumnWidth(resultTable,-1);


        } catch (Exception e) {
            clearResult(ui);
            throw e;
        } finally {
            uiModel.setBusy(false);
        }
    }

    public static void resizeColumnWidth(JXTable table, int maxWidth) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            // Account for header size
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            TableCellRenderer renderer = tableColumn.getCellRenderer();
            if (renderer==null) {
                renderer = table.getTableHeader().getDefaultRenderer();
            }
            Component component = renderer.getTableCellRendererComponent(table, tableColumn.getHeaderValue(), false, false, -1, column);
            double width     = component.getPreferredSize().width;
            for (int row = 0; row < table.getRowCount(); row++) {
                renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width + 1, width);
            }
            if (maxWidth>0 && width > maxWidth) {
                width = maxWidth;
            }
            tableColumn.setPreferredWidth((int) width);
            log.debug("Column {} - width (min/max/pref): {} {}/{}/{}", tableColumn.getModelIndex(), tableColumn.getWidth(), tableColumn.getMinWidth(), tableColumn.getMaxWidth(), tableColumn.getPreferredWidth());
        }
    }

    private void clearResult(ReportUI ui) {
        getResultModel().clear();
        ui.getConfigurationPane().setTitle(null);
    }

    void onVariablesChanges(String variableNameChanged) {
        Report report = getSelectedReport();
        boolean canExecute;
        if (report == null) {
            canExecute = false;
        } else {
            if (variableNameChanged != null) {
                // need to unload any variable using this variable as dependency
                report.unloadVariablesIfNecessary(variableNameChanged);
            }
            // Is report has all his optional variables loaded and selected
            canExecute = report.canExecute();
            if (!canExecute) {
                // the report can not be executed, this means either
                // 1. there is some variables to populate, or some
                // 2. variables are not all selected

                // unload some variables if necessary (all the variables depending on some other variables which are
                // no more ready to be used)

                report.unloadVariablesIfNecessary();

                // now check if report need populate?
                if (report.needPopulate()) {
                    // there is still some variables to populate, ask service to populate what it can
                    uiModel.setBusy(true);
                    try {
                        populateVariables(report);
                    } finally {
                        uiModel.setBusy(false);
                    }
                }
            }
        }
        setValid(canExecute);
    }

    public void initColumnRendererParameters(ReportUI ui, Report report) {
        if (report.needInitColumnRendererParameters()) {
            String reportId = report.definition().getId();
            ReportColumnRenderersParameters reportColumnRenderersParameters = reportRenderers.get(reportId);
            if (reportColumnRenderersParameters == null) {
                ObserveSwingDataSource dataSource = uiModel.getConfigModel().getLeftSourceModel().getSafeSource(true);
                ReportService reportService = dataSource.getReportService();
                reportColumnRenderersParameters = reportService.initColumnRendererParameters(report.definition());
                reportRenderers.put(reportId, reportColumnRenderersParameters);
            }
        }
    }

    String updateSelectedReportDescription(ReportDefinition report) {
        if (report == null) {
            return t("observe.ui.message.no.report.selected");
        }
        return report.getDescription(getClientConfig().getReferentialLocale());
    }

    String updateReportsCountInformation(List<ReportDefinition> reports) {
        if (reports == null || reports.size() < 2) {
            return t("observe.ui.datasource.editor.actions.report.no.report.found");
        }
        return t("observe.ui.datasource.editor.actions.report.report.count.found", reports.size() - 1);
    }

    private void populateVariables(Report report) {
        try {
            ObserveSwingDataSource dataSource = uiModel.getConfigModel().getLeftSourceModel().getSafeSource(true);
            ReportService reportService = dataSource.getReportService();
            Report result = reportService.populateVariables(report, uiModel.getSelectDataModel().getSelectionDataModel().getSelectedDataIds());
            // push back report to model (but with no fires at all)
            this.selectedReport = result;
            populateVariablesComponents(result);
        } catch (Exception e) {
            throw new ObserveSwingTechnicalException("unable to populate report : " + report.getName(), e);
        }
    }


    @SuppressWarnings("unchecked")
    private void populateVariablesComponents(Report report) {
        populateVariableAdjusting = true;
        try {
            for (ReportVariable<?> variable : report.getVariables()) {
                populateVariablesComponent((ReportVariable<? extends JavaBean>) variable);
            }
        } finally {
            populateVariableAdjusting = false;
        }
    }

    private <V extends JavaBean> void populateVariablesComponent(ReportVariable<V> variable) {
        String variableName = variable.getName();
        boolean variableLoaded = variable.isValuesLoaded();
        @SuppressWarnings("unchecked") FilterableComboBox<V> filterableComboBox = (FilterableComboBox<V>) variableComponents.get(variableName);
        Set<V> variableValues = variable.getValues();
        if (variableLoaded) {
            // variable can be enabled in ui
            filterableComboBox.setEnabled(true);
            filterableComboBox.setData(new ArrayList<>(variableValues));
            log.info(String.format("Set variable %s values: %d / %d", variableName, variableValues.size(), filterableComboBox.getModel().getData().size()));
            filterableComboBox.setSelectedItem(variable.getSelectedValue());
            log.info(String.format("Set variable %s selected value: %s", variableName, filterableComboBox.getModel().getSelectedItem()));
        } else {
            log.info(String.format("Variable %s not loaded %d value(s)", variableName, variableValues == null ? -1 : variableValues.size()));
            filterableComboBox.setEnabled(false);
            filterableComboBox.setData(new ArrayList<>());
            filterableComboBox.setSelectedItem(null);
        }
    }

    public void initVariables(ReportUI ui, Report report) {
        variableComponents = initVariableComponents(ui, report);
        if (variableComponents != null) {
            // There is some variables for this report, do a first populate variables
            populateVariables(report);
        }
    }

    public HtmlExportModel toJsonModel() {
        return new HtmlExportModel(getClientConfig().getGsonSupplier().get(),
                                   getClientConfig().getReferentialLocale(),
                                   getSelectedReport(),
                                   getResultModel().getColumnNames(),
                                   getResultModel().getRowNames(),
                                   getResultModel().getData(),
                                   getResultModel().isWithColumnHeader(),
                                   getResultModel().isWithRowHeader(),
                                   reportRenderers.get(getSelectedReport().definition().getId()),
                                   selectDataModel.getSelectionDataModel().getSelectedCount(),
                                   selectDataModel.getSelectionDataModel().getSelectedLabels());
    }

    private Map<String, FilterableComboBox<?>> initVariableComponents(ReportUI ui, Report report) {
        Map<String, FilterableComboBox<?>> result = new TreeMap<>();
        JPanel variablesPanel = ui.getReportVariableSelectorPanel();
        variablesPanel.removeAll();
        boolean useVariables = report.isVariableRequired();
        ui.getReportVariableSelectorPane().setVisible(useVariables);
        report.unload();
        if (useVariables) {
            for (ReportVariable<?> variable : report.getVariables()) {
                @SuppressWarnings("unchecked") FilterableComboBox<?> jComponent = initVariableComponent((ReportVariable<? extends JavaBean>) variable);
                String variableName = variable.getName();
                result.put(variableName, jComponent);
                JPanel p = new JPanel(new BorderLayout());
                //FIXME Make sure reference type is well translate to dto type
                p.add(new JLabel(t(I18nDecoratorHelper.getType(variable.getType()))), BorderLayout.WEST);
                p.add(jComponent, BorderLayout.CENTER);
                variablesPanel.add(p);
            }
            return result;
        } else {
            return null;
        }
    }

    private <V extends JavaBean> FilterableComboBox<?> initVariableComponent(ReportVariable<V> variable) {
        String variableName = variable.getName();
        log.info(String.format("Add widget for variable %s type: %s", variableName, variable.getType()));
        //FIXME Decorators, let's object deal with it and never ask at this level... Just do a toString()...
        Class<V> type = variable.getType();
        Decorator decorator = getDecoratorService().getDecoratorByType(type);

        FilterableComboBox<V> combo = UIHelper.newFilterableComboBox(type, decorator, new ArrayList<>());
        combo.setEnabled(false);
        combo.setShowReset(true);

        JComboBox<?> jComboBox = combo.getCombobox();
        jComboBox.addItemListener(e -> {
            if (!combo.isEnabled()) {
                return;
            }
            if (populateVariableAdjusting) {
                return;
            }
            if (e.getStateChange() == ItemEvent.DESELECTED) {
                // ne rien faire de l'évènement de déselection sauf si le modèle devient vide
                if (jComboBox.getSelectedItem() == null) {
                    updateVariable(variableName, null);
                }
                return;
            }
            Object o = e.getItem();
            updateVariable(variableName, o);
        });
        return combo;
    }

    private void updateVariable(String variableName, Object value) {
        log.info(String.format("Set variable [%s] to value %s", variableName, value));
        addVariable(variableName, value);
    }
}
