/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.report;

import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.config.ConfigUI;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializer;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.definition.ReportDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ReportUIHandler extends AdminTabUIHandler<ReportUI> implements UIHandler<ReportUI>, PropertyChangeListener {

    private static final Logger log = LogManager.getLogger(ReportUIHandler.class);

    /**
     * To revalidate tab ui (after changing the selected report, variables may have been added or removed).
     */
    private Runnable reValidateTabUI;

    @SuppressWarnings("unchecked")
    @Override
    public void afterInit(ReportUI ui) {
        DefaultUIInitializer.doInit(ui);
        super.afterInit(ui);
        log.debug(String.format(" specialized for [%s] for main ui %s@%d", ui.getStep(), parentUI.getClass().getName(), System.identityHashCode(this.ui)));
        reValidateTabUI = ui::revalidate;

        ReportModel stepModel = ui.getModel().getReportModel();
        stepModel.addPropertyChangeListener(this);

        ui.getReportSelector().setRenderer(new DefaultListCellRenderer() {
                                               private static final long serialVersionUID = 1L;

                                               @Override
                                               public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                                                   if (value == null) {
                                                       value = t("observe.ui.message.select.report");
                                                   } else if (value instanceof ReportDefinition) {
                                                       ReportDefinition report = (ReportDefinition) value;
                                                       value = report.getName(getClientConfig().getReferentialLocale());
                                                   }
                                                   return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                                               }
                                           }
        );
        stepModel.getResultModel().init(ui.getResultTable());
        ui.remove(ui.getDescriptionContainer());
        ui.getReportVariableSelectorPane().setVisible(false);
    }

    @Override
    protected void initConfig(ConfigUI configUI) {
        super.initConfig(configUI);
        ReportConfigUI extraConfig2 = new ReportConfigUI(UIHelper.initialContext(configUI, this));
        DefaultUIInitializer.doInit(extraConfig2);
        configUI.getExtraConfig2().add(extraConfig2);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (ui.getModel().getModelState() == WizardState.CANCELED) {
            // action canceled, nothing more to do
            return;
        }
        String propertyName = evt.getPropertyName();
        Object newValue = evt.getNewValue();
        ReportModel stepModel = ui.getStepModel();
        switch (propertyName) {
            case ReportModel.REPORTS_PROPERTY_NAME:
                @SuppressWarnings("unchecked") List<ReportDefinition> reports = (List<ReportDefinition>) newValue;
                stepModel.onReportsChanged(ui, reports);
                break;
            case ReportModel.SELECTED_REPORT_PROPERTY_NAME:
                Report report = (Report) newValue;
                Report oldReport = (Report) evt.getOldValue();
                stepModel.onSelectedReportChanged(ui, reValidateTabUI, oldReport, report);
                break;
            case ReportModel.VARIABLES_PROPERTY_NAME:
                String variableNameChanged = (String) newValue;
                SwingUtilities.invokeLater(() -> stepModel.onVariablesChanges(variableNameChanged));
                break;
            case ReportModel.VALID_PROPERTY_NAME:
                Boolean valid = (Boolean) newValue;
                SwingUtilities.invokeLater(() -> stepModel.onValidChanged(ui, valid != null && valid));
                break;
        }
    }

    @Override
    public void destroy() {
        ObserveSwingDataSource dataSource = ui.getModel().getConfigModel().getLeftSourceModel().getSafeSource(false);
        if (dataSource.isOpen()) {
            dataSource.close();
        }
    }
}
