/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.report;

import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.report.Report;
import io.ultreia.java4all.util.matrix.DataMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTable;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Le modèle de tableau de résultats d'un report.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ResultTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LogManager.getLogger(ResultTableModel.class);
    private final List<String> columnNames;
    private final List<String> rowNames;
    private transient DataMatrix data;
    private boolean withColumnHeader;
    private boolean withRowHeader;
    private JTableHeader tableHeader;
    private JXTable table;

    public ResultTableModel() {
        columnNames = new ArrayList<>();
        rowNames = new ArrayList<>();
    }

    public void init(JXTable table) {

        this.table = table;
        this.tableHeader = table.getTableHeader();

        table.setRowSorter(null);
        table.setCellSelectionEnabled(true);
        table.setRowSelectionAllowed(true);
        table.setAutoCreateColumnsFromModel(true);
        TableCellRenderer defaultHeaderRenderer = tableHeader.getDefaultRenderer();

        TableCellRenderer headerRenderer = new TableCellRenderer() {

            Font defaultFont;
            Font headerFont;

            Font getDefaultFont(JTable table) {
                if (defaultFont == null) {
                    defaultFont = table.getFont();
                }
                return defaultFont;
            }

            Font getHeaderFont(JTable table) {
                if (headerFont == null) {
                    headerFont = getDefaultFont(table).deriveFont(Font.BOLD);
                }
                return headerFont;
            }

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                JLabel comp = (JLabel) defaultHeaderRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//                comp.setText(value == null ? "" : value.toString());
                comp.setToolTipText(value == null ? "" : value.toString());
                comp.setFont(getHeaderFont(table));
                return comp;
            }
        };
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
            private static final long serialVersionUID = 1L;

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (withRowHeader && table.convertColumnIndexToModel(column) == 0) {
                    return headerRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
                JLabel comp = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setToolTipText(value == null ? "" : value.toString());
                return comp;
            }
        };
        this.table.setDefaultRenderer(Object.class, renderer);
        this.table.setDefaultRenderer(String.class, renderer);
//        this.tableHeader.setDefaultRenderer(headerRenderer);
        this.table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.table.setFillsViewportHeight(true);
        this.table.setShowHorizontalLines(true);
        this.table.setColumnSelectionAllowed(true);
        this.table.setRowSelectionAllowed(true);
//        this.table.setCellSelectionEnabled(true);
        this.tableHeader.setReorderingAllowed(true);
    }

    public void populate(ReferentialLocale referentialLocale, Report report, DataMatrix incomingData) {
        if (report == null) {
            // suppression du report
            clear();
            return;
        }

        log.info(String.format("from report : %s", report));
        incomingData.setX(0);
        incomingData.setY(0);
        data = new DataMatrix();

        columnNames.clear();
        String[] columnHeaders = report.getColumnHeaders(referentialLocale);
        if (columnHeaders != null) {
            columnNames.addAll(Arrays.asList(columnHeaders));
        }
        rowNames.clear();
        String[] rowHeaders = report.getRowHeaders(referentialLocale);
        if (rowHeaders != null) {
            rowNames.addAll(Arrays.asList(rowHeaders));
        }

        int nbRows = incomingData.getHeight();
        int nbCols = incomingData.getWidth();

        withColumnHeader = !columnNames.isEmpty();
        withRowHeader = !rowNames.isEmpty();
        if (withRowHeader) {
            // on ajoute une première colonne aux données
            nbCols += 1;
            // les données commencent à la colonne 1
            incomingData.setX(1);
        }

        if (!withColumnHeader && !withRowHeader) {
            // let's say we always use columns from incomingData
            table.setTableHeader(tableHeader);
            for (int i = 0; i < nbCols; i++) {
                columnNames.add((String) incomingData.getValue(i, 0));
            }
            incomingData.setY(-1);
            withColumnHeader = true;
            nbRows--;

        } else {
            table.setTableHeader(!withColumnHeader ? null : tableHeader);
        }
        table.createDefaultColumnsFromModel();


        data.setHeight(nbRows);
        data.setWidth(nbCols);

        log.info(String.format("final data dimension : %s vs %s", data.getDimension(), incomingData.getDimension()));

        data.createData();

        if (withRowHeader) {
            // la matrice colonne 0
            DataMatrix columnData = new DataMatrix();
            columnData.setWidth(1);
            columnData.setHeight(nbRows);

            columnData.createData();
            IntStream.range(0, nbRows).forEach(i -> {
                String rowName = rowNames.get(i);
                columnData.setValue(0, i, rowName);
            });
            columnNames.add(0, "");
            data.copyData(columnData);
        } else if (withColumnHeader) {

        }
        if (nbCols > 0) {
            data.copyData(incomingData);
        }
        fireTableStructureChanged();
        table.revalidate();
        table.repaint();
    }

    public void clear() {
        data = null;
        columnNames.clear();
        rowNames.clear();
        withColumnHeader = withRowHeader = false;
        fireTableStructureChanged();
    }

    @Override
    public int getRowCount() {
        return data == null ? 0 : data.getHeight();
    }

    @Override
    public int getColumnCount() {
        return data == null ? 0 : data.getWidth();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames.get(columnIndex);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data == null ? null : data.getValue(columnIndex, rowIndex);
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public List<String> getRowNames() {
        return rowNames;
    }

    public DataMatrix getData() {
        return data;
    }

    public boolean isWithColumnHeader() {
        return withColumnHeader;
    }

    public boolean isWithRowHeader() {
        return withRowHeader;
    }

    public String getClipboardContent(boolean copyRowHeaders, boolean copyColumnHeaders) {
        return getDataContent(copyRowHeaders, copyColumnHeaders, true, '\t');
    }

    public String getCsvContent() {
        return getDataContent(true, true, true, ';');
    }

    public String getDataContent(boolean copyRowHeaders, boolean copyColumnHeaders, boolean escapeCell, char separator) {
        if (data == null) {
            return "";
        }
        String result = "";
        if (copyColumnHeaders && withColumnHeader) {
            DataMatrix columns = new DataMatrix();
            columns.setHeight(1);
            int columnCount = getColumnCount();
            columns.setWidth(columnCount);
            columns.createData();
            IntStream.range(0, columnCount).forEach(i -> {
                String rowName = getColumnName(i);
                columns.setValue(i, 0, rowName);
            });
            result = columns.getClipboardContent(true, true, escapeCell, separator);
        }
        result += data.getClipboardContent(copyRowHeaders || !withRowHeader, true, escapeCell, separator);
        return result;
    }
}
