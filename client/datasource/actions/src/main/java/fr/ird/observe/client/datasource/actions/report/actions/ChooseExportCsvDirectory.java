package fr.ird.observe.client.datasource.actions.report.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.report.CsvExportUI;
import fr.ird.observe.client.util.UIFileHelper;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;

import java.awt.event.ActionEvent;
import java.io.File;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 02/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class ChooseExportCsvDirectory extends JComponentActionSupport<CsvExportUI> {

    public ChooseExportCsvDirectory() {
        super("", t("observe.ui.action.choose.export.csv.directory"), "fileChooser", ObserveKeyStrokesActions.KEY_STROKE_STORAGE_DO_CHOOSE_FILE);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, CsvExportUI ui) {
        File f = UIFileHelper.chooseDirectory(
                ui,
                t("observe.ui.title.choose.export.csv.directory"),
                t("observe.ui.action.choose.export.csv.directory"),
                new File(ui.getDirectoryText().getText())
        );
        if (f != null) {
            ui.getHandler().changeDirectory(f);
        }
    }
}
