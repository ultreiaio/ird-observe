package fr.ird.observe.client.datasource.actions.report.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.report.ReportConfigUI;
import fr.ird.observe.client.datasource.actions.report.ReportModel;
import fr.ird.observe.client.util.UIFileHelper;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;

import java.awt.event.ActionEvent;
import java.io.File;

import static io.ultreia.java4all.i18n.I18n.t;

public class ChooseFileReport extends JComponentActionSupport<ReportConfigUI> {

    public ChooseFileReport() {
        super("", t("observe.ui.action.choose.reportFile"), "fileChooser", ObserveKeyStrokesActions.KEY_STROKE_STORAGE_DO_CHOOSE_FILE);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ReportConfigUI ui) {
        ReportModel model = ui.getModel().getReportModel();
        File f = UIFileHelper.chooseDirectory(
                ui,
                t("observe.ui.title.choose.reportFile"),
                t("observe.ui.action.choose.reportFile"),
                new File(ui.getReportFile().getText())
        );

        if (f != null) {
            model.setReportFile(f);
        }
    }
}
