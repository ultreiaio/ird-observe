package fr.ird.observe.client.datasource.actions.report.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.actions.AdminTabUIActionSupport;
import fr.ird.observe.client.datasource.actions.report.ReportModel;
import fr.ird.observe.client.datasource.actions.report.ReportUI;
import fr.ird.observe.client.datasource.actions.report.ResultTableModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.report.Report;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

public class ExportToClipboard extends AdminTabUIActionSupport<ReportUI> {

    private static final Logger log = LogManager.getLogger(ExportToClipboard.class);

    public ExportToClipboard() {
        super(t("observe.ui.datasource.editor.actions.report.exportToClipboard"),
              t("observe.ui.datasource.editor.actions.synchro.copy.tip"),
              "export-clipboard",
              ObserveKeyStrokesActions.KEY_STROKE_EXPORT_CLIPBOARD);
        setCheckMenuItemIsArmed(false);
    }

    public static void copyReportToClipBoard(Report report, ReportModel stepModel) {
        if (report == null) {
            return;
        }
        ResultTableModel model = stepModel.getResultModel();
        log.info(String.format("Will copy result of report %s - result dimension : [%d,%d]", report.getName(), model.getRowCount(), model.getColumnCount()));
        String content = model.getClipboardContent(stepModel.isCopyRowHeaders(), stepModel.isCopyColumnHeaders());
        UIHelper.copyToClipBoard(content);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ReportUI ui) {
        ReportModel stepModel = ui.getStepModel();
        Report report = stepModel.getSelectedReport();
        copyReportToClipBoard(report, stepModel);
    }
}
