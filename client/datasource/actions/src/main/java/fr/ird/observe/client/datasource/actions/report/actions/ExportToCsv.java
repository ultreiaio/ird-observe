package fr.ird.observe.client.datasource.actions.report.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.actions.AdminTabUIActionSupport;
import fr.ird.observe.client.datasource.actions.report.CsvExportUI;
import fr.ird.observe.client.datasource.actions.report.ReportModel;
import fr.ird.observe.client.datasource.actions.report.ReportUI;
import fr.ird.observe.client.datasource.actions.report.ResultTableModel;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.ObserveUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 02/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class ExportToCsv extends AdminTabUIActionSupport<ReportUI> implements PropertyChangeListener {

    private static final Logger log = LogManager.getLogger(ExportToClipboard.class);
    private JButton applyButton;

    public ExportToCsv() {
        super(t("observe.ui.datasource.editor.actions.report.exportToCsv"),
              t("observe.ui.datasource.editor.actions.report.exportToCsv.tip"),
              "export-csv",
              ObserveKeyStrokesActions.KEY_STROKE_EXPORT_CSV);
        setCheckMenuItemIsArmed(false);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ReportUI ui) {
        ReportModel stepModel = ui.getStepModel();
        JAXXInitialContext context = new JAXXInitialContext().add(stepModel);
        CsvExportUI exportUi = new CsvExportUI(context);
        KeyStroke keyStroke = ObserveKeyStrokesActions.KEY_STROKE_ALT_ENTER;

        String applyText = ObserveKeyStrokesEditorApi.suffixTextWithKeyStroke(t("observe.ui.action.apply"), keyStroke);


        Object[] options = {applyText};


        JOptionPane optionPane = new JOptionPane(exportUi, JOptionPane.QUESTION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, options[0]);

        applyButton = UsageUIHandlerSupport.findButton(optionPane, applyText);
        Objects.requireNonNull(applyButton).setEnabled(false);

        stepModel.addPropertyChangeListener(ReportModel.EXPORT_CSV_FILE_PROPERTY_NAME, this);

        try {
            AbstractAction applyAction = new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyButton.doClick();
                }
            };

            applyAction.putValue(Action.ACCELERATOR_KEY, keyStroke);
            applyAction.putValue(Action.NAME, applyText);
            applyAction.putValue(Action.SMALL_ICON, UIHelper.getContentActionIcon("save"));

            applyButton.setIcon(UIHelper.getContentActionIcon("save"));

            InputMap inputMap = optionPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
            inputMap.put((KeyStroke) applyAction.getValue(Action.ACCELERATOR_KEY), "apply");

            ActionMap actionMap = optionPane.getActionMap();
            actionMap.put("apply", applyAction);

            File exportFile = stepModel.newExportCsvFile();
            ui.getStepModel().setExportCsvFile(exportFile);

            int response = UIHelper.askUser(getMainUI(), optionPane, new Dimension(600, 220), t("observe.ui.datasource.editor.actions.report.exportToCsv.tip"), options);

            boolean accept = response == 0;
            if (accept) {
                Path exportCsvFile = stepModel.getExportCsvFile().toPath();
                ResultTableModel model = stepModel.getResultModel();
                String content = model.getCsvContent();
                log.info("Export csv to: " + exportCsvFile);
                try {
                    Files.write(exportCsvFile, content.getBytes(StandardCharsets.UTF_8));
                } catch (IOException ex) {
                    throw new RuntimeException("Can't export csv to file: " + exportCsvFile, ex);
                }
                setUiStatus(t("observe.ui.datasource.editor.actions.report.exportToCsv.done", exportCsvFile));
            }
        } finally {
            stepModel.removePropertyChangeListener(ReportModel.EXPORT_CSV_FILE_PROPERTY_NAME, this);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        File exportFile = (File) evt.getNewValue();
        applyButton.setEnabled(exportFile != null && !exportFile.exists() && ObserveUtil.withCsvExtension(exportFile.getName()));
    }
}
