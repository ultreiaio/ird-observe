package fr.ird.observe.client.datasource.actions.report.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.actions.AdminTabUIActionSupport;
import fr.ird.observe.client.datasource.actions.report.ReportModel;
import fr.ird.observe.client.datasource.actions.report.ReportUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.report.html.HtmlExportModel;
import fr.ird.observe.report.html.HtmlExportModelTemplate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created at 15/11/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ExportToHtml extends AdminTabUIActionSupport<ReportUI> {

    private static final Logger log = LogManager.getLogger(ExportToClipboard.class);

    public ExportToHtml() {
        super(t("observe.ui.datasource.editor.actions.report.exportToHtml"),
              t("observe.ui.datasource.editor.actions.report.exportToHtml.tip"),
              "export-html",
              ObserveKeyStrokesActions.KEY_STROKE_EXPORT_HTML);
        setCheckMenuItemIsArmed(false);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ReportUI ui) {
        try {
            run();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }


    private void run() throws IOException {
        ReportModel stepModel = ui.getStepModel();

        String reportId = stepModel.getSelectedReport().getId();
        // generate result directory
        Path exportDirectory = stepModel.newExportHtmlDirectory();
        Files.createDirectories(exportDirectory);

        // generate data file
        Path dataFile = exportDirectory.resolve("data.json");
        log.info("Generate data file for report ({}) at {}", reportId, dataFile);
        HtmlExportModel dataModel = stepModel.toJsonModel();
        String dataContent = dataModel.getJson();
        Files.writeString(dataFile, dataContent);

        // generate html file
        Path htmlFile = exportDirectory.resolve("report.html");
        log.info("Generate html file for report ({}) at {}", reportId, htmlFile);
        String fileContent = HtmlExportModelTemplate.generate(dataModel);
        Files.writeString(htmlFile, fileContent);

        // open html file
        UIHelper.openLink(htmlFile.toUri());
        setUiStatus(t("observe.ui.datasource.editor.actions.report.exportToHtml.done", exportDirectory));
    }
}
