package fr.ird.observe.client.datasource.actions.report.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.actions.AdminTabUIActionSupport;
import fr.ird.observe.client.datasource.actions.report.ReportUI;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created by tchemit on 03/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ToggleConfigure extends AdminTabUIActionSupport<ReportUI> {

    public ToggleConfigure() {
        super("", n("observe.ui.action.configuration.tip"), "generate", ObserveKeyStrokesEditorApi.KEY_STROKE_NAVIGATION_CONFIGURE);
    }

    @Override
    public void init() {
        super.init();
        ui.getConfigurePopup().addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                ui.getToggleConfigure().setSelected(false);
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
                ui.getToggleConfigure().setSelected(false);
            }
        });
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ReportUI ui) {
        ui.getToggleConfigure().setSelected(true);
        SwingUtilities.invokeLater(() -> {
            JComponent c = ui.getToggleConfigure();
            JPopupMenu p = ui.getConfigurePopup();
            MenuAction.preparePopup(p, c, false);
        });
    }

}
