/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.save;

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.dto.ObserveUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Le modèle d'une opération d'export de données observers.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class SaveLocalModel extends AdminActionModel {

    public static final String BACKUP_FILE_PROPERTY_NAME = "backupFile";
    public static final String BACKUP_FILE_NAME_PROPERTY_NAME = "backupFileName";
    public static final String DO_BACKUP_PROPERTY_NAME = "doBackup";
    public static final String CAN_SAVE_LOCAL_PROPERTY_NAME = "canSaveLocal";
    public static final String LOCAL_SOURCE_NEED_SAVE_PROPERTY_NAME = "localSourceNeedSave";
    private static final Logger log = LogManager.getLogger(SaveLocalModel.class);

    /**
     * Steps that need to save local data source.
     */
    protected final Set<AdminStep> stepsForSave;
    /**
     * Where to do optional back of local data source.
     */
    protected File backupFile = new File("");
    /**
     * Flag to do or not a backup of local data source.
     * <p>
     * This is only used is {@link #isLocalSourceNeedSave()} is on.
     */
    protected boolean doBackup;
    /**
     * Flag to know if a save of local data source is required.
     */
    protected boolean localSourceNeedSave;
    /**
     * Flag to know if can do a backup.
     */
    protected boolean canDoBackup;

    public SaveLocalModel() {
        super(AdminStep.SAVE_LOCAL);
        stepsForSave = new LinkedHashSet<>();
    }

    public boolean isCanDoBackup() {
        return canDoBackup;
    }

    public void setCanDoBackup(boolean canDoBackup) {
        boolean oldValue = this.canDoBackup;
        this.canDoBackup = canDoBackup;
        firePropertyChange("canDoBackup", oldValue, canDoBackup);
    }

    @Override
    public void start(AdminUIModel uiModel) {
        ClientConfig config = getClientConfig();

        File backupFile = config.newBackupDataFile();
        setBackupFile(backupFile);
        setDoBackup(true);

        addPropertyChangeListener(BACKUP_FILE_PROPERTY_NAME, evt -> uiModel.validate());
        addPropertyChangeListener(LOCAL_SOURCE_NEED_SAVE_PROPERTY_NAME, evt -> {
            boolean localSourceNeedSave = (Boolean) evt.getNewValue();
            if (localSourceNeedSave) {
                // save the local data source is mandatory
                uiModel.setStepState(AdminStep.SAVE_LOCAL, WizardState.PENDING);
            }
            uiModel.validate();
        });

    }

    @Override
    public boolean validate(AdminUIModel uiModel) {
        return !isDoBackup() || uiModel.getStepState(step) == WizardState.SUCCESSED;
    }

    public boolean isLocalSourceNeedSave() {
        return localSourceNeedSave;
    }

    public void setLocalSourceNeedSave(boolean localSourceNeedSave) {
        this.localSourceNeedSave = localSourceNeedSave;
        firePropertyChange(LOCAL_SOURCE_NEED_SAVE_PROPERTY_NAME, localSourceNeedSave);
    }

    public boolean isDoBackup() {
        return doBackup;
    }

    public void setDoBackup(boolean doBackup) {
        boolean canSave = isCanSaveLocal();
        boolean oldValue = isDoBackup();
        this.doBackup = doBackup;
        firePropertyChange(DO_BACKUP_PROPERTY_NAME, oldValue, doBackup);
        firePropertyChange(CAN_SAVE_LOCAL_PROPERTY_NAME, canSave, isCanSaveLocal());
    }

    public File getBackupFile() {
        return backupFile;
    }

    public void setBackupFile(File backupFile) {
        boolean canSave = isCanSaveLocal();
        File oldValue = getBackupFile();
        String oldBackupFileName = getBackupFileName();
        this.backupFile = backupFile;
        firePropertyChange(BACKUP_FILE_PROPERTY_NAME, oldValue, backupFile);
        firePropertyChange(BACKUP_FILE_NAME_PROPERTY_NAME, oldBackupFileName, getBackupFileName());
        firePropertyChange(CAN_SAVE_LOCAL_PROPERTY_NAME, canSave, isCanSaveLocal());
    }

    public String getBackupFileName() {
        return ObserveUtil.removeSqlGzExtension(backupFile.getName());
    }

    public Set<AdminStep> getStepsForSave() {
        return stepsForSave;
    }

    public boolean containsStepForSave(AdminStep step) {
        return stepsForSave.contains(step);
    }

    /**
     * @return {@code true} si l'on peut reporter les modifications vers la base
     * locale.
     */
    public boolean isCanSaveLocal() {
        boolean validate = !doBackup ||
                backupFile != null &&
                        !backupFile.exists() && backupFile.getParentFile() != null && backupFile.getParentFile().exists() && ObserveUtil.withSqlGzExtension(backupFile.getName());
        if (validate) {
            String filename = getBackupFileName();
            validate = !filename.isEmpty() && !filename.contains(".");
        }

        log.debug(String.format("can save ? %s", validate));
        return validate;
    }

    public void addStepForSave(AdminStep step) {
        getStepsForSave().add(step);
        // save local data source is mandatory
        setLocalSourceNeedSave(true);
    }

}
