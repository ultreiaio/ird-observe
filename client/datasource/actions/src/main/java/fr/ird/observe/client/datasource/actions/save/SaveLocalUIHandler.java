/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.save;

import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.dto.ObserveUtil;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.io.File;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class SaveLocalUIHandler extends AdminTabUIHandler<SaveLocalUI> implements UIHandler<SaveLocalUI> {
    @Override
    public void afterInit(SaveLocalUI ui) {
        super.afterInit(ui);
        setAutoStart(ui.getContinueAction());
    }

    @Override
    protected void onComingFromPreviousStep(boolean pending) {
//        String text = "";
        //FIXME Each step should get this info...
        if (ui.getStepModel().containsStepForSave(AdminStep.SYNCHRONIZE)) {
            String text = t("observe.ui.datasource.editor.actions.synchro.referential.message.need.save.for.synchro.operation");
            text += "\n\n";
            ui.getNeedSaveText().setText(text);
        }
        //FIXME We never modify source on validate operation ? this is none sense
//        if (ui.getStepModel().containsStepForSave(AdminStep.VALIDATE)) {
//            text = t("observe.ui.datasource.editor.actions.synchro.referential.message.need.save.for.validation.operation");
//            text += "\n\n";
//        }
//        ui.getNeedSaveText().setText(text);
        if (pending && !ui.getStepModel().isLocalSourceNeedSave()) {
            super.onComingFromPreviousStep(true);
        }
        boolean local = ui.getModel().getConfigModel().getLeftSourceModel().isLocal();
        ui.getStepModel().setDoBackup(local);
        ui.getStepModel().setCanDoBackup(local);
    }

    public String updateText(boolean localSourceNeedSave) {
        return localSourceNeedSave ? t("observe.ui.datasource.editor.actions.synchro.referential.message.synchro.local.modification") : t("observe.ui.datasource.editor.actions.synchro.referential.message.no.local.modification");
    }

    public void changeDirectory(File f) {
        getUi().getStepModel().setBackupFile(new File(f, ObserveUtil.addSqlGzExtension(getUi().getStepModel().getBackupFileName())));
    }

    public void changeFilename(String filename) {
        getUi().getStepModel().setBackupFile(new File(ui.getDirectoryText().getText(), ObserveUtil.addSqlGzExtension(filename)));
    }
}
