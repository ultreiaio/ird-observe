package fr.ird.observe.client.datasource.actions.save.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.save.SaveLocalUI;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

public class Continue extends SaveLocalUIActionSupport {

    public Continue() {
        super(t("observe.ui.action.continue"), t("observe.ui.action.continue"), "wizard-start", 'O');
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SaveLocalUI ui) {
        sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.saveLocal.skip"));
        AdminUIModel model = ui.getModel();
        model.setStepState(AdminStep.SAVE_LOCAL, WizardState.SUCCESSED);
//        getUi().getProgression().setText(t("observe.synchro.message.saveLocal.skip"));
        // on passe directement à l'opération suivante
        if (model.getNextStep() != null) {
            model.gotoNextStep();
        }
    }
}
