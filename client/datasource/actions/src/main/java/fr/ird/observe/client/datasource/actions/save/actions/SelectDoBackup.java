package fr.ird.observe.client.datasource.actions.save.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.save.SaveLocalModel;
import fr.ird.observe.client.datasource.actions.save.SaveLocalUI;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

public class SelectDoBackup extends SaveLocalUIActionSupport {

    public SelectDoBackup() {
        super(t("observe.ui.action.do.backup.tip"), t("observe.ui.action.do.backup.tip"), "save", ObserveKeyStrokesActions.KEY_STROKE_STORAGE_DO_BACKUP);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SaveLocalUI ui) {
        SaveLocalModel stepModel = ui.getStepModel();
        boolean newValue = !stepModel.isDoBackup();
        stepModel.setDoBackup(newValue);
    }
}
