package fr.ird.observe.client.datasource.actions.save.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.save.SaveLocalModel;
import fr.ird.observe.client.datasource.actions.save.SaveLocalUI;
import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.SynchronizeModel;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.datasource.request.CreateDatabaseRequest;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeContext;
import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Date;

import static io.ultreia.java4all.i18n.I18n.t;

public class Start extends SaveLocalUIActionSupport {

    private static final Logger log = LogManager.getLogger(Start.class);

    public Start() {
        super(null, null, "wizard-start", 'D');
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        setText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        setTooltipText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        super.defaultInit(inputMap, actionMap);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SaveLocalUI ui) {

        addAdminWorker(getUi().getStart().getToolTipText(), this::doAction);

    }

    public WizardState doAction() throws Exception {
        log.debug(this);

        SaveLocalModel stepModel = ui.getModel().getSaveLocalModel();
        if (!stepModel.isLocalSourceNeedSave()) {
            sendMessage("Aucune modification sur la base locale, opération non requise.");
            // pas de modification sur la base locale
            return WizardState.SUCCESSED;
        }

        long t00 = TimeLog.getTime();
        int stepCount = 2;
        if (stepModel.isDoBackup() && ui.getModel().getConfigModel().getLeftSourceModel().isLocal()) {
            stepCount+=3;
        }
        if (stepModel.containsStepForSave(AdminStep.SYNCHRONIZE)) {
            stepCount++;
        }
        ProgressionModel progressionModel = stepModel.initProgressModel(stepCount);
        try (ObserveSwingDataSource source = ui.getModel().getConfigModel().getLeftSourceModel().getSafeSource(true)) {
            progressionModel.increments();
            if (stepModel.isDoBackup() && source.isLocal()) {

                sendMessage("Sauvegarde de la base locale vers " + stepModel.getBackupFile());

                File backupFile = stepModel.getBackupFile();

                DataSourceService dumpService = source.getDataSourceService();
                CreateDatabaseRequest request = CreateDatabaseRequest.builder(false, source.getVersion()).addGeneratedSchema().addVersionTable().addStandaloneTables().addAllData().build();
                SqlScript dump = dumpService.produceCreateSqlScript(request);
                progressionModel.increments();
                dump.copyAndCompress(backupFile.toPath());
                progressionModel.increments();

                stepModel.getClientConfig().updateBackupDirectory(backupFile);
                progressionModel.increments();
            }
            if (stepModel.containsStepForSave(AdminStep.SYNCHRONIZE)) {
                saveUnidirectionalSynchronizeReferential(source);
                progressionModel.increments();
            }
        }
        onEndAction(t00, stepCount, progressionModel);

        return WizardState.SUCCESSED;
    }


    private void saveUnidirectionalSynchronizeReferential(ObserveSwingDataSource source) {

        SynchronizeModel stepModel = ui.getModel().getSynchronizeReferentielModel();

        UnidirectionalSynchronizeContext referentialSynchronizeContext = stepModel.getReferentialSynchronizeContext();

        if (referentialSynchronizeContext.getSqlScript().isPresent()) {
            sendMessage("Sauvegarde des modifications de la synchronisation simple.");
            sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.script.path", referentialSynchronizeContext.getSqlScriptPath()));
        }
        sendMessage("Mise à jour de la date de dernière de synchronisation de référentiel simple.");
        referentialSynchronizeContext.finish(source.getSynchronizeService());
        if (source.isLocal()) {
            source.setModified(true);
        }
        sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.apply.done", new Date()));

    }
}
