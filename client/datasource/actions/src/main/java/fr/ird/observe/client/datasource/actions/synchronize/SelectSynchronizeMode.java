package fr.ird.observe.client.datasource.actions.synchronize;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.action.JAXXObjectActionSupport;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import java.awt.event.ActionEvent;

/**
 * To select synchronize mode on admin config ui.
 * <p>
 * Created at 25/06/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.6
 */
public class SelectSynchronizeMode extends JAXXObjectActionSupport<SynchronizeConfigUI> {

    private final SynchronizeMode mode;

    public SelectSynchronizeMode(SynchronizeMode mode) {
        super(SelectSynchronizeMode.class.getName() + "#" + mode, mode.getLabel(), mode.getLabel(), mode.getIconName(), mode.getKeyStroke());
        this.mode = mode;
    }

    @Override
    protected InputMap getInputMap(SynchronizeConfigUI ui, int inputMapCondition) {
        return ui.getInputMap(inputMapCondition);
    }

    @Override
    protected int getInputMapCondition() {
        return JComponent.WHEN_IN_FOCUSED_WINDOW;
    }

    @Override
    protected ActionMap getActionMap(SynchronizeConfigUI ui) {
        return ui.getActionMap();
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SynchronizeConfigUI ui) {
        ui.getStepModel().setSynchronizeMode(mode);
    }
}
