package fr.ird.observe.client.datasource.actions.synchronize;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JToggleButton;
import java.util.EnumSet;

/**
 * Contract of admin config ui when using synchronize mode.
 * <p>
 * Created at 25/06/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.6
 */
public interface SynchronizeConfigUI extends JAXXObject {
    Logger log = LogManager.getLogger(SynchronizeConfigUI.class);

    JToggleButton getBOTH();

    JToggleButton getFROM_LEFT_TO_RIGHT();

    JToggleButton getFROM_RIGHT_TO_LEFT();

    AdminUIModel getModel();

    SynchronizeModel getStepModel();

    JAXXButtonGroup getSynchronizeMode();

    ActionMap getActionMap();

    InputMap getInputMap(int condition);

    default void updateSynchroModes() {
        AdminUIModel model = getModel();
        SynchronizeModel stepModel = getStepModel();
        SynchronizeMode synchronizeMode = stepModel.getSynchronizeMode();
        log.info(String.format("Will update synchro modes (previous value: %s)", synchronizeMode));
        DataSourceSelectorModel leftSourceModel = model.getConfigModel().getLeftSourceModel();
        if (!leftSourceModel.isInit()) {
            return;
        }
        DataSourceSelectorModel rightSourceModel = model.getConfigModel().getRightSourceModel();
        if (!rightSourceModel.isInit()) {
            return;
        }
        boolean dataSourcesValid = leftSourceModel.validateExt() && rightSourceModel.validateExt();
        boolean leftToRightEnabled = dataSourcesValid && stepModel.isCanWrite(rightSourceModel);
        log.debug(String.format("Update synchro modes: leftToRight: %s", leftToRightEnabled));
        boolean rightToLeftEnabled = dataSourcesValid && stepModel.isCanWrite(leftSourceModel);
        log.debug(String.format("Update synchro modes: rightToLeft: %s", rightToLeftEnabled));
        boolean bothEnabled = leftToRightEnabled && rightToLeftEnabled;
        log.debug(String.format("Update synchro modes: both: %s", bothEnabled));
        getFROM_LEFT_TO_RIGHT().setEnabled(leftToRightEnabled);
        getFROM_RIGHT_TO_LEFT().setEnabled(rightToLeftEnabled);
        getBOTH().setEnabled(bothEnabled);
        EnumSet<SynchronizeMode> candidates = EnumSet.allOf(SynchronizeMode.class);
        if (!leftToRightEnabled) {
            candidates.remove(SynchronizeMode.FROM_LEFT_TO_RIGHT);
        }
        if (!rightToLeftEnabled) {
            candidates.remove(SynchronizeMode.FROM_RIGHT_TO_LEFT);
        }
        if (!bothEnabled) {
            candidates.remove(SynchronizeMode.BOTH);
        }
        if (!candidates.contains(synchronizeMode)) {
            if (candidates.isEmpty()) {
                synchronizeMode = null;
            } else {
                synchronizeMode = candidates.iterator().next();
            }
        }
        log.info(String.format("Will update synchro modes (new value: %s)", synchronizeMode));
        stepModel.setSynchronizeMode(null);
        stepModel.setSynchronizeMode(synchronizeMode);
    }
}
