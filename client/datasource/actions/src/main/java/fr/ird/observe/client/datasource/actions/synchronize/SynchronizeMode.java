package fr.ird.observe.client.datasource.actions.synchronize;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

import javax.swing.KeyStroke;

/**
 * Synchronize mode, defines on which side we can write.
 * <p>
 * Created on 10/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN)
public enum SynchronizeMode {
    /**
     * La base de gauche est en lecture, la base de droite est en écriture.
     */
    FROM_LEFT_TO_RIGHT(false, true, "copyToRight"),
    /**
     * La base de gauche est en écriture, la base de droite est en lecture.
     */
    FROM_RIGHT_TO_LEFT(true, false, "copyToLeft"),
    /**
     * La base de gauche est en écriture, la base de droite est en écriture.
     */
    BOTH(true, true, "copyToBoth");
    /**
     * Est ce que la source de gauche est en écriture ?
     */
    private final boolean leftWrite;
    /**
     * Est ce que la source de droite est en écriture ?
     */
    private final boolean rightWrite;

    private final KeyStroke keyStroke;

    private final String iconName;

    SynchronizeMode(boolean leftWrite, boolean rightWrite, String iconName) {
        this.leftWrite = leftWrite;
        this.rightWrite = rightWrite;
        this.keyStroke = ObserveKeyStrokesSupport.getFunctionKeyStroke(ordinal() + 1);
        this.iconName = iconName;
    }

    public boolean isLeftWrite() {
        return leftWrite;
    }

    public boolean isRightWrite() {
        return rightWrite;
    }

    public KeyStroke getKeyStroke() {
        return keyStroke;
    }

    public String getIconName() {
        return iconName;
    }

    public String getLabel() {
        return SynchronizeModeI18n.getLabel(this);
    }
}
