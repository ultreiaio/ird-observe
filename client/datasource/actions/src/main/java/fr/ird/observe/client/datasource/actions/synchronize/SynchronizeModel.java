package fr.ird.observe.client.datasource.actions.synchronize;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminUI;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroModel;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import io.ultreia.java4all.bean.JavaBean;

/**
 * Contract of {@link AdminActionModel} model when using synchronize mode.
 * <p>
 * Created at 25/06/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.6
 */
public interface SynchronizeModel extends JavaBean {

    String SYNCHRONIZE_MODE_PROPERTY_NAME = "synchronizeMode";

    SynchronizeMode getSynchronizeMode();

    void setSynchronizeMode(SynchronizeMode synchronizeMode);

    default void start(AdminUIModel uiModel) {

        addPropertyChangeListener(SYNCHRONIZE_MODE_PROPERTY_NAME, evt -> {
            if (uiModel.isWasStarted()) {
                // on ne propage plus rien (il n'y a plus de configuration possible...)
                return;
            }
            uiModel.validate();
        });

        setSynchronizeMode(SynchronizeMode.FROM_LEFT_TO_RIGHT);
    }

    default void afterInit(AdminUI parentUI) {
        addPropertyChangeListener(DataSynchroModel.SYNCHRONIZE_MODE_PROPERTY_NAME, evt -> {
            SynchronizeMode newValue = (SynchronizeMode) evt.getNewValue();
            if (parentUI.getTabs().getComponentCount() > 1 && newValue != null) {
                parentUI.getTabs().setTitleAt(1, getTitleWithMode(newValue));
            }
        });
    }

    boolean isCanWrite(DataSourceSelectorModel dataSourceSelectorModel);

    String getTitleWithMode(SynchronizeMode mode);
}
