package fr.ird.observe.client.datasource.actions.synchronize.data;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.synchronize.SynchronizeMode;
import fr.ird.observe.client.datasource.actions.synchronize.SynchronizeModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePaneModel;
import fr.ird.observe.client.datasource.api.data.DataTaskSupport;
import fr.ird.observe.client.datasource.api.data.TaskSide;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.TwoSideContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.DefaultListModel;

/**
 * Created on 02/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class DataSynchroModel extends AdminActionModel implements SynchronizeModel, TwoSideContext<TaskSide, DataSelectionTreePaneModel> {
    public static final String LEFT_MODEL = "leftModel";
    public static final String RIGHT_MODEL = "rightModel";
    private static final String TASKS_EMPTY_PROPERTY_NAME = "tasksEmpty";
    private static final Logger log = LogManager.getLogger(DataSynchroModel.class);
    /**
     * Left model.
     */
    private final DataSelectionTreePaneModel leftModel = new DataSelectionTreePaneModel();
    /**
     * Right model.
     */
    private final DataSelectionTreePaneModel rightModel = new DataSelectionTreePaneModel();
    /**
     * Registered tasks to apply.
     */
    private final DefaultListModel<DataTaskSupport> tasks = new DefaultListModel<>();
    /**
     * Synchronize mode.
     */
    private SynchronizeMode synchronizeMode;

    public DataSynchroModel() {
        super(AdminStep.DATA_SYNCHRONIZE);
    }

    @Override
    public DataSelectionTreePaneModel left() {
        return leftModel;
    }

    @Override
    public DataSelectionTreePaneModel right() {
        return rightModel;
    }

    @Override
    public void start(AdminUIModel uiModel) {
        DataSourceSelectorModel leftSourceModel = uiModel.getConfigModel().getLeftSourceModel();
        leftSourceModel.setRequiredReadOnReferential(true);
        leftSourceModel.setRequiredReadOnData(true);
        DataSourceSelectorModel rightSourceModel = uiModel.getConfigModel().getRightSourceModel();
        rightSourceModel.setRequiredReadOnReferential(true);
        rightSourceModel.setRequiredReadOnData(true);
        fireTasksEmptyChanged();
        SynchronizeModel.super.start(uiModel);
    }

    @Override
    public boolean isCanWrite(DataSourceSelectorModel dataSourceSelectorModel) {
        return dataSourceSelectorModel.isCanWriteData();
    }

    @Override
    public String getTitleWithMode(SynchronizeMode mode) {
        return I18n.t("observe.ui.datasource.editor.actions.synchro.data.withMode", mode.getLabel());
    }

    @Override
    public SynchronizeMode getSynchronizeMode() {
        return synchronizeMode;
    }

    @Override
    public void setSynchronizeMode(SynchronizeMode synchronizeMode) {
        Object oldValue = getSynchronizeMode();
        this.synchronizeMode = synchronizeMode;
        firePropertyChange(SYNCHRONIZE_MODE_PROPERTY_NAME, oldValue, synchronizeMode);
    }

    @Override
    public boolean validateConfig(AdminUIModel uiModel) {
        boolean valid = super.validateConfig(uiModel);
        if (!valid) {
            return false;
        }
        boolean atLeastOneWrite = false;
        DataSourceSelectorModel leftSourceModel = uiModel.getConfigModel().getLeftSourceModel();
        ObserveDataSourceInformation leftDataSourceInformation = leftSourceModel.getDataSourceInformation();
        if (leftDataSourceInformation.canWriteData()) {
            atLeastOneWrite = true;
        }
        DataSourceSelectorModel rightSourceModel = uiModel.getConfigModel().getRightSourceModel();
        ObserveDataSourceInformation rightDataSourceInformation = rightSourceModel.getDataSourceInformation();
        if (rightDataSourceInformation.canWriteData()) {
            atLeastOneWrite = true;
        }
        if (!atLeastOneWrite) {
            log.debug("can not write data on any side");
            return false;
        }
        return true;
    }

    @Override
    public boolean validate(AdminUIModel uiModel) {
        return uiModel.getStepState(step) == WizardState.SUCCESSED;
    }

    @Override
    public void destroy() {
        super.destroy();
        leftModel.dispose();
        rightModel.dispose();
        tasks.clear();
    }

    public void buildFirstSelectionModels() {
        leftModel.buildFirstSelectionModel();
        rightModel.buildFirstSelectionModel();
        leftModel.finalizeSelectionModel(leftModel.getSelectionDataModel().getRoot(), rightModel.getDataIds());
        rightModel.finalizeSelectionModel(rightModel.getSelectionDataModel().getRoot(), leftModel.getDataIds());
    }

    public void rebuildSelectionModel(TaskSide side, boolean rebuildFlatModel) {
        DataSelectionTreePaneModel otherSideModel = onOppositeSide(side);
        onSameSide(side).rebuildSelectionModel(rebuildFlatModel, otherSideModel.getDataIds());
    }

    public DefaultListModel<DataTaskSupport> getTasks() {
        return tasks;
    }

    public void addTask(DataTaskSupport task) {
        tasks.addElement(task);
        fireTasksEmptyChanged();
    }

    public boolean isTasksEmpty() {
        return tasks.isEmpty();
    }

    private void fireTasksEmptyChanged() {
        firePropertyChange(TASKS_EMPTY_PROPERTY_NAME, isTasksEmpty());
    }
}
