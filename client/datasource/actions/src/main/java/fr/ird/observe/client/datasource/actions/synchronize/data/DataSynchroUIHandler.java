package fr.ird.observe.client.datasource.actions.synchronize.data;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.config.ConfigModel;
import fr.ird.observe.client.datasource.actions.config.ConfigUI;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePaneHandler;
import fr.ird.observe.client.datasource.api.data.TaskSide;
import fr.ird.observe.client.datasource.editor.api.selection.actions.SelectUnselectWithOpposite;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.UIInitHelper;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JScrollPane;
import javax.swing.ToolTipManager;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 02/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
class DataSynchroUIHandler extends AdminTabUIHandler<DataSynchroUI> implements UIHandler<DataSynchroUI> {

    @Override
    public void beforeInit(DataSynchroUI ui) {
        super.beforeInit(ui);
        DataSynchroModel dataSynchroModel = parentUI.getModel().getDataSynchroModel();
        ui.setContextValue(dataSynchroModel.onSameSide(TaskSide.FROM_LEFT), DataSelectionTreePaneHandler.MODEL_NAMES.onSameSide(TaskSide.FROM_LEFT));
        ui.setContextValue(dataSynchroModel.onOppositeSide(TaskSide.FROM_LEFT), DataSelectionTreePaneHandler.MODEL_NAMES.onOppositeSide(TaskSide.FROM_LEFT));
    }

    @Override
    public void afterInit(DataSynchroUI ui) {
        super.afterInit(ui);
        setAutoStart(ui.getStart());
        UIInitHelper.init(ui.getContentSplitPane());
        UIInitHelper.init(ui.getActionsToConsumePane());
        DataSelectionTreePaneHandler.init(ui.getLeftTreePane());
        DataSelectionTreePaneHandler.init(ui.getRightTreePane());
        SelectUnselectWithOpposite.init(ui.getLeftTreePane().getTree(), null, new SelectUnselectWithOpposite(ui.getRightTreePane().getTree()));
        JScrollPane descriptionPane = ui.getDescriptionPane();
        descriptionPane.getParent().remove(descriptionPane);
        hideFixedPanelLabel(ui);
        ui.getStepModel().afterInit(parentUI);
    }

    @Override
    protected void initConfig(ConfigUI configUI) {
        configUI.getLeftSourceConfig().getSourceInfoLabel().setVisible(true);
        configUI.getLeftSourceModel().setSourceLabel(t("observe.ui.datasource.storage.config.left.storage"));
        configUI.getLeftSourceConfig().getSourceInfoLabel().setText(t("observe.ui.action.config.export.required.read.data"));
        configUI.getRightSourceConfig().getSourceInfoLabel().setText(t("observe.ui.action.config.export.required.read.data"));
        configUI.getRightSourceModel().setSourceLabel(t("observe.ui.datasource.storage.config.right.storage"));
        DataSynchroConfigUI extraConfig = new DataSynchroConfigUI(UIHelper.initialContext(configUI, this));
        configUI.getExtraConfig().add(extraConfig);
        ConfigModel configModel = configUI.getStepModel();
        configModel.addPropertyChangeListener(StorageUIModel.VALID_STEP_PROPERTY_NAME, evt -> extraConfig.updateSynchroModes());
        configModel.addPropertyChangeListener(StorageUIModel.VALID_PROPERTY_NAME, evt -> getModel().validate());
    }

    @Override
    public void destroy() {
        ToolTipManager.sharedInstance().unregisterComponent(ui.getLeftTreePane().getTree().getTree());
        ToolTipManager.sharedInstance().unregisterComponent(ui.getRightTreePane().getTree().getTree());
    }
}
