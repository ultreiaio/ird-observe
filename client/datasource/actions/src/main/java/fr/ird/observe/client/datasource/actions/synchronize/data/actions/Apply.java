package fr.ird.observe.client.datasource.actions.synchronize.data.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroUI;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePaneModel;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.data.CopyDataTask;
import fr.ird.observe.client.datasource.api.data.DataManager;
import fr.ird.observe.client.datasource.api.data.DataTaskSupport;
import fr.ird.observe.client.datasource.api.data.InsertMissingReferentialTask;
import fr.ird.observe.client.datasource.api.data.TaskSide;
import fr.ird.observe.client.datasource.api.data.UserCancelException;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.data.RootOpenableDto;
import io.ultreia.java4all.util.TimeLog;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.DefaultListModel;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 12/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class Apply extends DataSynchroUIActionSupport {

    public Apply() {
        super(t("observe.ui.action.apply"), t("observe.ui.action.apply"), "accept", 'S');
    }

    @Override
    protected void doActionPerformed(ActionEvent e, DataSynchroUI ui) {
        addAdminWorker(getUi().getStart().getToolTipText(), this::doApply);
    }

    private WizardState doApply() {

        DataSynchroModel stepModel = ui.getStepModel();
        DataSelectionTreePaneModel leftModel = stepModel.onSameSide(TaskSide.FROM_LEFT);
        String moduleName = leftModel.getSelectionDataModel().getConfig().getModuleName();
        Class<? extends RootOpenableDto> dataType = "ps".equals(moduleName) ? fr.ird.observe.dto.data.ps.common.TripDto.class : fr.ird.observe.dto.data.ll.common.TripDto.class;
        DefaultListModel<DataTaskSupport> tasks = stepModel.getTasks();

        Set<String> idsToCopyToLeft = new LinkedHashSet<>();
        Set<String> idsToCopyToRight = new LinkedHashSet<>();
        Iterator<DataTaskSupport> iterator = tasks.elements().asIterator();
        List<DataTaskSupport> finalTasksToConsume = new LinkedList<>();
        while (iterator.hasNext()) {
            DataTaskSupport task = iterator.next();
            if (task instanceof CopyDataTask) {
                task.getTaskSide().accept(
                        (CopyDataTask) task,
                        // add task id on right
                        t -> idsToCopyToRight.add(t.getData().getTopiaId()),
                        // add task id on left
                        t -> idsToCopyToLeft.add(t.getData().getTopiaId()));
            }
            finalTasksToConsume.add(task);
        }
        if (!idsToCopyToLeft.isEmpty()) {
            // first  add missing referential to left
            finalTasksToConsume.add(0, InsertMissingReferentialTask.of(TaskSide.FROM_RIGHT, dataType, idsToCopyToLeft));
        }
        if (!idsToCopyToRight.isEmpty()) {
            // first add missing referential to right
            finalTasksToConsume.add(0, InsertMissingReferentialTask.of(TaskSide.FROM_LEFT, dataType, idsToCopyToRight));
        }
        int stepCount = 4 + finalTasksToConsume.stream().mapToInt(DataTaskSupport::stepCount).sum();
        long t0 = TimeLog.getTime();
        ProgressionModel progressModel = stepModel.initProgressModel(stepCount);

        WizardState state;
        try (ObserveSwingDataSource leftSource = ObserveSwingDataSource.doOpenSource(leftModel.getSource())) {
            progressModel.increments();
            try (ObserveSwingDataSource rightSource = ObserveSwingDataSource.doOpenSource(stepModel.onOppositeSide(TaskSide.FROM_LEFT).getSource())) {
                progressModel.increments();
                try {
                    DataManager dataManager = new DataManager(progressModel, leftSource, rightSource);
                    dataManager.consume(finalTasksToConsume);
                    state = WizardState.SUCCESSED;
                } catch (Exception e) {
                    if (e instanceof UserCancelException) {
                        state = WizardState.CANCELED;
                    } else {
                        throw e;
                    }
                }
            }
            progressModel.increments();
        }
        onEndAction(t0, stepCount, progressModel);
        return state;
    }

}
