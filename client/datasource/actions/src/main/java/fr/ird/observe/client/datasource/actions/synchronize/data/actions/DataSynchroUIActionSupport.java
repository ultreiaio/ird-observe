package fr.ird.observe.client.datasource.actions.synchronize.data.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.actions.AdminTabUIActionSupport;
import fr.ird.observe.client.datasource.actions.config.ConfigModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroUI;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.KeyStroke;
import java.util.concurrent.Callable;

abstract class DataSynchroUIActionSupport extends AdminTabUIActionSupport<DataSynchroUI> {

    private String leftSourceLabel;
    private String rightSourceLabel;

    DataSynchroUIActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    DataSynchroUIActionSupport(String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    @Override
    protected void addAdminWorker(String label, Callable<WizardState> callable) {
        leftSourceLabel = rightSourceLabel = null;
        super.addAdminWorker(label, callable);
    }

    public String getLeftSourceLabel() {
        if (leftSourceLabel == null) {
            ConfigModel configModel = ui.getModel().getConfigModel();
            leftSourceLabel = configModel.getLeftSourceModel().getLabelWithUrl().substring(configModel.getLeftSourceModel().getLabel().length());
        }
        return leftSourceLabel;
    }

    public String getRightSourceLabel() {
        if (rightSourceLabel == null) {
            ConfigModel configModel = ui.getModel().getConfigModel();
            rightSourceLabel = configModel.getRightSourceModel().getLabelWithUrl().substring(configModel.getRightSourceModel().getLabel().length());
        }
        return rightSourceLabel;
    }
}
