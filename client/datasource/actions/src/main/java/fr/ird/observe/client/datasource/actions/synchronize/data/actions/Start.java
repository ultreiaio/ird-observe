package fr.ird.observe.client.datasource.actions.synchronize.data.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.config.ConfigModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroUI;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePane;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePaneHandler;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePaneModel;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.data.TaskSide;
import fr.ird.observe.dto.ProgressionModel;
import io.ultreia.java4all.util.TimeLog;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

public class Start extends DataSynchroUIActionSupport {

    public Start() {
        super(t("observe.ui.datasource.editor.actions.synchro.data.launch.operation"), t("observe.ui.datasource.editor.actions.synchro.data.launch.operation"), "wizard-start", 'D');
    }

    @Override
    protected void doActionPerformed(ActionEvent e, DataSynchroUI ui) {
        addAdminWorker(getUi().getStart().getToolTipText(), this::doStartAction);
    }

    private WizardState doStartAction() {

        DataSynchroUI tabUI = getUi();
        DataSynchroModel stepModel = ui.getStepModel();

        String leftSourceLabel = getLeftSourceLabel();
        String rightSourceLabel = getRightSourceLabel();

        ConfigModel configModel = ui.getModel().getConfigModel();
        int stepCount = 9;
        long t0 = TimeLog.getTime();
        ProgressionModel progressModel = stepModel.initProgressModel(stepCount);
        try (ObserveSwingDataSource leftSource = configModel.getLeftSourceModel().getSafeSource(true)) {
            progressModel.increments();
            DataSelectionTreePaneModel leftModel = stepModel.onSameSide(TaskSide.FROM_LEFT);
            leftModel.setSource(leftSource);

            try (ObserveSwingDataSource rightSource = configModel.getRightSourceModel().getSafeSource(true)) {
                progressModel.increments();
                DataSelectionTreePaneModel rightModel = stepModel.onOppositeSide(TaskSide.FROM_LEFT);
                rightModel.setSource(rightSource);

                DataSelectionTreePane leftTreePane = tabUI.getLeftTreePane();
                boolean rightCanWrite = rightSource.getDataSourceInformation().canWriteData()
                        && configModel.getRightSourceModel().isCanWriteReferential()
                        && stepModel.getSynchronizeMode().isRightWrite();
                DataSelectionTreePaneHandler.initDatasource(configModel.getLeftSourceModel(), rightCanWrite, stepModel.getSynchronizeMode().isLeftWrite(),leftTreePane, leftModel.getIdStates());
                progressModel.increments();
                sendMessage(t("observe.ui.datasource.editor.actions.synchro.data.message.data.loaded", leftSourceLabel));

                DataSelectionTreePane rightTreePane = tabUI.getRightTreePane();
                boolean leftCanWrite = leftSource.getDataSourceInformation().canWriteData()
                        && configModel.getLeftSourceModel().isCanWriteReferential()
                        && stepModel.getSynchronizeMode().isLeftWrite();
                DataSelectionTreePaneHandler.initDatasource(configModel.getRightSourceModel(), leftCanWrite, stepModel.getSynchronizeMode().isRightWrite(),rightTreePane, rightModel.getIdStates());
                progressModel.increments();
                sendMessage(t("observe.ui.datasource.editor.actions.synchro.data.message.data.loaded", rightSourceLabel));

                stepModel.buildFirstSelectionModels();
                progressModel.increments();

                DataSelectionTreePaneHandler.finalizeTree(leftTreePane);
                progressModel.increments();

                DataSelectionTreePaneHandler.finalizeTree(rightTreePane);
                progressModel.increments();
            }
            progressModel.increments();
        }
        onEndAction(t0, stepCount, progressModel);
        ui.getModel().setWasDone(true);
        return WizardState.NEED_FIX;
    }
}
