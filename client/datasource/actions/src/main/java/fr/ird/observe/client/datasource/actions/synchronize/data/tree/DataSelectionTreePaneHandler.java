package fr.ird.observe.client.datasource.actions.synchronize.data.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroUI;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.actions.RegisterCopy;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.actions.RegisterDelete;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.actions.ToggleIdState;
import fr.ird.observe.client.datasource.api.data.TaskSide;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUIHandler;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePane;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePaneHandler;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.navigation.tree.selection.IdState;
import fr.ird.observe.navigation.tree.selection.SelectionTree;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNode;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.TwoSideContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.tree.TreePath;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.function.Consumer;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public class DataSelectionTreePaneHandler implements UIHandler<DataSelectionTreePane> {

    public static final TwoSideContext<TaskSide, String> MODEL_NAMES = TwoSideContext.of(DataSynchroModel.LEFT_MODEL, DataSynchroModel.RIGHT_MODEL);
    private static final Logger log = LogManager.getLogger(DataSelectionTreePaneHandler.class);

    static DataSelectionTreePaneModel getModel(DataSelectionTreePane ui) {
        return ui.getContextValue(DataSelectionTreePaneModel.class, MODEL_NAMES.onSameSide(ui.getSide()));
    }

    //FIXME Maybe we can place this in afterInit method? check this out!
    public static void init(DataSelectionTreePane ui) {
        TaskSide taskSide = ui.getSide();
        SelectionTreePane treePane = ui.getTree();
        SelectionTree tree = treePane.getTree();
        UIInitHelper.init(tree);
        RegisterCopy.install(ui, ui.getCopy(), taskSide);
        RegisterDelete.install(ui, ui.getDelete(), taskSide);
        ToggleIdState.install(ui, ui.getToggleAdded(), IdState.ADDED);
        ToggleIdState.install(ui, ui.getToggleNewer(), IdState.UPDATED);
        ToggleIdState.install(ui, ui.getToggleOlder(), IdState.OBSOLETE);
        ToggleIdState.install(ui, ui.getToggleEquals(), IdState.EQUALS);
        DataSynchroUI parent = ui.getContextValue(DataSynchroUI.class, AdminTabUIHandler.ADMIN_TAB_UI);

        treePane.getCollapseAll().setText(null);
        treePane.getExpandAll().setText(null);
        treePane.getSelectAll().setText(null);
        treePane.getUnselectAll().setText(null);
        for (Component component : ui.getToolbar().getComponents()) {
            treePane.getToolbar().add(component);
        }
        boolean isLeft = taskSide.onLeft();
        if (!isLeft) {
            ui.remove(ui.getMiddleActions());
            ui.add(ui.getMiddleActions(), BorderLayout.WEST);
        }

        Consumer<TreeConfigUI> consumer = TreeConfigUIHandler::hideOptions;
        Consumer<TreeConfigUI> apply = u -> rebuildTree(parent.getModel().getDataSynchroModel(), ui, true);
        SelectionTreePaneHandler.init(treePane, consumer, apply);

        tree.setCellRenderer(new SynchroSelectionTreeCellRenderer());
        ToolTipManager.sharedInstance().registerComponent(tree);

        SelectionTreeModel treeModel = tree.getModel();
        DataSelectionTreePaneModel model = ui.getModel();
        model.setSelectionDataModel(treeModel);
        // When model idStates has changed, rebuild the tree (but not the flat model)
        model.addPropertyChangeListener(DataSelectionTreePaneModel.ID_STATES_PROPERTY_NAME, evt -> rebuildTree(parent.getStepModel(), ui, false));

        // When tree model has changed, rebuild accessibility to copy and delete action
        treeModel.addPropertyChangeListener(evt -> {
            boolean withDataSelected = !treeModel.isSelectionEmpty();
            ui.getCopy().setEnabled(ui.getModel().isCanCopy() && withDataSelected);
            ui.getDelete().setEnabled(ui.getModel().isCanDelete() && withDataSelected);
        });
        // When use double clicks in tree, update selection state of the selected node
        tree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    TreePath path = tree.getClosestPathForLocation(e.getX(), e.getY());
                    Object lastPathComponent = path.getLastPathComponent();
                    if (lastPathComponent instanceof SelectionTreeNode) {
                        SelectionTreeNode node = (SelectionTreeNode) lastPathComponent;
                        boolean newValue = node.isNotSelected();
                        log.debug(String.format("Node: %s - new selected value: %s", node, newValue));
                        tree.getModel().setValueAt(node, newValue);
                    }
                }
            }
        });
    }

    public static void initDatasource(DataSourceSelectorModel sourceModel, boolean oppositeCanWrite, boolean canWrite, DataSelectionTreePane ui, EnumSet<IdState> idStates) {

        ui.setContextValue(sourceModel);

        boolean canWriteData = canWrite && sourceModel.isCanWriteData();
        Color color = canWriteData ? Color.BLACK : Color.RED;
        ui.getModel().setCanCopy(oppositeCanWrite);
        ui.getModel().setCanDelete(canWriteData);
        ui.getToggleEquals().setSelected(idStates.contains(IdState.EQUALS));
        ui.getToggleOlder().setSelected(idStates.contains(IdState.OBSOLETE));
        ui.getToggleNewer().setSelected(idStates.contains(IdState.UPDATED));
        ui.getToggleAdded().setSelected(idStates.contains(IdState.ADDED));
        SelectionTreePaneHandler.initDataSource(ui.getTree(), sourceModel.getSource());
        JLabel dataSourceInformation = ui.getTree().getLabel();
        dataSourceInformation.setForeground(color);
    }

    public static void finalizeTree(DataSelectionTreePane ui) {
        SelectionTreePaneHandler.updateStatistics(ui.getTree());
        afterTreeBuild(ui);
    }

    public static void rebuildTree(DataSynchroModel stepModel, DataSelectionTreePane ui, boolean rebuildFlatModel) {
        stepModel.getActionExecutor().addAction(I18n.t("observe.ui.tree.reload"), ()-> {
            ui.getTree().getTree().clearSelection();
            stepModel.rebuildSelectionModel(ui.getSide(), rebuildFlatModel);
            finalizeTree(ui);
        });
    }

    public static void afterTreeBuild(DataSelectionTreePane ui) {
        EnumMap<IdState, Integer> idStateCount = ui.getModel().getIdStateCount();
        ((ToggleIdState) ui.getToggleEquals().getAction()).updateText(idStateCount);
        ((ToggleIdState) ui.getToggleOlder().getAction()).updateText(idStateCount);
        ((ToggleIdState) ui.getToggleNewer().getAction()).updateText(idStateCount);
        ((ToggleIdState) ui.getToggleAdded().getAction()).updateText(idStateCount);
        SelectionTree tree = ui.getTree().getTree();
        boolean isLocal = ui.getModel().getSource().isLocal();
        if (tree.getModel().isNotEmpty()) {
            tree.setSelectionRow(0);
        }
        if (isLocal) {
            SwingUtilities.invokeLater(tree::expandAll);
        }
    }

    @Override
    public void beforeInit(DataSelectionTreePane ui) {
        ui.setComponentPopupMenu(new JPopupMenu());
    }
}
