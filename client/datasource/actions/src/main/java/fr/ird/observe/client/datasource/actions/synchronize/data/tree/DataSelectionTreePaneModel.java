package fr.ird.observe.client.datasource.actions.synchronize.data.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePane;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePaneHandler;
import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModel;
import fr.ird.observe.navigation.tree.selection.IdAndLastUpdateDate;
import fr.ird.observe.navigation.tree.selection.IdState;
import fr.ird.observe.navigation.tree.selection.RootSelectionTreeNode;
import fr.ird.observe.navigation.tree.selection.SelectionTreeConfig;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModelSupport;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNode;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNodeBean;
import fr.ird.observe.navigation.tree.selection.bean.RootOpenSelectionTreeNodeBean;
import io.ultreia.java4all.bean.AbstractJavaBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.tree.TreeNode;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 27/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class DataSelectionTreePaneModel extends AbstractJavaBean {
    public static final String ID_STATES_PROPERTY_NAME = "idStates";
    private static final Logger log = LogManager.getLogger(DataSelectionTreePaneModel.class);
    private static final String SOURCE_PROPERTY_NAME = "source";
    private static final String CAN_COPY_PROPERTY_NAME = "canCopy";
    private static final String CAN_DELETE_PROPERTY_NAME = "canDelete";
    /**
     * Which id states we use in tree model.
     */
    private final EnumSet<IdState> idStates = EnumSet.allOf(IdState.class);
    /**
     * Id states count in tree model.
     */
    private final EnumMap<IdState, Integer> idStateCount = new EnumMap<>(IdState.class);
    /**
     * Data source.
     */
    private ObserveSwingDataSource source;
    /**
     * The last tree flat model computed (we keep it to be able to rebuild the tree model, without asking service
     * to rebuild the flat model).
     * <p>
     * Used for filter purpose.
     */
    private ToolkitTreeFlatModel treeFlatModel;
    /**
     * Selected data in data source.
     */
    private SelectionTreeModel selectionDataModel;
    /**
     * All existing ids (computed only once at init).
     */
    private List<IdAndLastUpdateDate> dataIds = List.of();

    /**
     * Can we copy from this side?
     */
    private boolean canCopy;
    /**
     * Can we delete from this side?
     */
    private boolean canDelete;

    public void dispose() {
        source = null;
        treeFlatModel = null;
        selectionDataModel = null;
        dataIds = null;
    }

    public ObserveSwingDataSource getSource() {
        return source;
    }

    public void setSource(ObserveSwingDataSource source) {
        this.source = source;
        firePropertyChange(SOURCE_PROPERTY_NAME, source);
    }

    public boolean isCanCopy() {
        return canCopy;
    }

    public void setCanCopy(boolean canCopy) {
        boolean oldValue = this.canCopy;
        this.canCopy = canCopy;
        firePropertyChange(CAN_COPY_PROPERTY_NAME, oldValue, canCopy);
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        boolean oldValue = this.canDelete;
        this.canDelete = canDelete;
        firePropertyChange(CAN_DELETE_PROPERTY_NAME, oldValue, canDelete);
    }

    public SelectionTreeModel getSelectionDataModel() {
        return selectionDataModel;
    }

    public void setSelectionDataModel(SelectionTreeModel selectionTreeModel) {
        this.selectionDataModel = selectionTreeModel;
    }

    public List<IdAndLastUpdateDate> getDataIds() {
        return dataIds;
    }

    public EnumSet<IdState> getIdStates() {
        return idStates;
    }

    public EnumMap<IdState, Integer> getIdStateCount() {
        return idStateCount;
    }

    public void buildFirstSelectionModel() {
        SelectionTreeConfig config = selectionDataModel.getConfig();
        SelectionTreeConfig newConfig = source.newSelectionTreeConfig();
        newConfig.setLoadData(true);
        newConfig.setLoadReferential(false);
        newConfig.setUseOpenData(source.isLocal());
        // do not show empty group by
        newConfig.setLoadEmptyGroupBy(false);
        // need to see all data
        newConfig.setLoadNullGroupBy(true);
        // need to see all data
        newConfig.setLoadDisabledGroupBy(true);
        config.init(newConfig);
        treeFlatModel = selectionDataModel.buildFlatModel(source.getNavigationService()::loadSelectionRoot);
        selectionDataModel.populate(treeFlatModel, this::computeDataIds);
    }

    public void finalizeSelectionModel(RootSelectionTreeNode root, List<IdAndLastUpdateDate> otherSideIds) {
        augmentsIdState(root, otherSideIds, idStateCount);
        filterRootNode(root, idStates);
    }

    public void rebuildSelectionModel(boolean rebuildFlatModel, List<IdAndLastUpdateDate> otherSideIds) {
        if (rebuildFlatModel) {
            try (ObserveSwingDataSource dataSource = ObserveSwingDataSource.doOpenSource(source)) {
                treeFlatModel = selectionDataModel.buildFlatModel(dataSource.getNavigationService()::loadSelectionRoot);
            }
        }
        selectionDataModel.populate(treeFlatModel, r -> {
            computeDataIds(r);
            finalizeSelectionModel(r, otherSideIds);
        });
    }

    public void toggleIdState(IdState idState) {
        if (idStates.contains(idState)) {
            idStates.remove(idState);
        } else {
            idStates.add(idState);
        }
        firePropertyChange(ID_STATES_PROPERTY_NAME, idStates);
    }

    public void computeDataIds(RootSelectionTreeNode root) {
        Enumeration<?> children = root.children();
        dataIds = new LinkedList<>();
        while (children.hasMoreElements()) {
            SelectionTreeNode topNode = (SelectionTreeNode) children.nextElement();
            Enumeration<TreeNode> dataEnum = topNode.children();
            while (dataEnum.hasMoreElements()) {
                SelectionTreeNode treeNode = (SelectionTreeNode) dataEnum.nextElement();
                SelectionTreeNodeBean userObject = treeNode.getUserObject();
                if (userObject instanceof RootOpenSelectionTreeNodeBean) {
                    RootOpenSelectionTreeNodeBean object = (RootOpenSelectionTreeNodeBean) userObject;
                    dataIds.add(object.toIdAndLastUpdateDate());
                }
            }
        }
    }

    public void augmentsIdState(RootSelectionTreeNode root, List<IdAndLastUpdateDate> otherSideIds, EnumMap<IdState, Integer> idStateCount) {
        idStateCount.clear();
        if (otherSideIds != null) {
            List<String> thisSideIds = SelectionTreeModelSupport.allDataIds(root);
            // optimization: only deal with this ids that are also on this side
            Map<String, IdAndLastUpdateDate> otherSideIdsById = otherSideIds.stream().filter(id -> thisSideIds.contains(id.getId())).collect(Collectors.toMap(IdAndLastUpdateDate::getId, Function.identity()));
            Enumeration<?> enumeration = root.postorderEnumeration();
            int addedCount = 0, equalsCount = 0, olderCount = 0, newerCount = 0;
            while (enumeration.hasMoreElements()) {
                SelectionTreeNode node = (SelectionTreeNode) enumeration.nextElement();
                SelectionTreeNodeBean userObject = node.getUserObject();
                if (userObject instanceof RootOpenSelectionTreeNodeBean) {
                    RootOpenSelectionTreeNodeBean object = (RootOpenSelectionTreeNodeBean) userObject;
                    IdAndLastUpdateDate otherSideId = otherSideIdsById.get(object.getId());
                    IdState idState;
                    if (otherSideId == null) {
                        // only on this side
                        idState = IdState.ADDED;
                        addedCount++;
                    } else {
                        // id exists on both side
                        int compare = object.getLastUpdateDate().compareTo(otherSideId.getLastUpdateDate());
                        if (compare == 0) {
                            // nodes are not identical (id and lastUpdateDate equals)
                            idState = IdState.EQUALS;
                            equalsCount++;
                        } else {
                            // nodes are not identical (id is equals but not lastUpdateDate)
                            if (compare < 0) {
                                // this side object is older
                                idState = IdState.OBSOLETE;
                                olderCount++;
                            } else {
                                // this side object is newer
                                idState = IdState.UPDATED;
                                newerCount++;
                            }
                        }
                    }
                    log.debug("Set idState: {}  for node: {}", idState, userObject.getId());
                    object.setIdState(idState);
                }
            }
            idStateCount.put(IdState.ADDED, addedCount);
            idStateCount.put(IdState.EQUALS, equalsCount);
            idStateCount.put(IdState.OBSOLETE, olderCount);
            idStateCount.put(IdState.UPDATED, newerCount);
        }
    }

    public void filterRootNode(RootSelectionTreeNode root, EnumSet<IdState> states) {
        Enumeration<?> enumeration = root.postorderEnumeration();
        List<SelectionTreeNode> nodeToRemove = new LinkedList<>();
        while (enumeration.hasMoreElements()) {
            SelectionTreeNode node = (SelectionTreeNode) enumeration.nextElement();
            SelectionTreeNodeBean userObject = node.getUserObject();
            if (userObject instanceof RootOpenSelectionTreeNodeBean) {
                RootOpenSelectionTreeNodeBean object = (RootOpenSelectionTreeNodeBean) userObject;
                IdState idState = object.getIdState();
                if (idState == null) {
                    continue;
                }
                if (!states.contains(idState)) {
                    nodeToRemove.add(node);
                }
            }
        }
        if (!nodeToRemove.isEmpty()) {
            log.info("Found {} node(s) to remove, using idStates: {}.", nodeToRemove.size(), states);
            nodeToRemove.forEach(n -> {
                SelectionTreeNode parent = n.getParent();
                n.removeFromParent();
                if (parent.getChildCount() == 0) {
                    parent.removeFromParent();
                }
            });
        }
    }

    public void removeData(List<SelectionTreeNode> data, SelectionTreePane tree) {
        SelectionTreeModel selectDataModel = getSelectionDataModel();
        int removedCount = 0;
        for (SelectionTreeNode datum : data) {
            String dataPath = datum.getNodePath().toString();
            log.debug("Remove {} from flat model.", dataPath);
            Set<String> paths = treeFlatModel.getMapping().keySet().stream().filter(e -> e.startsWith(dataPath)).collect(Collectors.toSet());
            for (String path : paths) {
                removedCount++;
                log.debug("Removed {} - {} from flat model.", removedCount, path);
                treeFlatModel.removeMapping(path);
            }
            SelectionTreeNode parent = datum.getParent();
            if (parent != null) {
                String parentPath = parent.getNodePath().toString();
                long count = treeFlatModel.getMapping().keySet().stream().filter(e -> e.startsWith(parentPath)).count();
                if (count == 1) {
                    removedCount++;
                    log.debug("Removed {} - {} from flat model.", removedCount, parentPath);
                    treeFlatModel.removeMapping(parentPath);
                }
            }
        }
        selectDataModel.removeData(data);
        if (removedCount > 0) {
            log.info("Removed {} mapping(s) from flat model.", removedCount);
            tree.getTree().getModel().updateDataCount(treeFlatModel);
            SelectionTreePaneHandler.updateStatistics(tree);
            firePropertyChange(ID_STATES_PROPERTY_NAME, idStates);
        }
    }
}
