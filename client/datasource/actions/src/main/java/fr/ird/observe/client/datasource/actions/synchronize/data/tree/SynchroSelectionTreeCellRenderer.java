package fr.ird.observe.client.datasource.actions.synchronize.data.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.selection.SelectionTreeCellRenderer;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNode;

/**
 * Created on 26/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class SynchroSelectionTreeCellRenderer extends SelectionTreeCellRenderer {
    @Override
    protected SynchroSelectionTreeNodeRendererContext newContext(SelectionTreeNode node) {
        return new SynchroSelectionTreeNodeRendererContext(node);
    }
}
