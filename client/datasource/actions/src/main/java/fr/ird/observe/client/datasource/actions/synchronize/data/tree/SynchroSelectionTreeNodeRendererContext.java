package fr.ird.observe.client.datasource.actions.synchronize.data.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.selection.IdState;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNode;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNodeBean;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNodeRendererContext;
import fr.ird.observe.navigation.tree.selection.bean.RootOpenSelectionTreeNodeBean;

import javax.swing.Icon;

import static io.ultreia.java4all.i18n.I18n.getDefaultLocale;

/**
 * Created on 26/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class SynchroSelectionTreeNodeRendererContext extends SelectionTreeNodeRendererContext {
    public SynchroSelectionTreeNodeRendererContext(SelectionTreeNode node) {
        super(node);
    }

    @Override
    protected String buildText(SelectionTreeNode node) {
        SelectionTreeNodeBean userObject = node.getUserObject();
        String text = userObject.getText();
        if (userObject instanceof RootOpenSelectionTreeNodeBean) {
            String dateLabel = I18nDecoratorHelper.getTimestampLabel(getDefaultLocale(), node.getState(SelectionTreeNodeBean.STATE_LAST_UPDATE_DATE));
            text += " (" + dateLabel + ")";
        }
        return text;
    }

    @Override
    protected String buildTip(SelectionTreeNode node, String text) {
        SelectionTreeNodeBean userObject = node.getUserObject();
        IdState idStateValue = null;
        if (userObject instanceof RootOpenSelectionTreeNodeBean) {
            idStateValue = node.getState(ToolkitTreeNodeBean.ID_STATE);
        }
        if (idStateValue == null) {
            return text;
        }
        return text + " - " + idStateValue.getDescription();
    }

    @Override
    protected Icon buildIcon(SelectionTreeNode node) {
        SelectionTreeNodeBean userObject = node.getUserObject();
        IdState idStateValue = null;
        if (userObject instanceof RootOpenSelectionTreeNodeBean) {
            idStateValue = node.getState(ToolkitTreeNodeBean.ID_STATE);
        }
        return idStateValue == null ? super.buildIcon(node) : idStateValue.getIcon();
    }
}
