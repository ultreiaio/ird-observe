package fr.ird.observe.client.datasource.actions.synchronize.data.tree.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroUI;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePane;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;

import javax.swing.KeyStroke;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public abstract class DataSelectionTreePaneActionSupport extends JComponentActionSupport<DataSelectionTreePane> {

    private final KeyStroke rightAcceleratorKey;

    DataSelectionTreePaneActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey, KeyStroke rightAcceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
        this.rightAcceleratorKey = rightAcceleratorKey;
    }

    DataSelectionTreePaneActionSupport(String actionCommandKey, String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey, KeyStroke rightAcceleratorKey) {
        super(actionCommandKey, label, shortDescription, actionIcon, acceleratorKey);
        this.rightAcceleratorKey = rightAcceleratorKey;
    }

    @Override
    public void init() {
        if (!ui.getSide().onLeft()) {
            setKeyStroke(rightAcceleratorKey);
        }
        super.init();
    }
    protected DataSynchroUI getDataSynchroUI() {
        return ui.getContextValue(DataSynchroUI.class, AdminTabUIHandler.ADMIN_TAB_UI);
    }
    protected DataSynchroModel getDataSynchroModel() {
        return getDataSynchroUI().getStepModel();
    }
}
