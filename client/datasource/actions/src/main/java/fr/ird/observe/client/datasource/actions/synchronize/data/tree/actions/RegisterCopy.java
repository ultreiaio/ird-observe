package fr.ird.observe.client.datasource.actions.synchronize.data.tree.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePane;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePaneModel;
import fr.ird.observe.client.datasource.api.data.CopyDataTask;
import fr.ird.observe.client.datasource.api.data.TaskSide;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNode;

import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.util.List;

/**
 * To register a copy task.
 * <p>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public class RegisterCopy extends DataSelectionTreePaneActionSupport {

    public static void install(DataSelectionTreePane ui, JButton editor, TaskSide taskSide) {
        RegisterCopy.init(ui, editor, new RegisterCopy(taskSide));
    }

    private RegisterCopy(TaskSide taskSide) {
        super("", taskSide.getCopyTipKey(), taskSide.getCopyIconName(), ObserveKeyStrokesActions.KEY_STROKE_DATA_SYNCHRO_COPY_LEFT, ObserveKeyStrokesActions.KEY_STROKE_DATA_SYNCHRO_COPY_RIGHT);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, DataSelectionTreePane ui) {
        TaskSide taskSide = ui.getSide();
        DataSynchroModel stepModel = getDataSynchroModel();
        DataSelectionTreePaneModel thisSideModel = stepModel.onSameSide(taskSide);
        DataSelectionTreePaneModel oppositeSideModel = stepModel.onOppositeSide(taskSide);
        List<SelectionTreeNode> oppositeNodes = new LinkedList<>();
        SelectionTreeModel thisSideTreeModel = thisSideModel.getSelectionDataModel();
        SelectionTreeModel oppositeSideTreeModel = oppositeSideModel.getSelectionDataModel();
        CopyDataTask.of(taskSide, thisSideTreeModel).forEach(t -> {
            stepModel.addTask(t);
            ToolkitIdLabel data = t.getData();
            if (t.isDataExistOnOpposite()) {
                oppositeSideTreeModel.getDataNode(data).ifPresent(oppositeNodes::add);
            }
        });
        thisSideModel.removeData(thisSideTreeModel.selectedDataNodes(), getDataSynchroUI().onSameSide(taskSide).getTree());
        oppositeSideModel.removeData(oppositeNodes, getDataSynchroUI().onOppositeSide(taskSide).getTree());
    }

}
