package fr.ird.observe.client.datasource.actions.synchronize.data.tree.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.synchronize.data.DataSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePane;
import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePaneModel;
import fr.ird.observe.client.datasource.api.data.DeleteDataTask;
import fr.ird.observe.client.datasource.api.data.TaskSide;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;

import javax.swing.JButton;
import java.awt.event.ActionEvent;

/**
 * To register a delete task.
 * <p>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public class RegisterDelete extends DataSelectionTreePaneActionSupport {
    public static void install(DataSelectionTreePane ui, JButton editor, TaskSide taskSide) {
        RegisterDelete.init(ui, editor, new RegisterDelete(taskSide));
    }

    private RegisterDelete(TaskSide taskSide) {
        super("", taskSide.getDeleteTipKey(), taskSide.getDeleteIconName(), ObserveKeyStrokesActions.KEY_STROKE_DATA_SYNCHRO_DELETE_LEFT, ObserveKeyStrokesActions.KEY_STROKE_DATA_SYNCHRO_DELETE_RIGHT);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, DataSelectionTreePane ui) {
        TaskSide taskSide = ui.getSide();
        DataSynchroModel stepModel = getDataSynchroModel();
        DataSelectionTreePaneModel model = stepModel.onSameSide(taskSide);
        SelectionTreeModel selectionDataModel = model.getSelectionDataModel();
        DeleteDataTask.of(taskSide, selectionDataModel).forEach(stepModel::addTask);
        model.removeData(selectionDataModel.selectedDataNodes(), getDataSynchroUI().onOppositeSide(taskSide).getTree());
    }
}
