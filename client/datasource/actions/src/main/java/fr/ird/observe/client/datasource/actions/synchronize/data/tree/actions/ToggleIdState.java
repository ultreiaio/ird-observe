package fr.ird.observe.client.datasource.actions.synchronize.data.tree.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.data.tree.DataSelectionTreePane;
import fr.ird.observe.navigation.tree.selection.IdState;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.JToggleButton;
import java.awt.event.ActionEvent;
import java.util.EnumMap;
import java.util.Objects;

/**
 * Created on 27/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class ToggleIdState extends DataSelectionTreePaneActionSupport {

    /**
     * Indicates the id state used in action.
     */
    private final IdState idState;

    public static void install(DataSelectionTreePane ui, JToggleButton editor, IdState idState) {
        ToggleIdState.init(ui, editor, new ToggleIdState(idState));
    }

    private ToggleIdState(IdState idState) {
        super(ToggleIdState.class.getName() + idState,
              "",
              idState.getAction(),
              null,
              null,
              null);
        this.idState = Objects.requireNonNull(idState);
        setIcon(this.idState.getSmallIcon());
    }

    public IdState getIdState() {
        return idState;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, DataSelectionTreePane ui) {
        ui.getModel().toggleIdState(getIdState());

    }

    public void updateText(EnumMap<IdState, Integer> idStateCount) {
        Integer count = idStateCount.getOrDefault(getIdState(), 0);
        getEditor().setText(String.valueOf(count));
        getEditor().setToolTipText(getTooltipText() + " - " + I18n.t("observe.ui.datasource.editor.actions.synchro.data.idStateCount", count));
        setEnabled(count != 0);
    }
}
