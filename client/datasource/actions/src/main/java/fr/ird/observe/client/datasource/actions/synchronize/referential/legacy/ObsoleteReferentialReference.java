package fr.ird.observe.client.datasource.actions.synchronize.referential.legacy;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;

import java.util.Objects;

/**
 * Représente une référence obsolète à remplacer.
 * <p>
 * Created on 13/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class ObsoleteReferentialReference<D extends ReferentialDto> {

    private final Class<D> referentialName;
    private final ToolkitIdLabel referentialReference;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static ObsoleteReferentialReference<?> create(Class<? extends ReferentialDto> referentialName, DecoratorService decoratorService, ToolkitIdLabel t) {
        decoratorService.installToolkitIdLabelDecorator(referentialName, t);
        return new ObsoleteReferentialReference(referentialName, t);
    }

    protected ObsoleteReferentialReference(Class<D> referentialName, ToolkitIdLabel referentialReference) {
        this.referentialName = referentialName;
        this.referentialReference = Objects.requireNonNull(referentialReference);
    }

    public Class<D> getReferentialName() {
        return referentialName;
    }

    public ToolkitIdLabel getReferentialReference() {
        return referentialReference;
    }

    public String getId() {
        return referentialReference.getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObsoleteReferentialReference<?> that = (ObsoleteReferentialReference<?>) o;
        return Objects.equals(getReferentialReference(), that.getReferentialReference());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getReferentialReference());
    }
}
