/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.synchronize.referential.legacy;


import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdLabel;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;

import static io.ultreia.java4all.i18n.I18n.t;

public class ObsoleteReferentialReferenceListCellRenderer extends DefaultListCellRenderer implements WithClientUIContextApi {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getListCellRendererComponent(
            JList list,
            Object value,
            int index,
            boolean isSelected,
            boolean cellHasFocus) {

        ObsoleteReferentialReference<?> referentialReference = (ObsoleteReferentialReference<?>) value;
        ToolkitIdLabel referentialReference1 = referentialReference.getReferentialReference();
        String type = t(I18nDecoratorHelper.getType(referentialReference.getReferentialName()));
        String text = type + " : " + referentialReference1.toString();
        return super.getListCellRendererComponent(
                list,
                text,
                index,
                isSelected,
                cellHasFocus
        );
    }
}
