/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.synchronize.referential.legacy;

import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.services.service.referential.SynchronizeEngine;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeContext;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeEngine;
import fr.ird.observe.services.service.referential.differential.DifferentialModelBuilder;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackResults;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalResult;
import io.ultreia.java4all.util.LeftOrRightContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.model.JaxxDefaultListModel;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;
import java.util.List;

/**
 * Le modèle de l'opération de synchronization de réferentiel.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
@SuppressWarnings("unused")
public class SynchronizeModel extends AdminActionModel {

    private static final Logger log = LogManager.getLogger(SynchronizeModel.class);

    private final JaxxDefaultListModel<ObsoleteReferentialReference<?>> obsoleteReferences = new JaxxDefaultListModel<>();
    private final DefaultListSelectionModel obsoleteReferencesSelectionModel;
    private final LeftOrRightContext<ObserveSwingDataSource> sources;
    /**
     * Data source we want to synchronize.
     */
    protected ObserveSwingDataSource source;
    /**
     * Data source which contains central referential.
     */
    protected ObserveSwingDataSource centralSource;
    /**
     * Result of synchronization.
     */
    private UnidirectionalResult referentialSynchronizeResult;
    private UnidirectionalSynchronizeContext referentialSynchronizeContext;
    private UnidirectionalCallbackResults referentialSynchronizeCallbackResults;

    public SynchronizeModel() {
        super(AdminStep.SYNCHRONIZE);
        obsoleteReferencesSelectionModel = new DefaultListSelectionModel();
        obsoleteReferencesSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        sources = new LeftOrRightContext<>() {
            @Override
            public ObserveSwingDataSource left() {
                return getSource();
            }

            @Override
            public ObserveSwingDataSource right() {
                return getCentralSource();
            }
        };
    }

    @Override
    public void start(AdminUIModel uiModel) {
        DataSourceSelectorModel leftSourceModel = uiModel.getConfigModel().getLeftSourceModel();
        leftSourceModel.setRequiredReadOnReferential(true);
        leftSourceModel.setRequiredWriteOnReferential(true);
        uiModel.getConfigModel().getRightSourceModel().setRequiredReadOnReferential(true);
    }

    @Override
    public boolean validate(AdminUIModel uiModel) {
        return uiModel.getStepState(step) == WizardState.SUCCESSED;
    }

    @Override
    public void destroy() {
        super.destroy();
        obsoleteReferencesSelectionModel.clearSelection();
        obsoleteReferences.clear();
        referentialSynchronizeCallbackResults = null;
        referentialSynchronizeResult = null;
        sources.accept(ObserveSwingDataSource::doCloseSource);
        centralSource = null;
        source = null;
        referentialSynchronizeContext = null;
    }

    public ObserveSwingDataSource getSource() {
        return source;
    }

    public void setSource(ObserveSwingDataSource source) {
        this.source = source;
    }

    public ObserveSwingDataSource getCentralSource() {
        return centralSource;
    }

    public void setCentralSource(ObserveSwingDataSource centralSource) {
        this.centralSource = centralSource;
    }

    public UnidirectionalResult getReferentialSynchronizeResult() {
        return referentialSynchronizeResult;
    }

    public void setReferentialSynchronizeResult(UnidirectionalResult referentialSynchronizeResult) {
        this.referentialSynchronizeResult = referentialSynchronizeResult;
    }

    public UnidirectionalSynchronizeEngine newEngine() {
        SynchronizeEngine synchronizeEngine = newReferentialSynchronizeEngine(sources);
        return new UnidirectionalSynchronizeEngine(synchronizeEngine);
    }

    public DifferentialModelBuilder newDifferentialModelBuilder() {
        return newDifferentialModelBuilder(sources);
    }

    public UnidirectionalSynchronizeContext getReferentialSynchronizeContext() {
        return referentialSynchronizeContext;
    }

    public void setReferentialSynchronizeContext(UnidirectionalSynchronizeContext referentialSynchronizeContext) {
        this.referentialSynchronizeContext = referentialSynchronizeContext;
    }

    public UnidirectionalCallbackResults getReferentialSynchronizeCallbackResults() {
        return referentialSynchronizeCallbackResults;
    }

    public void setReferentialSynchronizeCallbackResults(UnidirectionalCallbackResults referentialSynchronizeCallbackResults) {
        this.referentialSynchronizeCallbackResults = referentialSynchronizeCallbackResults;
    }

    public JaxxDefaultListModel<ObsoleteReferentialReference<?>> getObsoleteReferences() {
        return obsoleteReferences;
    }

    public void setObsoleteReferences(List<ObsoleteReferentialReference<?>> obsoleteReferences) {
        this.obsoleteReferences.setAllElements(obsoleteReferences);
    }

    public DefaultListSelectionModel getObsoleteReferencesSelectionModel() {
        return obsoleteReferencesSelectionModel;
    }
}
