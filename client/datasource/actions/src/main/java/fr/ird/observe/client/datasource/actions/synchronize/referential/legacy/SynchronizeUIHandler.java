/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.synchronize.referential.legacy;

import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.config.ConfigUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackRequest;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.CardLayout2;
import org.nuiton.jaxx.runtime.swing.model.JaxxDefaultListModel;

import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class SynchronizeUIHandler extends AdminTabUIHandler<SynchronizeUI> implements UIHandler<SynchronizeUI> {

    private static final Logger log = LogManager.getLogger(SynchronizeUIHandler.class);

    @Override
    public void afterInit(SynchronizeUI ui) {
        super.afterInit(ui);
        setAutoStart(ui.getStart());
        ui.getStepModel().getObsoleteReferencesSelectionModel().addListSelectionListener(this::updateSelectedObsoleteEntity);
    }

    @Override
    protected void initConfig(ConfigUI configUI) {
        super.initConfig(configUI);
        configUI.getRightSourceModel().setSourceLabel(I18n.t("observe.ui.datasource.storage.config.referentiel.storage"));
    }

    public FilterableComboBox<ToolkitIdLabel> getSafeComboBox() {
        SynchronizeUI ui = getUi();
        JPanel panel = ui.getSafeRefsPanel();
        return (FilterableComboBox<ToolkitIdLabel>) ui.getSafeRefsPanelLayout().getVisibleComponent(panel);
    }

    private void updateSelectedObsoleteEntity(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            return;
        }
        SynchronizeModel stepModel = ui.getStepModel();
        int row = stepModel.getObsoleteReferencesSelectionModel().getMinSelectionIndex();
        log.info("Selected row in obsolete references list: {}", row);
        JaxxDefaultListModel<ObsoleteReferentialReference<?>> obsoleteReferences = stepModel.getObsoleteReferences();
        if (row == -1) {
            // No selection in the obsolete list
            FilterableComboBox<ToolkitIdLabel> safeComboBox = getSafeComboBox();
            if (safeComboBox != null) {
                safeComboBox.setSelectedItem(null);
                safeComboBox.setEnabled(false);
                ui.setCanApply(false);
            }
            return;
        }
        if (obsoleteReferences.getElementAt(row) != null) {
            ObsoleteReferentialReference referentialReference = obsoleteReferences.getElementAt(row);
            String key = referentialReference.getReferentialName().getName();
            CardLayout2 safeRefsPanelLayout = ui.getSafeRefsPanelLayout();
            JPanel safeRefsPanel = ui.getSafeRefsPanel();
            if (safeRefsPanelLayout.contains(key)) {
                // la liste déroulante existe deja pour ce type
                FilterableComboBox<?> list = (FilterableComboBox<?>) safeRefsPanelLayout.getComponent(safeRefsPanel, key);
                list.setEnabled(true);
                if (!getSafeComboBox().equals(list)) {
                    // on l'affiche
                    safeRefsPanelLayout.show(safeRefsPanel, key);
                }
                updateCanApply();
            } else {
                UnidirectionalCallbackRequest<?> callbackRequest = stepModel.getReferentialSynchronizeContext().getCallbackRequests().getCallbackRequest(referentialReference.getReferentialName());
                Set<ToolkitIdLabel> availableReferentialSet = callbackRequest.getAvailableReferential();
                List<ToolkitIdLabel> data = new ArrayList<>(availableReferentialSet);
                // la liste n'existe pas encore
                Decorator decorator = getDecoratorService().getToolkitIdLabelDecoratorByType(callbackRequest.getDtoType());
                data.forEach(d -> d.registerDecorator(decorator));
                FilterableComboBox<ToolkitIdLabel> box = UIHelper.newToolkitIdLabelFilterableComboBox(
                        callbackRequest.getDtoType(),
                        decorator,
                        data);
                box.setBean(this);
                box.setProperty("safeEntity");
                box.setShowReset(true);
                box.getModel().addPropertyChangeListener("selectedItem", evt -> updateCanApply());
                safeRefsPanel.add(box, key);
                safeRefsPanelLayout.show(safeRefsPanel, key);
            }
        }
    }

    private void updateCanApply() {
        SynchronizeUI ui = getUi();
        FilterableComboBox<?> safeComboBox = getSafeComboBox();
        ui.setCanApply(safeComboBox != null && safeComboBox.getModel().getSelectedItem() != null);
    }

}
