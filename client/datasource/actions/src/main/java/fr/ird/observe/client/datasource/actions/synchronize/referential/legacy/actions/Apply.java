package fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.ObsoleteReferentialReference;
import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.SynchronizeModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.SynchronizeUI;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeEngine;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackResults;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

public class Apply extends SynchroUIActionSupport {

    public Apply() {
        super(t("observe.ui.action.apply"), t("observe.ui.datasource.editor.actions.synchro.referential.legacy.apply.operation"), "accept", 'Q');
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SynchronizeUI ui) {
        addAdminWorker(getUi().getStart().getToolTipText(), this::resolveObsoleteReference);
    }

    private <D extends ReferentialDto> WizardState resolveObsoleteReference() {

        @SuppressWarnings("unchecked") ObsoleteReferentialReference<D> obsoleteRef = (ObsoleteReferentialReference<D>) ui.getObsoleteReferencesList().getSelectedValue();

        FilterableComboBox<ToolkitIdLabel> safeComboBox = ui.getHandler().getSafeComboBox();
        ToolkitIdLabel safeRef = safeComboBox.getModel().getSelectedItem();

        SynchronizeModel stepModel = ui.getStepModel();
        UnidirectionalCallbackResults referentialSynchronizeCallbackResults = stepModel.getReferentialSynchronizeCallbackResults();
        @SuppressWarnings("unchecked") Class<D> dtoType = (Class<D>) safeRef.getType();
        referentialSynchronizeCallbackResults.addCallbackResult(dtoType, obsoleteRef.getId(), safeRef.getId());

        // On supprime le référentiel corrigé de la liste des référentiels à corriger
        stepModel.getObsoleteReferences().removeElement(obsoleteRef);

        // on supprime la sélection de la liste des remplacements
        safeComboBox.setSelectedItem(null);

        // S'il ne reste plus de référentiel à corriger, on peut terminer le traitement
        if (stepModel.getObsoleteReferences().isEmpty()) {

            try (ObserveSwingDataSource source = ObserveSwingDataSource.doOpenSource(stepModel.getSource())) {

                try (ObserveSwingDataSource centralSource = ObserveSwingDataSource.doOpenSource(stepModel.getCentralSource())) {
                    UnidirectionalSynchronizeEngine engine = stepModel.newEngine();
                    beforeSuccess(engine);
                    return WizardState.SUCCESSED;
                }
            }
        }
        return WizardState.NEED_FIX;
    }

}
