package fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.ObsoleteReferentialReference;
import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.SynchronizeModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.SynchronizeUI;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeContext;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeEngine;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackRequest;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackRequests;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackResults;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

public class Start extends SynchroUIActionSupport {

    private static final Logger log = LogManager.getLogger(Start.class);

    public Start() {
        super(null, null, "wizard-start", 'D');
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        setText(t("observe.ui.datasource.editor.actions.synchro.referential.legacy.launch.operation", t(ui.getStep().getOperationLabel())));
        setTooltipText(t("observe.ui.datasource.editor.actions.synchro.referential.legacy.launch.operation", t(ui.getStep().getOperationLabel())));
        super.defaultInit(inputMap, actionMap);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SynchronizeUI ui) {
        addAdminWorker(getUi().getStart().getToolTipText(), this::doStartAction0);
    }

    private WizardState doStartAction0() {

        log.debug(this);

        SynchronizeModel stepModel = ui.getStepModel();

        ProgressionModel progressionModel = stepModel.getProgressModel();

        // on cree les sources de données

        try (ObserveSwingDataSource source = ui.getModel().getConfigModel().getLeftSourceModel().getSafeSource(true)) {
            stepModel.setSource(source);

            try (ObserveSwingDataSource centralSource = ui.getModel().getConfigModel().getRightSourceModel().getSafeSource(true)) {
                stepModel.setCentralSource(centralSource);

                // construction du différentiel
                sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.build.diff", centralSource.getLabel()));

                UnidirectionalSynchronizeEngine engine = stepModel.newEngine();

                UnidirectionalSynchronizeContext context = engine.prepareContext(stepModel.newDifferentialModelBuilder(), progressionModel);
                stepModel.setReferentialSynchronizeContext(context);
                boolean needCallback = context.isNeedCallback();
                if (needCallback) {
                    // il existe des références obsolètes à traiter
                    sendMessage(t("observe.ui.datasource.editor.actions.operation.message.needFix"));
                    stepModel.setReferentialSynchronizeCallbackResults(new UnidirectionalCallbackResults());

                    // des références obsolètes ont été détectées, on prépare les interfaces graphiques avec les données à corriger
                    List<ObsoleteReferentialReference<?>> obsoleteReferentialReferences = new LinkedList<>();
                    UnidirectionalCallbackRequests callbackRequests = stepModel.getReferentialSynchronizeContext().getCallbackRequests();

                    DecoratorService decoratorService = getDecoratorService();
                    for (UnidirectionalCallbackRequest<?> callbackRequest : callbackRequests) {
                        Class<? extends ReferentialDto> dtoType = callbackRequest.getDtoType();
                        List<ObsoleteReferentialReference<?>> obsoleteReferences = callbackRequest.getReferentialToReplace().stream()
                                                                                                  .map(d -> ObsoleteReferentialReference.create(dtoType, decoratorService, d))
                                                                                                  .collect(Collectors.toList());
                        obsoleteReferentialReferences.addAll(obsoleteReferences);
                    }
                    stepModel.setObsoleteReferences(obsoleteReferentialReferences);
                    // need user fix on obsolete references
                    return WizardState.NEED_FIX;
                }

                // pas de reference obsolete à traiter
                // on termine le traitement
                //Update lastUpdateDate anyway (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2231)
                beforeSuccess(engine);
                return WizardState.SUCCESSED;
            }
        }
    }

}
