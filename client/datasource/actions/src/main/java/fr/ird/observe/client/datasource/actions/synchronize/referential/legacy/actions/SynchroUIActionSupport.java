package fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.actions.AdminTabUIActionSupport;
import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.SynchronizeModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.legacy.SynchronizeUI;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeContext;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeEngine;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackResults;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalResult;
import io.ultreia.java4all.util.LeftOrRightContext;
import io.ultreia.java4all.util.TimeLog;

import java.util.Collection;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.t;

public abstract class SynchroUIActionSupport extends AdminTabUIActionSupport<SynchronizeUI> {

    public SynchroUIActionSupport(String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }
    protected   void beforeSuccess(UnidirectionalSynchronizeEngine engine) {
        SynchronizeModel stepModel = ui.getStepModel();

        UnidirectionalSynchronizeContext referentialSynchronizeContext = stepModel.getReferentialSynchronizeContext();
        UnidirectionalCallbackResults referentialSynchronizeCallbackResults = stepModel.getReferentialSynchronizeCallbackResults();

        UnidirectionalResult referentialSynchronizeResult = engine.prepareResult(referentialSynchronizeContext, referentialSynchronizeCallbackResults);
        stepModel.setReferentialSynchronizeResult(referentialSynchronizeResult);
        long t00 = TimeLog.getTime();
        if (referentialSynchronizeResult.isEmpty()) {
            //Update lastUpdateDate anyway (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2231)
            sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.ref.is.updtodate"));
            ui.getModel().getSaveLocalModel().addStepForSave(AdminStep.SYNCHRONIZE);
        } else {
            for (Class<? extends ReferentialDto> referentialName : referentialSynchronizeResult.getReferentialNames()) {
                String referentialStr = t(I18nDecoratorHelper.getType(referentialName));
                Collection<String> referentialAdded = referentialSynchronizeResult.getReferentialAdded(referentialName);
                if (referentialAdded != null && !referentialAdded.isEmpty()) {
                    sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.referentiel.was.added", referentialStr, referentialAdded.size()));
                    for (String id : referentialAdded) {
                        sendMessage("  - " + id);
                    }
                }
                Collection<String> referentialUpdated = referentialSynchronizeResult.getReferentialUpdated(referentialName);
                if (referentialUpdated != null && !referentialUpdated.isEmpty()) {
                    sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.referentiel.was.modified", referentialStr, referentialUpdated.size()));
                    for (String id : referentialUpdated) {
                        sendMessage("  - " + id);
                    }
                }
                Collection<String> referentialReverted = referentialSynchronizeResult.getReferentialReverted(referentialName);
                if (referentialReverted != null && !referentialReverted.isEmpty()) {
                    sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.referentiel.was.reverted", referentialStr, referentialReverted.size()));
                    for (String id : referentialReverted) {
                        sendMessage("  - " + id);
                    }
                }
                Collection<String> referentialRemoved = referentialSynchronizeResult.getReferentialRemoved(referentialName);
                if (referentialRemoved != null && !referentialRemoved.isEmpty()) {
                    sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.referentiel.was.removed", referentialStr, referentialRemoved.size()));
                    for (String id : referentialRemoved) {
                        sendMessage("  - " + id);
                    }
                }
                List<LeftOrRightContext<String>> referentialReplaced = referentialSynchronizeResult.getReferentialReplaced(referentialName);
                if (referentialReplaced != null && !referentialReplaced.isEmpty()) {
                    sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.referentiel.was.replaced", referentialStr, referentialReplaced.size()));
                    for (LeftOrRightContext<String> ids : referentialReplaced) {
                        sendMessage("  - " + ids.left() + " → " + ids.right());
                    }
                }
            }
            sendMessage(t("observe.ui.datasource.editor.actions.synchro.referential.message.script.path", referentialSynchronizeContext.getSqlScriptPath()));

            ui.getModel().getSaveLocalModel().addStepForSave(AdminStep.SYNCHRONIZE);
        }
        onEndAction(t00, -1, null);
    }
}
