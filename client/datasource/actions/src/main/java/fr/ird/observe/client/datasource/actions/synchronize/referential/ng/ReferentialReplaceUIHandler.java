package fr.ird.observe.client.datasource.actions.synchronize.referential.ng;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.awt.Dimension;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Created on 11/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferentialReplaceUIHandler implements UIHandler<ReferentialReplaceUI> {

    public static final String CONTEXT_NAME = "replaceUI";
    protected final PropertyChangeListener listenData;
    private ReferentialReplaceUI ui;

    public ReferentialReplaceUIHandler() {
        this.listenData = evt -> ui.setReplaceReference((ToolkitIdLabel) evt.getNewValue());
    }

    @Override
    public void beforeInit(ReferentialReplaceUI ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(ReferentialReplaceUI ui) {
        FilterableComboBox<ToolkitIdLabel> beanComboBox = ui.getList();

        beanComboBox.setI18nPrefix("observe.common.");
        beanComboBox.setMinimumSize(new Dimension(0, 24));
        beanComboBox.setBeanType(ToolkitIdLabel.class);
        @SuppressWarnings("unchecked") List<ToolkitIdLabel> references = ui.getContextValue(List.class, CONTEXT_NAME);
        Decorator decorator = ui.getContextValue(Decorator.class, CONTEXT_NAME);
        beanComboBox.init(decorator, references);
    }
}
