package fr.ird.observe.client.datasource.actions.synchronize.referential.ng;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.config.ConfigModel;
import fr.ird.observe.client.datasource.actions.synchronize.SynchronizeMode;
import fr.ird.observe.client.datasource.actions.synchronize.SynchronizeModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task.ReferentialSynchronizeTaskListModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task.SwingReferentialSynchronizeTask;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModel;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.ToolkitIdTechnicalLabel;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.SynchronizeEngine;
import fr.ird.observe.services.service.referential.differential.DifferentialModelBuilder;
import fr.ird.observe.services.service.referential.differential.DifferentialType;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTaskType;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.LeftOrRight;
import io.ultreia.java4all.util.LeftOrRightContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 02/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
@SuppressWarnings("unused")
public class ReferentialSynchroModel extends AdminActionModel implements SynchronizeModel {

    public static final String SHOW_PROPERTIES_PROPERTY_NAME = "showProperties";
    public static final String LEFT_SOURCE_PROPERTY_NAME = "leftSource";
    public static final String RIGHT_SOURCE_PROPERTY_NAME = "rightSource";
    public static final String LEFT_TREE_MODEL_PROPERTY_NAME = "leftTreeModel";
    public static final String RIGHT_TREE_MODEL_PROPERTY_NAME = "rightTreeModel";
    public static final String COPY_LEFT_PROPERTY_NAME = "copyLeft";
    public static final String COPY_RIGHT_PROPERTY_NAME = "copyRight";
    public static final String SKIP_LEFT_PROPERTY_NAME = "skipLeft";
    public static final String SKIP_RIGHT_PROPERTY_NAME = "skipRight";
    public static final String DELETE_LEFT_PROPERTY_NAME = "deleteLeft";
    public static final String DELETE_RIGHT_PROPERTY_NAME = "deleteRight";
    public static final String DEACTIVATE_LEFT_PROPERTY_NAME = "deactivateLeft";
    public static final String DEACTIVATE_RIGHT_PROPERTY_NAME = "deactivateRight";
    public static final String DEACTIVATE_WITH_REPLACE_LEFT_PROPERTY_NAME = "deactivateWithReplaceLeft";
    public static final String DEACTIVATE_WITH_REPLACE_RIGHT_PROPERTY_NAME = "deactivateWithReplaceRight";
    public static final String REVERT_LEFT_PROPERTY_NAME = "revertLeft";
    public static final String REVERT_RIGHT_PROPERTY_NAME = "revertRight";
    private static final String TASKS_EMPTY_PROPERTY_NAME = "tasksEmpty";
    private static final Logger log = LogManager.getLogger(ReferentialSynchroModel.class);
    private final ReferentialSynchronizeTaskListModel tasks;
    private final LeftOrRightContext<ObserveSwingDataSource> sources;
    private final LeftOrRightContext<ReferentialSynchronizeTreeModel> treeModels;
    private ObserveSwingDataSource leftSource;
    private ObserveSwingDataSource rightSource;
    private SynchronizeMode synchronizeMode;
    private boolean showProperties;
    private ReferentialSynchronizeTreeModel leftTreeModel;
    private ReferentialSynchronizeTreeModel rightTreeModel;
    private boolean copyLeft;
    private boolean revertLeft;
    private boolean skipLeft;
    private boolean deleteLeft;
    private boolean deactivateLeft;
    private boolean deactivateWithReplaceLeft;
    private boolean copyRight;
    private boolean revertRight;
    private boolean skipRight;
    private boolean deleteRight;
    private boolean deactivateRight;
    private boolean deactivateWithReplaceRight;

    public ReferentialSynchroModel() {
        super(AdminStep.REFERENTIAL_SYNCHRONIZE);
        this.tasks = new ReferentialSynchronizeTaskListModel() {
            @Override
            public void addTasks(Collection<SwingReferentialSynchronizeTask> addedTasks) {
                super.addTasks(addedTasks);
                ReferentialSynchroModel.this.fireTasksEmptyChanged();
            }

            @Override
            public void removeTasks(Collection<SwingReferentialSynchronizeTask> removedTasks) {
                super.removeTasks(removedTasks);
                ReferentialSynchroModel.this.fireTasksEmptyChanged();
            }
        };
        sources = new LeftOrRightContext<>() {
            @Override
            public ObserveSwingDataSource left() {
                return getLeftSource();
            }

            @Override
            public ObserveSwingDataSource right() {
                return getRightSource();
            }
        };
        treeModels = new LeftOrRightContext<>() {
            @Override
            public ReferentialSynchronizeTreeModel left() {
                return getLeftTreeModel();
            }

            @Override
            public ReferentialSynchronizeTreeModel right() {
                return getRightTreeModel();
            }
        };
    }

    @Override
    public boolean isCanWrite(DataSourceSelectorModel dataSourceSelectorModel) {
        return dataSourceSelectorModel.isCanWriteReferential();
    }

    @Override
    public String getTitleWithMode(SynchronizeMode mode) {
        return I18n.t("observe.ui.datasource.editor.actions.synchro.referential.withMode", mode.getLabel());
    }


    @Override
    public void start(AdminUIModel uiModel) {

        ConfigModel configModel = uiModel.getConfigModel();
        configModel.getLeftSourceModel().setRequiredReadOnReferential(true);
        configModel.getRightSourceModel().setRequiredReadOnReferential(true);

        SynchronizeModel.super.start(uiModel);
        setShowProperties(true);
    }

    @Override
    public boolean validateConfig(AdminUIModel uiModel) {
        SynchronizeMode synchronizeMode = uiModel.getReferentialSynchroModel().getSynchronizeMode();
        if (synchronizeMode == null) {
            log.info("No synchronize mode selected");
            return false;
        }
        boolean valid = super.validateConfig(uiModel);
        if (!valid) {
            log.info("Model not valid from framework.");
            return false;
        }

        DataSourceSelectorModel leftSourceModel = uiModel.getConfigModel().getLeftSourceModel();
        ObserveDataSourceInformation leftDataSourceInformation = leftSourceModel.getDataSourceInformation();
        DataSourceSelectorModel rightSourceModel = uiModel.getConfigModel().getRightSourceModel();
        ObserveDataSourceInformation rightDataSourceInformation = rightSourceModel.getDataSourceInformation();

        if (synchronizeMode.isLeftWrite() && !leftSourceModel.isCanWriteReferential()) {
            log.info(String.format("can not use %s, can not write data referential on left data source", synchronizeMode));
            return false;
        }

        if (synchronizeMode.isRightWrite() && !rightSourceModel.isCanWriteReferential()) {
            log.info(String.format("can not use %s, can not write data referential on right data source", synchronizeMode));
            return false;
        }
        return true;
    }

    @Override
    public boolean validate(AdminUIModel uiModel) {
        return uiModel.getStepState(step) == WizardState.SUCCESSED;
    }

    @Override
    public void destroy() {
        super.destroy();
        tasks.clear();
        sources.accept(ObserveSwingDataSource::doCloseSource);
        leftSource = null;
        rightSource = null;
        leftTreeModel = null;
        rightTreeModel = null;
    }

    @Override
    public SynchronizeMode getSynchronizeMode() {
        return synchronizeMode;
    }

    @Override
    public void setSynchronizeMode(SynchronizeMode synchronizeMode) {
        Object oldValue = getSynchronizeMode();
        this.synchronizeMode = synchronizeMode;
        firePropertyChange(SYNCHRONIZE_MODE_PROPERTY_NAME, oldValue, synchronizeMode);
    }

    public ObserveSwingDataSource getLeftSource() {
        return leftSource;
    }

    public void setLeftSource(ObserveSwingDataSource leftSource) {
        this.leftSource = leftSource;
        firePropertyChange(LEFT_SOURCE_PROPERTY_NAME, leftSource);
    }

    public ObserveSwingDataSource getRightSource() {
        return rightSource;
    }

    public void setRightSource(ObserveSwingDataSource rightSource) {
        this.rightSource = rightSource;
        firePropertyChange(RIGHT_SOURCE_PROPERTY_NAME, rightSource);
    }

    public ReferentialSynchronizeTreeModel getLeftTreeModel() {
        return leftTreeModel;
    }

    public void setLeftTreeModel(ReferentialSynchronizeTreeModel leftTreeModel) {
        Object oldValue = getLeftTreeModel();
        this.leftTreeModel = leftTreeModel;
        firePropertyChange(LEFT_TREE_MODEL_PROPERTY_NAME, oldValue, leftTreeModel);
    }

    public ReferentialSynchronizeTreeModel getRightTreeModel() {
        return rightTreeModel;
    }

    public void setRightTreeModel(ReferentialSynchronizeTreeModel rightTreeModel) {
        Object oldValue = getRightTreeModel();
        this.rightTreeModel = rightTreeModel;
        firePropertyChange(RIGHT_TREE_MODEL_PROPERTY_NAME, oldValue, rightTreeModel);
    }

    public LeftOrRightContext<ObserveSwingDataSource> getSources() {
        return sources;
    }

    public LeftOrRightContext<ReferentialSynchronizeTreeModel> getTreeModels() {
        return treeModels;
    }

    public boolean isShowProperties() {
        return showProperties;
    }

    public void setShowProperties(boolean showProperties) {
        Object oldValue = isShowProperties();
        this.showProperties = showProperties;
        firePropertyChange(SHOW_PROPERTIES_PROPERTY_NAME, oldValue, showProperties);
    }

    public ReferentialSynchronizeTaskListModel getTasks() {
        return tasks;
    }

    public boolean isTasksEmpty() {
        return tasks.isEmpty();
    }

    private void fireTasksEmptyChanged() {
        firePropertyChange(TASKS_EMPTY_PROPERTY_NAME, isTasksEmpty());
    }

    public boolean isCopyRight() {
        return copyRight;
    }

    public void setCopyRight(boolean copyRight) {
        this.copyRight = copyRight;
        firePropertyChange(COPY_RIGHT_PROPERTY_NAME, copyRight);
    }

    public boolean isCopyLeft() {
        return copyLeft;
    }

    public void setCopyLeft(boolean copyLeft) {
        this.copyLeft = copyLeft;
        firePropertyChange(COPY_LEFT_PROPERTY_NAME, copyLeft);
    }

    public boolean isRevertRight() {
        return revertRight;
    }

    public void setRevertRight(boolean revertRight) {
        this.revertRight = revertRight;
        firePropertyChange(REVERT_RIGHT_PROPERTY_NAME, revertRight);
    }

    public boolean isRevertLeft() {
        return revertLeft;
    }

    public void setRevertLeft(boolean revertLeft) {
        this.revertLeft = revertLeft;
        firePropertyChange(REVERT_LEFT_PROPERTY_NAME, revertLeft);
    }

    public boolean isSkipLeft() {
        return skipLeft;
    }

    public void setSkipLeft(boolean skipLeft) {
        this.skipLeft = skipLeft;
        firePropertyChange(SKIP_LEFT_PROPERTY_NAME, skipLeft);
    }

    public boolean isSkipRight() {
        return skipRight;
    }

    public void setSkipRight(boolean skipRight) {
        this.skipRight = skipRight;
        firePropertyChange(SKIP_RIGHT_PROPERTY_NAME, skipRight);
    }

    public boolean isDeleteLeft() {
        return deleteLeft;
    }

    public void setDeleteLeft(boolean deleteLeft) {
        this.deleteLeft = deleteLeft;
        firePropertyChange(DELETE_LEFT_PROPERTY_NAME, deleteLeft);
    }

    public boolean isDeleteRight() {
        return deleteRight;
    }

    public void setDeleteRight(boolean deleteRight) {
        this.deleteRight = deleteRight;
        firePropertyChange(DELETE_RIGHT_PROPERTY_NAME, deleteRight);
    }

    public boolean isDeactivateRight() {
        return deactivateRight;
    }

    public void setDeactivateRight(boolean deactivateRight) {
        this.deactivateRight = deactivateRight;
        firePropertyChange(DEACTIVATE_RIGHT_PROPERTY_NAME, deactivateRight);
    }

    public boolean isDeactivateLeft() {
        return deactivateLeft;
    }

    public void setDeactivateLeft(boolean deactivateLeft) {
        this.deactivateLeft = deactivateLeft;
        firePropertyChange(DEACTIVATE_LEFT_PROPERTY_NAME, deactivateLeft);
    }

    public boolean isDeactivateWithReplaceRight() {
        return deactivateWithReplaceRight;
    }

    public void setDeactivateWithReplaceRight(boolean deactivateWithReplaceRight) {
        this.deactivateWithReplaceRight = deactivateWithReplaceRight;
        firePropertyChange(DEACTIVATE_WITH_REPLACE_RIGHT_PROPERTY_NAME, deactivateWithReplaceRight);
    }

    public boolean isDeactivateWithReplaceLeft() {
        return deactivateWithReplaceLeft;
    }

    public void setDeactivateWithReplaceLeft(boolean deactivateWithReplaceLeft) {
        this.deactivateWithReplaceLeft = deactivateWithReplaceLeft;
        firePropertyChange(DEACTIVATE_WITH_REPLACE_LEFT_PROPERTY_NAME, deactivateWithReplaceLeft);
    }

    public <D extends ReferentialDto, R extends ReferentialDtoReference> List<ToolkitIdLabel> getPossibleReplaceUniverse(LeftOrRight side, Class<D> dtoType, ToolkitIdLabel referenceToReplace) {

        Set<ToolkitIdLabel> referencesSet = getEnabledReferentialLabelSet(side, dtoType);

        Map<String, ToolkitIdLabel> references = new LinkedHashMap<>(Maps.uniqueIndex(referencesSet, ToolkitIdLabel::getId));
        references.remove(referenceToReplace.getId());

        Enumeration<SwingReferentialSynchronizeTask> elements = tasks.elements();
        while (elements.hasMoreElements()) {
            SwingReferentialSynchronizeTask task = elements.nextElement();
            if (!dtoType.equals(task.getType())) {
                continue;
            }
            SynchronizeTaskType taskType = task.getTaskType();
            if (side.onLeft() == task.isLeft()) {
                if (taskType == SynchronizeTaskType.DELETE) {
                    // on enlève ce référentiel car il a été supprimé de ce côté
                    references.remove(task.getId());
                }

                if (taskType == SynchronizeTaskType.DEACTIVATE || taskType == SynchronizeTaskType.DEACTIVATE_WITH_REPLACEMENT) {
                    // on enlève ce référentiel car il a été désactivé de ce côté
                    references.remove(task.getId());
                }
                continue;
            }
            if (taskType == SynchronizeTaskType.ADD) {
                // on ajoute ce référentiel car il a été ajouté depuis l'autre côté
                ToolkitIdTechnicalLabel referential = task.getReferential();
//                @SuppressWarnings("unchecked") R reference = ((DtoToReference<R>) referential).toReference(getDecoratorService().getReferentialLocale());
                references.put(referential.getId(), referential);
            }
            if (taskType == SynchronizeTaskType.UPDATE && task.getDifferential().getDifferentialType() == DifferentialType.DISABLED) {
                // on supprime ce référentiel car il a été désactivé depuis l'autre côté
                references.remove(task.getId());
            }

        }
        return new LinkedList<>(references.values());
    }

    public Map<Class<? extends BusinessDto>, Long> getUsageCount(LeftOrRight side, Class<? extends ReferentialDto> dtoType, String id) {
        return getSource(side).getUsageService().countReferential(ToolkitIdDtoBean.of(dtoType, id));
    }

    public <D extends ReferentialDto> Set<ToolkitIdLabel> getEnabledReferentialLabelSet(LeftOrRight side, Class<D> dtoType) {
        return getSource(side).getSynchronizeService().getEnabledReferentialLabelSet(dtoType);
    }

    public DifferentialModelBuilder newDifferentialModelBuilder() {
        return newDifferentialModelBuilder(sources);
    }

    public SynchronizeEngine newReferentialSynchronizeEngine() {
        return newReferentialSynchronizeEngine(sources);
    }

    private ObserveSwingDataSource getSource(LeftOrRight side) {
        return ObserveSwingDataSource.doOpenSource(sources.onSameSide(side));
    }

}
