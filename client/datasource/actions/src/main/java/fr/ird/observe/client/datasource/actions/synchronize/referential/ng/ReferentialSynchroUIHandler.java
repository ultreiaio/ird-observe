package fr.ird.observe.client.datasource.actions.synchronize.referential.ng;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.config.ConfigUI;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.util.UIHelper;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.ToolTipManager;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 02/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class ReferentialSynchroUIHandler extends AdminTabUIHandler<ReferentialSynchroUI> implements UIHandler<ReferentialSynchroUI> {

    @Override
    public void afterInit(ReferentialSynchroUI ui) {
        super.afterInit(ui);
        setAutoStart(ui.getStart());
        ui.getLeftTreePane().init();
        ui.getRightTreePane().init();
        ui.getStepModel().afterInit(parentUI);
        hideFixedPanelLabel(ui);
        ToolTipManager.sharedInstance().registerComponent(ui.getLeftTreePane().getTree());
        ToolTipManager.sharedInstance().registerComponent(ui.getRightTreePane().getTree());
    }

    @Override
    protected void initConfig(ConfigUI configUI) {
        super.initConfig(configUI);
        ReferentialSynchroConfigUI extraConfig = new ReferentialSynchroConfigUI(UIHelper.initialContext(configUI, this));
        configUI.getExtraConfig().add(extraConfig);
        configUI.getLeftSourceModel().setSourceLabel(t("observe.ui.datasource.storage.config.left.storage"));
        configUI.getRightSourceModel().setSourceLabel(t("observe.ui.datasource.storage.config.right.storage"));
        configUI.getStepModel().addPropertyChangeListener(StorageUIModel.VALID_STEP_PROPERTY_NAME, evt -> extraConfig.updateSynchroModes());
    }
}
