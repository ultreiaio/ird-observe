package fr.ird.observe.client.datasource.actions.synchronize.referential.ng;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialSynchroNodeSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTaskType;

import javax.swing.Icon;
import javax.swing.KeyStroke;
import java.util.function.Predicate;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created on 12/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum ReferentialSynchronizeResources {

    ADD(
            "copyToRight",
            "copyToLeft",
            n("observe.ui.datasource.editor.actions.synchro.referential.task.addToRight"),
            n("observe.ui.datasource.editor.actions.synchro.referential.task.addToLeft"),
            null,
            null,
            null,
            null,
            null,
            null,
            SynchronizeTaskType.ADD,
            ReferentialSynchroNodeSupport::isCanAdd),
    UPDATE(
            "copyToRight",
            "copyToLeft",
            n("observe.ui.datasource.editor.actions.synchro.referential.task.updateToRight"),
            n("observe.ui.datasource.editor.actions.synchro.referential.task.updateToLeft"),
            null,
            null,
            null,
            null,
            null,
            null,
            SynchronizeTaskType.UPDATE,
            ReferentialSynchroNodeSupport::isCanUpdate) {
    },
    COPY(
            "copyToRight",
            "copyToLeft",
            null,
            null,
            n("observe.ui.datasource.editor.actions.synchro.referential.action.copyToRight.tip"),
            n("observe.ui.datasource.editor.actions.synchro.referential.action.copyToLeft.tip"),
            ReferentialSynchroModel.COPY_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.COPY_RIGHT_PROPERTY_NAME,
            KeyStroke.getKeyStroke("pressed F1"),
            KeyStroke.getKeyStroke("shift pressed F1"),
            null,
            node -> false),
    DELETE(
            "deleteFromLeft",
            "deleteFromRight",
            n("observe.ui.datasource.editor.actions.synchro.referential.task.deleteFromLeft"),
            n("observe.ui.datasource.editor.actions.synchro.referential.task.deleteFromRight"),
            n("observe.ui.datasource.editor.actions.synchro.referential.action.deleteFromLeft.tip"),
            n("observe.ui.datasource.editor.actions.synchro.referential.action.deleteFromRight.tip"),
            ReferentialSynchroModel.DELETE_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.DELETE_RIGHT_PROPERTY_NAME,
            KeyStroke.getKeyStroke("pressed F5"),
            KeyStroke.getKeyStroke("shift pressed F5"),
            SynchronizeTaskType.DELETE,
            ReferentialSynchroNodeSupport::isCanDelete) {
        @Override
        public String getTaskReplaceLabel(boolean left) {
            if (left) {
                return n("observe.ui.datasource.editor.actions.synchro.referential.task.deleteFromLeftWithReplacement");
            }
            return n("observe.ui.datasource.editor.actions.synchro.referential.task.deleteFromRightWithReplacement");
        }
    },
    REVERT(
            "revertFromLeft",
            "revertFromRight",
            n("observe.ui.datasource.editor.actions.synchro.referential.task.revertFromLeft"),
            n("observe.ui.datasource.editor.actions.synchro.referential.task.revertFromRight"),
            n("observe.ui.datasource.editor.actions.synchro.referential.action.revertFromLeft.tip"),
            n("observe.ui.datasource.editor.actions.synchro.referential.action.revertFromRight.tip"),
            ReferentialSynchroModel.REVERT_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.REVERT_RIGHT_PROPERTY_NAME,
            KeyStroke.getKeyStroke("pressed F2"),
            KeyStroke.getKeyStroke("shift pressed F2"),
            SynchronizeTaskType.REVERT,
            ReferentialSynchroNodeSupport::isCanRevert),
    DEACTIVATE(
            "deactivateFromLeft",
            "deactivateFromRight",
            n("observe.ui.datasource.editor.actions.synchro.referential.task.deactivateFromLeft"),
            n("observe.ui.datasource.editor.actions.synchro.referential.task.deactivateFromRight"),
            n("observe.ui.datasource.editor.actions.synchro.referential.action.deactivateFromLeft.tip"),
            n("observe.ui.datasource.editor.actions.synchro.referential.action.deactivateFromRight.tip"),
            ReferentialSynchroModel.DEACTIVATE_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.DEACTIVATE_RIGHT_PROPERTY_NAME,
            KeyStroke.getKeyStroke("pressed F3"),
            KeyStroke.getKeyStroke("shift pressed F3"),
            SynchronizeTaskType.DEACTIVATE,
            ReferentialSynchroNodeSupport::isCanDelete),
    DEACTIVATE_WITH_REPLACEMENT(
            "deactivateFromLeft",
            "deactivateFromRight",
            n("observe.ui.datasource.editor.actions.synchro.referential.task.deactivateFromLeftWithReplacement"),
            n("observe.ui.datasource.editor.actions.synchro.referential.task.deactivateFromRightWithReplacement"),
            n("observe.ui.datasource.editor.actions.synchro.referential.action.deactivateFromLeftWithReplacement.tip"),
            n("observe.ui.datasource.editor.actions.synchro.referential.action.deactivateFromRightWithReplacement.tip"),
            ReferentialSynchroModel.DEACTIVATE_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.DEACTIVATE_RIGHT_PROPERTY_NAME,
            KeyStroke.getKeyStroke("pressed F4"),
            KeyStroke.getKeyStroke("shift pressed F4"),
            SynchronizeTaskType.DEACTIVATE_WITH_REPLACEMENT,
            ReferentialSynchroNodeSupport::isCanDelete);

    private final String leftActionName;
    private final String leftTaskI18nKey;
    private final String rightActionName;
    private final String leftActionTipI18nKey;
    private final String rightTaskI18nKey;
    private final String rightActionTipI18nKey;
    private final String leftPropertyName;
    private final String rightPropertyName;
    private final Predicate<ReferentialSynchroNodeSupport> predicate;
    private final SynchronizeTaskType taskType;
    private final KeyStroke leftKeyStroke;
    private final KeyStroke rightKeyStroke;
    private transient Icon actionLeftIcon;
    private transient Icon actionRightIcon;

    ReferentialSynchronizeResources(String leftActionName,
                                    String rightActionName,
                                    String leftTaskI18nKey,
                                    String rightTaskI18nKey,
                                    String leftActionTipI18nKey,
                                    String rightActionTipI18nKey,
                                    String leftPropertyName,
                                    String rightPropertyName,
                                    KeyStroke leftKeyStroke,
                                    KeyStroke rightKeyStroke,
                                    SynchronizeTaskType taskType,
                                    Predicate<ReferentialSynchroNodeSupport> predicate) {
        this.leftActionName = leftActionName;
        this.rightActionName = rightActionName;
        this.leftTaskI18nKey = leftTaskI18nKey;
        this.rightTaskI18nKey = rightTaskI18nKey;
        this.leftActionTipI18nKey = leftActionTipI18nKey;
        this.rightActionTipI18nKey = rightActionTipI18nKey;
        this.leftPropertyName = leftPropertyName;
        this.rightPropertyName = rightPropertyName;
        this.leftKeyStroke = leftKeyStroke;
        this.rightKeyStroke = rightKeyStroke;
        this.taskType = taskType;
        this.predicate = predicate;
    }

    public Predicate<ReferentialSynchroNodeSupport> getPredicate() {
        return predicate;
    }

    public Icon getIcon(boolean left) {
        if (actionLeftIcon == null) {
            actionLeftIcon = UIHelper.getUIManagerActionIcon(leftActionName);
            actionRightIcon = UIHelper.getUIManagerActionIcon(rightActionName);
        }
        return left ? actionLeftIcon : actionRightIcon;
    }

    public String getTaskLabel(boolean left) {
        return left ? leftTaskI18nKey : rightTaskI18nKey;
    }

    public String getTaskReplaceLabel(boolean left) {
        return getTaskLabel(left);
    }

    public String getActionTip(boolean left) {
        return left ? leftActionTipI18nKey : rightActionTipI18nKey;
    }

    public String getActionName(boolean left) {
        return left ? leftActionName : rightActionName;
    }

    public String getPropertyName(boolean left) {
        return left ? leftPropertyName : rightPropertyName;
    }

    public SynchronizeTaskType getTaskType() {
        return taskType;
    }

    public boolean withProperties() {
        return taskType != null && taskType.withProperties();
    }

    public KeyStroke getKeyStroke(boolean left) {
        return left ? leftKeyStroke : rightKeyStroke;
    }
}
