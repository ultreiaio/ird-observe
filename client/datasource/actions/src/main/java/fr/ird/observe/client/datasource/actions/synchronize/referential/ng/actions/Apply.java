package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchroUI;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task.ReferentialSynchronizeTaskListModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task.SwingReferentialSynchronizeTask;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task.SwingWithIncludedPropertyNamesTask;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModel;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.ToolkitIdTechnicalLabel;
import fr.ird.observe.services.service.referential.SynchronizeEngine;
import fr.ird.observe.services.service.referential.differential.Differential;
import fr.ird.observe.services.service.referential.synchro.BothSidesSqlRequestBuilder;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlRequest;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlResult;
import io.ultreia.java4all.util.LeftOrRightContext;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 12/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class Apply extends ReferentialSynchroUIActionSupport {

    public Apply() {
        super(t("observe.ui.action.apply"), t("observe.ui.action.apply"), "accept", 'S');
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ReferentialSynchroUI ui) {
        addAdminWorker("Application des modifications.", this::apply);
    }

    private WizardState apply() {

        ReferentialSynchroModel stepModel = ui.getStepModel();

        BothSidesSqlRequestBuilder builder =
                BothSidesSqlRequestBuilder.builder(stepModel.getTreeModels().apply(ReferentialSynchronizeTreeModel::getIdsOnlyExistingInThisSide));

        boolean showProperties = stepModel.isShowProperties();
        ReferentialSynchronizeTaskListModel tasks = stepModel.getTasks();

        boolean leftModified = false;
        boolean rightModified = false;

        Date newLastUpdateDate = new Date();

        Map<String, List<SwingReferentialSynchronizeTask>> tasksById = tasks.getTaskCache();
        for (Map.Entry<String, List<SwingReferentialSynchronizeTask>> entry : tasksById.entrySet()) {
            Collection<SwingReferentialSynchronizeTask> tasksForOneReferential = new LinkedList<>(entry.getValue());

            // First pass to process all unique tasks (insert, delete, deactivate)
            Iterator<SwingReferentialSynchronizeTask> iterator = tasksForOneReferential.iterator();
            while (iterator.hasNext()) {
                SwingReferentialSynchronizeTask task = iterator.next();
                if (task.isLeftModified()) {
                    leftModified = true;
                } else {
                    rightModified = true;
                }
                switch (task.getTaskType()) {
                    case ADD:
                        registerTask(builder, task, task.getReferential().getLastUpdateDate());
                        iterator.remove();
                        break;
                    case DELETE:
                        registerTask(builder, task, null);
                        iterator.remove();
                        break;
                    case DEACTIVATE:
                    case DEACTIVATE_WITH_REPLACEMENT:
                        registerTask(builder, task, newLastUpdateDate);
                        iterator.remove();
                        break;
                }
            }

            if (tasksForOneReferential.isEmpty()) {
                continue;
            }

            // Only update or revert

            SwingReferentialSynchronizeTask firstTask = tasksForOneReferential.iterator().next();
            Set<String> allProperties = ((SwingWithIncludedPropertyNamesTask) firstTask).getIncludedPropertyNames();

            Differential differential = firstTask.getDifferential();

            Date thisSideLastUpdateDate = differential.getThisSideDto().getLastUpdateDate();
            Date otherSideLastUpdateDate = differential.getOptionalOtherSideDto().map(ToolkitIdTechnicalLabel::getLastUpdateDate).orElseThrow();

            if (tasksForOneReferential.size() == 1) {

                // only one task
                boolean matchAllProperties = !showProperties || allProperties.size() == differential.getOptionalModifiedProperties().map(Set::size).orElse(0);
                Date lastUpdateDate = null;
                if (matchAllProperties) {

                    switch (firstTask.getTaskType()) {
                        case UPDATE:
                            // full update
                            // can use exactly this side dto last update date
                            lastUpdateDate = thisSideLastUpdateDate;

                            break;
                        case REVERT:
                            // full revert
                            // can use exactly other side dto last update date
                            lastUpdateDate = otherSideLastUpdateDate;
                            break;
                    }

                    registerTask(builder, firstTask, lastUpdateDate);
                    continue;
                }

            }

            // one or two tasks with partial update and partial revert

            for (SwingReferentialSynchronizeTask task : tasksForOneReferential) {

                Date lastUpdateDate = null;
                switch (task.getTaskType()) {
                    case UPDATE:
                        // partial update
                        // must be greater than other side dto last update date, but lesser than this side dto last update date
                        lastUpdateDate = new Date(otherSideLastUpdateDate.getTime() + allProperties.size());

                        break;
                    case REVERT:
                        // partial revert
                        // just increase last update date
                        lastUpdateDate = newLastUpdateDate;
                        break;
                }
                registerTask(builder, task, lastUpdateDate);
            }

        }
        LeftOrRightContext<OneSideSqlRequest> sqlRequest = builder.build();

        try (ObserveSwingDataSource leftSource = ObserveSwingDataSource.doOpenSource(stepModel.getLeftSource())) {
            try (ObserveSwingDataSource rightSource = ObserveSwingDataSource.doOpenSource(stepModel.getRightSource())) {
                SynchronizeEngine synchronizeEngine = stepModel.newReferentialSynchronizeEngine();
                LeftOrRightContext<OneSideSqlResult> sqlResult = synchronizeEngine.produceSql(sqlRequest);
                synchronizeEngine.executeSql(sqlResult);
                if (leftModified && leftSource.isLocal()) {
                    leftSource.setModified(true);
                }
                if (rightModified && rightSource.isLocal()) {
                    rightSource.setModified(true);
                }
            }
        }

        return WizardState.SUCCESSED;
    }

    private void registerTask(BothSidesSqlRequestBuilder builder, SwingReferentialSynchronizeTask task, Date lastUpdateDate) {

        Date thisSideLastUpdateDate = task.getDifferential().getThisSideDto().getLastUpdateDate();
        Date otherSideLastUpdateDate = task.getDifferential().getOptionalOtherSideDto().map(ToolkitIdTechnicalLabel::getLastUpdateDate).orElse(null);
        Date leftOldLastUpdateDate = task.isLeft() ? thisSideLastUpdateDate : otherSideLastUpdateDate;
        Date rightOldLastUpdateDate = task.isLeft() ? otherSideLastUpdateDate : thisSideLastUpdateDate;

        String noneValue = t("observe.Common.none");
        String taskLabel = t("observe.ui.datasource.editor.actions.synchro.referential.task.prepare", task.getStripDescription(), leftOldLastUpdateDate == null ? noneValue : leftOldLastUpdateDate, rightOldLastUpdateDate == null ? noneValue : rightOldLastUpdateDate, lastUpdateDate == null ? noneValue : lastUpdateDate);
        sendMessage(taskLabel);
        task.registerTask(builder, lastUpdateDate);
    }
}
