package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.synchronize.SynchronizeMode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchroUI;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTree;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTreePane;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModelsBuilder;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions.RegisterTaskActionSupport;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.services.service.referential.differential.DifferentialModelBuilder;
import io.ultreia.java4all.util.LeftOrRightContext;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;

import static io.ultreia.java4all.i18n.I18n.t;

public class Start extends ReferentialSynchroUIActionSupport {

    public Start() {
        super(t("observe.ui.datasource.editor.actions.synchro.referential.launch.operation"), t("observe.ui.datasource.editor.actions.synchro.referential.launch.operation"), "wizard-start", 'D');
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ReferentialSynchroUI ui) {
        addAdminWorker(getUi().getStart().getToolTipText(), this::doStartAction0);
    }

    private WizardState doStartAction0() {

        ReferentialSynchroModel stepModel = ui.getStepModel();

        try (ObserveSwingDataSource leftSource = ui.getModel().getConfigModel().getLeftSourceModel().getSafeSource(true)) {
            stepModel.setLeftSource(leftSource);

            try (ObserveSwingDataSource rightSource = ui.getModel().getConfigModel().getRightSourceModel().getSafeSource(true)) {
                stepModel.setRightSource(rightSource);
                DifferentialModelBuilder engine = stepModel.newDifferentialModelBuilder();
                SynchronizeMode synchronizeMode = stepModel.getSynchronizeMode();
                ProgressionModel progressionModel = stepModel.getProgressModel();
                ReferentialSynchronizeTreeModelsBuilder treeModelsBuilder = new ReferentialSynchronizeTreeModelsBuilder(synchronizeMode, stepModel.isShowProperties(), engine, progressionModel);
                LeftOrRightContext<ReferentialSynchronizeTreeModel> treePair = treeModelsBuilder.build();
                stepModel.setLeftTreeModel(treePair.left());
                stepModel.setRightTreeModel(treePair.right());
            }
        }

        stepModel.getTasks().removeAllElements();

        AdminUIModel model = ui.getModel();
        ReferentialSynchronizeTreeModel leftTreeModel = stepModel.getLeftTreeModel();
        initTree(ui.getLeftTreePane(),
                 leftTreeModel,
                 model.getConfigModel().getLeftSourceModel(),
                 t("observe.ui.datasource.editor.actions.synchro.referential.message.referential.leftData.loaded"),
                 evt -> RegisterTaskActionSupport.updateActions(stepModel, leftTreeModel));

        ReferentialSynchronizeTreeModel rightTreeModel = stepModel.getRightTreeModel();
        initTree(ui.getRightTreePane(),
                 rightTreeModel,
                 model.getConfigModel().getRightSourceModel(),
                 t("observe.ui.datasource.editor.actions.synchro.referential.message.referential.rightData.loaded"),
                 evt -> RegisterTaskActionSupport.updateActions(stepModel, rightTreeModel));
        ui.getModel().setWasDone(true);
        return WizardState.NEED_FIX;
    }

    private void initTree(ReferentialSelectionTreePane pane, ReferentialSynchronizeTreeModel treeModel, DataSourceSelectorModel dataSourceSelectorModel, String message, PropertyChangeListener listener) {
        ReferentialSelectionTree tree = pane.getTree();
        SynchronizeMode synchronizeMode = ui.getStepModel().getSynchronizeMode();

        pane.setContextValue(dataSourceSelectorModel);
        Color color =
                (treeModel.isLeft() && synchronizeMode.isLeftWrite() || !treeModel.isLeft() && synchronizeMode.isRightWrite()) ?
                        Color.BLACK : Color.RED;
        pane.setBorder(new TitledBorder(""));
        String label = dataSourceSelectorModel.getLabel();
        String labelWithUrl = dataSourceSelectorModel.getLabelWithUrl().substring(label.length() + 1);
        pane.getDataSourceLabel().setIcon(dataSourceSelectorModel.getSource().getIcon());
        pane.getDataSourceLabel().setText(dataSourceSelectorModel.getSource().getLabel() + " " + labelWithUrl);
        pane.getDataSourceLabel().setForeground(color);

        boolean isRight = pane.isRight();
        boolean canCopy = isRight ? synchronizeMode.isLeftWrite() : synchronizeMode.isRightWrite();
        boolean canRevert = isRight ? synchronizeMode.isRightWrite() : synchronizeMode.isLeftWrite();
        pane.getCopy().setVisible(canCopy);
        pane.getRevert().setVisible(canRevert);
        pane.getDeactivate().setVisible(canRevert);
        pane.getDeactivateWithReplace().setVisible(canRevert);
        pane.getDelete().setVisible(canRevert);

        tree.setModel(treeModel);
        treeModel.addPropertyChangeListener(ReferentialSynchronizeTreeModel.SELECTED_COUNT, listener);
        listener.propertyChange(null);
        sendMessage(message);
    }
}
