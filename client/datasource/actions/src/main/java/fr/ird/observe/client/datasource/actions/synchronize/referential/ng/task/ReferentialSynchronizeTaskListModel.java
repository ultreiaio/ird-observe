package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.referential.synchro.SynchronizeTaskType;

import javax.swing.DefaultListModel;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 13/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class ReferentialSynchronizeTaskListModel extends DefaultListModel<SwingReferentialSynchronizeTask> {

    private static final long serialVersionUID = 1L;
    private final Map<String, List<SwingReferentialSynchronizeTask>> taskCache;
    private boolean tasksIsAdjusting;

    public ReferentialSynchronizeTaskListModel() {
        this.taskCache = new LinkedHashMap<>();
    }

    public void addTasks(Collection<SwingReferentialSynchronizeTask> addedTasks) {
        if (addedTasks.isEmpty()) {
            return;
        }
        tasksIsAdjusting = true;
        try {
            int minIndex = size();
            int maxIndex = minIndex + addedTasks.size() - 1;
            ensureCapacity(maxIndex);
            for (SwingReferentialSynchronizeTask addedTask : addedTasks) {
                taskCache.computeIfAbsent(addedTask.getId(), k -> new LinkedList<>()).add(addedTask);
                addElement(addedTask);
            }
            super.fireIntervalAdded(this, minIndex, maxIndex);
        } finally {
            tasksIsAdjusting = false;
        }
    }

    public void removeTasks(Collection<SwingReferentialSynchronizeTask> removedTasks) {
        if (removedTasks.isEmpty()) {
            return;
        }
        for (SwingReferentialSynchronizeTask removedTask : removedTasks) {
            List<SwingReferentialSynchronizeTask> tasks = taskCache.get(removedTask.getId());
            if (tasks != null) {
                tasks.remove(removedTask);
            }
            removeElement(removedTask);
        }
    }

    @Override
    protected void fireIntervalAdded(Object source, int index0, int index1) {
        if (tasksIsAdjusting) {
            return;
        }
        super.fireIntervalAdded(source, index0, index1);
    }

    public Optional<SwingReferentialSynchronizeTask> getTask(String id, SynchronizeTaskType taskType) {
        List<SwingReferentialSynchronizeTask> tasks = taskCache.get(id);
        if (tasks != null) {
            for (SwingReferentialSynchronizeTask task : tasks) {
                if (Objects.equals(task.getTaskType(), taskType) && Objects.equals(id, task.getId())) {
                    return Optional.of(task);
                }
            }
        }
        return Optional.empty();
    }

    public Map<String, List<SwingReferentialSynchronizeTask>> getTaskCache() {
        return taskCache;
    }
}
