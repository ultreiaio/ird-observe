package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchronizeResources;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdTechnicalLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.differential.Differential;
import fr.ird.observe.services.service.referential.synchro.BothSidesSqlRequestBuilder;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTask;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTaskType;
import io.ultreia.java4all.util.LeftOrRight;

import javax.swing.Icon;
import java.util.Date;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 03/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class SwingReferentialSynchronizeTask  {

    private final String dataStr;
    private final String typeStr;
    private final String i18nKey;
    private final Differential differential;
    private final Icon icon;
    private final LeftOrRight side;
    private final SynchronizeTaskType taskType;
    public SwingReferentialSynchronizeTask(String i18nKey, Differential differential, Icon icon, LeftOrRight side, SynchronizeTaskType taskType) {
        this.i18nKey = i18nKey;
        this.differential = differential;
        this.icon = icon;
        this.side = side;
        this.taskType = taskType;
        this.dataStr = differential.getLabel();
        this.typeStr = t(I18nDecoratorHelper.getType(differential.getDtoType()));
    }

    public SwingReferentialSynchronizeTask(ReferentialSynchronizeResources resource, LeftOrRight side, Differential differential) {
        this(resource.getTaskLabel(side.onLeft()), differential, resource.getIcon(side.onLeft()), side, resource.getTaskType());
    }

    public final Differential getDifferential() {
        return differential;
    }

    public final boolean isLeft() {
        return side.onLeft();
    }

    public final boolean isLeftModified() {
        if (isLeft()) {
            return !taskType.toOtherSide();
        }
        return taskType.toOtherSide();
    }

    public final SynchronizeTaskType getTaskType() {
        return taskType;
    }

    public final String getDataStr() {
        return dataStr;
    }

    public final String getTypeStr() {
        return typeStr;
    }

    public final String getI18nKey() {
        return i18nKey;
    }

    public final Icon getIcon() {
        return icon;
    }

    public LeftOrRight getSide() {
        return side;
    }

    public Class<? extends ReferentialDto> getType() {
        return getDifferential().getDtoType();
    }

    public ToolkitIdTechnicalLabel getReferential() {
        return getDifferential().getThisSideDto();
    }

    public String getLabel() {
        return "<html><body>" + getDefaultLabel();
    }

    public String getDescription() {
        return "<html><body>" + getDefaultLabel();
    }

    public String getDefaultLabel() {
        return t(getI18nKey(), getTypeStr(), getDataStr());
    }

    public SynchronizeTask toTask(Date lastUpdateDate) {
        return SynchronizeTask.create(getDifferential().getDtoType(), getId(), lastUpdateDate);
    }

    public final void registerTask(BothSidesSqlRequestBuilder builder, Date lastUpdateDate) {
        builder.addTask(getSide(), taskType, toTask(lastUpdateDate));
    }

    public String getStripDescription() {
        return getDescription().replaceAll("<li>", "\n  * ").replaceAll("<[^>]+>", "");
    }

    public String getId() {
        return getReferential().getId();
    }

}
