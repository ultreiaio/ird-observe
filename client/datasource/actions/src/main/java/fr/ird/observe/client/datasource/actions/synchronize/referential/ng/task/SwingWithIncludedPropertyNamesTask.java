package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchronizeResources;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.differential.Differential;
import fr.ird.observe.services.service.referential.differential.DifferentialProperty;
import fr.ird.observe.services.service.referential.differential.DifferentialPropertyList;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTask;
import io.ultreia.java4all.util.LeftOrRight;

import java.util.Date;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.3.0
 */
public class SwingWithIncludedPropertyNamesTask extends SwingReferentialSynchronizeTask {


    private final Set<String> includedPropertyNames;
    private final DifferentialPropertyList includedProperties;

    public SwingWithIncludedPropertyNamesTask(ReferentialSynchronizeResources resource, LeftOrRight side, Differential differential, Set<String> includedPropertyNames, DifferentialPropertyList includedProperties) {
        super(resource, side, differential);
        this.includedPropertyNames = includedPropertyNames;
        this.includedProperties = includedProperties;
    }

    public Set<String> getIncludedPropertyNames() {
        return includedPropertyNames;
    }

    public DifferentialPropertyList getIncludedProperties() {
        return includedProperties;
    }

    @Override
    public String getLabel() {
        String result = super.getLabel();
        if (includedProperties != null) {
            result += String.format(" ( %s )", t("observe.ui.datasource.editor.actions.synchro.referential.task.include.properties", "<b><small>" + String.join(", ", includedProperties.getPropertyNameLabels(getDifferential().getDtoType())) + "</small></b>"));
        }
        return result;
    }

    public String getDescription() {
        StringBuilder result = new StringBuilder(super.getDescription());
        if (includedProperties != null) {
            result.append("<ul>");
            for (DifferentialProperty entry : includedProperties.getProperties()) {
                String propertyLabel = entry.getPropertyNameLabel(getDifferential().getDtoType());
                String oldValue = entry.getOldValueLabel();
                String newValue = entry.getNewValueLabel();
                result.append(String.format("<li>%s: <b><small>%s</small></b> → <b><small>%s</small></b></li>", propertyLabel, oldValue, newValue));
            }
        }
        return result.toString();
    }

    @Override
    public SynchronizeTask toTask(Date lastUpdateDate) {
        Class<? extends ReferentialDto> dtoType = getDifferential().getDtoType();
        return SynchronizeTask.createWithProperties(dtoType, getId(), lastUpdateDate, getIncludedPropertyNames(), getIncludedProperties());
    }
}
