package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchronizeResources;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.services.service.referential.differential.Differential;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTask;
import io.ultreia.java4all.util.LeftOrRight;

import java.util.Date;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class SwingWithReplaceDataTask extends SwingReferentialSynchronizeTask {

    private final ToolkitIdLabel replaceData;
    private final String replaceDataStr;

    public SwingWithReplaceDataTask(ReferentialSynchronizeResources resource, LeftOrRight side, Differential differential, ToolkitIdLabel replaceData) {
        super(resource.getTaskReplaceLabel(side.onLeft()), differential, resource.getIcon(side.onLeft()), side, resource.getTaskType());
        this.replaceData = replaceData;
        this.replaceDataStr = replaceData.toString();
    }

    public ToolkitIdLabel getReplaceData() {
        return replaceData;
    }

    public String getReplaceDataStr() {
        return replaceDataStr;
    }

    @Override
    public String getDefaultLabel() {
        return t(getI18nKey(), getTypeStr(), getDataStr(), getReplaceDataStr());
    }

    @Override
    public SynchronizeTask toTask(Date lastUpdateDate) {
        return SynchronizeTask.createWithReplace(getDifferential().getDtoType(), getId(), lastUpdateDate, getReplaceData().getId());
    }
}
