package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.RootSynchroNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.SynchroNodeSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.navigation.tree.JXTreeWithNoSearch;

import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class ReferentialSelectionTree extends JXTreeWithNoSearch {

    public ReferentialSelectionTree() {
        //FIXME:BodyContent Il faut changer ensuite le model...
        super(new ReferentialSynchronizeTreeModel(new RootSynchroNode(false, false, false), Map.of()));
        setLargeModel(true);
        setCellRenderer(new ReferentialSelectionTreeCellRenderer());
        setRootVisible(false);
        setRowHeight(30);
        getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        setMinimumSize(UIHelper.newMinDimension());
        setToggleClickCount(100);
        UIInitHelper.init(this);
    }

    public ReferentialSynchronizeTreeModel getTreeModel() {
        return (ReferentialSynchronizeTreeModel) getModel();
    }

    public Set<SynchroNodeSupport> getSelectedNodes() {
        TreePath[] selectedRow = getSelectionPaths();
        if (selectedRow == null || selectedRow.length == 0) {
            return Collections.emptySet();
        }
        Set<SynchroNodeSupport> result = new LinkedHashSet<>();
        for (TreePath treePath : selectedRow) {
            SynchroNodeSupport node = (SynchroNodeSupport) treePath.getLastPathComponent();
            result.add(node);
        }
        return result;
    }
}
