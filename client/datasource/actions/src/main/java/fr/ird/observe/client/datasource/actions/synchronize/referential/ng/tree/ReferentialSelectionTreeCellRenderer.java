package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialPropertyUpdatedNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.SynchroNodeSupport;
import org.jdesktop.swingx.tree.DefaultXTreeCellRenderer;

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class ReferentialSelectionTreeCellRenderer extends DefaultXTreeCellRenderer implements WithClientUIContextApi {

    private final JPanel panel;
    private final JCheckBox selected;
    private final Icon unselectedIcon;
    private final Icon partialIcon;

    ReferentialSelectionTreeCellRenderer() {
        selected = new JCheckBox();
        panel = new JPanel(new BorderLayout(2, 2));
        panel.setOpaque(false);
        panel.add(selected, BorderLayout.WEST);
        panel.add(this, BorderLayout.CENTER);
        partialIcon = Objects.requireNonNull(UIManager.getIcon("checkbox.partial"));
        unselectedIcon = Objects.requireNonNull(UIManager.getIcon("checkbox.empty"));
//        selected.setIcon(unselectedIcon);
        Icon selectedIcon = Objects.requireNonNull(UIManager.getIcon("checkbox.full"));
        selected.setSelectedIcon(selectedIcon);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {

        if (!(value instanceof SynchroNodeSupport)) {
            return this;
        }

        SynchroNodeSupport node = (SynchroNodeSupport) value;
        String text = node.toString(getDecoratorService().getReferentialLocale().getLocale());
        Icon icon = node.getIcon();

        super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);

        setIcon(icon);
        panel.setToolTipText(text);
        boolean selectedState;
        Icon selectedIcon = unselectedIcon;
        if (node.isLeaf()) {
            selectedState = node.isSelected();
        } else {
            // node is selected (says has the icon selected) only if all his children are selected
            selectedState = node.isFullySelected();
            if (!selectedState && node.isPartialSelected()) {
                // use partial icon, only if node is partial selected
                selectedIcon = partialIcon;
            }
        }
        selected.setSelected(selectedState);
        selected.setIcon(selectedIcon);
        selected.setEnabled(!(node instanceof ReferentialPropertyUpdatedNode) || node.getRoot().isShowProperties());
        panel.setPreferredSize(new Dimension(getPreferredSize().width + 20, getPreferredSize().height + 2));
        return panel;
    }
}
