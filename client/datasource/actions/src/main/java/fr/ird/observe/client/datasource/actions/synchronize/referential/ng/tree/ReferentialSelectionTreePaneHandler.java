package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions.RegisterCopy;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions.RegisterDeactivate;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions.RegisterDeactivateWithReplacement;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions.RegisterDelete;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions.RegisterRevert;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions.SelectUnselect;
import fr.ird.observe.client.util.init.DefaultUIInitializer;
import io.ultreia.java4all.util.LeftOrRight;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.awt.BorderLayout;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class ReferentialSelectionTreePaneHandler implements UIHandler<ReferentialSelectionTreePane> {

    @Override
    public void afterInit(ReferentialSelectionTreePane ui) {
        DefaultUIInitializer.doInit(ui);
    }

    void init(ReferentialSelectionTreePane ui) {
        boolean isLeft = ui.isLeft();
        LeftOrRight side = isLeft ? LeftOrRight.LEFT : LeftOrRight.RIGHT;
        RegisterCopy.init(ui, ui.getCopy(), new RegisterCopy(ui, side));
        RegisterRevert.init(ui, ui.getRevert(), new RegisterRevert(ui, side));
        RegisterDeactivate.init(ui, ui.getDeactivate(), new RegisterDeactivate(ui, side));
        RegisterDeactivateWithReplacement.init(ui, ui.getDeactivateWithReplace(), new RegisterDeactivateWithReplacement(ui, side));
        RegisterDelete.init(ui, ui.getDelete(), new RegisterDelete(ui, side));
        SelectUnselect.init(ui, null, new SelectUnselect(ui));
        if (!isLeft) {
            ui.remove(ui.getMiddleActions());
            ui.add(ui.getMiddleActions(), BorderLayout.WEST);
        }
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2693
        // Do not allow to delete (need to review delete actions, since it may cause error in some case if for example
        // an update is asked using the deleted data...)
        ui.getMiddleActions().remove(ui.delete);
    }
}

