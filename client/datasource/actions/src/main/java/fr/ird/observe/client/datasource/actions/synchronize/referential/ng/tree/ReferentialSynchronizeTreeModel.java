package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialPropertyUpdatedNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialSynchroNodeSupport;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialTypeSynchroNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialUpdatedSynchroNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.RootSynchroNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.SynchroNodeSupport;
import fr.ird.observe.dto.referential.ReferentialDto;

import javax.swing.tree.DefaultTreeModel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Created on 10/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferentialSynchronizeTreeModel extends DefaultTreeModel {

    public static final String SELECTED_COUNT = "selectedCount";
    public static final String SELECTED = "selected";
    private static final long serialVersionUID = 1L;
    private static final String SELECTION_EMPTY = "selectionEmpty";
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    private final Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistOnThisSide;

    /**
     * List of selected referential nodes.
     *
     * <b>Be ware, a node present here means it is selected or partial selected!</b>
     */
    private final List<ReferentialSynchroNodeSupport> selected;
    private boolean canAdd;
    private boolean canUpdate;
    private boolean canDelete;
    private boolean canRevert;

    private boolean adjusting;
    private int oldSelectedCount;

    ReferentialSynchronizeTreeModel(RootSynchroNode root, Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistOnThisSide) {
        super(root);
        this.idsOnlyExistOnThisSide = idsOnlyExistOnThisSide;
        this.selected = new LinkedList<>();
    }

    public void setValueAt(SynchroNodeSupport node, boolean value) {
        boolean wasAdjusting = adjusting;
        if (!wasAdjusting) {
            setAdjusting(true);
        }
        try {
            if (node instanceof ReferentialTypeSynchroNode) {
                ReferentialTypeSynchroNode node1 = (ReferentialTypeSynchroNode) node;
                node1.updateSelect(value);
                Enumeration<?> children = node1.children();
                while (children.hasMoreElements()) {
                    ReferentialSynchroNodeSupport childNode = (ReferentialSynchroNodeSupport) children.nextElement();
                    updateSelected(value, childNode);
                }
            } else if (node instanceof ReferentialSynchroNodeSupport) {
                ReferentialSynchroNodeSupport node1 = (ReferentialSynchroNodeSupport) node;
                node1.updateSelect(value);
                updateSelected(value, node1);
            } else if (node instanceof ReferentialPropertyUpdatedNode) {
                ReferentialPropertyUpdatedNode node1 = (ReferentialPropertyUpdatedNode) node;
                node1.updateSelect(value);
                updateSelected(node1.getParent().isSelected(), node1.getParent());
            }
        } finally {
            if (!wasAdjusting) {
                setAdjusting(false);
            }
        }
    }

    public int getSelectedCount() {
        return selected.size();
    }

    public boolean isSelectionEmpty() {
        return selected.isEmpty();
    }

    public List<ReferentialSynchroNodeSupport> getSelected() {
        return selected;
    }

    public Map<Class<? extends ReferentialDto>, Set<String>> getIdsOnlyExistingInThisSide() {
        return idsOnlyExistOnThisSide;
    }

    public synchronized void updateSelectedActions() {
        canAdd = canUpdate = canDelete = canRevert = false;
        for (ReferentialSynchroNodeSupport node : selected) {
            canAdd |= node.isCanAdd();
            canDelete |= node.isCanDelete();
            boolean canUpdate = node.isCanUpdate();
            boolean canRevert = node.isCanRevert();
            if ((canUpdate || canRevert) && node instanceof ReferentialUpdatedSynchroNode) {
                // in this special cas, ask if node is really selected (means not without selected children)
                canUpdate = canRevert = !node.isNotSelected();
            }
            this.canUpdate |= canUpdate;
            this.canRevert |= canRevert;
        }
    }

    public Collection<ReferentialSynchroNodeSupport> filterSelectedReferenceNodes(Predicate<ReferentialSynchroNodeSupport> predicate) {
        Set<ReferentialSynchroNodeSupport> result = new LinkedHashSet<>();
        for (ReferentialSynchroNodeSupport node : selected) {
            if (predicate.test(node)) {
                result.add(node);
            }
        }
        return result;
    }

    public boolean isCanAdd() {
        return canAdd;
    }

    public boolean isCanUpdate() {
        return canUpdate;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public boolean isCanRevert() {
        return canRevert;
    }

    public boolean isCanSkip() {
        return !isSelectionEmpty();
    }

    public boolean isLeft() {
        return getRoot().isLeft();
    }

    public void clearSelection() {
        setAdjusting(true);
        try {
            for (ReferentialSynchroNodeSupport node : selected) {
                node.updateSelect(false);
                nodeChanged(node);
                ReferentialTypeSynchroNode parent = node.getParent();
                if (parent != null) {
                    nodeChanged(parent);
                }
            }
            selected.clear();
        } finally {
            setAdjusting(false);
        }
    }

    public void removeReferenceNodes(Collection<SynchroNodeSupport> removedNodes) {
        setAdjusting(true);
        try {
            for (SynchroNodeSupport removedNode : removedNodes) {
                if (removedNode instanceof ReferentialPropertyUpdatedNode) {
                    removedNode.updateSelect(false);
                    removeNodeFromParent(removedNode);
                    continue;
                }

                if (removedNode instanceof ReferentialSynchroNodeSupport) {
                    ReferentialSynchroNodeSupport removedNode1 = (ReferentialSynchroNodeSupport) removedNode;
                    removedNode1.updateSelect(false);
                    selected.remove(removedNode1);
                    ReferentialTypeSynchroNode typeNode = removedNode1.getParent();
                    if (typeNode.getChildCount() == 1) {
                        removeNodeFromParent(typeNode);
                    } else {
                        removeNodeFromParent(removedNode);
                    }
                }
            }
        } finally {
            setAdjusting(false);
        }
    }

    @Override
    public RootSynchroNode getRoot() {
        return (RootSynchroNode) super.getRoot();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(name, listener);
    }

    public void removePropertyChangeListener(String name, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(name, listener);
    }

    private void updateSelected(boolean value, ReferentialSynchroNodeSupport node) {
        if (value) {
            selected.add(node);
        } else {
            selected.remove(node);
        }
    }

    private void fireSelectedChanged(boolean oldSelectionEmpty, int oldSelectedCount) {
        if (adjusting) {
            return;
        }
        updateSelectedActions();
        pcs.firePropertyChange(SELECTION_EMPTY, oldSelectionEmpty, isSelectionEmpty());
        pcs.firePropertyChange(SELECTED_COUNT, null, getSelectedCount());
        pcs.firePropertyChange(SELECTED, null, selected);
    }

    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
        if (adjusting) {
            oldSelectedCount = getSelectedCount();
        } else {
            fireSelectedChanged(oldSelectedCount == 0, oldSelectedCount);
        }
    }
}
