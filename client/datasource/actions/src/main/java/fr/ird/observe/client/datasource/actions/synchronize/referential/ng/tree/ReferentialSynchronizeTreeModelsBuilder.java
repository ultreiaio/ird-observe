package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.actions.synchronize.SynchronizeMode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialAddedSynchroNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialPropertyUpdatedNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialSynchroNodeSupport;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialTypeSynchroNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialUpdatedSynchroNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.RootSynchroNode;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.differential.Differential;
import fr.ird.observe.services.service.referential.differential.DifferentialList;
import fr.ird.observe.services.service.referential.differential.DifferentialModelBuilder;
import fr.ird.observe.services.service.referential.differential.DifferentialProperty;
import fr.ird.observe.services.service.referential.differential.DifferentialPropertyList;
import fr.ird.observe.services.service.referential.differential.DifferentialType;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.util.LeftOrRightContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created on 11/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferentialSynchronizeTreeModelsBuilder implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(ReferentialSynchronizeTreeModelsBuilder.class);

    private final DifferentialModelBuilder engine;
    private final RootSynchroNode leftRootNode;
    private final RootSynchroNode rightRootNode;
    private final ProgressionModel progressionModel;

    private static class CreateAddNode {

        private final boolean canCopy;
        private final boolean canDelete;

        private final Map<Class<? extends ReferentialDto>, Set<String>> idsBuilder = new LinkedHashMap<>();

        protected CreateAddNode(boolean canCopy, boolean canDelete) {
            this.canCopy = canCopy;
            this.canDelete = canDelete;
        }

        public void createNode(ReferentialTypeSynchroNode typeNode, Differential diffState) {
            ReferentialSynchroNodeSupport node = new ReferentialAddedSynchroNode(diffState, canCopy, canDelete);
            typeNode.add(node);
            idsBuilder.computeIfAbsent(diffState.getDtoType(), k -> new TreeSet<>()).add(diffState.getId());
        }

        public Map<Class<? extends ReferentialDto>, Set<String>> getIds() {
            return Collections.unmodifiableMap(idsBuilder);
        }
    }

    private static class CreateUpdateNode implements WithClientUIContextApi {

        private final boolean canCopy;
        private final boolean canRevert;

        protected CreateUpdateNode(boolean canCopy, boolean canRevert) {
            this.canCopy = canCopy;
            this.canRevert = canRevert;
        }

        public void createNode(ReferentialTypeSynchroNode typeNode, Differential diffState) {

            DifferentialPropertyList modifiedProperties = null;
            if (canCopy || canRevert) {

                modifiedProperties = diffState.getPropertiesModification();
                if (modifiedProperties == null || modifiedProperties.isEmpty()) {
                    // In this case the node can not be added
                    log.warn(String.format("Skip reference (no real property modified on %s)", diffState));
                    return;
                }
                log.debug(String.format("Modified properties: %s on %s", modifiedProperties, diffState.getId()));
            }
            ReferentialSynchroNodeSupport node = new ReferentialUpdatedSynchroNode(diffState, canCopy, canRevert);
            typeNode.add(node);

            if (modifiedProperties != null && !modifiedProperties.isEmpty()) {
                DifferentialPropertyList propertyNameByLabel = diffState.getPropertiesModification();
                boolean left = typeNode.getRoot().isLeft();
                for (DifferentialProperty entry : propertyNameByLabel.getProperties()) {
                    node.add(new ReferentialPropertyUpdatedNode(left, diffState.getDtoType(), entry));
                }
            }
        }

    }


    public ReferentialSynchronizeTreeModelsBuilder(SynchronizeMode synchronizeMode, boolean showProperties, DifferentialModelBuilder engine, ProgressionModel progressionModel) {
        this.engine = Objects.requireNonNull(engine);
        this.leftRootNode = new RootSynchroNode(true, Objects.requireNonNull(synchronizeMode).isLeftWrite(), showProperties);
        this.rightRootNode = new RootSynchroNode(false, synchronizeMode.isRightWrite(), showProperties);
        this.progressionModel = progressionModel;
    }

    private void create(RootSynchroNode rootNode, CreateAddNode addNode, CreateUpdateNode updateNode, DifferentialList differentialList) {

        DecoratorService decoratorService = getDecoratorService();
        Locale locale = decoratorService.getReferentialLocale().getLocale();
        Map<Class<? extends ReferentialDto>, DifferentialList> statesByType = differentialList.getStatesByType();
        List<Class<? extends ReferentialDto>> referentialNames = ObserveBusinessProject.get().sortReferentialTypesWithPackage(locale, statesByType.keySet());

        for (Class<? extends ReferentialDto> dtoType : referentialNames) {
            DifferentialList differentials = statesByType.get(dtoType);
            if (differentialList.getStates().isEmpty()) {
                continue;
            }
            ReferentialTypeSynchroNode typeNode = rootNode.getOrAddTypeNode(dtoType);
            Decorator decorator = decoratorService.getToolkitIdLabelDecoratorByType(dtoType);
            List<Differential> addDifferentials = new LinkedList<>();
            List<Differential> updateDifferentials = new LinkedList<>();
            for (Differential differential : differentials.getStates()) {
                if (differential.getDifferentialType() == DifferentialType.ADDED) {
                    addDifferentials.add(differential);
                } else {
                    updateDifferentials.add(differential);
                }
                differential.getThisSideDto().registerDecorator(decorator);
                differential.getOptionalOtherSideDto().ifPresent(d -> d.registerDecorator(decorator));
            }

            if (!addDifferentials.isEmpty()) {
                addAdd(typeNode, addDifferentials, addNode);
            }
            if (!updateDifferentials.isEmpty()) {
                addUpdate(typeNode, updateDifferentials, updateNode);
            }

        }

    }

    public LeftOrRightContext< ReferentialSynchronizeTreeModel> build() {
        LeftOrRightContext<DifferentialList> synchronizeDiffs = engine.build(progressionModel);
        DifferentialList leftDiff = synchronizeDiffs.left();
        DifferentialList rightDiff = synchronizeDiffs.right();

        boolean rightCanWrite = rightRootNode.isCanWrite();
        boolean leftCanWrite = leftRootNode.isCanWrite();

        CreateAddNode leftAddNode = new CreateAddNode(rightCanWrite, leftCanWrite);
        CreateUpdateNode leftUpdateNode = new CreateUpdateNode(rightCanWrite, leftCanWrite);
        create(leftRootNode, leftAddNode, leftUpdateNode, leftDiff);

        CreateAddNode rightAddNode = new CreateAddNode(leftCanWrite, rightCanWrite);
        CreateUpdateNode rightUpdateNode = new CreateUpdateNode(leftCanWrite, rightCanWrite);
        create(rightRootNode, rightAddNode, rightUpdateNode, rightDiff);

        ReferentialSynchronizeTreeModel leftTreeModel = new ReferentialSynchronizeTreeModel(leftRootNode, leftAddNode.getIds());
        ReferentialSynchronizeTreeModel rightTreeModel = new ReferentialSynchronizeTreeModel(rightRootNode, rightAddNode.getIds());
        return LeftOrRightContext.of(leftTreeModel, rightTreeModel);
    }

    private void addAdd(ReferentialTypeSynchroNode typeNode, List<Differential> diffStates, CreateAddNode createNode) {
        for (Differential diffState : diffStates) {
            createNode.createNode(typeNode, diffState);
        }
    }

    private void addUpdate(ReferentialTypeSynchroNode typeNode, List<Differential> diffStates, CreateUpdateNode createNode) {
        for (Differential diffState : diffStates) {
            createNode.createNode(typeNode, diffState);
        }
    }

}
