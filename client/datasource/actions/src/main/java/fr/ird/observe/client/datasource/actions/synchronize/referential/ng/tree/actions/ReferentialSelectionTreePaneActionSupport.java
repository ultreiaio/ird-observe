package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTreePane;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;

import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public abstract class ReferentialSelectionTreePaneActionSupport extends JComponentActionSupport<ReferentialSelectionTreePane> {
    ReferentialSelectionTreePaneActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    @Override
    public void init() {
        if (ui.isRight()) {
            KeyStroke acceleratorKey = getAcceleratorKey();
            setKeyStroke(KeyStroke.getKeyStroke(acceleratorKey.getKeyCode(), acceleratorKey.getModifiers() | KeyEvent.SHIFT_MASK));
        }
        super.init();
    }
}
