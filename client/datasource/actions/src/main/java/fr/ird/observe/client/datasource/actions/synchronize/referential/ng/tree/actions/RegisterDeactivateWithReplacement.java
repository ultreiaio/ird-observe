package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchronizeResources;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTreePane;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.LeftOrRight;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.4.0
 */
public class RegisterDeactivateWithReplacement extends RegisterTaskActionSupport {
    public RegisterDeactivateWithReplacement(ReferentialSelectionTreePane ui, LeftOrRight side) {
        super(ui, ReferentialSynchronizeResources.DEACTIVATE_WITH_REPLACEMENT, side);
    }

    protected boolean shouldReplace(Class<? extends ReferentialDto> dtoType, ToolkitIdLabel reference) {
        // always ask a replacement for this task
        return true;
    }

    @Override
    protected String getReplaceTitle(ToolkitIdLabel reference) {
        return t("observe.ui.datasource.editor.actions.synchro.referential.replaceBeforeDeactivate.title", typeStr, referenceStr);
    }

}
