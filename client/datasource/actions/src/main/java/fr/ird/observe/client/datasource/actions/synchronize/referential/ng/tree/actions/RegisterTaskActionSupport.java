package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialReplaceUI;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialReplaceUIHandler;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchroModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchroUI;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchronizeResources;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task.SwingReferentialSynchronizeTask;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task.SwingWithIncludedPropertyNamesTask;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.task.SwingWithReplaceDataTask;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTree;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTreePane;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialPropertyUpdatedNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialSynchroNodeSupport;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialUpdatedSynchroNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.SynchroNodeSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.differential.Differential;
import fr.ird.observe.services.service.referential.differential.DifferentialPropertyList;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTaskType;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.LeftOrRight;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;

import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.4.0
 */
public abstract class RegisterTaskActionSupport extends ReferentialSelectionTreePaneActionSupport implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(RegisterTaskActionSupport.class);
    protected final ReferentialSynchroUI parentUI;
    final Predicate<ReferentialSynchroNodeSupport> predicate;
    final ReferentialSelectionTreePane ui;
    private final LeftOrRight side;
    private final SynchronizeTaskType taskType;
    private final ReferentialSynchronizeResources resource;
    String typeStr;
    String referenceStr;

    public static void updateActions(ReferentialSynchroModel stepModel, ReferentialSynchronizeTreeModel treeModel) {
        treeModel.updateSelectedActions();
        if (treeModel.isLeft()) {
            stepModel.setCopyLeft(treeModel.isCanAdd() || treeModel.isCanUpdate());
            stepModel.setDeleteLeft(treeModel.isCanDelete());
            stepModel.setDeactivateLeft(treeModel.isCanDelete());
            stepModel.setDeactivateWithReplaceLeft(treeModel.isCanDelete());
            stepModel.setRevertLeft(treeModel.isCanRevert());
        } else {
            stepModel.setCopyRight(treeModel.isCanAdd() || treeModel.isCanUpdate());
            stepModel.setDeleteRight(treeModel.isCanDelete());
            stepModel.setDeactivateRight(treeModel.isCanDelete());
            stepModel.setDeactivateWithReplaceRight(treeModel.isCanDelete());
            stepModel.setRevertRight(treeModel.isCanRevert());
        }
    }

    RegisterTaskActionSupport(ReferentialSelectionTreePane ui, ReferentialSynchronizeResources resource,  LeftOrRight side) {
        super("", null, null, Objects.requireNonNull(resource).getKeyStroke(side.onLeft()));
        this.resource = resource;
        boolean left = side.onLeft();
        String tip = resource.getActionTip(left);
        if (tip != null) {
            putValue(SHORT_DESCRIPTION, t(tip));
        }
        String actionName = resource.getActionName(left);
        if (actionName != null) {
            putValue(Action.SMALL_ICON, UIHelper.getUIManagerActionIcon(actionName));
        }
        this.ui = ui;
        this.side = side;
        this.taskType = resource.getTaskType();
//        this.needReplace = resource.withReplace();
        parentUI = ui.getContextValue(ReferentialSynchroUI.class, "synchroParent");

        Predicate<ReferentialSynchroNodeSupport> predicate = resource.getPredicate();

        if (resource.withProperties()) {
            predicate = predicate.and(n -> {
                if (parentUI.getStepModel().isShowProperties() && n instanceof ReferentialUpdatedSynchroNode) {
                    ReferentialUpdatedSynchroNode node1 = (ReferentialUpdatedSynchroNode) n;
                    Optional<Set<String>> selectedPropertyNames = node1.getPropertyNamesSelected();
                    return selectedPropertyNames.map(t -> !t.isEmpty()).orElse(false);
                }
                return true;
            });
        }
        this.predicate = predicate;
        String propertyName = resource.getPropertyName(left);
        if (propertyName != null) {
            parentUI.getModel().getReferentialSynchroModel().addPropertyChangeListener(propertyName, evt -> setEnabled((Boolean) evt.getNewValue()));
        }
    }

    protected final SwingReferentialSynchronizeTask createTask(LeftOrRight side, Differential differential, ToolkitIdLabel replaceReference, Set<String> includedPropertyNames, DifferentialPropertyList includedModifiedProperties) {
        ReferentialSynchronizeResources resource = getResource();
        if (replaceReference != null) {
            return new SwingWithReplaceDataTask(resource, side, differential, replaceReference);
        }
        if (resource.withProperties()) {
            return new SwingWithIncludedPropertyNamesTask(resource, side, differential, includedPropertyNames, includedModifiedProperties);

        }
        return new SwingReferentialSynchronizeTask(resource, side, differential);
    }

    public ReferentialSynchronizeResources getResource() {
        return resource;
    }

    public boolean isLeft() {
        return side.onLeft();
    }

    public LeftOrRight getSide() {
        return side;
    }

    protected String getReplaceTitle(ToolkitIdLabel reference) {
        throw new NotImplementedException("");
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ReferentialSelectionTreePane referentialSelectionTreePane) {
        ReferentialSynchronizeTreeModel treeModel = getTreeModel();
        createTasks();
        treeModel.clearSelection();
        updateActions(parentUI.getStepModel(), treeModel);
        SwingUtilities.invokeLater(getTree()::repaint);
    }

    public Predicate<ReferentialSynchroNodeSupport> getPredicate() {
        return predicate;
    }

    protected void createTasks() {
        createTasks(getTreeModel(), getPredicate());
    }

    protected Collection<ReferentialSynchroNodeSupport> getReferenceReferentialSynchroNodes(Predicate<ReferentialSynchroNodeSupport> predicate) {
        return getTreeModel().filterSelectedReferenceNodes(predicate);
    }

    protected ReferentialSynchronizeTreeModel getTreeModel() {
        return ui.getTree().getTreeModel();
    }

    protected ReferentialSelectionTree getTree() {
        return ui.getTree();
    }

    private void createTasks(ReferentialSynchronizeTreeModel treeModel, Predicate<ReferentialSynchroNodeSupport> predicate) {

        Collection<SynchroNodeSupport> removedNodes = new LinkedList<>();
        Collection<SwingReferentialSynchronizeTask> addedTasks = new LinkedList<>();
        Collection<SwingReferentialSynchronizeTask> removedTasks = new LinkedList<>();

        getReferenceReferentialSynchroNodes(predicate).forEach(node -> process(node, addedTasks, removedTasks, removedNodes));

        treeModel.removeReferenceNodes(removedNodes);
        ReferentialSynchroModel stepModel = parentUI.getStepModel();
        stepModel.getTasks().addTasks(addedTasks);
        stepModel.getTasks().removeTasks(removedTasks);
        if (!stepModel.getTasks().isEmpty()) {
//            parentUI.getApply().setEnabled(true);
            SwingUtilities.invokeLater(() -> parentUI.getActionsToConsume().setSelectedValue(stepModel.getTasks().lastElement(), true));
        }
    }

    protected boolean shouldReplace(Class<? extends ReferentialDto> dtoType, ToolkitIdLabel reference) {
        // by default never ask for a replacement
        return false;
    }

    public void process(ReferentialSynchroNodeSupport node,
                        Collection<SwingReferentialSynchronizeTask> addedTasks,
                        Collection<SwingReferentialSynchronizeTask> removedTasks,
                        Collection<SynchroNodeSupport> removedNodes) {

        ToolkitIdLabel reference = node.getReferentialLabel();
        Class<? extends ReferentialDto> dtoType = node.getUserObject().getDtoType();
        ToolkitIdLabel replaceReference = null;

        if (shouldReplace(dtoType, reference)) {
            Decorator decorator = getDecoratorService().getToolkitIdLabelDecoratorByType(dtoType);
            List<ToolkitIdLabel> references = new LinkedList<>(parentUI.getStepModel().getPossibleReplaceUniverse(getSide(), dtoType, reference));
            ReferentialReplaceUI replaceUI = new ReferentialReplaceUI(new JAXXInitialContext()
                                                                              .add(ReferentialReplaceUIHandler.CONTEXT_NAME, reference)
                                                                              .add(ReferentialReplaceUIHandler.CONTEXT_NAME, references)
                                                                              .add(ReferentialReplaceUIHandler.CONTEXT_NAME, decorator)
                                                                              .add(this));
            typeStr = t(I18nDecoratorHelper.getType(dtoType));
            referenceStr = reference.toString();
            replaceUI.getMessage().setText(t("observe.ui.datasource.editor.actions.synchro.referential.replaceBefore.message", typeStr, referenceStr));

            int response = UIHelper.askUser(
                    ui,
                    t(getReplaceTitle(reference), typeStr, referenceStr),
                    replaceUI,
                    JOptionPane.WARNING_MESSAGE,
                    new Object[]{I18n.t("observe.ui.choice.replace"), I18n.t("observe.ui.choice.cancel")},
                    1);

            switch (response) {
                case JOptionPane.CLOSED_OPTION:
                case 1:

                    break;
                case 0:
                    replaceReference = replaceUI.getReplaceReference();
                    break;
            }
            log.info(String.format("replaceReference: %s", replaceReference));
            if (replaceReference == null) {
                log.warn("Skip this task, no replace referential reference selected");
                return;
            }
            log.info(String.format("Selected replace referential reference: %s", replaceReference));
        }

        boolean removeNode = true;
        Set<String> includedPropertyNames = null;
        DifferentialPropertyList includedProperties = null;
        Set<ReferentialPropertyUpdatedNode> toRemove = new LinkedHashSet<>();
        Differential userObject = node.getUserObject();
        if (!node.isLeaf() && node instanceof ReferentialUpdatedSynchroNode) {
            ReferentialUpdatedSynchroNode node1 = (ReferentialUpdatedSynchroNode) node;
            includedPropertyNames = node1.getPropertyNamesSelected().orElse(null);
            if (includedPropertyNames != null) {

                Enumeration<?> children = node.children();
                while (children.hasMoreElements()) {
                    ReferentialPropertyUpdatedNode childNode = (ReferentialPropertyUpdatedNode) children.nextElement();
                    if (childNode.isSelected()) {
                        toRemove.add(childNode);
                    } else {
                        removeNode = false;
                    }
                }

                Optional<SwingReferentialSynchronizeTask> optionalTask = parentUI.getModel().getReferentialSynchroModel().getTasks().getTask(userObject.getId(), taskType);
                if (optionalTask.isPresent()) {
                    SwingWithIncludedPropertyNamesTask existingTask = (SwingWithIncludedPropertyNamesTask) optionalTask.get();
                    log.info(String.format("Will merge with existing task: %s", existingTask));
                    removedTasks.add(existingTask);
                    includedPropertyNames.addAll(existingTask.getIncludedPropertyNames());
                    // Resort property names to keep same order that defined in Differential
                    Set<String> includedPropertyNames2 = new LinkedHashSet<>(node.getUserObject().getOptionalModifiedProperties().orElse(new LinkedHashSet<>()));
                    includedPropertyNames2.retainAll(includedPropertyNames);
                    includedPropertyNames = includedPropertyNames2;
                }
            }
            // if task is on this side, it means a revert, so flip properties
            boolean flip = taskType != null && !taskType.toOtherSide();
            includedProperties = node1.getUserObject().getPropertiesModification(includedPropertyNames, flip);
        }
        log.debug(String.format("Selected properties: %s", includedPropertyNames));
        SwingReferentialSynchronizeTask task = createTask(getSide(), userObject, replaceReference, includedPropertyNames, includedProperties);

        String stripDescription = task.getStripDescription();
        log.info(String.format("Register task: %s", stripDescription));

        sendMessage(stripDescription);
        if (removeNode) {
            removedNodes.add(node);
        } else {
            removedNodes.addAll(toRemove);
        }
        addedTasks.add(task);
    }

    public String sendMessage(String message) {
        addMessage(message);
        return message;
    }

    public void addMessage(String text) {
        JTextArea progression = parentUI.getProgression();
        progression.append(text + "\n");
        progression.setCaretPosition(progression.getDocument().getLength());
    }

}
