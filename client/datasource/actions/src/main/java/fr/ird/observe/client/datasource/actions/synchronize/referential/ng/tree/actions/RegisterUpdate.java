package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.ReferentialSynchronizeResources;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTreePane;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialSynchroNodeSupport;
import io.ultreia.java4all.util.LeftOrRight;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.4.0
 */
public class RegisterUpdate extends RegisterTaskActionSupport {

    private transient Collection<ReferentialSynchroNodeSupport> nodes;

    public RegisterUpdate(ReferentialSelectionTreePane ui, LeftOrRight side) {
        super(ui, ReferentialSynchronizeResources.UPDATE, side);
    }

    @Override
    protected Collection<ReferentialSynchroNodeSupport> getReferenceReferentialSynchroNodes(Predicate<ReferentialSynchroNodeSupport> predicate) {
        return nodes;
    }

    public void updateNodes() {
        this.nodes = super.getReferenceReferentialSynchroNodes(predicate);
    }
}

