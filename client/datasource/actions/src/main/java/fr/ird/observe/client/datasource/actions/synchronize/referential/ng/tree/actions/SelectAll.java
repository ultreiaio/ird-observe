package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTree;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTreePane;

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.Collections;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class SelectAll extends ReferentialSelectionTreePaneActionSupport {

    public SelectAll() {
        super("", t("observe.ui.tree.action.selectAll.tip"), "select", ObserveKeyStrokesActions.KEY_STROKE_SELECT_ALL_TREE);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ReferentialSelectionTreePane ui) {
        ReferentialSelectionTree tree = ui.getTree();
        SelectUnselect.selectUnSelect(tree, Collections.singleton(tree.getTreeModel().getRoot()), n -> true);
        SwingUtilities.invokeLater(tree::repaint);
    }
}
