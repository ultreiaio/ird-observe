package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTree;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSelectionTreePane;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModel;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialPropertyUpdatedNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialSynchroNodeSupport;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.ReferentialTypeSynchroNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.RootSynchroNode;
import fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node.SynchroNodeSupport;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class SelectUnselect extends ReferentialSelectionTreePaneActionSupport {

    private static final Logger log = LogManager.getLogger(SelectUnselect.class);

//    private final ReferentialSelectionTreePane opposite;

    public SelectUnselect(ReferentialSelectionTreePane opposite) {
        super(null, null, null, ObserveKeyStrokesSupport.KEY_STROKE_SPACE);
//        this.opposite = opposite;
    }

    @Override
    protected int getInputMapCondition() {
        return JComponent.WHEN_FOCUSED;
    }

    @Override
    protected InputMap getInputMap(ReferentialSelectionTreePane selectionTreePane, int inputMapCondition) {
        return selectionTreePane.getTree().getInputMap(inputMapCondition);
    }

    @Override
    protected ActionMap getActionMap(ReferentialSelectionTreePane selectionTreePane) {
        return selectionTreePane.getTree().getActionMap();
    }

    protected void defaultInit(ReferentialSelectionTreePane pane) {
        pane.getTree().getInputMap().put(ObserveKeyStrokesSupport.KEY_STROKE_SPACE, "none");
        //FIXME Remove any keystroke used in ui actions...
//        pane.getTree().getInputMap().put(ObserveKeyStrokesSupport.KEY_STROKE_DATA_SYNCHRO_COPY_FROM_LEFT, "none");
//        pane.getTree().getInputMap().put(ObserveKeyStrokesSupport.KEY_STROKE_DATA_SYNCHRO_DELETE_FROM_LEFT, "none");
//        pane.getTree().getInputMap().put(ObserveKeyStrokesSupport.KEY_STROKE_DATA_SYNCHRO_COPY_FROM_RIGHT, "none");
//        pane.getTree().getInputMap().put(ObserveKeyStrokesSupport.KEY_STROKE_DATA_SYNCHRO_DELETE_FROM_RIGHT, "none");
        defaultInit(getInputMap(pane, getInputMapCondition()), getActionMap(pane));
    }

    @Override
    public void init() {
        defaultInit(ui);
//        defaultInit(opposite);
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        super.defaultInit(inputMap, actionMap);
        inputMap.put(ObserveKeyStrokesSupport.KEY_STROKE_ENTER, getActionCommandKey());
        getUi().getTree().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    ReferentialSelectionTree tree = ui.getTree();
                    TreePath path = tree.getClosestPathForLocation(e.getX(), e.getY());
                    Object lastPathComponent = path.getLastPathComponent();
                    if (lastPathComponent instanceof SynchroNodeSupport) {
                        SynchroNodeSupport node = (SynchroNodeSupport) lastPathComponent;
                        selectUnSelect(tree, Collections.singleton(node), n -> !n.isSelected());
//                    actionPerformed(new ActionEvent(getUi().getTree(), 0, "Coming from mouse click"));
                    }
                }
            }
        });
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        boolean result = canExecutionActionFromLayer(getUi(), e);
        if (result) {
            log.debug(String.format("Accept action : %s", getName()));
        } else {
            log.debug(String.format("Reject action : %s", getName()));
        }
        return result;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ReferentialSelectionTreePane ui) {
        ReferentialSelectionTree tree = ui.getTree();
        selectUnSelect(tree, tree.getSelectedNodes(), n -> !n.isSelected());
//        if (tree.isFocusOwner()) {
//            selectUnSelect(tree);
//        } else {
//            selectUnSelect(opposite.getTree());
//        }
    }

    protected static void selectUnSelect(ReferentialSelectionTree tree, Set<SynchroNodeSupport> selectedNodes, Function<SynchroNodeSupport, Boolean> function) {
        if (selectedNodes.isEmpty()) {
            return;
        }
        boolean showProperties = tree.getTreeModel().getRoot().isShowProperties();

        // Get all typed nodes in selection
        Set<SynchroNodeSupport> typeNodes = new LinkedHashSet<>();
        // Get all referential nodes in selection
        Set<SynchroNodeSupport> referentialNodes = new LinkedHashSet<>();
        // Get all property nodes in selection
        Set<SynchroNodeSupport> propertyNodes = new LinkedHashSet<>();
        for (SynchroNodeSupport node : selectedNodes) {
            if (node instanceof ReferentialPropertyUpdatedNode) {
                if (!showProperties) {
                    // we can not modify selected property in such mode
                    continue;
                }
                propertyNodes.add(node);
                continue;
            }
            if (node instanceof ReferentialTypeSynchroNode) {
                typeNodes.add(node);
                continue;
            }
            if (node instanceof ReferentialSynchroNodeSupport) {
                referentialNodes.add(node);
            }
            if (node instanceof RootSynchroNode) {
                RootSynchroNode node1 = (RootSynchroNode) node;
                Enumeration<TreeNode> children = node1.children();
                while (children.hasMoreElements()) {
                    ReferentialTypeSynchroNode childNode = (ReferentialTypeSynchroNode) children.nextElement();
                    referentialNodes.add(childNode);
                }
            }
        }

        // To collect all nodes to modify (type nodes are always modified)
        List<SynchroNodeSupport> nodesToModify = new LinkedList<>(typeNodes);

        // Removes referential node if his type node is also selected
        referentialNodes.removeIf(node -> typeNodes.contains(node.getParent()));
        // Retained referential nodes need to be modified
        nodesToModify.addAll(referentialNodes);

        // Remove property node if one of his ancestor is also selected
        propertyNodes.removeIf(node -> referentialNodes.contains(node.getParent()) || typeNodes.contains(node.getParent().getParent()));
        // Retained property nodes need to be modified
        nodesToModify.addAll(propertyNodes);

        if (!nodesToModify.isEmpty()) {
            ReferentialSynchronizeTreeModel treeModel = tree.getTreeModel();
            treeModel.setAdjusting(true);

            try {
                for (SynchroNodeSupport node : nodesToModify) {
                    boolean newSelectedValue = function.apply(node);
                    log.info(String.format("Change selection of node: %s to value? %b", node, newSelectedValue));
                    treeModel.setValueAt(node, newSelectedValue);
                }
            } finally {
                treeModel.setAdjusting(false);
            }

            log.info("Some modification in tree need a repaint");
            SwingUtilities.invokeLater(tree::repaint);
        }
    }

}
