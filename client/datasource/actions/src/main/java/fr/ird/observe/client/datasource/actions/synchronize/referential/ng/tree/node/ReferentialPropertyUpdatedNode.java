package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.differential.DifferentialProperty;

import java.util.Locale;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.3.0
 */
public class ReferentialPropertyUpdatedNode extends SynchroNodeSupport {

    private final String propertyName;
    private final DifferentialProperty property;
    private final String label;
    private final String modificationLabel;

    public ReferentialPropertyUpdatedNode(boolean left, Class<? extends ReferentialDto> dtoType, DifferentialProperty property) {
        super(null, true);
        this.propertyName = property.getPropertyName();
        this.property = property;

        String leftValue = left ? property.getNewValueLabel() : property.getOldValueLabel();
        String rightValue = left ? property.getOldValueLabel() : property.getNewValueLabel();
        this.label = property.getPropertyNameLabel(dtoType);
        this.modificationLabel = String.format("<i>( %s )</i>", t("observe.ui.datasource.editor.actions.synchro.referential.property.modified", leftValue, rightValue));
    }

    public DifferentialProperty getModifiedProperty() {
        return property;
    }

    @Override
    public ReferentialUpdatedSynchroNode getParent() {
        return (ReferentialUpdatedSynchroNode) super.getParent();
    }

    public String getPropertyName() {
        return propertyName;
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public String toString(Locale locale) {
        return "<html><body>" + label + " " + getModificationLabel();
    }

    public String getModificationLabel() {
        return modificationLabel;
    }
}
