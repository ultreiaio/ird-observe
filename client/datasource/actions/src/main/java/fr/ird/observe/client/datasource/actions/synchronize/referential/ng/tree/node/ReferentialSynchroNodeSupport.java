package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.ToolkitIdTechnicalLabel;
import fr.ird.observe.services.service.referential.differential.Differential;

import java.util.Locale;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public abstract class ReferentialSynchroNodeSupport extends SynchroNodeSupport {

    private static final long serialVersionUID = 1L;

    private final boolean canAdd;
    private final boolean canUpdate;
    private final boolean canDelete;
    private final boolean canRevert;

    protected ReferentialSynchroNodeSupport(Differential diffState,
                                            String iconName,
                                            boolean canAdd,
                                            boolean canUpdate,
                                            boolean canDelete,
                                            boolean canRevert) {
        super(UIHelper.createActionIcon(iconName), diffState);
        this.canAdd = canAdd;
        this.canUpdate = canUpdate;
        this.canDelete = canDelete;
        this.canRevert = canRevert;
    }

    public boolean isCanAdd() {
        return canAdd;
    }

    public boolean isCanUpdate() {
        return canUpdate;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public boolean isCanRevert() {
        return canRevert;
    }

    @Override
    public ReferentialTypeSynchroNode getParent() {
        return (ReferentialTypeSynchroNode) super.getParent();
    }

    @Override
    public Differential getUserObject() {
        return (Differential) super.getUserObject();
    }

    public ToolkitIdLabel getReferentialLabel() {
        return getUserObject().getThisSideDto();
    }

    @Override
    public String toString(Locale locale) {
        Differential userObject = getUserObject();
        ToolkitIdTechnicalLabel dto = userObject.getThisSideDto();
        String text = "<html><body>" + dto.toString();
        text += " <i>(" + dto.getTopiaVersion() + " - " + I18nDecoratorHelper.getTimestampLabel(locale, dto.getLastUpdateDate()) + ")</i>";
        if (!isLeaf()) {
            text += "(" + getChildCount() + ")";
        }
        return text;
    }
}
