package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.apache.commons.lang3.tuple.Pair;

import javax.swing.UIManager;
import java.util.Locale;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferentialTypeSynchroNode extends SynchroNodeSupport {

    private static final long serialVersionUID = 1L;

    public ReferentialTypeSynchroNode(Class<? extends ReferentialDto> type) {
        super(UIManager.getIcon("navigation.sub.referentiel-16"), type);
    }

    @Override
    public RootSynchroNode getParent() {
        return (RootSynchroNode) super.getParent();
    }

//    @Override
//    public boolean isSelected() {
//        Enumeration<?> children = children();
//        while (children.hasMoreElements()) {
//            SynchroNodeSupport node = (SynchroNodeSupport) children.nextElement();
//            if (!node.isSelected()) {
//                return false;
//            }
//        }
//        return !isLeaf();
//    }
//
//    @Override
//    public void setSelected(boolean selected) {
//        Enumeration<?> children = children();
//        while (children.hasMoreElements()) {
//            SynchroNodeSupport node = (SynchroNodeSupport) children.nextElement();
//            node.setSelected(selected);
//        }
//    }

    @SuppressWarnings("unchecked")
    @Override
    public Class<? extends ReferentialDto> getUserObject() {
        return (Class<? extends ReferentialDto>) super.getUserObject();
    }

    @Override
    public String toString(Locale locale) {
        Class<? extends ReferentialDto> userObject = getUserObject();
        int childCount = getChildCount();
        Pair<BusinessModule, BusinessSubModule> load = ObserveBusinessProject.get().load(userObject);
        String referentialPackageTitle = ObserveBusinessProject.get().getReferentialPackageTitle(load.getKey(), load.getValue());
        String type = I18nDecoratorHelper.getType(locale, userObject);
        return String.format("<html><body>%s <i>(%d)</i>", referentialPackageTitle + " - " + type, childCount);
    }
}
