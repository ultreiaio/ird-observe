package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;

import java.util.Enumeration;
import java.util.Locale;

/**
 * Created on 10/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class RootSynchroNode extends SynchroNodeSupport {

    private static final long serialVersionUID = 1L;

    private final boolean left;
    private final boolean canWrite;
    private final boolean showProperties;

    public RootSynchroNode(boolean left, boolean canWrite, boolean showProperties) {
        super(null, null);
        this.left = left;
        this.canWrite = canWrite;
        this.showProperties = showProperties;
    }

    @Override
    public String toString(Locale locale) {
        return null;
    }

    @Override
    public boolean isLeft() {
        return left;
    }

    public boolean isCanWrite() {
        return canWrite;
    }

    public boolean isShowProperties() {
        return showProperties;
    }

    public <D extends ReferentialDto> ReferentialTypeSynchroNode getOrAddTypeNode(Class<D> dtoType) {
        ReferentialTypeSynchroNode node = getChild(dtoType);
        if (node == null) {
            node = new ReferentialTypeSynchroNode(dtoType);
            add(node);
        }
        return node;
    }

    public <D extends ReferentialDto> ReferentialTypeSynchroNode getChild(Class<D> dtoType) {
        Enumeration<?> children = children();
        while (children.hasMoreElements()) {
            ReferentialTypeSynchroNode o = (ReferentialTypeSynchroNode) children.nextElement();
            if (dtoType.equals(o.getUserObject())) {
                return o;
            }
        }
        return null;
    }

}
