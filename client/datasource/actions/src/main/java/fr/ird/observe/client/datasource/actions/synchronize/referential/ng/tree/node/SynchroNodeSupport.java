package fr.ird.observe.client.datasource.actions.synchronize.referential.ng.tree.node;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;

import javax.swing.Icon;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.Enumeration;
import java.util.Locale;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class SynchroNodeSupport extends DefaultMutableTreeNode implements WithClientUIContextApi {

    private static final long serialVersionUID = 1L;

    private final transient Icon icon;
    private transient boolean selected;

    protected SynchroNodeSupport(Icon icon, Object userObject) {
        setUserObject(userObject);
        this.icon = icon;
    }

    public abstract String toString(Locale locale);

    public Icon getIcon() {
        return icon;
    }

    public boolean isLeft() {
        return getRoot().isLeaf();
    }

    public boolean isRight() {
        return !isLeft();
    }

    public boolean isSelected() {
        return selected;
    }

    public boolean isPartialSelected() {
        if (isLeaf()) {
            return false;
        }
        int selectedCount = getSelectedCount();
        int childCount = getChildCount();
        return selectedCount > 0 && selectedCount < childCount;
    }

    public boolean isFullySelected() {
        if (isLeaf()) {
            return false;
        }
        int selectedCount = getSelectedCount();
        int childCount = getChildCount();
        return selectedCount == childCount;
    }

    public boolean isNotSelected() {
        if (isLeaf()) {
            return false;
        }
        int selectedCount = getSelectedCount();
        return selectedCount == 0;
    }

    public int getSelectedCount() {
        int result = 0;
        Enumeration<?> children = children();
        while (children.hasMoreElements()) {
            SynchroNodeSupport childNode = (SynchroNodeSupport) children.nextElement();
            if (childNode.isSelected()) {
                result++;
            }
        }
        return result;
    }

    public void updateSelect(boolean selected) {
        if (selected) {
            select(true);
        } else {
            unselect(true);
        }
    }

    @Override
    public RootSynchroNode getRoot() {
        return (RootSynchroNode) super.getRoot();
    }

    @Override
    public SynchroNodeSupport getParent() {
        return (SynchroNodeSupport) super.getParent();
    }

    protected void select(boolean first) {
        if (first) {
            setSelectedDeeply(true);
        } else {
            setSelected(true);
        }
        SynchroNodeSupport parent = getParent();
        if (parent.isRoot()) {
            return;
        }
        if (!parent.isNotSelected()) {
            parent.select(false);
        }
    }

    protected void unselect(boolean first) {
        if (first) {
            setSelectedDeeply(false);
        } else {
            setSelected(false);
        }
        SynchroNodeSupport parent = getParent();
        if (parent==null || parent.isRoot()) {
            return;
        }
        if (parent.isNotSelected()) {
            // no more selection on parent
            parent.unselect(false);
        }
    }

    protected void setSelected(boolean selected) {
        this.selected = selected;
    }

    private void setSelectedDeeply(boolean selected) {
        setSelected(selected);
        // Reset all all children
        Enumeration<?> children = children();
        while (children.hasMoreElements()) {
            SynchroNodeSupport node = (SynchroNodeSupport) children.nextElement();
            node.setSelectedDeeply(selected);
        }
    }
}
