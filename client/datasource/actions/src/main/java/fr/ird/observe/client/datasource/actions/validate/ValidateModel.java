/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.validate;

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.actions.AdminActionModel;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.validate.tree.RootValidationNode;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.validation.DtoValidationContext;
import fr.ird.observe.dto.validation.SeineBycatchObservedSystemConfig;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import fr.ird.observe.validation.api.result.ValidationResult;
import fr.ird.observe.validation.api.result.ValidationResultDtoMessage;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
@GenerateTemplate(template = "validationReport.ftl")
@GenerateJavaBeanDefinition
public class ValidateModel extends AdminActionModel {

    static final String PROPERTY_MODEL_MODE = "modelMode";
    public static final String PROPERTY_ROOT_NODE = "rootNode";
    private static final String PROPERTY_CONTEXT_NAME = "contextName";
    private static final String PROPERTY_SCOPES = "scopes";
    private static final String PROPERTY_REPORT_FILE = "reportFile";
    /**
     * le pattern du fichier de rapport après validation
     */
    private static final String REPORT_PATTERN = "report-%1$tF--%1$tk-%1$tM-%1$tS.html";
    private static final Logger log = LogManager.getLogger(ValidateModel.class);
    /**
     * Timestamp formatter.
     */
    private final SimpleDateFormat timestampFormatter;
    /**
     * les scopes a utiliser
     */
    private final EnumSet<NuitonValidatorScope> scopes;
    /**
     * le lastName du context de validation
     */
    private String contextName;
    /**
     * le type de modèle a utiliser
     */
    private ValidationModelMode modelMode;
    /**
     * le fichier où sauvegarder les résultats de la validation
     */
    private File reportFile = new File("");
    /**
     * tout les validateur de la base
     */
    private List<NuitonValidatorModel<?>> allValidators;
    /**
     * les validateurs sélectionnés
     */
    private Set<NuitonValidatorModel<?>> validators;
    private RootValidationNode rootNode;
    private DecoratorService decoratorService;
    private transient String reportContent;

    private Float validationSpeedMaxValue;
    private boolean validationSpeedEnable;
    private boolean validationLengthWeightEnable;
    private boolean validationUseDisabledReferential;
    private SeineBycatchObservedSystemConfig seineBycatchObservedSystemConfig;

    public static ValidateModel create() {
        return new ValidateModel();
    }

    public static Set<NuitonValidatorModel<?>> filter(List<NuitonValidatorModel<?>> validators,
                                                      boolean validateData,
                                                      boolean validateReferential,
                                                      EnumSet<NuitonValidatorScope> scopes,
                                                      String context) {

        return validators.stream()
                         .filter(input -> (validateData && !ReferentialDto.class.isAssignableFrom(input.getType()) || validateReferential && ReferentialDto.class.isAssignableFrom(input.getType()))
                                 && context.equals(input.getContext())
                                 && EnumSet.copyOf(input.getScopes()).stream().anyMatch(scopes::contains)).
                         collect(Collectors.toSet());

    }

    public ValidateModel() {
        super(AdminStep.VALIDATE);
        scopes = EnumSet.noneOf(NuitonValidatorScope.class);
        timestampFormatter = I18nDecoratorHelper.newTimestampFormat(I18n.getDefaultLocale());
    }

    @Override
    public void start(AdminUIModel uiModel) {

        ClientConfig config = getClientConfig();

        setValidationSpeedMaxValue(config.getValidationSpeedMaxValue());
        setValidationSpeedEnable(config.getValidationSpeedEnable());
        setValidationLengthWeightEnable(config.getValidationLengthWeightEnable());
        setValidationUseDisabledReferential(config.getValidationUseDisabledReferential());
        setSeineBycatchObservedSystemConfig(config.getSeineBycatchObservedSystem());

        this.decoratorService = ClientConfig.getDecoratorService();
        PropertyChangeListener listenValidationModified = evt -> {
            ValidateModel source = (ValidateModel) evt.getSource();
            log.debug(String.format("validation model [%s] changed on %s, new value = %s", source, evt.getPropertyName(), evt.getNewValue()));
            uiModel.validate();
            log.debug(String.format("nb validators = %d", source.getValidators().size())
            );
        };

        addPropertyChangeListener(listenValidationModified);
        addScope(NuitonValidatorScope.ERROR);
        setModelMode(ValidationModelMode.DATA);
        setContextName(DtoValidationContext.UPDATE_VALIDATION_CONTEXT);
        if (!config.getValidationReportDirectory().exists()) {
            boolean b = config.getValidationReportDirectory().mkdirs();
            if (!b) {
                throw new RuntimeException("Could not create directory " + config.getValidationReportDirectory());
            }
        }
        setDefaultReportFile();
    }

    @Override
    public boolean validate(AdminUIModel uiModel) {
        return uiModel.validate(AdminStep.SELECT_DATA) && uiModel.getStepState(step) == WizardState.SUCCESSED;
    }

    @Override
    public boolean validateConfig(AdminUIModel uiModel) {
        boolean valid = super.validateConfig(uiModel);
        if (!valid) {
            return false;
        }
        // il faut avoir sélectionné un context
        // il faut avoir au moins sélectionné un scope

        if (Strings.isEmpty(getContextName())) {
            log.debug("no validation context name");
            return false;
        }
        if (getScopes().isEmpty()) {
            log.debug("no validation scopes");
            return false;
        }
        if (getModelMode() == null) {
            log.debug("no validation model mode");
            return false;
        }

        // le fichier de rapport ne doit pas exister
        File file = getReportFile();

        if (file.exists()) {
            log.debug("report file already exists");
            return false;
        }

        // le repertoire du rapport doit exister
        File parentFile = file.getParentFile();
        if (parentFile == null || !parentFile.exists()) {
            return false;
        }

        if (getValidators().isEmpty()) {
            log.debug("no validators detected");
            return false;
        }

        // la base précédente doit être ouverte
        DataSourceSelectorModel localSourceModel = uiModel.getConfigModel().getLeftSourceModel();
        ObserveDataSourceInformation dataSourceInformation = localSourceModel.getDataSourceInformation();

        // pour valider une base il faut les droits
        SelectionTreeModel selectionModel = uiModel.getSelectDataModel().getSelectionDataModel();
        if (selectionModel.getConfig().isLoadData()) {
            if (!dataSourceInformation.canReadData()) {
                log.debug("can not read data");
                return false;
            }
        }
        if (selectionModel.getConfig().isLoadReferential()) {
            if (!dataSourceInformation.canReadReferential()) {
                log.debug("can not read referentiel");
                return false;
            }
        }
        return true;
    }

    public void setDefaultReportFile() {
        ClientConfig config = getClientConfig();
        File reportFile = new File(config.getValidationReportDirectory(), getDefaultReportFilename());
        setReportFile(reportFile);
    }

    public String getReportContent() {
        if (reportContent == null) {
            reportContent = ValidateModelTemplate.generate(this);
        }
        return reportContent;
    }

    @Override
    public void destroy() {
        super.destroy();
        if (validators != null) {
            validators = null;
        }
        if (allValidators != null) {
            allValidators = null;
        }
        rootNode = null;
        reportContent = null;
    }

    public ValidationRequestConfiguration toValidationRequestConfiguration() {
        ValidationRequestConfiguration configuration = new ValidationRequestConfiguration();
        configuration.setValidationSpeedMaxValue(getValidationSpeedMaxValue());
        configuration.setValidationSpeedEnable(isValidationSpeedEnable());
        configuration.setValidationLengthWeightEnable(isValidationLengthWeightEnable());
        configuration.setValidationUseDisabledReferential(isValidationUseDisabledReferential());
        configuration.setSeineBycatchObservedSystemConfig(getSeineBycatchObservedSystemConfig());
        return configuration;
    }

    /**
     * @return le nom par défaut du rapport de validation à enregistrer.
     */
    private String getDefaultReportFilename() {
        return String.format(REPORT_PATTERN, new Date());
    }

    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        Object oldValue = this.contextName;
        this.contextName = contextName;
        validators = null;
        firePropertyChange(PROPERTY_CONTEXT_NAME, oldValue, contextName);
    }

    public ValidationModelMode getModelMode() {
        return modelMode;
    }

    public void setModelMode(ValidationModelMode modelMode) {
        Object oldValue = this.modelMode;
        this.modelMode = modelMode;
        validators = null;
        firePropertyChange(PROPERTY_MODEL_MODE, oldValue, modelMode);
    }

    public EnumSet<NuitonValidatorScope> getScopes() {
        return scopes;
    }

    public File getReportFile() {
        return reportFile;
    }

    public void setReportFile(File reportFile) {
        Object oldValue = this.reportFile;
        this.reportFile = reportFile;
        firePropertyChange(PROPERTY_REPORT_FILE, oldValue, reportFile);
    }

    void setAllValidators(List<NuitonValidatorModel<?>> allValidators) {
        this.allValidators = allValidators;
    }

    public Set<NuitonValidatorModel<?>> getValidators() {
        if (validators == null) {
            if (allValidators != null && getContextName() != null) {
                validators = filter(
                        allValidators,
                        getModelMode().isData(),
                        getModelMode().isReferential(),
                        getScopes(),
                        getContextName());
            } else {
                validators = new HashSet<>();
            }
        }
        return validators;
    }

    public void setValidationResult(ValidationResult validationResult) {
        if (validationResult != null) {
            validationResult.applyOnNodes(n -> {
                ToolkitIdLabel data = n.getDatum();
                Class<? extends BusinessDto> type = data.getType();
                LinkedList<ValidationResultDtoMessage> messages = n.getMessages();
                if (messages != null) {
                    messages.forEach(m -> m.setType(type));
                }
                Decorator decorator = decoratorService.getToolkitIdLabelDecoratorByType(type);
                data.registerDecorator(decorator);
            });
        }
        RootValidationNode oldValue = this.rootNode;
        this.rootNode = new RootValidationNode(getModelMode(), validationResult);
        firePropertyChange(PROPERTY_ROOT_NODE, oldValue, rootNode);
    }

    public RootValidationNode getRootNode() {
        return rootNode;
    }

    public void addScope(NuitonValidatorScope scope) {
        scopes.add(scope);
        validators = null;
        firePropertyChange(PROPERTY_SCOPES, null, scopes);
    }

    public void removeScope(NuitonValidatorScope scope) {
        scopes.remove(scope);
        validators = null;
        firePropertyChange(PROPERTY_SCOPES, null, scopes);
    }

    public String getNow() {
        return timestampFormatter.format(new Date());
    }

    public boolean withoutMessages() {
        return rootNode == null || rootNode.isLeaf();
    }

    public void setDecoratorService(DecoratorService decoratorService) {
        this.decoratorService = decoratorService;
    }

    public Float getValidationSpeedMaxValue() {
        return validationSpeedMaxValue;
    }

    public void setValidationSpeedMaxValue(Float validationSpeedMaxValue) {
        Float oldValue = this.validationSpeedMaxValue;
        this.validationSpeedMaxValue = validationSpeedMaxValue;
        firePropertyChange("validationSpeedMaxValue", oldValue, validationSpeedMaxValue);
    }

    public boolean isValidationSpeedEnable() {
        return validationSpeedEnable;
    }

    public void setValidationSpeedEnable(boolean validationSpeedEnable) {
        boolean oldValue = this.validationSpeedEnable;
        this.validationSpeedEnable = validationSpeedEnable;
        firePropertyChange("validationSpeedEnable", oldValue, validationSpeedEnable);
    }

    public boolean isValidationLengthWeightEnable() {
        return validationLengthWeightEnable;
    }

    public void setValidationLengthWeightEnable(boolean validationLengthWeightEnable) {
        boolean oldValue = this.validationLengthWeightEnable;
        this.validationLengthWeightEnable = validationLengthWeightEnable;
        firePropertyChange("validationLengthWeightEnable", oldValue, validationLengthWeightEnable);
    }

    public boolean isValidationUseDisabledReferential() {
        return validationUseDisabledReferential;
    }

    public void setValidationUseDisabledReferential(boolean validationUseDisabledReferential) {
        boolean oldValue = this.validationUseDisabledReferential;
        this.validationUseDisabledReferential = validationUseDisabledReferential;
        firePropertyChange("validationUseDisabledReferential", oldValue, validationUseDisabledReferential);
    }

    public SeineBycatchObservedSystemConfig getSeineBycatchObservedSystemConfig() {
        return seineBycatchObservedSystemConfig;
    }

    public void setSeineBycatchObservedSystemConfig(SeineBycatchObservedSystemConfig seineBycatchObservedSystemConfig) {
        SeineBycatchObservedSystemConfig oldValue = this.seineBycatchObservedSystemConfig;
        this.seineBycatchObservedSystemConfig = seineBycatchObservedSystemConfig;
        firePropertyChange("seineBycatchObservedSystemConfig", oldValue, seineBycatchObservedSystemConfig);
    }

    public void reset() {
        setDefaultReportFile();
        reportContent = null;
    }
}
