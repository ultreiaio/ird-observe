/*
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.actions.validate;

import fr.ird.observe.client.datasource.actions.AdminTabUIHandler;
import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.config.ConfigUI;
import fr.ird.observe.client.datasource.actions.config.SelectDataUI;
import fr.ird.observe.client.datasource.actions.validate.actions.SelectValidationConfigFlag;
import fr.ird.observe.client.datasource.actions.validate.tree.RootValidationNode;
import fr.ird.observe.client.datasource.actions.validate.tree.ValidationNode;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import fr.ird.observe.validation.api.result.ValidationResultDtoMessage;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.api.io.NuitonValidatorModelHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.number.NumberEditor;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ValidateUIHandler extends AdminTabUIHandler<ValidateUI> implements UIHandler<ValidateUI> {

    private static final Logger log = LogManager.getLogger(ValidateUIHandler.class);

    @Override
    public void afterInit(ValidateUI ui) {
        super.afterInit(ui);
        setAutoStart(ui.getStart());
        JTable messageTable = ui.getMessageTable();

        messageTable.setDefaultRenderer(Object.class, new ValidationMessageTableRenderer());
        messageTable.getRowSorter().setSortKeys(
                Collections.singletonList(new RowSorter.SortKey(0, SortOrder.ASCENDING)));
        UIHelper.setI18nTableHeaderRenderer(
                messageTable,
                n("observe.ui.datasource.editor.actions.validate.validator.scope.header"),
                n("observe.ui.datasource.editor.actions.validate.validator.scope.header.tip"),
                n("observe.ui.datasource.editor.actions.validate.validator.field.header"),
                n("observe.ui.datasource.editor.actions.validate.validator.field.header.tip"),
                n("observe.ui.datasource.editor.actions.validate.validator.message.header"),
                n("observe.ui.datasource.editor.actions.validate.validator.message.header.tip"));
        UIHelper.fixTableColumnWidth(messageTable, 0, 25);

        ValidationTreeCellRenderer listRenderer = new ValidationTreeCellRenderer();
        JTree tree = ui.getTree();
        UIInitHelper.init(tree);
        tree.getInputMap().put(ObserveKeyStrokesActions.KEY_STROKE_COPY_TO_CLIPBOARD, "none");
        tree.setRootVisible(false);
        tree.setRowHeight(24);
        DefaultTreeModel treeModel = new DefaultTreeModel(new RootValidationNode(null, null));
        tree.setModel(treeModel);
        tree.setCellRenderer(listRenderer);
        tree.addTreeSelectionListener(e -> {
            TreePath path = e.getPath();
            ValidationNode selectedNode = null;
            if (path != null) {
                selectedNode = (ValidationNode) path.getLastPathComponent();
            }
            updateSelectedNode(selectedNode);
        });
        SelectDataUI selectTabUI = SelectDataUI.get(parentUI);
        AdminUIModel model = ui.getModel();
        model.getValidateModel().addPropertyChangeListener(ValidateModel.PROPERTY_MODEL_MODE, evt -> {
            ValidationModelMode value = (ValidationModelMode) evt.getNewValue();
            if (value == null) {
                // rien a faire pour le moment...
                return;
            }
            SelectionTreeModel selectDataModel = selectTabUI.getSelectTreePane().getTree().getModel();
            selectDataModel.getConfig().setLoadReferential(false);
            selectDataModel.getConfig().setLoadData(false);
            log.debug(String.format("validation model changed to %s", value));
            switch (value) {
                case REFERENTIEL:
                    selectDataModel.getConfig().setLoadReferential(true);
                    break;
                case DATA:
                    selectDataModel.getConfig().setLoadData(true);
                    break;
            }
        });
        List<NuitonValidatorModel<?>> validatorModels;
        try {
            validatorModels = new NuitonValidatorModelHelper().readAll(NuitonValidatorProviders.getDefaultFactoryName());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ValidateModel stepModel = model.getValidateModel();
        stepModel.addPropertyChangeListener(ValidateModel.PROPERTY_ROOT_NODE, evt -> {
            RootValidationNode rootNode = (RootValidationNode) evt.getNewValue();
            treeModel.setRoot(rootNode);
            if (rootNode.getChildCount() > 0) {
                SwingUtilities.invokeLater(() -> {
                    tree.requestFocusInWindow();
                    tree.setSelectionInterval(0, 0);
                });
            }
        });
        stepModel.setAllValidators(validatorModels);
        hideFixedPanelLabel(ui);
    }

    @Override
    protected void onComingFromPreviousStep(boolean pending) {
        getUi().getStepModel().getProgressModel().setValue(0);
        super.onComingFromPreviousStep(pending);
    }

    @Override
    protected void initConfig(ConfigUI configUI) {
        super.initConfig(configUI);
        ValidateConfigUI extraConfig = new ValidateConfigUI(UIHelper.initialContext(configUI, this));
        NumberEditor validationSpeedMaxValue = extraConfig.getValidationSpeedMaxValue();
        UIInitHelper.init(validationSpeedMaxValue, false, true);
        SelectValidationConfigFlag.init(extraConfig, extraConfig.getValidationSpeedEnable(), new SelectValidationConfigFlag("validationSpeedEnable", ValidateModel::isValidationSpeedEnable, ValidateModel::setValidationSpeedEnable, 5));
        SelectValidationConfigFlag.init(extraConfig, extraConfig.getValidationLengthWeightEnable(), new SelectValidationConfigFlag("validationLengthWeightEnable", ValidateModel::isValidationLengthWeightEnable, ValidateModel::setValidationLengthWeightEnable, 6));
        SelectValidationConfigFlag.init(extraConfig, extraConfig.getValidationUseDisabledReferential(), new SelectValidationConfigFlag("validationUseDisabledReferential", ValidateModel::isValidationUseDisabledReferential, ValidateModel::setValidationUseDisabledReferential, 7));
        extraConfig.getSeineBycatchObservedSystemConfig().setText("TODO");
        configUI.getExtraConfig().add(extraConfig);
    }

    @Override
    protected void onStateChangeToNeedFix() {
        super.onStateChangeToNeedFix();
//        updateSelectedNode(null);
        ui.getResumeLabel().setText(t("observe.ui.datasource.editor.actions.validate.save.reportFile", ui.getStepModel().getReportFile()));
        SwingUtilities.invokeLater(() -> {
            JTree tree = ui.getTree();
            tree.requestFocusInWindow();
        });
    }

    @Override
    protected void onStateChangeToRunning() {
        super.onStateChangeToRunning();
        ui.getMessageTable().clearSelection();
        ui.getMessagesModel().clear();
    }

    void updateSelectedNode(ValidationNode node) {
        // clean old messages
        ui.getMessageTable().clearSelection();
        ui.getMessagesModel().clear();
        log.debug(String.format("new selected ref = %s", node));
        if (node == null) {
            ui.getCopySelectedNodeId().setEnabled(false);
            return;
        }
        ui.getCopySelectedNodeId().setEnabled(node.getUserObject() instanceof ToolkitIdLabel);
        List<ValidationResultDtoMessage> messages = node.getMessages();
        ui.getMessagesModel().setMessages(messages);
    }

    // ------------------------------------------------------------------------
    // -- ValidateConfigUI methods
    // ------------------------------------------------------------------------

    String updateValidatorResumeLabel(@SuppressWarnings("unused") boolean valid) {
        ValidateModel validateModel = ui.getModel().getValidateModel();
        if (validateModel == null) {
            return null;
        }
        return t("observe.ui.datasource.editor.actions.validate.selected.validators", validateModel.getValidators().size());
    }

    private NuitonValidatorScope getValidatorScope(JCheckBox checkBox) {
        return (NuitonValidatorScope) checkBox.getClientProperty("value");
    }

    boolean isScopeSelected(Set<NuitonValidatorScope> scopes, JCheckBox checkBox) {
        NuitonValidatorScope scope = getValidatorScope(checkBox);
        return scopes.contains(scope);
    }

    public void changeValidationReportDirectory(ValidateConfigUI configUI, File f) {
        ui.getModel().getValidateModel().setReportFile(new File(f, configUI.validationReportFilenameText.getText()));
    }

    void changeValidationReportFilename(ValidateConfigUI configUI, String filename) {
        ui.getModel().getValidateModel().setReportFile(new File(configUI.validationReportDirectoryText.getText(), filename));
    }

}
