package fr.ird.observe.client.datasource.actions.validate;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.validation.api.result.ValidationResultDtoMessage;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ValidationMessageTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    private static final String[] columnNames = {"validator.scope", "validator.field", "validator.message"};

    private static final Class<?>[] columnClasses = {NuitonValidatorScope.class, String.class, String.class};

    protected final List<ValidationResultDtoMessage> messages = new ArrayList<>();

    @Override
    public int getRowCount() {
        return messages.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        // cells are never editable in this model
        return false;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        UIHelper.ensureColumnIndex(this, columnIndex);
        return columnClasses[columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        UIHelper.ensureColumnIndex(this, column);
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        ValidationResultDtoMessage message = messages.get(rowIndex);

        Object value;

        switch (columnIndex) {
            case 0:
                value = message.getScope();
                break;
            case 1:
                value = message.getFieldName();
                break;
            case 2:
                value = message.getMessage();
                break;
            default:
                value = null;
        }

        return value;
    }

    public void setMessages(Collection<ValidationResultDtoMessage> messages) {
        this.messages.clear();
        if (messages != null) {
            this.messages.addAll(messages);
        }
        fireTableDataChanged();
    }

    public void clear() {
        messages.clear();
        fireTableDataChanged();
    }

    public ValidationResultDtoMessage getRow(int row) {
        return messages.get(row);
    }
}
