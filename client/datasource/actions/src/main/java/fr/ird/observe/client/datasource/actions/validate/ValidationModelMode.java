package fr.ird.observe.client.datasource.actions.validate;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;

/**
 * Pour caractériser le type de modele de validation a utiliser.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION, pattern = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION_PATTERN)
})
public enum ValidationModelMode {

    /**
     * to validate only data.
     */
    DATA(false, true),

    /**
     * to validate only referential.
     */
    REFERENTIEL(true, false);

    private final boolean referential;
    private final boolean data;

    ValidationModelMode(boolean referential, boolean data) {
        this.referential = referential;
        this.data = data;
    }

    public boolean isReferential() {
        return referential;
    }

    public boolean isData() {
        return data;
    }

    public String getLabel() {
        return ValidationModelModeI18n.getLabel(this);
    }
}
