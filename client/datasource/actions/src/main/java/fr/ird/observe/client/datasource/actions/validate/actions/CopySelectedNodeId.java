package fr.ird.observe.client.datasource.actions.validate.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.ObserveKeyStrokesActions;
import fr.ird.observe.client.datasource.actions.validate.ValidateUI;
import fr.ird.observe.client.datasource.actions.validate.tree.ValidationNode;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.ToolkitIdLabel;

import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 31/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class CopySelectedNodeId extends ValidateUIActionSupport {

    public CopySelectedNodeId() {
        super("", t("observe.ui.action.copy.id.to.clipBoard"), "report-copy", ObserveKeyStrokesActions.KEY_STROKE_COPY_TO_CLIPBOARD);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ValidateUI ui) {
        TreePath selectionPath = ui.getTree().getSelectionPath();
        if (selectionPath == null) {
            return;
        }
        ValidationNode selectedValue = (ValidationNode) selectionPath.getLastPathComponent();
        if (selectedValue != null) {
            Object userObject = selectedValue.getUserObject();
            if (userObject instanceof ToolkitIdLabel) {
                UIHelper.copyToClipBoard(((ToolkitIdLabel) userObject).getId());
                setUiStatus(t("observe.ui.datasource.editor.actions.validate.copy.id"));
            }
        }
    }
}


