package fr.ird.observe.client.datasource.actions.validate.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.validate.ValidateConfigUI;
import fr.ird.observe.client.datasource.actions.validate.ValidateModel;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;

import java.awt.event.ActionEvent;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Created on 14/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SelectValidationConfigFlag extends ValidateConfigUIActionSupport {

    private final Function<ValidateModel, Boolean> getter;
    private final BiConsumer<ValidateModel, Boolean> setter;

    public SelectValidationConfigFlag(String propertyName, Function<ValidateModel, Boolean> getter, BiConsumer<ValidateModel, Boolean> setter, int strokeIndex) {
        super(SelectValidationScope.class.getName() + propertyName, "", null, null, ObserveKeyStrokesSupport.getFunctionKeyStroke(strokeIndex));
        this.getter = getter;
        this.setter = setter;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ValidateConfigUI ui) {
        ValidateModel validateModel = ui.getModel().getValidateModel();
        boolean oldValue = getter.apply(validateModel);
        setter.accept(validateModel, !oldValue);
    }

}
