package fr.ird.observe.client.datasource.actions.validate.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.validate.ValidateConfigUI;
import fr.ird.observe.client.datasource.actions.validate.ValidateModel;
import fr.ird.observe.client.datasource.actions.validate.ValidationModelMode;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.awt.event.ActionEvent;

public class SelectValidationScope extends ValidateConfigUIActionSupport {

    private final NuitonValidatorScope validatorScope;

    public SelectValidationScope(NuitonValidatorScope validatorScope) {
        super(SelectValidationScope.class.getName() + validatorScope, validatorScope.getLabel(), validatorScope.getLabel(), null, ObserveKeyStrokesSupport.getFunctionKeyStroke(ValidationModelMode.values().length - 1 + validatorScope.ordinal()));
        this.validatorScope = validatorScope;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ValidateConfigUI ui) {
        ValidateModel validateModel = ui.getModel().getValidateModel();
        if (validateModel.getScopes().contains(validatorScope)) {
            // supprime le scope
            validateModel.removeScope(validatorScope);
        } else {
            // ajout du scope
            validateModel.addScope(validatorScope);
        }
    }

}
