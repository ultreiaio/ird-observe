package fr.ird.observe.client.datasource.actions.validate.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.AdminUIModel;
import fr.ird.observe.client.datasource.actions.validate.ValidateModel;
import fr.ird.observe.client.datasource.actions.validate.ValidateUI;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import fr.ird.observe.services.service.ValidateService;
import fr.ird.observe.validation.api.request.DataValidationRequest;
import fr.ird.observe.validation.api.request.ReferentialValidationRequest;
import fr.ird.observe.validation.api.result.ValidationResult;
import fr.ird.observe.validation.api.result.ValidationResultBuilder;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

public class Start extends ValidateUIActionSupport {

    private static final Logger log = LogManager.getLogger(Start.class);

    public Start() {
        super(null, null, "wizard-start", 'D');
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        setText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        setTooltipText(t("observe.ui.datasource.editor.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        super.defaultInit(inputMap, actionMap);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ValidateUI ui) {
        addAdminWorker(ui.getStart().getToolTipText(), this::doAction);
    }


    public WizardState doAction() throws Exception {
        log.debug(this);

        WizardState init = initDB();

        if (init != null) {
            // error or cancel
            return init;
        }

        launchValidation();

//        ui.getModel().setWasDone(true);

        if (ui.getModel().getValidateModel().withoutMessages()) {
            sendMessage(t("observe.ui.datasource.editor.actions.validate.message.nothing.to.do"));
            sendMessage(t("observe.ui.datasource.editor.actions.validate.message.operation.done", new Date()));
            return WizardState.SUCCESSED;
        }

        sendMessage(t("observe.ui.datasource.editor.actions.validate.message.operation.needFix", new Date()));

        ValidateModel validationModel = ui.getModel().getValidateModel();
        sendMessage(t("observe.ui.datasource.editor.actions.validate.message.save.report", validationModel.getReportFile()));
        generateReportFile(validationModel);
        return WizardState.NEED_FIX;
    }

    private WizardState initDB() {

        // on récupère la source de données
        AdminUIModel model = ui.getModel();
        ObserveSwingDataSource source = model.getConfigModel().getLeftSourceModel().getSafeSource(false);

        ObserveSwingDataSource.doOpenSource(source);

        // recuperation des validateurs du modèle

        ValidateModel validationModel = model.getValidateModel();
        Set<NuitonValidatorModel<?>> validators = validationModel.getValidators();

        sendMessage(t("observe.ui.datasource.editor.actions.validate.message.use.storage", source.getLabel()));
        sendMessage(t("observe.ui.datasource.editor.actions.validate.message.prepare.validators"));

        if (!validators.isEmpty()) {

            // des validateurs ont été trouvés

            for (NuitonValidatorModel<?> v : validators) {
                String label = t(I18nDecoratorHelper.getType(v.getType()));
                sendMessage(t("observe.ui.datasource.editor.actions.validate.message.detected", label));
            }
        } else {
            sendMessage(t("observe.ui.datasource.editor.actions.validate.message.no.validation.detected"));
        }

        return null;
    }

    private void launchValidation() throws Exception {

        AdminUIModel model = ui.getModel();
        // on vide les anciens messages
        ValidateModel stepModel = model.getValidateModel();
        stepModel.setValidationResult(null);

        SelectionTreeModel dataModel = model.getSelectDataModel().getSelectionDataModel();

        try (ObserveSwingDataSource dataSourceToValidate = model.getConfigModel().getLeftSourceModel().getSource()) {
            int stepsCount = computeStepCount(dataSourceToValidate, dataModel);
            log.info("Step count: {}", stepsCount);
            ProgressionModel progressModel = stepModel.getProgressModel();
            progressModel.setMaximum(stepsCount);
            progressModel.setValue(0);

            if (!dataSourceToValidate.isOpen()) {
                dataSourceToValidate.open();
                progressModel.increments();
            }

            ValidateService validateService = dataSourceToValidate.getValidateService();

            Set<NuitonValidatorScope> scopes = stepModel.getScopes();
            String contextName = stepModel.getContextName();
            ValidationResult result;

            ValidationRequestConfiguration configuration = stepModel.toValidationRequestConfiguration();
            if (dataModel.getConfig().isLoadReferential()) {

                ReferentialValidationRequest request = new ReferentialValidationRequest();

                request.setReferentialTypes(dataModel.getSelectedReferential());
                request.setScopes(scopes);
                request.setValidationContext(contextName);

                progressModel.increments();
                result = validateService.validateReferential(configuration, request);
                progressModel.increments();

            } else if (dataModel.getConfig().isLoadData()) {

                try (ValidationResultBuilder resultBuilder = ValidationResultBuilder.create()) {
                    List<ToolkitIdLabel> selectedData = new LinkedList<>(dataModel.getSelectedData());
                    Class<? extends RootOpenableDto> dataType = dataModel.getRequest().getModuleName().equals("ps") ? fr.ird.observe.dto.data.ps.common.TripDto.class : fr.ird.observe.dto.data.ll.common.TripDto.class;
                    Decorator decorator = getDecoratorService().getToolkitIdLabelDecoratorByType(dataType);
                    selectedData.forEach(d -> d.registerDecorator(decorator));
                    decorator.sort(selectedData, 0);
                    int max = selectedData.size();
                    int index = 1;
                    for (ToolkitIdLabel datum : selectedData) {

                        String id = datum.getId();

                        DataValidationRequest request = new DataValidationRequest();
                        request.setDataIds(Collections.singleton(id));
                        request.setScopes(scopes);
                        request.setValidationContext(contextName);

                        sendMessage(String.format("[ %4d - %4d ] ", index, max) + t("observe.ui.datasource.editor.actions.validate.message.validate.trip", datum));

                        progressModel.increments();
                        result = validateService.validateData(configuration, request);
                        progressModel.increments();
                        resultBuilder.addResult(result);
                        index++;
                    }
                    result = resultBuilder.build();
                }

            } else {
                throw new IllegalStateException();
            }
            progressModel.increments();
            stepModel.setValidationResult(result);
            log.info("Step count (at the end): {} vs computed {}", progressModel.getValue(), stepsCount);
        }
    }

    private void generateReportFile(ValidateModel validationModel) throws IOException {
        File reportFile = validationModel.getReportFile();
        log.info("save report in " + reportFile);
        validationModel.setDecoratorService(getDecoratorService());
        String content = validationModel.getReportContent();
        FileUtils.write(reportFile, content, StandardCharsets.UTF_8.name());
    }

    private int computeStepCount(ObserveSwingDataSource dataSourceToValidate, SelectionTreeModel dataModel) {
        int stepsCount = 1;
        if (!dataSourceToValidate.isOpen()) {
            stepsCount++;
        }
        boolean loadReferential = dataModel.getConfig().isLoadReferential();
        if (loadReferential) {
            stepsCount += 2;
        }

        if (dataModel.getConfig().isLoadData()) {
            stepsCount += 2 * dataModel.getSelectedDataIds().size();
        }
        return stepsCount;
    }

}
