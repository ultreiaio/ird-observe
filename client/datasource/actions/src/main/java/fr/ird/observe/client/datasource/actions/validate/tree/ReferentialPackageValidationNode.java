package fr.ird.observe.client.datasource.actions.validate.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;
import java.util.Enumeration;
import java.util.Objects;

/**
 * Created on 31/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class ReferentialPackageValidationNode extends ValidationNode {

    private final String label;

    public ReferentialPackageValidationNode(NavigationScope navigationScope ,BusinessModule businessModule, BusinessSubModule businessSubModule) {
        setUserObject(navigationScope);
        this.label = ObserveBusinessProject.get().getReferentialPackageTitle(businessModule, businessSubModule);
    }

    protected ReferentialTypeValidationNode referentialTypeValidationNode(Class<? extends ReferentialDto> type) {
        Enumeration<TreeNode> children = children();
        while (children.hasMoreElements()) {
            TreeNode treeNode = children.nextElement();
            if (treeNode instanceof ReferentialTypeValidationNode && ((ReferentialTypeValidationNode) treeNode).getUserObject().getMainType().equals(type)) {
                return (ReferentialTypeValidationNode) treeNode;
            }
        }
        return null;
    }
    @Override
    public NavigationScope getUserObject() {
        return (NavigationScope) super.getUserObject();
    }

    @Override
    public String getId() {
        return getUserObject().getSubModuleType().getName();
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    protected String computeText() {
        return label + String.format(" (%s)", getChildCount());
    }

    @Override
    protected Icon computeIcon() {
        return DtoIconHelper.getIcon(Objects.requireNonNull(getUserObject().getIconPath()), false);
    }
}
