package fr.ird.observe.client.datasource.actions.validate.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScopes;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.validation.api.result.ValidationResultDtoMessage;

import javax.swing.Icon;
import java.util.List;
import java.util.Objects;

/**
 * Created on 30/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class ReferentialValidationNode extends ValidationNode {

    private final NavigationScope navigationScope;
    /**
     * Optional messages for the underlying data.
     */
    private final List<ValidationResultDtoMessage> messages;

    public ReferentialValidationNode(ToolkitIdLabel datum, List<ValidationResultDtoMessage> messages) {
        this.messages = messages == null ? List.of() : messages;
        Class<? extends BusinessDto> type = datum.getType();
        navigationScope = NavigationScopes.get().getScopes().values().stream().filter(s -> Objects.equals(s.getMainType(), type)).findAny().orElseThrow();
        setUserObject(datum);
    }

    @Override
    public ToolkitIdLabel getUserObject() {
        return (ToolkitIdLabel) super.getUserObject();
    }

    @Override
    public String getId() {
        return getUserObject().getId();
    }

    public List<ValidationResultDtoMessage> getMessages() {
        return messages;
    }

    @Override
    protected String computeText() {
        return computeText(messages);
    }

    @Override
    protected Icon computeIcon() {
        return DtoIconHelper.getIcon(Objects.requireNonNull(navigationScope.getIconPath()), true);
    }


}
