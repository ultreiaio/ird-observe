package fr.ird.observe.client.datasource.actions.validate.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.validate.ValidationModelMode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScopes;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.validation.api.result.ValidationResult;
import fr.ird.observe.validation.api.result.ValidationResultDto;
import org.apache.commons.lang3.tuple.Pair;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 30/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class RootValidationNode extends ValidationNode {

    public static void dataNode(ValidationNode parentNode, ValidationResultDto resultNode) {
        DataValidationNode result = new DataValidationNode(resultNode.getDatum(), resultNode.getMessages());
        parentNode.add(result);
        LinkedList<ValidationResultDto> children = resultNode.getChildren();
        if (children != null) {
            for (ValidationResultDto childNode : children) {
                dataNode(result, childNode);
            }
        }
    }

    public static void referentialNode(RootValidationNode parentNode, ValidationResultDto resultNode) {

        ToolkitIdLabel datum = resultNode.getDatum();
        @SuppressWarnings("unchecked") Class<? extends ReferentialDto> type = (Class<? extends ReferentialDto>) datum.getType();
        Pair<BusinessModule, BusinessSubModule> gav = ObserveBusinessProject.get().load(type);
        BusinessModule module = gav.getLeft();
        BusinessSubModule subModule = gav.getRight();
        NavigationScope navigationScope = NavigationScopes.get().getScopes().values().stream()
                                                          .filter(s -> s.getContentUiType().getName().endsWith(".ReferentialHomeUI")
                                                                  && s.getModuleType().equals(module.getClass())
                                                                  && s.getSubModuleType().equals(subModule.getClass()))
                                                          .findAny().orElseThrow();

        ReferentialPackageValidationNode packageValidationNode = parentNode.referentialPackageValidationNode(navigationScope);
        if (packageValidationNode == null) {

            packageValidationNode = new ReferentialPackageValidationNode(navigationScope, module, subModule);
            parentNode.add(packageValidationNode);
        }
        ReferentialTypeValidationNode typeValidationNode = packageValidationNode.referentialTypeValidationNode(type);
        if (typeValidationNode == null) {
            typeValidationNode = new ReferentialTypeValidationNode(type);
            packageValidationNode.add(typeValidationNode);
        }

        ReferentialValidationNode datumNode = new ReferentialValidationNode(datum, resultNode.getMessages());
        typeValidationNode.add(datumNode);
    }

    public RootValidationNode(ValidationModelMode modelMode, ValidationResult validationResult) {
        if (validationResult == null) {
            return;
        }
        Set<ValidationResultDto> nodes = validationResult.getNodes();
        switch (Objects.requireNonNull(modelMode)) {
            case DATA:
                for (ValidationResultDto node : nodes) {
                    dataNode(this, node);
                }
                break;
            case REFERENTIEL:
                for (ValidationResultDto node : nodes) {
                    referentialNode(this, node);
                }
                break;
            default:
                throw new IllegalStateException("Can not manage modelMode: " + modelMode);
        }
    }

    protected ReferentialPackageValidationNode referentialPackageValidationNode(NavigationScope navigationScope) {
        Enumeration<TreeNode> children = children();
        while (children.hasMoreElements()) {
            TreeNode treeNode = children.nextElement();
            if (treeNode instanceof ReferentialPackageValidationNode && ((ReferentialPackageValidationNode) treeNode).getUserObject().equals(navigationScope)) {
                return (ReferentialPackageValidationNode) treeNode;
            }
        }
        return null;
    }

    @Override
    protected String computeText() {
        return "Root";
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    protected Icon computeIcon() {
        return null;
    }

    @Override
    public String getId() {
        return "Root";
    }
}
