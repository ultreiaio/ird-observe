package fr.ird.observe.client.datasource.actions.validate.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Actions
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.validation.api.result.ValidationResultDtoMessage;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.apache.commons.lang3.mutable.MutableInt;

import javax.swing.Icon;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 30/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public abstract class ValidationNode extends DefaultMutableTreeNode {

    private String text;
    private Icon icon;

    protected abstract String computeText();

    protected abstract Icon computeIcon();

    @Override
    public ValidationNode getParent() {
        return (ValidationNode) super.getParent();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<? extends ValidationNode> getChildren() {
        return (List) children;
    }

    public abstract String getId();

    public String getLabel() {
        return getUserObject().toString();
    }

    public String getText() {
        if (text == null) {
            text = computeText();
        }
        return text;
    }

    public Icon getIcon() {
        if (icon == null) {
            icon = computeIcon();
        }
        return icon;
    }

    public int getMessagesCount() {
        return getMessages() == null ? 0 : getMessages().size();
    }

    public List<ValidationResultDtoMessage> getMessages() {
        return null;
    }

    public Map<String, List<ValidationResultDtoMessage>> getMessagesByLabel() {
        List<ValidationResultDtoMessage> messages = getMessages();
        if (messages == null) {
            return null;
        }
        Map<String, List<ValidationResultDtoMessage>> result = new TreeMap<>();
        for (ValidationResultDtoMessage message : messages) {
            String label = t(I18nDecoratorHelper.getPropertyI18nKey(message.getType(), message.getFieldName()));
            result.computeIfAbsent(label, e -> new LinkedList<>()).add(message);
        }
        return result;
    }

    protected String computeText(List<ValidationResultDtoMessage> messages) {
        StringBuilder buffer = new StringBuilder();
        buffer.append(getUserObject().toString());
        buffer.append(String.format(" / %s", ((ToolkitIdLabel) getUserObject()).getId()));
        if (getChildCount() > 0) {
            buffer.append(String.format(" (%s)", getChildCount()));
        }
        if (!messages.isEmpty()) {
            buffer.append(" / ");
            EnumMap<NuitonValidatorScope, Integer> scopes = getScopesCount(messages);
            Iterator<NuitonValidatorScope> itr = scopes.keySet().iterator();
            while (itr.hasNext()) {
                NuitonValidatorScope scope = itr.next();
                int nb = scopes.get(scope);
                String t = scope.getLabel() + "(s)";
                buffer.append(t).append(" : ").append(nb);
                if (itr.hasNext()) {
                    buffer.append(", ");
                }
            }
        }
        return buffer.toString();
    }

    protected EnumMap<NuitonValidatorScope, Integer> getScopesCount(List<ValidationResultDtoMessage> messages) {

        EnumMap<NuitonValidatorScope, MutableInt> temp = new EnumMap<>(NuitonValidatorScope.class);
        for (NuitonValidatorScope scope : NuitonValidatorScope.values()) {
            temp.put(scope, new MutableInt());
        }
        EnumMap<NuitonValidatorScope, Integer> result = new EnumMap<>(NuitonValidatorScope.class);
        if (messages == null) {
            return result;
        }
        messages.stream()
                .map(ValidationResultDtoMessage::getScope)
                .forEach(s -> temp.get(s).incrementAndGet());

        temp.entrySet().stream()
            .filter(e -> e.getValue().intValue() > 0)
            .forEach(entry -> result.put(entry.getKey(), entry.getValue().toInteger()));
        return result;
    }
}
