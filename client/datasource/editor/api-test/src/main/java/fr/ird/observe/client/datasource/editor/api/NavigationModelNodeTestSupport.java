package fr.ird.observe.client.datasource.editor.api;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUI;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUI;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUI;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUI;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUI;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNodes;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScopes;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.spi.module.BusinessModule;
import io.ultreia.java4all.lang.Objects2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

/**
 * Created on 19/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class NavigationModelNodeTestSupport {
    private static final Logger log = LogManager.getLogger(NavigationModelNodeTestSupport.class);

    protected final int expectedCount;

    protected NavigationModelNodeTestSupport(BusinessModule... businessModule) {
        int expectedCount = 1; // root node
        for (BusinessModule module : businessModule) {
            expectedCount += module.computeNodeCount();
        }
        this.expectedCount = expectedCount;
    }

    @Test
    public void loadNodes() {
        Set<Class<? extends NavigationNode>> nodeTypes = NavigationNodes.get().getNodes();
        log.info(String.format("Load %d node(s).", nodeTypes.size()));
        Assert.assertEquals(expectedCount, nodeTypes.size());
        for (Class<? extends NavigationNode> nodeType : nodeTypes) {
            NavigationNode instance = Objects2.newInstance(nodeType);
            Assert.assertNotNull(instance);
        }
    }

    @Test
    public void loadScopes() {
        Map<Class<? extends NavigationNode>, NavigationScope> scopes = NavigationScopes.get().getScopes();
        log.info(String.format("Load %d scopes(s).", scopes.size()));
        Assert.assertEquals(expectedCount, scopes.size());
    }

    protected void assertRootOpenableNode(ContentRootOpenableUINavigationNode node, Class<? extends DataDto> expectedDtoType, Class<? extends DataDtoReference> expectedReferenceType, Class<? extends ContentRootOpenableUI<?, ?>> expectedUiType) {
        Assert.assertEquals(node.getScope().getNodeDataType(), expectedReferenceType);
        Assert.assertEquals(node.getScope().getMainType(), expectedDtoType);
        Assert.assertEquals(node.getScope().getContentUiType(), expectedUiType);
    }

    protected void assertOpenableNode(ContentOpenableUINavigationNode node, Class<? extends DataDto> expectedDtoType, Class<? extends DataDtoReference> expectedReferenceType, Class<? extends ContentOpenableUI<?, ?>> expectedUiType) {
        Assert.assertEquals(node.getScope().getNodeDataType(), expectedReferenceType);
        Assert.assertEquals(node.getScope().getMainType(), expectedDtoType);
        Assert.assertEquals(node.getScope().getContentUiType(), expectedUiType);
    }

    protected void assertRootOpenableListNode(ContentRootListUINavigationNode node, Class<? extends DataDto> expectedDtoType, Class<? extends DataDtoReference> expectedReferenceType, Class<? extends ContentRootListUI<?, ?, ?>> expectedUiType) {
        Assert.assertEquals(node.getScope().getMainReferenceType(), expectedReferenceType);
        Assert.assertEquals(node.getScope().getNodeDataType(), expectedDtoType);
        Assert.assertEquals(node.getScope().getContentUiType(), expectedUiType);
    }

    protected void assertOpenableListNode(ContentListUINavigationNode node, Class<? extends DataDto> expectedDtoType, Class<? extends DataDtoReference> expectedReferenceType, Class<? extends ContentListUI<?, ?, ?>> expectedUiType) {
        Assert.assertEquals(node.getScope().getMainReferenceType(), expectedReferenceType);
        Assert.assertEquals(node.getScope().getNodeDataType(), expectedDtoType);
        Assert.assertEquals(node.getScope().getContentUiType(), expectedUiType);
    }

    protected void assertEditNode(ContentEditUINavigationNode node, Class<? extends DataDto> expectedDtoType, Class<? extends DataDtoReference> expectedReferenceType, Class<? extends ContentEditUI<?, ?>> expectedUiType) {
        Assert.assertEquals(node.getScope().getNodeDataType(), expectedReferenceType);
        Assert.assertEquals(node.getScope().getMainType(), expectedDtoType);
        Assert.assertEquals(node.getScope().getContentUiType(), expectedUiType);
    }

    protected void assertSimpleNode(ContentSimpleUINavigationNode node, Class<? extends DataDto> expectedDtoType, Class<? extends ContentSimpleUI<?, ?>> expectedUiType) {
        Assert.assertEquals(node.getScope().getMainType(), expectedDtoType);
        Assert.assertEquals(node.getScope().getContentUiType(), expectedUiType);
    }

    protected void assertLayoutNode(ContentLayoutUINavigationNode node, Class<? extends DataDto> expectedDtoType, Class<? extends ContentLayoutUI<?, ?>> expectedUiType) {
        Assert.assertEquals(node.getScope().getMainType(), expectedDtoType);
        Assert.assertEquals(node.getScope().getContentUiType(), expectedUiType);
    }

    protected void assertTableNode(ContentTableUINavigationNode node, Class<? extends DataDto> expectedDtoType, Class<? extends ContentTableUI<?, ?, ?>> expectedUiType) {
        Assert.assertEquals(node.getScope().getNodeDataType(), expectedDtoType);
        Assert.assertEquals(node.getScope().getContentUiType(), expectedUiType);
    }
}
