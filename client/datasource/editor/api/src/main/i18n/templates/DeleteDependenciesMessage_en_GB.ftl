<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeletePanel" -->
<#--
 #%L
 ObServe Client :: DataSource :: Editor :: API
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<head>
    <style>
        .error {
            color: red;
            font-style: italic;
            font-weight: bold;
        }

        .warning {
            color: orange;
            font-style: italic;
            font-weight: bold;
        }
    </style>
</head>
<body>
<#if .data_model.isComposition()>
    <h3 class="error">Need to delete other data</h3>
    <p>
        Some data requiring one of the data to delete have been found:
    </p>
    <ul>
        <#list .data_model.compositionsKeySet as key>
            <li>
                ${.data_model.compositionDecorateType(key)}
            </li>
        </#list>
    </ul>
    <p>
        <a href="showCompositions">Details of extra data to delete</a>.
    </p>
    <p class="error">
        To complete the deletion action, those data must be also deleted.
    </p>
</#if>
<#if .data_model.isAggregation()>
    <h3 class="warning">Need to dissociate other data</h3>
    <p>
        Some data using one of the data to delete have been found:
    </p>
    <ul>
        <#list .data_model.aggregationsKeySet as key>
            <li>
                ${.data_model.aggregationDecorateType(key)}
            </li>
        </#list>
    </ul>
    <p>
        <a href="showAggregations">Details on data to dissociate</a>.
    </p>
    <p class="warning">
        To complete the deletion action, those data must be dissociated.
    </p>
</#if>
</body>
</html>
