<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.editor.api.content.referential.usage.UsageForDeleteUIHandler.DeleteReferentialUsagesGetter" -->
<#--
 #%L
 ObServe Client :: DataSource :: Editor :: API
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<head>
    <style>
        .error {
            color: red;
            font-style: italic;
            font-weight: bold;
        }

    </style>
</head>
<body>
<h3 class="error">Need to replace referential</h3>
<p>
    The referential to delete is used by some data or referential.
</p>
<p class="error">
    To complete the deletion action, you must replace this referential.
</p>
</body>
</html>
