<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel" -->
<#-- @ftlvariable name="selectDataModel" type="fr.ird.observe.navigation.tree.selection.SelectionTreeModel" -->
<#--
 #%L
 ObServe Client :: DataSource :: Editor :: API
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<#import "storageModelDataSourceConfiguration_en_GB.ftl" as storageInfo>
<#if .data_model.backupAction>
    <h2>
        <#if .data_model.initModel.onConnectModeLocal || .data_model.initModel.onCreateMode>
            Local database backup
        <#elseif .data_model.initModel.onConnectModeRemote>
            Remote database backup
        <#elseif .data_model.initModel.onConnectModeServer>
            Server backup
        </#if>
    </h2>
    <hr/>
    <h3>Backup location:</h3>
    <ul>
        <li>${.data_model.backupFile.absolutePath}</li>
    </ul>
    <#if .data_model.useSelectData && .data_model.selectDataModel??>
        <h3>Data to export:</h3>
        <ul>
            <li>
                <#assign selectDataModel = .data_model.selectDataModel />
                <#assign selectDataCount = selectDataModel.getSelectedCount() />
                <#if selectDataCount == 0>
                    No data to export
                <#elseif selectDataModel.isDataFull()>
                    All data have to be exported
                    (<#if selectDataCount == 1> 1 trip <#else> ${selectDataCount} trips </#if>).
                <#else>
                    <#if selectDataCount == 1> 1 trip <#else> ${selectDataCount} trips </#if> à exporter
                    <ul>
                        <#list selectDataModel.getSelectedLabels() as key, value>
                            <li>
                                ${key}
                                <ul>
                                    <#list value as trip>
                                        <li>${trip}</li>
                                    </#list>
                                </ul>
                            </li>
                        </#list>
                    </ul>
                </#if>
            </li>
            <li>The referential will be exported.</li>
        </ul>
    </#if>
<#else>
    <#if .data_model.initModel.onConnectMode>
        <#if .data_model.initModel.onConnectModeLocal>
            <h2>Connection to the local database</h2>

            <@storageInfo.storageModelDataSourceInformation .data_model/>

        <#elseif .data_model.initModel.onConnectModeRemote>
            <#if !.data_model.adminAction??>
                <h2>Remote database connection</h2>
                <hr/>
                <h3>Remote database information :</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />
            <#else>
                <h2>${.data_model.adminActionLabel}</h2>
                <hr/>
                <h3>Remote connection information:</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />
                <@storageInfo.onAdminActionCreate .data_model />
                <@storageInfo.security securityModel=.data_model.securityModel dataSourceInformation=.data_model.dataSourceInformation />
            </#if>

        <#elseif .data_model.initModel.onConnectModeServer>
            <#if !.data_model.adminAction??>
                <h2>Remote server connection</h2>
                <hr/>
                <h3>Remote server informations:</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />
            <#else>
                <h2>${.data_model.adminAction.label}</h2>
                <hr/>
                <h3>Remote server informations :</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />
                <@storageInfo.onAdminActionCreate .data_model />
                <@storageInfo.security securityModel=.data_model.securityModel dataSourceInformation=.data_model.dataSourceInformation />
            </#if>
        </#if>
    <#elseif .data_model.initModel.onCreateMode>
        <#if .data_model.doBackup>
            <h2>Local database backup</h2>
            <hr/>
            <h3>Backup location:</h3>
            <ul>
                <li>${.data_model.backupFile.absolutePath}</li>
            </ul>
        </#if>
        <h2>Local database creation</h2>
        <hr/>
        <#if .data_model.initModel.onCreateModeImportInternalDump>
            <h3>Import of reference data from the previous imported reference data:</h3>
            <ul>
                <li>${.data_model.chooseDb.initialDbDump.absolutePath}</li>
            </ul>
        <#elseif .data_model.initModel.onCreateModeImportExternalDump>
            <h3>Backup import:</h3>
            <ul>
                <li>${.data_model.dumpFile.absolutePath}</li>
            </ul>
        <#elseif .data_model.initModel.onCreateModeImportRemoteStorage>
            <h3>Reference data import from a remote database:</h3>
            <@storageInfo.storageModelDataSourceInformation .data_model />
        <#elseif .data_model.initModel.onCreateModeImportServerStorage>
            <h3>Reference data import from a remote server:</h3>
            <@storageInfo.storageModelDataSourceInformation .data_model />
        </#if>
    </#if>
    <h3>Update policy</h3>
    <ul>
        <#if .data_model.willMigrate>
            <li>Will update to version: <strong>${.data_model.modelVersion}</strong></li>
            <li>Versions to apply:
                <ul>
                    <#list .data_model.dataSourceInformation.migrations as migrationVersion>
                        <li>v <em>${migrationVersion}</em></li>
                    </#list>
                </ul>
            </li>
        <#else>
            <li>No update available</li>
        </#if>
    </ul>
</#if>
</body>
</html>
