<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel" -->
<#-- @ftlvariable name="selectDataModel" type="fr.ird.observe.navigation.tree.selection.SelectionTreeModel" -->
<#--
 #%L
 ObServe Client :: DataSource :: Editor :: API
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<#import "storageModelDataSourceConfiguration_es_ES.ftl" as storageInfo>

<#if .data_model.backupAction>
    <h2>
        <#if .data_model.initModel.onConnectModeLocal || .data_model.initModel.onCreateMode>
            Copia de seguridad de la base local
        <#elseif .data_model.initModel.onConnectModeRemote>
            Copia de seguridad de la base remota
        <#elseif .data_model.initModel.onConnectModeServer>
            Copia de seguridad del servicio web
        </#if>
    </h2>
    <hr/>
    <h3>Ubicación de la copia de seguridad:</h3>
    <ul>
        <li>${.data_model.backupFile.absolutePath}</li>
    </ul>
    <#if .data_model.useSelectData && .data_model.selectDataModel??>
        <h3>Datos a esportar:</h3>
        <ul>
            <li>
                <#assign selectDataModel = .data_model.selectDataModel />
                <#assign selectDataCount = selectDataModel.getSelectedCount() />
                <#if selectDataCount == 0>
                    No hay datos de observaciones a exportar
                <#elseif selectDataModel.isDataFull()>
                    Todos los datos del observador deben ser exportados
                    (<#if selectDataCount == 1> 1 marea <#else> ${selectDataCount} mareas </#if>).
                <#else>
                    <#if selectDataCount == 1> 1 marea <#else> ${selectDataCount} mareas </#if> a exportar
                    <ul>
                        <#list selectDataModel.getSelectedLabels() as key, value>
                            <li>
                                ${key}
                                <ul>
                                    <#list value as trip>
                                        <li>${trip}</li>
                                    </#list>
                                </ul>
                            </li>
                        </#list>
                    </ul>
                </#if>
            </li>
            <li>El referencial va a ser exportado</li>
        </ul>
    </#if>
<#else>
    <#if .data_model.initModel.onConnectMode>
        <#if .data_model.initModel.onConnectModeLocal>
            <h2>Conexión a la base local</h2>
            <@storageInfo.storageModelDataSourceInformation .data_model/>

        <#elseif .data_model.initModel.onConnectModeRemote>
            <#if !.data_model.adminAction??>
                <h2>Conexión a una base remota</h2>
                <hr/>
                <h3>Informaciones sobre la base remota a utilizar:</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />
            <#else>
                <h2>${.data_model.adminActionLabel}</h2>
                <hr/>
                <h3>Informaciones sobre la base remota a utilizar:</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />

                <@storageInfo.onAdminActionCreate .data_model />
                <@storageInfo.security securityModel=.data_model.securityModel dataSourceInformation=.data_model.dataSourceInformation />
            </#if>

        <#elseif .data_model.initModel.onConnectModeServer>
            <#if !.data_model.adminAction??>
                <h2>Connexion a un servicio web remoto</h2>
                <hr/>
                <h3>Informaciones sobre el servicio web remoto a utilizar:</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />
            <#else>
                <h2>${.data_model.adminAction.label}</h2>
                <hr/>
                <h3>Informaciones sobre el servicio web remoto a utilizar:</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />
                <@storageInfo.onAdminActionCreate .data_model />
                <@storageInfo.security securityModel=.data_model.securityModel dataSourceInformation=.data_model.dataSourceInformation />
            </#if>
        </#if>
    <#elseif .data_model.initModel.onCreateMode>
        <#if .data_model.doBackup>
            <h2>Copia de seguridad de la base local</h2>
            <hr/>
            <h3>Ubicación de la copia de seguridad:</h3>
            <ul>
                <li>${.data_model.backupFile.absolutePath}</li>
            </ul>
        </#if>
        <h2>Creación de la base local</h2>
        <hr/>
        <#if .data_model.initModel.onCreateModeImportInternalDump>
            <h3>Importación con el último referencial descargado:</h3>
            <ul>
                <li>${.data_model.chooseDb.initialDbDump.absolutePath}</li>
            </ul>
        <#elseif .data_model.initModel.onCreateModeImportExternalDump>
            <h3>Importación con una copia de seguridad:</h3>
            <ul>
                <li>${.data_model.dumpFile.absolutePath}</li>
            </ul>
        <#elseif .data_model.initModel.onCreateModeImportRemoteStorage>
            <h3>Importación del referencial con una base remota:</h3>
            <@storageInfo.storageModelDataSourceInformation .data_model />
        <#elseif .data_model.initModel.onCreateModeImportServerStorage>
            <h3>Importación del referencial con un servicio web remoto:</h3>
            <@storageInfo.storageModelDataSourceInformation .data_model />
        </#if>
    </#if>
    <h3>Política de actualización</h3>
    <ul>
        <#if .data_model.willMigrate>
            <li>Actualización to versión actual: <strong>${.data_model.modelVersion}</strong></li>
            <li>Versions to apply:
                <ul>
                    <#list .data_model.dataSourceInformation.migrations as migrationVersion>
                        <li>v <em>${migrationVersion}</em></li>
                    </#list>
                </ul>
            </li>
        <#else>
            <li>No actualización posible</li>
        </#if>
    </ul>
</#if>
</body>
</html>
