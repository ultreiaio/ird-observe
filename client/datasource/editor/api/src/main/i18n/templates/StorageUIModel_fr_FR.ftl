<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel" -->
<#-- @ftlvariable name="selectDataModel" type="fr.ird.observe.navigation.tree.selection.SelectionTreeModel" -->
<#--
 #%L
 ObServe Client :: DataSource :: Editor :: API
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<#import "storageModelDataSourceConfiguration_fr_FR.ftl" as storageInfo>

<#if .data_model.backupAction>
    <h2>
        <#if .data_model.initModel.onConnectModeLocal || .data_model.initModel.onCreateMode>
            Sauvegarde de la base locale
        <#elseif .data_model.initModel.onConnectModeRemote>
            Sauvegarde de la base distante
        <#elseif .data_model.initModel.onConnectModeServer>
            Sauvegarde du serveur
        </#if>
    </h2>
    <hr/>
    <h3>Emplacement de la sauvegarde :</h3>
    <ul>
        <li>${.data_model.backupFile.absolutePath}</li>
    </ul>
    <#if .data_model.useSelectData && .data_model.selectDataModel??>
        <h3>Données à exporter :</h3>
        <ul>
            <li>
                <#assign selectDataModel = .data_model.selectDataModel />
                <#assign selectDataCount = selectDataModel.getSelectedCount() />
                <#if selectDataCount == 0>
                    Pas de données observateur à exporter
                <#elseif selectDataModel.isDataFull()>
                    Toutes les données observateur sont à exporter
                    (<#if selectDataCount == 1> 1 marée <#else> ${selectDataCount} marées </#if>).
                <#else>
                    <#if selectDataCount == 1> 1 marée <#else> ${selectDataCount} marées </#if> à exporter
                    <ul>
                        <#list selectDataModel.getSelectedLabels() as key, value>
                            <li>
                                ${key}
                                <ul>
                                    <#list value as trip>
                                        <li>${trip}</li>
                                    </#list>
                                </ul>
                            </li>
                        </#list>
                    </ul>
                </#if>
            </li>
            <li>Le référentiel sera exporté.</li>
        </ul>
    </#if>
<#else>
    <#if .data_model.initModel.onConnectMode>
        <#if .data_model.initModel.onConnectModeLocal>
            <h2>Connection à la base locale</h2>
            <@storageInfo.storageModelDataSourceInformation .data_model/>
        <#elseif .data_model.initModel.onConnectModeRemote>
            <#if !.data_model.adminAction??>
                <h2>Connexion directe à une base distante</h2>
                <hr/>
                <h3>Informations sur la base distante à utiliser :</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />
            <#else>
                <h2>${.data_model.adminActionLabel}</h2>
                <hr/>
                <h3>Informations sur la base distante à utiliser :</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />

                <@storageInfo.onAdminActionCreate .data_model />
                <@storageInfo.security securityModel=.data_model.securityModel dataSourceInformation=.data_model.dataSourceInformation />
            </#if>
        <#elseif .data_model.initModel.onConnectModeServer>
            <#if !.data_model.adminAction??>
                <h2>Connexion à un service web distant</h2>
                <hr/>
                <h3>Informations sur le service web distant à utiliser :</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />
            <#else>
                <h2>${.data_model.adminAction.label}</h2>
                <hr/>
                <h3>Informations sur le service web à utiliser :</h3>
                <@storageInfo.storageModelDataSourceInformation .data_model />
                <@storageInfo.onAdminActionCreate .data_model />
                <@storageInfo.security securityModel=.data_model.securityModel dataSourceInformation=.data_model.dataSourceInformation />
            </#if>
        </#if>
    <#elseif .data_model.initModel.onCreateMode>
        <#if .data_model.doBackup>
            <h2>Sauvegarde de la base locale</h2>
            <hr/>
            <h3>Emplacement de la sauvegarde :</h3>
            <ul>
                <li>${.data_model.backupFile.absolutePath}</li>
            </ul>
        </#if>
        <h2>Création de la base locale</h2>
        <hr/>
        <#if .data_model.initModel.onCreateModeImportInternalDump>
            <h3>Import depuis le dernier référentiel téléchargé :</h3>
            <ul>
                <li>${.data_model.chooseDb.initialDbDump.absolutePath}</li>
            </ul>
        <#elseif .data_model.initModel.onCreateModeImportExternalDump>
            <h3>Import depuis une sauvegarde :</h3>
            <ul>
                <li>${.data_model.dumpFile.absolutePath}</li>
            </ul>
        <#elseif .data_model.initModel.onCreateModeImportRemoteStorage>
            <h3>Import du référentiel depuis une une base distante :</h3>
            <@storageInfo.storageModelDataSourceInformation .data_model />
        <#elseif .data_model.initModel.onCreateModeImportServerStorage>
            <h3>Import du référentiel depuis un service web distant :</h3>
            <@storageInfo.storageModelDataSourceInformation .data_model />
        </#if>
    </#if>

    <h3>Politique de mise à jour</h3>
    <ul>
        <#if .data_model.willMigrate>
            <li>Mise à jour vers la version : <strong>${.data_model.modelVersion}</strong></li>
            <li>Versions à appliquer :
                <ul>
                    <#list .data_model.dataSourceInformation.migrations as migrationVersion>
                        <li>v <em>${migrationVersion}</em></li>
                    </#list>
                </ul>
            </li>
        <#else>
            <li>Pas de mise à jour possible</li>
        </#if>
    </ul>
</#if>
</body>
</html>
