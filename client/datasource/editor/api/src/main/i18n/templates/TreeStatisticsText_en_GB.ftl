<#-- @ftlvariable name=".data_model" type="fr.ird.observe.client.datasource.editor.api.config.TreeStatistics" -->
<#--
 #%L
 ObServe Client :: DataSource :: Editor :: API
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<#assign dataCount = .data_model.dataCount />
<#assign allDataCount = .data_model.allDataCount />
<p>
    <b>Module</b> ${.data_model.moduleLabel} /
    <b>Critère </b> «${.data_model.groupByLabel}» /
    <#if dataCount == 1>
        <#if dataCount == allDataCount>
            <b>Une</b> marée affichée sur <b>${allDataCount}</b>
        <#else>
            <b style='color: red;'>Une</b> marée affichée sur <b>${allDataCount}</b>
        </#if>
    <#else>
        <#if dataCount == allDataCount>
            <b>${dataCount}</b> marées affichées sur <b>${allDataCount}</b>
        <#else>
            <b style='color: red;'>${dataCount}</b> marées affichées sur <b>${allDataCount}</b>
        </#if>
    </#if>
</p>
</body>
</html>
