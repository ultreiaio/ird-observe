<#--
 #%L
 ObServe Client :: DataSource :: Editor :: API
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<#import "ObserveDataSourceConfiguration_es_ES.ftl" as configurationMacros>

<#macro storageModelDataSourceInformation storageModel>
<#-- @ftlvariable name="storageModel" type="fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel" -->
    <#if storageModel??>
        <#if storageModel.chooseDb.editLocalConfig>
            <@configurationMacros.localDataSourceConfiguration storageModel.localConfig.configuration/>
        <#elseif storageModel.chooseDb.editRemoteConfig >
            <@configurationMacros.remoteDataSourceConfiguration storageModel.remoteConfig.configuration/>
        <#elseif storageModel.chooseDb.editServerConfig >
            <@configurationMacros.serverDataSourceConfiguration storageModel.serverConfig.configuration />
        </#if>
        <#if storageModel.dataSourceInformation??>
            <@configurationMacros.dataSourceInformationRights storageModel.dataSourceInformation />
        </#if>
    <#else>
        NO STORAGE MODEL
    </#if>
</#macro>
<#macro security securityModel dataSourceInformation>
<#-- @ftlvariable name="securityModel" type="fr.ird.observe.datasource.security.model.DataSourceSecurityModel" -->
<#-- @ftlvariable name="dataSourceInformation" type="fr.ird.observe.datasource.configuration.ObserveDataSourceInformation" -->
    <h3>Seguridad</h3>
    <ul>
        <li><strong>Proprietario: </strong>${securityModel.administrator.name}</li>
        <li><strong>Técnicos: </strong>${securityModel.technicalUserNames?join(", ")}</li>
        <li><strong>Operador de entrada de datos: </strong>${securityModel.dataEntryOperatorUserNames?join(", ")}</li>
        <li><strong>Lectores: </strong>${securityModel.dataUserNames?join(", ")}</li>
        <li><strong>Referenciales: </strong>${securityModel.referentialUserNames?join(", ")}</li>
        <#if dataSourceInformation.owner??>
            <li><strong>Owner</strong></li>
        </#if>
        <#if dataSourceInformation.superUser??>
            <li><strong>Super user</strong></li>
        </#if>
    </ul>
</#macro>
<#macro onAdminActionCreate model>
<#-- @ftlvariable name="model" type="fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel" -->
    <#if model.adminAction.name() == "CREATE">
        <#if model.chooseDb.importReferential>
            <#if model.chooseDb.referentielImportMode.name() == "IMPORT_EXTERNAL_DUMP">
                <h3>Importación del reférencial con una copia de seguridad:</h3>
                <ul>
                    <li>${model.centralSourceModel.dumpFile.absolutePath}</li>
                </ul>
            <#elseif model.chooseDb.referentielImportMode.name() == "IMPORT_REMOTE_STORAGE">
                <h3>Importación del referencial con una base remota:</h3>
                <@storageModelDataSourceInformation model.centralSourceModel />
            <#elseif model.chooseDb.referentielImportMode.name() == "IMPORT_SERVER_STORAGE">
                <h3>Importación del referencial con un servicio web remoto:</h3>
                <@storageModelDataSourceInformation model.centralSourceModel />
            </#if>
        <#else>
            <h3>No Importación de referencial</h3>
        </#if>
        <#if model.chooseDb.importData>
            <#if model.chooseDb.dataImportMode.name() == "IMPORT_EXTERNAL_DUMP">
                <h3>Importación de datos con una copia de seguridad:</h3>
                <ul>
                    <li>${model.dataSourceModel.dumpFile.absolutePath}</li>
                </ul>
            <#elseif model.chooseDb.dataImportMode.name() == "IMPORT_REMOTE_STORAGE">
                <h3>Importación de datos con una base remota:</h3>
                <@storageModelDataSourceInformation model.dataSourceModel />
            <#elseif model.chooseDb.dataImportMode.name() == "IMPORT_SERVER_STORAGE">
                <h3>Importación de datos con un servicio web remoto:</h3>
                <@storageModelDataSourceInformation model.dataSourceModel />
            </#if>
        <#else>
            <h3>No Importación de datos</h3>
        </#if>
    </#if>
</#macro>
