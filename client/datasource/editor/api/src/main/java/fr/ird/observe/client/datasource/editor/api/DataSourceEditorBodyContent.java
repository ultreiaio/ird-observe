package fr.ird.observe.client.datasource.editor.api;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.ErrorHandler;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceListener;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.editor.api.content.data.map.ObserveMapPane;
import fr.ird.observe.client.datasource.editor.api.content.data.map.TripMapUI;
import fr.ird.observe.client.datasource.editor.api.focus.ContentZone;
import fr.ird.observe.client.datasource.editor.api.focus.NavigationZone;
import fr.ird.observe.client.datasource.editor.api.menu.DataSourceEditorMenu;
import fr.ird.observe.client.datasource.editor.api.menu.actions.ChangeStorageAction;
import fr.ird.observe.client.datasource.editor.api.menu.actions.ShowDataSourcePresetsAction;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.actions.CloseStorage;
import fr.ird.observe.client.datasource.editor.api.navigation.actions.ImportAvdthFile;
import fr.ird.observe.client.datasource.editor.api.navigation.actions.ReloadStorage;
import fr.ird.observe.client.datasource.editor.api.navigation.actions.SaveStorageToFile;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.body.MainUIBodyContent;
import fr.ird.observe.client.main.focus.MainUIFocusModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.ProtectedIdsCommon;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.server.security.InvalidAuthenticationTokenException;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JOptionPane;
import java.beans.PropertyChangeListener;
import java.util.Set;
import java.util.function.Supplier;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
@SuppressWarnings("rawtypes")
@AutoService(MainUIBodyContent.class)
public class DataSourceEditorBodyContent extends MainUIBodyContent<DataSourceEditor> implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(DataSourceEditorBodyContent.class);

    private final PropertyChangeListener reload;
    private DataSourceEditorMenu editorMenu;
    private ObserveSwingDataSource dataSource;
    private NavigationNode previousNode;

    public DataSourceEditorBodyContent() {
        super(1, DataSourceEditor.class);
        setSupplier(SingletonSupplier.of(createSupplier()));
        this.reload = e -> reload();
    }

    public void setPreviousNode(NavigationNode previousNode) {
        this.previousNode = previousNode;
    }

    private Supplier<DataSourceEditor> createSupplier() {
        return () -> {
            ObserveMainUI mainUI = getMainUI();
            DataSourceEditorModel model = new DataSourceEditorModel(editorMenu.getUiModel());
            return new DataSourceEditor(UIHelper.initialContext(mainUI, model));
        };
    }

    @Override
    public void install(ObserveMainUI mainUI) {
        super.install(mainUI);
        editorMenu = new DataSourceEditorMenu(mainUI);
        mainUI.getJMenuBar().add(editorMenu, 1);

        editorMenu.getUiModel().init();
        mainUI.getBusyModel().addPropertyChangeListener(BusyModel.BUSY_PROPERTY_NAME, reload);
        ShowDataSourcePresetsAction.init(mainUI, mainUI.getShowDataSourcePresets(), new ShowDataSourcePresetsAction());
        MainUIFocusModel focusModel = getFocusModel();
        focusModel.addAcceptedClassesInBlockingLayer(TripMapUI.class);
        focusModel.addAcceptedClassesInBlockingLayer(ObserveMapPane.class);
    }

    @Override
    public void uninstall(ObserveMainUI mainUI) {
        super.uninstall(mainUI);
        mainUI.getBusyModel().removePropertyChangeListener(BusyModel.BUSY_PROPERTY_NAME, reload);
        MainUIFocusModel focusModel = getFocusModel();
        focusModel.removeAcceptedClassesInBlockingLayer(TripMapUI.class);
        focusModel.removeAcceptedClassesInBlockingLayer(ObserveMapPane.class);
    }

    @Override
    public void show(ObserveMainUI mainUI) {
        try {
            openUI(mainUI);
            super.show(mainUI);
            setFocusZone(NavigationZone.ZONE_NAME);
            setUiStatus(t("observe.ui.message.db.loaded", dataSource.getLabel()));

        } catch (Exception e) {

            // Can't open data source in gui, let's close it right away
            try {
                dataSource.close();
            } finally {
                mainUI.removeBodyContent();
            }
            throw e;
        }
    }

    @Override
    public boolean canQuit(ObserveMainUI mainUI) {
        return !withValue() || get().getContentUIManager().closeSelectedContentUI();
    }

    @Override
    protected void hideSafe(ObserveMainUI mainUI) {
        //
        try {
            super.hideSafe(mainUI);
        } catch (Exception e) {
            log.error("Error while hiding body content", e);
        }
    }

    private void openUI(ObserveMainUI mainUI) {
        ClientConfig config = getClientConfig();
        log.info(String.format("loading ui for storage %s: %s", dataSource.getLabel(), mainUI));
        ProgressionModel progressModel = dataSource.getProgressModel();
        Project navigationEditModel = null;
        try {
            navigationEditModel = config.getNavigationEditModel();
        } catch (Exception e) {
            log.error("Can't load navigation edit model", e);
        }

        boolean withEditModel = navigationEditModel != null;
        getObserveSelectModel().clearModel();
        boolean canWriteData = dataSource.canWriteData();
        if (canWriteData) {
            if (withEditModel) {
                dataSource.sanitizeIds(progressModel, navigationEditModel);
                getObserveEditModel().load(navigationEditModel);
            }
            dataSource.sanitize(progressModel, getFloatingObjectPresetsManager());
        }
        DataSourceEditor dataSourceEditor = get();
        MainUIFocusModel focusModel = getFocusModel();
        focusModel.addZone(new NavigationZone(focusModel, dataSourceEditor));
        focusModel.addZone(new ContentZone(focusModel, dataSourceEditor));

        NavigationNode previousNode = this.previousNode;
        // remove it right now
        setPreviousNode(null);
        loadNavigationUI(dataSourceEditor, progressModel, previousNode);
        progressModel.increments();
    }

    private void reload() {
        if (isAlive()) {
            editorMenu.getUiModel().reload();
        }
    }

    @Override
    public void reloadContent(ObserveMainUI mainUI) {
        ObserveSwingDataSource mainDataSource = getDataSourcesManager().getMainDataSource();
        if (mainDataSource.isOpen()) {
            // need to remove any old listeners
            for (ObserveSwingDataSourceListener listener : mainDataSource.getObserveSwingDataSourceListener()) {
                if (listener instanceof MainDataSourceListener) {
                    log.info("Remove obsolete listener: " + listener);
                    mainDataSource.removeObserveSwingDataSourceListener(listener);
                }
            }
        }
        try {
            mainDataSource.getDataSourceService().retainExistingIds(Set.of(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_FLOAT_SIGNED));
        } catch (Exception e) {
            if (e instanceof InvalidAuthenticationTokenException || ErrorHandler.containsExceptionInStack(e, InvalidAuthenticationTokenException.class)) {
                onInvalidAuthenticationTokenException(mainUI);
                return;
            }
        }
        prepareMainStorage(mainDataSource);
        try {
            setDataSource(mainDataSource);
            mainUI.changeBodyContent(DataSourceEditor.class);
        } catch (Exception e) {
            mainUI.removeBodyContent();
            throw e;
        }
    }

    public void doChangeStorage(DataSourceInitMode initMode, Set<DataSourceConnectMode> dbModes, String title) {
        ChangeStorageAction action = new ChangeStorageAction(initMode, dbModes, title);
        action.run();
    }

    public void doChangeStorage() {
        if (editorMenu != null) {
            ChangeStorageAction action = (ChangeStorageAction) editorMenu.getChangeStorage().getAction();
            action.run();
        }
    }

    public void doImportAvdth() {
        if (isAlive()) {
            ImportAvdthFile action = (ImportAvdthFile) get().getNavigationUI().getImportAvdthFile().getAction();
            action.run();
        }
    }

    public void doCloseStorage() {
        if (isAlive()) {
            CloseStorage action = (CloseStorage) get().getNavigationUI().getCloseStorage().getAction();
            action.run();
        }
    }

    public void doReloadStorage() {
        if (isAlive()) {
            ReloadStorage action = (ReloadStorage) get().getNavigationUI().getReloadStorage().getAction();
            action.run();
        }
    }

    public void doSaveStorageToFile() {
        if (isAlive()) {
            SaveStorageToFile action = (SaveStorageToFile) get().getNavigationUI().getSaveStorageToFile().getAction();
            action.run();
        }
    }

    public void loadStorage(ObserveMainUI mainUI, ObserveSwingDataSource dataSource) throws DatabaseConnexionNotAuthorizedException, BabModelVersionException, DatabaseNotFoundException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException {
        prepareMainStorage(dataSource);
        try {
            dataSource.open();
            mainUI.changeBodyContent(DataSourceEditor.class);
        } catch (Exception e) {
            mainUI.removeBodyContent();
            throw e;
        }
    }

    //FIXME:BodyContent Do not use any longer this code since it modify gui when closing
    //FIXME:BodyContent It is better to init and close ui not from data source states
    public void prepareMainStorage(ObserveSwingDataSource dataSource) {
        getDataSourcesManager().setMainDataSource(dataSource);
        log.info(String.format("Prepare main storage: %s", dataSource));
        dataSource.addObserveSwingDataSourceListener(new MainDataSourceListener(this));
    }

    public void setDataSource(ObserveSwingDataSource dataSource) {
        this.dataSource = dataSource;
        reload();
    }

    protected void onInvalidAuthenticationTokenException(ObserveMainUI ui) {

        // on indique que la source de donnée a expiré
        getDataSourcesManager().getOptionalMainDataSource().ifPresent(ObserveSwingDataSource::expired);

        int result = UIHelper.askUser(ui,
                                      t("observe.ui.datasource.storage.server.sessionExpire.title"),
                                      t("observe.ui.datasource.storage.server.sessionExpire"),
                                      JOptionPane.OK_CANCEL_OPTION,
                                      new Object[]{
                                              t("observe.ui.datasource.storage.server.sessionExpire.reload"),
                                              t("observe.ui.datasource.storage.server.sessionExpire.change"),
                                              t("observe.ui.datasource.storage.server.sessionExpire.close")},
                                      0);

        // FIXME SBavencoff 23/03/2016 est on sùr que l'erreur proviens du main storage ?
        //FIXME:BodyContent il faut faire cela à un autre niveau
        switch (result) {
            case 0:
                doReloadStorage();
                break;
            case 1:
                doChangeStorage();
                break;
            default:
                doCloseStorage();
                break;
        }
    }

    /**
     * Load navigation tree.
     *
     * @param ui            data source ui
     * @param progressModel the progress model to interact with ui
     * @param previousNode  optional previous to select
     */
    protected void loadNavigationUI(DataSourceEditor ui, ProgressionModel progressModel, NavigationNode previousNode) {
        NavigationUI navigationUI = ui.getNavigationUI();
        NavigationTree tree = navigationUI.getTree();
        // need to keep paths now, after populate previous node may be altered
        String[] previousPaths = previousNode == null ? null : previousNode.toPaths();
        tree.getModel().populate();
        progressModel.increments();
        // select initial node
        if (previousNode != null) {
            try {
                tree.reselectInitialNode(previousPaths);
            } catch (Exception e) {
                log.error("Could not load previous node", e);
                previousNode = null;
            }
        }
        if (previousNode == null) {
            try {
                tree.selectInitialNode();
            } catch (Exception e) {
                log.error("Could not load initial node", e);
            }
        }
        progressModel.increments();
        tree.setVisible(true);
    }
}
