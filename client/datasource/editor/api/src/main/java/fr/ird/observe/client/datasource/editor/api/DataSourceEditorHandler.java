package fr.ird.observe.client.datasource.editor.api;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.actions.ChangeEditorFocus;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIManager;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.client.util.session.ObserveSwingSessionHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class DataSourceEditorHandler implements UIHandler<DataSourceEditor>, WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(DataSourceEditorHandler.class);

    private DataSourceEditor ui;
    private ObserveSwingSessionHelper swingSessionHelper;
    private boolean contentAdjusting;

    @Override
    public void beforeInit(DataSourceEditor ui) {
        this.ui = Objects.requireNonNull(ui);
        DataSourceEditorModel model = ui.getContextValue(DataSourceEditorModel.class);
        ui.setContextValue(model.getDatasourceMenuModel());
        swingSessionHelper = getObserveSwingSessionHelper();
        ui.setContextValue(getClientConfig());
        ui.setContextValue(new ContentUIManager(getMainUI().getMainUIBodyContentManager()));
    }

    @Override
    public void afterInit(DataSourceEditor ui) {

        JTable errorTable = ui.getErrorTable();
        UIInitHelper.initErrorTable(errorTable);
        ui.getContentSplitPane().setMinimumSize(new Dimension(100, 150));
        ui.getMessageView().setMinimumSize(new Dimension(200, 50));
        ui.getMessageView().setPreferredSize(new Dimension(200, 100));
        ui.getContent().setMinimumSize(new Dimension(200, 150));

        UIInitHelper.init(ui.getNavigationSplitPane());
        UIInitHelper.init(ui.getContentSplitPane());

        // register this action since there is no editor on which attach this action
        ChangeEditorFocus.init(ui, null, new ChangeEditorFocus());

        ui.getModel().addPropertyChangeListener(DataSourceEditorModel.PROPERTY_CONTENT, evt -> onContentChanged((ContentUI) evt.getOldValue(), (ContentUI) evt.getNewValue()));
    }

    private void onContentChanged(ContentUI previousContentUI, ContentUI contentUI) {
        if (contentAdjusting) {
            return;
        }
        blockFocus();
        contentAdjusting = true;
        try {
            log.info(String.format("Content ui changed from: %s to %s", previousContentUI == null ? null : previousContentUI.getModel().getPrefix(), contentUI == null ? null : contentUI.getModel().getPrefix()));
            swingSessionHelper.save();
            if (previousContentUI != null) {
                log.info(String.format("[%s] Will destroy previous content ui", previousContentUI.getClass().getSimpleName()));
                previousContentUI.destroy();
            }
            if (contentUI == null) {
                setNoContent();
            } else {
                setContent(contentUI);
                openContent(contentUI);
            }
        } catch (Exception e) {
            setNoContent();
            ui.getModel().setContent(null);
            throw e;
        } finally {
            contentAdjusting = false;
            SwingUtilities.invokeLater(this::unblockFocus);
        }
    }

    private void setNoContent() {
        setContent(ui.getEmptySelection());
    }

    private void setContent(JComponent content) {
        log.info("Set content: " + content.getName());
        ui.getContent().removeAll();
        ui.getContent().add(content, BorderLayout.CENTER);
    }

    public void updateContentSize() {
        //FIXME c bizarre dans la méthode suivant on l'utilise avec une valeur 700!!! a revoir
        SwingUtilities.invokeLater(() -> ui.getContentSplitPane().setDividerLocation(0.85));
    }

    public void updateContentSizeForce(int plusSize) {
        ui.getContentSplitPane().setDividerLocation(plusSize);
    }

    public void openContent(ContentUI contentUI) {
        log.info(String.format("%sWill open ui", contentUI.getModel().getPrefix()));
        contentUI.open();
        swingSessionHelper.addComponent(contentUI, true);
        contentUI.onOpened();
        updateContentSize();
        log.info(String.format("%s opened", contentUI.getModel().getPrefix()));
    }

}
