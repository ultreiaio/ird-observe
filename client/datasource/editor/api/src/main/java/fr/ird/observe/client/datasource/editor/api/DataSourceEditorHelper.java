package fr.ird.observe.client.datasource.editor.api;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.InitStorageModel;
import fr.ird.observe.client.datasource.api.InitStorageModelTemplate;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.h2.backup.BackupStorage;
import fr.ird.observe.client.datasource.h2.backup.BackupsManager;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.util.EnumSet;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

public class DataSourceEditorHelper implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(DataSourceEditorHelper.class);

    //FIXME Reuse the load storage action which will soon have the logic to load back a backup and anything else
    //FIXME Need to have a unified API and only one
    //FIXME first try to open specified ds (here local ds)
    //FIXME if can not load local ds, then feedback it and propose to load automatic backup, or to create a new local ds
    public void initStorage(ObserveDataSourceConfiguration previousDataSourceConfiguration) {

        ObserveMainUI mainUI = getMainUI();
        ObserveDataSourcesManager dataSourcesManager = getDataSourcesManager();
        BackupsManager backupsManager = getBackupsManager();
        ClientConfig config = getClientConfig();

        if (previousDataSourceConfiguration != null) {

            // reuse previous data source
            ObserveSwingDataSource previousDataSource = dataSourcesManager.newDataSource(previousDataSourceConfiguration);
            try {
                mainUI.getMainUIBodyContentManager().getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class).loadStorage(mainUI, previousDataSource);
            } catch (Exception e) {
                throw new ObserveSwingTechnicalException(e);
            }

        } else if (config.isLoadLocalStorage()) {

            boolean askToCreate = true;
            BackupStorage lastAutomaticBackup = backupsManager.getAutomaticBackups().stream().findFirst().orElse(null);
            Exception error = null;
            if (config.isLocalStorageExist()) {

                // une base locale existe, on l'ouvre

                ObserveSwingDataSource dataSource = dataSourcesManager.newLocalDataSource();

                try {

                    ObserveDataSourceInformation dataSourceInformation = dataSource.checkCanConnect(false);
                    dataSource.migrateDataIfPossible(dataSourceInformation, config.getModelVersion());

                    // la source sera utilisée dans les ui
                    mainUI.getMainUIBodyContentManager().getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class).loadStorage(mainUI, dataSource);

                    askToCreate = false;
                } catch (DatabaseConnexionNotAuthorizedException e) {
                    // May be data source is locked
                    log.error("Could not load local database", e);
                    JOptionPane.showMessageDialog(
                            mainUI,
                            t("observe.runner.initStorage.local.db.error.notAuthorized"),
                            t("observe.runner.title.error.dialog"),
                            JOptionPane.WARNING_MESSAGE
                    );
                    return;
                } catch (Exception e) {
                    log.error("Could not load local database", e);
                    error = e;
                }
            }
            if (askToCreate) {
                askToCreateLocalDatabase(dataSourcesManager, config, mainUI, lastAutomaticBackup, error);
            }
        }
        log.info(t("observe.runner.initStorage.done"));
    }

    protected void askToCreateLocalDatabase(ObserveDataSourcesManager dataSourcesManager, ClientConfig config, ObserveMainUI mainUI, BackupStorage lastAutomaticBackup, Exception error) {

        InitStorageModel initStorageModel = new InitStorageModel(config.getLocalDBDirectory(), lastAutomaticBackup, error);

        String text = InitStorageModelTemplate.generate(initStorageModel);

        Object[] options;
        int defaultOption;
        boolean noAutomaticBackup = lastAutomaticBackup == null;
        if (noAutomaticBackup || error != null) {
            options = new Object[]{
                    t("observe.runner.initStorage.choice.useRemoteStorage"),
                    t("observe.runner.initStorage.choice.createLocalStorage"),
                    t("observe.runner.initStorage.choice.doNothing")};
            defaultOption = 1;

        } else {
            options = new Object[]{
                    t("observe.runner.initStorage.choice.useRemoteStorage"),
                    t("observe.runner.initStorage.choice.createLocalStorage"),
                    t("observe.runner.initStorage.choice.loadLastAutomaticBackup"),
                    t("observe.runner.initStorage.choice.doNothing")};
            defaultOption = 2;

        }

        JLabel label = new JLabel(text);
        int response = askToUser(
                null,
                error == null ? t("observe.runner.initStorage.title.no.local.db.found") : t("observe.runner.initStorage.title.local.db.error"),
                label,
                JOptionPane.QUESTION_MESSAGE,
                options,
                defaultOption
        );
        log.debug(String.format("response : %d", response));

        DataSourceInitMode initMode = null;
        Set<DataSourceConnectMode> dbModes = EnumSet.noneOf(DataSourceConnectMode.class);
        String title = null;
        if (noAutomaticBackup) {
            if (response != JOptionPane.CLOSED_OPTION && response < 2) {

                if (response == 1) {
                    // creation de la base locale
                    initMode = DataSourceInitMode.CREATE;
                    dbModes.add(DataSourceConnectMode.LOCAL);
                    title = I18n.n("observe.runner.initStorage.title.create.local.db");
                } else {
                    // connexion à une base distante
                    initMode = DataSourceInitMode.CONNECT;
                    dbModes.add(DataSourceConnectMode.REMOTE);
                    dbModes.add(DataSourceConnectMode.SERVER);
                    title = I18n.n("observe.runner.initStorage.title.load.remote.db");
                }
            }
            if (initMode != null && !dbModes.isEmpty()) {
                DataSourceEditorBodyContent body = mainUI.getMainUIBodyContentManager().getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class);
                body.doChangeStorage(initMode, dbModes, title);
            }
        } else {
            if (response != JOptionPane.CLOSED_OPTION && response < 3) {
                switch (response) {
                    case 2:
                        // load last backup
                        loadBackup(dataSourcesManager, mainUI, lastAutomaticBackup);
                        break;
                    case 1:
                        // create local db
                        // creation de la base locale
                        initMode = DataSourceInitMode.CREATE;
                        dbModes.add(DataSourceConnectMode.LOCAL);
                        title = I18n.n("observe.runner.initStorage.title.create.local.db");
                        break;
                    case 0:
                        // use remote db
                        initMode = DataSourceInitMode.CONNECT;
                        dbModes.add(DataSourceConnectMode.REMOTE);
                        dbModes.add(DataSourceConnectMode.SERVER);
                        title = I18n.n("observe.runner.initStorage.title.load.remote.db");
                        break;
                }
            }
            if (initMode != null && !dbModes.isEmpty()) {
                DataSourceEditorBodyContent body = mainUI.getMainUIBodyContentManager().getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class);
                body.doChangeStorage(initMode, dbModes, title);
            }
        }
    }

    protected void loadBackup(ObserveDataSourcesManager dataSourcesManager, ObserveMainUI mainUI, BackupStorage backupStorage) {

        log.info("Will load last backup: " + backupStorage.getFile());
        try {
            ObserveDataSourceConfigurationTopiaH2 dataSourceConfigurationH2 = dataSourcesManager.newH2DataSourceConfiguration(I18n.n("observe.runner.initStorage.label.local"));
            ObserveSwingDataSource dataSource = dataSourcesManager.newDataSource(dataSourceConfigurationH2, SqlScript.of(backupStorage.getFile().toPath().toUri()));
            //FIXME:BodyContent Review progress model, not linked to ui any more...
            mainUI.getMainUIBodyContentManager().getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class).loadStorage(mainUI, dataSource);
        } catch (Exception e) {
            throw new ObserveSwingTechnicalException("Impossible de créer la base locale", e);
        }
    }

}
