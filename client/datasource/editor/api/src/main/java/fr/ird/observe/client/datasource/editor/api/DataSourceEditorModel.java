package fr.ird.observe.client.datasource.editor.api;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.menu.DataSourceEditorMenuModel;
import fr.ird.observe.client.datasource.validation.ContentMessageTableModel;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
@GenerateJavaBeanDefinition
public class DataSourceEditorModel extends AbstractJavaBean {

    public static final String PROPERTY_CONTENT = "content";
    /**
     * Shared message table model.
     */
    private final ContentMessageTableModel messageTableModel = new ContentMessageTableModel();
    /**
     * Data source main menu model.
     */
    private final DataSourceEditorMenuModel datasourceMenuModel;
    /**
     * Selected content in ui (can be null).
     */
    private ContentUI content;

    public DataSourceEditorModel(DataSourceEditorMenuModel datasourceMenuModel) {
        this.datasourceMenuModel = Objects.requireNonNull(datasourceMenuModel);
    }

    public DataSourceEditorMenuModel getDatasourceMenuModel() {
        return datasourceMenuModel;
    }

    public ContentUI getContent() {
        return content;
    }

    @SuppressWarnings("unchecked")
    public <U extends ContentUI> U getTypedContent() {
        return (U) content;
    }

    public void setContent(ContentUI content) {
        ContentUI oldValue = getContent();
        this.content = content;
        firePropertyChange(PROPERTY_CONTENT, oldValue, content);
    }

    public ContentMessageTableModel getMessageTableModel() {
        return messageTableModel;
    }

    /**
     * apply extra actions from previous opened content (go back to correct tab if any, ...)
     *
     * @param previousUi previous ui with ui states to apply
     */
    public void resetFromPreviousUi(ContentUI previousUi) {
        ContentUI newUi = getTypedContent();
        newUi.resetFromPreviousUi(previousUi);
    }
}
