package fr.ird.observe.client.datasource.editor.api;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceEvent;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceListenerAdapter;
import io.ultreia.java4all.application.context.ApplicationContext;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
class MainDataSourceListener extends ObserveSwingDataSourceListenerAdapter implements WithClientUIContextApi {

    private final ObserveDataSourcesManager dataSourcesManager;
    private final DataSourceEditorBodyContent bodyContent;

    public MainDataSourceListener(DataSourceEditorBodyContent bodyContent) {
        this.bodyContent = bodyContent;
        this.dataSourcesManager = getDataSourcesManager();
    }

    @Override
    public void onOpening(ObserveSwingDataSourceEvent event) {
        ObserveSwingDataSource s = event.getSource();
        displayInfo(t("observe.ui.message.db.loading", s.getLabel()));
    }

    @Override
    public void onOpened(ObserveSwingDataSourceEvent event) {
        ObserveSwingDataSource source = event.getSource();
        bodyContent.setDataSource(source);
    }

    @Override
    public void onClosing(ObserveSwingDataSourceEvent event) {
        super.onClosing(event);
        dataSourcesManager.clear();
    }


    @Override
    public void onClosed(ObserveSwingDataSourceEvent event) {
        ObserveSwingDataSource source = event.getSource();
        if (!ApplicationContext.get().isClosed()) {
            displayInfo(t("observe.ui.message.db.closed", source.getLabel()));
        }
        bodyContent.setDataSource(null);
        source.removeObserveSwingDataSourceListener(this);
    }

}
