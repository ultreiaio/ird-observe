package fr.ird.observe.client.datasource.editor.api;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.ObserveKeyStrokesSupport;

import javax.swing.KeyStroke;

/**
 * Created on 03/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ObserveKeyStrokesEditorApi extends ObserveKeyStrokesSupport {

    //FIXME Let's design a nice new API to register KeyStroke (a new annotation, processed to generate KeyStroke resources)
    // Navigation

    public static final KeyStroke KEY_STROKE_MAP_CONFIGURE = KeyStroke.getKeyStroke("ctrl pressed F2");
    public static final KeyStroke KEY_STROKE_NAVIGATION_CONFIGURE = KeyStroke.getKeyStroke("pressed F12");

    public static final KeyStroke KEY_STROKE_STORAGE_MENU_LEFT = KeyStroke.getKeyStroke("pressed F1");
    public static final KeyStroke KEY_STROKE_STORAGE_MENU_RIGHT = KeyStroke.getKeyStroke("pressed F6");

    public static final KeyStroke KEY_STROKE_NAVIGATION_CONFIGURE_LEFT = KeyStroke.getKeyStroke("pressed F2");
    public static final KeyStroke KEY_STROKE_NAVIGATION_CONFIGURE_RIGHT = KeyStroke.getKeyStroke("pressed F7");

    public static final KeyStroke KEY_STROKE_DATA_SYNCHRO_INFO_LEFT = KeyStroke.getKeyStroke("pressed F3");
    public static final KeyStroke KEY_STROKE_DATA_SYNCHRO_INFO_RIGHT = KeyStroke.getKeyStroke("pressed F8");

    public static final KeyStroke KEY_STROKE_SHOW_OBS = KeyStroke.getKeyStroke("ctrl pressed O");
    public static final KeyStroke KEY_STROKE_SHOW_OBSERVATIONS_TRIP_SEGMENT = KeyStroke.getKeyStroke("shift ctrl pressed O");
    public static final KeyStroke KEY_STROKE_SHOW_POINT_COUNT = KeyStroke.getKeyStroke("ctrl pressed P");
    public static final KeyStroke KEY_STROKE_SHOW_LOGBOOK = KeyStroke.getKeyStroke("ctrl pressed L");
    public static final KeyStroke KEY_STROKE_SHOW_LOGBOOK_TRIP_SEGMENT = KeyStroke.getKeyStroke("shift ctrl pressed L");
    public static final KeyStroke KEY_STROKE_LEGEND_TOGGLE = KeyStroke.getKeyStroke("pressed F11");
    public static final KeyStroke KEY_STROKE_LEGEND_TOP = KeyStroke.getKeyStroke("ctrl pressed T");
    public static final KeyStroke KEY_STROKE_LEGEND_BOTTOM = KeyStroke.getKeyStroke("ctrl pressed B");

    public static final KeyStroke KEY_STROKE_TOGGLE_MENU = KeyStroke.getKeyStroke("pressed F12");
    public static final KeyStroke KEY_STROKE_STORAGE_SHOW_CONFIGURE = KeyStroke.getKeyStroke("pressed F2");
    public static final KeyStroke KEY_STROKE_TOGGLE_EDIT = KeyStroke.getKeyStroke("pressed F3");
    public static final KeyStroke KEY_STROKE_STORAGE_BACKUP = KeyStroke.getKeyStroke("pressed F4");
    public static final KeyStroke KEY_STROKE_STORAGE_SHOW_INFO = KeyStroke.getKeyStroke("pressed F10");
    public static final KeyStroke KEY_STROKE_STORAGE_CLOSE = KeyStroke.getKeyStroke("ctrl pressed F4");
    public static final KeyStroke KEY_STROKE_STORAGE_RELOAD = KeyStroke.getKeyStroke("ctrl pressed F5");
    public static final KeyStroke KEY_STROKE_STORAGE_IMPORT_AVDTH = KeyStroke.getKeyStroke("ctrl pressed F6");
    public static final KeyStroke KEY_STROKE_STORAGE_SEARCH_OR_CREATE = KeyStroke.getKeyStroke("pressed F5");

    public static final KeyStroke KEY_STROKE_GO_DOWN = KeyStroke.getKeyStroke("alt pressed DOWN");
    public static final KeyStroke KEY_STROKE_GO_UP = KeyStroke.getKeyStroke("alt pressed UP");
    public static final KeyStroke KEY_STROKE_GO_PREVIOUS = KeyStroke.getKeyStroke("alt pressed LEFT");
    public static final KeyStroke KEY_STROKE_GO_NEXT = KeyStroke.getKeyStroke("alt pressed RIGHT");

    public static final KeyStroke KEY_STROKE_CHANGE_FOCUS = KeyStroke.getKeyStroke("pressed F1");
    public static final KeyStroke KEY_STROKE_CONTENT_CHANGE_MODE = KeyStroke.getKeyStroke("pressed F2");
    public static final KeyStroke KEY_STROKE_EDIT_REFERENTIAL = KeyStroke.getKeyStroke("pressed F2");
    public static final KeyStroke KEY_STROKE_SEARCH_GO_TO = KeyStroke.getKeyStroke("pressed F2");
    public static final KeyStroke KEY_STROKE_SEARCH_CREATE_NEW = KeyStroke.getKeyStroke("pressed F3");
    public static final KeyStroke KEY_STROKE_SEARCH_ADD_CRITERIA = KeyStroke.getKeyStroke("ctrl pressed N");
    public static final KeyStroke KEY_STROKE_SEARCH_DELETE_CRITERIA = KeyStroke.getKeyStroke("pressed F6");

    public static final KeyStroke KEY_STROKE_EXPAND_TREE_TABLE_NODE = KeyStroke.getKeyStroke("ctrl released RIGHT");
    public static final KeyStroke KEY_STROKE_COLLAPSE_TREE_TABLE_NODE = KeyStroke.getKeyStroke("ctrl released LEFT");

    // Selection tree

    public static final KeyStroke KEY_STROKE_SELECT_ALL_TREE = KeyStroke.getKeyStroke("ctrl pressed S");
    public static final KeyStroke KEY_STROKE_UNSELECT_ALL_TREE = KeyStroke.getKeyStroke("ctrl pressed U");

    // Content

    public static final KeyStroke KEY_STROKE_INSERT_CONFIGURE = KeyStroke.getKeyStroke("pressed F3");
    public static final KeyStroke KEY_STROKE_RESET_DATA = KeyStroke.getKeyStroke("pressed F4");
    public static final KeyStroke KEY_STROKE_SAVE_DATA = KeyStroke.getKeyStroke("pressed F5");
    public static final KeyStroke KEY_STROKE_DELETE_DATA_GLOBAL = KeyStroke.getKeyStroke("pressed F6");
    public static final KeyStroke KEY_STROKE_MOVE = KeyStroke.getKeyStroke("pressed F7");

    public static final KeyStroke KEY_STROKE_MOVE_LAYOUT = KeyStroke.getKeyStroke("pressed F8");
    public static final KeyStroke KEY_STROKE_ADD_PRESET = KeyStroke.getKeyStroke("pressed F9");
    public static final KeyStroke KEY_STROKE_FIX = KeyStroke.getKeyStroke("pressed F9");
    public static final KeyStroke KEY_STROKE_GENERATE = KeyStroke.getKeyStroke("pressed F9");
    public static final KeyStroke KEY_STROKE_CHANGE_ID = KeyStroke.getKeyStroke("pressed F7");
    public static final KeyStroke KEY_STROKE_BULK_MODIFY = KeyStroke.getKeyStroke("pressed F7");

    public static final KeyStroke KEY_STROKE_SHOW_TECHNICAL_INFORMATION = KeyStroke.getKeyStroke("pressed F10");
    public static final KeyStroke KEY_STROKE_REPLACE_REFERENTIAL = KeyStroke.getKeyStroke("pressed F8");
    public static final KeyStroke KEY_STROKE_SHOW_USAGES = KeyStroke.getKeyStroke("pressed F9");
    public static final KeyStroke KEY_STROKE_SHOW_UNIQUE_KEYS = KeyStroke.getKeyStroke("pressed F9");

    public static final KeyStroke KEY_STROKE_SELECT_TARGET = KeyStroke.getKeyStroke("pressed F2");
    public static final KeyStroke KEY_STROKE_SHOW_REPORT = KeyStroke.getKeyStroke("pressed F2");

    public static final KeyStroke KEY_STROKE_GO_TO_OPEN = KeyStroke.getKeyStroke("alt pressed O");
    public static final KeyStroke KEY_STROKE_TOGGLE_OBSERVATION_AVAILABILITY = KeyStroke.getKeyStroke("ctrl pressed F1");
    public static final KeyStroke KEY_STROKE_TOGGLE_LOGBOOK_AVAILABILITY = KeyStroke.getKeyStroke("ctrl pressed F2");
    public static final KeyStroke KEY_STROKE_COPY_TO_CLIPBOARD = KeyStroke.getKeyStroke("ctrl pressed C");

    public static final KeyStroke KEY_STROKE_BACK_TO_REFERENTIAL_LIST = KeyStroke.getKeyStroke("alt pressed BACK_SPACE");

    // Content Table
    public static final KeyStroke KEY_STROKE_SAVE_TABLE_ENTRY = KeyStroke.getKeyStroke("ctrl pressed S");
    public static final KeyStroke KEY_STROKE_SAVE_AND_NEW_TABLE_ENTRY = KeyStroke.getKeyStroke("ctrl pressed N");
    public static final KeyStroke KEY_STROKE_RESET_TABLE_ENTRY = KeyStroke.getKeyStroke("ctrl pressed R");
    public static final KeyStroke KEY_STROKE_SELECT_FIRST_TABLE_ENTRY = KeyStroke.getKeyStroke("alt pressed PAGE_UP");
    public static final KeyStroke KEY_STROKE_SELECT_PREVIOUS_TABLE_ENTRY = KeyStroke.getKeyStroke("alt pressed UP");
    public static final KeyStroke KEY_STROKE_SELECT_NEXT_TABLE_ENTRY = KeyStroke.getKeyStroke("alt pressed DOWN");
    public static final KeyStroke KEY_STROKE_SELECT_LAST_TABLE_ENTRY = KeyStroke.getKeyStroke("alt pressed PAGE_DOWN");

    public static final KeyStroke KEY_STROKE_MOVE_DOWN_TABLE_ENTRY = KeyStroke.getKeyStroke("altGraph pressed DOWN");
    public static final KeyStroke KEY_STROKE_MOVE_BOTTOM_TABLE_ENTRY = KeyStroke.getKeyStroke("altGraph pressed PAGE_DOWN");
    public static final KeyStroke KEY_STROKE_MOVE_UP_TABLE_ENTRY = KeyStroke.getKeyStroke("altGraph pressed UP");
    public static final KeyStroke KEY_STROKE_MOVE_TOP_TABLE_ENTRY = KeyStroke.getKeyStroke("altGraph pressed PAGE_UP");

    public static final KeyStroke KEY_STROKE_ADD_TABLE_ENTRY = KeyStroke.getKeyStroke("ctrl pressed EQUALS");
    public static final KeyStroke KEY_STROKE_DELETE_SELECTED_TABLE_ENTRY = KeyStroke.getKeyStroke("ctrl pressed MINUS");

    // Map

    public static final KeyStroke KEY_STROKE_MAP_EXPORT = KeyStroke.getKeyStroke("ctrl pressed S");
    public static final KeyStroke KEY_STROKE_MAP_ZOOM_IT = KeyStroke.getKeyStroke("ctrl pressed I");
    public static final KeyStroke KEY_STROKE_MAP_ZOOM_PLUS = KeyStroke.getKeyStroke("ctrl pressed P");
    public static final KeyStroke KEY_STROKE_MAP_ZOOM_MINUS = KeyStroke.getKeyStroke("ctrl pressed R");

    // WithDataFile

    public static final KeyStroke KEY_STROKE_IMPORT_DATA = KeyStroke.getKeyStroke("ctrl pressed I");
    public static final KeyStroke KEY_STROKE_DELETE_DATA = KeyStroke.getKeyStroke("shift ctrl pressed D");
    public static final KeyStroke KEY_STROKE_EXPORT_DATA = KeyStroke.getKeyStroke("ctrl pressed E");

    // Wizard

    public static final KeyStroke KEY_STROKE_STORAGE_DO_BACKUP = KeyStroke.getKeyStroke("ctrl pressed S");
    public static final KeyStroke KEY_STROKE_STORAGE_DO_CHOOSE_FILE = KeyStroke.getKeyStroke("alt pressed F");

    public static final KeyStroke KEY_STROKE_STORAGE_EXTRACT = KeyStroke.getKeyStroke("pressed F2");
    public static final KeyStroke KEY_STROKE_STORAGE_IMPORT = KeyStroke.getKeyStroke("pressed F3");

    public static final KeyStroke KEY_STROKE_STORAGE_DO_CONFIGURE_REMOTE = KeyStroke.getKeyStroke("alt pressed R");
    public static final KeyStroke KEY_STROKE_STORAGE_DO_CONFIGURE_SERVER = KeyStroke.getKeyStroke("alt pressed S");

    public static final KeyStroke KEY_STROKE_CONFIGURE_LOCAL_SOURCE = KeyStroke.getKeyStroke("alt pressed L");
    public static final KeyStroke KEY_STROKE_CONFIGURE_REMOTE_SOURCE = KeyStroke.getKeyStroke("alt pressed R");

    public static final KeyStroke KEY_STROKE_RESET = KeyStroke.getKeyStroke("ctrl pressed R");
    public static final KeyStroke KEY_STROKE_NEXT_STEP = KeyStroke.getKeyStroke("alt pressed N");
    public static final KeyStroke KEY_STROKE_PREVIOUS_STEP = KeyStroke.getKeyStroke("alt pressed P");
    public static final KeyStroke KEY_STROKE_APPLY = KeyStroke.getKeyStroke("alt pressed U");
    public static final KeyStroke KEY_STROKE_CANCEL = KeyStroke.getKeyStroke("alt pressed A");

}
