package fr.ird.observe.client.datasource.editor.api;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.callback.ObserveUICallback;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.Icon;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
@AutoService(ObserveUICallback.class)
public class ReloadDataSourceCallback implements ObserveUICallback {

    private ObserveMainUI mainUI;

    @Override
    public String getName() {
        return "db";
    }

    @Override
    public void init(ObserveMainUI mainUI) {
        this.mainUI = mainUI;
    }

    @Override
    public void run() {
        DataSourceEditorBodyContent body = Objects.requireNonNull(mainUI).getMainUIBodyContentManager().getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class);
        body.doReloadStorage();
    }

    @Override
    public void close() {
        this.mainUI = null;
    }

    @Override
    public String getLabel() {
        return n("observe.ui.action.reload.storage");
    }

    @Override
    public Icon getIcon() {
        return SwingUtil.getUIManagerActionIcon("db-reload");
    }
}
