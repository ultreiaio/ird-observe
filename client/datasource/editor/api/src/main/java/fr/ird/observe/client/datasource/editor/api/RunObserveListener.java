package fr.ird.observe.client.datasource.editor.api;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.ActionWorkerInMainUIListener;
import fr.ird.observe.client.ErrorHandler;
import fr.ird.observe.client.ObserveSwingApplicationContext;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.util.FloatConverter;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.spi.module.BusinessProjectI18nHelper;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.boot.DefaultI18nBootLoader;
import io.ultreia.java4all.lang.Strings;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.plaf.basic.CalendarHeaderHandler;
import org.jdesktop.swingx.plaf.basic.SpinningCalendarHeaderHandler;
import org.nuiton.converter.ConverterUtil;
import org.nuiton.jaxx.runtime.resources.UIResourcesProviders;
import org.nuiton.jaxx.runtime.swing.application.ActionExecutor;
import org.nuiton.jaxx.runtime.swing.application.ApplicationRunner;
import org.nuiton.jaxx.runtime.swing.application.event.ActionExecutorListener;
import org.nuiton.jaxx.runtime.swing.application.event.ApplicationRunnerEvent;
import org.nuiton.jaxx.runtime.swing.application.event.ApplicationRunnerListener;

import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

import static io.ultreia.java4all.i18n.I18n.t;

public class RunObserveListener implements ApplicationRunnerListener, WithClientUIContextApi {

    private static Logger log = LogManager.getLogger(RunObserveListener.class);
    private final List<ActionExecutorListener> executorListeners;

    /**
     * Method pour vérifier que le moteur de scripting est bien présent.
     * <p>
     * Voir http://forge.codelutin.com/issues/829
     *
     * @since 2.5
     */
    public static void checkScriptEngineFound(ApplicationRunner runner) {

        ScriptEngine scriptEngine = ObserveUtil.getScriptEngine();

        if (scriptEngine == null) {

            // script engine pas trouvé.
            String message = "Could not found script engine, use a Oracle jvm";
            if (Locale.FRANCE.getCountry().equals(Locale.getDefault().getCountry())) {
                message = "Moteur de scripting non trouvé, Utiliser une jvm fournie par Oracle.";
            }
            runner.onError(new ScriptException(message));
        }
    }

    public RunObserveListener(List<ActionExecutorListener> executorListeners) {
        this.executorListeners = Objects.requireNonNull(executorListeners);
    }

    @Override
    public void initOnce(ApplicationRunnerEvent event) {

        ApplicationRunner runner = event.getSource();

        // check script engine exist
        checkScriptEngineFound(runner);

        // Load jaxx ui resources
        UIResourcesProviders.load();

        // on veut avoir les traductions dès le début
        // on charge dans un premier temps les traductions fournies par l'application
        I18n.init(new DefaultI18nBootLoader(I18nConfiguration.createDefaultConfiguration()), Locale.FRANCE);

        // initialisation des convertisseurs

        Converter converter = ConverterUtil.getConverter(Date.class);
        if (converter != null) {
            ConvertUtils.deregister(Date.class);
        }
        DateConverter dateConverter = new DateConverter();
        dateConverter.setUseLocaleFormat(true);
        ConvertUtils.register(dateConverter, Date.class);

        converter = ConverterUtil.getConverter(Float.class);
        if (converter != null) {
            ConvertUtils.deregister(Float.class);
        }
        ConvertUtils.register(new FloatConverter(), Float.class);
        BusinessProjectI18nHelper.getI18nKeyPrefix();

        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2182
        UIManager.put(CalendarHeaderHandler.uiControllerID, SpinningCalendarHeaderHandler.class.getName());
        UIManager.put(SpinningCalendarHeaderHandler.ARROWS_SURROUND_MONTH, true);
        UIManager.put(SpinningCalendarHeaderHandler.FOCUSABLE_SPINNER_TEXT, true);
    }

    @Override
    public void init(ApplicationRunnerEvent event) throws Exception {

        ApplicationRunner runner = event.getSource();
        log.info(t("observe.runner.init", new Date(), Arrays.toString(runner.getArgs())));

        long t0 = System.nanoTime();

        // 1 - preparation de la configuration

        ClientConfig config = ClientConfig.forRunner(runner);

        log.info(t("observe.runner.config.loaded", config.getVersion()));

        // 2 - preparation des répertoires utilisateurs

        config.initUserDirectories();

        // 3 - Chargement de la configuration des logs utilisateur
        initLog(config);

        log.info(t("observe.runner.user.directories.loaded", config.getDataDirectory()));

        // 3 - preparation i18n

        ClientConfig.initI18n(config);

        log.info(t("observe.runner.i18n.loaded", config.getLocale().getDisplayLanguage()));

        // 4 - preparation de la configuration des ui

        config.initUIConfiguration();

        // 5 - preparation du context applicatif

        ObserveSwingApplicationContext.init(config);

        // 6 - détection de la base locale

        config.detectLocalDataBase();

        String time = Strings.convertTime(t0, System.nanoTime());

        log.info(t("observe.runner.context.loaded", time));
    }

    @Override
    public void start(ApplicationRunnerEvent event) throws IllegalAccessException, InstantiationException, InvocationTargetException {

        ApplicationRunner runner = event.getSource();
        log.info(t("observe.runner.start", new Date(), Arrays.toString(runner.getArgs())));

        ApplicationContext context = ApplicationContext.get();
        log.debug("Will use context : " + context);

        // 0 - init executors
        ActionExecutor actionExecutor = getActionExecutor();
        executorListeners.forEach(actionExecutor::addActionExecutorListener);

        // 1 - launch commandline actions

        ClientConfig config = getClientConfig();

        config.get().doAction(ClientConfig.Step.AfterInit.ordinal());

        if (!config.isDisplayMainUI()) {
            log.info(t("observe.runner.quit.without.ui"));

            ApplicationRunner.unlock();
            return;
        }

        // 2 - init validation

//        NuitonValidatorProviders.setValidationContext(getClientValidationContext());

        if (config.isBackupUse()) {
            getBackupsManager().start();
            getLocalDatabaseBackupTimer().start();
        }

        // 3 - init ui

        ObserveMainUI ui = startUI(context, config);
        ErrorHandler errorHandler = new ErrorHandler() {

            @Override
            protected void onInvalidAuthenticationTokenException() {

                // on indique que la source de donnée a expiré
                getDataSourcesManager().getOptionalMainDataSource().ifPresent(ObserveSwingDataSource::expired);

                int result = UIHelper.askUser(ui,
                                              t("observe.ui.datasource.storage.server.sessionExpire.title"),
                                              t("observe.ui.datasource.storage.server.sessionExpire"),
                                              JOptionPane.OK_CANCEL_OPTION,
                                              new Object[]{
                                                      t("observe.ui.datasource.storage.server.sessionExpire.reload"),
                                                      t("observe.ui.datasource.storage.server.sessionExpire.change"),
                                                      t("observe.ui.datasource.storage.server.sessionExpire.close")},
                                              0);

                // FIXME SBavencoff 23/03/2016 est on sùr que l'erreur proviens du main storage ?
                //FIXME:BodyContent il faut faire cela à un autre niveau
                DataSourceEditorBodyContent dataSourceEditor = ui.getMainUIBodyContentManager().getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class);
                switch (result) {
                    case 0:
                        dataSourceEditor.doReloadStorage();
                        break;
                    case 1:
                        dataSourceEditor.doChangeStorage();
                        break;
                    default:
                        dataSourceEditor.doCloseStorage();
                        break;
                }
            }
        };
        Thread.setDefaultUncaughtExceptionHandler(errorHandler);
        actionExecutor.addActionExecutorListener(new ActionWorkerInMainUIListener(errorHandler));

        log.info(t("observe.runner.ui.loaded"));

        Optional<ObserveDataSourceConfiguration> dataSourceToReload = ObserveSwingApplicationContext.getDataSourceToReload();
        ObserveSwingApplicationContext.setDataSourceToReload(null);
        if (!config.isLoadLocalStorage() && dataSourceToReload.isEmpty()) {

            // on ne charge rien au démarrage
            log.info(t("observe.runner.loaded", config.getVersion()));
            return;
        }

        // 4 - init storage

        Runnable initStorage = () -> new DataSourceEditorHelper().initStorage(dataSourceToReload.orElse(null));
        actionExecutor.addAction(t("observe.runner.load.database"), initStorage);

    }

    @Override
    public void close(ApplicationRunnerEvent event) {
        if (ObserveSwingApplicationContext.isInit()) {
            log.debug("Will close context...");
            ApplicationContext.get().close();
        }
    }

    @Override
    public void shutdown(ApplicationRunnerEvent event) throws Exception {
        ApplicationRunner runner = event.getSource();
        log.info("ObServe shutdown at " + new Date());
        try {
            // on éteint le context applicatif
            runner.onClose(false);
            // on ferme le service de traduction uniquement si on quitte definitivement l'application
            I18n.close();
        } finally {
            Runtime.getRuntime().halt(0);
        }
    }

    @Override
    public void shutdownWithError(ApplicationRunnerEvent event) {
        Exception exception = event.getException();
        log.error("error while closing " + exception.getMessage(), exception);
        Runtime.getRuntime().halt(1);
    }

    @Override
    public void error(ApplicationRunnerEvent event) {
        UIHelper.handlingError(event.getException());
    }

    public void initLog(ClientConfig config) {

        File logFile = config.initLog();

        log = LogManager.getLogger(RunObserveListener.class);
        log.info(String.format("Initialisation des logs depuis %s", logFile));
    }

    private ObserveMainUI startUI(ApplicationContext context, ClientConfig config) {
        ObserveMainUI ui = initUI(context, config);
        setMainUIVisible(ui, false);
        return ui;
    }
}
