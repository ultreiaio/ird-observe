package fr.ird.observe.client.datasource.editor.api.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.focus.ContentZone;
import fr.ird.observe.client.datasource.editor.api.focus.NavigationZone;
import fr.ird.observe.client.main.focus.MainUIFocusModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;

/**
 * Created on 11/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ChangeEditorFocus extends DataSourceEditorActionSupport {

    private static final Logger log = LogManager.getLogger(ChangeEditorFocus.class);

    public ChangeEditorFocus() {
        super(null, null, null, ObserveKeyStrokesEditorApi.KEY_STROKE_CHANGE_FOCUS);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, DataSourceEditor ui) {
        MainUIFocusModel focusModel = getFocusModel();
        String oldFocusOwnerZone = focusModel.getFocusOwnerZone();
        String newFocusOwnerZone = oldFocusOwnerZone == null || ContentZone.ZONE_NAME.equals(oldFocusOwnerZone) ? NavigationZone.ZONE_NAME : ContentZone.ZONE_NAME;
        log.info(String.format("Change focus zone from %s to %s", oldFocusOwnerZone, newFocusOwnerZone));
        focusModel.setFocusOwnerZone(newFocusOwnerZone);
    }

    protected boolean canExecuteAction(ActionEvent e) {
        return getDataSourcesManager().getOptionalMainDataSource().isPresent() && getFocusModel() != null;
    }
}
