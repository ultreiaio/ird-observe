package fr.ird.observe.client.datasource.editor.api.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.util.UIHelper;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 20/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class CopyAuthenticationToken extends AbstractAction implements WithClientUIContextApi {
    private final ObserveSwingDataSource source;
    private JComponent popup;

    public CopyAuthenticationToken(ObserveSwingDataSource source) {
        this.source = Objects.requireNonNull(source);
        putValue(SMALL_ICON, UIManager.getIcon("action.report-copy"));
        putValue(NAME, t("observe.ui.choice.copy.token"));
        putValue(SHORT_DESCRIPTION, t("observe.ui.choice.copy.token"));
    }

    public void setPopup(JComponent popup) {
        this.popup = popup;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        UIHelper.copyToClipBoard(source.getConnection().getAuthenticationToken());
        setUiStatus(t("observe.ui.copy.authentication.token.done"));
        if (popup != null) {
            if (popup instanceof JOptionPane) {
                JOptionPane optionPane = (JOptionPane) popup;
                JDialog dialog = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, optionPane);
                dialog.setVisible(false);
            } else {

                popup.setVisible(false);
            }
        }
    }
}
