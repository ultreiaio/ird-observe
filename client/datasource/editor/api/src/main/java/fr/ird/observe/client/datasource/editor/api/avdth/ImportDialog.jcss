/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
#configPanel {
  border:{new TitledBorder(t("observe.ui.datasource.avdth.import.configuration") + "      ")};
}

#block {
  block:{model.isExtractDone()};
}

#importFileLabel {
  text:"observe.ui.datasource.avdth.import.file";
}

#importFile {
  text:{model.getImportFilePath()};
}

#programLabel {
  text:"observe.Common.program";
}

#program {
  selectedItem:{model.getProgram()};
}

#oceanLabel {
  text:"observe.Common.ocean";
}

#ocean {
  selectedItem:{model.getOcean()};
}

#idTimestampLabel {
  text:"observe.ui.datasource.avdth.import.idTimestamp";
  toolTipText:"observe.ui.datasource.avdth.import.idTimestamp.tip";
}

#idTimestamp {
  text:{model.getIdTimestamp() + ""};
  enabled:false;
}

#progress {
  indeterminate:false;
  stringPainted:true;
  model:{model.getProgressionModel()};
  visible:false;
}

#messagesHeaderLabel {
  text:"observe.Common.messages";
  actionIcon:information;
}

#messagesPane {
  columnHeaderView:{messagesHeader};
}

#messages {
  rows:3;
  editable:false;
  focusable:false;
}

#extractActionStatus {
  actionIcon:wizard-state-pending;
}

#importActionStatus {
  actionIcon:wizard-state-pending;
}

#copyToClipBoard {
  focusable:false;
}
