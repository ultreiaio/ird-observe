package fr.ird.observe.client.datasource.editor.api.avdth;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.util.ObserveBlockingLayerUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.referential.common.OceanReference;
import fr.ird.observe.dto.referential.ps.common.ProgramReference;
import fr.ird.observe.spi.ui.BusyLayerUI;
import org.jdesktop.jxlayer.JXLayer;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.Table;

import javax.swing.SwingUtilities;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created on 12/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ImportDialogHandler implements UIHandler<ImportDialog>, WithClientUIContextApi {

    private ImportDialog ui;

    @Override
    public void beforeInit(ImportDialog ui) {
        ImportDialogModel model = ImportDialogModel.create(this);
        this.ui = ui;
        ui.setContextValue(model);
    }

    public void addMessage(String message) {
        if (!message.endsWith("\n")) {
            message += "\n";
        }
        String finalMessage = message;
        SwingUtilities.invokeLater(() -> ui.getMessages().append(finalMessage));
    }

    @Override
    public void afterInit(ImportDialog ui) {
        ImportDialogModel model = ui.getModel();
        UIInitHelper.init(ui.getProgram());
        UIInitHelper.init(ui.getOcean());
        DecoratorService decoratorService = getDecoratorService();
        ui.getProgram().init(decoratorService.getDecoratorByType(ProgramReference.class), model.getPrograms());
        ui.getOcean().init(decoratorService.getDecoratorByType(OceanReference.class), model.getOceans());
        ui.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                SwingUtilities.invokeLater(ui.getChooseFile()::grabFocus);
            }
        });

        ObserveBlockingLayerUI block = ui.getBlock();
        JXLayer<Table> layer = UIHelper.getLayer(ui.getConfigPanel());
        layer.setUI(block);

        // installation layer de blocage en mode busy
        BusyLayerUI.create(ui, model.getBusyModel());
        model.init(ui);
    }

}
