package fr.ird.observe.client.datasource.editor.api.avdth;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.validation.ContentMessageTableModel;
import fr.ird.observe.client.main.focus.FocusDispatcher;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.OceanReference;
import fr.ird.observe.dto.referential.ps.common.ProgramReference;
import fr.ird.observe.services.service.data.ps.AvdthDataImportConfiguration;
import fr.ird.observe.services.service.data.ps.AvdthDataImportResult;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.lang.Strings;

import javax.swing.JButton;
import javax.swing.JComponent;
import java.io.File;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 12/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@GenerateJavaBeanDefinition
public class ImportDialogModel extends AbstractJavaBean {
    public static final String MS_ACCESS_EXTENSION_PATTERN = "^.+\\.mdb|.+\\.MDB$";
    public static final String MS_ACCESS_EXTENSION = ".mdb";
    /**
     * Progression model.
     */
    private final ProgressionModel progressionModel;
    /**
     * Timestamp to used on each imported data.
     */
    private final Date now;
    /**
     * Available programs.
     */
    private final List<ProgramReference> programs;
    /**
     * Available oceans.
     */
    private final List<OceanReference> oceans;
    private final Path temporaryDirectory;
    private final ReferentialLocale referentialLocale;
    private final BusyModel busyModel;
    private final ContentMessageTableModel contentMessageTableModel;
    private final boolean canAlwaysImport;
    /**
     * AVDTH file to import.
     */
    private File importFile;
    /**
     * Program to set on all imported trip.
     */
    private ProgramReference program;
    /**
     * Ocean to set on all imported trip.
     */
    private OceanReference ocean;
    /**
     * Is extract done?
     */
    private boolean extractDone;
    /**
     * Is import done?
     */
    private boolean importDone;
    /**
     * Extract result.
     */
    private AvdthDataImportResult importResult;

    public static String addMsAccessExtension(String filePath) {
        if (!withMsAccessExtension(filePath)) {
            filePath += MS_ACCESS_EXTENSION;
        }
        return filePath;
    }

    public static String removeMsAccessExtension(String filePath) {
        if (withMsAccessExtension(filePath)) {
            filePath = Strings.removeEnd(filePath, MS_ACCESS_EXTENSION);
        }
        return filePath;
    }

    public static boolean withMsAccessExtension(String filePath) {
        return filePath.endsWith(MS_ACCESS_EXTENSION);
    }

    public static ImportDialogModel create(WithClientUIContextApi clientUIContext) {
        ObserveSwingDataSource dataSource = clientUIContext.getDataSourcesManager().getMainDataSource();
        // get only program for logbook
        List<ProgramReference> programs = dataSource.getReferentialService().getReferenceSet(ProgramReference.class, null).stream().filter(ProgramReference::isLogbook).collect(Collectors.toList());
        List<OceanReference> oceans = dataSource.getReferentialService().getReferenceSet(OceanReference.class, null).toArrayList();
        ClientConfig clientConfig = clientUIContext.getClientConfig();
        Path temporaryDirectory = clientConfig.getTemporaryDirectory().toPath();

        ReferentialLocale referentialLocale = clientConfig.getReferentialLocale();
        BusyModel busyModel = clientUIContext.getBusyModel();
        ImportDialogModel model = new ImportDialogModel(programs,
                                                        oceans,
                                                        temporaryDirectory,
                                                        referentialLocale,
                                                        busyModel,
                                                        clientConfig.isAvdthForceImport());
        model.setImportFile(clientConfig.getAvdthDirectory());
        return model;
    }

    public ImportDialogModel(List<ProgramReference> programs, List<OceanReference> oceans, Path temporaryDirectory, ReferentialLocale referentialLocale, BusyModel busyModel, boolean avdthForceImport) {
        this.programs = programs;
        this.canAlwaysImport = avdthForceImport;
        this.oceans = oceans;
        this.temporaryDirectory = temporaryDirectory;
        this.referentialLocale = referentialLocale;
        this.busyModel = busyModel;
        this.now = new Date();
        this.progressionModel = new ProgressionModel();
        this.contentMessageTableModel = new ContentMessageTableModel();
    }

    public boolean isCanAlwaysImport() {
        return canAlwaysImport;
    }

    public ContentMessageTableModel getContentMessageTableModel() {
        return contentMessageTableModel;
    }

    public List<ProgramReference> getPrograms() {
        return programs;
    }

    public List<OceanReference> getOceans() {
        return oceans;
    }

    public BusyModel getBusyModel() {
        return busyModel;
    }

    public void init(ImportDialog ui) {
        addPropertyChangeListener(evt -> {
            if (!"valid".equals(evt.getPropertyName())) {
                firePropertyChange("valid", isValid());
            }
        });
        getBusyModel().addPropertyChangeListener("busy", evt -> ui.getProgress().setVisible((Boolean) evt.getNewValue()));

        JButton extractAction = ui.getExtractAction();
        addPropertyChangeListener("valid", evt -> {
            if (!isExtractDone()) {
                extractAction.setEnabled(Objects.equals(true, evt.getNewValue()));
            }
        });
        progressionModel.installUI(ui.getProgress());
        progressionModel.addPropertyChangeListener("message", evt -> {
            String message = (String) evt.getNewValue();
            if (message.contains("Lecture de la marée")) {
                return;
            }
            ui.getHandler().addMessage(message);
        });
        ui.getProgress().setVisible(false);
        JComponent dispatchFocus = new FocusDispatcher().dispatchFocus(ui.getProgram());
        addPropertyChangeListener("importFile", evt -> {
            File newValue = (File) evt.getNewValue();
            if (newValue == null) {
                setOceanByCode(null);
                return;
            }
            String name = newValue.getName();
            if (name.startsWith("OI_")) {
                // set indian ocean
                setOceanByCode("2");
                dispatchFocus.requestFocusInWindow();
                return;
            }
            if (name.startsWith("OA_") || name.startsWith("ATL_")) {
                // set atlantic ocean
                setOceanByCode("1");
                dispatchFocus.requestFocusInWindow();
            }
        });
        addPropertyChangeListener("program", evt -> {
            ProgramReference newValue = (ProgramReference) evt.getNewValue();
            if (newValue == null) {
                return;
            }
            if (isValid()) {
                ui.getExtractAction().requestFocusInWindow();
            }
        });
    }

    public void setImportFile(String filePath) {
        File importFile = new File(ImportDialogModel.addMsAccessExtension(filePath));
        setImportFile(importFile);
    }

    public boolean isValid() {
        boolean result = program != null && ocean != null && importFile != null && importFile.exists() && withMsAccessExtension(importFile.getName());
        if (result && importResult != null) {
            result = importResult.isValid();
        }
        return result;
    }

    public boolean isExtractDone() {
        return extractDone;
    }

    public void setExtractDone(boolean extractDone) {
        boolean oldValue = this.extractDone;
        this.extractDone = extractDone;
        firePropertyChange("extractDone", oldValue, extractDone);
    }

    public ProgramReference getProgram() {
        return program;
    }

    public void setProgram(ProgramReference program) {
        ProgramReference oldValue = this.program;
        this.program = program;
        firePropertyChange("program", oldValue, program);
    }

    public OceanReference getOcean() {
        return ocean;
    }

    public void setOcean(OceanReference ocean) {
        OceanReference oldValue = this.ocean;
        this.ocean = ocean;
        firePropertyChange("ocean", oldValue, ocean);
    }

    public File getImportFile() {
        return importFile;
    }

    public void setImportFile(File importFile) {
        File oldValue = getImportFile();
        String oldValuePath = getImportFilePath();
        if (Objects.equals(oldValue, importFile)) {
            return;
        }
        this.importFile = importFile;
        firePropertyChange("importFile", oldValue, importFile);
        firePropertyChange("importFilePath", oldValuePath, getImportFilePath());
    }

    public String getImportFilePath() {
        return importFile == null ? "" : removeMsAccessExtension(importFile.getAbsolutePath());
    }

    public long getIdTimestamp() {
        return now.getTime() ;
    }

    public boolean isImportDone() {
        return importDone;
    }

    public void setImportDone(boolean importDone) {
        boolean oldValue = this.importDone;
        this.importDone = importDone;
        firePropertyChange("importDone", oldValue, importDone);
    }

    public ProgressionModel getProgressionModel() {
        return progressionModel;
    }

    public AvdthDataImportConfiguration toImportConfiguration() {
        Path temporaryDirectory = this.temporaryDirectory.resolve(getImportFile().getName() + "-" + getIdTimestamp());
        return new AvdthDataImportConfiguration(
                now,
                getIdTimestamp(),
                getProgram().getTopiaId(),
                getOcean().getTopiaId(),
                referentialLocale,
                temporaryDirectory,
                canAlwaysImport,
                false,
                getImportFile().toPath(),
                temporaryDirectory.resolve("export-" + getImportFile().getName().replace(".mdb", ".sql")),
                progressionModel
        );
    }

    public void setImportResult(AvdthDataImportResult importResult) {
        AvdthDataImportResult oldValue = this.importResult;
        this.importResult = importResult;
        firePropertyChange("importResult", oldValue, importResult);
    }

    public AvdthDataImportResult getImportResult() {
        return importResult;
    }

    public void setOceanByCode(String oceanCode) {
        OceanReference ocean = getOceans().stream().filter(o -> Objects.equals(oceanCode, o.getCode())).findFirst().orElse(null);
        setOcean(ocean);
    }
}
