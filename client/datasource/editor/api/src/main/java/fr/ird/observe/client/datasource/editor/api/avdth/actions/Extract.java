package fr.ird.observe.client.datasource.editor.api.avdth.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.GsonBuilder;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.avdth.ImportDialog;
import fr.ird.observe.client.datasource.editor.api.avdth.ImportDialogModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.services.service.data.ps.AvdthDataImportConfiguration;
import fr.ird.observe.services.service.data.ps.AvdthDataImportResult;
import fr.ird.observe.services.service.data.ps.MissingReferentialException;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.TimeLog;
import org.nuiton.jaxx.runtime.swing.action.RootPaneContainerActionSupport;

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Extract data from AVDTH database to sql script.
 * <p>
 * Created on 15/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class Extract extends RootPaneContainerActionSupport<ImportDialog> implements WithClientUIContextApi {

    public Extract() {
        super(t("observe.ui.datasource.avdth.extract"), t("observe.ui.datasource.avdth.extract.tip"), null, ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_EXTRACT);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ImportDialog ui) {
        ui.getExtractActionStatus().setIcon(UIHelper.getUIManagerActionIcon("wizard-state-running"));
        ui.getExtractAction().setEnabled(false);
        //FIXME I18N
        ui.getHandler().addMessage("Extraction de la base AVDTH terminée en cours...\n");
        getActionExecutor().addAction("Extract AVDTH data", this::execute);
    }

    protected void execute() {
        ImportDialogModel model = ui.getModel();
        AvdthDataImportConfiguration importConfiguration = model.toImportConfiguration();
        try {
            long t0 = TimeLog.getTime();
            ui.getHandler().addMessage(String.format("Extraction du fichier %s en cours...", importConfiguration.getAvdthFile()));
            AvdthDataImportResult result = getDataSourcesManager().getMainDataSource().getPsAvdthService().importData(importConfiguration);
            model.setImportResult(Objects.requireNonNull(result));
            ui.getHandler().addMessage(String.format("Extraction du fichier terminée (en %s).", Strings.convertTime(t0, TimeLog.getTime())));
            SwingUtilities.invokeLater(() -> afterExecute(result));
        } catch (Exception e) {
            afterError(e);
        }
    }

    protected void afterExecute(AvdthDataImportResult result) {
        ImportDialogModel model = ui.getModel();
        model.setExtractDone(true);
        ui.getExtractAction().setEnabled(false);
        String resultJson = new GsonBuilder().setPrettyPrinting().create().toJson(result);
        if (model.isValid()) {
            // success
            ui.getExtractActionStatus().setIcon(UIHelper.getUIManagerActionIcon("wizard-state-successed"));
            ui.getHandler().addMessage("Extraction de la base AVDTH terminée avec succès\n");
            ui.getHandler().addMessage(String.format("Résultat de l'extraction\n%s\n", resultJson));
            ui.getImportAction().setEnabled(true);
            ui.getImportAction().requestFocus();
        } else {
            ui.getHandler().addMessage("Extraction de la base AVDTH terminée en échec\n");
            ui.getHandler().addMessage(String.format("Messages\n%s\n", result.getMessages()));
            ui.getExtractActionStatus().setIcon(UIHelper.getUIManagerActionIcon("wizard-state-failed"));
            ui.getHandler().addMessage(String.format("Résultat de l'extraction\n%s\n", resultJson));
            if (model.isCanAlwaysImport()) {
                ui.getHandler().addMessage("\n***\n\nCertaines données ne seront pas importées (voir la section notReadResult des messages ci-dessus).\nCependant la configuration autorise l'import  (option avdth.forceImport), l'import est donc réalisable.\n\n***\n");
                ui.getImportAction().setEnabled(true);
                ui.getImportAction().requestFocus();
            } else {
                ui.getQuitAction().requestFocus();
            }
        }
    }

    protected void afterError(Exception e) {
        ImportDialogModel model = ui.getModel();
        model.setExtractDone(true);
        ui.getExtractAction().setEnabled(false);
        boolean missingReferential = e instanceof MissingReferentialException;
        if (missingReferential) {
            ui.getHandler().addMessage("Extraction de la base AVDTH terminée en échec, référentiels manquants...\n" + e.getMessage());
        } else {
            StringWriter w = new StringWriter();
            try (PrintWriter writer = new PrintWriter(w)) {
                e.printStackTrace(writer);
            }
            ui.getHandler().addMessage("Extraction de la base AVDTH terminée en échec\n");
            ui.getHandler().addMessage(String.format("Erreur\n%s\n", w));
        }
        ui.getExtractActionStatus().setIcon(UIHelper.getUIManagerActionIcon("wizard-state-failed"));
        ui.getQuitAction().requestFocus();
    }

}
