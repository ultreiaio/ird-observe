package fr.ird.observe.client.datasource.editor.api.avdth.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.avdth.ImportDialog;
import fr.ird.observe.client.datasource.editor.api.avdth.ImportDialogModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.services.service.data.ps.AvdthDataImportResult;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.TimeLog;
import org.nuiton.jaxx.runtime.swing.action.RootPaneContainerActionSupport;

import java.awt.event.ActionEvent;
import java.io.PrintWriter;
import java.io.StringWriter;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 15/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class Import extends RootPaneContainerActionSupport<ImportDialog> implements WithClientUIContextApi {

    public Import() {
        super(t("observe.ui.datasource.avdth.import"), t("observe.ui.datasource.avdth.import.tip"), null, ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_IMPORT);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ImportDialog ui) {
        ui.getImportAction().setEnabled(false);
        ui.getImportActionStatus().setIcon(UIHelper.getUIManagerActionIcon("wizard-state-running"));
        ui.getProgress().setIndeterminate(true);
        //FIXME I18N
        ui.getHandler().addMessage("Import dans la base ObServe en cours...");
        getActionExecutor().addAction("Import AVDTH data", this::execute);
    }

    protected void execute() {
        ImportDialogModel model = ui.getModel();
        try {
            AvdthDataImportResult result = model.getImportResult();
            long t0 = TimeLog.getTime();
            String message = String.format("Import du fichier %s en cours...", result.getSqlResultPath());
            model.getProgressionModel().setMessage(message);
            getDataSourcesManager().getMainDataSource().getDataSourceService().executeSqlScript(result.getSqlResultContent());
            model.getProgressionModel().setMessage(String.format("Import du fichier terminée (en %s).", Strings.convertTime(t0, TimeLog.getTime())));
            getDataSourcesManager().getMainDataSource().setModified(true);
        } catch (Exception e) {
            afterError(e);
        } finally {
            model.setImportDone(true);
            ui.getHandler().addMessage("Import dans la base ObServe terminé.");
            ui.getImportActionStatus().setIcon(UIHelper.getUIManagerActionIcon("wizard-state-successed"));
            ui.getQuitAction().requestFocus();
        }
    }

    protected void afterError(Exception e) {
        ImportDialogModel model = ui.getModel();
        model.setImportDone(true);
        ui.getImportAction().setEnabled(false);
        StringWriter w = new StringWriter();
        try (PrintWriter writer = new PrintWriter(w)) {
            e.printStackTrace(writer);
        }
        ui.getHandler().addMessage("Import dans la base ObServe terminé  en échec\n");
        ui.getHandler().addMessage(String.format("Erreur\n%s\n", w));
        ui.getImportActionStatus().setIcon(UIHelper.getUIManagerActionIcon("wizard-state-failed"));
        ui.getQuitAction().requestFocus();

    }

}
