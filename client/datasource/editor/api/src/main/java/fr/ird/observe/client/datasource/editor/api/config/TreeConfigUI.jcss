/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#loadData {
  enabled:{bean.isCanLoadData()};
}

#loadReferential {
  enabled:{bean.isCanLoadReferential()};
}

#optionPanel {
  border:{BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), I18n.t("observe.ui.datasource.tree.config.option") + "      ")};
}

#moduleChoose {
  border:{BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), I18n.t("observe.Common.navigation.config.moduleName") + "      ")};
}

#groupByChoose {
  border:{BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), I18n.t("observe.Common.navigation.config.groupByName") + "      ")};
}

#groupByOptionsChoose {
  border:{BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), I18n.t("observe.Common.navigation.config.groupByOption") + "      ")};
}

#resetConfiguration {
  enabled:{model.isModified()};
}

#applyConfiguration {
  enabled:{model.isModified() && !model.isEmpty()};
}

#actionsPane {
  border:{BorderFactory.createEmptyBorder(0, 5, 5, 5)};
}

#cancelMessage {
  text:"observe.ui.choice.cancel.tip";
}
