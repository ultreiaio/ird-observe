package fr.ird.observe.client.datasource.editor.api.config;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.client.datasource.editor.api.config.actions.ApplyConfigurationSupport;
import fr.ird.observe.client.datasource.editor.api.config.actions.Cancel;
import fr.ird.observe.client.datasource.editor.api.config.actions.ChangePropertyAction;
import fr.ird.observe.client.datasource.editor.api.config.actions.UseGroupByAction;
import fr.ird.observe.client.datasource.editor.api.config.actions.UseGroupByFlavorAction;
import fr.ird.observe.client.datasource.editor.api.config.actions.UseModuleAction;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.DataGroupByOption;
import fr.ird.observe.dto.data.DataGroupByTemporalOption;
import fr.ird.observe.navigation.tree.GroupByHelper;
import fr.ird.observe.navigation.tree.JXTreeWithNoSearch;
import fr.ird.observe.navigation.tree.TreeConfig;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.Component;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Created on 24/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TreeConfigUIHandler implements UIHandler<TreeConfigUI>, PropertyChangeListener {

    public static final String DISABLED = "$$disabled";
    private static final Logger log = LogManager.getLogger(TreeConfigUIHandler.class);
    private final Map<String, AbstractButton> allModules;
    private final Multimap<String, AbstractButton> allGroupBy;
    private final Map<String, AbstractButton> allOptions;
    private final Multimap<String, AbstractButton> groupByOptions;
    private final Map<String, AbstractButton> temporalOptions;
    /**
     * Keep the default height (to add some extra components for missing groupBy count in a module and always have
     * the same groupByChoose height at any cost).
     */
    private final int defaultGroupByComponentHeight;
    private TreeConfigUI ui;

    public static void updateStatistics(ToolkitTreeFlatModelRootRequest request,
                                        GroupByHelper groupByHelper,
                                        Supplier<Integer> groupByCountSupplier,
                                        Supplier<Long> dataCountSupplier,
                                        Supplier<Long> allDataCountSupplier,
                                        Consumer<String> textConsumer,
                                        Consumer<String> tipConsumer,
                                        Consumer<Icon> iconConsumer) {
        String statisticsText = "";
        String statisticsTip = "";
        Icon icon = null;
        if (request.isLoadData()) {
            TreeStatistics statistics = new TreeStatistics(
                    request,
                    groupByHelper,
                    groupByCountSupplier,
                    dataCountSupplier,
                    allDataCountSupplier);
            statisticsText = TreeStatisticsTemplate.generateTreeStatisticsText(statistics);
            statisticsTip = TreeStatisticsTemplate.generateTreeStatisticsToolTipText(statistics);
            icon = statistics.getModuleIcon();
        }
        textConsumer.accept(statisticsText);
        tipConsumer.accept(statisticsTip);
        iconConsumer.accept(icon);
    }

    public static TreeConfigUI createUI(JAXXObject ui, TreeConfig originalConfig, GroupByHelper groupByHelper, ApplyConfigurationSupport action, JXTreeWithNoSearch tree) {
        return new TreeConfigUI(UIHelper.initialContext(ui, originalConfig)
                                        .add(action)
                                        .add(groupByHelper)
                                        .add(tree));
    }

    public static void showPanel(JComponent editor, TreeConfigUI configUI) {
        JPopupMenu popup = Objects.requireNonNull(configUI.getComponentPopupMenu());
        Dimension preferredSize = configUI.getPreferredSize();
        popup.setMinimumSize(preferredSize);
        popup.removeAll();
        popup.add(configUI);
        popup.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                SwingUtilities.invokeLater(() -> configUI.getModuleChoose().getComponent(0).requestFocus());
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                SwingUtilities.invokeLater(ObserveUtil::cleanMemory);
                popup.removePopupMenuListener(this);
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
                SwingUtilities.invokeLater(ObserveUtil::cleanMemory);
                popup.removePopupMenuListener(this);
            }
        });
        popup.show(editor, 0, 0);
    }

    public static void hideOptions(TreeConfigUI ui) {
        disableOptions(ui);
        ui.getOptionPanel().setVisible(false);
    }

    public static void disableOptions(TreeConfigUI ui) {
        disableOption(ui, TreeConfig.LOAD_DATA);
        disableOption(ui, TreeConfig.LOAD_REFERENTIAL);
        disableOption(ui, TreeConfig.LOAD_EMPTY_GROUP_BY);
    }

    public static void disableOption(TreeConfigUI ui, String id) {
        JComponent component = (JComponent) ui.getObjectById(id);
        component.putClientProperty(DISABLED, true);
    }

    public static void hideData(TreeConfigUI ui) {
        ui.getOptionPanel().setVisible(false);
        ui.getGroupByPanel().setVisible(false);
        ui.getHandler().allModules.values().forEach(e -> e.setEnabled(true));
    }

    public static void hideModule(TreeConfigUI ui) {
        ui.getModuleChoose().setVisible(false);
    }

    public TreeConfigUIHandler() {
        allModules = new TreeMap<>();
        allGroupBy = ArrayListMultimap.create();
        groupByOptions = ArrayListMultimap.create();
        allOptions = new TreeMap<>();
        temporalOptions = new TreeMap<>();
        defaultGroupByComponentHeight = UIManager.getDefaults().getIcon("RadioButton.icon").getIconHeight();
    }

    @Override
    public void beforeInit(TreeConfigUI ui) {
        this.ui = ui;
    }

    @Override
    public void registerActions(TreeConfigUI ui) {

        ChangePropertyAction.create(allOptions, TreeConfig.LOAD_REFERENTIAL_I18N_LABEL, TreeConfig.LOAD_REFERENTIAL_I18N_DESCRIPTION, DtoIconHelper.getReferentialHome(true), TreeConfig.LOAD_REFERENTIAL, ui, newFunctionKeyStroke(13));
        ChangePropertyAction.create(allOptions, TreeConfig.LOAD_DATA_I18N_LABEL, TreeConfig.LOAD_DATA_I18N_DESCRIPTION, DtoIconHelper.getDataHome(true), TreeConfig.LOAD_DATA, ui, newFunctionKeyStroke(14));

        int moduleKeyStrokeCount = 0;
        for (BusinessModule module : ObserveBusinessProject.get().getModules()) {
            List<DataGroupByDtoDefinition<?, ?>> definitions = module.getDataGroupByDtoDefinitions();
            if (!definitions.isEmpty()) {
                String moduleName = module.getName();
                log.debug(String.format("Register module: %s", moduleName));
                UseModuleAction.create(allModules, module, ui, newFunctionKeyStroke(++moduleKeyStrokeCount));
                int groupByKeyStrokeCount = -1;
                for (DataGroupByDtoDefinition<?, ?> definition : definitions) {
                    log.debug(String.format("Register group by: %s - %s", moduleName, definition.getName()));
                    UseGroupByAction.create(moduleName, allGroupBy, definition, ui, newCharKeyStroke(++groupByKeyStrokeCount));
                }
            }
        }
        for (DataGroupByOption option : DataGroupByOption.values()) {
            ChangePropertyAction.create(allOptions, groupByOptions, option, ui, newFunctionKeyStroke(++moduleKeyStrokeCount));
        }
        for (DataGroupByTemporalOption temporalOption : DataGroupByTemporalOption.values()) {
            UseGroupByFlavorAction.create(allOptions, temporalOptions, groupByOptions, temporalOption, ui, newFunctionKeyStroke(++moduleKeyStrokeCount));
        }
        Cancel.init(ui, null, new Cancel());
        ApplyConfigurationSupport applyAction = ui.getContextValue(ApplyConfigurationSupport.class);
        ApplyConfigurationSupport.init(ui, ui.getApplyConfiguration(), Objects.requireNonNull(applyAction, String.format("Can't find applyAction in ui context: %s", ui)));
    }

    @Override
    public void afterInit(TreeConfigUI ui) {
        TreeConfig bean = ui.getBean();
        bean.addPropertyChangeListener(this);
        ui.getModel().open();
        log.info(String.format("Init with configuration: %s", bean));
        onLoadDataChanged(bean.isLoadData());
        reset(bean);
    }

    public void updateOptions() {
        for (Map.Entry<String, Collection<AbstractButton>> entry : groupByOptions.asMap().entrySet()) {
            entry.getValue().forEach(c -> {
                if (Objects.equals(true, c.getClientProperty(DISABLED))) {
                    c.setEnabled(false);
                }
            });
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();
        switch (propertyName) {
            case TreeConfig.LOAD_DATA: {
                updateOption(evt);
                boolean newValue = (boolean) evt.getNewValue();
                onLoadDataChanged(newValue);
                return;
            }
            case TreeConfig.LOAD_REFERENTIAL:
            case TreeConfig.LOAD_DISABLED_GROUP_BY:
            case TreeConfig.LOAD_EMPTY_GROUP_BY:
            case TreeConfig.LOAD_NULL_GROUP_BY:
                updateOption(evt);
                return;
            case TreeConfig.LOAD_TEMPORAL_GROUP_BY: {
                updateOption(evt);
                boolean newValue = (boolean) evt.getNewValue();
                boolean temporal = newValue && ui.getModel().getBean().isLoadData();
                updateTemporalOptionsEnabled(temporal);
                return;
            }
            case TreeConfig.MODULE_NAME: {
                Object newValue = evt.getNewValue();
                allModules.values().stream().filter(e -> newValue.equals(e.getName())).findFirst().ifPresent(e -> e.setSelected(true));
                return;
            }
            case TreeConfig.GROUP_BY_NAME: {
                Object newValue = evt.getNewValue();
                allGroupBy.values().stream().filter(e -> newValue.equals(e.getName())).findFirst().ifPresent(e -> e.setSelected(true));
                return;
            }
            case TreeConfig.GROUP_BY_FLAVOR: {
                String newValue = (String) evt.getNewValue();
                Optional<DataGroupByTemporalOption> temporalOption = DataGroupByTemporalOption.optionalValueOf(newValue);
                if (temporalOption.isPresent()) {
                    DataGroupByTemporalOption option = temporalOption.get();
                    temporalOptions.get(option.name()).setSelected(true);
                }
            }
        }
    }

    public void reset(TreeConfig bean) {
        String moduleName = bean.getModuleName();
        String groupByName = bean.getGroupByName();
        changeModuleName(moduleName, groupByName);
    }

    public void changeModuleName(String moduleName, String newGroupByName) {
        log.info(String.format("Will use module name: %s", moduleName));
        TreeConfig bean = ui.getBean();
        bean.setModuleName(moduleName);

        JPanel groupByChoose = ui.getGroupByChoose();
        for (Component component : groupByChoose.getComponents()) {
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                UseGroupByAction action = (UseGroupByAction) button.getAction();
                action.uninstall();
            }
        }
        groupByChoose.removeAll();
        Collection<AbstractButton> editors = allGroupBy.get(moduleName);
        if (newGroupByName == null) {
            if (!editors.isEmpty()) {
                newGroupByName = editors.iterator().next().getName();
            }
        }
        for (AbstractButton editor : editors) {
            UseGroupByAction action = (UseGroupByAction) editor.getAction();
            action.install();
        }
        int definitionMaxCount = ui.getModel().getDefinitionMaxCount();
        if (definitionMaxCount > editors.size()) {
            // Fill with extra component to always have the same number of component in the groupByChoose
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2340
            for (int i = editors.size(); i < definitionMaxCount; i++) {
                log.info("Add extra dummy component to fix size {} with height {}", i, defaultGroupByComponentHeight);
                groupByChoose.add(Box.createVerticalStrut(defaultGroupByComponentHeight));
            }
        }
        changeGroupByName(newGroupByName);
        SwingUtilities.invokeLater(ui::revalidate);
        SwingUtilities.invokeLater(ui::repaint);
        SwingUtilities.invokeLater(ui::requestFocusInWindow);
    }

    public void changeGroupByName(String groupByName) {
        log.info(String.format("Will use group by name: %s", groupByName));
        TreeConfig bean = ui.getBean();
        bean.setGroupByName(groupByName);
        if (bean.isLoadData() && groupByName != null) {
            List<String> authorizedOptions = ui.getModel().getGroupByOptionNames(groupByName);
            boolean useTemporal = authorizedOptions.contains(TreeConfig.LOAD_TEMPORAL_GROUP_BY);
            log.debug(String.format("Will use options: %s", authorizedOptions));
            log.debug(String.format("Use temporal? %s", useTemporal));
            for (Map.Entry<String, Collection<AbstractButton>> entry : groupByOptions.asMap().entrySet()) {
                String option = entry.getKey();
                boolean enabled = authorizedOptions.contains(option);
                log.debug(String.format("Option: %s - enabled? %b", option, enabled));
                entry.getValue().forEach(c -> c.setEnabled(enabled && !Objects.equals(true, c.getClientProperty(DISABLED))));
            }
            updateTemporalOptionsEnabled(useTemporal && bean.isLoadTemporalGroupBy());
        }
    }

    private void onLoadDataChanged(boolean newValue) {
        allModules.values().forEach(e -> e.setEnabled(newValue));
        allGroupBy.values().forEach(e -> e.setEnabled(newValue));
        groupByOptions.values().forEach(e -> e.setEnabled(newValue));
        String groupByName = ui.getModel().getBean().getGroupByName();
        if (groupByName != null) {
            changeGroupByName(groupByName);
        }
    }

    private void updateOption(PropertyChangeEvent evt) {
        boolean oldValue = (boolean) evt.getOldValue();
        boolean newValue = (boolean) evt.getNewValue();
        if (Objects.equals(oldValue, newValue)) {
            return;
        }
        String propertyName = evt.getPropertyName();
        AbstractButton editor = allOptions.get(propertyName);
        boolean oldSelectedValue = editor.isSelected();
        if (Objects.equals(oldSelectedValue, newValue)) {
            return;
        }
        log.info(String.format("Push back to ui new property name: %s → %b", propertyName, newValue));
        editor.setSelected(newValue);
    }

    private void updateTemporalOptionsEnabled(boolean temporal) {
        log.debug(String.format("updateTemporalOptionsEnabled: %s", temporal));
        temporalOptions.values().forEach(e -> e.setEnabled(temporal));
    }

    private KeyStroke newCharKeyStroke(int keyStrokeCount) {
        return KeyStroke.getKeyStroke("pressed " + (char) ('A' + (char) keyStrokeCount));
    }

    private KeyStroke newFunctionKeyStroke(int keyStrokeCount) {
        return ObserveKeyStrokesSupport.getFunctionKeyStroke(keyStrokeCount);
    }
}
