package fr.ird.observe.client.datasource.editor.api.config;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.navigation.tree.GroupByHelper;
import fr.ird.observe.navigation.tree.TreeConfig;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;

/**
 * Created on 09/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@GenerateJavaBeanDefinition
public class TreeConfigUIModel extends AbstractJavaBean implements PropertyChangeListener {
    public static final String MODIFIED = "modified";
    public static final String EMPTY = "empty";
    /**
     * Properties to monitor on application tree config to fire modified property..
     */
    private static final List<String> MONITORED_PROPERTIES = List.of(
            TreeConfig.LOAD_DATA,
            TreeConfig.LOAD_REFERENTIAL,
            TreeConfig.LOAD_DISABLED_GROUP_BY,
            TreeConfig.LOAD_EMPTY_GROUP_BY,
            TreeConfig.LOAD_NULL_GROUP_BY,
            TreeConfig.LOAD_TEMPORAL_GROUP_BY,
            TreeConfig.GROUP_BY_NAME,
            TreeConfig.GROUP_BY_FLAVOR,
            TreeConfig.MODULE_NAME);
    /**
     * Properties to monitor to fire empty property.
     */
    private static final List<String> EMPTY_PROPERTIES = List.of(
            TreeConfig.LOAD_DATA,
            TreeConfig.LOAD_REFERENTIAL);
    /**
     * Monitor to see if there is something modified in application config.
     */
    private final JavaBeanMonitor monitor;
    /**
     * Original bean (will never modify it).
     */
    private final TreeConfig originalBean;
    /**
     * Then bean used in ui.
     */
    private final TreeConfig bean;
    /**
     * GroupBy helper.
     */
    private final GroupByHelper groupByHelper;
    /**
     * To keep to max count of definitions for a module to fix a minimum size in the ui.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2340">issue 2340</a>
     */
    private final int definitionMaxCount;

    protected TreeConfigUIModel(GroupByHelper groupByHelper, TreeConfig originalBean) {
        this.groupByHelper = groupByHelper;
        this.originalBean = originalBean;
        this.bean = new TreeConfig();
        this.monitor = new JavaBeanMonitor(MONITORED_PROPERTIES.toArray(new String[0]));
        this.definitionMaxCount = computeDefinitionMaxCount(ObserveBusinessProject.get().getModules());
    }

    public int getDefinitionMaxCount() {
        return definitionMaxCount;
    }

    public TreeConfig getOriginalBean() {
        return originalBean;
    }

    public TreeConfig getBean() {
        return bean;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();
        if (MONITORED_PROPERTIES.contains(propertyName)) {
            fireModified();
            if (EMPTY_PROPERTIES.contains(propertyName)) {
                fireEmpty();
            }
        }
    }

    public boolean isEmpty() {
        return !bean.isLoadData() && !bean.isLoadReferential();
    }

    public boolean isModified() {
        return monitor.wasModified();
    }

    public void reset() {
        monitor.reset(bean, true);
        fireModified();
    }

    public void open() {
        bean.init(originalBean);
        monitor.setBean(bean);
        bean.removePropertyChangeListener(this);
        bean.addPropertyChangeListener(this);
    }

    /**
     * @return {@code true} if structure changed, means we won't be able to reselect previous node, {@code false} otherwise.
     */
    public boolean isStructureChanged() {
        Set<String> modifiedProperties = monitor.getModifiedProperties();
        if (modifiedProperties.contains(TreeConfig.LOAD_DATA)) {
            // loadData is now off
            return !bean.isLoadData();
        }
        if (modifiedProperties.contains(TreeConfig.LOAD_REFERENTIAL)) {
            // loadReferential is now off
            return !bean.isLoadReferential();
        }
        if (modifiedProperties.contains(TreeConfig.MODULE_NAME)) {
            // previous module name is off
            return true;
        }
        // changing any other property won't affect structure
        return false;
    }

    public void close(boolean flush) {
        if (flush) {
            monitor.apply(originalBean, true);
        }
        bean.removePropertyChangeListener(this);
    }

    public List<String> getGroupByOptionNames(String groupByName) {
        return groupByHelper.getGroupByOptionNames(groupByName);
    }

    private void fireModified() {
        firePropertyChange(MODIFIED, isModified());
    }

    private void fireEmpty() {
        firePropertyChange(EMPTY, isEmpty());
    }

    private int computeDefinitionMaxCount(List<BusinessModule> modules) {
        int definitionMaxCount = 0;
        for (BusinessModule module : modules) {
            List<DataGroupByDtoDefinition<?, ?>> definitions = module.getDataGroupByDtoDefinitions();
            definitionMaxCount = Math.max(definitionMaxCount, definitions.size());
        }
        return definitionMaxCount;
    }

}
