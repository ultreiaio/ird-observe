package fr.ird.observe.client.datasource.editor.api.config;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.DataGroupByTemporalOption;
import fr.ird.observe.navigation.tree.GroupByHelper;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;

import javax.swing.Icon;
import java.util.function.Supplier;

/**
 * Created on 17/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@GenerateTemplate(template = {"TreeStatisticsText.ftl", "TreeStatisticsToolTipText.ftl"})
public class TreeStatistics {

    private final String moduleLabel;
    private final String moduleDescription;
    private final String groupByLabel;
    private final String groupByDescription;
    private final DataGroupByTemporalOption temporalOption;
    private final boolean loadDisabledGroupBy;
    private final boolean loadEmptyGroupBy;
    private final boolean loadNullGroupBy;
    private final boolean loadTemporalGroupBy;
    private final int groupByCount;
    private final long dataCount;
    private final long allDataCount;
    private final Icon moduleIcon;

    public TreeStatistics(ToolkitTreeFlatModelRootRequest request,
                          GroupByHelper groupByHelper,
                          Supplier<Integer> groupByCountSupplier,
                          Supplier<Long> dataCountSupplier,
                          Supplier<Long> allDataCountSupplier) {

        String moduleName = request.getModuleName();
        String groupByName = request.getGroupByName();
        DataGroupByDtoDefinition<?, ?> definition = groupByHelper.getDefinition(groupByName);
        groupByLabel = definition.getDefinitionLabel(false);
        groupByDescription = groupByLabel + String.format(" ( %s )", definition.getMandatoryLabel());
        groupByCount = groupByCountSupplier.get();
        dataCount = dataCountSupplier.get();
        allDataCount = allDataCountSupplier.get();

        BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByName(moduleName);
        moduleLabel = businessModule.getModuleShortLabel();
        moduleDescription = businessModule.getLabel();
        moduleIcon = DtoIconHelper.getIcon(businessModule.getRootOpenableDataTypes().iterator().next(),"List", true);

        loadDisabledGroupBy = request.isLoadDisabledGroupBy();
        loadEmptyGroupBy = request.isLoadEmptyGroupBy();
        loadNullGroupBy = request.isLoadNullGroupBy();
        loadTemporalGroupBy = request.isLoadTemporalGroupBy();
        if (loadTemporalGroupBy) {
            String groupByFlavor = request.getGroupByFlavor();
            temporalOption = DataGroupByTemporalOption.optionalValueOf(groupByFlavor).orElse(null);
        } else {
            temporalOption = null;
        }
    }

    public DataGroupByTemporalOption getTemporalOption() {
        return temporalOption;
    }

    public String getModuleDescription() {
        return moduleDescription;
    }

    public boolean isLoadDisabledGroupBy() {
        return loadDisabledGroupBy;
    }

    public boolean isLoadEmptyGroupBy() {
        return loadEmptyGroupBy;
    }

    public boolean isLoadNullGroupBy() {
        return loadNullGroupBy;
    }

    public boolean isLoadTemporalGroupBy() {
        return loadTemporalGroupBy;
    }

    public String getModuleLabel() {
        return moduleLabel;
    }

    public int getGroupByCount() {
        return groupByCount;
    }

    public long getDataCount() {
        return dataCount;
    }

    public long getAllDataCount() {
        return allDataCount;
    }

    public String getGroupByLabel() {
        return groupByLabel;
    }

    public String getGroupByDescription() {
        return groupByDescription;
    }

    public Icon getModuleIcon() {
        return moduleIcon;
    }
}
