package fr.ird.observe.client.datasource.editor.api.config.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUIModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.navigation.tree.TreeConfig;
import io.ultreia.java4all.lang.Strings;

import javax.swing.JComponent;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 09/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ApplyConfigurationSupport extends TreeConfigUIActionSupport {

    protected ApplyConfigurationSupport() {
        super(t("observe.ui.action.apply"), t("observe.ui.action.apply"), UIHelper.getContentActionIconKey("save"), ObserveKeyStrokesEditorApi.KEY_STROKE_ALT_ENTER);
    }

    @Override
    protected final int getInputMapCondition() {
        return JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT;
    }

    @Override
    protected final void doActionPerformed(ActionEvent e, TreeConfigUI ui) {
        if (accept(ui)) {
            try {
                doActionPerformed(ui);
            } finally {
                atTheEnd(ui);
            }
        }
    }

    protected final void doActionPerformed(TreeConfigUI ui) {
        TreeConfigUIModel model = ui.getModel();
        boolean canReselectNode = !model.isStructureChanged();
        model.close(true);
        saveConfiguration(model.getOriginalBean());
        addSimpleAction(t("observe.ui.tree.reload"), () -> {
            long t0 = System.nanoTime();
            applyConsumer(ui, canReselectNode);
            setUiStatus(t("observe.ui.tree.loaded", Strings.convertTime(System.nanoTime() - t0)));
        });
    }

    protected boolean accept(TreeConfigUI sender) {
        return super.accept(sender) && sender.getModel().isModified();
    }

    protected abstract void saveConfiguration(TreeConfig originalConfig);

    protected abstract void applyConsumer(TreeConfigUI ui, boolean canReselectNode);

    private void atTheEnd(TreeConfigUI ui) {
        ui.getComponentPopupMenu().setVisible(false);
        ObserveUtil.cleanMemory();
    }

}
