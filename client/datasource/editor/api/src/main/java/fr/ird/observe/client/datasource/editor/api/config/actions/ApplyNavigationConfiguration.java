package fr.ird.observe.client.datasource.editor.api.config.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.navigation.tree.TreeConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.SwingUtilities;
import java.util.Objects;

/**
 * Created on 09/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ApplyNavigationConfiguration extends ApplyConfigurationSupport {
    private static final Logger log = LogManager.getLogger(ApplyNavigationConfiguration.class);

    private NavigationUI navigationUi;
    private NavigationTree tree;
    private NavigationNode selectedNode;

    public NavigationUI navigationUI(TreeConfigUI ui) {
        if (navigationUi == null) {
            navigationUi = (NavigationUI) Objects.requireNonNull(SwingUtilities.getAncestorOfClass(NavigationUI.class, tree(ui)));
        }
        return navigationUi;
    }

    public NavigationTree tree(TreeConfigUI ui) {
        if (tree == null) {
            tree = Objects.requireNonNull(ui.getContextValue(NavigationTree.class));
        }
        return tree;
    }

    public NavigationNode selectedNode(TreeConfigUI ui) {
        if (selectedNode == null) {
            selectedNode = tree(ui).getSelectedNode();
        }
        return selectedNode;
    }

    @Override
    protected boolean accept(TreeConfigUI sender) {
        boolean accept = super.accept(sender);
        if (accept) {
            NavigationTree tree = tree(ui);
            NavigationNode selectedNode = selectedNode(ui);
            log.info("Selected node: " + selectedNode);
            boolean selectionEmpty = tree.isSelectionEmpty();
            tree.clearSelection();
            if (!selectionEmpty && !tree.isSelectionEmpty()) {
                // Could not clear selection, means can't accept this action
                log.info(String.format("Reject action: %s since could not clear navigation selection.", this));
                return false;
            }
        }
        return accept;
    }

    @Override
    protected void saveConfiguration(TreeConfig originalConfig) {
        log.info(String.format("Save with configuration: %s", originalConfig));
        getClientConfig().saveTreeConfig(originalConfig);
    }

    @Override
    protected void applyConsumer(TreeConfigUI ui, boolean canReselectNode) {
        NavigationTree tree = tree(ui);
        NavigationNode selectedNode = selectedNode(ui);
        String[] paths = selectedNode == null ? null : selectedNode.toPaths();
        NavigationUI navigationUI = navigationUI(ui);
        navigationUI.getSearchOrCreate().setEnabled(ui.getModel().getBean().isLoadData());
        try {
            tree.getModel().populate();
        } finally {
            if (canReselectNode) {
                SwingUtilities.invokeLater(() -> tree.reselectInitialNode(paths));
            } else {
                //FIXME We could find out if we can still select something, meanwhile first node is also fair
                SwingUtilities.invokeLater(tree::selectFirstNode);
            }
        }
    }
}
