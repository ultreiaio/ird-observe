package fr.ird.observe.client.datasource.editor.api.config.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.navigation.tree.TreeConfig;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created on 09/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ApplySelectionConfiguration extends ApplyConfigurationSupport {

    public static final String APPLY_CONSUMER = "applyConsumer";
    private final Consumer<TreeConfigUI> consumer;

    public ApplySelectionConfiguration(Consumer<TreeConfigUI> consumer) {
        this.consumer = Objects.requireNonNull(consumer);
    }

    @Override
    protected void saveConfiguration(TreeConfig originalConfig) {
        // never save application configuration from selection
    }

    @Override
    protected void applyConsumer(TreeConfigUI ui, boolean canReselectNode) {
        consumer.accept(ui);
    }

}
