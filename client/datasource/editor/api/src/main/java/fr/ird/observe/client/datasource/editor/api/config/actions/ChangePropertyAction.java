package fr.ird.observe.client.datasource.editor.api.config.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.dto.data.DataGroupByOption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.util.Map;

/**
 * Created by tchemit on 03/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ChangePropertyAction extends TreeConfigUIActionSupport {
    private static final Logger log = LogManager.getLogger(ChangePropertyAction.class);
    private final String type;

    public static ChangePropertyAction create(Map<String, AbstractButton> options, String label, String tip, Icon icon, String type, TreeConfigUI ui, KeyStroke keyStroke) {
        AbstractButton editor = (AbstractButton) ui.getObjectById(type);
        options.put(type, editor);
        return init(ui, editor, new ChangePropertyAction(label, tip, type,icon,  keyStroke));
    }

    public static ChangePropertyAction create(Map<String, AbstractButton> allOptions, Multimap<String, AbstractButton> groupByOptions, DataGroupByOption dataGroupByOption, TreeConfigUI ui, KeyStroke keyStroke) {
        String type = dataGroupByOption.name();
        JCheckBox editor = new JCheckBox();
        editor.setName(type);
        ui.getGroupByOptionsChoose().add(editor);
        ui.get$objectMap().put(type, editor);
        allOptions.put(type, editor);
        groupByOptions.put(type, editor);
        return init(ui, editor, new ChangePropertyAction(dataGroupByOption.getLabel(), dataGroupByOption.getDescription(), type, null, keyStroke));
    }

    private ChangePropertyAction(String label, String tip, String type, Icon icon, KeyStroke keyStroke) {
        super(ChangePropertyAction.class.getName() + "#" + label, label, tip, (String) null, keyStroke);
        this.type = type;
        if (icon!=null) {
            setLargeIcon(icon);
        }
    }

    @Override
    protected void doActionPerformed(ActionEvent e, TreeConfigUI ui) {
        boolean oldValue = ui.getBean().get(type);
        boolean newValue = !oldValue;
        log.info(String.format("Will change model property: %s to value %b", type, newValue));
        ui.getBean().set(type, newValue);
    }
}
