package fr.ird.observe.client.datasource.editor.api.config.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.KeyStroke;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Created by tchemit on 03/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class UseGroupByAction extends TreeConfigUIActionSupport {

    private static final Logger log = LogManager.getLogger(UseGroupByAction.class);
    private final String groupByName;
    private JPanel groupByChoose;
    private JAXXButtonGroup buttonGroup;

    public static UseGroupByAction create(String moduleName, Multimap<String, AbstractButton> allGroupBy, DataGroupByDtoDefinition<?, ?> definition, TreeConfigUI ui, KeyStroke keyStroke) {
        String groupByName = definition.getName();
        JRadioButton editor = new JRadioButton();
        editor.setName(groupByName);
        allGroupBy.put(moduleName, editor);
        String text = definition.getDefinitionLabel(false);
        String tip = text + String.format(" ( %s )", definition.getMandatoryLabel());
        if (definition.isPropertyMandatory()) {
            editor.setFont(editor.getFont().deriveFont(Font.BOLD));
        } else {
            editor.setFont(editor.getFont().deriveFont(Font.ITALIC));
        }
        return init(ui, editor, new UseGroupByAction(definition, text, tip, keyStroke));
    }

    private UseGroupByAction(DataGroupByDtoDefinition<?, ?> definition, String text, String tip, KeyStroke keyStroke) {
        super(UseGroupByAction.class.getName() + "#" + definition.getName(), text, tip, DtoIconHelper.getIcon(definition.getDataType(), true), keyStroke);
        this.groupByName = definition.getName();
    }

    @Override
    public void init() {
        super.init();
        groupByChoose = ui.getGroupByChoose();
        buttonGroup = ui.getGroupByName();
    }

    @Override
    protected void doActionPerformed(ActionEvent e, TreeConfigUI ui) {
        String oldValue = ui.getBean().getGroupByName();
        if (Objects.equals(oldValue, groupByName)) {
            return;
        }
        log.info(String.format("Will use in model groupBy name: %s", groupByName));
        ui.getHandler().changeGroupByName(groupByName);
    }

    public void install() {
        editor.getModel().setGroup(buttonGroup);
        buttonGroup.add(editor);
        ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(getKeyStroke(), getActionCommandKey());
        ui.getActionMap().put(getActionCommandKey(), this);
        groupByChoose.add(editor);
    }

    public void uninstall() {
        ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).remove(getKeyStroke());
        ui.getActionMap().remove(getActionCommandKey());
        buttonGroup.remove(editor);
    }
}
