package fr.ird.observe.client.datasource.editor.api.config.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.dto.data.DataGroupByTemporalOption;
import fr.ird.observe.navigation.tree.TreeConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.AbstractButton;
import javax.swing.JRadioButton;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.Objects;

/**
 * Created by tchemit on 03/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class UseGroupByFlavorAction extends TreeConfigUIActionSupport {

    private static final Logger log = LogManager.getLogger(UseGroupByFlavorAction.class);
    private final String groupByFlavor;

    public static UseGroupByFlavorAction create(Map<String, AbstractButton> allOptions, Map<String, AbstractButton> temporalOptions, Multimap<String, AbstractButton> groupByOptions, DataGroupByTemporalOption groupByFlavor, TreeConfigUI ui, KeyStroke keyStroke) {
        JRadioButton editor = new JRadioButton();
        editor.setEnabled(false);
        String name = groupByFlavor.name();
        editor.setName(name);
        groupByOptions.put(TreeConfig.LOAD_TEMPORAL_GROUP_BY, editor);
        allOptions.put(name, editor);
        temporalOptions.put(name, editor);
        JAXXButtonGroup buttonGroup = ui.getGroupByFlavor();
        editor.getModel().setGroup(buttonGroup);
        buttonGroup.add(editor);
        ui.getGroupByOptionsChoose().add(editor);
        return init(ui, editor, new UseGroupByFlavorAction(groupByFlavor, keyStroke));
    }

    private UseGroupByFlavorAction(DataGroupByTemporalOption groupByFlavor, KeyStroke keyStroke) {
        super(UseGroupByFlavorAction.class.getName() + "#" + groupByFlavor, groupByFlavor.getLabel(), groupByFlavor.getDescription(), (String) null, keyStroke);
        this.groupByFlavor = groupByFlavor.name();
    }

    @Override
    protected void doActionPerformed(ActionEvent e, TreeConfigUI ui) {
        String oldValue = ui.getBean().getGroupByFlavor();
        if (Objects.equals(oldValue, groupByFlavor)) {
            return;
        }
        log.info(String.format("Will use group by flavor: %s", groupByFlavor));
        ui.getBean().setGroupByFlavor(groupByFlavor);
    }
}
