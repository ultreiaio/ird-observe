package fr.ird.observe.client.datasource.editor.api.config.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.spi.module.BusinessModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.AbstractButton;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.Objects;

/**
 * Created by tchemit on 03/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class UseModuleAction extends TreeConfigUIActionSupport {

    private static final Logger log = LogManager.getLogger(UseModuleAction.class);

    private final BusinessModule type;

    public static UseModuleAction create(Map<String, AbstractButton> allModules, BusinessModule type, TreeConfigUI ui, KeyStroke keyStroke) {
        String moduleName = type.getName();
        JToggleButton editor = new JToggleButton();
        editor.setName(moduleName);
        JAXXButtonGroup buttonGroup = ui.getModuleName();
        editor.getModel().setGroup(buttonGroup);
        ui.getModuleChoose().add(editor);
        editor.setEnabled(ui.getBean().isLoadData());
        allModules.put(moduleName, editor);
        return init(ui, editor, new UseModuleAction(type, keyStroke));
    }

    private UseModuleAction(BusinessModule type, KeyStroke keyStroke) {
        super(UseModuleAction.class.getName() + "#" + type, type.getLabel(), type.getLabel(), DtoIconHelper.getIcon(type.getRootOpenableDataTypes().iterator().next(), "List",true), keyStroke);
        this.type = type;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, TreeConfigUI ui) {
        String moduleName = type.getName();
        String oldValue = ui.getBean().getModuleName();
        if (Objects.equals(oldValue, moduleName)) {
            return;
        }
        log.info(String.format("Will use in model module name: %s", moduleName));
        ui.getHandler().changeModuleName(moduleName, null);
    }
}
