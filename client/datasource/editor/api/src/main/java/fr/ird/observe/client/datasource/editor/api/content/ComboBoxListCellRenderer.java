package fr.ird.observe.client.datasource.editor.api.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.ReferentialDtoReference;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Component;

/**
 * Created on 08/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ComboBoxListCellRenderer<R> implements ListCellRenderer<R> {

    private final ListCellRenderer<? super R> renderer;

    public ComboBoxListCellRenderer(ListCellRenderer<? super R> renderer) {
        this.renderer = renderer;
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends R> list,
                                                  R value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {
        //FIXME This is a hack prefer use a SwingX nice renderer
        JLabel comp = (JLabel) renderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        comp.setToolTipText(comp.getText());
        if (value instanceof ReferentialDtoReference) {
            boolean enabled = ((ReferentialDtoReference) value).isEnabled();
            if (!isSelected) {
                comp.setEnabled(enabled);
            } else {
                if (!enabled) {
                    comp.setToolTipText(I18n.t("observe.referential.Referential.disabled", comp.getText()));
                }
            }
        }
        return comp;
    }

}
