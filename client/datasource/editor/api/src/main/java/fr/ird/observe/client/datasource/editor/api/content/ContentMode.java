/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content;

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.Icon;

/**
 * Pour caractériser le mode d'un écran.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_TIP, pattern = "observe.Id.mode.@NAME@.tip")
public enum ContentMode {

    /**
     * pour un écran de création d'une nouvelle donnée
     */
    CREATE,
    /**
     * pour un écran de modification d'une donée
     */
    UPDATE,
    /**
     * pour un écran en lecture seul
     */
    READ;

    private Icon icon;

    public Icon getIcon() {
        return icon == null ? icon = SwingUtil.createActionIcon("mode-" + name().toLowerCase()) : icon;
    }

    public String getTip() {
        return ContentModeI18n.getTip(this);
    }

    public String getValidationContext() {
        return name().toLowerCase();
    }
}
