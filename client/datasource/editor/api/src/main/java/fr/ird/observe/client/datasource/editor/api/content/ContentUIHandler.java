/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content;

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.content.actions.InsertMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewContentTableUIEntry;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeletePanel;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpen;
import fr.ird.observe.client.datasource.editor.api.content.actions.reset.ResetAction;
import fr.ird.observe.client.datasource.editor.api.focus.ContentZone;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScopes;
import fr.ird.observe.client.datasource.validation.ContentMessageTableModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializer;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.UsageCountWithLabel;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.context.DefaultJAXXContext;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;
import javax.swing.MenuElement;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.Container;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Le contrôleur d'un écran d'édition.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public abstract class ContentUIHandler<U extends ContentUI> implements ObserveServicesProvider, WithClientUIContextApi {

    public static final String CLIENT_PROPERTY_TAB_INDEX = "TabIndex";
    static private final Logger log = LogManager.getLogger(ContentUIHandler.class);
    //    private final PropertyChangeListener onPermanentFocusOwnerChangedListener;
    private final SingletonSupplier<ObserveServicesProvider> servicesProviderSupplier;
    protected String prefix;
    protected U ui;
    protected ContentUIInitializer<U> initializer;
    private ContentOpen<U> contentOpen;
    private boolean init;
    //    private boolean focusAdjusting;
    private boolean withMainTab;
    private boolean withSubTab;

    public static <U extends ContentUI> void removeAllMessages(U ui) {
        ContentMessageTableModel model = ui.getErrorTableModel();
        model.removeMessages(ui, null);
    }

    public static void updateMenuAccessibility(JToggleButton toggle, JPopupMenu popup) {
        MenuElement[] subElements = popup.getSubElements();
        boolean enabled = false;
        if (subElements.length > 0) {
            for (MenuElement subElement : subElements) {
                if (subElement.getComponent().isEnabled()) {
                    enabled = true;
                    break;
                }
            }
        }
        toggle.setEnabled(enabled);
    }

    protected ContentUIHandler() {
//        this.onPermanentFocusOwnerChangedListener = this::onPermanentFocusOwnerChanged;
        this.servicesProviderSupplier = SingletonSupplier.of(getDataSourcesManager()::getMainDataSource);
    }

    protected abstract ContentOpen<U> createContentOpen(U ui);

    public abstract void installChangeModeAction();

    protected ContentUIInitializer<U> createContentUIInitializer(U ui) {
        return new ContentUIInitializer<>(ui);
    }

    public final void beforeInit(U ui) {
        this.ui = ui;
        prefix = "[" + ui.getClass().getSimpleName() + "] ";
        log.debug(String.format("%s[%s] Before init %s", prefix, this, prefix));
        this.initializer = createContentUIInitializer(ui);
        initializer.onBeforeInit(this);
    }

    public final void afterInit(U ui) {
        if (init) {
            throw new IllegalStateException("Can't init twice " + this);
        }
        init = true;
        // always set the class name as name of the top component, otherwise when saving via SwingSession we can not
        // distinguish the UI from another one
        ui.setName(ui.getClass().getName());
        this.contentOpen = Objects.requireNonNull(createContentOpen(ui));

        ContentUIModel model = ui.getModel();
        this.prefix = model.getPrefix();
        log.info(String.format("%sInit ui", prefix));
        DefaultUIInitializerResult initializerResult = contentOpen.init(initializer);
        withMainTab = initializerResult.getTabbedPaneValidator() != null;
        withSubTab = initializerResult.getSubTabbedPaneValidator() != null;
        initializer.afterInit(initializerResult);
    }

    protected Container computeFocusOwnerContainer() {
        ContentMode mode = getModel().getStates().getMode();
        Container container;
        if (mode == null || mode == ContentMode.READ || !getModel().getStates().isEditable()) {
            container = ui;
        } else {
            if (withMainTab) {
                container = (Container) ui.getObjectById(DefaultUIInitializer.MAIN_TABBED_PANE);
                if (withSubTab) {
                    container = (Container) ui.getObjectById(DefaultUIInitializer.SUB_TABBED_PANE);
                    if (!container.isShowing()) {
                        // stay on main tabbed pane
                        container = (Container) ui.getObjectById(DefaultUIInitializer.MAIN_TABBED_PANE);
                    }
                }
            } else {
                container = ui.getContentBody();
            }
        }

        return container;
    }

    public Component computeFocusOwner() {
        //FIXME Review this on Focus clean session
        Container container = computeFocusOwnerContainer();
        if (container instanceof JTabbedPane) {
            container = (Container) ((JTabbedPane) container).getSelectedComponent();
        }
        Component result = ui.getFocusTraversalPolicy().getFirstComponent(container);
        if (ui.getModel().getStates().isReadingMode()) {
            return result;
        }
        if (result instanceof JTabbedPane) {
            result = ((JTabbedPane) result).getSelectedComponent();
        }
        return ui.getFocusTraversalPolicy().getFirstComponent((Container) result);
    }

    public void onModeChanged(ContentMode newMode) {
    }

    public void onMainTabChanged(int previousIndex, int selectedIndex) {
    }

    public final void openUI() {
        log.info(String.format("%sOpening ui", prefix));
        contentOpen.doOpen();
        log.info(String.format("%s is opened.", prefix));
    }

    public final void onOpenedUI() {
        log.info(String.format("%sOn opened ui", prefix));
        contentOpen.doOpened();
        log.info(String.format("%s is opened.", prefix));
    }

    public final boolean closeUI() {
        log.info(String.format("%sClosing ui", prefix));
        boolean closed = contentOpen.doClose();
        log.info(String.format("%s is closed.", prefix));
        return closed;
    }

    public final void saveEditUI() {
        JButton saveButton = Objects.requireNonNull((JButton) Objects.requireNonNull(ui.getObjectById("save")));
        @SuppressWarnings("unchecked") ContentUIActionSupport<U> action = (ContentUIActionSupport<U>) saveButton.getAction();
        action.doAction();
    }

    @SuppressWarnings("unchecked")
    public final void resetFromPreviousUi(ContentUI ui) {
        contentOpen.resetFromPreviousUi((U) ui);
    }

    public final void destroyUI() {
        if (ui == null) {
            return;
        }
        UIHelper.removeAllDataBindings(getUi());
        getModel().close();
        removeAllMessages(ui);
        UIHelper.destroy(getUi());
        DefaultJAXXContext delegateContext = (DefaultJAXXContext) getUi().getDelegateContext();
        delegateContext.clear();
    }

    protected void onBeforeInit(U ui, ContentUIInitializer<U> initializer) {
    }

    public void initActions() {
    }

    public void onInit(U ui) {
    }

    public void onOpenForm(Form<?> form) {
    }

    public void onOpenAfterOpenModel() {
    }

    public void onPrepareValidationContext() {
    }

    public void onEndOpenUI() {
        //FIXME We should remove this code when issue #2928 is fixed
        ContentUIModelStates states = ui.getModel().getStates();
        if (!states.isReadingMode()) {
            states.setModified(states.isCreatingMode());
        }
    }

    public void startEditUI() {
        log.info(String.format("%sStarting edit ui", prefix));
        contentOpen.doStartEdit();
        log.info(String.format("%s is editing.", prefix));
    }

    public void stopEditUI() {
        log.info(String.format("%sStopping edit ui", prefix));
        contentOpen.doStopEdit();
        log.info(String.format("%s is no more editing.", prefix));
    }

    public void onInstallValidators(IdDto editBean) {
    }

    public void onUninstallValidators() {
    }

    public NavigationTree getNavigationTree() {
        return getDataSourceEditor().getNavigationUI().getTree();
    }

    public final JComponent getFocusComponent(String name) {
        List<JComponent> jComponents = contentOpen.getFocusComponents().get(name);
        return jComponents.isEmpty() ? null : jComponents.get(0);
    }

    public final List<Component> newComponentArray(Component... components) {
        return Arrays.asList(components);
    }

    public final U getUi() {
        return ui;
    }

    public ContentOpen<U> getContentOpen() {
        return contentOpen;
    }

    public final ResetAction<U> getResetAction() {
        @SuppressWarnings("unchecked") ResetAction<U> reset = (ResetAction<U>) ((JButton) Objects.requireNonNull(ui.getObjectById("reset"))).getAction();
        return reset;
    }

    public final <R extends ReferentialDtoReference> List<R> getReferentialReferences(Class<R> type, String... ids) {
        ObserveSwingDataSource mainDataSource = getDataSourcesManager().getMainDataSource();
        LinkedList<R> result = new LinkedList<>(mainDataSource.getReferentialReferences(type, ids));
        getDecoratorService().installDecorator(type, result.stream());
        return result;
    }

    @Override
    public final <S extends ObserveService> S getService(Class<S> serviceType) {
        return servicesProviderSupplier.get().getService(serviceType);
    }

    public final DataSourceEditor getDataSourceEditor() {
        return (DataSourceEditor) getMainUI().getMainUIBodyContentManager().getCurrentBody().get();
    }

    public ContentUIModel getModel() {
        return getUi().getModel();
    }

    public final void fixFormSize() {
        // we want to see the hole form on screen
        ui.setMinimumSize(ui.getPreferredSize());
        getDataSourceEditor().updateContentSize();
    }

    /**
     * @return actions usable in the navigation popup.
     */
    public List<AbstractButton> getNavigationPopupActions() {
        List<AbstractButton> result = new LinkedList<>();
        collectNavigationPopupActions(result, ui, "actions", "extraActions", "navigationActions");
        for (Object object : ui.get$objectMap().values()) {
            if (object != ui && object instanceof ContentUI) {
                collectNavigationPopupActions(result, (JAXXObject) object, "actions", "extraActions", "navigationActions");
            }
        }
        return result;
    }

    public final void grabFocusOnForm() {
        getFocusModel().refreshZoneFocus(ContentZone.ZONE_NAME);
    }

    public void addSubInsertAction(JComponent subElement1) {
        if (subElement1 instanceof AbstractButton) {
            InsertMenuAction<?> action = (InsertMenuAction<?>) ((AbstractButton) subElement1).getAction();
            action.moveTo(ui.getInsertPopup());
        } else {
            ui.getInsertPopup().add(subElement1);
        }
    }

    protected <J extends JComponent> Optional<J> getComponent(Class<J> componentType, String id) {
        Object objectById = ui.getObjectById(id);
        if (objectById == null) {
            return Optional.empty();
        }
        return Optional.of(componentType.cast(objectById));
    }

    protected void installCreateNewTableEntryAction(Class<? extends NavigationNode> nodeType) {
        NavigationScope navigationScope = NavigationScopes.getNavigationScope(nodeType);
        Class<DataDto> dtoType = navigationScope.getMainType();
        Class<DataDto> childDtoType = navigationScope.getChildType();
        String showDataPropertyName = navigationScope.getShowDataPropertyName();
        CreateNewContentTableUIEntry<DataDto, U> action = new CreateNewContentTableUIEntry<>(dtoType, childDtoType, n -> n.findChildByType(nodeType));
        JMenuItem editor = CreateNewContentTableUIEntry.createEditor(ui, dtoType, action);
        if (!Objects.equals(getModel().getSource().getScope().getSubModuleType(), navigationScope.getSubModuleType())) {
            BusinessModule module = navigationScope.computeModule(ObserveBusinessProject.get());
            BusinessSubModule subModule = Objects.requireNonNull(navigationScope.computeSubModule(ObserveBusinessProject.get(), module));
            editor.setText(subModule.getLabel() + " - " + editor.getText());
            editor.setToolTipText(subModule.getLabel() + " - " + editor.getToolTipText());
        }
        ui.getModel().getStates().addPropertyChangeListener(InsertMenuAction.PROPERTY_NAME_UPDATING_MODE_AND_NOT_MODIFIED, evt -> editor.setEnabled(getModel().isCreateNewDataActionEnabled(dtoType, (Boolean) evt.getNewValue(), showDataPropertyName)));
    }

    protected void collectActions(Container container, List<AbstractButton> result) {
        for (Component component : container.getComponents()) {
            if (component instanceof AbstractButton) {
                result.add((AbstractButton) component);
                continue;
            }
            if (component instanceof Container) {
                collectActions((Container) component, result);
            }
        }
    }

    protected DeletePanel computeOpenDeleteDependenciesExtraMessage(Class<? extends OpenableDto> dtoType, DeletePanel deletePanel, Collection<ToolkitIdLabel> toDelete) {
        Set<String> ids = toDelete.stream().map(ToolkitIdLabel::getId).collect(Collectors.toSet());
        UsageCount aggregationCount = getOpenableService().getOptionalDependenciesCount(dtoType, ids);
        UsageCount compositionCount = getOpenableService().getMandatoryDependenciesCount(dtoType, ids);
        if (aggregationCount == null && compositionCount == null) {
            return deletePanel;
        }
        if (aggregationCount == null) {
            aggregationCount = new UsageCount();
        }
        if (compositionCount == null) {
            compositionCount = new UsageCount();
        }
        ObserveBusinessProject businessProject = ObserveBusinessProject.get();
        Locale defaultLocale = I18n.getDefaultLocale();
        deletePanel.addDependenciesMessage(new UsageCountWithLabel(defaultLocale, businessProject, compositionCount), new UsageCountWithLabel(defaultLocale, businessProject, aggregationCount));
        return deletePanel;
    }

    private void collectNavigationPopupActions(List<AbstractButton> result, JAXXObject jaxxObject, String... containers) {
        for (Object object : jaxxObject.get$objectMap().values()) {
            if (object instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) object;
                boolean force = Objects.equals(true, button.getClientProperty("forceNavigation"));
                if (!force) {
                    if (!button.isShowing() && !(button instanceof JMenuItem)) {
                        log.debug(String.format("%sReject (hidden) action: %s - %s", prefix, button.getName(), button.getText()));
                        continue;
                    }
                    if (Objects.equals(true, button.getClientProperty("skipNavigation"))) {
                        log.debug(String.format("%sReject (skip) action: %s - %s", prefix, button.getName(), button.getText()));
                        continue;
                    }

                    boolean found = false;
                    for (String container : containers) {
                        found = SwingUtilities.getAncestorNamed(container, button) != null;
                        if (found) {
                            break;
                        }
                    }
                    if (!found) {
                        log.debug(String.format("%sReject (out of zone) action: %s - %s", prefix, button.getName(), button.getText()));
                        continue;
                    }
                }
                log.debug(String.format("%sKeep action: %s - %s", prefix, button.getName(), button.getText()));
                result.add(button);
            }
        }
    }

    public void fixToggleMenuVisibility() {
        updateMenuAccessibility(ui.getToggleInsert(), ui.getInsertPopup());
        updateMenuAccessibility(ui.getToggleConfigure(), ui.getConfigurePopup());
    }


    protected void installCreateNewOpenableAction(Class<? extends NavigationNode> acceptedNodeType, Callable<? extends NavigationNode> referenceMaker) {
        Function<NavigationNode, NavigationNode> getNode = n -> n.findChildByType(acceptedNodeType);
        JMenuItem editor = CreateNewOpenableUI.installAction(ui, acceptedNodeType, getNode, referenceMaker);
        NavigationScope navigationScope = NavigationScopes.getNavigationScope(acceptedNodeType);
        if (!Objects.equals(getModel().getSource().getScope().getSubModuleType(), navigationScope.getSubModuleType())) {
            BusinessModule module = navigationScope.computeModule(ObserveBusinessProject.get());
            BusinessSubModule subModule = Objects.requireNonNull(navigationScope.computeSubModule(ObserveBusinessProject.get(), module));
            editor.setText(subModule.getLabel() + " - " + editor.getText());
            editor.setToolTipText(subModule.getLabel() + " - " + editor.getToolTipText());
        }
        String showDataPropertyName = navigationScope.getShowDataPropertyName();
        Class<DataDto> mainType = navigationScope.getMainType();
        ui.getModel().getStates().addPropertyChangeListener(InsertMenuAction.PROPERTY_NAME_UPDATING_MODE_AND_NOT_MODIFIED, evt -> {
            boolean enabled = ui.getModel().isCreateNewDataActionEnabled(mainType, (Boolean) evt.getNewValue(), showDataPropertyName);
            editor.setEnabled(enabled);
        });
    }

    public void onEndOpenForCreateMode() {
        if (getModel().getStates().isCreatingMode() && withMainTab) {
            JTabbedPane mainTab = (JTabbedPane) ui.getObjectById(DefaultUIInitializer.MAIN_TABBED_PANE);
            mainTab.setSelectedIndex(0);
            if (withSubTab) {
                JTabbedPane subTab = (JTabbedPane) ui.getObjectById(DefaultUIInitializer.SUB_TABBED_PANE);
                subTab.setSelectedIndex(0);
            }
        }
    }
}
