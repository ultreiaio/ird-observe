package fr.ird.observe.client.datasource.editor.api.content;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.GoToTab;
import fr.ird.observe.client.datasource.editor.api.content.ui.DataReferenceListCellRenderer;
import fr.ird.observe.client.datasource.editor.api.content.ui.ReferentialReferenceListCellRenderer;
import fr.ird.observe.client.datasource.validation.ObserveSwingValidator;
import fr.ird.observe.client.util.SpringUtilities;
import fr.ird.observe.client.util.init.DefaultUIInitializer;
import fr.ird.observe.client.util.init.DefaultUIInitializerContext;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox;
import io.ultreia.java4all.jaxx.widgets.choice.BooleanEditor;
import io.ultreia.java4all.jaxx.widgets.combobox.BeanEnumEditor;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditor;
import io.ultreia.java4all.jaxx.widgets.list.DoubleList;
import io.ultreia.java4all.jaxx.widgets.list.ListHeader;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledPanel;
import org.nuiton.jaxx.runtime.init.UIInitializerSupport;
import org.nuiton.jaxx.runtime.swing.BlockingLayerUI;
import org.nuiton.jaxx.runtime.swing.Table;
import org.nuiton.jaxx.validator.JAXXValidator;
import org.nuiton.jaxx.widgets.datetime.DateEditor;
import org.nuiton.jaxx.widgets.datetime.DateTimeEditor;
import org.nuiton.jaxx.widgets.datetime.TimeEditor;
import org.nuiton.jaxx.widgets.datetime.UnlimitedTimeEditor;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.number.NumberEditor;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditor;
import org.nuiton.jaxx.widgets.text.BigTextEditor;
import org.nuiton.jaxx.widgets.text.NormalTextEditor;
import org.nuiton.jaxx.widgets.text.UrlEditor;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListCellRenderer;
import javax.swing.SpringLayout;
import java.awt.Container;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * To initialize ui.
 * <p>
 * Created on 9/26/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class ContentUIInitializer<UI extends ContentUI> extends UIInitializerSupport<UI, DefaultUIInitializerContext<UI>> implements WithClientUIContextApi {

    public static final String CLIENT_PROPERTY_FOCUS_COMPONENT = "focusComponent";
    public static final String SPRING_LAYOUT = "SpringLayout";
    public static final Class<?>[] MANAGED_TYPES = new Class<?>[]{
            JComponent.class,
            JToolBar.class,
            BlockingLayerUI.class,
            BeanCheckBox.class,
            NumberEditor.class,
            FilterableComboBox.class,
            ListHeader.class,
            DoubleList.class,
            DateEditor.class,
            TimeEditor.class,
            UnlimitedTimeEditor.class,
            DateTimeEditor.class,
            CoordinatesEditor.class,
            BooleanEditor.class,
            BeanEnumEditor.class,
            ObserveSwingValidator.class,
            JLabel.class,
            TemperatureEditor.class,
            NauticalLengthEditor.class,
            BigTextEditor.class,
            UrlEditor.class,
            NormalTextEditor.class,
            ContentUI.class,
            Table.class,
            JTable.class,
            JXTable.class,
            JXTitledPanel.class,
            JScrollPane.class,
            JTabbedPane.class
    };
    protected final UI ui;
    protected final DecoratorService decoratorService;
    private final boolean isParentUI;

    public ContentUIInitializer(UI ui) {
        super(ui, MANAGED_TYPES);
        this.ui = ui;
        this.decoratorService = getDecoratorService();
        this.isParentUI = !(ui instanceof NotStandaloneContentUI);
    }

    @Override
    protected DefaultUIInitializerResult build(DefaultUIInitializerContext<UI> initializerContext) {
        return new DefaultUIInitializerResult(initializerContext);
    }

    @Override
    protected DefaultUIInitializerContext<UI> createIInitializerContext(UI ui, Class<?>... types) {
        return new DefaultUIInitializerContext<>(ui, types);
    }

    @Override
    public DefaultUIInitializerResult initUI() {
        return (DefaultUIInitializerResult) super.initUI();
    }

    public void onBeforeInit(ContentUIHandler<UI> handler) {
        if (isParentUI) {
            getMainUI().setParentUI(ui);
        }
        handler.onBeforeInit(ui, this);
    }

    public void afterInit(DefaultUIInitializerResult initializerResult) {
        if (isParentUI) {
            getMainUI().removeParentUI(ui.getClass());
        }
        for (Object dependency : initializerResult.getDependencies()) {
            Class<?> aClass = dependency.getClass();
            ui.removeContextValue(aClass);
        }
    }

    @SuppressWarnings("unchecked")
    public <D extends DataDto> EditableContentUI<D> getParentEditUI() {
        return (EditableContentUI<D>) getMainUI().getParentUI(ContentUI.class);
    }

    @Override
    protected void initUI(DefaultUIInitializerContext<UI> initializerContext) {
        initializerContext
                .startFirstPass()
                .onSubComponents(JComponent.class, this::init)
                .onComponents(FilterableComboBox.class, true, this::init)
                .onComponents(ListHeader.class, true, this::init)
                .onComponents(DoubleList.class, true, this::init)
                .onComponents(TimeEditor.class, true, this::init)
                .onComponents(UnlimitedTimeEditor.class, true, this::init)
                .onComponents(DateTimeEditor.class, true, this::init)
                .onComponents(CoordinatesEditor.class, true, this::init)
                .onComponents(TemperatureEditor.class, true, this::init)
                .onComponents(NauticalLengthEditor.class, true, this::init)
                .onComponents(JToolBar.class, this::init)
                .onComponents(BeanCheckBox.class, this::init)
                .onComponents(NumberEditor.class, this::init)
                .onComponents(DateEditor.class, this::init)
                .onComponents(BooleanEditor.class, this::init)
                .onComponents(BeanEnumEditor.class, this::init)
                .onComponents(ObserveSwingValidator.class, this::init)
                .onComponents(JLabel.class, this::init)
                .onComponents(JSplitPane.class, this::init)
                .onComponents(BigTextEditor.class, this::init)
                .onComponents(UrlEditor.class, this::init)
                .onComponents(NormalTextEditor.class, this::init)
                .onSubComponents(JAXXValidator.class, this::init)
                .onComponents(Table.class, this::init)
                .onComponents(JTable.class, true, this::init)
                .onComponents(JXTitledPanel.class, false, this::init)
                .onSubComponents(JXTable.class, true, this::init)
                .onSubComponents(JScrollPane.class, this::init)
                .startSecondPass()
                .onSubComponents(JTabbedPane.class, this::init)
                .onSubComponents(BlockingLayerUI.class, this::init);
    }

    protected void init(JTabbedPane editor) {
        initializerContext.checkSecondPass();
        UIInitHelper.init(editor);
        //FIXME:Focus at init time always go back to first tab, default behavior is to be on last one???
        editor.setSelectedIndex(0);
        if (DefaultUIInitializer.MAIN_TABBED_PANE.equals(editor.getName())) {
            GoToTab.installMain(ui);
            initializerContext.setTabbedPaneValidator(editor);
            return;
        }
        if (DefaultUIInitializer.SUB_TABBED_PANE.equals(editor.getName())) {
            GoToTab.installSub(ui);
            initializerContext.setSubTabbedPaneValidator(editor);
        }
    }

    protected void init(BlockingLayerUI editor) {
        initializerContext.checkSecondPass();
        editor.setAcceptedComponentTypes(getFocusModel().getAcceptedClassesInBlockingLayer().toArray(new Class<?>[0]));
        String[] doNotBlockComponentIds = initializerContext.getDoNotBlockComponentIds().toArray(new String[0]);
        editor.setAcceptedComponentNames(doNotBlockComponentIds);
    }

    protected void init(TemperatureEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(ui, editor, getClientConfig().isAutoPopupNumberEditor(), getClientConfig().isShowNumberEditorButton(), getClientConfig().getTemperatureFormat());
    }

    protected void init(NauticalLengthEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(ui, editor, getClientConfig().isAutoPopupNumberEditor(), getClientConfig().isShowNumberEditorButton(), getClientConfig().getNauticalLengthFormat());
    }

    protected void init(JToolBar editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(BigTextEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(NormalTextEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(UrlEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    private void init(JLabel editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(ui, editor);
    }

    protected void init(JAXXValidator editor) {
        initializerContext.checkFirstPass();
        String tab = (String) ((JComponent) editor).getClientProperty("tab");
        if (tab != null) {
            initializerContext.addExtraTabUi(tab, editor);
        }
    }

    protected void init(ObserveSwingValidator<?> validator) {
        initializerContext.checkFirstPass();
        ((JAXXValidator) ui).listenValidatorContextNameAndRefreshFields(validator);
    }

    protected void init(NumberEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor, getClientConfig().isAutoPopupNumberEditor(), getClientConfig().isShowNumberEditorButton());
    }

    public JComponent getActionContainer() {
        return ui;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected void init(FilterableComboBox editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
        Class referenceType = editor.getBeanType();
        if (ReferentialDtoReference.class.isAssignableFrom(referenceType)) {
            prepareReferentialFilterableComboBox(editor.getBeanType(), editor);
        } else {
            prepareDataFilterableComboBox(referenceType, editor);
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected void init(ListHeader<?> editor) {
        initializerContext.checkFirstPass();
        addDecoratorClassifier(editor);
        UIInitHelper.init(editor);
        Class referenceType = editor.getBeanType();
        if (ReferentialDtoReference.class.isAssignableFrom(referenceType)) {
            prepareReferentialEntityList(referenceType, (ListHeader) editor);
        } else {
            prepareDataEntityList(referenceType, (ListHeader) editor);
        }
    }

    public void addDecoratorClassifier(ListHeader<?> editor) {
        String decoratorClassifier = ui.getModel().getScope().getDecoratorClassifier();
        if (decoratorClassifier != null) {
            editor.putClientProperty(UIInitHelper.DECORATOR_CLASSIFIER, decoratorClassifier);
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected void init(DoubleList editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
        Class referenceType = editor.getBeanType();
        if (ReferentialDtoReference.class.isAssignableFrom(referenceType)) {
            prepareReferentialDoubleList(referenceType, editor);
        } else {
            prepareDataDoubleList(referenceType, editor);
        }
    }

    protected void init(TimeEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(UnlimitedTimeEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(DateTimeEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(CoordinatesEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor, getClientConfig().getCoordinateFormat());
    }

    protected void init(BooleanEditor editor) {
        initializerContext.checkFirstPass();
        editor.init();
    }

    protected void init(BeanCheckBox editor) {
        initializerContext.checkFirstPass();
        editor.init();
    }

    protected void init(BeanEnumEditor<?> editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(DateEditor editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor, getClientConfig().getLocale());
    }

    protected void init(JXTitledPanel editor) {
    }

    protected void init(JComponent editor) {
        initializerContext.checkFirstPass();
        Object springLayout = editor.getClientProperty(SPRING_LAYOUT);
        if (springLayout != null) {
            setSpringLayout(editor, 2);
        }
        Object property = editor.getClientProperty(CLIENT_PROPERTY_FOCUS_COMPONENT);
        if (property == null) {
            return;
        }
        if (property instanceof Collection) {
            @SuppressWarnings({"unchecked", "rawtypes"}) Collection<JComponent> collection = (Collection) property;
            initializerContext.addFocusComponents(editor.getName(), collection);
        } else {
            initializerContext.addFocusComponent(editor.getName(), (JComponent) property);
        }
    }

    protected void init(Table editor) {
        initializerContext.checkFirstPass();
    }

    private void init(JTable editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    private void init(JXTable editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    private void init(JScrollPane editor) {
        initializerContext.checkFirstPass();
        if (editor.getColumnHeader() != null) {
            editor.getColumnHeader().setFocusable(false);
        }
    }

    public static void setSpringLayout(Container container, int columnCount) {
        int rowCount = container.getComponentCount() / columnCount;
        container.setLayout(new SpringLayout());
        SpringUtilities.makeCompactGrid(container,
                                        rowCount, columnCount, //rows, cols
                                        3, 3,        //initX, initY
                                        3, 3);       //xPad, yPad
    }

    //FIXME Add more methods to use ToolkitIdLabel
    @SuppressWarnings({"unchecked", "rawtypes"})
    private <R extends DataDtoReference> void prepareDataDoubleList(Class<R> dtoClass, DoubleList<R> editor) {
        Decorator decorator = getDecorator(dtoClass, editor);
        String entityLabel = t(I18nDecoratorHelper.getType(dtoClass));
        String title = t("observe.data.Data.type", entityLabel);
//        editor.getPopupSortLabel().setText(title);
        editor.setLabel(title);
        editor.putClientProperty("decorator", decorator);
        editor.init(decorator, new ArrayList<>(), new ArrayList<>());
        JList<R> selectedList = editor.getSelectedList();
        @SuppressWarnings({"RawTypeCanBeGeneric", "rawtypes"}) ListCellRenderer renderer = selectedList.getCellRenderer();
        selectedList.setCellRenderer(new DataReferenceListCellRenderer<>(renderer));
        editor.getUniverseList().setCellRenderer(new DataReferenceListCellRenderer<>(renderer));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private <R extends ReferentialDtoReference> void prepareReferentialDoubleList(Class<R> dtoClass, DoubleList<R> editor) {
        Decorator decorator = getDecorator(dtoClass, editor);
        String entityLabel = t(I18nDecoratorHelper.getType(dtoClass));
        String title = t("observe.referential.Referential.type", entityLabel);
//        editor.getPopupSortLabel().setText(title);
        editor.setLabel(title);
        editor.putClientProperty("decorator", decorator);
        editor.init(decorator, new ArrayList<>(), new ArrayList<>());
        JList<R> selectedList = editor.getSelectedList();
        @SuppressWarnings({"RawTypeCanBeGeneric", "rawtypes"}) ListCellRenderer renderer = selectedList.getCellRenderer();
        selectedList.setCellRenderer(new ReferentialReferenceListCellRenderer<>(renderer));
        editor.getUniverseList().setCellRenderer(new ReferentialReferenceListCellRenderer<>(renderer));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private <R extends DataDtoReference> void prepareDataEntityList(Class<R> dtoClass, ListHeader<R> editor) {
        Decorator decorator = getDecorator(dtoClass, editor);
        String entityLabel = t(I18nDecoratorHelper.getType(dtoClass));
        editor.setPopupTitleText(t("observe.data.Data.type", entityLabel));
        editor.putClientProperty("decorator", decorator);
        editor.init(decorator, Collections.emptyList());
        JList<R> list1 = editor.getList();
        @SuppressWarnings({"RawTypeCanBeGeneric", "rawtypes"}) ListCellRenderer renderer = list1.getCellRenderer();
        list1.setCellRenderer(new DataReferenceListCellRenderer<>(renderer));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private <R extends ReferentialDtoReference> void prepareReferentialEntityList(Class<R> dtoClass, ListHeader<R> editor) {
        Decorator decorator = getDecorator(dtoClass, editor);
        String entityLabel = t(I18nDecoratorHelper.getType(dtoClass));
        editor.setPopupTitleText(t("observe.referential.Referential.type", entityLabel));
        editor.putClientProperty("decorator", decorator);
        editor.init(decorator, Collections.emptyList());
        JList<R> list1 = editor.getList();
        @SuppressWarnings({"RawTypeCanBeGeneric", "rawtypes"}) ListCellRenderer renderer = list1.getCellRenderer();
        list1.setCellRenderer(new ReferentialReferenceListCellRenderer<>(renderer));
    }

    private <R extends DataDtoReference> void prepareDataFilterableComboBox(Class<R> dtoClass, FilterableComboBox<R> editor) {
        Decorator decorator = getDecorator(dtoClass, editor);
        String entityLabel = t(I18nDecoratorHelper.getType(dtoClass));
        editor.setPopupTitleText(t("observe.data.Data.type", entityLabel));
        editor.init(decorator, Collections.emptyList());
        JComboBox<R> combobox = editor.getCombobox();
        ListCellRenderer<R> toolTipRenderer = new ComboBoxListCellRenderer<>(combobox.getRenderer());
        combobox.setRenderer(toolTipRenderer);
    }

    private <R extends ReferentialDtoReference> void prepareReferentialFilterableComboBox(Class<R> dtoClass, FilterableComboBox<R> editor) {
        Decorator decorator = getDecorator(dtoClass, editor);
        String entityLabel = t(I18nDecoratorHelper.getType(dtoClass));
        editor.setPopupTitleText(t("observe.referential.Referential.type", entityLabel));
        editor.init(decorator, Collections.emptyList());
        JComboBox<R> combobox = editor.getCombobox();
        ListCellRenderer<R> toolTipRenderer = new ComboBoxListCellRenderer<>(combobox.getRenderer());
        combobox.setRenderer(toolTipRenderer);
    }

    private <R extends Decorated> Decorator getDecorator(Class<R> dtoClass, JComponent editor) {
        String decoratorClassifier = (String) editor.getClientProperty(UIInitHelper.DECORATOR_CLASSIFIER);
        return decoratorService.getDecoratorByType(dtoClass, decoratorClassifier);
    }
}
