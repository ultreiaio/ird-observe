/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content;

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditorBodyContent;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.main.body.MainUIBodyContentManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;

import java.lang.reflect.Constructor;

/**
 * Manager des ecrans d'editions
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */

public class ContentUIManager {

    private static final Logger log = LogManager.getLogger(ContentUIManager.class);

    private final DataSourceEditorBodyContent dataSourceEditorBody;

    public static JAXXInitialContext newContext(ContentUI parent) {
        return newContext(parent.getHandler().getDataSourceEditor(), parent.getModel().getSource()).add("parent", parent).add(parent);
    }

    public static JAXXInitialContext newContext(DataSourceEditor dataSourceEditor, NavigationNode node) {
        return new JAXXInitialContext()
                .add(node)
                .add(dataSourceEditor.getModel().getMessageTableModel())
                .add(dataSourceEditor);
    }

    public ContentUIManager(MainUIBodyContentManager mainUIBodyContentManager) {
        this.dataSourceEditorBody = mainUIBodyContentManager.getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class);
    }

    public void closeSafeSelectedContentUI() {
        ContentUI selectedContentUI = getDataSourceEditor().getModel().getContent();
        if (selectedContentUI != null) {
            selectedContentUI.getHandler().getContentOpen().closeSafeUI();
        }
    }

    public <U extends ContentUI> U createContent(NavigationNode node, Class<U> uiClass) {
        try {
            Constructor<U> constructor = uiClass.getConstructor(JAXXContext.class);
            log.info(String.format("[%s] Will create new ui", uiClass.getSimpleName()));
            JAXXInitialContext jaxxInitialContext = newContext(getDataSourceEditor(), node);
            return constructor.newInstance(jaxxInitialContext);
        } catch (Exception e) {
            throw new IllegalStateException("Could not create content ui " + uiClass, e);
        }
    }

    /**
     * Essaye de fermer l'écran d'édition s'il existe.
     *
     * @return {@code true} si le contenu a bien été fermé, {@code false} si on ne peut pas fermer l'écran
     * @since 1.5
     */
    public boolean closeSelectedContentUI() {
        ContentUI ui = getDataSourceEditor().getModel().getContent();
        if (ui == null) {
            // no content ui
            return true;
        }
        log.info(String.format("%sWill close ui", ui.getModel().getPrefix()));
        boolean closed;
        try {
            closed = ui.close();
        } catch (Exception e) {
            log.error(String.format("%sCould not close ui", ui.getModel().getPrefix()), e);
            // still we accept to qui the content
            closed = true;
        }
        return closed;
    }

    private DataSourceEditor getDataSourceEditor() {
        return dataSourceEditorBody.get();
    }

}
