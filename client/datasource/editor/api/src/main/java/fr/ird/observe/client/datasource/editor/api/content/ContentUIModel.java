/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content;

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.GroupByCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;

import java.io.Closeable;
import java.util.Objects;

/**
 * Le modèle d'un écran d'édition
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public abstract class ContentUIModel implements Closeable, WithClientUIContextApi {
    private final ContentUIModelStates states;
    private final NavigationNode source;
    protected ContentUI ui;

    protected ContentUIModel(NavigationNode source) {
        this.source = Objects.requireNonNull(source);
        this.states = createStates();
    }

    protected abstract ContentUIModelStates createStates();

    public final void init(ContentUI ui, DefaultUIInitializerResult initializerResult) {
        this.ui = Objects.requireNonNull(ui);
        states.init(ui, initializerResult);
    }

    public void open() {
        states.open(this);
    }

    @Override
    public final void close() {
        states.close();
    }

    public final NavigationScope getScope() {
        return source.getScope();
    }

    public NavigationNode getSource() {
        return source;
    }

    public ContentUIModelStates getStates() {
        return states;
    }

    public String getPrefix() {
        return getSource().getInitializer().getLogPrefix();
    }

    public boolean isCreateNewDataActionEnabled(Class<? extends DataDto> dtoType, Boolean updatingAndNotModified, String showDataPropertyName) {
        return updatingAndNotModified != null && updatingAndNotModified;
    }

    public DataGroupByParameter getGroupByValue() {
        NavigationNode navigationNode = getSource().upToGroupByValue();
        GroupByCapability<?> capability = (GroupByCapability<?>) navigationNode.getCapability();
        DataGroupByDto<?> groupByDto = capability.getReference();
        RootNavigationNode root = (RootNavigationNode) getSource().getRoot();
        ToolkitTreeFlatModelRootRequest request = root.getInitializer().getRequest();
        String groupByFlavor = request.getGroupByFlavor();
        return groupByDto.toParameter(groupByFlavor);
    }
}
