package fr.ird.observe.client.datasource.editor.api.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import io.ultreia.java4all.bean.AbstractJavaBean;

import javax.swing.Icon;
import java.io.Closeable;
import java.util.Objects;

/**
 * All states that ui can use from model.
 * <p>
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentUIModelStates extends AbstractJavaBean implements Closeable, WithClientUIContextApi {
    public static final String PROPERTY_MODE = "mode";
    public static final String PROPERTY_MODIFIED = "modified";
    public static final String PROPERTY_EDITING = "editing";
    public static final String PROPERTY_EDITABLE = "editable";
    public static final String PROPERTY_VALID = "valid";
    public static final String PROPERTY_CONTENT_TILE = "contentTitle";
    public static final String PROPERTY_ICON = "icon";
    public static final String PROPERTY_READING_MODE = "readingMode";
    public static final String PROPERTY_CREATING_MODE = "creatingMode";
    public static final String PROPERTY_UPDATING_MODE = "updatingMode";
    public static final String PROPERTY_OPENED = "opened";
    public static final String PROPERTY_SHOW_DATA = "showData";
    private static final String PROPERTY_ENABLED = "enabled";
    private static final String PROPERTY_CLOSING = "closing";
    /**
     * Content mode.
     */
    private ContentMode mode;
    /**
     * Content icon.
     */
    private Icon icon;
    /**
     * Content title.
     */
    private String contentTitle;
    /**
     * Is content enabled?
     */
    private boolean enabled;
    /**
     * Is content valid?
     */
    private boolean valid;
    /**
     * Is content modified?
     */
    private boolean modified;
    /**
     * Is content editable?
     */
    private boolean editable;
    /**
     * Is content editing?
     */
    private boolean editing;
    /**
     * Is content opened?
     */
    private boolean opened;
    /**
     * Is content closing?
     */
    private boolean closing;
    /**
     * Is a reset edit in progress?
     */
    private boolean resetEdit;
    /**
     * Can we show some data?
     */
    private boolean showData;

    public void open(ContentUIModel model) {
        NavigationNode source = model.getSource();
        opened = false;
        // Compute editable property
        boolean editable = source.getInitializer().isEditable();
        setEditable(editable);

        // Compute mode property
        ContentMode contentMode = source.getHandler().getContentMode();
        setMode(null);
        setMode(contentMode);

        // Compute content icon
        Icon icon = source.getHandler().getContentIcon();
        setIcon(icon);

        // Compute content title
        String contentTitle = source.getHandler().getContentTitle();
        setContentTitle(contentTitle);
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        boolean oldValue = isOpened();
        this.opened = opened;
        firePropertyChange(PROPERTY_OPENED, oldValue, opened);
    }

    public boolean isClosing() {
        return closing;
    }

    public void setClosing(boolean closing) {
        boolean oldValue = this.closing;
        this.closing = closing;
        firePropertyChange(PROPERTY_CLOSING, oldValue, closing);
    }

    public ContentMode getMode() {
        return mode;
    }

    public void setMode(ContentMode mode) {
        ContentMode oldValue = this.mode;
        this.mode = mode;
        firePropertyChange(PROPERTY_MODE, oldValue, mode);
        firePropertyChange(PROPERTY_READING_MODE, isReadingMode());
        firePropertyChange(PROPERTY_CREATING_MODE, isCreatingMode());
        firePropertyChange(PROPERTY_UPDATING_MODE, isUpdatingMode());
    }

    /**
     * @return {@code true} si l'écran est en mode creation
     * @see ContentMode#CREATE
     */
    public boolean isCreatingMode() {
        return ContentMode.CREATE.equals(getMode());
    }

    /**
     * @return {@code true} si l'écran est en mode mise a jour
     * @see ContentMode#UPDATE
     */
    public boolean isUpdatingMode() {
        return ContentMode.UPDATE.equals(getMode());
    }

    /**
     * @return {@code true} si l'écran est en mode lecture
     * @see ContentMode#READ
     */
    public boolean isReadingMode() {
        return ContentMode.READ.equals(getMode());
    }

    public final String getValidationContext() {
        return Objects.requireNonNull(mode).getValidationContext();
    }

    /**
     * Test if we can qui the form.
     *
     * @return {@code null} if was canceled, {@code true} if form can be quit, {@code false} otherwise.
     */
    public Boolean canQuit() {
        if (!isEnabled()) {
            // model disabled, no check
            return true;
        }
        if (!isEditing()) {
            // model not editing, no check
            return true;
        }
        if (isReadingMode()) {
            // model in reading mode, no check
            return true;
        }
        // can quit only if model not modified
        return !isGlobalModified();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        Object oldValue = this.enabled;
        this.enabled = enabled;
        firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
    }

    public boolean isShowData() {
        return showData;
    }

    public void setShowData(boolean showData) {
        this.showData = showData;
        firePropertyChange(PROPERTY_SHOW_DATA, showData);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        Object oldValue = this.editable;
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, oldValue, editable);
    }

    public boolean isEditing() {
        return editing;
    }

    public void setEditing(boolean editing) {
        Object oldValue = this.editing;
        this.editing = editing;
        firePropertyChange(PROPERTY_EDITING, oldValue, editing);
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        Object oldValue = this.valid;
        this.valid = valid;
        firePropertyChange(PROPERTY_VALID, oldValue, valid);
    }

    public boolean isResetEdit() {
        return resetEdit;
    }

    public void setResetEdit(boolean resetEdit) {
        this.resetEdit = resetEdit;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
        firePropertyChange(PROPERTY_MODIFIED, modified);
    }

    public boolean isGlobalModified() {
        return isModified();
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        Icon oldValue = getIcon();
        this.icon = icon;
        firePropertyChange(PROPERTY_ICON, oldValue, icon);
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        String oldValue = getContentTitle();
        this.contentTitle = contentTitle;
        firePropertyChange(PROPERTY_CONTENT_TILE, oldValue, contentTitle);
    }

    @Override
    public void close() {
        setOpened(false);
    }

    public void firePropertyChange(String propertyName, boolean value) {
        firePropertyChange(propertyName, null, value);
    }

    public void firePropertyChanged(String propertyName, Object value) {
        firePropertyChange(propertyName, null, value);
    }

    public void init(ContentUI ui, DefaultUIInitializerResult initializerResult) {

    }
}

