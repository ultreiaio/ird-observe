package fr.ird.observe.client.datasource.editor.api.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.i18n.spi.type.SimpleTypeTranslatorSupport;
import io.ultreia.java4all.i18n.spi.type.TypeTranslator;

/**
 * Created on 11/07/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
@AutoService(TypeTranslator.class)
public class ContentUIModelStatesTypeTranslator  extends SimpleTypeTranslatorSupport {

    public ContentUIModelStatesTypeTranslator() {
        super("fr.ird.observe.client.datasource.editor", ContentUIModelStates.class, "ModelStates");
    }

}
