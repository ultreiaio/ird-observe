package fr.ird.observe.client.datasource.editor.api.content;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.validation.ObserveSwingValidator;
import fr.ird.observe.dto.IdDto;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.validator.JAXXValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableModel;

public interface EditableContentUI<D extends IdDto> extends JAXXObject , JAXXValidator {

    ObserveSwingValidator<D> getValidator();

    SwingValidatorMessageTableModel getErrorTableModel();

    void startEdit();

    void resetEdit();

    void stopEdit();

    void saveEdit();
}
