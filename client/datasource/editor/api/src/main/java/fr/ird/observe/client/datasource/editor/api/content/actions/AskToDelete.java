package fr.ird.observe.client.datasource.editor.api.content.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.dto.data.DataDto;

import javax.swing.JOptionPane;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 04/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class AskToDelete<D extends DataDto> implements WithClientUIContextApi {

    public static final String TEMPLATE = "<html><body>" +
            "<p>%1$s</p>" +
            "<ul>%2$s</ul>" +
            "</body></html>";
    private final BiFunction<D, D, List<String>> getDeleteMessage;
    private final String deleteExtraMessage;

    public AskToDelete(BiFunction<D, D, List<String>> getDeleteMessage, String deleteExtraMessage) {
        this.getDeleteMessage = Objects.requireNonNull(getDeleteMessage);
        this.deleteExtraMessage = deleteExtraMessage;
    }

    public boolean needDelete(D originalBean, D editBean) {
        List<String> dataToDelete = getDeleteMessage.apply(originalBean, editBean);
        if (dataToDelete != null && !dataToDelete.isEmpty()) {
            String template = String.format(TEMPLATE, deleteExtraMessage, dataToDelete.stream().map(s -> "<li>" + s + "</li>").collect(Collectors.joining("")));
            int response = askToUser(
                    t("observe.ui.title.need.confirm"),
                    template,
                    JOptionPane.ERROR_MESSAGE,
                    new Object[]{
                            t("observe.ui.choice.continue"),
                            t("observe.ui.choice.cancel")},
                    0);
            return response == 0;
        }
        return true;
    }
}
