package fr.ird.observe.client.datasource.editor.api.content.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.AbstractButton;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;

/**
 * Created on 25/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public interface ConfigureMenuAction<U extends ContentUI> extends MenuAction {

    U getUi();

    @Override
    default JPopupMenu getPopupMenu() {
        return getUi().getConfigurePopup();
    }

    @Override
    default KeyStroke getMenuKeyStroke() {
        // always use the given key-stroke to avoid change between two content
        // a same such action is unique on a form, so can be determine once for all
        return (KeyStroke) getValue(ACCELERATOR_KEY);
    }

    @Override
    default void initEditor() {
        MenuAction.super.initEditor();
        AbstractButton editor = getEditor();
        JToggleButton toggleConfigure = getToggleConfigure();
        editor.putClientProperty("toggleButton", toggleConfigure);
        editor.addPropertyChangeListener("enabled", e -> ContentUIHandler.updateMenuAccessibility(toggleConfigure, getPopupMenu()));
    }

    default JToggleButton getToggleConfigure() {
        return getUi().getToggleConfigure();
    }

    default void moveTo(JToggleButton toggleConfigure) {
        setToggleConfigure(toggleConfigure);
    }

    default void setToggleConfigure(JToggleButton toggleConfigure) {
    }

}
