package fr.ird.observe.client.datasource.editor.api.content.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.util.UIFileHelper;
import fr.ird.observe.services.ObserveServicesProvider;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;
import org.nuiton.jaxx.widgets.file.JaxxFileChooser;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import java.io.File;

public abstract class ContentUIActionSupport<U extends ContentUI> extends JComponentActionSupport<U> implements WithClientUIContextApi {

    protected ContentUIActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
        if (this instanceof ConfigureMenuAction) {
            setCheckMenuItemIsArmed(false);
        }
    }

    protected ContentUIActionSupport(String actionCommandKey, String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(actionCommandKey, label, shortDescription, actionIcon, acceleratorKey);
    }

    protected ContentUIActionSupport(String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    protected ContentUIActionSupport(String actionCommandKey, String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(actionCommandKey, label, shortDescription, actionIcon, acceleratorKey);
    }

    @Override
    protected int getInputMapCondition() {
        return JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT;
    }

    protected JComponent getContainer(U u) {
        return u;
    }

    @Override
    protected final InputMap getInputMap(U u, int inputMapCondition) {
        return getContainer(u).getInputMap(inputMapCondition);
    }

    @Override
    protected final ActionMap getActionMap(U u) {
        return getContainer(u).getActionMap();
    }

    @Override
    public void defaultInit(InputMap inputMap, ActionMap actionMap) {
        if (getEditor() != null && getText() == null) {
            String text = (String) editor.getClientProperty("text");
            if (text != null) {
                setText(text);
            }
        }
        if (getEditor() != null && getTooltipText() == null) {
            String toolTipText = (String) editor.getClientProperty("toolTipText");
            if (toolTipText != null) {
                setTooltipText(toolTipText);
            }
        }
        super.defaultInit(inputMap, actionMap);
    }
    @Override
    public void updateEditorTexts() {
        super.updateEditorTexts();
    }

    public void doAction() {
        doActionPerformed(null, ui);
    }

    protected ObserveSwingDataSource getDataSource() {
        return getDataSourcesManager().getMainDataSource();
    }

    protected ObserveServicesProvider getServicesProvider() {
        return getDataSource();
    }

    protected DataSourceEditor getDataSourceEditor() {
        return ui.getHandler().getDataSourceEditor();
    }

    protected JaxxFileChooser.ToSave getToSave() {
        return JaxxFileChooser
                .forSaving()
                .setParent(getUi());
    }

    protected String getLog(String message) {
        return getUi().getModel().getPrefix() + message;
    }

    protected File chooseFile(String title, String buttonLabel, File incoming, String mainExtension, String... filters) {
        return UIFileHelper.chooseFile(getUi(), title, buttonLabel, incoming, mainExtension, filters);
    }

    protected boolean confirmOverwriteFileIfExist(File file) {
        return UIFileHelper.confirmOverwriteFileIfExist(getUi(), file);
    }
}
