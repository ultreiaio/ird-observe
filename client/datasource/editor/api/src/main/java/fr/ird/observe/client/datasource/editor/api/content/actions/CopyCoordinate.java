package fr.ird.observe.client.datasource.editor.api.content.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditorModel;

import javax.swing.JButton;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

/**
 * Created on 26/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.2
 */
public class CopyCoordinate<U extends ContentUI> extends ContentUIActionSupport<U> {


    protected final CoordinatesEditor source;
    protected final CoordinatesEditor target;

    public static <U extends ContentUI> void install(String tip, KeyStroke keyStore, U ui, CoordinatesEditor source, CoordinatesEditor target, JButton editor) {
        CopyCoordinate<U> action = new CopyCoordinate<>(tip, keyStore, source, target);
        CopyCoordinate.init(ui, editor, action);
        target.getToolbarLeft().add(editor, 0);
    }

    public CopyCoordinate(String tip, KeyStroke keyStore, CoordinatesEditor source, CoordinatesEditor target) {
        super(CopyCoordinate.class.getName() + "-" + keyStore,"", tip, "report-copy", keyStore);
        this.source = source;
        this.target = target;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        CoordinatesEditorModel sourceModel = source.getModel();
        CoordinatesEditorModel targetModel = target.getModel();
        targetModel.setLatitudeAndLongitude(sourceModel.getLatitude(), sourceModel.getLongitude());
    }
}
