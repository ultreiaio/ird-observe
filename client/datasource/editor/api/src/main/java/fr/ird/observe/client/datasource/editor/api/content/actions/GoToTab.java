package fr.ird.observe.client.datasource.editor.api.content.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.client.util.init.DefaultUIInitializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class GoToTab extends ContentUIActionSupport<ContentUI> {
    private static final Logger log = LogManager.getLogger(GoToTab.class);
    private final String tabbedPaneName;
    private final int tabIndex;

    public static void installMain(ContentUI ui) {
        install0(ui, DefaultUIInitializer.MAIN_TABBED_PANE, "shift pressed F");
    }

    public static void installSub(ContentUI ui) {
        install0(ui, DefaultUIInitializer.SUB_TABBED_PANE, "shift alt pressed F");
    }

    private static void install0(ContentUI ui, String tabbedPaneName, String keyStrokePrefix) {
        JTabbedPane tabbedPane = (JTabbedPane) ui.getObjectById(tabbedPaneName);
        if (tabbedPane != null) {
//            UIHelper.changeFontSize(tabbedPane, 11f);
            int tabCount = tabbedPane.getTabCount();
            for (int i = 0; i < tabCount; i++) {
                String titleAt = tabbedPane.getTitleAt(i);
                KeyStroke keyStroke = Objects.requireNonNull(KeyStroke.getKeyStroke(keyStrokePrefix + (i + 1)));
                tabbedPane.setTitleAt(i, titleAt);
                tabbedPane.setToolTipTextAt(i, titleAt + ObserveKeyStrokesSupport.keyStrokeToStr(keyStroke));
                GoToTab action = new GoToTab(tabbedPaneName, i, keyStroke);
                init(ui, null, action);
            }
        }
    }

    public GoToTab(String tabbedPaneName, int tabIndex, KeyStroke keyStroke) {
        super(GoToTab.class.getName() + "#" + tabbedPaneName + "#" + tabIndex, null, null, null, keyStroke);
        this.tabIndex = tabIndex;
        this.tabbedPaneName = tabbedPaneName;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ContentUI contentUI) {
        JTabbedPane tabbedPane = (JTabbedPane) contentUI.getObjectById(tabbedPaneName);
        tabbedPane.setSelectedIndex(tabIndex);
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        JTabbedPane tabbedPane = (JTabbedPane) ui.getObjectById(tabbedPaneName);
        boolean canExecute = tabbedPane.isEnabledAt(tabIndex);
        if (!canExecute) {
            log.info(String.format("Reject action : %s tab is not enabled.", getName()));
            return false;
        }
        canExecute = canExecutionActionFromLayer(getEditor(), e);
        if (canExecute) {
            log.info(String.format("Accept action : %s", getName()));
        } else {
            log.info(String.format("Reject action : %s", getName()));
        }
        return canExecute;
    }
}

