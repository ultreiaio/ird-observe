package fr.ird.observe.client.datasource.editor.api.content.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.AbstractButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;

/**
 * Created on 25/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public interface InsertMenuAction<U extends ContentUI> extends MenuAction {
    String PROPERTY_NAME_UPDATING_MODE_AND_NOT_MODIFIED = "updatingModeAndNotModified";

    U getUi();

    @Override
    default JPopupMenu getPopupMenu() {
        return getUi().getInsertPopup();
    }

    default void moveTo(JPopupMenu insertPopup) {
        KeyStroke keyStroke = MenuAction.getMenuKeyStroke(insertPopup);
        setKeyStroke(keyStroke);
        AbstractButton editor = getEditor();
        insertPopup.add(editor);
        if (editor instanceof JMenuItem) {
            ((JMenuItem) editor).setAccelerator(keyStroke);
        }
    }

    @Override
    default void initEditor() {
        MenuAction.super.initEditor();
        AbstractButton editor = getEditor();
        JToggleButton toggleInsert = getUi().getToggleInsert();
        editor.putClientProperty("toggleButton", toggleInsert);
        editor.addPropertyChangeListener("enabled", e -> ContentUIHandler.updateMenuAccessibility(toggleInsert, getPopupMenu()));
    }


}
