package fr.ird.observe.client.datasource.editor.api.content.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.Component;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created on 25/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ToggleInsert<U extends ContentUI> extends ContentUIActionSupport<U> {

    public ToggleInsert() {
        super("", n("observe.ui.content.action.insert.tip"), "add", ObserveKeyStrokesEditorApi.KEY_STROKE_INSERT_CONFIGURE);
//        setIcon(getContentScaledImage("add",22));
    }

    @Override
    public void init() {
        super.init();
        ui.getInsertPopup().addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                ui.getToggleInsert().setSelected(false);
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
                ui.getToggleInsert().setSelected(false);
            }
        });
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        ui.getToggleInsert().setSelected(true);
        SwingUtilities.invokeLater(() -> {
            JPopupMenu p = ui.getInsertPopup();
            int componentCount = p.getComponentCount();
            if (componentCount == 1) {
                Component component = p.getComponent(0);
                if (component.isEnabled() && component instanceof AbstractButton) {
                    ((AbstractButton) component).doClick();
                    ui.getToggleInsert().setSelected(false);
                }
            } else {
                JComponent c = ui.getToggleInsert();
                MenuAction.preparePopup(p, c, false);
            }
        });
    }

}
