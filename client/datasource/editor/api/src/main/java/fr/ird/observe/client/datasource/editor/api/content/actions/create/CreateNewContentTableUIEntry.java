package fr.ird.observe.client.datasource.editor.api.content.actions.create;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.content.actions.InsertMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.util.function.BiPredicate;
import java.util.function.Function;

/**
 * To create a new table data entry from the outside world.
 * <p>
 * Created on 26/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class CreateNewContentTableUIEntry<D extends DataDto, U extends ContentUI> extends ContentUIActionSupport<U> implements InsertMenuAction<U> {

    private static final Logger log = LogManager.getLogger(CreateNewContentTableUIEntry.class);
    private final BiPredicate<U, Class<D>> typePredicate;
    private final Function<NavigationNode, NavigationNode> getNode;
    private final Class<D> dtoType;
    private final Class<? extends BusinessDto> childDtoType;

    public static <D extends DataDto, U extends ContentUI> JMenuItem createEditor(U ui, Class<D> dtoType, CreateNewContentTableUIEntry<D, U> action) {
        JMenuItem editor = new JMenuItem();
        String fieldName = "add" + Strings.capitalize(dtoType.getSimpleName().replace("Dto", ""));
        editor.setName(fieldName);
        log.debug("Create new action: " + fieldName);
        ui.get$objectMap().put(editor.getName(), editor);
        init(ui, editor, action);
        return editor;
    }

    public CreateNewContentTableUIEntry(Class<D> dtoType, Class<? extends BusinessDto> childDtoType, Function<NavigationNode, NavigationNode> getNode) {
        super(null, null, "add", null);
        this.getNode = getNode;
        this.dtoType = dtoType;
        this.typePredicate = CreateNewPredicates.getPredicate(dtoType);
        setIcon(DtoIconHelper.getIcon(dtoType));
        this.childDtoType = childDtoType;
        setText(I18n.t(I18nDecoratorHelper.getPropertyI18nKey(this.childDtoType, "action.create")));
        setTooltipText(ObserveBusinessProject.get().createDataToolTipText(childDtoType));
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        return super.canExecuteAction(e) && (typePredicate == null || typePredicate.test(ui, (Class) childDtoType));
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        NavigationTree tree = getDataSourceEditor().getNavigationUI().getTree();
        NavigationNode source = ui.getModel().getSource();
        NavigationNode selectedNode = getNode.apply(source);
        ContentTableUI<?, ?, ?> newContentUI;
        if (!source.equals(selectedNode)) {
            tree.selectSafeNode(selectedNode);
            newContentUI = (ContentTableUI<?, ?, ?>) getDataSourceEditor().getModel().getContent();
        } else {
            newContentUI = (ContentTableUI<?, ?, ?>) ui;
        }
        doCreate(newContentUI);
    }

    protected <V extends ContentTableUI<?, ?, ?>> void doCreate(V ui) {
        // pour forcer le rechargement du binding
        @SuppressWarnings("unchecked") ChangeMode<V> changeMode = (ChangeMode<V>) ui.getHandler().getChangeMode();
        if (changeMode != null) {
            changeMode.rebuildEditableZone(ui);
        }
        ui.getModel().getStates().setCanSaveRow(true);
        ui.getModel().getStates().setCanSaveRow(false);
        ui.getTableModel().addNewEntry();
    }

    public Class<? extends DataDto> getDtoType() {
        return dtoType;
    }
}
