package fr.ird.observe.client.datasource.editor.api.content.actions.create;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.InsertMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.open.actions.ContentOpenableUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.util.concurrent.Callable;
import java.util.function.BiPredicate;

/**
 * Created on 18/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class CreateNewEditable<D extends OpenableDto, T extends EditableDto, U extends ContentOpenableUI<D, U>> extends ContentOpenableUIActionSupport<D, U> implements InsertMenuAction<U> {
    private static final Logger log = LogManager.getLogger(CreateNewEditable.class);

    private final BiPredicate<U, Class<T>> predicate;
    private final Class<T> editableType;
    private final Callable<? extends NavigationNode> referenceMaker;

    public static <D extends OpenableDto, T extends EditableDto, U extends ContentOpenableUI<D, U>> JMenuItem installAction(U ui, JMenuItem editor, CreateNewEditable<D, T, U> action) {
        log.info("Create new action: " + editor.getName());
        init(ui, editor, action);
        return editor;
    }

    public CreateNewEditable(Class<D> dataType, Class<T> editableType, Callable<? extends NavigationNode> referenceMaker) {
        super(dataType, null, null, "add", null);
        this.predicate = CreateNewPredicates.getPredicate(editableType);
        this.editableType = editableType;
        this.referenceMaker = referenceMaker;
        setText(I18n.t(I18nDecoratorHelper.getPropertyI18nKey(editableType, "action.add")));
        setTooltipText(I18n.t(I18nDecoratorHelper.getPropertyI18nKey(editableType, "action.add.tip")));
        setIcon(DtoIconHelper.getIcon(editableType));
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        ContentOpenableUIModel<D> model = ui.getModel();
        D bean = model.getStates().getBean();
        if (predicate != null && !predicate.test(ui, editableType)) {
            return;
        }
        try {
            NavigationNode newNode = referenceMaker.call();
            getDataSourceEditor().getNavigationUI().getTree().selectSafeNode(newNode);
        } catch (Exception exception) {
            throw new IllegalStateException(exception);
        }
    }
}
