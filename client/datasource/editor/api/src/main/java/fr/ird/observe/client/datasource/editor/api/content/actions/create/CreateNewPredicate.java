package fr.ird.observe.client.datasource.editor.api.content.actions.create;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.DataDto;

import java.util.Objects;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Consumer;

/**
 * A predicate to apply before launch a new {@code CreateNewAction}.
 * <p>
 * Created on 01/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class CreateNewPredicate<U extends ContentUI, D extends DataDto, V extends ContentUI> implements WithClientUIContextApi, BiPredicate<U, Class<D>> {

    private final Set<Class<? extends DataDto>> acceptedTypes;
    private final Class<V> targetType;
    private final Consumer<V> adaptNewContent;
    private String dtoLabel;

    protected CreateNewPredicate(Set<Class<? extends DataDto>> acceptedTypes, Class<V> targetType, Consumer<V> adaptNewContent) {
        this.acceptedTypes = acceptedTypes;
        this.targetType = Objects.requireNonNull(targetType);
        this.adaptNewContent = Objects.requireNonNull(adaptNewContent);
    }

    public Set<Class<? extends DataDto>> getAcceptedTypes() {
        return acceptedTypes;
    }

    public abstract boolean checkCanCreate(U source, Class<D> dtoType);

    public abstract boolean askUserToFix(U source, String dtoLabel, String message);

    public abstract V changeContent(U source, Class<V> targetType);

    public abstract String getMessage();

    @Override
    public final boolean test(U source, Class<D> dtoType) {
        boolean checkCanCreate = checkCanCreate(source, dtoType);
        boolean askUserToFix = false;
        if (!checkCanCreate) {
            setDtoLabel(dtoType);
            String message = getMessage();
            askUserToFix = askUserToFix(source, dtoLabel, message);
            if (askUserToFix) {
                applyFix(source);
            }
        }
        return result(checkCanCreate, askUserToFix);
    }

    public void applyFix(U source) {
        V targetUi = changeContent(source, targetType);
        adaptNewContent(targetUi);
    }

    public void setDtoLabel(Class<?> dtoType) {
        dtoLabel = I18nDecoratorHelper.getType(dtoType);
    }

    protected boolean result(boolean checkCanCreate, boolean askUserToFix) {
        return checkCanCreate;
    }

    public final void adaptNewContent(V target) {
        adaptNewContent.accept(target);
    }

    public final String getDtoLabel() {
        return dtoLabel;
    }
}
