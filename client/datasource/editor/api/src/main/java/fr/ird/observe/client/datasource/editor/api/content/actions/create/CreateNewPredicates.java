package fr.ird.observe.client.datasource.editor.api.content.actions.create;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.dto.data.DataDto;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * Created on 01/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class CreateNewPredicates {
    private static final Logger log = LogManager.getLogger(CreateNewPredicates.class);
    private final static SingletonSupplier<CreateNewPredicates> INSTANCE = SingletonSupplier.of(CreateNewPredicates::new);
    private final Map<Class<? extends DataDto>, CreateNewPredicate<?, ?, ?>> predicates;

    public static <U extends ContentUI, D extends DataDto, V extends ContentUI> CreateNewPredicate<U, D, V> getPredicate(Class<D> dtoType) {
        @SuppressWarnings({"unchecked", "rawtypes"}) CreateNewPredicate<U, D, V> result = (CreateNewPredicate) get().predicates.get(dtoType);
        log.info(String.format("For type: %s, found predicate: %s.", dtoType, result == null ? null : result.getClass().getName()));
        return result;
    }

    protected static CreateNewPredicates get() {
        return INSTANCE.get();
    }

    public CreateNewPredicates() {
        this.predicates = new LinkedHashMap<>();
        for (CreateNewPredicate<?, ?, ?> createNewPredicate : ServiceLoader.load(CreateNewPredicate.class)) {
            for (Class<? extends DataDto> t : createNewPredicate.getAcceptedTypes()) {
                log.info(String.format("Register predicate: %s for type: %s", createNewPredicate.getClass().getName(), t.getName()));
                predicates.put(t, createNewPredicate);
            }
        }
        log.info(String.format("Found %d create predicate(s).", predicates.size()));
    }
}
