package fr.ird.observe.client.datasource.editor.api.content.actions.create;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.content.actions.InsertMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScopes;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.navigation.id.CloseNodeVetoException;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.util.concurrent.Callable;
import java.util.function.BiPredicate;
import java.util.function.Function;

/**
 * To create a new openable data from the outside world.
 * Created on 03/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class CreateNewRootOpenableUI<D extends RootOpenableDto, U extends ContentUI> extends ContentUIActionSupport<U> implements InsertMenuAction<U> {

    private static final Logger log = LogManager.getLogger(CreateNewRootOpenableUI.class);

    private final Function<NavigationNode, NavigationNode> getNode;
    private final BiPredicate<U, Class<D>> typePredicate;
    private final Class<D> dtoType;
    private final Callable<? extends NavigationNode> referenceMaker;

    public static <D extends RootOpenableDto, U extends ContentUI> JMenuItem installAction(U ui, Class<? extends NavigationNode> acceptedNodeType, Function<NavigationNode, NavigationNode> getNode, Callable<? extends NavigationNode> referenceMaker) {
        NavigationScope navigationScope = NavigationScopes.getNavigationScope(acceptedNodeType);
        IdNode<?> editNode = navigationScope.computeEditNode(ui.getModel()::getObserveEditModel).get();
        CreateNewRootOpenableUI<D, U> action = new CreateNewRootOpenableUI<>(navigationScope.getMainType(), getNode, referenceMaker);
        return createEditor(ui, editNode, action);
    }

    public static <D extends RootOpenableDto, U extends ContentUI> JMenuItem createEditor(U ui, IdNode<?> editNode, CreateNewRootOpenableUI<D, U> action) {
        JMenuItem editor = new JMenuItem();
        String fieldName = "add" + Strings.capitalize(editNode.getSimpleName());
        editor.setName(fieldName);
        log.info("Create new action: " + fieldName);
        ui.get$objectMap().put(editor.getName(), editor);
        init(ui, editor, action);
        return editor;
    }

    public static void closeAndCreate(WithClientUIContextApi clientUIContext, DataSourceEditor dataSourceEditor, IdNode<?> editNode, Callable<? extends NavigationNode> reference) {
        try {
            ChangeMode.closeData(clientUIContext, dataSourceEditor, editNode);
        } catch (CloseNodeVetoException e1) {
            log.error("Could not close data from callback", e1);
            return;
        }
        try {
            NavigationNode newNode = reference.call();
            dataSourceEditor.getNavigationUI().getTree().selectSafeNode(newNode);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public CreateNewRootOpenableUI(Class<D> dtoType, Function<NavigationNode, NavigationNode> getNode, Callable<? extends NavigationNode> referenceMaker) {
        super(null, null, "add", null);
        this.getNode = getNode;
        this.dtoType = dtoType;
        this.typePredicate = CreateNewPredicates.getPredicate(dtoType);
        this.referenceMaker = referenceMaker;
        setIcon(DtoIconHelper.getIcon(dtoType));
        setText(I18n.t(I18nDecoratorHelper.getPropertyI18nKey(dtoType, "action.create")));
        setTooltipText(ObserveBusinessProject.get().createDataToolTipText(dtoType));
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        return super.canExecuteAction(e) && (typePredicate == null || typePredicate.test(ui, dtoType));
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        NavigationTree tree = getDataSourceEditor().getNavigationUI().getTree();
        NavigationNode parentNode = getNode.apply(tree.getSelectedNode());
        IdNode<? extends IdDto> editNode = parentNode.getInitializer().getEditNode();
        closeAndCreate(this, getDataSourceEditor(), editNode, referenceMaker);
    }
}
