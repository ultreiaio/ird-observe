package fr.ird.observe.client.datasource.editor.api.content.actions.delete;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUI;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUI;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUI;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUI;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUI;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.SimpleDto;

import javax.swing.AbstractButton;
import java.awt.event.ActionEvent;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 14/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public class DeleteAction<D extends IdDto, U extends ContentUI> extends ContentUIActionSupport<U> implements ConfigureMenuAction<U> {

    private final DeleteExecutor executor;

    public static class Builder<D extends IdDto, U extends ContentUI> implements AddOnStep<D, U>, AddCallStep<D, U>, AddThenStep<D, U>, BuildStep<D, U> {

        private final U ui;
        private final Class<D> dtoType;
        private Supplier<DeleteRequestBuilder.StepBuild> requestBuilderSupplier;
        private Function<DeleteRequest, Consumer<String>> requestConsumer;
        private Function<DeleteRequest, ? extends DeleteTreeAdapter<?>> treeAdapter;

        public Builder(U ui, Class<D> dtoType) {
            this.ui = Objects.requireNonNull(ui);
            this.dtoType = Objects.requireNonNull(dtoType);
        }

        @Override
        public AddCallStep<D, U> on(Supplier<DeleteRequestBuilder.StepBuild> requestBuilderSupplier) {
            this.requestBuilderSupplier = Objects.requireNonNull(requestBuilderSupplier);
            return this;
        }

        @Override
        public AddThenStep<D, U> call(Function<DeleteRequest, Consumer<String>> requestConsumer) {
            this.requestConsumer = Objects.requireNonNull(requestConsumer);
            return this;
        }

        @Override
        public BuildStep<D, U> then(Function<U, Function<DeleteRequest, ? extends DeleteTreeAdapter<?>>> treeAdapter) {
            this.treeAdapter = Objects.requireNonNull(treeAdapter).apply(ui);
            return this;
        }

        @Override
        public DeleteAction<D, U> install(Supplier<? extends AbstractButton> editor) {
            DeleteExecutor executor = new DeleteExecutor(requestBuilderSupplier, requestConsumer, treeAdapter);
            DeleteAction<D, U> action = new DeleteAction<>(dtoType, executor);
            init(ui, editor.get(), action);
            return action;
        }
    }

    public static <D extends IdDto, U extends ContentUI> AddOnStep<D, U> create(U ui, Class<D> dtoType) {
        return new Builder<>(ui, dtoType);
    }

    public static <D extends EditableDto, U extends ContentEditUI<D, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ObserveUtil.getFirstType(ui));
    }

    public static <D extends RootOpenableDto, U extends ContentRootOpenableUI<D, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ObserveUtil.getFirstType(ui));
    }

    public static <D extends OpenableDto, U extends ContentOpenableUI<D, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ObserveUtil.getFirstType(ui));
    }

    public static <D extends OpenableDto, U extends ContentListUI<D, ?, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ObserveUtil.getFirstType(ui));
    }

    public static <D extends RootOpenableDto, U extends ContentRootListUI<D, ?, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ObserveUtil.getFirstType(ui));
    }


    public static <D extends SimpleDto, U extends ContentLayoutUI<D, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ObserveUtil.getFirstType(ui));
    }

    protected DeleteAction(Class<D> dataType, DeleteExecutor executor) {
        super(n("observe.ui.action.delete"), t("observe.Common.action.delete.tip", I18nDecoratorHelper.getType(dataType)), "content-delete-16", ObserveKeyStrokesEditorApi.KEY_STROKE_DELETE_DATA_GLOBAL);
        this.executor = Objects.requireNonNull(executor);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        executor.execute(getActionExecutor(), getDataSourceEditor());
    }

    public interface AddOnStep<D extends IdDto, U extends ContentUI> {
        AddCallStep<D, U> on(Supplier<DeleteRequestBuilder.StepBuild> requestBuilderSupplier);
    }

    public interface AddCallStep<D extends IdDto, U extends ContentUI> {
        AddThenStep<D, U> call(Function<DeleteRequest, Consumer<String>> requestConsumer);
    }

    public interface AddThenStep<D extends IdDto, U extends ContentUI> {
        BuildStep<D, U> then(Function<U, Function<DeleteRequest, ? extends DeleteTreeAdapter<?>>> treeAdapter);
    }

    public interface BuildStep<D extends IdDto, U extends ContentUI> {
        DeleteAction<D, U> install(Supplier<? extends AbstractButton> editor);
    }
}
