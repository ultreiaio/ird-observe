package fr.ird.observe.client.datasource.editor.api.content.actions.delete;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.application.ActionExecutor;

import javax.swing.SwingUtilities;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 14/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public class DeleteExecutor {

    private static final Logger log = LogManager.getLogger(DeleteExecutor.class);
    /**
     * To build the request.
     */
    private final Supplier<DeleteRequestBuilder.StepBuild> requestBuilderSupplier;
    /**
     * To perform deletion.
     */
    private final Function<DeleteRequest, Consumer<String>> consumer;
    /**
     * To adapt tree.
     */
    private final Function<DeleteRequest, ? extends DeleteTreeAdapter<?>> treeAdapter;

    public DeleteExecutor(Supplier<DeleteRequestBuilder.StepBuild> requestBuilderSupplier, Function<DeleteRequest, Consumer<String>> consumer, Function<DeleteRequest, ? extends DeleteTreeAdapter<?>> treeAdapter) {
        this.requestBuilderSupplier = Objects.requireNonNull(requestBuilderSupplier);
        this.consumer = Objects.requireNonNull(consumer);
        this.treeAdapter = Objects.requireNonNull(treeAdapter);
    }

    public void execute(ActionExecutor actionExecutor, DataSourceEditor dataSourceEditor) {

        Optional<DeleteRequest> optionalRequest = getRequest(dataSourceEditor);
        if (optionalRequest.isPresent()) {
            DeleteRequest request = optionalRequest.get();
            String label = t("observe.ui.choice.confirm.delete");
            actionExecutor.addAction(label, () -> consume(request, dataSourceEditor.getNavigationUI()));
            return;
        }
        log.warn("User cancel action.");
    }

    protected void consume(DeleteRequest request, NavigationUI navigationUI) {
        NavigationTree tree = navigationUI.getTree();
        Set<String> ids = request.getIds();
        log.info(String.format("Delete adapt tree [start] %s", ids));
        Consumer<String> idConsumer = consumer.apply(request);
        for (String id : ids) {
            idConsumer.accept(id);
        }
        DeleteTreeAdapter<?> treeAdapter = this.treeAdapter.apply(request);
        try {
            SwingUtilities.invokeAndWait(() -> treeAdapter.adaptTree(request, navigationUI, tree));
        } catch (Exception e) {
            throw new ObserveSwingTechnicalException(e);
        }
        log.info(String.format("Delete adapt tree [end..] %s", ids));
    }

    protected Optional<DeleteRequest> getRequest(DataSourceEditor dataSourceEditor) {
        return Optional.ofNullable(requestBuilderSupplier.get()).map(stepBuild -> stepBuild.build(dataSourceEditor)).flatMap(Function.identity());
    }


}
