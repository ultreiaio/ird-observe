package fr.ird.observe.client.datasource.editor.api.content.actions.delete;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;

import java.util.Set;
import java.util.function.Supplier;

/**
 * Created on 01/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class DeleteLayoutTreeAdapter<OldParent extends ContentRootOpenableUINavigationNode> extends DeleteTreeAdapter<OldParent> {

    public DeleteLayoutTreeAdapter(ContentLayoutUINavigationNode incomingNode, Supplier<OldParent> parent) {
        super(incomingNode, e -> parent.get());
    }

    @Override
    public void removeChildren(NavigationUI navigationUI, OldParent parentNode, Set<String> ids) {
        // nothing to remove
    }
}

