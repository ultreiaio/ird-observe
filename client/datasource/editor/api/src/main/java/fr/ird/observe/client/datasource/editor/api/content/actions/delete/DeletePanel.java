package fr.ird.observe.client.datasource.editor.api.content.actions.delete;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.usage.UsageForDisplayUIHandler;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;
import fr.ird.observe.client.util.DisabledItemSelectionModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.UsageCountWithLabel;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.event.HyperlinkEvent;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.Vector;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 07/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@GenerateTemplate(template = "DeleteDependenciesMessage.ftl")
public class DeletePanel extends JPanel {

    private final Object[] options;
    private final String action;
    private final JPanel panelExtraMessages;
    private final DeleteRequestBuilder builder;
    private JOptionPane optionPane;
    private UsageCountWithLabel compositions;
    private UsageCountWithLabel aggregations;

    public DeletePanel(DeleteRequestBuilder builder) {

        super(new BorderLayout());
        this.builder = builder;

        action = ObserveKeyStrokesEditorApi.suffixTextWithKeyStroke(t("observe.ui.choice.confirm.delete"), ObserveKeyStrokesEditorApi.KEY_STROKE_ENTER);

        options = new Object[]{action};

        JPanel panelNorth = new JPanel(new BorderLayout());
        JPanel panelMessages = new JPanel(new GridLayout(0, 1));

        panelMessages.add(new JLabel(t(builder.getDeleteMessage())));
        if (builder.getDeleteExtraMessage() != null) {
            panelMessages.add(BorderLayout.NORTH, new JSeparator(SwingConstants.HORIZONTAL));
            panelMessages.add(new JLabel(builder.getDeleteExtraMessage()));
        }
        panelNorth.add(BorderLayout.NORTH, panelMessages);
        JList<?> list = new JList<>(new Vector<>(builder.getData()));
        list.setFocusable(false);
        list.setSelectionModel(new DisabledItemSelectionModel());
        panelNorth.add(BorderLayout.CENTER, new JScrollPane(list));
        panelExtraMessages = new JPanel(new GridLayout(0, 1));
        panelNorth.add(BorderLayout.SOUTH, panelExtraMessages);

        JLabel information = new JLabel(t("observe.ui.choice.cancel.tip"), UIHelper.getUIManagerActionIcon("information"), SwingConstants.LEFT);
        information.setFont(information.getFont().deriveFont(Font.ITALIC).deriveFont(11f));

        JPanel panelSouth = new JPanel(new GridLayout(0, 1));

        panelSouth.add(BorderLayout.NORTH, new JSeparator(SwingConstants.HORIZONTAL));
        panelSouth.add(BorderLayout.CENTER, information);

        add(BorderLayout.CENTER, panelNorth);
        add(BorderLayout.SOUTH, panelSouth);
    }

    public void addDependenciesMessage(UsageCountWithLabel compositions, UsageCountWithLabel aggregations) {
        setCompositions(compositions);
        setAggregations(aggregations);
        String message = DeletePanelTemplate.generate(this);
        JTextPane comp = new JTextPane();
        UIInitHelper.initInformationPane(comp, message);
        comp.addHyperlinkListener(e -> {
            if (e.getEventType() != HyperlinkEvent.EventType.ACTIVATED) {
                return;
            }
            String href = e.getDescription();
            switch (href) {
                case "showCompositions":
                    showCompositionsDetail(compositions);
                    break;
                case "showAggregations":
                    showAggregationsDetail(aggregations);
                    break;
            }
        });
        getPanelExtraMessages().add(comp);
    }

    protected void showCompositionsDetail(UsageCount usages) {
        @SuppressWarnings("unchecked") Class<? extends OpenableDto> dtoType = (Class<? extends OpenableDto>) builder.getDtoType();
        Collection<ToolkitIdLabel> data = builder.getData();
        UsageForDisplayUIHandler.showCompositionDataUsages(builder.getOpenableServiceSupplier().get(), dtoType, data, usages);
    }

    protected void showAggregationsDetail(UsageCount usages) {
        @SuppressWarnings("unchecked") Class<? extends OpenableDto> dtoType = (Class<? extends OpenableDto>) builder.getDtoType();
        Collection<ToolkitIdLabel> data = builder.getData();
        UsageForDisplayUIHandler.showAggregationDataUsages(builder.getOpenableServiceSupplier().get(), dtoType, data, usages);
    }

    public String compositionDecorateType(Class<? extends BusinessDto> type) {
        return compositions.getLabelWithCount(type);
    }

    public String aggregationDecorateType(Class<? extends BusinessDto> type) {
        return aggregations.getLabelWithCount(type);
    }

    public boolean isComposition() {
        return compositions != null && compositions.size() > 0;
    }

    public boolean isAggregation() {
        return aggregations != null && aggregations.size() > 0;
    }

    public Set<Class<? extends BusinessDto>> getCompositionsKeySet() {
        return compositions.keySet();
    }

    public Set<Class<? extends BusinessDto>> getAggregationsKeySet() {
        return aggregations.keySet();
    }

    public void setCompositions(UsageCountWithLabel compositions) {
        this.compositions = compositions;
    }

    public void setAggregations(UsageCountWithLabel aggregations) {
        this.aggregations = aggregations;
    }

    public JPanel getPanelExtraMessages() {
        return panelExtraMessages;
    }

    public JOptionPane getOptionPane() {
        if (optionPane == null) {
            optionPane = new JOptionPane(this, JOptionPane.WARNING_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, options[0]);
            JButton jButton = UsageUIHandlerSupport.findButton(optionPane, action);
            Objects.requireNonNull(jButton).setIcon(UIHelper.getContentActionIcon("delete"));
        }
        return optionPane;
    }

    public Object[] getOptions() {
        return options;
    }

    public String getAction() {
        return action;
    }

}
