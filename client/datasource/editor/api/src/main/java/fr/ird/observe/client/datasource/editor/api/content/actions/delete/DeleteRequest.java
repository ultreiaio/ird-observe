package fr.ird.observe.client.datasource.editor.api.content.actions.delete;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;

import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

/**
 * Created on 14/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public class DeleteRequest {

    /**
     * Ids to delete.
     */
    private final Set<String> ids;

    public DeleteRequest(Set<ToolkitIdLabel> data) {
        this.ids = data.stream().map(ToolkitIdLabel::getId).collect(Collectors.toSet());
    }

    public Set<String> getIds() {
        return ids;
    }

    public String getId() {
        return ids.iterator().next();
    }

    public boolean isSingle() {
        return ids.size() == 1;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DeleteRequest.class.getSimpleName() + "[", "]")
                .add("ids=" + ids)
                .toString();
    }
}
