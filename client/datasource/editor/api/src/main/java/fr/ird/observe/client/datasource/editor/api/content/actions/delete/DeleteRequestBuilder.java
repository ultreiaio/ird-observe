package fr.ird.observe.client.datasource.editor.api.content.actions.delete;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.navigation.id.CloseNodeVetoException;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.services.service.data.OpenableService;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JOptionPane;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 14/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
@SuppressWarnings("UnusedReturnValue")
public class DeleteRequestBuilder {
    private static final Logger log = LogManager.getLogger(DeleteRequestBuilder.class);
    /**
     * dto type to delete.
     */
    private final Class<? extends BusinessDto> dtoType;
    /**
     * Data to delete.
     */
    private final Collection<ToolkitIdLabel> data;
    /**
     * Business project (used to decorate types).
     */
    private final BusinessProject businessProject;
    /**
     * Optional edit node.
     */
    private IdNode<?> editNode;
    /**
     * Title of dialog.
     */
    private String deleteTitle;
    /**
     * Message of dialog.
     */
    private String deleteMessage;
    /**
     * Extra message of dialog.
     */
    private String deleteExtraMessage;
    /**
     * Extra message supplier.
     */
    private BiFunction<DeletePanel, Collection<ToolkitIdLabel>, DeletePanel> deleteExtraMessageSupplier;
    /**
     * Optional openable service supplier.
     */
    private Supplier<OpenableService> openableServiceSupplier;

    static class BuilderImpl implements StepSetExtra, DeleteRequestBuilder.StepBuild {
        private final DeleteRequestBuilder builder;

        public BuilderImpl(DeleteRequestBuilder builder) {
            this.builder = builder;
        }

        @Override
        public StepSetExtra setDeleteTitle(String deleteTitle) {
            builder.setDeleteTitle(deleteTitle);
            return this;
        }

        @Override
        public StepSetExtra setDeleteMessage(String deleteMessage) {
            builder.setDeleteMessage(deleteMessage);
            return this;
        }

        @Override
        public StepSetExtra setDeleteExtraMessage(String deleteExtraMessage) {
            builder.setDeleteExtraMessage(deleteExtraMessage);
            return this;
        }

        @Override
        public StepSetExtra setEditNode(IdNode<?> editNode) {
            builder.setEditNode(editNode);
            return this;
        }

        @Override
        public StepSetExtra setDeleteExtraMessageSupplier(BiFunction<DeletePanel, Collection<ToolkitIdLabel>, DeletePanel> deleteExtraMessageSupplier) {
            builder.setDeleteExtraMessageSupplier(deleteExtraMessageSupplier);
            return this;
        }

        @Override
        public StepSetExtra setOpenableServiceSupplier(Supplier<OpenableService> openableServiceSupplier) {
            builder.setOpenableServiceSupplier(openableServiceSupplier);
            return this;
        }

        @Override
        public Optional<DeleteRequest> build(DataSourceEditor dataSourceEditor) {
            return builder.build(dataSourceEditor);
        }
    }

    public static StepSetExtra create(Class<? extends BusinessDto> dtoType, ToolkitIdLabel id) {
        return create(dtoType, Collections.singleton(id));
    }

    public static StepSetExtra create(Class<? extends BusinessDto> dtoType, Collection<ToolkitIdLabel> data) {
        DeleteRequestBuilder builder = new DeleteRequestBuilder(ObserveBusinessProject.get(), dtoType, data);
        return new DeleteRequestBuilder.BuilderImpl(builder);
    }

    private DeleteRequestBuilder(BusinessProject businessProject, Class<? extends BusinessDto> dtoType, Collection<ToolkitIdLabel> data) {
        this.dtoType = Objects.requireNonNull(dtoType);
        this.data = Objects.requireNonNull(data);
        this.businessProject = Objects.requireNonNull(businessProject);
        //FIXME Make sure this is done before coming here...
//        getDecoratorService().installToolkitIdLabelDecorator(dtoType, data.stream());
    }

    public Class<? extends BusinessDto> getDtoType() {
        return dtoType;
    }

    public Collection<ToolkitIdLabel> getData() {
        return data;
    }

    public IdNode<?> getEditNode() {
        return editNode;
    }

    public String getDeleteTitle() {
        return deleteTitle;
    }

    public String getDeleteMessage() {
        return deleteMessage;
    }

    public String getDeleteExtraMessage() {
        return deleteExtraMessage;
    }

    public Supplier<OpenableService> getOpenableServiceSupplier() {
        return openableServiceSupplier;
    }

    public BusinessProject getBusinessProject() {
        return businessProject;
    }

    public void setDeleteExtraMessageSupplier(BiFunction<DeletePanel, Collection<ToolkitIdLabel>, DeletePanel> deleteExtraMessageSupplier) {
        this.deleteExtraMessageSupplier = deleteExtraMessageSupplier;
    }

    public void setOpenableServiceSupplier(Supplier<OpenableService> openableServiceSupplier) {
        this.openableServiceSupplier = openableServiceSupplier;
    }

    public DeleteRequestBuilder setDeleteTitle(String deleteTitle) {
        this.deleteTitle = deleteTitle;
        return this;
    }

    public DeleteRequestBuilder setDeleteMessage(String deleteMessage) {
        this.deleteMessage = deleteMessage;
        return this;
    }

    public DeleteRequestBuilder setDeleteExtraMessage(String deleteExtraMessage) {
        this.deleteExtraMessage = deleteExtraMessage;
        return this;
    }

    public DeleteRequestBuilder setEditNode(IdNode<?> editNode) {
        this.editNode = editNode;
        return this;
    }

    public Optional<DeleteRequest> build(DataSourceEditor dataSourceEditor) {
        if (deleteTitle == null) {
            deleteTitle = t("observe.ui.title.delete");
        }
        if (deleteMessage == null) {
            deleteMessage = t("observe.ui.action.delete.message", data.size(), businessProject.getLongTitle(dtoType));
        }
        boolean confirmDelete = confirmDelete(dataSourceEditor);
        if (!confirmDelete) {
            return Optional.empty();
        }
        if (editNode != null) {
            boolean canContinue = doCloseEditNode(dataSourceEditor);
            if (!canContinue) {
                return Optional.empty();
            }
        }
        return Optional.of(new DeleteRequest(new LinkedHashSet<>(data)));
    }

    protected boolean confirmDelete(DataSourceEditor dataSourceEditor) {
        DeletePanel deletePanel = createDeletePanel();
        JOptionPane optionPane = deletePanel.getOptionPane();
        int response = dataSourceEditor.getHandler().askToUser(optionPane, t(getDeleteTitle()), deletePanel.getOptions());
        return response == 0;
    }

    private boolean doCloseEditNode(DataSourceEditor dataSourceEditor) {
        String editNodeId = editNode.getId();
        if (editNode.isEnabled() && data.stream().anyMatch(d -> Objects.equals(d.getId(), editNodeId))) {
            try {
                ChangeMode.closeData(dataSourceEditor.getHandler(), dataSourceEditor, editNode);
            } catch (CloseNodeVetoException e) {
                log.error("Could not close data from callback", e);
                return false;
            }
        }
        return true;
    }

    private DeletePanel createDeletePanel() {
        DeletePanel deletePanel = new DeletePanel(this);
        if (deleteExtraMessageSupplier != null) {
            deletePanel = deleteExtraMessageSupplier.apply(deletePanel, data);
        }
        return deletePanel;
    }

    @SuppressWarnings("unused")
    public interface StepSetExtra extends StepBuild {

        StepSetExtra setDeleteTitle(String deleteTitle);

        StepSetExtra setDeleteMessage(String deleteMessage);

        StepSetExtra setDeleteExtraMessage(String deleteExtraMessage);

        StepSetExtra setEditNode(IdNode<?> editNode);

        StepSetExtra setOpenableServiceSupplier(Supplier<OpenableService> openableServiceSupplier);

        StepSetExtra setDeleteExtraMessageSupplier(BiFunction<DeletePanel, Collection<ToolkitIdLabel>, DeletePanel> deleteExtraMessageSupplier);
    }

    public interface StepBuild {

        Optional<DeleteRequest> build(DataSourceEditor dataSourceEditor);
    }

}
