package fr.ird.observe.client.datasource.editor.api.content.actions.delete;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationInitializer;

import java.util.Set;
import java.util.function.Function;

/**
 * Created on 19/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class DeleteRootTreeAdapter<OldParent extends NavigationNode> extends DeleteTreeAdapter<OldParent> {

    public DeleteRootTreeAdapter(NavigationNode incomingNode, Function<NavigationNode, OldParent> parentSupplier) {
        super(incomingNode, parentSupplier);
    }

    @Override
    public void removeChildren(NavigationUI navigationUI, OldParent parentNode, Set<String> ids) {
        super.removeChildren(navigationUI, parentNode, ids);
        // update navigation result
        navigationUI.getTree().getModel().updateNavigationResult();
        NavigationNode rootNode = parentNode.getRoot();
        RootNavigationInitializer initializer = (RootNavigationInitializer) rootNode.getInitializer();
        boolean removeGroupBy = parentNode.getChildCount() == 0 && !initializer.getRequest().isLoadEmptyGroupBy();
        if (removeGroupBy) {
            parentNode.removeFromParent();
            if (rootNode.isNotLeaf()) {
                navigationUI.getTree().selectFirstNode();
            }
        }
    }
}
