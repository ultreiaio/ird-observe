package fr.ird.observe.client.datasource.editor.api.content.actions.delete;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;

import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

/**
 * Created on 14/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public abstract class DeleteTreeAdapter<OldParent extends NavigationNode> {

    /**
     * Incoming node where operation is done.
     */
    private final NavigationNode incomingNode;
    /**
     * The parent node supplier (this node can be the incoming node, or one
     * of his ancestor).
     */
    private final Function<NavigationNode, OldParent> parentSupplier;

    public DeleteTreeAdapter(NavigationNode incomingNode, Function<NavigationNode, OldParent> parentSupplier) {
        this.incomingNode = Objects.requireNonNull(incomingNode);
        this.parentSupplier = Objects.requireNonNull(parentSupplier);
    }

    public void removeChildren(NavigationUI navigationUI, OldParent parentNode, Set<String> ids) {
        parentNode.removeChildren(ids);
    }

    public final void adaptTree(DeleteRequest request, NavigationUI navigationUI, NavigationTree tree) {
        OldParent parentNode = getOldParentNode(getIncomingNode());
        Set<String> ids = request.getIds();
        adaptParentNode(navigationUI, parentNode, ids);
        doFinalSelect(tree, parentNode);
    }

    public final OldParent getOldParentNode(NavigationNode incomingNode) {
        return parentSupplier.apply(incomingNode);
    }

    public void adaptParentNode(NavigationUI navigationUI, OldParent parentNode, Set<String> ids) {
        // reload node data to the root (all lastUpdateDate has been updated)
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2786
        parentNode.reloadNodeDataToRoot();
        // remove obsolete nodes
        removeChildren(navigationUI, parentNode, ids);
    }

    public final void doFinalSelect(NavigationTree tree, OldParent parentNode) {
        if (parentNode.getParent() == null) {
            // limit case: parent node was removed
            tree.selectFirstNode();
        } else {
            // re-select the node (to update associated form)
            tree.reSelectSafeNode(parentNode);
        }
    }

    public NavigationNode getIncomingNode() {
        return incomingNode;
    }
}
