package fr.ird.observe.client.datasource.editor.api.content.actions.id;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModelStates;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;

import java.awt.event.ActionEvent;
import java.util.Objects;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 25/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ChangeId<U extends ContentUI> extends ContentUIActionSupport<U> implements ConfigureMenuAction<U> {

    private final ChangeIdRequest request;
    private final ChangeIdExecutor executor;


    public static <U extends ContentReferentialUI<?, ?, U>> void installAction(U ui) {
        ChangeIdRequest request = ui.getModel().getChangeIdRequest();
        ChangeIdExecutor executor = new ChangeIdExecutor(ui.getToggleConfigure()) {
            @Override
            protected Set<String> existingIds(ObserveSwingDataSource dataSource, DtoReference reference) {
                @SuppressWarnings("unchecked") Class<? extends ReferentialDto> dtoType = (Class<? extends ReferentialDto>) reference.getDtoType();
                return dataSource.getReferentialService().loadIds(dtoType);
            }

            @Override
            protected void replaceId(ObserveSwingDataSource dataSource, DtoReference reference, String newId) {
                @SuppressWarnings("unchecked") Class<? extends ReferentialDto> dtoType = (Class<? extends ReferentialDto>) reference.getDtoType();
                dataSource.getReferentialService().changeId(dtoType, reference.getId(), newId);
            }

            @Override
            protected void updateUI() {
                ui.open();
                NavigationTree tree = ui.getHandler().getDataSourceEditor().getNavigationUI().getTree();
                NavigationNode selectedNode = tree.getSelectedNode();
                //FIXME:Tree No on a such referential node, there is no child! no no need to do this
                //FIXME:Test use a less brutal node method
                selectedNode.updateNode();
                selectedNode.nodeChanged();

                //FIXME Was this on v7
//                ui.getHandler().updateList();
            }
        };
        ChangeId<U> action = new ChangeId<>(request, executor);
        ui.getModel().getStates().addPropertyChangeListener(ContentReferentialUIModelStates.PROPERTY_SELECTED_BEAN_REFERENCE, evt -> {
            ReferentialDtoReference newValue = (ReferentialDtoReference) evt.getNewValue();
            ContentReferentialUIModelStates<?, ?> source = (ContentReferentialUIModelStates<?, ?>) evt.getSource();
            action.setEnabled(newValue != null && source.isEditable());
            request.reset();
        });
        ChangeId.init(ui, ui.getChangeId(), action);
    }

    public ChangeId(ChangeIdRequest request, ChangeIdExecutor executor) {
        super(t("observe.referential.Referential.action.changeId.tip"), t("observe.referential.Referential.action.changeId.tip"), "changeId", ObserveKeyStrokesEditorApi.KEY_STROKE_CHANGE_ID);
        this.request = Objects.requireNonNull(request);
        this.executor = Objects.requireNonNull(executor);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        executor.execute(request);
    }

}
