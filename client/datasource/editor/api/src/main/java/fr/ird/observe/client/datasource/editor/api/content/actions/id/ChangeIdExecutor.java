package fr.ird.observe.client.datasource.editor.api.content.actions.id;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.reference.DtoReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 25/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ChangeIdExecutor implements WithClientUIContextApi {
    private static final Logger log = LogManager.getLogger(ChangeIdExecutor.class);

    private final JComponent editor;
    private final ObserveSwingDataSource dataSource;

    public ChangeIdExecutor(JComponent editor) {
        this.editor = editor;
        this.dataSource = getDataSourcesManager().getMainDataSource();
    }

    protected abstract Set<String> existingIds(ObserveSwingDataSource dataSource, DtoReference reference);

    protected abstract void replaceId(ObserveSwingDataSource dataSource, DtoReference reference, String newId);

    protected abstract void updateUI();

    public void execute(ChangeIdRequest request) {

        DtoReference reference = request.getReference();
        String id = reference.getId();
        log.info(String.format("Will change id of: %s", id));

        Set<String> existingIds = existingIds(dataSource, reference);
        log.info(String.format("Loaded %d existing id(s).", existingIds.size()));

        String newId = getNewId(reference, existingIds);
        if (newId != null) {
            log.info(String.format("change id: %s to new id: %s", id, newId));
            getBusyModel().addTask("Change id");
            try {
                replaceId(dataSource, reference, newId);
                dataSource.setModified(true);
                updateUI();
            } finally {
                getBusyModel().popTask();
            }
        } else {
            log.info(String.format("User cancel change id: %s", id));
        }
    }

    private String getNewId(DtoReference reference, Set<String> existingIds) {

        String acceptOption = t("observe.ui.choice.confirm.replace");
        Object[] options = {acceptOption, t("observe.ui.choice.cancel")};

        JOptionPane ui = createUI(reference, existingIds, acceptOption, options);

        int response = askToUser(ui, t("observe.referential.Referential.action.changeId.choose.new.id.title"), options);
        String result = null;
        if (response == 0) {
            Object value = ui.getInputValue();
            if (isValueValid(reference.getDtoType(), existingIds, value)) {
                result = value.toString().trim();
            }
        }
        return result;
    }

    private JOptionPane createUI(DtoReference reference, Set<String> existingIds, String acceptOption, Object[] options) {

        Class<? extends BusinessDto> beanType = reference.getDtoType();

        String typeTitle = t("observe.referential.Referential.action.changeId.choose.new.id", t(I18nDecoratorHelper.getType(beanType)));
        JOptionPane pane = new JOptionPane(typeTitle, JOptionPane.INFORMATION_MESSAGE,
                                           JOptionPane.OK_CANCEL_OPTION, null,
                                           options, reference.getId());

        pane.setWantsInput(true);
        pane.setSelectionValues(null);
        pane.setInitialSelectionValue(reference.getId());
        pane.setComponentOrientation(editor.getComponentOrientation());
        pane.setPreferredSize(new Dimension(600, 120));

        //FIXME Find a nice way to do this
        Field field;
        try {
            field = pane.getUI().getClass().getDeclaredField("inputComponent");
        } catch (NoSuchFieldException ex) {
            try {
                field = pane.getUI().getClass().getSuperclass().getDeclaredField("inputComponent");
            } catch (NoSuchFieldException exc) {
                throw new ObserveSwingTechnicalException("Can't find field inputComponent on ui: " + pane.getUI(), exc);
            }
        }
        field.setAccessible(true);
        JButton jButton = Objects.requireNonNull(UsageUIHandlerSupport.findButton(pane, acceptOption));
        jButton.setEnabled(false);
        try {
            JTextField textField = (JTextField) field.get(pane.getUI());
            textField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    String newValue = ((JTextField) e.getSource()).getText();
                    log.debug(String.format("New id: %s", newValue));
                    boolean valid = isValueValid(beanType, existingIds, newValue);
                    jButton.setEnabled(valid);
                }
            });
        } catch (IllegalAccessException ex) {
            throw new ObserveSwingTechnicalException("Can't get field inputComponent on ui: " + pane, ex);
        }

        pane.addPropertyChangeListener(JOptionPane.INPUT_VALUE_PROPERTY, evt -> {
            boolean valid = isValueValid(beanType, existingIds, evt.getNewValue());
            jButton.setEnabled(valid);
        });
        return pane;
    }

    private boolean isValueValid(Class<? extends BusinessDto> type, Set<String> existingIds, Object newValue) {
        boolean result = newValue != null && newValue != JOptionPane.UNINITIALIZED_VALUE;
        if (result) {
            String newId = newValue.toString().trim();
            result = dataSource.getDataSourceService().isIdValid(type, newId);
            if (result) {
                result = !existingIds.contains(newId);
            }
        }
        return result;
    }

}
