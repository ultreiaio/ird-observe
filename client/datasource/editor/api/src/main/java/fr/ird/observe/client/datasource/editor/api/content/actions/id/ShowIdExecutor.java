package fr.ird.observe.client.datasource.editor.api.content.actions.id;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.SpringUtilities;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDtoReferenceWithCodeAware;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.choice.BooleanEditor;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.time.FastDateFormat;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.TitledBorder;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Locale;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 22/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ShowIdExecutor {

    private final DecoratorService decoratorService;
    private final FastDateFormat dateFormat;

    public ShowIdExecutor(DecoratorService decoratorService) {
        this.decoratorService = decoratorService;
        Locale locale = decoratorService.getReferentialLocale().getLocale();
        if (Locale.ENGLISH.equals(locale)) {
            dateFormat = FastDateFormat.getInstance("yyyy-MM-ddZZ HH:mm:ss");
        } else {
            dateFormat = FastDateFormat.getInstance("dd/MM/yyyy HH:mm:ss");
        }
    }

    public void execute(ShowIdRequest request, JComponent editor) {
        JPopupMenu ui = createUI(request.getReference());
        Dimension dim = ui.getPreferredSize();
        int x = (int) (editor.getPreferredSize().getWidth() - dim.getWidth());
        int y = editor.getHeight();
        ui.show(editor, x, y);
    }

    private JPopupMenu createUI(BusinessDto reference) {

        Class<? extends IdDto> beanType;
        if (reference instanceof DtoReference) {
            DtoReference dtoReference = (DtoReference) reference;

            beanType = dtoReference.getDtoType();
            decoratorService.installDecorator(dtoReference);
        } else {
            beanType = reference.getClass();
            decoratorService.installDecorator(reference);
        }

        JPanel content = new JPanel(new SpringLayout());

        FocusListener l = new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                JTextField source = (JTextField) e.getSource();
                source.setSelectionStart(0);
                source.setSelectionEnd(source.getText().length());
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        };

        boolean referential = reference instanceof ReferentialDtoReference;

        MutableInt rowNumber = new MutableInt();
        addTextField(rowNumber, content, t("observe.Common.label"), reference.toString(), l);
        addTextField(rowNumber, content, t("observe.Common.id"), reference.getId(), l);
        if (reference instanceof ReferentialDtoReferenceWithCodeAware) {
            addTextField(rowNumber, content, t("observe.referential.Referential.code"), ((ReferentialDtoReferenceWithCodeAware) reference).getCode(), l);
        }
        addTextField(rowNumber, content, t("observe.Common.createDate"), dateFormat.format(reference.getTopiaCreateDate()), l);
        addTextField(rowNumber, content, t("observe.Common.lastUpdateDate"), reference.getLastUpdateDate() == null ? "" : dateFormat.format(reference.getLastUpdateDate()), l);
        addTextField(rowNumber, content, t("observe.Common.version"), reference.getTopiaVersion(), l);

        if (referential) {
            addBooleanField(rowNumber, content, t("observe.Common.enabled"), ((ReferentialDtoReference) reference).isEnabled());
            addBooleanField(rowNumber, content, t("observe.Common.needComment"), ((ReferentialDtoReference) reference).isNeedComment());
            addTextField(rowNumber, content, t("observe.Common.homeId"), reference.getHomeId(), l);
            addTextField(rowNumber, content, t("observe.Common.uri"), ((ReferentialDtoReference) reference).getUri(), l);
        }

        SpringUtilities.makeCompactGrid(content, rowNumber.intValue(), 2, 5, 5, 5, 5);

        String title = t("observe.Common.technicalInformation", t(I18nDecoratorHelper.getType(beanType)));

        content.setBorder(new TitledBorder(title + "      "));

        JPopupMenu popup = new JPopupMenu();
        popup.setBorderPainted(true);
        popup.add(content);
        popup.pack();
        return popup;
    }

    private void addTextField(MutableInt rowNumber, JPanel content, String text, Object value, FocusListener l) {
        if (value == null) {
            return;
        }
        JTextField comp = new JTextField(String.valueOf(value));
        comp.setEditable(false);
        comp.addFocusListener(l);
        addField(rowNumber, content, text, comp);
    }

    private void addBooleanField(MutableInt rowNumber, JPanel content, String text, Boolean value) {
        if (value == null) {
            return;
        }
        JLabel comp = new JLabel(I18n.t(BooleanEditor.Value.valueOf(value).getDefaultLibelle()));
        addField(rowNumber, content, text, comp);
    }

    private void addField(MutableInt rowNumber, JPanel content, String text, JComponent value) {
        content.add(new JLabel(text));
        content.add(Objects.requireNonNull(value));
        rowNumber.increment();
    }
}
