package fr.ird.observe.client.datasource.editor.api.content.actions.id;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUI;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUI;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUI;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUI;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUITableModel;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModelStates;
import fr.ird.observe.dto.reference.ReferentialDtoReference;

import javax.swing.JToggleButton;
import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created on 22/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ShowTechnicalInformations<U extends ContentUI> extends ContentUIActionSupport<U> implements ConfigureMenuAction<U> {

    private final ShowIdRequest request;
    private final ShowIdExecutor executor;

    JToggleButton toggleConfigure;

    @Override
    public JToggleButton getToggleConfigure() {
        return toggleConfigure == null ? ConfigureMenuAction.super.getToggleConfigure() : toggleConfigure;
    }

    @Override
    public void setToggleConfigure(JToggleButton toggleConfigure) {
        this.toggleConfigure = toggleConfigure;
    }

    public static <U extends ContentEditUI<?, U>> void installAction(U ui) {
        ShowIdRequest request = ui.getModel().toShowIdRequest();
        ShowTechnicalInformations<U> action = new ShowTechnicalInformations<>(request, new ShowIdExecutor(ui.getHandler().getDecoratorService()));
        ui.getModel().getStates().addPropertyChangeListener(ContentEditUIModelStates.PROPERTY_MODE, evt -> {
            ContentMode newValue = (ContentMode) evt.getNewValue();
            action.setEnabled(newValue != null && newValue != ContentMode.CREATE);
            request.reset();
        });
        ShowTechnicalInformations.init(ui, ui.getShowTechnicalInformations(), action);
    }

    public static <U extends ContentLayoutUI<?, U>> void installAction(U ui) {
        ShowIdRequest request = ui.getModel().toShowIdRequest();
        ShowTechnicalInformations<U> action = new ShowTechnicalInformations<>(request, new ShowIdExecutor(ui.getHandler().getDecoratorService()));
        ui.getModel().getStates().addPropertyChangeListener(ContentEditUIModelStates.PROPERTY_MODE, evt -> {
            ContentMode newValue = (ContentMode) evt.getNewValue();
            action.setEnabled(newValue != null && newValue != ContentMode.CREATE);
            request.reset();
        });
        ShowTechnicalInformations.init(ui, ui.getShowTechnicalInformations(), action);
    }

    public static <U extends ContentOpenableUI<?, U>> void installAction(U ui) {
        ShowIdRequest request = ui.getModel().getShowIdRequest();
        ShowTechnicalInformations<U> action = new ShowTechnicalInformations<>(request, new ShowIdExecutor(ui.getHandler().getDecoratorService()));
        ui.getModel().getStates().addPropertyChangeListener(ContentOpenableUIModelStates.PROPERTY_MODE, evt -> {
            ContentMode newValue = (ContentMode) evt.getNewValue();
            action.setEnabled(newValue != null && newValue != ContentMode.CREATE);
            request.reset();
        });
        ShowTechnicalInformations.init(ui, ui.getShowTechnicalInformations(), action);
    }

    public static <U extends ContentRootOpenableUI<?, U>> void installAction(U ui) {
        ShowIdRequest request = ui.getModel().getShowIdRequest();
        ShowTechnicalInformations<U> action = new ShowTechnicalInformations<>(request, new ShowIdExecutor(ui.getHandler().getDecoratorService()));
        ui.getModel().getStates().addPropertyChangeListener(ContentOpenableUIModelStates.PROPERTY_MODE, evt -> {
            ContentMode newValue = (ContentMode) evt.getNewValue();
            action.setEnabled(newValue != null && newValue != ContentMode.CREATE);
            request.reset();
        });
        ShowTechnicalInformations.init(ui, ui.getShowTechnicalInformations(), action);
    }

    public static <U extends ContentTableUI<?, ?, U>> void installAction(U ui) {
        ShowIdRequest request = ui.getModel().getShowIdRequest();
        ShowTechnicalInformations<U> action = new ShowTechnicalInformations<>(request, new ShowIdExecutor(ui.getHandler().getDecoratorService()));
        ui.getTableModel().addPropertyChangeListener(ContentTableUITableModel.SELECTED_ROW_PROPERTY, evt -> request.reset());
        ui.getModel().getStates().addPropertyChangeListener(ContentTableUIModelStates.PROPERTY_CAN_SAVE_ROW, evt -> request.reset());
        ui.getModel().getStates().addPropertyChangeListener(ContentTableUIModelStates.PROPERTY_MODIFIED, evt -> request.reset());
        ShowTechnicalInformations.init(ui, ui.getShowTechnicalInformations(), action);
    }

    public static <U extends ContentListUI<?, ?, U>> void installAction(U ui) {
        ShowIdRequest request = ui.getModel().getShowIdRequest();
        ShowTechnicalInformations<U> action = new ShowTechnicalInformations<>(request, new ShowIdExecutor(ui.getHandler().getDecoratorService()));
        ui.getModel().getStates().addPropertyChangeListener(ContentListUIModelStates.PROPERTY_ONE_SELECTED_DATA, evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            action.setEnabled(newValue != null && newValue);
            request.reset();
        });
        ShowTechnicalInformations.init(ui, ui.getShowTechnicalInformations(), action);
    }

    public static <U extends ContentRootListUI<?, ?, U>> void installAction(U ui) {
        ShowIdRequest request = ui.getModel().getShowIdRequest();
        ShowTechnicalInformations<U> action = new ShowTechnicalInformations<>(request, new ShowIdExecutor(ui.getHandler().getDecoratorService()));
        ui.getModel().getStates().addPropertyChangeListener(ContentListUIModelStates.PROPERTY_ONE_SELECTED_DATA, evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            action.setEnabled(newValue != null && newValue);
            request.reset();
        });
        ShowTechnicalInformations.init(ui, ui.getShowTechnicalInformations(), action);
    }

    public static <U extends ContentReferentialUI<?, ?, U>> void installAction(U ui) {
        ShowIdRequest request = ui.getModel().getShowIdRequest();
        ShowTechnicalInformations<U> action = new ShowTechnicalInformations<>(request, new ShowIdExecutor(ui.getHandler().getDecoratorService()));
        ui.getModel().getStates().addPropertyChangeListener(ContentReferentialUIModelStates.PROPERTY_SELECTED_BEAN_REFERENCE, evt -> {
            ReferentialDtoReference newValue = (ReferentialDtoReference) evt.getNewValue();
            action.setEnabled(newValue != null);
            request.reset();
        });
        ShowTechnicalInformations.init(ui, ui.getShowTechnicalInformations(), action);
    }

    public ShowTechnicalInformations(ShowIdRequest request, ShowIdExecutor executor) {
        super(ShowTechnicalInformations.class.getName() + "#" + request, n("observe.ui.action.show.technical.information.tip"), n("observe.ui.action.show.technical.information.tip"),
              "show-information", ObserveKeyStrokesEditorApi.KEY_STROKE_SHOW_TECHNICAL_INFORMATION);
        this.request = Objects.requireNonNull(request);
        this.executor = Objects.requireNonNull(executor);
        setCheckMenuItemIsArmed(false);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U u) {
        executor.execute(request, getToggleConfigure());
    }

}

