package fr.ird.observe.client.datasource.editor.api.content.actions.mode;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.focus.ContentZone;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.navigation.id.CloseNodeRequest;
import fr.ird.observe.navigation.id.CloseNodeVetoException;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.id.IdProjectManager;
import fr.ird.observe.navigation.id.OpenNodeRequest;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.tree.TreeNode;
import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 27/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ChangeMode<U extends ContentUI> extends ContentUIActionSupport<U> {
    private static final Logger log = LogManager.getLogger(ChangeMode.class);
    private final ChangeModeRequest request;
    private final ChangeModeProducer<U> changeModeProducer;
    private final ChangeModeExecutor<U> executor;

    public static <U extends ContentUI> ChangeMode<U> installAction(U ui) {
        ChangeModeRequest request = ChangeModeRequest.of(ui);
        ChangeModeProducer<U> changeModeProducer = new ChangeModeProducer<>(ui, request);
        ChangeModeExecutor<U> executor = new ChangeModeExecutor<>();
        ChangeMode<U> action = new ChangeMode<>(request, changeModeProducer, executor);
        return installAction(ui, action);
    }

    public static <U extends ContentUI> ChangeMode<U> installAction(U ui, ChangeMode<U> action) {
        init(ui, Objects.requireNonNull(ui).getMode(), action);
        return action;
    }

    public static void closeData(WithClientUIContextApi clientUIContext, DataSourceEditor dataSourceEditor, IdNode<?> nodeToClose) throws CloseNodeVetoException {
        if (Objects.requireNonNull(nodeToClose).isDisabled()) {
            return;
        }
        IdProjectManager navigationEditManager = clientUIContext.getObserveIdModelManager();
        CloseNodeRequest closeRequest = navigationEditManager.createCloseIdNodeRequest(nodeToClose);
        navigationEditManager.applyCloseIdNodeRequest(closeRequest);
        clientUIContext.getDataSourcesManager().saveNavigationToConfig();
        NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();
        TreeNode[] path = tree.getSelectedNode().getPath();
        NavigationNode nodeToReload = (NavigationNode) path[path.length - 1];
        log.info("Will reload node and all his children: " + nodeToReload);
        nodeToReload.nodeChangedDeep();
    }

    public static void openData(WithClientUIContextApi clientUIContext, IdNode<?> editNode, IdNode<?> selectNode, String id) throws CloseNodeVetoException {
        IdProjectManager navigationEditManager = clientUIContext.getObserveIdModelManager();
        OpenNodeRequest openRequest = navigationEditManager.createOpenIdNodeRequest(clientUIContext.getObserveEditModel(), editNode, selectNode, id);
        navigationEditManager.applyOpenIdNodeRequest(openRequest);
        clientUIContext.getDataSourcesManager().saveNavigationToConfig();
    }

    public ChangeMode(ChangeModeRequest request, ChangeModeProducer<U> changeModeProducer, ChangeModeExecutor<U> executor) {
        super("", null, null, ObserveKeyStrokesEditorApi.KEY_STROKE_CONTENT_CHANGE_MODE);
        this.request = Objects.requireNonNull(request);
        this.changeModeProducer = Objects.requireNonNull(changeModeProducer);
        this.executor = executor;
    }

    protected void updateToolTipText(String text) {
        setTooltipText(t(text, request.getDataType()));
    }

    @Override
    public void rebuildTexts(boolean updateAction) {
        super.rebuildTexts(updateAction);
        getEditor().setIcon(getIcon());
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        return executor != null && super.canExecuteAction(e) && getUi().getModel().getStates().isEditable();
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        if (executor == null) {
            return;
        }
        executor.execute(ui, request);
    }

    public ChangeModeRequest getRequest() {
        return request;
    }

    @SuppressWarnings("unused")
    public ChangeModeProducer<U> getChangeModeProducer() {
        return changeModeProducer;
    }

    @Override
    public void init() {
        super.init();
        request.addPropertyChangeListener(ChangeModeRequest.PROPERTY_CHANGE_MODE, evt -> onChanged((ContentMode) evt.getNewValue()));
    }

    public void onChanged(ContentMode newMode) {
        log.debug(String.format("Change mode to %s", newMode));
        rebuildAction(ui, newMode);
        rebuildTitle(ui, newMode);
        rebuildZones(ui);
        if (newMode != null) {
            rebuildFocus();
        }
    }

    protected void rebuildTitle(U ui, ContentMode newValue) {
        ContentUIModelStates states = ui.getModel().getStates();

        String titleSuffix;
        if (newValue == null) {
            // no action possible
            titleSuffix = I18n.t("observe.Common.action.notEditable");
        } else {
            switch (newValue) {
                case CREATE:
                    titleSuffix = I18n.t("observe.Common.action.creating");
                    break;
                case UPDATE:
                    titleSuffix = I18n.t("observe.Common.action.updating");
                    break;
                case READ:
                    boolean editable = states.isEditable();
                    if (editable) {
                        titleSuffix = I18n.t("observe.Common.action.reading");
                    } else {
                        titleSuffix = I18n.t("observe.Common.action.notEditable");
                    }
                    break;
                default:
                    throw new IllegalStateException("Can't come here");
            }
        }
        String contentTitle = states.getContentTitle() + " - " + titleSuffix;
        ui.setTitle(contentTitle);
    }

    protected void rebuildAction(U ui, ContentMode newValue) {
        boolean editable;
        String tip;
        Icon icon;
        ContentUIModelStates states = ui.getModel().getStates();
        if (newValue == null) {
            // no action possible
            tip = I18n.n("observe.data.Openable.action.notEditable.tip");
            icon = UIHelper.getContentActionIcon("not-editable");
            editable = false;
        } else {
            switch (newValue) {
                case CREATE:
                    editable = false;
                    tip = I18n.n("observe.data.Openable.action.create.tip");
                    icon = UIHelper.getContentActionIcon("add");
                    break;
                case UPDATE:
                    // can close now
                    editable = true;
                    tip = I18n.n("observe.data.Openable.action.close.tip");
                    icon = UIHelper.getContentActionIcon("unlocked");
                    break;
                case READ:
                    editable = states.isEditable();
                    if (editable) {
                        // can unlock
                        tip = I18n.n("observe.data.Openable.action.reopen.tip");
                        icon = UIHelper.getContentActionIcon("locked");
                    } else {
                        // no action possible
                        tip = I18n.n("observe.data.Openable.action.notEditable.tip");
                        icon = UIHelper.getContentActionIcon("not-editable");
                    }
                    break;
                default:
                    throw new IllegalStateException("Can't come here");
            }
        }

        setEnabled(editable);
        setIcon(icon);
        KeyStroke acceleratorKey = getAcceleratorKey();
        if (!editable) {
            removeKeyStrokeToText();
            setKeyStroke(null);
        }
        updateToolTipText(tip);
        rebuildTexts(true);
        if (!editable) {
            setKeyStroke(acceleratorKey);
        }
    }

    protected void rebuildZones(U ui) {
        if (rebuildZonePredicate(ui)) {
            rebuildEditableZone(ui);
        } else {
            rebuildNotEditableZone(ui);
        }
    }

    protected boolean rebuildZonePredicate(U ui) {
        return ui.getStates().isEditable() && !ui.getStates().isReadingMode();
    }

    public void rebuildEditableZone(U ui) {
        ui.getHandler().getDataSourceEditor().getMessageView().setVisible(true);
        ui.getHandler().fixFormSize();
        ui.getHandler().fixToggleMenuVisibility();
    }

    protected void rebuildNotEditableZone(U ui) {
        ui.getHandler().getDataSourceEditor().getMessageView().setVisible(false);
        ui.getHandler().fixFormSize();
        ui.getHandler().fixToggleMenuVisibility();
    }

    protected void rebuildFocus() {
        dirtyFocusZone(ContentZone.ZONE_NAME);
    }
}
