package fr.ird.observe.client.datasource.editor.api.content.actions.mode;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.navigation.id.CloseNodeVetoException;
import fr.ird.observe.navigation.id.IdNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.SwingUtilities;
import java.util.Objects;

/**
 * Created on 27/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ChangeModeExecutor<U extends ContentUI> {
    private static final Logger log = LogManager.getLogger(ChangeModeExecutor.class);

    public void execute(U ui, ChangeModeRequest request) {

        DataSourceEditor dataSourceEditor = ui.getHandler().getDataSourceEditor();
        if (Objects.equals(request.getChangeMode(), ContentMode.READ)) {
            doOpen(ui, dataSourceEditor, request);
        } else {
            doClose(ui, dataSourceEditor, request);
        }
    }

    protected void doClose(U ui, DataSourceEditor dataSourceEditor, ChangeModeRequest request) {
        try {
            IdNode<? extends IdDto> selectedNode = request.getSelectNode(ui);
            IdNode<? extends IdDto> editNode = request.getEditNode(ui);
            String id = request.getSelectedId(ui, selectedNode);
            log.info(String.format("Will close: %s → (select: %s - edit: %s)", id, selectedNode, editNode));
            ChangeMode.closeData(ui.getHandler(), dataSourceEditor, editNode);
            if (ui.getModel().getStates().isUpdatingMode()) {
                ui.stopEdit();
            }
            afterClose(ui, dataSourceEditor);
        } catch (CloseNodeVetoException e1) {
            log.error("Could not close data from callback", e1);
        }
    }

    protected void doOpen(U ui, DataSourceEditor dataSourceEditor, ChangeModeRequest request) {
        try {
            IdNode<?> selectedNode = request.getSelectNode(ui);
            IdNode<? extends IdDto> editNode = request.getEditNode(ui);
            String id = request.getSelectedId(ui, selectedNode);
            log.info(String.format("Will open: %s → (select: %s - edit: %s)", id, selectedNode, editNode));
            NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();
            NavigationNode previousOpenedNode = editNode.isEnabled() ? tree.getModel().getNodeFromModelNode(editNode) : null;
            ChangeMode.openData(ui.getHandler(), editNode, selectedNode, id);
            afterOpen(ui, dataSourceEditor, previousOpenedNode, id);
        } catch (CloseNodeVetoException e1) {
            log.error("Could not close data from callback", e1);
        }
    }

    protected void afterClose(U ui, DataSourceEditor dataSourceEditor) {
        ui.stopEdit();
        NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();
        NavigationNode node = tree.getSelectedNode();
        node.getParent().refreshToRoot();
        node.nodeChangedDeep();

        log.info(String.format("Will reselect node: %s", node));
        tree.reSelectSafeNodeThen(node, () -> dataSourceEditor.getModel().resetFromPreviousUi(ui));
    }

    protected void afterOpen(U ui, DataSourceEditor dataSourceEditor, NavigationNode previousOpenedNode, String id) {
        NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();

        if (previousOpenedNode != null) {
            previousOpenedNode.refreshToRoot();
        }
        NavigationNode selectedNode = tree.getSelectedNode();
        selectedNode.nodeChanged(true, true);
        if (previousOpenedNode == null || !Objects.equals(previousOpenedNode.getParent(), selectedNode.getParent())) {
            selectedNode.getParent().refreshToRoot();
        }
        selectedNode.reloadNodeData();
        selectedNode.nodeChangedDeep();
        afterOpenReselectNode(ui, dataSourceEditor, tree, selectedNode, id);
        SwingUtilities.invokeLater(tree::invalidateCellSizeCache);
//        SwingUtilities.invokeLater(tree::repaint);
    }

    protected void afterOpenReselectNode(U ui, DataSourceEditor dataSourceEditor, NavigationTree tree, NavigationNode selectedNode, String id) {
        log.info(String.format("Will reselect node: %s", selectedNode));
        tree.reSelectSafeNodeThen(selectedNode, () -> dataSourceEditor.getModel().resetFromPreviousUi(ui));
    }

}
