package fr.ird.observe.client.datasource.editor.api.content.actions.mode;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 28/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ChangeModeProducer<U extends ContentUI> {

    private final ChangeModeRequest request;
    private final U ui;
    private final PropertyChangeListener listener;

    private boolean adjusting;

    public ChangeModeProducer(U ui, ChangeModeRequest request, String... properties) {
        this.ui = Objects.requireNonNull(ui);
        this.request = Objects.requireNonNull(request);

        Set<String> modelStates = new LinkedHashSet<>();
        modelStates.add(ContentUIModelStates.PROPERTY_MODE);
        modelStates.add(ContentUIModelStates.PROPERTY_EDITABLE);
        modelStates.add(ContentUIModelStates.PROPERTY_CONTENT_TILE);
        modelStates.addAll(Arrays.asList(properties));

        listener = (PropertyChangeEvent evt) -> {
            if (modelStates.contains(evt.getPropertyName())) {
                if (adjusting) {
                    return;
                }
                try {
                    adjusting = true;
                    doRebuildChangeMode();
                } finally {
                    adjusting = false;
                }

            }
        };
        ui.getModel().getStates().addPropertyChangeListener(ContentUIModelStates.PROPERTY_OPENED, evt -> {
            ContentUIModelStates source = (ContentUIModelStates) evt.getSource();
            boolean open = (boolean) evt.getNewValue();
            if (open) {
                source.addPropertyChangeListener(listener);
                doRebuildChangeMode();
            } else {
                source.removePropertyChangeListener(listener);
            }
        });
    }

    protected ContentMode rebuildChangeMode(ContentMode newValue) {
        return newValue;
    }

    private void doRebuildChangeMode() {
        ContentMode changeMode = rebuildChangeMode(ui.getStates().getMode());
        request.setChangeMode(null);
        request.setChangeMode(changeMode);
    }

}
