package fr.ird.observe.client.datasource.editor.api.content.actions.mode;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.navigation.id.IdNode;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/**
 * Created on 27/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
@GenerateJavaBeanDefinition
public class ChangeModeRequest extends AbstractJavaBean {
    public static final String PROPERTY_CHANGE_MODE = "changeMode";
    private final String dataType;
    private ContentMode changeMode;

    public static ChangeModeRequest of(ContentUI ui) {
        return new ChangeModeRequest(ui.getModel().getSource().getScope().getI18nTranslation("type"));
    }

    public ChangeModeRequest(String dataType) {
        this.dataType = dataType;
    }

    public String getDataType() {
        return dataType;
    }

    public ContentMode getChangeMode() {
        return changeMode;
    }

    public void setChangeMode(ContentMode changeMode) {
        ContentMode oldValue = getChangeMode();
        this.changeMode = changeMode;
        firePropertyChange(PROPERTY_CHANGE_MODE, oldValue, changeMode);
    }

    protected IdNode<? extends IdDto> getSelectNode(ContentUI ui) {
        IdNode<? extends IdDto> selectedNode = ui.getModel().getSource().getInitializer().getSelectNode();
        IdNode<? extends IdDto> editNode = ui.getModel().getSource().getInitializer().getEditNode();
        if (!selectedNode.getType().equals(editNode.getType())) {
            selectedNode = selectedNode.getParent();
        }
        return selectedNode;
    }

    protected IdNode<? extends IdDto> getEditNode(ContentUI ui) {
        return ui.getModel().getSource().getInitializer().getEditNode();
    }

    protected String getSelectedId(ContentUI ui, IdNode<?> selectedNode) {
        return selectedNode.getId();
    }

    public boolean isEditable() {
        return changeMode != null;
    }
}
