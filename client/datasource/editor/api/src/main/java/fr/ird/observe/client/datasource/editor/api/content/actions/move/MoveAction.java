package fr.ird.observe.client.datasource.editor.api.content.actions.move;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUI;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUI;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUI;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.services.service.data.MoveRequest;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.AbstractButton;
import java.awt.event.ActionEvent;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * To move a data.
 * <p>
 * Created on 12/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public class MoveAction<D extends DataDto, U extends ContentUI> extends ContentUIActionSupport<U> implements ConfigureMenuAction<U> {

    /**
     * To execute the move action.
     */
    private final MoveExecutor executor;

    public static class Builder<D extends DataDto, U extends ContentUI> implements AddOnStep<D, U>, AddCallStep<D, U>, AddThenStep<D, U>, BuildStep<D, U> {

        private final U ui;
        private final Class<D> dtoType;
        private Supplier<MoveRequestBuilderStepBuild> requestBuilderSupplier;
        private MoveCall requestConsumer;
        private Function<MoveRequest, ? extends MoveTreeAdapter<?, ?, ?>> treeAdapter;

        public Builder(U ui, Class<D> dtoType) {
            this.ui = Objects.requireNonNull(ui);
            this.dtoType = Objects.requireNonNull(dtoType);
        }

        @Override
        public AddCallStep<D, U> on(Supplier<MoveRequestBuilderStepBuild> requestBuilderSupplier) {
            this.requestBuilderSupplier = Objects.requireNonNull(requestBuilderSupplier);
            return this;
        }

        @Override
        public AddThenStep<D, U> call(MoveCall requestConsumer) {
            this.requestConsumer = Objects.requireNonNull(requestConsumer);
            return this;
        }

        @Override
        public BuildStep<D, U> then(Function<U, Function<MoveRequest, ? extends MoveTreeAdapter<?, ?, ?>>> treeAdapter) {
            this.treeAdapter = Objects.requireNonNull(treeAdapter).apply(ui);
            return this;
        }

        @Override
        public MoveAction<D, U> install(Supplier<? extends AbstractButton> editor) {
            MoveExecutor moveExecutor = new MoveExecutor(requestBuilderSupplier, requestConsumer, treeAdapter);
            MoveAction<D, U> action = new MoveAction<>(dtoType, moveExecutor);
            init(ui, editor.get(), action);
            return action;
        }
    }

    public static <D extends DataDto, C extends ContainerChildDto, U extends ContentTableUI<D, C, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ui.getModel().getSource().getScope().getMainType());
    }

    public static <D extends SimpleDto, U extends ContentLayoutUI<D, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ObserveUtil.getFirstType(ui));
    }

    public static <D extends DataDto, U extends ContentUI> AddOnStep<D, U> create(U ui, Class<D> dtoType) {
        return new Builder<>(ui, dtoType);
    }

    public static <D extends EditableDto, U extends ContentEditUI<D, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ObserveUtil.getFirstType(ui));
    }

    public static <D extends OpenableDto, U extends ContentOpenableUI<D, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ObserveUtil.getFirstType(ui));
    }

    public static <D extends OpenableDto, U extends ContentListUI<D, ?, U>> AddOnStep<D, U> create(U ui) {
        return create(ui, ObserveUtil.getFirstType(ui));
    }

    protected MoveAction(Class<D> dataType, MoveExecutor executor) {
        super(null, null, "move", ObserveKeyStrokesEditorApi.KEY_STROKE_MOVE);
        this.executor = Objects.requireNonNull(executor);
        setText(I18n.t(I18nDecoratorHelper.getPropertyI18nKey(dataType, "action.move")));
        setTooltipText(I18n.t(I18nDecoratorHelper.getPropertyI18nKey(dataType, "action.move")));
    }

    @Override
    protected final void doActionPerformed(ActionEvent e, U ui) {
        executor.execute(getActionExecutor(), getDataSourceEditor());
    }

    public interface AddOnStep<D extends DataDto, U extends ContentUI> {
        AddCallStep<D, U> on(Supplier<MoveRequestBuilderStepBuild> requestBuilderSupplier);
    }

    public interface AddCallStep<D extends DataDto, U extends ContentUI> {
        AddThenStep<D, U> call(MoveCall requestConsumer);
    }

    public interface AddThenStep<D extends DataDto, U extends ContentUI> {
        BuildStep<D, U> then(Function<U, Function<MoveRequest, ? extends MoveTreeAdapter<?, ?, ?>>> treeAdapter);
    }

    public interface BuildStep<D extends DataDto, U extends ContentUI> {
        MoveAction<D, U> install(Supplier<? extends AbstractButton> editor);
    }

    @FunctionalInterface
    public interface MoveCall {

        Set<String> apply(MoveRequest moveRequest, ToolkitIdDtoBean id, Set<String> ids) throws ConcurrentModificationException;
    }
}
