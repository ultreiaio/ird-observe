package fr.ird.observe.client.datasource.editor.api.content.actions.move;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;
import fr.ird.observe.client.util.DisabledItemSelectionModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.navigation.id.CloseNodeVetoException;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.services.service.data.MoveRequest;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 14/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
@SuppressWarnings("UnusedReturnValue")
public class MoveRequestBuilder implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(MoveRequestBuilder.class);
    /**
     * Dto type of data to move.
     */
    private final Class<? extends DataDto> dtoType;
    /**
     * Id of parent data to move.
     */
    private final ToolkitIdDtoBean oldParentId;
    /**
     * Data to move.
     */
    private final Collection<ToolkitIdLabel> data;
    /**
     * To get selected target request value.
     */
    private final JCheckBox selectTarget;
    /**
     * Select target action.
     */
    private final AbstractAction selectTargetAction;
    /**
     * Apply action.
     */
    private final AbstractAction applyAction;
    /**
     * Optional edit node.
     */
    private IdNode<?> editNode;
    /**
     * To get group by value.
     */
    private Supplier<DataGroupByParameter> groupByValueSupplier;
    /**
     * To get parent candidates.
     */
    private BiFunction<DataGroupByParameter, String, List<ToolkitIdLabel>> parentCandidates;
    /**
     * Reference type of parent target.
     */
    private Class<? extends BusinessDto> parentTargetDtoType;
    /**
     * Title of dialog to select new parent.
     */
    private String askNewParentTitle;
    /**
     * Message of dialog to select new parent.
     */
    private String askNewParentMessage;
    /**
     * Option pane apply button.
     */
    private JButton jButton;
    /**
     * Option pane.
     */
    private JOptionPane optionPane;

    static class BuilderImpl implements MoveRequestBuilderStepConfigure, MoveRequestBuilderStepBuild {
        private final MoveRequestBuilder builder;

        public BuilderImpl(MoveRequestBuilder builder) {
            this.builder = builder;
        }

        @Override
        public MoveRequestBuilderStepConfigure setParentTargetDtoType(Class<? extends BusinessDto> parentTargetDtoType) {
            builder.setParentTargetDtoType(parentTargetDtoType);
            return this;
        }

        @Override
        public MoveRequestBuilderStepConfigure setAskNewParentTitle(String askNewParentTitle) {
            builder.setAskNewParentTitle(askNewParentTitle);
            return this;
        }

        @Override
        public MoveRequestBuilderStepConfigure setAskNewParentMessage(String askNewParentMessage) {
            builder.setAskNewParentMessage(askNewParentMessage);
            return this;
        }

        @Override
        public MoveRequestBuilderStepConfigure setEditNode(IdNode<?> editNode) {
            builder.setEditNode(editNode);
            return this;
        }

        @Override
        public MoveRequestBuilderStepBuild setParentCandidates(BiFunction<DataGroupByParameter, String, List<ToolkitIdLabel>> parentCandidates) {
            builder.setParentCandidates(parentCandidates);
            return this;
        }

        @Override
        public MoveRequestBuilderStepConfigure setGroupByValue(Supplier<DataGroupByParameter> groupByValue) {
            builder.setGroupByValueSupplier(groupByValue);
            return this;
        }

        @Override
        public Optional<MoveRequest> build(DataSourceEditor dataSourceEditor) {
            return builder.build(dataSourceEditor);
        }
    }

    public static MoveRequestBuilderStepConfigure create(Class<? extends DataDto> dtoType, ToolkitIdDtoBean oldParentId, ToolkitIdLabel id) {
        return create(dtoType, oldParentId, Collections.singleton(id));
    }

    public static MoveRequestBuilderStepConfigure create(Class<? extends DataDto> dtoType, ToolkitIdDtoBean oldParentId, Collection<ToolkitIdLabel> data) {
        MoveRequestBuilder builder = new MoveRequestBuilder(dtoType, oldParentId, data);
        return new BuilderImpl(builder);
    }

    private MoveRequestBuilder(Class<? extends DataDto> dtoType, ToolkitIdDtoBean oldParentId, Collection<ToolkitIdLabel> data) {
        this.dtoType = Objects.requireNonNull(dtoType);
        this.oldParentId = Objects.requireNonNull(oldParentId);
        this.data = Objects.requireNonNull(data);
        KeyStroke keyStroke = ObserveKeyStrokesEditorApi.KEY_STROKE_SELECT_TARGET;
        String t = ObserveKeyStrokesEditorApi.suffixTextWithKeyStroke(t("observe.ui.move.selectTarget"), keyStroke);
        this.selectTarget = new JCheckBox(t);
        selectTargetAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectTarget.setSelected(!selectTarget.isSelected());
                log.info(String.format("select target? %s", selectTarget.isSelected()));
            }
        };
        selectTargetAction.putValue(Action.ACCELERATOR_KEY, keyStroke);
        selectTargetAction.putValue(Action.NAME, t);
        selectTarget.setFocusable(false);
        keyStroke = ObserveKeyStrokesEditorApi.KEY_STROKE_MOVE;
        t = ObserveKeyStrokesEditorApi.suffixTextWithKeyStroke(t("observe.ui.choice.confirm.move"), keyStroke);

        applyAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                log.info("Apply :)");
                jButton.doClick();
            }
        };
        applyAction.putValue(Action.ACCELERATOR_KEY, keyStroke);
        applyAction.putValue(Action.NAME, t);
    }

    private MoveRequestBuilder setEditNode(IdNode<?> editNode) {
        this.editNode = editNode;
        return this;
    }

    public MoveRequestBuilder setParentTargetDtoType(Class<? extends BusinessDto> parentTargetDtoType) {
        this.parentTargetDtoType = parentTargetDtoType;
        return this;
    }

    public MoveRequestBuilder setAskNewParentTitle(String askNewParentTitle) {
        this.askNewParentTitle = askNewParentTitle;
        return this;
    }

    public MoveRequestBuilder setAskNewParentMessage(String askNewParentMessage) {
        this.askNewParentMessage = askNewParentMessage;
        return this;
    }

    public MoveRequestBuilder setParentCandidates(BiFunction<DataGroupByParameter, String, List<ToolkitIdLabel>> parentCandidates) {
        this.parentCandidates = parentCandidates;
        return this;
    }

    public MoveRequestBuilder setGroupByValueSupplier(Supplier<DataGroupByParameter> groupByValueSupplier) {
        this.groupByValueSupplier = groupByValueSupplier;
        return this;
    }

    public Optional<MoveRequest> build(DataSourceEditor dataSourceEditor) {
        if (parentTargetDtoType == null) {
            parentTargetDtoType = oldParentId.getType();
        }
        if (askNewParentTitle == null) {
            askNewParentTitle = I18nDecoratorHelper.getPropertyI18nKey(dtoType, "action.move.choose.parent.title");
        }
        if (askNewParentMessage == null) {
            askNewParentMessage = I18nDecoratorHelper.getPropertyI18nKey(dtoType, "action.move.choose.parent.message");
        }

        DataGroupByParameter groupBy = Objects.requireNonNull(this.groupByValueSupplier, "No groupByValue supplier set.").get();
        List<ToolkitIdLabel> parentCandidates = Objects.requireNonNull(this.parentCandidates, "No parent candidates set.").apply(groupBy, oldParentId.getId());
        ToolkitIdLabel newParentId = askNewParent(parentCandidates, askNewParentTitle, askNewParentMessage).orElse(null);
        if (newParentId == null) {
            return Optional.empty();
        }
        Set<String> ids = data.stream().map(ToolkitIdLabel::getId).collect(Collectors.toSet());
        if (editNode != null) {
            if (editNode.isEnabled() && ids.contains(editNode.getId())) {
                try {
                    ChangeMode.closeData(dataSourceEditor.getHandler(), dataSourceEditor, editNode);
                } catch (CloseNodeVetoException e) {
                    log.error("Could not close data from callback", e);
                    return Optional.empty();
                }
            }
        }
        return Optional.of(new MoveRequest(oldParentId, newParentId, ids, selectTarget.isSelected()));
    }

    private Optional<ToolkitIdLabel> askNewParent(List<ToolkitIdLabel> parentCandidates, String dialogTitle, String dialogMessage) {
        Decorator decorator = getDecoratorService().getToolkitIdLabelDecoratorByType(parentTargetDtoType);
        FilterableComboBox<ToolkitIdLabel> editor = UIHelper.newToolkitIdLabelFilterableComboBox(
                parentTargetDtoType,
                decorator,
                parentCandidates);

        String continueActionText = (String) applyAction.getValue(Action.NAME);
        Object[] options = {continueActionText};
        JPanel panel = new JPanel(new BorderLayout(3, 3));

        JPanel panelNorth = new JPanel(new BorderLayout());
        JPanel panelMessages = new JPanel(new BorderLayout(3, 3));
        panelMessages.add(BorderLayout.NORTH, new JLabel(t(t("observe.ui.action.move.message", data.size(), I18nDecoratorHelper.getType(dtoType)))));

        JList<?> list = new JList<>(new Vector<>(data));
        list.setFocusable(false);
        list.setSelectionModel(new DisabledItemSelectionModel());
        list.setVisibleRowCount(5);
        panelMessages.add(BorderLayout.CENTER, new JScrollPane(list));

        JPanel panelMessage2 = new JPanel(new GridLayout(0, 1));
        panelMessage2.add(new JLabel());
        panelMessage2.add(new JLabel(t(dialogMessage)));
        panelMessages.add(BorderLayout.SOUTH, panelMessage2);

        panelNorth.add(BorderLayout.NORTH, panelMessages);
        panelNorth.add(BorderLayout.CENTER, editor);

        JPanel panelSelectTarget = new JPanel(new GridLayout(0, 1));
        //FIXME:Move Add a default client configuration option to set this value
        selectTarget.setSelected(true);
        panelSelectTarget.add(new JLabel());
        panelSelectTarget.add(selectTarget);
        panelNorth.add(BorderLayout.SOUTH, panelSelectTarget);

        JPanel panelSouth = new JPanel(new GridLayout(0, 1));
        panelSouth.add(BorderLayout.NORTH, new JSeparator(SwingConstants.HORIZONTAL));
        JLabel information = new JLabel(t("observe.ui.choice.cancel.tip"), UIHelper.getUIManagerActionIcon("information"), SwingConstants.LEFT);
        information.setFont(information.getFont().deriveFont(Font.ITALIC).deriveFont(11f));
        panelSouth.add(BorderLayout.CENTER, information);

        panel.add(BorderLayout.NORTH, panelNorth);
        panel.add(BorderLayout.SOUTH, panelSouth);

        InputMap inputMap1 = panel.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap1.put((KeyStroke) selectTargetAction.getValue(Action.ACCELERATOR_KEY), "selectTarget");

        InputMap inputMap = panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put((KeyStroke) selectTargetAction.getValue(Action.ACCELERATOR_KEY), "selectTarget");
        inputMap.put((KeyStroke) applyAction.getValue(Action.ACCELERATOR_KEY), "apply");

        ActionMap actionMap = panel.getActionMap();
        actionMap.put("selectTarget", selectTargetAction);
        actionMap.put("apply", applyAction);

        optionPane = new JOptionPane(panel, JOptionPane.QUESTION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, options[0]) {
            @Override
            public void selectInitialValue() {

                JLayeredPane parent = (JLayeredPane) optionPane.getParent().getParent();

                InputMap inputMap = parent.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);

                inputMap.put(KeyStroke.getKeyStroke("pressed ENTER"), "none");
                inputMap.put(KeyStroke.getKeyStroke("ctrl pressed ENTER"), "none");
                if (editor.getCombobox().getModel().getSize() == 1) {
                    // auto-select unique data
                    editor.setSelectedItem(parentCandidates.get(0));
                }
                SwingUtilities.invokeLater(editor.getCombobox()::requestFocus);
            }
        };
        jButton = UsageUIHandlerSupport.findButton(optionPane, continueActionText);
        Objects.requireNonNull(jButton).setIcon(UIHelper.getUIManagerActionIcon("move"));
        jButton.setEnabled(false);
        editor.getModel().addPropertyChangeListener("selectedItem", evt -> jButton.setEnabled(evt.getNewValue() != null));
        int response = UIHelper.askUser(getMainUI(), optionPane, new Dimension(600, 350), t(dialogTitle), options);

        ToolkitIdLabel newParent = null;
        if (response == 0) {
            // will replace and delete
            newParent = editor.getModel().getSelectedItem();
            log.info(String.format("Selected parent id: %s", newParent));
        }
        return Optional.ofNullable(newParent);
    }

}
