package fr.ird.observe.client.datasource.editor.api.content.actions.move;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.navigation.id.IdNode;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * Created on 26/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.2
 */
public interface MoveRequestBuilderStepConfigure {

    MoveRequestBuilderStepConfigure setParentTargetDtoType(Class<? extends BusinessDto> parentTargetDtoType);

    MoveRequestBuilderStepConfigure setAskNewParentTitle(String askNewParentTitle);

    MoveRequestBuilderStepConfigure setAskNewParentMessage(String askNewParentMessage);

    MoveRequestBuilderStepConfigure setEditNode(IdNode<?> editNode);

    MoveRequestBuilderStepConfigure setGroupByValue(Supplier<DataGroupByParameter> groupByValue);

    MoveRequestBuilderStepBuild setParentCandidates(BiFunction<DataGroupByParameter, String, List<ToolkitIdLabel>> parentCandidates);
}
