package fr.ird.observe.client.datasource.editor.api.content.actions.move;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUIHandler;
import fr.ird.observe.client.datasource.editor.api.config.TreeStatistics;
import fr.ird.observe.client.datasource.editor.api.config.TreeStatisticsTemplate;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTreeModel;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationInitializer;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;
import fr.ird.observe.client.util.DisabledItemSelectionModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.navigation.id.CloseNodeVetoException;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.tree.GroupByHelper;
import fr.ird.observe.navigation.tree.TreeConfig;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.services.service.NavigationService;
import fr.ird.observe.services.service.data.MoveRequest;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 14/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
@SuppressWarnings("UnusedReturnValue")
public class MoveRootRequestBuilder implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(MoveRootRequestBuilder.class);
    /**
     * Dto type of data to move.
     */
    private final Class<? extends DataDto> dtoType;
    /**
     * Id of parent data to move.
     */
    private final ToolkitIdDtoBean oldParentId;
    /**
     * Data to move.
     */
    private final Collection<ToolkitIdLabel> data;
    /**
     * To get selected target request value.
     */
    private final JCheckBox selectTarget;
    /**
     * Select target action.
     */
    private final AbstractAction selectTargetAction;
    /**
     * Apply action.
     */
    private final AbstractAction applyAction;
    /**
     * Root configuration action.
     */
    private final AbstractAction configureAction;
    /**
     * Optional edit node.
     */
    private IdNode<?> editNode;
    /**
     * To get group by value.
     */
    private Supplier<DataGroupByParameter> groupByValueSupplier;
    /**
     * To get parent candidates.
     */
    private BiFunction<DataGroupByParameter, String, List<ToolkitIdLabel>> parentCandidates;
    /**
     * Reference type of parent target.
     */
    private Class<? extends BusinessDto> parentTargetDtoType;
    /**
     * Title of dialog to select new parent.
     */
    private String askNewParentTitle;
    /**
     * Message of dialog to select new parent.
     */
    private String askNewParentMessage;
    /**
     * Option pane apply button.
     */
    private JButton jButton;
    /**
     * Option pane.
     */
    private JOptionPane optionPane;
    private DataSourceEditor dataSourceEditor;
    private FilterableComboBox<ToolkitIdLabel> parentEditor;
    private JPanel groupByNamePanel;
    private JLabel groupByNameLabel;
    private GroupByHelper groupByHelper;
    private ToolkitTreeFlatModelRootRequest rootRequest;
    private JComboBox<DataGroupByDto<?>> groupByValuesEditor;
    private DataGroupByDtoSet<?, ?> dataGroupByDtoSet;
    private TreeConfig treeConfig;
    private String groupByParameterValue;

    static class BuilderImpl implements MoveRequestBuilderStepConfigure, MoveRequestBuilderStepBuild {
        private final MoveRootRequestBuilder builder;

        public BuilderImpl(MoveRootRequestBuilder builder) {
            this.builder = builder;
        }

        @Override
        public MoveRequestBuilderStepConfigure setEditNode(IdNode<?> editNode) {
            builder.setEditNode(editNode);
            return this;
        }

        @Override
        public MoveRequestBuilderStepBuild setParentCandidates(BiFunction<DataGroupByParameter, String, List<ToolkitIdLabel>> parentCandidates) {
            builder.setParentCandidates(parentCandidates);
            return this;
        }

        @Override
        public MoveRequestBuilderStepConfigure setGroupByValue(Supplier<DataGroupByParameter> groupByValue) {
            builder.setGroupByValueSupplier(groupByValue);
            return this;
        }

        @Override
        public MoveRequestBuilderStepConfigure setParentTargetDtoType(Class<? extends BusinessDto> parentTargetDtoType) {
            builder.setParentTargetDtoType(parentTargetDtoType);
            return this;
        }

        @Override
        public MoveRequestBuilderStepConfigure setAskNewParentTitle(String askNewParentTitle) {
            builder.setAskNewParentTitle(askNewParentTitle);
            return this;
        }

        @Override
        public MoveRequestBuilderStepConfigure setAskNewParentMessage(String askNewParentMessage) {
            builder.setAskNewParentMessage(askNewParentMessage);
            return this;
        }

        @Override
        public Optional<MoveRequest> build(DataSourceEditor dataSourceEditor) {
            return builder.build(dataSourceEditor);
        }
    }

    public static MoveRequestBuilderStepConfigure create(Class<? extends DataDto> dtoType, ToolkitIdDtoBean oldParentId, ToolkitIdLabel id) {
        return create(dtoType, oldParentId, Collections.singleton(id));
    }

    public static MoveRequestBuilderStepConfigure create(Class<? extends DataDto> dtoType, ToolkitIdDtoBean oldParentId, Collection<ToolkitIdLabel> data) {
        MoveRootRequestBuilder builder = new MoveRootRequestBuilder(dtoType, oldParentId, data);
        return new BuilderImpl(builder);
    }

    private MoveRootRequestBuilder(Class<? extends DataDto> dtoType, ToolkitIdDtoBean oldParentId, Collection<ToolkitIdLabel> data) {
        this.dtoType = Objects.requireNonNull(dtoType);
        this.oldParentId = Objects.requireNonNull(oldParentId);
        this.data = Objects.requireNonNull(data);
        KeyStroke keyStroke = ObserveKeyStrokesEditorApi.KEY_STROKE_SELECT_TARGET;
        String t = ObserveKeyStrokesEditorApi.suffixTextWithKeyStroke(t("observe.ui.move.selectTarget"), keyStroke);
        this.selectTarget = new JCheckBox(t);
        selectTargetAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectTarget.setSelected(!selectTarget.isSelected());
                log.info(String.format("select target? %s", selectTarget.isSelected()));
            }
        };
        selectTargetAction.putValue(Action.ACCELERATOR_KEY, keyStroke);
        selectTargetAction.putValue(Action.NAME, t);
        selectTarget.setFocusable(false);
        keyStroke = ObserveKeyStrokesEditorApi.KEY_STROKE_MOVE;
        t = ObserveKeyStrokesEditorApi.suffixTextWithKeyStroke(t("observe.ui.choice.confirm.move"), keyStroke);

        applyAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jButton.doClick();
            }
        };
        applyAction.putValue(Action.ACCELERATOR_KEY, keyStroke);
        applyAction.putValue(Action.NAME, t);
        keyStroke = ObserveKeyStrokesEditorApi.KEY_STROKE_INSERT_CONFIGURE;
        t = ObserveKeyStrokesEditorApi.suffixTextWithKeyStroke("", keyStroke);
        String tip = ObserveKeyStrokesEditorApi.suffixTextWithKeyStroke(t("observe.ui.tree.action.configure.tip"), keyStroke);
        configureAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doConfigure();
            }
        };
        configureAction.putValue(Action.LARGE_ICON_KEY, UIHelper.getUIManagerActionIcon("generate"));
        configureAction.putValue(Action.ACCELERATOR_KEY, keyStroke);
        configureAction.putValue(Action.NAME, t);
        configureAction.putValue(Action.SHORT_DESCRIPTION, tip);
    }

    private MoveRootRequestBuilder setEditNode(IdNode<?> editNode) {
        this.editNode = editNode;
        return this;
    }

    public MoveRootRequestBuilder setParentCandidates(BiFunction<DataGroupByParameter, String, List<ToolkitIdLabel>> parentCandidates) {
        this.parentCandidates = parentCandidates;
        return this;
    }

    public MoveRootRequestBuilder setGroupByValueSupplier(Supplier<DataGroupByParameter> groupByValueSupplier) {
        this.groupByValueSupplier = groupByValueSupplier;
        return this;
    }

    public MoveRootRequestBuilder setParentTargetDtoType(Class<? extends BusinessDto> parentTargetDtoType) {
        this.parentTargetDtoType = parentTargetDtoType;
        return this;
    }

    public MoveRootRequestBuilder setAskNewParentTitle(String askNewParentTitle) {
        this.askNewParentTitle = askNewParentTitle;
        return this;
    }

    public MoveRootRequestBuilder setAskNewParentMessage(String askNewParentMessage) {
        this.askNewParentMessage = askNewParentMessage;
        return this;
    }

    public Optional<MoveRequest> build(DataSourceEditor dataSourceEditor) {
        this.dataSourceEditor = dataSourceEditor;
        if (parentTargetDtoType == null) {
            parentTargetDtoType = oldParentId.getType();
        }
        if (askNewParentTitle == null) {
            askNewParentTitle = I18nDecoratorHelper.getPropertyI18nKey(dtoType, "action.move.choose.parent.title");
        }
        if (askNewParentMessage == null) {
            askNewParentMessage = I18nDecoratorHelper.getPropertyI18nKey(dtoType, "action.move.choose.parent.message");
        }

        DataGroupByParameter dataGroupByParameter = Objects.requireNonNull(this.groupByValueSupplier, "No groupByValue supplier set.").get();

        groupByParameterValue = dataGroupByParameter.getValue();

        NavigationTreeModel navigationTreeModel = dataSourceEditor.getNavigationUI().getTree().getModel();
        groupByHelper = navigationTreeModel.getGroupByHelper();

        RootNavigationInitializer initializer = navigationTreeModel.getRoot().getInitializer();
        rootRequest = initializer.getRequest();
        dataGroupByDtoSet = initializer.getGroupBy();
        treeConfig = new TreeConfig();
        treeConfig.init(navigationTreeModel.getConfig());

        List<ToolkitIdLabel> parentCandidates = Objects.requireNonNull(this.parentCandidates, "No parent candidates set.").apply(dataGroupByParameter, oldParentId.getId());
        ToolkitIdLabel newParentId = askNewParent(parentCandidates, askNewParentTitle, askNewParentMessage).orElse(null);
        if (newParentId == null) {
            return Optional.empty();
        }
        Set<String> ids = data.stream().map(ToolkitIdLabel::getId).collect(Collectors.toSet());
        if (editNode != null) {
            if (editNode.isEnabled() && ids.contains(editNode.getId())) {
                try {
                    ChangeMode.closeData(dataSourceEditor.getHandler(), dataSourceEditor, editNode);
                } catch (CloseNodeVetoException e) {
                    log.error("Could not close data from callback", e);
                    return Optional.empty();
                }
            }
        }
        return Optional.of(new MoveRequest(oldParentId, newParentId, ids, selectTarget.isSelected()));
    }

    private Optional<ToolkitIdLabel> askNewParent(List<ToolkitIdLabel> parentCandidates, String dialogTitle, String dialogMessage) {
        Decorator decorator = getDecoratorService().getToolkitIdLabelDecoratorByType(parentTargetDtoType);
        parentEditor = UIHelper.newToolkitIdLabelFilterableComboBox(parentTargetDtoType, decorator, parentCandidates);

        String continueActionText = (String) applyAction.getValue(Action.NAME);
        Object[] options = {continueActionText};
        JPanel panel = new JPanel(new BorderLayout(3, 3));

        JPanel panelMessages = new JPanel(new BorderLayout(3, 3));
        panelMessages.add(BorderLayout.NORTH, new JLabel(t(t("observe.ui.action.move.message", data.size(), I18nDecoratorHelper.getType(dtoType)))));

        JList<?> list = new JList<>(new Vector<>(data));
        list.setFocusable(false);
        list.setSelectionModel(new DisabledItemSelectionModel());
        list.setVisibleRowCount(5);
        panelMessages.add(BorderLayout.CENTER, new JScrollPane(list));

        groupByNamePanel = new JPanel(new BorderLayout());
        groupByNamePanel.setBorder(BorderFactory.createTitledBorder(/*BorderFactory.createLoweredBevelBorder(),*/ t("observe.Common.navigation.config.groupByName") + "      "));

        JToolBar toolBar = new JToolBar();
        toolBar.setBorderPainted(false);
        toolBar.setFloatable(false);
        toolBar.add(new JButton(configureAction));

        groupByNamePanel.add(BorderLayout.CENTER, groupByNameLabel = new JLabel(""));
        groupByNamePanel.add(BorderLayout.EAST, toolBar);
        groupByNamePanel.add(BorderLayout.SOUTH, groupByValuesEditor = new JComboBox<>());


        JPanel panelConfiguration = new JPanel(new BorderLayout());
        parentEditor.setBorder(BorderFactory.createTitledBorder(/*BorderFactory.createLoweredBevelBorder(),*/t(dialogMessage) + "      "));

        panelConfiguration.add(BorderLayout.CENTER, groupByNamePanel);
        panelConfiguration.add(BorderLayout.SOUTH, parentEditor);

        updateConfigurationLabel();

        updateGroupByValues();

        groupByValuesEditor.addItemListener(e -> {
            if (e.getStateChange() != ItemEvent.SELECTED) {
                return;
            }
            DataGroupByDto<?> newGroupByValue = (DataGroupByDto<?>) e.getItem();
            onGroupByValueChanged(newGroupByValue);
        });

        JPanel panelSelectTarget = new JPanel(new GridLayout(0, 1));
        //FIXME:Move Add a default client configuration option to set this value
        selectTarget.setSelected(true);
        panelSelectTarget.add(new JLabel());
        panelSelectTarget.add(selectTarget);

        JPanel panelSouth = new JPanel(new GridLayout(0, 1));
        panelSouth.add(BorderLayout.NORTH, new JSeparator(SwingConstants.HORIZONTAL));
        JLabel information = new JLabel(t("observe.ui.choice.cancel.tip"), UIHelper.getUIManagerActionIcon("information"), SwingConstants.LEFT);
        information.setFont(information.getFont().deriveFont(Font.ITALIC).deriveFont(11f));
        panelSouth.add(BorderLayout.CENTER, information);

        JPanel panelNorth = new JPanel(new BorderLayout());
        panelNorth.add(BorderLayout.NORTH, panelMessages);
        panelNorth.add(BorderLayout.CENTER, panelConfiguration);
        panelNorth.add(BorderLayout.SOUTH, panelSelectTarget);

        panel.add(BorderLayout.NORTH, panelNorth);
        panel.add(BorderLayout.SOUTH, panelSouth);

        InputMap inputMap1 = panel.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap1.put((KeyStroke) selectTargetAction.getValue(Action.ACCELERATOR_KEY), "selectTarget");
        inputMap1.put((KeyStroke) configureAction.getValue(Action.ACCELERATOR_KEY), "configure");

        InputMap inputMap = panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put((KeyStroke) selectTargetAction.getValue(Action.ACCELERATOR_KEY), "selectTarget");
        inputMap.put((KeyStroke) applyAction.getValue(Action.ACCELERATOR_KEY), "apply");
        inputMap.put((KeyStroke) configureAction.getValue(Action.ACCELERATOR_KEY), "configure");

        ActionMap actionMap = panel.getActionMap();
        actionMap.put("selectTarget", selectTargetAction);
        actionMap.put("apply", applyAction);
        actionMap.put("configure", configureAction);

        optionPane = new JOptionPane(panel, JOptionPane.QUESTION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, options[0]) {
            @Override
            public void selectInitialValue() {

                JLayeredPane parent = (JLayeredPane) optionPane.getParent().getParent();

                InputMap inputMap = parent.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);

                inputMap.put(KeyStroke.getKeyStroke("pressed ENTER"), "none");
                inputMap.put(KeyStroke.getKeyStroke("ctrl pressed ENTER"), "none");
                if (parentEditor.getCombobox().getModel().getSize() == 1) {
                    // auto-select unique data
                    parentEditor.setSelectedItem(parentCandidates.get(0));
                }
                SwingUtilities.invokeLater(parentEditor.getCombobox()::requestFocus);
            }
        };
        optionPane.setComponentPopupMenu(new JPopupMenu());
        jButton = UsageUIHandlerSupport.findButton(optionPane, continueActionText);
        Objects.requireNonNull(jButton).setIcon(UIHelper.getUIManagerActionIcon("move"));
        jButton.setEnabled(false);
        parentEditor.getModel().addPropertyChangeListener("selectedItem", evt -> jButton.setEnabled(canMove(evt.getNewValue())));
        int response = UIHelper.askUser(getMainUI(), optionPane, new Dimension(600, 350), t(dialogTitle), options);

        ToolkitIdLabel newParent = null;
        if (response == 0) {
            // will replace and delete
            newParent = parentEditor.getModel().getSelectedItem();
            log.info(String.format("Selected parent id: %s", newParent));
        }
        return Optional.ofNullable(newParent);
    }

    private void onGroupByValueChanged(DataGroupByDto<?> newGroupByValue) {
        DataGroupByParameter newGroupBy = new DataGroupByParameter(rootRequest.getGroupByName(), rootRequest.getGroupByFlavor(), newGroupByValue.getFilterValue());

        List<ToolkitIdLabel> newParentCandidates = parentCandidates.apply(newGroupBy, oldParentId.getId());
        ToolkitIdLabel selectedItem = parentEditor.getModel().getSelectedItem();
        parentEditor.setData(newParentCandidates);
        parentEditor.setSelectedItem(newParentCandidates.contains(selectedItem) ? selectedItem : null);

        if (parentEditor.getCombobox().getModel().getSize() == 1) {
            // auto-select unique data
            parentEditor.setSelectedItem(newParentCandidates.get(0));
        }
        SwingUtilities.invokeLater(parentEditor.getCombobox()::requestFocus);
    }

    private void doConfigure() {
        SwingUtilities.invokeLater(() -> {
            NavigationUI ui = dataSourceEditor.getNavigationUI();

            TreeConfigUI configUI = TreeConfigUIHandler.createUI(ui,
                                                                 treeConfig,
                                                                 ui.getTree().getModel().getGroupByHelper(),
                                                                 new MoveRootApplyNavigationConfiguration(this),
                                                                 ui.getTree());

            TreeConfigUIHandler.hideOptions(configUI);
            TreeConfigUIHandler.hideModule(configUI);
            configUI.getHandler().updateOptions();
            configUI.setComponentPopupMenu(optionPane.getComponentPopupMenu());
            SwingUtilities.invokeLater(() -> TreeConfigUIHandler.showPanel(groupByNamePanel, configUI));
        });
    }

    public void updateConfiguration(TreeConfig bean) {
        treeConfig = bean;
        rootRequest = bean.toRootRequest(groupByHelper);
        log.warn(String.format("Will use new request: %s", rootRequest));

        updateConfigurationLabel();

        NavigationService navigationService = getDataSourcesManager().getMainDataSource().getNavigationService();
        dataGroupByDtoSet = navigationService.getGroupByDtoSet(rootRequest);
        getDecoratorService().installDecorator(dataGroupByDtoSet);

        groupByParameterValue = dataGroupByDtoSet.size() == 0 ? null : dataGroupByDtoSet.toList().get(0).getFilterValue();
        updateGroupByValues();
        SwingUtilities.invokeLater(() -> {
            JDialog dialog = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, optionPane);
            dialog.pack();
        });
    }

    private void updateConfigurationLabel() {
        TreeStatistics statistics = new TreeStatistics(
                rootRequest,
                groupByHelper,
                () -> 0,
                () -> 0L,
                () -> 0L);
        String statisticsText = TreeStatisticsTemplate.generateTreeStatisticsText(statistics);
        groupByNameLabel.setText(statisticsText.substring(0, statisticsText.lastIndexOf(" /")));

    }

    private void updateGroupByValues() {

        @SuppressWarnings("unchecked") List<DataGroupByDto<?>> groupByValues = (List<DataGroupByDto<?>>) dataGroupByDtoSet.toList();

        DataGroupByDto<?> selectedGroupByValue = groupByParameterValue == null ? null : dataGroupByDtoSet.tryGetReferenceById(groupByParameterValue).orElse(null);

        DefaultComboBoxModel<DataGroupByDto<?>> model = (DefaultComboBoxModel<DataGroupByDto<?>>) groupByValuesEditor.getModel();
        model.removeAllElements();
        model.addAll(groupByValues);
        model.setSelectedItem(selectedGroupByValue != null ? selectedGroupByValue : groupByValues.isEmpty() ? null : groupByValues.get(0));
    }

    private boolean canMove(Object selectedItem) {
        return selectedItem != null;
    }
}
