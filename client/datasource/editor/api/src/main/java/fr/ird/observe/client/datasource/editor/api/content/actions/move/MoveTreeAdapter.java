package fr.ird.observe.client.datasource.editor.api.content.actions.move;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.services.service.data.MoveRequest;

import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

/**
 * Created on 12/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public abstract class MoveTreeAdapter<OldParent extends NavigationNode, NewParent extends NavigationNode, NewNode extends NavigationNode> {

    private final NavigationNode incomingNode;
    private final Function<NavigationNode, OldParent> oldParentSupplier;

    protected MoveTreeAdapter(NavigationNode incomingNode, Function<NavigationNode, OldParent> oldParentSupplier) {
        this.incomingNode = Objects.requireNonNull(incomingNode);
        this.oldParentSupplier = Objects.requireNonNull(oldParentSupplier);
    }

    public abstract NewParent getNewParentNode(OldParent oldParentNode, String newParentId);

    public abstract NewNode getNewSelectedNodeFromNewParentNode(NewParent newParentNode, String newId);

    public final void adaptTree(MoveRequest request, NavigationTree tree, Set<String> newIds, String oldGroupByValue) {
        Set<String> ids = request.getIds();
        boolean selectTarget = request.isSelectTarget();
        boolean multiple = !request.isSingle();
        OldParent oldParentNode = getOldParentNode(getIncomingNode());
        String newParentId = request.getNewParentId().getId();
        if (request.isNewParentIsRootType()) {
            NavigationTreeConfig config = tree.getModel().getConfig();
            String groupByValue = oldParentNode.getContext().getServicesProvider().getRootOpenableService().getGroupByValue(config.getGroupByName(), config.getGroupByFlavor(), newParentId);
            if (!Objects.equals(oldGroupByValue, groupByValue)) {
                // the navigation must be updated, new parent groupBy value has changed
                String[] previousPaths = oldParentNode.toPaths();
                tree.getModel().updateNavigationResult();
                // update the old parent parent node
                ContentRootListUINavigationNode oldParentNodeParent = (ContentRootListUINavigationNode) oldParentNode.upToGroupByValue();
                oldParentNodeParent.updateNode();
                // get the new old parent node
                oldParentNode = (OldParent) tree.getModel().getNodeFromPath(previousPaths, false);
                // need also to update the new parent old parent node
                ContentRootListUINavigationNode newParentOldParentNode = (ContentRootListUINavigationNode) tree.getModel().getNodeFromPath(new String[]{oldGroupByValue}, false);
                if (newParentOldParentNode != null) {
                    // only update it if it still exist (the node may have been removed from navigation tree)
                    newParentOldParentNode.updateNode();
                    if (!config.isLoadEmptyGroupBy() && newParentOldParentNode.getChildCount() == 0) {
                        // the configuration requires that node to be removed
                        newParentOldParentNode.removeFromParent();
                    }
                }
            }
        }
        NewParent newParentNode = getNewParentNode(oldParentNode, newParentId);

        // kep old node paths
        String[] oldNodePaths = oldParentNode.toPaths();
        NavigationNode nodeToSelect = adaptNewParentNode(tree, newParentNode, multiple || !selectTarget ? null : newIds.iterator().next());
        if (!selectTarget) {
            // stay on old parent node
            nodeToSelect = tree.getModel().getNodeFromPath(oldNodePaths, false);
        }
        adaptOldParentNode(oldParentNode, ids);
        doFinalSelect(selectTarget, tree, nodeToSelect);
    }

    public final OldParent getOldParentNode(NavigationNode incomingNode) {
        return oldParentSupplier.apply(incomingNode);
    }

    public final void adaptOldParentNode(OldParent oldParentNode, Set<String> ids) {
        // reload node
        oldParentNode.reloadNodeDataToRootAndChildren();
    }

    public final NavigationNode adaptNewParentNode(NavigationTree tree, NewParent newParentNode, String newId) {
        // reload node
        String[] paths = newParentNode.toPaths();
        ContentRootListUINavigationNode newParentOldParentNode = (ContentRootListUINavigationNode) newParentNode.upToGroupByValue();
        newParentOldParentNode.updateNode();
        newParentNode = (NewParent) tree.getModel().getNodeFromPath(paths, false);
        if (newId == null) {
            return newParentNode;
        }
        // get new node
        return getNewSelectedNodeFromNewParentNode(newParentNode, newId);
    }

    public final void doFinalSelect(boolean selectTarget, NavigationTree tree, NavigationNode nodeToSelect) {
        if (selectTarget) {
            // select new node
            tree.selectSafeNode(nodeToSelect);
        } else {
            if (nodeToSelect.getParent() == null) {
                // limit case: node to select is old parent node and was removed
                tree.selectFirstNode();
            } else {
                // re-select the node (to update associated form)
                tree.reSelectSafeNode(nodeToSelect);
            }
        }
    }

    public NavigationNode getIncomingNode() {
        return incomingNode;
    }

}
