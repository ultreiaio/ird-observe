package fr.ird.observe.client.datasource.editor.api.content.actions.move.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUI;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.services.service.data.MoveLayoutRequest;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.AbstractButton;
import java.awt.event.ActionEvent;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * To move a data.
 * <p>
 * Created on 12/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public class MoveLayoutAction<D extends DataDto, U extends ContentUI> extends ContentUIActionSupport<U> implements ConfigureMenuAction<U> {

    /**
     * To execute the move action.
     */
    private final MoveLayoutExecutor executor;

    public static class Builder<D extends DataDto, U extends ContentUI> implements AddOnStep<D, U>, AddCallStep<D, U>, AddThenStep<D, U>, BuildStep<D, U> {

        private final U ui;
        private final Class<D> dtoType;
        private Supplier<MoveLayoutRequestBuilderStepBuild> requestBuilderSupplier;
        private Consumer<MoveLayoutRequest> requestConsumer;
        private Function<MoveLayoutRequest, ? extends MoveLayoutTreeAdapter<?, ?>> treeAdapter;

        public Builder(U ui, Class<D> dtoType) {
            this.ui = Objects.requireNonNull(ui);
            this.dtoType = Objects.requireNonNull(dtoType);
        }

        @Override
        public AddCallStep<D, U> on(Supplier<MoveLayoutRequestBuilderStepBuild> requestBuilderSupplier) {
            this.requestBuilderSupplier = Objects.requireNonNull(requestBuilderSupplier);
            return this;
        }

        @Override
        public AddThenStep<D, U> call(Consumer<MoveLayoutRequest> requestConsumer) {
            this.requestConsumer = Objects.requireNonNull(requestConsumer);
            return this;
        }

        @Override
        public BuildStep<D, U> then(Function<U, Function<MoveLayoutRequest, ? extends MoveLayoutTreeAdapter<?, ?>>> treeAdapter) {
            this.treeAdapter = Objects.requireNonNull(treeAdapter).apply(ui);
            return this;
        }

        @Override
        public MoveLayoutAction<D, U> install(Supplier<? extends AbstractButton> editor) {
            MoveLayoutExecutor moveExecutor = new MoveLayoutExecutor(requestBuilderSupplier, requestConsumer, treeAdapter);
            MoveLayoutAction<D, U> action = new MoveLayoutAction<>(dtoType, moveExecutor);
            init(ui, editor.get(), action);
            return action;
        }
    }

    public static <D extends DataDto, C extends ContainerChildDto, U extends ContentTableUI<D, C, U>> AddOnStep<D, U> create(U ui) {
        return new Builder<>(ui, ui.getModel().getSource().getScope().getMainType());
    }

    public static <D extends RootOpenableDto, U extends ContentRootOpenableUI<D, U>> AddOnStep<D, U> create(U ui) {
        return new Builder<>(ui, ui.getModel().getSource().getScope().getMainType());
    }

    public static <D extends SimpleDto, U extends ContentLayoutUI<D, U>> AddOnStep<D, U> create(U ui) {
        return new Builder<>(ui, ui.getModel().getSource().getScope().getMainType());
    }

    public static <D extends DataDto, U extends ContentUI> AddOnStep<D, U> create(U ui, Class<D> dtoType) {
        return new Builder<>(ui, dtoType);
    }

    protected MoveLayoutAction(Class<D> dataType, MoveLayoutExecutor executor) {
        super(null, null, "move", ObserveKeyStrokesEditorApi.KEY_STROKE_MOVE_LAYOUT);
        this.executor = Objects.requireNonNull(executor);
        setText(I18n.t(I18nDecoratorHelper.getPropertyI18nKey(dataType, "action.move.all")));
        setTooltipText(I18n.t(I18nDecoratorHelper.getPropertyI18nKey(dataType, "action.move.all")));
    }

    @Override
    protected final void doActionPerformed(ActionEvent e, U ui) {
        executor.execute(getActionExecutor(), getDataSourceEditor());
    }

    public interface AddOnStep<D extends DataDto, U extends ContentUI> {
        AddCallStep<D, U> on(Supplier<MoveLayoutRequestBuilderStepBuild> requestBuilderSupplier);
    }

    public interface AddCallStep<D extends DataDto, U extends ContentUI> {
        AddThenStep<D, U> call(Consumer<MoveLayoutRequest> requestConsumer);
    }

    public interface AddThenStep<D extends DataDto, U extends ContentUI> {
        BuildStep<D, U> then(Function<U, Function<MoveLayoutRequest, ? extends MoveLayoutTreeAdapter<?, ?>>> treeAdapter);
    }

    public interface BuildStep<D extends DataDto, U extends ContentUI> {
        MoveLayoutAction<D, U> install(Supplier<? extends AbstractButton> editor);
    }
}
