package fr.ird.observe.client.datasource.editor.api.content.actions.move.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.services.service.data.MoveLayoutRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.application.ActionExecutor;

import javax.swing.SwingUtilities;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 14/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class MoveLayoutExecutor {

    private static final Logger log = LogManager.getLogger(MoveLayoutExecutor.class);
    /**
     * To build the request.
     */
    private final Supplier<MoveLayoutRequestBuilderStepBuild> requestBuilderSupplier;
    /**
     * To consume request.
     */
    private final Consumer<MoveLayoutRequest> requestConsumer;
    /**
     * To adapt tree.
     */
    private final Function<MoveLayoutRequest, ? extends MoveLayoutTreeAdapter<?, ?>> treeAdapter;

    public MoveLayoutExecutor(Supplier<MoveLayoutRequestBuilderStepBuild> requestBuilderSupplier, Consumer<MoveLayoutRequest> requestConsumer, Function<MoveLayoutRequest, ? extends MoveLayoutTreeAdapter<?, ?>> treeAdapter) {
        this.requestBuilderSupplier = Objects.requireNonNull(requestBuilderSupplier);
        this.requestConsumer = Objects.requireNonNull(requestConsumer);
        this.treeAdapter = Objects.requireNonNull(treeAdapter);
    }

    public void execute(ActionExecutor actionExecutor, DataSourceEditor dataSourceEditor) {
        Optional<MoveLayoutRequest> optionalRequest = getRequest(dataSourceEditor);
        if (optionalRequest.isPresent()) {
            MoveLayoutRequest request = optionalRequest.get();
            String label = t("observe.ui.choice.confirm.move");
            actionExecutor.addAction(label, () -> consume(request, dataSourceEditor.getNavigationUI().getTree()));
            return;
        }
        log.warn("User cancel action.");
    }

    protected Optional<MoveLayoutRequest> getRequest(DataSourceEditor dataSourceEditor) {
        return Optional.ofNullable(requestBuilderSupplier.get()).map(b -> b.build(dataSourceEditor)).flatMap(Function.identity());
    }

    protected void consume(MoveLayoutRequest request, NavigationTree tree) {
        String newParentId = request.getNewParentId();
        String oldParentId = request.getOldParentId();
        log.info(String.format("Move adapt tree [start] from parent: %s to %s", oldParentId, newParentId));

        NavigationTreeConfig config = tree.getModel().getConfig();
        String oldGroupByValue = tree.getModel().getDataSourcesManager().getMainDataSource().getRootOpenableService().getGroupByValue(config.getGroupByName(), config.getGroupByFlavor(), newParentId);

        requestConsumer.accept(request);

        MoveLayoutTreeAdapter<?, ?> moveTreeAdapter = treeAdapter.apply(request);
        try {
            SwingUtilities.invokeAndWait(() -> moveTreeAdapter.adaptTree(request, tree, oldGroupByValue));
        } catch (InterruptedException | InvocationTargetException e) {
            throw new ObserveSwingTechnicalException(e.getCause());
        }
        log.info(String.format("Move adapt tree [end..] from parent: %s to %s", oldParentId, newParentId));
    }
}
