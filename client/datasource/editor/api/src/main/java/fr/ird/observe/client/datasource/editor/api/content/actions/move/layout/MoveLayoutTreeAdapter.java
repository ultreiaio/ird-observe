package fr.ird.observe.client.datasource.editor.api.content.actions.move.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.services.service.data.MoveLayoutRequest;

import javax.swing.SwingUtilities;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 01/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class MoveLayoutTreeAdapter<ParentNode extends ContentRootOpenableUINavigationNode, SourceNode extends NavigationNode> {

    private final Supplier<ParentNode> oldParentSupplier;

    protected MoveLayoutTreeAdapter(Supplier<ParentNode> oldParentSupplier) {
        this.oldParentSupplier = Objects.requireNonNull(oldParentSupplier);
    }

    public abstract SourceNode getNodeToSelect(ParentNode parentNode);

    public final void adaptTree(MoveLayoutRequest request, NavigationTree tree, String oldGroupByValue) {
        boolean selectTarget = request.isSelectTarget();
        ParentNode oldParentNode = oldParentSupplier.get();
        NavigationTreeConfig config = tree.getModel().getConfig();
        String newParentId = request.getNewParentId();
        String groupByValue = oldParentNode.getContext().getServicesProvider().getRootOpenableService().getGroupByValue(config.getGroupByName(), config.getGroupByFlavor(), newParentId);
        if (!Objects.equals(oldGroupByValue, groupByValue)) {
            // the navigation must be updated, new parent groupBy value has changed
            tree.getModel().updateNavigationResult();
            // update the old parent parent node
            ContentRootListUINavigationNode oldParentNodeParent = oldParentNode.getParent();
            oldParentNodeParent.updateNode();
            // get the new old parent node
            oldParentNode = (ParentNode) oldParentNodeParent.findChildById(request.getOldParentId());

            // need also to update the new parent old parent node
            ContentRootListUINavigationNode newParentOldParentNode = (ContentRootListUINavigationNode) tree.getModel().getNodeFromPath(new String[]{oldGroupByValue}, false);
            if (newParentOldParentNode != null) {
                // only update it if it still exist (the node may have been removed from navigation tree)
                newParentOldParentNode.updateNode();
                if (!config.isLoadEmptyGroupBy() && newParentOldParentNode.getChildCount() == 0) {
                    // the configuration requires that node to be removed
                    newParentOldParentNode.removeFromParent();
                }
            }
        } else {

            oldParentNode.updateNode();
        }
        @SuppressWarnings("unchecked") ParentNode newParentNode = (ParentNode) tree.getModel().getNodeFromPath(new String[]{groupByValue, newParentId}, true);
        newParentNode.updateNode();
        if (selectTarget) {
            NavigationNode nodeToSelect = getNodeToSelect(newParentNode);
            tree.selectSafeNode(nodeToSelect);
        } else {
            NavigationNode nodeToSelect = getNodeToSelect(oldParentNode);
            tree.reSelectSafeNode(nodeToSelect);
        }
        SwingUtilities.invokeLater(tree::repaint);
    }

}
