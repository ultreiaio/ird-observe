package fr.ird.observe.client.datasource.editor.api.content.actions.open;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.NotStandaloneContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIModel;
import fr.ird.observe.client.datasource.editor.api.content.ui.format.NauticalLengthFormatChangeListener;
import fr.ird.observe.client.datasource.editor.api.content.ui.format.TemperatureFormatChangeListener;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ContainerCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.NodeCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ReferenceCapability;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializer;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.JVetoableTabbedPane;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;
import org.nuiton.jaxx.validator.swing.tab.JTabbedPaneValidator;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditorModel;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditor;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import java.awt.event.InputEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

import static fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler.removeAllMessages;
import static fr.ird.observe.client.datasource.editor.api.content.actions.InsertMenuAction.PROPERTY_NAME_UPDATING_MODE_AND_NOT_MODIFIED;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 27/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
@SuppressWarnings("unused")
public class ContentOpen<U extends ContentUI> {
    private static final Logger log = LogManager.getLogger(ContentOpen.class);
    private final ContentOpenExecutor<U> contentOpenExecutor;
    private final ContentEditExecutor<U> contentEditExecutor;
    private final PropertyChangeListener onCoordinateFormatChangedListener;
    protected final U ui;
    private final Map<ContentTableUI<?, ?, ?>, JPanel> subUiMap = new LinkedHashMap<>();
    private boolean toggleDateTimeEditorSliderIsChanging;
    private boolean toggleTimeEditorSliderIsChanging;
    private boolean coordinateFormatChangedIsChanging;
    protected List<CoordinatesEditor> coordinateEditors;
    protected List<TemperatureEditor> temperatureEditors;
    protected List<NauticalLengthEditor> nauticalLengthEditors;
    protected JTabbedPaneValidator tabbedPaneValidator;
    protected JTabbedPaneValidator subTabbedPaneValidator;
    private ArrayListMultimap<String, JComponent> focusComponents;
    private final ArrayListMultimap<ContentUI, JComponent> configureActions;
    private List<JTable> tables;
    private ActionMap newActionMap;
    private InputMap newInputMap;
    private List<FilterableComboBox<?>> comboBoxes;
    private JTabbedPane mainTabbedPane;

    public ContentOpen(U ui, ContentOpenExecutor<U> contentOpenExecutor) {
        this(ui, contentOpenExecutor, null);
    }

    public ContentOpen(U ui, ContentOpenExecutor<U> contentOpenExecutor, ContentEditExecutor<U> contentEditExecutor) {
        this.ui = ui;
        this.contentOpenExecutor = Objects.requireNonNull(contentOpenExecutor);
        this.contentEditExecutor = contentEditExecutor;
        this.onCoordinateFormatChangedListener = evt -> {
            CoordinateFormat newValue = (CoordinateFormat) evt.getNewValue();
            onCoordinateFormatChanged(newValue);
        };
        configureActions = ArrayListMultimap.create();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public DefaultUIInitializerResult init(ContentUIInitializer<U> initializer) {
        DefaultUIInitializerResult initializerResult = initializer.initUI();
        tables = initializerResult.getComponentsList(JTable.class);
        comboBoxes = (List) initializerResult.getComponentsList(FilterableComboBox.class);
        tabbedPaneValidator = initializerResult.getTabbedPaneValidator();
        subTabbedPaneValidator = initializerResult.getSubTabbedPaneValidator();
        coordinateEditors = initializerResult.getCoordinateEditors();
        temperatureEditors = initializerResult.getTemperatureEditors();
        nauticalLengthEditors = initializerResult.getNauticalLengthEditors();
        focusComponents = initializerResult.getFocusComponents();
        coordinateEditors.forEach(e -> e.getModel().addPropertyChangeListener(CoordinatesEditorModel.PROPERTY_FORMAT, onCoordinateFormatChangedListener));
        NauticalLengthFormatChangeListener.install(ui);
        TemperatureFormatChangeListener.install(ui);
        ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).setParent(null);
        ui.getActionMap().setParent(null);
        JComponent actionContainer = initializer.getActionContainer();
        initActions(initializerResult);
        ContentUIModel model = ui.getModel();

        @SuppressWarnings("unchecked") ContentUIHandler<U> handler = (ContentUIHandler<U>) ui.getHandler();
        model.init(ui, initializerResult);
        handler.onInit(ui);

        postInstallActions(actionContainer, initializerResult);

        model.getStates().addPropertyChangeListener(ContentUIModelStates.PROPERTY_MODE, evt -> {
            ContentMode newValue = (ContentMode) evt.getNewValue();
            ui.getHandler().onModeChanged(newValue);
        });
        if (tabbedPaneValidator != null) {
            mainTabbedPane = (JTabbedPane) ui.getObjectById(DefaultUIInitializer.MAIN_TABBED_PANE);
            if (mainTabbedPane != null) {
                if (mainTabbedPane instanceof JVetoableTabbedPane) {
                    JVetoableTabbedPane tabbedPane = (JVetoableTabbedPane) mainTabbedPane;
                    tabbedPane.addChangeListener(l -> {
                        JVetoableTabbedPane source = (JVetoableTabbedPane) l.getSource();
                        ui.getHandler().onMainTabChanged(source.getPreviousIndex(), source.getSelectedIndex());
                    });
                } else {
                    mainTabbedPane.addChangeListener(new ChangeListener() {
                        int previousIndex = -1;

                        @Override
                        public void stateChanged(ChangeEvent l) {
                            JTabbedPane source = (JTabbedPane) l.getSource();
                            int selectedIndex = source.getSelectedIndex();
                            ui.getHandler().onMainTabChanged(previousIndex, selectedIndex);
                            previousIndex = selectedIndex;
                        }
                    });
                }
            }
        }
        return initializerResult;
    }



    public void doOpen() {
        String prefix = ui.getModel().getPrefix();
        log.info(String.format("%s Loading content...", prefix));
        contentOpenExecutor.loadContent(ui);
        if (mainTabbedPane != null) {
            int selectedIndex = mainTabbedPane.getSelectedIndex();
            ui.getHandler().onMainTabChanged(-1, selectedIndex);
        }
        log.info(String.format("%s Content loaded.", prefix));
        ui.getStates().setOpened(true);
        if (contentEditExecutor != null) {
            contentEditExecutor.doEditOnOpen(ui);
        }
    }

    public void doOpened() {
        String prefix = ui.getModel().getPrefix();
        log.info(String.format("%s Do Opened...", prefix));
        contentOpenExecutor.onOpened(ui);
        log.info(String.format("%s Do opened done.", prefix));
    }

    public boolean doClose() {
        String prefix = ui.getModel().getPrefix();
        log.info(String.format("%s Closing content...", prefix));
        Close close = tryToClose();
        ui.getStates().setClosing(true);
        try {
            boolean canQuit = applyBeforeSave(close);
            if (!canQuit) {
                log.info(String.format("%s Closing content was canceled.", prefix));
                return false;
            }
            closeSafeUI();
            log.info(String.format("%s Content closed.", prefix));
            ui.getStates().setOpened(false);
        } finally {
            ui.getStates().setClosing(false);
        }
        return true;
    }

    public void doStartEdit() {
        if (contentEditExecutor != null) {
            log.info(String.format("%s Content start edit.", ui.getModel().getPrefix()));
            if (!(ui instanceof NotStandaloneContentUI)) {
                removeAllMessages(ui);
            }
            contentEditExecutor.startEdit(ui);
        }
    }

    public void doStopEdit() {
        if (contentEditExecutor != null) {
            log.info(String.format("%s Content stop edit.", ui.getModel().getPrefix()));
            contentEditExecutor.stopEdit(ui);
            removeAllMessages(ui);
        }
    }

    public final void restartEditUI() {

        ContentUIModel model = ui.getModel();

        if (!model.getStates().isEditable()) {
            // l'écran n'est pas éditable, on ne re-ouvre pas on quite de suite
            return;
        }
        log.info(String.format("%sWill restart edit.", model.getPrefix()));

        // on ne peut redémarrer une edition que si la donnee
        // est exactement une entité (pas possible sur une liste)
        NavigationTree tree = ui.getHandler().getNavigationTree();

        removeAllMessages(ui);

        tree.getSelectedNode().nodeChanged(false, true);

        model.getStates().setMode(ContentMode.UPDATE);

        // restart edit
        ui.startEdit();

//        //FIXME:Focus forcing!!!
//        ui.getHandler().grabFocusOnForm();
    }

    public void resetFromPreviousUi(U ui) {
        if (tabbedPaneValidator != null) {
            if (mainTabbedPane != null) {
                JTabbedPane oldMainTabbedPane = (JTabbedPane) ui.getObjectById(DefaultUIInitializer.MAIN_TABBED_PANE);
                SwingUtilities.invokeLater(() -> mainTabbedPane.setSelectedIndex((oldMainTabbedPane).getSelectedIndex()));
            }
        }
    }

    public boolean applyBeforeSave(Close close) {
        boolean needStop;
        boolean canQuit;
        switch (close) {
            case SAVE:
                ui.saveEdit();
                needStop = true;
                canQuit = true;
                break;
            case RESET:
                needStop = true;
                canQuit = true;
                break;
            case CLOSE:
                canQuit = true;
                needStop = false;
                break;
            default:
                canQuit = false;
                needStop = false;
        }
        if (!canQuit) {
            return false;
        }
        if (needStop && contentEditExecutor != null) {
            contentEditExecutor.stopEdit(ui);
            if (ui.getModel().getStates().isEditing()) {
                log.info(String.format("%s Closing cancel (could not stop editing).", ui.getModel().getPrefix()));
                return false;
            }
        }
        return true;
    }

    public Close tryToClose() {
        String prefix = ui.getModel().getPrefix();
        log.info(String.format("%s Try to close content...", prefix));
        ContentUIModelStates states = ui.getModel().getStates();
        Boolean canQuit = states.canQuit();
        if (canQuit == null) {
            // cancel
            return Close.CANCEL;
        }
        if (canQuit) {
            // simple close
            return Close.CLOSE;
        }
        // ask user what to do
        if (states.isValid()) {
            // ask user if wants to save
            int response = UIHelper.askUser(
                    ui,
                    t("observe.ui.title.need.confirm"),
                    t("observe.ui.message.quit.valid.edit"),
                    JOptionPane.WARNING_MESSAGE,
                    new Object[]{
                            t("observe.ui.choice.save"),
                            t("observe.ui.choice.doNotSave"),
                            t("observe.ui.choice.cancel")},
                    0);
            log.debug("response : " + response);
            switch (response) {
                case 0:
                    return Close.SAVE;
                case 1:
                    return Close.RESET;
                default:
                    return Close.CANCEL;
            }
        } else {
            // ask user if wants to quit without saving since edit is not valid
            int response = UIHelper.askUser(
                    ui,
                    t("observe.ui.title.need.confirm"),
                    t("observe.ui.message.quit.invalid.edit"),
                    JOptionPane.ERROR_MESSAGE,
                    new Object[]{
                            t("observe.ui.choice.continue"),
                            t("observe.ui.choice.cancel")},
                    0);
            log.debug("response : " + response);
            if (response == 0) {
                return Close.RESET;
            }
            return Close.CANCEL;
        }
    }

    public final void closeSafeUI() {
        ContentUIHandler<?> handler = ui.getHandler();
        String prefix = ui.getModel().getPrefix();

        log.info(String.format("%sClosing safely ui", prefix));
        if (tabbedPaneValidator != null) {
            tabbedPaneValidator.uninstall(ui.getErrorTableModel());
        }
        if (subTabbedPaneValidator != null) {
            subTabbedPaneValidator.uninstall(ui.getErrorTableModel());
        }
        coordinateEditors.forEach(e -> e.getModel().removePropertyChangeListener(CoordinatesEditorModel.PROPERTY_FORMAT, onCoordinateFormatChangedListener));
        NauticalLengthFormatChangeListener.uninstall(ui);
        TemperatureFormatChangeListener.uninstall(ui);
        //FIXME:Focus switch focus to navigation
//        handler.uninstallPermanentFocusOwnerListener();

        removeAllMessages(ui);
        ContentUIModel model = ui.getModel();
        ui.getErrorTableModel().clearValidators();
        boolean editable = model.getStates().isEditable();
        model.close();
        NavigationTree tree = handler.getNavigationTree();
        NavigationNode node = tree.getSelectedNode();
        if (editable && node.isReference()) {
            ReferenceCapability<?> capability = (ReferenceCapability<?>) node.getCapability();
            if (capability.getReference().isNotPersisted()) {
                NavigationNode parentNode = node.getParent();
                if (parentNode != null) {
                    // node still attached, so remove it
                    log.info(String.format("%s Remove not persisted node: %s", prefix, node));
                    node.removeFromParent();
                    tree.reloadAndSelectSafeNode(parentNode);
                }
            }
        }
        log.info(String.format("%sContent safely closed.", prefix));
    }

    public final void onOpenForm(Form<?> form) {
        prepareCoordinateEditors();
        ContentUIHandler<?> handler = ui.getHandler();
        subUiMap.keySet().forEach(u -> onOpenTabUIModel(u, form));
        handler.onOpenForm(form);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public final void onOpenTabUIModel(ContentTableUI<?, ?, ?> subUi, Form<?> form) {
        ContentTableUIModel<?, ?> subUiModel = subUi.getModel();
//        FIXME Need to do this ?
//        subUiModel.setReferentialReferenceSets(getModel().getReferentialReferenceSets());
        ObserveBusinessProject.get().getOptionalFormDefinition(subUiModel.getChildType()).ifPresent(t -> subUi.getModel().getStates().getReferenceCache().loadReferentialReferenceSetsInModel(t, true));

        subUiModel.getStates().setForm((Form) form);
        form.getObject().copy(subUiModel.getStates().getBean());
        subUi.open();
    }

    public void installValidators(IdDto editBean) {
    }

    public void uninstallValidators() {
    }

    public final void prepareValidationContext() {
        prepareValidationContext(true);
    }

    public void prepareValidationContext(boolean doReset) {
    }

    public void resetCoordinateEditors() {
        coordinateEditors.forEach(CoordinatesEditor::reset);
    }

    public void prepareCoordinateEditors() {
        for (CoordinatesEditor coordinateEditor : coordinateEditors) {
            String propertyLatitude = coordinateEditor.getModel().getPropertyLatitude();
            String propertyLongitude = coordinateEditor.getModel().getPropertyLongitude();
            String propertyQuadrant = coordinateEditor.getModel().getPropertyQuadrant();
            JavaBean bean = (JavaBean) coordinateEditor.getModel().getBean();
            // 1. Mise à jour latitude/longitude:
            Float latitude = bean.get(propertyLatitude);
            Float longitude = bean.get(propertyLongitude);
            Integer quadrant = bean.get(propertyQuadrant);

            coordinateEditor.setLatitudeAndLongitude(latitude, longitude);
            // 2. Mise à jour du quadrant :
            // Si le bean de données contient un quadrant, on met simplement à jour le composant de coordonnées pour sélectionner le quadrant voulu
            // sinon, on réinitialise les quadrants du composant afin qu'aucun d'eux ne soit sélectionné (par exemple dans le cas de la création de la première activité d'une route)
            if (quadrant == null) {
                coordinateEditor.resetQuadrant();
            } else {
                coordinateEditor.setQuadrant(quadrant);
            }
        }
        ui.getModel().getStates().setModified(ui.getModel().getStates().isCreatingMode());
    }

    public void addConfigureActions(ContentUI subUi, boolean move) {
        MenuElement[] subElements = subUi.getConfigurePopup().getSubElements();
        for (MenuElement subElement : subElements) {
            JComponent subElement1 = (JComponent) subElement;
            if (move && subElement1 instanceof AbstractButton) {
                ConfigureMenuAction<?> action = (ConfigureMenuAction<?>) ((AbstractButton) subElement1).getAction();
                action.moveTo(ui.getToggleConfigure());
            }
            configureActions.put(subUi, subElement1);
        }
    }

    public ArrayListMultimap<ContentUI, JComponent> getConfigureActions() {
        return configureActions;
    }

    public final <D extends DataDto, C extends ContainerChildDto, SubUi extends ContentTableUI<D, C, SubUi>> void initTabUI(SubUi subUi, JPanel subUiPane, int tabIndex, Consumer<D> dataConsumer) {
        subUiMap.put(subUi, subUiPane);
        subUiPane.remove(subUi);
        subUiPane.add(subUi.getContentBody());
        subUiPane.putClientProperty("contentUI", subUi);
        ContentTableUIModel<?, ?> model = subUi.getModel();
        if (!model.getStates().isStandalone()) {
            log.info(String.format("%sInit not standalone table ui: %s", ui.getModel().getPrefix(), subUi.getClass().getSimpleName()));
            InputMap inputMap = ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
            ActionMap actionMap = ui.getActionMap();
            ui.getTitleRightToolBar().add(subUi.getSelectToolbar(), 0);
            subUi.getSelectToolbar().setVisible(false);
            MenuElement[] subElements = subUi.getInsertPopup().getSubElements();
            ContentUIHandler<?> handler = ui.getHandler();
            for (MenuElement subElement : subElements) {
                JComponent subElement1 = (JComponent) subElement;
                handler.addSubInsertAction(subElement1);
            }
            if (subElements.length > 0) {
                subUi.putClientProperty(ContentUIHandler.CLIENT_PROPERTY_TAB_INDEX, tabIndex);
            }
            addConfigureActions(subUi, true);
            model.getStates().getTableModel().addTableModelListener(e -> {
                if (ui.getModel().getStates().isReadingMode()) {
                    return;
                }
                if (subUi.getModel().getStates().getTableModel().isCreate())
                    if (e.getType() == TableModelEvent.DELETE || e.getType() == TableModelEvent.INSERT) {
                        return;
                    }
                if (ui.getModel().getStates().isResetEdit()) {
                    return;
                }
                dataConsumer.accept(subUi.getModel().getStates().getBeanToSave());
            });
        }
    }


    public void registerInnerAction(JButton button, InputMap inputMap, ActionMap actionMap) {
        ContentUIActionSupport<?> action = (ContentUIActionSupport<?>) button.getAction();
        action.register(inputMap, actionMap);
    }

    public final void startEditTabUIModel() {
        ContentUIModelStates states = ui.getModel().getStates();
        for (ContentTableUI<?, ?, ?> tableUI : subUiMap.keySet()) {
            tableUI.getModel().getStates().setMode(states.getMode());
            tableUI.startEdit();
        }
    }

    public final void stopEditTabUIModel() {
        for (ContentTableUI<?, ?, ?> tableUI : subUiMap.keySet()) {
            tableUI.getModel().getStates().setMode(ContentMode.READ);
            tableUI.stopEdit();
        }
    }

    public final void selectFirstTab() {
        selectTab(0);
    }

    public final void selectTab(int tabIndex) {
        if (mainTabbedPane != null) {
            mainTabbedPane.setSelectedIndex(tabIndex);
        }
    }

    public final void selectParentTab(int tabIndex) {
        JTabbedPane parent = (JTabbedPane) SwingUtilities.getAncestorOfClass(JTabbedPane.class, ui.getContentBody());
        if (parent != null) {
            parent.setSelectedIndex(tabIndex);
        }
    }

    public ArrayListMultimap<String, JComponent> getFocusComponents() {
        return focusComponents;
    }

    protected U getUi() {
        return ui;
    }

    public List<FilterableComboBox<?>> getComboBoxes() {
        return comboBoxes;
    }

    public void initActions(DefaultUIInitializerResult initializerResult) {
        ContentUIHandler<?> handler = ui.getHandler();
        handler.initActions();
        handler.installChangeModeAction();

        InputMap inputMap = ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        KeyStroke[] uiActionKeyStrokes = inputMap.allKeys();
        log.info(String.format("%sFound %d actions to share", ui.getModel().getPrefix(), uiActionKeyStrokes.length));
        ActionMap actionMap = ui.getActionMap();
        newActionMap = new ActionMap();
        newInputMap = new InputMap();
        for (KeyStroke actionKeyStroke : uiActionKeyStrokes) {
            Object actionMapKey = inputMap.get(actionKeyStroke);
            Action action = actionMap.get(actionMapKey);
            if (action instanceof MenuAction) {
                log.info(String.format("Skip menu action %s → %s", actionKeyStroke, actionMapKey));
                continue;
            }
            log.info(String.format("Register action %s → %s", actionKeyStroke, actionMapKey));
            newInputMap.put(actionKeyStroke, actionMapKey);
            newActionMap.put(actionMapKey, action);
        }

        ContentUIModel model = ui.getModel();
        NodeCapability<?> capability = model.getSource().getCapability();
        if (capability instanceof ContainerCapability) {
            // This is used by create actions to enable or not them
            //FIXME Should have something cleaner...
            model.getStates().addPropertyChangeListener(evt -> {
                String propertyName = evt.getPropertyName();
                if (propertyName.equals(ContentUIModelStates.PROPERTY_UPDATING_MODE) ||
                        propertyName.equals(ContentUIModelStates.PROPERTY_MODIFIED)) {
                    ContentUIModelStates source = (ContentUIModelStates) evt.getSource();
                    boolean enabled = source.isUpdatingMode() && !source.isModified();
                    source.firePropertyChange(PROPERTY_NAME_UPDATING_MODE_AND_NOT_MODIFIED, enabled);
                }
            });
        }
    }

    protected void postInstallActions(JComponent actionContainer, DefaultUIInitializerResult initializerResult) {
        KeyStroke[] keyStrokes = newInputMap.allKeys();
        initializerResult.getComponents(FilterableComboBox.class).forEach(editor -> UIInitHelper.setBinding(editor.getCombobox(), keyStrokes, newInputMap, newActionMap));
        initializerResult.getComponents(JTable.class).forEach(editor -> UIInitHelper.cleanInputMap(editor, keyStrokes));
        for (Map.Entry<ContentTableUI<?, ?, ?>, JPanel> entry : subUiMap.entrySet()) {
            ContentTableUI<?, ?, ?> subUi = entry.getKey();
            InputMap newInputMapSubUi = subUi.getHandler().getContentOpen().getNewInputMap();
            ActionMap newActionMapSubUi = subUi.getHandler().getContentOpen().getNewActionMap();
            newInputMapSubUi.setParent(newInputMap);
            newActionMapSubUi.setParent(newActionMap);
            KeyStroke[] keyStrokesSubUi = newInputMapSubUi.allKeys();
            for (KeyStroke keyStroke : keyStrokesSubUi) {
                if (newInputMap.get(keyStroke) != null) {
                    Object o = newInputMapSubUi.get(keyStroke);
                    if (o != null) {
                        newActionMapSubUi.remove(o);
                        newInputMapSubUi.remove(keyStroke);
                    }
                }
            }
            subUi.getContentBody().setInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, newInputMapSubUi);
            subUi.getContentBody().setActionMap(newActionMapSubUi);
            subUi.getHandler().getContentOpen().getTables().forEach(editor -> UIInitHelper.cleanInputMap(editor, keyStrokesSubUi));
            subUi.getHandler().getContentOpen().getComboBoxes().forEach(editor -> UIInitHelper.setBinding(editor.getCombobox(), keyStrokesSubUi, newInputMapSubUi, newActionMapSubUi));
            //FIXME:Actions We should define some scope only for subUi ?
        }
    }

    public ActionMap getNewActionMap() {
        return newActionMap;
    }

    public InputMap getNewInputMap() {
        return newInputMap;
    }

    public List<JTable> getTables() {
        return tables;
    }

    private synchronized void onCoordinateFormatChanged(CoordinateFormat newValue) {
        if (!coordinateFormatChangedIsChanging) {
            coordinateFormatChangedIsChanging = true;
            boolean modified = ui.getModel().getStates().isModified();
            try {
                coordinateEditors.forEach(e -> e.setFormat(newValue));
            } finally {
                coordinateFormatChangedIsChanging = false;
                if (!modified) {
                    ui.getModel().getStates().setModified(false);
                }
            }
        }
    }

    public void prefixAction(JMenuItem button, String prefix) {
        ContentUIActionSupport<?> action = (ContentUIActionSupport<?>) button.getAction();
        KeyStroke accelerator = action.getKeyStroke();
        action.setKeyStroke(KeyStroke.getKeyStroke(accelerator.getKeyCode(), InputEvent.SHIFT_DOWN_MASK));
        action.setText(prefix + action.getText());
        if (action.getTooltipText() != null) {
            action.setTooltipText(prefix + action.getTooltipText());
        }
        action.updateEditorTexts();
        button.setAccelerator(action.getKeyStroke());
    }

    public void updateConfigurePopup(ContentUI subUi, boolean reset) {
        JPopupMenu popup = ui.getConfigurePopup();
        if (reset) {
            popup.removeAll();
        }
        InputMap inputMap = ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = ui.getActionMap();
        if (subUi != null) {
            for (JComponent subElement : configureActions.get(subUi)) {
                popup.add(subElement);
                if (subElement instanceof AbstractButton) {
                    AbstractButton button = (AbstractButton) subElement;
                    ContentUIActionSupport<?> action = (ContentUIActionSupport<?>) button.getAction();
                    action.defaultInit(inputMap, actionMap);
                }
            }
            if (subUi instanceof ContentTableUI<?, ?, ?>) {
                ((ContentTableUI<?, ?, ?>) subUi).getSelectToolbar().setVisible(((ContentTableUI<?, ?, ?>) subUi).getTableModel().getRowCount() > 1);
            }
        }
        ui.getHandler().fixToggleMenuVisibility();
    }

    public enum Close {
        /**
         * Cancel close
         */
        CANCEL,
        /**
         * Save content and close
         */
        SAVE,
        /**
         * Reset content and close
         */
        RESET,
        /**
         * Do close
         */
        CLOSE
    }
}
