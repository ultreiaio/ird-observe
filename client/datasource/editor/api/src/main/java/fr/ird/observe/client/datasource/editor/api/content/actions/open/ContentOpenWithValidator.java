package fr.ird.observe.client.datasource.editor.api.content.actions.open;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.dto.IdDto;
import org.nuiton.jaxx.validator.JAXXValidator;

/**
 * Created on 31/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ContentOpenWithValidator<U extends ContentUI & JAXXValidator> extends ContentOpen<U> {

    public ContentOpenWithValidator(U ui, ContentOpenExecutor<U> contentOpenExecutor) {
        super(ui, contentOpenExecutor);
    }

    public ContentOpenWithValidator(U ui, ContentOpenExecutor<U> contentOpenExecutor, ContentEditExecutor<U> contentEditExecutor) {
        super(ui, contentOpenExecutor, contentEditExecutor);
    }

    @Override
    public void prepareValidationContext(boolean doReset) {
        ClientValidationContext validationContext = ui.getHandler().getClientValidationContext();
        if (doReset) {
            validationContext.reset();
        }
        ui.getHandler().onPrepareValidationContext();
        coordinateEditors.forEach(e -> validationContext.setCoordinatesEditor(e.getName().replace("Coordinates", ""), e));
        temperatureEditors.forEach(e -> validationContext.setTemperatureEditorModel(e.getModel().getEditorKey(), e.getModel()));
        nauticalLengthEditors.forEach(e -> validationContext.setNauticalLengthEditorModel(e.getModel().getEditorKey(), e.getModel()));
        ui.setValidatorChanged(false);
    }

    @Override
    public void installValidators(IdDto editBean) {
        // reset all validators
        ui.setValidatorBean(null);
        // mark ui as editing
        ui.getModel().getStates().setEditing(true);

        if (tabbedPaneValidator != null) {
            tabbedPaneValidator.install(ui.getErrorTableModel());
        }
        if (subTabbedPaneValidator != null) {
            subTabbedPaneValidator.install(ui.getErrorTableModel());
        }
        // attach validators
        ui.setValidatorBean(editBean);

        ui.getHandler().onInstallValidators(editBean);
    }

    @Override
    public void uninstallValidators() {
        ui.setValidatorBean(null);
        ui.getHandler().onUninstallValidators();
    }

}
