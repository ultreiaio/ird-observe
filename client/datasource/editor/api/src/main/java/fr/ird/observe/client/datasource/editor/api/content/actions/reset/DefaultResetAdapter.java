package fr.ird.observe.client.datasource.editor.api.content.actions.reset;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationInitializer;

/**
 * Created on 19/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class DefaultResetAdapter<U extends ContentUI> implements ResetAdapter<U> {
    @Override
    public void onCreate(U ui) {
        if (ui.getModel().getStates().isCreatingMode()) {
            ui.stopEdit();

            ContentUIHandler<?> handler = ui.getHandler();
            NavigationTree tree = handler.getNavigationTree();
            NavigationNode parentNode = tree.getSelectedNode().getParent();
            tree.getSelectedNode().removeFromParent();
            if (parentNode instanceof ContentRootListUINavigationNode) {
                NavigationNode rootNode = parentNode.getRoot();
                RootNavigationInitializer initializer = (RootNavigationInitializer) rootNode.getInitializer();
                boolean removeGroupBy = parentNode.getChildCount() == 0 && !initializer.getRequest().isLoadEmptyGroupBy();
                if (removeGroupBy) {
                    parentNode.removeFromParent();
                    if (rootNode.isNotLeaf()) {
                        tree.selectFirstNode();
                    }
                    return;
                }
            }
            tree.reloadAndSelectSafeNode(parentNode);
        }
    }

    @Override
    public void onUpdate(U ui) {
        ContentUIModel model = ui.getModel();
        if (model.getStates().isResetEdit()) {
            // avoid re-entrant code
            return;
        }
        // mark model starting a reset action
        model.getStates().setResetEdit(true);

        if (ui instanceof ContentTableUI) {
            //FIXME Not sure this is needed since later in states.copyFormToBean this is also done
            ((ContentTableUI<?, ?, ?>) ui).getTableModel().clearInlineModels();
        }
        ContentUIHandler<?> handler = ui.getHandler();
        try {
            // stop editing
            handler.stopEditUI();

            //FIXME Check this is ok ?
//            // suppression des messages de validation
//            ContentUIHandler.removeAllMessages(ui);

            // do open content again (will reload any content)
            try {
                handler.openUI();
            } catch (Exception ex) {
                handler.stopEditUI();
                throw ex;
            }
        } finally {
            // reset is done
            model.getStates().setResetEdit(false);
        }
    }

}
