/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.actions.reset;

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;

import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * To reset current form..
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ResetAction<U extends ContentUI> extends ContentUIActionSupport<U> {

    private final ResetAdapter<U> adapter;

    public static <U extends ContentUI> void installAction(U ui, JButton editor) {
        installAction(ui, editor, new DefaultResetAdapter<>());
    }

    public static <U extends ContentUI> void installAction(U ui, JButton editor, ResetAdapter<U> adapter) {
        ResetAction<U> action = prepareAction(adapter);
        init(Objects.requireNonNull(ui), Objects.requireNonNull(editor), action);
    }

    public static <U extends ContentUI> ResetAction<U> prepareAction(ResetAdapter<U> adapter) {
        return new ResetAction<>(adapter);
    }

    public ResetAction(ResetAdapter<U> adapter) {
        super(n("observe.Common.action.reset"),
              n("observe.Common.action.reset.tip"),
              "revert",
              ObserveKeyStrokesEditorApi.KEY_STROKE_RESET_DATA);
        this.adapter = adapter;
    }

    @Override
    protected void doActionPerformed(ActionEvent event, U ui) {
        doReset();
    }

    public void doReset() {
        if (ui.getModel().getStates().isCreatingMode()) {
            adapter.onCreate(ui);
        } else {
            adapter.onUpdate(ui);
        }
    }

}
