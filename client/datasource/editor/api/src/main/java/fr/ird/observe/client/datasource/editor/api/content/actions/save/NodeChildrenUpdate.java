package fr.ird.observe.client.datasource.editor.api.content.actions.save;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.BusinessDto;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Created on 23/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class NodeChildrenUpdate {

    private final NavigationNode node;
    private final Set<Class<? extends NavigationNode>> nodeTypesToRemove;
    private final Set<Class<? extends NavigationNode>> nodeTypesToAdd;

    public static class BuilderOnUpdatedBean<D extends BusinessDto> implements BuilderStepAddPredicate<D> {

        private final NavigationNode node;
        private final D bean;
        private final Set<Class<? extends NavigationNode>> nodeTypesToRemove;
        private final Set<Class<? extends NavigationNode>> nodeTypesToAdd;
        private final Set<Class<? extends NavigationNode>> existingNodes;

        public BuilderOnUpdatedBean(NavigationNode node, D bean) {
            this.node = node;
            this.bean = bean;
            nodeTypesToRemove = new LinkedHashSet<>();
            nodeTypesToAdd = new LinkedHashSet<>();
            existingNodes = node.getCapability().existingNodeTypes();
        }

        @Override
        public BuilderOnUpdatedBean<D> onPredicate(Predicate<D> predicate, Class<? extends NavigationNode> nodeType) {
            if (predicate.test(bean)) {
                if (!existingNodes.contains(nodeType)) {
                    nodeTypesToAdd.add(nodeType);
                }
            } else {
                if (existingNodes.contains(nodeType)) {
                    nodeTypesToRemove.add(nodeType);
                }
            }
            return this;
        }

        @Override
        public NodeChildrenUpdate build() {
            return new NodeChildrenUpdate(node, nodeTypesToRemove, nodeTypesToAdd);
        }
    }

    public static class BuilderOnBeanToUpdate<D extends BusinessDto> implements BuilderStepAddPredicate<D> {

        private final NavigationNode node;
        private final D oldBean;
        private final D bean;
        private final Set<Class<? extends NavigationNode>> nodeTypesToRemove;
        private final Set<Class<? extends NavigationNode>> nodeTypesToAdd;
        private final Set<Class<? extends NavigationNode>> existingNodes;

        public BuilderOnBeanToUpdate(NavigationNode node, D oldBean, D bean) {
            this.node = node;
            this.oldBean = oldBean;
            this.bean = bean;
            nodeTypesToRemove = new LinkedHashSet<>();
            nodeTypesToAdd = new LinkedHashSet<>();
            existingNodes = node.getCapability().existingNodeTypes();
        }

        @Override
        public BuilderOnBeanToUpdate<D> onPredicate(Predicate<D> predicate, Class<? extends NavigationNode> nodeType) {
            if (BusinessDto.valueChangedToFalse(oldBean, bean, predicate)) {
                if (existingNodes.contains(nodeType)) {
                    // need to remove this type of node
                    nodeTypesToRemove.add(nodeType);
                }
            }
            return this;
        }

        @Override
        public NodeChildrenUpdate build() {
            return new NodeChildrenUpdate(node, nodeTypesToRemove, nodeTypesToAdd);
        }
    }

    public static <D extends BusinessDto> BuilderOnUpdatedBean<D> afterUpdate(NavigationNode node, D bean) {
        return new BuilderOnUpdatedBean<>(node, bean);
    }

    public static <D extends BusinessDto> BuilderOnBeanToUpdate<D> beforeUpdate(NavigationNode node, D oldBean, D bean) {
        return new BuilderOnBeanToUpdate<>(node, oldBean, bean);
    }

    public NodeChildrenUpdate(NavigationNode node, Set<Class<? extends NavigationNode>> nodeTypesToRemove, Set<Class<? extends NavigationNode>> nodeTypesToAdd) {
        this.node = node;
        this.nodeTypesToRemove = nodeTypesToRemove;
        this.nodeTypesToAdd = nodeTypesToAdd;
    }

    public void update(NavigationTree tree) {
        if (isEmpty()) {
            // nothing to update
            return;
        }
        if (!nodeTypesToAdd.isEmpty()) {
            TreePath path = new TreePath(node.getPath());
            boolean expanded = tree.isExpanded(path);
            // if a node has to be added, let's rebuild the hole children
            node.removeAllChildren();
            node.getHandler().buildChildren();
            if (expanded) {
                tree.expandPath(path);
            }
            return;
        }
        // only remove
        List<NavigationNode> nodesToRemove = new LinkedList<>();
        Enumeration<? extends TreeNode> children = node.children();
        while (children.hasMoreElements()) {
            NavigationNode treeNode = (NavigationNode) children.nextElement();
            if (nodeTypesToRemove.contains(treeNode.getClass())) {
                nodesToRemove.add(treeNode);
            }
        }
        for (NavigationNode node : nodesToRemove) {
            node.removeFromParent();
        }
    }

    public List<String> getMessagesForNodesToRemove() {
        List<String> result = new LinkedList<>();
        Enumeration<? extends TreeNode> children = node.children();
        while (children.hasMoreElements()) {
            TreeNode treeNode = children.nextElement();
            if (nodeTypesToRemove.contains(treeNode.getClass())) {
                if (treeNode instanceof ContentEditUINavigationNode) {
                    result.add(I18n.t("observe.data.message.will.delete.sub.data.type", treeNode.toString(), ((ContentEditUINavigationNode) treeNode).getCapability().getReference().getId()));
                    continue;
                } else if (treeNode instanceof ContentTableUINavigationNode) {
                    if (((ContentTableUINavigationNode) treeNode).getInitializer().isEmpty()) {
                        continue;
                    }
                }
                result.add(I18n.t("observe.data.message.will.delete.sub.data.type2", treeNode));
            }
        }
        return result.isEmpty() ? null : result;
    }

    public boolean isEmpty() {
        return nodeTypesToAdd.isEmpty() && nodeTypesToRemove.isEmpty();
    }

    public Set<Class<? extends NavigationNode>> getNodeTypesToRemove() {
        return nodeTypesToRemove;
    }

    public Set<Class<? extends NavigationNode>> getNodeTypesToAdd() {
        return nodeTypesToAdd;
    }

    public NavigationNode getNode() {
        return node;
    }

    public interface BuilderStepAddPredicate<D extends BusinessDto> extends BuilderStepBuild {
        BuilderStepAddPredicate<D> onPredicate(Predicate<D> predicate, Class<? extends NavigationNode> nodeType);
    }

    interface BuilderStepBuild {
        NodeChildrenUpdate build();
    }
}
