package fr.ird.observe.client.datasource.editor.api.content.actions.save;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.Objects;
import java.util.function.Supplier;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created on 30/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public class SaveAction<D extends BusinessDto, U extends ContentUI> extends ContentUIActionSupport<U> {
    private static final Logger log = LogManager.getLogger(SaveAction.class);

    /**
     * Request to execute.
     */
    private final Supplier<SaveRequest<D>> request;
    /**
     * To execute the save action.
     */
    private final SaveConsumer<D> executor;
    /**
     * To adapt ui after the effective save.
     */
    private final SaveUIAdapter<D, U> uiAdapter;

    public static class Builder<D extends BusinessDto, U extends ContentUI> implements AddOnStep<D, U>, AddCallStep<D, U>, AddThenStep<D, U>, BuildStep<D, U> {

        private final U ui;
        private final Class<D> dtoType;
        private Supplier<SaveRequest<D>> request;
        private SaveConsumer<D> executor;
        private SaveUIAdapter<D, U> uiAdapter;

        public Builder(U ui, Class<D> dtoType) {
            this.ui = Objects.requireNonNull(ui);
            this.dtoType = Objects.requireNonNull(dtoType);
        }

        @Override
        public AddCallStep<D, U> on(Supplier<SaveRequest<D>> request) {
            this.request = Objects.requireNonNull(request);
            return this;
        }

        @Override
        public AddThenStep<D, U> call(SaveConsumer<D> executor) {
            this.executor = Objects.requireNonNull(executor);
            return this;
        }

        @Override
        public BuildStep<D, U> then(SaveUIAdapter<D, U> uiAdapter) {
            this.uiAdapter = Objects.requireNonNull(uiAdapter);
            return this;
        }

        @Override
        public SaveAction<D, U> install(AbstractButton editor) {
            SaveAction<D, U> action = new SaveAction<>(dtoType, request, executor, uiAdapter);
            SaveAction.init(ui, editor, action);
            return action;
        }
    }

    public static <D extends BusinessDto, U extends ContentUI> AddOnStep<D, U> create(U ui, Class<D> dtoType) {
        return new Builder<>(ui, dtoType);
    }

    public SaveAction(Class<D> dataType, Supplier<SaveRequest<D>> request, SaveConsumer<D> executor, SaveUIAdapter<D, U> uiAdapter) {
        super(n("observe.ui.action.save"), null, "content-save-16", ObserveKeyStrokesEditorApi.KEY_STROKE_SAVE_DATA);
        this.request = Objects.requireNonNull(request);
        this.executor = Objects.requireNonNull(executor);
        this.uiAdapter = Objects.requireNonNull(uiAdapter);
        setTooltipText(ObserveBusinessProject.get().saveDataToolTipText(dataType));
    }

    @Override
    protected final void doActionPerformed(ActionEvent e, U ui) {
        SaveRequest<D> request = this.request.get();

        boolean canExecute = request.canExecute();
        if (canExecute) {
            D beanToSave = request.getBeanToSave();
            log.info(String.format("Will save: %s.", beanToSave));
            try {
                SaveResultDto saveResult = executor.consume(request, beanToSave);
                saveResult.toDto(beanToSave);
                getDataSourcesManager().getMainDataSource().setModified(true);
            } catch (ConcurrentModificationException ex) {
                throw new RuntimeException(ex);
            }

            log.info(String.format("Saved: %s, will adapt ui now.", beanToSave));
            if (!ui.getModel().getStates().isClosing()) {
                SwingUtilities.invokeLater(() -> uiAdapter.adaptUi(getDataSourceEditor(), ui, request.isNotPersisted(), beanToSave));
            }
        }
    }

    public interface AddOnStep<D extends BusinessDto, U extends ContentUI> {
        AddCallStep<D, U> on(Supplier<SaveRequest<D>> request);
    }

    public interface AddCallStep<D extends BusinessDto, U extends ContentUI> {
        AddThenStep<D, U> call(SaveConsumer<D> executor);
    }

    public interface AddThenStep<D extends BusinessDto, U extends ContentUI> {
        BuildStep<D, U> then(SaveUIAdapter<D, U> uiAdapter);
    }

    public interface BuildStep<D extends BusinessDto, U extends ContentUI> {
        SaveAction<D, U> install(AbstractButton editor);
    }
}
