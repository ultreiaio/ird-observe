package fr.ird.observe.client.datasource.editor.api.content.actions.save;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;
import fr.ird.observe.dto.BusinessDto;

/**
 * Created on 01/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.17
 */
public abstract class SavePredicateSupport<S extends ContentUIModelStates, D extends BusinessDto> implements SavePredicate<D> {

    protected final S states;

    protected SavePredicateSupport(S states) {
        this.states = states;
    }

    @Override
    public final boolean test(D originalBean, D editBean) {
        // as a side effect of cleaning beanToSave, states may have become not valid
        return checkStatesIsValid() && testForValid(originalBean, editBean);
    }

    protected abstract boolean testForValid(D originalBean, D editBean);

    protected boolean checkStatesIsValid() {
        return states.isValid();
    }
}
