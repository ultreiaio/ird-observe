package fr.ird.observe.client.datasource.editor.api.content.actions.save;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.DataGroupByDto;

import java.util.Objects;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Supplier;

/**
 * Created on 16/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class SaveRequest<D extends BusinessDto> {

    private final ToolkitIdDtoBean parent;
    private final String id;
    private final Supplier<D> originalBeanSupplier;
    private final Supplier<D> beanToSaveSupplier;
    private final boolean notPersisted;
    private final BiPredicate<D, D> predicate;
    private D originalBean;
    private D beanToSave;

    public static <D extends BusinessDto> SaveRequest<D> create(ToolkitIdDtoBean parentId, String id, Supplier<D> originalBeanSupplier, Supplier<D> beanToSaveSupplier, BiPredicate<D, D> predicate) {
        return new SaveRequest<>(parentId, id, originalBeanSupplier, beanToSaveSupplier, predicate);
    }

    public static <D extends BusinessDto> SaveRequest<D> create(DataGroupByDto<?> parentId, String id, Supplier<D> originalBeanSupplier, Supplier<D> beanToSaveSupplier, BiPredicate<D, D> predicate) {
        //FIXME
//        return new SaveRequest<>(parentId.getId(), id, originalBeanSupplier, beanToSaveSupplier, predicate);
        return new SaveRequest<>( id, originalBeanSupplier, beanToSaveSupplier, predicate);
    }

    public static <D extends BusinessDto> SaveRequest<D> create(String id, Supplier<D> originalBeanSupplier, Supplier<D> beanToSaveSupplier, BiPredicate<D, D> predicate) {
        return new SaveRequest<>(id, originalBeanSupplier, beanToSaveSupplier, predicate);
    }

    protected SaveRequest(String id, Supplier<D> originalBeanSupplier, Supplier<D> beanToSaveSupplier, BiPredicate<D, D> predicate) {
        this.parent = null;
        this.id = id;
        this.notPersisted = id == null;
        this.originalBeanSupplier = Objects.requireNonNull(originalBeanSupplier);
        this.beanToSaveSupplier = Objects.requireNonNull(beanToSaveSupplier);
        this.predicate = predicate;
    }

    protected SaveRequest(ToolkitIdDtoBean parentId, String id, Supplier<D> originalBeanSupplier, Supplier<D> beanToSaveSupplier, BiPredicate<D, D> predicate) {
        this.parent = Objects.requireNonNull(parentId);
        this.id = id;
        this.notPersisted = id == null;
        this.originalBeanSupplier = Objects.requireNonNull(originalBeanSupplier);
        this.beanToSaveSupplier = Objects.requireNonNull(beanToSaveSupplier);
        this.predicate = predicate;
    }

    public String getParentId() {
        return Optional.ofNullable(parent).map(ToolkitIdDtoBean::getId).orElse(null);
    }

    public String getId() {
        return id;
    }

    public D getOriginalBean() {
        if (originalBean == null) {
            originalBean = originalBeanSupplier.get();
        }
        return originalBean;
    }

    public D getBeanToSave() {
        if (beanToSave == null) {
            beanToSave = beanToSaveSupplier.get();
        }
        return beanToSave;
    }

    public boolean isNotPersisted() {
        return notPersisted;
    }


    public boolean canExecute() {
        if (id != null && predicate != null) {
            D originalBean = getOriginalBean();
            D beanToSave = getBeanToSave();
            return predicate.test(originalBean, beanToSave);
        }
        return true;
    }
}
