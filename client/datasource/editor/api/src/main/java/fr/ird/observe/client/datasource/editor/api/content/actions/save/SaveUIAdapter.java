package fr.ird.observe.client.datasource.editor.api.content.actions.save;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.dto.IdDto;

/**
 * To adapt ui after an effective save.
 * <p>
 * Created on 30/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public interface SaveUIAdapter<D extends IdDto, U extends ContentUI> {

    /**
     * Invoked when the save action was consumed.
     *
     * @param dataSourceEditor current datasource editor
     * @param ui               current ui where the save action was consumed
     * @param notPersisted     {@code true} if bean to save was not persisted
     * @param savedBean        the saved bean
     */
    void adaptUi(DataSourceEditor dataSourceEditor, U ui, boolean notPersisted, D savedBean);
}
