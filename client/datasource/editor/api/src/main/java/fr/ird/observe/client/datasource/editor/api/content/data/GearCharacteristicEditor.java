package fr.ird.observe.client.datasource.editor.api.content.data;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.referential.common.GearCharacteristicReference;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxCellEditor;

import javax.swing.JTable;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 01/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class GearCharacteristicEditor extends FilterableComboBoxCellEditor {
    private final GearUseFeaturesMeasurementsTableModelSupport<?, ?> tableModel;

    public GearCharacteristicEditor(GearUseFeaturesMeasurementsTableModelSupport<?, ?> tableModel, Decorator decorator) {
        super(UIHelper.newFilterableComboBox(GearCharacteristicReference.class, decorator, new ArrayList<>()));
        this.tableModel = tableModel;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        @SuppressWarnings("unchecked") FilterableComboBox<GearCharacteristicReference> component = (FilterableComboBox<GearCharacteristicReference>) getComponent();
        List<GearCharacteristicReference> allowedCharacteristics = tableModel.getAllowedCharacteristics();
        List<GearCharacteristicReference> data = new ArrayList<>(allowedCharacteristics);
        for (int i = 0, rowCount = tableModel.getRowCount(); i < rowCount; i++) {
            if (i != row) {
                GearCharacteristicReference valueAt = (GearCharacteristicReference) tableModel.getValueAt(i, 0);
                data.remove(valueAt);
            }
        }
        component.setData(data);
        return super.getTableCellEditorComponent(table, value, isSelected, row, column);
    }
}
