package fr.ird.observe.client.datasource.editor.api.content.data;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.ProtectedIdsCommon;
import fr.ird.observe.dto.referential.common.GearCharacteristicListItemReference;
import fr.ird.observe.dto.referential.common.GearCharacteristicReference;
import fr.ird.observe.dto.referential.common.GearCharacteristicTypeReference;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxCellEditor;
import org.jdesktop.swingx.table.DatePickerCellEditor;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 4/7/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.16
 */
public class GearUseFeatureMeasurementCellEditor implements TableCellEditor {

    private final Decorator decoratorListItem;
    private final Map<String, GearCharacteristicListItemReference> gearCharacteristicListItemsById;
    private final Map<String, List<GearCharacteristicListItemReference>> gearCharacteristicListItemsByCharacteristicId;
    protected TableCellEditor editor;
    private Map<String, TableCellEditor> editorsByGearCharacteristicId;

    public GearUseFeatureMeasurementCellEditor(Decorator decoratorListItem, Map<String, GearCharacteristicListItemReference> gearCharacteristicListItemsById) {
        this.decoratorListItem = decoratorListItem;
        this.gearCharacteristicListItemsById = gearCharacteristicListItemsById;
        this.gearCharacteristicListItemsByCharacteristicId = new TreeMap<>();
        gearCharacteristicListItemsById.values().forEach(e -> gearCharacteristicListItemsByCharacteristicId.computeIfAbsent(e.getGearCharacteristic().getId(), l -> new ArrayList<>()).add(e));
    }

    @Override
    public Object getCellEditorValue() {
        return editor.getCellEditorValue();
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return editor.shouldSelectCell(anEvent);
    }

    @Override
    public boolean stopCellEditing() {
        return editor.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        editor.cancelCellEditing();
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        editor.addCellEditorListener(l);
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        editor.removeCellEditorListener(l);
    }

    private Map<String, TableCellEditor> getEditorsByGearCharacteristicId(JTable table) {
        if (editorsByGearCharacteristicId == null) {
            editorsByGearCharacteristicId = new TreeMap<>();
            editorsByGearCharacteristicId.put(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_TEXT, table.getDefaultEditor(Object.class));
            editorsByGearCharacteristicId.put(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_BOOLEAN, table.getDefaultEditor(Boolean.class));
            editorsByGearCharacteristicId.put(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_INTEGER_SIGNED, NumberCellEditor.newInteger4ColumnEditor(true));
            editorsByGearCharacteristicId.put(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_FLOAT_SIGNED, NumberCellEditor.newFloatColumnEditor(true));
            editorsByGearCharacteristicId.put(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_INTEGER_UNSIGNED, NumberCellEditor.newInteger4ColumnEditor(false));
            editorsByGearCharacteristicId.put(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_FLOAT_UNSIGNED, NumberCellEditor.newFloatColumnEditor(false));
            boolean french = I18n.getDefaultLocale().equals(Locale.FRANCE);
            editorsByGearCharacteristicId.put(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_DATE, new DatePickerCellEditor(new SimpleDateFormat(french ? "dd/MM/yyyy" : "yyyy-MM-dd")));
            editorsByGearCharacteristicId.put(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_LIST, UIHelper.newCellEditor(new ArrayList<>(), decoratorListItem));
        }
        return editorsByGearCharacteristicId;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        GearCharacteristicReference characteristic = (GearCharacteristicReference) table.getModel().getValueAt(row, 0);
        if (characteristic == null) {
            editor = table.getDefaultEditor(Object.class);
        } else {
            GearCharacteristicTypeReference gearCharacteristicType = characteristic.getGearCharacteristicType();
            value = gearCharacteristicType.getOptionalTypeValue(value, gearCharacteristicListItemsById).orElse(null);
            Map<String, TableCellEditor> editors = getEditorsByGearCharacteristicId(table);
            editor = editors.get(gearCharacteristicType.getId());
            if (gearCharacteristicType.isList()) {
                List<GearCharacteristicListItemReference> gearCharacteristicListItemReferences = gearCharacteristicListItemsByCharacteristicId.get(characteristic.getId());
                @SuppressWarnings("unchecked") FilterableComboBox<GearCharacteristicListItemReference> component = (FilterableComboBox<GearCharacteristicListItemReference>) ((FilterableComboBoxCellEditor) editor).getComponent();
                component.setData(gearCharacteristicListItemReferences);
                component.setSelectedItem((GearCharacteristicListItemReference) value);
            }
        }
        return editor.getTableCellEditorComponent(table, value, isSelected, row, column);
    }
}
