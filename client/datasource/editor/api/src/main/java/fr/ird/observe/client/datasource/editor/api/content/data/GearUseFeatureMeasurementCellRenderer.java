package fr.ird.observe.client.datasource.editor.api.content.data;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.dto.ProtectedIdsCommon;
import fr.ird.observe.dto.referential.common.GearCharacteristicListItemReference;
import fr.ird.observe.dto.referential.common.GearCharacteristicReference;
import fr.ird.observe.dto.referential.common.GearCharacteristicTypeReference;
import org.jdesktop.swingx.JXTable;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 4/7/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.16
 */
public class GearUseFeatureMeasurementCellRenderer implements TableCellRenderer {

    private final Map<String, TableCellRenderer> renderersByCharacteristicTypeId;
    private final ImmutableMap<String, GearCharacteristicListItemReference> gearCharacteristicListItemsById;

    public GearUseFeatureMeasurementCellRenderer(JXTable table, ImmutableMap<String, GearCharacteristicListItemReference> gearCharacteristicListItemsById) {
        this.gearCharacteristicListItemsById = gearCharacteristicListItemsById;
        this.renderersByCharacteristicTypeId = new TreeMap<>();
        TableCellRenderer booleanRenderer = table.getDefaultRenderer(Boolean.class);
        renderersByCharacteristicTypeId.put(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_BOOLEAN, booleanRenderer);
        renderersByCharacteristicTypeId.put(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_DATE, JXTableUtil.newDateTableCellRenderer());
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        GearCharacteristicReference gearCharacteristic = (GearCharacteristicReference) table.getModel().getValueAt(row, 0);
        TableCellRenderer tableCellRenderer = null;
        if (gearCharacteristic != null) {
            GearCharacteristicTypeReference gearCharacteristicType = gearCharacteristic.getGearCharacteristicType();
            tableCellRenderer = renderersByCharacteristicTypeId.get(gearCharacteristicType.getId());
            value = gearCharacteristicType.getOptionalTypeValue(value, gearCharacteristicListItemsById).orElse(null);
        }
        if (tableCellRenderer == null) {
            tableCellRenderer = table.getDefaultRenderer(Object.class);
        }
        return tableCellRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }

}
