package fr.ird.observe.client.datasource.editor.api.content.data;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUITableModel;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableListProperty;
import fr.ird.observe.client.util.table.EditableTableModelWithCache;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.GearUseFeaturesAware;
import fr.ird.observe.dto.data.GearUseFeaturesMeasurementAware;
import fr.ird.observe.dto.referential.common.GearCharacteristicListItemReference;
import fr.ird.observe.dto.referential.common.GearCharacteristicReference;
import fr.ird.observe.dto.referential.common.GearDto;
import fr.ird.observe.dto.referential.common.GearReference;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.validator.swing.SwingValidator;

import javax.swing.JScrollPane;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created on 05/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class GearUseFeaturesMeasurementsTableModelSupport<E extends BusinessDto & GearUseFeaturesMeasurementAware, P extends ContainerChildDto & GearUseFeaturesAware> extends EditableTableModelWithCache<E> {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger(GearUseFeaturesMeasurementsTableModelSupport.class);
    private ImmutableMap<String, GearDto> gearsById;
    private ImmutableMap<String, GearCharacteristicListItemReference> gearCharacteristicListItemsById;

    private FilterableComboBox<GearCharacteristicReference> gearCharacteristicEditor;
    private List<GearCharacteristicReference> allowedCharacteristics;

    private ContentTableUI<?, P, ?> ui;

    @SuppressWarnings("unchecked")
    public static <E extends BusinessDto & GearUseFeaturesMeasurementAware, P extends ContainerChildDto & GearUseFeaturesAware> void onInit(ContentTableUI<?, P, ?> ui, JScrollPane scrollPane, EditableTable<E, ?> table, SwingValidator<E> rowValidator) {
        table.setRowHeight(22);
        GearUseFeaturesMeasurementsTableModelSupport<E, P> model = (GearUseFeaturesMeasurementsTableModelSupport<E, P>) table.getModel();
        ui.getTableModel().registerInlineModel(model, table);
        model.ui = ui;
        ContentTableUITableModel<?, P, ?> tableUIModel = ui.getTableModel();
        @SuppressWarnings("unchecked") P tableEditBean = (P) ui.getTableEditBean();
        tableEditBean.addPropertyChangeListener("gear", evt -> model.onGearChanged(ui, tableUIModel, (GearReference) evt.getNewValue(), false));
        tableUIModel.addPropertyChangeListener(ContentTableUITableModel.SELECTED_ROW_PROPERTY, evt -> {
            int newValue = (int) evt.getNewValue();
            if (newValue != -1) {
                model.onGearChanged(ui, tableUIModel, tableEditBean.getGear(), true);
            }
        });
        model.gearsById = Maps.uniqueIndex(ui.getHandler().getReferentialService().loadDtoList(GearDto.class), GearDto::getId);
        model.gearCharacteristicListItemsById = Maps.uniqueIndex(ui.getHandler().getReferentialReferences(GearCharacteristicListItemReference.class), GearCharacteristicListItemReference::getId);
        JXTableUtil.setI18nTableHeaderRenderer(table, GearUseFeaturesMeasurementAware.class, "gearCharacteristic", "measurementValue");

        Decorator decorator = ui.getHandler().getDecoratorService().getDecoratorByType(GearCharacteristicReference.class);
        JXTableUtil.initRenderers(table, JXTableUtil.newDecoratedRenderer(decorator), new GearUseFeatureMeasurementCellRenderer(table, model.gearCharacteristicListItemsById));

//        List<GearCharacteristicReference> characteristics = ui.getHandler().getReferentialReferences(GearCharacteristicReference.class);
        Decorator decoratorListItem = ui.getHandler().getDecoratorService().getDecoratorByType(GearCharacteristicListItemReference.class);
        GearCharacteristicEditor gearCharacteristicEditor = new GearCharacteristicEditor(model, decorator);
        JXTableUtil.initEditors(table, gearCharacteristicEditor, new GearUseFeatureMeasurementCellEditor(decoratorListItem, model.gearCharacteristicListItemsById));
        JXTableUtil.setMinimumHeight(table.getRowHeight() * 15, table, scrollPane);
        model.gearCharacteristicEditor = (FilterableComboBox<GearCharacteristicReference>) gearCharacteristicEditor.getComponent();
        table.init(ui, rowValidator);

        UIHelper.fixTableColumnWidth(ui.getTable(), 1, 75);
        UIHelper.fixTableColumnWidth(ui.getTable(), 2, 75);
    }

    public GearUseFeaturesMeasurementsTableModelSupport(EditableListProperty<E> listProperty) {
        super(listProperty);
    }

    private void onGearChanged(ContentTableUI<?, P, ?> ui, ContentTableUITableModel<?, P, ?> tableModel, GearReference newGear, boolean selectionRowChanged) {
        if (!tableModel.isEditable()) {
            return;
        }
        if (tableModel.isAdjusting()) {
            return;
        }
        String gearId = newGear == null ? null : newGear.getId();
        log.info("Use gear: {}, selection row changed? {}, existing measurements ({})", gearId, selectionRowChanged, getRowCount());
        if (!selectionRowChanged && !ui.getStates().isResetEdit()) {
            // This means user has changed the gear, need to reset measurement, then add default ones
            setDefaultMeasurementsOnSelectedRow(gearId, tableModel.getSelectedRow());
        }
        updateAllowedCharacteristics(gearId);
    }

    @Override
    public final int getColumnCount() {
        return 2;
    }

    @Override
    public final Object getValueAt0(E row, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return row.getGearCharacteristic();
            case 1:
                return row.getMeasurementValue();
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public final boolean setValueAt0(E row, Object aValue, int rowIndex, int columnIndex) {
        boolean modified = isModified();
        switch (columnIndex) {
            case 0:
                modified |= GearUseFeaturesMeasurementAware.updateGearCharacteristic(row, (GearCharacteristicReference) aValue);
                break;
            case 1:
                modified |= GearUseFeaturesMeasurementAware.updateMeasurementValue(row, aValue);
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return modified;
    }

    public void setDefaultMeasurementsOnSelectedRow(String gearId, int editingRow) {
        List<E> measurements = getDefaultGearUseFeaturesMeasurement(gearId);
        log.info(String.format("User mode, use default measurements: %d", measurements.size()));
        cache().update(editingRow, measurements);
        setData(measurements);
        setModified(false);
        listProperty().set(measurements);
    }

    public void updateAllowedCharacteristics(String gearId) {
        allowedCharacteristics = getAllowedCharacteristics(gearId);
        gearCharacteristicEditor.setData(allowedCharacteristics);
    }

    public List<GearCharacteristicReference> getAllowedCharacteristics() {
        return allowedCharacteristics;
    }

    protected List<E> getDefaultGearUseFeaturesMeasurement(String gearId) {
        if (gearId == null) {
            return new ArrayList<>();
        }
        GearDto gearDto = gearsById.get(gearId);
        return GearUseFeaturesMeasurementAware.getDefaultGearUseFeaturesMeasurement(Objects.requireNonNull(gearDto), this::createNewRow);
    }

    protected List<GearCharacteristicReference> getAllowedCharacteristics(String gearId) {
        if (gearId == null) {
            return new ArrayList<>();
        }
        GearDto gearDto = gearsById.get(gearId);
        return Objects.requireNonNull(gearDto).getAllowedGearCharacteristic();
    }
}

