package fr.ird.observe.client.datasource.editor.api.content.data;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUIHandler;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.DataGroupByDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Store here any mutual code for some actions.
 */
public class TripActionHelper {

    private static final Logger log = LogManager.getLogger(TripActionHelper.class);

    public static void disableCreateActionIfProgramDisabled(ContentUI ui, DataGroupByDto<?> program) {
        if (ui.getModel().getStates().isEditable()) {
            if (program.isDisabled() && !ui.getModel().getClientConfig().isValidationUseDisabledReferential()) {
                String editorName = (String) ui.getClientProperty(ContentListUIHandler.CLIENT_PROPERTY_CREATE_ACTION);
                JMenuItem createAction = (JMenuItem) ui.getObjectById(editorName);
                createAction.setEnabled(false);
            }
        }
    }

    public static boolean askUserToFix(ContentUI source, String dtoLabel, String message) {
        // Let's ask user to fill them
        int response = source.getModel().askToUser(
                t("observe.data.Trip.title.can.not.create.trip.sub.data", dtoLabel),
                new JLabel(message),
                JOptionPane.WARNING_MESSAGE,
                new Object[]{
                        t("observe.data.Trip.choice.go.to.trip"),
                        t("observe.ui.choice.cancel")
                },
                0);
        log.debug("response : " + response);
        return response == 0;
    }

    public static <V extends TripUI<?>> V changeContent(ContentUI source, NavigationNode tripNode, Class<V> targetType) {
        DataSourceEditor dataSourceEditor = source.getHandler().getDataSourceEditor();
        NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();
        tree.selectSafeNode(tripNode);
        ContentUI target = dataSourceEditor.getModel().getContent();
        if (tripNode.getInitializer().getEditNodeId() == null) {
            target.getMode().doClick();
            target = dataSourceEditor.getModel().getContent();
        }
        return targetType.cast(target);
    }
}
