package fr.ird.observe.client.datasource.editor.api.content.data;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.data.map.ObserveMapPane;
import fr.ird.observe.client.datasource.editor.api.content.data.map.TripMapUI;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUI;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.data.TripAwareService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import java.util.Set;

public abstract class TripUIHelper<D extends RootOpenableDto, U extends ContentRootOpenableUI<D, U> & TripUI<D>> implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(TripUIHelper.class);

    private final U ui;
    private final String prefix;
    private boolean opening;

    public TripUIHelper(U ui, String prefix) {
        this.ui = ui;
        this.prefix = prefix;
    }

    protected abstract TripAwareService getService(ObserveServicesProvider servicesProvider);

    protected TripMapDto getMap(TripMapConfigDto tripMapConfig) {
        return getService(ui.getHandler()).getTripMap(tripMapConfig);
    }

    private static final Set<String> UPDATE_MAP_MODEL_PROPERTIES = Set.of(
            TripMapConfigDto.PROPERTY_ADD_OBSERVATIONS,
            TripMapConfigDto.PROPERTY_ADD_OBSERVATIONS_TRIP_SEGMENT,
            TripMapConfigDto.PROPERTY_ADD_LOGBOOK,
            TripMapConfigDto.PROPERTY_ADD_LOGBOOK_TRIP_SEGMENT
    );

    public void installMap(TripAware trip) {

        TripMapUI tripMap = ui.getTripMap();

        TripMapConfigDto tripMapConfig = tripMap.getTripMapConfig();
        tripMapConfig.setAddObservations(true);
        tripMapConfig.setAddObservationsTripSegment(true);
        tripMapConfig.setAddLogbook(true);
        tripMapConfig.setAddLogbookTripSegment(true);
        SwingUtilities.invokeLater(tripMap::validate);
        ClientConfig config = getClientConfig();

        trip.addPropertyChangeListener(TripAware.PROPERTY_OBSERVATIONS_AVAILABILITY, evt -> onObservationsAvailabilityChanged((Boolean) evt.getNewValue()));
        trip.addPropertyChangeListener(TripAware.PROPERTY_LOGBOOK_AVAILABILITY, evt -> onLogbookAvailabilityChanged((Boolean) evt.getNewValue()));

        tripMap.getHandler().setConfig(config);
        tripMapConfig.addPropertyChangeListener(e -> {
            if (UPDATE_MAP_MODEL_PROPERTIES.contains(e.getPropertyName())) {
                updateMapModel();
            }
        });
        tripMap.getObserveMapPane().addPropertyChangeListener(ObserveMapPane.PROPERTY_REBUILD_MODEL, e -> rebuildTripMap());
        ui.getMainTabbedPane().addChangeListener(this::onTabChanged);
        // Disable map tab because can't manage it :(
        ui.getMainTabbedPane().setEnabledAt(ui.getMainTabbedPane().getTabCount() - 1, false);
    }

    public void onOpened() {
        ui.putClientProperty(ContentListUIHandler.CLIENT_PROPERTY_CREATE_ACTION, "addCommonTrip");
        TripActionHelper.disableCreateActionIfProgramDisabled(ui, ui.getModel().getSource().getInitializer().getParentReference());
        // Re-set mapTab enabled state
        ui.processDataBinding("mapTab.enabled");
        ui.getMainTabbedPane().setEnabledAt(ui.getMainTabbedPane().getTabCount() - 1, ui.getMapTab().isEnabled());
        if (ui.getModel().getStates().isCreatingMode()) {
            ui.getMainTabbedPane().setSelectedIndex(0);
        }
    }

    private void buildTripMap() {
        if (ui.getModel().getStates().isCreatingMode()) {
            return;
        }
        if (opening) {
            return;
        }
        if (!ui.getModel().getStates().isOpened()) {
            return;
        }
        SwingUtilities.invokeLater(() -> {
            getBusyModel().addTask("Build map");
            try {
                TripMapUI tripMap = ui.getTripMap();
                TripMapConfigDto tripMapConfig = tripMap.getTripMapConfig();
                log.info(prefix + String.format("Ask to build map: show observation? %s, show logbook? %s", tripMapConfig.isAddObservations(), tripMapConfig.isAddLogbook()));
                TripMapDto tripMapDto = getMap(tripMapConfig);
                Set<String> excludedFeatureNames = tripMap.getObserveMapPane().getExcludedFeatureNames();
                tripMap.getHandler().doOpenMap(tripMapConfig, tripMapDto, excludedFeatureNames);
            } finally {
                getBusyModel().popTask();
            }
        });
    }

    private void rebuildTripMap() {
        if (ui.getModel().getStates().isCreatingMode()) {
            return;
        }
        if (opening) {
            return;
        }
        if (!ui.getModel().getStates().isOpened()) {
            return;
        }

        SwingUtilities.invokeLater(() -> {
            getBusyModel().addTask("Rebuild map");
            try {
                TripMapUI tripMap = ui.getTripMap();
                TripMapConfigDto tripMapConfig = tripMap.getTripMapConfig();
                TripMapDto tripMapDto = tripMap.getObserveMapPane().getTripMapDto();
                Set<String> excludedFeatureNames = tripMap.getObserveMapPane().getExcludedFeatureNames();
                tripMap.getHandler().doOpenMap(tripMapConfig, tripMapDto, excludedFeatureNames);
            } finally {
                getBusyModel().popTask();
            }
        });
    }

    private void onTabChanged(ChangeEvent e) {
        JTabbedPane selectedComponent = (JTabbedPane) e.getSource();
        TripMapUI tripMap1 = ui.getTripMap();
        if (selectedComponent.getSelectedComponent().equals(tripMap1)) {
            ui.getActions().setVisible(false);
            ui.getHandler().getDataSourceEditor().getMessageView().setVisible(false);
            if (ui.isBuildMap()) {
                try {
                    buildTripMap();
                } finally {
                    ui.setBuildMap(false);
                }
            }
        } else {
            ui.getActions().setVisible(true);
            ui.getHandler().getDataSourceEditor().getMessageView().setVisible(true);
            ui.getHandler().fixFormSize();

        }
    }

    public void updateMapModel() {
        if (opening) {
            return;
        }
        ui.getTripMap().getHandler().doCloseMap();
        buildTripMap();
    }

    public void onOpenModel(TripAware trip) {
        opening = true;
        try {
            TripMapConfigDto tripMapConfig = ui.getTripMap().getTripMapConfig();
            tripMapConfig.setTripId(trip.getId());
            onObservationsAvailabilityChanged(trip.isObservationsAvailability());
            onLogbookAvailabilityChanged(trip.isLogbookAvailability());
        } finally {
            opening = false;
        }
    }

    private void onObservationsAvailabilityChanged(boolean newValue) {
        log.info(String.format("ObservationsAvailabilityChanged to: %s", newValue));
        ui.getTripMap().getAddObservations().setEnabled(newValue);
        ui.getTripMap().getTripMapConfig().setAddObservations(newValue);
    }

    private void onLogbookAvailabilityChanged(boolean newValue) {
        log.info(String.format("LogbookAvailabilityChanged to: %s", newValue));
        ui.getTripMap().getAddLogbook().setEnabled(newValue);
        ui.getTripMap().getTripMapConfig().setAddLogbook(newValue);
    }
}
