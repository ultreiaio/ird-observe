package fr.ird.observe.client.datasource.editor.api.content.data.edit;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowTechnicalInformations;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.actions.reset.ResetAction;
import fr.ird.observe.dto.data.EditableDto;

/**
 * Created by tchemit on 26/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ContentEditUIHandler<D extends EditableDto, U extends ContentEditUI<D, U>> extends ContentUIHandler<U> {

    protected abstract void installSaveAction();

    protected abstract void installDeleteAction();

    protected abstract void installCreateNewAction();

    @Override
    public ContentEditUIModel<D> getModel() {
        return ui.getModel();
    }

    @Override
    protected final ContentEditUIInitializer<U> createContentUIInitializer(U ui) {
        return new ContentEditUIInitializer<>(ui);
    }

    @Override
    protected ContentOpenWithValidator<U> createContentOpen(U ui) {
        ContentEditUIOpenExecutor<D, U> executor = new ContentEditUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor);
    }

    @Override
    public final ContentOpenWithValidator<U> getContentOpen() {
        return (ContentOpenWithValidator<U>) super.getContentOpen();
    }

    @Override
    public final void initActions() {
        installResetAction();
        installCreateNewAction();
        installSaveAction();
        installDeleteAction();
        installExtraActions();
        ui.getConfigurePopup().addSeparator();
        ShowTechnicalInformations.installAction(ui);
    }

    protected void installExtraActions() {
        // by default no extra actions
    }


    @Override
    public final void installChangeModeAction() {
        ChangeMode.installAction(ui);
    }

    protected final void installResetAction() {
        ResetAction.installAction(ui, ui.getReset());
    }
}
