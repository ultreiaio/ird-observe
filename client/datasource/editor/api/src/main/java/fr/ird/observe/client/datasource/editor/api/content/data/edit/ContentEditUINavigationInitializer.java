package fr.ird.observe.client.datasource.editor.api.content.data.edit;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import io.ultreia.java4all.decoration.Decorator;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 25/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentEditUINavigationInitializer extends NavigationInitializer<ContentEditUINavigationContext> {

    private final Supplier<? extends DtoReference> parentReference;
    private DataDtoReference reference;
    private Decorator decorator;

    public ContentEditUINavigationInitializer(NavigationScope scope, Supplier<? extends DtoReference> parentReference, DataDtoReference reference) {
        super(scope);
        this.reference = Objects.requireNonNull(reference);
        this.parentReference = Objects.requireNonNull(parentReference);
    }

    public String getText() {
        return decorator.decorate(reference);
    }

    @Override
    protected DataDtoReference init(NavigationContext<ContentEditUINavigationContext> context) {
        decorator = context.getDecoratorService().getDecoratorByType(reference.getReferenceType(), getScope().getDecoratorClassifier());
        reference.registerDecorator(decorator);
        return getReference();
    }

    @Override
    protected void open(NavigationContext<ContentEditUINavigationContext> context) {
        updateSelectNodeId(getSelectId());
    }

    @Override
    protected void reload(NavigationContext<ContentEditUINavigationContext> context) {
        this.reference = context.reloadReference(reference, decorator);
    }

    @Override
    public String toPath() {
        return reference.getId();
    }

    public DtoReference getParentReference() {
        return parentReference.get();
    }

    public ToolkitIdDtoBean getParentShortReference() {
        return getParentReference().toShortDto();
    }

    public DataDtoReference getReference() {
        return reference;
    }

    public final String getSelectedParentId() {
        return getParentReference().getId();
    }

    public final String getSelectId() {
        return getReference().getId();
    }

    public final boolean isPersisted() {
        return getReference().getId() != null;
    }

    public final boolean isNotPersisted() {
        return !isPersisted();
    }

    public DataDtoReference updateReference(ContentEditUINavigationContext context, String id) {
        this.reference = context.getReference(getReference().getReferenceType(), id, decorator);
        return getReference();
    }
}
