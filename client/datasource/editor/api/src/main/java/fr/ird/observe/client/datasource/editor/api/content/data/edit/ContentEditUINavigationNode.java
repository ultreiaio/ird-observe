package fr.ird.observe.client.datasource.editor.api.content.data.edit;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ReferenceContainerCapability;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.function.Supplier;

/**
 * Created on 08/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentEditUINavigationNode extends NavigationNode {
    private static final Logger log = LogManager.getLogger(ContentEditUINavigationNode.class);

    public static <N extends ContentEditUINavigationNode> N init(N node, Supplier<? extends DtoReference> parentReference, DataDtoReference reference) {
        ContentEditUINavigationInitializer initializer = new ContentEditUINavigationInitializer(node.getScope(), parentReference, reference);
        node.init(initializer);
        return node;
    }

    @Override
    public ContentEditUINavigationInitializer getInitializer() {
        return (ContentEditUINavigationInitializer) super.getInitializer();
    }

    @Override
    public ContentEditUINavigationContext getContext() {
        return (ContentEditUINavigationContext) super.getContext();
    }

    @Override
    public ContentEditUINavigationHandler<?> getHandler() {
        return (ContentEditUINavigationHandler<?>) super.getHandler();
    }

    @Override
    public ContentEditUINavigationCapability<?> getCapability() {
        return (ContentEditUINavigationCapability<?>) super.getCapability();
    }

    public DataDtoReference getReference() {
        return getInitializer().getReference();
    }

    public DtoReference getParentReference() {
        return getInitializer().getParentReference();
    }

    public void updateReference(String id) {
        boolean notPersisted = getInitializer().isNotPersisted();
        NavigationNode parent = getParent();
        int oldPosition = parent.getIndex(this);
        DataDtoReference reference = getInitializer().updateReference(getContext(), id);
        if (notPersisted) {
            getInitializer().updateSelectNodeId(id);
        }
        // reload node data first
        reloadNodeData();

        if (notPersisted && isContainer()) {
            // can now load children if any
            dirty();
            populateChildrenIfNotLoaded();
        }

        ReferenceContainerCapability<?> capability = (ReferenceContainerCapability<?>) parent.getCapability();
        int newPosition = capability.getNodePosition(reference);
        if (oldPosition != newPosition) {
            log.info(String.format("Move node from: %d to %d", oldPosition, newPosition));
            parent.moveNode(this, newPosition);
        }

        // repaint selected node and his children
        nodeChangedDeep();

        // reload from parent to root
        parent.reloadNodeDataToRoot();
    }
}
