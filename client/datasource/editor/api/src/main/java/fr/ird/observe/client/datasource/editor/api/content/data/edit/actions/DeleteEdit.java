package fr.ird.observe.client.datasource.editor.api.content.data.edit.actions;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUI;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.decoration.Decorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.util.Objects;
import java.util.function.Consumer;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 28/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public final class DeleteEdit<D extends EditableDto, U extends ContentEditUI<D, U>> extends ContentEditUIActionSupport<D, U> {

    private static final Logger log = LogManager.getLogger(DeleteEdit.class);

    private final Consumer<String> consumer;

    public static <D extends EditableDto, U extends ContentEditUI<D, U>> void installAction(U ui, Consumer<String> consumer) {
        DeleteEdit<D, U> action = new DeleteEdit<>(ui.getModel().getSource().getScope().getMainType(), consumer);
        init(ui, Objects.requireNonNull(ui).getDelete(), action);
    }

    public static <E extends IdDto> boolean confirmForEntityDelete(ContentUI parent, E bean) {
        return confirmForEntityDelete(parent, bean, null);
    }

    public static <E extends IdDto> boolean confirmForEntityDelete(ContentUI parent, E bean, String extraMessage) {
        WithClientUIContextApi clientUIContext = parent.getHandler();
        @SuppressWarnings("unchecked") Class<E> beanClass = (Class<E>) bean.getClass();

        BusyModel busyModel = clientUIContext.getBusyModel();
        busyModel.addTask("Confirm to delete");
        int response;
        try {
            DecoratorService decoratorService = clientUIContext.getDecoratorService();
            Decorator decorator = decoratorService.getDecoratorByType(beanClass);
            String beanStr;
            String messageDelete;
            String type = I18nDecoratorHelper.getType(beanClass);

            if (bean.isNotPersisted()) {
                // delete new entity
                messageDelete = t("observe.ui.message.delete.new", type);
            } else {
                try {
                    // delete existing entity
                    beanStr = decorator.decorate(bean);
                    messageDelete = t("observe.ui.message.delete", type, beanStr);
                } catch (Exception e) {
                    messageDelete = t("observe.ui.message.delete.new", type);
                }
            }

            if (extraMessage != null) {
                messageDelete += '\n' + extraMessage;
            }

            response = clientUIContext.askToUser(
                    parent,
                    t("observe.ui.title.delete"),
                    messageDelete,
                    JOptionPane.WARNING_MESSAGE,
                    new Object[]{t("observe.ui.choice.confirm.delete"),
                            t("observe.ui.choice.cancel")},
                    0);
        } finally {
            busyModel.popTask();
        }
        return response == 0;
    }

    private DeleteEdit(Class<D> dataType, Consumer<String> consumer) {
        super(dataType, n("observe.Common.action.delete"), t("observe.Common.action.delete.tip", I18nDecoratorHelper.getType(dataType)), "content-delete-16", ObserveKeyStrokesEditorApi.KEY_STROKE_DELETE_DATA_GLOBAL);
        this.consumer = consumer;
    }

    @Override
    protected void doActionPerformed(ActionEvent event, U ui) {
        D bean = ui.getModel().getStates().getBean();
        boolean canDelete = askToDelete(ui, bean);
        if (canDelete) {
            doDelete(bean);
            afterDelete(ui, bean);
        }
    }

    private void doDelete(D bean) {
        String typeLabel = I18nDecoratorHelper.getType(getDataType());
        String id = bean.getId();
        log.info(String.format("Will delete `%s` %s", typeLabel, id));
        consumer.accept(id);
        log.info(String.format("DeleteMultiple done for %s %s", typeLabel, id));
    }

    private boolean askToDelete(U ui, D bean) {
        return confirmForEntityDelete(ui, bean);
    }

    private void afterDelete(U ui, D bean) {

        log.info("After delete: " + bean.getId());

        getDataSourcesManager().getMainDataSource().setModified(true);

        NavigationTree tree = getDataSourceEditor().getNavigationUI().getTree();
        NavigationNode parentNode = tree.getSelectedNode().upToReferenceNode(ui.getModel().getSource().getScope().getMainReferenceType()).getParent();

        ui.stopEdit();
        ContentUIHandler.removeAllMessages(ui);

        NavigationNode node = tree.getSelectedNode();
        node.removeFromParent();
        tree.reloadAndSelectSafeNode(parentNode);
    }

}

