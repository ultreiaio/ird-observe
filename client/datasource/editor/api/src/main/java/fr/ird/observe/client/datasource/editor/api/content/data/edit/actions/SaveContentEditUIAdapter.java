package fr.ird.observe.client.datasource.editor.api.content.data.edit.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveUIAdapter;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUI;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUINavigationNode;
import fr.ird.observe.dto.data.EditableDto;

/**
 * Created on 16/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class SaveContentEditUIAdapter<D extends EditableDto, U extends ContentEditUI<D, U>> implements SaveUIAdapter<D, U> {

    @Override
    public void adaptUi(DataSourceEditor dataSourceEditor, U ui, boolean notPersisted, D savedBean) {

        ContentEditUIModel<D> model = ui.getModel();
        ui.stopEdit();

        ContentEditUINavigationNode node = model.getSource(); //??? .upToReferenceNode(model.getSource().getScope().getMainReferenceType());

        node.updateReference(savedBean.getId());

        //FIXME A startEdit should do the math?
        dataSourceEditor.getNavigationUI().getTree().reSelectSafeNodeThen(node, () -> {
            dataSourceEditor.getModel().resetFromPreviousUi(ui);
            if (notPersisted) {
                dataSourceEditor.getNavigationUI().getTree().expandPath(dataSourceEditor.getNavigationUI().getTree().getSelectionPath());
            }
        });

    }
}
