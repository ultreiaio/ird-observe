package fr.ird.observe.client.datasource.editor.api.content.data.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowTechnicalInformations;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeProducer;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.actions.FixData;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.actions.GotoData;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.SimpleDto;

import javax.swing.JButton;
import javax.swing.JComponent;
import java.awt.BorderLayout;
import java.awt.Container;
import java.util.function.Function;

/**
 * Created on 30/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ContentLayoutUIHandler<D extends SimpleDto, U extends ContentLayoutUI<D, U>> extends ContentUIHandler<U> {

    protected abstract void installCreateNewAction();

    protected abstract void installDeleteAction();

    protected abstract void installMoveAction();

    protected abstract void installPanelsAction();

    @Override
    public ContentLayoutUIModel<D> getModel() {
        return ui.getModel();
    }

    @Override
    protected Container computeFocusOwnerContainer() {
        ContentLayoutUIModelStates<D> states = getModel().getStates();
        if (!states.isShowData()) {
            return ui.getHideForm();
        }
        if (ContentMode.UPDATE.equals(states.getMode())) {
            return ui.getTitleRightToolBar();
        }
        return ui;
    }

    @Override
    protected ContentLayoutUIInitializer<U> createContentUIInitializer(U ui) {
        return new ContentLayoutUIInitializer<>(ui);
    }

    @Override
    protected ContentOpenWithValidator<U> createContentOpen(U ui) {
        ContentLayoutUIOpenExecutor<D, U> executor = new ContentLayoutUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor);
    }

    @Override
    public final ContentOpenWithValidator<U> getContentOpen() {
        return (ContentOpenWithValidator<U>) super.getContentOpen();
    }

    @Override
    public void initActions() {
        FixData.installAction(ui);
        installPanelsAction();
        installCreateNewAction();
        installDeleteAction();
        installMoveAction();
        ShowTechnicalInformations.installAction(ui);
    }

    @Override
    public void installChangeModeAction() {
        ChangeModeRequest request = ChangeModeRequest.of(ui);
        ChangeModeExecutor<U> executor = new ChangeModeExecutor<>();
        ChangeModeProducer<U> changeModeProducer = new ChangeModeProducer<>(ui, request, ContentLayoutUIModelStates.PROPERTY_SHOW_DATA) {
            @Override
            protected ContentMode rebuildChangeMode(ContentMode newValue) {
                ContentLayoutUIModelStates<D> source = ui.getStates();
                boolean showData = source.isShowData();
                if (!showData) {
                    newValue = null;
                }
                return newValue;
            }
        };
        ChangeMode<U> changeMode = new ChangeMode<>(request, changeModeProducer, executor) {
            protected boolean rebuildZonePredicate(U ui) {
                return request.isEditable();
            }

            @Override
            public void rebuildEditableZone(U ui) {
                JComponent body = ui.getContentBody();
                body.remove(ui.getShowForm());
                body.remove(ui.getHideForm());
                body.remove(ui.getEmptyForm());
                body.add(ui.getShowForm(), BorderLayout.CENTER);
                if (ui.getStates().isReadingMode()) {
                    ui.getActions().setVisible(false);
                    super.rebuildNotEditableZone(ui);
                } else {
                    ui.getActions().setVisible(true);
                    super.rebuildEditableZone(ui);
                }
                ui.getHandler().getDataSourceEditor().getMessageView().setVisible(false);
            }

            @Override
            protected void rebuildNotEditableZone(U ui) {
                boolean showData = ui.getStates().isShowData();
                JComponent body = ui.getContentBody();
                body.remove(ui.getShowForm());
                body.remove(ui.getHideForm());
                body.remove(ui.getEmptyForm());
                if (!showData) {
                    body.add(ui.getHideForm(), BorderLayout.CENTER);
                }
                ui.getActions().setVisible(false);
                super.rebuildNotEditableZone(ui);
            }
        };
        ChangeMode.installAction(ui, changeMode);
    }

    @Override
    public void onPrepareValidationContext() {
        super.onPrepareValidationContext();
        ui.getValidator().setContext(getModel().getStates().getValidationContext());
    }

    protected <X extends NavigationNode> void initGotoAction(JButton gotoAction, Function<NavigationNode, X> nodeType, int number) {
        GotoData.installAction(ui, gotoAction, nodeType, ObserveKeyStrokesEditorApi.getFunctionKeyStroke(number + 12));
    }
}

