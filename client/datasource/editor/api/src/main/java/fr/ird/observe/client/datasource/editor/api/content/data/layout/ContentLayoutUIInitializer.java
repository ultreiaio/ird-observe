package fr.ird.observe.client.datasource.editor.api.content.data.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.decoration.ObserveI18nDecoratorHelper;
import org.jdesktop.swingx.JXTitledPanel;

import javax.swing.JLabel;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 30/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ContentLayoutUIInitializer<UI extends ContentLayoutUI<?, ?>> extends ContentUIInitializer<UI> {

    protected ContentLayoutUIInitializer(UI ui) {
        super(ui);
    }

    @Override
    public DefaultUIInitializerResult initUI() {
        DefaultUIInitializerResult result = super.initUI();
        UIHelper.setLayerUI(ui.getContentBody(), null);
        UIHelper.setLayerUI(ui.getShowForm(), ui.getBlockLayerUI());

        String label = ui.getModel().getSource().getScope().getI18nTranslation("type");
        ui.getHideFormInformation().setText(t("observe.data.Trip.title.can.not.create.trip.sub.data", label));
        return result;
    }

    @Override

    protected void init(JXTitledPanel editor) {
        @SuppressWarnings("unchecked") Class<? extends DataDto> dataType = (Class<? extends DataDto>) editor.getClientProperty("type");
        JLabel label = (JLabel) editor.getLeftDecoration();
        label.setIcon(DtoIconHelper.getIcon(dataType));
        label.setText(ObserveI18nDecoratorHelper.getType(dataType));
    }

}
