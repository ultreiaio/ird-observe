package fr.ird.observe.client.datasource.editor.api.content.data.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeleteRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowIdRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.layout.MoveLayoutRequestBuilderStepConfigure;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveRequest;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUIModelSupport;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.ObserveServicesProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created on 30/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ContentLayoutUIModel<D extends SimpleDto> extends ContentSimpleUIModelSupport<D> {

    private static final Logger log = LogManager.getLogger(ContentLayoutUIModel.class);

    public ContentLayoutUIModel(NavigationNode source) {
        super(source);
    }

    public abstract Form<D> loadForm(ObserveServicesProvider servicesProvider, String id);

    @Override
    public ContentLayoutUINavigationNode getSource() {
        return (ContentLayoutUINavigationNode) super.getSource();
    }

    @Override
    public ContentLayoutUIModelStates<D> getStates() {
        return (ContentLayoutUIModelStates<D>) super.getStates();
    }

    public final ShowIdRequest toShowIdRequest() {
        return new ShowIdRequest(getSource()::getReference);
    }

    public abstract MoveLayoutRequestBuilderStepConfigure toMoveRequest();

    public abstract DeleteRequestBuilder.StepBuild toDeleteRequest();

    @Override
    public SaveRequest<D> toSaveRequest() {
        return null;
    }

    @Override
    public final Form<D> openForm(ObserveServicesProvider servicesProvider, String selectedId) {
        open();
        log.info(String.format("%s Load Form from id: %s", getPrefix(), selectedId));
        Form<D> form = loadForm(servicesProvider, selectedId);
        getStates().openForm(form);
        return form;
    }

    @Override
    public boolean isCreateNewDataActionEnabled(Class<? extends DataDto> dtoType, Boolean updatingAndNotModified, String showDataPropertyName) {
        return updatingAndNotModified != null && updatingAndNotModified && getStates().isShowData();
    }
}
