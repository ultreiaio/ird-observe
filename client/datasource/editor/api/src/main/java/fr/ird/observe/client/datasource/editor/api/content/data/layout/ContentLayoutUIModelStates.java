package fr.ird.observe.client.datasource.editor.api.content.data.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUIModelStatesSupport;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.SimpleDto;

/**
 * Created on 30/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ContentLayoutUIModelStates<D extends SimpleDto> extends ContentSimpleUIModelStatesSupport<D> {

    public ContentLayoutUIModelStates(ContentLayoutUIModel<D> model, D bean, String selectedId) {
        super(model, bean, selectedId);
        DecoratorService decoratorService = getDecoratorService();
        decoratorService.installDecorator(bean);
    }

    @Override
    public ContentLayoutUINavigationNode source() {
        return (ContentLayoutUINavigationNode) super.source();
    }

    @Override
    public void open(ContentUIModel model) {
        super.open(model);
        @SuppressWarnings("unchecked") ContentLayoutUIModel<D> realModel = (ContentLayoutUIModel<D>) model;
        ContentLayoutUINavigationNode source = realModel.getSource();
        // Compute showData property
        boolean showData = source.getInitializer().isShowData();
        setShowData(showData);
    }
}

