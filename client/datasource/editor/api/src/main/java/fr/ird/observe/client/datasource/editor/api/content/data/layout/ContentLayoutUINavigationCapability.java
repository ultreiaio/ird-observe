package fr.ird.observe.client.datasource.editor.api.content.data.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ContainerCapability;
import fr.ird.observe.dto.reference.DtoReference;

import java.util.List;
import java.util.Objects;

/**
 * Created on 30/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ContentLayoutUINavigationCapability<N extends ContentLayoutUINavigationNode> implements ContainerCapability<N> {

    private final N node;

    public ContentLayoutUINavigationCapability(N node) {
        this.node = Objects.requireNonNull(node);
    }

    protected abstract List<Class<? extends DtoReference>> getAcceptedTypes();

    public boolean acceptChildReferenceType(Class<? extends DtoReference> dtoReferenceType) {
        return getAcceptedTypes().contains(dtoReferenceType);
    }

    @Override
    public N getNode() {
        return node;
    }
}
