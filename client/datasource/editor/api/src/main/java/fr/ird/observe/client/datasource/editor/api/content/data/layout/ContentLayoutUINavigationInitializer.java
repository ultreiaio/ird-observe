package fr.ird.observe.client.datasource.editor.api.content.data.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import io.ultreia.java4all.decoration.Decorator;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created on 30/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ContentLayoutUINavigationInitializer extends NavigationInitializer<ContentLayoutUINavigationContext> {
    private final Function<DtoReference, Boolean> showDataFunction;
    private final Function<DataDtoReference, String> textFunction;
    private final Supplier<? extends DataDtoReference> reference;
    private Decorator decorator;
    private String text;
    private Boolean showData;

    public ContentLayoutUINavigationInitializer(NavigationScope scope, Supplier<? extends DataDtoReference> reference) {
        super(scope);
        this.reference = Objects.requireNonNull(reference);
        String showDataPropertyName = scope.getShowDataPropertyName();
        this.showDataFunction = showDataPropertyName == null ? e -> true : e -> e.get(showDataPropertyName);
        this.textFunction = t -> computeText();
    }

    @Override
    protected Object init(NavigationContext<ContentLayoutUINavigationContext> context) {
        decorator = context.getDecoratorService().getDecoratorByType(getScope().getParentReferenceType(), getScope().getDecoratorClassifier());
        return getScope().getMainType();
    }

    @Override
    protected void open(NavigationContext<ContentLayoutUINavigationContext> context) {
    }

    @Override
    protected void reload(NavigationContext<ContentLayoutUINavigationContext> context) {
        showData = null;
        text = null;
    }

    @Override
    public String toString() {
        return super.toString() + "" + getScope().getMainType().getName();
    }

    @Override
    public String toPath() {
        return getScope().getNodeDataType().getName();
    }

    public DataDtoReference getReference() {
        return reference.get();
    }

    public String getSelectedId() {
        return getReference().getId();
    }

    public boolean isShowData() {
        return showData == null ? showData = showDataFunction.apply(getReference()) : showData;
    }

    public String getText() {
        return text == null ? text = textFunction.apply(getReference()) : text;
    }

    public boolean isNotPersisted() {
        // can't have a parent not persisted
        return false;
    }

    private String computeText() {
        String result;
        if (isShowData()) {
            // add statistics
            result = decorator.decorate(getReference());
        } else {
            result = getScope().getI18nTranslation("type");
        }
        return result;
    }
}

