package fr.ird.observe.client.datasource.editor.api.content.data.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.reference.DataDtoReference;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 30/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ContentLayoutUINavigationNode extends NavigationNode {

    public static <N extends ContentLayoutUINavigationNode> N init(N node, Supplier<? extends DataDtoReference> reference) {
        ContentLayoutUINavigationInitializer initializer = new ContentLayoutUINavigationInitializer(Objects.requireNonNull(node).getScope(), reference);
        node.init(initializer);
        return node;
    }

    @Override
    public ContentLayoutUINavigationInitializer getInitializer() {
        return (ContentLayoutUINavigationInitializer) super.getInitializer();
    }

    @Override
    public ContentLayoutUINavigationContext getContext() {
        return (ContentLayoutUINavigationContext) super.getContext();
    }

    @Override
    public ContentLayoutUINavigationHandler<?> getHandler() {
        return (ContentLayoutUINavigationHandler<?>) super.getHandler();
    }

    @Override
    public ContentLayoutUINavigationCapability<?> getCapability() {
        return (ContentLayoutUINavigationCapability<?>) super.getCapability();
    }

    public DataDtoReference getReference() {
        return getInitializer().getReference();
    }
}

