package fr.ird.observe.client.datasource.editor.api.content.data.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentEditExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.form.Form;

/**
 * Created on 30/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ContentLayoutUIOpenExecutor<D extends SimpleDto, U extends ContentLayoutUI<D, U>> implements ContentOpenExecutor<U>, ContentEditExecutor<U> {

    @Override
    public void loadContent(U ui) {
        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();
        contentOpen.resetCoordinateEditors();
        //FIXME chemit 20100913 : il vaudrait le faire uniquement lors de l'édition
        // chaque arrive sur un écran invalide le cache de validation
        getClientValidationContext().reset();

        ContentUIHandler.removeAllMessages(ui);

        openModel(ui);

    }

    @Override
    public void onOpened(U ui) {
        ContentLayoutUIHandler<D, U> handler = ui.getHandler();
        handler.fixFormSize();
        handler.onEndOpenUI();
    }

    public void openModel(U ui) {
        ContentLayoutUIModel<D> model = ui.getModel();
        if (!model.getScope().isStandalone()) {
            return;
        }
        ContentLayoutUIHandler<D, U> handler = ui.getHandler();
        ContentLayoutUIModelStates<D> states = model.getStates();
        ContentOpenWithValidator<U> contentOpen = handler.getContentOpen();
        Form<D> form = model.openForm(handler, states.getSelectedId());
        contentOpen.onOpenForm(form);
        handler.onOpenAfterOpenModel();
    }

    @Override
    public void startEdit(U ui) {
        ContentLayoutUIModelStates<D> states = ui.getModel().getStates();
        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();
        contentOpen.prepareValidationContext();
        contentOpen.installValidators(states.getBean());
        contentOpen.startEditTabUIModel();
        states.setModified(false);
        //FIXME:Focus Normalement pas besoin de faire ça car la methode est toujours appelée dans open
//        if (model.isCreatingMode()) {
//            grabFocusOnForm();
//        }
    }

    @Override
    public void stopEdit(U ui) {
        ContentLayoutUIModelStates<D> states = ui.getModel().getStates();
        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();
        ClientValidationContext context = getClientValidationContext();

        contentOpen.stopEditTabUIModel();

        context.reset();
        ui.setValidatorChanged(false);

        // mark ui as not editing
        states.setEditing(false);

        // mark ui as valid while not editing
        states.setValid(true);

        // mark ui as not modified
        states.setModified(false);

        // detach all validators
        contentOpen.uninstallValidators();
    }
}

