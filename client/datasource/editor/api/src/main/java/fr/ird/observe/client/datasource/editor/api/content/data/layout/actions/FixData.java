package fr.ird.observe.client.datasource.editor.api.content.data.layout.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewPredicate;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewPredicates;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUI;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUIModelStates;
import fr.ird.observe.dto.data.SimpleDto;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.SwingConstants;
import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 30/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class FixData<D extends SimpleDto, U extends ContentLayoutUI<D, U>> extends ContentLayoutUIAction<D, U> {

    private final CreateNewPredicate<ContentUI, ?, ContentUI> typePredicate;

    public static <D extends SimpleDto, U extends ContentLayoutUI<D, U>> void installAction(U ui) {
        FixData<D, U> action = new FixData<>(ui.getModel().getSource().getScope().getMainType());
        init(ui, Objects.requireNonNull(ui).getFix(), action);
        ui.getModel().getStates().addPropertyChangeListener(ContentLayoutUIModelStates.PROPERTY_SHOW_DATA, evt -> {
            boolean showData = (boolean) evt.getNewValue();
            if (!showData) {
                action.prepareContent(ui);
            }
        });
    }

    public FixData(Class<D> mainDataType) {
        super(FixData.class.getName(), null, null, "generate", ObserveKeyStrokesEditorApi.KEY_STROKE_FIX, mainDataType);
        setText(I18n.t("observe.data.Trip.choice.go.to.trip"));
        this.typePredicate = CreateNewPredicates.getPredicate(mainDataType);
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        return typePredicate != null && super.canExecuteAction(e);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        Objects.requireNonNull(typePredicate).applyFix(ui);
    }

    private void prepareContent(U ui) {
        if (typePredicate == null) {
            ui.getHideForm().removeAll();
            ui.getHideFormInformation().setHorizontalAlignment(SwingConstants.CENTER);
            ui.getHideForm().add(ui.getHideFormInformation());
        } else {
            typePredicate.setDtoLabel(getDataType());
            ui.getHideFormInformation().setText(t("observe.data.Trip.title.can.not.create.trip.sub.data", typePredicate.getDtoLabel()));
            ui.getHideFormContent().setText(typePredicate.getMessage());
        }
    }
}
