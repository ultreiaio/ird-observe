package fr.ird.observe.client.datasource.editor.api.content.data.layout.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUI;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.SimpleDto;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.JButton;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.util.Objects;
import java.util.function.Function;

/**
 * Created on 01/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class GotoData<X extends NavigationNode, D extends SimpleDto, U extends ContentLayoutUI<D, U>> extends ContentLayoutUIAction<D, U> {

    private final Function<NavigationNode, X> nodeType;

    public static <X extends NavigationNode, D extends SimpleDto, U extends ContentLayoutUI<D, U>> void installAction(U ui, JButton editor, Function<NavigationNode, X> nodeType, KeyStroke keyStroke) {
        GotoData<X, D, U> action = new GotoData<>(ui.getModel().getSource().getScope().getMainType(), nodeType, keyStroke);
        init(ui, Objects.requireNonNull(editor), action);
    }

    public GotoData(Class<D> dataType, Function<NavigationNode, X> nodeType, KeyStroke keyStroke) {
        super(GotoData.class.getName() + "#" + keyStroke, null, null, "go-down", keyStroke, dataType);
        this.nodeType = nodeType;
        setText(I18n.t("observe.Common.action.goto.data"));
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        NavigationTree tree = getDataSourceEditor().getNavigationUI().getTree();
        NavigationNode selectedNode = tree.getSelectedNode();
        X childrenReferenceNode = nodeType.apply(selectedNode);
        tree.selectSafeNode(childrenReferenceNode);
    }
}

