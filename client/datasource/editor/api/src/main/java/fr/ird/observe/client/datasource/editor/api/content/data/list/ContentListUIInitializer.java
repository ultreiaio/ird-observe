package fr.ird.observe.client.datasource.editor.api.content.data.list;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.ui.ReferenceListFromMultipleContainerNodeCellRenderer;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.reference.DataDtoReference;
import io.ultreia.java4all.jaxx.widgets.list.ListHeader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JList;
import java.util.Collections;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * To initialize ui.
 * <p>
 * Created on 9/26/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
class ContentListUIInitializer<R extends DataDtoReference, UI extends ContentListUI<?, R, ?>> extends ContentUIInitializer<UI> {

    private static final Logger log = LogManager.getLogger(ContentListUIInitializer.class);

    ContentListUIInitializer(UI ui) {
        super(ui);
    }

    @Override
    public DefaultUIInitializerResult initUI() {
        DefaultUIInitializerResult result = super.initUI();
        ContentListUIModel<?> model = ui.getModel();
        ui.getListHeader().setLabelText(model.getSource().getScope().getI18nTranslation("list.title"));
        String type = model.getSource().getScope().getI18nTranslation("type");
        ui.getHideFormInformation().setText(t("observe.data.Trip.title.can.not.create.trip.sub.data", type));
        ui.getEmptyFormInformation().setText(String.format(model.getSource().getScope().getI18nTranslation("list.message.none"), type));
        // set list renderer
        JList<R> list = ui.getList();
        list.setFixedCellHeight(24);
        list.setFixedCellWidth(200);
        NavigationTree tree = getMainUI().getMainUIBodyContentManager().getBody(DataSourceEditor.class).get().getNavigationUI().getTree();
        ReferenceListFromMultipleContainerNodeCellRenderer renderer2 = new ReferenceListFromMultipleContainerNodeCellRenderer(tree, ui.getListHeader().getHandler().getDecorator());
        list.setCellRenderer(renderer2);

        //noinspection unchecked
        model.getStates().addPropertyChangeListener(ContentListUIModelStates.PROPERTY_DATA, e -> updateList((List<R>) e.getNewValue()));

        // init renderer
        renderer2.init();
        return result;
    }

    private void updateList(List<R> data) {
        ListHeader<R> list = ui.getListHeader();
        if (data != null && !data.isEmpty()) {
            log.debug(list.getName() + " - " + data.size());
            list.setData(data);
        } else {
            list.setData(Collections.emptyList());
        }
    }

}
