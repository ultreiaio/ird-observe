/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.data.list;

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeleteRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowIdRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRequestBuilderStepConfigure;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.navigation.id.IdNode;

/**
 * Le modèle pour un écran d'édition avec des fils.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since .14
 */
public abstract class ContentListUIModel<R extends DataDtoReference> extends ContentUIModel {

    public ContentListUIModel(NavigationNode source) {
        super(source);
    }

    public abstract MoveRequestBuilderStepConfigure toMoveRequest();

    public abstract DeleteRequestBuilder.StepSetExtra toDeleteRequest();

    @Override
    public ContentListUINavigationNode getSource() {
        return (ContentListUINavigationNode) super.getSource();
    }

    @SuppressWarnings("unchecked")
    @Override
    public ContentListUIModelStates<R> getStates() {
        return (ContentListUIModelStates<R>) super.getStates();
    }

    public final ShowIdRequest getShowIdRequest() {
        return new ShowIdRequest(getStates()::getSafeSelectedData);
    }

    public final ChangeModeRequest toChangeModeRequest() {
        return new ChangeModeRequest(getSource().getScope().getI18nTranslation("type")) {
            @Override
            protected IdNode<? extends IdDto> getSelectNode(ContentUI ui) {
                return getObserveSelectModel().forNavigationNode(getEditNode(ui)).orElseThrow(IllegalStateException::new);
            }

            @Override
            protected IdNode<? extends IdDto> getEditNode(ContentUI ui) {
                return getSource().getInitializer().getEditNode();
            }

            @Override
            protected String getSelectedId(ContentUI ui, IdNode<?> selectedNode) {
                return getStates().getSelectedData().map(DtoReference::getId).orElse(null);
            }
        };
    }
}
