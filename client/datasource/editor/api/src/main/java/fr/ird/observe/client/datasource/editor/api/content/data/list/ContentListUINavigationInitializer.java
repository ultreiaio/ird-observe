package fr.ird.observe.client.datasource.editor.api.content.data.list;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.services.service.data.EmptyChildrenDataReferenceSet;
import io.ultreia.java4all.decoration.Decorator;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created on 25/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentListUINavigationInitializer extends NavigationInitializer<ContentListUINavigationContext> {
    /**
     * How to get show data information.
     */
    private final Function<DtoReference, Boolean> showDataFunction;
    private final EmptyChildrenDataReferenceSet<? extends OpenableDto, ? extends DataDtoReference> references;
    private final Supplier<? extends DtoReference> parentReference;
    private Boolean showData;
    private Decorator decorator;

    public ContentListUINavigationInitializer(NavigationScope scope, Supplier<? extends DtoReference> parentReference, EmptyChildrenDataReferenceSet<? extends OpenableDto, ? extends DataDtoReference> references) {
        super(scope);
        this.parentReference = parentReference;
        String showDataPropertyName = scope.getShowDataPropertyName();
        this.showDataFunction = showDataPropertyName == null ? e -> true : e -> e.get(showDataPropertyName);
        this.references = references;
    }

    @Override
    protected Object init(NavigationContext<ContentListUINavigationContext> context) {
        decorator = context.getDecoratorService().getDecoratorByType(getScope().getMainReferenceType(), getScope().getDecoratorClassifier());
        return getScope().getMainType();
    }

    @Override
    protected void open(NavigationContext<ContentListUINavigationContext> context) {
        if (getScope().isSelectNode()) {
            updateSelectNodeId(getSelectedParentId());
        }
        references.get().stream().forEach(e -> e.registerDecorator(decorator));
    }

    @Override
    protected void reload(NavigationContext<ContentListUINavigationContext> context) {
        references.reload();
        showData = null;
    }

    public boolean isShowData() {
        return showData == null ? showData = showDataFunction.apply(getParentReference()) : showData;
    }

    @Override
    public String toPath() {
        return getScope().getNodeDataType().getName();
    }

    public DtoReference getParentReference() {
        return parentReference.get();
    }

    public final String getSelectedParentId() {
        return getParentReference().getId();
    }

    public List<? extends DataDtoReference> getReferences() {
        return references.get().toArrayList();
    }

    public List<? extends DataDtoReference> getReferencesWithout(Set<String> idsToExclude) {
        //FIXME Add this in framework (.without(idsToExclude)
        return references.get().stream().filter(r -> !idsToExclude.contains(r.getId())).collect(Collectors.toList());
    }

    public final boolean isOpen() {
        return Objects.equals(getSelectedParentId(), getEditNode().getParent().getId());
    }

    @SuppressWarnings("unchecked")
    public <R extends DataDtoReference> Comparator<R> getReferenceComparator() {
        return (Comparator<R>) references.getComparator();
    }

    public int getInitialCount() {
        return references.getInitialCount();
    }
}

