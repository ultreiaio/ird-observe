package fr.ird.observe.client.datasource.editor.api.content.data.list;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.services.service.data.EmptyChildrenDataReferenceSet;

import java.awt.Font;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created on 07/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentListUINavigationNode extends NavigationNode {

    public static <D extends OpenableDto, R extends DataDtoReference, N extends ContentListUINavigationNode> N init(N node, Supplier<? extends DtoReference> parentId, EmptyChildrenDataReferenceSet<D, R> references) {
        ContentListUINavigationInitializer initializer = new ContentListUINavigationInitializer(node.getScope(), parentId, references);
        node.init(initializer);
        return node;
    }

    @Override
    public ContentListUINavigationInitializer getInitializer() {
        return (ContentListUINavigationInitializer) super.getInitializer();
    }

    @Override
    public ContentListUINavigationContext getContext() {
        return (ContentListUINavigationContext) super.getContext();
    }

    @Override
    public ContentListUINavigationHandler<?> getHandler() {
        return (ContentListUINavigationHandler<?>) super.getHandler();
    }

    @Override
    public ContentListUINavigationCapability<?> getCapability() {
        return (ContentListUINavigationCapability<?>) super.getCapability();
    }

    public List<? extends DataDtoReference> getReferences() {
        return getInitializer().getReferences();
    }

    public DtoReference getParentReference() {
        return getInitializer().getParentReference();
    }

    public NavigationNode addMissingChildren(String newIdToSelect) {
        ContentListUINavigationInitializer initializer = getInitializer();
        Set<String> existingIds = getChildrenReferences(getScope().getMainReferenceType()).stream().map(IdDto::getId).collect(Collectors.toSet());
        List<? extends DataDtoReference> references = initializer.getReferencesWithout(existingIds);
        ContentListUINavigationCapability<?> capability = getCapability();
        NavigationNode result = null;
        for (DataDtoReference reference : references) {
            capability.insertChildNode(reference);
        }
        if (newIdToSelect != null) {
            result = findChildById(newIdToSelect);
        }
        return result;
    }

    @Override
    public Font getNodeFont(Font defaultFont) {
        if (!getInitializer().isOpen()) {
            return defaultFont.deriveFont(Font.PLAIN);
        }
        IdNode<? extends IdDto> editNode = getInitializer().getEditNode();
        if (editNode.isDisabled()) {
            return defaultFont.deriveFont(Font.PLAIN);
        }
        String parentEditId = editNode.getParent().getId();
        if (Objects.equals(parentEditId, getParentReference().getId())) {
            return defaultFont.deriveFont(Font.BOLD);
        }
        return defaultFont.deriveFont(Font.PLAIN);
    }
}
