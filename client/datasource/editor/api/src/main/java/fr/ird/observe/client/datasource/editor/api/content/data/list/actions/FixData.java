package fr.ird.observe.client.datasource.editor.api.content.data.list.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewPredicate;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewPredicates;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUI;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUIModelStates;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.JTextPane;
import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Created on 14/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class FixData<D extends OpenableDto, R extends DataDtoReference, U extends ContentListUI<D, R, U>> extends ContentListUIActionSupport<D, R, U> {

    private final CreateNewPredicate<ContentUI, D, ContentUI> typePredicate;

    public static <D extends OpenableDto, R extends DataDtoReference, U extends ContentListUI<D, R, U>> void installAction(U ui) {
        FixData<D, R, U> action = new FixData<>(ui.getModel().getSource().getScope().getMainType());
        init(ui, Objects.requireNonNull(ui).getFix(), action);
        ui.getModel().getStates().addPropertyChangeListener(ContentListUIModelStates.PROPERTY_SHOW_DATA, evt -> {

            boolean showData = (boolean) evt.getNewValue();
            if (!showData) {
                action.prepareContent(ui);
            }
        });
    }

    public FixData(Class<D> dataType) {
        super(dataType, null, null, "generate", ObserveKeyStrokesEditorApi.KEY_STROKE_FIX);
        setText(I18n.t("observe.data.Trip.choice.go.to.trip"));
        this.typePredicate = CreateNewPredicates.getPredicate(dataType);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        typePredicate.applyFix(ui);
    }

    private void prepareContent(U ui) {
        JTextPane content = ui.getHideFormContent();
        typePredicate.setDtoLabel(getDataType());
        content.setText(typePredicate.getMessage());
    }
}

