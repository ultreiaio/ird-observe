/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.data.list.actions;

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUI;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;

import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public final class GotoOpenData<D extends OpenableDto, R extends DataDtoReference, U extends ContentListUI<D, R, U>> extends ContentListUIActionSupport<D, R, U> {

    public static <D extends OpenableDto, R extends DataDtoReference, U extends ContentListUI<D, R, U>> void installAction(U ui) {
        GotoOpenData<D, R, U> action = new GotoOpenData<>(ui.getModel().getSource().getScope().getMainType());
        init(ui, Objects.requireNonNull(ui).getGotoOpen(), action);
    }

    public GotoOpenData(Class<D> dataType) {
        super(dataType, null, null, "go-down", ObserveKeyStrokesEditorApi.KEY_STROKE_GO_TO_OPEN);
        setText(I18n.t("observe.Common.action.goto.open"));
        setTooltipText(ObserveBusinessProject.get().gotoOpenDataToolTipText(dataType));
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        IdNode<?> idNode = ui.getModel().getSource().getInitializer().getEditNode();
        NavigationTree tree = getDataSourceEditor().getNavigationUI().getTree();
        NavigationNode selectedNode = tree.getSelectedNode();
        NavigationNode childrenReferenceNode = selectedNode.findChildById(idNode.getId());
        tree.selectSafeNode(childrenReferenceNode);
    }

}
