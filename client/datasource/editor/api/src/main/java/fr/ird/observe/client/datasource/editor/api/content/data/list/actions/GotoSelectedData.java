package fr.ird.observe.client.datasource.editor.api.content.data.list.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUI;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUIModel;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.Objects;

public final class GotoSelectedData<D extends OpenableDto, R extends DataDtoReference, U extends ContentListUI<D, R, U>> extends ContentListUIActionSupport<D, R, U> {

    public static <D extends OpenableDto, R extends DataDtoReference, U extends ContentListUI<D, R, U>> void installAction(U ui) {
        GotoSelectedData<D, R, U> action = new GotoSelectedData<>(ui.getModel().getSource().getScope().getMainType());
        init(ui, Objects.requireNonNull(ui).getGotoSelected(), action);
    }

    public GotoSelectedData(Class<D> dataType) {
        super(dataType, null, null, "go-down", ObserveKeyStrokesEditorApi.KEY_STROKE_ALT_ENTER);
        setText(I18n.t("observe.Common.action.goto.selected"));
        setTooltipText(ObserveBusinessProject.get().gotoSelectedDataToolTipText(dataType));
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {

        ContentListUIModel<?> model = ui.getModel();
        boolean oneSelectedData = model.getStates().isOneSelectedData();
        if (!oneSelectedData) {
            return;
        }

        DataDtoReference dataReference = model.getStates().getSelectedDatas().get(0);
        Objects.requireNonNull(dataReference);

        NavigationTree tree = getDataSourceEditor().getNavigationUI().getTree();

        NavigationNode selectedNode = tree.getSelectedNode();
        NavigationNode nodeToSelect = selectedNode.findChildById(dataReference.getId());

        SwingUtilities.invokeLater(() -> tree.selectSafeNode(nodeToSelect));
    }
}
