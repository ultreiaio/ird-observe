package fr.ird.observe.client.datasource.editor.api.content.data.map;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.I18n;
import org.geotools.styling.Style;
import org.locationtech.jts.geom.Coordinate;
import org.opengis.feature.simple.SimpleFeature;

import java.awt.Color;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveMapPaneLegendItem {

    protected static final int LEGEND_ITEM_HEIGHT = 20;
    protected static final int LEGEND_SYMBOL_WIDTH = 50;
    protected static final int LEGEND_MARGIN = 5;
    protected static final Color LEGEND_BACKGROUND = new Color(1f, 1f, 1f, 0.8f);
    protected static final Color LEGEND_TEXT_NOT_SELECTED = Color.RED;
    protected final SimpleFeature simpleFeature;
    protected final Style style;
    protected final String label;
    protected final int notValidCount;
    protected final int validCount;
    protected final String name;
    protected boolean selected;

    public ObserveMapPaneLegendItem(String name, SimpleFeature simpleFeature, Style style, String label, int notValidCount, int validCount, boolean selected) {
        this.name = name;
        this.simpleFeature = simpleFeature;
        this.style = style;
        this.label = label;
        this.notValidCount = notValidCount;
        this.validCount = validCount;
        this.selected = selected;
    }

    public static Coordinate[] lineCoordinates() {
        Coordinate[] coordinates = new Coordinate[2];
        coordinates[0] = new Coordinate(LEGEND_MARGIN, LEGEND_ITEM_HEIGHT / 2.);
        coordinates[1] = new Coordinate(LEGEND_SYMBOL_WIDTH - LEGEND_MARGIN, LEGEND_ITEM_HEIGHT / 2.);
        return coordinates;
    }

    public static Coordinate pointCoordinates() {
        return new Coordinate(LEGEND_SYMBOL_WIDTH / 2., LEGEND_ITEM_HEIGHT / 2.);
    }

    public String getName() {
        return name;
    }

    public SimpleFeature getSimpleFeature() {
        return simpleFeature;
    }

    public Style getStyle() {
        return style;
    }

    public String getLabel(boolean addPointCount) {
        String label = this.label;
        if (addPointCount && validCount > 0) {
            if (validCount == 1) {
                label += " " + I18n.t("observe.ui.datasource.editor.content.map.legend.valid.count");
            } else {
                label += " " + I18n.t("observe.ui.datasource.editor.content.map.legend.valid.counts", validCount);
            }
        }
        if (notValidCount > 0) {
            if (notValidCount == 1) {
                label += " " + I18n.t("observe.ui.datasource.editor.content.map.legend.not.valid.count");
            } else {
                label += " " + I18n.t("observe.ui.datasource.editor.content.map.legend.not.valid.counts", notValidCount);

            }
        }
        return label;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
