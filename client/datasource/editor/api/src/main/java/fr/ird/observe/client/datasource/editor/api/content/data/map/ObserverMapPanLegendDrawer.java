package fr.ird.observe.client.datasource.editor.api.content.data.map;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.geotools.geometry.jts.LiteShape;
import org.geotools.legend.Drawer;
import org.geotools.styling.LineSymbolizer;
import org.geotools.styling.Rule;
import org.geotools.styling.SLD;
import org.geotools.styling.Symbolizer;
import org.locationtech.jts.geom.Geometry;
import org.opengis.feature.simple.SimpleFeature;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserverMapPanLegendDrawer {

    protected final Drawer drawer;


    public ObserverMapPanLegendDrawer() {
        drawer = Drawer.create();

    }

    public void drawDirect(BufferedImage bi, SimpleFeature feature, Rule rule) {
        AffineTransform affineTransform = new AffineTransform();

        LiteShape shape = new LiteShape(null, affineTransform, false);
        for (Symbolizer symbolizer : rule.getSymbolizers()) {
            if (symbolizer instanceof LineSymbolizer) {
                LineSymbolizer lineSymbolizer = (LineSymbolizer) symbolizer;

                Geometry geometry = findGeometry(feature, lineSymbolizer);
                if (geometry != null) {

                    Graphics graphics = bi.getGraphics();
                    Graphics2D g = (Graphics2D) graphics;

                    shape.setGeometry(geometry);


                    Color c = SLD.color(lineSymbolizer);
                    int w = SLD.width(lineSymbolizer);
                    float[] lineDash = SLD.lineDash(lineSymbolizer);
                    if (c != null && w > 0) {
                        g.setColor(c);

                        Stroke str = new BasicStroke(
                                w,
                                BasicStroke.CAP_SQUARE,
                                BasicStroke.JOIN_MITER,
                                10.0f, lineDash, 0.0f
                        );
                        g.setStroke(str);

                        g.draw(shape);
                    }
                }
            } else {
                drawer.drawFeature(bi, feature, affineTransform, false, symbolizer, null, shape);
            }

        }


    }

    protected Geometry findGeometry(SimpleFeature feature, LineSymbolizer lineSymbolizer) {
        String geomName = lineSymbolizer.getGeometryPropertyName();

        Geometry geom;
        if (geomName == null) {
            geom = (Geometry) feature.getDefaultGeometry();
        } else {
            geom = (Geometry) feature.getAttribute(geomName);
        }
        return geom;
    }


}
