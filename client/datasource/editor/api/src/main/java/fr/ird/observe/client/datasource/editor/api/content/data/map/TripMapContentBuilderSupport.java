package fr.ird.observe.client.datasource.editor.api.content.data.map;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.TripMapConfig;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapContentBuilder;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.TripMapPointType;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.styling.NamedLayer;
import org.geotools.styling.SLD;
import org.geotools.styling.SLDParser;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.geotools.styling.StyledLayer;
import org.geotools.styling.StyledLayerDescriptor;
import org.geotools.styling.UserLayer;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class TripMapContentBuilderSupport implements TripMapContentBuilder {

    protected static final String OBSERVATION_POINTS_LAYER_NAME = "Observation points";

    protected static final String LOGBOOK_POINTS_LAYER_NAME = "Logbook points";
    protected static final Set<String> DEFAULT_VARIABLE_NAMES = Set.of(
            "mapLayer1Color",
            "mapLayer2Color",
            "mapLayer3Color",
            "mapLayer4Color",
            "mapLayer5Color"
    );
    private static final Logger log = LogManager.getLogger(TripMapContentBuilderSupport.class);
    protected static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd");

    protected final List<ObserveMapPaneLegendItem> legendItems;
    protected final Map<TripMapPointType, List<TripMapPoint>> validPoints = new EnumMap<>(TripMapPointType.class);
    protected final Map<TripMapPointType, List<TripMapPoint>> notValidPoints = new EnumMap<>(TripMapPointType.class);
    protected final Set<TripMapPointType> acceptedTripPointTypes;
    protected final SimpleFeatureBuilder lineBuilder;
    private final boolean withLabelLine;
    private final SimpleFeatureBuilder pointBuilder;
    protected MapContent mapContent;
    protected StyledLayerDescriptor styledLayerDescriptor;
    private GeometryFactory geometryFactory;

    public static TripMapContentBuilderSupport of(TripMapConfig config, TripMapConfigDto tripMapConfig, TripMapDto tripMapDto, List<TripMapPoint> tripMapPoints, Set<String> excludedFeatureNames) throws Exception {
        TripMapContentBuilderSupport builder = (TripMapContentBuilderSupport) config.getTripMapContentBuilders().stream().filter(t -> t.accept(tripMapDto)).findFirst().orElseThrow(IllegalStateException::new);
        builder.reset();
        String mapDateFormat = config.getMapDateFormat();

        try {
            DATE_FORMAT = new SimpleDateFormat(mapDateFormat);
        } catch (Exception e) {
            // bad date format
            log.error(String.format("Bad date format: %s, will use default one: MM-dd", mapDateFormat), e);
        }
        File styleFile = builder.getStyleFile(config.getMapDirectory().toPath());
        String content = Files.readString(styleFile.toPath());

        Map<String, String> replaceMapping = config.getVariables(builder);
        for (Map.Entry<String, String> entry : replaceMapping.entrySet()) {
            content = content.replaceAll("\\$\\{" + entry.getKey() + "}", entry.getValue());
        }

        Path newPath = config.getTemporaryDirectory().toPath().resolve("map-temp" + System.nanoTime());

        Files.write(newPath, content.getBytes(StandardCharsets.UTF_8));
        builder.setStyledLayerDescriptor(newPath.toFile());
        for (File layerFile : config.getMapLayerFiles()) {
            builder.addLayer(layerFile);
        }
        builder.addLines(tripMapConfig, tripMapPoints, excludedFeatureNames);
        builder.addPoints(tripMapConfig, tripMapPoints, excludedFeatureNames);
        return builder;
    }

    protected static SimpleFeatureBuilder createLineBuilder(boolean withLabel) {
        SimpleFeatureTypeBuilder lineFeatureTypeBuilder = new SimpleFeatureTypeBuilder();
        lineFeatureTypeBuilder.setName("lineBuilder");
        lineFeatureTypeBuilder.setCRS(DefaultGeographicCRS.WGS84);
        lineFeatureTypeBuilder.add("line", LineString.class);
        if (withLabel) {
            lineFeatureTypeBuilder.add("label", String.class);
        }
        lineFeatureTypeBuilder.add("type", String.class);
        return new SimpleFeatureBuilder(lineFeatureTypeBuilder.buildFeatureType());

    }

    protected static SimpleFeatureBuilder createPolygonBuilder(@SuppressWarnings("SameParameterValue") String name) {
        SimpleFeatureTypeBuilder polygonFeatureTypeBuilder = new SimpleFeatureTypeBuilder();
        polygonFeatureTypeBuilder.setName(name);
        polygonFeatureTypeBuilder.setCRS(DefaultGeographicCRS.WGS84);
        polygonFeatureTypeBuilder.add("zone", Polygon.class);
        polygonFeatureTypeBuilder.add("label", String.class);
        polygonFeatureTypeBuilder.add("type", String.class);
        SimpleFeatureType polygonFeatureType = polygonFeatureTypeBuilder.buildFeatureType();
        return new SimpleFeatureBuilder(polygonFeatureType);
    }

    protected static SimpleFeatureBuilder createPointBuilder() {

        SimpleFeatureTypeBuilder pointFeatureTypeBuilder = new SimpleFeatureTypeBuilder();
        pointFeatureTypeBuilder.setName("point Builder");
        pointFeatureTypeBuilder.setCRS(DefaultGeographicCRS.WGS84);
        pointFeatureTypeBuilder.add("point", Point.class);
        pointFeatureTypeBuilder.add("label", String.class);
        pointFeatureTypeBuilder.add("pointType", String.class);

        SimpleFeatureType pointFeatureType = pointFeatureTypeBuilder.buildFeatureType();
        return new SimpleFeatureBuilder(pointFeatureType);
    }

    public static Coordinate[] create(TripMapPoint... points) {
        return Arrays.stream(points).map(TripMapContentBuilderSupport::create).toArray(Coordinate[]::new);
    }

    protected static void fill(Coordinate[] array, TripMapPoint... points) {
        int index = 0;
        for (TripMapPoint point : points) {
            Coordinate coordinate = TripMapContentBuilderSupport.create(point);
            array[index++] = coordinate;
        }
    }

    public static Coordinate create(TripMapPoint point) {
        return new Coordinate(point.getLongitude(), point.getLatitude());
    }

    protected TripMapContentBuilderSupport(Set<TripMapPointType> acceptedTripPointTypes, SimpleFeatureBuilder lineBuilder) {
        this.lineBuilder = lineBuilder;
        this.legendItems = new ArrayList<>();
        this.acceptedTripPointTypes = acceptedTripPointTypes;
        this.withLabelLine = lineBuilder.getFeatureType().getAttributeCount() == 3;
        this.pointBuilder = createPointBuilder();
    }

    @Override
    public File getStyleFile(Path mapDirectory) {
        return mapDirectory.resolve(getStyleFileName()).toFile();
    }

    @Override
    public void reset() {
        legendItems.clear();
        validPoints.clear();
        notValidPoints.clear();
        if (mapContent != null) {
            mapContent.dispose();
        }
        mapContent = new MapContent();
    }

    public MapContent getMapContent() {
        return mapContent;
    }

    public List<ObserveMapPaneLegendItem> getLegendItems() {
        return legendItems;
    }

    @Override
    public void setStyledLayerDescriptor(File styleFile) throws FileNotFoundException {
        StyleFactory styleFactory = CommonFactoryFinder.getStyleFactory(null);
        SLDParser styleReader = new SLDParser(styleFactory, styleFile);
        styleReader.setOnLineResourceLocator(new ClassPathResourceLocator());
        styledLayerDescriptor = styleReader.parseSLD();
    }

    @Override
    public void addLayer(File layerFile) throws IOException {
        FileDataStore store = FileDataStoreFinder.getDataStore(layerFile);
        SimpleFeatureSource featureSource = store.getFeatureSource();
        Style style = SLD.createSimpleStyle(featureSource.getSchema());
        style = findStyle(styledLayerDescriptor, store.getNames().get(0).getLocalPart(), style);
        Layer layer = new FeatureLayer(featureSource, style, layerFile.getName());
        mapContent.addLayer(layer);
    }

    public void addPoints(List<TripMapPoint> tripMapPoints, String layerName, String notValidPointsLabel, Set<String> excludedFeatureNames) {
        DefaultFeatureCollection pointsFeatures = new DefaultFeatureCollection();
        Style stylePoints = findStyle(styledLayerDescriptor, layerName);

        for (TripMapPoint tripMapPoint : tripMapPoints) {
            if (tripMapPoint.isValid()) {
                addPoint(pointsFeatures, tripMapPoint, excludedFeatureNames);
            } else {
                notValidPoints.computeIfAbsent(tripMapPoint.getType(), e -> new LinkedList<>()).add(tripMapPoint);
            }
        }
        if (!pointsFeatures.isEmpty()) {
            Layer pointsLayer = new FeatureLayer(pointsFeatures, stylePoints, layerName);
            mapContent.addLayer(pointsLayer);
        }
        Set<TripMapPointType> pointTypeInLegend = validPoints.keySet();
        pointTypeInLegend.retainAll(acceptedTripPointTypes);

        Set<TripMapPointType> notValidTypes = notValidPoints.keySet().stream().sorted(Comparator.comparing(TripMapPointType::getLabel)).collect(Collectors.toCollection(LinkedHashSet::new));

        List<TripMapPointType> sortedPointTypeInLegend = pointTypeInLegend.stream().sorted(Comparator.comparing(TripMapPointType::getLabel)).collect(Collectors.toList());
        for (TripMapPointType type : sortedPointTypeInLegend) {
            List<TripMapPoint> notValidPointsForType = notValidPoints.get(type);
            List<TripMapPoint> validPointsForType = validPoints.get(type);
            int notValidCount = notValidPointsForType == null ? 0 : notValidPointsForType.size();
            int validCount = validPointsForType == null ? 0 : validPointsForType.size();
            String label = type.getLabel();
            if (notValidCount > 0) {
                notValidTypes.remove(type);
            }
            addPointLegend(stylePoints, type.name(), label, notValidCount, validCount, excludedFeatureNames);
        }
        if (!notValidTypes.isEmpty()) {
            for (TripMapPointType type : notValidTypes) {
                List<TripMapPoint> notValidPointsForType = notValidPoints.get(type);
                String label = I18n.t(notValidPointsLabel, notValidPointsForType.size()) + " - " + type.getLabel();
                addPointLegend(stylePoints, "notValidPoint", label, 0, 0, excludedFeatureNames);
            }
        }
        validPoints.clear();
        notValidPoints.clear();
    }

    protected void addPoint(DefaultFeatureCollection pointsFeatures, TripMapPoint tripMapPoint, Set<String> excludedFeatureNames) {
        if (!excludedFeatureNames.contains(tripMapPoint.getType().name())) {
            // display this point
            Point point = getGeometryFactory().createPoint(create(tripMapPoint));
            pointBuilder.add(point);
            pointBuilder.add(formatDate(tripMapPoint.getTime()));
            pointBuilder.add(tripMapPoint.getType().name());
            pointsFeatures.add(pointBuilder.buildFeature(null));
        }
        validPoints.computeIfAbsent(tripMapPoint.getType(), e -> new LinkedList<>()).add(tripMapPoint);
    }

    protected GeometryFactory getGeometryFactory() {
        if (geometryFactory == null) {
            geometryFactory = JTSFactoryFinder.getGeometryFactory();
        }
        return geometryFactory;
    }

    protected Style findStyle(StyledLayerDescriptor styledLayerDescriptor, final String layerName, Style defaultStyle) {
        Optional<StyledLayer> styledLayerOptional = styledLayerDescriptor.layers().stream().filter(input -> input.getName().equals(layerName)).findFirst();
        Style style = defaultStyle;
        if (styledLayerOptional.isPresent()) {
            StyledLayer styledLayer = styledLayerOptional.get();
            Style[] styles = new Style[0];
            if (styledLayer instanceof UserLayer) {
                styles = ((UserLayer) styledLayer).getUserStyles();
            } else if (styledLayer instanceof NamedLayer) {
                styles = ((NamedLayer) styledLayer).getStyles();
            }
            if (styles.length == 1) {
                style = styles[0];
            } else {
                for (Style styleTmp : styles) {
                    if (styleTmp.isDefault()) {
                        style = styleTmp;
                    } else {
                        styleTmp.getName();
                    }
                }
            }
        }
        return style;
    }

    protected Style findStyle(StyledLayerDescriptor styledLayerDescriptor, final String layerName) {
        Style style = findStyle(styledLayerDescriptor, layerName, null);
        if (style == null) {
            throw new RuntimeException(String.format("No style found for layer name '%s' and style name '%s'", layerName, null));
        }
        return style;
    }

    protected String formatDate(Date date) {
        return DATE_FORMAT.format(date);
    }

    protected void addLine(DefaultFeatureCollection linesFeatures, String lineName, Coordinate[] coordinates, Set<String> excludedFeatureNames) {
        if (excludedFeatureNames.contains(lineName)) {
            return;
        }
        LineString line = getGeometryFactory().createLineString(coordinates);
        lineBuilder.add(line);
        lineBuilder.add(lineName);
        linesFeatures.add(lineBuilder.buildFeature(null));
    }

    protected void addLine(DefaultFeatureCollection linesFeatures, String lineName, Coordinate[] coordinates, Date date, Set<String> excludedFeatureNames) {
        if (excludedFeatureNames.contains(lineName)) {
            return;
        }
        LineString line = getGeometryFactory().createLineString(coordinates);
        lineBuilder.add(line);
        lineBuilder.add(date == null ? null : formatDate(date));
        lineBuilder.add(lineName);
        linesFeatures.add(lineBuilder.buildFeature(null));
    }

    protected void addLineLegend(Style styleLines, String legendName, String legendLabel, Set<String> excludedFeatureNames) {
        LineString line = getGeometryFactory().createLineString(ObserveMapPaneLegendItem.lineCoordinates());
        lineBuilder.add(line);
        if (withLabelLine) {
            lineBuilder.add("");
        }
        lineBuilder.add(legendName);
        SimpleFeature simpleFeature = lineBuilder.buildFeature(null);
        ObserveMapPaneLegendItem legendTripDay = new ObserveMapPaneLegendItem(legendName, simpleFeature, styleLines, legendLabel, 0, 0, !excludedFeatureNames.contains(legendName));
        legendItems.add(legendTripDay);
    }

    protected void addPointLegend(Style stylePoints, String legendName, String legendLabel, int notValidCount, int validCount, Set<String> excludedFeatureNames) {
        Point point = getGeometryFactory().createPoint(ObserveMapPaneLegendItem.pointCoordinates());
        pointBuilder.add(point);
        pointBuilder.add("");
        pointBuilder.add(legendName);
        SimpleFeature simpleFeature = pointBuilder.buildFeature(null);
        ObserveMapPaneLegendItem legendPoint = new ObserveMapPaneLegendItem(legendName, simpleFeature, stylePoints, legendLabel, notValidCount, validCount, !excludedFeatureNames.contains(legendName));
        legendItems.add(legendPoint);
    }
}
