/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#waitLoadingLabel {
  text: "observe.ui.datasource.editor.content.map.waitLoading";
  horizontalAlignment: {JLabel.CENTER};
  verticalAlignment: {JLabel.CENTER};
  enabled: false;
}

#zoomMinus {
  enabled:{observeMapPane.getScaleInMiles() < 2000};
}

#addObservations {
  selected:{tripMapConfig.isAddObservations()};
}

#addLogbook {
  selected:{tripMapConfig.isAddLogbook()};
}

#addObservationsTripSegment {
  enabled:{tripMapConfig.isAddObservations()};
  selected:{tripMapConfig.isAddObservationsTripSegment()};
}

#addLogbookTripSegment {
  enabled:{tripMapConfig.isAddLogbook()};
  selected:{tripMapConfig.isAddLogbookTripSegment()};
}

.legendPosition {
  buttonGroup: legendPosition;
}

#legendPositionTop {
  selected:{observeMapPane.isLegendPositionTop()};
  enabled:{observeMapPane.isShowLegend() && observeMapPane.isLegendPositionBottom()}
}

#legendPositionBottom {
  selected:{observeMapPane.isLegendPositionBottom()};
  enabled:{observeMapPane.isShowLegend() && observeMapPane.isLegendPositionTop()}
}

#addPointCount {
  enabled:{observeMapPane.isShowLegend()};
  selected:{observeMapPane.isAddPointCount()};
}
