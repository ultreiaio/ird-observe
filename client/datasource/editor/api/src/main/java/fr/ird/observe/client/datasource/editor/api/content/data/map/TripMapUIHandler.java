package fr.ird.observe.client.datasource.editor.api.content.data.map;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.TripMapConfig;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.data.TripMapPoint;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.swing.JMapPane;
import org.geotools.swing.event.MapPaneEvent;
import org.geotools.swing.event.MapPaneListener;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TripMapUIHandler implements UIHandler<TripMapUI> {

    public static final double ZOOM_STEP_RATIO = 0.1;

    private static final Logger log = LogManager.getLogger(TripMapUIHandler.class);

    private TripMapUI ui;
    private TripMapConfig config;
    private ReferencedEnvelope tripArea;
    private boolean rendererRunning;
    private double zoomRatio = 1;
    private Point zoomCenter;
    private TripMapUIInitializer initializer;

    public void setConfig(TripMapConfig config) {
        this.config = config;

        JMapPane mapPane = getObserveMapPane();
        mapPane.setBackground(config.getMapBackgroundColor());
    }

    private ObserveMapPane getObserveMapPane() {
        return ui.getObserveMapPane();
    }

    public void doOpenMap(TripMapConfigDto tripMapConfig, TripMapDto tripMapDto, Set<String> excludedFeatureNames) {
        try {
            flipContent();

            ObserveMapPane mapPane = getObserveMapPane();

            if (mapPane.getMapContent() != null) {
                // appeler pour libéré les listeners
                mapPane.getMapContent().dispose();
            }

            List<TripMapPoint> tripMapPoints = new ArrayList<>(tripMapDto.getPoints());
            log.info(String.format("Found %d point(s).", tripMapPoints.size()));
            mapPane.setTripMapDto(tripMapDto);
            TripMapContentBuilderSupport mapContentBuilder = TripMapContentBuilderSupport.of(config,
                                                                                             tripMapConfig,
                                                                                             tripMapDto,
                                                                                             tripMapPoints,
                                                                                             excludedFeatureNames);

            // set zoom
            tripArea = new ReferencedEnvelope();
            for (TripMapPoint point : tripMapPoints) {
                if (point.isValid()) {
                    tripArea.expandToInclude(new DirectPosition2D(point.getLongitude(), point.getLatitude()));
                }
            }
            tripArea.expandBy(1.1);

            mapPane.setMapContent(mapContentBuilder.getMapContent());
            mapPane.setLegendItems(mapContentBuilder.getLegendItems());

        } catch (Exception e) {
            throw new RuntimeException("Unable to load trip map activity points", e);
        }

    }

    private void flipContent() {
        ui.getContentLayout().first(ui.getContent());
    }

    public void doCloseMap() {
        flipContent();
    }

    public void zoomApply() {
        if (zoomRatio != 1 && !rendererRunning) {

            JMapPane mapPane = getObserveMapPane();
            log.debug("Zoom ratio: " + zoomRatio);
            ReferencedEnvelope displayArea = mapPane.getDisplayArea();

            double deltaWidth = displayArea.getWidth() * (zoomRatio - 1);
            double deltaHeight = displayArea.getHeight() * (zoomRatio - 1);

            double ratioLeft = zoomCenter.getX() / mapPane.getWidth();

            // l'axe de Y est inversé entre le référentiel du composant swing et le référentiel géographique
            double ratioTop = 1 - (zoomCenter.getY() / mapPane.getHeight());

            double deltaLeft = deltaWidth * ratioLeft;
            double deltaRight = deltaLeft - deltaWidth;

            double deltaTop = deltaHeight * ratioTop;
            double deltaBottom = deltaTop - deltaHeight;

            log.debug(String.format("Map mouse zoom (zoom ratio : %s, deltaLeft : %s, deltaRight : %s, deltaTop : %s, deltaBottom : %s)",
                                    zoomRatio, deltaLeft, deltaRight, deltaRight, deltaBottom));

            ReferencedEnvelope newDisplayArea = new ReferencedEnvelope(
                    displayArea.getMinX() + deltaLeft,
                    displayArea.getMaxX() + deltaRight,
                    displayArea.getMinY() + deltaTop,
                    displayArea.getMaxY() + deltaBottom,
                    displayArea.getCoordinateReferenceSystem()
            );
            // -230 is the good value (don't ask me why ?)
            if (newDisplayArea.getLowerCorner().getOrdinate(0) > -230) {
                mapPane.setDisplayArea(newDisplayArea);
            }

            zoomRatio = 1;

        }

    }

    @Override
    public void beforeInit(TripMapUI ui) {
        this.ui = ui;
        initializer = new TripMapUIInitializer(ui);
    }

    @Override
    public void afterInit(TripMapUI ui) {
        ui.getToggleLegend().setSelected(true);
        ui.getAddObservations().setSelected(true);
        ui.getAddLogbook().setSelected(true);
        ui.getLegendPositionBottom().setSelected(true);
        initializer.initUI();
        ObserveMapPane mapPane = getObserveMapPane();

        MouseMapListener mouseMapListener = new MouseMapListener();
        mapPane.addMouseWheelListener(mouseMapListener);
        mapPane.addMouseMotionListener(mouseMapListener);
        mapPane.addMouseListener(mouseMapListener);
        mapPane.addMapPaneListener(new TripMapListener());
        ui.getConfigurePopup().add(new JSeparator(JSeparator.HORIZONTAL), 4);
        ui.getConfigurePopup().add(new JSeparator(JSeparator.HORIZONTAL), 2);
        rendererRunning = false;
    }

    public ReferencedEnvelope getTripArea() {
        return tripArea;
    }

    public double getZoomRatio() {
        return zoomRatio;
    }

    public void setZoomRatio(double zoomRatio) {
        this.zoomRatio = zoomRatio;
    }

    public void setZoomCenter(Point zoomCenter) {
        this.zoomCenter = zoomCenter;
    }

    public void updateMap(boolean showLegend) {
        getObserveMapPane().setShowLegend(showLegend);
        SwingUtilities.invokeLater(getObserveMapPane()::repaint);
    }

    private class MouseMapListener implements MouseWheelListener, MouseListener, MouseMotionListener {

        Point2D startPointInWorld;
        AffineTransform startScreenToWorldTransform;
        ReferencedEnvelope startDisplayArea;

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            int notches = e.getWheelRotation();
            zoomRatio = zoomRatio * (1 + (ZOOM_STEP_RATIO * notches * -1));
            zoomCenter = e.getPoint();
            zoomApply();
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() < 2) {
                return;
            }
            Point point = e.getPoint();
            Optional<ObserveMapPaneLegendItem> legendItem = getObserveMapPane().getLegendItem(point);
            legendItem.ifPresent(i -> {
                i.setSelected(!i.isSelected());
                log.info("Change selected value for legend item: {} to value: {}", i.getName(), i.isSelected());
                SwingUtilities.invokeLater(ui.getObserveMapPane()::fireRebuildModel);
            });
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                startMove(e.getPoint());
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                endMove(e.getPoint());
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }

        @Override
        public void mouseDragged(MouseEvent e) {
//            endMove(e.getPoint());
        }

        @Override
        public void mouseMoved(MouseEvent e) {

        }

        void startMove(Point2D startPointInScreen) {
            JMapPane mapPane = getObserveMapPane();

            startDisplayArea = mapPane.getDisplayArea();

            startScreenToWorldTransform = mapPane.getScreenToWorldTransform();

            startPointInWorld = new Point2D.Double();

            startScreenToWorldTransform.transform(startPointInScreen, startPointInWorld);

        }

        void endMove(Point2D endPointInScreen) {

            if (startScreenToWorldTransform == null) {
                return;
            }
            Point2D endPointInWorld = new Point2D.Double();
            startScreenToWorldTransform.transform(endPointInScreen, endPointInWorld);

            double transX = startPointInWorld.getX() - endPointInWorld.getX();
            double transY = startPointInWorld.getY() - endPointInWorld.getY();

            ReferencedEnvelope endDisplayArea = new ReferencedEnvelope(startDisplayArea);

            endDisplayArea.translate(transX, transY);

            JMapPane mapPane = getObserveMapPane();

            mapPane.setDisplayArea(endDisplayArea);

            if (log.isDebugEnabled()) {
                log.debug(String.format("Translate (x : %s, y : %s)", transX, transY));
            }
        }
    }

    protected class TripMapListener implements MapPaneListener {

        boolean firstRendering;

        @Override
        public void onNewMapContent(MapPaneEvent ev) {
            firstRendering = true;
        }

        @Override
        public void onDisplayAreaChanged(MapPaneEvent ev) {
        }

        @Override
        public void onRenderingStarted(MapPaneEvent ev) {
            rendererRunning = true;
        }

        @Override
        public void onRenderingStopped(MapPaneEvent ev) {
            rendererRunning = false;
            if (firstRendering) {

                if (!tripArea.isEmpty()) {
                    JMapPane mapPane = getObserveMapPane();
                    mapPane.setDisplayArea(tripArea);
                }

                ui.getContentLayout().last(ui.getContent());
                firstRendering = false;
            } else {
                zoomApply();
            }
        }
    }

}
