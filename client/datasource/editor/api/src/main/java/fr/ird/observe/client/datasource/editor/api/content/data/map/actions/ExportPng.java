package fr.ird.observe.client.datasource.editor.api.content.data.map.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.map.TripMapUI;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.client.util.UIFileHelper;
import fr.ird.observe.dto.ObserveUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 25/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ExportPng extends TripMapUIActionSupport {

    private static final Logger log = LogManager.getLogger(ExportPng.class);

    public ExportPng() {
        super(
                t("observe.ui.datasource.editor.content.map.action.exportPng"),
                t("observe.ui.datasource.editor.content.map.action.exportPng.tip"),
                "save",
                ObserveKeyStrokesEditorApi.KEY_STROKE_MAP_EXPORT);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, TripMapUI ui) {

        File file = UIFileHelper.chooseFile(ui,
                                            t("observe.ui.datasource.editor.content.map.export.chooseFile.title"),
                                            t("observe.ui.datasource.editor.content.map.export.chooseFile.ok"),
                                            null,
                                            ObserveUtil.PNG_EXTENSION,
                                            ObserveUtil.PNG_EXTENSION_PATTERN,
                                            t("observe.ui.datasource.editor.content.map.export.chooseFile.png"));

        if (file != null && UIFileHelper.confirmOverwriteFileIfExist(ui, file)) {

            BufferedImage im = new BufferedImage(ui.getWidth(), ui.getHeight(), BufferedImage.TYPE_INT_ARGB);
            ui.paint(im.getGraphics());
            try {
                ImageIO.write(im, "PNG", file);
            } catch (IOException e) {
                throw new ObserveSwingTechnicalException("unable to export map ", e);
            }

            log.info(t("observe.ui.datasource.editor.content.map.export.success", file));
            //FIXME:BodyContent Find a way to dispatch messages to main ui
//            ClientUIContextApi.displayInfo(t("observe.ui.datasource.editor.content.map.export.success", file));
        }
    }
}
