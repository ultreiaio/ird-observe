package fr.ird.observe.client.datasource.editor.api.content.data.map.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.map.ObserveMapPane;
import fr.ird.observe.client.datasource.editor.api.content.data.map.TripMapUI;

import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 16/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.1
 */
public class LegendPositionTop extends TripMapUIActionSupport implements ConfigureMapAction {

    public LegendPositionTop() {
        super(
                t("observe.ui.datasource.editor.content.map.legendTop"),
                t("observe.ui.datasource.editor.content.map.legendTop.tip"),
                "show-help",
                ObserveKeyStrokesEditorApi.KEY_STROKE_LEGEND_TOP);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, TripMapUI ui) {
        ObserveMapPane observeMapPane = ui.getObserveMapPane();
        boolean showLegend = observeMapPane.isShowLegend();
        if (!showLegend) {
            return;
        }
        observeMapPane.setLegendPosition(ObserveMapPane.LegendPosition.TOP);
        ui.getHandler().updateMap(true);
    }
}
