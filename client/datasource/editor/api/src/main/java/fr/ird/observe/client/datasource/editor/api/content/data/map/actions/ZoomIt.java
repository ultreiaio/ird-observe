package fr.ird.observe.client.datasource.editor.api.content.data.map.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.map.TripMapUI;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.swing.JMapPane;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 25/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ZoomIt extends TripMapUIActionSupport {

    public ZoomIt() {
        super(
                t("observe.ui.datasource.editor.content.map.action.zoomIt"),
                t("observe.ui.datasource.editor.content.map.action.zoomIt.tip"),
                "center",
                ObserveKeyStrokesEditorApi.KEY_STROKE_MAP_ZOOM_IT);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, TripMapUI ui) {
        ReferencedEnvelope tripArea = ui.getHandler().getTripArea();
        if (!tripArea.isEmpty()) {
            JMapPane mapPane = ui.getObserveMapPane();
            mapPane.setDisplayArea(tripArea);
        }
    }
}
