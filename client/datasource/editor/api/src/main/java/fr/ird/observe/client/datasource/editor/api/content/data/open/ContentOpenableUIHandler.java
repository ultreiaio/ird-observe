/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.data.open;

import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewEditable;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeletePanel;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowTechnicalInformations;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.actions.reset.ResetAction;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;

import javax.swing.JMenuItem;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.Callable;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class ContentOpenableUIHandler<D extends OpenableDto, U extends ContentOpenableUI<D, U>> extends ContentUIHandler<U> {

    protected void installMoveAction() {
        // move action is optional
    }

    protected abstract void installCreateNewAction();

    protected abstract void installSaveAction();

    protected abstract void installDeleteAction();

    protected DeletePanel computeDeleteDependenciesExtraMessage(DeletePanel deletePanel, Collection<ToolkitIdLabel> toDelete) {
        Class<D> mainType = ui.getModel().getScope().getMainType();
        return super.computeOpenDeleteDependenciesExtraMessage(mainType, deletePanel, toDelete);
    }

    protected void installCreateNewOpenableAction(Callable<? extends NavigationNode> referenceMaker) {
        CreateNewOpenableUI.installAction(ui, ui.getModel().getScope().getNodeType(), NavigationNode::getParent, referenceMaker);
    }

    protected <T extends EditableDto> void installCreateNewEditableAction(Class<T> editableType, Callable<? extends NavigationNode> referenceMaker) {
        CreateNewEditable<D, T, U> action = new CreateNewEditable<>(ui.getModel().getSource().getScope().getMainType(), editableType, referenceMaker);
        String modelProperty = "add" + editableType.getSimpleName().replace("Dto", "");
        JMenuItem editor = (JMenuItem) Objects.requireNonNull(ui.getObjectById(modelProperty));
        CreateNewEditable.installAction(ui, editor, action);
    }

    @Override
    public ContentOpenableUIModel<D> getModel() {
        return ui.getModel();
    }

    @Override
    protected final ContentOpenableUIInitializer<U> createContentUIInitializer(U ui) {
        return new ContentOpenableUIInitializer<>(ui);
    }

    @Override
    protected ContentOpenWithValidator<U> createContentOpen(U ui) {
        ContentOpenableUIOpenExecutor<D, U> executor = new ContentOpenableUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor);
    }

    @Override
    public final ContentOpenWithValidator<U> getContentOpen() {
        return (ContentOpenWithValidator<U>) super.getContentOpen();
    }

    @Override
    public void onPrepareValidationContext() {
        super.onPrepareValidationContext();
        ui.getValidator().setContext(getModel().getStates().getValidationContext());
    }

    @Override
    public final void initActions() {
        installCreateNewAction();
        installResetAction();
        installSaveAction();
        installDeleteAction();
        installMoveAction();
        ui.getConfigurePopup().addSeparator();
        ShowTechnicalInformations.installAction(ui);
    }

    @Override
    public final void installChangeModeAction() {
        ChangeMode.installAction(ui);
    }

    protected final void installResetAction() {
        ResetAction.installAction(ui, ui.getReset());
    }
}
