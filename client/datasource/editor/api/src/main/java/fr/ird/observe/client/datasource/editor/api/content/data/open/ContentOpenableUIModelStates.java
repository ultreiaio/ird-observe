package fr.ird.observe.client.datasource.editor.api.content.data.open;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUIModelStatesSupport;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.ps.common.TripDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentOpenableUIModelStates<D extends OpenableDto> extends ContentSimpleUIModelStatesSupport<D> {
    private static final Logger log = LogManager.getLogger(ContentOpenableUIModelStates.class);
    private static final String PROPERTY_HISTORICAL_DATA = "historicalData";
    /**
     * Historical data (found on trip data).
     */
    private boolean historicalData;

    public ContentOpenableUIModelStates(ContentOpenableUIModel<D> model, D bean, String selectedId) {
        super(model, bean, selectedId);
        DecoratorService decoratorService = getDecoratorService();
        decoratorService.installDecorator(bean);
    }

    @Override
    public ContentOpenableUINavigationNode source() {
        return (ContentOpenableUINavigationNode) super.source();
    }

    public ToolkitIdDtoBean selectedParent() {
        return source().getInitializer().getParentShortReference();
    }

    public final boolean isHistoricalData() {
        return historicalData;
    }

    public final void setHistoricalData(boolean historicalData) {
        boolean old = isHistoricalData();
        this.historicalData = historicalData;
        firePropertyChange(PROPERTY_HISTORICAL_DATA, old, historicalData);
    }

    @Override
    public final void open(ContentUIModel model) {
        super.open(model);
        boolean historicalData = false;
        if (getBean().isPersisted()) {
            ClientValidationContext context = getClientValidationContext();
            if (context.getSelectModel().getPs().getCommonTrip().isEnabled()) {
                TripDto currentPsCommonTrip = context.getCurrentPsCommonTrip();
                if (currentPsCommonTrip != null && currentPsCommonTrip.isHistoricalData()) {
                    log.info(String.format("%sUsing a historical fish trip %s", getPrefix(), currentPsCommonTrip.getId()));
                    historicalData = true;
                }
            }
        }
        setHistoricalData(historicalData);
    }

}
