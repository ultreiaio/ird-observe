package fr.ird.observe.client.datasource.editor.api.content.data.open;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationHandler;
import fr.ird.observe.navigation.id.IdNode;

import java.awt.Color;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 24/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentOpenableUINavigationHandler<N extends ContentOpenableUINavigationNode> extends NavigationHandler<N> {

    private final N node;

    public ContentOpenableUINavigationHandler(N node) {
        this.node = Objects.requireNonNull(node);
    }

    @Override
    public abstract ContentOpenableUINavigationContext getContext();

    @Override
    public final N getNode() {
        return node;
    }

    @Override
    public final boolean isLeaf() {
        if (!getNode().getAllowsChildren()) {
            return true;
        }
        if (getNode().getInitializer().isNotPersisted()) {
            return true;
        }
        if (!getNode().isLoaded()) {
            return getNode().getInitializer().isLeaf();
        }
        return getNode().getChildCount() == 0;
    }

    @Override
    public final boolean canBuildChildren() {
        return getNode().getInitializer().isPersisted() && super.canBuildChildren();
    }

    @Override
    protected Color getColor0(Supplier<Color> defaultValue) {
        if (!getNode().getInitializer().isPersisted()) {
            return getNotPersistedColor();
        }
        return super.getColor0(defaultValue);
    }

    @Override
    public String getText() {
        return getNode().getInitializer().isPersisted() ? getNode().getInitializer().getText() : getNode().getScope().getI18nTranslation("navigation.unsaved");
    }

    @Override
    public String getContentTitle() {
        return getNode().getScope().getI18nTranslation("title");
    }

    @Override
    public final ContentMode getContentMode() {
        ContentMode contentMode = super.getContentMode();
        if (contentMode != null) {
            return contentMode;
        }
        if (!getNode().getInitializer().isPersisted()) {
            return ContentMode.CREATE;
        }
        IdNode<?> idNode = getNode().getInitializer().getEditNode();
        return idNode.isEnabled() && getNode().getInitializer().isOpen() ? ContentMode.UPDATE : ContentMode.READ;
    }
}
