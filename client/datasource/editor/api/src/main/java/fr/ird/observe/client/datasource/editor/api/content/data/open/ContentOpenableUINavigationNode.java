package fr.ird.observe.client.datasource.editor.api.content.data.open;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ReferenceContainerCapability;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Font;
import java.util.function.Supplier;

/**
 * Created on 07/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentOpenableUINavigationNode extends NavigationNode {
    private static final Logger log = LogManager.getLogger(ContentOpenableUINavigationNode.class);

    public static <N extends ContentOpenableUINavigationNode> N init(N node, Supplier<? extends DtoReference> parentReference, DataDtoReference reference) {
        ContentOpenableUINavigationInitializer initializer = new ContentOpenableUINavigationInitializer(node.getScope(), parentReference, reference);
        node.init(initializer);
        return node;
    }

    @Override
    public ContentOpenableUINavigationInitializer getInitializer() {
        return (ContentOpenableUINavigationInitializer) super.getInitializer();
    }

    @Override
    public ContentOpenableUINavigationContext getContext() {
        return (ContentOpenableUINavigationContext) super.getContext();
    }

    @Override
    public ContentOpenableUINavigationHandler<?> getHandler() {
        return (ContentOpenableUINavigationHandler<?>) super.getHandler();
    }

    @Override
    public ContentOpenableUINavigationCapability<?> getCapability() {
        return (ContentOpenableUINavigationCapability<?>) super.getCapability();
    }

    public DataDtoReference getReference() {
        return getInitializer().getReference();
    }

    public DtoReference getParentReference() {
        return getInitializer().getParentReference();
    }

    public void updateReference(String id) {
        boolean notPersisted = getInitializer().isNotPersisted();
        NavigationNode parent = getParent();
        int oldPosition = parent.getIndex(this);
        if (notPersisted) {
            getInitializer().updateEditNodeId(id);
            getInitializer().updateSelectNodeId(id);
            // we need to set by hand the id on node reference, to be able to update it by the very next statement
            getInitializer().getReference().setId(id);
        }
        // reload node data
        reloadNodeData();
        DataDtoReference reference = getInitializer().getReference();

        if (notPersisted) {
            dirty();
            populateChildrenIfNotLoaded();
        }

        ReferenceContainerCapability<?> capability = (ReferenceContainerCapability<?>) parent.getCapability();
        int newPosition = capability.getNodePosition(reference);
        if (oldPosition != newPosition) {
            log.info(String.format("Move node from: %d to %d", oldPosition, newPosition));
            parent.moveNode(this, newPosition);
        }

        // repaint selected node and his children
        nodeChangedDeep();

        // reload from parent to root
        parent.reloadNodeDataToRoot();
    }

    @Override
    public Font getNodeFont(Font defaultFont) {
        if (getInitializer().isOpen()) {
            return defaultFont.deriveFont(Font.BOLD);
        }
        return defaultFont.deriveFont(Font.PLAIN);
    }
}
