package fr.ird.observe.client.datasource.editor.api.content.data.open.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveUIAdapter;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.dto.data.OpenableDto;

import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created on 16/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class SaveContentOpenableUIAdapter<D extends OpenableDto, U extends ContentOpenableUI<D, U>> implements SaveUIAdapter<D, U> {

    private final Predicate<D> predicate;
    private final Function<U, AbstractButton> buttonGetter;

    public SaveContentOpenableUIAdapter() {
        this.predicate = t -> false;
        this.buttonGetter = null;
    }

    public SaveContentOpenableUIAdapter(Predicate<D> predicate, Function<U, AbstractButton> buttonGetter) {
        this.predicate = Objects.requireNonNull(predicate);
        this.buttonGetter = Objects.requireNonNull(buttonGetter);
    }

    @Override
    public void adaptUi(DataSourceEditor dataSourceEditor, U ui, boolean notPersisted, D bean) {

        ContentOpenableUIModel<?> model = ui.getModel();
        ui.stopEdit();

        ContentOpenableUINavigationNode node = model.getSource(); //??? .upToReferenceNode(model.getSource().getScope().getMainReferenceType());
//        IdDto bean = request.getBean();
        // We need to inject ot node the new reference (it could does not know the id if not persisted)
        // As I prefer to use a unique code (for persisted or not, keep it like this)
        node.updateReference(bean.getId());

        NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();
        afterNodeUpdated(dataSourceEditor, ui, tree, node, notPersisted, bean);
    }

    protected void afterNodeUpdated(DataSourceEditor dataSourceEditor, U ui, NavigationTree tree, ContentOpenableUINavigationNode node, boolean notPersisted, D bean) {

        //FIXME A startEdit should do the math?
        tree.reSelectSafeNodeThen(node, () -> {
            if (notPersisted && predicate.test(bean)) {
                // reload ui and do click
                U newUi = dataSourceEditor.getModel().getTypedContent();
                SwingUtilities.invokeLater(() -> {
                    AbstractButton button = Objects.requireNonNull(buttonGetter).apply(newUi);
                    if (!button.isEnabled()) {
                        button.setEnabled(true);
                    }
                    button.doClick();
                });
                return;
            } else {
                dataSourceEditor.getModel().resetFromPreviousUi(ui);
            }

            if (notPersisted) {
                tree.expandPath(tree.getSelectionPath());
            }
        });
    }
}
