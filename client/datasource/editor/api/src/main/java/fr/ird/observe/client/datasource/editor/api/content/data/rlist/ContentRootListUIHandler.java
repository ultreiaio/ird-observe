/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.data.rlist;

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewRootOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowTechnicalInformations;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeProducer;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpen;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.actions.BulkModify;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.actions.FixData;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.actions.GotoOpenData;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.actions.GotoSelectedData;
import fr.ird.observe.client.datasource.editor.api.content.ui.ReferenceListFromMultipleContainerNodeCellRenderer;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import io.ultreia.java4all.jaxx.widgets.list.ListHeader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenuItem;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.function.Function;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public abstract class ContentRootListUIHandler<D extends RootOpenableDto, R extends DataDtoReference, U extends ContentRootListUI<D, R, U>> extends ContentUIHandler<U> {

    public static final String CLIENT_PROPERTY_CREATE_ACTION = "CreateAction";
    private static final Logger log = LogManager.getLogger(ContentRootListUIHandler.class);

    protected abstract void installDeleteAction();

    protected abstract void installCreateNewAction();

//    protected DeletePanel computeDeleteDependenciesExtraMessage(DeletePanel deletePanel, Collection<ToolkitIdLabel> toDelete) {
//        Class<D> mainType = ui.getModel().getScope().getMainType();
//        return super.computeRootOpenDeleteDependenciesExtraMessage(mainType, deletePanel, toDelete);
//    }

    protected void installCreateNewOpenableAction(Callable<? extends NavigationNode> referenceMaker) {
        JMenuItem editor = CreateNewRootOpenableUI.installAction(this.ui, this.ui.getModel().getSource().getScope().getNodeChildType(), Function.identity(), referenceMaker);
        ui.putClientProperty(CLIENT_PROPERTY_CREATE_ACTION, editor.getName());
        ui.getModel().getStates().addPropertyChangeListener(ContentRootListUIModelStates.PROPERTY_SHOW_DATA, evt -> editor.setEnabled((Boolean) evt.getNewValue()));
    }

    @Override
    public ContentRootListUIModel<R> getModel() {
        return ui.getModel();
    }

    @Override
    protected final ContentUIInitializer<U> createContentUIInitializer(U ui) {
        return new ContentRootListUIInitializer<>(ui);
    }

    @Override
    protected ContentOpen<U> createContentOpen(U ui) {
        return new ContentOpen<>(ui, new ContentRootListUIOpenExecutor<>());
    }

    @Override
    protected Container computeFocusOwnerContainer() {
        ContentRootListUIModelStates<R> states = getModel().getStates();
        ContentMode mode = states.getMode();
        Container container;
        boolean showData = states.isShowData();
        boolean empty = states.isEmpty();
        if (!empty) {
            return ui.getContentBody();
        }
        if (mode == null || mode == ContentMode.READ || !showData) {
            container = ui;
        } else {
            container = ui.getContentBody();
        }
        return container;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public final void onInit(U ui) {
        super.onInit(ui);

        ui.getListHeader().setLabelText(getModel().getSource().getScope().getI18nTranslation("list.title"));
        String type = getModel().getSource().getScope().getI18nTranslation("type");
        ui.getHideFormInformation().setText(t("observe.data.Trip.title.can.not.create.trip.sub.data", type));
        // set list renderer
        JList<R> list = ui.getList();
        list.setFixedCellHeight(24);
        list.setFixedCellWidth(200);
        NavigationTree tree = getNavigationTree();
        ReferenceListFromMultipleContainerNodeCellRenderer renderer2 = new ReferenceListFromMultipleContainerNodeCellRenderer(tree, ui.getListHeader().getHandler().getDecorator());
        list.setCellRenderer(renderer2);

        //noinspection unchecked
        getModel().getStates().addPropertyChangeListener(ContentRootListUIModelStates.PROPERTY_DATA, e -> updateList((List) e.getNewValue()));

        // init renderer
        renderer2.init();
    }

    @Override
    public void initActions() {
        GotoOpenData.installAction(ui);
        GotoSelectedData.installAction(ui);
        FixData.installAction(ui);
        installDeleteAction();
        installCreateNewAction();
        BulkModify.installAction(ui);
        ShowTechnicalInformations.installAction(ui);
    }

    @Override
    public void installChangeModeAction() {
        ChangeModeRequest request = ui.getModel().toChangeModeRequest();
        ChangeModeExecutor<U> executor = new ChangeModeExecutor<>() {
            @Override
            protected void afterOpenReselectNode(U ui, DataSourceEditor dataSourceEditor, NavigationTree tree, NavigationNode selectedNode, String id) {
                Class<? extends DataDtoReference> referenceType = ContentRootListUIHandler.this.ui.getModel().getSource().getScope().getMainReferenceType();
                NavigationNode referenceNode = tree.getSelectedNode().downToReferenceNode(referenceType, id);
                log.info(String.format("Will reselect node: %s", selectedNode));
                tree.reSelectSafeNode(referenceNode);
            }

            @Override
            protected void afterClose(U ui, DataSourceEditor dataSourceEditor) {
                super.afterClose(ui, dataSourceEditor);
                ui.getModel().getStates().setMode(ContentMode.CREATE);
                ui.getList().repaint();
            }
        };
        ChangeModeProducer<U> changeModeProducer = new ChangeModeProducer<>(ui, request,
                                                                            ContentRootListUIModelStates.PROPERTY_SHOW_DATA,
                                                                            ContentRootListUIModelStates.PROPERTY_EMPTY,
                                                                            ContentRootListUIModelStates.PROPERTY_ONE_SELECTED_DATA) {
            @Override
            public ContentMode rebuildChangeMode(ContentMode newValue) {
                ContentRootListUIModelStates<R> source = ui.getStates();
                boolean showData = source.isShowData();
                if (!showData || source.isEmpty()) {
                    newValue = null;
                } else {
                    boolean open = source.isOneSelectedData() && Objects.equals(source.getEditNodeId(), source.getSelectedDataId());
                    newValue = open ? ContentMode.UPDATE : ContentMode.READ;
                }
                return newValue;
            }
        };
        ChangeMode<U> action = new ChangeMode<>(request, changeModeProducer, executor) {
            protected boolean rebuildZonePredicate(U ui) {
                return request.isEditable() && !ui.getStates().isEmpty();
            }

            @Override
            public void rebuildEditableZone(U ui) {
                JComponent body = ui.getContentBody();
                body.remove(ui.getShowForm());
                body.remove(ui.getHideForm());
                body.remove(ui.getEmptyForm());
                body.add(ui.getShowForm(), BorderLayout.CENTER);
                ui.getActions().setVisible(true);
                super.rebuildEditableZone(ui);
            }

            @Override
            public void rebuildNotEditableZone(U ui) {
                JComponent body = ui.getContentBody();
                boolean showData = ui.getStates().isShowData();
                boolean empty = ui.getStates().isEmpty();
                body.remove(ui.getShowForm());
                body.remove(ui.getHideForm());
                body.remove(ui.getEmptyForm());
                if (!showData) {
                    body.add(ui.getHideForm(), BorderLayout.CENTER);
                } else if (empty) {
                    String type = getModel().getSource().getScope().getI18nTranslation("type");
                    DataGroupByDto<?> dataGroupByDto = getModel().getSource().getParentReference();
                    ui.getEmptyFormInformation().setText(String.format(getModel().getSource().getScope().getI18nTranslation("root.list.message.none"), type, dataGroupByDto));
                    body.add(ui.getEmptyForm(), BorderLayout.CENTER);
                }
                ui.getActions().setVisible(false);
                super.rebuildNotEditableZone(ui);
            }
        };
        ChangeMode.installAction(ui, action);

    }

    /**
     * When a data has been selected in the list.
     *
     * @param event the mouse event fired
     */
    final void onDataSelected(MouseEvent event) {
        if (event.getClickCount() > 1) {
            if (getModel().getStates().isOneSelectedData()) {
                gotoChild(getUi().getList().getSelectedValue());
            }
        }
    }

    private void updateList(List<R> data) {
        ListHeader<R> list = ui.getListHeader();
        if (data != null && !data.isEmpty()) {
            log.debug(list.getName() + " - " + data.size());
            list.setData(data);
        } else {
            list.setData(Collections.emptyList());
        }
    }

    private void gotoChild(DataDtoReference entity) {
        if (entity == null) {
            return;
        }
        NavigationTree treeHelper = getNavigationTree();
        NavigationNode parentNode = treeHelper.getSelectedNode();
        String id = entity.getId();
        NavigationNode node = parentNode.findChildById(id);
        log.debug(String.format("%swill go to node %s for %s", prefix, node, id));
        treeHelper.selectSafeNode(node);
    }
}
