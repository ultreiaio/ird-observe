package fr.ird.observe.client.datasource.editor.api.content.data.rlist;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import io.ultreia.java4all.decoration.Decorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentRootListUIModelStates<R extends DataDtoReference> extends ContentUIModelStates {
    public static final String PROPERTY_DATA = "data";
    public static final String PROPERTY_SELECTED_DATAS = "selectedDatas";
    public static final String PROPERTY_ONE_SELECTED_DATA = "oneSelectedData";
    public static final String PROPERTY_ONE_OR_MORE_SELECTED_DATA = "oneOrMoreSelectedData";
    public static final String PROPERTY_EMPTY = "empty";
    public static final String PROPERTY_LAYOUT_NAME = "layoutName";
    private static final Logger log = LogManager.getLogger(ContentRootListUIModelStates.class);
    private final ReferencesCache referenceCache;
    private final String editNodeId;
    private final boolean parentOpen;
    /**
     * Label decorator.
     */
    private final Decorator labelDecorator;
    private final ContentRootListUINavigationNode source;
    private List<R> data;
    private List<R> selectedDatas;
    private String layoutName;

    public ContentRootListUIModelStates(ContentRootListUIModel<R> model, String editNodeId, boolean parentOpen) {
        this.source = Objects.requireNonNull(model).getSource();
        ContentRootListUINavigationContext context = source.getContext();
        this.referenceCache = context.newReferenceCache();
        this.editNodeId = editNodeId;
        this.parentOpen = parentOpen;
        this.labelDecorator = context.getDecoratorService().getToolkitIdLabelDecoratorByType(model.getScope().getMainType());
    }

    public ContentRootListUINavigationNode source() {
        return source;
    }

    @Override
    public void open(ContentUIModel model) {
        super.open(model);
        @SuppressWarnings("unchecked") ContentRootListUIModel<R> realModel = (ContentRootListUIModel<R>) model;
        ContentRootListUINavigationNode source = realModel.getSource();
        addPropertyChangeListener(PROPERTY_ONE_SELECTED_DATA, evt -> {
            boolean newValue = (boolean) evt.getNewValue();
            if (newValue) {
                ContentMode contentMode = source.getHandler().getContentMode();
                if (ContentMode.UPDATE == contentMode) {
                    boolean open = Objects.equals(source.getInitializer().getEditNodeId(), realModel.getStates().getSelectedData().orElseThrow(IllegalStateException::new).getId());
                    contentMode = open ? ContentMode.UPDATE : ContentMode.READ;
                }
                setMode(null);
                setMode(contentMode);
            } else {
                setMode(null);
                setMode(ContentMode.READ);
            }
        });
        // Compute showData property
        boolean showData = source.getInitializer().isShowData();
        setShowData(showData);

        log.info(String.format("%s parentSelectedId = %s", source.getInitializer().getLogPrefix(), selectedParent()));
        @SuppressWarnings("unchecked") List<R> data = (List<R>) source.getReferences();
        setData(data);
        if (!data.isEmpty()) {
            List<R> firstData = new LinkedList<>();
            firstData.add(data.get(0));
            setSelectedDatas(firstData);
        }
    }

    public boolean isParentOpen() {
        return parentOpen;
    }

    public String getEditNodeId() {
        return editNodeId;
    }

    public DataGroupByDto<?> selectedParent() {
        return source().getInitializer().getParentReference();
    }

    public String getSelectedParentId() {
        return selectedParent().getId();
    }

    public List<R> getData() {
        return data;
    }

    public void setData(List<R> data) {
        boolean wasEmpty = isEmpty();
        this.data = data;
        // on force toujours la propagation de la liste
        firePropertyChange(PROPERTY_DATA, null, data);
        firePropertyChange(PROPERTY_EMPTY, wasEmpty, isEmpty());
        setSelectedDatas(null);
        setLayoutName(isEmpty() ? "empty" : "list");
    }

    public List<R> getSelectedDatas() {
        return selectedDatas;
    }

    public List<ToolkitIdLabel> getSelectedDatasLabel() {
        return selectedDatas.stream().map(d -> {
            ToolkitIdLabel label = d.toLabel();
            label.registerDecorator(labelDecorator);
            return label;
        }).collect(Collectors.toList());
    }

    public void setSelectedDatas(List<R> selectedDatas) {
        List<R> old = getSelectedDatas();
        this.selectedDatas = selectedDatas;
        if (log.isDebugEnabled()) {
            log.debug("New selected datas : " + selectedDatas);
        }
        firePropertyChange(PROPERTY_SELECTED_DATAS, old, selectedDatas);
        firePropertyChange(PROPERTY_ONE_SELECTED_DATA, isOneSelectedData());
        firePropertyChange(PROPERTY_ONE_OR_MORE_SELECTED_DATA, isOneOrMoreSelectedData());
    }

    public Optional<R> getSelectedData() {
        return Optional.ofNullable(isOneSelectedData() ? selectedDatas.get(0) : null);
    }

    public String getSelectedDataId() {
        return getOptionalSelectedDataId().orElse(null);
    }

    public Optional<String> getOptionalSelectedDataId() {
        return getSelectedData().map(DtoReference::getId);
    }

    public R getSafeSelectedData() {
        return isOneSelectedData() ? selectedDatas.get(0) : null;
    }

    public boolean isOneSelectedData() {
        return selectedDatas != null && selectedDatas.size() == 1;
    }

    public boolean isOneOrMoreSelectedData() {
        return selectedDatas != null && !selectedDatas.isEmpty();
    }

    public boolean isEmpty() {
        return data == null || data.isEmpty();
    }

    public String getLayoutName() {
        return layoutName;
    }

    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
        firePropertyChange(PROPERTY_LAYOUT_NAME, layoutName);
    }

    public Set<String> getSelectedIds() {
        return getSelectedDatas().stream().map(IdDto::getId).collect(Collectors.toSet());
    }

    @Override
    public void close() {
        super.close();
        referenceCache.destroy();
        data = null;
        selectedDatas = null;
    }

    public ReferencesCache getReferenceCache() {
        return referenceCache;
    }

    public ReferencesCache createReferenceCache(boolean hideDisabledReferential) {
        return null;
    }
}
