package fr.ird.observe.client.datasource.editor.api.content.data.rlist;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.GroupByCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ReferenceContainerCapability;
import fr.ird.observe.dto.data.DataGroupByDto;

import java.util.Enumeration;
import java.util.Objects;

/**
 * Created on 04/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentRootListUINavigationCapability<N extends ContentRootListUINavigationNode> implements GroupByCapability<N>, ReferenceContainerCapability<N> {

    private final N node;

    protected ContentRootListUINavigationCapability(N node) {
        this.node = Objects.requireNonNull(node);
    }

    @Override
    public DataGroupByDto<?> getReference() {
        return getNode().getParentReference();
    }

    @Override
    public void updateReference(DataGroupByDto<?> reference) {
        getNode().getInitializer().setParentReference(reference);
        Enumeration<?> children = getNode().children();
        while (children.hasMoreElements()) {
            ContentRootOpenableUINavigationNode childrenNode = (ContentRootOpenableUINavigationNode) children.nextElement();
            childrenNode.getInitializer().setParentReference(() -> reference);
        }
    }

    @Override
    public N getNode() {
        return node;
    }
}
