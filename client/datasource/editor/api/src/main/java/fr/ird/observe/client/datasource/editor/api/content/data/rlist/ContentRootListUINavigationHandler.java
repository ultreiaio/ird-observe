package fr.ird.observe.client.datasource.editor.api.content.data.rlist;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationHandler;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.reference.DtoReference;

import java.awt.Color;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 24/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentRootListUINavigationHandler<N extends ContentRootListUINavigationNode> extends NavigationHandler<N> {

    private final N node;

    public ContentRootListUINavigationHandler(N node) {
        this.node = Objects.requireNonNull(node);
    }

    @Override
    public abstract ContentRootListUINavigationCapability<N> getCapability();

    @Override
    public abstract ContentRootListUINavigationContext getContext();

    @Override
    public final N getNode() {
        return node;
    }

    @Override
    public final void buildChildren() {
        getCapability().buildChildren();
    }

    @Override
    public final int getNodePosition(DtoReference reference) {
        return getCapability().getNodePosition(reference);
    }

    @Override
    public String getText() {
        return node.getParentReference().toString() + getChildCountText();
    }

    @Override
    protected Color getColor0(Supplier<Color> defaultValue) {
        if (!getNode().getInitializer().isShowData()) {
            return getDisabledColor();
        }
        DataGroupByDto<?> parentReference = getNode().getParentReference();
        if (parentReference.isDisabled()) {
            return getDisabledColor();
        }
        if (getChildCount() == 0) {
            return getEmptyColor();
        }
        return super.getColor0(defaultValue);
    }

    @Override
    public final String getContentTitle() {
        String parentStr = node.getParentReference().toString();
        return parentStr + " - " + node.getScope().getI18nTranslation("type") + getChildCountText();
    }

    @Override
    public final ContentMode getContentMode() {
        ContentMode contentMode = super.getContentMode();
        if (contentMode != null) {
            return contentMode;
        }
        ContentRootListUINavigationInitializer initializer = node.getInitializer();
        boolean showData = initializer.isShowData();
        if (!showData) {
            return ContentMode.READ;
        }
        if (initializer.isOpen()) {
            // there is a child open
            return ContentMode.UPDATE;
        }
        // in any other case, we can open or create (thanks to new Navigation API)
        return ContentMode.CREATE;
    }

    @Override
    public boolean isLeaf() {
        return getChildCount() == 0;
    }

    protected int getChildCount() {
        return node.isLoaded() ? node.getChildCount() : node.getInitializer().getInitialCount();
    }

    protected String getChildCountText() {
        return String.format(" (%d)", getChildCount());
    }
}
