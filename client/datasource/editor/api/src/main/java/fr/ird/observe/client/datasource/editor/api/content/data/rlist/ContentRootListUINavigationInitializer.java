package fr.ird.observe.client.datasource.editor.api.content.data.rlist;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.services.service.data.RootEmptyChildrenDataReferenceSet;
import io.ultreia.java4all.decoration.Decorator;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 25/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentRootListUINavigationInitializer extends NavigationInitializer<ContentRootListUINavigationContext> {
    /**
     * How to get show data information.
     */
    private final Function<DataGroupByDto<?>, Boolean> showData;
    private final RootEmptyChildrenDataReferenceSet<? extends RootOpenableDto, ? extends DataDtoReference> references;
    private DataGroupByDto<?> parentReference;
    private Decorator decorator;
    private boolean open;

    public ContentRootListUINavigationInitializer(NavigationScope scope, DataGroupByDto<?> parentReference, RootEmptyChildrenDataReferenceSet<? extends RootOpenableDto, ? extends DataDtoReference> references) {
        super(scope);
        String showDataPropertyName = scope.getShowDataPropertyName();
        this.showData = showDataPropertyName == null ? e -> true : e -> e.get(showDataPropertyName);
        this.parentReference = Objects.requireNonNull(parentReference);
        this.references = references;
    }

    @Override
    protected Object init(NavigationContext<ContentRootListUINavigationContext> context) {
        decorator = context.getDecoratorService().getDecoratorByType(getScope().getMainReferenceType(), getScope().getDecoratorClassifier());
        return getParentReference();
    }

    @Override
    protected void open(NavigationContext<ContentRootListUINavigationContext> context) {
        open = true;
        references.get().stream().forEach(e -> e.registerDecorator(decorator));
    }

    @Override
    protected void reload(NavigationContext<ContentRootListUINavigationContext> context) {
        references.reload();
    }

    public boolean isShowData() {
        return showData.apply(parentReference);
    }

    @Override
    public String toPath() {
        return parentReference.getFilterValue();
    }

    public DataGroupByDto<?> getParentReference() {
        return parentReference;
    }

    public void setParentReference(DataGroupByDto<?> parentReference) {
        this.parentReference = parentReference;
    }

    public List<? extends DataDtoReference> getReferences() {
        return references.get().toArrayList();
    }

    public List<? extends DataDtoReference> getReferencesWithout(Set<String> idsToExclude) {
        //FIXME Add this in framework (.without(idsToExclude)
        return references.get().stream().filter(r -> !idsToExclude.contains(r.getId())).collect(Collectors.toList());
    }

    public final boolean isOpen() {
        IdNode<? extends IdDto> editNode = getEditNode();
        if (editNode.isDisabled()) {
            return false;
        }
        //FIXME We can't ask anything on references until node is open, otherwise we will load all the root list nodes...
        //FIXME Need to get the groupByValue for the root edit node
        if (!open) {
            return false;
        }
        String editNodeId = editNode.getId();
        return references.get().stream().anyMatch(r -> editNodeId.equals(r.getId()));
    }

    @SuppressWarnings("unchecked")
    public <R extends DataDtoReference> Comparator<R> getReferenceComparator() {
        return (Comparator<R>) references.getComparator();
    }

    public int getInitialCount() {
        return references.getInitialCount();
    }
}

