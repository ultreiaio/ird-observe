package fr.ird.observe.client.datasource.editor.api.content.data.rlist;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.services.service.data.RootEmptyChildrenDataReferenceSet;

import java.awt.Font;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 07/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentRootListUINavigationNode extends NavigationNode {

    public static <D extends RootOpenableDto, R extends DataDtoReference, N extends ContentRootListUINavigationNode> N init(N node, DataGroupByDto<?> parentId, RootEmptyChildrenDataReferenceSet<D, R> references) {
        ContentRootListUINavigationInitializer initializer = new ContentRootListUINavigationInitializer(node.getScope(), parentId, references);
        node.init(initializer);
        return node;
    }

    @Override
    public ContentRootListUINavigationInitializer getInitializer() {
        return (ContentRootListUINavigationInitializer) super.getInitializer();
    }

    @Override
    public ContentRootListUINavigationContext getContext() {
        return (ContentRootListUINavigationContext) super.getContext();
    }

    @Override
    public ContentRootListUINavigationHandler<?> getHandler() {
        return (ContentRootListUINavigationHandler<?>) super.getHandler();
    }

    @Override
    public ContentRootListUINavigationCapability<?> getCapability() {
        return (ContentRootListUINavigationCapability<?>) super.getCapability();
    }

    public List<? extends DataDtoReference> getReferences() {
        return getInitializer().getReferences();
    }

    public DataGroupByDto<?> getParentReference() {
        return getInitializer().getParentReference();
    }

    public NavigationNode addMissingChildren(String newIdToSelect) {
        ContentRootListUINavigationInitializer initializer = getInitializer();
        Set<String> existingIds = getChildrenReferences(getScope().getMainReferenceType()).stream().map(IdDto::getId).collect(Collectors.toSet());
        List<? extends DataDtoReference> references = initializer.getReferencesWithout(existingIds);
        ContentRootListUINavigationCapability<?> capability = getCapability();
        NavigationNode result = null;
        for (DataDtoReference reference : references) {
            capability.insertChildNode(reference);
        }
        if (newIdToSelect != null) {
            result = findChildById(newIdToSelect);
        }
        return result;
    }


    @Override
    public Font getNodeFont(Font defaultFont) {
        if (!getInitializer().isOpen()) {
            return defaultFont.deriveFont(Font.PLAIN);
        }
        return defaultFont.deriveFont(Font.BOLD);
    }
}
