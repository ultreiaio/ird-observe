package fr.ird.observe.client.datasource.editor.api.content.data.rlist.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUI;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.bulk.BulkModifyUI;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.bulk.BulkModifyUIModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.navigation.tree.ModuleGroupByHelper;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.spi.ui.BusyLayerUI;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.JDialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Created at 25/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class BulkModify<D extends RootOpenableDto, R extends DataDtoReference, U extends ContentRootListUI<D, R, U>> extends ContentRootListUIActionSupport<D, R, U> implements ConfigureMenuAction<U> {

    public BulkModify(Class<D> dataType) {
        super(dataType, I18n.t("observe.data.Trip.action.bulkModify"), I18n.t("observe.data.Trip.action.bulkModify"), "changeId", ObserveKeyStrokesEditorApi.KEY_STROKE_BULK_MODIFY);
        setCheckMenuItemIsArmed(false);
    }

    public static <D extends RootOpenableDto, R extends DataDtoReference, U extends ContentRootListUI<D, R, U>> void installAction(U ui) {
        BulkModify<D, R, U> action = new BulkModify<>(ui.getModel().getSource().getScope().getMainType());
        init(ui, Objects.requireNonNull(ui).getBulk(), action);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {

        NavigationTreeConfig navigationTreeConfig = getDataSourceEditor().getNavigationUI().getTree().getModel().getConfig();
        if (!navigationTreeConfig.isLoadData()) {
            // can't search, data are not loaded
            throw new IllegalStateException("LoadData is off on navigation config, can't do a bulk modify");
        }
        String moduleName = navigationTreeConfig.getModuleName();
        BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByName(moduleName);
        BulkModifyUIModel<R> model = new BulkModifyUIModel<>(navigationTreeConfig,
                                                             getClientConfig().isShowSensibleCriteriaInTripBulkChanges(),
                                                             new ModuleGroupByHelper(businessModule),
                                                             getMainUI().getBusyModel(),
                                                             ui.getModel().getStates().createReferenceCache(!navigationTreeConfig.isLoadDisabledGroupBy()),
                                                             ui.getModel().getStates().getSelectedDatas());

        BulkModifyUI<R> content = new BulkModifyUI<>(UIHelper.initialContext(ui, model));
        JDialog dialog = new JDialog(getMainUI(), I18n.t("observe.data.Trip.bulkModify.title", model.getData().size()), true);
        dialog.setMinimumSize(new Dimension(800, 200));
        dialog.setContentPane(content);

        // block dialog when busyModel model is busy
        BusyLayerUI.create(dialog, model.getBusyModel());

        dialog.pack();
        UIHelper.center(getMainUI(), dialog);
        dialog.setVisible(true);
    }

}


