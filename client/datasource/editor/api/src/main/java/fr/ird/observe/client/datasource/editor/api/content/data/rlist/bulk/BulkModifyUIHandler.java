package fr.ird.observe.client.datasource.editor.api.content.data.rlist.bulk;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ComboBoxListCellRenderer;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.bulk.actions.BulkModifyUIActionSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.renderer.DefaultListRenderer;
import org.jdesktop.swingx.renderer.StringValue;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.CardLayout2;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.List;

/**
 * Created at 25/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class BulkModifyUIHandler<R extends DataDtoReference> implements UIHandler<BulkModifyUI<R>>, WithClientUIContextApi {
    public static final String ESCAPE_ACTION = "escape";
    private static final Logger log = LogManager.getLogger(BulkModifyUIHandler.class);

    @Override
    public void afterInit(BulkModifyUI<R> ui) {
        BulkModifyUIModel<R> model = ui.getModel();

        @SuppressWarnings("unchecked") ListCellRenderer<DataGroupByDtoDefinition<?, ?>> availableCriteriaRenderer =
                new DefaultListRenderer((StringValue) value -> {
                    if (value == null) {
                        return null;
                    }
                    DataGroupByDtoDefinition<?, ?> definition = (DataGroupByDtoDefinition<?, ?>) value;
                    return definition.getDefinitionLabel() + String.format(" ( %s )", definition.getMandatoryLabel());
                });
        ui.getAvailableCriteria().setRenderer(availableCriteriaRenderer);
        InputMap inputMap = ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = ui.getActionMap();
        AbstractAction escapeAction = new AbstractAction(ESCAPE_ACTION) {
            @Override
            public void actionPerformed(ActionEvent e) {
                BulkModifyUIActionSupport.close(ui);
            }
        };
        inputMap.put(ObserveKeyStrokesEditorApi.KEY_STROKE_ESCAPE, ESCAPE_ACTION);
        actionMap.put(ESCAPE_ACTION, escapeAction);
    }

    void updateSelectedCriteria(BulkModifyUI<R> ui, ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            JComboBox<?> source = (JComboBox<?>) event.getSource();
            DataGroupByDtoDefinition<?, ?> selectedItem = (DataGroupByDtoDefinition<?, ?>) source.getSelectedItem();
            updateSelectedCriteria(ui, selectedItem);
        }
    }

    void updateSelectedCriteria(BulkModifyUI<R> ui, DataGroupByDtoDefinition<?, ?> selectedItem) {
        log.info("new selected criteria {}", selectedItem);
        ui.getModel().setSelectedCriteria(selectedItem);
        if (selectedItem == null) {
            ui.getCriteriaValuesPanel().setVisible(false);
            ui.getModel().setSelectedValue(null);
            return;
        }
        FilterableComboBox<ReferentialDtoReference> oldSafeBox = getSafeComboBox(ui);
        if (oldSafeBox!=null) {
            oldSafeBox.setSelectedItem(null);
        }
        ui.getCriteriaValuesPanel().setVisible(true);
        @SuppressWarnings("unchecked") Class<ReferentialDtoReference> dataType = (Class<ReferentialDtoReference>) selectedItem.getPropertyType();
        FilterableComboBox<ReferentialDtoReference> safeComboBox = getSafeComboBox(ui);

        String key = selectedItem.getPropertyName();
        CardLayout2 safeRefsPanelLayout = ui.getCriteriaValuesPanelLayout();
        JPanel safeRefsPanel = ui.getCriteriaValuesPanel();
        if (safeRefsPanelLayout.contains(key)) {
            // la liste déroulante existe deja pour ce type
            FilterableComboBox<?> list = (FilterableComboBox<?>) safeRefsPanelLayout.getComponent(safeRefsPanel, key);
            list.setEnabled(true);
            if (!safeComboBox.equals(list)) {
                // on l'affiche
                safeRefsPanelLayout.show(safeRefsPanel, key);
            }
            updateCanApply(ui);
        } else {
            // la liste n'existe pas encore
            List<ReferentialDtoReference> data = ui.getModel().getReferencesCache().getReferentialReferences(selectedItem.getPropertyName());
            Decorator decorator = getDecoratorService().getDecoratorByType(dataType);
            data.forEach(decorator::decorate);
            FilterableComboBox<ReferentialDtoReference> box = UIHelper.newFilterableComboBox(
                    dataType,
                    decorator,
                    data);
            box.setBean(ui.getModel());
            box.setProperty(BulkModifyUIModel.PROPERTY_SELECTED_VALUE);
            box.setShowReset(true);
            box.getModel().addPropertyChangeListener(FilterableComboBoxModel.PROPERTY_SELECTED_ITEM, evt ->  {
                ReferentialDtoReference newValue = (ReferentialDtoReference) evt.getNewValue();
                ui.getModel().setSelectedValue(newValue);
                updateCanApply(ui);
            });
            JComboBox<ReferentialDtoReference> combobox = box.getCombobox();
            ListCellRenderer<ReferentialDtoReference> toolTipRenderer = new ComboBoxListCellRenderer<>(combobox.getRenderer());
            combobox.setRenderer(toolTipRenderer);
            safeRefsPanel.add(box, key);
            safeRefsPanelLayout.show(safeRefsPanel, key);
        }
    }

    public void close(BulkModifyUI<R> ui) {
        // clean models
        ui.getModel().close();
    }

    @SuppressWarnings("unchecked")
    public FilterableComboBox<ReferentialDtoReference> getSafeComboBox(BulkModifyUI<R> ui) {
        JPanel panel = ui.getCriteriaValuesPanel();
        return (FilterableComboBox<ReferentialDtoReference>) ui.getCriteriaValuesPanelLayout().getVisibleComponent(panel);
    }


    private void updateCanApply(BulkModifyUI<R> ui) {
        ui.getModel().fireCanApply();
    }
}
