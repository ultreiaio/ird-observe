package fr.ird.observe.client.datasource.editor.api.content.data.rlist.bulk;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.navigation.tree.ModuleGroupByHelper;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created at 25/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
@GenerateJavaBeanDefinition
public class BulkModifyUIModel<R extends DataDtoReference> extends AbstractJavaBean {

    public static final String PROPERTY_SELECTED_CRITERIA = "selectedCriteria";
    public static final String PROPERTY_SELECTED_VALUE = "selectedValue";
    public static final String PROPERTY_CAN_APPLY = "canApply";
    /**
     * Sensible criteria to hide if client configuration option is off.
     * <p>
     * See {@link ClientConfig#isShowSensibleCriteriaInTripBulkChanges()}
     */
    private static final Set<String> SENSIBLE_CRITERIA = Set.of(
            "dataLlCommonTripGroupByOcean",
            "dataPsCommonTripGroupByOcean",
            "dataPsCommonTripGroupByLandingHarbour"
    );
    /**
     * Module groupBy helper.
     */
    private final ModuleGroupByHelper moduleGroupByHelper;
    /**
     * Busy model used to block ui when doing something.
     */
    private final BusyModel busyModel;
    /**
     * The navigation tree config (used to create this criteria)
     */
    private final NavigationTreeConfig navigationTreeConfig;
    /**
     * References caches.
     */
    private final ReferencesCache referencesCache;
    /**
     * Available criteria types.
     */
    private final DefaultComboBoxModel<DataGroupByDtoDefinition<?, ?>> availableCriteria = new DefaultComboBoxModel<>();
    /**
     * Data to modify.
     */
    private final List<R> data;
    /**
     * Navigation configuration criteria.
     */
    private final DataGroupByDtoDefinition<?, ?> navigationCriteriaDefinition;
    /**
     * Selected criteria name.
     */
    private DataGroupByDtoDefinition<?, ?> selectedCriteria;
    /**
     * Selected criteria value.
     */
    private ReferentialDtoReference selectedValue;

    public BulkModifyUIModel(NavigationTreeConfig navigationTreeConfig,
                             boolean showSensibleCriteriaInTripBulkChanges,
                             ModuleGroupByHelper moduleGroupByHelper,
                             BusyModel busyModel,
                             ReferencesCache referencesCache,
                             List<R> data) {

        this.navigationTreeConfig = Objects.requireNonNull(navigationTreeConfig);
        this.moduleGroupByHelper = Objects.requireNonNull(moduleGroupByHelper);
        this.busyModel = Objects.requireNonNull(busyModel);
        this.availableCriteria.addAll(moduleGroupByHelper.getGroupByDefinitions().stream().filter(c -> acceptCriteria(c, showSensibleCriteriaInTripBulkChanges)).collect(Collectors.toList()));
        this.referencesCache = Objects.requireNonNull(referencesCache);
        this.data = Objects.requireNonNull(data);
        this.navigationCriteriaDefinition = getModuleGroupByHelper().getDefinition(navigationTreeConfig.getGroupByName());
        // Using the navigation tree config criteria (if possible)
        if (BulkModifyUIModel.acceptCriteria(navigationCriteriaDefinition, showSensibleCriteriaInTripBulkChanges)) {
            setSelectedCriteria(navigationCriteriaDefinition);
        } else {
            // otherwise use the first available criteria
            setSelectedCriteria(availableCriteria.getElementAt(0));
        }
    }

    public static boolean acceptCriteria(DataGroupByDtoDefinition<?, ?> d, boolean showSensibleCriteriaInTripBulkChanges) {
        if (!d.isQualitative() || d.isQualitativeWithSecondLevel()) {
            return false;
        }
        return showSensibleCriteriaInTripBulkChanges || !SENSIBLE_CRITERIA.contains(d.getName());
    }

    public DataGroupByDtoDefinition<?, ?> getNavigationCriteriaDefinition() {
        return navigationCriteriaDefinition;
    }

    public BusyModel getBusyModel() {
        return busyModel;
    }

    public NavigationTreeConfig getNavigationTreeConfig() {
        return navigationTreeConfig;
    }

    public ModuleGroupByHelper getModuleGroupByHelper() {
        return moduleGroupByHelper;
    }

    public ReferencesCache getReferencesCache() {
        return referencesCache;
    }

    public List<R> getData() {
        return data;
    }

    public ComboBoxModel<DataGroupByDtoDefinition<?, ?>> getAvailableCriteria() {
        return availableCriteria;
    }

    public DataGroupByDtoDefinition<?, ?> getSelectedCriteria() {
        return selectedCriteria;
    }

    public void setSelectedCriteria(DataGroupByDtoDefinition<?, ?> selectedCriteria) {
        DataGroupByDtoDefinition<?, ?> oldValue = this.selectedCriteria;
        this.selectedCriteria = selectedCriteria;
        firePropertyChange(PROPERTY_SELECTED_CRITERIA, oldValue, selectedCriteria);
        fireCanApply();
    }

    public ReferentialDtoReference getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(ReferentialDtoReference selectedValue) {
        ReferentialDtoReference oldValue = this.selectedValue;
        this.selectedValue = selectedValue;
        firePropertyChange(PROPERTY_SELECTED_VALUE, oldValue, selectedValue);
        fireCanApply();
    }

    public boolean isCanApply() {
        if (getSelectedCriteria() == null) {
            return false;
        }
        if (getSelectedValue() == null) {
            return !getSelectedCriteria().isPropertyMandatory();
        }
        return true;
    }

    public void close() {
        selectedCriteria = null;
        selectedValue = null;
    }

    void fireCanApply() {
        firePropertyChange(PROPERTY_CAN_APPLY, isCanApply());
    }
}
