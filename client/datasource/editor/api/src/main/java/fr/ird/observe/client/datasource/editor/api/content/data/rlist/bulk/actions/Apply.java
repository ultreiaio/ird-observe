package fr.ird.observe.client.datasource.editor.api.content.data.rlist.bulk.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.bulk.BulkModifyUI;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.bulk.BulkModifyUIModel;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.spi.ui.BusyModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created at 25/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class Apply<R extends DataDtoReference> extends BulkModifyUIActionSupport<R> implements WithClientUIContextApi {
    private static final Logger log = LogManager.getLogger(Apply.class);

    public Apply() {
        super(t("observe.ui.datasource.storage.action.apply"), t("observe.ui.datasource.storage.action.apply.tip"), "accept", ObserveKeyStrokesEditorApi.KEY_STROKE_SAVE_DATA);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, BulkModifyUI<R> ui) {
        BulkModifyUIModel<?> model = ui.getModel();
        BusyModel busyModel = model.getBusyModel();
        busyModel.addTask("Apply");
        try {
            DataGroupByDtoDefinition<?, ?> selectedCriteria = model.getSelectedCriteria();
            ReferentialDtoReference selectedValue = model.getSelectedValue();
            log.info("Will change property {} to value {} on {} trip(s).", selectedCriteria.getDefinitionLabel(), selectedValue, model.getData().size());

            @SuppressWarnings("unchecked") Class<RootOpenableDto> dataType = (Class<RootOpenableDto>) selectedCriteria.getDataType();
            ObserveSwingDataSource mainDataSource = getDataSourcesManager().getMainDataSource();
            mainDataSource.getRootOpenableService().updateQualitativeProperty(dataType, selectedCriteria.toParameter(selectedValue == null ? null : selectedValue.getTopiaId(), null), model.getData().stream().map(IdDto::getId).collect(Collectors.toSet()));
            mainDataSource.setModified(true);
            if (selectedCriteria.equals(model.getNavigationCriteriaDefinition())) {
                // Need to rebuild the navigation tree
                DataSourceEditor currentBody = (DataSourceEditor) getMainUI().getMainUIBodyContentManager().getCurrentBody().get();
                log.info("Will rebuild navigation result");
                NavigationTree tree = currentBody.getNavigationUI().getTree();
                NavigationNode selectedNode = tree.getSelectedNode();
                tree.getModel().populate();
                String[] paths = selectedNode.toPaths();
                NavigationNode nodeFromPath = tree.getModel().getNodeFromPath(paths, false);
                if (nodeFromPath != null) {
                    tree.reSelectSafeNode(nodeFromPath);
                } else {
                    tree.selectInitialNode();
                }
                SwingUtilities.invokeLater(tree::repaint);
            }
        } finally {
            busyModel.popTask();
            JDialog parent = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, ui);
            parent.dispose();
        }
    }
}
