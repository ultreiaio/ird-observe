/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.data.ropen;

import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewRootOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowTechnicalInformations;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.actions.reset.ResetAction;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.RootOpenableDto;

import java.util.concurrent.Callable;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class ContentRootOpenableUIHandler<D extends RootOpenableDto, U extends ContentRootOpenableUI<D, U>> extends ContentUIHandler<U> {

    protected void installMoveAction() {
        // move action is optional
    }

    protected abstract void installCreateNewAction();

    protected abstract void installSaveAction();

    protected abstract void installDeleteAction();

//    protected DeletePanel computeDeleteDependenciesExtraMessage(DeletePanel deletePanel, Collection<ToolkitIdLabel> toDelete) {
//        Class<D> mainType = ui.getModel().getScope().getMainType();
//        return super.computeRootOpenDeleteDependenciesExtraMessage(mainType, deletePanel, toDelete);
//    }

    protected void installCreateNewOpenableAction(Callable<? extends NavigationNode> referenceMaker) {
        CreateNewRootOpenableUI.installAction(ui, ui.getModel().getScope().getNodeType(), NavigationNode::getParent, referenceMaker);
    }

    @Override
    public ContentRootOpenableUIModel<D> getModel() {
        return ui.getModel();
    }

    @Override
    protected final ContentRootOpenableUIInitializer<U> createContentUIInitializer(U ui) {
        return new ContentRootOpenableUIInitializer<>(ui);
    }

    @Override
    protected final ContentOpenWithValidator<U> createContentOpen(U ui) {
        ContentRootOpenableUIOpenExecutor<D, U> executor = new ContentRootOpenableUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor);
    }

    @Override
    public final ContentOpenWithValidator<U> getContentOpen() {
        return (ContentOpenWithValidator<U>) super.getContentOpen();
    }

    @Override
    public void onPrepareValidationContext() {
        super.onPrepareValidationContext();
        ui.getValidator().setContext(getModel().getStates().getValidationContext());
    }

    @Override
    public final void initActions() {
        installCreateNewAction();
        installResetAction();
        installSaveAction();
        installDeleteAction();
        installMoveAction();
        ui.getConfigurePopup().addSeparator();
        ShowTechnicalInformations.installAction(ui);
    }

    @Override
    public final void installChangeModeAction() {
        ChangeMode.installAction(ui);
    }

    protected final void installResetAction() {
        ResetAction.installAction(ui, ui.getReset());
    }
}
