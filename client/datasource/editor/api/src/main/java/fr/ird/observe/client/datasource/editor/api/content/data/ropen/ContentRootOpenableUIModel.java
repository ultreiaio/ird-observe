/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.data.ropen;

import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeleteRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowIdRequest;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUIModelSupport;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.ObserveServicesProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Le modèle pour un écran d'édition avec des fils.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public abstract class ContentRootOpenableUIModel<D extends RootOpenableDto> extends ContentSimpleUIModelSupport<D> {

    private static final Logger log = LogManager.getLogger(ContentRootOpenableUIModel.class);

    public ContentRootOpenableUIModel(ContentRootOpenableUINavigationNode source) {
        super(source);
    }

    public abstract Form<D> loadForm(ObserveServicesProvider servicesProvider, String id);

    public abstract Form<D> preCreateForm(ObserveServicesProvider servicesProvider, String parentId);

    public abstract DeleteRequestBuilder.StepBuild toDeleteRequest();

    @Override
    public ContentRootOpenableUINavigationNode getSource() {
        return (ContentRootOpenableUINavigationNode) super.getSource();
    }

    @Override
    public ContentRootOpenableUIModelStates<D> getStates() {
        return (ContentRootOpenableUIModelStates<D>) super.getStates();
    }

    public final ShowIdRequest getShowIdRequest() {
        return new ShowIdRequest(getSource()::getReference);
    }

    @Override
    public final Form<D> openForm(ObserveServicesProvider servicesProvider, String selectedId) {
        open();
        Form<D> form;
        if (selectedId == null) {
            String parentId = getStates().getSelectedParentId();
            log.info(String.format("%s PreCreate Form from parent id: %s", getPrefix(), parentId));
            form = preCreateForm(servicesProvider, parentId);
        } else {
            log.info(String.format("%s Load Form from id: %s", getPrefix(), selectedId));
            form = loadForm(servicesProvider, selectedId);
        }
        getStates().openForm(form);
        return form;
    }
}
