package fr.ird.observe.client.datasource.editor.api.content.data.ropen;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import io.ultreia.java4all.decoration.Decorator;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 25/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public final class ContentRootOpenableUINavigationInitializer extends NavigationInitializer<ContentRootOpenableUINavigationContext> {

    private Supplier<? extends DataGroupByDto<?>> parentReference;
    private DataDtoReference reference;
    private Decorator decorator;

    public ContentRootOpenableUINavigationInitializer(NavigationScope scope, Supplier<? extends DataGroupByDto<?>> parentReference, DataDtoReference reference) {
        super(scope);
        this.parentReference = Objects.requireNonNull(parentReference);
        this.reference = Objects.requireNonNull(reference);
    }

    public String getText() {
        return decorator.decorate(reference);
    }

    @Override
    protected DataDtoReference init(NavigationContext<ContentRootOpenableUINavigationContext> context) {
        decorator = context.getDecoratorService().getDecoratorByType(reference.getReferenceType(), getScope().getDecoratorClassifier());
        reference.registerDecorator(decorator);
        return getReference();
    }

    @Override
    protected void open(NavigationContext<ContentRootOpenableUINavigationContext> context) {
        updateSelectNodeId(getSelectId());
    }

    @Override
    protected void reload(NavigationContext<ContentRootOpenableUINavigationContext> context) {
        this.reference = context.reloadReference(getReference(), decorator);
    }

    @Override
    public String toPath() {
        return reference.getId();
    }

    public DataGroupByDto<?> getParentReference() {
        return parentReference.get();
    }

    public DataDtoReference getReference() {
        return reference;
    }

    public String getSelectId() {
        return getReference().getId();
    }

    public String getSelectedParentId() {
        return getParentReference().getId();
    }

    public boolean isOpen() {
        return Objects.equals(getSelectId(), getEditNodeId());
    }

    public boolean isPersisted() {
        return getSelectId() != null;
    }

    public boolean isNotPersisted() {
        return getSelectId() == null;
    }

    public void setParentReference(Supplier<? extends DataGroupByDto<?>> parentReference) {
        this.parentReference = Objects.requireNonNull(parentReference);
    }

    public void setReference(DataDtoReference reference) {
        this.reference = Objects.requireNonNull(reference);
    }

    public void updateEditNodeId(String id) {
        if (!getScope().isEditNode()) {
            throw new IllegalStateException(String.format("%s Can't update edit id on this node.", this));
        }
        getEditNode().setId(id);
    }

    public DataDtoReference updateReference(ContentRootOpenableUINavigationContext context, String id) {
        this.reference = context.getReference(getReference().getReferenceType(), id, decorator);
        return getReference();
    }
}
