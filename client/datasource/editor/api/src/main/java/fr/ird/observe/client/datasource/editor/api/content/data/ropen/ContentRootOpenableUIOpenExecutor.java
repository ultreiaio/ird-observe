package fr.ird.observe.client.datasource.editor.api.content.data.ropen;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentEditExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.form.Form;

/**
 * Created on 27/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentRootOpenableUIOpenExecutor<D extends RootOpenableDto, U extends ContentRootOpenableUI<D, U>> implements ContentOpenExecutor<U>, ContentEditExecutor<U> {

    @Override
    public void loadContent(U ui) {

        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();
        contentOpen.resetCoordinateEditors();
        //FIXME chemit 20100913 : il vaudrait le faire uniquement lors de l'édition
        // chaque arrive sur un écran invalide le cache de validation
        getClientValidationContext().reset();
        ContentUIHandler.removeAllMessages(ui);
        openModel(ui);
    }

    @Override
    public void onOpened(U ui) {
        ContentRootOpenableUIHandler<D, U> handler = ui.getHandler();
        handler.fixFormSize();
        handler.onEndOpenForCreateMode();
        handler.onEndOpenUI();
    }

    public void openModel(U ui) {
        ContentRootOpenableUIModel<D> model = ui.getModel();
        String selectedId = model.getStates().getSelectedId();
        ContentRootOpenableUIHandler<D, U> handler = ui.getHandler();
        ContentOpenWithValidator<U> contentOpen = handler.getContentOpen();
        Form<D> form = model.openForm(handler, selectedId);
        contentOpen.onOpenForm(form);
        handler.onOpenAfterOpenModel();
    }

    @Override
    public void startEdit(U ui) {
        ContentRootOpenableUIModelStates<D> states = ui.getModel().getStates();
        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();
        contentOpen.prepareValidationContext();
        contentOpen.installValidators(states.getBean());
        contentOpen.startEditTabUIModel();

        states.setModified(states.isCreatingMode());
    }

    @Override
    public void stopEdit(U ui) {
        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();
        ClientValidationContext context = getClientValidationContext();
        ContentUIModelStates states = ui.getModel().getStates();

        contentOpen.stopEditTabUIModel();

        context.reset();
        ui.setValidatorChanged(false);

        // mark ui as not editing
        states.setEditing(false);

        // mark ui as valid while not editing
        states.setValid(true);

        // mark ui as not modified
        states.setModified(false);

        // detach all validators
        contentOpen.uninstallValidators();
    }
}
