package fr.ird.observe.client.datasource.editor.api.content.data.ropen.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveUIAdapter;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTreeModel;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ReferenceContainerCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.navigation.tree.NavigationResult;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.JOptionPanes;

import java.util.Objects;

/**
 * Created on 16/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class SaveContentRootOpenableUIAdapter<D extends RootOpenableDto, U extends ContentRootOpenableUI<D, U>> implements SaveUIAdapter<D, U> {

    private static final Logger log = LogManager.getLogger(SaveContentRootOpenableUIAdapter.class);

    @Override
    public final void adaptUi(DataSourceEditor dataSourceEditor, U ui, boolean notPersisted, D bean) {

        ContentRootOpenableUIModel<?> model = ui.getModel();
        ui.stopEdit();

        ContentRootOpenableUINavigationNode node = model.getSource();
        NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();

        // get old groupBy dto
        @SuppressWarnings("unchecked") DataGroupByDto<D> oldGroupByDto = (DataGroupByDto<D>) node.getParentReference();

        ToolkitTreeFlatModelRootRequest navigationRequest = ((RootNavigationNode) node.getRoot()).getInitializer().getRequest();
        ComputeNavigationRequestChange<D,U> computeNavigationRequestChange = new ComputeNavigationRequestChange<>(oldGroupByDto, navigationRequest, bean);
        computeNavigationRequestChange.warnUserIfNecessary(ui);

        // We need to inject ot node the new reference (it could does not know the id if not persisted)
        // As I prefer to use a unique code (for persisted or not, keep it like this)
        node = updateReference(dataSourceEditor.getNavigationUI(), node, computeNavigationRequestChange);

        tree.reSelectSafeNodeThen(node, () -> {
            dataSourceEditor.getModel().resetFromPreviousUi(ui);
            if (notPersisted) {
                tree.expandPath(tree.getSelectionPath());
            }
        });
    }

    public static class ComputeNavigationRequestChange<D extends RootOpenableDto, U extends ContentRootOpenableUI<D, U>> {
        private final DataGroupByDto<D> oldGroupByDto;
        private final ToolkitTreeFlatModelRootRequest navigationRequest;
        private final D bean;
        private final String newGroupByValue;
        private final boolean groupByChanged;
        private final boolean changeNavigationRequest;
        private final boolean addNullGroupBy;
        private final boolean addDisabledGroupBy;

        ComputeNavigationRequestChange(DataGroupByDto<D> oldGroupByDto, ToolkitTreeFlatModelRootRequest navigationRequest, D bean) {
            this.oldGroupByDto = oldGroupByDto;
            this.navigationRequest = navigationRequest;
            this.bean = bean;
            this.newGroupByValue = oldGroupByDto.definition().toGroupByValue(bean, navigationRequest.getGroupByFlavor());
            this.groupByChanged = !Objects.equals(oldGroupByDto.getFilterValue(), newGroupByValue);
            boolean changeNavigationRequest = false;
            boolean addNullGroupBy = false;
            boolean addDisabledGroupBy = false;
            if (groupByChanged) {
                // check if old navigation request is compliant with new groupBy value
                if (newGroupByValue == null) {
                    if (!navigationRequest.isLoadNullGroupBy()) {
                        // need to add null groupBy in navigation request
                        changeNavigationRequest = true;
                        addNullGroupBy = true;
                    }
                } else {
                    if (oldGroupByDto.definition().isQualitative() && !navigationRequest.isLoadDisabledGroupBy()) {
                        ReferentialDtoReference groupByObjectValue = (ReferentialDtoReference) oldGroupByDto.definition().toGroupByObjectValue(bean);
                        if (groupByObjectValue.isDisabled()) {
                            // need to add disabled groupBy in navigation request
                            changeNavigationRequest = true;
                            addDisabledGroupBy = true;
                        }
                    }
                }
            }
            this.changeNavigationRequest = changeNavigationRequest;
            this.addNullGroupBy = addNullGroupBy;
            this.addDisabledGroupBy = addDisabledGroupBy;
        }

        public void warnUserIfNecessary(U ui) {
            if (!isChangeNavigationRequest()) {
                return;
            }
            // display message to user
            if (isAddNullGroupBy()) {
                // add null groupBy in navigation request
                JOptionPanes.displayWarning(ui, I18n.t("observe.ui.tree.need.reload.title"), I18n.t("observe.ui.tree.need.reload.message"));
                return;
            }
            if (isAddDisabledGroupBy()) {
                // add disabled groupBy in navigation request
                JOptionPanes.displayWarning(ui, I18n.t("observe.ui.tree.need.reload.title"), I18n.t("observe.ui.tree.need.reload.message"));
            }
        }

        public DataGroupByDto<D> getOldGroupByDto() {
            return oldGroupByDto;
        }

        public ToolkitTreeFlatModelRootRequest getNavigationRequest() {
            return navigationRequest;
        }

        public D getBean() {
            return bean;
        }

        public String getNewGroupByValue() {
            return newGroupByValue;
        }

        public boolean isGroupByChanged() {
            return groupByChanged;
        }

        public boolean isChangeNavigationRequest() {
            return changeNavigationRequest;
        }

        public boolean isAddNullGroupBy() {
            return addNullGroupBy;
        }

        public boolean isAddDisabledGroupBy() {
            return addDisabledGroupBy;
        }

        NavigationResult updateNavigationResult(NavigationTreeModel navigationTreeModel) {
            NavigationTreeConfig navigationTreeConfig = navigationTreeModel.getConfig();
            if (isAddNullGroupBy()) {
                navigationTreeConfig.setLoadNullGroupBy(true);
                navigationTreeModel.getClientConfig().saveTreeConfig(navigationTreeConfig);
                navigationRequest.setLoadNullGroupBy(true);
            }
            if (isAddDisabledGroupBy()) {
                navigationTreeConfig.setLoadDisabledGroupBy(true);
                navigationRequest.setLoadDisabledGroupBy(true);
                navigationTreeModel.getClientConfig().saveTreeConfig(navigationTreeConfig);
            }
            return navigationTreeModel.updateNavigationResult();
        }
    }

    public ContentRootOpenableUINavigationNode updateReference(NavigationUI navigationUI,
                                                               ContentRootOpenableUINavigationNode node,
                                                               ComputeNavigationRequestChange<D, U> computeNavigationRequestChange) {
        boolean notPersisted = node.getInitializer().isNotPersisted();
        String id = computeNavigationRequestChange.getBean().getId();
        DataGroupByDto<D> oldGroupByDto = computeNavigationRequestChange.getOldGroupByDto();
        boolean groupByChanged = computeNavigationRequestChange.isGroupByChanged();
        String newGroupByValue = computeNavigationRequestChange.getNewGroupByValue();
        ContentRootListUINavigationNode parent = node.getParent();
        if (groupByChanged) {
            // the navigation must be updated, new parent groupBy value has changed
            NavigationResult navigationResult = computeNavigationRequestChange.updateNavigationResult(navigationUI.getTree().getModel());
            // groupBy has changed (remove node from parent)
            RootNavigationNode rootNode = (RootNavigationNode) node.getRoot();
            node.removeFromParent();
            // remove old parent node if navigation tree config requires it
            boolean removeGroupBy = parent.getChildCount() == 0 && !rootNode.getInitializer().getRequest().isLoadEmptyGroupBy();
            if (removeGroupBy) {
                parent.removeFromParent();
            } else {
                // update old parent to root
                parent.reloadNodeDataToRoot();
            }
            // find parent
            parent = (ContentRootListUINavigationNode) rootNode.findChildById(newGroupByValue);
            if (parent == null) {
                // must create it (will update navigation result)
                DataGroupByDto<?> reference = navigationResult.getGroupBy(newGroupByValue);
                parent = rootNode.getCapability().insertChildNode(reference);
            } else {
                // update parent reference in node
                DataGroupByDto<?> parentReference = parent.getInitializer().getParentReference();
                node.getInitializer().setParentReference(() -> parentReference);
            }
        }

        DataDtoReference reference = node.getInitializer().updateReference(node.getContext(), id);
        if (notPersisted) {
            node.getInitializer().updateEditNodeId(id);
            node.getInitializer().updateSelectNodeId(id);
        }
        // reload node data
        node.reloadNodeDataAndChildren();

        if (notPersisted) {
            node.dirty();
            node.populateChildrenIfNotLoaded();
        }

        ReferenceContainerCapability<?> capability = parent.getCapability();
        if (groupByChanged) {
            if (parent.isNotLoaded()) {
                // will load all children even the updated one
                parent.populateChildrenIfNotLoaded();
                // need to get correct node from parent
                int newPosition = capability.getNodePosition(reference);
                node = (ContentRootOpenableUINavigationNode) parent.getChildAt(newPosition);
            } else {
                // need to add updated node
                int newPosition = capability.getNodePosition(reference);
                log.info(String.format("Change groupBy node from: %s to %s (position: %d)", oldGroupByDto.getFilterValue(), newGroupByValue, newPosition));
                parent.insert(node, newPosition);
            }
        } else {
            int oldPosition = parent.getIndex(node);
            int newPosition = capability.getNodePosition(reference);
            if (oldPosition != newPosition) {
                log.info(String.format("Move node from: %d to %d", oldPosition, newPosition));
                parent.moveNode(node, newPosition);
            }
        }
        // repaint selected node and his children
        node.nodeChangedDeep();

        // reload from parent to root
        parent.reloadNodeDataToRoot();

        // return current node (it could have changed (coming from his parent))
        return node;
    }
}
