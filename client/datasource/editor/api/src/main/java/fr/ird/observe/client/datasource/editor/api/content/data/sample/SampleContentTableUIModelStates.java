package fr.ird.observe.client.datasource.editor.api.content.data.sample;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIModelStates;
import fr.ird.observe.dto.data.AcquisitionMode;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.observation.SampleMeasureDto;
import fr.ird.observe.dto.referential.common.SizeMeasureTypeReference;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.common.WeightMeasureTypeReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created on 08/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public abstract class SampleContentTableUIModelStates<D extends DataDto, C extends ContainerChildDto> extends ContentTableUIModelStates<D, C> {

    private static final String PROPERTY_DEFAULT_SIZE_MEASURE_TYPE = "defaultSizeMeasureType";
    private static final String PROPERTY_DEFAULT_WEIGHT_MEASURE_TYPE = "defaultWeightMeasureType";
    private static final Logger log = LogManager.getLogger(SampleContentTableUIModelStates.class);
    private SizeMeasureTypeReference defaultSizeMeasureType;
    private WeightMeasureTypeReference defaultWeightMeasureType;
    private Map<String, SizeMeasureTypeReference> sizeMeasureTypeReferenceMap;
    private Map<String, WeightMeasureTypeReference> weightMeasureTypeReferenceMap;

    public SampleContentTableUIModelStates(ContentTableUIModel<D, C> model, D bean, C tableEditBean, String selectedId, boolean standalone) {
        super(model, bean, tableEditBean, selectedId, standalone);
        setWithIndex(true);
        addPropertyChangeListener(ContentTableUIModelStates.PROPERTY_FORM, evt -> onFormLoaded());
    }

    protected void onFormLoaded() {
        log.info("On form loaded, load default measure types.");
        loadSizeMeasureTypes(getReferenceCache());
        loadWeightMeasureTypes(getReferenceCache());
    }

    public abstract AcquisitionMode getDefaultAcquisitionMode();

    public SizeMeasureTypeReference getDefaultSizeMeasureType() {
        return defaultSizeMeasureType;
    }

    public void setDefaultSizeMeasureType(SizeMeasureTypeReference defaultSizeMeasureType) {
        SizeMeasureTypeReference oldValue = getDefaultSizeMeasureType();
        this.defaultSizeMeasureType = defaultSizeMeasureType;
        firePropertyChange(PROPERTY_DEFAULT_SIZE_MEASURE_TYPE, oldValue, defaultSizeMeasureType);
    }

    public WeightMeasureTypeReference getDefaultWeightMeasureType() {
        return defaultWeightMeasureType;
    }

    public void setDefaultWeightMeasureType(WeightMeasureTypeReference defaultWeightMeasureType) {
        WeightMeasureTypeReference oldValue = getDefaultWeightMeasureType();
        this.defaultWeightMeasureType = defaultWeightMeasureType;
        firePropertyChange(PROPERTY_DEFAULT_WEIGHT_MEASURE_TYPE, oldValue, defaultWeightMeasureType);
    }

    public void loadSizeMeasureTypes(ReferencesCache referenceCache) {
        List<SizeMeasureTypeReference> sizeMeasureTypeReferences = referenceCache.getReferentialReferences(SampleMeasureDto.PROPERTY_SIZE_MEASURE_TYPE);
        sizeMeasureTypeReferenceMap = Maps.uniqueIndex(sizeMeasureTypeReferences, SizeMeasureTypeReference::getId);
    }

    public void loadWeightMeasureTypes(ReferencesCache referenceCache) {
        List<WeightMeasureTypeReference> weightMeasureTypeReferences = referenceCache.getReferentialReferences(SampleMeasureDto.PROPERTY_WEIGHT_MEASURE_TYPE);
        weightMeasureTypeReferenceMap = Maps.uniqueIndex(weightMeasureTypeReferences, WeightMeasureTypeReference::getId);
    }

    public Optional<SizeMeasureTypeReference> getSpeciesDefaultSizeMeasureType(SpeciesReference species) {
        SizeMeasureTypeReference result = null;
        if (species != null && species.getSizeMeasureTypeId() != null) {
            String sizeMeasureId = species.getSizeMeasureTypeId();
            result = sizeMeasureTypeReferenceMap.get(sizeMeasureId);
            log.info("Use as default size measure type: " + result);
        } else {
            log.info("No default size measure type defined (species is null, or no default size measure defined on it.");
        }
        return Optional.ofNullable(result);
    }

    public Optional<WeightMeasureTypeReference> getSpeciesDefaultWeightMeasureType(SpeciesReference species) {
        WeightMeasureTypeReference result = null;
        if (species != null && species.getWeightMeasureTypeId() != null) {
            String weightMeasureId = species.getWeightMeasureTypeId();
            result = weightMeasureTypeReferenceMap.get(weightMeasureId);
            log.info("Use as default weight measure type: " + result);
        } else {
            log.info("No default weight measure type defined (species is null, or no default weight measure defined on it.");
        }
        return Optional.ofNullable(result);
    }

}
