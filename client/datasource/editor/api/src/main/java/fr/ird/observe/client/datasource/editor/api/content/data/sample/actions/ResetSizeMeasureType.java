package fr.ird.observe.client.datasource.editor.api.content.data.sample.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.ContentTableUIActionSupport;
import fr.ird.observe.dto.data.DataDto;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;

import javax.swing.JButton;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

public class ResetSizeMeasureType<U extends ContentTableUI<?, ?, U>> extends ContentTableUIActionSupport<U> {

    private final FilterableComboBox<?> combo;

    public static <U extends ContentTableUI<?, ?, U>> void install(U ui, JButton editor, FilterableComboBox<?> combo) {
        ResetSizeMeasureType<U> action = new ResetSizeMeasureType<>(combo);
        init(ui, editor, action);
    }

    public ResetSizeMeasureType(FilterableComboBox<?> combo) {
        super(null, t("observe.referential.common.SizeMeasureType.action.reset.to.default.tip"), "combobox-reset2", null);
        this.combo = combo;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        combo.setSelectedItem(null);
        ContentTableUIModel<?, ?> model = ui.getModel();
        DataDto tableEditBean = model.getStates().getTableEditBean();
        if (tableEditBean.get("species") != null) {
            combo.setSelectedItem(model.getStates().get("defaultSizeMeasureType"));
        }
    }
}
