package fr.ird.observe.client.datasource.editor.api.content.data.simple;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.actions.reset.ResetAction;
import fr.ird.observe.dto.data.SimpleDto;

/**
 * Created by tchemit on 26/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ContentSimpleUIHandler<D extends SimpleDto, U extends ContentSimpleUI<D, U>> extends ContentUIHandler<U> {

    protected abstract void installSaveAction();

    @Override
    public ContentSimpleUIModel<D> getModel() {
        return ui.getModel();
    }

    @Override
    protected ContentSimpleUIInitializer<U> createContentUIInitializer(U ui) {
        return new ContentSimpleUIInitializer<>(ui);
    }

    @Override
    protected ContentOpenWithValidator<U> createContentOpen(U ui) {
        ContentSimpleUIOpenExecutor<D, U> executor = new ContentSimpleUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor);
    }

    @Override
    public final ContentOpenWithValidator<U> getContentOpen() {
        return (ContentOpenWithValidator<U>) super.getContentOpen();
    }

    @Override
    public void initActions() {
        installResetAction();
        installSaveAction();
    }

    @Override
    public void installChangeModeAction() {
        ChangeMode.installAction(ui);
    }

    protected void installResetAction() {
        ResetAction.installAction(ui, ui.getReset());
    }

    @Override
    public void onPrepareValidationContext() {
        super.onPrepareValidationContext();
        ui.getValidator().setContext(getModel().getStates().getValidationContext());
    }

}
