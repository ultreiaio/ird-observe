package fr.ird.observe.client.datasource.editor.api.content.data.simple;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.ObserveServicesProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by tchemit on 26/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ContentSimpleUIModel<D extends SimpleDto> extends ContentSimpleUIModelSupport<D> {

    private static final Logger log = LogManager.getLogger(ContentSimpleUIModel.class);

    public ContentSimpleUIModel(NavigationNode source) {
        super(source);
    }

    public abstract Form<D> loadForm(ObserveServicesProvider servicesProvider, String id);

    @Override
    public ContentSimpleUINavigationNode getSource() {
        return (ContentSimpleUINavigationNode) super.getSource();
    }

    @Override
    public ContentSimpleUIModelStates<D> getStates() {
        return (ContentSimpleUIModelStates<D>) super.getStates();
    }

    @Override
    public final Form<D> openForm(ObserveServicesProvider servicesProvider, String selectedId) {
        open();
        log.info("{}Load Form from id: {}", getPrefix(), selectedId);
        Form<D> form = loadForm(servicesProvider, selectedId);
        getStates().openForm(form);
        return form;
    }
}
