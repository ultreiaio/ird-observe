package fr.ird.observe.client.datasource.editor.api.content.data.simple;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SavePredicate;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SavePredicateSupport;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.UsingLayout;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.stats.WithStatistics;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.decoration.Decorator;

import java.util.Objects;

/**
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentSimpleUIModelStatesSupport<D extends DataDto> extends ContentUIModelStates {
    public static final String PROPERTY_FORM = "form";
    public static final String PROPERTY_SELECTED_ID = "selectedId";
    /**
     * Reference cache.
     */
    private final ReferencesCache referenceCache;
    /**
     * Log prefix.
     */
    private final String prefix;
    /**
     * Label decorator.
     *
     * @see #getBeanLabel()
     */
    private final Decorator labelDecorator;
    /**
     * Incoming node.
     */
    private final NavigationNode source;
    /**
     * Edit bean used in form.
     */
    private D bean;
    /**
     * Data id.
     */
    private String selectedId;
    /**
     * Loaded form.
     */
    private Form<D> form;

    public ContentSimpleUIModelStatesSupport(ContentSimpleUIModelSupport<D> model, D bean, String selectedId) {
        source = Objects.requireNonNull(model).getSource();
        NavigationContext<?> context = source.getContext();
        this.referenceCache = context.newReferenceCache();
        this.bean = Objects.requireNonNull(bean);
        this.selectedId = selectedId;
        this.prefix = model.getPrefix();
        if (UsingLayout.class.isAssignableFrom(bean.getClass())
                || EditableDto.class.isAssignableFrom(bean.getClass())
                || OpenableDto.class.isAssignableFrom(bean.getClass())
                || RootOpenableDto.class.isAssignableFrom(bean.getClass())) {
            DecoratorService decoratorService = getDecoratorService();
            decoratorService.installDecorator(bean);
            this.labelDecorator = decoratorService.getToolkitIdLabelDecoratorByType(bean.getClass());
        } else {
            this.labelDecorator = null;
        }
    }

    public NavigationNode source() {
        return source;
    }

    public final ToolkitIdLabel getBeanLabel() {
        ToolkitIdLabel label = getBean().toLabel();
        label.registerDecorator(labelDecorator);
        return label;
    }

    @Override
    public void init(ContentUI ui, DefaultUIInitializerResult initializerResult) {
        super.init(ui, initializerResult);
        onAfterInitAddReferentialFilters(getClientConfig(), getObserveSelectModel(), ui.getHandler(), referenceCache);
        referenceCache.registerComponents(initializerResult);
    }

    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
    }

    protected void loadReferentialCacheOnOpenForm(Form<D> form) {
        getReferenceCache().loadReferentialReferenceSetsInModel(form);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    protected void copyFormToBean(Form<D> form) {
        form.getObject().copy(getBean());
        if (WithStatistics.class.isAssignableFrom(getBean().getClass())) {
            WithStatistics withStatistics = (WithStatistics) form.getObject();
            withStatistics.copyTo((WithStatistics) getBean());
        }
    }

    public D getBeanToSave() {
        D bean = getBean();
        bean.autoTrim();
        return bean;
    }

    public final String getSelectedId() {
        return selectedId;
    }

    public final void setSelectedId(String selectedId) {
        Object oldValue = getForm();
        this.selectedId = selectedId;
        firePropertyChange(PROPERTY_SELECTED_ID, oldValue, selectedId);
    }

    public final D getBean() {
        return bean;
    }

    public void setBean(D bean) {
        D oldValue = this.bean;
        this.bean = bean;
        firePropertyChange("bean", oldValue, bean);
    }

    public final Form<D> getForm() {
        return form;
    }

    public final void setForm(Form<D> form) {
        Object oldValue = getForm();
        this.form = form;
        firePropertyChange(PROPERTY_FORM, oldValue, form);
        //FIXME If not editing the form we should not use any filter, nor data in combobox, the incoming value is enough
        getReferenceCache().updateUiWithReferenceSetsFromModel(form);
    }

    public final void openForm(Form<D> form) {
        // install indexes on object (see DataDto.installIndex method)
        form.getObject().installIndex();
        loadReferentialCacheOnOpenForm(form);
        setForm(form);
        copyFormToBean(form);
    }

    @Override
    public final void close() {
        super.close();
        referenceCache.destroy();
        form = null;
    }

    public final ReferencesCache getReferenceCache() {
        return referenceCache;
    }

    public final String getPrefix() {
        return prefix;
    }

    public SavePredicate<D> savePredicate() {
        return new SavePredicateSupport<>(this) {
            @Override
            protected boolean testForValid(D originalBean, D editBean) {
                return true;
            }
        };
    }
}
