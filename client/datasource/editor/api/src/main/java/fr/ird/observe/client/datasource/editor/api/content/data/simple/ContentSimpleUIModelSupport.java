package fr.ird.observe.client.datasource.editor.api.content.data.simple;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveRequest;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.ObserveServicesProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by tchemit on 26/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ContentSimpleUIModelSupport<D extends DataDto> extends ContentUIModel {

    private static final Logger log = LogManager.getLogger(ContentSimpleUIModelSupport.class);

    public ContentSimpleUIModelSupport(NavigationNode source) {
        super(source);
    }

    public abstract Form<D> openForm(ObserveServicesProvider servicesProvider, String selectedId);

    @SuppressWarnings("unchecked")
    @Override
    public ContentSimpleUIModelStatesSupport<D> getStates() {
        return (ContentSimpleUIModelStatesSupport<D>) super.getStates();
    }

    @Override
    public boolean isCreateNewDataActionEnabled(Class<? extends DataDto> dtoType, Boolean updatingAndNotModified, String showDataPropertyName) {
        boolean result = updatingAndNotModified != null && updatingAndNotModified;
        if (result) {
            D bean = getStates().getBean();
            if (showDataPropertyName != null) {
                result = bean.get(showDataPropertyName);
                if (result) {
                    // got a matching bean property
                    log.debug(String.format("%sUse dto bean property %s to enable %s create action.", getPrefix(), showDataPropertyName, dtoType.getName()));
                }
            }
            return result;
        }
        return false;
    }

    public abstract SaveRequest<D> toSaveRequest();
}
