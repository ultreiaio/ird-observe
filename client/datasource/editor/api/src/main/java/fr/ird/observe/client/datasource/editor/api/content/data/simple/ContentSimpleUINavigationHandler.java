package fr.ird.observe.client.datasource.editor.api.content.data.simple;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationHandler;

import java.util.Objects;

/**
 * Created on 24/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentSimpleUINavigationHandler<N extends ContentSimpleUINavigationNode> extends NavigationHandler<N> {

    private final N node;

    public ContentSimpleUINavigationHandler(N node) {
        this.node = Objects.requireNonNull(node);
    }

    @Override
    public abstract ContentSimpleUINavigationContext getContext();

    @Override
    public N getNode() {
        return node;
    }

    @Override
    public final String getText() {
        return getNode().getScope().getI18nTranslation("type");
    }

    @Override
    public final ContentMode getContentMode() {
        ContentMode contentMode = super.getContentMode();
        return contentMode == null ? node.getParent().getHandler().getContentMode() : contentMode;
    }
}
