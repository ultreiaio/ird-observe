package fr.ird.observe.client.datasource.editor.api.content.data.simple;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.dto.reference.DtoReference;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 25/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentSimpleUINavigationInitializer extends NavigationInitializer<ContentSimpleUINavigationContext> {

    private final Supplier<? extends DtoReference> parentReference;

    public ContentSimpleUINavigationInitializer(NavigationScope scope, Supplier<? extends DtoReference> parentReference) {
        super(scope);
        this.parentReference = Objects.requireNonNull(parentReference);
    }

    @Override
    protected Object init(NavigationContext<ContentSimpleUINavigationContext> context) {
        return getScope().getMainType();
    }

    @Override
    protected void open(NavigationContext<ContentSimpleUINavigationContext> context) {
    }

    @Override
    protected void reload(NavigationContext<ContentSimpleUINavigationContext> context) {
    }

    @Override
    public String toString() {
        return super.toString() + "" + getScope().getMainType().getName();
    }

    @Override
    public String toPath() {
        return getScope().getNodeDataType().getName();
    }

    public DtoReference getParentReference() {
        return parentReference.get();
    }

    public String getSelectedId() {
        return getParentReference().getId();
    }
}
