package fr.ird.observe.client.datasource.editor.api.content.data.simple;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.NullCapability;
import fr.ird.observe.dto.reference.DtoReference;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 08/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentSimpleUINavigationNode extends NavigationNode {

    public static <N extends ContentSimpleUINavigationNode> N init(N node, Supplier<? extends DtoReference> parentReference) {
        ContentSimpleUINavigationInitializer initializer = new ContentSimpleUINavigationInitializer(Objects.requireNonNull(node).getScope(), parentReference);
        node.init(initializer);
        return node;
    }

    @Override
    public ContentSimpleUINavigationInitializer getInitializer() {
        return (ContentSimpleUINavigationInitializer) super.getInitializer();
    }

    @Override
    public ContentSimpleUINavigationContext getContext() {
        return (ContentSimpleUINavigationContext) super.getContext();
    }

    @Override
    public ContentSimpleUINavigationHandler<?> getHandler() {
        return (ContentSimpleUINavigationHandler<?>) super.getHandler();
    }

    @Override
    public NullCapability<?> getCapability() {
        return (NullCapability<?>) super.getCapability();
    }

    public DtoReference getParentReference() {
        return getInitializer().getParentReference();
    }
}
