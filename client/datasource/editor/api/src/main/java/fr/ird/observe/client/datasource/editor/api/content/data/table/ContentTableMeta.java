/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.data.table;

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.bean.definition.JavaBeanPropertyDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.function.Function;

/**
 * La définition d'une méta donnée d'un tableau, i.e la définition d'une
 * colonne.
 *
 * @param <D> le type de la donnée encapsulée par la colonne
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
//FIXME Review this and use JavaBean APi instead of introspection stuff.
public class ContentTableMeta<D extends JavaBean> {

    private static final Logger log = LogManager.getLogger(ContentTableMeta.class);

    /**
     * Type of enclosing class.
     */
    protected final Class<D> enclosingClass;
    /**
     * Property name (name of the column).
     */
    protected final String name;
    /**
     * To get property value from an enclosing object.
     */
    protected final transient Function<D, Object> readMethod;
    private final Class<?> type;

    public ContentTableMeta(Class<D> enclosingClass, String name) {
        this.name = name;
        this.enclosingClass = enclosingClass;
        JavaBeanDefinition definition = JavaBeanDefinitionStore.getDefinition(enclosingClass).orElseThrow();
        JavaBeanPropertyDefinition<D, Object> propertyDefinition = definition.readProperty(name);
        this.type = propertyDefinition.type();
        this.readMethod = propertyDefinition.getter();
        log.debug(String.format("Init table meta (dto type: %s, property name: %s, property type: %s", enclosingClass.getName(), name, getType().getName()));
    }

    /**
     * @return the name of this meta (will be used as columnName)
     */
    public String getName() {
        return name;
    }

    /**
     * @return the type of this meta (will be used as columnClass)
     */
    public Class<?> getType() {
        return type;
    }

    /**
     * @param bean the bean to request
     * @return the value of the property of the given bean
     */
    public Object getValue(D bean) {
        try {
            return readMethod.apply(bean);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Class<D> getEnclosingClass() {
        return enclosingClass;
    }
}
