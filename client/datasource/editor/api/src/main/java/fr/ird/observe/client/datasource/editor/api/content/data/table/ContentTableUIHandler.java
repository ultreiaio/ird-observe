/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.data.table;

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.actions.InsertMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewContentTableUIEntry;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowTechnicalInformations;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeProducer;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.actions.reset.ResetAction;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.FixData;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.datafile.DeleteDataFile;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.datafile.ExportDataFile;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.datafile.ImportDataFile;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.DeleteEntry;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.ResetEntry;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.SaveAndNewEntry;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.SaveTableEntry;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.select.SelectFirst;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.select.SelectLast;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.select.SelectNext;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.select.SelectPrevious;
import fr.ird.observe.client.datasource.editor.api.content.data.table.sortable.AutoSelectWithMoveUpAndDownShowPopupAction;
import fr.ird.observe.client.datasource.editor.api.focus.ContentZone;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.main.focus.FocusDispatcher;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTable;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Rectangle;
import java.util.Objects;
import java.util.function.Function;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public abstract class ContentTableUIHandler<D extends DataDto, C extends ContainerChildDto, U extends ContentTableUI<D, C, U>> extends ContentUIHandler<U> {
    public static final String CLIENT_PROPERTY_CREATE_ACTION = "CreateTableAction";

    private static final Logger log = LogManager.getLogger(ContentTableUIHandler.class);
    int oldIndex;
    private ChangeMode<U> changeMode;

    protected abstract void installSaveAction();

    protected void installMoveAction() {
        // move action is optional
    }

    @Override
    public ContentTableUIModel<D, C> getModel() {
        return ui.getModel();
    }

    protected ContentTableUITableModel<D, C, U> getTableModel() {
        return ui.getTableModel();
    }

    @Override
    protected ContentTableUIInitializer<D, C, U> createContentUIInitializer(U ui) {
        return new ContentTableUIInitializer<>(ui);
    }

    @Override
    protected ContentOpenWithValidator<U> createContentOpen(U ui) {
        ContentTableUIOpenExecutor<D, C, U> executor = new ContentTableUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor);
    }

    @Override
    public final ContentOpenWithValidator<U> getContentOpen() {
        return (ContentOpenWithValidator<U>) super.getContentOpen();
    }

    protected Component getFocusComponentOnSelectedRow(U ui, boolean notPersisted, boolean newRow, C tableEditBean, C previousRowBean) {
        return getFocusComponent(ui.getEditorPanel().getName());
    }

    @Override
    protected Container computeFocusOwnerContainer() {
        ContentTableUIModelStates<D, C> states = getModel().getStates();
        ContentMode mode = states.getMode();
        Container container;
        boolean showData = states.isShowData();
        if (mode == null || mode == ContentMode.READ || states.isEmpty() || !showData) {
            if (!showData || mode == null || mode == ContentMode.READ) {
                container = ui;
            } else {
                container = ui.getTitleRightToolBar();
            }
        } else {
            container = ui.getEditor();
        }
        return container;
    }

    protected void onTableModelChanged() {
        ui.getSelectToolbar().setVisible(getTableModel().getRowCount() > 1);
    }

    @Override
    public void onInit(U ui) {

        JXTable table = ui.getTable();
        ContentTableUITableModel<D, C, U> tableModel = ui.getTableModel();

        // init table
        tableModel.initTableUI(table);
        if (ui.getModel().getStates().isSortable()) {
            tableModel.setTableSortable(table);
        }

        // when model change in table, let's add a hook
        tableModel.addTableModelListener(e -> onTableModelChanged());

        // When selected row has changed, update selection model
        tableModel.addPropertyChangeListener(ContentTableUITableModel.SELECTED_ROW_PROPERTY, evt -> onSelectedItemChanged((Integer) evt.getNewValue()));
        tableModel.addPropertyChangeListener(ContentTableUITableModel.EMPTY_PROPERTY, e -> ui.getStates().fireEmptyPropertyChange());
        // We can't install those actions in initActions method, since we need the rowSorter installed on the table
        if (ui.getModel().getStates().isWithIndex()) {
            log.info("{}Install sortable popup actions", ui.getModel().getPrefix());
            new AutoSelectWithMoveUpAndDownShowPopupAction(ui);
        }
    }

    @Override
    public void initActions() {
        installCreateNewTableEntryAction();
        SaveTableEntry.installAction(ui);
        installResetAction();
        installSaveAction();
        FixData.installAction(ui);
        ResetEntry.init(ui, ui.getResetEntry(), new ResetEntry());
        SaveAndNewEntry.init(ui, ui.getSaveAndNewEntry(), new SaveAndNewEntry());
        DeleteEntry.installAction(ui);
        installMoveAction();
        ui.getConfigurePopup().addSeparator();
        ShowTechnicalInformations.installAction(ui);
        getComponent(JButton.class, "importData").ifPresent(e -> ImportDataFile.installAction(ui, e));
        getComponent(JButton.class, "deleteData").ifPresent(e -> DeleteDataFile.installAction(ui, e));
        getComponent(JButton.class, "exportData").ifPresent(e -> ExportDataFile.installAction(ui, e));

        SelectFirst.init(ui, ui.getSelectFirstTableEntry(), new SelectFirst());
        SelectPrevious.init(ui, ui.getSelectPreviousTableEntry(), new SelectPrevious());
        SelectNext.init(ui, ui.getSelectNextTableEntry(), new SelectNext());
        SelectLast.init(ui, ui.getSelectLastTableEntry(), new SelectLast());
    }

    @Override
    public void installChangeModeAction() {
        ChangeModeRequest request = ChangeModeRequest.of(ui);
        ChangeModeExecutor<U> executor = new ChangeModeExecutor<>();
        ChangeModeProducer<U> changeModeProducer = new ChangeModeProducer<>(ui, request,
                                                                            ContentTableUIModelStates.PROPERTY_SHOW_DATA,
                                                                            ContentTableUIModelStates.PROPERTY_EMPTY) {
            @Override
            protected ContentMode rebuildChangeMode(ContentMode newValue) {
                ContentTableUIModelStates<D, C> source = ui.getStates();
                boolean showData = source.isShowData();
                if (!showData) {
                    newValue = null;
                }
                return newValue;
            }
        };
        changeMode = new ChangeMode<>(request, changeModeProducer, executor) {
            protected boolean rebuildZonePredicate(U ui) {
                return request.isEditable() && (!ui.getStates().isEmpty() || ui.getStates().isModified());
            }

            @Override
            public void rebuildEditableZone(U ui) {
                JComponent body = ui.getContentBody();
                body.remove(ui.getShowForm());
                body.remove(ui.getHideForm());
                body.remove(ui.getEmptyForm());
                body.add(ui.getShowForm(), BorderLayout.CENTER);
                if (ui.getStates().isReadingMode()) {
                    ui.getActions().setVisible(false);
                    super.rebuildNotEditableZone(ui);
                } else {
                    ui.getActions().setVisible(true);
                    super.rebuildEditableZone(ui);
                }
            }

            @Override
            protected void rebuildNotEditableZone(U ui) {
                boolean showData = ui.getStates().isShowData();
                boolean empty = ui.getStates().isEmpty();
                JComponent body = ui.getContentBody();
                body.remove(ui.getShowForm());
                body.remove(ui.getHideForm());
                body.remove(ui.getEmptyForm());
                if (!showData) {
                    body.add(ui.getHideForm(), BorderLayout.CENTER);
                } else if (empty) {
                    body.add(ui.getEmptyForm(), BorderLayout.CENTER);
                }
                ui.getActions().setVisible(false);
                super.rebuildNotEditableZone(ui);
            }
        };
        ChangeMode.installAction(ui, changeMode);
    }

    protected void installResetAction() {
        ResetAction.installAction(ui, ui.getReset());
    }

    public ChangeMode<U> getChangeMode() {
        return changeMode;
    }

    protected void reselectRow() {
        ContentTableUITableModel<D, C, U> tableModel = getTableModel();
        if (!tableModel.isEmpty()) {
            int selectedRow = oldIndex;
            if (selectedRow == -1) {
                selectedRow = 0;
            }
            if (selectedRow >= tableModel.getRowCount()) {
                selectedRow = tableModel.getRowCount() - 1;
            }
            log.info(String.format("Select row: %d", selectedRow));
            tableModel.changeSelectedRow(-1);
            tableModel.changeSelectedRow(selectedRow);
        }
    }

    public void removeSelectedRow(int selectedRow) {
        getTableModel().doRemoveRow(selectedRow, false);
    }

    final void updateEditor(ListSelectionEvent event) {

        if (ui.getValidatorTable() == null) {
            log.debug(String.format("%s skip validator is null!", prefix));
            return;
        }
        if (getModel().getStates().getBean() == null) {
            log.debug(String.format("%s skip bean is null!", prefix));
            return;
        }
        log.debug(event);
        if (getTableModel().isEmpty()) {
            // le modèle est vide, on ne propage rien
            //FIXME le modèle de selection ne devrait plus générer des évènements des que le modèle est vide ?
            log.debug(String.format("%s empty model, do nothing", prefix));
            return;
        }

        ListSelectionModel sModel = (ListSelectionModel) event.getSource();
        int newIndex = sModel.getAnchorSelectionIndex();

        if (newIndex == -1 || sModel.isSelectionEmpty()) {
            //FIXME le modèle de selection ne devrait plus générer des évènements des que le modèle est vide ?
            log.debug(String.format("%s skip with row == -1, or empty selection, do nothing", prefix));
            return;
        }

        SwingUtilities.invokeLater(() -> {
            if (!ui.getSelectionModel().isSelectionEmpty()) {
                // on veut toujours que la ligne sélectionnée soit visible
                Rectangle rect = ui.getTable().getCellRect(ui.getSelectionModel().getAnchorSelectionIndex(), 0, false);
                ui.getTable().scrollRectToVisible(rect);
            }
        });

        int selectedRow = getTableModel().getSelectedRow();
        if (newIndex == selectedRow) {
            // on bloque du code re-entrant
            log.debug(String.format("%s new index already set in model %d, do nothing", prefix, newIndex));
            return;
        }
        // on doit changer de ligne sélectionne dans le modèle
        getTableModel().changeSelectedRow(newIndex);
    }

    protected void installCreateNewTableEntryAction() {
        Class<DataDto> dtoType;
        ContentTableUIModel<D, C> model = ui.getModel();
        boolean standalone = model.getStates().isStandalone();
        Function<NavigationNode, NavigationNode> nodeFunction;
        NavigationScope scope = model.getSource().getScope();
        if (standalone) {
            dtoType = scope.getMainType();
            nodeFunction = n -> n;
        } else {
            dtoType = scope.getChildType();
            nodeFunction = n -> {
                Integer tabIndex = (Integer) ui.getClientProperty(ContentUIHandler.CLIENT_PROPERTY_TAB_INDEX);
                if (tabIndex != null) {
                    log.info(String.format("%sSelect tab: %d, before create action.", prefix, tabIndex));
                    getContentOpen().selectParentTab(tabIndex);
                }
                return n;
            };
        }
        Class<DataDto> childDtoType = scope.getChildType();
        CreateNewContentTableUIEntry<DataDto, U> action = new CreateNewContentTableUIEntry<>(dtoType, childDtoType, nodeFunction);
        JMenuItem editor = CreateNewContentTableUIEntry.createEditor(ui, dtoType, action);
        ui.getTableModel().addPropertyChangeListener(evt -> {
            String propertyName = evt.getPropertyName();
            if (propertyName.equals(ContentTableUITableModel.EDITABLE_PROPERTY) || propertyName.equals(ContentTableUITableModel.CREATE_PROPERTY)) {
                ContentTableUITableModel<?, ?, ?> tableModel = (ContentTableUITableModel<?, ?, ?>) evt.getSource();
                boolean enabled = tableModel.isEditable() && !tableModel.isCreate();
                editor.setEnabled(enabled);
            }
        });
        String showDataPropertyName = scope.getShowDataPropertyName();
        model.getStates().addPropertyChangeListener(InsertMenuAction.PROPERTY_NAME_UPDATING_MODE_AND_NOT_MODIFIED, evt -> editor.setEnabled(model.isCreateNewDataActionEnabled(dtoType, (Boolean) evt.getNewValue(), showDataPropertyName)));
        ui.putClientProperty(CLIENT_PROPERTY_CREATE_ACTION, editor);
    }

    private final FocusDispatcher focusDispatcher = new FocusDispatcher();

    protected void onSelectedItemChanged(Integer selectedRow) {
        blockFocus();
        Component focusComponent = null;
        try {
            C previousTableEditBean = ui.getTableModel().getTableEditBean();
            //FIXME Check what it was doing, but no more now...
//                        getModel().setRowSaved(!create);
            if (selectedRow == -1) {
                log.info(prefix + ">>>>>>>>>> will clear selection");
                // on supprime la selection
                ui.getSelectionModel().clearSelection();
                log.debug(prefix + "<<<<<<<<<< has clear selection...");
            } else {
                // on met a jour le modele de selection
                log.debug(String.format("%s>>>>>>>>>> will set selection to %d", prefix, selectedRow));
                ui.getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
            }
            if (!ui.getModel().getStates().isReadingMode() && selectedRow != -1) {
                C tableEditBean = ui.getTableModel().getTableEditBean();
                focusComponent = ui.getHandler().getFocusComponentOnSelectedRow(ui, tableEditBean.isNotPersisted(), ui.getTableModel().isCreate(), tableEditBean, previousTableEditBean);
                if (focusComponent != null) {
                    log.info(String.format("%sNew entry form focus owner: %s", prefix, focusComponent));
                    if (focusComponent instanceof JComponent) {
                        focusComponent = focusDispatcher.dispatchFocus((JComponent) focusComponent);
                    }
//                    SwingUtilities.invokeLater(focusComponent::requestFocusInWindow);
                }
            }

        } finally {
            Component finalFocusComponent = focusComponent;
            SwingUtilities.invokeLater(() -> {
                if (getFocusModel() != null) {
                    unblockFocus();
                    //FIXME Review this on Focus clean session (the blockFocus does not work!!!)
                    if (Objects.equals(getFocusModel().getFocusOwnerZone(), ContentZone.ZONE_NAME)) {
                        ui.getHandler().grabFocusOnForm();
                        if (finalFocusComponent != null) {
                            SwingUtilities.invokeLater(finalFocusComponent::requestFocusInWindow);
                        }
                    }
                }
            });
        }
    }
}
