package fr.ird.observe.client.datasource.editor.api.content.data.table;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.NotStandaloneContentUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import io.ultreia.java4all.i18n.I18n;
import org.nuiton.jaxx.widgets.text.BigTextEditor;

import javax.swing.JComponent;
import java.awt.Dimension;

/**
 * To initialize ui.
 * <p>
 * Created on 9/26/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class ContentTableUIInitializer<D extends DataDto, C extends ContainerChildDto, U extends ContentTableUI<D, C, U>> extends ContentUIInitializer<U> {

    public ContentTableUIInitializer(U ui) {
        super(ui);
    }

    protected ContentTableUIModel<D, C> getModel() {
        return ui.getModel();
    }

    protected ContentTableUIHandler<D, C, U> getHandler() {
        return ui.getHandler();
    }

    @Override
    public DefaultUIInitializerResult initUI() {
        DefaultUIInitializerResult initializerResult = super.initUI();

        ContentTableUITableModel<?, ?, ?> tableModel = ui.getTableModel();
        ui.setContextValue(tableModel);

        if (ui.getContentBody() != null) {
            // on supprime le layer de blocage en mode disable
            UIHelper.setLayerUI(ui.getContentBody(), null);
        }

        if (ui.getExtraZone() != null) {
            // et on le remet sur les zones non clicables de l'ui
            UIHelper.setLayerUI(ui.getExtraZone(), ui.getBlockLayerUI());
        }
        // on ajoute un layer pour bloquer l'édition des entrées si nécessaire et redispatcher le focus
        UIHelper.setLayerUI(ui.getEditor(), ui.getEditorBlockLayerUI());

        ui.getTable().getTableHeader().setReorderingAllowed(false);
        ui.getTable().setRowSorter(null);
        ui.getSelectFirstTableEntry().setEnabled(false);
        ui.getSelectPreviousTableEntry().setEnabled(false);
        ui.getSelectNextTableEntry().setEnabled(false);
        ui.getSelectLastTableEntry().setEnabled(false);

        ui.getTitleRightToolBar().add(ui.getSelectToolbar(), 0);

        String message = I18n.t("observe.Common.list.message.none", I18nDecoratorHelper.getType(getModel().getScope().getChildType()));
        ui.getEmptyFormInformation().setText(message);
        return initializerResult;
    }

    @Override
    public JComponent getActionContainer() {
        if (!ui.getModel().getStates().isStandalone()) {
            return (JComponent) ((NotStandaloneContentUI<?>) ui).getParentUI();
        }
        return super.getActionContainer();
    }

    @Override
    protected void init(BigTextEditor editor) {
        super.init(editor);
        editor.setMinimumSize(new Dimension(50, 3 * 24));
        editor.getTextEditor().setRows(2);
    }
}
