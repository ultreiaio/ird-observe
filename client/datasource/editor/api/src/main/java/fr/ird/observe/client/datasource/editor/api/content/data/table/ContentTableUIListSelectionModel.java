package fr.ird.observe.client.datasource.editor.api.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.DefaultListSelectionModel;

/**
 * Le modèle de sélection pour un tableau.
 * <p>
 * Le modèle permet le blocage du changement de ligne sélectionnée dans le
 * tableau lorsque les conditions suivantes sont remplies :
 * <p>
 * - le tableau est editable - une entrée était précédemment sélectionné -
 * l'entrée en cours d'édition est modifié
 * <p>
 * Si on ne peut pas explicitement changer de ligne d'édition, on demande alors
 * à l'utilisateur s'il veut sauver l'entrée en cours d'édition (si elle est
 * valide), ou bien la perte des données (si elle n'est pas valide).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ContentTableUIListSelectionModel extends DefaultListSelectionModel {

    private static final Logger log = LogManager.getLogger(ContentTableUIListSelectionModel.class);
    private static final long serialVersionUID = 1L;

    private final ContentTableUITableModel<?, ?, ?> model;

    public ContentTableUIListSelectionModel(ContentTableUITableModel<?, ?, ?> model) {
        this.model = model;
    }

    @Override
    public void setSelectionInterval(int index0, int index1) {
        log.trace(index0 + " - " + index1);

        boolean canContinue = canContinue(model, index0);

        log.debug("can continue  for row " + index0 + " : " + canContinue);

        if (canContinue) {
            // on peut aller sur la nouvelle ligne demandée
            super.setSelectionInterval(index0, index1);
        }
    }

    protected boolean canContinue(ContentTableUITableModel<?, ?, ?> model, int newIndex) {
        boolean canContinue;
        if (model == null) {
            // le model n'est pas encore disponible
            return false;
        }

        if (!model.isEditable()) {
            // le modèle n'est pas editable
            // donc aucun controle a effectuer
            // on peut change de selection
            return true;
        }

        // le modèle est editable

        int editingRow = model.getSelectedRow();

        if (isSelectionEmpty() || editingRow == -1) {
            // pas de précédente selection
            // ou pas de ligne en cours d'édition
            // on peut donc sélectionner cette nouvelle ligne
            return true;
        }

        // le modèle a une ligne en cours d'édition

        if (editingRow == newIndex) {
            // on veut aller sur la ligne en cours d'édition
            // aucune verification a faire
            // ce cas peut arriver lors d'un ajustement de ligne
            return true;
        }

        // une ligne en cours d'édition est différente de celle qu'on
        // veut attendre on demande au modèle si on peut quitter cette ligne
        canContinue = model.isCanQuitEditingRow();

        return canContinue;
    }

}
