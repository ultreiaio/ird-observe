/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.data.table;

import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowIdRequest;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUIModelSupport;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.ObserveServicesProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Le modèle d'un écran d'édition avec tableau.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public abstract class ContentTableUIModel<D extends DataDto, C extends ContainerChildDto> extends ContentSimpleUIModelSupport<D> {

    private static final Logger log = LogManager.getLogger(ContentTableUIModel.class);

    public ContentTableUIModel(NavigationNode source) {
        super(source);
    }

    public abstract Form<D> loadForm(ObserveServicesProvider servicesProvider, String selectedId);

    public abstract C newTableEditBean();

    @Override
    public ContentTableUINavigationNode getSource() {
        return (ContentTableUINavigationNode) super.getSource();
    }

    @Override
    public ContentTableUIModelStates<D, C> getStates() {
        return (ContentTableUIModelStates<D, C>) super.getStates();
    }

    public final Class<C> getChildType() {
        return getScope().getChildType();
    }

    @Override
    public final Form<D> openForm(ObserveServicesProvider servicesProvider, String selectedId) {
        if (!getStates().isStandalone()) {
            // Do not load form for standalone form
            return null;
        }
        open();
        log.info(String.format("Load Form from id: %s", selectedId));
        Form<D> form = loadForm(servicesProvider, selectedId);
        getStates().openForm(form);
        return form;
    }

    public final ShowIdRequest getShowIdRequest() {
        return new ShowIdRequest(() -> getStates().getTableEditBean());
    }

}
