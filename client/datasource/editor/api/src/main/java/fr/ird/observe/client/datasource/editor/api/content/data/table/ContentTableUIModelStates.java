package fr.ird.observe.client.datasource.editor.api.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SavePredicate;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUIModelStatesSupport;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.WithIndex;
import fr.ird.observe.dto.form.Form;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentTableUIModelStates<D extends DataDto, C extends ContainerChildDto> extends ContentSimpleUIModelStatesSupport<D> {

    public static final String PROPERTY_CAN_SAVE_ROW = "canSaveRow";
    public static final String PROPERTY_CAN_RESET_ROW = "canResetRow";
    public static final String PROPERTY_EMPTY = "empty";
    /**
     * Table edit bean.
     */
    private final C tableEditBean;
    /**
     * Is can save row?
     */
    private boolean canSaveRow;
    /**
     * Can reset row?
     */
    private boolean canResetRow;
    /**
     * When the ui is called from another one (this means we do not load bean from the handler)
     */
    private boolean standalone = true;
    /**
     * Is the child type is {@link WithIndex}
     */
    private boolean withIndex = false;
    /**
     * Table model.
     */
    private ContentTableUITableModel<D, C, ?> tableModel;

    public ContentTableUIModelStates(ContentTableUIModel<D, C> model, D bean, C tableEditBean, String selectedId, boolean standalone) {
        super(model, bean, selectedId);
        this.tableEditBean = Objects.requireNonNull(tableEditBean);
        model.getDecoratorService().installDecorator(tableEditBean);
        setStandalone(standalone);
    }

    @Override
    public ContentTableUINavigationNode source() {
        return (ContentTableUINavigationNode) super.source();
    }

    @Override
    public void open(ContentUIModel model) {
        super.open(model);
        @SuppressWarnings("unchecked") ContentTableUIModel<D, C> realModel = (ContentTableUIModel<D, C>) model;
        ContentTableUINavigationNode source = realModel.getSource();
        // Compute showData property
        boolean showData = source.getInitializer().isShowData();
        setShowData(showData);
    }

    @Override
    public boolean isGlobalModified() {
        return getTableModel() != null && (super.isGlobalModified() || getTableModel().isModified() || isCanSaveRow());
    }

    public boolean isEmpty() {
        return getTableModel() == null || getTableModel().isEmpty();
    }

    public boolean isStandalone() {
        return standalone;
    }

    public void setStandalone(boolean standalone) {
        this.standalone = standalone;
    }

    public C getTableEditBean() {
        return tableEditBean;
    }

    public boolean isWithIndex() {
        return withIndex;
    }

    public void setWithIndex(boolean withIndex) {
        this.withIndex = withIndex;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void init(ContentUI ui, DefaultUIInitializerResult initializerResult) {
        super.init(ui, initializerResult);
        setTableModel(((ContentTableUI<D, C, ?>) ui).getTableModel());
    }

    @Override
    public Boolean canQuit() {
        if (isReadingMode()) {
            return true;
        }
        if (!getTableModel().isCanQuitEditingRow()) {
            return null;
        }
        return super.canQuit();
    }

    public boolean isCanSaveRow() {
        return canSaveRow;
    }

    public void setCanSaveRow(boolean canSaveRow) {
        boolean oldValue = this.canSaveRow;
        this.canSaveRow = canSaveRow;
        firePropertyChange(PROPERTY_CAN_SAVE_ROW, oldValue, canSaveRow);
    }

    public boolean isCanResetRow() {
        return canResetRow;
    }

    public void setCanResetRow(boolean canResetRow) {
        this.canResetRow = canResetRow;
        firePropertyChange(PROPERTY_CAN_RESET_ROW, canResetRow);
    }

    public ContentTableUITableModel<D, C, ?> getTableModel() {
        return tableModel;
    }

    void setTableModel(ContentTableUITableModel<D, C, ?> tableModel) {
        this.tableModel = Objects.requireNonNull(tableModel);
    }

    public boolean isFormEmpty() {
        Form<D> form = getForm();
        if (form == null) {
            return true;
        }
        Collection<?> originalCollection = form.getObject().get(getTableModel().getChildrenPropertyName());
        return originalCollection == null || originalCollection.isEmpty();
    }

    public void fireEmptyPropertyChange() {
        firePropertyChange(PROPERTY_EMPTY, isEmpty());
    }

    public void initDefault(C newTableBean) {
        // by default nothing to set
    }

    @SuppressWarnings("unchecked")
    @Override
    public D getBeanToSave() {
        D bean = getBean();
        if (isWithIndex()) {
            @SuppressWarnings("rawtypes") List data = getTableModel().getData();
            bean.set(getTableModel().getChildrenPropertyName(), WithIndex.reorder(data));
        }
        bean.autoTrim();
        return bean;
    }

    @Override
    protected void copyFormToBean(Form<D> form) {
        getTableModel().clearInlineModels();
        super.copyFormToBean(form);
    }

    @Override
    public SavePredicate<D> savePredicate() {
        return new TableSavePredicate<>(this);
    }

    public void prepareTableEditBeanBeforeSave() {
        getTableEditBean().autoTrim();
    }

    public final boolean isSortable() {
        return true;
    }
}
