package fr.ird.observe.client.datasource.editor.api.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationHandler;

import java.awt.Color;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 24/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentTableUINavigationHandler<N extends ContentTableUINavigationNode> extends NavigationHandler<N> {

    private final N node;

    public ContentTableUINavigationHandler(N node) {
        this.node = Objects.requireNonNull(node);
    }

    @Override
    public abstract ContentTableUINavigationContext getContext();

    @Override
    protected Color getColor0(Supplier<Color> defaultValue) {
        if (!getNode().getInitializer().isShowData()) {
            return getDisabledColor();
        }
        if (getNode().getInitializer().isEmpty()) {
            return getEmptyColor();
        }
        return super.getColor0(defaultValue);
    }

    @Override
    public N getNode() {
        return node;
    }

    @Override
    public String getText() {
        String result = getNode().getScope().getI18nTranslation("title");
        Long count = getNode().getInitializer().getCount();
        if (count != null) {
            result += String.format(" (%d)", count);
        }
        return result;
    }

    @Override
    public final ContentMode getContentMode() {
        ContentMode contentMode = super.getContentMode();
        if (contentMode != null) {
            return contentMode;
        }
        boolean showData = getNode().getInitializer().isShowData();
        if (!showData) {
            return ContentMode.READ;
        }
        return node.getParent().getHandler().getContentMode();
    }

}
