package fr.ird.observe.client.datasource.editor.api.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentEditExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.form.Form;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JSplitPane;
import java.awt.Dimension;

/**
 * Created on 27/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentTableUIOpenExecutor<D extends DataDto, C extends ContainerChildDto, U extends ContentTableUI<D, C, U>> implements ContentOpenExecutor<U>, ContentEditExecutor<U> {
    private static final Logger log = LogManager.getLogger(ContentTableUIOpenExecutor.class);

    @Override
    public void loadContent(U ui) {

        ContentTableUIHandler<D, C, U> handler = ui.getHandler();
        ContentOpenWithValidator<U> contentOpen = handler.getContentOpen();
        contentOpen.resetCoordinateEditors();
        //FIXME chemit 20100913 : il vaudrait le faire uniquement lors de l'édition
        // chaque arrive sur un écran invalide le cache de validation
        if (!(this instanceof NotStandaloneContentTableUIOpenExecutor<?, ?, ?>)) {
            getClientValidationContext().reset();
            ContentUIHandler.removeAllMessages(ui);
        }


        ContentTableUIModel<D, C> model = ui.getModel();
        String prefix = model.getSource().getInitializer().getLogPrefix();

        handler.oldIndex = ui.getTableModel().getSelectedRow();
        log.info(String.format("%s Selected index on open: %d", prefix, handler.oldIndex));

        openModel(ui);

    }

    @Override
    public void onOpened(U ui) {
        ContentTableUIHandler<D, C, U> handler = ui.getHandler();
        fixFormSize(ui);
        handler.onEndOpenUI();
    }

    public void openModel(U ui) {
        ContentTableUIModel<D, C> model = ui.getModel();
        ContentTableUIModelStates<D, C> states = model.getStates();
        ContentTableUIHandler<D, C, U> handler = ui.getHandler();
        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();
        ContentTableUITableModel<D, C, U> tableModel = ui.getTableModel();
        tableModel.detachModel();

        String prefix = model.getSource().getInitializer().getLogPrefix();
        model.open();
        String id = states.getSelectedId();
        if (id == null) {
            throw new IllegalStateException(String.format("%s Could not find id form %s", prefix, this));
        }

        // preparation du bean d'édition
        Form<D> form = model.openForm(handler, id);
        contentOpen.onOpenForm(form);

        // initialisation du modèle du tableau
        tableModel.attachModel();

        boolean canEdit = states.isUpdatingMode();
        if (!canEdit) {
            tableModel.setEditable(false);
        }

        handler.onOpenAfterOpenModel();
    }

    @Override
    public void startEdit(U ui) {
        ContentTableUIModelStates<D, C> states = ui.getModel().getStates();
        ContentTableUIHandler<D, C, U> handler = ui.getHandler();
        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();

        //FIXME:Focus Why?
//        model.fireFocusOnTable();
        contentOpen.prepareValidationContext(!(this instanceof NotStandaloneContentTableUIOpenExecutor<?, ?, ?>));
        contentOpen.installValidators(states.getBean());
        handler.reselectRow();
        states.setModified(false);
    }

    @Override
    public void stopEdit(U ui) {
        ClientValidationContext context = getClientValidationContext();
        ContentTableUIModelStates<D, C> states = ui.getModel().getStates();
        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();

        context.reset();
        ui.setValidatorChanged(false);

        // mark ui as not editing
        states.setEditing(false);

        // mark ui as valid while not editing
        states.setValid(true);

        // mark ui as not modified
        states.setModified(false);

        // detach all validators
        contentOpen.uninstallValidators();
    }

    public void fixFormSize(U ui) {

        ContentTableUIModelStates<D, C> states = ui.getModel().getStates();
        if (states.isShowData()) {
            // we want to see the hole form on screen
            ui.setMinimumSize(ui.getPreferredSize());
        } else {
            // we want to see the hole form on screen
            Dimension preferredSize = ui.getPreferredSize();
            JSplitPane parentContainer = ui.getParentContainer(JSplitPane.class);
            if (parentContainer != null) {
                ui.setMinimumSize(new Dimension(preferredSize.width, parentContainer.getPreferredSize().height - 200));
            }
        }
    }

    @Override
    public void doRead(U ui) {
        // select first row
        ui.getHandler().reselectRow();
    }
}
