/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.data.table;

import fr.ird.observe.client.datasource.editor.api.content.data.edit.actions.DeleteEdit;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.SaveTableEntry;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableTableModelWithCache;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.WithIndex;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.DecoratedSorter;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.sort.TableSortController;
import org.nuiton.jaxx.validator.swing.SwingValidator;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SortOrder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Le modèle d'un tableau où les données sont une association sur une entité.
 * <p>
 * Ce modèle n'est pas éditable.
 * <p>
 * Les données sont stockées dans la liste {@link #data} qui sert de cache, car
 * on veut pouvoir valider en temps réel l'entité principale (celle qui contient
 * l'association), il faut donc toujours que les données de l'association soient
 * synchronisées. L'utilisation d'un cache est cependant requise car sinon cela
 * est trop couteux (notamment pour le rendu du tableau...).
 * <p>
 * Le cache sera recalculé à chaque fois que l'on modifie la structure des
 * données de l'association (ajout d'une entrée, suppression d'une entrée).
 * <p>
 * De plus le cache permet de travailler sur une liste (alors que l'association
 * n'est peut-être pas ordonnée) et cela facilite les opérations sur les données
 * du tableau).
 * <p>
 * Le modèle définit plusieurs propriétés : <ul> <li>{@link #editable} : un
 * drapeau pour savoir si le modèle est editable</li> <li>{@link #modified} : un
 * drapeau pour savoir si le modèle est modifié</li> <li>{@link #create} : un
 * drapeau pour savoir si l'entrée en cours d'édition est une nouvelle
 * entrée</li> <li>{@link #selectedRow} : l'index de l'entrée sélectionnée</li>
 * </ul> FIXME a finir...
 *
 * @param <D> le type de l'entité qui contient la liste
 * @param <C> le type de l'entité d'une entrée de la liste
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class ContentTableUITableModel<D extends DataDto, C extends ContainerChildDto, U extends ContentTableUI<D, C, U>> extends AbstractTableModel {

    /**
     * Le nom de la propriété pour indiquer que l'entrée en cours d'édition est en mode création.
     */
    public static final String CREATE_PROPERTY = "create";
    /**
     * Le nom de la propriété de la ligne en cours d'édition
     */
    public static final String SELECTED_ROW_PROPERTY = "selectedRow";
    /**
     * Le nom de la propriété pour éditer le modèle
     */
    public static final String EDITABLE_PROPERTY = "editable";
    /**
     * Le nom de la propriété pour savoir si le modèle est vide
     */
    public static final String EMPTY_PROPERTY = "empty";
    /**
     * Le nom de la propriété modifié du modèle
     */
    private static final String MODIFIED_PROPERTY = "modified";
    private static final long serialVersionUID = 1L;

    /**
     * Logger
     */
    private static final Logger log = LogManager.getLogger(ContentTableUITableModel.class);

    /**
     * la liste des métas du modèle
     */
    protected final List<ContentTableMeta<C>> metas;

    /**
     * pour la propagation des modifications d'états
     */
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    /**
     * Property name to get/set children list in parent dto.
     */
    private final String childrenPropertyName;
    private final ContentTableUIListSelectionModel selectionModel;
    private final U context;
    private final Map<EditableTableModelWithCache<?>, EditableTable<?, ?>> inlineModels;
    /**
     * la liste des données du modèle
     */
    protected List<C> data = new ArrayList<>();
    /**
     * un drapeau pour savoir si le modèle a été modifié
     */
    protected boolean modified;
    /**
     * un drapeau pour savoir si le modèle est éditable
     */
    protected boolean editable;
    /**
     * un drapeau pour savoir si on édite une nouvelle entree
     */
    protected boolean create;
    /**
     * l'entrée sélectionnée (-1 quand pas de sélection)
     */
    protected int selectedRow = -1;
    /**
     * un drapeau pour savoir si le modèle a ete initialisé.
     */
    private boolean init;
    private boolean adjusting;

    public static <D extends IdDto & JavaBean> ContentTableMeta<D> newTableMeta(Class<D> childType, String property) {
        return new ContentTableMeta<>(childType, property);
    }

    protected ContentTableUITableModel(U context, ContentTableUIModel<D, C> model) {
        this.context = Objects.requireNonNull(context);
        this.childrenPropertyName = model.getScope().getChildrenPropertyName();
        this.metas = Collections.unmodifiableList(createMetas());
        this.selectionModel = new ContentTableUIListSelectionModel(this);
        this.inlineModels = new LinkedHashMap<>();
    }

    public void registerInlineModel(EditableTableModelWithCache<?> inlineModel, EditableTable<?, ?> table) {
        inlineModels.put(inlineModel, table);
    }

    public Map<EditableTableModelWithCache<?>, EditableTable<?, ?>> inlineModels() {
        return inlineModels;
    }

    protected abstract List<ContentTableMeta<C>> createMetas();

    public abstract void initTableUI(JXTable table);

    protected void setTableSortable(JXTable table) {
        TableSortController<TableModel> sorter = new TableSortController<>(this);
        sorter.setSortable(true);
        sorter.setSortOrder(0, SortOrder.ASCENDING);
        sorter.setMaxSortKeys(1);
        table.setRowSorter(sorter);
        if (getColumnMeta(0).getType().equals(int.class)) {
            sorter.setComparator(0, Comparator.comparingInt(v -> (int) v));
        }
        int index = -1;
        for (ContentTableMeta<C> meta : metas) {
            index++;
            Class<?> type = meta.getType();
            if (Decorated.class.isAssignableFrom(type)) {
                DecoratorService decoratorService = getContext().getModel().getDecoratorService();
                Decorator decorator = decoratorService.getDecoratorByType(type);
                int decoratorIndex = decorator.configuration().getIndex();
                DecoratedSorter<?> decoratedSorter = decorator.definition().sorter();
                Comparator<?> comparator = decoratedSorter.getComparator((DecoratorDefinition) decorator.definition(), decoratorService.getReferentialLocale().getLocale(), decoratorIndex);
                sorter.setComparator(index, comparator);
            } else if (boolean.class.equals(type)) {
                sorter.setComparator(index, Comparator.comparingInt(v -> (boolean) v ? 1 : 0));
            } else if (Boolean.class.equals(type)) {
                sorter.setComparator(index, Comparator.nullsLast(Comparator.comparingInt(v -> (Boolean) v ? 1 : 0)));
            } else if (Enum.class.isAssignableFrom(type)) {
                sorter.setComparator(index, Comparator.comparing(Object::toString));
            }
        }

        table.setSortable(true);
    }

    protected ContentTableUIModel<D, C> getModel() {
        return getHandler().getModel();
    }

    public void initTableUISize(JTable table) {
    }

    protected void onAfterLoadRowBeanToEdit(int editingRow, boolean newRow) {
        String prefix = getModel().getPrefix();
        int modelRow = getModelRow(editingRow);
        inlineModels.keySet().forEach(inlineModel -> inlineModel.onAfterLoadRowBeanToEdit(prefix, modelRow, newRow));
    }

    protected void onBeforeUpdateRowFromEditBean(int editingRow) {
        boolean create = isCreate();
        int modelRow = getModelRow(editingRow);
        inlineModels.keySet().forEach(inlineModel -> inlineModel.onBeforeUpdateRowFromEditBean(modelRow, create));
    }

    protected void onBeforeAddNewEntry(int row) {
        int modelRow = getModelRow(row);
        inlineModels.keySet().forEach(inlineModel -> inlineModel.storeInCacheForRowIfSelected(modelRow));
    }

    protected void onBeforeResetRow(int row) {
        String prefix = getModel().getPrefix();
        int modelRow = getModelRow(row);
        inlineModels.forEach((inlineModel, table) -> inlineModel.onBeforeResetRow(prefix, modelRow, table));
    }

    protected void onAfterResetRow(int row) {
        // do nothing by default
    }

    protected void onRemovedRow(int modelRow) {
        inlineModels.keySet().forEach(inlineModel -> inlineModel.removeRow(modelRow));
    }

    /**
     * When the selected row changed.
     *
     * @param ui              incoming table ui
     * @param editingRow      new editing row
     * @param tableEditBean   table  edit bean
     * @param previousRowBean previous row bean
     * @param notPersisted    {@code true} if in notPersisted mode, {@code false} otherwise
     * @param newRow          {code true} if this is a new row (create mode), {@code false} otherwise
     */
    protected void onSelectedRowChanged(U ui, int editingRow, C tableEditBean, C previousRowBean, boolean notPersisted, boolean newRow) {
        // update actions on ui
        SaveTableEntry.updateSaveAction(ui, newRow);
    }

    protected void stopEditTableEditBean(U ui, C tableEditBean) {
        inlineModels.values().forEach(UIHelper::cancelEditing);
        inlineModels.keySet().forEach(t -> t.getRowValidator().setParentValidator(null));
        // make inline models not editable (to avoid any messages from it)
        inlineModels.keySet().forEach(t -> t.setEditable(false));
        getValidator().setBean(null);
    }

    protected void startEditTableEditBean(U ui, C tableEditBean) {
        SwingValidator<C> validator = getValidator();
        validator.setBean(tableEditBean);
        validator.setChanged(false);
        inlineModels.keySet().forEach(t -> t.getRowValidator().setParentValidator(validator));
        startEditTableEditBeanOnInlineModels();
    }

    protected void startEditTableEditBeanOnInlineModels() {
        // make inline models editable
        inlineModels.keySet().forEach(t -> t.setEditable(true));
    }

    public final String getChildrenPropertyName() {
        return childrenPropertyName;
    }

    public final ContentTableUIListSelectionModel getSelectionModel() {
        return selectionModel;
    }

    public final void selectFirst() {
        select(0);
    }

    public final void selectLast() {
        select(getRowCount() - 1);
    }

    public final void selectPrevious(int selectedRow) {
        select(selectedRow - 1);
    }

    public final void selectNext(int selectedRow) {
        select(selectedRow + 1);
    }

    /**
     * Positionne un bean dans le modèle.
     * <p>
     * Cela va initialiser la liste à utiliser.
     */
    public final void attachModel() {
        // pas de ligne sélectionnée
        changeSelectedRow(-1);

        // pas en mode creation
        setCreate(false);

        setInit(true);

        updateEmpty();

        if (log.isDebugEnabled()) {
            log.debug("editable : " + isEditable());
            log.debug("size : " + getRowCount());
        }

        // notify listeners
        fireTableDataChanged();
    }

    public final void detachModel() {
        setModified(false);
        int size = getRowCount();
        // on indique que le modèle n'est plus lie au bean
        // cela permet de ne plus charger l'association dans le cache
        setInit(false);
        if (size > 0) {
            // il y avait des données que l'on a supprimé
            fireTableRowsDeleted(0, size - 1);
            updateEmpty();
        }
        changeSelectedRow(-1);
        setCreate(false);
    }

    /**
     * Permet l'ajout d'une nouvelle entrée à éditer
     */
    public final void addNewEntry() {
        ensureEditable();

        int selectedRow = getSelectedRow();
        if (selectedRow > -1) {
            // il y avait une ligne précédemment sélectionnée,
            // on doit verifier que l'on peut changer d'entrée
            if (!isCanQuitEditingRow()) {
                // on ne peut pas quitter la ligne en cours d'édition
                // on annule donc l'opération
                return;
            }
        }
        // on est autorise a ajouter une nouvelle entrée
        // hook before changing row
        // FIXME I am not quite sure of this flow, should not it be managed another way, somewhere else?
        onBeforeAddNewEntry(selectedRow);

        int row = getRowCount();

        C bean = getModel().newTableEditBean();
        data.add(bean);
        if (getModel().getStates().isWithIndex()) {
            ((WithIndex) bean).setIndex(getRowCount());
        }

        updateBeanList(false);

        this.create = true;
        fireTableRowsInserted(row, row);
        this.create = false;

        int viewRow = getContext().getTable().convertRowIndexToView(row);

        updateEmpty();

        // on est en mode creation
        setCreate(true);

        // la nouvelle ligne est celle en cours d'édition
        changeSelectedRow(viewRow);
    }

    public final void addNewEntry(C bean) {
        ensureEditable();

        int selectedRow = getSelectedRow();
        if (selectedRow > -1) {
            // il y avait une ligne précédemment sélectionnée,
            // on doit verifier que l'on peut changer d'entrée
            if (!isCanQuitEditingRow()) {
                // on ne peut pas quitter la ligne en cours d'édition
                // on annule donc l'opération
                return;
            }
        }
        // on est autorise a ajouter une nouvelle entrée
        // hook before changing row
        // FIXME I am not quite sure of this flow, should not it be managed another way, somewhere else?
        onBeforeAddNewEntry(selectedRow);

        int row = getRowCount();

        data.add(bean);
        if (getModel().getStates().isWithIndex()) {
            ((WithIndex) bean).setIndex(getRowCount());
        }

        updateBeanList(false);

        this.create = true;
        fireTableRowsInserted(row, row);
        this.create = false;

        int viewRow = getContext().getTable().convertRowIndexToView(row);

        updateEmpty();

        // on est en mode creation
        setCreate(true);

        // la nouvelle ligne est celle en cours d'édition
        changeSelectedRow(viewRow);
    }

    protected String getDeleteExtraMessage(C bean) {
        return null;
    }

    public final void doRemoveRow(int rowToDelete, boolean force) {
        int modelRow = getModelRow(rowToDelete);
        log.info("Do remove row {} (model row {})", rowToDelete, modelRow);
        C bean = getValueAt(modelRow);

        if (force || DeleteEdit.confirmForEntityDelete(context, bean, getDeleteExtraMessage(bean))) {
            // delete row
            removeRow(rowToDelete);
            rowToDelete--;

            installOrderIfNecessary();
            // on veut sélectionner la ligne précédente si elle existe
            // ou bien la meme ligne (si on était sur la premiere ligne)


            // on force toujours le passage sur la ligne d'avant
            // afin que le binding se déroule bien meme si ensuite on rechange
            // la ligne sélectionnée (cas ou on était sur la premier ligne et
            // que le modèle n'est pas vide)
            changeSelectedRow(rowToDelete);
            if (rowToDelete == -1 && !isEmpty()) {
                // on repasse sur la premiere ligne
                // car le modèle n'est pas vide
                changeSelectedRow(0);
            }
        }
    }

    public final boolean isCanQuitEditingRow() {
        if (selectedRow == -1) {
            // aucune ligne sélectionnée
            // on peut changer la ligne sans verification
            return true;
        }

        if (!create && !isModelModified()) {
            // on est sur une ligne en mode mise a jour
            // et aucune changement n'a ete effectue
            // on peut continuer sans rien tester
            return true;
        }

        // une ligne était précédemment en cours d'édition et a ete modifiée

        log.debug(String.format("Editing row %d was modified, need confirmation", getSelectedRow()));

        boolean canContinue = false;

        if (isModelValid()) {
            getModel().getStates().prepareTableEditBeanBeforeSave();
        }

        if (isModelValid()) {
            // la ligne est valide, on demande a l'utilisateur s'il
            // veut la sauvegarder

            int response = context.getHandler().askToUser(
                    I18n.t("observe.ui.title.need.confirm"),
                    I18n.t("observe.ui.message.table.editBean.modified"),
                    JOptionPane.WARNING_MESSAGE,
                    new Object[]{
                            I18n.t("observe.ui.choice.save"),
                            I18n.t("observe.ui.choice.doNotSave"),
                            I18n.t("observe.ui.choice.cancel")},
                    0);
            if (log.isDebugEnabled()) {
                log.debug("response : " + response);
            }

            switch (response) {
                case JOptionPane.CLOSED_OPTION:
                case 2:

                    break;
                case 0:
                    // will save ui
                    // sauvegarde des modifications
                    updateRowFromEditBean();
                    canContinue = true;
                    break;
                case 1:
                    // edition annulée
                    canContinue = true;
                    if (create) {

                        // on doit supprimer la ligne de creation
                        removeRow(getSelectedRow());
                    } else {

                        // reset row
                        resetEditBean();
                    }
                    break;
            }

        } else {

            // le validateur n'est pas ok, on ne peut que proposer la perte
            // des donnees car elles sont ne pas enregistrables

            int response = context.getHandler().askToUser(
                    I18n.t("observe.ui.title.need.confirm"),
                    I18n.t("observe.ui.message.table.editBean.modified.but.invalid"),
                    JOptionPane.ERROR_MESSAGE,
                    new Object[]{
                            I18n.t("observe.ui.choice.continue"),
                            I18n.t("observe.ui.choice.cancel")},
                    0);
            if (log.isDebugEnabled()) {
                log.debug("response : " + response);
            }
            if (response == 0) {
                // wil reset ui
                canContinue = true;
                if (create) {
                    // on doit supprimer la ligne de creation
                    removeRow(getSelectedRow());
                }
            }
        }
        return canContinue;
    }

    public final void resetEditBean() {

        int modelRow = getModelRow(selectedRow);

        onBeforeResetRow(selectedRow);

        C rowBean = getValueAt(modelRow);

        load(rowBean, getTableEditBean());

        // plus de modification sur le bean d'édition
        getValidator().setChanged(false);

        onAfterResetRow(selectedRow);
    }

    /**
     * Sélectionne la ligne dont l'index est donné.
     *
     * @param row l'index de la nouvelle ligne a éditer
     */
    public final void changeSelectedRow(int row) {

        int modelRow = getModelRow(row);

        log.info("Change selected row: {} (model row: {})", row, modelRow);

        if (log.isDebugEnabled()) {
            log.debug("row      : " + row);
            log.debug("editable : " + isEditable());
            log.debug("size     : " + getRowCount());
        }

        C rowBean = null;
        C previousRowBean = null;
        if (modelRow > -1) {
            ensureRowIndex(modelRow);
            rowBean = getValueAt(modelRow);
            if (modelRow > 0) {
                previousRowBean = getValueAt(modelRow - 1);
            }
        }

        boolean modified = getModel().getStates().isModified();
        C tableEditBean = getTableEditBean();
        onSelectedRowChanged(rowBean, previousRowBean, tableEditBean, row);
        getModel().getStates().setModified(modified);
    }

    /**
     * Pour mettre a jour la ligne en cours d'édition a partir du bean d'édition
     */
    public final void updateRowFromEditBean() {
        ensureEditable();

        int editingRow = getSelectedRow();
        int modelRow = getModelRow(editingRow);
        C tableEditBean = getTableEditBean();
        onBeforeUpdateRowFromEditBean(editingRow);

        // mettre a jour la ligne
        C rowBean = getValueAt(modelRow);
        load(tableEditBean, rowBean);

        if (create) {
            // We are no more in create mode
            setCreate(false);
        }

        if (getModel().getStates().isSortable()) {
            // We need to update the hole table since the sort process may move this row to somewhere else
            fireTableRowsUpdated(0, getRowCount() - 1);
        } else {
            fireTableRowsUpdated(editingRow, editingRow);
        }

        // plus de modification sur le bean d'édition
        getValidator().setChanged(false);

        // le model a ete modifie
        setModified(true);
        // on valide le bean principal
        // pour cela on doit recharger l'association dans le bean principale
        // car vu que l'on travaille sur des collections, si on ne supprime
        // pas la liste avant de vouloir valider, alors aucune validation ne
        // sera déclenchée (car pas de propriété modifié dans le bean...)
        getParentValidator().doValidate();
    }

    public final C getTableEditBean() {
        return getModel().getStates().getTableEditBean();
    }

    public final boolean isAdjusting() {
        return adjusting;
    }

    public final void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    public final boolean isNewRow() {
        return getTableEditBean().getId() == null;
    }

    public final boolean isCreate() {
        return create;
    }

    public final void setCreate(boolean create) {
        boolean old = this.create;
        this.create = create;
        firePropertyChange(CREATE_PROPERTY, old, create);
    }

    public final int getSelectedRow() {
        return selectedRow;
    }

    public final void setSelectedRow(int selectedRow) {
        // Not used here!
        log.debug(String.format("New selected row: %d", selectedRow));
    }

    public final boolean isModified() {
        return modified;
    }

    public final void setModified(boolean modified) {
        boolean oldModified = this.modified;
        this.modified = modified;
        firePropertyChange(MODIFIED_PROPERTY, oldModified, modified);
    }

    public final boolean isEditable() {
        return editable;
    }

    public final void setEditable(boolean editable) {
        boolean oldModified = this.editable;
        this.editable = editable;
        firePropertyChange(EDITABLE_PROPERTY, oldModified, editable);
    }

    public final boolean isEmpty() {
        return getRowCount() == 0;
    }

    public final List<C> getData() {
        if (data == null) {
            if (init) {
                // mode is initialized, get list of children from bean
                D bean = getBean();
                Collection<C> children = getChildren(bean);
                if (children == null || children.isEmpty()) {
                    data = new ArrayList<>();
                } else {
                    data = new ArrayList<>(children);
                }
            } else {
                // mode not yet initialized, just return an empty list
                data = new ArrayList<>();
            }
        }
        return data;
    }

    @Override
    public final int getRowCount() {
        List<C> list = getData();
        return list == null ? 0 : list.size();
    }

    @Override
    public final int getColumnCount() {
        return metas.size();
    }

    @Override
    public final String getColumnName(int columnIndex) {
        ensureColumnIndex(columnIndex);
        return metas.get(columnIndex).getName();
    }

    @Override
    public final Class<?> getColumnClass(int columnIndex) {
        ensureColumnIndex(columnIndex);
        return metas.get(columnIndex).getType();
    }

    @Override
    public final Object getValueAt(int row, int column) {
        ensureColumnIndex(column);
        ContentTableMeta<C> meta = getColumnMeta(column);
        C bean = getValueAt(row);
        return bean == null ? null : meta.getValue(bean);
    }

    public final C getValueAt(int row) {
        ensureRowIndex(row);
        List<C> list = getData();
        return list.get(row);
    }

    /**
     * @param <T>    the type of the column property
     * @param column the column to scan
     * @return the list of used properties for a given column
     */
    @SuppressWarnings({"unchecked"})
    public final <T> List<T> getColumnValues(int column) {
        List<T> result = new ArrayList<>();
        if (!isEmpty()) {
            for (int i = 0; i < getRowCount(); i++) {
                T value = (T) getValueAt(i, column);
                if (value != null) {
                    result.add(value);
                }
            }
        }

        return result;
    }

    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public final void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public final void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    public final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    public final PropertyChangeListener[] getPropertyChangeListeners(String propertyName) {
        return pcs.getPropertyChangeListeners(propertyName);
    }

    public final void moveTop(int selectedRow) {
        moveTo(selectedRow, 0);
    }

    public final void moveBottom(int selectedRow) {
        moveTo(selectedRow, getRowCount() - 1);
    }

    public final void moveUp(int selectedRow) {
        moveTo(selectedRow, selectedRow - 1);
    }

    public final void moveDown(int selectedRow) {
        moveTo(selectedRow, selectedRow + 1);
    }

    public final SwingValidator<C> getValidator() {
        return context.getValidatorTable();
    }

    protected final Collection<C> getChildren(D bean) {
        return bean.get(childrenPropertyName);
    }

    protected final void loadRowBeanToEdit(C rowBean, C tableEditBean, int editingRow, boolean newRow) {
        if (rowBean == null) {
            tableEditBean.clear();
        } else {
            load(rowBean, tableEditBean);
        }
        onAfterLoadRowBeanToEdit(editingRow, newRow);
    }

    protected final void load(C source, C target) {
        source.copy(target);
    }

    protected final void setChildren(D parent, List<C> children) {
        parent.set(childrenPropertyName, new LinkedList<>(children));
    }

    protected final void onSelectedRowChanged(C rowBean, C previousRowBean, C tableEditBean, int selectedRow) {

        int oldSelectedRow = this.selectedRow;
        this.selectedRow = selectedRow;

        boolean withSelection = selectedRow > -1;
        boolean newRow = isCreate();

        if (editable) {
            stopEditTableEditBean(context, tableEditBean);
        }

        setAdjusting(true);

        try {
            // on charge le bean d'édition
            loadRowBeanToEdit(rowBean, tableEditBean, selectedRow, newRow);
        } finally {
            setAdjusting(false);
        }

        boolean notPersisted = tableEditBean.isNotPersisted();

        // on modifie la ligne d'édition
        // but no fire until every thing is ready on editor
        this.selectedRow = selectedRow;

        log.debug(String.format("callback new selectedRow : %d : %s", selectedRow, tableEditBean.getId()));

        boolean opened = getModel().getStates().isOpened();
        if (editable && opened) {
            onSelectedRowChanged(context, selectedRow, tableEditBean, previousRowBean, notPersisted, newRow);
            if (newRow) {
                // on new row, must go back to first tab (if any)
                getContext().getHandler().getContentOpen().selectFirstTab();
            }
        }

        if (editable && opened && withSelection) {
            startEditTableEditBean(context, tableEditBean);
        }
        firePropertyChange(SELECTED_ROW_PROPERTY, oldSelectedRow, selectedRow);
    }

    protected final ContentTableUIHandler<D, C, U> getHandler() {
        return context.getHandler();
    }

    protected final D getBean() {
        return getModel().getStates().getBean();
    }

    private void updateEmpty() {
        firePropertyChange(EMPTY_PROPERTY, null, isEmpty());
    }

    protected SwingValidator<D> getParentValidator() {
        return context == null ? null : context.getValidator();
    }

    protected final U getContext() {
        return context;
    }

    protected final void removeRow(int row) {
        ensureRowIndex(row);
        changeSelectedRow(-1);
        int modelRow = getModelRow(row);
        getData().remove(modelRow);

        updateBeanList(!create);

        // model has changed
        if (!create) {
            setModified(true);
        }
        fireTableRowsDeleted(row, row);
        if (create) {
            // quit create mode
            setCreate(false);
        }
        updateEmpty();
        onRemovedRow(modelRow);
    }

    protected final Object getValueAt(C bean, ContentTableMeta<C> meta) {
        return meta.getValue(bean);
    }

    protected final void setInit(boolean init) {
        this.init = init;
        // init state always clear cache
        clearCache();
    }

    private void ensureColumnIndex(int columnIndex) throws ArrayIndexOutOfBoundsException {
        if (columnIndex < 0 || columnIndex >= metas.size()) {
            throw new ArrayIndexOutOfBoundsException("column index should be in [0," + metas.size() + "], but was " + columnIndex);
        }
    }

    private void ensureRowIndex(int rowIndex) throws ArrayIndexOutOfBoundsException {
        int size = getRowCount();
        if (rowIndex < 0 || rowIndex >= size) {
            throw new ArrayIndexOutOfBoundsException("row index should be in [0," + (getRowCount() - 1) + "], but was " + rowIndex);
        }
    }

    private void ensureEditable() throws IllegalStateException {
        if (!editable) {
            throw new IllegalStateException("can not edit this model since it is marked as none editable " + this);
        }
    }

    private ContentTableMeta<C> getColumnMeta(int columnIndex) {
        ensureColumnIndex(columnIndex);
        return metas.get(columnIndex);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected void moveTo(int selectedRow, int newSelectedRow) {
        if (!isCanQuitEditingRow()) {
            return;
        }
        C remove = data.remove(selectedRow);
        data.add(newSelectedRow, remove);
        installOrderIfNecessary();
        if (newSelectedRow > selectedRow) {
            fireTableRowsUpdated(selectedRow, newSelectedRow);
        } else {
            fireTableRowsUpdated(newSelectedRow, selectedRow);
        }
        inlineModels.keySet().forEach(inlineModel -> inlineModel.onMoveTo(selectedRow, newSelectedRow));
        changeSelectedRow(newSelectedRow);
        getModel().getStates().setCanSaveRow(false);
        getModel().getStates().setCanResetRow(false);
        getValidator().setChanged(false);
        setModified(true);

    }

    private void clearCache() {
        data = null;
    }

    private void updateBeanList(boolean shouldChanged) {
        SwingValidator<D> parentValidator = getParentValidator();
        boolean wasChanged = parentValidator.isChanged();

        // set back list of children in bean, to get parent validation in real-time on bean
        setChildren(getBean(), data);
        parentValidator.doValidate();
        if (!shouldChanged && !wasChanged) {
            // set back changed state in parent validator
            parentValidator.setChanged(false);
        }

    }

    private boolean isModelModified() {
        return getValidator().isChanged();
    }

    private boolean isModelValid() {
        return getValidator().isValid();
    }

    private void select(int selectedRow) {
        getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
    }

    public void clearInlineModels() {
        inlineModels.keySet().forEach(EditableTableModelWithCache::clear);
    }

    private int getModelRow(int viewRow) {
        return viewRow == -1 ? -1 : getContext().getTable().convertRowIndexToModel(viewRow);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void installOrderIfNecessary() {
        if (getModel().getStates().isWithIndex()) {
            WithIndex.installOrder((List)getData());
        }
    }
}
