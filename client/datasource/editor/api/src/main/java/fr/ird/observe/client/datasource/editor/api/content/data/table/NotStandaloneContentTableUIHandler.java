package fr.ird.observe.client.datasource.editor.api.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.EditableContentUI;
import fr.ird.observe.client.datasource.editor.api.content.NotStandaloneContentUI;
import fr.ird.observe.client.datasource.editor.api.content.NotStandaloneContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;

/**
 * Created by tchemit on 07/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class NotStandaloneContentTableUIHandler<D extends DataDto, C extends ContainerChildDto, U extends ContentTableUI<D, C, U> & NotStandaloneContentUI<D>> extends ContentTableUIHandler<D, C, U> implements NotStandaloneContentUIHandler<D> {

    private EditableContentUI<D> parent;

    @Override
    public EditableContentUI<D> getParentUI() {
        if (parent == null) {
            parent = NotStandaloneContentUIHandler.getParentUI(ui);
        }
        return parent;
    }

    @Override
    protected ContentOpenWithValidator<U> createContentOpen(U ui) {
        ContentTableUIOpenExecutor<D, C, U> executor = new NotStandaloneContentTableUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor);
    }

    @Override
    public void installChangeModeAction() {
        // not for this one
    }

    protected void onTableModelChanged() {
        // parent ui manages this
    }
}
