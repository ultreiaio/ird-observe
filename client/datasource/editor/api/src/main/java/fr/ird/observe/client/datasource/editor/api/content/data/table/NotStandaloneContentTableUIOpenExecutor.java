package fr.ird.observe.client.datasource.editor.api.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;

/**
 * Created on 27/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class NotStandaloneContentTableUIOpenExecutor<D extends DataDto, C extends ContainerChildDto, U extends ContentTableUI<D, C, U>> extends ContentTableUIOpenExecutor<D, C, U> {

    @Override
    public void openModel(U ui) {
        ContentTableUIModel<D, C> model = ui.getModel();
        ui.getTableModel().detachModel();

        model.open();

        // initialisation du modèle du tableau
        ui.getTableModel().attachModel();

        boolean canEdit = model.getStates().isUpdatingMode();
        if (!canEdit) {
            ui.getTableModel().setEditable(false);
        }

        ContentTableUIHandler<D, C, U> handler = ui.getHandler();
        handler.onOpenAfterOpenModel();
        if (!model.getStates().isUpdatingMode()) {
            handler.reselectRow();
        }
    }

    @Override
    public void startEdit(U ui) {
        ui.getValidatorTable().setParentValidator(ui.getValidator());
        super.startEdit(ui);
    }

    @Override
    public void stopEdit(U ui) {
        ui.getValidatorTable().setParentValidator(null);
        super.stopEdit(ui);
    }

    @Override
    public void doEditOnOpen(U ui) {
        // Do not try to edit on open, just try to select the first row in table
        ui.getHandler().reselectRow();
    }
}
