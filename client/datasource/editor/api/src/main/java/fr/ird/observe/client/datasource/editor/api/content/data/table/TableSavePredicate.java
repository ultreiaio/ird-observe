package fr.ird.observe.client.datasource.editor.api.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.save.SavePredicateSupport;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;

import java.util.List;

/**
 * Created on 23/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TableSavePredicate<D extends DataDto, C extends ContainerChildDto, S extends ContentTableUIModelStates<D, C>> extends SavePredicateSupport<S, D> {

    public TableSavePredicate(S states) {
        super(states);
    }

    @Override
    protected boolean testForValid(D oldBean, D bean) {
        return test(states, oldBean, bean);
    }

    public boolean test(S states, D oldBean, D bean) {
        ContentTableUITableModel<D, C, ?> tableModel = states.getTableModel();
        if (states.isModified() || states.isCanSaveRow()) {
            // flush modified row to table model if table not empty
            if (!tableModel.isEmpty()) {
                tableModel.updateRowFromEditBean();
            }
        }
        List<C> objets = tableModel.getData();
        boolean canContinue;
        try {
            canContinue = canSave(states, oldBean, bean, objets);
        } catch (Exception e) {
            canContinue = false;
        }
        return canContinue;
    }

    public boolean canSave(S states, D originalBean, D bean, List<C> data) {
        // by default can always save
        return true;
    }

}
