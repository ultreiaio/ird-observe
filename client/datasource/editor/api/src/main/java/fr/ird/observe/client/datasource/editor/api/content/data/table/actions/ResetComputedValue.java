package fr.ird.observe.client.datasource.editor.api.content.data.table.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.IdDto;
import org.nuiton.jaxx.widgets.number.NumberEditor;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * To reset computed value.
 * <p>
 * Created on 09/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ResetComputedValue extends ContentTableUIActionSupport<ContentTableUI<?, ?, ?>> {

    /**
     * Table bean property name of value.
     */
    private final String propertyName;
    /**
     * Table bean property name of computed value.
     */
    private final String computedValuePropertyName;

    public ResetComputedValue(String propertyName, String computedValuePropertyName) {
        super(null, null, "data-calcule", null);
        this.propertyName = Objects.requireNonNull(propertyName);
        this.computedValuePropertyName = Objects.requireNonNull(computedValuePropertyName);
    }

    protected abstract String getToolTipText(Object computed);

    @Override
    protected final void doActionPerformed(ActionEvent e, ContentTableUI<?, ?, ?> ui) {
        resetBean(ui.getModel().getStates().getTableEditBean());
        delegate(ui).grabFocus();
    }

    @Override
    public final void defaultInit(InputMap inputMap, ActionMap actionMap) {
        super.defaultInit(inputMap, actionMap);
        editor.setDisabledIcon(UIHelper.getUIManagerActionIcon("data-observe"));
        delegate(ui).getRightToolbar().add(editor);
        ui.getModel().getStates().getTableEditBean().addPropertyChangeListener(computedValuePropertyName, evt -> {
            Object newValue = evt.getNewValue();
            // when computed value change, let's update tooltipText
            editor.setToolTipText(getToolTipText(newValue));
            // can use this action only if there is a value in model
            editor.setEnabled(newValue != null);
        });
    }

    protected final void resetBean(IdDto bean) {
        bean.set(computedValuePropertyName, null);
    }

    protected final NumberEditor delegate(ContentTableUI<?, ?, ?> ui) {
        return (NumberEditor) ui.getObjectById(propertyName);
    }
}
