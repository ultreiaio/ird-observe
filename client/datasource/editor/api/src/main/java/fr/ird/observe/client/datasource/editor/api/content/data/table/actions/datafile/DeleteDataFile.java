package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.datafile;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.dto.data.WithDataFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 16/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public final class DeleteDataFile extends WithDataFileActionSupport<ContentTableUI<?, ?, ?>> {

    private static final Logger log = LogManager.getLogger(DeleteDataFile.class);

    public static void installAction(ContentTableUI<?, ?, ?> ui, JButton editor) {
        DeleteDataFile action = new DeleteDataFile();
        log.info(String.format("Install action %s on %s", action, Objects.requireNonNull(editor).getName()));
        DeleteDataFile.init(ui, Objects.requireNonNull(editor), action);
    }

    private DeleteDataFile() {
        super(n("observe.data.WithDataFile.action.deleteDataFile"), n("observe.data.WithDataFile.action.deleteDataFile.tip"), "content-delete-16", ObserveKeyStrokesEditorApi.KEY_STROKE_DELETE_DATA);
    }

    @Override
    protected void doActionPerformed(WithDataFile tableEditBean) {
        int response = askToUser(t("observe.ui.title.delete"),
                               t("observe.data.WithDataFile.delete.data.file.message"),
                               JOptionPane.WARNING_MESSAGE,
                               new Object[]{t("observe.ui.choice.confirm.delete"),
                                       t("observe.ui.choice.cancel")},
                               1);
        boolean doDelete = response == 0;
        if (doDelete) {
            log.info(getLog(String.format("Delete data from dto: %s", tableEditBean.getTopiaId())));
            tableEditBean.setData(null);
            tableEditBean.setHasData(false);
        }
    }

}
