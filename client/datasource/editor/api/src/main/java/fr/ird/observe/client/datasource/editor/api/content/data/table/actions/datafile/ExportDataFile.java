package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.datafile;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.dto.data.DataFileDto;
import fr.ird.observe.dto.data.WithDataFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JButton;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 16/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public final class ExportDataFile extends WithDataFileActionSupport<ContentTableUI<?, ?, ?>> {

    private static final Logger log = LogManager.getLogger(ExportDataFile.class);

    public static void installAction(ContentTableUI<?, ?, ?> ui, JButton editor) {
        ExportDataFile action = new ExportDataFile();
        log.info(String.format("Install action %s on %s", action, Objects.requireNonNull(editor).getName()));
        ExportDataFile.init(ui, (JButton) Objects.requireNonNull(ui).getObjectById("exportData"), action);
    }

    private ExportDataFile() {
        super(n("observe.data.WithDataFile.action.exportDataFile"), n("observe.data.WithDataFile.action.exportDataFile.tip"), "data-export", ObserveKeyStrokesEditorApi.KEY_STROKE_EXPORT_DATA);
    }

    @Override
    protected void doActionPerformed(WithDataFile tableEditBean) {
        DataFileDto dataFile = tableEditBean.getData();
        if (dataFile == null) {
            dataFile = getService().getDataFile(tableEditBean.getTopiaId());
        }
        File file = getToSave()
                .setTitle(t("observe.data.WithDataFile.choose.title.exportDataFile"))
                .setApprovalText(t("observe.data.WithDataFile.action.exportDataFile.filter"))
                .setFilename(dataFile.getName())
                .setUseAcceptAllFileFilter(true)
                .choose();

        if (file != null && confirmOverwriteFileIfExist(file)) {
            try {
                log.info(getLog(String.format("Save data to file %s", file)));
                Files.write(file.toPath(), dataFile.getContent());
            } catch (IOException e) {
                throw new ObserveSwingTechnicalException("Could not save binary data to " + file, e);
            }
        }
    }
}
