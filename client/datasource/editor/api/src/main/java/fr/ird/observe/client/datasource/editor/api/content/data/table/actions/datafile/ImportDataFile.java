package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.datafile;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.dto.data.DataFileDto;
import fr.ird.observe.dto.data.WithDataFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.io.File;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 16/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public final class ImportDataFile extends WithDataFileActionSupport<ContentTableUI<?, ?, ?>> {

    private static final Logger log = LogManager.getLogger(ImportDataFile.class);

    public static void installAction(ContentTableUI<?, ?, ?> ui, JButton editor) {
        ImportDataFile action = new ImportDataFile();
        log.info(String.format("Install action %s on %s", action, Objects.requireNonNull(editor).getName()));
        ImportDataFile.init(ui, Objects.requireNonNull(editor), action);
    }

    private ImportDataFile() {
        super(n("observe.data.WithDataFile.action.importDataFile"), n("observe.data.WithDataFile.action.importDataFile.tip"), "data-import", ObserveKeyStrokesEditorApi.KEY_STROKE_IMPORT_DATA);
    }

    @Override
    protected void doActionPerformed(WithDataFile tableEditBean) {
        if (tableEditBean.isHasData()) {
            DataFileDto dataFile = tableEditBean.getData();
            if (dataFile == null) {
                dataFile = getService().getDataFile(tableEditBean.getTopiaId());
            }
            int response = askToUser(t("observe.ui.title.confirmReplace"),
                                   t("observe.data.WithDataFile.action.replace.data.file.message", dataFile.getName()),
                                   JOptionPane.WARNING_MESSAGE,
                                   new Object[]{t("observe.ui.choice.confirm.replace"),
                                           t("observe.ui.choice.cancel")},
                                   1);
            boolean doReplace = response == 0;
            if (!doReplace) {
                return;
            }
        }

        File file = chooseFile(t("observe.data.WithDataFile.choose.title.importData"),
                               t("observe.data.WithDataFile.action.importData"),
                               null,
                               null);
        if (file != null) {
            log.info(getLog(String.format("Import data from file: %s", file)));
            WithDataFile.setFile(tableEditBean, file);
        }
    }

}
