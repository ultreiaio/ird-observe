package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.datafile;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.ContentTableUIActionSupport;
import fr.ird.observe.dto.data.WithDataFile;
import fr.ird.observe.services.service.data.DataFileService;

import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

/**
 * Created on 16/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class WithDataFileActionSupport<U extends ContentTableUI<?, ?, ?>> extends ContentTableUIActionSupport<U> {

    public WithDataFileActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    protected abstract void doActionPerformed(WithDataFile tableEditBean);

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        WithDataFile tableEditBean = (WithDataFile) getUi().getModel().getStates().getTableEditBean();
        doActionPerformed(tableEditBean);
    }

    protected DataFileService getService() {
        return getServicesProvider().getDataFileService();
    }
}
