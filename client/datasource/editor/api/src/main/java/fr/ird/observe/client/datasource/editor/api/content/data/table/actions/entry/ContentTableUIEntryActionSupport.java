package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.content.NotStandaloneContentUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUITableModel;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.ContentTableUIActionSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.swing.action.JAXXObjectActionSupport;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by tchemit on 02/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ContentTableUIEntryActionSupport extends ContentTableUIActionSupport<ContentTableUI<?, ?, ?>> implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(ContentTableUIEntryActionSupport.class);
    private final PropertyChangeListener propertyChangeListener;
    private boolean canExecuteFromRead;

    public static <U extends JAXXObject, A extends JAXXObjectActionSupport<U>> A init(U ui, AbstractButton editor, A action) {
        JAXXObjectActionSupport.init(ui, editor, action);
        editor.putClientProperty(ACTIVATE_FROM_POPUP, true);
        return action;
    }

    protected ContentTableUIEntryActionSupport(String text, String tip, String icon, KeyStroke keyStroke) {
        super(text, tip, icon, keyStroke);
        propertyChangeListener = e -> {
            boolean b = computeEnabled(this.ui.getTableModel().getSelectedRow(), this.ui.getTableModel().getRowCount());
            log.debug("Set enabled? " + b + " on " + getActionCommandKey());
            setEnabled(b);
        };
        setEnabled(false);
    }

    @Override
    protected JComponent getContainer(ContentTableUI<?, ?, ?> ui) {
        if (ui.getModel().getStates().isStandalone()) {
            return super.getContainer(ui);
        }
        // if not standalone, attach to body since this is the only part which will be displayed and action aware
        return (JComponent) ((NotStandaloneContentUI<?>) ui).getParentUI();
    }

    public abstract boolean computeEnabled(int selectedRow, int rowCount);

    protected abstract void actionPerformed(ContentTableUITableModel<?, ?, ?> tableModel, int selectedRow);

    public boolean isCanExecuteFromRead() {
        return canExecuteFromRead;
    }

    public void setCanExecuteFromRead(boolean canExecuteFromRead) {
        this.canExecuteFromRead = canExecuteFromRead;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ContentTableUI<?, ?, ?> contentTableUI) {
        if (!isEnabled()) {
            //FIXME should not have to check this here...
            log.info(String.format("Reject action: %s :: %s", getActionCommandKey(), this));
            return;
        }
        log.info(String.format("Accept action: %s :: %s", getActionCommandKey(), this));
        int selectedRow = ui.getTable().getSelectedRow();
        actionPerformed(ui.getTableModel(), selectedRow);
    }

    @Override
    public void init() {
        super.init();
        this.ui.getTableModel().addPropertyChangeListener(ContentTableUITableModel.SELECTED_ROW_PROPERTY, propertyChangeListener);
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        if (!isCanExecuteFromRead() && ui.getModel().getStates().isReadingMode()) {
            log.info(String.format("Reject action : %s from reading mode.", getName()));
            return false;
        }
        return super.canExecuteAction(e);
    }
}
