package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.ContentTableUIActionSupport;
import io.ultreia.java4all.i18n.I18n;

import java.awt.event.ActionEvent;

/**
 * Created on 10/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public final class DeleteEntry<U extends ContentTableUI<?, ?, ?>> extends ContentTableUIActionSupport<U> implements ConfigureMenuAction<U> {

    public static <U extends ContentTableUI<?, ?, U>> void installAction(U ui) {
        DeleteEntry<U> action = new DeleteEntry<>();
        // by default make it disabled, ui binding may not be fired if model is empty...
        ui.getDeleteEntry().setEnabled(false);
        DeleteEntry.init(ui, ui.getDeleteEntry(), action);
    }

    public DeleteEntry() {
        super(I18n.n("observe.ui.action.delete"), I18n.n("observe.Common.action.delete.entry.tip"), "content-delete-16", ObserveKeyStrokesEditorApi.KEY_STROKE_DELETE_DATA_GLOBAL);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        ui.getHandler().removeSelectedRow(ui.getTableModel().getSelectedRow());
    }
}
