package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUITableModel;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.ContentTableUIActionSupport;
import io.ultreia.java4all.i18n.I18n;

import java.awt.event.ActionEvent;

/**
 * Created on 10/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public final class ResetEntry extends ContentTableUIActionSupport<ContentTableUI<?, ?, ?>> {

    public ResetEntry() {
        super(I18n.n("observe.Common.action.reset"), I18n.n("observe.Common.action.reset.new.entry.tip"), "revert", ObserveKeyStrokesEditorApi.KEY_STROKE_RESET_TABLE_ENTRY);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, ContentTableUI<?, ?, ?> ui) {
        ContentTableUITableModel<?, ?, ?> tableModel = ui.getTableModel();
        if (tableModel.isCreate()) {
            // reset new entry
            tableModel.doRemoveRow(tableModel.getSelectedRow(), true);
            ContentTableUIModelStates<?, ?> states = ui.getModel().getStates();
            if (states.isFormEmpty()) {
                tableModel.setModified(false);
                states.setCanSaveRow(false);
                states.setModified(false);
                // plus de modification sur le bean d'édition
                ui.getValidator().setChanged(false);
            }
        } else {
            // reset existing entry
            ui.getStates().setResetEdit(true);
            try {
                tableModel.resetEditBean();
            } finally {
                ui.getStates().setResetEdit(false);
            }
        }
    }

}
