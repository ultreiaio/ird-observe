package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.ContentTableUIActionSupport;

import javax.swing.AbstractButton;
import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Created on 18/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public final class SaveAndNewEntry extends ContentTableUIActionSupport<ContentTableUI<?, ?, ?>> {

    public SaveAndNewEntry() {
        super(null, null, "add", ObserveKeyStrokesEditorApi.KEY_STROKE_SAVE_AND_NEW_TABLE_ENTRY);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ContentTableUI<?, ?, ?> contentUI) {
        contentUI.getSaveEntry().getAction().actionPerformed(e);
        AbstractButton editor = (AbstractButton) contentUI.getClientProperty(ContentTableUIHandler.CLIENT_PROPERTY_CREATE_ACTION);
        Objects.requireNonNull(editor).doClick();
    }
}
