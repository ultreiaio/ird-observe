package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.ContentTableUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.DataDto;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.Icon;
import javax.swing.JButton;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 10/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SaveTableEntry extends ContentTableUIActionSupport<ContentTableUI<?, ?, ?>> {

    private final String saveEntryText;
    private final String saveEntryTip;
    private final String saveNewEntryText;
    private final String saveNewEntryTip;
    private final Icon newIcon;
    private final Icon saveIcon;

    public static <D extends DataDto, C extends ContainerChildDto, U extends ContentTableUI<D, C, U>> void installAction(U ui) {
        SaveTableEntry action = new SaveTableEntry(ui.getModel().getScope());
        init(ui, ui.getSaveEntry(), action);
    }

    public static void updateSaveAction(ContentTableUI<?, ?, ?> ui, boolean create) {
        SaveTableEntry action = (SaveTableEntry) ui.getSaveEntry().getAction();
        action.updateSaveAction(create);
    }

    public SaveTableEntry(NavigationScope context) {
        super(null, null, "content-save-16", ObserveKeyStrokesEditorApi.KEY_STROKE_SAVE_TABLE_ENTRY);
        saveNewEntryText = context.getI18nTranslation("action.save");
        saveNewEntryTip = context.getI18nTranslation("action.save.tip");
        saveEntryText = I18n.n("observe.ui.action.save");
        saveEntryTip = I18n.n("observe.Common.action.save.entry.tip");
        saveIcon = getIcon();
        newIcon = UIHelper.getUIManagerActionIcon("add");
    }

    @Override
    public void init() {
        super.init();
        updateSaveAction(true);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ContentTableUI<?, ?, ?> contentUI) {
        contentUI.getStates().prepareTableEditBeanBeforeSave();
        if (!contentUI.getValidatorTable().isValid()) {
            // as a side effect on cleaning table edit bean to save, it could now be no more valid
            return;
        }
        boolean create = contentUI.getTableModel().isCreate();
        contentUI.getTableModel().updateRowFromEditBean();
        if (create) {
            // now save action (no more add action)
            updateSaveAction(false);
        }
    }

    public void updateSaveAction(boolean create) {
        JButton saveEntry = (JButton) getEditor();
        JButton saveAndNewEntry = ui.getSaveAndNewEntry();
        String andNextText = " " + t("observe.Common.action.save.and.next");
        String andNextTip = " " + t("observe.Common.action.save.and.next.tip");
        String text;
        String tip;
        if (create) {
            text = t(saveNewEntryText);
            tip = t(saveNewEntryTip);
            saveEntry.setIcon(newIcon);
        } else {
            text = t(saveEntryText);
            tip = t(saveEntryTip);
            saveEntry.setIcon(saveIcon);
        }
        saveEntry.putClientProperty("text", text);
        saveEntry.putClientProperty("toolTipText", tip);
        saveAndNewEntry.putClientProperty("text", text + andNextText);
        saveAndNewEntry.putClientProperty("toolTipText", tip + andNextTip);
        ObserveKeyStrokesSupport.addKeyStroke2(saveEntry, ObserveKeyStrokesEditorApi.KEY_STROKE_SAVE_TABLE_ENTRY);
        ObserveKeyStrokesSupport.addKeyStroke2(saveAndNewEntry, ObserveKeyStrokesEditorApi.KEY_STROKE_SAVE_AND_NEW_TABLE_ENTRY);
    }
}
