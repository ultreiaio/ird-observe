package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.move;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.ClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.ContentTableUIEntryActionSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.KeyStroke;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.event.RowSorterEvent;
import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created at 29/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public abstract class MoveAbstractAction extends ContentTableUIEntryActionSupport {
    private static final Logger log = LogManager.getLogger(MoveAbstractAction.class);
    protected boolean enabledFromRowSorter;

    protected MoveAbstractAction(String text, String tip, String icon, KeyStroke keyStroke) {
        super(text, tip, icon, keyStroke);
        setCheckMenuItemIsArmed(false);
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(ui.getTable().getRowSorter()).addRowSorterListener(this::onRowSorterChanged);
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        boolean result = super.canExecuteAction(e);
        if (result && !enabledFromRowSorter) {
            log.warn("Can't execute action {}, rowSorter is not compliant with a move action.", this);
            String message = t("observe.ui.datasource.editor.action.move.forbidden.message");
            ((ClientUIContextApi) this).displayWarning(t("observe.ui.datasource.editor.action.forbidden.title"), message);
            result = false;
        }
        return result;
    }

    private void onRowSorterChanged(RowSorterEvent e) {
        enabledFromRowSorter = computeMoveEnabled(e);
    }

    private boolean computeMoveEnabled(RowSorterEvent e) {
        RowSorter<?> rowSorter = e.getSource();
        for (RowSorter.SortKey sortKey : rowSorter.getSortKeys()) {
            if (sortKey.getColumn() != 0) {
                return false;
            }
            return sortKey.getSortOrder() == SortOrder.ASCENDING;
        }
        return true;
    }

}
