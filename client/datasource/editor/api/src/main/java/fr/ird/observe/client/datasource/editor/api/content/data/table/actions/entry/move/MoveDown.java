package fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.move;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUITableModel;

import static io.ultreia.java4all.i18n.I18n.n;

public final class MoveDown extends MoveAbstractAction {

    public MoveDown() {
        super(n("observe.Common.action.move.down"),
              n("observe.Common.action.move.down.tip"),
              "move-down",
              ObserveKeyStrokesEditorApi.KEY_STROKE_MOVE_DOWN_TABLE_ENTRY);
    }

    @Override
    public boolean computeEnabled(int selectedRow, int rowCount) {
        return rowCount > 0 && selectedRow < rowCount - 1;
    }

    @Override
    protected void actionPerformed(ContentTableUITableModel<?, ?, ?> tableModel, int selectedRow) {
        tableModel.moveDown(selectedRow);
        ui.getTable().repaint();
    }
}
