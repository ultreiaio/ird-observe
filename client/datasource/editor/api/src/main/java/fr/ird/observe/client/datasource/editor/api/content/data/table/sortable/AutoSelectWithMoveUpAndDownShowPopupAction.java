package fr.ird.observe.client.datasource.editor.api.content.data.table.sortable;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUITableModel;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.move.MoveBottom;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.move.MoveDown;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.move.MoveTop;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.entry.move.MoveUp;
import fr.ird.observe.client.util.table.popup.AutoSelectRowAndShowPopupActionSupport;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.table.TableModel;

/**
 * A auto select popup action with move down and up on it to change order in table.
 * <p>
 * Created by tchemit on 02/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AutoSelectWithMoveUpAndDownShowPopupAction extends AutoSelectRowAndShowPopupActionSupport {

    private final ContentTableUI<?, ?, ?> ui;
    private final JMenuItem moveTop;
    private final JMenuItem moveUp;
    private final JMenuItem moveDown;
    private final JMenuItem moveBottom;

    public AutoSelectWithMoveUpAndDownShowPopupAction(ContentTableUI<?, ?, ?> ui) {
        super(ui.getTable());
        this.ui = ui;
        JPopupMenu popup = getPopup();
        popup.removeAll();
        popup.add(this.moveTop = new JMenuItem());
        popup.add(this.moveUp = new JMenuItem());
        popup.add(this.moveDown = new JMenuItem());
        popup.add(this.moveBottom = new JMenuItem());
        MoveTop.init(ui, moveTop, new MoveTop());
        MoveUp.init(ui, moveUp, new MoveUp());
        MoveDown.init(ui, moveDown, new MoveDown());
        MoveBottom.init(ui, moveBottom, new MoveBottom());
    }

    @Override
    protected boolean canShowPopup(TableModel model) {
        boolean canShowPopup = super.canShowPopup(model);
        if (canShowPopup) {
            canShowPopup = ((ContentTableUITableModel<?, ?, ?>) model).isEditable();
        }
        return canShowPopup;
    }

    @Override
    protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {
        if (!ui.getModel().getStates().isUpdatingMode()) {
            moveTop.setEnabled(false);
            moveBottom.setEnabled(false);
            moveUp.setEnabled(false);
            moveDown.setEnabled(false);
        }
    }

}
