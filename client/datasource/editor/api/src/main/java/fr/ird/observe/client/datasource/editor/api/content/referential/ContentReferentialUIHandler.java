/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.referential;

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ChangeId;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowTechnicalInformations;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeProducer;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.actions.reset.ResetAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveAction;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.BackToListReferential;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.CreateReferential;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.DeleteReferential;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.DetailReferential;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.ModifyReferential;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.ReferentialResetAdapter;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.ReplaceReferentialUsages;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.SaveContentReferentialUIAdapter;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.ShowUsagesReferential;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialDtoReferenceWithNoCodeAware;
import fr.ird.observe.services.service.SaveResultDto;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.list.ListHeader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.CardLayout2Ext;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @param <D> referential dto type
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ContentReferentialUIHandler<D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> extends ContentUIHandler<U> {

    private static final Logger log = LogManager.getLogger(ContentReferentialUIHandler.class);

    private final Runnable reValidate;

    public ContentReferentialUIHandler() {
        super();
        reValidate = () -> {
            // revalidate ui layout
            ContentReferentialUI<D, R, U> ui1 = getUi();
            Container parent = ui1.getParent();
            if (parent == null) {
                // plus de parent donc rien a faire
                return;
            }
            ui1.revalidate();
            Component component = ui1.getHandler().computeFocusOwner();
            if (component != null) {
                component.requestFocusInWindow();
            }
        };
    }

    @Override
    public void onInit(U ui) {
        Class<ReferentialDto> mainType = getModel().getScope().getMainType();
        super.onInit(ui);
        if (ReferentialDtoReferenceWithNoCodeAware.class.isAssignableFrom(mainType)) {
            // remove code editor
            ui.getCodeAndHomeIdLabel().setText(t("observe.Common.homeId"));
            ui.getCodeAndHomeIdPanel().remove(0);
            ui.getCodeAndHomeIdPanel().add(new JLabel());
        }
        ui.getViewLayout().addPropertyChangeListener(CardLayout2Ext.SELECTED_PROPERTY_NAME, evt -> SwingUtilities.invokeLater(reValidate));
        ui.getModel().getStates().addPropertyChangeListener(ContentUIModelStates.PROPERTY_EDITING, e -> updateView((Boolean) e.getNewValue()));
        JList<R> list = ui.getList();
        list.setFixedCellHeight(20);
        list.setFixedCellWidth(200);
        list.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                selectBean(list.getSelectedValuesList());
            }
        });
        list.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if (ui.getStates().isEditable()) {
                        ui.getModify().doClick();
                    } else {
                        ui.getDetail().doClick();
                    }
                }
            }
        });
    }

    @Override
    public ContentReferentialUIModel<D, R> getModel() {
        return ui.getModel();
    }

    @Override
    protected ContentOpenWithValidator<U> createContentOpen(U ui) {
        ContentReferentialUIOpenExecutor<D, R, U> executor = new ContentReferentialUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor) {
            @Override
            public void resetFromPreviousUi(U ui) {
                super.resetFromPreviousUi(ui);
                R selectedBeanReference = ui.getModel().getStates().getSelectedBeanReference();
                if (selectedBeanReference != null) {
                    this.ui.getList().setSelectedValue(selectedBeanReference, true);
                }
            }
        };
    }

    @Override
    public final ContentOpenWithValidator<U> getContentOpen() {
        return (ContentOpenWithValidator<U>) super.getContentOpen();
    }

    @Override
    protected final ContentUIInitializer<U> createContentUIInitializer(U ui) {
        return new ContentReferentialUIInitializer<>(ui);
    }

    @Override
    public void initActions() {
        ResetAction.installAction(ui, ui.getReset(), new ReferentialResetAdapter<>());
        BackToListReferential.installAction(ui);
        DetailReferential.installAction(ui);
        DeleteReferential.installAction(ui);
        ChangeId.installAction(ui);
        ReplaceReferentialUsages.installAction(ui);
        ui.getConfigurePopup().addSeparator();
        ShowUsagesReferential.installAction(ui);
        ShowTechnicalInformations.installAction(ui);
        installCreateAction();
        installModifyAction();
        installSaveAction();
    }

    @Override
    protected Container computeFocusOwnerContainer() {
        if (getModel().getSource().getHandler().getChildCount() == 0) {
            return ui;
        }
        if (Objects.equals(ui.getViewLayout().getSelected(), ContentReferentialUI.LIST_VIEW)) {
            return ui.getContentBody();
        }
        ContentMode mode = getModel().getStates().getMode();
        if (mode == null || mode == ContentMode.READ || !getModel().getStates().isEditable()) {
            if (Objects.equals(ui.getViewLayout().getSelected(), ContentReferentialUI.DETAIL_VIEW)) {
                // can't give focus to form
                return ui.getActions();
            }
            return ui.getContentBody();
        }
        return super.computeFocusOwnerContainer();
    }

    @Override
    public void installChangeModeAction() {
        ChangeModeRequest request = ChangeModeRequest.of(ui);
        ChangeModeProducer<U> changeModeProducer = new ChangeModeProducer<>(ui, request) {
            @Override
            protected ContentMode rebuildChangeMode(ContentMode newValue) {
                return ui.getModel().getStates().isEditable() ? newValue : null;
            }
        };
        ChangeMode<U> changeMode = new ChangeMode<>(request, changeModeProducer, null) {
            @Override
            protected void rebuildTitle(U ui, ContentMode newValue) {
                if (Objects.equals(newValue, ContentMode.READ) ) {
                    ContentReferentialUIModelStates<D, R> states = ui.getModel().getStates();
                    boolean editable = states.isEditable();
                    String titleSuffix;
                    if (editable) {
                        titleSuffix = I18n.t("observe.Common.action.updating");
                    } else {
                        titleSuffix = I18n.t("observe.Common.action.notEditable");
                    }
                    String contentTitle = states.getContentTitle() + " - " + titleSuffix;
                    ui.setTitle(contentTitle);
                    return;
                }
                super.rebuildTitle(ui, newValue);
            }

            protected void rebuildAction(U ui, ContentMode newValue) {
                boolean editable;
                String tip;
                Icon icon;
                ContentUIModelStates states = ui.getModel().getStates();
                if (newValue == null) {
                    tip = I18n.n("observe.data.Openable.action.notEditable.tip");
                    icon = UIHelper.getContentActionIcon("not-editable");
                } else {
                    switch (newValue) {
                        case CREATE:
                            tip = I18n.n("observe.data.Openable.action.create.tip");
                            icon = UIHelper.getContentActionIcon("add");
                            break;
                        case UPDATE:
                            tip = I18n.n("observe.data.Openable.action.update.tip");
                            icon = UIHelper.getContentActionIcon("unlocked");
                            break;
                        case READ:
                            editable = states.isEditable();
                            if (editable) {
                                tip = I18n.n("observe.data.Openable.action.editable.tip");
                                icon = UIHelper.getContentActionIcon("unlocked");
                            } else {
                                tip = I18n.n("observe.data.Openable.action.notEditable.tip");
                                icon = UIHelper.getContentActionIcon("not-editable");
                            }
                            break;
                        default:
                            throw new IllegalStateException("Can't come here");
                    }
                }

                setEnabled(false);
                setIcon(icon);
                KeyStroke acceleratorKey = getAcceleratorKey();
                setKeyStroke(null);
                updateToolTipText(tip);
                rebuildTexts(true);
                setKeyStroke(acceleratorKey);
            }

            @Override
            protected boolean rebuildZonePredicate(U ui) {
                return ui.getStates().isEditable();
            }

            @Override
            public void rebuildEditableZone(U ui) {
                ui.getActions().setVisible(true);
                ui.getToggleInsert().setVisible(true);
                super.rebuildEditableZone(ui);
            }

            @Override
            protected void rebuildNotEditableZone(U ui) {
                boolean empty = ui.getList().getModel().getSize() == 0;
                ui.getActions().setVisible(!empty);
                ui.getToggleInsert().setVisible(false);
                super.rebuildNotEditableZone(ui);
            }

        };
        changeMode.setKeyStroke(null);
        ChangeMode.installAction(ui, changeMode);
    }

    protected void installSaveAction() {
        SaveAction.<D, U>create(ui, ui.getModel().getScope().getMainType())
                .on(ui.getModel()::toSaveRequest)
                .call((r, d) -> doSave(d))
                .then(new SaveContentReferentialUIAdapter<>())
                .install(ui.getSave());
    }

    protected SaveResultDto doSave(D bean) throws ConcurrentModificationException {
        ToolkitIdLabel replaceReference = getModel().getStates().getReplaceReference();
        if (replaceReference != null) {
            String id = bean.getId();
            String replaceId = replaceReference.getId();
            log.info(String.format("Do replace reference before save (%s → %s)", id, replaceId));
            getReferentialService().replaceReference(getModel().getSource().getScope().getMainType(), id, replaceId);
        }
        SaveResultDto result = getReferentialService().save(bean);
        bean.copy(getModel().getStates().getForm().getObject());
        return result;
    }

    protected void installCreateAction() {
        CreateReferential.installAction(ui);
    }

    protected void installModifyAction() {
        ModifyReferential.installAction(ui);
    }

    @Override
    public void onPrepareValidationContext() {
        super.onPrepareValidationContext();
        ContentReferentialUIModelStates<D, R> states = getModel().getStates();
        ui.getValidator().setContext(states.getValidationContext());
        ListHeader<R> jList = ui.getReferentialListHeader();
        List<R> data = jList.getData();
        log.info(String.format("%sSet referentiel list [%s] in validation context : %d", getModel().getPrefix(), getModel().getScope().getMainType().getSimpleName(), data.size()));
        getClientValidationContext().setEditingReferentielList(data);
    }

    final void selectBean(List<R> selectedReference) {
        ContentReferentialUIModel<D, R> model = getModel();
        log.info(String.format("%sWill select entity(ies) [%s]", prefix, selectedReference));
        model.getStates().setSelection(selectedReference);
//        if (selectedReference == null) {
//            model.getStates().setSelectedBeanReference(null);
//        } else {
//            model.getStates().setSelectedBeanReference(selectedReference);
//
//            //TODO update data cache
//            log.debug("Need update referential cache");
//        }
        if (!model.getStates().isResetEdit()) {
            getContentOpen().selectFirstTab();
        }
    }

    private void updateView(boolean editing) {
        log.debug("Editing has changed : " + editing);
//        if (!getModel().getStates().isReadingMode()) {
//            JButton deleteAction = ui.getDelete();
//            if (editing) {
//                ui.getDetailRealActions().add(deleteAction);
//            } else {
//                ui.getListActions().add(deleteAction);
//            }
//        }
        ui.getViewLayout().setSelected(editing ? ContentReferentialUI.DETAIL_VIEW : ContentReferentialUI.LIST_VIEW);
    }
}
