package fr.ird.observe.client.datasource.editor.api.content.referential;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.i18n.I18n;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.Table;

import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * Created on 11/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.9
 */
public class ContentReferentialUIInitializer<E extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<E, R, U>> extends ContentUIInitializer<U> {

    ContentReferentialUIInitializer(U ui) {
        super(ui);
    }

    /*

  <java.lang.String id='listText' initializer='t("observe.ui.type.list", pluralTypeI18nKey)'/>

     */

    @Override
    public DefaultUIInitializerResult initUI() {

        String pluralTypeI18nKey = I18nDecoratorHelper.getType(ui.getModel().getScope().getMainType());
        String typeI18nKey = I18nDecoratorHelper.getType(ui.getModel().getScope().getMainType());

        ui.getReferentialListHeader().setLabelText(I18n.t("observe.ui.type.list", pluralTypeI18nKey));
        ui.getCreate().setToolTipText(I18n.t("observe.ui.type.action.create", typeI18nKey));
        ui.getDetail().setToolTipText(I18n.t("observe.ui.type.action.view", typeI18nKey));
        ui.getModify().setToolTipText(I18n.t("observe.ui.type.action.edit", typeI18nKey));
        ui.getDelete().setToolTipText(I18n.t("observe.ui.type.action.delete", typeI18nKey));
        ui.getSave().setToolTipText(I18n.t("observe.ui.type.action.save", typeI18nKey));

        return super.initUI();
    }

    @Override
    protected void init(Table editor) {
        super.init(editor);
        if ("editI18nTable2".equals(editor.getName())) {
            addI18n((ContentI18nReferentialUI<?, ?, ?>) ui, editor);
        }
    }

    protected void addI18n(ContentI18nReferentialUI<?, ?, ?> ui, Table editI18nTable) {
        int y = editI18nTable.getComponentCount() / 2;
        editI18nTable.add(ui.getLabel1Label(), new GridBagConstraints(0, y, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(SwingUtil.boxComponentWithJxLayer(ui.getLabel1()), new GridBagConstraints(1, y, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(ui.getLabel2Label(), new GridBagConstraints(2, y, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(SwingUtil.boxComponentWithJxLayer(ui.getLabel2()), new GridBagConstraints(3, y, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(ui.getLabel3Label(), new GridBagConstraints(0, y + 1, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(SwingUtil.boxComponentWithJxLayer(ui.getLabel3()), new GridBagConstraints(1, y + 1, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(ui.getLabel4Label(), new GridBagConstraints(2, y + 1, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(SwingUtil.boxComponentWithJxLayer(ui.getLabel4()), new GridBagConstraints(3, y + 1, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(ui.getLabel5Label(), new GridBagConstraints(0, y + 2, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(SwingUtil.boxComponentWithJxLayer(ui.getLabel5()), new GridBagConstraints(1, y + 2, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(ui.getLabel6Label(), new GridBagConstraints(2, y + 2, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(SwingUtil.boxComponentWithJxLayer(ui.getLabel6()), new GridBagConstraints(3, y + 2, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(ui.getLabel7Label(), new GridBagConstraints(0, y + 3, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(SwingUtil.boxComponentWithJxLayer(ui.getLabel7()), new GridBagConstraints(1, y + 3, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(ui.getLabel8Label(), new GridBagConstraints(2, y + 3, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 3, 3, 3), 0, 0));
        editI18nTable.add(SwingUtil.boxComponentWithJxLayer(ui.getLabel8()), new GridBagConstraints(3, y + 3, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
    }
}
