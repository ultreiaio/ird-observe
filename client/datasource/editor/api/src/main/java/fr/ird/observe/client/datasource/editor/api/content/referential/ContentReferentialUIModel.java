/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.referential;

import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeleteRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ChangeIdRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.id.ShowIdRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveRequest;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;

/**
 * Le modèle pour un écran d'édition du référentiel.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since .14
 */
public abstract class ContentReferentialUIModel<D extends ReferentialDto, R extends ReferentialDtoReference> extends ContentUIModel {

    protected ContentReferentialUIModel(NavigationNode source) {
        super(source);
    }

    @Override
    public ContentReferentialUINavigationNode getSource() {
        return (ContentReferentialUINavigationNode) super.getSource();
    }

    @SuppressWarnings("unchecked")
    @Override
    public ContentReferentialUIModelStates<D, R> getStates() {
        return (ContentReferentialUIModelStates<D, R>) super.getStates();
    }

    public final void openForm(Form<D> form) {
        getReferenceCache().loadReferentialReferenceSetsInModel(form);
        getStates().setForm(form);
        form.getObject().copy(getStates().getBean());
    }

    public final void updateUiWithReferenceSetsFromModel() {
        getReferenceCache().updateUiWithReferenceSetsFromModel(getStates().getForm());
    }

    public final ReferencesCache getReferenceCache() {
        return getStates().getReferenceCache();
    }

    public final ShowIdRequest getShowIdRequest() {
        return new ShowIdRequest(getStates()::getSelectedBeanReference);
    }

    public final ChangeIdRequest getChangeIdRequest() {
        return new ChangeIdRequest(getStates()::getSelectedBeanReference);
    }

    public final SaveRequest<D> toSaveRequest() {
        return SaveRequest.create(getStates().getBean().getId(), () -> getStates().getForm().getObject(), getStates()::getBeanToSave, getStates().savePredicate());
    }

    public final DeleteRequestBuilder.StepBuild toDeleteRequest() {
        return DeleteRequestBuilder.create(getSource().getScope().getMainReferenceType(), getStates().getSelectedBeanReference().toLabel());
    }
}
