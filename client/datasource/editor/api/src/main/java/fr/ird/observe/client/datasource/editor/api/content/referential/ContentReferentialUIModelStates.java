package fr.ird.observe.client.datasource.editor.api.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SavePredicate;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;

import javax.swing.ListModel;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.2
 */
public abstract class ContentReferentialUIModelStates<D extends ReferentialDto, R extends ReferentialDtoReference> extends ContentUIModelStates {
    public static final String PROPERTY_SELECTED_BEAN_REFERENCE = "selectedBeanReference";
    /**
     * Reference cache.
     */
    private final ReferencesCache referenceCache;
    /**
     * Edit bean.
     */
    private final D bean;
    private final Class<D> mainType;
    private final Class<R> mainReferenceType;
    private final ContentReferentialUINavigationNode source;
    private List<R> selection;
    private Form<D> form;
    private ToolkitIdLabel replaceReference;
    private ContentReferentialUI<D, R, ?> ui;

    public ContentReferentialUIModelStates(ContentReferentialUIModel<D, R> model, D bean) {
        source = Objects.requireNonNull(model).getSource();
        this.referenceCache = source.getContext().newReferenceCache();
        this.bean = Objects.requireNonNull(bean);
        this.mainType = model.getScope().getMainType();
        this.mainReferenceType = model.getScope().getMainReferenceType();
        model.getDecoratorService().installDecorator(bean);
    }

    public abstract R toReference(D bean, ReferentialLocale referentialLocale);

    public ContentReferentialUINavigationNode source() {
        return source;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void init(ContentUI ui, DefaultUIInitializerResult initializerResult) {
        this.ui = (ContentReferentialUI<D, R, ?>) ui;
        super.init(ui, initializerResult);
        onAfterInitAddReferentialFilters(getClientConfig(), getObserveSelectModel(), ui.getHandler(), referenceCache);
        referenceCache.registerComponents(initializerResult);
    }

    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
    }

    @Override
    public void open(ContentUIModel model) {
        super.open(model);
        ListModel<R> listModel = ui.getList().getModel();
        if (listModel.getSize() > 0) {
            setSelection(List.of(listModel.getElementAt(0)));
        }
    }

    public D getBeanToSave() {
        D bean = getBean();
        bean.autoTrim();
        return bean;
    }

    public ToolkitIdDtoBean toUsageRequest(String id) {
        return ToolkitIdDtoBean.of(mainType, id);
    }

    public D getBean() {
        return bean;
    }

    public Form<D> getForm() {
        return form;
    }

    public void setForm(Form<D> form) {
        this.form = form;
    }

    public R getSelectedBeanReference() {
        return isSelectionSingle() ? getSelection().get(0) : null;
    }

    public boolean isSelectionEmpty() {
        return getSelection() == null || getSelection().isEmpty();
    }

    public boolean isSelectionSingle() {
        return getSelection() != null && getSelection().size() == 1;
    }

    public boolean isSelectionMultiple() {
        return getSelection() != null && getSelection().size() > 1;
    }

    public List<R> getSelection() {
        return selection;
    }

    public void setSelection(List<R> selection) {
        List<R> oldValue = getSelection();
        R oldSelectionBean = getSelectedBeanReference();
        boolean oldSelectionEmpty = isSelectionEmpty();
        boolean oldSelectionSingle = isSelectionSingle();
        boolean oldSelectionMultiple = isSelectionMultiple();
        this.selection = selection;
        firePropertyChange("selection", oldValue, selection);
        firePropertyChange("selectionEmpty", oldSelectionEmpty, isSelectionEmpty());
        firePropertyChange("selectionSingle", oldSelectionSingle, isSelectionSingle());
        firePropertyChange("selectionMultiple", oldSelectionMultiple, isSelectionMultiple());
        firePropertyChange(PROPERTY_SELECTED_BEAN_REFERENCE, oldSelectionBean, getSelectedBeanReference());
    }

    public ToolkitIdLabel getReplaceReference() {
        return replaceReference;
    }

    public void setReplaceReference(ToolkitIdLabel replaceReference) {
        this.replaceReference = replaceReference;
    }

    @Override
    public void close() {
        super.close();
        referenceCache.destroy();
        form = null;
    }

    public final ReferencesCache getReferenceCache() {
        return referenceCache;
    }

    public final List<R> getReferentialReferences(String... ids) {
        ObserveSwingDataSource mainDataSource = getDataSourcesManager().getMainDataSource();
        LinkedList<R> result = new LinkedList<>(mainDataSource.getReferentialReferences(mainReferenceType, ids));
        getDecoratorService().installDecorator(mainReferenceType, result.stream());
        return result;
    }

    public SavePredicate<D> savePredicate() {
        return new ReferentialSavePredicate<>(this);
    }

    public Class<D> mainType() {
        return mainType;
    }

    public Class<R> mainReferenceType() {
        return mainReferenceType;
    }
}
