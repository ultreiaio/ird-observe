package fr.ird.observe.client.datasource.editor.api.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import io.ultreia.java4all.decoration.Decorator;

import java.util.Comparator;
import java.util.List;

/**
 * Created on 25/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentReferentialUINavigationInitializer extends NavigationInitializer<ContentReferentialUINavigationContext> {
    private final long initialCount;
    private ReferentialDtoReferenceSet<? extends ReferentialDtoReference> references;
    private Decorator decorator;

    public ContentReferentialUINavigationInitializer(NavigationScope scope, long initialCount) {
        super(scope);
        this.initialCount = initialCount;
    }

    @Override
    protected Object init(NavigationContext<ContentReferentialUINavigationContext> context) {
        decorator = context.getDecoratorService().getDecoratorByType(getScope().getMainReferenceType(), getScope().getDecoratorClassifier());
        return getScope().getMainType();
    }

    @Override
    protected void open(NavigationContext<ContentReferentialUINavigationContext> context) {
        references = context.getReferentialReferenceSet(getScope().getMainReferenceType(), decorator);
        references.sort(Comparator.comparing(Object::toString));
    }

    public long getInitialCount() {
        return initialCount;
    }

    @Override
    public String toString() {
        return super.toString() + "" + getScope().getMainType().getName();
    }

    @Override
    public String toPath() {
        return getScope().getNodeDataType().getName();
    }

    @Override
    protected void reload(NavigationContext<ContentReferentialUINavigationContext> context) {
        open(context);
    }

    public List<? extends ReferentialDtoReference> getReferences() {
        return references.toArrayList();
    }
}
