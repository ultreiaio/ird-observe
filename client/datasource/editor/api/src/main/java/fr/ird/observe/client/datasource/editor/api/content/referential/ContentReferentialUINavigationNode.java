package fr.ird.observe.client.datasource.editor.api.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.NullCapability;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import io.ultreia.java4all.lang.Objects2;

import java.util.List;

/**
 * Created on 16/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class ContentReferentialUINavigationNode extends NavigationNode {

    public static <N extends ContentReferentialUINavigationNode> N create(Class<N> nodeType, long initialCount) {
        N node = Objects2.newInstance(nodeType);
        return init(node, initialCount);
    }

    public static <N extends ContentReferentialUINavigationNode> N init(N node, long initialCount) {
        ContentReferentialUINavigationInitializer initializer = new ContentReferentialUINavigationInitializer(node.getScope(), initialCount);
        node.init(initializer);
        return node;
    }

    @Override
    public ContentReferentialUINavigationInitializer getInitializer() {
        return (ContentReferentialUINavigationInitializer) super.getInitializer();
    }

    @Override
    public ContentReferentialUINavigationContext getContext() {
        return (ContentReferentialUINavigationContext) super.getContext();
    }

    @Override
    public ContentReferentialUINavigationHandler<?> getHandler() {
        return (ContentReferentialUINavigationHandler<?>) super.getHandler();
    }

    @Override
    public final NullCapability<?> getCapability() {
        return (NullCapability<?>) super.getCapability();
    }

    public List<? extends ReferentialDtoReference> getReferences() {
        return getInitializer().getReferences();
    }

}
