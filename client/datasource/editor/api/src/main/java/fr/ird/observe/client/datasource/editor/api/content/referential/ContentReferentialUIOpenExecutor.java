package fr.ird.observe.client.datasource.editor.api.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentEditExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenExecutor;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.form.FormDefinition;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.I18nReferentialDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.services.service.referential.ReferentialService;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import java.awt.Font;
import java.util.Optional;

import static fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler.removeAllMessages;

/**
 * Created on 27/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentReferentialUIOpenExecutor<D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> implements ContentOpenExecutor<U>, ContentEditExecutor<U> {

    @Override
    public void loadContent(U ui) {
        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();
        ContentReferentialUIModel<D, R> model = ui.getModel();
        ContentReferentialUIModelStates<D, R> states = model.getStates();

        contentOpen.resetCoordinateEditors();

        //FIXME chemit 20100913 : il vaudrait le faire uniquement lors de l'édition
        // chaque arrive sur un écran invalide le cache de validation
        getClientValidationContext().reset();

        removeAllMessages(ui);

        // open model
        model.open();

        Optional<FormDefinition<D>> formDefinitionOptional = ObserveBusinessProject.get().getOptionalFormDefinition(model.getScope().getMainType());
        if (formDefinitionOptional.isPresent()) {
            FormDefinition<D> formDefinition = formDefinitionOptional.get();
            model.getReferenceCache().loadReferentialReferenceSetsInModel(formDefinition, true);
            model.updateUiWithReferenceSetsFromModel();
        }

        if (I18nReferentialDto.class.isAssignableFrom(model.getScope().getMainType())) {
            // on met en gras le libelle sélectionné en base

            setMainI18nLanguage(ui, model.getClientConfig().getReferentialLocale());
        }
        //FIXME Do this in model.open
        JList<R> list = ui.getList();
        if (list.getModel().getSize() > 0) {
            list.clearSelection();
            list.setSelectedIndex(0);
        }

        JPanel listActions = ui.getListActions();
        if (states.isEditable()) {
            listActions.remove(ui.getDetail());
        } else {
            listActions.remove(ui.getModify());
            listActions.remove(ui.getSave());
        }
    }

    public static void setMainI18nLanguage(JAXXObject ui, ReferentialLocale localeEnum) {
        String libelleName = localeEnum.getLibelle() + "Label";
        for (int i = 1; i <= 8; i++) {
            String lib = "label" + i + "Label";
            JLabel label = (JLabel) ui.getObjectById(lib);
            if (label == null) {
                // not in ui actually
                continue;
            }
            Font font = label.getFont();
            Font normalFont = font.deriveFont(Font.PLAIN);
            Font boldFont = font.deriveFont(Font.BOLD);

            if (libelleName.equals(lib)) {
                // on met en gras le label
                font = boldFont;
            } else {
                // on met en normal le label
                font = normalFont;
            }
            label.setFont(font);
            ((JComponent) ui.getObjectById("label" + i)).setFont(font);
        }
    }

    @Override
    public void onOpened(U ui) {
        ContentReferentialUIHandler<D, R, U> handler = ui.getHandler();
        handler.getContentOpen().prepareCoordinateEditors();
        handler.fixFormSize();
        handler.onEndOpenForCreateMode();
    }

    @Override
    public void startEdit(U ui) {
        ContentReferentialUIModel<D, R> model = ui.getModel();
        ContentReferentialUIModelStates<D, R> states = model.getStates();
        ContentReferentialUIHandler<D, R, U> handler = ui.getHandler();
        ContentOpenWithValidator<U> contentOpen = handler.getContentOpen();
        boolean canEdit = states.isEditable();
        ReferentialService referentialService = handler.getReferentialService();
        Class<D> mainType = model.getScope().getMainType();
        if (canEdit) {

            removeAllMessages(ui);
            states.setEditing(true);
            contentOpen.prepareValidationContext();
            contentOpen.installValidators(states.getBean());

            if (states.isCreatingMode()) {
//                addInfoMessage(t("observe.ui.message.creating.referentiel"));
                Form<D> form = referentialService.preCreate(mainType);
                model.openForm(form);
            } else {
                R selectedValue = states.getSelectedBeanReference();
                Form<D> form = referentialService.loadForm(mainType, selectedValue.getId());
                model.openForm(form);
//                addInfoMessage(t("observe.ui.message.updating.referentiel"));
            }

            // do edit
            if (states.isUpdatingMode()) {
                // nothing has changed just after starting editing
                ui.setValidatorChanged(false);
                states.setModified(false);
            }
        } else {

            // reset all validators
            ui.setValidatorBean(null);

            R selectedValue = states.getSelectedBeanReference();
            Form<D> form = referentialService.loadForm(mainType, selectedValue.getId());
            model.openForm(form);

            // pass in editing mode (without any modification possible)
            states.setEditing(true);
            getClientValidationContext().reset();
        }
    }

    @Override
    public void stopEdit(U ui) {
        ClientValidationContext context = getClientValidationContext();
        ContentReferentialUIModel<D, R> model = ui.getModel();
        ContentReferentialUIModelStates<D, R> states = model.getStates();
        ContentOpenWithValidator<U> contentOpen = ui.getHandler().getContentOpen();

        context.reset();
        ui.setValidatorChanged(false);

        // mark ui as not editing
        states.setEditing(false);

        // mark ui as valid while not editing
        states.setValid(true);

        // mark ui as not modified
        states.setModified(false);

        // detach all validators
        contentOpen.uninstallValidators();

        if (!states.isReadingMode()) {
            // on retourne en mode mise a jour sur la liste
            states.setMode(ContentMode.UPDATE);
            removeAllMessages(ui);
//            addInfoMessage(t("observe.ui.message.referentiel.editable"));
        }
        model.updateUiWithReferenceSetsFromModel();
    }
}
