package fr.ird.observe.client.datasource.editor.api.content.referential;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeProducer;
import fr.ird.observe.client.datasource.editor.api.content.actions.mode.ChangeModeRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpen;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.OpenReferentialType;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.util.UIHelper;
import io.ultreia.java4all.i18n.I18n;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 9/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class ReferentialHomeUIHandler extends ContentUIHandler<ReferentialHomeUI> implements UIHandler<ReferentialHomeUI> {

    @Override
    public void onInit(ReferentialHomeUI ui) {
        super.onInit(ui);

        JPanel panel = ui.getContentBody();
        panel.setBackground(Color.WHITE);
        NavigationNode referentialNode = ui.getModel().getSource();
        int childCount = referentialNode.getChildCount();
        boolean makeTwoRowLayout = childCount > 3;

        Enumeration<?> children = referentialNode.children();
        if (makeTwoRowLayout) {
            JPanel panelLeft = new JPanel(new GridLayout(0, 1));
            JPanel panelRight = new JPanel(new GridLayout(0, 1));
            panel.setLayout(new GridLayout(0, 2));
            panelLeft.setBackground(Color.WHITE);
            panelRight.setBackground(Color.WHITE);
            panel.add(panelLeft);
            panel.add(panelRight);
            int mid = childCount / 2;
            boolean evenCount = childCount % 2 == 1;
            if (evenCount) {
                mid++;
            }
            int index = 0;
            while (children.hasMoreElements()) {
                ContentReferentialUINavigationNode child = (ContentReferentialUINavigationNode) children.nextElement();
                if (index < mid) {
                    OpenReferentialType.install(ui, panelLeft, index++, child);
                } else {
                    OpenReferentialType.install(ui, panelRight, index++, child);
                }
            }
            if (evenCount) {
                panelRight.add(new JLabel());
            }
        } else {
            int index = 0;
            while (children.hasMoreElements()) {
                ContentReferentialUINavigationNode child = (ContentReferentialUINavigationNode) children.nextElement();
                OpenReferentialType.install(ui, panel, index++, child);
            }
        }
    }

    @Override
    public List<AbstractButton> getNavigationPopupActions() {
        List<AbstractButton> result = new LinkedList<>();
        collectActions(ui.getContentBody(), result);
        return result;
    }

    @Override
    protected ContentOpen<ReferentialHomeUI> createContentOpen(ReferentialHomeUI ui) {
        return new ContentOpen<>(ui, new ReferentialHomeUIOpenExecutor<>(), null);
    }

    @Override
    public void installChangeModeAction() {
        ChangeModeRequest request = new ChangeModeRequest(ui.getModel().getSource().getScope().getI18nTranslation("title"));
        ChangeModeProducer<ReferentialHomeUI> changeModeProducer = new ChangeModeProducer<>(ui, request) {
            @Override
            protected ContentMode rebuildChangeMode(ContentMode newValue) {
                return ui.getModel().getStates().isEditable() ? newValue : null;
            }
        };
        ChangeMode<ReferentialHomeUI> changeMode = new ChangeMode<>(request, changeModeProducer, null) {
            protected void rebuildTitle(ReferentialHomeUI ui, ContentMode newValue) {
                ContentUIModelStates states = ui.getModel().getStates();
                String titleSuffix;
                if (newValue == null) {
                    titleSuffix = I18n.t("observe.Common.action.notEditable");
                } else {
                    titleSuffix = I18n.t("observe.Common.action.editable");
                }
                String contentTitle = states.getContentTitle() + " - " + titleSuffix;
                ui.setTitle(contentTitle);
            }

            @Override
            public void rebuildEditableZone(ReferentialHomeUI ui) {
                ui.getActions().setVisible(false);
                ui.getToggleInsert().setVisible(true);
                super.rebuildEditableZone(ui);
            }

            @Override
            protected void rebuildNotEditableZone(ReferentialHomeUI ui) {
                ui.getActions().setVisible(false);
                ui.getToggleInsert().setVisible(false);
                super.rebuildNotEditableZone(ui);
            }

            protected void rebuildAction(ReferentialHomeUI ui, ContentMode newValue) {
                String tip;
                Icon icon;
                if (newValue == null) {
                    tip = I18n.n("observe.data.Openable.action.notEditable.tip");
                    icon = UIHelper.getContentActionIcon("not-editable");
                } else {
                    tip = I18n.n("observe.data.Openable.action.editable.tip");
                    icon = UIHelper.getContentActionIcon("unlocked");
                }

                setEnabled(false);
                setIcon(icon);
                KeyStroke acceleratorKey = getAcceleratorKey();
                setKeyStroke(null);
                updateToolTipText(tip);
                rebuildTexts(true);
                setKeyStroke(acceleratorKey);
            }

        };
        ChangeMode.installAction(ui, changeMode);
    }

}
