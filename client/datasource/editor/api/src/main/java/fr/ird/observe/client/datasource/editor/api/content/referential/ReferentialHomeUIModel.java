package fr.ird.observe.client.datasource.editor.api.content.referential;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;

/**
 * Created on 9/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public abstract class ReferentialHomeUIModel extends ContentUIModel {

    protected ReferentialHomeUIModel(NavigationNode source) {
        super(source);
    }

    @Override
    public ReferentialHomeUINavigationNode getSource() {
        return (ReferentialHomeUINavigationNode) super.getSource();
    }

    @Override
    public ReferentialHomeUIModelStates getStates() {
        return (ReferentialHomeUIModelStates) super.getStates();
    }
}
