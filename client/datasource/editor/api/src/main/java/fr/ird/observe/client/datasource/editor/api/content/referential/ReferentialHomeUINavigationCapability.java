package fr.ird.observe.client.datasource.editor.api.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ContainerCapability;
import fr.ird.observe.dto.referential.ReferentialDto;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 24/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ReferentialHomeUINavigationCapability<N extends ReferentialHomeUINavigationNode> implements ContainerCapability<N> {

    private final N node;

    public ReferentialHomeUINavigationCapability(N node) {
        this.node = Objects.requireNonNull(node);
    }

    @Override
    public N getNode() {
        return node;
    }

    @Override
    public List<Class<? extends NavigationNode>> getAcceptedNodeTypes() {
        Map<Class<? extends ReferentialDto>, Class<? extends ContentReferentialUINavigationNode>> nodeTypes = getNode().getInitializer().getNodeTypes();
        return new LinkedList<>(nodeTypes.values());
    }

    @Override
    public void buildChildren() {
        N node = getNode();
        List<Class<? extends ContentReferentialUINavigationNode>> sortedTypeList = node.getInitializer().getNodeTypesList(node.getContext().getDecoratorService().getReferentialLocale().getLocale());
        Map<Class<? extends ReferentialDto>, Long> referentialCountMap = node.getInitializer().getReferentialCountMap();
        Map<Class<? extends ReferentialDto>, Class<? extends ContentReferentialUINavigationNode>> nodeTypes = node.getInitializer().getNodeTypes();
        Map<Class<? extends ContentReferentialUINavigationNode>, Long> nodesCount = new LinkedHashMap<>();
        for (Map.Entry<Class<? extends ReferentialDto>, Long> entry : referentialCountMap.entrySet()) {
            Class<? extends ReferentialDto> dtoType = entry.getKey();
            Long count = entry.getValue();
            Class<? extends ContentReferentialUINavigationNode> nodeType = nodeTypes.get(dtoType);
            if (nodeType != null) {
                nodesCount.put(nodeType, count);
            }
        }
        for (Class<? extends ContentReferentialUINavigationNode> dtoClass : sortedTypeList) {
            long initialCount = nodesCount.get(dtoClass);
            ContentReferentialUINavigationNode childNode = ContentReferentialUINavigationNode.create(dtoClass, initialCount);
            node.add(childNode);
        }
    }

}
