package fr.ird.observe.client.datasource.editor.api.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 25/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ReferentialHomeUINavigationInitializer extends NavigationInitializer<ReferentialHomeUINavigationContext> {

    private final Map<Class<? extends ReferentialDto>, Long> referentialCountMap;
    private SingletonSupplier<List<Class<? extends ReferentialDto>>> types;
    private SingletonSupplier<Map<Class<? extends ReferentialDto>, Class<? extends ContentReferentialUINavigationNode>>> nodeTypes;

    public ReferentialHomeUINavigationInitializer(NavigationScope scope, Map<Class<? extends ReferentialDto>, Long> referentialCountMap) {
        super(scope);
        this.referentialCountMap = Objects.requireNonNull(referentialCountMap);
    }

    @Override
    protected Object init(NavigationContext<ReferentialHomeUINavigationContext> context) {
        this.types = SingletonSupplier.of(() -> getScope().computeTypes(context.getClientConfig().getLocale(), getSubModule()));
        this.nodeTypes = SingletonSupplier.of(getScope()::computeReferentialNodeTypes);
        return getPackageTitle();
    }

    @Override
    protected void open(NavigationContext<ReferentialHomeUINavigationContext> context) {
    }

    @Override
    protected void reload(NavigationContext<ReferentialHomeUINavigationContext> context) {
    }

    public final List<Class<? extends ReferentialDto>> getTypes() {
        return types.get();
    }

    public final Map<Class<? extends ReferentialDto>, Class<? extends ContentReferentialUINavigationNode>> getNodeTypes() {
        return nodeTypes.get();
    }

    public final List<Class<? extends ContentReferentialUINavigationNode>> getNodeTypesList(Locale referentialLocale) {
        Map<Class<? extends ReferentialDto>, Class<? extends ContentReferentialUINavigationNode>> nodeTypes = getNodeTypes();
        return ObserveUtil.sortTypes(nodeTypes.keySet(), I18nDecoratorHelper::getType, referentialLocale).stream().map(nodeTypes::get).collect(Collectors.toList());
    }

    public Map<Class<? extends ReferentialDto>, Long> getReferentialCountMap() {
        return referentialCountMap;
    }

    @Override
    public String toString() {
        return super.toString() + " - " + getPackageTitle();
    }

    @Override
    public String toPath() {
        return getScope().getSubModuleType().getName();
    }

    public String getPackageTitle() {
        return getBusinessProject().getReferentialPackageTitle(getModule(), getSubModule());
    }
}
