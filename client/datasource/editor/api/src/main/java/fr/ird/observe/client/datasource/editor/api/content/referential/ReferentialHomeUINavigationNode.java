package fr.ird.observe.client.datasource.editor.api.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.lang.Objects2;

import java.util.Map;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class ReferentialHomeUINavigationNode extends NavigationNode {

    public static <N extends ReferentialHomeUINavigationNode> N create(Class<N> nodeType, Map<Class<? extends ReferentialDto>, Long> referentialCountMap) {
        N node = Objects2.newInstance(nodeType);
        return init(node, referentialCountMap);
    }

    public static <N extends ReferentialHomeUINavigationNode> N init(N node, Map<Class<? extends ReferentialDto>, Long> referentialCountMap) {
        ReferentialHomeUINavigationInitializer initializer = new ReferentialHomeUINavigationInitializer(node.getScope(), referentialCountMap);
        node.init(initializer);
        return node;
    }

    @Override
    public ReferentialHomeUINavigationInitializer getInitializer() {
        return (ReferentialHomeUINavigationInitializer) super.getInitializer();
    }

    @Override
    protected ReferentialHomeUINavigationCapability<?> createCapability() {
        return new ReferentialHomeUINavigationCapability<>(this);
    }

    @Override
    public ReferentialHomeUINavigationContext getContext() {
        return (ReferentialHomeUINavigationContext) super.getContext();
    }

    @Override
    public ReferentialHomeUINavigationHandler<?> getHandler() {
        return (ReferentialHomeUINavigationHandler<?>) super.getHandler();
    }

    @Override
    public ReferentialHomeUINavigationCapability<?> getCapability() {
        return (ReferentialHomeUINavigationCapability<?>) super.getCapability();
    }
}
