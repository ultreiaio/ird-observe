package fr.ird.observe.client.datasource.editor.api.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.save.SavePredicateSupport;
import fr.ird.observe.client.datasource.editor.api.content.referential.usage.UsageForDesactivateUIHandler;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.UsageService;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 23/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ReferentialSavePredicate<D extends ReferentialDto, R extends ReferentialDtoReference, S extends ContentReferentialUIModelStates<D, R>> extends SavePredicateSupport<S,D> {

    public ReferentialSavePredicate(S states) {
        super(states);
    }

    @Override
    protected boolean testForValid(D oldBean, D bean) {
        String id = bean.getId();
        if (id == null) {
            return true;
        }

        states.setReplaceReference(null);

        if (oldBean != null && oldBean.getStatus() == ReferenceStatus.enabled && bean.getStatus() == ReferenceStatus.disabled) {

            // referential was disabled
            // find out if there is some usages, if so ask user what to do

            UsageService usageService = states.getReferenceCache().getDataSource().getUsageService();
            ToolkitIdDtoBean request = states.toUsageRequest(bean.getId());
            UsageCount usages = usageService.countReferential(request);

            if (!usages.isEmpty()) {
                // some usages were found

                // get replacements
                List<R> referentialReferences = states.getReferentialReferences();
                List<R> referenceList = referentialReferences
                        .stream()
                        .filter(ReferentialDtoReference::isEnabled)
                        .filter(r -> !bean.getId().equals(r.getId()))
                        .collect(Collectors.toList());

                ToolkitIdLabel dtoLabel = bean.toLabel();
                states.getDecoratorService().installToolkitIdLabelDecorator(bean.getClass(), dtoLabel);
                Pair<Boolean, ToolkitIdLabel> result = UsageForDesactivateUIHandler.showUsages(usageService, dtoLabel, request, usages, referenceList.stream().map(ReferentialDtoReference::toLabel).collect(Collectors.toList()));
                boolean willSave = result.getLeft();
                if (!willSave) {
                    return false;
                }
                states.setReplaceReference(result.getRight());
            }
        }
        return true;
    }
}
