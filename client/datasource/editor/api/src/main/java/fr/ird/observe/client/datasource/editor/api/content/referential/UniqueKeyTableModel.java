/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.referential;

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.dto.I18nDecoratorHelper;

import javax.swing.table.AbstractTableModel;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Un modèle de tableau pour afficher les clef metier des objets du
 * référentiel.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public class UniqueKeyTableModel<E> extends AbstractTableModel implements WithClientUIContextApi {

    private static final long serialVersionUID = 1L;
    protected final String[] columns;
    protected final List<Object[]> datas;
    private final Class<E> beanType;

    public UniqueKeyTableModel(Class<E> beanType, String[] columns, List<Object[]> datas) {
        this.beanType = beanType;
        this.columns = columns;
        this.datas = datas;
    }

    @Override
    public int getRowCount() {
        return datas.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return datas.get(rowIndex)[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        // tableau non editable
        return false;
    }

    @Override
    public String getColumnName(int column) {
        String property = columns[column];
        return t(I18nDecoratorHelper.getPropertyI18nKey(beanType, property));
    }
}
