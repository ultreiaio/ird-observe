package fr.ird.observe.client.datasource.editor.api.content.referential.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpen;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUI;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 10/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public final class BackToListReferential<D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> extends ContentReferentialUIActionSupport<D, R, U> {

    public static <D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> void installAction(U ui) {
        BackToListReferential<D, R, U> action = new BackToListReferential<>(ui.getModel().getScope().getMainType());
        init(ui, ui.getBackToList(), action);
    }

    public static <E extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<E, R, U>> void backToList(ContentReferentialUI<E, R, U> ui) {
        R selectedValue = ui.getList().getSelectedValue();
        ContentOpen<U> contentOpen = ui.getHandler().getContentOpen();
        ContentOpen.Close close = contentOpen.tryToClose();
        if (close == ContentOpen.Close.CANCEL) {
            return;
        }if (close == ContentOpen.Close.CLOSE) {
            // special case, in this form we always need to stop editing (editing does not means editable...)
            //FIXME Review the form to stop using the editing property for this
            close = ContentOpen.Close.RESET;
        }
        boolean canQuit = contentOpen.applyBeforeSave(close);
        if (!canQuit) {
            return;
        }
        ui.getList().setSelectedValue(selectedValue, true);
    }

    public BackToListReferential(Class<D> dataType) {
        super(dataType, t("observe.referential.Referential.action.back.to.list"), t("observe.referential.Referential.action.back.to.list.tip"), "go-back", ObserveKeyStrokesEditorApi.KEY_STROKE_BACK_TO_REFERENTIAL_LIST);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, U ui) {
        backToList(ui);
    }

}
