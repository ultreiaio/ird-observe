package fr.ird.observe.client.datasource.editor.api.content.referential.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.content.actions.InsertMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModel;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.util.function.Consumer;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 10/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public final class CreateReferential<D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> extends ContentReferentialUIActionSupport<D, R, U> implements InsertMenuAction<U> {
    private static final Logger log = LogManager.getLogger(CreateReferential.class);

    private final Consumer<U> consumer;

    public static class DefaultConsumer<D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> implements Consumer<U> {

        @Override
        public void accept(U ui) {
            ContentReferentialUIModel<D, R> model = ui.getModel();
            log.info(String.format("%sWill create new entity [%s]", model.getPrefix(), model.getScope().getMainType()));
            model.getStates().setMode(ContentMode.CREATE);
            model.getStates().getBean().clear();
            ui.startEdit();

            ui.getDetailRealActions().remove(ui.getReset());
        }
    }

    public static <D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> void installAction(U ui) {
        installAction(ui, new DefaultConsumer<>());
    }

    public static <D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> void installAction(U ui, Consumer<U> consumer) {
        CreateReferential<D, R, U> action = new CreateReferential<>(ui.getModel().getScope().getMainType(), consumer);
        init(ui, ui.getCreate(), action);
    }

    public CreateReferential(Class<D> dataType, Consumer<U> consumer) {
        super(dataType, t("observe.referential.Referential.action.create"), "", "add", null);
        this.consumer = consumer;
    }

    @Override
    protected void doActionPerformed(ActionEvent event, U ui) {
        consumer.accept(ui);
    }
}
