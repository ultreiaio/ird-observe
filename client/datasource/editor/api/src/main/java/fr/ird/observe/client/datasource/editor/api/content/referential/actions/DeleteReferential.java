/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.content.referential.actions;

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.actions.DeleteEdit;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModel;
import fr.ird.observe.client.datasource.editor.api.content.referential.usage.UsageForDeleteUIHandler;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.UsageService;
import fr.ird.observe.services.service.referential.ReferentialService;
import io.ultreia.java4all.i18n.I18n;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Action pour sélectionner un nœud (attaché à l'éditeur) dans l'arbre de navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public final class DeleteReferential<D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> extends ContentReferentialUIActionSupport<D, R, U> implements ConfigureMenuAction<U> {

    private static final Logger log = LogManager.getLogger(DeleteReferential.class);

    private ToolkitIdLabel replaceReference;

    public DeleteReferential(Class<D> dataType) {
        super(dataType, n("observe.Common.action.delete"), I18n.t("observe.referential.Referential.action.delete.tip", I18nDecoratorHelper.getType(dataType)), "content-delete-16", ObserveKeyStrokesEditorApi.KEY_STROKE_DELETE_DATA_GLOBAL);
    }

    public static <D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> void installAction(U ui) {
        DeleteReferential<D, R, U> action = new DeleteReferential<>(ui.getModel().getScope().getMainType());
        init(ui, ui.getDelete(), action);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, U ui) {
        ContentReferentialUIModel<D, R> model = ui.getModel();
        List<R> selection = model.getStates().getSelection();
        Set<R> deleted = new LinkedHashSet<>(selection.size());
        Class<D> beanType = ui.getModel().getScope().getMainType();
        Class<R> referenceType = model.getScope().getMainReferenceType();
        ReferentialService referentialService = getServicesProvider().getReferentialService();
        UsageService usageService = getServicesProvider().getUsageService();
        for (R selectedBeanReference : selection) {
            boolean canDelete = askToDelete(ui, model, beanType, referenceType, usageService, selectedBeanReference);
            if (canDelete) {
                doDelete(beanType, referentialService, selectedBeanReference, replaceReference);
                deleted.add(selectedBeanReference);
            }
        }
        afterDelete(ui, deleted);
    }

    private boolean askToDelete(U ui, ContentReferentialUIModel<D, R> model, Class<D> beanType, Class<R> referenceType, UsageService usageService, R bean) {

        // recherche des utilisation du bean dans la base
        ToolkitIdDtoBean request = model.getStates().toUsageRequest(bean.getId());
        UsageCount usages = usageService.countReferential(request);

        replaceReference = null;

        if (!usages.isEmpty()) {

            // some usages were found

            // get replacements
            List<R> referentialReferences = ui.getHandler().getReferentialReferences(referenceType);
            List<R> referenceList = referentialReferences
                    .stream()
                    .filter(ReferentialDtoReference::isEnabled)
                    .filter(r -> !bean.getId().equals(r.getId()))
                    .collect(Collectors.toList());

            ToolkitIdLabel dtoToDeleteLabel = bean.toLabel();
            getDecoratorService().installToolkitIdLabelDecorator(beanType, dtoToDeleteLabel);
            Pair<Boolean, ToolkitIdLabel> result = UsageForDeleteUIHandler.showUsages(usageService, dtoToDeleteLabel, request, usages, referenceList.stream().map(ReferentialDtoReference::toLabel).collect(Collectors.toList()));
            Boolean canContinue = result.getLeft();
            if (!canContinue) {
                log.debug("user refuse to continue");
                return false;
            }

            replaceReference = result.getRight();
            if (replaceReference == null) {
                log.debug("user did not select replace");
                return false;
            }
        }
        return DeleteEdit.confirmForEntityDelete(ui, bean);
    }

    private void doDelete(Class<D> beanType, ReferentialService referentialService, ReferentialDtoReference bean, ToolkitIdLabel replaceReference) {

        String id = bean.getId();
        if (replaceReference != null) {
            String replaceId = replaceReference.getId();
            log.info(String.format("Do replace reference before delete (%s → %s)", id, replaceId));
            referentialService.replaceReference(beanType, id, replaceId);
        }
        referentialService.delete(beanType, id);
    }

    private void afterDelete(U ui, Set<R> deleted) {
        ui.getModel().getStates().setSelection(null);
        ui.stopEdit();

        DataSourceEditor dataSourceEditor = getDataSourceEditor();
        NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();
        boolean needUpdateNavigationResult = isNeedUpdateNavigationResult(tree.getRootNode(), deleted);
        if (needUpdateNavigationResult) {
            // update navigation result
            tree.getModel().updateNavigationResult();
        }
        NavigationNode selectedNode = tree.getSelectedNode();
        selectedNode.reloadNodeData();
        tree.reSelectSafeNodeThen(selectedNode, () -> dataSourceEditor.getModel().resetFromPreviousUi(ui));
    }

    private boolean isNeedUpdateNavigationResult(RootNavigationNode rootNode, Set<R> deleted) {
        boolean needUpdateNavigationResult = false;
        if (rootNode.getInitializer().getRequest().isLoadData()) {
            for (R r : deleted) {
                String id = r.getId();
                NavigationNode groupByNode = rootNode.findChildById(id);
                if (groupByNode != null) {
                    // remove node from tree
                    rootNode.remove(groupByNode);
                    needUpdateNavigationResult = true;
                }
            }
        }
        return needUpdateNavigationResult;
    }
}
