package fr.ird.observe.client.datasource.editor.api.content.referential.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUI;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * To open referential type node from Referential Home.
 * Created on 04/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class OpenReferentialType<U extends ReferentialHomeUI> extends ReferentialHomeUIActionSupport<U> {

    private final ContentReferentialUINavigationNode child;

    public static <U extends ReferentialHomeUI> OpenReferentialType<U> install(U ui, JPanel panel, int index, ContentReferentialUINavigationNode child) {
        KeyStroke keyStroke = KeyStroke.getKeyStroke("pressed " + (char) ('A' + index));
        if (index > 25) {
            keyStroke = KeyStroke.getKeyStroke("shift pressed " + (char) ('A' + index - 26));
        }
        String commandName = "OpenReferentialType-" + index;
        OpenReferentialType<U> action = new OpenReferentialType<>(commandName, child.getHandler().getText(), child, keyStroke);
        JButton editor = new JButton();
        editor.setHorizontalAlignment(JButton.LEFT);
        editor.setForeground(child.getColor());
        panel.add(editor);
        init(ui, editor, action);
        return action;
    }

    OpenReferentialType(String actionName, String label, ContentReferentialUINavigationNode child, KeyStroke keyStroke) {
        super(actionName, label, t("observe.referential.Referential.action.openType"), "go-detail", keyStroke);
        this.child = child;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U u) {
        NavigationTree navigation = getDataSourceEditor().getNavigationUI().getTree();
        navigation.selectSafeNode(child);
    }
}
