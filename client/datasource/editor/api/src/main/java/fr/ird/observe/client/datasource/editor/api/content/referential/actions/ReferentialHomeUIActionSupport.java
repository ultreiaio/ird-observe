package fr.ird.observe.client.datasource.editor.api.content.referential.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUI;

import javax.swing.KeyStroke;

/**
 * Created on 04/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ReferentialHomeUIActionSupport<U extends ReferentialHomeUI> extends ContentUIActionSupport<U> {

    public ReferentialHomeUIActionSupport(String label, String shortDescription, String actionIcon, KeyStroke keyStroke) {
        super(label, shortDescription, actionIcon, keyStroke);
    }

    public ReferentialHomeUIActionSupport(String actionCommandKey, String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(actionCommandKey, label, shortDescription, actionIcon, acceleratorKey);
    }
}
