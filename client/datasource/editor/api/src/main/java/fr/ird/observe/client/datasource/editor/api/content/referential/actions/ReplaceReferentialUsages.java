package fr.ird.observe.client.datasource.editor.api.content.referential.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModel;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.referential.usage.UsageForReplaceUIHandler;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.UsageService;
import io.ultreia.java4all.i18n.I18n;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * To replace referential usages by another one.
 * Created on 20/05/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.4
 */
public class ReplaceReferentialUsages<D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> extends ContentReferentialUIActionSupport<D, R, U> implements ConfigureMenuAction<U> {

    private static final Logger log = LogManager.getLogger(ReplaceReferentialUsages.class);

    private ToolkitIdLabel replaceReference;

    public ReplaceReferentialUsages(Class<D> dataType) {
        super(dataType, n("observe.referential.Referential.action.replaceUsages"), I18n.t("observe.referential.Referential.action.replaceUsages.tip", I18nDecoratorHelper.getType(dataType)), "move", ObserveKeyStrokesEditorApi.KEY_STROKE_REPLACE_REFERENTIAL);
    }

    public static <D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> void installAction(U ui) {
        ReplaceReferentialUsages<D, R, U> action = new ReplaceReferentialUsages<>(ui.getModel().getScope().getMainType());
        init(ui, ui.getReplaceUsages(), action);
        ui.getModel().getStates().addPropertyChangeListener(ContentReferentialUIModelStates.PROPERTY_SELECTED_BEAN_REFERENCE, evt -> {
            ReferentialDtoReference newValue = (ReferentialDtoReference) evt.getNewValue();
            ObserveSwingDataSource mainDataSource = ui.getModel().getDataSourcesManager().getMainDataSource();
            action.setEnabled(newValue != null && (mainDataSource.isLocal() || mainDataSource.canWriteData()));
        });
    }

    @Override
    protected void doActionPerformed(ActionEvent event, U ui) {
        ContentReferentialUIModel<D, R> model = ui.getModel();
        R selectedBeanReference = model.getStates().getSelectedBeanReference();
        askToReplace(ui, selectedBeanReference);
        if (replaceReference != null) {
            doReplace(ui, selectedBeanReference, replaceReference);
        }
    }

    private void askToReplace(U ui, R bean) {

        ContentReferentialUIModel<D, R> model = ui.getModel();

        // recherche des utilisation du bean dans la base
        UsageService usageService = getServicesProvider().getUsageService();
        ToolkitIdDtoBean request = model.getStates().toUsageRequest(bean.getId());
        UsageCount usages = usageService.countReferentialInData(request);
        replaceReference = null;

        Class<D> beanType = model.getScope().getMainType();
        Class<R> referenceType = model.getScope().getMainReferenceType();
        if (usages.isEmpty()) {
            JOptionPane.showMessageDialog(getMainUI(), I18n.t("observe.referential.Referential.action.replaceUsages.no.data.usage", bean));
            return;
        }

        // some usages were found

        // get replacements
        List<R> referentialReferences = ui.getHandler().getReferentialReferences(referenceType);
        List<R> referenceList = referentialReferences
                .stream()
                .filter(ReferentialDtoReference::isEnabled)
                .filter(r -> !bean.getId().equals(r.getId()))
                .collect(Collectors.toList());

        ToolkitIdLabel dtoToReplaceLabel = bean.toLabel();
        getDecoratorService().installToolkitIdLabelDecorator(beanType, dtoToReplaceLabel);
        Pair<Boolean, ToolkitIdLabel> result = UsageForReplaceUIHandler.showUsages(usageService, dtoToReplaceLabel, request, usages, referenceList.stream().map(ReferentialDtoReference::toLabel).collect(Collectors.toList()));
        Boolean canContinue = result.getLeft();
        if (!canContinue) {
            log.debug("user refuse to continue");
            return;
        }

        replaceReference = result.getRight();
        if (replaceReference == null) {
            log.debug("user did not select replace");
        }
    }

    private void doReplace(U ui, ReferentialDtoReference bean, ToolkitIdLabel replaceReference) {

        String id = bean.getId();
        Class<D> beanType = ui.getModel().getScope().getMainType();
        if (replaceReference == null) {
            return;
        }
        String replaceId = replaceReference.getId();
        log.info(String.format("Do replace reference (%s → %s)", id, replaceId));
        getServicesProvider().getReferentialService().replaceReferenceInData(beanType, id, replaceId);
    }

}
