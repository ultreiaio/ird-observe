package fr.ird.observe.client.datasource.editor.api.content.referential.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveUIAdapter;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModelStates;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;

import java.util.List;

/**
 * Created on 19/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class SaveContentReferentialUIAdapter<D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> implements SaveUIAdapter<D, U> {

    @Override
    public void adaptUi(DataSourceEditor dataSourceEditor, U ui, boolean notPersisted, D bean) {
        ui.stopEdit();
        NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();
        NavigationNode selectedNode = tree.getSelectedNode();
        if (notPersisted) {
            selectedNode.reloadNodeData();
        }

        ContentReferentialUIModelStates<D, R> states = ui.getModel().getStates();
        R reference = states.toReference(bean, dataSourceEditor.getConfig().getReferentialLocale());
        states.setSelection(List.of(reference));

        updateNavigationTreeStructure(notPersisted, bean, tree, states);
        tree.reSelectSafeNodeThen(selectedNode, () -> dataSourceEditor.getModel().resetFromPreviousUi(ui));
    }

    private void updateNavigationTreeStructure(boolean notPersisted, D bean, NavigationTree tree, ContentReferentialUIModelStates<D, R> states) {
        RootNavigationNode rootNode = tree.getRootNode();
        RootNavigationInitializer initializer = rootNode.getInitializer();
        ToolkitTreeFlatModelRootRequest request = initializer.getRequest();
        if (!request.isLoadData()) {
            // no data loading, no update to perform
            return;
        }
        Class<?> groupByType = initializer.getGroupBy().getPropertyType();
        if (!groupByType.equals(states.mainReferenceType())) {
            // the groupBy type is not the navigation configuration one (no update to perform)
            return;
        }
        String groupById = bean.getId();
        if (notPersisted) {
            if (!request.isLoadEmptyGroupBy()) {
                // navigation configuration won't add this node
                return;
            }
            if (!bean.isEnabled() && !request.isLoadDisabledGroupBy()) {
                // navigation configuration won't add this node
                return;
            }
            // add the new groupBy node
            rootNode.getCapability().createMissingNode(groupById);
        } else {
            // update the (optional) groupBy node
            rootNode.getCapability().updateChildNode(groupById);
        }
    }
}
