package fr.ird.observe.client.datasource.editor.api.content.referential.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModel;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModelStates;
import fr.ird.observe.client.datasource.usage.UsageForDisplayUI;
import fr.ird.observe.client.datasource.usage.UsagesGetter;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.UsageCountWithLabel;
import fr.ird.observe.services.service.UsageService;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.SingletonSupplier;

import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.util.Collection;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 27/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public final class ShowUsagesReferential<D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> extends ContentReferentialUIActionSupport<D, R, U> implements ConfigureMenuAction<U> {

    public static <D extends ReferentialDto, R extends ReferentialDtoReference, U extends ContentReferentialUI<D, R, U>> void installAction(U ui) {
        ShowUsagesReferential<D, R, U> action = new ShowUsagesReferential<>(ui.getModel().getScope().getMainType());
        init(ui, ui.getShowUsages(), action);
        ui.getModel().getStates().addPropertyChangeListener(ContentReferentialUIModelStates.PROPERTY_SELECTED_BEAN_REFERENCE, evt -> {
            ReferentialDtoReference newValue = (ReferentialDtoReference) evt.getNewValue();
            ObserveSwingDataSource mainDataSource = ui.getModel().getDataSourcesManager().getMainDataSource();
            action.setEnabled(newValue != null && (mainDataSource.isLocal() || mainDataSource.canReadAll()));
        });
    }

    public ShowUsagesReferential(Class<D> dataType) {
        super(dataType, t("observe.referential.Referential.action.show.usages.tip"), t("observe.referential.Referential.action.show.usages.tip"), "show-usages", ObserveKeyStrokesEditorApi.KEY_STROKE_SHOW_USAGES);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, U ui) {

        ContentReferentialUIModel<D, R> model = ui.getModel();
        R bean = model.getStates().getSelectedBeanReference();

        // recherche des utilisation du bean dans la base
        UsageService usageService = getServicesProvider().getUsageService();
        ToolkitIdDtoBean request = model.getStates().toUsageRequest(bean.getId());
        UsageCount usages = usageService.countReferential(request);
        DecoratorService decoratorService = getDecoratorService();
        ToolkitIdLabel reference = bean.toLabel();
        decoratorService.installToolkitIdLabelDecorator(request.getType(), reference);
        showReferentialUsages(usageService, reference, request, usages);
    }

    void showReferentialUsages(UsageService usageService,
                               ToolkitIdLabel reference,
                               ToolkitIdDtoBean request,
                               UsageCount usages) {
        ObserveBusinessProject businessProject = ObserveBusinessProject.get();
        UsageCountWithLabel realUsages = new UsageCountWithLabel(I18n.getDefaultLocale(), businessProject, usages);
        String type = businessProject.getLongTitle(request.getType());

        String message = t("observe.ui.message.show.referential.usages", type, reference);
        UsagesGetter getter = new UsagesGetter() {
            @Override
            public UsageCountWithLabel getCount() {
                return realUsages;
            }

            @Override
            public <D extends BusinessDto> SingletonSupplier<Collection<ToolkitIdLabel>> getUsages(Class<D> dtoType) {
                return SingletonSupplier.of(() -> usageService.findReferential(request, dtoType));
            }
        };
        UsageForDisplayUI usagesUI = UsageForDisplayUI.build(message, getter);

        Object[] options = {t("observe.ui.choice.quit")};
        JOptionPane pane = new JOptionPane(usagesUI, JOptionPane.INFORMATION_MESSAGE,
                                           JOptionPane.DEFAULT_OPTION, null,
                                           options, options[0]);

        usagesUI.getHandler().setUISize(pane);
        askToUser(pane, t("observe.ui.title.show.referential.usage"), options);
    }

}

