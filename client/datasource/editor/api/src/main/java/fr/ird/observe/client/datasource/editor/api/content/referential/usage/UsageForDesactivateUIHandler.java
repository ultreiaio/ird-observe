package fr.ird.observe.client.datasource.editor.api.content.referential.usage;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.usage.UsagePanel;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;
import fr.ird.observe.client.datasource.usage.UsagesGetter;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.UsageCountWithLabel;
import fr.ird.observe.services.service.UsageService;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxModel;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JOptionPane;
import java.util.Collection;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 16/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1
 */
public class UsageForDesactivateUIHandler extends UsageUIHandlerSupport<UsageForDesactivateUI> {

    private static final Logger log = LogManager.getLogger(UsageForDesactivateUIHandler.class);

    @GenerateTemplate(template = "DisableReferentialMessage.ftl")
    public static class DisableReferentialUsagesGetter implements UsagesGetter {
        private final UsageCountWithLabel usages;
        private final UsageService usageService;
        private final ToolkitIdDtoBean request;

        public DisableReferentialUsagesGetter(UsageCountWithLabel usages, UsageService usageService, ToolkitIdDtoBean request) {
            this.usages = usages;
            this.usageService = usageService;
            this.request = request;
        }

        @Override
        public UsageCountWithLabel getCount() {
            return usages;
        }

        @Override
        public <D extends BusinessDto> SingletonSupplier<Collection<ToolkitIdLabel>> getUsages(Class<D> dtoType) {
            return SingletonSupplier.of(() -> usageService.findReferential(request, dtoType));
        }

        public String getInformationMessage() {
            return DisableReferentialUsagesGetterTemplate.generate(this);
        }
    }

    public static Pair<Boolean, ToolkitIdLabel> showUsages(UsageService usageService,
                                                           ToolkitIdLabel dto,
                                                           ToolkitIdDtoBean request,
                                                           UsageCount usages,
                                                           List<ToolkitIdLabel> referenceList) {
        ObserveBusinessProject businessProject = ObserveBusinessProject.get();
        UsageCountWithLabel realUsages = new UsageCountWithLabel(I18n.getDefaultLocale(), businessProject, usages);
        DisableReferentialUsagesGetter getter = new DisableReferentialUsagesGetter(realUsages, usageService, request);

        Class<? extends BusinessDto> dtoType = dto.getType();
        String type = businessProject.getLongTitle(dtoType);
        String message = t("observe.ui.message.show.usage.referential.disabled", type, dto);

        UsageForDesactivateUI usagesUI = UsageForDesactivateUI.build(dtoType, message, getter, referenceList);
        String informationMessage = getter.getInformationMessage();
        UIInitHelper.initInformationPane(usagesUI.getInformationPane(), informationMessage);

        String replaceText = t("observe.ui.choice.save");
        Object[] options = {replaceText, t("observe.ui.choice.cancel")};
        JOptionPane pane = new JOptionPane(usagesUI, JOptionPane.WARNING_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, options[0]);

        usagesUI.getHandler().attachToOptionPane(pane, replaceText);
        usagesUI.getHandler().setUISize(pane);

        int response = usagesUI.getHandler().askToUser(pane, t("observe.ui.title.need.confirm.to.disable.referential"), options);
        log.debug("response : " + response);

        if (response == 0) {
            // will save ui
            ToolkitIdLabel selectedReplace = usagesUI.getSelectedReplace();
            return Pair.of(true, selectedReplace);
        }
        // any other case : do not save
        return Pair.of(false, null);
    }

    @Override
    protected UsagePanel getUsages() {
        return ui.getUsagesPanel();
    }

    @Override
    protected FilterableComboBox<ToolkitIdLabel> getReplace() {
        return ui.getReplace();
    }

    @Override
    public void afterInit(UsageForDesactivateUI ui) {
        super.afterInit(ui);
        getReplace().getModel().addPropertyChangeListener(FilterableComboBoxModel.PROPERTY_SELECTED_ITEM, evt -> updateCanApply());
        ui.getShouldReplace().addItemListener(evt -> updateCanApply());
    }

    @Override
    public void attachToOptionPane(JOptionPane pane, String message) {
        super.attachToOptionPane(pane, message);
        updateCanApply();
    }

    private void updateCanApply() {
        boolean canApply = !ui.getShouldReplace().isSelected() || getReplace().getModel().getSelectedItem() != null;
        ui.setCanApply(canApply);
    }
}
