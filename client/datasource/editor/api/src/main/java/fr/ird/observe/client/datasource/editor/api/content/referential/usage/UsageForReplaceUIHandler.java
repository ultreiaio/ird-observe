package fr.ird.observe.client.datasource.editor.api.content.referential.usage;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.usage.UsagePanel;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;
import fr.ird.observe.client.datasource.usage.UsagesGetter;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.UsageCountWithLabel;
import fr.ird.observe.services.service.UsageService;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxModel;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.commons.lang3.tuple.Pair;

import javax.swing.JOptionPane;
import java.util.Collection;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 20/05/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.4
 */
public class UsageForReplaceUIHandler extends UsageUIHandlerSupport<UsageForReplaceUI> {

    public static Pair<Boolean, ToolkitIdLabel> showUsages(UsageService usageService,
                                                           ToolkitIdLabel dto,
                                                           ToolkitIdDtoBean request,
                                                           UsageCount usages,
                                                           List<ToolkitIdLabel> referenceList) {
        ObserveBusinessProject businessProject = ObserveBusinessProject.get();
        UsageCountWithLabel realUsages = new UsageCountWithLabel(I18n.getDefaultLocale(), businessProject, usages);
        UsageForReplaceUIHandler.ReplaceReferentialUsagesGetter getter = new UsageForReplaceUIHandler.ReplaceReferentialUsagesGetter(realUsages, usageService, request);
        Class<? extends BusinessDto> dtoType = dto.getType();
        String type = businessProject.getLongTitle(dtoType);
        String message = t("observe.ui.message.show.usage.referential.replace", type, dto);

        UsageForReplaceUI usagesUI = UsageForReplaceUI.build(dtoType, message, getter, referenceList);

        String replaceText = t("observe.ui.choice.replace");
        Object[] options = {replaceText, t("observe.ui.choice.cancel")};
        JOptionPane pane = new JOptionPane(usagesUI, JOptionPane.WARNING_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, options[0]);

        usagesUI.getHandler().attachToOptionPane(pane, replaceText);

        usagesUI.getHandler().setUISize(pane);
        int response = usagesUI.getHandler().askToUser(pane, t("observe.ui.title.can.not.replace.referential"), options);
        if (response == 0) {
            // will replace and replace
            ToolkitIdLabel selectedReplace = usagesUI.getSelectedReplace();
            return Pair.of(true, selectedReplace);
        }
        // any other case : do not replace, do not replace
        return Pair.of(false, null);
    }

    @Override
    protected UsagePanel getUsages() {
        return ui.getUsagesPanel();
    }

    @Override
    protected FilterableComboBox<ToolkitIdLabel> getReplace() {
        return ui.getReplace();
    }

    @Override
    public void afterInit(UsageForReplaceUI ui) {
        super.afterInit(ui);
        getReplace().getModel().addPropertyChangeListener(FilterableComboBoxModel.PROPERTY_SELECTED_ITEM, evt -> updateCanApply());
    }

    @Override
    public void attachToOptionPane(JOptionPane pane, String message) {
        super.attachToOptionPane(pane, message);
        updateCanApply();
    }

    private void updateCanApply() {
        boolean canApply = getReplace().getModel().getSelectedItem() != null;
        ui.setCanApply(canApply);
    }

    public static class ReplaceReferentialUsagesGetter implements UsagesGetter {
        private final UsageCountWithLabel usages;
        private final UsageService usageService;
        private final ToolkitIdDtoBean request;

        public ReplaceReferentialUsagesGetter(UsageCountWithLabel usages, UsageService usageService, ToolkitIdDtoBean request) {
            this.usages = usages;
            this.usageService = usageService;
            this.request = request;
        }

        @Override
        public UsageCountWithLabel getCount() {
            return usages;
        }

        @Override
        public <D extends BusinessDto> SingletonSupplier<Collection<ToolkitIdLabel>> getUsages(Class<D> dtoType) {
            return SingletonSupplier.of(() -> usageService.findReferential(request, dtoType));
        }
    }
}
