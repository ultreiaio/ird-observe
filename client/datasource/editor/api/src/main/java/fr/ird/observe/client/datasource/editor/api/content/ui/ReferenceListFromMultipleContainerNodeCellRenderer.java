package fr.ird.observe.client.datasource.editor.api.content.ui;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.reference.DtoReference;
import io.ultreia.java4all.decoration.Decorator;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Component;
import java.util.Objects;

/**
 * Un renderer de liste d'entité qui reprendre la cosmétique de l'arbre
 * de navigation.
 *
 * @since 1.5
 */
public class ReferenceListFromMultipleContainerNodeCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = 1L;


    private final transient NavigationTree treeHelper;
    private final  Decorator decorator;
    private final Color defaultBackgroundColor;
    private final Color alternateBackgroundColor;
    private NavigationNode containerNode;

    public ReferenceListFromMultipleContainerNodeCellRenderer(NavigationTree treeHelper, Decorator decorator) {
        this.treeHelper = treeHelper;
        this.decorator = Objects.requireNonNull(decorator);
        defaultBackgroundColor = UIManager.getColor("List.background");
        alternateBackgroundColor = new Color(242, 242, 242);
        //UIManager.getColor("Table.alternateRowColor");
    }

    public void init() {
        containerNode = treeHelper.getSelectedNode();
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list,
                                                  Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {

        // obtain the text from the delegate renderer
        JLabel comp = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        if (!(value instanceof DtoReference) || containerNode == null) {

            // rien de plus a faire
            return comp;
        }

        // recuperation du nœud correspondant dans l'arbre
        NavigationNode node = containerNode.findChildById(((DtoReference) value).getId());

        if (node == null) {

            // nœud non trouve (cela ne devrait jamais arrive
            return comp;
        }

        Icon icon = node.getIcon();
        comp.setIcon(icon);
        comp.setForeground(node.getColor());
        comp.setFont(node.getNodeFont( comp.getFont()));
        String text = decorator.decorate(node.getUserObject());
        comp.setText(text);
        if (isSelected) {
            return comp;
        }
        if (index % 2 == 1) {
            comp.setBackground(alternateBackgroundColor);
        } else {
            comp.setBackground(defaultBackgroundColor);
        }
        return comp;
    }
}
