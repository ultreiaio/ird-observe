package fr.ird.observe.client.datasource.editor.api.content.ui;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.ReferentialDtoReference;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Component;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Un renderer de liste d'entites d'un referentiel dans le quel on veut
 * differencier les entites qui sont desactivees.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public class ReferentialReferenceListCellRenderer<R extends ReferentialDtoReference> implements ListCellRenderer<R> {

    /**
     * la couleur a utiliser pour les entites desactivees
     */
    private final Color disableColor = Color.LIGHT_GRAY;
    private final ListCellRenderer<? super R> delegate;
    private final Color defaultBackgroundColor;
    private final Color alternateBackgroundColor;
    /**
     * la couleur normal pour les entites non desactivees
     */
    private Color normalColor;

    public ReferentialReferenceListCellRenderer(ListCellRenderer<? super R> delegate) {
        this.delegate = delegate;
        defaultBackgroundColor = UIManager.getColor("List.background");
        alternateBackgroundColor = new Color(242, 242, 242);
        //UIManager.getColor("Table.alternateRowColor");
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends R> list, R value, int index, boolean isSelected, boolean cellHasFocus) {
        JComponent comp;
        comp = (JComponent) delegate.getListCellRendererComponent(
                list,
                value,
                index,
                isSelected,
                cellHasFocus);
        if (normalColor == null) {
            // premiere fois, on intialise la couleur dite normale
            normalColor = comp.getForeground();
        }

        String tip = null;

        // par defaut, on utilise la couleur normale
        Color col = normalColor;
        if (value != null) {

            boolean enabled = value.isEnabled();

            if (!enabled) {
                // l'entite est desactivee
                // on la grise pour bien la differencier
                col = disableColor;
                tip = t("observe.referential.Referential.obsolete", ((JLabel) comp).getText());
            }
        }
        comp.setForeground(col);
        comp.setToolTipText(tip);
        if (isSelected) {
            return comp;
        }
        if (index % 2 == 1) {
            comp.setBackground(alternateBackgroundColor);
        } else {
            comp.setBackground(defaultBackgroundColor);
        }
        return comp;
    }

}
