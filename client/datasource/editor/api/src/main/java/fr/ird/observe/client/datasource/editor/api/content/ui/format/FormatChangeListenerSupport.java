package fr.ird.observe.client.datasource.editor.api.content.ui.format;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import org.nuiton.jaxx.validator.swing.SwingValidator;

import javax.swing.JComponent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * Support to fix format changed in some editors.
 * <p>
 * This listener purpose is:
 * <ul>
 *     <li>to launch validation when a editor format has changed (since validator messages are using the format)</li>
 *     <li>but also avoid to alter validator changed state and the ui model modified state (since when changing format we do NOT modify form data)</li>
 * </ul>
 * Created at 07/01/2025.
 *
 * @param <Ui> type of ui
 * @param <W> type of editor
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
public abstract class FormatChangeListenerSupport<Ui extends ContentUI, W extends JComponent> implements PropertyChangeListener {
    /**
     * Client property key to store the listener in client properties of the editor.
     */
    static final String FORMAT_CHANGED_LISTENER = "formatChangedListener";
    /**
     * Content ui (used to block or not listener action, when for example the form is not in editing mode,...)
     */
    private final Ui ui;
    /**
     * The validator associated to the editor (the very one which could generate some validation messages)
     */
    private final SwingValidator<?> validator;

    protected FormatChangeListenerSupport(Ui ui, SwingValidator<?> validator) {
        this.ui = Objects.requireNonNull(ui);
        this.validator = Objects.requireNonNull(validator);
    }

    public abstract void install(W editor);

    public abstract void uninstall(W editor);

    @Override
    public final void propertyChange(PropertyChangeEvent evt) {
        if (!ui.getModel().getStates().isEditing()) {
            return;
        }
        if (ui.getModel().getStates().isReadingMode()) {
            return;
        }
        SwingValidator<?> parentValidator = validator.getParentValidator();
        boolean withParentValidator = parentValidator != null;
        boolean modified = ui.getModel().getStates().isModified();
        boolean parentChanged = withParentValidator && parentValidator.isChanged();
        boolean changed = validator.isChanged();
        try {
            validator.doValidate();
        } finally {
            if (!changed) {
                validator.setChanged(false);
            }
            if (withParentValidator && !parentChanged) {
                parentValidator.setChanged(false);
            }
            if (!modified) {
                ui.getModel().getStates().setModified(false);
            }
        }
    }
}
