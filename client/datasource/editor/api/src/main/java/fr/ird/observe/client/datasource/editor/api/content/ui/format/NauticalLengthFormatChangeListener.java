package fr.ird.observe.client.datasource.editor.api.content.ui.format;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditor;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditorModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.validator.JAXXValidator;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.ValidatorField;

import java.util.Optional;

/**
 * Implementation of {@link FormatChangeListenerSupport} for {@link NauticalLengthEditor} editor.
 * <p>
 * Created at 07/01/2025.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
public class NauticalLengthFormatChangeListener<Ui extends ContentUI> extends FormatChangeListenerSupport<Ui, NauticalLengthEditor> {

    private static final Logger log = LogManager.getLogger(NauticalLengthFormatChangeListener.class);

    public static <Ui extends ContentUI> void install(Ui ui) {
        if (ui instanceof JAXXValidator) {
            JAXXValidator jaxxValidator = (JAXXValidator) ui;
            jaxxValidator.getValidatorEditors().asMap().forEach((k, v) -> {
                if (k instanceof NauticalLengthEditor) {
                    NauticalLengthEditor editor = (NauticalLengthEditor) k;
                    // We always have only one validator field associated to a such component
                    ValidatorField validatorField = v.iterator().next();
                    String validatorId = validatorField.validatorId();
                    SwingValidator<?> validator = jaxxValidator.getValidator(validatorId);
                    NauticalLengthFormatChangeListener<Ui> listener = new NauticalLengthFormatChangeListener<>(ui, validator);
                    listener.install(editor);
                }
            });
        }
    }

    public static <Ui extends ContentUI> void uninstall(Ui ui) {
        if (ui instanceof JAXXValidator) {
            JAXXValidator jaxxValidator = (JAXXValidator) ui;
            jaxxValidator.getValidatorEditors().asMap().forEach((k, v) -> {
                if (k instanceof NauticalLengthEditor) {
                    NauticalLengthEditor editor = (NauticalLengthEditor) k;
                    Optional<NauticalLengthFormatChangeListener<?>> optionalListener = getListener(editor);
                    optionalListener.ifPresent(l -> l.uninstall(editor));
                }
            });
        }
    }

    public NauticalLengthFormatChangeListener(Ui ui, SwingValidator<?> validator) {
        super(ui, validator);
    }

    @Override
    public void install(NauticalLengthEditor editor) {
        log.info("Install NauticalLengthFormatChangeListener ({}) on {}", this, editor.getModel().getEditorKey());
        editor.putClientProperty(FORMAT_CHANGED_LISTENER, this);
        editor.getModel().addPropertyChangeListener(NauticalLengthEditorModel.PROPERTY_FORMAT, this);
    }

    @Override
    public void uninstall(NauticalLengthEditor editor) {
        log.info("Uninstall NauticalLengthFormatChangeListener ({}) on {}", this, editor.getModel().getEditorKey());
        editor.putClientProperty(FORMAT_CHANGED_LISTENER, null);
        editor.getModel().removePropertyChangeListener(NauticalLengthEditorModel.PROPERTY_FORMAT, this);
    }

    private static Optional<NauticalLengthFormatChangeListener<?>> getListener(NauticalLengthEditor editor) {
        return Optional.ofNullable((NauticalLengthFormatChangeListener<?>) editor.getClientProperty(FORMAT_CHANGED_LISTENER));
    }
}
