package fr.ird.observe.client.datasource.editor.api.content.ui.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.action.AddNewRow;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.action.DeleteSelectedRow;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.action.MoveBottom;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.action.MoveDown;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.action.MoveTop;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.action.MoveUp;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.client.util.table.EditableTableModel;
import fr.ird.observe.client.util.table.EditableTableModelChanged;
import fr.ird.observe.client.util.table.action.MoveToNextCell;
import fr.ird.observe.client.util.table.action.MoveToNextRow;
import fr.ird.observe.client.util.table.action.MoveToPreviousCell;
import fr.ird.observe.client.util.table.action.MoveToPreviousRow;
import fr.ird.observe.dto.data.InlineDataDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.table.TableUtilities;
import org.nuiton.jaxx.validator.swing.SwingValidator;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.util.EventObject;
import java.util.Set;

/**
 * Created on 04/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class EditableTable<E extends InlineDataDto, M extends EditableTableModel<E>> extends JXTable {

    private static final Set<String> RECOMPUTE_ACTIONS_PROPERTY = Set.of(
            EditableTableModel.EDITABLE_PROPERTY,
            EditableTableModel.VALID_PROPERTY,
            EditableTableModel.MODIFIED_PROPERTY,
            EditableTableModel.SELECTION_EMPTY_PROPERTY,
            EditableTableModel.EMPTY_PROPERTY,
            EditableTableModel.ROW_COUNT_PROPERTY
    );
    private static final Logger log = LogManager.getLogger(EditableTable.class);
    /**
     * Listener fired when selection model has changed.
     */
    private transient ListSelectionListener whenSelectionModelChanged;

    final transient MoveTop<?, ?> moveTopAction;
    final transient MoveBottom<?, ?> moveBottomAction;
    final transient MoveUp<?, ?> moveUpAction;
    final transient MoveDown<?, ?> moveDownAction;
    private AddNewRow<ContentUI, EditableTable<E, M>> addNewRow;
    private DeleteSelectedRow<ContentUI, EditableTable<E, M>> deleteSelectedRow;

    public EditableTable(M model) {
        super(model);
        if (model.isCanMoveRows()) {
            moveTopAction = new MoveTop<>(this);
            moveBottomAction = new MoveBottom<>(this);
            moveUpAction = new MoveUp<>(this);
            moveDownAction = new MoveDown<>(this);
        } else {
            moveTopAction = null;
            moveBottomAction = null;
            moveUpAction = null;
            moveDownAction = null;
        }
        TableModelListener l = e -> {
            if (TableUtilities.isInsert(e)) {
                SwingUtilities.invokeLater(() -> {
                    int viewRow = convertRowIndexToView(e.getFirstRow());
                    scrollRectToVisible(getCellRect(viewRow, 0, true));
                });
            }
        };
        getModel().addTableModelListener(l);
    }

    @SuppressWarnings("unchecked")
    @Override
    public final M getModel() {
        return (M) super.getModel();
    }

    public void init(ContentTableUI<?, ?, ?> ui, SwingValidator<E> rowValidator) {
        init(ui, ui.getValidatorTable(), rowValidator);
        InputMap inputMap = ui.getContentBody().getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = ui.getContentBody().getActionMap();
        addNewRow.register(inputMap, actionMap);
        deleteSelectedRow.register(inputMap, actionMap);

    }

    public void init(ContentOpenableUI<?, ?> ui, SwingValidator<E> rowValidator) {
        init(ui, ui.getValidator(), rowValidator);
        InputMap inputMap = ui.getContentBody().getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = ui.getContentBody().getActionMap();
        addNewRow.register(inputMap, actionMap);
        deleteSelectedRow.register(inputMap, actionMap);
    }

    private final Set<KeyStroke> moveRowsKeyStrokes = Set.of(
            ObserveKeyStrokesEditorApi.KEY_STROKE_MOVE_TOP_TABLE_ENTRY,
            ObserveKeyStrokesEditorApi.KEY_STROKE_MOVE_BOTTOM_TABLE_ENTRY,
            ObserveKeyStrokesEditorApi.KEY_STROKE_MOVE_UP_TABLE_ENTRY,
            ObserveKeyStrokesEditorApi.KEY_STROKE_MOVE_DOWN_TABLE_ENTRY
    );
    private final Set<KeyStroke> contentKeyStrokes = Set.of(
            ObserveKeyStrokesEditorApi.KEY_STROKE_SAVE_TABLE_ENTRY,
            ObserveKeyStrokesEditorApi.KEY_STROKE_SAVE_AND_NEW_TABLE_ENTRY,
            ObserveKeyStrokesEditorApi.KEY_STROKE_RESET_TABLE_ENTRY
    );

    protected void init(ContentUI ui, SwingValidator<?> validator, SwingValidator<E> rowValidator) {

        InputMap inputMap = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        boolean canMoveRows = getModel().isCanMoveRows();
        if (canMoveRows) {
            for (KeyStroke keyStroke : moveRowsKeyStrokes) {
                inputMap.put(keyStroke, "none");
            }

            new InlineAutoSelectWithMoveUpAndDownShowPopupAction(ui, this);
        }
        JXTitledPanel panel = (JXTitledPanel) SwingUtilities.getAncestorOfClass(JXTitledPanel.class, this);

        for (KeyStroke keyStroke : contentKeyStrokes) {
            inputMap.put(keyStroke, "none");
        }

        inputMap.put(ObserveKeyStrokesEditorApi.KEY_STROKE_ADD_TABLE_ENTRY, "none");
        inputMap.put(ObserveKeyStrokesEditorApi.KEY_STROKE_DELETE_SELECTED_TABLE_ENTRY, "none");
        M tableModel = getModel();
        Class<E> type = tableModel.type();
        ListSelectionModel selectionModel = getSelectionModel();
        selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tableModel.setSelectionModel(selectionModel);
        installSelectionListener();
        installTableKeyListener();

        getTableHeader().setReorderingAllowed(false);
        setAutoStartEditOnKeyStroke(true);
        setSortable(false);
        setFillsViewportHeight(true);

        JToolBar toolbar = new JToolBar();
        UIInitHelper.init(toolbar);

        JButton addMeasure = new JButton();
        JButton deleteMeasure = new JButton();
        toolbar.add(addMeasure);
        toolbar.add(deleteMeasure);
        panel.setRightDecoration(toolbar);

        addNewRow = AddNewRow.installAction(ui, type, addMeasure, this);
        deleteSelectedRow = DeleteSelectedRow.installAction(ui, type, deleteMeasure, this);

        tableModel.addPropertyChangeListener(evt -> recomputeActions(evt, tableModel, addMeasure, deleteMeasure));

        installOnMainTable(ui, validator, rowValidator);
        setHighlighters(
//                HighlighterFactory.createSimpleStriping(),
                new ColorHighlighter((renderer, adapter) -> {
                    int row = adapter.convertRowIndexToModel(adapter.row);
                    return !tableModel.isRowValid(row);
                }, ui.getHandler().getClientConfig().getFloatingObjectMaterialErrorColor(), Color.WHITE),
                new ColorHighlighter((renderer, adapter) -> {
                    int row = adapter.convertRowIndexToModel(adapter.row);
                    return tableModel.isRowEmpty(row);
                }, ui.getHandler().getClientConfig().getTableEmptyRowColor(), Color.WHITE)
        );
    }

    public AddNewRow<ContentUI, EditableTable<E, M>> getAddNewRow() {
        return addNewRow;
    }

    public DeleteSelectedRow<ContentUI, EditableTable<E, M>> getDeleteSelectedRow() {
        return deleteSelectedRow;
    }

    @Override
    public boolean editCellAt(int row, int column, EventObject e) {
        if (!(e instanceof KeyEvent)) {
            return super.editCellAt(row, column, e);
        }
        KeyEvent keyEvent = (KeyEvent) e;
        if (keyEvent.isControlDown()) {
            if (keyEvent.getKeyChar() == KeyEvent.CHAR_UNDEFINED) {
                return false;
            }
            KeyStroke keyStrokeForEvent = KeyStroke.getKeyStrokeForEvent(keyEvent);
            if (contentKeyStrokes.contains(keyStrokeForEvent)) {
                return false;
            }
        }

        if (keyEvent.isAltGraphDown() && getModel().isCanMoveRows()) {
            if (keyEvent.getKeyChar() == KeyEvent.CHAR_UNDEFINED) {
                return false;
            }
            KeyStroke keyStrokeForEvent = KeyStroke.getKeyStrokeForEvent(keyEvent);
            if (moveRowsKeyStrokes.contains(keyStrokeForEvent)) {
                return false;
            }
        }
        return super.editCellAt(row, column, e);
    }

    private void recomputeActions(PropertyChangeEvent evt, M tableModel, JButton addMeasure, JButton deleteMeasure) {
        if (!RECOMPUTE_ACTIONS_PROPERTY.contains(evt.getPropertyName())) {
            return;
        }
        boolean editable = tableModel.isEditable();
        boolean canAdd = editable && tableModel.isValid();
        boolean canDelete = editable && !tableModel.isSelectionEmpty();
        if (canAdd) {
            if (!tableModel.isEmpty()) {
                canAdd = tableModel.isCanCreateNewRow(tableModel.getRowCount() - 1);
            }
        }
        addMeasure.setEnabled(canAdd);
        deleteMeasure.setEnabled(canDelete);
    }

    public final void installSelectionListener() {
        // always try to uninstall it before to avoid any memory leak
        uninstallSelectionListener();
        getModel().setSelectionModel(getSelectionModel());
        ListSelectionListener listener = getWhenSelectionModelChanged();
        getSelectionModel().addListSelectionListener(listener);
    }

    public final void uninstallSelectionListener() {
        getModel().setSelectionModel(null);
        ListSelectionListener listener = getWhenSelectionModelChanged();
        getSelectionModel().removeListSelectionListener(listener);
    }

    public final void installTableKeyListener() {
        M model = getModel();
        MoveToNextCell.install(model, this);
        MoveToPreviousCell.install(model, this);
        MoveToNextRow.install(model, this);
        MoveToPreviousRow.install(model, this);
    }

    public <U extends ContentUI> void installOnMainTable(U ui, SwingValidator<?> validator, SwingValidator<E> rowValidator) {
        M model = getModel();
        model.setRowValidator(rowValidator);
        ContentUIModelStates states = ui.getModel().getStates();
        // listen mode property on states
        states.addPropertyChangeListener(ContentUIModelStates.PROPERTY_MODE, evt -> {
            model.setEditable(!states.isReadingMode());
            rowValidator.setBean(null);
        });
//        states.addPropertyChangeListener(ContentUIModelStates.PROPERTY_EDITING, evt -> {
//            boolean isEditing = (boolean) evt.getNewValue();
//            model.setEditable(isEditing);
////            E selectedRow = model.getSelectedRow();
////            if (isEditing && model.getSelectedRow() !=null) {
////                rowValidator.setBean(selectedRow);
////            } else {
////                rowValidator.setBean(null);
////            }
//        });
        // listen modified property on table model
        model.addPropertyChangeListener(EditableTableModel.MODIFIED_PROPERTY, new EditableTableModelChanged(validator, model));
        // listen selectedRow property on table model
        model.addPropertyChangeListener(EditableTableModel.SELECTED_ROW_PROPERTY, evt -> {
            log.warn(String.format("Selected row modified: %s, states editing ?: %b", evt.getNewValue(), states.isEditing()));
            if (states.isEditing()) {
                @SuppressWarnings("unchecked") E newValue = (E) evt.getNewValue();
                rowValidator.setBean(newValue);
            } else {
                rowValidator.setBean(null);
            }
        });
        model.setEditable(states.isEditing());
        model.onModifiedChanged(validator, false);
        //FIXME Remove this, deal with update of model more finely
        model.addTableModelListener(e -> {
            if (model.isEditable() || states.isEditing()) {
                log.warn(String.format("Table model changed, will recompute valid states: %s", e));
                model.validate();
            }
        });
    }

    public boolean stopEdit() {
        if (isEditing()) {

            // stop editing
            try {
                boolean stopEdit = getCellEditor().stopCellEditing();
                if (!stopEdit) {
                    log.warn("Could not stop edit cell...");
                    return false;
                }
            } catch (Exception e) {
                log.error("Can't stop edit, but still will do it...", e);
                removeEditor();
                return true;
            }
        }
        return true;
    }

    protected final ListSelectionListener getWhenSelectionModelChanged() {
        if (whenSelectionModelChanged == null) {
            whenSelectionModelChanged = e -> {
                if (!e.getValueIsAdjusting()) {
                    int selectedRow = getSelectedRow();
                    if (selectedRow >= getModel().getRowCount()) {
                        selectedRow = getModel().getRowCount() - 1;
                        log.info("Decrease selectedRow!!! to " + selectedRow);
                    }
                    getModel().setSelectedRowIndex(selectedRow);
                }
            };
        }
        return whenSelectionModelChanged;
    }

    public void disableMoveActions() {
        moveTopAction.setEnabled(false);
        moveBottomAction.setEnabled(false);
        moveUpAction.setEnabled(false);
        moveDownAction.setEnabled(false);
    }
}
