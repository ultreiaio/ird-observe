package fr.ird.observe.client.datasource.editor.api.content.ui.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.action.MoveBottom;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.action.MoveDown;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.action.MoveTop;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.action.MoveUp;
import fr.ird.observe.client.util.table.popup.AutoSelectRowAndShowPopupActionSupport;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Created on 21/10/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public class InlineAutoSelectWithMoveUpAndDownShowPopupAction extends AutoSelectRowAndShowPopupActionSupport {

    private final ContentUI ui;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public InlineAutoSelectWithMoveUpAndDownShowPopupAction(ContentUI ui, EditableTable<?, ?> table) {
        super(table);
        table.setSortable(false);
        table.setRowSorter(null);
        this.ui = ui;
        JPopupMenu popup = getPopup();
        popup.removeAll();
        JMenuItem moveTop;
        popup.add(moveTop = new JMenuItem());
        JMenuItem moveUp;
        popup.add(moveUp = new JMenuItem());
        JMenuItem moveDown;
        popup.add(moveDown = new JMenuItem());
        JMenuItem moveBottom;
        popup.add(moveBottom = new JMenuItem());
        MoveTop.<ContentUI, MoveTop>init(ui, moveTop, table.moveTopAction);
        MoveUp.<ContentUI, MoveUp>init(ui, moveUp, table.moveUpAction);
        MoveDown.<ContentUI, MoveDown>init(ui, moveDown, table.moveDownAction);
        MoveBottom.<ContentUI, MoveBottom>init(ui, moveBottom, table.moveBottomAction);
    }

    @Override
    public EditableTable<?, ?> getTable() {
        return (EditableTable<?, ?>) super.getTable();
    }

    @Override
    protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {
        if (ui.getModel().getStates().isReadingMode()) {
            getTable().disableMoveActions();
        }
    }
}
