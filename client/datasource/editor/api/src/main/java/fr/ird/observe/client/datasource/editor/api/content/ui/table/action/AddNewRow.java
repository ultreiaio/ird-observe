package fr.ird.observe.client.datasource.editor.api.content.ui.table.action;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.InlineDataDto;

import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 19/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class AddNewRow<U extends ContentUI, T extends EditableTable<?, ?>> extends EditableTableAction<U, T> {

    public static <U extends ContentUI, E extends InlineDataDto, T extends EditableTable<E, ?>> AddNewRow<U, T> installAction(U ui, Class<E> type, AbstractButton editor, T table) {
        String typeLabel = I18nDecoratorHelper.getType(type);
        String text = t("observe.data.InlineData.action.add");
        String tip = t("observe.data.InlineData.action.add.tip", typeLabel);
        AddNewRow<U, T> action = new AddNewRow<>(table, "", tip);
        init(ui, editor, action);
        editor.setFont(editor.getFont().deriveFont(10f));
        return action;
    }

    public AddNewRow(T table, String text, String tip) {
        super(table, text, tip, "add", ObserveKeyStrokesEditorApi.KEY_STROKE_ADD_TABLE_ENTRY);
        setCheckMenuItemIsArmed(false);
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        return super.canExecuteAction(e) && table.getModel().isCanCreateNewRow(table.getSelectedRow());
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        UIHelper.stopEditing(table);
        table.getModel().addNewRow();
        SwingUtilities.invokeLater(() -> table.editCellAt(table.getSelectedRow(), 0));
    }
}
