package fr.ird.observe.client.datasource.editor.api.content.ui.table.action;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableTableModel;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.InlineDataDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.AbstractButton;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 19/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class DeleteSelectedRow<U extends ContentUI, T extends EditableTable<?, ?>> extends EditableTableAction<U, T> {

    private static final Logger log = LogManager.getLogger(DeleteSelectedRow.class);

    private final String deleteMessage;

    public static <U extends ContentUI, E extends InlineDataDto, T extends EditableTable<E, ?>> DeleteSelectedRow<U, T> installAction(U ui, Class<E> type, AbstractButton editor, T table) {
        String typeLabel = I18nDecoratorHelper.getType(type);
        String text = t("observe.data.InlineData.action.delete");
        String tip = t("observe.data.InlineData.action.delete.tip", typeLabel);
        String message = t("observe.data.InlineData.action.delete.message", typeLabel);

        DeleteSelectedRow<U, T> action = new DeleteSelectedRow<>(table, "", tip, message);
        init(ui, editor, action);
        editor.setFont(editor.getFont().deriveFont(10f));
        return action;
    }

    public DeleteSelectedRow(T table, String text, String tip, String deleteMessage) {
        super(table, text, tip, "content-delete-16", ObserveKeyStrokesEditorApi.KEY_STROKE_DELETE_SELECTED_TABLE_ENTRY);
        this.deleteMessage = deleteMessage;
        setCheckMenuItemIsArmed(false);
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        return super.canExecuteAction(e) && table.getModel().isEditable() && !table.getModel().isSelectionEmpty();
    }

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        UIHelper.cancelEditing(table);
        EditableTableModel<?> tableModel = table.getModel();
        InlineDataDto data = tableModel.getSelectedRow();
        if (!Objects.requireNonNull(data).isDataEmpty()) {
            getDecoratorService().installDecorator(data);
            log.info(String.format("%sDelete: %s", ui.getModel().getPrefix(), data));
            String message = String.format("<html><body><p>%s</p><br><p><b><center>%s</center></b></p>", deleteMessage, data);
            int response = askToUser(
                    t("observe.ui.title.delete"),
                    message,
                    JOptionPane.WARNING_MESSAGE,
                    new Object[]{t("observe.ui.choice.confirm.delete"),
                            t("observe.ui.choice.cancel")},
                    1);
            if (response != 0) {
                // user cancel
                return;
            }
        }
        tableModel.removeSelectedRow();
    }
}
