package fr.ird.observe.client.datasource.editor.api.content.ui.table.action;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.table.EditableTableModel;

import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;

/**
 * Created on 21/10/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public abstract class SortableEditableTableAction<UI extends ContentUI, T extends EditableTable<?, ?>> extends EditableTableAction<UI, T> {

    private final PropertyChangeListener propertyChangeListener;

    protected abstract boolean computeEnabled(UI ui, int selectedRow, int rowCount);

    protected abstract void actionPerformed(EditableTableModel<?> tableModel, int selectedRow);

    public SortableEditableTableAction(T table, String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(table, label, shortDescription, actionIcon, acceleratorKey);
        setCheckMenuItemIsArmed(false);
        propertyChangeListener = e -> {
            boolean b = computeEnabled(ui, table.getModel().getSelectedRowIndex(), table.getModel().getRowCount());
            setEnabled(b);
        };
        setEnabled(false);
    }

    @Override
    public void init() {
        super.init();
        table.getModel().addPropertyChangeListener(EditableTableModel.SELECTED_ROW_PROPERTY, propertyChangeListener);
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        return super.canExecuteAction(e) && table.stopEdit();
    }

    @Override
    protected void doActionPerformed(ActionEvent e, UI ui) {
        T container = getContainer(ui);
        int selectedRow = container.getSelectedRow();
        actionPerformed(container.getModel(), selectedRow);
        SwingUtilities.invokeLater(container::requestFocusInWindow);
    }
}
