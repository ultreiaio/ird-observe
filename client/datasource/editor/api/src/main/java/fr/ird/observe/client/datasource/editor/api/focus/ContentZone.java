package fr.ird.observe.client.datasource.editor.api.focus;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditorModel;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.main.focus.MainUIFocusModel;
import fr.ird.observe.client.main.focus.MainUIFocusZone;

import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.util.Objects;

/**
 * Created on 10/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public class ContentZone extends MainUIFocusZone<JSplitPane> {
    public static final String ZONE_NAME = "content";
    public static final String PROPERTY_FOCUS_OWNER = "focusOwner";
    /**
     * Data source editor model (responsible to change content).
     */
    private final DataSourceEditorModel dataSourceEditorModel;
    /**
     * The content in the zone (could be null).
     */
    private ContentUI content;

    public ContentZone(MainUIFocusModel focusModel, DataSourceEditor dataSourceEditor) {
        super(ZONE_NAME, focusModel, Objects.requireNonNull(dataSourceEditor).getContentSplitPane());
        this.dataSourceEditorModel = dataSourceEditor.getModel();
    }

    @Override
    protected void install() {
        super.install();
        dataSourceEditorModel.addPropertyChangeListener(DataSourceEditorModel.PROPERTY_CONTENT, this);
    }

    @Override
    protected void uninstall() {
        if (content != null) {
            uninstallContent();
        }
        dataSourceEditorModel.removePropertyChangeListener(DataSourceEditorModel.PROPERTY_CONTENT, this);
        super.uninstall();
    }

    @Override
    protected Component computeFocusOwner(JSplitPane container, Component previousFocusOwner) {
        Component result = null;
        if (content != null) {
            result = content.computeFocusOwner();
        }
        return result;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        super.propertyChange(evt);
        if (DataSourceEditorModel.PROPERTY_CONTENT.equals(evt.getPropertyName())) {
            if (content != null) {
                uninstallContent();
            }
            dirty();
            content = (ContentUI) evt.getNewValue();
            installContent();
            if (isOwner()) {
                refreshFocus();
            }
            return;
        }
        if (PROPERTY_FOCUS_OWNER.equals(evt.getPropertyName())) {
            if (isBlock()) {
                return;
            }
            if (content == null) {
                return;
            }
            Component component = (Component) evt.getNewValue();
            if (component == null) {
                return;
            }
            if (!SwingUtilities.isDescendingFrom(component, content)) {
                // does not belong to this zone
                return;
            }

            block();
            try {
                setComponent(component);
            } finally {
                unblock();
            }
        }
    }

    protected void uninstallContent() {
        getModel().getKeyboardFocusManager().removePropertyChangeListener(PROPERTY_FOCUS_OWNER, this);
    }

    protected void installContent() {
        getModel().getKeyboardFocusManager().addPropertyChangeListener(PROPERTY_FOCUS_OWNER, this);
    }
}
