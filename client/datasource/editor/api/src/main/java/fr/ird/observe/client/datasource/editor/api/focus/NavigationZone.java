package fr.ird.observe.client.datasource.editor.api.focus;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.main.focus.MainUIFocusModel;
import fr.ird.observe.client.main.focus.MainUIFocusZone;
import org.jdesktop.swingx.JXTitledPanel;

import java.awt.Component;
import java.util.Objects;

/**
 * Navigation zone.
 * <p>
 * Created on 10/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public class NavigationZone extends MainUIFocusZone<JXTitledPanel> {

    public static final String ZONE_NAME = "navigation";
    private final DataSourceEditor dataSourceEditor;

    public NavigationZone(MainUIFocusModel focusModel, DataSourceEditor dataSourceEditor) {
        super(ZONE_NAME, focusModel, dataSourceEditor.getNavigationUI());
        this.dataSourceEditor = Objects.requireNonNull(dataSourceEditor);
    }

    @Override
    protected Component computeFocusOwner(JXTitledPanel container, Component previousFocusOwner) {
        return dataSourceEditor.getNavigationUI().getTree();
    }
}
