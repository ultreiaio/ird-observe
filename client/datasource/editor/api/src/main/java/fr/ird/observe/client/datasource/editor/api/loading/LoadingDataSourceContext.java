package fr.ird.observe.client.datasource.editor.api.loading;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.action.ActionContext;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;

import java.util.Objects;

public class LoadingDataSourceContext extends ActionContext {

    private final ObserveMainUI mainUI;
    private final StorageUIModel model;
    private final ObserveDataSourcesManager dataSourcesManager;
    private final FloatingObjectPresetsStorage floatingObjectPresets;
    private final ObserveSwingDataSource currentDataSource;
    private ObserveSwingDataSource localDataSource;

    public LoadingDataSourceContext(StorageUIModel model, ObserveMainUI mainUI, ClientConfig config, ObserveDataSourcesManager dataSourcesManager, FloatingObjectPresetsStorage floatingObjectPresets) {
        super(Objects.requireNonNull(model).getProgressModel(), Objects.requireNonNull(config));
        this.mainUI = mainUI;
        this.model = Objects.requireNonNull(model);
        this.dataSourcesManager = Objects.requireNonNull(dataSourcesManager);
        this.floatingObjectPresets = Objects.requireNonNull(floatingObjectPresets);
        this.currentDataSource = dataSourcesManager.getMainDataSource();
        if (this.currentDataSource != null && this.currentDataSource.isLocal()) {
            this.localDataSource = this.currentDataSource;
        }
    }

    public ObserveMainUI getMainUI() {
        return mainUI;
    }

    public StorageUIModel getModel() {
        return model;
    }

    public ObserveDataSourcesManager getDataSourcesManager() {
        return dataSourcesManager;
    }

    public FloatingObjectPresetsStorage getFloatingObjectPresets() {
        return floatingObjectPresets;
    }

    public ObserveSwingDataSource getCurrentDataSource() {
        return currentDataSource;
    }

    public ObserveSwingDataSource getLocalDataSource() {
        return localDataSource;
    }

    public void setLocalDataSource(ObserveSwingDataSource localDataSource) {
        this.localDataSource = localDataSource;
    }
}
