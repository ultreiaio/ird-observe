package fr.ird.observe.client.datasource.editor.api.loading.close;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.action.ActionWithSteps;
import fr.ird.observe.client.datasource.editor.api.loading.LoadingDataSourceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

public class BackupLocalDataSourceActionStep extends CloseDataSourceActionStepSupport {

    private static final Logger log = LogManager.getLogger(BackupLocalDataSourceActionStep.class);

    public BackupLocalDataSourceActionStep(LoadingDataSourceContext actionContext, boolean pending) {
        super(actionContext, pending, true);
    }

    @Override
    public String getErrorTitle() {
        return t("observe.error.storage.backup.local.db");
    }


    @Override
    protected int computeStepCount0() {
        return 1;
    }

    @Override
    public boolean skip(ActionWithSteps<LoadingDataSourceContext> mainAction) {
        return skipIfPreviousFail(mainAction);
    }

    @Override
    public void doAction0(ActionWithSteps<LoadingDataSourceContext> mainAction) {
        ObserveSwingDataSource localDataSource = Objects.requireNonNull(actionContext.getLocalDataSource());
        Path f = actionContext.getModel().getBackupFile().toPath();
        log.info(String.format("Do backup with %s in %s", localDataSource, f));
        try {
            localDataSource.backupLocalDatabase(f);
            actionContext.getConfig().updateBackupDirectory(f.toFile());
            incrementsProgress();
        } catch (Exception e) {
            log.error("Could not backup local datasource", e);
            throw e;
        }
    }
}
