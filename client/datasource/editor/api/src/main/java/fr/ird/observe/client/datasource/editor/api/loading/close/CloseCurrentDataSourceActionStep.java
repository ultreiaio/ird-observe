package fr.ird.observe.client.datasource.editor.api.loading.close;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.action.ActionWithSteps;
import fr.ird.observe.client.datasource.editor.api.loading.LoadingDataSourceContext;

import static io.ultreia.java4all.i18n.I18n.t;

public class CloseCurrentDataSourceActionStep extends CloseDataSourceActionStepSupport {

    public CloseCurrentDataSourceActionStep(LoadingDataSourceContext actionContext, boolean pending) {
        super(actionContext, pending, false);
    }

    @Override
    public String getErrorTitle() {
        return t("observe.error.storage.close.current.db");
    }

    @Override
    protected int computeStepCount0() {
        return 1;
    }

    @Override
    public boolean skip(ActionWithSteps<LoadingDataSourceContext> mainAction) {
        return getInputState().skip();
    }

    @Override
    public void doAction0(ActionWithSteps<LoadingDataSourceContext> mainAction) {
        doCloseDatasource(actionContext.getCurrentDataSource());
    }
}
