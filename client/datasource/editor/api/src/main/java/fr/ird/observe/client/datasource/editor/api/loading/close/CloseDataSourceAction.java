package fr.ird.observe.client.datasource.editor.api.loading.close;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.action.ActionStep;
import fr.ird.observe.client.datasource.api.action.ActionWithSteps;
import fr.ird.observe.client.datasource.api.action.CloseLocalDataSourceFeedBackModel;
import fr.ird.observe.client.datasource.api.action.FeedBackBuilderModel;
import fr.ird.observe.client.datasource.editor.api.loading.LoadingDataSourceContext;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class CloseDataSourceAction extends ActionWithSteps<LoadingDataSourceContext> {
    private static final Logger log = LogManager.getLogger(CloseDataSourceAction.class);

    public CloseDataSourceAction(LoadingDataSourceContext context) {
        super(context, true);
    }

    @Override
    protected List<? extends CloseDataSourceActionStepSupport> createSteps(LoadingDataSourceContext actionContext) {
        ClientConfig config = actionContext.getConfig();
        ObserveSwingDataSource currentDataSource = actionContext.getCurrentDataSource();
        StorageUIModel model = actionContext.getModel();
        boolean localStorageExist = config.isLocalStorageExist();
        boolean currentDataSourceIsLocal = currentDataSource != null && currentDataSource.isLocal();
        boolean doBackupLocal = localStorageExist && model.isDoBackup();
        boolean doDestroyLocal = localStorageExist && model.getChooseDb().getInitModel().isOnCreateMode();
        boolean doOpenLocal = !currentDataSourceIsLocal && doBackupLocal;
        boolean doCloseLocal = doDestroyLocal && (currentDataSourceIsLocal || doOpenLocal);
        boolean doCloseCurrentDataSource = currentDataSource != null && !doCloseLocal;
        if (doOpenLocal) {
            log.info("Will open local data source");
        }
        if (doBackupLocal) {
            log.info("Will backup local data source");
        }
        if (doCloseLocal) {
            log.info("Will close local data source");
        }
        if (doDestroyLocal) {
            log.info("Will destroy local data source");
        }
        if (doCloseCurrentDataSource) {
            log.info("Will close current data source");
        }
        return List.of(
                // Open local data source
                new OpenLocalDataSourceActionStep(actionContext, doOpenLocal),
                // Backup local data source
                new BackupLocalDataSourceActionStep(actionContext, doBackupLocal),
                // Close local data source
                new CloseLocalDataSourceActionStep(actionContext, doCloseLocal),
                // Destroy local data source
                new DestroyLocalDataSourceActionStep(actionContext, doDestroyLocal),
                // Close current data source
                new CloseCurrentDataSourceActionStep(actionContext, doCloseCurrentDataSource));
    }

    @Override
    protected FeedBackBuilderModel createFeedBackModel(ClientConfig config, ActionStep<LoadingDataSourceContext> stepsFailed) {
        return new CloseLocalDataSourceFeedBackModel(config, stepsFailed);
    }

}
