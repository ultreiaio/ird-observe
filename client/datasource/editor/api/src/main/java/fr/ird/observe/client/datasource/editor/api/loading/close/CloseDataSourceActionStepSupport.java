package fr.ird.observe.client.datasource.editor.api.loading.close;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.action.ActionStep;
import fr.ird.observe.client.datasource.editor.api.loading.LoadingDataSourceContext;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

public abstract class CloseDataSourceActionStepSupport extends ActionStep<LoadingDataSourceContext> {

    private static final Logger log = LogManager.getLogger(CloseDataSourceActionStepSupport.class);

    protected CloseDataSourceActionStepSupport(LoadingDataSourceContext actionContext, boolean pending, boolean needFeedBackOnFail) {
        super(actionContext, pending, needFeedBackOnFail);
    }

    protected void doCloseDatasource(ObserveSwingDataSource dataSource) {
        if (dataSource != null && dataSource.isOpen()) {
            log.info(String.format("close data source: %s", dataSource));
            try {
                dataSource.close();
                incrementsProgress();
            } catch (Exception e) {
                log.error("Could not close local data source", e);
                throw e;
            }
        }
    }

    protected ObserveSwingDataSource createLocalDataSource() {
        // create the local data source
        ObserveDataSourcesManager dataSourcesManager = actionContext.getDataSourcesManager();
        ObserveDataSourceConfigurationTopiaH2 localConfiguration = dataSourcesManager.newH2DataSourceConfiguration(I18n.n("observe.runner.initStorage.label.local"));

        // can't migrate datasource in this case
        //FIXME I don't understand why in this cas we can't try to migrate local data source?
        localConfiguration.setCanMigrate(false);

        //do open local datasource
        ObserveSwingDataSource localDataSource = dataSourcesManager.newDataSource(localConfiguration);

        Objects.requireNonNull(localDataSource);
        return localDataSource;
    }
}
