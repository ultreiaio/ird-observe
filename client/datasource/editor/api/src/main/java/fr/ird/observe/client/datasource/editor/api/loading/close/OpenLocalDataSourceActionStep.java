package fr.ird.observe.client.datasource.editor.api.loading.close;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.action.ActionWithSteps;
import fr.ird.observe.client.datasource.editor.api.loading.LoadingDataSourceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static io.ultreia.java4all.i18n.I18n.t;

public class OpenLocalDataSourceActionStep extends CloseDataSourceActionStepSupport {

    private static final Logger log = LogManager.getLogger(OpenLocalDataSourceActionStep.class);

    public OpenLocalDataSourceActionStep(LoadingDataSourceContext actionContext, boolean pending) {
        super(actionContext, pending, true);
    }

    @Override
    public String getErrorTitle() {
        return t("observe.error.storage.open.local.db");
    }


    @Override
    protected int computeStepCount0() {
        return 1;
    }

    @Override
    public boolean skip(ActionWithSteps<LoadingDataSourceContext> mainAction) {
        return getInputState().skip();
    }

    @Override
    public void doAction0(ActionWithSteps<LoadingDataSourceContext> mainAction) throws Exception {

        //do open local datasource
        ObserveSwingDataSource localDataSource = createLocalDataSource();

        // Let's check if the datasource is opened or not :
        // we could have close the datasource through the ui
        // If the datasource is closed, we try to open it
        if (!localDataSource.isOpen()) {
            try {

                localDataSource.open();
                actionContext.setLocalDataSource(localDataSource);
                incrementsProgress();
            } catch (Exception e) {
                log.error(t("observe.error.storage.could.not.load.local.db", e.getMessage()), e);
                actionContext.setLocalDataSource(null);
                throw e;
            }
        }
    }
}
