package fr.ird.observe.client.datasource.editor.api.loading.open;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.action.ActionStep;
import fr.ird.observe.client.datasource.api.action.ActionWithSteps;
import fr.ird.observe.client.datasource.api.action.FeedBackBuilderModel;
import fr.ird.observe.client.datasource.api.action.OpenLocalDataSourceFeedBackModel;
import fr.ird.observe.client.datasource.editor.api.loading.LoadingDataSourceContext;

import java.util.List;

public class OpenDataSourceAction extends ActionWithSteps<LoadingDataSourceContext> {

    public OpenDataSourceAction(LoadingDataSourceContext context) {
        super(context, false);
    }

    @Override
    protected List<? extends OpenDataSourceActionStepSupport> createSteps(LoadingDataSourceContext actionContext) {
        return List.of(new OpenDataSourceActionStep(actionContext, true));
    }

    @Override
    protected FeedBackBuilderModel createFeedBackModel(ClientConfig config, ActionStep<LoadingDataSourceContext> stepsFailed) {
        return new OpenLocalDataSourceFeedBackModel(config, stepsFailed, this.getContext().getModel().isLocal());
    }

}
