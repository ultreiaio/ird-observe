package fr.ird.observe.client.datasource.editor.api.loading.open;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.action.ActionWithSteps;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditorBodyContent;
import fr.ird.observe.client.datasource.editor.api.loading.LoadingDataSourceContext;
import fr.ird.observe.client.datasource.editor.api.wizard.DataSourceHelper;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static io.ultreia.java4all.i18n.I18n.t;

public class OpenDataSourceActionStep extends OpenDataSourceActionStepSupport {
    private static final Logger log = LogManager.getLogger(OpenDataSourceActionStep.class);

    public OpenDataSourceActionStep(LoadingDataSourceContext actionContext, boolean needFeedBackOnFail) {
        super(actionContext, true, needFeedBackOnFail);
    }

    @Override
    public String getErrorTitle() {
        return t("observe.error.storage.open.local.db");
    }

    @Override
    protected int computeStepCount0() {
        int stepsCount = 0;

        // open new data source
        stepsCount++;

        if (actionContext.getModel().getChooseDb().getInitModel().isOnConnectModeLocal()) {
            // use local data source (requires migrate)
            stepsCount += 2;
        }

        ClientConfig config = actionContext.getConfig();
        int openIds = config.getNavigationEditModelCount();
        if (openIds > 0) {
            stepsCount += 1 + openIds;
        }
        stepsCount += actionContext.getFloatingObjectPresets().size();
        return stepsCount;
    }

    @Override
    public boolean skip(ActionWithSteps<LoadingDataSourceContext> mainAction) {
        return getInputState().skip();
    }

    @Override
    public void doAction0(ActionWithSteps<LoadingDataSourceContext> mainAction) throws Exception {

        StorageUIModel model = actionContext.getModel();
        ClientConfig config = model.getClientConfig();
        ObserveDataSourcesManager dataSourcesManager = model.getDataSourcesManager();

        ObserveSwingDataSource newDataSource = DataSourceHelper.initDataSourceFromModel(model);
        newDataSource.setProgressModel(actionContext.getProgressModel());

        DataSourceInitModel initModel = model.getChooseDb().getInitModel();
        boolean useLocalDataSource = initModel.isOnConnectMode() && initModel.isOnConnectModeLocal();

        if (useLocalDataSource) {
            // try an explicit data source migration

            ObserveDataSourceInformation dataSourceInformation;
            try {
                dataSourceInformation = newDataSource.checkCanConnect(false);
                incrementsProgress();
            } catch (Exception e) {
                log.error(String.format("could not get local data source information: %s", newDataSource), e);
                throw e;
            }
            //FIXME Ask user to migrate or cancel
            try {
                newDataSource.migrateData(dataSourceInformation, config.getModelVersion());
                incrementsProgress();
            } catch (Exception e) {
                log.error(String.format("could not migrate local data source: %s", newDataSource), e);
                throw e;
            }
        }
        actionContext.getMainUI().getMainUIBodyContentManager().getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class).prepareMainStorage(newDataSource);

        try {
            if (initModel.isOnCreateMode()) {
                // do create local data source
                SqlScript dump = DataSourceHelper.getCreationConfigurationDto(model);
                newDataSource.createFromDump(dump);
            } else {
                // open data source
                newDataSource.open();
            }
            log.info(String.format("Main storage opened %s", newDataSource.getLabel()));
            incrementsProgress();
            actionContext.getMainUI().changeBodyContent(DataSourceEditor.class);
        } catch (Exception e) {
            log.error(String.format("could not open data source: %s", newDataSource), e);
            dataSourcesManager.setMainDataSource(null);
            throw e;
        }
    }
}
