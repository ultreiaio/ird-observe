package fr.ird.observe.client.datasource.editor.api.menu;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.main.MainUIModel;
import fr.ird.observe.client.main.body.MainUIBodyContentManager;
import org.nuiton.jaxx.runtime.spi.UIHandler;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class DataSourceEditorMenuHandler implements UIHandler<DataSourceEditorMenu>, WithClientUIContextApi {

    @Override
    public void beforeInit(DataSourceEditorMenu ui) {
        MainUIModel mainUIModel = ui.getContextValue(MainUIModel.class);
        MainUIBodyContentManager mainUIBodyContentManager = ui.getContextValue(MainUIBodyContentManager.class);
        ObserveDataSourcesManager dataSourcesManager = getDataSourcesManager();
        ui.setContextValue(new DataSourceEditorMenuModel(mainUIModel, dataSourcesManager, mainUIBodyContentManager));
    }
}
