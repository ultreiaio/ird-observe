package fr.ird.observe.client.datasource.editor.api.menu;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.main.MainUIModel;
import fr.ird.observe.client.main.body.MainUIBodyContentManager;
import fr.ird.observe.client.main.body.NoBodyContentComponent;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.Icon;
import javax.swing.JComponent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
@GenerateJavaBeanDefinition
public class DataSourceEditorMenuModel extends AbstractJavaBean {

    private static final Logger log = LogManager.getLogger(DataSourceEditorMenuModel.class.getName());
    private static final Icon DB_NONE_ICON = UIHelper.getUIManagerActionIcon("db-none");
    private final MainUIModel mainUIModel;
    private final ObserveDataSourcesManager dataSourcesManager;
    private final MainUIBodyContentManager mainUIBodyContentManager;

    private boolean storageCloseEnabled;
    private boolean storageReloadEnabled;
    private boolean storageImportEnabled;
    private boolean storageImportAvdthFileEnabled = false;
    private boolean storageSaveEnabled;
    private boolean startServerVisible;
    private boolean storageEnabled;
    private boolean h2WebServer;
    private boolean storageStatusEnabled;
    private String storageStatusText;
    private String storageStatusTip;
    private Icon storageStatusIcon;
    private boolean adjusting;
    private boolean showOptional;

    public DataSourceEditorMenuModel(MainUIModel mainUIModel, ObserveDataSourcesManager dataSourcesManager, MainUIBodyContentManager mainUIBodyContentManager) {
        this.mainUIModel = mainUIModel;
        this.dataSourcesManager = dataSourcesManager;
        this.mainUIBodyContentManager = mainUIBodyContentManager;
    }

    public MainUIModel getMainUIModel() {
        return mainUIModel;
    }

    public MainUIBodyContentManager getMainUIBodyContentManager() {
        return mainUIBodyContentManager;
    }

    public ObserveDataSourcesManager getDataSourcesManager() {
        return dataSourcesManager;
    }

    public ClientConfig getClientConfig() {
        return mainUIModel.getClientConfig();
    }

    public BusyModel getBusyModel() {
        return mainUIModel.getBusyModel();
    }

    public boolean isShowOptional() {
        return showOptional;
    }

    public void setShowOptional(boolean showOptional) {
        boolean oldValue = this.showOptional;
        this.showOptional = showOptional;
        firePropertyChange("showOptional", oldValue, showOptional);
    }

    public void init() {
        boolean showOptional = getClientConfig().isShowDataSourceOptionalActions();
        setShowOptional(showOptional);

        addPropertyChangeListener(evt -> {
            if (adjusting) {
                return;
            }
            log.info(String.format("Data source editor menu model property %s changed, reload main ui model.", evt.getPropertyName()));
            reload();
        });
    }

    public void reload() {

        ClientConfig config = getClientConfig();
        log.debug(String.format("Reload ui model: %s", this));
        adjusting = true;

        try {

            boolean notBusy = !getBusyModel().isBusy();
            ObserveSwingDataSource mainDataSource = getDataSourcesManager().getMainDataSource();
            boolean mainStorageOpened = mainDataSource != null;
            boolean mainStorageLocal = mainStorageOpened && mainDataSource.isLocal();
            boolean mainStorageSql = mainStorageOpened && mainDataSource.isRemote();

            Class<? extends JComponent> bodyContentType = getMainUIBodyContentManager().getCurrentBodyType();

            setStorageEnabled(MainUIModel.acceptBodyType(bodyContentType, notBusy, DataSourceEditor.class.getName(), NoBodyContentComponent.class.getName()));

            setStorageStatusText(updateStorageStatusText());
            setStorageStatusTip(updateStorageStatusToolTipText());
            setStorageStatusIcon(updateStorageStatusIcon());
            setStorageStatusEnabled(MainUIModel.acceptBodyType(bodyContentType, notBusy, DataSourceEditor.class.getName(), NoBodyContentComponent.class.getName()));
            setStorageCloseEnabled(MainUIModel.acceptBodyType(bodyContentType, mainStorageOpened, DataSourceEditor.class.getName()));
            setStorageReloadEnabled(MainUIModel.acceptBodyType(bodyContentType, mainStorageOpened, DataSourceEditor.class.getName()));

            setStartServerVisible(MainUIModel.acceptBodyType(bodyContentType, config.isLocalStorageExist() && notBusy, NoBodyContentComponent.class.getName()));
            setStorageImportEnabled(MainUIModel.acceptBodyType(bodyContentType, DataSourceEditor.class.getName(), NoBodyContentComponent.class.getName()));
            setStorageSaveEnabled(MainUIModel.acceptBodyType(bodyContentType, mainStorageOpened, DataSourceEditor.class.getName()));
            // only authorize for local data source
            setStorageImportAvdthFileEnabled(MainUIModel.acceptBodyType(bodyContentType, mainStorageLocal || mainStorageSql, DataSourceEditor.class.getName()));
        } finally {
            adjusting = false;
        }
    }


    public boolean isH2WebServer() {
        return h2WebServer;
    }

    public void setH2WebServer(boolean h2WebServer) {
        this.h2WebServer = h2WebServer;
        firePropertyChange("h2WebServer", null, h2WebServer);
    }

    public boolean isStorageEnabled() {
        return storageEnabled;
    }

    public void setStorageEnabled(boolean storageEnabled) {
        this.storageEnabled = storageEnabled;
        firePropertyChange("storageEnabled", null, storageEnabled);
    }

    public boolean isStorageCloseEnabled() {
        return storageCloseEnabled;
    }

    public void setStorageCloseEnabled(boolean storageCloseEnabled) {
        this.storageCloseEnabled = storageCloseEnabled;
        firePropertyChange("storageCloseEnabled", null, storageCloseEnabled);
    }

    public boolean isStorageReloadEnabled() {
        return storageReloadEnabled;
    }

    public void setStorageReloadEnabled(boolean storageReloadEnabled) {
        this.storageReloadEnabled = storageReloadEnabled;
        firePropertyChange("storageReloadEnabled", null, storageReloadEnabled);
    }

    public boolean isStorageImportEnabled() {
        return storageImportEnabled;
    }

    public void setStorageImportEnabled(boolean storageImportEnabled) {
        this.storageImportEnabled = storageImportEnabled;
        firePropertyChange("storageImportEnabled", null, storageImportEnabled);
    }

    public boolean isStorageImportAvdthFileEnabled() {
        return storageImportAvdthFileEnabled;
    }

    public void setStorageImportAvdthFileEnabled(boolean storageImportAvdthFileEnabled) {
        boolean oldValue = this.storageImportAvdthFileEnabled;
        this.storageImportAvdthFileEnabled = storageImportAvdthFileEnabled;
        firePropertyChange("storageImportAvdthFileEnabled", oldValue, storageImportAvdthFileEnabled);
    }

    public boolean isStorageSaveEnabled() {
        return storageSaveEnabled;
    }

    public void setStorageSaveEnabled(boolean storageSaveEnabled) {
        this.storageSaveEnabled = storageSaveEnabled;
        firePropertyChange("storageSaveEnabled", null, storageSaveEnabled);
    }

    public boolean isStartServerVisible() {
        return startServerVisible;
    }

    public void setStartServerVisible(boolean startServerVisible) {
        this.startServerVisible = startServerVisible;
        firePropertyChange("startServerVisible", null, startServerVisible);
    }

    public boolean isStorageStatusEnabled() {
        return storageStatusEnabled;
    }

    public void setStorageStatusEnabled(boolean storageStatusEnabled) {
        this.storageStatusEnabled = storageStatusEnabled;
        firePropertyChange("storageStatusEnabled", null, storageStatusEnabled);
    }

    public String getStorageStatusText() {
        return storageStatusText;
    }

    public void setStorageStatusText(String storageStatusText) {
        this.storageStatusText = storageStatusText;
        firePropertyChange("storageStatusText", null, storageStatusText);
    }

    public String getStorageStatusTip() {
        return storageStatusTip;
    }

    public void setStorageStatusTip(String storageStatusTip) {
        this.storageStatusTip = storageStatusTip;
        firePropertyChange("storageStatusTip", null, storageStatusTip);
    }

    public Icon getStorageStatusIcon() {
        return storageStatusIcon;
    }

    public void setStorageStatusIcon(Icon storageStatusIcon) {
        this.storageStatusIcon = storageStatusIcon;
        firePropertyChange("storageStatusIcon", null, storageStatusIcon);
    }

    protected Icon updateStorageStatusIcon() {
        return getDataSourcesManager().getOptionalMainDataSource().map(ObserveSwingDataSource::getIcon).orElse(DB_NONE_ICON);
    }

    protected String updateStorageStatusText() {
        return getDataSourcesManager().getOptionalMainDataSource().map(ObserveSwingDataSource::getLabel).orElse(t("observe.ui.message.db.none.loaded"));
//        return ObserveKeyStrokesSupport.suffixTextWithKeyStroke(text, ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_POPUP_MENU);
    }

    protected String updateStorageStatusToolTipText() {
        return getDataSourcesManager().getOptionalMainDataSource().map(s -> t("observe.ui.message.loaded.tip", s.getLabel())).orElse(t("observe.ui.message.db.none.loaded.tip"));
//        return ObserveKeyStrokesSupport.suffixTextWithKeyStroke(text, ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_POPUP_MENU);
    }

}
