package fr.ird.observe.client.datasource.editor.api.menu.actions;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageStep;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUI;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUILauncher;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class ChangeStorageAction extends LoadStorageActionSupport {

    private static final Logger log = LogManager.getLogger(ChangeStorageAction.class);

    private final DataSourceInitMode initMode;
    private final Set<DataSourceConnectMode> authorizedConnectModes;
    private final String title;

    @SuppressWarnings("unused")
    public ChangeStorageAction() {
        this(null, null, null);
    }

    public ChangeStorageAction(DataSourceInitMode initMode, Set<DataSourceConnectMode> authorizedConnectModes, String title) {
        super(t("observe.ui.action.change.storage"), t("observe.ui.action.change.storage.tip"), "db-change", 'C');
        this.initMode = initMode == null ? DataSourceInitMode.CONNECT : initMode;
        this.authorizedConnectModes = authorizedConnectModes == null ? EnumSet.noneOf(DataSourceConnectMode.class) : authorizedConnectModes;
        this.title = title;
    }

    @Override
    protected String getTitle() {
        return t(title);
    }

    @Override
    protected void init(StorageUI ui) {
        log.debug(String.format("Incoming db mode : %s", authorizedConnectModes.stream().map(DataSourceConnectMode::name).collect(Collectors.joining(", "))));
        StorageUIModel model = ui.getModel();
        ChooseDbModel chooseDb = model.getChooseDb();
        DataSourceInitModel initModel = model.getInitModel();
        if (!authorizedConnectModes.isEmpty()) {
            log.info(String.format("will use incoming mode %s", authorizedConnectModes.stream().map(DataSourceConnectMode::name).collect(Collectors.joining(", "))));

            model.setExcludeSteps(Arrays.asList(StorageStep.SELECT_DATA,
//                                                StorageStep.BACKUP,
                                                StorageStep.CONFIG_REFERENTIAL,
                                                StorageStep.CONFIG_DATA,
                                                StorageStep.ROLES));
            initModel.setAuthorizedInitModes(EnumSet.of(initMode));
            initModel.setAuthorizedConnectModes(EnumSet.copyOf(authorizedConnectModes));
            chooseDb.setInitMode(initMode);
            chooseDb.setConnectMode(authorizedConnectModes.stream().findFirst().orElse(null));

        } else {
            boolean localOpened = getDataSourcesManager().getOptionalMainDataSource().map(ObserveSwingDataSource::isLocal).orElse(false);
            if (localOpened) {
                log.debug("Can not use local db (already opened)");
                initModel.getAuthorizedConnectModes().remove(DataSourceConnectMode.LOCAL);
            }
        }
        chooseDb.updateEditConfig();
        model.updateUniverse();
    }

    @Override
    protected StorageUILauncher startWizard(ObserveMainUI mainUI) {
        log.info(String.format("Start change storage with dbMode: %s", authorizedConnectModes.stream().map(DataSourceConnectMode::name).collect(Collectors.joining(", "))));
        return super.startWizard(mainUI);
    }

}
