package fr.ird.observe.client.datasource.editor.api.menu.actions;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageStep;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUI;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.EnumSet;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class ImportStorageFromFileAction extends LoadStorageActionSupport {

    private static final Logger log = LogManager.getLogger(ImportStorageFromFileAction.class);

    public ImportStorageFromFileAction() {
        super(t("observe.ui.action.load.from.file"), t("observe.ui.action.load.from.file.tip"), "local-import", 'I');
    }

    @Override
    protected String getTitle() {
        return t("observe.ui.title.import.localDB");
    }

    @Override
    protected void init(StorageUI ui) {
        StorageUIModel model = ui.getModel();

        model.setDumpFile(getClientConfig().getImportDirectory());

        ChooseDbModel chooseDb = model.getChooseDb();
        DataSourceInitModel initModel = model.getInitModel();
        initModel.setAuthorizedInitModes(EnumSet.of(DataSourceInitMode.CREATE));
        initModel.setAuthorizedCreateModes(EnumSet.of(DataSourceCreateMode.IMPORT_EXTERNAL_DUMP));
        chooseDb.setInitMode(DataSourceInitMode.CREATE);
        chooseDb.setConnectMode(DataSourceConnectMode.LOCAL);
        chooseDb.setCreateMode(DataSourceCreateMode.IMPORT_EXTERNAL_DUMP);

        chooseDb.updateEditConfig();

        if (chooseDb.isLocalStorageExist()) {
            // sauvegarde base locale possible
            model.setSteps(StorageStep.CONFIG, StorageStep.BACKUP, StorageStep.CONFIRM);
            // et requise par défaut
            model.setDoBackup(true);
        } else {
            model.setSteps(StorageStep.CONFIG, StorageStep.CONFIRM);
        }
    }

    @Override
    protected void doChangeStorage(StorageUI ui) {
        log.info("Start importing new datasource from backup file...");
        super.doChangeStorage(ui);
    }
}
