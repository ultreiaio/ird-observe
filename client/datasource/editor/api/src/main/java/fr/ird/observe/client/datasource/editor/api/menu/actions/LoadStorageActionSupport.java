package fr.ird.observe.client.datasource.editor.api.menu.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.loading.LoadingDataSourceContext;
import fr.ird.observe.client.datasource.editor.api.loading.close.CloseDataSourceAction;
import fr.ird.observe.client.datasource.editor.api.loading.open.OpenDataSourceAction;
import fr.ird.observe.client.datasource.editor.api.menu.DataSourceEditorMenu;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUI;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUILauncher;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.body.NoBodyContentComponent;
import org.nuiton.jaxx.runtime.JAXXContext;

import java.awt.event.ActionEvent;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public abstract class LoadStorageActionSupport extends DataSourceEditorMenuActionSupport implements Runnable, WithClientUIContextApi {

    public LoadStorageActionSupport(String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    protected abstract String getTitle();

    protected abstract void init(StorageUI ui);

    @Override
    protected void doActionPerformed(ActionEvent event, DataSourceEditorMenu ui) {
        run();
    }

    @Override
    public void run() {

        ObserveMainUI mainUI = getMainUI();
        boolean canContinue = mainUI.changeBodyContent(NoBodyContentComponent.class);
        if (!canContinue) {
            return;
        }
        startWizard(mainUI).start();

    }

    protected StorageUILauncher startWizard(ObserveMainUI mainUI) {
        JAXXContext rootContext = ui == null ? mainUI : ui;
        return new StorageUILauncher(rootContext, mainUI, getTitle()) {

            boolean withPreviousDatasource;

            @Override
            protected void init(StorageUI ui) {
                super.init(ui);
                withPreviousDatasource = getDataSourcesManager().getOptionalMainDataSource().isPresent();
                LoadStorageActionSupport.this.init(ui);
            }

            @Override
            public void doAction(StorageUI ui) {
                super.doAction(ui);
                doChangeStorage(ui);
            }

            @Override
            public void doClose(StorageUI ui, boolean wasCanceled) {
                super.doClose(ui, wasCanceled);
                if (wasCanceled && withPreviousDatasource) {
                    mainUI.changeBodyContent(DataSourceEditor.class);
                }
            }
        };
    }

    protected void doChangeStorage(StorageUI ui) {

        StorageUIModel model = ui.getModel();

        model.getProgressModel().installUI(ui.getCONFIRM().getProgressBar(), false);

        LoadingDataSourceContext actionContext = new LoadingDataSourceContext(
                model,
                getMainUI(),
                getClientConfig(),
                getDataSourcesManager(),
                getFloatingObjectPresetsManager()
        );

        CloseDataSourceAction closeDataSource = new CloseDataSourceAction(actionContext);
        OpenDataSourceAction openDataSource = new OpenDataSourceAction(actionContext);
        closeDataSource.run();

        if (closeDataSource.notFail()) {
            openDataSource.run();
        }

    }

}
