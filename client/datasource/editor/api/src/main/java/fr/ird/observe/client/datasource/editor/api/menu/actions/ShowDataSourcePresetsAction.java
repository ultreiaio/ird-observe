package fr.ird.observe.client.datasource.editor.api.menu.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.presets.RemotePresetsUI;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.actions.MainUIActionSupport;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 20/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ShowDataSourcePresetsAction extends MainUIActionSupport {

    public ShowDataSourcePresetsAction() {
        super(t("observe.ui.action.connexions"), t("observe.ui.action.connexions.tip"), null, 'G');
        setIcon(DataSourceConnectMode.REMOTE.getIcon());
    }

    @Override
    protected void doActionPerformed(ActionEvent event, ObserveMainUI ui) {
        ui.changeBodyContent(RemotePresetsUI.class);
    }
}
