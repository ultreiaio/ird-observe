package fr.ird.observe.client.datasource.editor.api.menu.actions;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.actions.CopyAuthenticationToken;
import fr.ird.observe.client.datasource.editor.api.menu.DataSourceEditorMenu;
import fr.ird.observe.client.datasource.editor.api.navigation.actions.ShowInformation;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class ShowStorageInfoAction extends DataSourceEditorMenuActionSupport implements WithClientUIContextApi {

    public ShowStorageInfoAction() {
        super(t("observe.ui.action.info.storage"), t("observe.ui.action.info.storage.tip"), "information", 'O');
    }

    @Override
    protected void doActionPerformed(ActionEvent event, DataSourceEditorMenu ui) {

        ObserveSwingDataSource source = getDataSourcesManager().getOptionalMainDataSource().orElse(null);
        JPanel content = ShowInformation.createContent(source);
        boolean withTokenOption = source != null && source.isServer();
        List<Object> optionsList = new LinkedList<>();
        optionsList.add(t("observe.ui.choice.quit"));
        CopyAuthenticationToken action = null;
        if (withTokenOption) {
            optionsList.add(0, (action = new CopyAuthenticationToken(source)).getValue(Action.NAME));
        }
        Object[] options = optionsList.toArray();
        String defaultOption = (String) options[0];
        JOptionPane optionPane = new JOptionPane(content, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, defaultOption);
        if (withTokenOption) {
            Objects.requireNonNull(action).setPopup(optionPane);
            JButton jButton = UsageUIHandlerSupport.findButton(optionPane, defaultOption);
            Objects.requireNonNull(jButton).setAction(action);
            optionPane.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentShown(ComponentEvent e) {
                    SwingUtilities.invokeLater(jButton::grabFocus);
                }
            });
        }
        askToUser(optionPane, t("observe.ui.title.storage.info"), options);
    }
}
