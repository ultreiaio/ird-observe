package fr.ird.observe.client.datasource.editor.api.menu.actions;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.editor.api.menu.DataSourceEditorMenu;
import fr.ird.observe.client.datasource.h2.server.H2ServerBodyContent;
import fr.ird.observe.client.datasource.h2.server.H2ServerUI;
import fr.ird.observe.client.main.ObserveMainUI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.tools.Server;
import org.nuiton.jaxx.widgets.error.ErrorDialogUI;

import java.awt.event.ActionEvent;
import java.io.File;
import java.sql.SQLException;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class StartServerModeAction extends DataSourceEditorMenuActionSupport implements Runnable, WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(StartServerModeAction.class);

    public StartServerModeAction() {
        super(t("observe.ui.action.start.server.mode"), t("observe.ui.action.start.server.mode.tip"), "db-start-server", 'S');
    }

    @Override
    protected void doActionPerformed(ActionEvent event, DataSourceEditorMenu ui) {
        run();
    }

    @Override
    public void run() {

        log.info("Will start server mode...");
        ClientConfig config = ui.getUiModel().getClientConfig();
        File dbDirectory = new File(config.getLocalDBDirectory(), "obstuna");
        Integer port = config.getH2ServerPort();
        Server server = null;
        try {

            server = Server.createTcpServer("-tcp",
                                            "-tcpAllowOthers",
                                            "-ifExists",
                                            "-baseDir", dbDirectory.getAbsolutePath(),
                                            "-tcpDaemon",
                                            "-tcpPort",
                                            String.valueOf(port));

            log.info(String.format("Starting H2 server at %s", server.getURL()));
            server.start();

        } catch (SQLException e) {
            log.error("Could not start h2 server ", e);
            ErrorDialogUI.showError(e);
        }

        if (server != null) {
            ObserveMainUI mainUI = getMainUI();
            mainUI.getMainUIBodyContentManager().getBodyTyped(H2ServerUI.class, H2ServerBodyContent.class).setServer(server);
            mainUI.changeBodyContent(H2ServerUI.class);
        }

    }

}
