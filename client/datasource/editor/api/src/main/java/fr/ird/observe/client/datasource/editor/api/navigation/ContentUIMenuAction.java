package fr.ird.observe.client.datasource.editor.api.navigation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import org.nuiton.jaxx.runtime.swing.action.JAXXObjectActionSupport;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created on 23/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentUIMenuAction extends AbstractAction implements MenuAction {

    private final Action action;
    private final JPopupMenu popup;
    private final JMenuItem editor;

    private boolean withKeyStroke;

    private static String getActionText(AbstractButton abstractButton) {
        String text = abstractButton.getText();
        if (text == null || text.equals(abstractButton.getName())) {
            text = abstractButton.getToolTipText();
        }
        Objects.requireNonNull(text, "Button with no text, nor tip, kind of strange, don't you think?");
        return text;
    }

    public static ContentUIMenuAction createWithKeyStroke(JPopupMenu popup, AbstractButton abstractButton) {
        ContentUIMenuAction contentUIMenuAction = new ContentUIMenuAction(popup, abstractButton);
        JAXXObjectActionSupport<?> action = (JAXXObjectActionSupport<?>) abstractButton.getAction();
        contentUIMenuAction.setKeyStroke(action.getKeyStroke());
        contentUIMenuAction.setWithKeyStroke(true);
        return contentUIMenuAction;
    }

    static KeyStroke getMenuKeyStroke(int count) {
        return KeyStroke.getKeyStroke("pressed " + (char) ('A' + count));
    }

    ContentUIMenuAction(JPopupMenu popup, AbstractButton abstractButton) {
        super(ObserveKeyStrokesSupport.removeKeyStroke(getActionText(Objects.requireNonNull(abstractButton))), abstractButton.getIcon());
        this.popup = popup;
        this.editor = new JMenuItem();
        String toolTipText = abstractButton.getToolTipText();
        if (toolTipText != null) {
            toolTipText = ObserveKeyStrokesSupport.removeKeyStroke(toolTipText);
            putValue(SHORT_DESCRIPTION, toolTipText);
            String name = (String) getValue(NAME);
            if (name != null && name.isEmpty()) {
                putValue(NAME, toolTipText);
            }
        }

        this.action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                abstractButton.doClick();
            }
        };
        this.action.setEnabled(abstractButton.isEnabled());
    }

    public boolean isWithKeyStroke() {
        return withKeyStroke;
    }

    public void setWithKeyStroke(boolean withKeyStroke) {
        this.withKeyStroke = withKeyStroke;
    }

    public void init() {
        initUI();
        String toolTipText = (String) getValue(SHORT_DESCRIPTION);
        KeyStroke keyStroke = (KeyStroke) getValue(ACCELERATOR_KEY);
        if (toolTipText != null && keyStroke != null) {
            toolTipText = ObserveKeyStrokesSupport.suffixTextWithKeyStroke(toolTipText, keyStroke);
            putValue(SHORT_DESCRIPTION, toolTipText);
        }
        editor.setAction(this);
        editor.setEnabled(action.isEnabled());
    }

    @Override
    public KeyStroke getMenuKeyStroke() {
        if (isWithKeyStroke()) {
            return (KeyStroke) getValue(ACCELERATOR_KEY);
        }
        int usedCount = (int) Arrays.stream(getPopupMenu().getSubElements()).filter(p -> !((ContentUIMenuAction) ((AbstractButton) p).getAction()).isWithKeyStroke()).count();
        return getMenuKeyStroke(usedCount);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        action.actionPerformed(e);
    }

    @Override
    public JPopupMenu getPopupMenu() {
        return popup;
    }

    @Override
    public AbstractButton getEditor() {
        return editor;
    }

    @Override
    public void setKeyStroke(KeyStroke keyStroke) {
        putValue(ACCELERATOR_KEY, keyStroke);
    }
}
