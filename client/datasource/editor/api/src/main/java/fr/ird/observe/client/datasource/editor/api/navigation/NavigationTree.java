package fr.ird.observe.client.datasource.editor.api.navigation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.navigation.tree.JXTreeWithNoSearch;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeSelectionModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.Arrays;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class NavigationTree extends JXTreeWithNoSearch implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(NavigationTree.class);

    public NavigationTree() {
        super(new NavigationTreeModel());
        setLargeModel(true);
        setRootVisible(false);
    }

    @Override
    public void updateUI() {
        setSelectionModel(new NavigationTreeSelectionModel(getBusyModel()));
        super.updateUI();
    }

    @Override
    public NavigationTreeModel getModel() {
        return (NavigationTreeModel) super.getModel();
    }

    @Override
    public NavigationTreeSelectionModel getSelectionModel() {
        return (NavigationTreeSelectionModel) super.getSelectionModel();
    }

    public NavigationNode getSelectedNode() {
        return isSelectionEmpty() || getSelectionPath() == null ? null : (NavigationNode) getSelectionPath().getLastPathComponent();
    }

    /**
     * Selects the given {@code node} in the tree without doing any check on previous content loaded.
     * <p>
     * This method should be used only programmatically.
     *
     * @param node the node to select
     */
    public void reloadAndSelectSafeNode(NavigationNode node) {
        log.info(String.format("reload and to select safe node [%s]", node));
        node.reloadNodeData();
        TreePath path = new TreePath(getModel().getPathToRoot(node));
        getSelectionModel().setSkipCheckPreviousContent(true);
        try {
            setSelectionPath(path);
        } finally {
            getSelectionModel().setSkipCheckPreviousContent(false);
        }
        SwingUtilities.invokeLater(() -> scrollPathToVisible(path));
    }

    /**
     * Selects the given {@code node} in the tree without doing any check on previous content loaded.
     * <p>
     * This method should be used only programmatically.
     *
     * @param node the node to select
     */
    public void selectSafeNode(TreeNode node) {
        log.info(String.format("try to select safe node [%s]", node));
        TreePath path = new TreePath(getModel().getPathToRoot(node));
        getSelectionModel().setSkipCheckPreviousContent(true);
        getSelectionModel().setSkipBusyModel(true);
        try {
            setSelectionPath(path);
        } finally {
            getSelectionModel().setSkipCheckPreviousContent(false);
            getSelectionModel().setSkipBusyModel(false);
        }
        SwingUtilities.invokeLater(() -> scrollPathToVisible(path));
    }

    public void reSelectSafeNodeThen(TreeNode node, Runnable then) {
        log.info(String.format("try to reselect safe node [%s]", node));
        TreePath path = new TreePath(getModel().getPathToRoot(node));
        getSelectionModel().setSkipBusyModel(true);
        try {
            getSelectionModel().clearSelection();
            getSelectionModel().setSkipCheckPreviousContent(true);
            try {
                setSelectionPath(path);
            } finally {
                getSelectionModel().setSkipCheckPreviousContent(false);
            }
        } finally {
            getSelectionModel().setSkipBusyModel(false);
        }
        SwingUtilities.invokeLater(() -> scrollPathToVisible(path));
        if (then != null) {
            then.run();
        }
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2749
        resetHorizontalScrollBar();
    }

    public void reSelectSafeNode(TreeNode node) {
        reSelectSafeNodeThen(node, null);
    }

    public boolean isRowSelected(int requiredRow) {
        int[] selectedRows = getSelectionRows();
        if (selectedRows != null) {
            return Arrays.stream(selectedRows).anyMatch(selectedRow -> requiredRow == selectedRow);
        }
        return false;
    }

    /**
     * Selects the given {@code node} in the registered tree.
     *
     * @param node the node to select
     */
    public void selectNode(NavigationNode node) {
        if (node == null) {
            log.error("Can't load null node.", new NullPointerException());
            return;
        }
        log.info(String.format("try to select node [%s]", node));
        TreePath path = new TreePath(getModel().getPathToRoot(node));
        setSelectionPath(path);
        SwingUtilities.invokeLater(() -> scrollPathToVisible(path));
        resetHorizontalScrollBar();
    }

    /**
     * Selects the given {@code node} in the registered tree.
     *
     * @param editNode the node to select
     */
    public void selectNode(IdNode<?> editNode) {
        NavigationNode node = getModel().getNodeFromModelNode(editNode);
        if (node != null) {
            selectNode(node);
        }
    }

    /**
     * Select initial node when data source just opened.
     */
    public void selectInitialNode() {
        NavigationNode selectedNode = getModel().getInitialNode();
        log.info(String.format("Initial selected node: %s", selectedNode));
        if (selectedNode != null) {
            reSelectSafeNode(selectedNode);
        }
        SwingUtilities.invokeLater(this::requestFocusInWindow);
    }

    /**
     * Select initial node when data source just opened.
     */
    public void reselectInitialNode(String[] paths) {
        try {
            NavigationNode node = paths == null ? null : getModel().getNodeFromPath(paths, true);
            if (node == null) {
                selectInitialNode();
            } else {
                selectSafeNode(node);
            }
        } catch (Exception e) {
            log.error("Could not re select initial node", e);
        }
        SwingUtilities.invokeLater(this::requestFocusInWindow);
    }

    public RootNavigationNode getRootNode() {
        return getModel().getRoot();
    }

    public void selectFirstNode() {
        setSelectionRow(0);
    }

    public NavigationNode getNodeForRow(int row) {
        return (NavigationNode) getPathForRow(row).getLastPathComponent();
    }

    public void installShowPopupHandler(NavigationTreeShowPopupHandler navigationTreeShowPopupHandler) {
        addKeyListener(navigationTreeShowPopupHandler);
        addMouseListener(navigationTreeShowPopupHandler);
    }

    public void installSelectionListener(NavigationTreeSelectionListenerImpl selectionListener) {
        getSelectionModel().addNavigationTreeSelectionListener(selectionListener);
        addTreeWillExpandListener(selectionListener);
        addTreeSelectionListener(selectionListener);
        addMouseListener(selectionListener);
    }

    public void resetHorizontalScrollBar() {
        JScrollPane scrollPane = (JScrollPane) SwingUtilities.getAncestorOfClass(JScrollPane.class, this);
        JScrollBar horizontalScrollBar = scrollPane.getHorizontalScrollBar();
        SwingUtilities.invokeLater(() -> horizontalScrollBar.setValue(0));
    }
}
