package fr.ird.observe.client.datasource.editor.api.navigation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import org.jdesktop.swingx.renderer.CellContext;
import org.jdesktop.swingx.renderer.DefaultTreeRenderer;
import org.jdesktop.swingx.renderer.IconValue;
import org.jdesktop.swingx.renderer.LabelProvider;
import org.jdesktop.swingx.renderer.StringValue;

import javax.swing.Icon;
import java.awt.Font;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class NavigationTreeCellRenderer extends DefaultTreeRenderer {

    private static class NavigationTreeStringIconValue implements StringValue, IconValue {

        @Override
        public Icon getIcon(Object value) {
            NavigationNode n = (NavigationNode) value;
            return n == null ? null : n.getIcon();
        }

        @Override
        public String getString(Object value) {
            NavigationNode n = (NavigationNode) value;
            return n == null ? null : n.toString();
        }
    }

    private static class NavigationTreeLabelProvider extends LabelProvider {

        private Font defaultFont;

        public NavigationTreeLabelProvider() {
            super(new NavigationTreeStringIconValue());
        }

        @Override
        protected void configureVisuals(CellContext context) {
            NavigationNode node = (NavigationNode) context.getValue();
//            if (!context.isSelected()) {
            defaultVisuals.setForeground(node.getColor());
//            }
            super.configureVisuals(context);
            if (defaultFont == null) {
                defaultFont = rendererComponent.getFont();
            }
            rendererComponent.setFont(node.getNodeFont(defaultFont));
            rendererComponent.setBorder(null);
        }
    }

    public NavigationTreeCellRenderer() {
        super(new NavigationTreeLabelProvider());
    }
}
