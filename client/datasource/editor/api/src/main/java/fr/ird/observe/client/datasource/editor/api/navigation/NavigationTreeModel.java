package fr.ird.observe.client.datasource.editor.api.navigation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.select.*;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.navigation.tree.GroupByHelper;
import fr.ird.observe.navigation.tree.NavigationResult;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.services.service.NavigationService;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class NavigationTreeModel extends DefaultTreeModel implements WithClientUIContextApi, JavaBean {

    private static final Logger log = LogManager.getLogger(NavigationTreeModel.class);
    public static final String PROPERTY_NAVIGATION_RESULT = "navigationResult";

    private final NavigationTreeConfig config;
    private final GroupByHelper groupByHelper;
    private final PropertyChangeSupport pcs;

    NavigationTreeModel() {
        super(new RootNavigationNode());
        this.config = new NavigationTreeConfig();
        groupByHelper = new GroupByHelper(ObserveBusinessProject.get());
        this.pcs = new PropertyChangeSupport(this);
    }

    public GroupByHelper getGroupByHelper() {
        return groupByHelper;
    }

    public NavigationTreeConfig getConfig() {
        return config;
    }

    @Override
    public RootNavigationNode getRoot() {
        return (RootNavigationNode) super.getRoot();
    }

    @Override
    public void setRoot(TreeNode root) {
        log.info(String.format("setRoot init: %s", root));
        RootNavigationNode oldRoot = getRoot();
        if (Objects.equals(root, oldRoot)) {
            throw new IllegalStateException("Can't set the same root node twice");
        }
        if (oldRoot != null && oldRoot.isLoaded()) {
            // clean old root
            log.info(String.format("Clean old root node: %s", oldRoot));
            oldRoot.clear();
        }
        log.info(String.format("Opening new root node: %s", root));
        RootNavigationNode node = (RootNavigationNode) root;
        node.open();

        log.info(String.format("Root node is open: %s", root));
        super.setRoot(root);
        // now can use tree model inside node
        node.getInitializer().setTreeModel(this);
        log.info(String.format("setRoot done: %s", root));
    }

    @Override
    public void clear() {
        setRoot(new RootNavigationNode());
    }

    /**
     * @return Select initial node when data source just opened.
     */
    public NavigationNode getInitialNode() {
        NavigationNode selectedNode = null;
        RootNavigationNode rootNode = getRoot();
        for (SelectNodeStrategy selectNodeStrategy : getSelectNodeStrategies(rootNode.getInitializer().getRequest())) {
            Optional<NavigationNode> optionalSelectedNode;
            try {
                optionalSelectedNode = selectNodeStrategy.apply(rootNode);
            } catch (Exception e) {
                log.error("can't get initial node from strategy: " + selectNodeStrategy, e);
                optionalSelectedNode = Optional.empty();
            }
            if (optionalSelectedNode.isPresent()) {
                selectedNode = optionalSelectedNode.get();
                break;
            }
        }
        return selectedNode;
    }

    public NavigationNode getNodeFromPath(String[] model, boolean checkGroupBy) {
        Optional<NavigationNode> node = new SelectNodesByPath(model, checkGroupBy).apply(getRoot());
        return node.orElse(null);
    }

    public NavigationNode getNodeFromModelNode(IdNode<?> model) {
        Optional<NavigationNode> node = new SelectNodesByModelNode(model).apply(getRoot());
        return node.orElse(null);
    }

    public List<SelectNodeStrategy> getSelectNodeStrategies(ToolkitTreeFlatModelRootRequest request) {
        Project navigationEdit = getObserveEditModel();
        ObserveSwingDataSource mainDataSource = getDataSourcesManager().getMainDataSource();
        boolean canReadData = mainDataSource.canReadData();
        List<SelectNodeStrategy> result = new LinkedList<>();
        String[] path = getClientConfig().getNavigationLastSelectedPath();
        result.add(new SelectNodesByPath(path, true));
        if (canReadData && request.isLoadData()) {
            String moduleName = request.getModuleName();
            switch (moduleName) {
                case "ps":
                    if (navigationEdit.getPs().isEnabled()) {
                        result.add(new SelectNodesByModel(navigationEdit.getPs()));
                    }
                    break;
                case "ll":
                    if (navigationEdit.getLl().isEnabled()) {
                        result.add(new SelectNodesByModel(navigationEdit.getLl()));
                    }

                    break;
            }
        }
        result.add(new SelectNodesDefault());
        return result;
    }

    public void populate() {
        NavigationResult newNavigationResult = createOrUpdateNavigationResult(null);
        RootNavigationNode root = RootNavigationNode.create(newNavigationResult);
        setRoot(root);
        fireNavigationResultChanged();
    }

    public NavigationResult updateNavigationResult() {
        RootNavigationInitializer initializer = getRoot().getInitializer();
        NavigationResult navigationResult = createOrUpdateNavigationResult(initializer.getNavigationResult());
        getRoot().getContext().initReference(navigationResult.getData());
        initializer.setNavigationResult(navigationResult);
        // update navigation ui
        fireNavigationResultChanged();
        return navigationResult;
    }

    @Override
    public JavaBeanDefinition javaBeanDefinition() {
        return null;
    }

    @Override
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    protected NavigationResult createOrUpdateNavigationResult(NavigationResult navigationResult) {
        NavigationService navigationService = getDataSourcesManager().getMainDataSource().getNavigationService();
        Locale locale = getDecoratorService().getReferentialLocale().getLocale();
        ToolkitTreeFlatModelRootRequest request;
        Date timestamp;
        NavigationResult newNavigationResult;
        if (navigationResult == null) {
            log.info("Ask for new navigation result (no previous result)");
            timestamp = null;
            request = getConfig().toRootRequest(groupByHelper);
        } else {
            log.info("Ask for new navigation result from: " + I18nDecoratorHelper.getTimestampWithSecondsLabel(locale, navigationResult.getTimestamp()));
            // Only on local mode, can always trust the service timestamp
            // otherwise always use a null time stamp to skip null result by service
            timestamp = getDataSourcesManager().getMainDataSource().isLocal() ? navigationResult.getTimestamp() : null;
            request = navigationResult.getRequest();
        }
        newNavigationResult = navigationService.getNavigation(request, timestamp);
        log.info("Generated new navigation result at: {}", I18nDecoratorHelper.getTimestampWithSecondsLabel(locale, newNavigationResult.getTimestamp()));
        return newNavigationResult;
    }

    protected void fireNavigationResultChanged() {
        pcs.firePropertyChange(PROPERTY_NAVIGATION_RESULT, 0, 1);
    }
}
