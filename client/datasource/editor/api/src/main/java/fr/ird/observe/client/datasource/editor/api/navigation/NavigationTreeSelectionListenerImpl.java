package fr.ird.observe.client.datasource.editor.api.navigation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditorModel;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIManager;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.navigation.tree.navigation.event.NavigationTreeSelectionEvent;
import fr.ird.observe.navigation.tree.navigation.event.NavigationTreeSelectionListener;
import fr.ird.observe.navigation.tree.navigation.event.NavigationTreeSelectionVetoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

class NavigationTreeSelectionListenerImpl implements NavigationTreeSelectionListener, TreeSelectionListener, MouseListener, WithClientUIContextApi {
    private static final Logger log = LogManager.getLogger(NavigationTreeSelectionListenerImpl.class);

    private final ContentUIManager contentUIManager;
    private final NavigationUI ui;
    private final DataSourceEditorModel dataSourceEditorModel;
    private final NavigationTree tree;
    private int rowToSelect;

    NavigationTreeSelectionListenerImpl(DataSourceEditorModel dataSourceEditorModel, ContentUIManager contentUIManager, NavigationUI ui, NavigationTree tree) {
        this.dataSourceEditorModel = Objects.requireNonNull(dataSourceEditorModel);
        this.tree = Objects.requireNonNull(tree);
        this.contentUIManager = Objects.requireNonNull(contentUIManager);
        this.ui = ui;
    }

    @Override
    public void valueChanged(NavigationTreeSelectionEvent event) throws NavigationTreeSelectionVetoException {
        boolean canChange = event.isSkipCheckPreviousContent() || contentUIManager.closeSelectedContentUI();
        if (!canChange) {
            // cancel the change of node
            throw new NavigationTreeSelectionVetoException(event, "Can't change current navigation node.");
        }
        if (event.isSkipCheckPreviousContent()) {
            contentUIManager.closeSafeSelectedContentUI();
        }
    }

    @Override
    public void treeWillExpand(TreeExpansionEvent event) {
        // do nothing
        TreePath path = event.getPath();
        NavigationNode node = (NavigationNode) path.getLastPathComponent();
        log.info(String.format("Will expand - do open node: %s", node));
        node.open();
    }

    @Override
    public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
        //FIXME check this is working ?
//        tree.getSelectionModel().fireTreeWillCollapseEvent(event);

        NavigationNode node = (NavigationNode) event.getPath().getLastPathComponent();

        // when collapsing a parent, if a son is selected, then closing before collapse
        NavigationNode selectedNode = tree.getSelectedNode();
        if (selectedNode == null) {
            // no selected node, skip
            return;
        }
        if (selectedNode.equals(node) || !selectedNode.isNodeAncestor(node)) {
            // selected node is node to collapse, nor an ancestor of node to collapse, skip
            return;
        }
        // ask to close selected content
        boolean canChange = contentUIManager.closeSelectedContentUI();
        if (!canChange) {
            // raise an exception to stop collapse operation
            throw new ExpandVetoException(event, "Can not collapse node " + event.getPath());
        }
    }

    @Override
    public void valueChanged(TreeSelectionEvent event) {

        ObserveSwingDataSource source = getDataSourcesManager().getMainDataSource();
        if (source == null || !source.isOpen()) {
            //FIXME Is this can really happen ? not sure
            // no open data source
            log.warn("No open Data source.");
            return;
        }
        if (tree.isSelectionEmpty()) {
            log.info("No selection, show empty panel...");
            dataSourceEditorModel.setContent(null);
            return;
        }

        TreePath path = event.getPath();
        NavigationNode node = (NavigationNode) path.getLastPathComponent();

        String params = node.toString();
        String message = t("observe.ui.action.open.screen", params);
        log.info("Open selection: " + message);
        if (node.isLoaded() && node instanceof ContentListUINavigationNode && node.getChildCount() == 0) {
            //FIXME Find out why node is said loaded but in fact
            ContentListUINavigationNode node1 = (ContentListUINavigationNode) node;
            int initialCount = node1.getInitializer().getInitialCount();
            if (node.getChildCount() != initialCount) {
                node.dirty();
            }
        }

        node.open();

        ui.getHandler().updateActions(node);

        addSimpleAction(message, () -> doOpenContent(node));
    }

    private void doOpenContent(NavigationNode node) {

        // obtain the ui type to show
        Class<? extends ContentUI> uiClass = node.getScope().getContentUiType();
        log.info(String.format("Will open content for node = %s, ui = %s", node, uiClass));

        // compute the selected ids to put in data context
        node.getContext().updateSelectedNodes(node);

        // save last selected path in configuration
        getDataSourcesManager().saveSelectedPath(node.toPaths());

        // create new content
        ContentUI content = contentUIManager.createContent(node, node.getScope().getContentUiType());

        // open content
        dataSourceEditorModel.setContent(content);

        ObserveUtil.cleanMemory();
    }


    @Override
    public void mouseClicked(MouseEvent e) {
        if (!tree.isEnabled()) {
            return;
        }
        if (e.isConsumed()) {
            return;
        }
        boolean rightClick = SwingUtilities.isRightMouseButton(e);

        boolean doubleClick = e.getClickCount() == 2;
        // get the coordinates of the mouse click
        Point p = e.getPoint();

        int closestRowForLocation = tree.getClosestRowForLocation(e.getX(), e.getY());

        log.info(String.format("Click on navigation tree: (rightClick? %b, doubleClick? %b) - at row %s (point %s)", rightClick, doubleClick, closestRowForLocation, p));

        if (e.getClickCount() == 1) {
            // need to compute row to select
            rowToSelect = -1;
            if (tree.isRowSelected(closestRowForLocation)) {
                rowToSelect = closestRowForLocation;
            } else {

                // try to change selection

                TreePath pathForRow = tree.getPathForRow(closestRowForLocation);
                if (pathForRow == null) {
                    e.consume();
                    return;
                }
                Rectangle pathBounds = tree.getPathBounds(pathForRow);
                if (pathBounds != null && pathBounds.getX() > p.getX()) {
                    // we never acts when point is before the rectangle, because the arrow button may be used for this...
                    log.info("Cancel click on tree navigation (before path rectangle (probably on arrow button)");
                    e.consume();
                    return;
                }
                log.info(String.format("Do select row: %d", closestRowForLocation));
                tree.setSelectionPath(pathForRow);

                if (tree.isRowSelected(closestRowForLocation)) {
                    rowToSelect = closestRowForLocation;
                }
            }
        } else {

            // re-use previous rowToSelect
            if (rowToSelect == -1) {
                log.info("Cancel double-click, no previous rowToSelect");
                e.consume();
                return;
            }
        }

        if (rowToSelect == -1) {
            e.consume();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // do nothing
    }
}
