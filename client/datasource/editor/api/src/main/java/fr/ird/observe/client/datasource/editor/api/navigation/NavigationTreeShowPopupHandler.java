package fr.ird.observe.client.datasource.editor.api.navigation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditorModel;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.AbstractButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created on 1/8/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.11
 */
public class NavigationTreeShowPopupHandler implements KeyListener, MouseListener {

    private static final Logger log = LogManager.getLogger(NavigationTreeShowPopupHandler.class);

    private final JPopupMenu popup;
    private final NavigationTree tree;
    private final JMenuItem noAction;
    private final DataSourceEditorModel dataSourceEditorModel;

    NavigationTreeShowPopupHandler(DataSourceEditorModel dataSourceEditorModel, NavigationTree tree, JPopupMenu popup, JMenuItem noAction) {
        this.dataSourceEditorModel = Objects.requireNonNull(dataSourceEditorModel);
        this.popup = Objects.requireNonNull(popup);
        this.tree = Objects.requireNonNull(tree);
        this.noAction = Objects.requireNonNull(noAction);
    }

    private void showPopup(int row, Point p) {

        log.info(String.format("Will show popup from row: %d", row));

        NavigationNode selectedNode = tree.getNodeForRow(row);

        log.info(String.format("Found selected node: %s", selectedNode));

        SwingUtilities.invokeLater(() -> {
            beforeOpenPopup(selectedNode);
            popup.show(tree, p.x, p.y);
        });
    }

    private void beforeOpenPopup(NavigationNode selectedNode) {
        // clean popup
        popup.removeAll();
        popup.setLabel(selectedNode.toString());
        ContentUI selectedContentUI = dataSourceEditorModel.getContent();
        ContentUIHandler<?> handler = selectedContentUI.getHandler();
        List<AbstractButton> actions = handler.getNavigationPopupActions();
        int length = 0;
        if (selectedContentUI instanceof ReferentialHomeUI) {
            actions.forEach(a -> ContentUIMenuAction.createWithKeyStroke(popup, a).init());
            length = popup.getSubElements().length;
        } else {
            if (selectedContentUI.getMode().isEnabled() || selectedContentUI instanceof ContentListUI) {
                ContentUIMenuAction.createWithKeyStroke(popup, selectedContentUI.getMode()).init();
                actions.forEach(a -> ContentUIMenuAction.createWithKeyStroke(popup, a).init());
                popup.addSeparator();
                length = popup.getSubElements().length;
            }
            Arrays.stream(selectedContentUI.getConfigurePopup().getSubElements()).forEach(a -> ContentUIMenuAction.createWithKeyStroke(popup, (AbstractButton) a).init());
            if (popup.getSubElements().length > length) {
                popup.addSeparator();
            }
            length = popup.getSubElements().length;
            Arrays.stream(selectedContentUI.getInsertPopup().getSubElements()).forEach(a -> new ContentUIMenuAction(popup, (AbstractButton) a).init());
        }
        if (length == 0) {
            popup.add(noAction);
        }
    }

    private int getLowestSelectedRowCount() {
        if (tree.isSelectionEmpty()) {
            throw new IllegalStateException("Can't have empty selection");
        }
        int[] selectedRows = tree.getSelectionRows();
        int lowestRow = -1;
        if (selectedRows != null) {
            for (int row : selectedRows) {
                lowestRow = Math.max(lowestRow, row);
            }
        }
        return lowestRow;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (!tree.isEnabled()) {
            return;
        }
        if (e.isConsumed()) {
            return;
        }
        if (e.getKeyCode() == KeyEvent.VK_CONTEXT_MENU && !tree.isSelectionEmpty()) {
            // get the lowest selected row
            int lowestRow = getLowestSelectedRowCount();
            // get the selected column
            Rectangle r = tree.getRowBounds(lowestRow);
            // get the point in the middle lower of the cell
            Point p = new Point(r.x + r.width / 2, r.y + r.height);
            log.debug(String.format("Row %d found t point [%s]", lowestRow, p));
            showPopup(lowestRow, p);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (!tree.isEnabled()) {
            return;
        }
        if (e.isConsumed()) {
            return;
        }
        boolean rightClick = SwingUtilities.isRightMouseButton(e);
        if (!rightClick) {
            return;
        }
        boolean doubleClick = e.getClickCount() == 2;
        if (doubleClick) {
            return;
        }
        // get the coordinates of the mouse click
        Point p = e.getPoint();
        int closestRowForLocation = tree.getClosestRowForLocation(e.getX(), e.getY());
        log.info(String.format("Point of click: (rightClick? %b, doubleClick? %b) - at row %s (point %s)", true, false, closestRowForLocation, p));

        int rowToSelect = -1;

        if (tree.isRowSelected(closestRowForLocation)) {
            rowToSelect = closestRowForLocation;
        }

        if (rowToSelect == -1) {
            return;
        }
        showPopup(rowToSelect, p);
        e.consume();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // do nothing
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // do nothing
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // do nothing
    }
}
