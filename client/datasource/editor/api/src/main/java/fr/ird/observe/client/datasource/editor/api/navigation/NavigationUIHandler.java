package fr.ird.observe.client.datasource.editor.api.navigation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditorModel;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUIHandler;
import fr.ird.observe.client.datasource.editor.api.menu.DataSourceEditorMenuModel;
import fr.ird.observe.client.datasource.editor.api.navigation.actions.ReloadAction;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.navigation.tree.NavigationResult;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import org.nuiton.jaxx.runtime.init.UIInitializerResult;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.AbstractButton;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import java.util.Set;

/**
 * Created by tchemit on 01/04/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class NavigationUIHandler implements UIHandler<NavigationUI>, WithClientUIContextApi {

    private Set<ReloadAction> actionsToReload;
    private NavigationUIInitializer initializer;

    public static void updateStatistics(NavigationUI ui) {
        NavigationResult navigationResult = ui.getTree().getRootNode().getInitializer().getNavigationResult();
        TreeConfigUIHandler.updateStatistics(navigationResult.getRequest(),
                                             ui.getTree().getModel().getGroupByHelper(),
                                             navigationResult::getGroupByCount,
                                             navigationResult::getGroupByDataCount,
                                             navigationResult::getDataCount,
                                             ui::setStatisticsText,
                                             ui::setStatisticsTip,
                                             ui.getStatisticsLabel()::setIcon);
    }

    @Override
    public void beforeInit(NavigationUI ui) {
        initializer = new NavigationUIInitializer(ui);
        initializer.registerDependencies(ui.getContextValue(DataSourceEditorModel.class).getDatasourceMenuModel());
        ui.setComponentPopupMenu(new JPopupMenu());
    }

    @Override
    public void afterInit(NavigationUI ui) {
        NavigationTreeConfig incoming = getClientConfig().getNavigationConfig(getDataSourcesManager().getMainDataSource());
        ui.getTree().getModel().getConfig().init(incoming);
        UIInitHelper.init(ui.getNavigationScrollPane());
        UIInitializerResult initializerResult = initializer.initUI();
        actionsToReload = ReloadAction.collect(initializerResult.getSubComponents(AbstractButton.class));
        ui.getMenuPopup().add(new JSeparator(), 4);
        DataSourceEditorMenuModel menuModel = ui.getContextValue(DataSourceEditorMenuModel.class);
        ui.getLabel().setText(menuModel.getStorageStatusText());
        ui.getLabel().setToolTipText(menuModel.getStorageStatusTip());
        ui.getLabel().setIcon(menuModel.getStorageStatusIcon());
        ObserveSwingDataSource mainDataSource = getDataSourcesManager().getMainDataSource();
        boolean mainStorageOpened = mainDataSource != null;
        boolean mainStorageLocal = mainStorageOpened && mainDataSource.isLocal();
        boolean mainStorageRemote = mainStorageOpened && mainDataSource.isRemote();
        ui.getImportAvdthFile().setEnabled(mainStorageLocal || mainStorageRemote);
        ui.getSearchOrCreate().setEnabled(mainStorageOpened && incoming.isLoadData());
        // To update statistics programmatic
        ui.getTree().getModel().addPropertyChangeListener(NavigationTreeModel.PROPERTY_NAVIGATION_RESULT, evt -> updateStatistics(ui));
    }

    public void updateActions(NavigationNode selectedNode) {
        actionsToReload.forEach(reloadAction -> reloadAction.updateAction(selectedNode));
    }
}
