package fr.ird.observe.client.datasource.editor.api.navigation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditorModel;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIManager;
import fr.ird.observe.client.util.init.UIInitHelper;
import org.nuiton.jaxx.runtime.init.UIInitializerContext;
import org.nuiton.jaxx.runtime.init.UIInitializerSupport;

import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import java.util.Objects;

/**
 * Created on 15/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
class NavigationUIInitializer extends UIInitializerSupport<NavigationUI, UIInitializerContext<NavigationUI>> {

    public NavigationUIInitializer(NavigationUI ui) {
        super(ui, JButton.class, JScrollPane.class, JToggleButton.class, JCheckBoxMenuItem.class, NavigationTree.class, JToolBar.class);
    }

    @Override
    protected UIInitializerContext<NavigationUI> createIInitializerContext(NavigationUI ui, Class<?>... types) {
        return new UIInitializerContext<>(ui, types);
    }

    @Override
    protected void initUI(UIInitializerContext<NavigationUI> initializerContext) {
        initializerContext
                .startFirstPass()
                .onComponents(JButton.class, true, this::init)
                .onComponents(JToggleButton.class, this::init)
                .onComponents(NavigationTree.class, this::init)
                .onSubComponents(JScrollPane.class, this::init)
                .onComponents(JToolBar.class, this::init)
                .startSecondPass();
    }

    protected void init(JScrollPane editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(JToolBar editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.init(editor);
    }

    protected void init(JButton editor) {
        initializerContext.checkFirstPass();
        editor.setFocusable(false);
        editor.setFocusPainted(false);
    }

    protected void init(JToggleButton editor) {
        initializerContext.checkFirstPass();
        editor.setFocusable(false);
        editor.setFocusPainted(false);
    }

    protected void init(NavigationTree editor) {
        initializerContext.checkFirstPass();
        editor.setFont(editor.getFont().deriveFont(11f));
        editor.setCellRenderer(new NavigationTreeCellRenderer());
        NavigationUI ui = initializerContext.getUi();
        DataSourceEditorModel dataSourceEditorModel = Objects.requireNonNull(ui.getContextValue(DataSourceEditorModel.class));
        ContentUIManager contentUIManager = Objects.requireNonNull(ui.getContextValue(ContentUIManager.class));
        editor.installShowPopupHandler(new NavigationTreeShowPopupHandler(dataSourceEditorModel, editor, ui.getNavigationPopup(), ui.getNoAction()));
        editor.installSelectionListener(new NavigationTreeSelectionListenerImpl(dataSourceEditorModel, contentUIManager, ui, editor));
    }
}
