package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.navigation.id.IdModule;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;

import java.util.LinkedList;
import java.util.List;

/**
 * Created on 03/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
class EditModule {
    private final String moduleName;
    private final List<SelectData> dataItems;
    private final List<SelectReferential> referentialItems;

    public EditModule(NavigationUI ui, ObserveBusinessProject project, IdModule editModel) {
        BusinessModule module = editModel == null ? project.getCommonBusinessModule() : editModel.getModule();
        this.moduleName = module.getName();
        this.dataItems = new LinkedList<>();
        this.referentialItems = new LinkedList<>();
        if (editModel != null) {
            for (IdNode<?> editNode : editModel.getEditableNodes()) {
                SelectData action = SelectData.install(ui, project, editNode);
                dataItems.add(action);
            }
        }
        for (BusinessSubModule subModule : module.getSubModules()) {
            if (subModule.getReferentialPackage().isPresent()) {
                SelectReferential action = SelectReferential.install(ui, project, module, subModule);
                referentialItems.add(action);
            }
        }
    }

    public String getModuleName() {
        return moduleName;
    }

    public void addData() {
        dataItems.forEach(SelectData::initUIForReal);
    }

    public void addReferential() {
        referentialItems.forEach(SelectReferential::initUIForReal);
    }
}
