/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.navigation.actions;

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.Enumeration;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class GoDown extends NavigationUIActionSupport implements ReloadAction {

    private boolean dirty = true;
    private NavigationNode selectedNode;

    public GoDown() {
        super(n("observe.ui.action.goDown.tip"), "navigate-down", ObserveKeyStrokesEditorApi.KEY_STROKE_GO_DOWN);
        setText(null);
    }

    @Override
    public NavigationNode getSelectedNode() {
        return selectedNode;
    }

    @Override
    public void updateAction(NavigationNode selectedNode) {
        this.selectedNode = selectedNode;
        dirty = true;
        editor.setEnabled(selectedNode.isNotLeaf());
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        if (dirty) {
            try {
                populatePopup(ui.getScopeDownPopup(), ui.getTree());
            } finally {
                dirty = false;
            }
        }
        SwingUtilities.invokeLater(() -> {
            JComponent c = ui.getRightToolbar();
            JPopupMenu p = ui.getScopeDownPopup();
            adaptPopupSize(ui.getNavigationScrollPane().getViewport(), p, p);
            MenuAction.preparePopup(p, c, false);
        });
    }

    private void populatePopup(JPopupMenu scopePopup, JTree tree) {
        int max = 2 * 26 - 1;
        scopePopup.setOpaque(true);
        NavigationNode node = this.selectedNode;
        NavigationNode nodeOriginal = node;
        scopePopup.removeAll();
        scopePopup.setLayout(new GridLayout(0, 1));
        Enumeration<?> e = node.children();
        int position = 0;
        boolean full = false;
        while (e.hasMoreElements()) {
            node = (NavigationNode) e.nextElement();
            if (position++ > max) {
                full = true;
                break;
            }
            addMenuItem(scopePopup, tree, node);
        }

        if (nodeOriginal.getChildCount() == 1 && !nodeOriginal.getChildAt(0).isLeaf()) {
            e = nodeOriginal.getChildAt(0).children();
            while (e.hasMoreElements()) {
                node = (NavigationNode) e.nextElement();
                if (position++ > max) {
                    full = true;
                    break;
                }
                addMenuItem(scopePopup, tree, node);
            }
        }
        if (full) {
            JMenuItem menuItem = new JMenuItem("< Trop d'entrées à afficher >");
            menuItem.setHorizontalTextPosition(SwingConstants.CENTER);
            menuItem.setEnabled(false);
            scopePopup.add(menuItem);
        }
    }

}
