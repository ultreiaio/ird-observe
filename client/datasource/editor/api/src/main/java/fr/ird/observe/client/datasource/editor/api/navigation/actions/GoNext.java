package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created on 11/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GoNext extends NavigationUIActionSupport implements ReloadAction {
    private static final Logger log = LogManager.getLogger(GoNext.class);
    private NavigationNode selectedNode;

    public GoNext() {
        super(n("observe.ui.tree.action.goNext.tip"), "navigate-next", ObserveKeyStrokesEditorApi.KEY_STROKE_GO_NEXT);
        setText(null);
    }

    @Override
    public NavigationNode getSelectedNode() {
        return selectedNode;
    }

    @Override
    public void updateAction(NavigationNode selectedNode) {
        this.selectedNode = selectedNode;
        SingletonSupplier<Boolean> withNext = Objects.requireNonNull(selectedNode).getWithNext();
        if (withNext.withValue()) {
            // got a cache value
            editor.setEnabled(withNext.get());
        } else {
            editor.setEnabled(true);
        }
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        log.info(String.format("Will get next of: %s", selectedNode));
        if (selectedNode.getWithNext().get()) {
            NavigationNode result = selectedNode.getNext();
            log.info(String.format("Next node is : %s", result));
            SwingUtilities.invokeLater(() -> ui.getTree().selectSafeNode(result));
        } else {
            log.info(String.format("No next node for : %s", selectedNode));
            editor.setEnabled(false);
            setUiStatus(I18n.t("observe.ui.tree.action.no.next"));
        }
    }
}
