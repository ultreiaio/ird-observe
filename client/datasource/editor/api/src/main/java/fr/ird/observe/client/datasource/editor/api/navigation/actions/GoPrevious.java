package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created on 11/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GoPrevious extends NavigationUIActionSupport implements ReloadAction {
    private static final Logger log = LogManager.getLogger(GoPrevious.class);
    private NavigationNode selectedNode;

    public GoPrevious() {
        super(n("observe.ui.tree.action.goPrevious.tip"), "navigate-previous", ObserveKeyStrokesEditorApi.KEY_STROKE_GO_PREVIOUS);
        setText(null);
    }

    @Override
    public NavigationNode getSelectedNode() {
        return selectedNode;
    }

    @Override
    public void updateAction(NavigationNode selectedNode) {
        this.selectedNode = Objects.requireNonNull(selectedNode);
        SingletonSupplier<Boolean> withPrevious = selectedNode.getWithPrevious();
        editor.setEnabled(!withPrevious.withValue() || withPrevious.get());
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        log.info(String.format("Will get previous of: %s", selectedNode));
        if (selectedNode.getWithPrevious().get()) {
            NavigationNode result = selectedNode.getPrevious();
            log.info(String.format("Previous node is : %s", result));
            SwingUtilities.invokeLater(() -> ui.getTree().selectSafeNode(result));
        } else {
            log.info(String.format("No previous node form : %s", selectedNode));
            editor.setEnabled(false);
            setUiStatus(I18n.t("observe.ui.tree.action.no.previous"));
        }
    }
}
