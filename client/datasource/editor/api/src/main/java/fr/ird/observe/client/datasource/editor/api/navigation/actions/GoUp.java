/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.navigation.actions;

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreeNode;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class GoUp extends NavigationUIActionSupport implements ReloadAction {

    private boolean dirty = true;
    private NavigationNode selectedNode;

    public GoUp() {
        super(n("observe.ui.action.goUp.tip"), "navigate-up", ObserveKeyStrokesEditorApi.KEY_STROKE_GO_UP);
        setText(null);
    }

    @Override
    public NavigationNode getSelectedNode() {
        return selectedNode;
    }

    @Override
    public void updateAction(NavigationNode selectedNode) {
        this.selectedNode = selectedNode;
        dirty = true;
        editor.setEnabled(selectedNode.getLevel() > 1);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        if (dirty) {
            try {
                populatePopup(ui.getScopeUpPopup(), ui.getTree());
            } finally {
                dirty = false;
            }
        }
        SwingUtilities.invokeLater(() -> {
            JComponent c = ui.getRightToolbar();
            JPopupMenu p = ui.getScopeUpPopup();
            adaptPopupSize(ui.getNavigationScrollPane().getViewport(), p, p);
            MenuAction.preparePopup(p, c, true);
        });
    }

    private void populatePopup(JPopupMenu scopePopup, JTree tree) {
        NavigationNode selectedNode = this.selectedNode;
        scopePopup.removeAll();
        scopePopup.setMaximumSize(ui.getTree().getPreferredSize());
        scopePopup.setLayout(new GridLayout(0, 1));
        for (TreeNode node : selectedNode.getPath()) {
            if (node.getParent() == null || selectedNode.equals(node)) {
                continue;
            }
            addMenuItem(scopePopup, tree, (NavigationNode) node);
        }
    }

}
