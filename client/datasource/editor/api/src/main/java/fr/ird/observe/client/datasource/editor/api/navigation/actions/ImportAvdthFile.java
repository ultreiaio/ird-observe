package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.avdth.ImportDialog;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.util.UIHelper;

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 07/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ImportAvdthFile extends MenuActionSupport implements Runnable {

    public ImportAvdthFile() {
        super(t("observe.ui.action.import.avdth"), t("observe.ui.action.import.avdth.tip"), "local-import", ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_IMPORT_AVDTH);
        setCheckMenuItemIsArmed(false);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        SwingUtilities.invokeLater(this);
    }

    @Override
    public void run() {
        ImportDialog importDialog = new ImportDialog();
        UIHelper.center(getMainUI(), importDialog);
        importDialog.setVisible(true);
    }
}
