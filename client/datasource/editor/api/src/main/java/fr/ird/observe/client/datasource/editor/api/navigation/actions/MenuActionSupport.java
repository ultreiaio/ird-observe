package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditorBodyContent;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

/**
 * Created on 06/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class MenuActionSupport extends NavigationUIActionSupport implements MenuAction {

    public static void toggle(NavigationUI ui, JPopupMenu popupMenu) {
        toggle(ui.getToggleMenu(), popupMenu);
    }

    public static void toggle(JToggleButton editor, JPopupMenu popupMenu) {
        popupMenu.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                editor.setSelected(true);
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                editor.setSelected(false);
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
                editor.setSelected(false);
            }
        });
    }

    MenuActionSupport(String text, String tip, String icon, KeyStroke keyStroke) {
        super(text, tip, icon, keyStroke);
    }

    public DataSourceEditorBodyContent getDataSourceEditorBodyContent() {
        return getMainUI().getMainUIBodyContentManager().getBodyTyped(DataSourceEditor.class, DataSourceEditorBodyContent.class);
    }

    public DataSourceEditor getDataSourceEditor() {
        return getDataSourceEditorBodyContent().get();
    }

    @Override
    public final JPopupMenu getPopupMenu() {
        return ui.getMenuPopup();
    }

    @Override
    public final KeyStroke getMenuKeyStroke() {
        return getAcceleratorKey();
    }
}

