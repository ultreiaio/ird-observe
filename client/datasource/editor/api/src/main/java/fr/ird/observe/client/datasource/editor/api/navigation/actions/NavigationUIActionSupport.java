package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;

import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Objects;

public abstract class NavigationUIActionSupport extends JComponentActionSupport<NavigationUI> implements WithClientUIContextApi {

    public static void adaptPopupSize(JViewport viewport, JPopupMenu popup, JComponent content) {
        popup.pack();
        Dimension extentSize = viewport.getExtentSize();
        int width = extentSize.width;
        int height = content.getPreferredSize().height;
        if (content == popup) {
            height = popup.getSubElements().length * 22;
        }
        height = Math.max(26, height);
        content.setPreferredSize(new Dimension(width, height));
        popup.pack();
    }

    protected NavigationUIActionSupport(String name, String label, String shortDescription) {
        super(name, label, shortDescription, null, null);
    }

    protected NavigationUIActionSupport(String label, String shortDescription) {
        super(label, shortDescription, null, null);
    }

    protected NavigationUIActionSupport(String label, String shortDescription, Icon actionIcon) {
        super(label, shortDescription, null, null);
        setLargeIcon(Objects.requireNonNull(actionIcon));
    }

    protected NavigationUIActionSupport(String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super("", shortDescription, actionIcon, acceleratorKey);
    }

    protected NavigationUIActionSupport(String shortDescription, String tip, String actionIcon, KeyStroke acceleratorKey) {
        super(shortDescription, tip, actionIcon, acceleratorKey);
    }

    @Override
    protected InputMap getInputMap(NavigationUI navigationUI, int inputMapCondition) {
        return navigationUI.getTree().getInputMap(inputMapCondition);
    }

    @Override
    protected ActionMap getActionMap(NavigationUI navigationUI) {
        return navigationUI.getTree().getActionMap();
    }

    @Override
    protected int getInputMapCondition() {
        return JComponent.WHEN_FOCUSED;
    }

    protected void addMenuItem(JPopupMenu popupMenu, JTree tree, NavigationNode node) {
        JMenuItem editor = new JMenuItem();
        Component treeCellRendererComponent = tree.getCellRenderer().getTreeCellRendererComponent(tree, node, false, false, false, 0, false);

        SelectNode.init(ui, editor, SelectNode.create(popupMenu, node));
        //FIXME should be able to change selection color to white (as it is perfectly done for accelerator)
        editor.setForeground(treeCellRendererComponent.getForeground());
    }
}
