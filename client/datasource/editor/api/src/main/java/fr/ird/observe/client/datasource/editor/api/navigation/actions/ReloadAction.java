package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.AbstractButton;
import javax.swing.Action;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 24/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public interface ReloadAction extends Action {

    static Set<ReloadAction> collect(JAXXObject ui) {
        return ui.get$objectMap().values().stream()
                .filter(t -> t instanceof AbstractButton && ((AbstractButton) t).getAction() instanceof ReloadAction)
                .map(o -> (ReloadAction) ((AbstractButton) o).getAction())
                .collect(Collectors.toSet());
    }

    static Set<ReloadAction> collect(Stream<AbstractButton> stream) {
        return stream.filter(t -> t != null && t.getAction() instanceof ReloadAction)
                .map(o -> (ReloadAction) o.getAction())
                .collect(Collectors.toSet());
    }

    NavigationNode getSelectedNode();

    void updateAction(NavigationNode selectedNode);
}
