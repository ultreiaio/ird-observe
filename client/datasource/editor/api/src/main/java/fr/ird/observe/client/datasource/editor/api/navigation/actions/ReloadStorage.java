package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditorBodyContent;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 07/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ReloadStorage extends MenuActionSupport implements Runnable {
    private static final Logger log = LogManager.getLogger(ReloadStorage.class);

    public ReloadStorage() {
        super(t("observe.ui.action.reload.storage"), t("observe.ui.action.reload.storage.tip"), "db-reload", ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_RELOAD);
        setCheckMenuItemIsArmed(false);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        run();
    }

    @Override
    public void run() {
        getActionExecutor().addAction(getText(), this::run0);
    }

    private void run0() {

        // Track data source
        ObserveSwingDataSource dataSource = getDataSourcesManager().getMainDataSource();

        log.info(String.format("Will close main data source: %s", dataSource));

        DataSourceEditorBodyContent bodyContent = getDataSourceEditorBodyContent();

        // keep selected node to select it on new navigation tree
        NavigationNode selectedNode = bodyContent.get().getNavigationUI().getTree().getSelectedNode();
        bodyContent.setPreviousNode(selectedNode);

        // Close storage
        bodyContent.doCloseStorage();

        //FIXME:Datasource See why we do this?
        dataSource.setCanMigrate(getClientConfig());

        log.info(String.format("Will load main data source: %s", dataSource));

        try {
            bodyContent.loadStorage(getMainUI(), dataSource);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new ObserveSwingTechnicalException(e);
        }

    }
}
