package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageBackupUILauncher;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 07/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SaveStorageToFile extends MenuActionSupport implements Runnable {

    public SaveStorageToFile() {
        super(t("observe.ui.action.save.to.file"), t("observe.ui.action.save.to.file.tip"), "local-export", ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_BACKUP);
        setCheckMenuItemIsArmed(false);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        SwingUtilities.invokeLater(this);
    }

    @Override
    public void run() {
        boolean canContinue = getDataSourceEditor().getContentUIManager().closeSelectedContentUI();
        if (canContinue) {
            // check user permissions
            ObserveSwingDataSource service = getDataSourcesManager().getMainDataSource();
            if (!service.isLocal() && !(service.canReadData() && service.canWriteData())) {
                askToUser(
                        t("observe.ui.title.can.not.export.obstuna"),
                        t("observe.ui.datasource.storage.required.read.on.data", service),
                        JOptionPane.ERROR_MESSAGE,
                        new Object[]{t("observe.ui.choice.cancel")},
                        0
                );
                return;
            }
            String title = service.isLocal() ?
                    t("observe.ui.title.save.localDB") :
                    t("observe.ui.title.save.remoteDB");
            StorageBackupUILauncher launcher = new StorageBackupUILauncher(ui, getMainUI(), title);
            launcher.start();
        }
    }
}
