package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.search.SearchUI;
import fr.ird.observe.client.datasource.editor.api.navigation.search.SearchUIModel;
import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.SearchCriteriaUIModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.navigation.tree.ModuleGroupByHelper;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.spi.ui.BusyLayerUI;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JDialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * To search a trip and select it, or create a new trip within the selected navigation configuration.
 * <p>
 * Created on 03/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class SearchOrCreate extends NavigationUIActionSupport {
    private static final Logger log = LogManager.getLogger(SearchOrCreate.class);

    public SearchOrCreate() {
        super(null, n("observe.ui.tree.action.searchOrCreate.tip"), "navigate-search", ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_SEARCH_OR_CREATE);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        boolean canContinue = getMainUI().getMainUIBodyContentManager().getCurrentBody().canQuit(getMainUI());
        if (!canContinue) {
            return;
        }
        NavigationTreeConfig navigationTreeConfig = ui.getTree().getModel().getConfig();
        log.warn("Start action with configuration {}", navigationTreeConfig);
        if (!navigationTreeConfig.isLoadData()) {
            // can't search, data are not loaded
            throw new IllegalStateException("LoadData is off on navigation config, can't do a search");
        }
        String moduleName = navigationTreeConfig.getModuleName();
        BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByName(moduleName);
        SearchUIModel model = new SearchUIModel(navigationTreeConfig, new ModuleGroupByHelper(businessModule), getMainUI().getBusyModel());

        // Add a first criteria using navigation tree config
        DataGroupByDtoDefinition<?, ?> firstCriteriaDefinition = model.getModuleGroupByHelper().getDefinition(navigationTreeConfig.getGroupByName());
        SearchCriteriaUIModel<?> firstCriteria = model.createCriteria(firstCriteriaDefinition);
        SearchUI search = new SearchUI(UIHelper.initialContext(ui, model));
        JDialog dialog = new JDialog(getMainUI(), I18n.t("observe.ui.datasource.searchOrCreate.title"), true);
        dialog.setMinimumSize(new Dimension(800, 669));
        dialog.setContentPane(search);

        // block dialog when busyModel model is busy
        BusyLayerUI.create(dialog, model.getBusyModel());

        model.addCriteria(firstCriteria);

        dialog.pack();
        UIHelper.center(getMainUI(), dialog);
        dialog.setVisible(true);
    }
}
