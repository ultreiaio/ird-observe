package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Created on 01/06/18.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
abstract class SelectInEditMenu extends NavigationUIActionSupport implements MenuAction {

    SelectInEditMenu(String actionName, String text, String tip, Icon icon) {
        super(actionName, text, tip);
        putValue(Action.SMALL_ICON, icon);
    }

    @Override
    public final JPopupMenu getPopupMenu() {
        return ui.getEditPopup();
    }

//    @Override
//    public final void initEditor() {
//        // real menu, use default ui
//    }

    @Override
    public final void initUI() {
        // can't use this one, invoked at init time
        // we need to init action only when used (keyStrokes are dynamic)
    }

    public void initUIForReal() {
        MenuAction.super.initUI();
        ((JMenuItem) editor).setAccelerator(getAcceleratorKey());
    }
}
