/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.navigation.actions;

import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class SelectNode extends NavigationUIActionSupport implements MenuAction {

    private final JPopupMenu popupMenu;
    private final NavigationNode node;

    public static SelectNode create(JPopupMenu popupMenu, NavigationNode node) {
        SelectNode action = new SelectNode(popupMenu, node);
        action.setCheckMenuItemIsArmed(true);
        action.setIcon(node.getHandler().getIcon(true));
        return action;
    }

    //FIXME Push back in jaxx
    static KeyStroke getMenuKeyStroke(JPopupMenu popupMenu) {
        int length = popupMenu.getSubElements().length;
        switch (length / 26) {
            case 0:
                return KeyStroke.getKeyStroke("pressed " + (char) ('A' + length));
            case 1:
                return KeyStroke.getKeyStroke("shift pressed " + (char) ('A' + length % 26));
            default:
                throw new IllegalStateException("Can't do this");
        }
    }

    public SelectNode(JPopupMenu popupMenu, NavigationNode node) {
        super(node.toString(), n("observe.ui.action.selectNode.tip"));
        this.popupMenu = popupMenu;
        this.node = node;
        setCheckMenuItemIsArmed(false);
    }

    @Override
    public void init() {
        super.init();
        editor.setFont(node.getNodeFont(popupMenu.getFont()));
        editor.setForeground(node.getColor());
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        SwingUtilities.invokeLater(() -> ui.getTree().selectSafeNode(node));
    }

    @Override
    public JPopupMenu getPopupMenu() {
        return popupMenu;
    }

    @Override
    public KeyStroke getMenuKeyStroke() {
        return getMenuKeyStroke(getPopupMenu());
    }
}
