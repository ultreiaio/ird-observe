package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;

import javax.swing.Icon;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;

/**
 * Created on 01/06/18.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public final class SelectReferential extends SelectInEditMenu {

    private final String referentialName;

    public static SelectReferential install(NavigationUI ui, ObserveBusinessProject project, BusinessModule module, BusinessSubModule subModule) {
        String text = project.getReferentialPackageTitle(module, subModule);
        String tip = project.gotoReferentialHomeToolTipText(module, subModule);
        Icon icon = DtoIconHelper.getReferentialHome(module, subModule, true);
        SelectReferential action = new SelectReferential(text, tip, icon);
        return init(ui, new JMenuItem(), action);
    }

    private SelectReferential(String text, String tip, Icon icon) {
        super(SelectReferential.class.getName() + "#" + text, text, tip, icon);
        this.referentialName = text;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        NavigationNode node = ui.getTree().getRootNode().findChildById(referentialName);
        ui.getTree().selectNode(node);
    }
}
