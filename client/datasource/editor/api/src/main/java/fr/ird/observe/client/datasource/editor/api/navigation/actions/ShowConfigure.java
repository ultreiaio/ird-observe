package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUIHandler;
import fr.ird.observe.client.datasource.editor.api.config.actions.ApplyNavigationConfiguration;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class ShowConfigure extends MenuActionSupport {

    public ShowConfigure() {
        super(t("observe.ui.action.info.configure"), n("observe.ui.tree.action.configure.tip"), "generate"/*UIHelper.getContentActionIconKey("configure")*/, ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_SHOW_CONFIGURE);
        setCheckMenuItemIsArmed(false);
    }

    @Override
    public void init() {
        super.init();
        MenuActionSupport.toggle(ui, ui.getComponentPopupMenu());
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        SwingUtilities.invokeLater(() -> {
            TreeConfigUI configUI = TreeConfigUIHandler.createUI(ui,
                                                                 ui.getTree().getModel().getConfig(),
                                                                 ui.getTree().getModel().getGroupByHelper(),
                                                                 new ApplyNavigationConfiguration(),
                                                                 ui.getTree());
            configUI.setComponentPopupMenu(ui.getComponentPopupMenu());
            TreeConfigUIHandler.showPanel(ui.getNavigationScrollPane(), configUI);
        });
    }

}
