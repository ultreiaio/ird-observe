package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.nuiton.jaxx.runtime.swing.action.MenuAction;

import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.Optional;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created on 02/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public final class ShowEdit extends MenuActionSupport {

    private final Map<String, EditModule> modules;
    private final EditModule commonModule;

    public ShowEdit(NavigationUI ui) {
        super(n("observe.ui.menu.navigation"), n("observe.ui.menu.navigation"), UIHelper.getContentActionIconKey("unlocked"), ObserveKeyStrokesEditorApi.KEY_STROKE_TOGGLE_EDIT);
        setCheckMenuItemIsArmed(false);
        Project editModel = getObserveEditModel();
        ObserveBusinessProject project = ObserveBusinessProject.get();
        EditModule commonModule = new EditModule(ui, project, null);
        EditModule psModule = new EditModule(ui, project, editModel.getPs());
        EditModule llModule = new EditModule(ui, project, editModel.getLl());
        this.modules = Map.of(psModule.getModuleName(), psModule, llModule.getModuleName(), llModule);
        this.commonModule = commonModule;
    }

    @Override
    public void init() {
        super.init();
        MenuActionSupport.toggle(ui, ui.getEditPopup());
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        JPopupMenu popupMenu = ui.getEditPopup();
        populateMenu(popupMenu);
        SwingUtilities.invokeLater(() -> {
            popupMenu.show(ui.getNavigationScrollPane(), 0, 0);
            MenuAction.selectFirst(popupMenu);
        });
    }

    private void populateMenu(JPopupMenu popupMenu) {
        popupMenu.removeAll();
        NavigationTreeConfig config = ui.getTree().getModel().getConfig();
        Optional<EditModule> module = Optional.ofNullable(modules.get(config.getModuleName()));
        if (config.isLoadData()) {
            module.ifPresent(EditModule::addData);
        }
        if (config.isLoadReferential()) {
            if (popupMenu.getComponentCount() > 0) {
                popupMenu.addSeparator();
            }
            commonModule.addReferential();
            module.ifPresent(EditModule::addReferential);
        }
    }
}
