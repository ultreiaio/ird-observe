package fr.ird.observe.client.datasource.editor.api.navigation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSourceTemplate;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.actions.CopyAuthenticationToken;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class ShowInformation extends MenuActionSupport {

    public static JPanel createContent(ObserveSwingDataSource source) {
        String text = ObserveSwingDataSourceTemplate.generate(source);
        JPanel content = new JPanel(new BorderLayout());
        content.add(new JLabel(text), BorderLayout.CENTER);
        return content;
    }

    public ShowInformation() {
        super(t("observe.ui.action.info.storage"), t("observe.ui.action.info.storage.tip"), "information"/*UIHelper.getContentActionIconKey("information")*/, ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_SHOW_INFO);
        setCheckMenuItemIsArmed(false);
    }

    @Override
    public void init() {
        super.init();
        MenuActionSupport.toggle(ui, ui.getComponentPopupMenu());
    }

    @Override
    protected void doActionPerformed(ActionEvent e, NavigationUI ui) {
        ObserveSwingDataSource source = getDataSourcesManager().getOptionalMainDataSource().orElse(null);
        JPanel content = createContent(source);
        boolean withTokenOption = source != null && source.isServer();
        JPopupMenu popup = ui.getComponentPopupMenu();
        if (withTokenOption) {
            CopyAuthenticationToken action = new CopyAuthenticationToken(source);
            JButton copyToken = new JButton(action);
            JPanel actions = new JPanel();
            actions.add(copyToken, BorderLayout.CENTER);
            content.add(actions, BorderLayout.SOUTH);
            PopupMenuListener popupMenuListener = new PopupMenuListener() {
                @Override
                public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                    SwingUtilities.invokeLater(copyToken::grabFocus);
                }

                @Override
                public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                    popup.removePopupMenuListener(this);
                }

                @Override
                public void popupMenuCanceled(PopupMenuEvent e) {
                    popup.removePopupMenuListener(this);
                }
            };
            popup.addPopupMenuListener(popupMenuListener);
        }
        String title = t("observe.ui.title.storage.info");
        content.setBorder(new TitledBorder(title + "  "));
        popup.removeAll();
        popup.add(content);
        popup.pack();
        popup.show(ui.getNavigationScrollPane(), 0, 0);
    }

}
