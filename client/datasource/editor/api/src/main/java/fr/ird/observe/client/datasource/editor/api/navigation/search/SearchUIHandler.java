package fr.ird.observe.client.datasource.editor.api.navigation.search;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.search.actions.SearchActionSupport;
import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.SearchCriteriaUI;
import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.SearchCriteriaUIModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.renderer.DefaultListRenderer;
import org.jdesktop.swingx.renderer.StringValue;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 04/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class SearchUIHandler implements UIHandler<SearchUI>, WithClientUIContextApi {

    public static final String ESCAPE_ACTION = "escape";

    @Override
    public void afterInit(SearchUI ui) {
        SearchUIModel model = ui.getModel();

        MattePainter p = new MattePainter();
        p.setFillPaint(new GradientPaint(0, 0, Color.GRAY, 400, 10, Color.PINK));
        ui.getResultPanel().setTitlePainter(p);

        FilterableComboBox<ToolkitIdLabel> result = ui.getResult();
        Class<? extends RootOpenableDto> dtoType = ui.getModel().getModuleGroupByHelper().getDtoType();
        UIInitHelper.init(result);
        String entityLabel = I18nDecoratorHelper.getType(ObserveBusinessProject.get().getMapping().getReferenceType(dtoType));
        result.setPopupTitleText(t("observe.data.Data.type", entityLabel));
        result.init(getDecoratorService().getToolkitIdLabelDecoratorByType(dtoType), model.getResult());
        @SuppressWarnings("unchecked") ListCellRenderer<DataGroupByDtoDefinition<?, ?>> availableCriteriaRenderer =
                new DefaultListRenderer((StringValue) value -> {
                    if (value == null) {
                        return null;
                    }
                    DataGroupByDtoDefinition<?, ?> definition = (DataGroupByDtoDefinition<?, ?>) value;
                    return definition.getDefinitionLabel() + String.format(" ( %s )", definition.getMandatoryLabel());
                });
        ui.getAvailableCriteria().setRenderer(availableCriteriaRenderer);
        InputMap inputMap = ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = ui.getActionMap();
        AbstractAction escapeAction = new AbstractAction(ESCAPE_ACTION) {
            @Override
            public void actionPerformed(ActionEvent e) {
                SearchActionSupport.close(ui);
            }
        };
        inputMap.put(ObserveKeyStrokesEditorApi.KEY_STROKE_ESCAPE, ESCAPE_ACTION);
        actionMap.put(ESCAPE_ACTION, escapeAction);

        // listen when available criteria names changed, it means a new criteria was added, or removed
        model.addPropertyChangeListener(SearchUIModel.PROPERTY_AVAILABLE_CRITERIA, evt -> {
            List<DataGroupByDtoDefinition<?, ?>> availableCriteria = model.getAvailableCriteria();
            updateAvailableCriteria(ui, availableCriteria);
            updateCriteria(ui, availableCriteria, model.getCriteriaList());
        });

        List<DataGroupByDtoDefinition<?, ?>> availableCriteria = model.getAvailableCriteria();
        // set availableCriteria at start
        updateAvailableCriteria(ui, availableCriteria);
        // set criteria at start
        updateCriteria(ui, availableCriteria, model.getCriteriaList());
    }

    void updateSelectedCriteria(SearchUI ui, ItemEvent event) {
        DataGroupByDtoDefinition<?, ?> selectedItem;
        if (event.getStateChange() == ItemEvent.SELECTED) {
            JComboBox<?> source = (JComboBox<?>) event.getSource();
            selectedItem = (DataGroupByDtoDefinition<?, ?>) source.getSelectedItem();
        } else {
            selectedItem = null;
        }
        ui.getModel().setSelectedCriteria(selectedItem);
    }

    private void updateAvailableCriteria(SearchUI ui, List<DataGroupByDtoDefinition<?, ?>> availableCriteria) {
        DefaultComboBoxModel<DataGroupByDtoDefinition<?, ?>> comboBoxModel = (DefaultComboBoxModel<DataGroupByDtoDefinition<?, ?>>) ui.getAvailableCriteria().getModel();
        comboBoxModel.removeAllElements();
        comboBoxModel.addAll(availableCriteria);
        comboBoxModel.setSelectedItem(availableCriteria.isEmpty() ? null : availableCriteria.get(0));
    }

    private void updateCriteria(SearchUI ui, List<DataGroupByDtoDefinition<?, ?>> availableCriteria, List<SearchCriteriaUIModel<?>> criteria) {
        JPanel criteriaPanel = ui.getSelectedCriteriaListPanel();

        // keep track new criteria to add in ui
        List<SearchCriteriaUIModel<?>> criteriaToAdd = new LinkedList<>(criteria);

        // get existing criteria indexed by their names
        List<SearchCriteriaUI<?>> existingCriteria = Stream.of(criteriaPanel.getComponents()).filter(c -> c instanceof SearchCriteriaUI).map(c -> (SearchCriteriaUI<?>) c).collect(Collectors.toList());

        // first round to remove obsolete criteria, and update criteriaToAdd
        for (SearchCriteriaUI<?> existingCriteriaComponent : existingCriteria) {
            if (availableCriteria.contains(existingCriteriaComponent.getModel().getDefinition())) {
                // remove this criteria
                criteriaPanel.remove(existingCriteriaComponent);
            } else {
                // this criteria is still used, remove it from criteriaToAdd list
                criteriaToAdd.remove(existingCriteriaComponent.getModel());
            }
        }

        // second round to add missing criteria
        for (SearchCriteriaUIModel<?> newCriteriaModel : criteriaToAdd) {
            JAXXInitialContext initialContext = UIHelper.initialContext(ui, newCriteriaModel);
            SearchCriteriaUI<?> newCriteria = new SearchCriteriaUI<>(initialContext);
            criteriaPanel.add(newCriteria);
        }

        if (criteria.isEmpty()) {
            // re-add the information label about no criteria defined
            criteriaPanel.add(ui.getNoCriteriaDefinedPanel());
        } else {
            // remove the information label about no criteria defined
            criteriaPanel.remove(ui.getNoCriteriaDefinedPanel());
        }
        SwingUtilities.invokeLater(ui::revalidate);
        SwingUtilities.invokeLater(ui::repaint);
    }

    public void close(SearchUI ui) {
        // clear all comboBox list
        Stream.of(ui.getSelectedCriteriaListPanel().getComponents()).filter(c -> c instanceof SearchCriteriaUI).map(c -> (SearchCriteriaUI<?>) c).forEach(c -> c.getCriteriaValues().setData(List.of()));
        // clea result list
        ui.getResult().setData(List.of());
        // clean models
        ui.getModel().close();
        // remove criteria ui
        ui.getSelectedCriteriaListPanel().removeAll();
    }
}
