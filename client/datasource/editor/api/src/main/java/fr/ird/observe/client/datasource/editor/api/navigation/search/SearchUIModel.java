package fr.ird.observe.client.datasource.editor.api.navigation.search;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.SearchCriteriaUIModel;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.navigation.tree.ModuleGroupByHelper;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.i18n.I18n;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created on 04/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
@GenerateJavaBeanDefinition
public class SearchUIModel extends AbstractJavaBean {
    public static final String PROPERTY_AVAILABLE_CRITERIA = "availableCriteria";
    public static final String PROPERTY_CRITERIA_LIST = "criteriaList";
    public static final String PROPERTY_SELECTED_CRITERIA = "selectedCriteria";
    public static final String PROPERTY_SELECTED_RESULT = "selectedResult";
    public static final String PROPERTY_RESULT = "result";
    public static final String PROPERTY_RESULT_TITLE = "resultTitle";
    /**
     * Module groupBy helper.
     */
    private final ModuleGroupByHelper moduleGroupByHelper;
    /**
     * Busy model used to block ui when doing something.
     */
    private final BusyModel busyModel;
    /**
     * The navigation tree config (used to create this criteria)
     */
    private final NavigationTreeConfig navigationTreeConfig;
    /**
     * Available criteria types.
     */
    private final List<DataGroupByDtoDefinition<?, ?>> availableCriteria;
    /**
     * All criteria for the search.
     */
    private final List<SearchCriteriaUIModel<?>> criteriaList;
    /**
     * Data result.
     */
    private List<ToolkitIdLabel> result;
    /**
     * Selected criteria name.
     */
    private DataGroupByDtoDefinition<?, ?> selectedCriteria;
    /**
     * Selected result.
     */
    private ToolkitIdLabel selectedResult;

    public SearchUIModel(NavigationTreeConfig navigationTreeConfig,
                         ModuleGroupByHelper moduleGroupByHelper,
                         BusyModel busyModel) {
        this.navigationTreeConfig = navigationTreeConfig;
        this.moduleGroupByHelper = moduleGroupByHelper;
        this.busyModel = busyModel;
        this.criteriaList = new LinkedList<>();
        this.availableCriteria = new LinkedList<>(moduleGroupByHelper.getGroupByDefinitions());
        result = new LinkedList<>();
    }

    public BusyModel getBusyModel() {
        return busyModel;
    }

    public DataGroupByDtoDefinition<?, ?> getSelectedCriteria() {
        return selectedCriteria;
    }

    public void setSelectedCriteria(DataGroupByDtoDefinition<?, ?> selectedCriteria) {
        DataGroupByDtoDefinition<?, ?> oldValue = this.selectedCriteria;
        this.selectedCriteria = selectedCriteria;
        firePropertyChange(PROPERTY_SELECTED_CRITERIA, oldValue, selectedCriteria);
    }

    public List<DataGroupByDtoDefinition<?, ?>> getAvailableCriteria() {
        return availableCriteria;
    }

    public List<SearchCriteriaUIModel<?>> getCriteriaList() {
        return criteriaList;
    }

    public SearchCriteriaUIModel<?> createCriteria(DataGroupByDtoDefinition<?, ?> definition) {
        boolean navigationCriteria = Objects.equals(navigationTreeConfig.getGroupByName(), definition.getName());
        SearchCriteriaUIModel<?> criteriaModel = new SearchCriteriaUIModel<>(definition, navigationCriteria, getBusyModel(), getCriteriaList().size());
        if (navigationCriteria) {
            if (criteriaModel.isQualitative()) {
                criteriaModel.setLoadDisabledGroupBy(navigationTreeConfig.isLoadDisabledGroupBy());
                criteriaModel.setLoadEmptyGroupBy(true);
                criteriaModel.setLoadNullGroupBy(!criteriaModel.isPropertyMandatory());
            } else if (navigationTreeConfig.isLoadTemporalGroupBy()) {
                criteriaModel.setLoadTemporalGroupBy(true);
                criteriaModel.setGroupByFlavor(navigationTreeConfig.getGroupByFlavor());
            }
        } else if (criteriaModel.isQualitative()) {
            // no null groupBy if criteria is mandatory
            criteriaModel.setLoadNullGroupBy(!criteriaModel.isPropertyMandatory());
            // always show disabled groupBy by default (for create purpose)
            criteriaModel.setLoadDisabledGroupBy(true);
            // always show empty groupBy by default (for create purpose)
            criteriaModel.setLoadEmptyGroupBy(true);
        }
        return criteriaModel;
    }

    public void addCriteria(SearchCriteriaUIModel<?> criteriaModel) {

        getCriteriaList().add(criteriaModel);
        fireCriteriaList();
        // remove criteria from available criteria type
        getAvailableCriteria().remove(criteriaModel.getDefinition());
        fireAvailableCriteria();
    }

    public void removeCriteria(SearchCriteriaUIModel<?> criteriaModel) {
        getCriteriaList().remove(criteriaModel);
        // add criteria from available criteria type
        getAvailableCriteria().add(criteriaModel.getDefinition());
        // sort available criteria types
        getModuleGroupByHelper().sort(getAvailableCriteria());
        fireAvailableCriteria();
        fireCriteriaList();
        List<SearchCriteriaUIModel<?>> list = getCriteriaList();
        for (int i = 0; i < list.size(); i++) {
            SearchCriteriaUIModel<?> searchCriteriaUIModel = list.get(i);
            searchCriteriaUIModel.setIndex(i);
        }
        recomputeResult();
    }

    public List<ToolkitIdLabel> getResult() {
        return result;
    }

    public void setResult(List<ToolkitIdLabel> result) {
        String oldResultTitle = getResultTitle();
        List<ToolkitIdLabel> oldValue = this.result;
        this.result = result;
        firePropertyChange(PROPERTY_RESULT, oldValue, result);
        firePropertyChange(PROPERTY_RESULT_TITLE, oldResultTitle, getResultTitle());
    }

    public String getResultTitle() {
        if (result == null || result.isEmpty()) {
            return I18n.t("observe.ui.datasource.search.result.none");
        }
        return I18n.t("observe.ui.datasource.search.result", result.size());
    }

    public ToolkitIdLabel getSelectedResult() {
        return selectedResult;
    }

    public void setSelectedResult(ToolkitIdLabel selectedResult) {
        ToolkitIdLabel oldValue = this.selectedResult;
        this.selectedResult = selectedResult;
        firePropertyChange(PROPERTY_SELECTED_RESULT, oldValue, selectedResult);
    }

    public void recomputeResult() {
        // first round to get all results by id (and get all ids)
        Set<String> allIds = new TreeSet<>();
        List<Map<String, ToolkitIdLabel>> resultAsMap = new LinkedList<>();
        for (SearchCriteriaUIModel<?> criteriaModel : getCriteriaList()) {
            Map<String, ToolkitIdLabel> criteriaResult = criteriaModel.getResultAsMap();
            if (criteriaResult == null) {
                // no result set for this criteria
                continue;
            }
            resultAsMap.add(criteriaResult);
            allIds.addAll(criteriaResult.keySet());
        }
        // second round to get common ids
        Set<String> commonIds = new TreeSet<>(allIds);
        for (Map<String, ToolkitIdLabel> map : resultAsMap) {
            commonIds.retainAll(map.keySet());
        }
        // third round to get result
        List<ToolkitIdLabel> result = new LinkedList<>();
        for (Map<String, ToolkitIdLabel> map : resultAsMap) {
            for (Map.Entry<String, ToolkitIdLabel> entry : map.entrySet()) {
                String id = entry.getKey();
                if (commonIds.contains(id)) {
                    result.add(entry.getValue());
                    commonIds.remove(id);
                }
            }
        }
        // compute best selected result
        ToolkitIdLabel selectedResult = getSelectedResult();
        if (selectedResult != null && !commonIds.contains(selectedResult.getId())) {
            selectedResult = null;
        }
        if (selectedResult == null && result.size() > 0) {
            selectedResult = result.get(0);
        }
        setResult(result);
        setSelectedResult(selectedResult);
    }

    public ModuleGroupByHelper getModuleGroupByHelper() {
        return moduleGroupByHelper;
    }

    private void fireAvailableCriteria() {
        firePropertyChange(PROPERTY_AVAILABLE_CRITERIA, getAvailableCriteria());
    }

    private void fireCriteriaList() {
        firePropertyChange(PROPERTY_CRITERIA_LIST, getCriteriaList());
    }

    public void close() {
        result = null;
        selectedResult = null;
        for (SearchCriteriaUIModel<?> searchCriteriaUIModel : criteriaList) {
            searchCriteriaUIModel.close();
        }
    }

    public SearchCriteriaUIModel<?> getCriteria(DataGroupByDtoDefinition<?, ?> definition) {
        for (SearchCriteriaUIModel<?> criteria : criteriaList) {
            if (definition.equals(criteria.getDefinition())) {
                return criteria;
            }
        }
        // no criteria found for this definition
        return null;
    }
}
