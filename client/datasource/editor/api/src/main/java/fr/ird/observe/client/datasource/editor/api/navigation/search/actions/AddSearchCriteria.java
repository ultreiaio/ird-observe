package fr.ird.observe.client.datasource.editor.api.navigation.search.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.search.SearchUI;
import fr.ird.observe.client.datasource.editor.api.navigation.search.SearchUIModel;
import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.SearchCriteriaUIModel;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

/**
 * Created on 04/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class AddSearchCriteria extends SearchActionSupport implements WithClientUIContextApi {

    public AddSearchCriteria() {
        super("", I18n.n("observe.ui.datasource.search.add.criteria.tip"), "add", ObserveKeyStrokesEditorApi.KEY_STROKE_SEARCH_ADD_CRITERIA);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SearchUI ui) {

        SearchUIModel model = ui.getModel();
        DataGroupByDtoDefinition<?, ?> selectedCriteria = model.getSelectedCriteria();
        SearchCriteriaUIModel<?> newCriteria = model.createCriteria(selectedCriteria);
        model.addCriteria(newCriteria);
        repackDialog(getMainUI(), ui);
    }

    public static void repackDialog(ObserveMainUI mainUI, SearchUI ui) {
        SwingUtilities.invokeLater(() -> {
            JDialog dialog = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, ui);
            dialog.pack();
            System.out.println("Dialog dimension: " + dialog.getSize());
            UIHelper.center(mainUI, dialog);
        });
    }
}
