package fr.ird.observe.client.datasource.editor.api.navigation.search.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUI;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.search.SearchUI;
import fr.ird.observe.client.datasource.editor.api.navigation.search.SearchUIModel;
import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.SearchCriteriaUIModel;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.data.DataGroupByDefinition;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.dto.data.DataGroupByTemporalOption;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 04/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class CreateNewData extends SearchActionSupport implements WithClientUIContextApi {
    public CreateNewData() {
        super(I18n.n("observe.ui.datasource.search.create.new.data"), I18n.n("observe.ui.datasource.search.create.new.data"), "add", ObserveKeyStrokesEditorApi.KEY_STROKE_SEARCH_CREATE_NEW);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SearchUI ui) {

        DataSourceEditor editor = (DataSourceEditor) getMainUI().getMainUIBodyContentManager().getCurrentBody().get();
        NavigationTree tree = editor.getNavigationUI().getTree();
        NavigationTreeConfig navigationTreeConfig = tree.getModel().getConfig();
        String navigationTreeConfigGroupByName = navigationTreeConfig.getGroupByName();
        String navigationTreeConfigGroupByFlavor = navigationTreeConfig.isLoadTemporalGroupBy() ? navigationTreeConfig.getGroupByFlavor() : null;

        SearchUIModel model = ui.getModel();
        BusinessModule module = model.getModuleGroupByHelper().getModule();

        // get navigation tree config groupByDefinition
        DataGroupByDtoDefinition<?, ?> navigationTreeCriteriaDefinition = model.getModuleGroupByHelper().getDefinition(navigationTreeConfigGroupByName);

        // get criteria for this definition
        SearchCriteriaUIModel<?> navigationTreeCriteria = model.getCriteria(navigationTreeCriteriaDefinition);

        boolean temporal = navigationTreeCriteriaDefinition.isTemporal();

        DataGroupByParameter navigationTreeGroupByParameter;
        DataGroupByParameter selectedGroupByParameter;
        if (navigationTreeCriteria == null) {
            // user did not fill the special (navigation) criteria, use then an empty one with navigation config
            navigationTreeGroupByParameter = new DataGroupByParameter(navigationTreeConfigGroupByName, navigationTreeConfigGroupByFlavor, null);
            selectedGroupByParameter = new DataGroupByParameter(navigationTreeConfigGroupByName, navigationTreeConfigGroupByFlavor, null);
        } else {

            navigationTreeGroupByParameter = navigationTreeCriteria.toGroupByParameter();
            selectedGroupByParameter = navigationTreeCriteria.toGroupByParameter();
        }


        // need to reload this since in search ui it is attached to another decorator
        DataGroupByDto<?> selectedGroupByValue = getDataSourcesManager().getMainDataSource().getRootOpenableService().getGroupByDtoValue(navigationTreeCriteriaDefinition.getContainerType(), selectedGroupByParameter);
        getDecoratorService().installDecorator(selectedGroupByValue);

        String selectedGroupByParameterGroupByFlavor = selectedGroupByParameter.getGroupByFlavor();

        DataGroupByDto<?> navigationSelectedGroupByValue = selectedGroupByValue;
        boolean temporalMismatch = temporal && !Objects.equals(navigationTreeConfigGroupByFlavor, selectedGroupByParameterGroupByFlavor);
        if (temporal) {
            // check if groupByFlavor is the same
            if (temporalMismatch) {
                //  navigation groupBy value is different from the selected one, need to redefine the navigation groupBy parameter
                String groupByParameterValue = computeNavigationTemporalGroupByValue(navigationTreeGroupByParameter.getValue(), selectedGroupByParameterGroupByFlavor, navigationTreeConfigGroupByFlavor);

                navigationTreeGroupByParameter = new DataGroupByParameter(navigationTreeConfigGroupByName, navigationTreeConfigGroupByFlavor, groupByParameterValue);
                navigationSelectedGroupByValue = getDataSourcesManager().getMainDataSource().getRootOpenableService().getGroupByDtoValue(navigationTreeCriteriaDefinition.getContainerType(), navigationTreeGroupByParameter);
                getDecoratorService().installDecorator(navigationSelectedGroupByValue);
            }
        }

        // try to get groupBy node in navigation tree
        ContentRootListUINavigationNode groupByNode = (ContentRootListUINavigationNode) tree.getModel().getNodeFromPath(new String[]{navigationTreeGroupByParameter.getValue()}, false);

        if (groupByNode == null) {
            // groupBy does not exist in navigation tree, must create it and can not use navigation result since data is not in data base :)
            groupByNode = tree.getRootNode().getCapability().insertChildNode(navigationSelectedGroupByValue);
        }
        tree.selectNode(groupByNode);

        // create new trip node
        NavigationNode dataNode;
        if (module.equals(ObserveBusinessProject.get().getPsBusinessModule())) {
            // create a new ps trip
            fr.ird.observe.dto.data.ps.common.TripReference trip = new fr.ird.observe.dto.data.ps.common.TripReference();
            dataNode = groupByNode.getCapability().insertChildNode(trip);
        } else if (module.equals(ObserveBusinessProject.get().getLlBusinessModule())) {
            // create a new ps trip
            fr.ird.observe.dto.data.ll.common.TripReference trip = new fr.ird.observe.dto.data.ll.common.TripReference();
            dataNode = groupByNode.getCapability().insertChildNode(trip);
        } else {
            throw new IllegalStateException("Can't create a new trip with module (" + module + ")");
        }
        tree.selectNode(dataNode);

        SwingUtilities.invokeLater(() -> {
            // once the content exist, can fill it with the navigation criteria and all other search criteria
            fillBeanInForm(model,
                           editor,
                           selectedGroupByParameterGroupByFlavor,
                           selectedGroupByValue,
                           navigationTreeCriteriaDefinition,
                           temporalMismatch);
            close(ui);
        });
    }

    private <D extends RootOpenableDto, F extends DataGroupByDto<D>> void fillBeanInForm(SearchUIModel model,
                                                                                         DataSourceEditor editor,
                                                                                         String selectedGroupByParameterGroupByFlavor,
                                                                                         DataGroupByDto<?> selectedGroupByValue,
                                                                                         DataGroupByDefinition<D, F> navigationTreeCriteriaDefinition,
                                                                                         boolean temporalMismatch) {

        @SuppressWarnings("unchecked") ContentRootOpenableUI<D, ?> contentUI = (ContentRootOpenableUI<D, ?>) editor.getModel().getContent();
        D bean = contentUI.getModel().getStates().getBean();
        String groupByValueId = selectedGroupByValue.getId();
        if (groupByValueId == null) {
            // reset the groupBy value in bean (it may has been set from the form create method)
            navigationTreeCriteriaDefinition.setGroupByValue(bean, null);
        } else {

            // set new value If was temporal and search groupBy definition is not the same than the navigation tree one
            if (temporalMismatch) {

                // need to convert value (otherwise the very same value is in the groupByNode and in the bean)
                Date date;
                Optional<DataGroupByTemporalOption> optionalSelectedGroupByTemporalOption = DataGroupByTemporalOption.optionalValueOf(selectedGroupByParameterGroupByFlavor);
                if (optionalSelectedGroupByTemporalOption.isEmpty()) {
                    // full date
                    date = DataGroupByTemporalOption.newFullDate(selectedGroupByValue.getFilterValue());
                } else {
                    // partial date
                    DataGroupByTemporalOption selectedGroupByTemporalOption = optionalSelectedGroupByTemporalOption.get();
                    date = selectedGroupByTemporalOption.newDate(selectedGroupByValue.getFilterValue());
                }
                navigationTreeCriteriaDefinition.setGroupByValue(bean, date);
            }
        }
        for (SearchCriteriaUIModel<?> criteriaModel : model.getCriteriaList()) {
            if (navigationTreeCriteriaDefinition.equals(criteriaModel.getDefinition())) {
                // already processed
                continue;
            }
            fillBeanInForm(contentUI, bean, criteriaModel);
        }
    }

    private <D extends RootOpenableDto> void fillBeanInForm(ContentRootOpenableUI<D, ?> contentUI,
                                                            D bean,
                                                            SearchCriteriaUIModel<?> criteriaModel) {

        @SuppressWarnings("unchecked") DataGroupByDtoDefinition<D, ?> definition = (DataGroupByDtoDefinition<D, ?>) criteriaModel.getDefinition();
        DataGroupByDto<?> selectedGroupByValue = criteriaModel.getSelectedGroupByValue();
        String groupByValue = selectedGroupByValue == null ? null : selectedGroupByValue.getFilterValue();
        if (groupByValue == null) {
            // criteria value is empty
            // reset the groupBy value in bean (it may has been set from the form create method)
            definition.setGroupByValue(bean, null);
            return;
        }
        if (definition.isQualitativeWithSecondLevel()) {
            // nothing to do here, such criteria is never shown on form
            return;
        }
        Object realGroupByValue = null;
        if (definition.isQualitative()) {
            // obtain referential reference
            Optional<ReferentialDtoReference> referentialValue = contentUI.getModel().getStates().getReferenceCache().tryGetReferentialReferenceById(definition.getPropertyName(), groupByValue);
            // set it on bean
            realGroupByValue = referentialValue.orElse(null);
        } else if (definition.isTemporal()) {
            if (!criteriaModel.isLoadTemporalGroupBy()) {
                // full date
                realGroupByValue = DataGroupByTemporalOption.newFullDate(selectedGroupByValue.getFilterValue());
            } else {
                // partial date
                DataGroupByTemporalOption temporalOption = DataGroupByTemporalOption.optionalValueOf(criteriaModel.getGroupByFlavor()).orElseThrow();
                realGroupByValue = temporalOption.newDate(groupByValue);
            }
        }
        definition.setGroupByValue(bean, realGroupByValue);
    }

    private String computeNavigationTemporalGroupByValue(String value, String selectedGroupByFlavor, String navigationGroupByFlavor) {
        if (value == null) {
            // limit case, temporal null value is always null what so ever is the groupBy flavors
            return null;
        }

        Optional<DataGroupByTemporalOption> navigationTemporalOption = DataGroupByTemporalOption.optionalValueOf(navigationGroupByFlavor);
        Optional<DataGroupByTemporalOption> selectedTemporalOption = DataGroupByTemporalOption.optionalValueOf(selectedGroupByFlavor);

        if (selectedTemporalOption.isEmpty()) {
            if (navigationTemporalOption.isEmpty()) {
                // same format, no conversion
                return value;
            }
            // selected is full date
            Date fullDate = DataGroupByTemporalOption.newFullDate(value);
            // convert it to navigation groupBy flavor
            return Objects.requireNonNull(navigationTemporalOption.orElseThrow()).formatDate(fullDate);
        } else {
            Date date = selectedTemporalOption.get().newDate(value);
            if (navigationTemporalOption.isEmpty()) {
                // navigation ask for a full data
                return DataGroupByTemporalOption.formatFullDate(date);
            } else {
                // transform from selected format to navigation format
                return navigationTemporalOption.get().formatDate(date);
            }
        }
    }
}
