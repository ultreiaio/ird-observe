package fr.ird.observe.client.datasource.editor.api.navigation.search.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.api.navigation.search.SearchUI;
import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.SearchCriteriaUIModel;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeConfig;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.List;

/**
 * Created on 04/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class GotoSelectedData extends SearchActionSupport implements WithClientUIContextApi {
    public GotoSelectedData() {
        super(I18n.n("observe.ui.datasource.search.goto.selected.data"), I18n.n("observe.ui.datasource.search.goto.selected.data"), "go-jump", ObserveKeyStrokesEditorApi.KEY_STROKE_SEARCH_GO_TO);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SearchUI ui) {
        ToolkitIdLabel selectedResult = ui.getModel().getSelectedResult();
        List<SearchCriteriaUIModel<?>> criteriaList = ui.getModel().getCriteriaList();

        DataSourceEditor editor = (DataSourceEditor) getMainUI().getMainUIBodyContentManager().getCurrentBody().get();
        NavigationTree tree = editor.getNavigationUI().getTree();
        NavigationTreeConfig navigationTreeConfig = tree.getModel().getConfig();

        close(ui);
        //FIXME Using the criteriaList could help us to find the correct groupBy in navigation tree, if matches the
        //FIXME navigation config (but not sure all criteria list were applied, and not modified)
        // need to get the groupBy from the navigation configuration
        String dataId = selectedResult.getId();
        String groupByValue = getDataSourcesManager().getMainDataSource().getRootOpenableService().getGroupByValue(navigationTreeConfig.getGroupByName(), navigationTreeConfig.getGroupByFlavor(), dataId);
        NavigationNode nodeToSelect = tree.getModel().getNodeFromPath(new String[]{groupByValue, dataId}, false);
        if (nodeToSelect == null) {
            // This means the navigation tree configuration is not compliant with it
            //TODO Say it to user, and tell him what to do (or do it for him)
            throw new IllegalStateException(String.format("Can not find node for groupBy value: %s, the navigation tree configuration must be changed...", groupByValue));
        }
        tree.selectNode(nodeToSelect);
        SwingUtilities.invokeLater(ObserveUtil::cleanMemory);
    }
}
