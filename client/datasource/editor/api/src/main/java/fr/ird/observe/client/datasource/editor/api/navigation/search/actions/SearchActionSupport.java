package fr.ird.observe.client.datasource.editor.api.navigation.search.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.search.SearchUI;
import fr.ird.observe.dto.ObserveUtil;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;

import javax.swing.JDialog;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * Created on 04/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public abstract class SearchActionSupport extends JComponentActionSupport<SearchUI> {

    protected SearchActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    public static void close(SearchUI ui) {
        SwingUtilities.getAncestorOfClass(JDialog.class, ui).setVisible(false);
        ui.getHandler().close(ui);
        SwingUtilities.invokeLater(ObserveUtil::cleanMemory);
    }
}
