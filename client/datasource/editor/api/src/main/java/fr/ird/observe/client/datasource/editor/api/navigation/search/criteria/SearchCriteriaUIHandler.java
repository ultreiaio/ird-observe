package fr.ird.observe.client.datasource.editor.api.navigation.search.criteria;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.navigation.search.SearchUI;
import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.actions.ChangePropertyAction;
import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.actions.UseGroupByFlavorAction;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByOption;
import fr.ird.observe.dto.data.DataGroupByTemporalOption;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.navigation.tree.ToolkitTreeLoadingConfig;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.util.SingletonSupplier;
import org.jdesktop.swingx.painter.MattePainter;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
import java.awt.Color;
import java.awt.GradientPaint;
import java.util.List;

/**
 * Created on 04/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class SearchCriteriaUIHandler<G extends DataGroupByDto<?>> implements UIHandler<SearchCriteriaUI<G>>, WithClientUIContextApi {

    private SingletonSupplier<SearchUI> parentSupplier;

    public SearchUI getParent() {
        return parentSupplier.get();
    }

    private SearchUI getParent0(SearchCriteriaUI<G> ui) {
        return (SearchUI) SwingUtilities.getAncestorOfClass(SearchUI.class, ui);
    }

    @Override
    public void afterInit(SearchCriteriaUI<G> ui) {

        parentSupplier = SingletonSupplier.of(() -> getParent0(ui));

        MattePainter p = new MattePainter();
        p.setFillPaint(new GradientPaint(0, 0, Color.GRAY, 400, 10, Color.CYAN.brighter()));
        ui.setTitlePainter(p);

        SearchCriteriaUIModel<G> model = ui.getModel();

        FilterableComboBox<G> result = ui.getCriteriaValues();
        Class<G> dtoType = model.getContainerType();
        UIInitHelper.init(result);
        result.setPopupTitleText(model.getDefinition().getDefinitionLabel());
        result.init(getDecoratorService().getDecoratorByType(dtoType, DecoratorService.FOR_SEARCH_CLASSIFIER), List.of());

        boolean navigationCriteria = model.isNavigationCriteria();

        ui.setName(model.getName());
        // add options for this criteria
        if (model.isQualitative()) {
            // remove temporal options
            ui.getGroupByOptionsChoose().remove(ui.getLoadTemporalGroupBy());
            ui.getGroupByOptionsChoose().remove(ui.getYear());
            ui.getGroupByOptionsChoose().remove(ui.getMonth());

            if (model.isPropertyMandatory()) {
                // can't use the null groupBy
                ui.getLoadNullGroupBy().setEnabled(false);
            } else {
                // set initial value
                ui.getLoadNullGroupBy().setSelected(model.isLoadNullGroupBy());
            }
            if (navigationCriteria && !model.isLoadDisabledGroupBy()) {
                // can't use the disabled groupBy
                ui.getLoadDisabledGroupBy().setEnabled(false);
            } else {
                // set initial values
                ui.getLoadDisabledGroupBy().setSelected(model.isLoadDisabledGroupBy());
            }
            ui.getLoadEmptyGroupBy().setSelected(model.isLoadEmptyGroupBy());

        } else {
            if (model.isTemporal()) {
                // remove qualitative options
                ui.getGroupByOptionsChoose().remove(ui.getLoadDisabledGroupBy());
                ui.getGroupByOptionsChoose().remove(ui.getLoadEmptyGroupBy());
                ui.getGroupByOptionsChoose().remove(ui.getLoadNullGroupBy());

                String groupByFlavor = model.getGroupByFlavor();
                if (groupByFlavor != null) {
                    ((JRadioButton) ui.getObjectById(groupByFlavor)).setSelected(true);
                }
            }
        }
        model.addPropertyChangeListener(evt -> {
            if (SearchCriteriaUIModel.PROPERTIES_TO_COMPUTE_AVAILABLE_GROUP_BY_VALUES.contains(evt.getPropertyName())) {
                computeGroupByValues(ui);
            }
        });
        model.addPropertyChangeListener(ToolkitTreeLoadingConfig.LOAD_TEMPORAL_GROUP_BY, evt -> {
            boolean newValue = (boolean) evt.getNewValue();
            if (newValue) {
                model.setGroupByFlavor(DataGroupByTemporalOption.year.name());
            } else {
                model.setGroupByFlavor(null);
                ui.getGroupByFlavor().clearSelection();
            }
        });
        // compute initial values
        computeGroupByValues(ui);

        model.addPropertyChangeListener(SearchCriteriaUIModel.PROPERTY_SELECTED_GROUP_BY_VALUE, evt -> {
            Object newValue = evt.getNewValue();
            applyCriteria(ui, newValue != null);
        });
    }

    @Override
    public void registerActions(SearchCriteriaUI<G> ui) {
        for (DataGroupByOption option : DataGroupByOption.values()) {
            JCheckBox editor = (JCheckBox) ui.getObjectById(option.name());
            ChangePropertyAction.create(option, ui, editor, null);
        }
        for (DataGroupByTemporalOption temporalOption : DataGroupByTemporalOption.values()) {
            JRadioButton editor = (JRadioButton) ui.getObjectById(temporalOption.name());
            UseGroupByFlavorAction.create(temporalOption, ui, editor, null);
        }
    }

    private void computeGroupByValues(SearchCriteriaUI<G> ui) {
        SearchCriteriaUIModel<G> model = ui.getModel();
        BusyModel busyModel = model.getBusyModel();
        busyModel.addTask("Compute groupBy values");
        try {
            @SuppressWarnings("unchecked") DataGroupByDtoSet<?, G> groupByDtoSet = (DataGroupByDtoSet<?, G>) getDataSourcesManager().getMainDataSource().getNavigationService().getGroupByDtoSet(model.getGroupByRequest());
            List<G> groupByValues = groupByDtoSet.toList();
            G selectedGroupByValue = model.getSelectedGroupByValue();
            if (selectedGroupByValue != null) {
                if (!groupByValues.contains(selectedGroupByValue)) {
                    selectedGroupByValue = null;
                }
            }
            model.setAvailableGroupByValues(groupByValues);
            model.setSelectedGroupByValue(selectedGroupByValue);
        } finally {
            busyModel.popTask();
        }
    }

    private void applyCriteria(SearchCriteriaUI<G> ui, boolean add) {
        SearchCriteriaUIModel<G> model = ui.getModel();
        BusyModel busyModel = getParent().getModel().getBusyModel();
        busyModel.addTask("Apply criteria");
        try {
            if (add) {
                // compute results for this criteria
                List<ToolkitIdLabel> criteriaResult = getDataSourcesManager().getMainDataSource().getRootOpenableService().getBrothers(model.toGroupByParameter(), null);
                model.setResult(criteriaResult);
            } else {
                // remove results for this criteria
                model.setResult(null);
            }
            // ask parent to recompute available data universe
            getParent().getModel().recomputeResult();
        } finally {
            busyModel.popTask();
        }
    }
}
