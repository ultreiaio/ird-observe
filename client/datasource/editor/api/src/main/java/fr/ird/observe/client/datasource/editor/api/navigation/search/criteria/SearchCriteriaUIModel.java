package fr.ird.observe.client.datasource.editor.api.navigation.search.criteria;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.navigation.tree.ToolkitTreeLoadingConfig;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.i18n.I18n;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Describes a search criteria.
 * <p>
 * Created on 04/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
@GenerateJavaBeanDefinition
public class SearchCriteriaUIModel<G extends DataGroupByDto<?>> extends AbstractJavaBean {
    public static final String PROPERTY_AVAILABLE_GROUP_BY_VALUES = "availableGroupByValues";
    public static final String PROPERTY_SELECTED_GROUP_BY_VALUE = "selectedGroupByValue";
    public static final String PROPERTY_RESULT = "result";
    public static final String PROPERTY_VALID = "valid";
    public static final String PROPERTY_INDEX = "index";
    public static final String PROPERTY_CRITERIA_TITLE = "criteriaTitle";
    /**
     * Properties to listen to recompute available groupBy values.
     */
    public static final Set<String> PROPERTIES_TO_COMPUTE_AVAILABLE_GROUP_BY_VALUES = Set.of(
            ToolkitTreeLoadingConfig.LOAD_EMPTY_GROUP_BY,
            ToolkitTreeLoadingConfig.LOAD_DISABLED_GROUP_BY,
            ToolkitTreeLoadingConfig.LOAD_NULL_GROUP_BY,
            ToolkitTreeLoadingConfig.LOAD_TEMPORAL_GROUP_BY,
            ToolkitTreeLoadingConfig.GROUP_BY_FLAVOR);
    /**
     * Criteria definition.
     */
    private final DataGroupByDtoDefinition<?, G> definition;
    /**
     * Is criteria is the navigation config criteria (if so, then block some options)
     */
    private final boolean navigationCriteria;
    /**
     * Busy model used to block ui when doing something.
     */
    private final BusyModel busyModel;
    /**
     * The request used to compute groupBy values.
     * <p>
     * All the configuration is delegate to this object.
     */
    private final ToolkitTreeFlatModelRootRequest groupByRequest;
    /**
     * Index of this criteria (used to display it on title).
     */
    private int index;
    /**
     * Available groupBy values.
     */
    private List<G> availableGroupByValues;
    /**
     * Selected groupBy value.
     */
    private G selectedGroupByValue;
    /**
     * Can we apply this criteria?
     */
    private boolean valid;
    /**
     * Data result.
     */
    private List<ToolkitIdLabel> result;

    public SearchCriteriaUIModel(DataGroupByDtoDefinition<?, G> definition, boolean navigationCriteria, BusyModel busyModel, int index) {
        this.definition = definition;
        this.navigationCriteria = navigationCriteria;
        this.busyModel = busyModel;
        this.index = index;
        this.groupByRequest = new ToolkitTreeFlatModelRootRequest();
        groupByRequest.setModuleName(definition.getBusinessModule().getName());
        groupByRequest.setGroupByName(definition.getName());
        groupByRequest.setLoadData(true);
        groupByRequest.setLoadReferential(false);
    }

    public boolean isNavigationCriteria() {
        return navigationCriteria;
    }

    public BusyModel getBusyModel() {
        return busyModel;
    }

    public ToolkitTreeFlatModelRootRequest getGroupByRequest() {
        return groupByRequest;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        String oldTitle = getCriteriaTitle();
        int oldValue = this.index;
        this.index = index;
        firePropertyChange(PROPERTY_INDEX, oldValue, index);
        firePropertyChange(PROPERTY_CRITERIA_TITLE, oldTitle, getCriteriaTitle());
    }

    public List<ToolkitIdLabel> getResult() {
        return result;
    }

    public Map<String, ToolkitIdLabel> getResultAsMap() {
        return result == null ? null : result.stream().collect(Collectors.toMap(ToolkitIdBean::getId, Function.identity()));
    }

    public void setResult(List<ToolkitIdLabel> result) {
        List<ToolkitIdLabel> oldValue = this.result;
        this.result = result;
        firePropertyChange(PROPERTY_RESULT, oldValue, result);
    }

    public boolean isPropertyMandatory() {
        return definition.isPropertyMandatory();
    }

    public String getCriteriaTitle() {
        String definitionLabel = definition.getDefinitionLabel() + String.format(" ( %s )", definition.getMandatoryLabel());
        if (availableGroupByValues == null || availableGroupByValues.isEmpty()) {
            return I18n.t("observe.ui.datasource.search.criteria.title.none", getIndex() + 1, definitionLabel);
        }
        return I18n.t("observe.ui.datasource.search.criteria.title", getIndex() + 1, definitionLabel, availableGroupByValues.size());
    }

    public String getMandatoryLabel() {
        return definition.getMandatoryLabel();
    }

    public String getName() {
        return definition.getName();
    }

    public boolean isTemporal() {
        return definition.isTemporal();
    }

    public boolean isQualitative() {
        return definition.isQualitative();
    }

    public DataGroupByDtoDefinition<?, ?> getDefinition() {
        return definition;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        boolean oldValue = isValid();
        this.valid = valid;
        firePropertyChange(PROPERTY_VALID, oldValue, isValid());
    }

    public boolean isLoadEmptyGroupBy() {
        return groupByRequest.isLoadEmptyGroupBy();
    }

    public void setLoadEmptyGroupBy(boolean loadEmptyGroupBy) {
        boolean oldValue = isLoadEmptyGroupBy();
        groupByRequest.setLoadEmptyGroupBy(loadEmptyGroupBy);
        firePropertyChange(ToolkitTreeLoadingConfig.LOAD_EMPTY_GROUP_BY, oldValue, loadEmptyGroupBy);
        recomputeValid();
    }

    public boolean isLoadDisabledGroupBy() {
        return groupByRequest.isLoadDisabledGroupBy();
    }

    public void setLoadDisabledGroupBy(boolean loadDisabledGroupBy) {
        boolean oldValue = isLoadDisabledGroupBy();
        groupByRequest.setLoadDisabledGroupBy(loadDisabledGroupBy);
        firePropertyChange(ToolkitTreeLoadingConfig.LOAD_DISABLED_GROUP_BY, oldValue, loadDisabledGroupBy);
        recomputeValid();
    }

    public boolean isLoadNullGroupBy() {
        return groupByRequest.isLoadNullGroupBy();
    }

    public void setLoadNullGroupBy(boolean loadNullGroupBy) {
        boolean oldValue = isLoadNullGroupBy();
        groupByRequest.setLoadNullGroupBy(loadNullGroupBy);
        firePropertyChange(ToolkitTreeLoadingConfig.LOAD_NULL_GROUP_BY, oldValue, loadNullGroupBy);
        recomputeValid();
    }

    public boolean isLoadTemporalGroupBy() {
        return groupByRequest.isLoadTemporalGroupBy();
    }

    public void setLoadTemporalGroupBy(boolean loadTemporalGroupBy) {
        boolean oldValue = isLoadTemporalGroupBy();
        groupByRequest.setLoadTemporalGroupBy(loadTemporalGroupBy);
        firePropertyChange(ToolkitTreeLoadingConfig.LOAD_TEMPORAL_GROUP_BY, oldValue, loadTemporalGroupBy);
        recomputeValid();
    }

    public String getGroupByFlavor() {
        return groupByRequest.getGroupByFlavor();
    }

    public void setGroupByFlavor(String groupByFlavor) {
        String oldValue = getGroupByFlavor();
        groupByRequest.setGroupByFlavor(groupByFlavor);
        firePropertyChange(ToolkitTreeLoadingConfig.GROUP_BY_FLAVOR, oldValue, groupByFlavor);
        recomputeValid();
    }

    public List<G> getAvailableGroupByValues() {
        return availableGroupByValues;
    }

    public void setAvailableGroupByValues(List<G> availableGroupByValues) {
        String oldCriteriaTitle = getCriteriaTitle();
        List<? extends DataGroupByDto<?>> oldValue = this.availableGroupByValues;
        this.availableGroupByValues = availableGroupByValues;
        firePropertyChange(PROPERTY_AVAILABLE_GROUP_BY_VALUES, oldValue, availableGroupByValues);
        firePropertyChange(PROPERTY_CRITERIA_TITLE, oldCriteriaTitle, getCriteriaTitle());
    }

    public G getSelectedGroupByValue() {
        return selectedGroupByValue;
    }

    public void setSelectedGroupByValue(G selectedGroupByValue) {
        G oldValue = getSelectedGroupByValue();
        this.selectedGroupByValue = selectedGroupByValue;
        firePropertyChange(PROPERTY_SELECTED_GROUP_BY_VALUE, oldValue, selectedGroupByValue);
        recomputeValid();
    }

    private void recomputeValid() {
        boolean valid = true;
        if (isPropertyMandatory()) {
            // need a value
            valid = getSelectedGroupByValue() != null;
        }
        setValid(valid);
    }

    public DataGroupByParameter toGroupByParameter() {
        return new DataGroupByParameter(getName(), getGroupByFlavor(), getSelectedGroupByValue() == null ? null : getSelectedGroupByValue().getId());
    }

    public void close() {
        availableGroupByValues = null;
        selectedGroupByValue = null;
        result = null;
    }

    public Class<G> getContainerType() {
        return definition.getContainerType();
    }
}
