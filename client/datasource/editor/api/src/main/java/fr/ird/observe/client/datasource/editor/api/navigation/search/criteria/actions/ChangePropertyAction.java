package fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.SearchCriteriaUI;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByOption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JCheckBox;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

/**
 * Created on 05/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class ChangePropertyAction <G extends DataGroupByDto<?>>extends SearchCriteriaUIActionSupport<G> {
    private static final Logger log = LogManager.getLogger(ChangePropertyAction.class);

    private final String type;

    public static <G extends DataGroupByDto<?>>ChangePropertyAction<G> create(DataGroupByOption dataGroupByOption, SearchCriteriaUI<G> ui, JCheckBox editor, KeyStroke keyStroke) {
        String type = dataGroupByOption.name();
        editor.setName(type);
        return init(ui, editor, new ChangePropertyAction<>(dataGroupByOption.getLabel(), dataGroupByOption.getDescription(), type, keyStroke));
    }

    private ChangePropertyAction(String label, String tip, String type, KeyStroke keyStroke) {
        super(ChangePropertyAction.class.getName() + "#" + label, label, tip, null, keyStroke);
        this.type = type;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SearchCriteriaUI<G> ui) {
        boolean oldValue = ui.getModel().get(type);
        boolean newValue = !oldValue;
        log.info(String.format("Will change model property: %s to value %b", type, newValue));
        ui.getModel().set(type, newValue);
    }
}
