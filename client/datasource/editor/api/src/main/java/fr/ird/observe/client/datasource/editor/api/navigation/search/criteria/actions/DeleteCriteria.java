package fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.search.SearchUI;
import fr.ird.observe.client.datasource.editor.api.navigation.search.actions.AddSearchCriteria;
import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.SearchCriteriaUI;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.i18n.I18n;

import java.awt.event.ActionEvent;

/**
 * Created on 05/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class DeleteCriteria<G extends DataGroupByDto<?>> extends SearchCriteriaUIActionSupport<G> implements WithClientUIContextApi {

    public DeleteCriteria() {
        super(null, I18n.n("observe.ui.datasource.search.delete.criteria.tip"), "delete", ObserveKeyStrokesEditorApi.KEY_STROKE_SEARCH_DELETE_CRITERIA);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SearchCriteriaUI<G> ui) {
        SearchUI parent = ui.getHandler().getParent();
        BusyModel busyModel = parent.getModel().getBusyModel();
        busyModel.addTask("Delete criteria");
        try {
            parent.getModel().removeCriteria(ui.getModel());
            AddSearchCriteria.repackDialog(getMainUI(), parent);
        } finally {
            busyModel.popTask();
        }
    }
}
