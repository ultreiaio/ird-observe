package fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.search.criteria.SearchCriteriaUI;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByTemporalOption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JRadioButton;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Created on 05/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class UseGroupByFlavorAction<G extends DataGroupByDto<?>> extends SearchCriteriaUIActionSupport<G> {
    private static final Logger log = LogManager.getLogger(UseGroupByFlavorAction.class);

    public static <G extends DataGroupByDto<?>>UseGroupByFlavorAction<G> create(DataGroupByTemporalOption groupByFlavor, SearchCriteriaUI<G> ui, JRadioButton editor, KeyStroke keyStroke) {
        editor.setEnabled(false);
        return init(ui, editor, new UseGroupByFlavorAction<>(groupByFlavor, keyStroke));
    }

    private final String groupByFlavor;

    private UseGroupByFlavorAction(DataGroupByTemporalOption groupByFlavor, KeyStroke keyStroke) {
        super(UseGroupByFlavorAction.class.getName() + "#" + groupByFlavor, groupByFlavor.getLabel(), groupByFlavor.getDescription(), null, keyStroke);
        this.groupByFlavor = groupByFlavor.name();
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SearchCriteriaUI ui) {
        String oldValue = ui.getModel().getGroupByFlavor();
        if (Objects.equals(oldValue, groupByFlavor)) {
            return;
        }
        log.info(String.format("Will use group by flavor: %s", groupByFlavor));
        ui.getModel().setGroupByFlavor(groupByFlavor);
    }
}
