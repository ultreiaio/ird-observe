package fr.ird.observe.client.datasource.editor.api.navigation.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 23/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class NavigationContext<C extends NavigationContext<C>> implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(NavigationContext.class);

    private NavigationInitializer<C> initializer;

    /**
     * Init initializer using this context.
     *
     * @param initializer initializer to init
     * @return the initializer userObject
     */
    public final Object init(NavigationInitializer<C> initializer) {
        this.initializer = Objects.requireNonNull(initializer);
        return initializer.doInit(this);
    }

    /**
     * Open node.
     */
    public final void open() {
        initializer.doOpen(this);
    }

    /**
     * Reload initializer.
     *
     * @return new user object for the node
     */
    public final Object reload() {
        return initializer.doReLoad(this);
    }

    public final ReferentialLocale getReferentialLocale() {
        return getDecoratorService().getReferentialLocale();
    }

    public final ObserveServicesProvider getServicesProvider() {
        return getMainDataSource();
    }

    public final ObserveSwingDataSource getMainDataSource() {
        return getDataSourcesManager().getMainDataSource();
    }

    public final <R extends DtoReference> R reloadReference(R reference, Decorator decorator) {
        reference = getMainDataSource().getReferenceProvider().getReference(reference.getReferenceType0(), reference.getId(), decorator.definition().classifier());
        reference.registerDecorator(decorator);
        log.info(String.format("Reloaded reference %s → %s", reference.getId(), reference));
        return reference;
    }

    public final <R extends DtoReference> R getReference(Class<R> referenceType, String id, Decorator decorator) {
        R reference = getMainDataSource().getReferenceProvider().getReference(referenceType, id, decorator.definition().classifier());
        reference.registerDecorator(decorator);
        log.info(String.format("Loaded reference %s → %s", reference.getId(), reference));
        return reference;
    }

    public final <R extends ReferentialDtoReference> ReferentialDtoReferenceSet<R> getReferentialReferenceSet(Class<R> referenceType, Decorator decorator) {
        ReferentialDtoReferenceSet<R> reference = getMainDataSource().getReferentialService().getReferenceSet(referenceType, null);
        reference.stream().forEach(e -> e.registerDecorator(decorator));
        log.info(String.format("Loaded %d reference(s) of type: %s", reference.size(), reference.getType().getName()));
        return reference;
    }

    public final <D extends RootOpenableDto, R extends DataGroupByDto<D>> void initReference(DataGroupByDtoSet<D, R> references) {
        if (references == null) {
            return;
        }
        getDecoratorService().installDecorator(references);
        references.stream().forEach(reference -> {
            if (reference.isNull()) {
                // the service just gave us the i18n key
                reference.setFilterText(I18n.t(reference.getFilterText()));
            }
        });
        log.info(String.format("Init %d reference(s) of type: %s", references.size(), references.getType().getName()));
    }

    public final void updateSelectedNodes(NavigationNode node) {
        List<IdNode<?>> selectNodes = new LinkedList<>();
        NavigationNode n = node;
        while (n.getParent() != null) {
            if (n.getScope().isSelectNode()) {
                // always update select node id
                n.updateSelectNodeId();
                IdNode<?> selectNode = n.getInitializer().getSelectNode();
                log.info(String.format("Node %s Keep persisted selected node with id: %s", n.getClass().getSimpleName(), selectNode.getClass().getName()));
                selectNodes.add(selectNode);
            }
            n = n.getParent();
        }
        Project navigationSelect = getObserveSelectModel();
        navigationSelect.replaceBy(selectNodes);
        log.debug(String.format("New selected ids from tree\n\t%s", selectNodes.stream().map(IdNode::getId).collect(Collectors.joining("\n\t"))));
    }

    public final ReferencesCache newReferenceCache() {
        return new ReferencesCache(getMainDataSource(), !getClientConfig().isValidationUseDisabledReferential());
    }

    protected NavigationInitializer<C> getInitializer() {
        return initializer;
    }
}
