package fr.ird.observe.client.datasource.editor.api.navigation.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfigOption;
import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.NodeCapability;
import fr.ird.observe.client.util.DtoIconHelper;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.stats.StatisticValue;

import javax.swing.Icon;
import javax.swing.UIManager;
import java.awt.Color;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Handler of a node to do specific stuff.
 * <p>
 * Created on 24/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class NavigationHandler<N extends NavigationNode> {
    private Color disabledColor;
    private Color emptyColor;
    private Color notPersistedColor;
    private Color notLoadedColor;

    public abstract N getNode();

    public abstract NodeCapability<?> getCapability();

    public abstract String getText();

    public NavigationContext<?> getContext() {
        return getNode().getContext();
    }

    public String getContentTitle() {
        return getNode().getScope().getI18nTranslation("type");
    }

    public ContentMode getContentMode() {
        if (!getNode().getInitializer().isEditable()) {
            return ContentMode.READ;
        }
        return null;
    }

    public final Icon getContentIcon() {
        return getIcon(false);
    }

    public boolean isLeaf() {
        if (!getNode().getAllowsChildren()) {
            return true;
        }
        return getNode().isLoaded() && getNode().getChildCount() == 0;
    }

    public final Color getColor() {
        return getColor0(() -> {
            if (getNode().isRoot()) {
                return null;
            }
            // ask to parent (except for override methods)
            return getNode().getParent().getColor();
        });
    }

    public final Icon getIcon() {
        return getIcon(getNode().getScope().isSubNode());
    }

    /**
     * @return {@code true} if this node can build some children
     */
    public boolean canBuildChildren() {
        return getNode().isContainer();
    }

    public final void open() {
        N node = getNode();
        // We can't predicate if structure was modified, so let's always refresh internal states
        //FIXME Find a way to make this more smooth (listen datasource modification API (to create!!!))
        node.dirtyStructure();
        node.getContext().open();
        if (node.isLoaded()) {
            // already loaded, nothing more to do
            return;
        }
        if (canBuildChildren()) {
            buildChildren();
            if (canLoadChildrenOnOpen()) {
                autoLoadChildren();
            }
        }
        node.loaded();
    }

    public void buildChildren() {
    }

    public int getNodePosition(DtoReference reference) {
        return getNode().getChildCount();
    }

    public final Icon getIcon(boolean small) {
        String iconPath = getNode().getScope().getIconPath();
        return DtoIconHelper.getIcon(Objects.requireNonNull(iconPath), small);
    }

    public final Color getDisabledColor() {
        if (disabledColor == null) {
            disabledColor = UIManager.getColor(ClientConfigOption.TREE_NODE_DISABLED_COLOR.getKey());
        }
        return disabledColor;
    }

    public final Color getEmptyColor() {
        if (emptyColor == null) {
            emptyColor = UIManager.getColor(ClientConfigOption.TREE_NODE_EMPTY_COLOR.getKey());
        }
        return emptyColor;
    }

    public final Color getNotPersistedColor() {
        if (notPersistedColor == null) {
            notPersistedColor = UIManager.getColor(ClientConfigOption.TREE_NODE_UNSAVED_COLOR.getKey());
        }
        return notPersistedColor;
    }

    public final Color getNotLoadedColor() {
        if (notLoadedColor == null) {
            notLoadedColor = UIManager.getColor(ClientConfigOption.TREE_NODE_UNLOADED_COLOR.getKey());
        }
        return getColor0(() -> notLoadedColor);
    }

    protected Color getColor0(Supplier<Color> defaultValue) {
        return defaultValue.get();
    }

    /**
     * @return {@code true} if this node can load his children while opening
     */
    protected final boolean canLoadChildrenOnOpen() {
        if (getNode().getChildCount() != 1) {
            // never auto-load children if more than one child
            return false;
        }
        NavigationNode child = getNode().getChildAt(0);
        return child.isReferenceContainer() || child.isReference();
    }

    protected final void autoLoadChildren() {
        Enumeration<?> children = getNode().children();
        while (children.hasMoreElements()) {
            NavigationNode childrenNode = (NavigationNode) children.nextElement();
            if (childrenNode.isNotLoaded()) {
                childrenNode.populateChildrenIfNotLoaded();
                //FIXME Make sure this is ok ?
//                childrenNode.loaded();
            }
        }
    }

    public static String decorateStatistics(StatisticValue... stats) {
        Iterator<StatisticValue> iterator = List.of(stats).iterator();
        StringBuilder result = new StringBuilder(" ( ");
        while (iterator.hasNext()) {
            StatisticValue next = iterator.next();
            result.append(next.getValueWithSymbol());
            if (iterator.hasNext()) {
                result.append(" - ");
            }
        }
        result.append(" )");
        return result.toString();
    }
}
