package fr.ird.observe.client.datasource.editor.api.navigation.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.Objects;
import java.util.Optional;

/**
 * Created on 25/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class NavigationInitializer<C extends NavigationContext<C>> {

    private final NavigationScope scope;
    private final SingletonSupplier<BusinessModule> module;
    private final SingletonSupplier<BusinessSubModule> subModule;
    private final SingletonSupplier<Boolean> subNode;
    private final SingletonSupplier<String> logPrefix;
    private final ObserveBusinessProject businessProject;
    private SingletonSupplier<IdNode<?>> selectNode;
    private SingletonSupplier<IdNode<?>> editNode;
    private SingletonSupplier<Boolean> editable;
    private SingletonSupplier<ReferentialLocale> referentialLocale;

    public NavigationInitializer(NavigationScope scope) {
        this.scope = Objects.requireNonNull(scope);
        this.businessProject = ObserveBusinessProject.get();
        this.module = SingletonSupplier.of(() -> getScope().computeModule(businessProject));
        this.subModule = SingletonSupplier.of(() -> getScope().computeSubModule(ObserveBusinessProject.get(), getModule()));
        this.subNode = SingletonSupplier.of(scope::isSubNode);
        this.logPrefix = SingletonSupplier.of(() -> String.format("[%s] ", getScope().getContentUiType().getSimpleName()));
    }

    protected abstract Object init(NavigationContext<C> context);

    protected abstract void open(NavigationContext<C> context);

    protected abstract void reload(NavigationContext<C> context);

    public final Object doInit(NavigationContext<C> context) {
        this.selectNode = scope.computeSelectNode(context::getObserveSelectModel);
        this.editNode = scope.computeEditNode(context::getObserveEditModel);
        this.editable = scope.computeEditable(context);
        this.referentialLocale = SingletonSupplier.of(context::getReferentialLocale);
        return init(context);
    }

    public final void doOpen(NavigationContext<C> context) {
        editable.clear();
        open(context);
    }

    public final Object doReLoad(NavigationContext<C> context) {
        reload(context);
        return init(context);
    }

    public final NavigationScope getScope() {
        return scope;
    }

    public final ObserveBusinessProject getBusinessProject() {
        return businessProject;
    }

    public final BusinessModule getModule() {
        return module.get();
    }

    public final BusinessSubModule getSubModule() {
        return subModule.get();
    }

    public final Boolean isSubNode() {
        return subNode.get();
    }

    public final String getLogPrefix() {
        return logPrefix.get();
    }

    public final IdNode<?> getSelectNode() {
        return selectNode.get();
    }

    public final IdNode<? extends IdDto> getEditNode() {
        return editNode.get();
    }

    public final boolean isEditable() {
        return editable.get();
    }

    public final String getEditNodeId() {
        return Optional.ofNullable(getEditNode()).map(IdNode::getId).orElse(null);
    }

    public final void updateSelectNodeId(String id) {
        if (!getScope().isSelectNode()) {
            throw new IllegalStateException(String.format("%s Can't update select id on this node.", getLogPrefix()));
        }
        getSelectNode().setId(id);
    }

    @Override
    public String toString() {
        return getLogPrefix();
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale.get();
    }

    public abstract String toPath();
}
