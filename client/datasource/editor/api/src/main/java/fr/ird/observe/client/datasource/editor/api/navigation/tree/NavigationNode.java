package fr.ird.observe.client.datasource.editor.api.navigation.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUINavigationCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTreeModel;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ContainerCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.GroupByCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.NodeCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.NullCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ReferenceCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ReferenceContainerCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.reference.DtoReference;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.Icon;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import java.awt.Color;
import java.awt.Font;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;

/**
 * Created on 25/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class NavigationNode extends DefaultMutableTreeNode {
    private static final Logger log = LogManager.getLogger(NavigationNode.class);
    final SingletonSupplier<NavigationScope> scope;
    private final NavigationContext<?> context;
    private final NavigationHandler<?> handler;
    private final NodeCapability<?> capability;
    private final SingletonSupplier<NavigationNode> previous;
    private final SingletonSupplier<NavigationNode> next;
    private final SingletonSupplier<Boolean> withPrevious;
    private final SingletonSupplier<Boolean> withNext;
    private boolean loaded;
    private NavigationInitializer<?> initializer;
    private boolean adjusting;

    //--------------------------------------------------------------------------------------------
    // API methods
    //--------------------------------------------------------------------------------------------

    public NavigationNode() {
        this.scope = SingletonSupplier.of(() -> Objects.requireNonNull(NavigationScopes.getNavigationScope(getClass())));
        this.context = createContext();
        this.capability = createCapability();
        this.handler = createHandler();
        setAllowsChildren(isReferenceContainer() || isContainer());
        this.previous = SingletonSupplier.of(this::findPrevious, false);
        this.next = SingletonSupplier.of(this::findNext, false);
        this.withPrevious = SingletonSupplier.of(() -> getPrevious() != null);
        this.withNext = SingletonSupplier.of(() -> getNext() != null);
    }

    protected abstract NavigationContext<?> createContext();

    protected abstract NavigationHandler<?> createHandler();

    public final void init(NavigationInitializer<?> initializer) {
        this.initializer = Objects.requireNonNull(initializer);
        log.info(String.format("Init node (%d): %s", RootNavigationNode.incrementsAndGetNodeCount(), getClass().getName()));
        // init node context
        @SuppressWarnings({"unchecked", "rawtypes"}) Object userData = context.init((NavigationInitializer) initializer);
        log.info("Init done with userData: " + userData);
        // set user object of this node
        setUserObject(userData);
    }

    public final void open() {
        getHandler().open();
    }

    protected NodeCapability<?> createCapability() {
        return new NullCapability<>(this);
    }

    public final NavigationScope getScope() {
        return scope.get();
    }

    public NavigationInitializer<?> getInitializer() {
        return initializer;
    }

    public NavigationContext<?> getContext() {
        return context;
    }

    public NavigationHandler<?> getHandler() {
        return handler;
    }

    public NodeCapability<?> getCapability() {
        return capability;
    }

    public final boolean isLoaded() {
        return loaded;
    }

    public final boolean isNotLoaded() {
        return !loaded;
    }

    public final void dirty() {
        if (isNotLoaded()) {
            // Avoid re-entrant code
            return;
        }
        loaded = false;
        log.info(String.format("%s Make dirty node %s", getInitializer().getLogPrefix(), this));
        dirtyStructure();
    }

    public void dirtyStructure() {
        withPrevious.clear();
        withNext.clear();
        previous.clear();
        next.clear();
    }

    public final void loaded() {
        if (isLoaded()) {
            // Avoid re-entrant code
            return;
        }
        loaded = true;
        // when node is loaded, refresh ui
        getTreeModel().ifPresent(t -> nodeChanged());
    }

    public final boolean isContainer() {
        return getCapability() instanceof ContainerCapability;
    }

    public final boolean isLayoutContainer() {
        return getCapability() instanceof ContentLayoutUINavigationCapability;
    }

    public final boolean isReferenceContainer() {
        return getCapability() instanceof ReferenceContainerCapability;
    }

    public final boolean isGroupBy() {
        return getCapability() instanceof GroupByCapability<?>;
    }

    public final boolean isReference() {
        return getCapability() instanceof ReferenceCapability;
    }

    public final boolean isEqualsId(String id) {
        Object userObject = getUserObject();
        if (userObject == null) {
            return false;
        }
        if (userObject instanceof String) {
            return Objects.equals(id, userObject);
        }
        if (userObject instanceof Class<?>) {
            return Objects.equals(id, ((Class<?>) userObject).getName());
        }
        if (userObject instanceof DtoReference) {
            return Objects.equals(id, ((DtoReference) userObject).getId());
        }
        if (userObject instanceof DataGroupByDto<?>) {
            return Objects.equals(id, ((DataGroupByDto<?>) userObject).getFilterValue());
        }
        throw new IllegalStateException(String.format("Can't compute isEqualsId with userObject: %s on %s", userObject, this));
    }

    public final boolean isEqualsPath(String path) {
        return Objects.equals(toPath(), path);
    }

    public final Icon getIcon() {
        return getHandler().getIcon();
    }

    public final Color getColor() {
        return isNotLoaded() ? getHandler().getNotLoadedColor() : getHandler().getColor();
    }

    public NavigationNode getPrevious() {
        return previous.get();
    }

    public NavigationNode getNext() {
        return next.get();
    }

    public SingletonSupplier<Boolean> getWithPrevious() {
        return withPrevious;
    }

    public SingletonSupplier<Boolean> getWithNext() {
        return withNext;
    }

    @Override
    public final String toString() {
        return getHandler().getText();
    }

    @Override
    public final boolean isLeaf() {
        return getHandler().isLeaf();
    }

    public final boolean isNotLeaf() {
        return !isLeaf();
    }

    public final <R extends DtoReference> List<R> getChildrenReferences(R reference) {
        @SuppressWarnings("unchecked") Class<R> referenceType = (Class<R>) reference.getReferenceType();
        Enumeration<?> children = children();
        List<R> result = new LinkedList<>();
        String excludedId = reference.getId();
        while (children.hasMoreElements()) {
            NavigationNode o = (NavigationNode) children.nextElement();
            if (o.isReference()) {
                ReferenceCapability<?> capability = (ReferenceCapability<?>) o.getCapability();
                DtoReference nodeReference = capability.getReference();
                if (nodeReference.isNotPersisted()) {
                    continue;
                }
                if (referenceType.isAssignableFrom(nodeReference.getReferenceType()) && !Objects.equals(excludedId, nodeReference.getId())) {
                    result.add(referenceType.cast(nodeReference));
                }
            }
        }
        return result;
    }

    public final <R extends DataGroupByDto<?>> List<R> getChildrenReferences(R reference) {
        @SuppressWarnings("unchecked") Class<R> referenceType = (Class<R>) reference.getClass();
        Enumeration<?> children = children();
        List<R> result = new LinkedList<>();
        String excludedId = reference.getId();
        while (children.hasMoreElements()) {
            NavigationNode o = (NavigationNode) children.nextElement();
            if (o.isGroupBy()) {
                GroupByCapability<?> capability = (GroupByCapability<?>) o.getCapability();
                DataGroupByDto<?> nodeReference = capability.getReference();
                if (referenceType.isAssignableFrom(nodeReference.getClass()) && !Objects.equals(excludedId, nodeReference.getId())) {
                    result.add(referenceType.cast(nodeReference));
                }
            }
        }
        return result;
    }

    public final <R extends DtoReference> List<R> getChildrenReferences(Class<R> referenceType) {
        Enumeration<?> children = children();
        List<R> result = new LinkedList<>();
        while (children.hasMoreElements()) {
            NavigationNode o = (NavigationNode) children.nextElement();
            if (o.isReference()) {
                ReferenceCapability<?> capability = (ReferenceCapability<?>) o.getCapability();
                DtoReference nodeReference = capability.getReference();
                if (nodeReference.isNotPersisted()) {
                    continue;
                }
                if (referenceType.isAssignableFrom(nodeReference.getReferenceType())) {
                    result.add(referenceType.cast(nodeReference));
                }
            }
        }
        return result;
    }

    //--------------------------------------------------------------------------------------------
    // Update methods
    //--------------------------------------------------------------------------------------------

    public final <R extends DtoReference> int getChildrenPosition(R reference, Comparator<R> referenceComparator) {
        List<R> list = getChildrenReferences(reference);
        list.add(reference);
        list.sort(referenceComparator);
        return list.indexOf(reference);
    }

    public final <R extends DataGroupByDto<?>> int getGroupByChildrenPosition(R reference, Decorator decorator) {
        List<R> list = getChildrenReferences(reference);
        list.add(reference);
        decorator.sort(list, 1);
        return list.indexOf(reference);
    }

    public final void populateChildrenIfNotLoaded() {
        if (isLoaded()) {
            // already loaded
            return;
        }
        if (!getHandler().canBuildChildren()) {
            // nothing to load
            return;
        }
        getHandler().buildChildren();
        loaded();
    }

    public final void reloadNodeData() {
        reloadNodeData(false);
    }

    public void reloadNodeData(boolean reloadChildren) {
        dirty();
        Object userObject = getContext().reload();
        setUserObject(userObject);
        loaded();
        if (reloadChildren) {
            Enumeration<?> children = children();
            while (children.hasMoreElements()) {
                NavigationNode childNode = (NavigationNode) children.nextElement();
                childNode.reloadNodeData();
            }
        }
    }

    public void reloadNodeDataAndChildren() {
        dirty();
        Object userObject = getContext().reload();
        setUserObject(userObject);
        loaded();
        Enumeration<?> children = children();
        while (children.hasMoreElements()) {
            NavigationNode childNode = (NavigationNode) children.nextElement();
            if (childNode.isLoaded()) {
                childNode.updateNode();
            } else {
                Object childrenUserObject = childNode.getContext().reload();
                childNode.setUserObject(childrenUserObject);
            }
        }
    }

    public void updateSelectNodeId() {
        if (getScope().isSelectNode()) {
            ReferenceCapability<?> capability = (ReferenceCapability<?>) getCapability();
            String id = capability.getReference().getId();
            getInitializer().updateSelectNodeId(id);
        }
    }

    //--------------------------------------------------------------------------------------------
    // Node changed methods
    //--------------------------------------------------------------------------------------------

    public final void updateNode() {
        reloadNodeData();
        removeAllChildren();
        dirty();
        populateChildrenIfNotLoaded();
        loaded();
    }

    public final void nodeChanged(boolean refreshFromParent, boolean refreshChildren) {
        if (refreshFromParent) {
            getParent().nodeChanged(false, false);
        }
        if (refreshChildren) {
            nodeChangedDeep();
        } else {
            nodeChanged();
        }
    }

    public final void nodeChanged() {
        NavigationTreeModel model = getTreeModel().orElseThrow(IllegalStateException::new);
        log.debug(String.format("Will refresh node %s", this));
        model.nodeChanged(this);
    }

    public final void nodeChangedDeep() {
        NavigationTreeModel model = getTreeModel().orElseThrow(IllegalStateException::new);
        log.debug(String.format("Will refresh deep node %s", this));
        model.nodeChanged(this);
        // repaint children nodes
        Enumeration<?> e = this.children();
        while (e.hasMoreElements()) {
            NavigationNode child = (NavigationNode) e.nextElement();
            model.nodeChanged(child);
            //FIXME-2021-09-05 Too much node changes, and are we using this ?
//            if (child.isNotLeaf()) {
//                child.nodeChangedDeep();
//            }
        }
    }

    public final void refreshToRoot() {
        if (!isRoot()) {
            log.info(String.format("Refresh to Root node: %s", this));
            nodeChanged();
            getParent().refreshToRoot();
        }
    }

    public final void reloadNodeDataToRoot() {
        if (!isRoot()) {
            log.info(String.format("Reload to Root node: %s", this));
            reloadNodeData();
            getParent().reloadNodeDataToRoot();
        }
    }

    public final void reloadNodeDataToRootAndChildren() {
        log.info(String.format("Reload to Root node: %s", this));
        updateNode();
        getParent().reloadNodeDataToRoot();
    }

    //--------------------------------------------------------------------------------------------
    // Modify structure methods
    //--------------------------------------------------------------------------------------------

    public boolean isAdjusting() {
        return adjusting;
    }

    public void adjusting() {
        this.adjusting = true;
    }

    public void unAdjusting() {
        this.adjusting = false;
    }

    @Override
    public void insert(MutableTreeNode newChild, int childIndex) {
        Optional<NavigationTreeModel> treeModel = getTreeModel();
        if (treeModel.isPresent()) {
            adjusting();
            log.info(String.format("Insert node from model: %s", newChild));
            try {
                treeModel.get().insertNodeInto(newChild, this, childIndex);
            } finally {
                unAdjusting();
            }
            return;
        }
        super.insert(newChild, childIndex);
    }

    @Override
    public void remove(int childIndex) {
        Optional<NavigationTreeModel> treeModel = getTreeModel();
        if (treeModel.isPresent()) {
            adjusting();
            NavigationNode childNode = getChildAt(childIndex);
            log.info(String.format("%s Remove node from model: %s", getInitializer(), childNode));
            try {
                treeModel.get().removeNodeFromParent(childNode);
            } finally {
                unAdjusting();
            }
            return;
        }
        super.remove(childIndex);
    }

    public void removeChildren(Set<String> ids) {
        for (String id : ids) {
            NavigationNode childNode = findChildById(id);
            Objects.requireNonNull(childNode, String.format("Could not find child node from %s with id: '%s'.", id, this)).removeFromParent();
        }
    }

    public void moveNode(NavigationNode child, int position) {
        NavigationTreeModel model = getTreeModel().orElseThrow(IllegalStateException::new);
        remove(child);
        insert(child, position);
        model.nodeStructureChanged(this);
    }

    //--------------------------------------------------------------------------------------------
    // Find methods
    //--------------------------------------------------------------------------------------------

    public NavigationNode findChildById(String id) {
        if (id == null) {
            return null;
        }
        if (isEqualsId(id)) {
            return this;
        }
        populateChildrenIfNotLoaded();
        if (getChildCount() == 0) {
            return null;
        }
        Enumeration<?> children = children();
        while (children.hasMoreElements()) {
            NavigationNode node = (NavigationNode) children.nextElement();
            if (node.isEqualsId(id)) {
                return node;
            }
        }
        return null;
    }

    public <N extends NavigationNode> N findChildByType(Class<N> childType) {
        populateChildrenIfNotLoaded();
        if (isLeaf()) {
            return null;
        }
        Enumeration<?> children = children();
        while (children.hasMoreElements()) {
            NavigationNode node = (NavigationNode) children.nextElement();
            if (childType.isAssignableFrom(node.getClass())) {
                return childType.cast(node);
            }
        }
        return null;
    }

    public <N extends NavigationNode> N findChildByType(Class<N> childType, String id) {
        populateChildrenIfNotLoaded();
        if (isLeaf()) {
            return null;
        }
        Enumeration<?> children = children();
        while (children.hasMoreElements()) {
            NavigationNode node = (NavigationNode) children.nextElement();
            if (node.isEqualsId(id) && childType.isAssignableFrom(node.getClass())) {
                return childType.cast(node);
            }
        }
        return null;
    }

    public NavigationNode findPrevious() {
        NavigationNode result = findPreviousSibling();
        if (result != null) {
            log.info(String.format("%s - Found direct previous sibling: %s", getInitializer(), result.getInitializer()));
            return result;
        }
        // go to parent to find this previous sibling
        NavigationNode parent = getParent();
        do {
            NavigationNode newParent = null;
            if (parent != null) {
                log.info(String.format("%s - Will try to get previous parent sibling from parent: %s", getInitializer(), parent.getInitializer()));
                newParent = parent.findPrevious();
            }
            // get new parent candidate
            if (newParent == null) {
                return null;
            }
            log.info(String.format("%s - Found previous parent sibling candidate: %s", getInitializer(), newParent.getInitializer()));

            // go to the last child
            result = newParent.findLastChild(getClass());
            if (result == null) {
                // will try from this parent
                parent = newParent;
                log.info(String.format("%s - No matching previous sibling from parent candidate: %s", getInitializer(), newParent.getInitializer()));
            } else {
                log.info(String.format("%s - Found matching previous sibling from parent candidate: %s", getInitializer(), result.getInitializer()));
            }
        } while (result == null);
        return result;
    }

    public NavigationNode findNext() {
        NavigationNode result = findNextSibling();
        if (result != null) {
            log.info(String.format("%s - Found direct next sibling: %s", getInitializer(), result.getInitializer()));
            return result;
        }
        // go to parent to find this next sibling
        NavigationNode parent = getParent();
        do {
            // get new parent candidate
            NavigationNode newParent = parent == null ? null : parent.findNext();
            if (newParent == null) {
                return null;
            }
            log.info(String.format("%s - Found next parent sibling candidate: %s", getInitializer(), newParent.getInitializer()));
            // go to first child
            result = newParent.findFirstChild(getClass());
            if (result == null) {
                // will try from this parent
                parent = newParent;
                log.info(String.format("%s - No matching next sibling from parent candidate: %s", getInitializer(), newParent.getInitializer()));
            } else {
                log.info(String.format("%s - Found matching next sibling from parent candidate: %s", getInitializer(), result.getInitializer()));
            }
        } while (result == null);
        return result;
    }

    /**
     * @return previous sibling with exact node type matching
     */
    public NavigationNode findPreviousSibling() {
        NavigationNode parent = getParent();
        if (parent == null) {
            return null;
        }
        int index = parent.getIndex(this);
        Vector<?> children = parent.children;
        for (int i = index - 1; i > -1; i--) {
            NavigationNode o = (NavigationNode) children.elementAt(i);
            if (Objects.equals(getClass(), o.getClass())) {
                return o;
            }
        }
        return null;
    }

    /**
     * @param siblingId id to seek
     * @return sibling node with the given id
     */
    public NavigationNode findSibling(String siblingId) {
        NavigationNode parent = getParent();
        return parent.findChildByType(getClass(), siblingId);
    }

    /**
     * @param siblingId id to seek
     * @return sibling node with the given id (using parent sibling on given id)
     */
    public NavigationNode findParentSibling(String siblingId) {
        NavigationNode sibling = getParent().findSibling(siblingId);
        return sibling.findChildByType(getClass());
    }

    /**
     * @return next sibling with exact node type matching
     */
    public NavigationNode findNextSibling() {
        NavigationNode parent = getParent();
        if (parent == null) {
            return null;
        }
        int index = parent.getIndex(this);
        int childCount = parent.getChildCount();
        Vector<?> children = parent.children;
        for (int i = index + 1; i < childCount; i++) {
            NavigationNode o = (NavigationNode) children.elementAt(i);
            if (Objects.equals(getClass(), o.getClass())) {
                return o;
            }
        }
        return null;
    }

    /**
     * @return first child with exact node type matching
     */
    public NavigationNode findFirstChild(Class<? extends NavigationNode> childNodeType) {
        populateChildrenIfNotLoaded();

        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            NavigationNode o = (NavigationNode) children.elementAt(i);
            if (Objects.equals(o.getClass(), childNodeType)) {
                return o;
            }
        }
        return null;
    }

    /**
     * @return last child with exact node type matching
     */
    public NavigationNode findLastChild(Class<? extends NavigationNode> childNodeType) {
        populateChildrenIfNotLoaded();

        int childCount = getChildCount();
        for (int i = childCount - 1; i > -1; i--) {
            NavigationNode o = (NavigationNode) children.elementAt(i);
            if (Objects.equals(o.getClass(), childNodeType)) {
                return o;
            }
        }
        return null;
    }

    //--------------------------------------------------------------------------------------------
    // Go up methods
    //--------------------------------------------------------------------------------------------


    public NavigationNode upToGroupByValue() {
        NavigationNode result = this;
        while (result != null) {
            if (result.isGroupBy()) {
                return result;
            }
            result = result.getParent();
        }
        throw new IllegalStateException(String.format("Can't go up to group by value from node: %s", this));
    }

    public NavigationNode upToReferenceNode(Class<? extends DtoReference> referenceType) {
        NavigationNode result = this;
        while (result != null) {
            if (result.acceptReferenceNode(referenceType)) {
                return result;
            }
            result = result.getParent();
        }
        throw new IllegalStateException(String.format("Can't go up reference node of type: %s from node: %s", referenceType.getName(), this));
    }

    //--------------------------------------------------------------------------------------------
    // Go down methods
    //--------------------------------------------------------------------------------------------

    public NavigationNode downToReferenceNode(Class<? extends DtoReference> referenceType, String id) {
        if (acceptReferenceNode(referenceType)) {
            return this;
        }
        Enumeration<?> children = children();
        while (children.hasMoreElements()) {
            NavigationNode childrenNode = (NavigationNode) children.nextElement();
            if (childrenNode.acceptReferenceNode(id)) {
                return childrenNode;
            }
        }
        throw new IllegalStateException(String.format("Can't go down reference node of type: %s from node: %s", referenceType.getName(), this));
    }

    //--------------------------------------------------------------------------------------------
    // TreeNode override methods
    //--------------------------------------------------------------------------------------------

    @Override
    public final NavigationNode getRoot() {
        return (NavigationNode) super.getRoot();
    }

    @Override
    public NavigationNode getParent() {
        return (NavigationNode) super.getParent();
    }

    @Override
    public NavigationNode getChildAt(int index) {
        return (NavigationNode) super.getChildAt(index);
    }

    @Override
    public NavigationNode getFirstChild() {
        return (NavigationNode) super.getFirstChild();
    }

    @Override
    public NavigationNode getLastChild() {
        return (NavigationNode) super.getLastChild();
    }

    @Override
    public NavigationNode getChildAfter(TreeNode aChild) {
        return (NavigationNode) super.getChildAfter(aChild);
    }

    @Override
    public NavigationNode getChildBefore(TreeNode aChild) {
        return (NavigationNode) super.getChildBefore(aChild);
    }

    @Override
    public NavigationNode getNextSibling() {
        return (NavigationNode) super.getNextSibling();
    }

    @Override
    public NavigationNode getPreviousSibling() {
        return (NavigationNode) super.getPreviousSibling();
    }

    @Override
    public NavigationNode getFirstLeaf() {
        return (NavigationNode) super.getFirstLeaf();
    }

    @Override
    public NavigationNode getLastLeaf() {
        return (NavigationNode) super.getLastLeaf();
    }

    @Override
    public NavigationNode getNextLeaf() {
        return (NavigationNode) super.getNextLeaf();
    }

    @Override
    public NavigationNode getPreviousLeaf() {
        return (NavigationNode) super.getPreviousLeaf();
    }

    public final String toPath() {
        return getInitializer().toPath();
    }

    public final String[] toPaths() {
        TreeNode[] nodes = getPath();
        int length = nodes.length;
        // never save root path which is null
        String[] paths = new String[length - 1];
        for (int i = 1; i < length; i++) {
            NavigationNode currentNode = (NavigationNode) nodes[i];
            String currentPath = currentNode.toPath();
            paths[i - 1] = currentPath;
        }
        return paths;
    }

    //--------------------------------------------------------------------------------------------
    // Internal methods
    //--------------------------------------------------------------------------------------------

    protected boolean acceptReferenceContainerNode(Class<? extends DtoReference> referenceType) {
        if (isReferenceContainer()) {
            ReferenceContainerCapability<?> capability = (ReferenceContainerCapability<?>) getCapability();
            return capability.acceptChildReferenceType(referenceType);
        }
        return false;
    }

    protected boolean acceptReferenceNode(Class<? extends DtoReference> referenceType) {
        if (isReference()) {
            ReferenceCapability<?> capability = (ReferenceCapability<?>) getCapability();
            return capability.acceptReferenceType(referenceType);
        }
        return false;
    }

    protected boolean acceptReferenceNode(String id) {
        if (isReference()) {
            return isEqualsId(id);
        }
        return false;
    }

    protected Optional<NavigationTreeModel> getTreeModel() {
        NavigationNode root = getRoot();
        if (!isAdjusting() && root instanceof RootNavigationNode) {
            RootNavigationNode rootNavigationNode = (RootNavigationNode) root;
            NavigationTreeModel treeModel = rootNavigationNode.getInitializer().getTreeModel();
            return Optional.ofNullable(treeModel);
        }
        return Optional.empty();
    }


    public Font getNodeFont(Font defaultFont) {
        // by default always ask to parent for this
        // is overridden on necessary nodes which contains full logic of edit nodes
        return getParent().getNodeFont(defaultFont);
    }

}
