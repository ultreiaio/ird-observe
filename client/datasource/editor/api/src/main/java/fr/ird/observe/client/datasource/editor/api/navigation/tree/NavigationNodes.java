package fr.ird.observe.client.datasource.editor.api.navigation.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.ServiceLoaders;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 20/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class NavigationNodes {

    private static final Logger log = LogManager.getLogger(NavigationNodes.class);
    public static NavigationNodes INSTANCE;
    private Set<Class<? extends NavigationNode>> nodes;

    public static NavigationNodes get() {
        return INSTANCE == null ? INSTANCE = new NavigationNodes() : INSTANCE;
    }

    public Set<Class<? extends NavigationNode>> getNodes() {
        if (nodes == null) {
            this.nodes = new LinkedHashSet<>();
            List<Class<NavigationNode>> classes = ServiceLoaders.loadTypes(NavigationNode.class);
            for (Class<NavigationNode> nodeType : classes) {
                log.debug(String.format("Add node: %s", nodeType.getName()));
                nodes.add(nodeType);
            }
        }
        return nodes;
    }

}
