package fr.ird.observe.client.datasource.editor.api.navigation.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUINavigationNode;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.function.Supplier;

/**
 * Created on 20/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public final class NavigationScope {

    public static final String TYPE_UI_CONTENT = "ui.content";
    public static final String TYPE_UI_NODE = "ui.node";
    public static final String TYPE_UI_NODE_PARENT = "ui.node.parent";
    public static final String TYPE_UI_NODE_DATA = "ui.node.data";
    public static final String TYPE_UI_NODE_CHILD = "ui.node.child";

    public static final String TYPE_MODEL_PARENT = "model.parent";
    public static final String TYPE_MODEL_PARENT_REFERENCE = "model.parentReference";
    public static final String TYPE_MODEL_MAIN = "model.main";
    public static final String TYPE_MODEL_MAIN_REFERENCE = "model.mainReference";
    public static final String TYPE_MODEL_CHILD = "model.child";
    public static final String TYPE_MODEL_CHILD_REFERENCE = "model.childReference";

    public static final String TYPE_NAVIGATION_MODULE = "navigation.module";
    public static final String TYPE_NAVIGATION_SUB_MODULE = "navigation.subModule";
    public static final String TYPE_NAVIGATION_EDIT_NODE = "navigation.editNode";
    public static final String TYPE_NAVIGATION_SELECT_NODE = "navigation.selectNode";

    public static final String PROPERTY_UI_ICON_PATH = "ui.iconPath";
    public static final String PROPERTY_UI_SUB_NODE = "ui.subNode";
    public static final String PROPERTY_UI_NO_AUTO_LOAD = "ui.notAutoLoad";

    public static final String PROPERTY_UI_DECORATOR_CLASSIFIER = "ui.decoratorClassifier";

    public static final String PROPERTY_MODEL_CHILDREN_PROPERTY_NAME = "model.childrenPropertyName";
    public static final String PROPERTY_MODEL_SHOW_DATA_PROPERTY_NAME = "model.showDataPropertyName";
    public static final String PROPERTY_MODEL_EMPTY_NODE_PROPERTY_NAME = "model.emptyNodePropertyName";
    public static final String PROPERTY_MODEL_INITIAL_COUNT_PROPERTY_NAME = "model.initialCountPropertyName";
    public static final String PROPERTY_MODEL_STANDALONE = "model.standalone";
    public static final String PROPERTY_MODEL_PERMISSION = "model.permission";

    public static final String PROPERTY_NAVIGATION_SELECT_NODE = "navigation.selectNode";
    public static final String PROPERTY_NAVIGATION_EDIT_NODE = "navigation.editNode";

    private final Map<String, String> properties = new TreeMap<>();
    private final Map<String, Class<?>> types = new TreeMap<>();
    private final Map<String, String> i18nMapping = new TreeMap<>();
    private final Map<Class<? extends IdDto>, Class<? extends NavigationNode>> nodeChildTypes = new LinkedHashMap<>();

    private transient Permission permission;

    public Permission getPermission() {
        if (permission == null) {
            String name = properties.getOrDefault(PROPERTY_MODEL_PERMISSION, Permission.NONE.name());
            permission = Permission.valueOf(name);
        }
        return permission;
    }

    public boolean isSubNode() {
        return Objects.equals("true", properties.get(PROPERTY_UI_SUB_NODE));
    }

    @SuppressWarnings("unused")
    public boolean isAutoLoad() {
        return !isNoAutoLoad();
    }

    public boolean isNoAutoLoad() {
        return Objects.equals("true", properties.get(PROPERTY_UI_NO_AUTO_LOAD));
    }

    public String getIconPath() {
        return properties.get(PROPERTY_UI_ICON_PATH);
    }

    public String getDecoratorClassifier() {
        return properties.get(PROPERTY_UI_DECORATOR_CLASSIFIER);
    }

    public String getChildrenPropertyName() {
        return properties.get(PROPERTY_MODEL_CHILDREN_PROPERTY_NAME);
    }

    public String getShowDataPropertyName() {
        return properties.get(PROPERTY_MODEL_SHOW_DATA_PROPERTY_NAME);
    }

    public String getEmptyNodePropertyName() {
        return properties.get(PROPERTY_MODEL_EMPTY_NODE_PROPERTY_NAME);
    }

    public String getInitialCountPropertyName() {
        return properties.get(PROPERTY_MODEL_INITIAL_COUNT_PROPERTY_NAME);
    }

    public String getI18nKey(String property) {
        return i18nMapping.get(property);
    }

    public String getI18nTranslation(String property) {
        return I18n.t(getI18nKey(property));
    }

    public boolean isSelectNode() {
        return Boolean.parseBoolean(properties.getOrDefault(PROPERTY_NAVIGATION_SELECT_NODE, "false"));
    }

    public boolean isEditNode() {
        return Boolean.parseBoolean(properties.getOrDefault(PROPERTY_NAVIGATION_EDIT_NODE, "false"));
    }

    public boolean isStandalone() {
        return Boolean.parseBoolean(properties.getOrDefault(PROPERTY_MODEL_STANDALONE, "true"));
    }

    public <N extends NavigationNode> Class<N> getNodeType() {
        return getType(TYPE_UI_NODE);
    }

    public <O> Class<O> getNodeDataType() {
        return getType(TYPE_UI_NODE_DATA);
    }

    public <N extends NavigationNode> Class<N> getNodeChildType() {
        return getType(TYPE_UI_NODE_CHILD);
    }

    public <U extends ContentUI> Class<U> getContentUiType() {
        return getType(TYPE_UI_CONTENT);
    }

    public <B extends BusinessModule> Class<B> getModuleType() {
        return getType(TYPE_NAVIGATION_MODULE);
    }

    public <B extends BusinessSubModule> Class<B> getSubModuleType() {
        return getType(TYPE_NAVIGATION_SUB_MODULE);
    }

    public <D extends IdDto> Class<D> getParentType() {
        return getType(TYPE_MODEL_PARENT);
    }

    public <D extends DtoReference> Class<D> getParentReferenceType() {
        return getType(TYPE_MODEL_PARENT_REFERENCE);
    }

    public <D extends IdDto> Class<D> getMainType() {
        return getType(TYPE_MODEL_MAIN);
    }

    public <D extends DtoReference> Class<D> getMainReferenceType() {
        return getType(TYPE_MODEL_MAIN_REFERENCE);
    }

    public <D extends IdDto> Class<D> getChildType() {
        return getType(TYPE_MODEL_CHILD);
    }

    public <D extends IdNode<?>> Class<D> getSelectNodeType() {
        return getType(TYPE_NAVIGATION_SELECT_NODE);
    }

    public <D extends IdNode<?>> Class<D> getEditNodeType() {
        return getType(TYPE_NAVIGATION_EDIT_NODE);
    }

    public Map<Class<? extends IdDto>, Class<? extends NavigationNode>> getNodeChildTypes() {
        return nodeChildTypes;
    }

    @Override
    public String toString() {
        return com.google.common.base.MoreObjects.toStringHelper(this)
                .omitNullValues()
                .add("properties", properties)
                .add("types", types)
                .add("i18nMapping", i18nMapping)
                .add("nodeChildTypes", nodeChildTypes)
                .toString();
    }

    public SingletonSupplier<IdNode<?>> computeSelectNode(Supplier<Project> model) {
        Class<IdNode<?>> nodeType = getSelectNodeType();
        return nodeType == null ? SingletonSupplier.of(() -> null, isSelectNode()) : SingletonSupplier.of(() -> model.get().forNodeType(nodeType).orElse(null), isSelectNode());
    }

    public SingletonSupplier<IdNode<?>> computeEditNode(Supplier<Project> model) {
        Class<IdNode<?>> nodeType = getEditNodeType();
        return nodeType == null ? SingletonSupplier.of(() -> null, isEditNode()) : SingletonSupplier.of(() -> model.get().forNodeType(nodeType).orElse(null), isEditNode());
//        return SingletonSupplier.of(() -> model.get().forNodeType(nodeType).orElseThrow(IllegalStateException::new), isEditNode());
    }

    public BusinessModule computeModule(ObserveBusinessProject project) {
        return getModuleType() == null ? null : project.getBusinessModule(getModuleType());
    }

    public BusinessSubModule computeSubModule(ObserveBusinessProject project, BusinessModule module) {
        return getModuleType() == null ? null : project.getBusinessSubModule(module, getSubModuleType());
    }

    public List<Class<? extends ReferentialDto>> computeTypes(Locale locale, BusinessSubModule subModule) {
        return new LinkedList<>(ObserveUtil.sortTypes(subModule.getReferentialTypes(), I18nDecoratorHelper::getType, locale));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public Map<Class<? extends ReferentialDto>, Class<? extends ContentReferentialUINavigationNode>> computeReferentialNodeTypes() {
        return (Map) getNodeChildTypes();
    }

    @SuppressWarnings("unchecked")
    <D> Class<D> getType(String type) {
        return (Class<D>) types.get(type);
    }

    SingletonSupplier<Boolean> computeEditable(NavigationContext<?> context) {
        return SingletonSupplier.of(() -> context.getMainDataSource().canExecute(getPermission()));
    }

}
