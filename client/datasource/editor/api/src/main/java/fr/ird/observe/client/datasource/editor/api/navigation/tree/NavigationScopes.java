package fr.ird.observe.client.datasource.editor.api.navigation.tree;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.ultreia.java4all.util.json.adapters.ClassAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 20/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class NavigationScopes {

    public static final String CLASSIFIER = ".scope";
    private static final Logger log = LogManager.getLogger(NavigationScopes.class);
    public static NavigationScopes INSTANCE;
    private Map<Class<? extends NavigationNode>, NavigationScope> scopes;

    public static NavigationScopes get() {
        return INSTANCE == null ? INSTANCE = new NavigationScopes() : INSTANCE;
    }

    public static <N extends NavigationNode> NavigationScope getNavigationScope(Class<N> nodeType) throws IllegalStateException {
        NavigationScope scope = get().getScopes().get(nodeType);
        if (scope != null) {
            return scope;
        }
        throw new IllegalStateException(String.format("Can't find navigation scope for node: %s", nodeType.getName()));
    }

    public static String getScopeFilename(String nodeType) {
        return nodeType + NavigationScopes.CLASSIFIER;
    }

    public Map<Class<? extends NavigationNode>, NavigationScope> getScopes() {
        if (scopes == null) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Class.class, new ClassAdapter())
                    .enableComplexMapKeySerialization()
                    .setPrettyPrinting().create();
            this.scopes = new LinkedHashMap<>();
            for (Class<? extends NavigationNode> nodeType : NavigationNodes.get().getNodes()) {
                log.debug(String.format("Looking scope for: %s", nodeType.getName()));
                NavigationScope navigationScope = load(nodeType, gson);
                scopes.put(nodeType, navigationScope);
                log.debug(String.format("Found %s", navigationScope));
            }
        }
        return scopes;
    }

    private NavigationScope load(Class<? extends NavigationNode> nodeType, Gson gson) {
        String resourcePath = getScopeFilename(nodeType.getSimpleName());
        URL resource = nodeType.getResource(resourcePath);
        if (resource == null) {
            throw new IllegalStateException(String.format("Can't find navigation scope for node at %s", resourcePath));
        }
        log.debug(String.format("Will load navigation scope for type: %s at location: %s", nodeType.getName(), resourcePath));
        try (Reader reader = new InputStreamReader(resource.openStream(), StandardCharsets.UTF_8)) {
            return gson.fromJson(reader, NavigationScope.class);
        } catch (Exception e) {
            throw new IllegalStateException(String.format("Can't load navigation scope for node: %s", nodeType.getName()), e);
        }
    }

}
