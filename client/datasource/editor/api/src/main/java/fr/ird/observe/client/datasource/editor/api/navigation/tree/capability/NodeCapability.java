package fr.ird.observe.client.datasource.editor.api.navigation.tree.capability;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;

import javax.swing.tree.TreeNode;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 05/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public interface NodeCapability<N extends NavigationNode> {

    N getNode();

    default Set<Class<? extends NavigationNode>> existingNodeTypes() {
        Enumeration<? extends TreeNode> children = getNode().children();
        Set<Class<? extends NavigationNode>> existingNodes = new LinkedHashSet<>();
        while (children.hasMoreElements()) {
            NavigationNode treeNode = (NavigationNode) children.nextElement();
            existingNodes.add(treeNode.getClass());
        }
        return existingNodes;
    }
}
