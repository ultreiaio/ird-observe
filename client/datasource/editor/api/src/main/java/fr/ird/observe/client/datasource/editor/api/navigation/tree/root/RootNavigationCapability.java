package fr.ird.observe.client.datasource.editor.api.navigation.tree.root;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.GroupByContainerCapability;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.navigation.tree.NavigationResult;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 08/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class RootNavigationCapability implements GroupByContainerCapability<RootNavigationNode> {
    private final RootNavigationNode node;

    private final SingletonSupplier<List<Class<? extends NavigationNode>>> acceptedNodeTypes;

    public RootNavigationCapability(RootNavigationNode node) {
        this.node = Objects.requireNonNull(node);
        this.acceptedNodeTypes = SingletonSupplier.of(() -> getNode().getInitializer().getRootNodeProviders().stream().flatMap(m -> m.getAcceptedNodeTypes().stream()).collect(Collectors.toList()));
    }

    @Override
    public RootNavigationNode getNode() {
        return node;
    }

    @Override
    public List<Class<? extends NavigationNode>> getAcceptedNodeTypes() {
        return acceptedNodeTypes.get();
    }

    @Override
    public void buildChildren() {
        RootNavigationNode rootNode = getNode();
        rootNode.getInitializer().buildChildren(rootNode);
    }

    @Override
    public ContentRootListUINavigationNode createChildNode(DataGroupByDto<?> reference) {
        return newChildNode(reference);
    }

    @Override
    public ContentRootListUINavigationNode insertChildNode(DataGroupByDto<?> reference) {
        if (reference.decorator().isEmpty()) {
            getNode().getContext().getDecoratorService().installDecorator(reference);
        }
        ContentRootListUINavigationNode result = createChildNode(reference);
        int nodePosition = getNodePosition(reference);
        getNode().insert(result, nodePosition);
        return result;
    }

    @Override
    public void updateChildNode(String id) {
        // Update also navigation result, this method is asking it
        NavigationResult navigationResult = getNode().getInitializer().getTreeModel().updateNavigationResult();

        ContentRootListUINavigationNode groupByNode = (ContentRootListUINavigationNode) getNode().findChildById(id);

        DataGroupByDto<?> reference = navigationResult.getGroupBy(id);
        if (reference == null) {
            // this case can happen if groupBy is not compliant with the navigation configuration
            if (groupByNode != null) {
                // this case can happen if groupBy was compliant with the navigation configuration, but not any longer
                // just remove the obsolete groupBy node
                groupByNode.removeFromParent();
            }
            return;
        }
        if (groupByNode == null) {
            // this case can happen if group by was disabled, then enabled and navigation configuration does not accept disabled groupBy
            // we need just then to insert the new groupBy node
            insertChildNode(reference);
            return;
        }
        int oldNodePosition = getNode().getIndex(groupByNode);
        int newNodePosition = getNodePosition(reference);
        // Propagate reference as parentReference to in children and as reference to this node
        groupByNode.getCapability().updateReference(reference);
        // need to update groupByNode
        groupByNode.nodeChanged();
        if (oldNodePosition != newNodePosition) {
            // position has changed
            getNode().moveNode(groupByNode, newNodePosition);
        }
    }

    @Override
    public ContentRootListUINavigationNode createMissingNode(String id) {
        // Update also navigation result, this method is asking it
        NavigationResult navigationResult = getNode().getInitializer().getTreeModel().updateNavigationResult();
        DataGroupByDto<?> reference = navigationResult.getGroupBy(id);
        if (reference == null) {
            //FIXME How the reference could not be found in navigation result?
            reference = getNode().getContext().computeGroupByDto(id);
        }
        return insertChildNode(reference);
    }

    @Override
    public int getNodePosition(DataGroupByDto<?> reference) {
        if (reference.decorator().isEmpty()) {
            getNode().getContext().getDecoratorService().installDecorator(reference);
        }
        Decorator decorator = reference.decorator().orElseThrow();
        return getNode().getGroupByChildrenPosition(reference, decorator);
    }

    public ContentRootListUINavigationNode newChildNode(DataGroupByDto<?> reference) {
        RootNavigationNode node = getNode();
        RootNavigationTreeNodeProvider provider = getNode().getInitializer().getRootNodeProvider(reference.definition().getBusinessModule().getName());
        return provider.newChildNode(node, reference);
    }

    public <N extends ReferentialHomeUINavigationNode> N createReferentialHomeUINavigationNode(Class<N> nodeType, Map<Class<? extends ReferentialDto>, Long> referentialCountMap) {
        return ReferentialHomeUINavigationNode.create(nodeType, referentialCountMap);
    }

    public <N extends ReferentialHomeUINavigationNode> void addReferentialHomeUINavigationNode(Class<N> nodeType, Map<Class<? extends ReferentialDto>, Long> referentialCountMap) {
        N node = createReferentialHomeUINavigationNode(nodeType, referentialCountMap);
        getNode().add(node);
    }

}
