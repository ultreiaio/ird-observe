package fr.ird.observe.client.datasource.editor.api.navigation.tree.root;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;

import java.util.Set;

/**
 * Created on 23/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class RootNavigationContext extends NavigationContext<RootNavigationContext> {

    public String computeGroupByValue(String dataId) {
        ToolkitTreeFlatModelRootRequest request = getInitializer().getRequest();
        return getDataSourcesManager().getMainDataSource().getRootOpenableService().getGroupByValue(request.getGroupByName(), request.getGroupByFlavor(), dataId);
    }

    public DataGroupByDto<?> computeGroupByDto(String dataId) {
        ToolkitTreeFlatModelRootRequest request = getInitializer().getRequest();
        return getDataSourcesManager().getMainDataSource().getRootOpenableService().getGroupByDtoValue(getInitializer().getNavigationResult().getData().getType(), new DataGroupByParameter(request.getGroupByName(), request.getGroupByFlavor(), dataId));
    }

    public boolean dataNotExists(String dataId) {
        return getDataSourcesManager().getMainDataSource().getDataSourceService().retainExistingIds(Set.of(dataId)).isEmpty();
    }

    @Override
    protected RootNavigationInitializer getInitializer() {
        return (RootNavigationInitializer) super.getInitializer();
    }
}
