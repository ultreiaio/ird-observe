package fr.ird.observe.client.datasource.editor.api.navigation.tree.root;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTreeModel;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.navigation.tree.NavigationResult;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created on 25/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class RootNavigationInitializer extends NavigationInitializer<RootNavigationContext> {
    private final SingletonSupplier<List<? extends RootNavigationTreeNodeProvider>> rootNodeProviders;
    private final SingletonSupplier<Boolean> local;
    private NavigationResult navigationResult;
    private NavigationTreeModel treeModel;

    public RootNavigationInitializer(NavigationScope scope, NavigationResult navigationResult, ObserveSwingDataSource mainDataSource) {
        super(scope);
        this.navigationResult = Objects.requireNonNull(navigationResult);
        this.rootNodeProviders = SingletonSupplier.of(RootNavigationTreeNodeProvider::getProviders);
        this.local = SingletonSupplier.of(mainDataSource::isLocal);
    }

    @Override
    protected Object init(NavigationContext<RootNavigationContext> context) {
        context.initReference(getGroupBy());
        return null;
    }

    @Override
    protected void open(NavigationContext<RootNavigationContext> context) {
    }

    @Override
    protected void reload(NavigationContext<RootNavigationContext> context) {
    }

    @Override
    public String toPath() {
        return null;
    }

    public final List<? extends RootNavigationTreeNodeProvider> getRootNodeProviders() {
        return rootNodeProviders.get();
    }

    public final boolean isLocal() {
        return local.get();
    }

    public NavigationTreeModel getTreeModel() {
        return treeModel;
    }

    public void setTreeModel(NavigationTreeModel treeModel) {
        this.treeModel = treeModel;
    }

    public NavigationResult getNavigationResult() {
        return navigationResult;
    }

    public void setNavigationResult(NavigationResult navigationResult) {
        this.navigationResult = Objects.requireNonNull(navigationResult);
    }

    public ToolkitTreeFlatModelRootRequest getRequest() {
        return navigationResult.getRequest();
    }

    public DataGroupByDtoSet<?, ?> getGroupBy() {
        return navigationResult.getData();
    }

    public void buildChildren(RootNavigationNode rootNode) {
        ToolkitTreeFlatModelRootRequest request = getRequest();
        NavigationResult navigationResult = getNavigationResult();
        String moduleName = request.getModuleName();
        if (request.isLoadData()) {
            RootNavigationTreeNodeProvider dataProvider = getRootNodeProvider(moduleName);
            dataProvider.initRootDataNode(navigationResult, rootNode);
        }
        if (request.isLoadReferential()) {
            Stream<? extends RootNavigationTreeNodeProvider> referentialProviders;
            if (request.isLoadData()) {
                // only load common + this module
                referentialProviders = getRootNodeProviders(Set.of("common", moduleName));
            } else {
                // load all referential
                // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2338
                referentialProviders = getRootNodeProviders().stream();
            }
            referentialProviders.forEach(p -> p.initRootReferentialNode(rootNode, navigationResult));
        }
    }

    public RootNavigationTreeNodeProvider getRootNodeProvider(String moduleName) {
        return getRootNodeProviders().stream().filter(p -> p.getModule().acceptModuleName(moduleName)).findFirst().orElseThrow();
    }

    public Stream<? extends RootNavigationTreeNodeProvider> getRootNodeProviders(Set<String> moduleNames) {
        return getRootNodeProviders().stream().filter(p -> moduleNames.contains(p.getModule().getName()));
    }
}
