package fr.ird.observe.client.datasource.editor.api.navigation.tree.root;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.navigation.tree.NavigationResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Font;
import java.util.Objects;

@AutoService(NavigationNode.class)
public class RootNavigationNode extends NavigationNode {

    private static final Logger log = LogManager.getLogger(RootNavigationNode.class);

    public static int nodeCount;

    public static int incrementsAndGetNodeCount() {
        return ++nodeCount;
    }

    public static RootNavigationNode create(NavigationResult navigationResult) {
        RootNavigationNode node = new RootNavigationNode();
        init(node, Objects.requireNonNull(navigationResult));
        return node;
    }

    public static RootNavigationNode init(RootNavigationNode node, NavigationResult navigationResult) {
        RootNavigationInitializer initializer = new RootNavigationInitializer(node.getScope(), navigationResult, node.getContext().getMainDataSource());
        node.init(initializer);
        return node;
    }

    public RootNavigationNode() {
        nodeCount = 0;
    }

    @Override
    public RootNavigationInitializer getInitializer() {
        return (RootNavigationInitializer) super.getInitializer();
    }

    @Override
    protected RootNavigationContext createContext() {
        return new RootNavigationContext();
    }

    @Override
    protected RootNavigationHandler createHandler() {
        return new RootNavigationHandler(this);
    }

    @Override
    protected RootNavigationCapability createCapability() {
        return new RootNavigationCapability(this);
    }

    @Override
    public RootNavigationHandler getHandler() {
        return (RootNavigationHandler) super.getHandler();
    }

    @Override
    public RootNavigationContext getContext() {
        return (RootNavigationContext) super.getContext();
    }

    @Override
    public RootNavigationCapability getCapability() {
        return (RootNavigationCapability) super.getCapability();
    }

    public void clear() {
        RootNavigationInitializer initializer = getInitializer();
        if (initializer == null) {
            // node was never loaded
            return;
        }
        // detach from model
        initializer.setTreeModel(null);
        // remove all nodes
        removeAllChildren();
        log.info(String.format("Clear rootNode: %s (created nodes: %d).", this, nodeCount));
        // reset node count
        nodeCount = 0;
    }

    @Override
    public Font getNodeFont(Font defaultFont) {
        // by default use plain font
        return defaultFont.deriveFont(Font.PLAIN);
    }
}
