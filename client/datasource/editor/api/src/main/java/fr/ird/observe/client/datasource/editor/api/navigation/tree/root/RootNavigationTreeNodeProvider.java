package fr.ird.observe.client.datasource.editor.api.navigation.tree.root;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScopes;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.navigation.tree.NavigationResult;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public abstract class RootNavigationTreeNodeProvider {
    public static List<RootNavigationTreeNodeProvider> PROVIDERS;
    private final BusinessModule module;
    private final int priority;
    private final List<Class<? extends NavigationNode>> acceptedNodeTypes;

    public static synchronized List<RootNavigationTreeNodeProvider> getProviders() {
        if (PROVIDERS == null) {
            PROVIDERS = new LinkedList<>();
            for (RootNavigationTreeNodeProvider o : ServiceLoader.load(RootNavigationTreeNodeProvider.class)) {
                PROVIDERS.add(o);
            }
            PROVIDERS.sort(Comparator.comparing(RootNavigationTreeNodeProvider::priority));
        }
        return PROVIDERS;
    }

    public static List<Class<? extends NavigationNode>> referentialHomes(BusinessModule module) {
        Map<Class<?>, Integer> priority = new LinkedHashMap<>();
        for (BusinessSubModule subModule : module.getSubModules()) {
            priority.put(subModule.getClass(), module.getSubModulePriority(subModule));
        }
        return NavigationScopes.get().getScopes().values().stream()
                .filter(s -> Objects.equals(module.getClass(), s.getModuleType())
                        && ReferentialHomeUINavigationNode.class.isAssignableFrom(s.getNodeType())
                )
                .sorted(Comparator.comparingInt(s -> priority.get(s.getSubModuleType())))
                .map(NavigationScope::getNodeType)
                .collect(Collectors.toList());
    }

    protected RootNavigationTreeNodeProvider(BusinessModule module) {
        this(module, null);
    }

    protected RootNavigationTreeNodeProvider(BusinessModule module, Class<? extends NavigationNode> acceptedEntryPointNode) {
        this.module = Objects.requireNonNull(module);
        this.acceptedNodeTypes = new LinkedList<>();
        if (acceptedEntryPointNode != null) {
            acceptedNodeTypes.add(acceptedEntryPointNode);
        }
        List<Class<? extends NavigationNode>> referentialHomes = referentialHomes(module);
        acceptedNodeTypes.addAll(referentialHomes);
        this.priority = Objects.requireNonNull(module.project()).getModulePriority(module);
    }

    public void initRootDataNode(NavigationResult navigationResult, RootNavigationNode rootNode) {
        Optional<Class<? extends ContentRootListUINavigationNode>> dataNodeType = getDataNodeType();
        if (dataNodeType.isPresent()) {
            initRootDataNode0(navigationResult, rootNode);
        }
    }

    public void initRootReferentialNode(RootNavigationNode rootNode, NavigationResult navigationResult) {
        Map<Class<? extends ReferentialDto>, Long> referentialCountMap = navigationResult.getReferentialCountMap();
        getReferentialNodeTypes().forEach(t -> rootNode.getCapability().addReferentialHomeUINavigationNode(t, referentialCountMap));
    }

    protected void initRootDataNode0(NavigationResult navigationResult, RootNavigationNode rootNode) {
        for (DataGroupByDto<?> groupByDto : navigationResult.getData().toArrayList()) {
            ContentRootListUINavigationNode childNode = newChildNode(rootNode, groupByDto);
            rootNode.add(childNode);
        }
    }

    public List<Class<? extends ReferentialHomeUINavigationNode>> getReferentialNodeTypes() {
        @SuppressWarnings({"unchecked", "rawtypes"}) List<Class<? extends ReferentialHomeUINavigationNode>> result = (List) getAcceptedNodeTypes().stream().filter(ReferentialHomeUINavigationNode.class::isAssignableFrom).collect(Collectors.toList());
        return result;
    }

    public Optional<Class<? extends ContentRootListUINavigationNode>> getDataNodeType() {
        @SuppressWarnings({"unchecked", "rawtypes"}) Optional<Class<? extends ContentRootListUINavigationNode>> result = (Optional) getAcceptedNodeTypes().stream().filter(c -> !ReferentialHomeUINavigationNode.class.isAssignableFrom(c)).findFirst();
        return result;
    }

    public List<Class<? extends NavigationNode>> getAcceptedNodeTypes() {
        return acceptedNodeTypes;
    }

    public int priority() {
        return priority;
    }

    public BusinessModule getModule() {
        return module;
    }

    public ContentRootListUINavigationNode newChildNode(RootNavigationNode node, DataGroupByDto<?> groupByDto) {
        throw new IllegalStateException("Not implemented");
    }
}
