package fr.ird.observe.client.datasource.editor.api.navigation.tree.select;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.navigation.id.IdModel;
import fr.ird.observe.navigation.id.IdNode;

import java.util.List;
import java.util.Optional;

/**
 * To select a node from the navigation model node.
 * <p>
 * Created on 18/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class SelectNodesByModel implements SelectNodeStrategy {

    private final IdModel model;

    public SelectNodesByModel(IdModel model) {
        this.model = model;
    }

    @Override
    public Optional<NavigationNode> apply(RootNavigationNode rootNode) {
        NavigationNode result = null;
        if (model.isEnabled()) {
            // get all ids from model
            List<IdNode<?>> nodesWithIds = model.getNodesWithIds();
            IdNode<?> modelNodeToFind = nodesWithIds.get(nodesWithIds.size() - 1);
            result = SelectNodesByModelNode.findFromRootNode(rootNode, modelNodeToFind);
        }
        return Optional.ofNullable(result);
    }
}
