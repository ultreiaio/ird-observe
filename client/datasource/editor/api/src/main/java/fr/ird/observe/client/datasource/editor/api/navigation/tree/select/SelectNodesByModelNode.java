package fr.ird.observe.client.datasource.editor.api.navigation.tree.select;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUINavigationCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ReferenceContainerCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.navigation.id.IdNode;

import java.util.Enumeration;
import java.util.List;
import java.util.Optional;

/**
 * To select a node from the navigation model node.
 * <p>
 * Created on 18/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class SelectNodesByModelNode implements SelectNodeStrategy {

    private final IdNode<?> model;

    public static NavigationNode findFromRootNode(RootNavigationNode rootNode, IdNode<?> editNode) {
        // get root edit node
        IdNode<?> root = editNode.getRoot();
        String dataId = root.getId();
        // check if data id exists on this source
        if (rootNode.getContext().dataNotExists(dataId)) {
            return null;
        }
        String groupByValue = rootNode.getContext().computeGroupByValue(dataId);
        NavigationNode groupByNode = SelectNodesByPath.findChildByPath(rootNode, groupByValue);
        if (groupByNode == null) {
            return null;
        }
        // start normal search from groupBy node
        return findNode(groupByNode, editNode);
    }

    public static NavigationNode findNode(NavigationNode source, IdNode<?> editNode) {
        NavigationNode result = source;
        List<IdNode<?>> nodesFromRoot = editNode.getNodesFromRoot();
        for (IdNode<?> node : nodesFromRoot) {
            result = findChildNode(result, node);
            if (result == null) {
                return null;
            }
        }
        return result;
    }

    public static NavigationNode findChildNode(NavigationNode source, IdNode<?> editNode) {
        source.populateChildrenIfNotLoaded();
        if (source.isLeaf()) {
            return null;
        }
        String id = editNode.getId();
        Class<? extends DtoReference> type = editNode.getReferenceType();
        Enumeration<?> children = source.children();
        while (children.hasMoreElements()) {
            NavigationNode child = (NavigationNode) children.nextElement();
            if (child.isReference()) {
                if (child.isEqualsId(id)) {
                    // found by matching id
                    return child;
                }
                continue;
            }
            if (child.isReferenceContainer()) {
                ReferenceContainerCapability<?> capability = (ReferenceContainerCapability<?>) child.getCapability();
                if (capability.acceptChildReferenceType(type)) {
                    return findChildNode(child, editNode);
                }
            }
            if (child.isLayoutContainer()) {
                ContentLayoutUINavigationCapability<?> capability = (ContentLayoutUINavigationCapability<?>) child.getCapability();
                if (capability.acceptChildReferenceType(type)) {
                    return findChildNode(child, editNode);
                }
            }
        }
        return null;
    }

    public SelectNodesByModelNode(IdNode<?> model) {
        this.model = model;
    }

    @Override
    public Optional<NavigationNode> apply(RootNavigationNode rootNode) {
        NavigationNode result = null;
        if (model.isEnabled()) {
            result = findFromRootNode(rootNode, model);
        }
        return Optional.ofNullable(result);
    }
}
