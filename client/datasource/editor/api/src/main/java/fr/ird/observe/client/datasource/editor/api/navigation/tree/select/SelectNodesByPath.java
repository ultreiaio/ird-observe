package fr.ird.observe.client.datasource.editor.api.navigation.tree.select;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;

import java.util.Enumeration;
import java.util.Objects;
import java.util.Optional;

/**
 * To select a node from a path.
 * <p>
 * Created on 18/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class SelectNodesByPath implements SelectNodeStrategy {

    private final String[] path;
    private final boolean checkGroupBy;

    public static Optional<NavigationNode> findChildByPath(RootNavigationNode rootNode, boolean checkGroupBy, String... path) {
        if (path == null || path.length == 0) {
            // no path
            return Optional.empty();
        }
        NavigationNode result = Objects.requireNonNull(rootNode);
        int length = path.length;
        String firstPath = path[0];
        if (firstPath != null && firstPath.contains("BusinessSubModule")) {
            return getNavigationNode(result, 0, path);
        }
        if (length == 1) {
            // can't apply checkGroupBy (have no data id)
            // just try with given groupBy value
            return getNavigationNode(result, 0, path);
        }
        if (!rootNode.getInitializer().getRequest().isLoadData()) {
            // asking for a data node, but no data on this tree
            return Optional.empty();
        }
        if (checkGroupBy) {
            // need at least two entries in path (the groupBy value + data id)
            // if no data id can no find out new groupBy value...
            String dataId = path[1];
            // check if data id exists on this source
            if (rootNode.getContext().dataNotExists(dataId)) {
                return Optional.empty();
            }
            String groupByValue = rootNode.getContext().computeGroupByValue(dataId);
            NavigationNode groupByNode = findChildByPath(result, groupByValue);
            if (groupByNode != null) {
                return getNavigationNode(groupByNode, 1, path);
            }
        }
        return getNavigationNode(result, 0, path);
    }

    public static NavigationNode findChildByPath(NavigationNode node, String path) {
        node.populateChildrenIfNotLoaded();
        if (node.getChildCount() == 0) {
            return null;
        }
        Enumeration<?> children = node.children();
        while (children.hasMoreElements()) {
            NavigationNode child = (NavigationNode) children.nextElement();
            if (child.isEqualsPath(path)) {
                return child;
            }
        }
        return null;
    }

    private static Optional<NavigationNode> getNavigationNode(NavigationNode selectedNode, int i, String... path) {
        for (int length = path.length; i < length; i++) {
            String s = path[i];
            selectedNode = findChildByPath(selectedNode, s);
            if (selectedNode == null) {
                break;
            }
        }
        return Optional.ofNullable(selectedNode);
    }

    public SelectNodesByPath(String[] path, boolean checkGroupBy) {
        this.path = path;
        this.checkGroupBy = checkGroupBy;
    }

    @Override
    public Optional<NavigationNode> apply(RootNavigationNode rootNode) {
        return findChildByPath(Objects.requireNonNull(rootNode), checkGroupBy, path);

    }
}
