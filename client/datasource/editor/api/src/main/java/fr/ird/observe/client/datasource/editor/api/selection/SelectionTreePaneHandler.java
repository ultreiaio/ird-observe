package fr.ird.observe.client.datasource.editor.api.selection;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSourceTemplate;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUIHandler;
import fr.ird.observe.client.datasource.editor.api.config.actions.ApplySelectionConfiguration;
import fr.ird.observe.client.datasource.editor.api.selection.actions.ShowConfigurePanel;
import fr.ird.observe.client.datasource.editor.api.selection.actions.ShowInformationPanel;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.navigation.tree.selection.RootSelectionTreeNode;
import fr.ird.observe.navigation.tree.selection.SelectionTree;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.function.Consumer;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectionTreePaneHandler implements UIHandler<SelectionTreePane>, PropertyChangeListener {
    private SelectionTreePane ui;

    public static void updateStatistics(SelectionTreePane ui) {
        SelectionTreeModel model = ui.getTree().getModel();
        RootSelectionTreeNode navigationResult = model.getRoot();
        TreeConfigUIHandler.updateStatistics(model.getRequest(),
                                             model.getGroupByHelper(),
                                             navigationResult::getChildCount,
                                             model::allDataCount,
                                             model::getDataCount,
                                             ui::setStatisticsText,
                                             ui::setStatisticsTip,
                                             ui.getStatisticsLabel()::setIcon);
    }

    public static void init(SelectionTreePane ui, Consumer<TreeConfigUI> consumer, Consumer<TreeConfigUI> apply) {
        JMenuItem showConfigure = ui.getShowConfigure();
        showConfigure.putClientProperty(ShowConfigurePanel.UI_CONSUMER, consumer);
        showConfigure.putClientProperty(ApplySelectionConfiguration.APPLY_CONSUMER, apply);
        ShowConfigurePanel.init(ui, showConfigure, new ShowConfigurePanel());
    }

    public static void initDataSource(SelectionTreePane ui, ObserveSwingDataSource dataSource) {
        String labelWithUrl = dataSource.getLabelWithUrl();
        JLabel dataSourceInformation = ui.getLabel();
        dataSourceInformation.setIcon(dataSource.getIcon());
        dataSourceInformation.setText(labelWithUrl);
        ShowInformationPanel.init(ui, ui.getShowInformation(), new ShowInformationPanel(() -> ObserveSwingDataSourceTemplate.generate(dataSource)));
    }

    @Override
    public void beforeInit(SelectionTreePane ui) {
        this.ui = ui;
        ui.setComponentPopupMenu(new JPopupMenu());
    }

    @Override
    public void afterInit(SelectionTreePane ui) {
        SelectionTree tree = ui.getTree();
        UIInitHelper.init(ui.getTreePane());
        UIInitHelper.init(tree);
        tree.getModel().addPropertyChangeListener(SelectionTreeModel.SELECTED_COUNT, this);
        onSelectedCountChanged(tree.getModel().getSelectedCount());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        int newValue = (int) evt.getNewValue();
        onSelectedCountChanged(newValue);
    }

    private void onSelectedCountChanged(int newValue) {
        String text;
        boolean referential = ui.getTree().getModel().getConfig().isLoadReferential();
        if (newValue == 0) {
            text = referential ? t("observe.selection.no.referential.selected") : t("observe.selection.no.data.selected");
        } else {
            text = referential ? t("observe.selection.selected.referential", newValue) : t("observe.selection.selected.data", newValue);
        }
        ui.setSelectionText(text);
    }
}
