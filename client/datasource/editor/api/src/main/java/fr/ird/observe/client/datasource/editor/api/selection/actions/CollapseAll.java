package fr.ird.observe.client.datasource.editor.api.selection.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePane;

import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import static io.ultreia.java4all.i18n.I18n.t;

public class CollapseAll extends SelectionTreePaneActionSupport {

    public CollapseAll() {
        super("", t("observe.ui.tree.action.collapseAll.tip"), "collapse", ObserveKeyStrokesEditorApi.KEY_STROKE_COLLAPSE_TREE_TABLE_NODE, KeyStroke.getKeyStroke(ObserveKeyStrokesEditorApi.KEY_STROKE_COLLAPSE_TREE_TABLE_NODE.getKeyCode(), ObserveKeyStrokesEditorApi.KEY_STROKE_COLLAPSE_TREE_TABLE_NODE.getModifiers() | KeyEvent.SHIFT_DOWN_MASK));
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SelectionTreePane ui) {
        ui.getTree().collapseAll();
    }
}
