package fr.ird.observe.client.datasource.editor.api.selection.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePane;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.navigation.tree.selection.SelectionTree;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class SelectUnselect extends SelectionTreePaneActionSupport {

    private static final Logger log = LogManager.getLogger(SelectUnselect.class);
//    private KeyStroke[] keyStrokes;

    public static void installUI(SelectionTreePane parent) {
        SelectUnselect action = new SelectUnselect();
//        action.setExcludeKeyStrokes(keyStrokes);
        SelectUnselect.init(parent, null, action);
    }

    public SelectUnselect() {
        super(null, null, null, ObserveKeyStrokesSupport.KEY_STROKE_SPACE, KeyStroke.getKeyStroke(ObserveKeyStrokesEditorApi.KEY_STROKE_SPACE.getKeyCode(), ObserveKeyStrokesEditorApi.KEY_STROKE_SPACE.getModifiers() | KeyEvent.SHIFT_DOWN_MASK));
    }

    @Override
    protected InputMap getInputMap(SelectionTreePane selectionTreePane, int inputMapCondition) {
        return selectionTreePane.getTree().getInputMap(inputMapCondition);
    }

    @Override
    protected ActionMap getActionMap(SelectionTreePane selectionTreePane) {
        return selectionTreePane.getTree().getActionMap();
    }

    protected void defaultInit(SelectionTreePane pane) {
        pane.getTree().getInputMap().put(ObserveKeyStrokesSupport.KEY_STROKE_SPACE, "none");
        //FIXME Remove any keystroke used in ui actions...
//        if (keyStrokes != null) {
//            Arrays.stream(keyStrokes).forEach(keyStroke -> pane.getTree().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyStroke, "none"));
//            Arrays.stream(keyStrokes).forEach(keyStroke -> pane.getTree().getInputMap().put(keyStroke, "none"));
//        }

        defaultInit(getInputMap(pane, getInputMapCondition()), getActionMap(pane));
    }

    @Override
    public void init() {
        defaultInit(ui);
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        super.defaultInit(inputMap, actionMap);
        inputMap.put(ObserveKeyStrokesSupport.KEY_STROKE_ENTER, getActionCommandKey());
        getUi().getTree().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    actionPerformed(new ActionEvent(getUi().getTree(), 0, "yo"));
                }
            }
        });
    }

    @Override
    protected boolean canExecuteAction(ActionEvent e) {
        boolean result = canExecutionActionFromLayer(getUi(), e);
        if (result) {
            log.debug(String.format("Accept action : %s", getName()));
        } else {
            log.debug(String.format("Reject action : %s", getName()));
        }
        return result;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SelectionTreePane ui) {
        selectUnSelect(ui.getTree());
    }

    protected void selectUnSelect(SelectionTree tree) {
        TreePath[] selectionPath = tree.getSelectionPaths();
        if (selectionPath == null) {
            return;
        }
        for (TreePath treePath : selectionPath) {
            Object selectedRow = treePath.getLastPathComponent();
            if (selectedRow == null) {
                return;
            }
            SelectionTreeNode node = (SelectionTreeNode) selectedRow;
            boolean selected = node.isNotSelected();
            tree.getModel().setValueAt(node, selected);
        }
    }

//    public void setExcludeKeyStrokes(KeyStroke... keyStrokes) {
//        this.keyStrokes = keyStrokes;
//    }
}
