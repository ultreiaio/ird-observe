package fr.ird.observe.client.datasource.editor.api.selection.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePane;
import fr.ird.observe.navigation.tree.selection.SelectionTree;

import java.awt.event.ActionEvent;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class SelectUnselectWithOpposite extends SelectUnselect {

    private final SelectionTreePane opposite;

    public SelectUnselectWithOpposite(SelectionTreePane opposite) {
        this.opposite = opposite;
    }

    @Override
    public void init() {
        super.init();
        defaultInit(opposite);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SelectionTreePane ui) {
        SelectionTree tree = ui.getTree();
        if (tree.isFocusOwner()) {
            selectUnSelect(tree);
        } else {
            selectUnSelect(opposite.getTree());
        }
    }

}
