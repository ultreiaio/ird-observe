package fr.ird.observe.client.datasource.editor.api.selection.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePane;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;

import javax.swing.KeyStroke;

abstract class SelectionTreePaneActionSupport extends JComponentActionSupport<SelectionTreePane> {
    private final KeyStroke oppositeAcceleratorKey;

    SelectionTreePaneActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey, KeyStroke oppositeAcceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
        this.oppositeAcceleratorKey = oppositeAcceleratorKey;
    }

    @Override
    public void init() {
        if (ui.isOpposite()) {
            setKeyStroke(oppositeAcceleratorKey);
        }
        super.init();
    }
}
