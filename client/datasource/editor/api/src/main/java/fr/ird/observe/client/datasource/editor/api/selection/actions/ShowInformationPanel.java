package fr.ird.observe.client.datasource.editor.api.selection.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.navigation.actions.MenuActionSupport;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePane;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.function.Supplier;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public class ShowInformationPanel extends SelectionTreePaneActionSupport {

    private final Supplier<String> informationTextSupplier;

    public ShowInformationPanel(Supplier<String> informationTextSupplier) {
        super(t("observe.ui.action.info.storage"), t("observe.ui.action.info.storage.tip"), "information", ObserveKeyStrokesEditorApi.KEY_STROKE_DATA_SYNCHRO_INFO_LEFT, ObserveKeyStrokesEditorApi.KEY_STROKE_DATA_SYNCHRO_INFO_RIGHT);
        this.informationTextSupplier = informationTextSupplier;
        setCheckMenuItemIsArmed(false);
    }

    @Override
    public void init() {
        super.init();
        MenuActionSupport.toggle(ui.getToggleMenu(), ui.getComponentPopupMenu());
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SelectionTreePane ui) {
        String text = informationTextSupplier.get();
        JPanel content = new JPanel(new BorderLayout());
        content.add(new JLabel(text), BorderLayout.CENTER);
        String title = t("observe.ui.title.storage.info");
        content.setBorder(new TitledBorder(title + "  "));
        JPopupMenu popup = ui.getComponentPopupMenu();
        popup.removeAll();
        popup.add(content);
        content.setBorder(new TitledBorder(title + "  "));
        popup.removeAll();
        popup.add(content);
        popup.pack();
        popup.show(ui.getTree(), 0, 0);
    }
}
