package fr.ird.observe.client.datasource.editor.api.wizard;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Created on 28/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
@GenerateJavaBeanDefinition
public class ChooseDbModelExt extends ChooseDbModel {
    private static final Logger log = LogManager.getLogger(ChooseDbModelExt.class);

    private final StorageUIModel model;

    public ChooseDbModelExt(StorageUIModel model) {
        this.model = model;
    }

    @Override
    public void setDataImportMode(DataSourceCreateMode dataImportMode) {
        DataSourceCreateMode oldValue = getDataImportMode();
        if (Objects.equals(oldValue, dataImportMode)) {
            return;
        }
        super.setDataImportMode(dataImportMode);
        // reset selected data to import
        model.setSelectDataModel(null);
        model.updateUniverseAndValidate();
    }

    @Override
    public void setReferentielImportMode(DataSourceCreateMode referentielImportMode) {
        DataSourceCreateMode oldValue = getReferentielImportMode();
        if (Objects.equals(oldValue, referentielImportMode)) {
            return;
        }
        super.setReferentielImportMode(referentielImportMode);
        model.updateUniverseAndValidate();
    }

    @Override
    public void setInitMode(DataSourceInitMode initMode) {
        super.setInitMode(initMode);
        model.updateUniverseAndValidate();
    }

    @Override
    public void setConnectMode(DataSourceConnectMode connectMode) {
        super.setConnectMode(connectMode);
        model.updateUniverseAndValidate();
    }

    @Override
    public void setCreateMode(DataSourceCreateMode createMode) {
        super.setCreateMode(createMode);
        model.updateUniverseAndValidate();
    }

    @Override
    protected void adaptInitModel(ClientConfig config) {
        if (model.getAdminAction() != null) {
            // une action d'admin obstuna toujours sur base distante
            getInitModel().setConnectMode(DataSourceConnectMode.REMOTE);
        }
        super.adaptInitModel(config);
    }

    @Override
    protected boolean computeCanMigrate() {
        boolean canMigrate = super.computeCanMigrate();
        if (initModel.getConnectMode() != null) {
            switch (initModel.getConnectMode()) {
                case REMOTE:
                case SERVER:
                    // can migrate when update admin action
                    canMigrate = ObstunaAdminAction.UPDATE.equals(model.getAdminAction());
                    break;
            }
        }
        return canMigrate;
    }

    @Override
    public void updateEditConfig() {
        super.updateEditConfig();

        model.getRemoteConfig().setCanMigrate(editRemoteConfig && isCanMigrate());
        model.getServerConfig().setCanMigrate(editServerConfig && isCanMigrate());
        if (model.isInit()) {
            if (model.isValid()) {
                try {
                    String txt = model.getChooseDb().getInitModel().getDescription();
                    model.setSummaryText(txt);
                } catch (Exception e) {
                    log.error("Could nod get summaryText", e);
                }
            }
        }
    }
}
