package fr.ird.observe.client.datasource.editor.api.wizard;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceEvent;
import fr.ird.observe.client.datasource.api.event.ObserveSwingDataSourceListenerAdapter;
import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.datasource.request.CreateDatabaseRequest;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.service.DataSourceService;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.nio.file.Path;

/**
 * Created on 01/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
public class DataSourceHelper {
    private static final Logger log = LogManager.getLogger(DataSourceHelper.class);

    public static void importModel(StorageUIModel source, StorageUIModel dst) {
        source.getChooseDb().copyTo(dst.getChooseDb());

        dst.setBackupFile(source.getBackupFile());
        dst.setDoBackup(source.isDoBackup());
        dst.setPreviousDataSourceConfiguration(source.getPreviousDataSourceConfiguration());

        source.getLocalConfig().copyTo(dst.getLocalConfig());
        source.getRemoteConfig().copyTo(dst.getRemoteConfig());
        source.getServerConfig().copyTo(dst.getServerConfig());

        dst.setSelectDataModel(source.getSelectDataModel());
        dst.setSecurityModel(source.getSecurityModel());
        dst.setAdminAction(source.getAdminAction());
        dst.setSelectAll(source.isSelectAll());

        dst.setDataSourceInformation(source.getDataSourceInformation());
        dst.getChooseDb().updateEditConfig();
    }

    public static void exportModel(StorageUIModel source, StorageUIModel dst) {
        source.getChooseDb().copyTo(dst.getChooseDb());

        dst.setBackupFile(source.getBackupFile());
        dst.setDoBackup(source.isDoBackup());
        dst.setPreviousDataSourceConfiguration(source.getPreviousDataSourceConfiguration());
        source.getLocalConfig().copyTo(dst.getLocalConfig());
        source.getRemoteConfig().copyTo(dst.getRemoteConfig());
        source.getServerConfig().copyTo(dst.getServerConfig());

        dst.setSelectDataModel(source.getSelectDataModel());
        dst.setSecurityModel(source.getSecurityModel());
        dst.setAdminAction(source.getAdminAction());
        dst.setSelectAll(source.isSelectAll());

        dst.getChooseDb().updateEditConfig();
        dst.setDataSourceInformation(source.getDataSourceInformation());
    }

    public static SqlScript getCreationConfigurationDto(StorageUIModel model) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException {
        ChooseDbModel chooseDb = model.getChooseDb();
        DataSourceInitModel initModel = chooseDb.getInitModel();
        SqlScript importDatabase = null;
        ObserveDataSourceConfiguration configSrc = null;
        switch (initModel.getCreateMode()) {

            case EMPTY:
                break;
            case IMPORT_INTERNAL_DUMP: {
                File dumpFile = model.getClientConfig().getInitialDbDump();
                importDatabase = SqlScript.of(dumpFile.toURI());
            }
            break;
            case IMPORT_EXTERNAL_DUMP: {
                File dumpFile = model.getDumpFile();
                importDatabase = SqlScript.of(dumpFile.toURI());
            }
            break;
            case IMPORT_LOCAL_STORAGE:
                configSrc = model.getLocalConfig().getConfiguration();
                break;
            case IMPORT_REMOTE_STORAGE:
                configSrc = model.getRemoteConfig().getConfiguration();
                break;
            case IMPORT_SERVER_STORAGE:
                configSrc = model.getServerConfig().getConfiguration();
                break;
        }
        if (configSrc != null) {
            try (ObserveSwingDataSource source = model.getDataSourcesManager().newDataSource(configSrc)) {
                source.open();
                DataSourceService dumpService = source.getDataSourceService();
                CreateDatabaseRequest request = CreateDatabaseRequest.builder(false, source.getVersion()).addGeneratedSchema().addVersionTable().addStandaloneTables().build();
                importDatabase = dumpService.produceCreateSqlScript(request);
            }
        }
        return importDatabase;
    }

    public static ObserveSwingDataSource initDataSourceFromModel(StorageUIModel model) {
        ClientConfig config = model.getClientConfig();
        ObserveDataSourcesManager dataSourcesManager = model.getDataSourcesManager();

        ObserveSwingDataSource dataSource = null;
        ObserveDataSourceConfiguration configuration;

        final DataSourceInitModel initModel = model.getChooseDb().getInitModel();
        switch (initModel.getInitMode()) {
            case CREATE:
                configuration = toH2StorageConfig(model, I18n.n("observe.ui.datasource.storage.label.local"));

                dataSource = dataSourcesManager.newDataSource(configuration);
                dataSource.addObserveSwingDataSourceListener(
                        new ObserveSwingDataSourceListenerAdapter() {

                            @Override
                            public void onOpened(ObserveSwingDataSourceEvent event) {
                                ObserveSwingDataSource dataSource = event.getSource();

                                // local data source now exists
                                config.setLocalStorageExist(true);

                                switch (initModel.getCreateMode()) {
                                    case IMPORT_EXTERNAL_DUMP:
                                        // update import directory
                                        File importDirectory = model.getDumpFile().getParentFile();
                                        log.info(String.format("update import directory to: %s", importDirectory));
                                        config.setImportDirectory(importDirectory);
                                        break;
                                    case IMPORT_REMOTE_STORAGE:
                                    case IMPORT_SERVER_STORAGE:
                                        // Let's generate again initial dump
                                        Path f = config.getInitialDbDump().toPath();
                                        log.info(String.format("create initial dump with %s to: %s", dataSource, f));
                                        dataSource.backupLocalDatabase(f);
                                        config.setInitialDumpExist(true);
                                        break;
                                }
                                config.saveForUser();
                            }
                        });

                break;
            case CONNECT:
                switch (initModel.getConnectMode()) {
                    case LOCAL:
                        configuration = toH2StorageConfig(model, I18n.n("observe.ui.datasource.storage.label.local"));
                        dataSource = dataSourcesManager.newDataSource(configuration);
                        break;
                    case REMOTE:
                        configuration = toPGStorageConfig(model, I18n.n("observe.ui.datasource.storage.label.remote"));
                        dataSource = dataSourcesManager.newDataSource(configuration);
                        break;
                    case SERVER:
                        configuration = toRestStorageConfig(model, I18n.n("observe.ui.datasource.storage.label.server"));
                        dataSource = dataSourcesManager.newDataSource(configuration);
                        break;
                }
                break;
        }
        if (model.getDataSourceInformation() != null) {
            dataSource.setOwner(model.getDataSourceInformation().isOwner());
            dataSource.setSuperUser(model.getDataSourceInformation().isSuperUser());
        }
        return dataSource;
    }

    static ObserveDataSourceConfigurationTopiaH2 toH2StorageConfig(StorageUIModel model, String label) {
        ObserveDataSourceConfigurationTopiaH2 h2Config = model.getLocalConfig().getConfiguration();
        return ObserveDataSourceConfigurationTopiaH2.create(
                label,
                h2Config.getDirectory(),
                h2Config.getDbName(),
                h2Config.getLogin(),
                h2Config.getPassword(),
                //tchemit 2021-04-26 always use the application model version, otherwise topia won't do correct migration if required
                model.getModelVersion()
        );
    }

    public static ObserveDataSourceConfigurationTopiaPG toPGStorageConfig(StorageUIModel model, String label) {
        ObserveDataSourceConfigurationTopiaPG pgConfig = model.getRemoteConfig().getConfiguration();
        return ObserveDataSourceConfigurationTopiaPG.create(
                label,
                pgConfig.getUrl(),
                pgConfig.getLogin(),
                pgConfig.getPassword(),
                pgConfig.isUseSsl(),
                //tchemit 2021-04-26 always use the application model version, otherwise topia won't do correct migration if required
                model.getModelVersion()
        );
    }

    public static ObserveDataSourceConfigurationRest toRestStorageConfig(StorageUIModel model, String label) {
        ObserveDataSourceConfigurationRest restConfig = model.getServerConfig().getConfiguration();
        return ObserveDataSourceConfigurationRest.create(
                label,
                restConfig.getUrl(),
                restConfig.getLogin(),
                restConfig.getPassword(),
                restConfig.getDatabaseName(),
                //tchemit 2021-04-26 always use the application model version, otherwise topia won't do correct migration if required
                model.getModelVersion()
        );
    }
}
