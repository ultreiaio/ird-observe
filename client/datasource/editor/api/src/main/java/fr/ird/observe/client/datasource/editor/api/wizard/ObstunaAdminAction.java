/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.wizard;

import fr.ird.observe.client.datasource.editor.api.wizard.launchers.CreateLauncher;
import fr.ird.observe.client.datasource.editor.api.wizard.launchers.RemoteUILauncher;
import fr.ird.observe.client.datasource.editor.api.wizard.launchers.SecurityLauncher;
import fr.ird.observe.client.datasource.editor.api.wizard.launchers.UpdateLauncher;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;
import org.nuiton.jaxx.runtime.JAXXContext;

import java.awt.Window;

/**
 * Les différents types d'actions d'administration possible sur une base
 * distante.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION, pattern = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION_PATTERN)
})
public enum ObstunaAdminAction {

    /**
     * pour créer une nouvelle base obstuna.
     * <p>
     * Il faut au préalable avoir exécuté le script {@code create-obstuna_v2.sh}.
     */
    CREATE,
    /**
     * pour mettre à jour une base distante.
     * <p>
     * Il faut au préalable avoir une base en version {@code 1.0}.
     */
    UPDATE,
    /**
     * pour mettre à jour la sécurité d'une base obstuna.
     * <p>
     * Il faut au préalable avoir une base en version {@code 1.4}.
     */
    SECURITY;

    public static ObstunaAdminAction valueOfIgnoreCase(String value) {
        for (ObstunaAdminAction step : ObstunaAdminAction.values()) {
            if (step.name().equalsIgnoreCase(value)) {
                return step;
            }
        }
        return null;
    }

    public String getLabel() {
        return ObstunaAdminActionI18n.getLabel(this);
    }

    public final RemoteUILauncher newLauncher(JAXXContext context, Window frame, DataSourceConnectMode connectMode) {
        switch (this) {
            case CREATE:
                return new CreateLauncher(context, frame, connectMode);
            case UPDATE:
                return new UpdateLauncher(context, frame, connectMode);
            case SECURITY:
                return new SecurityLauncher(context, frame, connectMode);
            default:
                throw new IllegalStateException("Can't manage admin action: " + this);
        }
    }

    @Override
    public String toString() {
        return ObstunaAdminActionI18n.getLabel(this);
    }
}
