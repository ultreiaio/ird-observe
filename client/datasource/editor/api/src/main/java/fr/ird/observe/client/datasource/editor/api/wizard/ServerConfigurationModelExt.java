package fr.ird.observe.client.datasource.editor.api.wizard;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.form.ServerConfigurationModel;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.function.Supplier;

/**
 * Created on 25/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
@GenerateJavaBeanDefinition
class ServerConfigurationModelExt extends ServerConfigurationModel {
    /**
     * Optional admin action.
     */
    private final Supplier<ObstunaAdminAction> adminAction;

    ServerConfigurationModelExt(Supplier<ObstunaAdminAction> adminAction) {
        this.adminAction = adminAction;
    }

    @Override
    protected void testDataSourceInformation(ObserveDataSourceInformation dataSourceInformation) {
        if (dataSourceInformation == null) {
            return;
        }
        super.testDataSourceInformation(dataSourceInformation);
        if (isConnexionFailed()) {
            return;
        }
        if (adminAction.get() != null) {
            testIsOwner(dataSourceInformation);
            testsIsSuperUser(dataSourceInformation);
        }
    }
}
