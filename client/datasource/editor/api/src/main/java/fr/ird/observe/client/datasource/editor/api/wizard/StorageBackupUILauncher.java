/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.wizard;

import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.data.CopyDataTask;
import fr.ird.observe.client.datasource.api.data.DataManager;
import fr.ird.observe.client.datasource.api.data.TaskSide;
import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXContext;

import java.awt.Window;
import java.io.File;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Un wizard pour effectuer des backup de storages.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public class StorageBackupUILauncher extends StorageUILauncher {

    /**
     * Logger
     */
    private static final Logger log = LogManager.getLogger(StorageBackupUILauncher.class);

    public StorageBackupUILauncher(JAXXContext context, Window frame, String title) {
        super(context, frame, title);
    }

    @Override
    protected void init(StorageUI ui) {
        super.init(ui);
        ui.getBACKUP().getSelectDoBackup().setSelected(true);
        ui.getBACKUP().getSelectDoBackup().setVisible(false);

        StorageUIModel model = ui.getModel();
        ObserveSwingDataSource source = model.getDataSourcesManager().getMainDataSource();

        ChooseDbModel chooseDb = model.getChooseDb();
        DataSourceInitModel initModel = model.getInitModel();
        initModel.setAuthorizedInitModes(EnumSet.of(DataSourceInitMode.CONNECT));
        DataSourceConnectMode dataSourceType = source.getConfiguration().getConnectMode();
        initModel.setAuthorizedConnectModes(EnumSet.of(dataSourceType));
        initModel.setAuthorizedCreateModes(EnumSet.noneOf(DataSourceCreateMode.class));
        chooseDb.setInitMode(DataSourceInitMode.CONNECT);
        chooseDb.setConnectMode(dataSourceType);

        chooseDb.updateEditConfig();
        model.setDoBackup(true);

        model.setSteps(StorageStep.BACKUP, StorageStep.SELECT_DATA, StorageStep.CONFIRM);
        ui.setTitle(title);
        try {

            // Selection des données uniquement pour une base locale
            // Voir https://forge.codelutin.com/issues/7207
            boolean selectAllData = model.isLocal();
            model.setSelectAll(selectAllData);

        } catch (Exception e) {
            log.error(e, e);
        }
        model.getProgressModel().installUI(ui.getCONFIRM().getProgressBar(), false);
    }

    @Override
    public void doAction(StorageUI ui) {
        super.doAction(ui);
        StorageUIModel storageModel = ui.getModel();
        File backupFile = storageModel.getBackupFile();
        Objects.requireNonNull(backupFile, "file where to backup can not be null");
        createBackup(storageModel, backupFile);

        storageModel.getClientConfig().updateBackupDirectory(backupFile);
    }

    private void createBackup(StorageUIModel storageModel, File backupFile) {
        ObserveDataSourcesManager dataSourcesManager = storageModel.getDataSourcesManager();
        ObserveSwingDataSource incomingSource = dataSourcesManager.getMainDataSource();
        SelectionTreeModel dataModel = storageModel.getSelectDataModel();
        ProgressionModel progressModel = storageModel.getProgressModel();

        if (dataModel.isDataFull() && incomingSource.isLocal()) {

            // on local storage we can perform efficient backup
            progressModel.setMaximum(1);
            try {
                log.info(String.format("Exporting to %s", backupFile));
                incomingSource.backup(backupFile);
                progressModel.increments();
            } catch (Exception e) {
                throw new ObserveSwingTechnicalException(e);
            }
            return;
        }

        // need to create a temporary local data storage and copy data inside it then do the backup

        // create copy tasks from incoming source to target source
        List<CopyDataTask> selectedData = CopyDataTask.of(TaskSide.FROM_LEFT, dataModel).collect(Collectors.toList());
        progressModel.setMaximum(3 + 2 * selectedData.size());

        try (ObserveSwingDataSource targetSource = dataSourcesManager.newTemporaryH2DataSource("Backup-" + backupFile.getName())) {

            // create temporary source
            SqlScript referentialDump = incomingSource.extract(false, true, null);
            targetSource.createFromDump(referentialDump);
            progressModel.increments();

            // insert selected data
            DataManager dataManager = new DataManager(progressModel, incomingSource, targetSource);
            dataManager.consume(selectedData);
            progressModel.increments();

            // finally backup temporary storage
            log.info(String.format("Exporting to %s", backupFile));
            targetSource.backup(backupFile);
            progressModel.increments();
        } catch (DatabaseNotFoundException | DatabaseConnexionNotAuthorizedException | BabModelVersionException |
                 IncompatibleDataSourceCreateConfigurationException |
                 DataSourceCreateWithNoReferentialImportException e) {
            throw new ObserveSwingTechnicalException(e);
        }
    }
}
