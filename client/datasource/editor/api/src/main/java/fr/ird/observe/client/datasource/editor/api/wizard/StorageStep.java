/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.wizard;

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;
import org.nuiton.jaxx.runtime.swing.wizard.WizardStep;

/**
 * Pour caractériser les étapes (correspond aux onglets de l'ui).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION, pattern = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION_PATTERN)
})
public enum StorageStep implements WizardStep {

    /**
     * pour choisir le mode de connexion (local ou remote)
     */
    CHOOSE_DB_MODE,
    /**
     * pour configurer la connexion à la source de données (uniquement si
     * besoin)
     */
    CONFIG,
    /**
     * pour configurer le referentiel a utiliser lors de la creation d'une base
     * distante
     */
    CONFIG_REFERENTIAL,
    /**
     * pour configurer la sources des données a utiliser lors de la creation d'une base
     * distante
     */
    CONFIG_DATA,
    /**
     * pour effectuer une sauvegarde de la base locale. (uniquement disponible
     * si on veut générer une nouvelle base locale)
     */
    BACKUP,
    /**
     * pour sélectionner les données à sauvegarder (unqiuement sur une ui de
     * backup)
     */
    SELECT_DATA,
    /**
     * Pour sélectionner les rôles à appliquer pour la création ou mise à jour
     * d'une base distante
     */
    ROLES,
    /**
     * pour confirmer et réaliser les actions demandées
     */
    CONFIRM;

    @Override
    public String getLabel() {
        return StorageStepI18n.getLabel(this);
    }

    @Override
    public String getDescription() {
        return StorageStepI18n.getDescription(this);
    }
}
