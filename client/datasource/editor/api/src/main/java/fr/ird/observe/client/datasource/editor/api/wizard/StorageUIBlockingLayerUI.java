package fr.ird.observe.client.datasource.editor.api.wizard;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.ObserveBlockingLayerUI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * FIXME:BodyContent I don't see the point of this blocking ui which redefine action binding with key events...
 * Created on 09/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class StorageUIBlockingLayerUI extends ObserveBlockingLayerUI {

    /** Logger */
    private static final Logger log = LogManager.getLogger(StorageUIBlockingLayerUI.class);

//    private static final Set<Integer> GLOBAL_KEY_CODES = ImmutableSet.of(
//            KeyEvent.VK_F1,
//            KeyEvent.VK_F2,
//            KeyEvent.VK_F3,
//            KeyEvent.VK_F4,
//            KeyEvent.VK_F5,
//            KeyEvent.VK_F6,
//            KeyEvent.VK_F7,
//            KeyEvent.VK_F8,
//            KeyEvent.VK_F9,
//            KeyEvent.VK_F10,
//            KeyEvent.VK_F11,
//            KeyEvent.VK_F12
//    );

    private final StorageUI ui;

    StorageUIBlockingLayerUI(StorageUI ui) {
        this.ui = ui;
    }

    //FIXME:BodyContent I really don't see the point of doing this here...
    //FIXME:BodyContent There is no link with main ui nor navigation focus mechanism...
//    @Override
//    protected void processMouseEvent(MouseEvent e, JXLayer<? extends JComponent> l) {
//
//        switch (e.getID()) {
//            case MouseEvent.MOUSE_ENTERED:
//                log.debug("Enter in formula zone: " + e);
//                if (ClientUIContextApplicationComponent.value().getMainUI() != null) {
//                    ClientUIContextApplicationComponent.value().getMainUIModel().setFocusOnNavigation(false);
//                }
//        }
//        super.processMouseEvent(e, l);
//    }

    //FIXME:BodyContent I don't see the point of this blocking ui which redefine action binding with key events...
//    @Override
//    protected void processKeyEvent(KeyEvent e, JXLayer<? extends JComponent> l) {
//
//        if (block) {
//            return;
//        }
//
//        JComponent tabs = (JComponent) ((JAXXObject) ui).getObjectById("tabs");
//        InputMap inputMap = tabs.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
//        ActionMap actionMap = tabs.getActionMap();
//
//        boolean consumed = e.isConsumed();
//
//        if (!consumed && e.isControlDown() && e.getKeyChar() != '\uFFFF') {
//
//            KeyStroke keyStroke = KeyStroke.getKeyStroke("ctrl pressed " + (char) e.getKeyCode());
//
//            if (keyStroke == null) {
//                super.processKeyEvent(e, l);
//                return;
//            }
//            consumed = doAction(keyStroke, inputMap, actionMap);
//        }
//
//        if (!consumed && e.getID() == KeyEvent.KEY_RELEASED && !e.isAltDown() && !e.isAltGraphDown()
//                && !e.isMetaDown() && GLOBAL_KEY_CODES.contains(e.getKeyCode())) {
//
//            if (!e.isShiftDown()) {
//
//                KeyStroke keyStroke = KeyStroke.getKeyStroke(e.getKeyCode(), e.isControlDown() ? KeyEvent.CTRL_DOWN_MASK : 0);
//                if (keyStroke == null) {
//                    super.processKeyEvent(e, l);
//                    return;
//                }
//
//                consumed = doAction(keyStroke, inputMap, actionMap);
//
//            } else if (e.isControlDown()) {
//
//                KeyStroke keyStroke = KeyStroke.getKeyStroke(e.getKeyCode(), KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK);
//                if (keyStroke == null) {
//                    super.processKeyEvent(e, l);
//                    return;
//                }
//
//                consumed = doAction(keyStroke, inputMap, actionMap);
//            }
//
//        }
//
//        if (consumed) {
//            e.consume();
//        } else {
//            super.processKeyEvent(e, l);
//        }
//
//    }
//
//    protected boolean doAction(KeyStroke keyStroke, InputMap inputMap, ActionMap actionMap) {
//
//        String actionName = (String) inputMap.get(keyStroke);
//        if (actionName != null) {
//
//            Action action = actionMap.get(actionName);
//
//            JComponent editor = (JComponent) action.getValue(JAXXObjectActionSupport.EDITOR);
//            if (editor == null || (editor.isVisible() && editor.isEnabled())) {
//
//                if (log.isInfoEnabled()) {
//                    log.info("Found action: " + action.getValue(Action.NAME) + " for keyStroke: " + keyStroke);
//                }
//                SwingUtilities.invokeLater(() -> action.actionPerformed(new ActionEvent(ui, 0, (String) action.getValue(Action.NAME))));
//            } else {
//                if (log.isInfoEnabled()) {
//                    log.info("Found disabled action: " + action.getValue(Action.NAME) + " for keyStroke: " + keyStroke);
//                }
//            }
//            // We found the action, it is now consumed whatever we have done with it.
//            return true;
//        }
//        return false;
//    }
}
