/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.wizard;

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.client.datasource.editor.api.wizard.tabs.ChooseDbModeUI;
import fr.ird.observe.client.datasource.editor.api.wizard.tabs.RolesUIHandler;
import fr.ird.observe.client.datasource.editor.api.wizard.tabs.SelectDataUIHandler;
import fr.ird.observe.client.datasource.editor.api.wizard.tabs.StorageTabUI;
import fr.ird.observe.client.datasource.editor.api.wizard.tabs.actions.ChooseDbModeUISelectRadioButtonAction;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Enumeration;

/**
 * Le handler des actions sur base.
 * <p>
 * On retrouve ici les méthodes pour importer, exporter, faire des backup,
 * synchroniser les bases.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class StorageUIHandler implements UIHandler<StorageUI>, WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(StorageUIHandler.class);

    private StorageUI ui;

    @Override
    public void beforeInit(StorageUI ui) {
        this.ui = ui;
        // verification du context parent
        UIHelper.checkJAXXContextEntry(ui, UIHelper.newContextEntryDef(StorageUIModel.class));
        UIHelper.checkJAXXContextEntry(ui, UIHelper.newContextEntryDef("apply", Runnable.class));
        UIHelper.checkJAXXContextEntry(ui, UIHelper.newContextEntryDef("cancel", Runnable.class));
    }

    @Override
    public void afterInit(StorageUI storageUI) {
        StorageUIModel model = ui.getModel();
        // listen when step change
        model.addPropertyChangeListener(StorageUIModel.STEP_PROPERTY_NAME, evt -> {
            StorageUIModel model1 = (StorageUIModel) evt.getSource();
            StorageStep oldStep = (StorageStep) evt.getOldValue();
            StorageStep newStep = (StorageStep) evt.getNewValue();
            log.debug(String.format("step has changed <old:%s, new:%s>", oldStep, newStep));
            int oldStepIndex = oldStep == null ? -1 : model1.getStepIndex(oldStep);
            int newStepIndex = model1.getStepIndex(newStep);
            JTabbedPane tabs = ui.getTabs();
            if (oldStepIndex + 1 == newStepIndex) {
                // go forward, create new tab
                StorageTabUI c = (StorageTabUI) ui.getObjectById(newStep.name());
                c.setVisible(true);
                String title = newStep.getLabel();
                String tip = newStep.getDescription();
                log.debug(String.format("Create tab %s ui = %s", title, c));
                tabs.addTab(title, null, c, tip);
                //FIXME Use Ctrl + Fxx accelerator
//                tabs.setMnemonicAt(newStepIndex, title.charAt(0));
                // select new tab
                int index = tabs.indexOfComponent(c);
                if (index > -1) {
                    tabs.setSelectedIndex(index);
                }
                ui.onStepChanged(oldStep, newStep);
            } else if (oldStepIndex > newStepIndex) {
                // go backward, remove obsolete tabs
                int index = newStepIndex + 1;
                while (tabs.getTabCount() > index) {
                    log.debug("remove tab : " + index);
                    Component componentAt = tabs.getComponentAt(index);
                    componentAt.setVisible(false);
                    tabs.remove(index);
                }
                ui.onStepChanged(oldStep, newStep);
            } else {
                throw new IllegalStateException("can not go from " + oldStep + " to " + newStep);
            }
        });
        // get used data source configuration
        ObserveDataSourceConfiguration dataSourceConfiguration = ui.getContextValue(ObserveDataSourceConfiguration.class);
        if (dataSourceConfiguration != null) {
            log.warn(String.format("Using incoming datasource configuration: %s", dataSourceConfiguration));
        }
        // load model
        model.init(ui, dataSourceConfiguration);
        ChooseDbModeUI tabUi = ui.getCHOOSE_DB_MODE();
        addGroupMnemonic(tabUi, tabUi.getConnectMode(), 1);
        addGroupMnemonic(tabUi, tabUi.getCreateMode(), DataSourceConnectMode.values().length + 1);
    }

    void start() {
        ui.getModel().start();
        ui.setSize(1024, 800);
        UIHelper.center(ui.getContextValue(Window.class, "parent"), ui);
        ui.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                SwingUtilities.invokeLater(() -> {
                    JComponent focusOwner = getFocusComponent(ui.getModel().getStep());
                    if (focusOwner != null) {
                        focusOwner.requestFocusInWindow();
                    }
                });
            }
        });
        ui.setVisible(true);
    }

    void onStepChanged(StorageStep oldStep, StorageStep newStep) {
        if (newStep == null) {
            return;
        }
        log.debug(String.format("new step : %s", newStep));
        StorageUIModel model = ui.getModel();
        boolean mustRecompute = oldStep == null || oldStep.ordinal() < newStep.ordinal() || StorageStep.CONFIRM == newStep;
        if (mustRecompute) {
            switch (newStep) {
                case BACKUP:
                case CHOOSE_DB_MODE:
                    break;
                case CONFIG:
                    // ask again connexion
                    model.resetConnexionStatus();
                    break;
                case CONFIG_DATA:
                case CONFIG_REFERENTIAL:
                    // update steps universe
                    ui.getModel().updateUniverse();

                    break;
                case SELECT_DATA:
                    // init select data model
                    SelectDataUIHandler.loadSelectData(this, ui.getSELECT_DATA());
                    break;
                case ROLES:
                    // update security roles
                    RolesUIHandler.updateSecurity(this, model, ui.getROLES().getRolesModel());
                    break;
                case CONFIRM:
                    // generate summary report
                    String text = model.computeReport(oldStep);
                    ui.CONFIRM.getResume().setText(text);
                    break;
            }
        }
        JComponent focusComponent = getFocusComponent(newStep);
        if (focusComponent != null) {
            SwingUtilities.invokeLater(focusComponent::requestFocusInWindow);
        }
    }

    void destroy() {
        ui.getModel().destroy();
        log.debug(String.format("destroy ui %s", ui.getName()));
        UIHelper.TabbedPaneIterator<Component> itr = UIHelper.newTabbedPaneIterator(ui.getTabs());
        while (itr.hasNext()) {
            StorageTabUI tab = (StorageTabUI) itr.next();
            log.debug(String.format("destroy ui %s", tab.getName()));
            tab.destroy();
        }
        UIHelper.destroy(ui);
    }

    private JComponent getFocusComponent(StorageStep newStep) {
        DataSourceInitModel initMode = ui.getModel().getChooseDb().getInitModel();
        JComponent focusOwner;
        switch (newStep) {
            case CHOOSE_DB_MODE:
                if (initMode.isOnCreateMode()) {
                    focusOwner = ui.getCHOOSE_DB_MODE().getOnCreateMode();
                } else {
                    switch (initMode.getConnectMode()) {
                        case LOCAL:
                            focusOwner = ui.getCHOOSE_DB_MODE().getOnConnectModeLocal();
                            break;
                        case REMOTE:
                            focusOwner = ui.getCHOOSE_DB_MODE().getOnConnectModeRemote();
                            break;
                        case SERVER:
                            focusOwner = ui.getCHOOSE_DB_MODE().getOnConnectModeServer();
                            break;
                        default:
                            focusOwner = null;
                    }
                }
                break;
            case CONFIG:
                focusOwner = ui.getCONFIG().getHandler().getFocusOwner(initMode);
                break;
            case CONFIG_REFERENTIAL:
                focusOwner = ui.getCONFIG_REFERENTIAL().getTestRemoteConnexion();
                break;
            case CONFIG_DATA:
                focusOwner = ui.getCONFIG_DATA().getTestRemoteConnexion();
                break;
            case BACKUP:
                focusOwner = ui.getBACKUP().getChooseBackupDirectory();
                break;
            case SELECT_DATA:
                focusOwner = ui.getSELECT_DATA().getSelectedTreePane().getTree();
                break;
            case ROLES:
                focusOwner = ui.getROLES().getRoles();
                break;
            case CONFIRM:
                focusOwner = ui.getApply();
                break;
            default:
                focusOwner = null;
        }
        return focusOwner;
    }

    private void addGroupMnemonic(ChooseDbModeUI ui, JAXXButtonGroup buttonGroup, int initialIndex) {
        Enumeration<AbstractButton> elements = buttonGroup.getElements();
        int index = initialIndex;
        while (elements.hasMoreElements()) {
            AbstractButton abstractButton = elements.nextElement();
            ChooseDbModeUISelectRadioButtonAction action = new ChooseDbModeUISelectRadioButtonAction(this.ui, abstractButton, index++, "pressed F");
            ChooseDbModeUISelectRadioButtonAction.init(ui, abstractButton, action);
        }
    }
}
