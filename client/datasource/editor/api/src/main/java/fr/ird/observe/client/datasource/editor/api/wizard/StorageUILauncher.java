/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.wizard;

import fr.ird.observe.client.ClientUIContextApplicationComponent;
import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import fr.ird.observe.dto.ObserveUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.swing.wizard.BusyChangeListener;
import org.nuiton.jaxx.runtime.swing.wizard.WizardModel;
import org.nuiton.jaxx.runtime.swing.wizard.WizardUILancher;

import java.awt.Window;
import java.util.Arrays;
import java.util.EnumSet;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Le wizard de base pour les opération sur le service de données.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class StorageUILauncher extends WizardUILancher<StorageStep, StorageUIModel, StorageUI> {//implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(StorageUILauncher.class);

    protected final String title;

    /**
     * Méthode pour lancer l'action de connexion à une base distante.
     * <p>
     * Cette méthode doit être appelée avant toute action avec une base distante
     * :
     * <p>
     * synchronisation, récupération du référentiel distant...
     *
     * @param context le context applicatif
     * @param mainUI  la fenêtre principale parent (peut être null)
     * @param model   le modèle de source de données à utiliser
     * @see StorageUI
     */
    public static void obtainRemoteConnexion(JAXXContext context, Window mainUI, StorageUIModel model) {
        obtainConnexion(context, mainUI, model, t("observe.ui.title.connect.remoteDB"), DataSourceConnectMode.REMOTE);
    }

    /**
     * Méthode pour lancer l'action de connexion à une base distante.
     * <p>
     * Cette méthode doit être appelée avant toute action avec un server distant
     * :
     * <p>
     * synchronisation, récupération du référentiel distant...
     *
     * @param context le context applicatif
     * @param mainUI  la fenêtre principale parent (peut être null)
     * @param model   le modèle de source de données à utiliser
     * @see StorageUI
     */
    public static void obtainServerConnexion(JAXXContext context, Window mainUI, StorageUIModel model) {
        obtainConnexion(context, mainUI, model, t("observe.ui.title.connect.serverDB"), DataSourceConnectMode.SERVER);
    }

    /**
     * Méthode pour lancer l'action de connexion à une source distante.
     * <p>
     * Cette méthode doit être appelée avant toute action avec un source distante
     * :
     * <p>
     * synchronisation, récupération du référentiel distant...
     *
     * @param context le context applicatif
     * @param mainUI  la fenêtre principale parent (peut être null)
     * @param model   le modèle de source de données à utiliser
     * @param title   le titre de la fenêtre
     * @param dbMode  le type déconnexion (base distante ou serveur distant)
     * @see StorageUI
     */
    public static void obtainConnexion(JAXXContext context, Window mainUI, StorageUIModel model, String title, DataSourceConnectMode dbMode) {

        if (mainUI == null) {
            mainUI = ClientUIContextApplicationComponent.value().getMainUI();
        }
//        addStorageUIHandler(context);
        StorageUILauncher launcher = new StorageUILauncher(context, mainUI, model, title) {

            @Override
            protected void init(StorageUI ui) {
                StorageUIModel model = ui.getModel();
                DataSourceInitModel initModel = model.getInitModel();
                initModel.setAuthorizedInitModes(EnumSet.of(DataSourceInitMode.CONNECT));
                initModel.getAuthorizedConnectModes().remove(DataSourceConnectMode.LOCAL);

                ChooseDbModel chooseDb = model.getChooseDb();

                model.setExcludeSteps(Arrays.asList(
                        StorageStep.CHOOSE_DB_MODE,
                        StorageStep.CONFIG_REFERENTIAL,
                        StorageStep.CONFIG_DATA,
                        StorageStep.SELECT_DATA,
                        StorageStep.ROLES,
                        StorageStep.BACKUP,
                        StorageStep.CONFIRM)
                );
                model.setSteps(StorageStep.CONFIG);
                model.updateUniverse();

                chooseDb.setInitMode(DataSourceInitMode.CONNECT);
                chooseDb.setConnectMode(dbMode);
            }

            @Override
            protected StorageUI createUI(JAXXContext context,
                                         Window mainUI,
                                         Class<StorageUI> storageUIClass,
                                         Class<StorageUIModel> modelClass,
                                         StorageUIModel model) throws Exception {
                if (!(mainUI instanceof JAXXObject)) {
                    mainUI = null;
                }
                return super.createUI(context, mainUI, storageUIClass, modelClass, model);
            }

            @Override
            public void doAction(StorageUI ui) {
                super.doAction(ui);
                log.debug("Apply new remote connexion to " + model);
                model.setValueAdjusting(true);
                try {
                    DataSourceHelper.exportModel(ui.getModel(), model);
                } finally {
                    model.setValueAdjusting(false);
                }
                model.validate();
            }
        };

        launcher.start();
    }

    /**
     * Méthode pour lancer l'action de connexion à une base locale ou distante.
     *
     * @param context le context applicatif
     * @param mainUI  la fenêtre principale parent (peut être null)
     * @param model   le modèle de source de données à utiliser
     * @see StorageUI
     */
    public static void obtainConnexion(JAXXContext context, Window mainUI, DataSourceSelectorModel model) {

        if (mainUI == null) {
            mainUI = ClientUIContextApplicationComponent.value().getMainUI();
        }
        StorageUILauncher launcher = new StorageUILauncher(context, mainUI, model, t("observe.ui.title.connect.existingDB")) {

            @Override
            protected void init(StorageUI ui) {
                StorageUIModel model = ui.getModel();
                ChooseDbModel chooseDb = model.getChooseDb();
                DataSourceConnectMode dbMode = chooseDb.getInitModel().getConnectMode();
                DataSourceInitModel initModel = model.getInitModel();
                initModel.setAuthorizedInitModes(EnumSet.of(DataSourceInitMode.CONNECT));
                initModel.setAuthorizedCreateModes(EnumSet.noneOf(DataSourceCreateMode.class));

                // exclusion des étapes non requises
                model.setExcludeSteps(Arrays.asList(
                        StorageStep.SELECT_DATA,
                        StorageStep.CONFIG_REFERENTIAL,
                        StorageStep.CONFIG_DATA,
                        StorageStep.ROLES,
                        StorageStep.BACKUP,
                        StorageStep.CONFIRM)
                );
                model.setSteps(StorageStep.CONFIG);
                model.updateUniverse();

                // par défaut, on se positionne sur la base distante ?
                chooseDb.setInitMode(DataSourceInitMode.CONNECT);
                chooseDb.setConnectMode(dbMode);
            }

            @Override
            protected StorageUI createUI(JAXXContext context,
                                         Window mainUI,
                                         Class<StorageUI> storageUIClass,
                                         Class<StorageUIModel> modelClass,
                                         StorageUIModel model) throws Exception {
                if (!(mainUI instanceof JAXXObject)) {
                    mainUI = null;
                }
                return super.createUI(context, mainUI, storageUIClass, modelClass, model);
            }

            @Override
            public void doAction(StorageUI ui) {
                super.doAction(ui);
                if (ui.getModel().isValid()) {
                    log.info(String.format("Apply new connexion %s to %s", ui.getModel(), model));
                    DataSourceHelper.exportModel(ui.getModel(), model);
                }
                model.validate();
                model.fireDataSourceChanged();
            }
        };

        launcher.start();
    }

    public StorageUILauncher(JAXXContext context, Window frame, StorageUIModel model, String title) {
        super(context, frame, StorageUI.class, StorageUIModel.class, model);
        this.title = title;
    }

    public StorageUILauncher(JAXXContext context, Window frame, String title) {
        super(context, frame, StorageUI.class, StorageUIModel.class);
        this.title = title;
    }

    @Override
    protected void init(StorageUI ui) {
        log.info("Will init " + ui.getName());
        super.init(ui);
        if (title != null) {
            ui.setTitle(title);
        }
        BusyChangeListener listener = new BusyChangeListener(ui);
        UIHelper.setLayerUI(ui.getTabs(), ui.getBusyBlockLayerUI());
        listener.setBlockingUI(ui.getBusyBlockLayerUI());
        ui.getModel().addPropertyChangeListener(WizardModel.BUSY_PROPERTY_NAME, listener);
        ui.setContextValue(this);
    }

    @Override
    public void start() {
        super.start();
        ui.getModel().setBusy(false);
    }

    @Override
    protected void doCancel(StorageUI ui) {
        log.debug("Will cancel " + ui.getName());
        ui.getModel().setBusy(true);
        super.doCancel(ui);
        //FIXME:BodyContent We should not have to do this ? or it should be done by invoker
//        getContentUIManager().restartEdit();
    }

    @Override
    public void doAction(StorageUI ui) {
        ui.getModel().setBusy(true);
    }

    @Override
    public void doClose(StorageUI ui, boolean wasCanceled) {
        log.debug("Will close " + ui.getName() + " (was canceled ? " + wasCanceled + ")");
        super.doClose(ui, wasCanceled);
        ui.getModel().setBusy(false);
        ui.dispose();

        ObserveUtil.cleanMemory();

    }

    public String getTitle() {
        return title;
    }

}
