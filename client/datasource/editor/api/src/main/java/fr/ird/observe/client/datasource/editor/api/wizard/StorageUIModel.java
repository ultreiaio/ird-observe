/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.wizard;

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.ConnexionStatus;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.client.datasource.config.form.LocalConfigurationModel;
import fr.ird.observe.client.datasource.config.form.RemoteConfigurationModel;
import fr.ird.observe.client.datasource.config.form.ServerConfigurationModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.datasource.security.model.DataSourceSecurityModel;
import fr.ird.observe.datasource.security.model.DataSourceUserDto;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.swing.wizard.WizardModel;
import org.nuiton.jaxx.runtime.swing.wizard.WizardUILancher;

import javax.swing.Icon;
import java.io.File;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Le modele de l'ui pour changer de source de données.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
@GenerateTemplate(template = "StorageUIModel.ftl")
public class StorageUIModel extends WizardModel<StorageStep> implements WithClientUIContextApi {

    public static final String DO_BACKUP_PROPERTY_NAME = "doBackup";
    public static final String BACKUP_FILE_PROPERTY_NAME = "backupFile";
    public static final String BACKUP_FILE_NAME_PROPERTY_NAME = "backupFileName";
    public static final String DUMP_FILE_PROPERTY_NAME = "dumpFile";
    public static final String DUMP_FILE_PATH_PROPERTY_NAME = "dumpFilePath";
    public static final String PREVIOUS_SERVICE_PROPERTY_NAME = "previousSource";
    public static final String VALID_PROPERTY_NAME = "valid";
    public static final String ALREADY_APPLIED_PROPERTY_NAME = "alreadyApplied";
    public static final Icon VALID_ICON = ConnexionStatus.SUCCESS.getIcon();
    public static final Icon NOT_VALID_ICON = ConnexionStatus.FAILED.getIcon();
    public static final String SUMMARY_TEXT_PROPERTY_NAME = "summaryText";
    private static final Logger log = LogManager.getLogger(StorageUIModel.class);
    private static final char[] EMPTY_PASSWORD = new char[0];
    /**
     * Local data source configuration.
     */
    protected final LocalConfigurationModel localConfig;
    /**
     * Remote data source configuration.
     */
    protected final RemoteConfigurationModel remoteConfig;
    /**
     * Server data source configuration.
     */
    protected final ServerConfigurationModel serverConfig;
    /**
     * Choose db model.
     */
    protected final ChooseDbModel chooseDb;
    /**
     * Progress model used to show operation progression.
     */
    private final ProgressionModel progressModel = new ProgressionModel();
    /**
     * Is backup of local data source required?
     * <p>
     * Only used if local data source exists...
     */
    protected boolean doBackup;
    /**
     * To avoid re-entrant code while invoking apply action.
     *
     * @since 4.0.4
     */
    protected boolean alreadyApplied;
    /**
     * Where to do the backup of local data source (when {@link #doBackup} is on.
     */
    protected File backupFile = new File("");
    /**
     * Previous optional data source configuration.
     */
    protected ObserveDataSourceConfiguration previousDataSourceConfiguration;
    /**
     * Data selection model (if required)
     */
    protected SelectionTreeModel selectDataModel;
    /**
     * Security model (if required)
     */
    protected DataSourceSecurityModel securityModel;
    /**
     * Data source information.
     **/
    protected ObserveDataSourceInformation dataSourceInformation;
    /**
     * Import file location.
     */
    protected File dumpFile;
    /**
     * Import data configuration.
     */
    protected ObserveDataSourceConfiguration importDataConfig;
    /**
     * Optional admin action.
     */
    protected ObstunaAdminAction adminAction;
    /**
     * Configuration of referential data source to import.
     */
    protected StorageUIModel centralSourceModel;
    /**
     * Configuration of data data source to import.
     */
    protected StorageUIModel dataSourceModel;
    /**
     * To select all trips.
     *
     * @since 6.0
     */
    private boolean selectAll;

    /**
     * Is model init ?
     */
    private boolean init;
    /**
     * The summary text.
     */
    private String summaryText;

    public StorageUIModel() {
        super(StorageStep.class, StorageStep.CHOOSE_DB_MODE, StorageStep.BACKUP, StorageStep.CONFIRM);
        this.chooseDb = new ChooseDbModelExt(this);
        chooseDb.addPropertyChangeListener(ChooseDbModel.VALIDATE_PROPERTY_NAME, evt -> validate());
        this.localConfig = new LocalConfigurationModel();
        localConfig.addPropertyChangeListener(LocalConfigurationModel.DATASOURCE_INFORMATION_PROPERTY_NAME, evt -> {
            if (!getChooseDb().isEditRemoteConfig() && !getChooseDb().isEditServerConfig()) {
                setDataSourceInformation((ObserveDataSourceInformation) evt.getNewValue());
            }
        });
        this.remoteConfig = new RemoteConfigurationModelExt(this::getAdminAction);

        // listen when connexionStatus has changed on delegate model to revalidate this model
        remoteConfig.addPropertyChangeListener(RemoteConfigurationModel.CONNEXION_STATUS_PROPERTY_NAME, evt -> validate());
        remoteConfig.addPropertyChangeListener(RemoteConfigurationModel.DATASOURCE_INFORMATION_PROPERTY_NAME, evt -> {
            if (getChooseDb().isEditRemoteConfig()) {
                setDataSourceInformation((ObserveDataSourceInformation) evt.getNewValue());
            }
        });
        this.serverConfig = new ServerConfigurationModelExt(this::getAdminAction);
        serverConfig.addPropertyChangeListener(ServerConfigurationModel.DATASOURCE_INFORMATION_PROPERTY_NAME, evt -> {
            if (getChooseDb().isEditServerConfig()) {
                setDataSourceInformation((ObserveDataSourceInformation) evt.getNewValue());
            }
        });
        // listen when connexionStatus has changed on delegate model to revalidate this model
        serverConfig.addPropertyChangeListener(ServerConfigurationModel.CONNEXION_STATUS_PROPERTY_NAME, evt -> validate());
        securityModel = new DataSourceSecurityModel();

        Version modelVersion = getClientConfig().getModelVersion();

        setLocalConfig(ObserveDataSourceConfigurationTopiaH2.create(
                I18n.n("observe.ui.datasource.storage.label.local"),
                null,
                ClientConfig.DB_NAME,
                "",
                EMPTY_PASSWORD,
                modelVersion));

        setRemoteConfig(ObserveDataSourceConfigurationTopiaPG.create(
                I18n.n("observe.ui.datasource.storage.label.remote"),
                "",
                "",
                EMPTY_PASSWORD,
                false,
                modelVersion));

        setServerConfig(ObserveDataSourceConfigurationRest.create(
                I18n.n("observe.ui.datasource.storage.label.server"),
                null,
                "",
                EMPTY_PASSWORD,
                null,
                modelVersion));
    }

    public DataSourceInitModel getInitModel() {
        return getChooseDb().getInitModel();
    }

    public RemoteConfigurationModel getRemoteConfig() {
        return remoteConfig;
    }

    public ServerConfigurationModel getServerConfig() {
        return serverConfig;
    }

    public LocalConfigurationModel getLocalConfig() {
        return localConfig;
    }

    public ChooseDbModel getChooseDb() {
        return chooseDb;
    }

    public void fromStorageConfig(ObserveDataSourceConfigurationTopiaH2 sourceConfig) {
        getChooseDb().getInitModel().setInitMode(sourceConfig.getDatabaseFile().exists() ? DataSourceInitMode.CONNECT : DataSourceInitMode.CREATE);
        getChooseDb().getInitModel().setConnectMode(DataSourceConnectMode.LOCAL);
        setLocalConfig(sourceConfig);
    }

    public void fromStorageConfig(ObserveDataSourceConfigurationTopiaPG sourceConfig) {
        getChooseDb().getInitModel().setConnectMode(DataSourceConnectMode.REMOTE);
        setRemoteConfig(sourceConfig);
    }

    public void fromStorageConfig(ObserveDataSourceConfigurationRest sourceConfig) {
        getChooseDb().getInitModel().setConnectMode(DataSourceConnectMode.SERVER);
        setServerConfig(sourceConfig);
    }

    public void setServerConfig(ObserveDataSourceConfigurationRest restConfig) {
        getServerConfig().fromConfig(restConfig);
    }

    public void setRemoteConfig(ObserveDataSourceConfigurationTopiaPG remoteConfig) {
        getRemoteConfig().fromConfig(remoteConfig);
    }

    public void setLocalConfig(ObserveDataSourceConfigurationTopiaH2 localConfig) {
        getLocalConfig().fromConfig(localConfig);
    }

    @Override
    public void setStep(StorageStep step) {
        setAlreadyApplied(false);
        super.setStep(step);
    }

    public String getSummaryText() {
        return summaryText;
    }

    public void setSummaryText(String summaryText) {
        String oldValue = getSummaryText();
        this.summaryText = summaryText;
        firePropertyChange(SUMMARY_TEXT_PROPERTY_NAME, oldValue, summaryText);
    }

    /**
     * La méthode pour initialiser le modèle à partir du context applicatif et
     * d'un service existant.
     *
     * @param context le context applicatif
     * @param source  le service existant
     */
    public void init(JAXXContext context, ObserveDataSourceConfiguration source) {

        ClientConfig config = getClientConfig();

        StorageUIModel incomingModel = WizardUILancher.newModelEntry(StorageUIModel.class).getContextValue(context);
        if (incomingModel != null) {
            log.warn("from a incoming model " + incomingModel.getLabel() + " : " + incomingModel);
            // on initialise a partir d'un autre modèle
            DataSourceHelper.importModel(incomingModel, this);
            return;
        }

        // ajout paramétrage depuis la configuration

        initFromConfig(config);
        setPreviousDataSourceConfiguration(source);

        if (source == null) {
            log.debug("no service found, using default configuration");
            getChooseDb().initModes(config);
            return;
        }

        // on initialise le modèle à partir d'un service existant
        log.debug("from a previous service " + source);
        if (source.isLocal()) {
            // on est actuellement connecte sur une base locale
            fromStorageConfig((ObserveDataSourceConfigurationTopiaH2) source);
        } else if (source.isRemote()) {
            // on est sur une base distante
            fromStorageConfig((ObserveDataSourceConfigurationTopiaPG) source);
        } else if (source.isServer()) {
            // on est sur une base distante
            fromStorageConfig((ObserveDataSourceConfigurationRest) source);
        }
    }

    /**
     * La méthode pour initialiser le modèle à partir de la configuration.
     *
     * @param config configuration
     */
    public void init(ClientConfig config) {
        initFromConfig(config);
        log.debug("no service found, using default configuration");
        getChooseDb().initModes(config);
    }

    /**
     * La méthode pour initialiser le modèle à partir du context applicatif et
     * d'une configuration de source de donnée existante.
     *
     * @param previousConfig la configuration de service existant
     * @since 2.0
     */
    public void initFromPreviousConfig(ObserveDataSourceConfiguration previousConfig, ObserveDataSourceInformation previousInfo) {

        Objects.requireNonNull(previousConfig, "previousConfig parameter can not be null in method initFromPreviousConfig");

        ClientConfig config = getClientConfig();
        initFromConfig(config);

        if (previousConfig.isLocal()) {
            // on est actuellement connecte sur une base locale
            fromStorageConfig((ObserveDataSourceConfigurationTopiaH2) previousConfig);
        } else if (previousConfig.isRemote()) {
            // on est sur une base distante
            fromStorageConfig((ObserveDataSourceConfigurationTopiaPG) previousConfig);
        } else if (previousConfig.isServer()) {
            // on est sur une base distante
            fromStorageConfig((ObserveDataSourceConfigurationRest) previousConfig);
        }
        setDataSourceInformation(previousInfo);
    }

    /**
     * La méthode pour initialiser le modèle à partir du context applicatif et
     * d'un service existant.
     */
    public void initFromModel() {
        ClientConfig config = getClientConfig();
        initFromConfig(config);
        ChooseDbModel chooseDb = getChooseDb();
        DataSourceInitModel initModel = getInitModel();
        initModel.setAuthorizedInitModes(EnumSet.of(DataSourceInitMode.CONNECT));
        initModel.getAuthorizedConnectModes().remove(DataSourceConnectMode.LOCAL);
        chooseDb.initModes(config);
        initModel.setConnectMode(DataSourceConnectMode.REMOTE);
        start();

        if (log.isDebugEnabled()) {
            removePropertyChangeListener(UIHelper.LOG_PROPERTY_CHANGE_LISTENER);
            addPropertyChangeListener(UIHelper.LOG_PROPERTY_CHANGE_LISTENER);
        }
        // on teste si la connexion distante existe
        if (isValid()) {
            boolean valid = testRemote();
            log.info(String.format("Central source valid ? %s", valid));
        }
    }

    /**
     * To init internal states from config.
     *
     * @param config configuration
     */
    protected void initFromConfig(ClientConfig config) {
        setBackupFile(config.newBackupDataFile());
        setDumpFile(config.getImportDirectory());
        setDoBackup(false);
        ObserveDataSourcesManager dataSourcesManager = getDataSourcesManager();
        setLocalConfig(dataSourcesManager.newH2DataSourceConfiguration(I18n.n("observe.ui.datasource.storage.label.local")));
        setRemoteConfig(dataSourcesManager.newPGDataSourceConfiguration(I18n.n("observe.ui.datasource.storage.label.remote")));
        setServerConfig(dataSourcesManager.newRestDataSourceConfiguration(I18n.n("observe.ui.datasource.storage.label.server")));
    }

    public boolean isNeedReferentialDataSource() {
        return adminAction != null && adminAction == ObstunaAdminAction.CREATE;
    }

    public boolean isNeedDataDataSource() {
        return adminAction != null && adminAction == ObstunaAdminAction.CREATE;
    }

    @Override
    public void start() {

        boolean testRemote = dataSourceInformation != null;

        if (isNeedReferentialDataSource()) {
            getCentralSourceModel().initFromModel();
        }
        if (isNeedDataDataSource()) {
            getDataSourceModel().initFromModel();
        }

        super.start();

        chooseDb.start();

        firePropertyChange(DUMP_FILE_PROPERTY_NAME, getDumpFile());
        firePropertyChange(BACKUP_FILE_PROPERTY_NAME, getBackupFile());
        firePropertyChange(DO_BACKUP_PROPERTY_NAME, isDoBackup());
        firePropertyChange(PREVIOUS_SERVICE_PROPERTY_NAME, getPreviousDataSourceConfiguration());

        init = true;

        if (testRemote) {
            log.info("Test previous data source configuration...");
            test(true);
        } else if (isValid()) {
            log.info("Test valid storage model...");
            test(true);
        }
    }

    @Override
    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (isValueAdjusting()) {
            return;
        }
        super.firePropertyChange(propertyName, oldValue, newValue);
    }

    @Override
    protected void firePropertyChange(String propertyName, Object newValue) {
        if (isValueAdjusting()) {
            return;
        }
        super.firePropertyChange(propertyName, newValue);
    }

    public DataSourceSecurityModel getSecurityModel() {
        return securityModel;
    }

    public void setSecurityModel(DataSourceSecurityModel securityModel) {
        this.securityModel = securityModel;
    }

    public ObstunaAdminAction getAdminAction() {
        return adminAction;
    }

    public void setAdminAction(ObstunaAdminAction adminAction) {
        this.adminAction = adminAction;
    }

    // Ne pas supprimer, utilisé dans les templates
    public String getAdminActionLabel() {
        return getAdminAction().getLabel();
    }

    public SelectionTreeModel getSelectDataModel() {
        return selectDataModel;
    }

    public void setSelectDataModel(SelectionTreeModel selectDataModel) {
        this.selectDataModel = selectDataModel;
    }

    public boolean isWillMigrate() {
        return chooseDb.isCanMigrate()
                && dataSourceInformation != null
                && !dataSourceInformation.getMigrations().isEmpty();

    }

    public StorageUIModel getCentralSourceModel() {
        if (centralSourceModel == null) {
            centralSourceModel = new StorageUIModel() {

                @Override
                public String getLabel() {
                    return t("observe.ui.datasource.storage.label.import.referentiel") + getImportUrl(
                            I18n.n("observe.ui.datasource.storage.label.import.referentiel"),
                            I18n.n("observe.ui.datasource.storage.label.import.referentiel.remote"),
                            I18n.n("observe.ui.datasource.storage.label.import.referentiel.server"));
                }

                @Override
                public void validate() {
                    super.validate();

                    // on déclenche la revalidation du modèle
                    StorageUIModel.this.firePropertyChange(VALID_PROPERTY_NAME, isValid());
                }
            };
        }
        return centralSourceModel;
    }

    public String getImportUrl(String createKey, String remoteKey, String serverKey) {
        if (getChooseDb().getInitModel().isOnCreateMode()) {
            return I18n.t(createKey, getLocalConfig().getDirectory().getAbsolutePath());
        } else if (getChooseDb().getInitModel().isOnConnectModeRemote()) {
            return I18n.t(remoteKey, getRemoteConfig().getJdbcUrl());
        } else if (getChooseDb().getInitModel().isOnConnectModeServer()) {
            return I18n.t(serverKey, getServerConfig().getServerUrl());
        }
        return null;
    }

    public StorageUIModel getDataSourceModel() {
        if (dataSourceModel == null) {
            dataSourceModel = new StorageUIModel() {

                @Override
                public String getLabel() {
                    return t("observe.ui.datasource.storage.label.import.data") + getImportUrl(
                            I18n.n("observe.ui.datasource.storage.label.import.data"),
                            I18n.n("observe.ui.datasource.storage.label.import.data.remote"),
                            I18n.n("observe.ui.datasource.storage.label.import.data.server"));
                }

                @Override
                public void validate() {
                    super.validate();

                    // on déclenche la revalidation du modèle
                    StorageUIModel.this.firePropertyChange(VALID_PROPERTY_NAME, isValid());
                }
            };
        }
        return dataSourceModel;
    }

    @Override
    public void updateUniverse() {
        ChooseDbModel chooseDb = getChooseDb();
        DataSourceInitModel initMode = chooseDb.getInitModel();
        if (initMode.getInitMode() == null) {
            // pas de mode choisi donc l'univers ne change pas
            return;
        }
        List<StorageStep> universe = new ArrayList<>();
        if (adminAction == null) {
            // when doing an admin mode we do not choose db mode, we always work on remote
            universe.add(StorageStep.CHOOSE_DB_MODE);
        }

        boolean canBackup = chooseDb.isLocalStorageExist();
        switch (initMode.getInitMode()) {
            case CREATE:
                DataSourceCreateMode dataSourceCreateMode = chooseDb.getInitModel().getCreateMode();
                if (dataSourceCreateMode != null) {
                    switch (dataSourceCreateMode) {
                        case IMPORT_EXTERNAL_DUMP:
                            universe.add(StorageStep.CONFIG);
                            break;
                        //case IMPORT_LOCAL_STORAGE:
                        case IMPORT_REMOTE_STORAGE:
                            importDataConfig = getRemoteConfig().getConfiguration();
                            universe.add(StorageStep.CONFIG);
                            break;
                        case IMPORT_SERVER_STORAGE:
                            importDataConfig = getServerConfig().getConfiguration();
                            universe.add(StorageStep.CONFIG);
                            break;
                    }
                }
                break;
            case CONNECT:
                switch (initMode.getConnectMode()) {

                    case LOCAL:
                        // pas de backup si on veut utiliser la base locale
                        canBackup = false;
                        break;
                    case REMOTE:
                    case SERVER:
                        canBackup = false;
                        universe.add(StorageStep.CONFIG);
                        break;
                }
                break;
        }
        if (canBackup) {
            universe.add(StorageStep.BACKUP);

            // when backup is possible always
            setDoBackup(true);
        }
        if (adminAction != null) {
            if (adminAction == ObstunaAdminAction.CREATE) {
                universe.add(StorageStep.CONFIG_REFERENTIAL);
                if (chooseDb.getReferentielImportMode() != null && chooseDb.getReferentielImportMode() != DataSourceCreateMode.EMPTY) {
                    // can import data only if import a referentiel
                    universe.add(StorageStep.CONFIG_DATA);
                }

                if (chooseDb.getDataImportMode() != null && chooseDb.getDataImportMode() != DataSourceCreateMode.EMPTY) {
                    universe.add(StorageStep.SELECT_DATA);
                }
            }
            universe.add(StorageStep.ROLES);
        }
        universe.add(StorageStep.CONFIRM);
        if (excludeSteps != null) {
            universe.removeAll(excludeSteps);
        }
        log.debug(String.format("steps to use : %s", universe));
        setSteps(universe.toArray(new StorageStep[0]));
    }

    @Override
    public boolean validate(StorageStep s) {
        boolean validate = super.validate(s);
        if (validate) {
            ChooseDbModel chooseDb = getChooseDb();
            DataSourceInitModel initModel = chooseDb.getInitModel();
            switch (s) {
                case CHOOSE_DB_MODE:
                    validate = chooseDb.validate();
                    break;
                case CONFIG:
                    if ((initModel.isOnConnectMode() && initModel.isOnConnectModeRemote()) || (initModel.isOnCreateMode() && initModel.isOnCreateModeImportRemoteStorage())) {
                        validate = getRemoteConfig().isConnexionSuccess();
                    } else if ((initModel.isOnConnectMode() && initModel.isOnConnectModeServer()) || (initModel.isOnCreateMode() && initModel.isOnCreateModeImportServerStorage())) {
                        validate = getServerConfig().isConnexionSuccess();
                    } else if (initModel.isOnCreateMode() && initModel.isOnCreateModeImportExternalDump()) {
                        validate = isValidDumpFile(dumpFile);
                    }
                    break;
                case CONFIG_REFERENTIAL:
                    if (chooseDb.getReferentielImportMode() == null) {
                        return false;
                    }
                    switch (chooseDb.getReferentielImportMode()) {

                        case EMPTY:
                        case IMPORT_LOCAL_STORAGE:
                        case IMPORT_INTERNAL_DUMP:

                            break;
                        case IMPORT_EXTERNAL_DUMP:
                            // external dumb must be filled
                            validate = isValidDumpFile(centralSourceModel.getDumpFile());
                            break;
                        case IMPORT_REMOTE_STORAGE:
                        case IMPORT_SERVER_STORAGE:
                            // remote db connexion must be valid
                            validate = centralSourceModel.isValid();
                            if (validate) {
                                // check remote db != remote import db
                                validate = !Objects.equals(centralSourceModel.getRemoteUrlWithDatabase(), getRemoteUrlWithDatabase());
                            }
                            if (validate) {

                                validate = centralSourceModel.getDataSourceInformation().canReadReferential();
                            }
                            break;
                    }

                    break;

                case CONFIG_DATA:
                    if (chooseDb.getDataImportMode() == null) {
                        return false;
                    }
                    switch (chooseDb.getDataImportMode()) {

                        case EMPTY:
                        case IMPORT_LOCAL_STORAGE:
                        case IMPORT_INTERNAL_DUMP:

                            break;
                        case IMPORT_EXTERNAL_DUMP:
                            // external dumb must be filled
                            validate = isValidDumpFile(dataSourceModel.getDumpFile());
                            break;
                        case IMPORT_REMOTE_STORAGE:
                            // remote db connexion must be valid
                            validate = dataSourceModel.isValid();

                            if (validate) {

                                // check remote db != remote import db
                                validate = !Objects.equals(dataSourceModel.getRemoteUrlWithDatabase(), getRemoteUrlWithDatabase());
                            }
                            if (validate) {
                                validate = dataSourceModel.getDataSourceInformation().canReadData();
                            }
                            break;
                    }
                    break;
                case BACKUP:
                    validate = !doBackup || backupFile != null && !backupFile.exists() && ObserveUtil.withSqlGzExtension(backupFile.getName());
                    if (validate) {
                        String filename = getBackupFileName();
                        validate = !filename.isEmpty() && !filename.contains(".");
                    }
                    break;
                case SELECT_DATA:
                    // chemit 20100525 : aucune contrainte dans ce cas
                    validate = selectDataModel != null;
                    break;
                case ROLES:
                    Set<DataSourceUserDto> role = getSecurityModel().getUsers();
                    int assigned = getSecurityModel().getAssigned();
                    validate = role.size() == assigned;
                    break;
                case CONFIRM:
                    break;
            }
        }
        return validate;
    }

    @Override
    public void validate() {
        super.validate();
        // toujours forcer la propagation
        firePropertyChange(VALID_PROPERTY_NAME, isValid());
    }

    public boolean isValid() {
        boolean result = false;
        ChooseDbModel chooseDb = getChooseDb();
        DataSourceInitModel initMode = chooseDb.getInitModel();
        if (initMode != null) {
            switch (initMode.getInitMode()) {
                case CREATE:
                    result = initMode.getCreateMode() != null && validate(StorageStep.CONFIG);
                    break;
                case CONNECT:
                    switch (initMode.getConnectMode()) {
                        case LOCAL:
                            result = getLocalConfig().getDatabaseFile() != null && getLocalConfig().getDatabaseFile().exists();
                            break;
                        case REMOTE:
                        case SERVER:
                            result = validate(StorageStep.CONFIG);
                            break;
                    }
            }
        }
        return result;
    }

    public void updateUniverseAndValidate() {
        if (isNotInit()) {
            return;
        }
        updateUniverse();
        validate();
    }

    public boolean isAlreadyApplied() {
        return alreadyApplied;
    }

    public void setAlreadyApplied(boolean alreadyApplied) {
        boolean oldValue = isAlreadyApplied();
        if (Objects.equals(oldValue, alreadyApplied)) {
            return;
        }
        this.alreadyApplied = alreadyApplied;
        firePropertyChange(ALREADY_APPLIED_PROPERTY_NAME, oldValue, alreadyApplied);
    }

    // Ne pas supprimer, utilise dans des templates
    public boolean isUseSelectData() {
        return getSteps() != null && getSteps().contains(StorageStep.SELECT_DATA);
    }

    // Ne pas supprimer, utilise dans des templates

    public boolean isBackupAction() {
        return getSteps() != null && getStepIndex(StorageStep.BACKUP) == 0;
    }

    public boolean isLocal() {
        return getChooseDb().getInitModel().isOnConnectModeLocal() || getChooseDb().getInitModel().isOnCreateMode();
    }

    public String getLabel() {
        String txt;
        if (isLocal()) {
            txt = getLocalConfig().getLabel();
        } else if (getChooseDb().getInitModel().isOnConnectModeRemote()) {
            txt = getRemoteConfig().getLabel();
        } else {
            txt = getServerConfig().getLabel();
        }
        return txt;
    }

    public String getLabelWithUrl() {
        String txt;
        switch (getChooseDb().getInitModel().getConnectMode()) {
            case LOCAL:
                txt = getLocalConfig().getDatabaseFile().getAbsolutePath();
                break;
            case SERVER:
                ObserveDataSourceConfigurationRest restConfig = getServerConfig().getConfiguration();
                txt = restConfig.getUrl() == null ? "" : restConfig.getUrl();
                Optional<String> optionalDatabaseName = Optional.ofNullable(restConfig.getDatabaseName());
                txt += " - " + t("observe.ui.datasource.storage.server.dataBase.name") + " " + (optionalDatabaseName.orElse(t("observe.ui.datasource.storage.server.default.dataBase")));
                break;
            case REMOTE:
                txt = getRemoteConfig().getConfiguration().getUrl();
                break;
            default:
                throw new IllegalStateException(String.format("Can't have a such mode: %s", getChooseDb().getInitModel().isOnConnectMode()));
        }
        return String.format("%s (%s)", getLabel(), txt);
    }


    public File getBackupFile() {
        return backupFile;
    }

    public void setBackupFile(File backupFile) {
        File oldValue = getBackupFile();
        if (Objects.equals(oldValue, backupFile)) {
            return;
        }
        String oldBackupFileName = getBackupFileName();
        this.backupFile = backupFile;
        firePropertyChange(BACKUP_FILE_PROPERTY_NAME, oldValue, backupFile);
        firePropertyChange(BACKUP_FILE_NAME_PROPERTY_NAME, oldBackupFileName, getBackupFileName());
        if (isInit()) {
            validate();
        }
    }

    public String getBackupFileName() {
        return ObserveUtil.removeSqlGzExtension(backupFile.getName());
    }

    public boolean isDoBackup() {
        return doBackup;
    }

    public void setDoBackup(boolean doBackup) {
        boolean oldValue = isDoBackup();
        if (Objects.equals(oldValue, doBackup)) {
            return;
        }
        this.doBackup = doBackup;
        firePropertyChange(DO_BACKUP_PROPERTY_NAME, oldValue, doBackup);
    }

    public Version getModelVersion() {
        return getClientConfig().getModelVersion();
    }

    public ObserveDataSourceConfiguration getPreviousDataSourceConfiguration() {
        return previousDataSourceConfiguration;
    }

    public void setPreviousDataSourceConfiguration(ObserveDataSourceConfiguration previousDataSourceConfiguration) {
        this.previousDataSourceConfiguration = previousDataSourceConfiguration;
    }

    public File getDumpFile() {
        return dumpFile;
    }

    public void setDumpFile(File dumpFile) {
        File oldValue = getDumpFile();
        if (Objects.equals(oldValue, dumpFile)) {
            return;
        }
        this.dumpFile = dumpFile;
        firePropertyChange(DUMP_FILE_PROPERTY_NAME, oldValue, dumpFile);
        firePropertyChange(DUMP_FILE_PATH_PROPERTY_NAME, null, getDumpFilePath());
        if (isInit()) {
            validate();
        }
    }

    public String getDumpFilePath() {
        return dumpFile == null ? "" : ObserveUtil.removeSqlGzExtension(dumpFile.getAbsolutePath());
    }

    public String getRemoteUrlWithDatabase() {
        String url = "";
        if (getChooseDb().isEditRemoteConfig()) {
            url = getRemoteConfig().getConfiguration().getUrl();
        } else if (getChooseDb().isEditServerConfig()) {
            url = getServerConfig().getServerUrl() + "-" + getServerConfig().getDatabaseName();
        }
        return url;
    }

    /**
     * Teste une connexion à distance à partir du modèle passé en paramètre.
     *
     * @return {@code true} si la connexion a pu être établie, {@code false} sinon.
     */
    public boolean testRemote() {
        return test(false);
    }

    public boolean test(boolean testLocal) {
        setBusy(true);
        try {
            boolean checkRemote = getChooseDb().isEditRemoteConfig() || getChooseDb().getInitModel().isOnConnectModeRemote() || (getChooseDb().getInitModel().isOnCreateMode() && getChooseDb().getInitModel().isOnCreateModeImportRemoteStorage());
            boolean checkServer = getChooseDb().isEditServerConfig() || getChooseDb().getInitModel().isOnConnectModeServer() || (getChooseDb().getInitModel().isOnCreateMode() && getChooseDb().getInitModel().isOnCreateModeImportServerStorage());
            if (checkRemote) {
                return getRemoteConfig().testConnexion(getDataSourcesManager()::newSimpleDataSource);
            }
            if (checkServer) {
                return getServerConfig().testConnexion(getDataSourcesManager()::newSimpleDataSource);
            }
            if (testLocal) {
                return getLocalConfig().testConnexion(getDataSourcesManager()::newSimpleDataSource);
            }
            return true;
        } finally {
            setBusy(false);
        }
    }

    public void checkImportDbVersion(ObserveSwingDataSource source) {
        Version importServiceDbVersion = source.getVersion();
        Version currentDbVersion = getModelVersion();
        if (importServiceDbVersion.before(currentDbVersion)) {
            throw new IllegalStateException(String.format("Import db version (%s) is not compatible with the current database version (%s)", importServiceDbVersion, currentDbVersion));
        }
    }

    public ObserveDataSourceInformation getDataSourceInformation() {
        return dataSourceInformation;
    }

    public void setDataSourceInformation(ObserveDataSourceInformation dataSourceInformation) {
        this.dataSourceInformation = dataSourceInformation;
    }

    private boolean isValidDumpFile(File dumpFile) {
        return dumpFile != null && dumpFile.exists() && dumpFile.getName().endsWith(".sql.gz");
    }

    public boolean isSelectAll() {
        return selectAll;
    }

    public void setSelectAll(boolean selectAll) {
        this.selectAll = selectAll;
    }

    public ProgressionModel getProgressModel() {
        return progressModel;
    }

    public boolean isInit() {
        return init;
    }

    public boolean isNotInit() {
        return !init;
    }

    public Icon getSourceIcon() {
        if (isNotInit()) {
            return null;
        }
        DataSourceInitModel initModel = getChooseDb().getInitModel();
        Icon icon = null;
        if (initModel.isOnConnectMode()) {
            icon = initModel.getConnectMode().getIcon();
        }
        if (initModel.isOnCreateMode()) {
            icon = initModel.getCreateMode().getIcon();
        }
        //FIXME Use a create db icon
        return Objects.requireNonNullElse(icon, DataSourceConnectMode.LOCAL.getIcon());
    }

    public Icon getValidIcon(boolean valid) {
        return valid ? VALID_ICON : NOT_VALID_ICON;
    }

    public void resetConnexionStatus() {
        getLocalConfig().setConnexionStatus(ConnexionStatus.UNTESTED);
        getRemoteConfig().setConnexionStatus(ConnexionStatus.UNTESTED);
        getServerConfig().setConnexionStatus(ConnexionStatus.UNTESTED);
    }

    public String computeReport(StorageStep step) {
        log.debug(String.format("Build report from step %s", step));
        try {
            return StorageUIModelTemplate.generate(this);
        } catch (Exception e) {
            log.error("Could not compute report", e);
            return e.getMessage();
        }
    }
}
