package fr.ird.observe.client.datasource.editor.api.wizard.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUI;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUILauncher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.wizard.WizardUILancher;

import javax.swing.JFrame;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

public class Apply extends StorageUIActionSupport {

    private static final Logger log = LogManager.getLogger(Apply.class);

    public Apply() {
        super(t("observe.ui.datasource.storage.action.apply"), t("observe.ui.datasource.storage.action.apply.tip"), "accept", ObserveKeyStrokesEditorApi.KEY_STROKE_APPLY);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, StorageUI ui) {
        ui.getModel().setAlreadyApplied(true);
        Runnable action = WizardUILancher.APPLY_DEF.getContextValue(ui);
        if (action == null) {
            final StorageUILauncher launcher = ui.getContextValue(StorageUILauncher.class);
            action = () -> {
                try {
                    launcher.doAction(ui);
                } finally {
                    launcher.doClose(ui, false);
                }
            };
        }
        JFrame mainUI = getMainUI();
        if (mainUI == null) {
            log.warn(String.format("Launch standalone apply action %s", action));
            action.run();
        } else {
            getActionExecutor().addAction(t("observe.ui.action.storage.applyAction"), action);
        }
    }
}
