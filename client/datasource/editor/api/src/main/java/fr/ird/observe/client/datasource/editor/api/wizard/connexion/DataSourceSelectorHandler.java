package fr.ird.observe.client.datasource.editor.api.wizard.connexion;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.ConnexionStatus;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformationTemplate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;

import javax.swing.Icon;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

public class DataSourceSelectorHandler implements UIHandler<DataSourceSelector> {

    private static final Logger log = LogManager.getLogger(DataSourceSelectorHandler.class);

    private Icon validIcon;
    private Icon notValidIcon;
    private Icon successIcon;
    private Icon failedIcon;
    private DataSourceSelectorModel model;
    private DataSourceSelector ui;

    @Override
    public void beforeInit(DataSourceSelector ui) {
        this.ui = ui;
        successIcon = ConnexionStatus.SUCCESS.getIcon();
        failedIcon = ConnexionStatus.FAILED.getIcon();
        validIcon = UIHelper.getUIManagerActionIcon("validate");
        notValidIcon = SwingValidatorUtil.getErrorIcon();
        model = Objects.requireNonNull(ui.getContextValue(DataSourceSelectorModel.class));
    }

    @Override
    public void afterInit(DataSourceSelector ui) {
        model.addPropertyChangeListener(DataSourceSelectorModel.VALID_PROPERTY_NAME, e -> {
//            if (model.isNotInit()) {
//                return;
//            }
            DataSourceSelectorModel source = (DataSourceSelectorModel) e.getSource();
            onValidChanged(source);
        });
    }

    void onValidChanged(DataSourceSelectorModel source) {
        boolean valid = source.isValid();
        log.info(String.format("On valid changed for source: %s", model.getLabel()));
        String sourceLabel;
        String sourcePolicy = null;
        Icon sourceIcon = model.getSourceIcon();
        Icon statusIcon;
        if (valid) {
            statusIcon = successIcon;
            sourceLabel = model.getLabelWithUrl();
            ObserveDataSourceInformation dataSourceInformation = model.getDataSourceInformation();
            if (model.isInit() && dataSourceInformation != null && dataSourceInformation.getVersion() != null) {
                sourcePolicy = ObserveDataSourceInformationTemplate.generate(dataSourceInformation);
            }
        } else {
            statusIcon = failedIcon;
            sourceLabel = t("observe.Common.storage.not.valid");
        }
        setBorderColor(valid);
        ui.getSourceLabel().setIcon(sourceIcon);
        ui.getSourceLabel().setText(sourceLabel);
        ui.getSourcePolicy().setText(sourcePolicy);
        ui.getSourceStatus().setIcon(statusIcon);
    }

    void onDatasourceChanged(DataSourceSelectorModel source) {
        boolean valid = source.validateExt();
        log.info(String.format("On data source changed for source: %s", model.getLabel()));
        Icon validationIcon;
        String validationMessage;
        if (valid) {
            validationIcon = validIcon;
            validationMessage = t("observe.ui.datasource.actions.config.data.source.valid");

        } else {
            validationIcon = notValidIcon;
            validationMessage = source.getValidationMessage();
            if (validationMessage == null) {
                // means not using it
                validationIcon = null;
            }
        }
        setBorderColor(valid);
        ui.getSourceValidation().setIcon(validationIcon);
        ui.getSourceValidation().setText(validationMessage);
        onValidChanged(source);
    }

    private void setBorderColor(boolean valid) {
        TitledBorder border = (TitledBorder) ui.getBorder();
        border.setTitleColor(valid ? Color.BLACK : Color.RED);
        ui.setBorder(border);
    }
}
