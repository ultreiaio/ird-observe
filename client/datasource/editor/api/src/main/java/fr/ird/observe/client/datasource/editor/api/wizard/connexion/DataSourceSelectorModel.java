package fr.ird.observe.client.datasource.editor.api.wizard.connexion;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.DataSourceHelper;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.KeyStroke;
import java.util.Optional;

public class DataSourceSelectorModel extends StorageUIModel {
    public static final String VALIDATION_MESSAGE_PROPERTY_NAME = "validationMessage";
    public static final String DATA_SOURCE_CHANGED_PROPERTY_NAME = "dataSourceChanged";
    private final KeyStroke keyStroke;
    private String sourceLabel;
    private String validationMessage;
    private ObserveSwingDataSource source;
    private boolean requiredReadOnReferential;
    private boolean requiredWriteOnReferential;
    private boolean requiredReadOnData;
    private boolean requiredWriteOnData;

    public DataSourceSelectorModel(KeyStroke keyStroke) {
        this.keyStroke = keyStroke;
    }

    public ObserveSwingDataSource getSafeSourceNotOpened() {
        ObserveSwingDataSource source = getSafeSource(false);
        String targetSourceLabel = getLabelWithUrl().substring(getLabel().length());
        source.getConfiguration().setLabel(targetSourceLabel);
        return source;
    }

    public String getSourceLabel() {
        return sourceLabel;
    }

    public void setSourceLabel(String sourceLabel) {
        String oldValue = getSourceLabel();
        this.sourceLabel = sourceLabel;
        firePropertyChange("sourceLabel", oldValue, sourceLabel);
        firePropertyChange("label", null, getLabel());
    }

    public String getValidationMessage() {
        return validationMessage;
    }

    public void setValidationMessage(String validationMessage) {
        String oldValue = getValidationMessage();
        this.validationMessage = validationMessage;
        firePropertyChange(VALIDATION_MESSAGE_PROPERTY_NAME, oldValue, validationMessage);
    }

    public boolean isRequiredWriteOnReferential() {
        return requiredWriteOnReferential;
    }

    public void setRequiredWriteOnReferential(boolean requiredWriteOnReferential) {
        this.requiredWriteOnReferential = requiredWriteOnReferential;
    }

    public boolean isRequiredWriteOnData() {
        return requiredWriteOnData;
    }

    public void setRequiredWriteOnData(boolean requiredWriteOnData) {
        this.requiredWriteOnData = requiredWriteOnData;
    }

    public boolean isRequiredReadOnReferential() {
        return requiredReadOnReferential;
    }

    public void setRequiredReadOnReferential(boolean requiredReadOnReferential) {
        this.requiredReadOnReferential = requiredReadOnReferential;
    }

    public boolean isRequiredReadOnData() {
        return requiredReadOnData;
    }

    public void setRequiredReadOnData(boolean requiredReadOnData) {
        this.requiredReadOnData = requiredReadOnData;
    }

    public KeyStroke getKeyStroke() {
        return keyStroke;
    }

    public ObserveSwingDataSource getSource() {
        return source;
    }

    public void removeSource() {
        ObserveSwingDataSource.doCloseSource(source);
        source = null;
    }

    public ObserveSwingDataSource getSafeSource(boolean open) {
        if (source == null || open && !source.isOpen()) {
            source = DataSourceHelper.initDataSourceFromModel(this);
        }
        if (open) {
            ObserveSwingDataSource.doOpenSource(source);
        }
        return source;
    }

    public boolean isCanWriteReferential() {
        return isLocal() || Optional.ofNullable(getDataSourceInformation()).map(ObserveDataSourceInformation::canWriteReferential).orElse(false);
    }

    public boolean isCanWriteData() {
        return isLocal() || Optional.ofNullable(getDataSourceInformation()).map(ObserveDataSourceInformation::canWriteData).orElse(false);
    }

    public boolean validateExt() {
        setValidationMessage(null);
        if (!isValid()) {
            // skip extra validation
            setValidationMessage(null);
            return false;
        }
        ObserveDataSourceInformation dataSourceInformation = getDataSourceInformation();
        if (getChooseDb().getInitModel().isOnConnectMode() && getChooseDb().getInitModel().isOnConnectModeLocal() && getChooseDb().isLocalStorageExist() && dataSourceInformation == null) {
            // hack to get at once the data source information since while using this mode, the data source information
            // is not loaded
            //FIXME Remove this hack and find out why data source information is not well computed when needed
            test(true);
        }
        if (dataSourceInformation == null) {
            // skip extra validation
            setValidationMessage(null);
            return false;
        }
        if (isRequiredReadOnReferential() && !dataSourceInformation.canReadReferential()) {
            setValidationMessage(I18n.t("observe.ui.datasource.actions.config.data.source.requires.read.referential"));
            return false;
        }
        if (isRequiredWriteOnReferential() && !isCanWriteReferential()) {
            setValidationMessage(I18n.t("observe.ui.datasource.actions.config.data.source.requires.write.referential"));
            return false;
        }
        if (isRequiredReadOnData() && !dataSourceInformation.canReadData()) {
            setValidationMessage(I18n.t("observe.ui.datasource.actions.config.data.source.requires.read.data"));
            return false;
        }
        if (isRequiredWriteOnData() && !dataSourceInformation.canWriteData()) {
            setValidationMessage(I18n.t("observe.ui.datasource.actions.config.data.source.requires.write.data"));
            return false;
        }
        return true;
    }

    public void fireDataSourceChanged() {
        firePropertyChange(DATA_SOURCE_CHANGED_PROPERTY_NAME, validateExt());
    }
}
