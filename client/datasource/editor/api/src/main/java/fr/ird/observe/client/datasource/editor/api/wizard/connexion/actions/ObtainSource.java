package fr.ird.observe.client.datasource.editor.api.wizard.connexion.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.wizard.StorageUILauncher;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelector;
import fr.ird.observe.client.datasource.editor.api.wizard.connexion.DataSourceSelectorModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import java.awt.Window;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

public class ObtainSource extends JComponentActionSupport<DataSourceSelector> {

    private static final Logger log = LogManager.getLogger(ObtainSource.class);

    public ObtainSource() {
        super(t("observe.ui.action.configure"), null, "config", null);
    }

    @Override
    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        KeyStroke keyStroke = ui.getModel().getKeyStroke();
        setKeyStroke(keyStroke);
        super.defaultInit(inputMap, actionMap);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, DataSourceSelector ui) {
        DataSourceSelectorModel model = ui.getModel();
        log.info("Start obtain connexion for source: " + model.getSourceLabel());
        try {
            model.removeSource();
            StorageUILauncher.obtainConnexion(ui, ui.getParentContainer(Window.class), model);
        } finally {
            log.info(String.format("End obtain connexion for source: %s - %b", model.getSourceLabel(), model.isValid()));
        }
    }
}
