package fr.ird.observe.client.datasource.editor.api.wizard.launchers;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.DataSourceHelper;
import fr.ird.observe.client.datasource.editor.api.wizard.ObstunaAdminAction;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUI;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXContext;

import java.awt.Window;
import java.io.File;
import java.util.Objects;
import java.util.Set;

import static fr.ird.observe.client.datasource.editor.api.wizard.DataSourceHelper.toPGStorageConfig;

/**
 * Created on 14/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class CreateLauncher extends RemoteUILauncher {
    public static final String referentialDbLabel = I18n.n("observe.ui.datasource.storage.label.reference.import.db");
    private static final Logger log = LogManager.getLogger(CreateLauncher.class);
    private SqlScript referential;
    private SqlScript data;

    public static ObserveSwingDataSource toImportDataSourceConfig(StorageUIModel model) {

        if (!model.getChooseDb().doImportData()) {
            throw new IllegalStateException(String.format("Must be in select data mode to come here: %s", model));
        }

        String dbLabel = I18n.n("observe.ui.datasource.storage.label.data.import.db");
        ObserveSwingDataSource importDataSource;

        ObserveDataSourcesManager dataSourcesManager = model.getDataSourcesManager();
        switch (model.getChooseDb().getDataImportMode()) {
            case IMPORT_EXTERNAL_DUMP:
                File dumpFile = model.getDataSourceModel().getDumpFile();
                try (ObserveSwingDataSource importDataSource2 = dataSourcesManager.newTemporaryH2DataSource(dbLabel)) {
                    importDataSource = importDataSource2;
                    try {
                        importDataSource2.createFromDump(SqlScript.of(dumpFile.toURI()));
                    } catch (IncompatibleDataSourceCreateConfigurationException
                             | DataSourceCreateWithNoReferentialImportException
                             | DatabaseNotFoundException
                             | DatabaseConnexionNotAuthorizedException
                             | BabModelVersionException e) {
                        throw new ObserveSwingTechnicalException("Could not create import data source", e);
                    }
                }
                break;
            case IMPORT_REMOTE_STORAGE: {
                // import referentiel from a remote db
                ObserveDataSourceConfigurationTopiaPG dataSourceConfig = DataSourceHelper.toPGStorageConfig(model.getDataSourceModel(), dbLabel);
                dataSourceConfig.setCanMigrate(false);
                importDataSource = dataSourcesManager.newDataSource(dataSourceConfig);
            }
            break;
            case IMPORT_SERVER_STORAGE: {
                // import referentiel from a server db
                ObserveDataSourceConfigurationRest dataSourceConfig = DataSourceHelper.toRestStorageConfig(model.getDataSourceModel(), dbLabel);
                importDataSource = dataSourcesManager.newDataSource(dataSourceConfig);
            }
            break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return importDataSource;
    }

    public static SqlScript extract(ObserveDataSourcesManager dataSourcesManager, File dumpFile, boolean pg, boolean loadSchema, Set<String> importDataIds) {
        try (ObserveSwingDataSource importDataSource = dataSourcesManager.newTemporaryH2DataSource(referentialDbLabel)) {
            try {
                importDataSource.createFromDump(SqlScript.of(dumpFile.toURI()));
                return importDataSource.extract(pg, loadSchema, importDataIds);
            } catch (IncompatibleDataSourceCreateConfigurationException
                     | DataSourceCreateWithNoReferentialImportException
                     | DatabaseNotFoundException
                     | DatabaseConnexionNotAuthorizedException
                     | BabModelVersionException e) {
                throw new ObserveSwingTechnicalException("Could not create import data source", e);
            }
        }
    }

    public static SqlScript extract(ObserveDataSourcesManager dataSourcesManager, ObserveDataSourceConfiguration sourceConfiguration, boolean pg, boolean loadSchema, Set<String> importDataIds) {
        try (ObserveSwingDataSource importDataSource = dataSourcesManager.newDataSource(sourceConfiguration)) {
            try {
                importDataSource.open();
                return importDataSource.extract(pg, loadSchema, importDataIds);
            } catch (IncompatibleDataSourceCreateConfigurationException
                     | DataSourceCreateWithNoReferentialImportException
                     | DatabaseNotFoundException
                     | DatabaseConnexionNotAuthorizedException
                     | BabModelVersionException e) {
                throw new ObserveSwingTechnicalException("Could not create import data source", e);
            }
        }
    }

    public CreateLauncher(JAXXContext context, Window frame, DataSourceConnectMode connectMode) {
        super(ObstunaAdminAction.CREATE, context, frame, connectMode);
    }

    @Override
    protected void init(StorageUI ui) {
        StorageUIModel model = ui.getModel();
        // on autorise le mode de creation import referentiel
        // depuis une source distante
        //FIXME This logic should be done in StorageModel not here, like this!
        model.getChooseDb().setCanMigrate(true);
        model.getRemoteConfig().setCanCreateDatabase(true);
        model.getServerConfig().setCanCreateDatabase(true);
        super.init(ui);
    }

    @Override
    protected ObserveSwingDataSource initTask(StorageUIModel model) {
        ObserveSwingDataSource dataSource = getDataSource(model);
        // can create empty data base
        boolean importReferential = model.getChooseDb().doImportReferential();
        boolean importData = model.getChooseDb().doImportData();
        if (!importReferential && !importData) {
            // no import at all
            log.info("Create empty database.");
            return dataSource;
        }

        DataSourceCreateMode referentialImportMode = model.getChooseDb().getReferentielImportMode();
        DataSourceCreateMode dataImportMode = model.getChooseDb().getDataImportMode();

        File referentialDump = null;
        ObserveDataSourceConfiguration referentialConfig = null;
        File dataDump = null;
        ObserveDataSourceConfiguration dataConfig = null;
        Set<String> selectedDataIds = null;
        if (importReferential && referentialImportMode != null) {
            switch (referentialImportMode) {
                case IMPORT_EXTERNAL_DUMP:
                    referentialDump = model.getCentralSourceModel().getDumpFile();

                    break;
                case IMPORT_REMOTE_STORAGE:
                    ObserveDataSourceConfigurationTopiaPG pgConfig = toPGStorageConfig(model.getCentralSourceModel(), referentialDbLabel);
                    pgConfig.setCanMigrate(false);
                    referentialConfig = pgConfig;
                    break;
                case IMPORT_SERVER_STORAGE:
                    referentialConfig = DataSourceHelper.toRestStorageConfig(model.getCentralSourceModel(), referentialDbLabel);
                    break;
                default:
                    throw new IllegalStateException("Can't come here");
            }
        }
        if (importData && dataImportMode != null) {
            String dbLabel = I18n.n("observe.ui.datasource.storage.label.data.import.db");
            selectedDataIds = model.getSelectDataModel().getSelectedDataIds();
            switch (dataImportMode) {
                case IMPORT_EXTERNAL_DUMP:
                    dataDump = model.getDataSourceModel().getDumpFile();
                    break;
                case IMPORT_REMOTE_STORAGE: {
                    ObserveDataSourceConfigurationTopiaPG dataSourceConfig = toPGStorageConfig(model.getDataSourceModel(), dbLabel);
                    dataSourceConfig.setCanMigrate(false);
                    dataConfig = dataSourceConfig;
                }
                break;
                case IMPORT_SERVER_STORAGE: {
                    dataConfig = DataSourceHelper.toRestStorageConfig(model.getDataSourceModel(), dbLabel);
                    break;
                }
                default:
                    throw new IllegalStateException("Can't come here");
            }
        }

        if (referentialDump != null) {
            if (dataDump != null) {
                if (referentialDump.equals(dataDump)) {

                    log.info(String.format("Extract referential + data from dump %s", referentialDump));
                    referential = extract(model.getDataSourcesManager(), referentialDump, true, true, selectedDataIds);
                } else {
                    log.info(String.format("Extract referential from dump %s", referentialDump));
                    referential = extract(model.getDataSourcesManager(), referentialDump, true, true, null);
                    log.info(String.format("Extract data from dump %s %s", dataDump, selectedDataIds));
                    data = extract(model.getDataSourcesManager(), dataDump, true, false, selectedDataIds);
                }
            } else {
                log.info(String.format("Extract referential from dump %s", referentialDump));
                referential = extract(model.getDataSourcesManager(), referentialDump, true, true, null);
                if (dataConfig != null) {
                    log.info(String.format("Extract data from config %s %s", dataConfig.getLabel(), selectedDataIds));
                    data = extract(model.getDataSourcesManager(), dataConfig, true, false, selectedDataIds);
                }
            }
        } else {
            Objects.requireNonNull(referentialConfig);
            if (dataConfig != null) {
                if (referentialConfig.equals(dataConfig)) {
                    log.info(String.format("Extract referential + data from config %s %s", referentialConfig.getLabel(), selectedDataIds));
                    referential = extract(model.getDataSourcesManager(), referentialConfig, true, true, selectedDataIds);
                } else {
                    log.info(String.format("Extract referential from config %s", referentialConfig.getLabel()));
                    referential = extract(model.getDataSourcesManager(), referentialConfig, true, true, null);
                    log.info(String.format("Extract data from config %s %s", dataConfig.getLabel(), selectedDataIds));
                    data = extract(model.getDataSourcesManager(), dataConfig, true, false, selectedDataIds);
                }
            } else {
                log.info(String.format("Extract referential from config %s", referentialConfig.getLabel()));
                referential = extract(model.getDataSourcesManager(), referentialConfig, true, true, null);
                if (dataDump != null) {
                    log.info(String.format("Extract data from dump %s %s", dataDump, selectedDataIds));
                    data = extract(model.getDataSourcesManager(), dataDump, true, false, selectedDataIds);
                }
            }
        }
        Objects.requireNonNull(referential);
        return dataSource;
    }

    @Override
    protected void execute(ObserveSwingDataSource dataSource, ObserveDataSourceInformation dataSourceInformation) throws Exception {
        log.info("Create db...");
        try {
            if (referential != null) {
                dataSource.createFromImport(referential, data);
            } else {
                dataSource.createEmpty();
            }
            log.info(String.format("Open [%s] and create it.", dataSource.getLabel()));
        } finally {
            ObserveSwingDataSource.doCloseSource(dataSource);
        }
    }

}

