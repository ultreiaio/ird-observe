/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.wizard.launchers;

import fr.ird.observe.client.ClientUIContextApplicationComponent;
import fr.ird.observe.client.ErrorHandler;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.client.datasource.editor.api.wizard.ObstunaAdminAction;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageStep;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUI;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUILauncher;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.security.model.DataSourceSecurityModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXContext;

import java.awt.Dialog;
import java.awt.Window;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Un wizard pour effectuer des opération de création ou mise à jour d'une
 * base distante.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public abstract class RemoteUILauncher extends StorageUILauncher {

    private static final Logger log = LogManager.getLogger(RemoteUILauncher.class);
    private final DataSourceConnectMode connectMode;
    private final ErrorHandler errorHandler;
    protected ObstunaAdminAction action;

    public static ObserveSwingDataSource getDataSource(StorageUIModel model) {

        ObserveDataSourceConfiguration configuration;
        if (model.getChooseDb().getInitModel().isOnConnectModeRemote()) {
            configuration = model.getRemoteConfig().getConfiguration();
            //FIXME this should be done before this, no need to hardcode this that way
            // pas autorise a migrer automatiquement
            configuration.setCanMigrate(false);
        } else {
            configuration = model.getServerConfig().getConfiguration();
        }
        return ClientUIContextApplicationComponent.value().getDataSourcesManager().newDataSource(configuration);
    }

    public RemoteUILauncher(ObstunaAdminAction action, JAXXContext context, Window frame, DataSourceConnectMode connectMode) {
        super(context, frame, action.getLabel());
        this.action = action;
        this.connectMode = connectMode;
        errorHandler = new ErrorHandler() {
            @Override
            protected void onInvalidAuthenticationTokenException() {
                // not used here
            }
        };
        Thread.setDefaultUncaughtExceptionHandler(errorHandler);
    }

    @Override
    protected void init(StorageUI ui) {
        super.init(ui);
        ui.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        errorHandler.setContainer(ui);
        StorageUIModel model = ui.getModel();
        ChooseDbModel chooseDb = model.getChooseDb();
        DataSourceInitModel initModel = model.getInitModel();
        initModel.setAuthorizedInitModes(EnumSet.of(DataSourceInitMode.CONNECT));
        initModel.getAuthorizedConnectModes().remove(DataSourceConnectMode.LOCAL);

        model.setAdminAction(action);
        chooseDb.setInitMode(DataSourceInitMode.CONNECT);
        chooseDb.setConnectMode(connectMode);
        chooseDb.updateEditConfig();

        List<StorageStep> steps = new ArrayList<>();
        steps.add(StorageStep.CONFIG);
        if (action == ObstunaAdminAction.CREATE) {
            // referential import configuration
            steps.add(StorageStep.CONFIG_REFERENTIAL);
            // data import configuration
            steps.add(StorageStep.CONFIG_DATA);
            // select data to import
            steps.add(StorageStep.SELECT_DATA);
        }
        steps.add(StorageStep.ROLES);
        steps.add(StorageStep.CONFIRM);
        model.setSteps(steps.toArray(new StorageStep[0]));
        ui.setSize(800, 600);
        UIHelper.addApplicationIcon(ui);
    }

    @Override
    public void doAction(StorageUI ui) {
        super.doAction(ui);
        try {
            StorageUIModel model = ui.getModel();
            try (ObserveSwingDataSource dataSource = initTask(model)) {
                ObserveDataSourceInformation dataSourceInformation = model.getDataSourceInformation();
                execute(dataSource, dataSourceInformation);
                DataSourceSecurityModel securityModel = model.getSecurityModel();
                applySecurity(dataSource, securityModel);
            }
        } catch (Exception e) {
            errorHandler.createFeedback(e);
        }
    }

    protected abstract ObserveSwingDataSource initTask(StorageUIModel model) throws Exception;

    protected abstract void execute(ObserveSwingDataSource dataSource, ObserveDataSourceInformation dataSourceInformation) throws Exception;

    protected void applySecurity(ObserveSwingDataSource dataSource, DataSourceSecurityModel securityModel) {
        log.info(String.format("Will apply security model %s", securityModel));
        dataSource.applySecurity(securityModel.getUsers());
    }
}
