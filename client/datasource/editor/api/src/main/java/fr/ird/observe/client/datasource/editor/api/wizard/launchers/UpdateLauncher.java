package fr.ird.observe.client.datasource.editor.api.wizard.launchers;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.ObstunaAdminAction;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import io.ultreia.java4all.util.Version;
import org.nuiton.jaxx.runtime.JAXXContext;

import java.awt.Window;

/**
 * Created on 14/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class UpdateLauncher extends RemoteUILauncher {

    private Version targetVersion;

    public UpdateLauncher(JAXXContext context, Window frame, DataSourceConnectMode connectMode) {
        super(ObstunaAdminAction.UPDATE, context, frame, connectMode);
    }

    @Override
    protected ObserveSwingDataSource initTask(StorageUIModel model) {

        ObserveDataSourceConfiguration configuration;
        if (model.getChooseDb().getInitModel().isOnConnectModeRemote()) {
            configuration = model.getRemoteConfig().getConfiguration();
        } else {
            configuration = model.getServerConfig().getConfiguration();
        }
        ObserveSwingDataSource dataSource = model.getDataSourcesManager().newDataSource(configuration);
        targetVersion = model.getClientConfig().getModelVersion();
        return dataSource;
    }

    @Override
    protected void execute(ObserveSwingDataSource dataSource, ObserveDataSourceInformation dataSourceInformation) {
        dataSource.migrateData(dataSourceInformation, targetVersion);
    }


}

