package fr.ird.observe.client.datasource.editor.api.wizard.tabs;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.AbstractButton;
import javax.swing.JRadioButton;
import java.awt.Font;
import java.io.File;
import java.util.Date;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
class ChooseDbModeUIHandler extends StorageTabUIHandler<ChooseDbModeUI> implements UIHandler<ChooseDbModeUI> {
    private static final Logger log = LogManager.getLogger(ChooseDbModeUIHandler.class);

    @Override
    public void afterInit(ChooseDbModeUI ui) {
        ChooseDbModel chooseDbModel = ui.getChooseDbModel();
        DataSourceInitModel initModel = chooseDbModel.getInitModel();
        ui.get$objectMap().forEach((id, component) -> {
            if (component instanceof BeanCheckBox) {
                BeanCheckBox editor = (BeanCheckBox) component;
                editor.init();
            } else if (component instanceof AbstractButton) {
                AbstractButton editor = (AbstractButton) component;
                String property = editor.getName();
                log.info(String.format("Register initModel property: %s", property));
                initModel.addPropertyChangeListener(property, evt -> editor.setSelected((Boolean) evt.getNewValue()));
            }
        });
        JRadioButton onCreateModeImportInternalDump = ui.getOnCreateModeImportInternalDump();
        File f = getClientConfig().getInitialDbDump();
        String text = onCreateModeImportInternalDump.getText() + "  ";
        if (f.exists()) {
            text += t("observe.ui.datasource.storage.internalDump.last.modified", new Date(f.lastModified()));
        } else {
            text += t("observe.ui.datasource.storage.internalDump.not.exist");
        }
        onCreateModeImportInternalDump.setText(text);
        chooseDbModel.addPropertyChangeListener(ChooseDbModel.CAN_MIGRATE_PROPERTY_NAME, evt -> {
            boolean canMigrate = (boolean) evt.getNewValue();
            onCanMigrateChanged(canMigrate);
        });
        onCanMigrateChanged(chooseDbModel.isCanMigrate());
        initModel.addPropertyChangeListener(DataSourceInitModel.INIT_MODE_PROPERTY_NAME, evt -> {
            DataSourceInitMode newValue = (DataSourceInitMode) evt.getNewValue();
            if (newValue != null) {
                switch (newValue) {
                    case CONNECT:
                        ui.getOnConnectMode().setFont(ui.getFont().deriveFont(Font.BOLD));
                        ui.getOnCreateMode().setFont(ui.getFont());
                        break;
                    case CREATE:
                        ui.getOnConnectMode().setFont(ui.getFont());
                        ui.getOnCreateMode().setFont(ui.getFont().deriveFont(Font.BOLD));
                        break;
                }
            }
        });
    }


    private void onCanMigrateChanged(boolean canMigrate) {
        log.info(String.format("onCanMigrateChanged: %s", canMigrate));
        String label;
        if (canMigrate) {
            label = I18n.t("observe.ui.datasource.storage.report.can.migrate", ui.getConfig().getModelVersion());
        } else {
            label = t("observe.ui.datasource.storage.report.can.not.migrate");
        }
        ui.getMigrationPolicy().setText(label);
    }
}
