/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

.dataSourceCreateMode {
  buttonGroup:"dataSourceCreateMode";
  value:{DataSourceCreateMode.%%};
}

#configContent {
  layout:{configLayout};
  border:{new TitledBorder(t("observe.ui.datasource.storage.config.data.storage") + "     ")};
}

#dataModeConfig {
  layout:{new GridLayout(0,1)};
  border:{new TitledBorder(t("observe.ui.datasource.storage.config.data.mode") + "     ")};
}

#noImportDataConfig {
  enabled: false;
  text:"observe.ui.datasource.storage.noImportData.config";
}

#centralSourceLabel {
  _no:{n("observe.ui.datasource.storage.no.remote.storage")};
}

#centralSourceInfoLabel {
  actionIcon:"information";
  text:"observe.ui.datasource.storage.config.export.required.read.data";
}

#centralSourceServerLabel {
  _no:{n("observe.ui.datasource.storage.no.server.storage")};
}

#centralSourceServerInfoLabel {
  actionIcon:"information";
  text:"observe.ui.datasource.storage.config.export.required.read.data";
}

#dumpFile {
  text:{centralSourceModel.getDumpFilePath()};
}
