package fr.ird.observe.client.datasource.editor.api.wizard.tabs;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.datasource.editor.api.wizard.tabs.actions.ConfigDataUISelectDataSourceCreateModeAction;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import fr.ird.observe.dto.ObserveUtil;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.io.File;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class ConfigDataUIHandler extends StorageTabUIHandler<ConfigDataUI> implements UIHandler<ConfigDataUI> {
    @Override
    public void registerActions(ConfigDataUI ui) {
        ConfigDataUISelectDataSourceCreateModeAction.create(ui, DataSourceCreateMode.EMPTY, 1);
        ConfigDataUISelectDataSourceCreateModeAction.create(ui, DataSourceCreateMode.IMPORT_EXTERNAL_DUMP, 2);
        ConfigDataUISelectDataSourceCreateModeAction.create(ui, DataSourceCreateMode.IMPORT_REMOTE_STORAGE, 3);
        ConfigDataUISelectDataSourceCreateModeAction.create(ui, DataSourceCreateMode.IMPORT_SERVER_STORAGE, 4);
    }

    @Override
    public void afterInit(ConfigDataUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));

            ConfigDataUISelectDataSourceCreateModeAction.refreshConfig(ui, DataSourceCreateMode.EMPTY);
            ui.getCentralSourceModel().addPropertyChangeListener(StorageUIModel.VALID_PROPERTY_NAME, e -> {
                boolean valid = (boolean) e.getNewValue();
                StorageUIModel source = (StorageUIModel) e.getSource();
                onCentralModelValidChanged(source, valid, ui.getCentralSourceLabel(), ui.getCentralSourceServerLabel(),
                                           ui.getCentralSourcePolicy(), ui.getCentralSourceStatus(),
                                           ui.getCentralSourceServerPolicy(), ui.getCentralSourceServerStatus());
            });
        }
    }

    public void setDumpFile(String filePath) {
        File dumpFile = new File(ObserveUtil.addSqlGzExtension(filePath));
        ui.getCentralSourceModel().setDumpFile(dumpFile);
        ui.getModel().validate();
    }
}
