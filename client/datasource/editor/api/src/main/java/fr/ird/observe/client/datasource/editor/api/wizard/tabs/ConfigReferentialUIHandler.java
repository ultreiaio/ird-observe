package fr.ird.observe.client.datasource.editor.api.wizard.tabs;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.datasource.editor.api.wizard.tabs.actions.ConfigReferentialUISelectDataSourceCreateModeAction;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import fr.ird.observe.dto.ObserveUtil;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JComponent;
import java.io.File;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class ConfigReferentialUIHandler extends StorageTabUIHandler<ConfigReferentialUI> implements UIHandler<ConfigReferentialUI> {

    @Override
    public void registerActions(ConfigReferentialUI ui) {
        ConfigReferentialUISelectDataSourceCreateModeAction.create(ui, DataSourceCreateMode.EMPTY, 1);
        ConfigReferentialUISelectDataSourceCreateModeAction.create(ui, DataSourceCreateMode.IMPORT_EXTERNAL_DUMP, 2);
        ConfigReferentialUISelectDataSourceCreateModeAction.create(ui, DataSourceCreateMode.IMPORT_REMOTE_STORAGE, 3);
        ConfigReferentialUISelectDataSourceCreateModeAction.create(ui, DataSourceCreateMode.IMPORT_SERVER_STORAGE, 4);
    }

    @Override
    public void afterInit(ConfigReferentialUI ui) {

        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));
            ConfigReferentialUISelectDataSourceCreateModeAction.refreshConfig(ui, DataSourceCreateMode.IMPORT_EXTERNAL_DUMP);
        }
        ui.getCentralSourceModel().addPropertyChangeListener(StorageUIModel.VALID_PROPERTY_NAME, e -> {
            boolean valid = (boolean) e.getNewValue();
            StorageUIModel source = (StorageUIModel) e.getSource();
            onCentralModelValidChanged(source, valid, ui.getCentralSourceLabel(), ui.getCentralSourceServerLabel(),
                                       ui.getCentralSourcePolicy(), ui.getCentralSourceStatus(),
                                       ui.getCentralSourceServerPolicy(), ui.getCentralSourceServerStatus());
        });
    }

    public void setDumpFile(String filePath) {
        File dumpFile = new File(ObserveUtil.addSqlGzExtension(filePath));
        ui.getCentralSourceModel().setDumpFile(dumpFile);
        ui.getModel().validate();
    }

    private void refreshConfig(String configId) {
        JComponent c = (JComponent) ui.getObjectById(configId);
        if (c != null) {
            ui.configLayout.show(ui.configContent, configId);
        }
    }
}
