<!--
  #%L
  ObServe Client :: DataSource :: Editor :: API
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<StorageTabUI>

  <import>
    fr.ird.observe.client.datasource.editor.api.wizard.StorageStep
    fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel
    fr.ird.observe.datasource.configuration.DataSourceConnectMode
    fr.ird.observe.datasource.configuration.DataSourceCreateMode

    java.io.File
    org.jdesktop.swingx.JXTitledPanel
  </import>

  <StorageStep id='step' initializer='StorageStep.CONFIG'/>

  <StorageUIModel id='model'/>

  <CardLayout id='configLayout'/>

  <JPanel id='content' layout='{new BorderLayout()}'>
    <JPanel id='configContent' layout='{configLayout}' constraints='BorderLayout.CENTER'>

      <JXTitledPanel id='IMPORT_EXTERNAL_DUMP' constraints='"IMPORT_EXTERNAL_DUMP"' styleClass="createMode">
        <Table id='IMPORT_EXTERNAL_DUMP_content'>
          <row weighty='1'>
            <cell weightx='1' fill="horizontal">
              <JTextField id="dumpFile" onKeyReleased='handler.setDumpFile(((JTextField)event.getSource()).getText())'/>
            </cell>
            <cell anchor="east">
              <JButton id="chooseDumpFile"/>
            </cell>
          </row>
        </Table>
      </JXTitledPanel>
      <JXTitledPanel id='IMPORT_SERVER_STORAGE' constraints='"IMPORT_SERVER_STORAGE"' styleClass="createMode"/>

      <JXTitledPanel id='IMPORT_REMOTE_STORAGE' constraints='"IMPORT_REMOTE_STORAGE"' styleClass="createMode"/>

      <JXTitledPanel id='SERVER' constraints='"SERVER"' styleClass="connectMode"/>

      <JXTitledPanel id='REMOTE' constraints='"REMOTE"' styleClass="connectMode"/>
    </JPanel>
  </JPanel>

</StorageTabUI>
