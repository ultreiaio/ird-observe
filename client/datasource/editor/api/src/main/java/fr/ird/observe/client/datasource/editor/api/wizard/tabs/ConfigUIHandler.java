package fr.ird.observe.client.datasource.editor.api.wizard.tabs;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitModel;
import fr.ird.observe.client.datasource.config.RemoteConfig;
import fr.ird.observe.client.datasource.config.ServerConfig;
import fr.ird.observe.client.datasource.config.form.ConfigurationModel;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageStep;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUI;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.dto.ObserveUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class ConfigUIHandler extends StorageTabUIHandler<ConfigUI> implements UIHandler<ConfigUI> {

    private static final Logger log = LogManager.getLogger(ConfigUIHandler.class);
    private final PropertyChangeListener onUsePreset;
    private final PropertyChangeListener onModeChanged;
    private RemoteConfig remoteConfig;
    private ServerConfig serverConfig;

    public ConfigUIHandler() {
        onUsePreset = evt -> {
            boolean testConnexion = (boolean) evt.getNewValue();
            if (testConnexion) {
                StorageUI parentContainer = Objects.requireNonNull(ui.getParentContainer(StorageUI.class));

                StorageStep nextStep = parentContainer.getModel().getNextStep();
                if (nextStep != null) {
                    // go to next step
                    parentContainer.getNextStep().doClick();
                    if (parentContainer.getModel().getNextStep() == null) {
                        // last step, apply
                        parentContainer.getApply().doClick();
                    }
                } else if (getMainUI() != null) {
                    parentContainer.getApply().doClick();
                }
            }
        };
        onModeChanged = evt -> {
            DataSourceInitModel model = (DataSourceInitModel) evt.getSource();
            String id = null;
            if (model.isOnConnectMode()) {
                if (model.isOnConnectModeRemote()) {
                    id = DataSourceConnectMode.REMOTE.name();
                } else if (model.isOnConnectModeServer()) {
                    id = DataSourceConnectMode.SERVER.name();
                }
            } else if (model.isOnCreateMode()) {
                id = model.getCreateMode().name();
            }

            if (id != null) {
                refreshConfig(ui, id);
            }
        };
    }

    @Override
    public void afterInit(ConfigUI ui) {
        StorageUIModel storageUIModel = ui.getContextValue(StorageUIModel.class);
        ChooseDbModel chooseDb = storageUIModel.getChooseDb();
        remoteConfig = new RemoteConfig(UIHelper.initialContext(ui, storageUIModel.getRemoteConfig()));
        serverConfig = new ServerConfig(UIHelper.initialContext(ui, storageUIModel.getServerConfig()));

        chooseDb.getInitModel().addPropertyChangeListener(DataSourceInitModel.INIT_MODE_PROPERTY_NAME, onModeChanged);
        chooseDb.getInitModel().addPropertyChangeListener(DataSourceInitModel.CONNECT_MODE_PROPERTY_NAME, onModeChanged);
        chooseDb.getInitModel().addPropertyChangeListener(DataSourceInitModel.CREATE_MODE_PROPERTY_NAME, onModeChanged);

        remoteConfig.getModel().addPropertyChangeListener(ConfigurationModel.USE_PRESET_PROPERTY_NAME, onUsePreset);
        serverConfig.getModel().addPropertyChangeListener(ConfigurationModel.USE_PRESET_PROPERTY_NAME, onUsePreset);
    }

    public void setDumpFile(String filePath) {
        if (filePath.isEmpty()) {
            return;
        }
        File dumpFile = new File(filePath);
        if (!dumpFile.isDirectory()) {
            if (!ObserveUtil.withSqlGzExtension(filePath)) {
                filePath = ObserveUtil.addSqlGzExtension(filePath);
            }
            dumpFile = new File(filePath);
        }
        ui.getModel().setDumpFile(dumpFile);
        ui.getModel().validate();
    }

    private void refreshConfig(ConfigUI ui, String configId) {
        JComponent c = (JComponent) ui.getObjectById(configId);
        if (c != null) {
            log.info("Refresh config: " + configId);
            if (c.equals(ui.IMPORT_REMOTE_STORAGE)) {
                ui.IMPORT_REMOTE_STORAGE.setContentContainer(remoteConfig);
                ui.IMPORT_REMOTE_STORAGE.setRightDecoration(remoteConfig.getToolbar());
            } else if (c.equals(ui.IMPORT_SERVER_STORAGE)) {
                ui.IMPORT_SERVER_STORAGE.setContentContainer(serverConfig);
                ui.IMPORT_SERVER_STORAGE.setRightDecoration(serverConfig.getToolbar());
            } else if (c.equals(ui.REMOTE)) {
                ui.REMOTE.setContentContainer(remoteConfig);
                ui.REMOTE.setRightDecoration(remoteConfig.getToolbar());
            } else if (c.equals(ui.SERVER)) {
                ui.SERVER.setContentContainer(serverConfig);
                ui.SERVER.setRightDecoration(serverConfig.getToolbar());
            }
            ui.configLayout.show(ui.configContent, configId);
            String text = (String) c.getClientProperty("description");
            ui.setDescriptionText(t(text));
            SwingUtilities.invokeLater(ui::invalidate);
            SwingUtilities.invokeLater(ui::validate);
        }
    }

    public JComponent getFocusOwner(DataSourceInitModel initModel) {
        JComponent focusOwner;
        if (initModel.isOnCreateMode()) {
            switch (initModel.getCreateMode()) {
                case IMPORT_EXTERNAL_DUMP:
                    focusOwner = ui.getChooseDumpFile();
                    break;
                case IMPORT_REMOTE_STORAGE:
                    focusOwner = remoteConfig.getToggleConfigurations();
                    break;
                case IMPORT_SERVER_STORAGE:
                    focusOwner = serverConfig.getToggleConfigurations();
                    break;
                default:
                    focusOwner = null;
            }
        } else {
            switch (initModel.getConnectMode()) {
                case REMOTE:
                    focusOwner = remoteConfig.getToggleConfigurations();
                    break;
                case SERVER:
                    focusOwner = serverConfig.getToggleConfigurations();
                    break;
                default:
                    focusOwner = null;
            }
        }
        return focusOwner;
    }
}
