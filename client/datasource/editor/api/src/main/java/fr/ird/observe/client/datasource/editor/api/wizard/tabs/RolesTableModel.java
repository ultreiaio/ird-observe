/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.wizard.tabs;


import fr.ird.observe.datasource.security.model.DataSourceSecurityModel;
import fr.ird.observe.datasource.security.model.DataSourceUserDto;
import fr.ird.observe.datasource.security.model.DataSourceUserRole;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Roles model used to select roles to affect to each database user.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class RolesTableModel extends AbstractTableModel {

    public static final String[] COLUMN_NAMES = {
            n("observe.security.role"),
            n("observe.security.role.unused"),
            n("observe.security.role.referential"),
            n("observe.security.role.user"),
            n("observe.security.role.dataEntryOperator"),
            n("observe.security.role.technical"),
            n("observe.security.role.administrator")
    };
    public static final String[] COLUMN_NAME_TIPS = {
            n("observe.security.role.tip"),
            n("observe.security.role.unused.tip"),
            n("observe.security.role.referential.tip"),
            n("observe.security.role.user.tip"),
            n("observe.security.role.dataEntryOperator.tip"),
            n("observe.security.role.technical.tip")
    };
    protected static final Class<?>[] COLUMN_CLASSES = {
            String.class,
            Boolean.class,
            Boolean.class,
            Boolean.class,
            Boolean.class,
            Boolean.class
    };
    private static final long serialVersionUID = 1L;
    protected DataSourceSecurityModel model;
    protected List<DataSourceUserDto> roles;

    /**
     * Init the table model from security model.
     *
     * @param model security model
     */
    public void init(DataSourceSecurityModel model) {
        this.model = model;
        this.roles = new ArrayList<>(model.getUsersWithoutAdministrator());
        Collections.sort(roles);
        for (DataSourceUserDto role : roles) {
            String name = role.getName();
            if (name.equals("referentiel") || name.equals("referential") || name.endsWith("-referentiel") || name.endsWith("-referential")) {
                role.setRole(DataSourceUserRole.REFERENTIAL);
            } else if (name.equals("technicien") || name.equals("technician") || name.endsWith("-technicien") || name.endsWith("-technician")) {
                role.setRole(DataSourceUserRole.TECHNICAL);
            } else if (name.equals("utilisateur") || name.equals("user") || name.endsWith("-utilisateur") || name.endsWith("-user")) {
                role.setRole(DataSourceUserRole.USER);
            } else if (name.equals("saisisseur") || name.equals("dataEntryOperator") || name.endsWith("-saisisseur") || name.endsWith("-dataEntryOperator")) {
                role.setRole(DataSourceUserRole.DATA_ENTRY_OPERATOR);
            }
        }
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        // only roles are editable (from column 1)
        return columnIndex > 0;
    }

    public void clear() {
        model = null;
        roles = null;
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return roles == null ? 0 : roles.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_CLASSES.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;

        DataSourceUserDto user = getUser(rowIndex);
        if (user != null) {

            switch (columnIndex) {
                case 0:
                    value = user.getName();
                    break;
                case 1:
                    value = DataSourceUserRole.UNUSED.equals(user.getRole());
                    break;
                case 2:
                    value = DataSourceUserRole.REFERENTIAL.equals(user.getRole());
                    break;
                case 3:
                    value = DataSourceUserRole.USER.equals(user.getRole());
                    break;
                case 4:
                    value = DataSourceUserRole.DATA_ENTRY_OPERATOR.equals(user.getRole());
                    break;
                case 5:
                    value = DataSourceUserRole.TECHNICAL.equals(user.getRole());
                    break;
                default:
                    throw new IllegalStateException(String.format("can not get value for row %d, col %d", rowIndex, columnIndex));
            }
        }
        return value;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            // not possible
            return;
        }

        Boolean value = (Boolean) aValue;
        DataSourceUserDto userDto = getUser(rowIndex);

        DataSourceUserRole role;

        if (!value) {
            role = null;
        } else {
            switch (columnIndex) {
                case 2:
                    role = DataSourceUserRole.REFERENTIAL;
                    break;
                case 3:
                    role = DataSourceUserRole.USER;
                    break;
                case 4:
                    role = DataSourceUserRole.DATA_ENTRY_OPERATOR;
                    break;
                case 5:
                    role = DataSourceUserRole.TECHNICAL;
                    break;
                default:
                    role = DataSourceUserRole.UNUSED;
            }
        }

        model.setRole(userDto, role, true);

        // notify change
        fireTableRowsUpdated(rowIndex, rowIndex);
    }

    protected DataSourceUserDto getUser(int rowIndex) {
        return roles == null ? null : roles.get(rowIndex);
    }

}
