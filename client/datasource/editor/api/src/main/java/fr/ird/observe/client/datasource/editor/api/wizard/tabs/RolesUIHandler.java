package fr.ird.observe.client.datasource.editor.api.wizard.tabs;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.datasource.security.model.DataSourceSecurityModel;
import fr.ird.observe.datasource.security.model.DataSourceUserDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.editor.MyDefaultCellEditor;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class RolesUIHandler extends StorageTabUIHandler<RolesUI> implements UIHandler<RolesUI> {

    private static final Logger log = LogManager.getLogger(RolesUIHandler.class);

    public static void updateSecurity(WithClientUIContextApi context, StorageUIModel model, RolesTableModel roleModel) {
        DataSourceSecurityModel security = model.getSecurityModel();
        ObserveSwingDataSource dataSource = null;
        switch (model.getChooseDb().getInitModel().getConnectMode()) {
            case REMOTE:
                ObserveDataSourceConfigurationTopiaPG pgConfig = model.getRemoteConfig().getConfiguration();
                dataSource = context.getDataSourcesManager().newDataSource(pgConfig);
                break;
            case SERVER:
                ObserveDataSourceConfigurationRest restConfig = model.getServerConfig().getConfiguration();
                dataSource = context.getDataSourcesManager().newDataSource(restConfig);
                break;
        }
        if (dataSource != null) {
            try {
                Set<DataSourceUserDto> users = dataSource.getUsers();
                log.info(String.format("Db roles : %s", users));
                security.init(users);
                roleModel.init(security);

            } catch (Exception e) {
                throw new RuntimeException("Could not obtain db roles", e);
            } finally {
                if (dataSource.isOpen()) {
                    dataSource.close();
                }
            }
        }
    }

    @Override
    public void afterInit(RolesUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));
        }
        ui.getSecurityModel().addPropertyChangeListener(evt -> {
            log.debug(String.format("Security model changed [%s] <%s : %s>", evt.getPropertyName(), evt.getOldValue(), evt.getNewValue()));
            ui.getModel().validate();
        });

        JTable table = ui.getRoles();
        table.setRowHeight(22);

        UIHelper.fixTableColumnWidth(table, 1, 100);
        UIHelper.fixTableColumnWidth(table, 2, 100);
        UIHelper.fixTableColumnWidth(table, 3, 100);
        UIHelper.fixTableColumnWidth(table, 4, 150);
        UIHelper.fixTableColumnWidth(table, 5, 100);

        TableCellRenderer renderer = table.getDefaultRenderer(Object.class);

        UIHelper.setI18nTableHeaderRenderer(
                table,
                RolesTableModel.COLUMN_NAMES[0],
                RolesTableModel.COLUMN_NAME_TIPS[0],
                RolesTableModel.COLUMN_NAMES[1],
                RolesTableModel.COLUMN_NAME_TIPS[1],
                RolesTableModel.COLUMN_NAMES[2],
                RolesTableModel.COLUMN_NAME_TIPS[2],
                RolesTableModel.COLUMN_NAMES[3],
                RolesTableModel.COLUMN_NAME_TIPS[3],
                RolesTableModel.COLUMN_NAMES[4],
                RolesTableModel.COLUMN_NAME_TIPS[4],
                RolesTableModel.COLUMN_NAMES[5],
                RolesTableModel.COLUMN_NAME_TIPS[5]
        );

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newStringTableCellRenderer(renderer, 50, true));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 5, UIHelper.newBooleanTableCellRenderer(renderer));

        UIHelper.setTableColumnEditor(table, 1, MyDefaultCellEditor.newBooleanEditor());
        UIHelper.setTableColumnEditor(table, 2, MyDefaultCellEditor.newBooleanEditor());
        UIHelper.setTableColumnEditor(table, 3, MyDefaultCellEditor.newBooleanEditor());
        UIHelper.setTableColumnEditor(table, 4, MyDefaultCellEditor.newBooleanEditor());
        UIHelper.setTableColumnEditor(table, 5, MyDefaultCellEditor.newBooleanEditor());

    }

}
