package fr.ird.observe.client.datasource.editor.api.wizard.tabs;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUI;
import fr.ird.observe.client.datasource.editor.api.config.TreeConfigUIHandler;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePane;
import fr.ird.observe.client.datasource.editor.api.selection.SelectionTreePaneHandler;
import fr.ird.observe.client.datasource.editor.api.selection.actions.SelectUnselect;
import fr.ird.observe.client.datasource.editor.api.wizard.ObstunaAdminAction;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.client.datasource.editor.api.wizard.launchers.CreateLauncher;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.client.util.ObserveSwingTechnicalException;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.navigation.tree.selection.SelectionTree;
import fr.ird.observe.navigation.tree.selection.SelectionTreeConfig;
import fr.ird.observe.navigation.tree.selection.SelectionTreeModel;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.SwingUtilities;
import java.util.List;
import java.util.function.Consumer;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class SelectDataUIHandler extends StorageTabUIHandler<SelectDataUI> implements UIHandler<SelectDataUI> {

    public static void loadSelectData(WithClientUIContextApi context, SelectDataUI ui) {
        StorageUIModel model = ui.getModel();
        // To avoid multiple init
        boolean init = ui.getClientProperty("__init") == null;
        if (init) {
            ui.putClientProperty("__init", true);
        }
        boolean createDataSource = ObstunaAdminAction.CREATE == model.getAdminAction();
        try {
            SelectionTreePane treePane = ui.getSelectedTreePane();
            SelectionTree tree = treePane.getTree();
            SelectionTreeModel treeModel = tree.getModel();
            if (init) {
                Project navigationEditModel = context.getClientConfig().getNavigationEditModel();
                if (navigationEditModel != null) {
                    List<String> openIds = navigationEditModel.getIds();
                    treeModel.setEditIds(openIds);
                }
            }
            if (createDataSource) {
                // create temporary data source
                try (ObserveSwingDataSource dataSource = CreateLauncher.toImportDataSourceConfig(model)) {
                    SelectionTreePaneHandler.initDataSource(treePane, dataSource);
                    dataSource.open();
                    if (init) {
                        doInit(treeModel.getConfig(), dataSource);
                    }
                    treeModel.populate(dataSource.getNavigationService()::loadSelectionRoot);
                }
            } else {
                // use main data source
                ObserveSwingDataSource dataSource = context.getDataSourcesManager().getMainDataSource();
                SelectionTreePaneHandler.initDataSource(treePane, dataSource);
                if (init) {
                    doInit(treeModel.getConfig(), dataSource);
                }
                treeModel.populate(dataSource.getNavigationService()::loadSelectionRoot);
            }
            SelectionTreePaneHandler.updateStatistics(treePane);
            boolean selectAll = model.isSelectAll();
            if (selectAll) {
                treeModel.selectAll();
                SwingUtilities.invokeLater(tree::expandAll);
            }
            if (init) {
                model.setSelectDataModel(treeModel);
            }
            if (tree.getRowCount() > 0) {
                tree.setSelectionRow(0);
            }
            SwingUtilities.invokeLater(tree::requestFocusInWindow);
        } catch (Exception e) {
            throw new ObserveSwingTechnicalException("Can't reload selection tree", e);
        }
    }

    public static void doInit(SelectionTreeConfig modelConfig, ObserveSwingDataSource dataSource) {
        SelectionTreeConfig selectionConfig = dataSource.newSelectionTreeConfig();
        // do not load referential
        selectionConfig.setLoadReferential(false);
        // do not show empty group by
        selectionConfig.setLoadEmptyGroupBy(false);
        // do load data
        selectionConfig.setLoadData(true);
        // use open data (if any)
        selectionConfig.setUseOpenData(true);
        // need to see all data
        selectionConfig.setLoadNullGroupBy(true);
        // need to see all data
        selectionConfig.setLoadDisabledGroupBy(true);
        modelConfig.init(selectionConfig);
    }

    @Override
    public void registerActions(SelectDataUI ui) {
        SelectUnselect.installUI(ui.getSelectedTreePane());
        Consumer<TreeConfigUI> consumer = TreeConfigUIHandler::hideOptions;
        Consumer<TreeConfigUI> apply = u -> loadSelectData(this, ui);
        SelectionTreePaneHandler.init(ui.getSelectedTreePane(), consumer, apply);
    }

    @Override
    public void afterInit(SelectDataUI ui) {
        if (ui.getStep() != null) {
            String description = t(ui.getStep().getDescription());
            description = ObserveKeyStrokesSupport.suffixTextWithKeyStroke(description, ObserveKeyStrokesSupport.KEY_STROKE_ENTER);
            ui.setDescriptionText(description);
        }
    }
}
