<!--
  #%L
  ObServe Client :: DataSource :: Editor :: API
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<JPanel id="tabPanel" abstract='true'
        implements='org.nuiton.jaxx.runtime.swing.wizard.WizardStepUI&lt;StorageStep, StorageUIModel&gt;'>

  <import>
    fr.ird.observe.client.util.UIHelper
    fr.ird.observe.client.datasource.editor.api.wizard.StorageStep
    fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel

    java.awt.Color

    javax.swing.JLabel
    javax.swing.border.LineBorder

    static io.ultreia.java4all.i18n.I18n.t
    static io.ultreia.java4all.i18n.I18n.n
  </import>

  <StorageUIModel id='model' initializer='getContextValue(StorageUIModel.class)'/>

  <script><![CDATA[

public abstract StorageStep getStep();

public void destroy() {
    description.setText("");
    UIHelper.destroy(this);
}

protected void setDescriptionText(String text) {
    description.setText(text);
}

protected String getProgressString(int currentStep, int nbStep) {
    return StorageTabUIHandler.getProgressString(currentStep, nbStep, getStep());
}
]]>
  </script>

  <!-- titre -->
  <JPanel layout='{new BorderLayout()}' constraints='BorderLayout.NORTH'>
    <JProgressBar id='progress' constraints='BorderLayout.CENTER'/>
  </JPanel>

  <!-- content -->
  <JPanel id='content' constraints='BorderLayout.CENTER'/>

  <!-- description -->
  <JScrollPane id="descriptionPane" constraints='BorderLayout.SOUTH'>
    <JTextArea id='description'/>
  </JScrollPane>

</JPanel>
