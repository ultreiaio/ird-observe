/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.api.wizard.tabs;

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageStep;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUIModel;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformationTemplate;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.Icon;
import javax.swing.JLabel;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Le controleur commun à tous les onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class StorageTabUIHandler<U extends JAXXObject> implements WithClientUIContextApi {

    protected U ui;

    protected static String getProgressString(int currentStep, int nbStep, StorageStep step) {
        String txt = "";
        if (step != null) {
            txt = n("observe.ui.datasource.storage.step.label");
            txt = t(txt, currentStep + 1, nbStep, step.getLabel());
        }
        return txt;
    }

    void onCentralModelValidChanged(StorageUIModel source, boolean valid, JLabel centralSourceLabel, JLabel centralSourceServerLabel, JLabel centralSourcePolicy, JLabel centralSourceStatus, JLabel centralSourceServerPolicy, JLabel centralSourceServerStatus) {
        Icon validIcon = source.getValidIcon(valid);
        centralSourceLabel.setText(updateStorageLabel(source, valid, centralSourceLabel, true));
        centralSourcePolicy.setText(updateDataSourcePolicy(source, valid/*, true*/));
        centralSourceStatus.setIcon(validIcon);
        centralSourceServerLabel.setText(updateStorageLabel(source, valid, centralSourceServerLabel, false));
        centralSourceServerPolicy.setText(updateDataSourcePolicy(source, valid/*, false*/));
        centralSourceServerStatus.setIcon(validIcon);
    }

    String updateStorageLabel(StorageUIModel service, boolean serviceValid, JLabel label, boolean remote) {
        String text;
        if (serviceValid && remote == service.getChooseDb().getInitModel().isOnConnectModeRemote()) {
            // on récupère le label du service
            text = service.getLabelWithUrl();
        } else {
            // aucun service configuré
            text = t((String) label.getClientProperty("no"));
        }
        return text;
    }

    String updateDataSourcePolicy(StorageUIModel sourceModel, boolean valid/*, boolean remote*/) {
        String text = null;
        //FIXME Can't find out what is was design for
//        if (valid && remote == sourceModel.getChooseDb().getInitModel().isOnConnectModeRemote()) {
        if (valid) {
            ObserveDataSourceInformation dataSourceInformation = sourceModel.getDataSourceInformation();
            if (dataSourceInformation != null) {
                text = ObserveDataSourceInformationTemplate.generate(dataSourceInformation);
            }
        } else {
            text = t("observe.Common.storage.not.valid");
        }
        return text;
    }

    public final void beforeInit(U ui) {
        this.ui = ui;
    }
}
