package fr.ird.observe.client.datasource.editor.api.wizard.tabs.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.config.ChooseDbModel;
import fr.ird.observe.client.datasource.config.DataSourceInitMode;
import fr.ird.observe.client.datasource.editor.api.wizard.StorageUI;
import fr.ird.observe.client.datasource.editor.api.wizard.tabs.ChooseDbModeUI;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;

import javax.swing.AbstractButton;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

/**
 * Created by tchemit on 17/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ChooseDbModeUISelectRadioButtonAction extends StorageTabUIActionSupport<ChooseDbModeUI> {

    public static final String ACTION_NAME = ChooseDbModeUISelectRadioButtonAction.class.getName();
    private final StorageUI ui;
    private final AbstractButton editor;

    public ChooseDbModeUISelectRadioButtonAction(StorageUI ui, AbstractButton editor, int position, String keystrokePrefix) {
        super(ACTION_NAME + keystrokePrefix + position, editor.getText(), editor.getToolTipText(), null, KeyStroke.getKeyStroke(keystrokePrefix + (position)));
        this.ui = ui;
        this.editor = editor;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ChooseDbModeUI configUI) {

        if (!editor.isShowing()) {
            return;
        }
        editor.setSelected(true);

        Object value = editor.getClientProperty("$value");
        DataSourceInitMode initMode = (DataSourceInitMode) editor.getClientProperty("initMode");

        ChooseDbModel chooseDb = ui.getModel().getChooseDb();
        if (chooseDb.getInitModel().getInitMode() != initMode) {
            // switch to this init mode
            chooseDb.setInitMode(initMode);
        }
        switch (chooseDb.getInitModel().getInitMode()) {

            case CONNECT:
                chooseDb.setConnectMode((DataSourceConnectMode) value);
                break;
            case CREATE:
                chooseDb.setCreateMode((DataSourceCreateMode) value);
                break;
        }
        Boolean changeStep = (Boolean) editor.getClientProperty("changeStep");
        if (changeStep != null && changeStep) {
            SwingUtilities.invokeLater(ui.getNextStep()::doClick);
            return;
        }
        Boolean apply = (Boolean) editor.getClientProperty("apply");
        if (apply != null && apply) {
            SwingUtilities.invokeLater(() -> {
                if (ui.getModel().getNextStep() != null) {
                    ui.getNextStep().doClick();
                }
                ui.getApply().doClick();
            });
        }
    }
}
