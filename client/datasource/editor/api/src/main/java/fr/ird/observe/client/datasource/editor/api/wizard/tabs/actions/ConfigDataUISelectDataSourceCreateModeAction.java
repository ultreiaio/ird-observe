package fr.ird.observe.client.datasource.editor.api.wizard.tabs.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.wizard.tabs.ConfigDataUI;
import fr.ird.observe.datasource.configuration.DataSourceCreateMode;
import io.ultreia.java4all.i18n.I18n;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.JRadioButton;
import java.awt.event.ActionEvent;

/**
 * Created on 19/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ConfigDataUISelectDataSourceCreateModeAction extends StorageTabUIActionSupport<ConfigDataUI> {

    private final DataSourceCreateMode createMode;

    public static ConfigDataUISelectDataSourceCreateModeAction create(ConfigDataUI ui, DataSourceCreateMode createMode, int order) {
        String text = createMode == DataSourceCreateMode.EMPTY ? I18n.n("observe.ui.datasource.storage.no.data.import") : createMode.getLabel();
        ConfigDataUISelectDataSourceCreateModeAction action = new ConfigDataUISelectDataSourceCreateModeAction(createMode, text, order);
        JRadioButton editor = (JRadioButton) ui.getObjectById(createMode.name());
        init(ui, editor, action);
        return action;
    }

    public static void refreshConfig(ConfigDataUI ui, DataSourceCreateMode createMode) {
        ui.getChooseDbModel().setDataImportMode(createMode);
        JAXXButtonGroup buttonGroup = ui.getDataSourceCreateMode();
        buttonGroup.setSelectedValue(createMode);
        buttonGroup.getButton(createMode).setSelected(true);
        String configId = createMode.name();
        ui.getConfigLayout().show(ui.getConfigContent(), configId);
    }

    public ConfigDataUISelectDataSourceCreateModeAction(DataSourceCreateMode createMode, String text, int order) {
        super(ConfigDataUISelectDataSourceCreateModeAction.class.getName() + createMode.name(), text, text, null, ObserveKeyStrokesEditorApi.getFunctionKeyStroke(order));
        this.createMode = createMode;
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ConfigDataUI ui) {
        refreshConfig(ui, createMode);
    }
}
