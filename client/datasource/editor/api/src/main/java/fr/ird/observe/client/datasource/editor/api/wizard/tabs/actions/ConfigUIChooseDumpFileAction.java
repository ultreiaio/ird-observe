package fr.ird.observe.client.datasource.editor.api.wizard.tabs.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: API
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.wizard.tabs.ConfigUI;
import fr.ird.observe.client.util.UIFileHelper;
import fr.ird.observe.dto.ObserveUtil;

import java.awt.event.ActionEvent;
import java.io.File;

import static io.ultreia.java4all.i18n.I18n.t;

public class ConfigUIChooseDumpFileAction extends StorageTabUIActionSupport<ConfigUI> {

    public ConfigUIChooseDumpFileAction() {
        super("", t("observe.ui.action.choose.db.dump"), "fileChooser", ObserveKeyStrokesEditorApi.KEY_STROKE_STORAGE_DO_CHOOSE_FILE);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, ConfigUI ui) {
        File f = UIFileHelper.chooseFile(ui,
                                         t("observe.ui.title.choose.db.dump"),
                                         t("observe.ui.action.choose.db.dump"),
                                         ui.getModel().getDumpFile(),
                                         ObserveUtil.SQL_GZ_EXTENSION,
                                         ObserveUtil.SQL_GZ_EXTENSION_PATTERN,
                                         t("observe.ui.action.choose.db.dump.description"));
        if (f != null) {
            ui.getHandler().setDumpFile(f.getAbsolutePath());
        }
    }
}
