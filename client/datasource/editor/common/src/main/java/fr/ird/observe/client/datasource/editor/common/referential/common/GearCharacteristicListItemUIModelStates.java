package fr.ird.observe.client.datasource.editor.common.referential.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: Common
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModelStates;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.GearCharacteristicListItemDto;
import fr.ird.observe.dto.referential.common.GearCharacteristicListItemReference;
import fr.ird.observe.dto.referential.common.GearCharacteristicReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;

/**
 * Created on 25/7/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class GearCharacteristicListItemUIModelStates extends ContentReferentialUIModelStates<GearCharacteristicListItemDto, GearCharacteristicListItemReference> {

    public GearCharacteristicListItemUIModelStates(GearCharacteristicListItemUIModel model) {
        super(model, GearCharacteristicListItemDto.newDto(new java.util.Date()));
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(GearCharacteristicListItemDto.PROPERTY_GEAR_CHARACTERISTIC, ReferencesFilterHelper.<GearCharacteristicReference>newPredicateList(r -> r.getGearCharacteristicType().isList()));
    }

    @Override
    public GearCharacteristicListItemReference toReference(GearCharacteristicListItemDto bean, ReferentialLocale referentialLocale) {
        return bean.toReference(referentialLocale);
    }

}
