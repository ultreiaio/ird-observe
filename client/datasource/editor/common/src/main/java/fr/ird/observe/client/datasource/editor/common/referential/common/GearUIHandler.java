package fr.ird.observe.client.datasource.editor.common.referential.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: Common
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModelStates;
import fr.ird.observe.dto.referential.common.GearCharacteristicReference;
import fr.ird.observe.dto.referential.common.GearDto;
import fr.ird.observe.dto.referential.common.GearReference;
import io.ultreia.java4all.jaxx.widgets.list.DoubleList;
import io.ultreia.java4all.jaxx.widgets.list.DoubleListModel;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.model.JaxxDefaultListModel;
import org.nuiton.jaxx.runtime.swing.model.JaxxFilterableListModel;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

class GearUIHandler extends ContentReferentialUIHandler<GearDto, GearReference, GearUI> implements UIHandler<GearUI> {

    private final PropertyChangeListener onAllowedGearCharacteristicChanged;

    @SuppressWarnings("unchecked")
    GearUIHandler() {
        this.onAllowedGearCharacteristicChanged = evt -> onAllowedGearCharacteristicChanged((List<GearCharacteristicReference>) evt.getNewValue());
    }

    private boolean initDefaultCharacteristicUniverse;

    @Override
    public void onMainTabChanged(int previousIndex, int selectedIndex) {
        if (previousIndex < 2 && selectedIndex == 2) {
            if (getModel().getStates().isReadingMode() || initDefaultCharacteristicUniverse) {
                return;
            }
            // init if required to default characteristics universe
            GearDto bean = getModel().getStates().getBean();
            onAllowedGearCharacteristicChanged(bean.getAllowedGearCharacteristic());
            initDefaultCharacteristicUniverse = true;
        }
    }

    @Override
    public void startEditUI() {
        GearDto bean = getModel().getStates().getBean();
        DoubleListModel<GearCharacteristicReference> gearCharacteristicDoubleListModel = getUi().getAllowedGearCharacteristic().getModel();
        if (!getModel().getStates().isReadingMode()) {
            gearCharacteristicDoubleListModel.removePropertyChangeListener(DoubleListModel.PROPERTY_SELECTED, onAllowedGearCharacteristicChanged);
        }
//        bean.removePropertyChangeListener(GearDto.PROPERTY_ALLOWED_GEAR_CHARACTERISTIC, onAllowedGearCharacteristicChanged);
        super.startEditUI();
        onAllowedGearCharacteristicChanged(bean.getAllowedGearCharacteristic());
        if (!getModel().getStates().isReadingMode()) {
            gearCharacteristicDoubleListModel.addPropertyChangeListener(DoubleListModel.PROPERTY_SELECTED, onAllowedGearCharacteristicChanged);
        }
//        bean.addPropertyChangeListener(GearDto.PROPERTY_ALLOWED_GEAR_CHARACTERISTIC, onAllowedGearCharacteristicChanged);
    }

    @Override
    public void stopEditUI() {
        DoubleListModel<GearCharacteristicReference> gearCharacteristicDoubleListModel = getUi().getAllowedGearCharacteristic().getModel();
        if (!getModel().getStates().isReadingMode()) {
            gearCharacteristicDoubleListModel.removePropertyChangeListener(DoubleListModel.PROPERTY_SELECTED, onAllowedGearCharacteristicChanged);
        }
//        GearDto bean = getModel().getStates().getBean();
//        bean.removePropertyChangeListener(GearDto.PROPERTY_ALLOWED_GEAR_CHARACTERISTIC, onAllowedGearCharacteristicChanged);
        super.stopEditUI();
    }

    void onAllowedGearCharacteristicChanged(List<GearCharacteristicReference> allowCharacteristics) {
        ContentReferentialUIModelStates<GearDto, GearReference> states = getModel().getStates();
        DoubleList<GearCharacteristicReference> defaultGearCharacteristic = getUi().getDefaultGearCharacteristic();
        DoubleListModel<GearCharacteristicReference> defaultGearCharacteristicModel = defaultGearCharacteristic.getModel();
        JaxxFilterableListModel<GearCharacteristicReference> universeModel = defaultGearCharacteristicModel.getUniverseModel();
        if (universeModel.isValueIsAdjusting()) {
            return;
        }
        JaxxDefaultListModel<GearCharacteristicReference> selectedModel = defaultGearCharacteristicModel.getSelectedModel();
        if (selectedModel.isValueIsAdjusting()) {
            return;
        }
        boolean modified = states.isModified();
        List<GearCharacteristicReference> oldSelected = new ArrayList<>(defaultGearCharacteristicModel.getSelected());
        List<GearCharacteristicReference> newSelected = new ArrayList<>(oldSelected);
        newSelected.removeIf(r -> !allowCharacteristics.contains(r));
        defaultGearCharacteristicModel.setUniverse(allowCharacteristics);
        defaultGearCharacteristicModel.setSelected(newSelected);
        if (!states.isReadingMode()) {
            states.setModified(modified);
            if (!modified) {
                getUi().getValidator().setChanged(false);
            }
        }
    }
}
