<!--
  #%L
  ObServe Client :: DataSource :: Editor :: Common
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.editor.api.content.referential.ContentI18nReferentialUI beanScope="bean"
                                                                                          i18n="fr.ird.observe.dto.referential.common.SpeciesDto"
                                                                                          superGenericType='SpeciesDto, SpeciesReference, SpeciesUI'>
  <import>
    fr.ird.observe.dto.referential.common.SpeciesDto
    fr.ird.observe.dto.referential.common.SpeciesReference
    fr.ird.observe.dto.referential.common.SpeciesGroupReference
    fr.ird.observe.dto.referential.common.OceanReference
    fr.ird.observe.dto.referential.common.SizeMeasureTypeReference
    fr.ird.observe.dto.referential.common.WeightMeasureTypeReference

    org.nuiton.jaxx.widgets.number.NumberEditor
    io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox
    io.ultreia.java4all.jaxx.widgets.list.DoubleList
    org.nuiton.jaxx.widgets.text.NormalTextEditor

    static fr.ird.observe.client.util.UIHelper.getStringValue
  </import>
  <BeanValidator id='validator' autoField='true' beanClass='fr.ird.observe.dto.referential.common.SpeciesDto'
                 context='create' errorTableModel='{getErrorTableModel()}'/>
  <SpeciesUIModel id='model' constructorParams='@override:getNavigationSource(this)'/>
  <SpeciesDto id='bean'/>
  <Table id="editView" insets="0" fill="both">
    <row>
      <cell anchor="north" weightx="1" weighty="1">
        <JTabbedPane id='mainTabbedPane'>
          <tab id='generalTab' i18nProperty="">
            <Table fill="both" weightx="1">
              <row>
                <cell>
                  <Table id='editTable' styleClass="caracteristic" addToContainer="true" forceOverride="3">
                    <row>
                      <cell anchor="west">
                        <JLabel id='faoAndWormsIdLabel' styleClass="italic"/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JPanel layout='{new GridLayout()}'>
                          <NormalTextEditor id='faoCode'/>
                          <NumberEditor id='wormsId' styleClass="long10"/>
                        </JPanel>
                      </cell>
                    </row>
                    <row>
                      <cell anchor='west'>
                        <JLabel id='speciesGroupLabel'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <FilterableComboBox id='speciesGroup' genericType='SpeciesGroupReference'/>
                      </cell>
                    </row>
                  </Table>
                </cell>
              </row>
              <row>
                <cell>
                  <Table id='editI18nTable' addToContainer="true" forceOverride="3">
                    <row>
                      <cell anchor='west'>
                        <JLabel id='scientificLabelLabel' styleClass="italic"/>
                      </cell>
                      <cell anchor='east' fill="both" columns="3">
                        <NormalTextEditor id='scientificLabel'/>
                      </cell>
                    </row>
                  </Table>
                </cell>
              </row>
              <row>
                <cell weighty="1">
                  <JLabel styleClass="skipI18n"/>
                </cell>
              </row>
            </Table>
          </tab>
          <tab id='otherTab' i18nProperty="">
            <Table fill="both">
              <row>
                <cell weightx="1">
                  <Table id="editTaillePoids" fill='both' insets="1">
                    <row>
                      <cell anchor='west'>
                        <JLabel id='sizeMeasureTypeLabel'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <FilterableComboBox id='sizeMeasureType' genericType='SizeMeasureTypeReference'/>
                      </cell>
                    </row>
                    <row>
                      <cell anchor='west'>
                        <JLabel id='weightMeasureTypeLabel'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <FilterableComboBox id='weightMeasureType' genericType='WeightMeasureTypeReference'/>
                      </cell>
                    </row>
                    <row>
                      <cell anchor="west">
                        <JLabel id='minLengthMaxLabel'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JPanel layout='{new GridLayout()}'>
                          <NumberEditor id='minLength' styleClass="float2"/>
                          <NumberEditor id='maxLength' styleClass="float2"/>
                        </JPanel>
                      </cell>
                    </row>
                    <row>
                      <cell anchor="west">
                        <JLabel id='minMaxWeightLabel'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JPanel layout='{new GridLayout()}'>
                          <NumberEditor id='minWeight' styleClass="float2"/>
                          <NumberEditor id='maxWeight' styleClass="float2"/>
                        </JPanel>
                      </cell>
                    </row>
                  </Table>
                </cell>
              </row>
              <row>
                <cell weightx="1" weighty="1">
                  <DoubleList id='ocean' genericType='OceanReference'/>
                </cell>
              </row>
            </Table>
          </tab>
        </JTabbedPane>
      </cell>
    </row>
  </Table>
  <JLabel id='codeAndHomeIdLabel' styleClass="i18n"/>
</fr.ird.observe.client.datasource.editor.api.content.referential.ContentI18nReferentialUI>
