<#--
 #%L
 ObServe Client :: DataSource :: Editor :: LL
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<p>
  Pour pouvoir créer une donnée d'observation de type <i>${dtoLabel}</i>, le formulaire marée doit d'abord être complété
  :
</p>
<ul>
  <li>La case <i>Observations</i> doit être cochée</li>
  <li>Le saisisseur des observations doit être sélectionné</li>
</ul>
<hr/>
<br/>
<p>
  Cliquer sur <b>Accéder au formulaire marée</b> pour compléter ces données et enregistrer.
<p>
  <br/>
<p> La case <i>observations</i> sera cochée automatiquement, et vous serez redirigé vers l'onglet
  à renseigner.
</p>
<br/>
<p> Vous pourrez ensuite saisir la donnée souhaitée.</p>
</body>
</html>
