package fr.ird.observe.client.datasource.editor.ll;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationTreeNodeProvider;
import fr.ird.observe.client.datasource.editor.ll.data.common.TripListUINavigationNode;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.spi.module.ObserveBusinessProject;

/**
 * FIXME Generate this
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
@AutoService(RootNavigationTreeNodeProvider.class)
public class LlRootNavigationTreeNodeProvider extends RootNavigationTreeNodeProvider {

    public LlRootNavigationTreeNodeProvider() {
        super(ObserveBusinessProject.get().getLlBusinessModule(), TripListUINavigationNode.class);
    }

    @Override
    public TripListUINavigationNode newChildNode(RootNavigationNode node, DataGroupByDto<?> groupByDto) {
        return TripListUINavigationNode.create(groupByDto, node.getInitializer().getRequest().getGroupByFlavor());
    }
}
