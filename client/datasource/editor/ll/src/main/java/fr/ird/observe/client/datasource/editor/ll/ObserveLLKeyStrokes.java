package fr.ird.observe.client.datasource.editor.ll;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;

import javax.swing.KeyStroke;

/**
 * All keystrokes registered for Longline content.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public class ObserveLLKeyStrokes extends ObserveKeyStrokesEditorApi {
    public static final KeyStroke KEY_STROKE_COPY_SETTING_START_COORDINATE = KeyStroke.getKeyStroke("shift ctrl pressed A");
    public static final KeyStroke KEY_STROKE_COPY_SETTING_END_COORDINATE = KeyStroke.getKeyStroke("shift ctrl pressed B");
    public static final KeyStroke KEY_STROKE_COPY_HAULING_START_COORDINATE = KeyStroke.getKeyStroke("shift ctrl pressed C");

    public static final KeyStroke KEY_STROKE_COPY_FIRST_VALUES_AND_SAVE = KeyStroke.getKeyStroke("pressed F8");
    public static final KeyStroke KEY_STROKE_COPY_LAST_VALUES_AND_SAVE = KeyStroke.getKeyStroke("pressed F9");
    public static final KeyStroke KEY_STROKE_SELECT_TRANSSHIPMENT = KeyStroke.getKeyStroke("alt pressed T");
    public static final KeyStroke KEY_STROKE_RESET_BRANCHLINE = KeyStroke.getKeyStroke("alt pressed Z");
    public static final KeyStroke KEY_STROKE_SAVE_BRANCHLINE = KeyStroke.getKeyStroke("alt pressed G");
    public static final KeyStroke KEY_STROKE_SET_DEFAULT_TIMER_TIME_ON_BOARD = KeyStroke.getKeyStroke("alt pressed T");
    public static final KeyStroke KEY_STROKE_GENERATE_ID = KeyStroke.getKeyStroke("alt pressed G");
}
