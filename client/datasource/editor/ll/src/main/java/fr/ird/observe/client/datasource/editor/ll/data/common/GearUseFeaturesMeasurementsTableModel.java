package fr.ird.observe.client.datasource.editor.ll.data.common;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.GearUseFeaturesMeasurementsTableModelSupport;
import fr.ird.observe.client.util.table.EditableListProperty;
import fr.ird.observe.dto.data.ll.common.GearUseFeaturesDto;
import fr.ird.observe.dto.data.ll.common.GearUseFeaturesMeasurementDto;

import java.util.Date;

/**
 * Created on 4/7/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.16
 */
public class GearUseFeaturesMeasurementsTableModel extends GearUseFeaturesMeasurementsTableModelSupport<GearUseFeaturesMeasurementDto, GearUseFeaturesDto> {

    private static final long serialVersionUID = 1L;

    public static void onInit(TripGearUseFeaturesUI ui) {
        onInit(ui, ui.getMeasurementsScrollPane(), ui.getMeasurementsTable(), ui.getValidatorMeasure());
    }

    public GearUseFeaturesMeasurementsTableModel(EditableListProperty<GearUseFeaturesMeasurementDto> listProperty) {
        super(listProperty);
    }

    @Override
    protected GearUseFeaturesMeasurementDto createNewRow() {
        return GearUseFeaturesMeasurementDto.newDto(new Date());
    }
}
