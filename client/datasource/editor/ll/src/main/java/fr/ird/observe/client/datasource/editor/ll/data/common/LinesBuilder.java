package fr.ird.observe.client.datasource.editor.ll.data.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.map.TripMapContentBuilderSupport;
import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.TripMapPointType;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.locationtech.jts.geom.Coordinate;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * To build longline lines.
 * <p>
 * Created on 14/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.1
 */
public abstract class LinesBuilder {

    private static final Logger log = LogManager.getLogger(LinesBuilder.class);

    public static class LogbookLinesBuilder extends LinesBuilder {

        private static final String LOGBOOK_LINES_LAYER_NAME = "Logbook lines";

        @Override
        public String tripLineId() {
            return "logbookTrip";
        }

        @Override
        public String settingLineId() {
            return "logbookSetting";
        }

        @Override
        public String haulingLineId() {
            return "logbookHauling";
        }

        @Override
        public boolean addSettingLine(TripMapPointType previousTripMapPointType, TripMapPointType tripMapPointType) {
            return previousTripMapPointType.equals(TripMapPointType.llActivityLogbookWithSettingStart) && tripMapPointType.equals(TripMapPointType.llActivityLogbookWithSettingEnd);
        }

        @Override
        public boolean addHaulingLine(TripMapPointType previousTripMapPointType, TripMapPointType tripMapPointType) {
            return previousTripMapPointType.equals(TripMapPointType.llActivityLogbookWithHaulingStart) && tripMapPointType.equals(TripMapPointType.llActivityLogbookWithHaulingEnd);
        }

        @Override
        public boolean skipLine(TripMapPointType previousTripMapPointType, TripMapPointType tripMapPointType) {
            return previousTripMapPointType.equals(TripMapPointType.llActivityLogbookWithSettingEnd) && tripMapPointType.equals(TripMapPointType.llActivityLogbookWithHaulingStart);
        }

        @Override
        public String tripLineTitle() {
            return I18n.t("observe.ui.datasource.editor.content.map.legend.logbook.trip");
        }

        @Override
        public String settingLineTitle() {
            return I18n.t("observe.ui.datasource.editor.content.map.legend.logbook.setting");
        }

        @Override
        public String haulingLineTitle() {
            return I18n.t("observe.ui.datasource.editor.content.map.legend.logbook.hauling");
        }

        @Override
        public String layerName() {
            return LOGBOOK_LINES_LAYER_NAME;
        }
    }

    public static class ObservationsLinesBuilder extends LinesBuilder {

        private static final String OBSERVATION_LINES_LAYER_NAME = "Observation lines";

        @Override
        public String tripLineId() {
            return "observationsTrip";
        }

        @Override
        public String settingLineId() {
            return "observationsSetting";
        }

        @Override
        public String haulingLineId() {
            return "observationsHauling";
        }

        @Override
        public boolean addSettingLine(TripMapPointType previousTripMapPointType, TripMapPointType tripMapPointType) {
            return previousTripMapPointType.equals(TripMapPointType.llActivityObsWithSettingStart) && tripMapPointType.equals(TripMapPointType.llActivityObsWithSettingEnd);
        }

        @Override
        public boolean addHaulingLine(TripMapPointType previousTripMapPointType, TripMapPointType tripMapPointType) {
            return previousTripMapPointType.equals(TripMapPointType.llActivityObsWithHaulingStart) && tripMapPointType.equals(TripMapPointType.llActivityObsWithHaulingEnd);
        }

        @Override
        public boolean skipLine(TripMapPointType previousTripMapPointType, TripMapPointType tripMapPointType) {
            return previousTripMapPointType.equals(TripMapPointType.llActivityObsWithSettingEnd) && tripMapPointType.equals(TripMapPointType.llActivityObsWithHaulingStart);
        }

        @Override
        public String tripLineTitle() {
            return I18n.t("observe.ui.datasource.editor.content.map.legend.obs.trip");
        }

        @Override
        public String settingLineTitle() {
            return I18n.t("observe.ui.datasource.editor.content.map.legend.obs.setting");
        }

        @Override
        public String haulingLineTitle() {
            return I18n.t("observe.ui.datasource.editor.content.map.legend.obs.hauling");
        }

        @Override
        public String layerName() {
            return OBSERVATION_LINES_LAYER_NAME;
        }
    }

    private final List<Coordinate[]> tripLines;
    private final List<Coordinate[]> settingLines;
    private final List<Coordinate[]> haulingLines;
    private TripMapPoint previousTripMapPoint;
    private TripMapPoint tripLineStart;
    private int index;

    protected LinesBuilder() {
        this.tripLines = new LinkedList<>();
        this.settingLines = new LinkedList<>();
        this.haulingLines = new LinkedList<>();
    }

    public abstract boolean addSettingLine(TripMapPointType previousTripMapPointType, TripMapPointType tripMapPointType);

    public abstract boolean addHaulingLine(TripMapPointType previousTripMapPointType, TripMapPointType tripMapPointType);

    public abstract boolean skipLine(TripMapPointType previousTripMapPointType, TripMapPointType tripMapPointType);

    public abstract String tripLineId();
    public abstract String tripLineTitle();

    public abstract String settingLineId();
    public abstract String settingLineTitle();

    public abstract String haulingLineId();
    public abstract String haulingLineTitle();

    public abstract String layerName();

    public void build(List<TripMapPoint> tripMapPoints) {
        reset();
        Iterator<TripMapPoint> iterator = tripMapPoints.iterator();
        if (iterator.hasNext()) {
            previousTripMapPoint = iterator.next();
        }
        while (iterator.hasNext()) {
            TripMapPoint tripMapPoint = iterator.next();
            index++;
            TripMapPointType tripMapPointType = tripMapPoint.getType();
            if (tripMapPoint.isValid()) {
                offerNextPoint(tripMapPoint);
            } else {
                log.debug(String.format("Point %d → %s - skip (not valid)", index, tripMapPointType));
            }
            previousTripMapPoint = tripMapPoint;
        }
    }

    public List<Coordinate[]> getTripLines() {
        return tripLines;
    }

    public List<Coordinate[]> getSettingLines() {
        return settingLines;
    }

    public List<Coordinate[]> getHaulingLines() {
        return haulingLines;
    }

    protected void offerNextPoint(TripMapPoint tripMapPoint) {
        TripMapPointType tripMapPointType = tripMapPoint.getType();
        log.debug(String.format("Point %d → %s", index, tripMapPointType));
        TripMapPointType previousTripMapPointType = previousTripMapPoint.getType();

        boolean previousPointValid = previousTripMapPoint.isValid();
        if (addSettingLine(previousTripMapPointType, tripMapPointType)) {
            if (previousPointValid) {
                settingLines.add(TripMapContentBuilderSupport.create(previousTripMapPoint, tripMapPoint));
                log.debug(String.format("Point %d → %s - add setting line %d (%s → %s)", index, tripMapPoint.getType(), settingLines.size(), previousTripMapPoint.getType(), tripMapPoint.getType()));
            }
            return;
        }
        if (skipLine(previousTripMapPointType, tripMapPointType)) {
            return;
        }
        if (addHaulingLine(previousTripMapPointType, tripMapPointType)) {
            if (previousPointValid) {
                haulingLines.add(TripMapContentBuilderSupport.create(previousTripMapPoint, tripMapPoint));
                log.debug(String.format("Point %d → %s - add hauling line %d (%s → %s)", index, tripMapPoint.getType(), haulingLines.size(), previousTripMapPoint.getType(), tripMapPoint.getType()));
            }
            return;
        }
        if (tripLineStart == null) {
            tripLineStart = tripMapPoint;
            log.debug(String.format("Point %d → %s - add first start for first line", index, tripMapPoint.getType()));
            return;
        }
        tripLines.add(TripMapContentBuilderSupport.create(tripLineStart, tripMapPoint));
        log.debug(String.format("Point %d → %s - add trip line %d (%s → %s)", index, tripMapPoint.getType(), tripLines.size(), tripLineStart.getType(), tripMapPoint.getType()));
        tripLineStart = tripMapPoint;
    }

    protected void reset() {
        index = 0;
        previousTripMapPoint = tripLineStart = null;
        tripLines.clear();
        settingLines.clear();
        haulingLines.clear();
    }
}
