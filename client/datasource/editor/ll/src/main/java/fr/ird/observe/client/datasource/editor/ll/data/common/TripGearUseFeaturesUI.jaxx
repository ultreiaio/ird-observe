<!--
  #%L
  ObServe Client :: DataSource :: Editor :: LL
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI
    beanScope="bean" i18n="fr.ird.observe.dto.data.ll.common.GearUseFeaturesDto"
    superGenericType='TripGearUseFeaturesDto, GearUseFeaturesDto, TripGearUseFeaturesUI'>

  <import>
    fr.ird.observe.dto.data.ll.common.TripGearUseFeaturesDto
    fr.ird.observe.dto.data.ll.common.GearUseFeaturesDto
    fr.ird.observe.dto.data.ll.common.GearUseFeaturesMeasurementDto
    fr.ird.observe.dto.referential.common.GearReference
    fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable

    io.ultreia.java4all.jaxx.widgets.choice.BooleanEditor
    io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox
    org.nuiton.jaxx.widgets.number.NumberEditor
    org.nuiton.jaxx.widgets.text.BigTextEditor
    org.jdesktop.swingx.JXTitledPanel

    static fr.ird.observe.client.util.UIHelper.getStringValue
    static io.ultreia.java4all.i18n.I18n.t
  </import>

  <TripGearUseFeaturesUIModel id='model' constructorParams='@override:getNavigationSource(this)'/>
  <TripGearUseFeaturesUIModelStates id='states'/>
  <TripGearUseFeaturesUITableModel id='tableModel' constructorParams='@override:this'/>
  <GearUseFeaturesMeasurementsTableModel id='measurementsTableModel' initializer="getTableModel().getMeasurementsTableModel()"/>
  <TripGearUseFeaturesDto id='bean'/>
  <GearUseFeaturesDto id='tableEditBean'/>

  <BeanValidator id='validator' beanClass='fr.ird.observe.dto.data.ll.common.TripGearUseFeaturesDto'
                 errorTableModel='{getErrorTableModel()}' context='update'/>
  <BeanValidator id='validatorTable' autoField='true' errorTableModel='{getErrorTableModel()}' context='update'
                 beanClass='fr.ird.observe.dto.data.ll.common.GearUseFeaturesDto'/>
  <BeanValidator id='validatorMeasure' errorTableModel='{getErrorTableModel()}' context='update'
                 beanClass='fr.ird.observe.dto.data.ll.common.GearUseFeaturesMeasurementDto'>
    <field name='gearCharacteristic' component='measurementsScrollPane'/>
    <field name='measurementValue' component='measurementsScrollPane'/>
  </BeanValidator>

  <Table id='editorPanel' fill='both' insets='0' beanScope="tableEditBean">
    <row>
      <cell weightx="1">
        <JPanel layout="{new GridLayout()}">
          <JXTitledPanel id="definitionPanel" contentContainer='{definitionForm}'>
            <Table id='definitionForm' fill='both' insets='1'>
              <!-- gear -->
              <row>
                <cell>
                  <JLabel id='gearLabel'/>
                </cell>
                <cell weightx='1' anchor='east'>
                  <FilterableComboBox id='gear' genericType='GearReference'/>
                </cell>
              </row>
              <!-- number -->
              <row>
                <cell>
                  <JLabel id='numberLabel'/>
                </cell>
                <cell weightx='1' anchor='east'>
                  <NumberEditor id='number' styleClass="int6"/>
                </cell>
              </row>
              <!-- usedInTrip -->
              <row>
                <cell>
                  <JLabel id='usedInTripLabel'/>
                </cell>
                <cell weightx='1' anchor='east'>
                  <BooleanEditor id='usedInTrip'/>
                </cell>
              </row>
              <!-- comment -->
              <row>
                <cell columns="2" weighty='1'>
                  <BigTextEditor id="comment"/>
                </cell>
              </row>
            </Table>
          </JXTitledPanel>
          <JXTitledPanel id="measurementsPanel" contentContainer='{measurementsInternalPanel}'>
            <JPanel id="measurementsInternalPanel" layout='{new GridLayout()}'>
              <JScrollPane id='measurementsScrollPane'>
                <EditableTable id='measurementsTable' constructorParams='measurementsTableModel' genericType="GearUseFeaturesMeasurementDto, GearUseFeaturesMeasurementsTableModel"/>
              </JScrollPane>
            </JPanel>
          </JXTitledPanel>
        </JPanel>
      </cell>
    </row>
  </Table>
  <JMenuItem id='moveAll' styleClass='editableAndNotModifiedAndTableNotEmpty'/>
</fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI>
