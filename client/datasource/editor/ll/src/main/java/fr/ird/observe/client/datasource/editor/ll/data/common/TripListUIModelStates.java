package fr.ird.observe.client.datasource.editor.ll.data.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.form.FormDefinition;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/**
 * Created at 25/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
@GenerateJavaBeanDefinition
public class TripListUIModelStates extends GeneratedTripListUIModelStates {

    public TripListUIModelStates(GeneratedTripListUIModel model) {
        super(model);
    }

    @Override
    public ReferencesCache createReferenceCache(boolean hideDisabledReferential) {
        ObserveSwingDataSource mainDataSource = getDataSourcesManager().getMainDataSource();
        ReferencesCache referencesCache = new ReferencesCache(mainDataSource, hideDisabledReferential);
        TripUIModelStates.fillReferenceCache(referencesCache);
        FormDefinition<BusinessDto> formDefinition = ObserveBusinessProject.get().getOptionalFormDefinition(source().getScope().getMainType()).orElseThrow();
        referencesCache.loadReferentialReferenceSetsInModel(formDefinition, false);
        return referencesCache;
    }
}
