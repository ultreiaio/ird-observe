package fr.ird.observe.client.datasource.editor.ll.data.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.datasource.editor.api.content.data.map.TripMapContentBuilderSupport;
import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.TripMapPointType;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.styling.Style;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.simple.SimpleFeature;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created on 04/07/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
@AutoService(fr.ird.observe.dto.data.TripMapContentBuilder.class)
public class TripMapContentBuilder extends TripMapContentBuilderSupport {

    private static final LinesBuilder.LogbookLinesBuilder LOGBOOK_LINES_BUILDER = new LinesBuilder.LogbookLinesBuilder();
    private static final ZonesBuilder.LogbookZonesBuilder LOGBOOK_ZONES_BUILDER = new ZonesBuilder.LogbookZonesBuilder();
    private static final LinesBuilder.ObservationsLinesBuilder OBSERVATIONS_LINES_BUILDER = new LinesBuilder.ObservationsLinesBuilder();
    private static final ZonesBuilder.ObservationsZonesBuilder OBSERVATIONS_ZONES_BUILDER = new ZonesBuilder.ObservationsZonesBuilder();
    private final SimpleFeatureBuilder polygonBuilder;

    public TripMapContentBuilder() {
        super(new LinkedHashSet<>(Arrays.asList(TripMapPointType.llActivityObs, TripMapPointType.llActivityObsInHarbour, TripMapPointType.llActivityObsWithSettingStart, TripMapPointType.llActivityObsWithSettingEnd, TripMapPointType.llActivityObsWithHaulingStart, TripMapPointType.llActivityObsWithHaulingEnd, TripMapPointType.llActivityLogbook, TripMapPointType.llActivityLogbookInHarbour, TripMapPointType.llActivityLogbookWithSettingStart, TripMapPointType.llActivityLogbookWithSettingEnd, TripMapPointType.llActivityLogbookWithHaulingStart, TripMapPointType.llActivityLogbookWithHaulingEnd)), createLineBuilder(false));
        polygonBuilder = createPolygonBuilder("longlineFishingZoneBuilder");
    }

    @Override
    public Set<String> getVariableNames() {
        Set<String> result = new LinkedHashSet<>(DEFAULT_VARIABLE_NAMES);
        result.add("mapLlStyleObservationsLineTripColor");
        result.add("mapLlStyleObservationsLineSettingColor");
        result.add("mapLlStyleObservationsLineHaulingColor");
        result.add("mapLlStyleObservationsPointColor");
        result.add("mapLlStyleLogbookLineTripColor");
        result.add("mapLlStyleLogbookLineSettingColor");
        result.add("mapLlStyleLogbookLineHaulingColor");
        result.add("mapLlStyleLogbookPointColor");
        return result;
    }

    @Override
    public String getStyleFileName() {
        return "ll-style.xml";
    }

    @Override
    public boolean accept(TripMapDto tripMapDto) {
        return TripAware.isLonglineId(tripMapDto.getTripId());
    }

    @Override
    public void addLines(TripMapConfigDto tripMapConfig, List<TripMapPoint> tripMapPoints, Set<String> excludedFeatureNames) {
        if (tripMapConfig.isAddObservations()) {
            List<TripMapPoint> obsPoints = getObsPoints(tripMapPoints);
            addFishingZone(OBSERVATIONS_ZONES_BUILDER, obsPoints);
            addLines(OBSERVATIONS_LINES_BUILDER, obsPoints, tripMapConfig.isAddObservationsTripSegment(), excludedFeatureNames);
        }
        if (tripMapConfig.isAddLogbook()) {
            List<TripMapPoint> logbookPoints = getLogbookPoints(tripMapPoints);
            addFishingZone(LOGBOOK_ZONES_BUILDER, logbookPoints);
            addLines(LOGBOOK_LINES_BUILDER, logbookPoints, tripMapConfig.isAddLogbookTripSegment(), excludedFeatureNames);
        }
    }

    @Override
    public void addPoints(TripMapConfigDto tripMapConfig, List<TripMapPoint> tripMapPoints, Set<String> excludedFeatureNames) {
        if (tripMapConfig.isAddObservations()) {
            addPoints(getObsPoints(tripMapPoints), OBSERVATION_POINTS_LAYER_NAME, n("observe.ui.datasource.editor.content.map.observation.points.not.valid"), excludedFeatureNames);
        }
        if (tripMapConfig.isAddLogbook()) {
            addPoints(getLogbookPoints(tripMapPoints), LOGBOOK_POINTS_LAYER_NAME, n("observe.ui.datasource.editor.content.map.logbook.points.not.valid"), excludedFeatureNames);
        }
    }

    private void addFishingZone(ZonesBuilder builder, List<TripMapPoint> tripMapPoints) {
        DefaultFeatureCollection polygonsFeatures = new DefaultFeatureCollection();
        builder.build(tripMapPoints);
        List<TripMapPoint[]> zones = builder.getZones();
        if (!zones.isEmpty()) {
            for (TripMapPoint[] zone : zones) {
                createZone(polygonsFeatures, zone[0], zone[1], zone[2], zone[3]);
            }
            Style styleLines = findStyle(styledLayerDescriptor, builder.layerName());
            Layer layerLines = new FeatureLayer(polygonsFeatures, styleLines, builder.layerName());
            mapContent.addLayer(layerLines);
        }
    }

    private void addLines(LinesBuilder builder, List<TripMapPoint> tripMapPoints, boolean addTripSegment, Set<String> excludedFeatureNames) {
        DefaultFeatureCollection linesFeatures = new DefaultFeatureCollection();
        Set<String> lineTypes = new TreeSet<>();
        builder.build(tripMapPoints);
        if (addTripSegment) {
            addLines(builder.tripLineId(), linesFeatures, lineTypes, builder.getTripLines(), excludedFeatureNames);
        }
        addLines(builder.settingLineId(), linesFeatures, lineTypes, builder.getSettingLines(), excludedFeatureNames);
        addLines(builder.haulingLineId(), linesFeatures, lineTypes, builder.getHaulingLines(), excludedFeatureNames);
        if (!lineTypes.isEmpty()) {
            Style styleLines = findStyle(styledLayerDescriptor, builder.layerName());
            Layer layerLines = new FeatureLayer(linesFeatures, styleLines, builder.layerName());
            mapContent.addLayer(layerLines);
            if (lineTypes.contains(builder.tripLineId())) {
                addLineLegend(styleLines, builder.tripLineId(), builder.tripLineTitle(), excludedFeatureNames);
            }
            if (lineTypes.contains(builder.settingLineId())) {
                addLineLegend(styleLines, builder.settingLineId(), builder.settingLineTitle(), excludedFeatureNames);
            }
            if (lineTypes.contains(builder.haulingLineId())) {
                addLineLegend(styleLines, builder.haulingLineId(), builder.haulingLineTitle(), excludedFeatureNames);
            }
        }
    }

    private void addLines(String lineType, DefaultFeatureCollection linesFeatures, Set<String> lineTypes, List<Coordinate[]> lines, Set<String> excludedFeatureNames) {
        if (!lines.isEmpty()) {
            lineTypes.add(lineType);
            for (Coordinate[] tripLine : lines) {
                addLine(linesFeatures, lineType, tripLine, excludedFeatureNames);
            }
        }
    }

    protected void createZone(DefaultFeatureCollection polygonsFeatures, TripMapPoint tripMapPoint, TripMapPoint tripMapPoint1, TripMapPoint tripMapPoint2, TripMapPoint tripMapPoint3) {
        Coordinate[] coordinates = create(tripMapPoint3, tripMapPoint2, tripMapPoint1, tripMapPoint, tripMapPoint3);
        Polygon polygon = getGeometryFactory().createPolygon(coordinates);
        polygonBuilder.add(polygon);
        polygonBuilder.add(formatDate(tripMapPoint3.getTime()));
        polygonBuilder.add("longlineFishingZone");
        SimpleFeature feature = polygonBuilder.buildFeature(null);
        polygonsFeatures.add(feature);
    }

    private List<TripMapPoint> getObsPoints(List<TripMapPoint> tripMapPoints) {
        return tripMapPoints.stream().filter(f -> f.getType().isTrip() || f.getType().isObs()).collect(Collectors.toList());
    }

    private List<TripMapPoint> getLogbookPoints(List<TripMapPoint> tripMapPoints) {
        return tripMapPoints.stream().filter(f -> f.getType().isTrip() || f.getType().isLogbook()).collect(Collectors.toList());
    }
}
