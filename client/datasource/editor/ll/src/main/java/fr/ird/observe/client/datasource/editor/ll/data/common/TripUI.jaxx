<!--
  #%L
  ObServe Client :: DataSource :: Editor :: LL
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUI
    beanScope="bean" i18n="fr.ird.observe.dto.data.ll.common.TripDto" superGenericType='TripDto, TripUI'
    implements="fr.ird.observe.client.datasource.editor.api.content.data.TripUI&lt;TripDto&gt;">

  <import>
    fr.ird.observe.dto.data.ll.common.TripDto
    fr.ird.observe.dto.referential.common.HarbourReference
    fr.ird.observe.dto.referential.common.VesselReference
    fr.ird.observe.dto.referential.common.PersonReference
    fr.ird.observe.dto.referential.common.OceanReference
    fr.ird.observe.dto.referential.common.SpeciesReference
    fr.ird.observe.dto.referential.common.DataQualityReference
    fr.ird.observe.dto.referential.ll.common.ProgramReference
    fr.ird.observe.dto.referential.ll.common.TripTypeReference
    fr.ird.observe.dto.referential.ll.common.ObservationMethodReference
    io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox
    org.nuiton.jaxx.widgets.datetime.DateEditor

    fr.ird.observe.client.datasource.editor.api.content.data.map.TripMapUI

    org.nuiton.jaxx.widgets.number.NumberEditor
    io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox
    io.ultreia.java4all.jaxx.widgets.list.DoubleList
    org.nuiton.jaxx.widgets.text.BigTextEditor
    org.nuiton.jaxx.widgets.text.NormalTextEditor
    org.jdesktop.swingx.JXPanel

    static fr.ird.observe.client.util.UIHelper.getStringValue
    static io.ultreia.java4all.i18n.I18n.n
  </import>

  <TripUIModel id='model' constructorParams='@override:getNavigationSource(this)'/>
  <TripUIModelStates id='states'/>
  <TripDto id='bean'/>
  <Boolean id="buildMap" javaBean="true"/>
  <BeanValidator id='validator' beanClass='fr.ird.observe.dto.data.ll.common.TripDto'
                 errorTableModel='{getErrorTableModel()}' autoField='true' context='update'>
    <field name='activityObs' component='{toggleInsert}'/>
    <field name='activityLogbook' component='{toggleInsert}'/>
    <field name='atLeastOneSelected' component='availableDataPanel'/>
  </BeanValidator>

  <JPanel id="contentBody" layout='{new BorderLayout()}'>
    <JTabbedPane id='mainTabbedPane' constraints='BorderLayout.CENTER'>
      <tab id='generalTab' i18nProperty="">
        <Table fill="both">
          <row>
            <cell anchor='west'>
              <JLabel id='tripTypeLabel'/>
            </cell>
            <cell anchor='east'>
              <FilterableComboBox id='tripType' genericType='TripTypeReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='captainLabel'/>
            </cell>
            <cell anchor='east'>
              <FilterableComboBox id='captain' genericType='PersonReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='vesselLabel'/>
            </cell>
            <cell anchor='east' weightx="1" fill="both">
              <FilterableComboBox id='vessel' genericType='VesselReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='oceanLabel'/>
            </cell>
            <cell anchor='east' weightx="1" fill="both">
              <FilterableComboBox id='ocean' genericType='OceanReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='departureLandingHarbourLabel'/>
            </cell>
            <cell anchor='east' weightx="1" fill="both">
              <JPanel layout="{new GridLayout()}">
                <FilterableComboBox id='departureHarbour' genericType='HarbourReference'/>
                <FilterableComboBox id='landingHarbour' genericType='HarbourReference'/>
              </JPanel>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='startEndDateLabel'/>
            </cell>
            <cell anchor='east' weightx="1" fill="both">
              <JPanel layout="{new GridLayout()}">
                <DateEditor id='startDate'/>
                <DateEditor id='endDate'/>
              </JPanel>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='noOfDaysLabel' styleClass="italic"/>
            </cell>
            <cell anchor='west' weightx="0.5">
              <JLabel id='noOfDays' styleClass="skipI18n"/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='homeIdErsIdLabel'/>
            </cell>
            <cell anchor='east' weightx="1" fill="both">
              <JPanel layout="{new GridLayout()}">
                <JPanel layout='{new BorderLayout()}'>
                  <NormalTextEditor id='homeId' constraints='BorderLayout.CENTER'/>
                  <JToolBar id='homeIdToolbar2' constraints='BorderLayout.EAST'>
                    <JButton id='generateHomeId'/>
                  </JToolBar>
                </JPanel>
                <NormalTextEditor id='ersId'/>
              </JPanel>
            </cell>
          </row>
          <row>
            <cell columns="2">
              <JPanel id='associatedActivityAndFishingOperationCountPanel' layout="{new GridLayout(1, 0)}">
                <JPanel id='associatedActivityPanel' layout="{new BorderLayout()}">
                  <JLabel id='associatedActivityCountLabel' styleClass="italic" constraints='BorderLayout.WEST'/>
                  <JLabel id='associatedActivityCount' styleClass="skipI18n" constraints='BorderLayout.CENTER'/>
                </JPanel>
                <JPanel id='associatedFishingOperationCountPanel' layout="{new BorderLayout()}">
                  <JLabel id='associatedFishingOperationCountLabel' styleClass="italic" constraints='BorderLayout.WEST'/>
                  <JLabel id='associatedFishingOperationCount' styleClass="skipI18n" constraints='BorderLayout.CENTER'/>
                </JPanel>
              </JPanel>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='noOfCrewMembersLabel'/>
            </cell>
            <cell anchor='west' weightx="0.5">
              <NumberEditor id='noOfCrewMembers' styleClass="int6"/>
            </cell>
          </row>
          <row>
            <cell columns="2">
              <JPanel id='availableDataPanel' layout="{new GridLayout(1,0)}" beanScope="states">
                <BeanCheckBox id='observationsAvailability'/>
                <BeanCheckBox id='logbookAvailability'/>
              </JPanel>
            </cell>
          </row>
          <row>
            <cell columns='2' fill='both' weighty="1">
              <BigTextEditor id="generalComment"/>
            </cell>
          </row>
        </Table>
      </tab>
      <tab id='speciesTab' i18nProperty="skip" title="observe.data.ll.common.Trip.species">
        <DoubleList id='species' genericType='SpeciesReference'/>
      </tab>
      <tab id='observationsTab' i18nProperty="">
        <Table fill="both">
          <row>
            <cell anchor='west'>
              <JLabel id='observationsProgramLabel'/>
            </cell>
            <cell anchor='east'>
              <FilterableComboBox id='observationsProgram' genericType='ProgramReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='observationMethodLabel'/>
            </cell>
            <cell anchor='east'>
              <FilterableComboBox id='observationMethod' genericType='ObservationMethodReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='observerLabel'/>
            </cell>
            <cell anchor='east'>
              <FilterableComboBox id='observer' genericType='PersonReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='observationsDataEntryOperatorLabel'/>
            </cell>
            <cell anchor='east'>
              <FilterableComboBox id='observationsDataEntryOperator' genericType='PersonReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='observationsDataQualityLabel'/>
            </cell>
            <cell anchor='east' weightx="1" fill="both">
              <FilterableComboBox id='observationsDataQuality' genericType='DataQualityReference'/>
            </cell>
          </row>
          <row>
            <cell columns='2'>
              <JPanel id='observationsActivityAndFishingOperationCountPanel' layout="{new GridLayout(1, 0)}">
                <JPanel id='observationsActivityCountPanel' layout="{new BorderLayout()}">
                  <JLabel id='observationsActivityCountLabel' styleClass="italic" constraints='BorderLayout.WEST'/>
                  <JLabel id='observationsActivityCount' styleClass="skipI18n" constraints='BorderLayout.CENTER'/>
                </JPanel>
                <JPanel id='observationsFishingOperationCountPanel' layout="{new BorderLayout()}">
                  <JLabel id='observationsFishingOperationCountLabel' styleClass="italic" constraints='BorderLayout.WEST'/>
                  <JLabel id='observationsFishingOperationCount' styleClass="skipI18n" constraints='BorderLayout.CENTER'/>
                </JPanel>
              </JPanel>
            </cell>
          </row>
          <row>
            <cell columns='2' fill='both' weighty="1">
              <BigTextEditor id="observationsComment"/>
            </cell>
          </row>
        </Table>
      </tab>
      <tab id='logbookTab' i18nProperty="">
        <Table fill="both">
          <row>
            <cell anchor='west'>
              <JLabel id='logbookProgramLabel'/>
            </cell>
            <cell anchor='east'>
              <FilterableComboBox id='logbookProgram' genericType='ProgramReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='logbookDataEntryOperatorLabel'/>
            </cell>
            <cell anchor='east'>
              <FilterableComboBox id='logbookDataEntryOperator' genericType='PersonReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='sampleDataEntryOperatorLabel'/>
            </cell>
            <cell anchor='east'>
              <FilterableComboBox id='sampleDataEntryOperator' genericType='PersonReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='landingDataEntryOperatorLabel'/>
            </cell>
            <cell anchor='east'>
              <FilterableComboBox id='landingDataEntryOperator' genericType='PersonReference'/>
            </cell>
          </row>
          <row>
            <cell anchor='west'>
              <JLabel id='logbookDataQualityLabel'/>
            </cell>
            <cell anchor='east' weightx="1" fill="both">
              <FilterableComboBox id='logbookDataQuality' genericType='DataQualityReference'/>
            </cell>
          </row>
          <row>
            <cell columns='2' weightx="1">
              <JPanel id='logbookActivityAndFishingOperationCountPanel' layout="{new GridLayout(1, 0)}">
                <JPanel id='logbookActivityCountPanel' layout="{new BorderLayout()}">
                  <JLabel id='logbookActivityCountLabel' styleClass="italic" constraints='BorderLayout.WEST'/>
                  <JLabel id='logbookActivityCount' styleClass="skipI18n" constraints='BorderLayout.CENTER'/>
                </JPanel>
                <JPanel id='logbookFishingOperationCountPanel' layout="{new BorderLayout()}">
                  <JLabel id='logbookFishingOperationCountLabel' styleClass="italic" constraints='BorderLayout.WEST'/>
                  <JLabel id='logbookFishingOperationCount' styleClass="skipI18n" constraints='BorderLayout.CENTER'/>
                </JPanel>

              </JPanel>
            </cell>
          </row>
          <row>
            <cell columns='2' fill='both' weighty="1">
              <BigTextEditor id="logbookComment"/>
            </cell>
          </row>
        </Table>
      </tab>
      <tab id="mapTab" i18nProperty="">
        <TripMapUI id="tripMap"/>
      </tab>
    </JTabbedPane>
  </JPanel>
</fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUI>
