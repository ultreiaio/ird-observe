package fr.ird.observe.client.datasource.editor.ll.data.common;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ll.common.TripDto;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * FIXME:Focus tab0 ui.getTripType(), tab1 ui.getTripMap().getZoomIt()
 * Created on 8/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
class TripUIHandler extends GeneratedTripUIHandler {

    private final VetoableChangeListener logbooksAvailabilityListener;
    private final VetoableChangeListener observationsAvailabilityListener;

    TripUIHandler() {
        logbooksAvailabilityListener = this::onLogbooksAvailabilityChanged;
        observationsAvailabilityListener = this::onObservationsAvailabilityChanged;
    }

    @Override
    public void onInit(TripUI ui) {
        super.onInit(ui);

        TripUIModelStates states = ui.getModel().getStates();
        states.addVetoableChangeListener(TripDto.PROPERTY_LOGBOOK_AVAILABILITY, logbooksAvailabilityListener);
        states.addVetoableChangeListener(TripDto.PROPERTY_OBSERVATIONS_AVAILABILITY, observationsAvailabilityListener);
        UIHelper.changeFontSize(ui.getGenerateHomeId(), 10f);
    }

    @Override
    public void startEditUI() {
        super.startEditUI();
        TripUIModel model = getModel();
        TripUIModelStates states = model.getStates();
        TripDto bean = states.getBean();
        ui.getOcean().setEnabled(!bean.withLogbookData() && !bean.withObservationData());
    }

    private void onLogbooksAvailabilityChanged(PropertyChangeEvent event) throws PropertyVetoException {
        TripUIModel model = getModel();
        if (!model.getStates().isOpened() || !model.getStates().isUpdatingMode()) {
            return;
        }
        TripDto bean = model.getStates().getBean();
        boolean reset;
        if (model.getStates().isUpdatingMode() && Objects.equals(Boolean.FALSE, event.getNewValue())) {
            if (bean.withLogbookData()) {
                String message = t("observe.data.ll.common.Trip.with.logbooks.message");
                displayWarning(t("observe.data.ll.common.Trip.with.logbooks.title"), message);
                throw new PropertyVetoException(message, event);
            }
            // ResetAction logbook meta-data
            reset = true;
        } else {
            // Set logbook default values
            reset = false;
        }
        model.getStates().setDefaultLogbookValues(reset);
    }

    private void onObservationsAvailabilityChanged(PropertyChangeEvent event) throws PropertyVetoException {
        TripUIModel model = getModel();
        if (!model.getStates().isOpened() || !model.getStates().isUpdatingMode()) {
            return;
        }
        TripDto bean = model.getStates().getBean();
        boolean reset;
        if (Objects.equals(Boolean.FALSE, event.getNewValue())) {
            if (bean.withObservationData()) {
                String message = t("observe.data.ll.common.Trip.with.observations.message");
                displayWarning(t("observe.data.ll.common.Trip.with.observations.title"), message);
                throw new PropertyVetoException(message, event);
            }
            // ResetAction observation meta-data
            reset = true;
        } else {
            // Set observation default values
            reset = false;
        }
        model.getStates().setDefaultObservationValues(reset);
    }

    public void onOpenAfterOpenModel() {
        ui.observationsActivityCount.setText("     " + ui.getBean().getActivityObsStatValue());
        ui.logbookActivityCount.setText("     " + ui.getBean().getActivityLogbookStatValue());
    }
}

