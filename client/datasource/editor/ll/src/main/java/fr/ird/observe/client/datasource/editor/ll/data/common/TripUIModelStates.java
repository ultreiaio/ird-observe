package fr.ird.observe.client.datasource.editor.ll.data.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.common.PersonReference;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.dto.referential.ll.common.ProgramReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

@GenerateJavaBeanDefinition
public class TripUIModelStates extends GeneratedTripUIModelStates {

    public TripUIModelStates(GeneratedTripUIModel model) {
        super(model);
        PropertyChangeListener propertyChangeListener = e -> ((TripDto) e.getSource()).updateNoOfDays();
        getBean().addPropertyChangeListener(TripDto.PROPERTY_START_DATE, propertyChangeListener);
        getBean().addPropertyChangeListener(TripDto.PROPERTY_END_DATE, propertyChangeListener);
        getBean().addPropertyChangeListener(TripDto.PROPERTY_LOGBOOK_AVAILABILITY, this::firePropertyChange);
        getBean().addPropertyChangeListener(TripDto.PROPERTY_OBSERVATIONS_AVAILABILITY, this::firePropertyChange);
    }

    static void fillReferenceCache( ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(TripDto.PROPERTY_CAPTAIN, ReferencesFilterHelper.newPredicateList(PersonReference::isCaptain));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_OBSERVER, ReferencesFilterHelper.newPredicateList(PersonReference::isObserver));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_OBSERVATIONS_DATA_ENTRY_OPERATOR, ReferencesFilterHelper.newPredicateList(PersonReference::isDataEntryOperator));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_LOGBOOK_DATA_ENTRY_OPERATOR, ReferencesFilterHelper.newPredicateList(PersonReference::isDataEntryOperator));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_LANDING_DATA_ENTRY_OPERATOR, ReferencesFilterHelper.newPredicateList(PersonReference::isDataEntryOperator));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_SAMPLE_DATA_ENTRY_OPERATOR, ReferencesFilterHelper.newPredicateList(PersonReference::isDataEntryOperator));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_VESSEL, ReferencesFilterHelper.<VesselReference>newPredicateList(v -> v.getVesselType().isLongline()));

        referenceCache.addReferentialFilter(TripDto.PROPERTY_OBSERVATIONS_PROGRAM, ReferencesFilterHelper.newPredicateList(ProgramReference::isObservation));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_LOGBOOK_PROGRAM, ReferencesFilterHelper.newPredicateList(ProgramReference::isLogbook));
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        fillReferenceCache( referenceCache);
        referenceCache.addReferentialFilter(TripDto.PROPERTY_SPECIES, ReferencesFilterHelper.newLlSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListLonglineCommonTripId()));
    }

    @Override
    protected void loadReferentialCacheOnOpenForm(Form<TripDto> form) {
        super.loadReferentialCacheOnOpenForm(form);
    }

    @Override
    protected void copyFormToBean(Form<TripDto> form) {
        firePropertyChange(TripDto.PROPERTY_LOGBOOK_AVAILABILITY, isLogbookAvailability());
        firePropertyChange(TripDto.PROPERTY_OBSERVATIONS_AVAILABILITY, isObservationsAvailability());
        super.copyFormToBean(form);
    }

    public void setDefaultObservationValues(boolean reset) {
        TripDto bean = getBean();
        if (reset) {
            bean.setObservationsComment(null);
            bean.setObservationsDataEntryOperator(null);
            bean.setObservationsDataQuality(null);
            bean.setObservationMethod(null);
//            bean.setObservationsProgram(null);
            bean.setObserver(null);
        }
    }

    public void setDefaultLogbookValues(boolean reset) {
        TripDto bean = getBean();
        if (reset) {
            bean.setLogbookComment(null);
            bean.setLogbookDataEntryOperator(null);
            bean.setLogbookDataQuality(null);
//            bean.setLogbookProgram(null);
        }
    }

    public boolean isLogbookAvailability() {
        return getBean().isLogbookAvailability();
    }

    public void setLogbookAvailability(boolean logbookAvailability) {
        boolean oldValue = isLogbookAvailability();
        try {
            fireVetoableChange(TripDto.PROPERTY_LOGBOOK_AVAILABILITY, oldValue, logbookAvailability);
            getBean().setLogbookAvailability(logbookAvailability);
            firePropertyChange(TripDto.PROPERTY_LOGBOOK_AVAILABILITY, oldValue, logbookAvailability);
        } catch (PropertyVetoException ignored) {
            firePropertyChange(TripDto.PROPERTY_LOGBOOK_AVAILABILITY, oldValue);
        }
    }

    public boolean isObservationsAvailability() {
        return getBean().isObservationsAvailability();
    }

    public void setObservationsAvailability(boolean observationsAvailability) {
        boolean oldValue = isObservationsAvailability();
        try {
            fireVetoableChange(TripDto.PROPERTY_OBSERVATIONS_AVAILABILITY, oldValue, observationsAvailability);
            getBean().setObservationsAvailability(observationsAvailability);
            firePropertyChange(TripDto.PROPERTY_OBSERVATIONS_AVAILABILITY, oldValue, observationsAvailability);
        } catch (PropertyVetoException ignored) {
            firePropertyChange(TripDto.PROPERTY_OBSERVATIONS_AVAILABILITY, oldValue);
        }
    }

}
