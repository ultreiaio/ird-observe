package fr.ird.observe.client.datasource.editor.ll.data.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.TripMapPointType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * To build longline lines.
 * <p>
 * Created on 14/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.1
 */
public abstract class ZonesBuilder {

    private static final Logger log = LogManager.getLogger(ZonesBuilder.class);

    public static class LogbookZonesBuilder extends ZonesBuilder {

        private static final String LOGBOOK_ZONE_LAYER_NAME = "Logbook zone";

        @Override
        public boolean addZone(TripMapPointType previous3TripMapPointType, TripMapPointType previous2TripMapPointType, TripMapPointType previous1TripMapPointType, TripMapPointType tripMapPointType) {
            return previous3TripMapPointType.equals(TripMapPointType.llActivityLogbookWithSettingStart)
                    && previous2TripMapPointType.equals(TripMapPointType.llActivityLogbookWithSettingEnd)
                    && previous1TripMapPointType.equals(TripMapPointType.llActivityLogbookWithHaulingStart)
                    && tripMapPointType.equals(TripMapPointType.llActivityLogbookWithHaulingEnd);
        }

        @Override
        public String layerName() {
            return LOGBOOK_ZONE_LAYER_NAME;
        }
    }

    public static class ObservationsZonesBuilder extends ZonesBuilder {

        private static final String OBSERVATION_ZONE_LAYER_NAME = "Observation zone";

        @Override
        public boolean addZone(TripMapPointType previous3TripMapPointType, TripMapPointType previous2TripMapPointType, TripMapPointType previous1TripMapPointType, TripMapPointType tripMapPointType) {
            return previous3TripMapPointType.equals(TripMapPointType.llActivityObsWithSettingStart)
                    && previous2TripMapPointType.equals(TripMapPointType.llActivityObsWithSettingEnd)
                    && previous1TripMapPointType.equals(TripMapPointType.llActivityObsWithHaulingStart)
                    && tripMapPointType.equals(TripMapPointType.llActivityObsWithHaulingEnd);
        }

        @Override
        public String layerName() {
            return OBSERVATION_ZONE_LAYER_NAME;
        }
    }

    private final List<TripMapPoint[]> zones;
    private TripMapPoint previous3TripMapPoint;
    private TripMapPoint previous2TripMapPoint;
    private TripMapPoint previous1TripMapPoint;
    private int index;

    protected ZonesBuilder() {
        this.zones = new LinkedList<>();
    }

    public abstract boolean addZone(TripMapPointType previousTripMapPointType, TripMapPointType previous2TripMapPointType, TripMapPointType previous3TripMapPointType, TripMapPointType tripMapPointType);

    public abstract String layerName();

    public void build(List<TripMapPoint> tripMapPoints) {
        reset();
        Iterator<TripMapPoint> iterator = tripMapPoints.iterator();
        if (iterator.hasNext()) {
            previous3TripMapPoint = iterator.next();
        }
        if (iterator.hasNext()) {
            previous2TripMapPoint = iterator.next();
        }
        if (iterator.hasNext()) {
            previous1TripMapPoint = iterator.next();
        }
        while (iterator.hasNext()) {
            TripMapPoint tripMapPoint = iterator.next();
            index++;
            if (tripMapPoint.isValid()) {
                offerNextPoint(tripMapPoint);
            } else {
                log.info(String.format("Point %d → %s - skip (not valid)", index, tripMapPoint.getType()));
            }
            previous3TripMapPoint = previous2TripMapPoint;
            previous2TripMapPoint = previous1TripMapPoint;
            previous1TripMapPoint = tripMapPoint;
        }
    }

    public List<TripMapPoint[]> getZones() {
        return zones;
    }

    protected void offerNextPoint(TripMapPoint tripMapPoint) {
        TripMapPointType tripMapPointType = tripMapPoint.getType();
        log.info(String.format("Point %d → %s", index, tripMapPointType));
        if (!previous3TripMapPoint.isValid() || !previous2TripMapPoint.isValid() || !previous1TripMapPoint.isValid()) {
            return;
        }
        if (addZone(previous3TripMapPoint.getType(), previous2TripMapPoint.getType(), previous1TripMapPoint.getType(), tripMapPointType)) {
            zones.add(new TripMapPoint[]{previous3TripMapPoint, previous2TripMapPoint, previous1TripMapPoint, tripMapPoint});
            log.info(String.format("Point %d → %s - add zone %d (%s → %s)", index, tripMapPoint.getType(), zones.size(), previous3TripMapPoint.getType(), tripMapPoint.getType()));
        }
    }

    protected void reset() {
        index = 0;
        previous3TripMapPoint = previous2TripMapPoint = previous1TripMapPoint = null;
        zones.clear();
    }
}
