package fr.ird.observe.client.datasource.editor.ll.data.common.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes;
import fr.ird.observe.client.datasource.editor.ll.data.common.TripUI;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created by tchemit on 23/12/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TripUIGenerateHomeId extends ContentUIActionSupport<TripUI> {

    public TripUIGenerateHomeId() {
        super("", n("observe.data.ll.common.Trip.action.generateHomeId.tip"), "generate", ObserveLLKeyStrokes.KEY_STROKE_GENERATE_ID);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, TripUI ui) {
        String homeId = getDataSource().getAnonymousService().generateHomeId();
        ui.getModel().getStates().getBean().setHomeId(homeId);
    }
}
