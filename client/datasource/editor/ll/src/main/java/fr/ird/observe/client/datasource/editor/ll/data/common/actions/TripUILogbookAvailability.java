package fr.ird.observe.client.datasource.editor.ll.data.common.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes;
import fr.ird.observe.client.datasource.editor.ll.data.common.TripUI;
import io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox;

import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Created on 29/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public class TripUILogbookAvailability extends ContentUIActionSupport<TripUI> {

    public TripUILogbookAvailability() {
        super(null, null, null, ObserveLLKeyStrokes.KEY_STROKE_TOGGLE_LOGBOOK_AVAILABILITY);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, TripUI ui) {
        BeanCheckBox editor = ui.getLogbookAvailability();
        boolean newValue = editor.isSelected();
        if (!Objects.equals(e.getSource(), editor)) {
            // real toggle
            newValue = !newValue;
        }
        ui.getModel().getStates().setLogbookAvailability(newValue);
    }
}
