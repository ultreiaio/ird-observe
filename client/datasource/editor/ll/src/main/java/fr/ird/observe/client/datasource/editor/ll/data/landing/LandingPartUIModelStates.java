package fr.ird.observe.client.datasource.editor.ll.data.landing;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.ll.landing.LandingPartDto;
import fr.ird.observe.dto.referential.common.DataQualityReference;
import fr.ird.observe.dto.referential.common.WeightMeasureMethodReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class LandingPartUIModelStates extends GeneratedLandingPartUIModelStates {

    private final WeightMeasureMethodReference defaultWeightMeasureMethod;
    private final DataQualityReference defaultDataQuality;

    public LandingPartUIModelStates(GeneratedLandingPartUIModel model) {
        super(model);
        String id = model.getClientConfig().getLonglineLandingPartDefaultWeightMeasureMethod();
        this.defaultWeightMeasureMethod = model.getSource().getContext().getMainDataSource().getReferentialReferenceSet(WeightMeasureMethodReference.class).tryGetReferenceById(id).orElseThrow(IllegalStateException::new);
        this.defaultDataQuality = model.getSource().getContext().getMainDataSource().getReferentialReferenceSet(DataQualityReference.class).tryGetReferenceById(ProtectedIdsLl.LL_LOGBOOK_LANDING_PART_DEFAULT_DATA_QUALITY_ID).orElseThrow(IllegalStateException::new);
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(LandingPartDto.PROPERTY_SPECIES, ReferencesFilterHelper.newLlSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListLonglineLandingId()));
    }

    @Override
    public void initDefault(LandingPartDto newTableBean) {
        newTableBean.setWeightMeasureMethod(defaultWeightMeasureMethod);
        newTableBean.setDataQuality(defaultDataQuality);
    }
}
