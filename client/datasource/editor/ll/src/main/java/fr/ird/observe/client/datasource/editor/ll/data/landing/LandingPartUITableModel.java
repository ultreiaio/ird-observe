package fr.ird.observe.client.datasource.editor.ll.data.landing;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.EditableContentUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ll.landing.LandingDto;

import javax.swing.JTable;

/**
 * Created on 10/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class LandingPartUITableModel extends GeneratedLandingPartUITableModel {

    private static final long serialVersionUID = 1L;

    public LandingPartUITableModel(EditableContentUI<LandingDto> parentUi, LandingPartUI ui, LandingPartUIModel model) {
        super(parentUi, ui, model);
    }

    @Override
    public void initTableUISize(JTable table) {
        super.initTableUISize(table);
        UIHelper.fixTableColumnWidth(table, 1, 75);
        UIHelper.fixTableColumnWidth(table, 2, 75);
        UIHelper.fixTableColumnWidth(table, 3, 75);
        UIHelper.fixTableColumnWidth(table, 4, 75);
    }
}
