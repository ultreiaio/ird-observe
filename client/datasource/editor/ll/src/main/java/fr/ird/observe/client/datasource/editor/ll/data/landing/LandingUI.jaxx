<!--
  #%L
  ObServe Client :: DataSource :: Editor :: LL
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI
    beanScope="bean" i18n="fr.ird.observe.dto.data.ll.landing.LandingDto" superGenericType='LandingDto, LandingUI'>

  <import>
    fr.ird.observe.dto.data.ll.landing.LandingDto
    fr.ird.observe.dto.data.ll.landing.LandingPartDto
    fr.ird.observe.dto.referential.common.DataQualityReference
    fr.ird.observe.dto.referential.common.HarbourReference
    fr.ird.observe.dto.referential.common.PersonReference
    fr.ird.observe.dto.referential.common.SpeciesReference
    fr.ird.observe.dto.referential.common.VesselReference
    fr.ird.observe.dto.referential.ll.landing.DataSourceReference
    fr.ird.observe.dto.referential.ll.landing.CompanyReference

    io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox
    org.nuiton.jaxx.widgets.datetime.DateEditor
    org.nuiton.jaxx.widgets.text.BigTextEditor
    org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor

    static fr.ird.observe.client.util.UIHelper.getStringValue
    static io.ultreia.java4all.i18n.I18n.n
  </import>

  <LandingUIModel id='model' constructorParams='@override:getNavigationSource(this)'/>
  <LandingUIModelStates id='states'/>
  <LandingDto id='bean'/>
  <BeanValidator id='validator' context='update' autoField='true'
                 beanClass='fr.ird.observe.dto.data.ll.landing.LandingDto'
                 errorTableModel='{getErrorTableModel()}'>
    <field name='longitude' component='coordinate'/>
    <field name='latitude' component='coordinate'/>
    <field name='quadrant' component='coordinate'/>
    <field name='landingPart' component='landingPartPanel'/>
  </BeanValidator>

  <JPanel id="contentBody" layout='{new BorderLayout()}'>
    <Table insets="0" fill="both" constraints='BorderLayout.CENTER'>
      <row>
        <cell weightx="1" weighty="1">
          <JTabbedPane id='mainTabbedPane'>
            <tab id='characteristicsTab' i18nProperty="">
              <Table fill='both'>
                <row>
                  <cell>
                    <JLabel id='dataSourceLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <FilterableComboBox id='dataSource' genericType='DataSourceReference'/>
                  </cell>
                </row>
                <row>
                  <cell>
                    <JLabel id='personLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <FilterableComboBox id='person' genericType='PersonReference'/>
                  </cell>
                </row>
                <row>
                  <cell>
                    <JLabel id='processingCompanyLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <FilterableComboBox id='processingCompany' genericType='CompanyReference'/>
                  </cell>
                </row>
                <row>
                  <cell>
                    <JLabel id='shippingCompanyLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <FilterableComboBox id='shippingCompany' genericType='CompanyReference'/>
                  </cell>
                </row>
                <row>
                  <cell>
                    <JLabel id='brokerageCompanyLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <FilterableComboBox id='brokerageCompany' genericType='CompanyReference'/>
                  </cell>
                </row>
                <row>
                  <cell>
                    <JLabel id='harbourLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <FilterableComboBox id='harbour' genericType='HarbourReference'/>
                  </cell>
                </row>
                <row>
                  <cell>
                    <JLabel id='vesselLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <FilterableComboBox id='vessel' genericType='VesselReference'/>
                  </cell>
                </row>
                <row>
                  <cell>
                    <JLabel id='coordinateLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <CoordinatesEditor id='coordinate'/>
                  </cell>
                </row>
                <row>
                  <cell anchor='west'>
                    <JLabel id='startDateLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1">
                    <DateEditor id='startDate'/>
                  </cell>
                </row>
                <row>
                  <cell anchor='west'>
                    <JLabel id='endDateLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1">
                    <DateEditor id='endDate'/>
                  </cell>
                </row>
                <row>
                  <cell fill="both" weighty="1" columns="2">
                    <BigTextEditor id="comment"/>
                  </cell>
                </row>
              </Table>
            </tab>
            <tab id='landingPartTab' i18nProperty="">
              <JPanel id="landingPartPanel" layout='{new BorderLayout()}'>
                <LandingPartUI id="landingPartUI" constructorParams="newContext()"/>
              </JPanel>
            </tab>
          </JTabbedPane>
        </cell>
      </row>
    </Table>
  </JPanel>

  <Object id="delegateContentUI" initializer="mainTabbedPane"/>
  <LandingPartUIModelStates id='landingPartStates' initializer="landingPartUI.getModel().getStates()"/>
  <JButton id='selectTransshipment'/>
  <JButton id='save'/>
</fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI>
