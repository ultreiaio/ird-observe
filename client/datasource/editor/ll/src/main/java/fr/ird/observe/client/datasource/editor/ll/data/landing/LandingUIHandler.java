package fr.ird.observe.client.datasource.editor.ll.data.landing;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUIOpenExecutor;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.data.ll.landing.LandingDto;
import fr.ird.observe.dto.data.ll.landing.LandingPartDto;
import fr.ird.observe.dto.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.dto.referential.ll.landing.DataSourceReference;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import java.beans.PropertyChangeListener;

/**
 * Created on 12/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
class LandingUIHandler extends GeneratedLandingUIHandler {

    private final PropertyChangeListener dataSourceChanged;

    LandingUIHandler() {
        dataSourceChanged = evt -> onDataSourceChanged((DataSourceReference) evt.getNewValue());
    }

    @Override
    protected void onBeforeInit(LandingUI ui, ContentUIInitializer<LandingUI> initializer) {
        initializer.registerDependencies(LandingPartUIModel.create(ui));
    }

    @Override
    protected ContentOpenWithValidator<LandingUI> createContentOpen(LandingUI ui) {
        ContentOpenableUIOpenExecutor<LandingDto, LandingUI> executor = new ContentOpenableUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor) {
            @Override
            public DefaultUIInitializerResult init(ContentUIInitializer<LandingUI> initializer) {
                DefaultUIInitializerResult result = super.init(initializer);
                String type = ObserveI18nDecoratorHelper.getType(LandingPartDto.class) + " - ";
                InputMap inputMap = ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
                ActionMap actionMap = ui.getActionMap();

                prefixAction(ui.getLandingPartUI().getDeleteEntry(), type);
                prefixAction(ui.getLandingPartUI().getShowTechnicalInformations(), type);
                registerInnerAction(ui.getLandingPartUI().getResetEntry(), inputMap, actionMap);
                registerInnerAction(ui.getLandingPartUI().getSaveEntry(), inputMap, actionMap);
                registerInnerAction(ui.getLandingPartUI().getSaveAndNewEntry(), inputMap, actionMap);

                addConfigureActions(ui, false);
                initTabUI(ui.getLandingPartUI(), ui.getLandingPartPanel(), 1, d -> ui.getModel().getStates().getBean().setLandingPart(d.getLandingPart()));
                ui.getHarbour().getToolbarRight().add(ui.getSelectTransshipment());
                return result;
            }
        };
    }


    @Override
    public void onMainTabChanged(int previousIndex, int selectedIndex) {
        super.onMainTabChanged(previousIndex, selectedIndex);
        LandingPartUI subUi = ui.getLandingPartUI();
        boolean partTab = selectedIndex == 1;
        subUi.getSelectToolbar().setVisible(partTab && subUi.getTableModel().getRowCount() > 1);
        getContentOpen().updateConfigurePopup(ui, true);
        if (partTab) {
            ui.getConfigurePopup().addSeparator();
            getContentOpen().updateConfigurePopup(subUi, false);
        }
    }

    @Override
    public void onOpenAfterOpenModel() {
        super.onOpenAfterOpenModel();
        LandingDto bean = getModel().getStates().getBean();
        bean.addPropertyChangeListener(LandingDto.PROPERTY_DATA_SOURCE, dataSourceChanged);
        ui.getLandingPartTab().setEnabled(bean.isPersisted());
        onDataSourceChanged(bean.getDataSource());
    }

    private void onDataSourceChanged(DataSourceReference dataSource) {
        if (ui.getModel().getStates().isReadingMode()) {
            return;
        }
        if (dataSource == null) {
            ui.getPerson().setEnabled(false);
            getModel().getStates().getBean().setPerson(null);
        } else {
            ui.getPerson().setEnabled(true);
        }
    }
}
