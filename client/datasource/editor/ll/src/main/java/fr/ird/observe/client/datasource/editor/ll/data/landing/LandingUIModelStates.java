package fr.ird.observe.client.datasource.editor.ll.data.landing;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.ll.landing.LandingDto;
import fr.ird.observe.dto.referential.common.HarbourReference;
import fr.ird.observe.dto.referential.common.PersonReference;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Objects;

@GenerateJavaBeanDefinition
public class LandingUIModelStates extends GeneratedLandingUIModelStates {

    private final HarbourReference transshipmentHarbour;
    private final VesselReference transshipmentVessel;
    private LandingPartUIModel landingPartUIModel;

    public LandingUIModelStates(GeneratedLandingUIModel model) {
        super(model);
        ObserveSwingDataSource mainDataSource = model.getSource().getContext().getMainDataSource();
        transshipmentHarbour = mainDataSource.getReferentialReferenceSet(HarbourReference.class).tryGetReferenceById(ProtectedIdsLl.LL_LANDING_TRANSSHIPMENT_HARBOUR_ID).orElseThrow(IllegalStateException::new);
        transshipmentVessel = mainDataSource.getReferentialReferenceSet(VesselReference.class).tryGetReferenceById(ProtectedIdsLl.LL_LANDING_TRANSSHIPMENT_VESSEL_ID).orElseThrow(IllegalStateException::new);
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(LandingDto.PROPERTY_PERSON, ReferencesFilterHelper.newPredicateList(PersonReference::isDataSource));
        referenceCache.addReferentialFilter(LandingDto.PROPERTY_VESSEL, ReferencesFilterHelper.newSubVesselList(clientConfig.getVesselTypeLonglineLandingId()));
    }

    @Override
    public LandingDto getBeanToSave() {
        LandingDto bean = super.getBeanToSave();
        bean.setLandingPart(getLandingPartUIModel().getStates().getBeanToSave().getLandingPart());
        return bean;
    }

    public void selectTransshipment() {
        getBean().setHarbour(getTransshipmentHarbour());
        getBean().setVessel(getTransshipmentVessel());
    }

    public LandingPartUIModel getLandingPartUIModel() {
        return landingPartUIModel;
    }

    public void setLandingPartUIModel(LandingPartUIModel landingPartUIModel) {
        this.landingPartUIModel = Objects.requireNonNull(landingPartUIModel);
        firePropertyChange("landingPartUIModel", null, landingPartUIModel);
    }

    public HarbourReference getTransshipmentHarbour() {
        return transshipmentHarbour;
    }

    public VesselReference getTransshipmentVessel() {
        return transshipmentVessel;
    }
}
