package fr.ird.observe.client.datasource.editor.ll.data.landing.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes;
import fr.ird.observe.client.datasource.editor.ll.data.landing.LandingUI;
import fr.ird.observe.client.datasource.editor.ll.data.landing.LandingUIModel;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created by tchemit on 14/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LandingUISelectTransshipment extends ContentUIActionSupport<LandingUI> {

    public LandingUISelectTransshipment() {
        super("", n("observe.data.ll.landing.Landing.action.selectTransshipment"), "data-calcule", ObserveLLKeyStrokes.KEY_STROKE_SELECT_TRANSSHIPMENT);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, LandingUI ui) {
        LandingUIModel model = ui.getModel();
        model.getStates().selectTransshipment();
    }
}
