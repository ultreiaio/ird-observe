package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUIOpenExecutor;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIHandler;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.sample.SampleUIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.data.ll.logbook.ActivitySampleDto;
import fr.ird.observe.dto.data.ll.logbook.SamplePartDto;
import fr.ird.observe.dto.decoration.ObserveI18nDecoratorHelper;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.AbstractButton;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.util.Objects;

/**
 * Created on 12/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public class ActivitySampleUIHandler extends GeneratedActivitySampleUIHandler {
    static {
        I18n.n("observe.data.ll.logbook.ActivitySample.action.move");
    }

    @Override
    protected void onBeforeInit(ActivitySampleUI ui, ContentUIInitializer<ActivitySampleUI> initializer) {
        SamplePartUIModel partUIModel = SamplePartUIModelStates.create(ui);
        initializer.registerDependencies(partUIModel);
    }

    @Override
    protected ContentOpenWithValidator<ActivitySampleUI> createContentOpen(ActivitySampleUI ui) {
        ContentEditUIOpenExecutor<ActivitySampleDto, ActivitySampleUI> executor = new ContentEditUIOpenExecutor<>();

        return new ContentOpenWithValidator<>(ui, executor, executor) {

            @Override
            public DefaultUIInitializerResult init(ContentUIInitializer<ActivitySampleUI> initializer) {
                DefaultUIInitializerResult result = super.init(initializer);
                String type = ObserveI18nDecoratorHelper.getType(SamplePartDto.class) + " - ";
                InputMap inputMap = ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
                ActionMap actionMap = ui.getActionMap();

                SamplePartUI subUi = ui.getSamplePartUI();
                prefixAction(subUi.getDeleteEntry(), type);
                prefixAction(subUi.getShowTechnicalInformations(), type);
                registerInnerAction(subUi.getResetEntry(), inputMap, actionMap);
                registerInnerAction(subUi.getSaveEntry(), inputMap, actionMap);
                registerInnerAction(subUi.getSaveAndNewEntry(), inputMap, actionMap);

                addConfigureActions(ui, false);
                initTabUI(subUi, ui.getSamplePartPanel(), 1, d -> ui.getModel().getStates().getBean().setSamplePart(d.getSamplePart()));

                getContentOpen().updateConfigurePopup(ui, true);
                ui.getConfigurePopup().addSeparator();
                getContentOpen().updateConfigurePopup(subUi, false);

                subUi.getStates().getTableModel().addTableModelListener(e -> onTableModelChanged());
                return result;
            }
        };
    }

    @Override
    public void onEndOpenUI() {
        super.onEndOpenUI();
        SamplePartUI subUi = ui.getSamplePartUI();
        if (ui.getModel().getStates().isCreatingMode()) {
            ui.getInsertPopup().setEnabled(true);
            fixToggleMenuVisibility();
            SwingUtilities.invokeLater(() -> {
                AbstractButton editor = (AbstractButton) subUi.getClientProperty(ContentTableUIHandler.CLIENT_PROPERTY_CREATE_ACTION);
                Objects.requireNonNull(editor).doClick();
                SwingUtilities.invokeLater(() -> subUi.getSpecies().requestFocusInWindow());
            });
        } else {
            onTableModelChanged();
        }
    }

    @Override
    protected void installExtraActions() {
        MoveAction.create(ui)
                .on(SampleUIHelper.newSupplier(ui, ui.getModel()::toRootMoveRequest, ui.getModel()::toMoveRequest))
                .call((r, newParentId, ids) -> getLlCommonTripService().moveActivitySample(r))
                .then(SampleUIHelper::create)
                .install(ui::getMove);
    }

    protected void onTableModelChanged() {
        SamplePartUI subUi = ui.getSamplePartUI();
        subUi.getSelectToolbar().setVisible(subUi.getTableModel().getRowCount() > 1);
    }
}
