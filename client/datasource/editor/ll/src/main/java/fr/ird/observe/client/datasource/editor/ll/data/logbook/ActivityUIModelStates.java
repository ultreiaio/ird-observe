package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.data.ll.logbook.ActivityDto;
import fr.ird.observe.dto.data.ll.observation.ActivityReference;
import fr.ird.observe.dto.data.ll.pairing.ActivityPairingDto;
import fr.ird.observe.dto.data.ll.pairing.ActivityPairingEngine;
import fr.ird.observe.dto.referential.ll.common.VesselActivityReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@GenerateJavaBeanDefinition
public class ActivityUIModelStates extends GeneratedActivityUIModelStates {

    public static final String PROPERTY_ACTIVITY_OBS = "activityObs";
    public static final Set<String> PROPERTIES_FOR_UPDATE_ACTIVITIES_OBS_LIST = new LinkedHashSet<>(Arrays.asList(
            ActivityDto.PROPERTY_VESSEL_ACTIVITY,
            ActivityDto.PROPERTY_LATITUDE,
            ActivityDto.PROPERTY_LONGITUDE,
            ActivityDto.PROPERTY_QUADRANT,
            ActivityDto.PROPERTY_START_DATE,
            ActivityDto.PROPERTY_START_TIME,
            ActivityDto.PROPERTY_START_TIME_STAMP,
            ActivityDto.PROPERTY_END_DATE,
            ActivityDto.PROPERTY_END_TIME,
            ActivityDto.PROPERTY_END_TIME_STAMP));

    private static final Logger log = LogManager.getLogger(ActivityUIModelStates.class);
    private final SingletonSupplier<ActivityPairingEngine> pairingEngineSupplier;
    private List<ActivityReference> activityObs = new ArrayList<>();

    public ActivityUIModelStates(GeneratedActivityUIModel model) {
        super(model);
        ObserveSwingDataSource dataSource = getReferenceCache().getDataSource();
        this.pairingEngineSupplier = SingletonSupplier.of(() -> {
            ActivityPairingDto data = dataSource.getLlActivityPairingService().getActivityPairingDto(getClientValidationContext().getSelectModel().getLl().getCommonTrip().getId());
            return data == null ? null : new ActivityPairingEngine(data);
        }, false);
        getBean().addPropertyChangeListener(evt -> {
            String propertyName = evt.getPropertyName();
            if (!isOpened() || isReadingMode() || !PROPERTIES_FOR_UPDATE_ACTIVITIES_OBS_LIST.contains(propertyName)) {
                return;
            }
            pairingEngine().ifPresent(this::updateActivityObsCandidates);
        });
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(ActivityDto.PROPERTY_VESSEL_ACTIVITY, ReferencesFilterHelper.newPredicateList(VesselActivityReference::isLogbook));
    }

    public List<ActivityReference> getActivityObs() {
        return activityObs;
    }

    public void setActivityObs(List<ActivityReference> activityObs) {
        ActivityReference relatedObservedActivity = getBean().getRelatedObservedActivity();
        this.activityObs = Objects.requireNonNull(activityObs);

        boolean removeRelatedObservedActivity = relatedObservedActivity != null && !activityObs.contains(relatedObservedActivity);
        firePropertyChange(PROPERTY_ACTIVITY_OBS, null, activityObs);
        if (isOpened() && removeRelatedObservedActivity) {
            log.info(String.format("%s Removed not matching related observed activity: %s", getPrefix(), relatedObservedActivity));
            getBean().setRelatedObservedActivity(null);
        }
    }

    void updateActivityObsCandidates(ActivityPairingEngine pairingContext) {
        log.debug(String.format("%s Will update release observed activities...", getPrefix()));
        List<ActivityReference> activityObs = pairingContext.getObservationActivitiesCandidates(getBean());
        log.info(String.format("%s Found %d related observed activities.", getPrefix(), activityObs.size()));
        setActivityObs(activityObs);
    }

    public void startEditUI() {
        pairingEngine().ifPresent(this::updateActivityObsCandidates);
    }

    public void stopEditUI() {
        activityObs = new ArrayList<>();
        firePropertyChange(PROPERTY_ACTIVITY_OBS, null, activityObs);
        pairingEngineSupplier.clear();
    }

    private Optional<ActivityPairingEngine> pairingEngine() {
        return Optional.ofNullable(pairingEngineSupplier.get());
    }
}
