package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRequestBuilderStepConfigure;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.sample.SampleUIHelper;
import fr.ird.observe.dto.data.ll.logbook.SampleDto;

/**
 * Created on 8/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class SampleListUIHandler extends GeneratedSampleListUIHandler {

    @Override
    protected void installMoveAction() {
        MoveAction.create(ui)
                .on(SampleUIHelper.newSupplier(this, this, getModel()::getGroupByValue, getModel()::toMoveRequest, this::toMoveRequest))
                .call((r, newParentId, ids) -> getOpenableService().move(SampleDto.class, r))
                .then(SampleUIHelper::create)
                .install(ui::getMove);
    }

    public MoveRequestBuilderStepConfigure toMoveRequest() {
        SampleListUIModel model = getModel();
        return MoveRequestBuilder
                .create(SampleDto.class, model.getStates().selectedParent(), model.getStates().getSelectedDatasLabel())
                .setEditNode(model.getSource().getInitializer().getEditNode())
                .setGroupByValue(model::getGroupByValue);
    }

}
