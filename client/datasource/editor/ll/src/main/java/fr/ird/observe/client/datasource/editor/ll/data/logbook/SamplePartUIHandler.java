package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.AcquisitionMode;
import fr.ird.observe.client.datasource.editor.api.content.data.sample.actions.ResetSizeMeasureType;
import fr.ird.observe.client.datasource.editor.api.content.data.sample.actions.ResetWeightMeasureType;
import fr.ird.observe.dto.data.ll.logbook.SamplePartDto;
import fr.ird.observe.dto.referential.common.SizeMeasureTypeReference;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.common.WeightMeasureTypeReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.event.ChangeEvent;
import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.util.Optional;

/**
 * Created on 8/31/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class SamplePartUIHandler extends GeneratedSamplePartUIHandler {
    private static final Logger log = LogManager.getLogger(SamplePartUIHandler.class);
    /**
     * @since 3.0
     */
    private final PropertyChangeListener weightChanged;

    /**
     * @since 3.0
     */
    private final PropertyChangeListener lengthChanged;

    /**
     * @since 3.0
     */
    private final PropertyChangeListener speciesChanged;

    public SamplePartUIHandler() {
        weightChanged = evt -> onWeightChanged((Float) evt.getNewValue());
        lengthChanged = evt -> onLengthChanged((Float) evt.getNewValue());
        speciesChanged = evt -> onSpeciesChanged((SpeciesReference) evt.getNewValue());
    }

    @Override
    public void onInit(SamplePartUI ui) {
        super.onInit(ui);
        ui.getAcquisitionModeGroup().addChangeListener(this::onAcquisitionModeChanged);
        ui.getSizeMeasureType().getToolbarRight().add(ui.getDefaultSizeMeasureType());
        ui.getWeightMeasureType().getToolbarRight().add(ui.getDefaultWeightMeasureType());
        ResetSizeMeasureType.install(ui, ui.getDefaultSizeMeasureType(), ui.getSizeMeasureType());
        ResetWeightMeasureType.install(ui, ui.getDefaultWeightMeasureType(), ui.getWeightMeasureType());
    }

    protected void onSpeciesChanged(SpeciesReference species) {
        SamplePartUIModel model = getModel();
        Optional<SizeMeasureTypeReference> sizeMeasureType = model.getStates().getSpeciesDefaultSizeMeasureType(species);
        Optional<WeightMeasureTypeReference> weightMeasureType = model.getStates().getSpeciesDefaultWeightMeasureType(species);
        model.getStates().setDefaultSizeMeasureType(sizeMeasureType.orElse(null));
        model.getStates().setDefaultWeightMeasureType(weightMeasureType.orElse(null));
        resetDefaultSizeMeasureType(false);
        resetDefaultWeightMeasureType(false);
    }

    protected void onWeightChanged(Float newValue) {
        SamplePartUIModel model = getModel();
        SamplePartDto tableEditBean = model.getStates().getTableEditBean();
        if (newValue == null) {
            tableEditBean.setWeightMeasureMethod(null);
        } else {
            if (tableEditBean.getWeightMeasureMethod() == null) {
                tableEditBean.setWeightMeasureMethod(model.getStates().getDefaultWeightMeasureMethod());
            }
        }
    }

    protected void onLengthChanged(Float newValue) {
        SamplePartUIModel model = getModel();
        SamplePartDto tableEditBean = model.getStates().getTableEditBean();
        if (newValue == null) {
            tableEditBean.setSizeMeasureMethod(null);
        } else {
            if (tableEditBean.getSizeMeasureMethod() == null) {
                tableEditBean.setSizeMeasureMethod(model.getStates().getDefaultSizeMeasureMethod());
            }
        }
    }

    public void resetDefaultSizeMeasureType(boolean force) {
        ui.getSizeMeasureType().setSelectedItem(null);
        SamplePartUIModel model = getModel();
        if (force || model.getStates().getTableEditBean().getSpecies() != null) {
            ui.getSizeMeasureType().setSelectedItem(model.getStates().getDefaultSizeMeasureType());
        }
    }

    public void resetDefaultWeightMeasureType(boolean force) {
        ui.getWeightMeasureType().setSelectedItem(null);
        SamplePartUIModel model = getModel();
        if (force || (model.getStates().getTableEditBean().getSpecies() != null && model.getStates().getTableEditBean().getAcquisitionMode() == 1)) {
            ui.getWeightMeasureType().setSelectedItem(model.getStates().getDefaultWeightMeasureType());
        }
    }

    /**
     * Le mode de saisie a été mis à jour.
     *
     * @param event le nouveau de mode de saisie à utiliser
     * @since 1.8
     */
    public void onAcquisitionModeChanged(ChangeEvent event) {
        JAXXButtonGroup source = (JAXXButtonGroup) event.getSource();
        AcquisitionMode newMode = (AcquisitionMode) source.getSelectedValue();
        if (newMode == null) {
            // mode null (cela peut arriver avec les bindings)
            return;
        }
        boolean createMode = ui.getTableModel().isCreate();
        SamplePartDto editBean = ui.getModel().getStates().getTableEditBean();
        switch (newMode) {
            case number:
                onAcquisitionModeChangedToNumber(createMode, editBean);
                break;
            case individual:
                onAcquisitionModeChangedToIndividual(createMode, editBean);
                break;
        }
        if (createMode) {
            // on propage le mode de saisie dans le bean
            getModel().getStates().getTableEditBean().setAcquisitionMode(newMode.ordinal());
        }
    }

    protected void updateAcquisitionMode0(AcquisitionMode acquisitionMode) {
        JAXXButtonGroup acquisitionModeGroup = ui.getAcquisitionModeGroup();
        acquisitionModeGroup.setSelectedValue(null);
        acquisitionModeGroup.setSelectedValue(acquisitionMode);
    }

    @Override
    protected Component getFocusComponentOnSelectedRow(SamplePartUI ui, boolean notPersisted, boolean newRow, SamplePartDto tableEditBean, SamplePartDto previousRowBean) {
        if (notPersisted && previousRowBean == null) {
            return ui.getSpecies();
        }
        if (getModel().getStates().getTableEditBean().getAcquisitionMode() == 0) {
            return ui.getCount();
        }
        return ui.getLength();
    }

    public void onSelectedRowChanged(SamplePartDto tableEditBean, SamplePartDto previousRowBean, boolean notPersisted, boolean newRow) {

        onLengthChanged(tableEditBean.getLength());
        onWeightChanged(tableEditBean.getWeight());

        SamplePartUIModel sampleModel = getModel();

        SpeciesReference species = tableEditBean.getSpecies();
        log.info(String.format("%s selected species %s", prefix, species));

        Optional<SizeMeasureTypeReference> defaultSizeMeasureType;
        Optional<WeightMeasureTypeReference> defaultWeightMeasureType;

        Optional<SizeMeasureTypeReference> sizeMeasureType = Optional.empty();
        Optional<WeightMeasureTypeReference> weightMeasureType = Optional.empty();
        if (notPersisted) {
            boolean unsetSpecies = false;
            if (newRow) {
                // use default mode
                AcquisitionMode acquisitionMode = sampleModel.getStates().getDefaultAcquisitionMode();
                if (previousRowBean != null) {

                    // get previous species
                    species = previousRowBean.getSpecies();
                    // get previous acquisition mode
                    acquisitionMode = AcquisitionMode.valueOf(previousRowBean.getAcquisitionMode());
                    // get previous size measure type
                    sizeMeasureType = Optional.ofNullable(previousRowBean.getSizeMeasureType());
                    // get previous weight measure type
                    weightMeasureType = Optional.ofNullable(previousRowBean.getWeightMeasureType());
                } else {
                    unsetSpecies = true;
                }
                updateAcquisitionMode0(acquisitionMode);
            } else {

                sizeMeasureType = Optional.ofNullable(tableEditBean.getSizeMeasureType());
                weightMeasureType = Optional.ofNullable(tableEditBean.getWeightMeasureType());
            }

            // get default size measure type
            defaultSizeMeasureType = sampleModel.getStates().getSpeciesDefaultSizeMeasureType(species);
            defaultWeightMeasureType = sampleModel.getStates().getSpeciesDefaultWeightMeasureType(species);

            if (unsetSpecies) {
                // unset species (this will not set again species in widget)
                species = null;
                sizeMeasureType = defaultSizeMeasureType;
                weightMeasureType = defaultWeightMeasureType;
            }

        } else {
            updateAcquisitionMode0(AcquisitionMode.valueOf(tableEditBean.getAcquisitionMode()));

            // get default size measure type
            defaultSizeMeasureType = sampleModel.getStates().getSpeciesDefaultSizeMeasureType(species);
            defaultWeightMeasureType = sampleModel.getStates().getSpeciesDefaultWeightMeasureType(species);

            species = null;
            sizeMeasureType = Optional.ofNullable(tableEditBean.getSizeMeasureType());
            weightMeasureType = Optional.ofNullable(tableEditBean.getWeightMeasureType());
        }

        // use default size measure type
        sampleModel.getStates().setDefaultSizeMeasureType(defaultSizeMeasureType.orElse(null));
        sampleModel.getStates().setDefaultWeightMeasureType(defaultWeightMeasureType.orElse(null));

        ui.getSizeMeasureType().setSelectedItem(null);
        sizeMeasureType.ifPresent(ui.getSizeMeasureType()::setSelectedItem);

        ui.getWeightMeasureType().setSelectedItem(null);
        weightMeasureType.ifPresent(ui.getWeightMeasureType()::setSelectedItem);

        // Not sure we need to reset any species list, if it never changes :):):)
//        List<SpeciesReference> availableSpecies = sampleModel.getReferenceProvider().getReferentialReferences("species");
//        ui.getSpecies().setData(availableSpecies);

        if (species != null) {
            log.info(String.format("%s Will set species : %s", prefix, species));
            ui.getSpecies().setSelectedItem(null);
            ui.getSpecies().setSelectedItem(species);
        }
    }

    public void stopEditTableEditBean(SamplePartDto tableEditBean) {
        //FIXME Should we have to remove this all the time ? May be a adjusting Flag could do the trick
        tableEditBean.removePropertyChangeListener(SamplePartDto.PROPERTY_WEIGHT, weightChanged);
        tableEditBean.removePropertyChangeListener(SamplePartDto.PROPERTY_LENGTH, lengthChanged);
        tableEditBean.removePropertyChangeListener(SamplePartDto.PROPERTY_SPECIES, speciesChanged);
    }

    public void startEditTableEditBean(SamplePartDto tableEditBean) {
        tableEditBean.addPropertyChangeListener(SamplePartDto.PROPERTY_WEIGHT, weightChanged);
        tableEditBean.addPropertyChangeListener(SamplePartDto.PROPERTY_LENGTH, lengthChanged);
        tableEditBean.addPropertyChangeListener(SamplePartDto.PROPERTY_SPECIES, speciesChanged);
    }

    protected void onAcquisitionModeChangedToNumber(boolean createMode, SamplePartDto editBean) {
        // weight not enabled
        ui.getWeight().setEnabled(false);
        ui.getWeightMeasureMethod().setEnabled(false);
        // count enabled
        ui.getCount().setEnabled(true);
        if (createMode) {
            // delete any weight value
            editBean.setWeight(null);
            editBean.setWeightMeasureType(null);
            editBean.setWeightMeasureMethod(null);
            // delete count (force use to reset it)
            editBean.setCount(null);
        }
    }

    protected void onAcquisitionModeChangedToIndividual(boolean createMode, SamplePartDto editBean) {
        // weight enabled
        ui.getWeight().setEnabled(true);
        ui.getWeightMeasureMethod().setEnabled(true);
        // count not enabled (set to one)
        ui.getCount().setEnabled(false);
        if (createMode) {
            // Always set to one
            editBean.setCount(1);
            editBean.setWeightMeasureType(getModel().getStates().getDefaultWeightMeasureType());
        }
    }

}
