package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.client.datasource.editor.api.content.data.sample.SampleContentTableUIModelStates;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.AcquisitionMode;
import fr.ird.observe.dto.data.ll.logbook.SampleDto;
import fr.ird.observe.dto.data.ll.logbook.SamplePartDto;
import fr.ird.observe.dto.referential.common.SizeMeasureMethodReference;
import fr.ird.observe.dto.referential.common.SexReference;
import fr.ird.observe.dto.referential.common.WeightMeasureMethodReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class SamplePartUIModelStates extends SampleContentTableUIModelStates<SampleDto, SamplePartDto> {
    private final SexReference defaultSex;
    private final SizeMeasureMethodReference defaultSizeMeasureMethod;
    private final WeightMeasureMethodReference defaultWeightMeasureMethod;

    public static SamplePartUIModel create(ActivitySampleUI parentUI) {
        ActivitySampleUINavigationNode parentSource = ActivitySampleUI.getNavigationSource(parentUI);
        SamplePartUINavigationNode node = SamplePartUINavigationNode.create(parentSource::getParentReference);
        node.setParent(parentSource);
        return new SamplePartUIModel(node);
    }

    public SamplePartUIModelStates(GeneratedSamplePartUIModel model) {
        super(model, SampleDto.newDto(new java.util.Date()), SamplePartDto.newDto(new java.util.Date()), model.getSource().getInitializer().getSelectedId(), model.getSource().getInitializer().getScope().isStandalone());
        ObserveSwingDataSource mainDataSource = model.getSource().getContext().getMainDataSource();
        defaultSex = mainDataSource.getReferentialReferenceSet(SexReference.class).tryGetReferenceById(ProtectedIdsLl.LL_LOGBOOK_SAMPLE_PART_DEFAULT_SEX_ID).orElseThrow(IllegalStateException::new);
        defaultSizeMeasureMethod = mainDataSource.getReferentialReferenceSet(SizeMeasureMethodReference.class).tryGetReferenceById(ProtectedIdsLl.LL_LOGBOOK_SAMPLE_PART_DEFAULT_SIZE_MEASURE_METHOD_ID).orElseThrow(IllegalStateException::new);
        defaultWeightMeasureMethod = mainDataSource.getReferentialReferenceSet(WeightMeasureMethodReference.class).tryGetReferenceById(ProtectedIdsLl.LL_LOGBOOK_SAMPLE_PART_DEFAULT_WEIGHT_MEASURE_METHOD_ID).orElseThrow(IllegalStateException::new);
    }

    @Override
    public SamplePartUITableModel getTableModel() {
        return (SamplePartUITableModel) super.getTableModel();
    }

    @Override
    public AcquisitionMode getDefaultAcquisitionMode() {
        return AcquisitionMode.individual;
    }

    public SizeMeasureMethodReference getDefaultSizeMeasureMethod() {
        return defaultSizeMeasureMethod;
    }

    public WeightMeasureMethodReference getDefaultWeightMeasureMethod() {
        return defaultWeightMeasureMethod;
    }

    public SexReference getDefaultSex() {
        return defaultSex;
    }

    @Override
    public void initDefault(SamplePartDto newTableBean) {
        newTableBean.setSex(getDefaultSex());
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(SamplePartDto.PROPERTY_SPECIES, ReferencesFilterHelper.newLlSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListLonglineLogbookSampleId()));
    }
}
