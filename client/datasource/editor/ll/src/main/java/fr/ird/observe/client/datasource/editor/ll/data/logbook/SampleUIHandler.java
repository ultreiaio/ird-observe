package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRequestBuilderStepConfigure;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUIOpenExecutor;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.sample.SampleUIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.data.ll.logbook.SampleDto;
import fr.ird.observe.dto.data.ll.logbook.SamplePartDto;
import fr.ird.observe.dto.decoration.ObserveI18nDecoratorHelper;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;

/**
 * Created on 12/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
class SampleUIHandler extends GeneratedSampleUIHandler {
    @Override
    protected void onBeforeInit(SampleUI ui, ContentUIInitializer<SampleUI> initializer) {
        initializer.registerDependencies(SamplePartUIModel.create(ui));
    }

    @Override
    protected ContentOpenWithValidator<SampleUI> createContentOpen(SampleUI ui) {
        ContentOpenableUIOpenExecutor<SampleDto, SampleUI> executor = new ContentOpenableUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor) {
            @Override
            public DefaultUIInitializerResult init(ContentUIInitializer<SampleUI> initializer) {
                DefaultUIInitializerResult result = super.init(initializer);
                String type = ObserveI18nDecoratorHelper.getType(SamplePartDto.class) + " - ";
                InputMap inputMap = ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
                ActionMap actionMap = ui.getActionMap();

                prefixAction(ui.getSamplePartUI().getDeleteEntry(), type);
                prefixAction(ui.getSamplePartUI().getShowTechnicalInformations(), type);
                registerInnerAction(ui.getSamplePartUI().getResetEntry(), inputMap, actionMap);
                registerInnerAction(ui.getSamplePartUI().getSaveEntry(), inputMap, actionMap);
                registerInnerAction(ui.getSamplePartUI().getSaveAndNewEntry(), inputMap, actionMap);

                addConfigureActions(ui, false);
                initTabUI(ui.getSamplePartUI(), ui.getSamplePartPanel(), 1, d -> ui.getModel().getStates().getBean().setSamplePart(d.getSamplePart()));
                return result;
            }
        };
    }

    @Override
    public void onMainTabChanged(int previousIndex, int selectedIndex) {
        super.onMainTabChanged(previousIndex, selectedIndex);
        SamplePartUI subUi = ui.getSamplePartUI();
        boolean partTab = selectedIndex == 1;
        subUi.getSelectToolbar().setVisible(partTab && subUi.getTableModel().getRowCount() > 1);
        getContentOpen().updateConfigurePopup(ui, true);
        if (partTab) {
            ui.getConfigurePopup().addSeparator();
            getContentOpen().updateConfigurePopup(subUi, false);
        }
    }

    @Override
    protected void installMoveAction() {
        MoveAction.create(ui)
                .on(SampleUIHelper.newSupplier(this, this, getModel()::getGroupByValue, getModel()::toMoveRequest, this::toMoveRequest))
                .call((r, newParentId, ids) -> getOpenableService().move(SampleDto.class, r))
                .then(SampleUIHelper::create)
                .install(ui::getMove);
    }

    public MoveRequestBuilderStepConfigure toMoveRequest() {
        SampleUIModel model = getModel();
        return MoveRequestBuilder
                .create(SampleDto.class, model.getSource().getParent().getParentReference().toLabel(), model.getStates().getBeanLabel())
                .setEditNode(model.getSource().getInitializer().getEditNode())
                .setGroupByValue(model::getGroupByValue);
    }
}
