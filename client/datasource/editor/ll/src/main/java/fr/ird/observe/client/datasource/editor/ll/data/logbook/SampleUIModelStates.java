package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.logbook.SampleDto;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Objects;

@GenerateJavaBeanDefinition
public class SampleUIModelStates extends GeneratedSampleUIModelStates {

    private SamplePartUIModel samplePartUIModel;

    public SampleUIModelStates(GeneratedSampleUIModel model) {
        super(model);
    }

    @Override
    public SampleDto getBeanToSave() {
        SampleDto beanToSave = super.getBeanToSave();
        beanToSave.setSamplePart(getSamplePartUIModel().getStates().getBeanToSave().getSamplePart());
        return beanToSave;
    }

    public SamplePartUIModel getSamplePartUIModel() {
        return samplePartUIModel;
    }

    public void setSamplePartUIModel(SamplePartUIModel samplePartUIModel) {
        this.samplePartUIModel = Objects.requireNonNull(samplePartUIModel);
        firePropertyChange("samplePartUIModel", null, samplePartUIModel);
    }
}
