<!--
  #%L
  ObServe Client :: DataSource :: Editor :: LL
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI
        beanScope="bean" i18n="fr.ird.observe.dto.data.ll.logbook.BaitsCompositionDto"
        superGenericType='SetGlobalCompositionDto, BaitsCompositionDto, SetBaitsCompositionUI'
        implements='fr.ird.observe.client.datasource.editor.api.content.NotStandaloneContentUI&lt;SetGlobalCompositionDto&gt;'>

    <import>
        fr.ird.observe.dto.data.ll.logbook.SetGlobalCompositionDto
        fr.ird.observe.dto.data.ll.logbook.BaitsCompositionDto
        fr.ird.observe.dto.referential.ll.common.BaitTypeReference
        fr.ird.observe.dto.referential.ll.common.BaitSettingStatusReference
        fr.ird.observe.client.datasource.editor.api.content.data.table.*

        org.nuiton.jaxx.widgets.number.NumberEditor
        io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox

        static fr.ird.observe.client.util.UIHelper.getStringValue
        static io.ultreia.java4all.i18n.I18n.n
    </import>

    <SetBaitsCompositionUIModel id='model' initializer='getContextValue(SetBaitsCompositionUIModel.class)'/>
    <SetBaitsCompositionUIModelStates id='states'/>
    <SetBaitsCompositionUITableModel id='tableModel' initializer="getContextValue(SetBaitsCompositionUITableModel.class)"/>
    <SetGlobalCompositionDto id='bean'/>
    <BaitsCompositionDto id='tableEditBean'/>
    <BeanValidator id='validator' autoField='true' context='update-extra-baitsComposition'
                   beanClass='fr.ird.observe.dto.data.ll.logbook.SetGlobalCompositionDto'
                   errorTableModel='{getErrorTableModel()}'>
        <field name="baitsComposition" component="editorPanel"/>
    </BeanValidator>
    <BeanValidator id='validatorTable' autoField='true' context='update' errorTableModel='{getErrorTableModel()}'
                   beanClass='fr.ird.observe.dto.data.ll.logbook.BaitsCompositionDto'/>

    <Table id='editorPanel' fill='both' insets='1' beanScope="tableEditBean">

        <!-- baitType -->
        <row>
            <cell>
                <JLabel id='baitTypeLabel'/>
            </cell>
            <cell weightx='1' anchor='east'>
                <FilterableComboBox id='baitType' genericType='BaitTypeReference'/>
            </cell>
        </row>

        <!-- baitSettingStatus -->
        <row>
            <cell>
                <JLabel id='baitSettingStatusLabel'/>
            </cell>
            <cell weightx='1' anchor='east'>
                <FilterableComboBox id='baitSettingStatus' genericType='BaitSettingStatusReference'/>
            </cell>
        </row>

        <!-- individualSize -->
        <row>
            <cell>
                <JLabel id='individualSizeLabel'/>
            </cell>
            <cell weightx='1' anchor='east'>
                <NumberEditor id='individualSize' styleClass="int6"/>
            </cell>
        </row>

        <!-- individualWeight -->
        <row>
            <cell>
                <JLabel id='individualWeightLabel'/>
            </cell>
            <cell weightx='1' anchor='east'>
                <NumberEditor id='individualWeight' styleClass="float3"/>
            </cell>
        </row>

        <!-- proportion -->
        <row>
            <cell>
                <JLabel id='proportionLabel'/>
            </cell>
            <cell weightx='1' anchor='east'>
                <NumberEditor id='proportion' styleClass="int6"/>
            </cell>
        </row>

    </Table>

    <Table id='extraZone' fill='both'>

        <!-- proportion sum -->
        <row>
            <cell anchor="west">
                <JLabel id='baitsCompositionProportionSumLabel'/>
            </cell>
            <cell weightx="0.05">
                <JLabel id='baitsCompositionProportionSum'/>
            </cell>
            <cell weightx="0.95">
                <JPanel/>
            </cell>
        </row>

    </Table>

</fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI>
