package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.ll.logbook.BranchlinesCompositionDto;
import fr.ird.observe.dto.referential.ll.common.LineTypeReference;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class SetBranchlinesCompositionUIModelStates extends GeneratedSetBranchlinesCompositionUIModelStates {
    private final LineTypeReference defaultBranchlinesCompositionTopType;
    private final LineTypeReference defaultBranchlinesCompositionTracelineType;
    private final int defaultBranchlinesCompositionProportion;

    public SetBranchlinesCompositionUIModelStates(GeneratedSetBranchlinesCompositionUIModel model) {
        super(model);
        ObserveSwingDataSource mainDataSource = model.getSource().getContext().getMainDataSource();
        defaultBranchlinesCompositionTopType = mainDataSource.getReferentialReferenceSet(LineTypeReference.class).tryGetReferenceById(ProtectedIdsLl.LL_LOGBOOK_BRANCHLINE_COMPOSITION_TOP_TYPE_ID).orElseThrow(IllegalStateException::new);
        defaultBranchlinesCompositionTracelineType = mainDataSource.getReferentialReferenceSet(LineTypeReference.class).tryGetReferenceById(ProtectedIdsLl.LL_LOGBOOK_BRANCHLINE_COMPOSITION_TRACELINE_TYPE_ID).orElseThrow(IllegalStateException::new);
        defaultBranchlinesCompositionProportion = 100;
    }

    @Override
    public void initDefault(BranchlinesCompositionDto newTableBean) {
        newTableBean.setProportion(getDefaultBranchlinesCompositionProportion());
        newTableBean.setTopType(getDefaultBranchlinesCompositionTopType());
        newTableBean.setTracelineType(getDefaultBranchlinesCompositionTracelineType());
    }

    public LineTypeReference getDefaultBranchlinesCompositionTopType() {
        return defaultBranchlinesCompositionTopType;
    }

    public LineTypeReference getDefaultBranchlinesCompositionTracelineType() {
        return defaultBranchlinesCompositionTracelineType;
    }

    public int getDefaultBranchlinesCompositionProportion() {
        return defaultBranchlinesCompositionProportion;
    }
}
