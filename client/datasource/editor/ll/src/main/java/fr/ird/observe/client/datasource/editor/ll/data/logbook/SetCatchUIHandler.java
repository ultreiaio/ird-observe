package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.CatchAcquisitionMode;
import fr.ird.observe.dto.data.ll.logbook.CatchDto;
import fr.ird.observe.dto.referential.ll.common.CatchFateReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JComponent;
import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * Created on 9/11/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class SetCatchUIHandler extends GeneratedSetCatchUIHandler {

    private static final Logger log = LogManager.getLogger(SetCatchUIHandler.class);

    final PropertyChangeListener catchFateChanged;
    final PropertyChangeListener depredatedChanged;

    SetCatchUIHandler() {
        catchFateChanged = evt -> onCatchFateChanged((CatchFateReference) evt.getNewValue());
        depredatedChanged = evt -> onDepredatedChanged((Boolean) evt.getNewValue(), (CatchDto) evt.getSource());
    }

    @Override
    protected Component getFocusComponentOnSelectedRow(SetCatchUI ui, boolean notPersisted, boolean newRow, CatchDto tableEditBean, CatchDto previousRowBean) {
        JComponent requestFocus;
        CatchAcquisitionMode acquisitionModeEnum = CatchAcquisitionMode.valueOf(tableEditBean.getAcquisitionMode());
        if (newRow) {
            // go back to first pane
            ui.getMainTabbedPane().setSelectedIndex(0);
            requestFocus = ui.getSpecies();
        } else {
            if (acquisitionModeEnum.equals(CatchAcquisitionMode.GROUPED)) {
                requestFocus = ui.getCount();
            } else {
                requestFocus = ui.getCatchHealthStatus();
            }
        }
        return requestFocus;
    }

    void updateCatchAcquisitionMode(CatchAcquisitionMode newMode) {
        log.debug(String.format("%s Change CatchAcquisitionMode %s", prefix, newMode));
        if (newMode == null) {
            // mode null (cela peut arriver avec les bindings)
            return;
        }
        boolean createMode = ui.getTableModel().isCreate();
        CatchDto editBean = ui.getTableEditBean();
        if (createMode) {
            editBean.setAcquisitionMode(newMode.ordinal());
        }
        switch (newMode) {
            case GROUPED:
                if (createMode) {
                    editBean.setCount(null);
                    editBean.setTagNumber(null);
                    editBean.setNumber(null);
                }
                break;
            case INDIVIDUAL:
                if (createMode) {
                    // This is the only possible value
                    editBean.setCount(1);
                }
                break;
        }

        boolean isGrouped = CatchAcquisitionMode.GROUPED.equals(newMode);
        boolean isIndividual = !isGrouped;
        ui.getCount().setEnabled(isGrouped);
        ui.getNumber().setEnabled(isIndividual);
        ui.getTagNumber().setEnabled(isIndividual);
    }

    void onCatchFateChanged(CatchFateReference newValue) {
        if (newValue == null || !ProtectedIdsLl.LL_LOGBOOK_CATCH_DISCARDED_CATCH_FATE_ID.equals(newValue.getId())) {
            // not discarded
            ui.getDiscardHealthStatus().setEnabled(false);
            ui.getHookWhenDiscarded().setEnabled(false);
            CatchDto tableEditBean = getModel().getStates().getTableEditBean();
            tableEditBean.setHookWhenDiscarded(null);
            tableEditBean.setDiscardHealthStatus(null);
        } else {
            // discarded
            ui.getDiscardHealthStatus().setEnabled(true);
            ui.getHookWhenDiscarded().setEnabled(true);
        }
    }

    void onDepredatedChanged(Boolean newValue, CatchDto tableEditBean) {
        if (Objects.equals(true, newValue)) {
            // depredated
            ui.getBeatDiameter().setEnabled(true);
            ui.getPredator().setEnabled(true);
            ui.getCountDepredated().setEnabled(true);
            ui.getDepredatedProportion().setEnabled(true);
        } else {
            // not depredated
            ui.getBeatDiameter().setEnabled(false);
            ui.getPredator().setEnabled(false);
            ui.getCountDepredated().setEnabled(false);
            ui.getDepredatedProportion().setEnabled(false);
            tableEditBean.setBeatDiameter(null);
            tableEditBean.setPredator(null);
            tableEditBean.setDepredatedProportion(null);
            tableEditBean.setCountDepredated(null);
        }
    }

}
