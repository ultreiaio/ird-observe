package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.CatchAcquisitionMode;
import fr.ird.observe.dto.data.ll.logbook.CatchDto;
import fr.ird.observe.dto.referential.ll.common.CatchFateReference;
import fr.ird.observe.dto.referential.ll.common.HealthStatusReference;
import fr.ird.observe.dto.referential.ps.common.SpeciesFateReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class SetCatchUIModelStates extends GeneratedSetCatchUIModelStates {

    private final HealthStatusReference defaultCatchHealthStatus;

    public SetCatchUIModelStates(GeneratedSetCatchUIModel model) {
        super(model);
        ObserveSwingDataSource mainDataSource = model.getSource().getContext().getMainDataSource();
        this.defaultCatchHealthStatus = mainDataSource.getReferentialReferenceSet(HealthStatusReference.class).tryGetReferenceById(ProtectedIdsLl.LL_LOGBOOK_CATCH_LONGLINE_DEFAULT_CATCH_HEALTH_STATUS_ID).orElseThrow(IllegalStateException::new);
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(CatchDto.PROPERTY_CATCH_FATE, ReferencesFilterHelper.newPredicateList(CatchFateReference::isLogbook));
        referenceCache.addReferentialFilter(CatchDto.PROPERTY_SPECIES, ReferencesFilterHelper.newLlSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListLonglineLogbookCatchId()));
        referenceCache.addReferentialFilter(CatchDto.PROPERTY_PREDATOR, ReferencesFilterHelper.newLlSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListLonglineDepredatorId()));
    }

    @Override
    public void initDefault(CatchDto newTableBean) {
        newTableBean.setCatchHealthStatus(getDefaultCatchHealthStatus());
        newTableBean.setAcquisitionMode(CatchAcquisitionMode.GROUPED.ordinal());
        newTableBean.setDepredated(false);
    }

    public HealthStatusReference getDefaultCatchHealthStatus() {
        return defaultCatchHealthStatus;
    }
}
