package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.CatchAcquisitionMode;
import fr.ird.observe.dto.data.ll.logbook.CatchDto;

/**
 * Created on 12/4/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public class SetCatchUITableModel extends GeneratedSetCatchUITableModel {

    SetCatchUITableModel(SetCatchUI context) {
        super(context);
    }

    @Override
    protected void onSelectedRowChanged(SetCatchUI ui, int editingRow, CatchDto tableEditBean, CatchDto previousRowBean, boolean notPersisted, boolean newRow) {
        CatchAcquisitionMode acquisitionModeEnum = CatchAcquisitionMode.valueOf(tableEditBean.getAcquisitionMode());
        SetCatchUIHandler handler = ui.getHandler();
        if (newRow) {
//            ui.getPredator().setEnabled(false);
//            ui.getDiscardHealthStatus().setEnabled(false);
//            ui.getBeatDiameter().setEnabled(false);
        } else {

            //FIXME Voir si pas besoin aussi de relancer les binding (catchFateChanged, branchlineChanged) ?
//            handler.onDepredatedChanged(tableEditBean.isDepredated(), tableEditBean);
//            handler.onCatchFateChanged(tableEditBean.getCatchFate());
        }

        ui.getAcquisitionModeGroup().setSelectedValue(null);
        ui.getAcquisitionModeGroup().setSelectedValue(acquisitionModeEnum);


        tableEditBean.removePropertyChangeListener(CatchDto.PROPERTY_CATCH_FATE, handler.catchFateChanged);
        tableEditBean.addPropertyChangeListener(CatchDto.PROPERTY_CATCH_FATE, handler.catchFateChanged);

        tableEditBean.removePropertyChangeListener(CatchDto.PROPERTY_DEPREDATED, handler.depredatedChanged);
        tableEditBean.addPropertyChangeListener(CatchDto.PROPERTY_DEPREDATED, handler.depredatedChanged);

        super.onSelectedRowChanged(ui, editingRow, tableEditBean, previousRowBean, notPersisted, newRow);
    }

    @Override
    protected void onAfterLoadRowBeanToEdit(int editingRow, boolean newRow) {
        super.onAfterLoadRowBeanToEdit(editingRow, newRow);
//        if (!newRow) {
            SetCatchUIHandler handler = getContext().getHandler();
            CatchDto tableEditBean = getTableEditBean();
            handler.onDepredatedChanged(tableEditBean.getDepredated(), tableEditBean);
            handler.onCatchFateChanged(tableEditBean.getCatchFate());
//        }
    }
}
