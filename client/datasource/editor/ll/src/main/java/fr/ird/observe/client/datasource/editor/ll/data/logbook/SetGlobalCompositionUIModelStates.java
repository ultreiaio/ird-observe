package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.logbook.SetGlobalCompositionDto;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class SetGlobalCompositionUIModelStates extends GeneratedSetGlobalCompositionUIModelStates {

    private SetBaitsCompositionUIModel baitsCompositionModel;
    private SetBranchlinesCompositionUIModel branchlinesCompositionModel;
    private SetFloatlinesCompositionUIModel floatlinesCompositionModel;
    private SetHooksCompositionUIModel hooksCompositionModel;

    public SetGlobalCompositionUIModelStates(GeneratedSetGlobalCompositionUIModel model) {
        super(model);
    }

    public SetBaitsCompositionUIModel getBaitsCompositionModel() {
        return baitsCompositionModel;
    }

    public void setBaitsCompositionModel(SetBaitsCompositionUIModel baitsCompositionModel) {
        SetBaitsCompositionUIModel oldValue = this.baitsCompositionModel;
        this.baitsCompositionModel = baitsCompositionModel;
        firePropertyChange("baitsCompositionModel", oldValue, baitsCompositionModel);
    }

    public SetBranchlinesCompositionUIModel getBranchlinesCompositionModel() {
        return branchlinesCompositionModel;
    }

    public void setBranchlinesCompositionModel(SetBranchlinesCompositionUIModel branchlinesCompositionModel) {
        SetBranchlinesCompositionUIModel oldValue = this.branchlinesCompositionModel;
        this.branchlinesCompositionModel = branchlinesCompositionModel;
        firePropertyChange("branchlinesCompositionModel", oldValue, branchlinesCompositionModel);
    }

    public SetFloatlinesCompositionUIModel getFloatlinesCompositionModel() {
        return floatlinesCompositionModel;
    }

    public void setFloatlinesCompositionModel(SetFloatlinesCompositionUIModel floatlinesCompositionModel) {
        SetFloatlinesCompositionUIModel oldValue = this.floatlinesCompositionModel;
        this.floatlinesCompositionModel = floatlinesCompositionModel;
        firePropertyChange("floatlinesCompositionModel", oldValue, floatlinesCompositionModel);
    }

    public SetHooksCompositionUIModel getHooksCompositionModel() {
        return hooksCompositionModel;
    }

    public void setHooksCompositionModel(SetHooksCompositionUIModel hooksCompositionModel) {
        SetHooksCompositionUIModel oldValue = this.hooksCompositionModel;
        this.hooksCompositionModel = hooksCompositionModel;
        firePropertyChange("hooksCompositionModel", oldValue, hooksCompositionModel);
    }

    @Override
    public SetGlobalCompositionDto getBeanToSave() {
        SetGlobalCompositionDto bean = super.getBeanToSave();
        bean.setFloatlinesComposition(getFloatlinesCompositionModel().getStates().getBeanToSave().getFloatlinesComposition());
        bean.setBranchlinesComposition(getBranchlinesCompositionModel().getStates().getBeanToSave().getBranchlinesComposition());
        bean.setHooksComposition(getHooksCompositionModel().getStates().getBeanToSave().getHooksComposition());
        bean.setBaitsComposition(getBaitsCompositionModel().getStates().getBeanToSave().getBaitsComposition());
        return bean;
    }
}
