package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.ll.logbook.HooksCompositionDto;
import fr.ird.observe.dto.referential.ll.common.HookTypeReference;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class SetHooksCompositionUIModelStates extends GeneratedSetHooksCompositionUIModelStates {

    private final HookTypeReference defaultHooksCompositionType;
    private final int defaultHooksCompositionProportion;

    public SetHooksCompositionUIModelStates(GeneratedSetHooksCompositionUIModel model) {
        super(model);
        ObserveSwingDataSource mainDataSource = model.getSource().getContext().getMainDataSource();
        defaultHooksCompositionType = mainDataSource.getReferentialReferenceSet(HookTypeReference.class).tryGetReferenceById(ProtectedIdsLl.LL_LOGBOOK_HOOKS_COMPOSITION_TYPE_ID).orElseThrow(IllegalStateException::new);
        defaultHooksCompositionProportion = 100;
    }

    @Override
    public void initDefault(HooksCompositionDto newTableBean) {
        newTableBean.setProportion(getDefaultHooksCompositionProportion());
        newTableBean.setHookType(getDefaultHooksCompositionType());
    }

    public HookTypeReference getDefaultHooksCompositionType() {
        return defaultHooksCompositionType;
    }

    public int getDefaultHooksCompositionProportion() {
        return defaultHooksCompositionProportion;
    }
}
