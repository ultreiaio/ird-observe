/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#states {
  modified:{validator.isChanged()};
  valid:{validator.isValid()};
}

DateTimeEditor {
  showReset:true;
}

#copyFirstValuesAndSave {
  enabled:{!states.isReadingMode() && states.getCopyFirstSetCandidate() != null && states.isValid()};
  text:"observe.data.ll.logbook.Set.action.copyFirstValuesAndSave";
  toolTipText:"observe.data.ll.logbook.Set.action.copyFirstValuesAndSave";
  _keyStroke:{fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes.KEY_STROKE_COPY_FIRST_VALUES_AND_SAVE};
}

#copyLastValuesAndSave {
  enabled:{!states.isReadingMode() && states.getCopyLastSetCandidate() != null && states.isValid()};
  text:"observe.data.ll.logbook.Set.action.copyLastValuesAndSave";
  toolTipText:"observe.data.ll.logbook.Set.action.copyLastValuesAndSave";
  _keyStroke:{fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes.KEY_STROKE_COPY_LAST_VALUES_AND_SAVE};
}

#shooterSpeed {
  enabled:{Objects.equals(true, bean.getShooterUsed())};
}

#swivelWeight {
  enabled:{Objects.equals(true, bean.getWeightedSwivel())};
}

#snapWeight {
  enabled:{Objects.equals(true, bean.getWeightedSnap())};
}
