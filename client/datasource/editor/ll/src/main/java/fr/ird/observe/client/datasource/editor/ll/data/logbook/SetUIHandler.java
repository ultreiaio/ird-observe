package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.CopyCoordinate;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveAction;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.actions.SaveContentEditUIAdapter;
import fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes;
import fr.ird.observe.client.datasource.editor.ll.data.common.TripUINavigationNode;
import fr.ird.observe.dto.data.ll.common.TripReference;
import fr.ird.observe.dto.data.ll.logbook.SetDto;
import io.ultreia.java4all.i18n.I18n;

import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * Created on 9/1/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
class SetUIHandler extends GeneratedSetUIHandler {

    private final PropertyChangeListener lightsticksUsedChanged;
    private final PropertyChangeListener shooterUsedChanged;
    private final PropertyChangeListener weightedSwivelChanged;
    private final PropertyChangeListener weightedSnapChanged;

    SetUIHandler() {
        lightsticksUsedChanged = evt -> onLightsticksUsedChanged((Boolean) evt.getNewValue());
        shooterUsedChanged = evt -> onShooterUsedChanged((Boolean) evt.getNewValue());
        weightedSwivelChanged = evt -> onWeightedSwivelChanged((Boolean) evt.getNewValue());
        weightedSnapChanged = evt -> onWeightedSnapChanged((Boolean) evt.getNewValue());
    }

    @Override
    protected void installSaveAction() {
        SaveAction
                .create(ui, SetDto.class)
                .on(ui.getModel()::toSaveRequest)
                .call((r, d) -> getEditableService().save(r.getParentId(), d))
                .then(new SetDtoSetUISaveContentEditUIAdapter())
                .install(ui.getSave());
        SaveAction.create(ui, SetDto.class)
                  .on(ui.getModel().getStates()::toSaveRequestWithFirstCopy)
                  .call((r, d) -> getLlCommonTripService().saveAndCopyProperties(ui.getModel().getSource().getInitializer().getSelectedParentId(), r.getParentId(), d))
                  .then(new SetDtoSetUISaveContentEditUIAdapter())
                  .install(ui.getCopyFirstValuesAndSave());
        SaveAction.create(ui, SetDto.class)
                  .on(ui.getModel().getStates()::toSaveRequestWithLastCopy)
                  .call((r, d) -> getLlCommonTripService().saveAndCopyProperties(ui.getModel().getSource().getInitializer().getSelectedParentId(), r.getParentId(), d))
                  .then(new SetDtoSetUISaveContentEditUIAdapter())
                  .install(ui.getCopyLastValuesAndSave());
    }

    @Override
    protected void installExtraActions() {
        super.installExtraActions();
        CopyCoordinate.install(I18n.t("observe.ui.action.copySettingStartCoordinate"),
                               ObserveLLKeyStrokes.KEY_STROKE_COPY_SETTING_START_COORDINATE,
                               ui,
                               ui.getSettingStart(),
                               ui.getSettingEnd(),
                               ui.getCopySettingStartCoordinate());
        CopyCoordinate.install(I18n.t("observe.ui.action.copySettingEndCoordinate"),
                               ObserveLLKeyStrokes.KEY_STROKE_COPY_SETTING_END_COORDINATE,
                               ui,
                               ui.getSettingEnd(),
                               ui.getHaulingStart(),
                               ui.getCopySettingEndCoordinate());
        CopyCoordinate.install(I18n.t("observe.ui.action.copyHaulingStartCoordinate"),
                               ObserveLLKeyStrokes.KEY_STROKE_COPY_HAULING_START_COORDINATE,
                               ui,
                               ui.getHaulingStart(),
                               ui.getHaulingEnd(),
                               ui.getCopyHaulingStartCoordinate());
    }
    //FIXME:Focus getHomeId/getSettingVesselSpeed/getLightsticksUsed/getHaulingDirectionSameAsSetting/

    @Override
    public void startEditUI() {

        SetUIModel model = getModel();
        SetDto bean = model.getStates().getBean();
        bean.removePropertyChangeListener(SetDto.PROPERTY_LIGHTSTICKS_USED, lightsticksUsedChanged);
        bean.removePropertyChangeListener(SetDto.PROPERTY_SHOOTER_USED, shooterUsedChanged);
        bean.removePropertyChangeListener(SetDto.PROPERTY_WEIGHTED_SNAP, weightedSnapChanged);
        bean.removePropertyChangeListener(SetDto.PROPERTY_WEIGHTED_SWIVEL, weightedSwivelChanged);

        super.startEditUI();

        bean.addPropertyChangeListener(SetDto.PROPERTY_LIGHTSTICKS_USED, lightsticksUsedChanged);
        bean.addPropertyChangeListener(SetDto.PROPERTY_SHOOTER_USED, shooterUsedChanged);
        bean.addPropertyChangeListener(SetDto.PROPERTY_WEIGHTED_SNAP, weightedSnapChanged);
        bean.addPropertyChangeListener(SetDto.PROPERTY_WEIGHTED_SWIVEL, weightedSwivelChanged);

        onLightsticksUsedChanged(bean.getLightsticksUsed());
        onShooterUsedChanged(bean.getShooterUsed());
        onWeightedSnapChanged(bean.getWeightedSnap());
        onWeightedSwivelChanged(bean.getWeightedSwivel());
        model.getStates().setModified(model.getStates().isCreatingMode());
    }

    private void onLightsticksUsedChanged(Boolean newValue) {

        SetUI ui = getUi();

        SetDto tableEditBean = getModel().getStates().getBean();
        if (Objects.equals(true, newValue)) {

            // with lightsticks
            ui.getTotalLightsticksCount().setEnabled(true);
            ui.getLightsticksType().setEnabled(true);
            ui.getLightsticksColor().setEnabled(true);
            ui.getLightsticksPerBasketCount().setEnabled(true);
        } else {

            // without lightsticks
            ui.getTotalLightsticksCount().setEnabled(false);
            ui.getLightsticksType().setEnabled(false);
            ui.getLightsticksColor().setEnabled(false);
            ui.getLightsticksPerBasketCount().setEnabled(false);
            tableEditBean.setTotalLightsticksCount(null);
            tableEditBean.setLightsticksType(null);
            tableEditBean.setLightsticksColor(null);
            tableEditBean.setLightsticksPerBasketCount(null);
        }
    }

    private void onShooterUsedChanged(Boolean newValue) {
        if (!Objects.equals(true, newValue)) {
            getModel().getStates().getBean().setShooterSpeed(null);
        }
    }

    private void onWeightedSnapChanged(Boolean newValue) {
        if (!Objects.equals(true, newValue)) {
            getModel().getStates().getBean().setSnapWeight(null);
        }
    }

    private void onWeightedSwivelChanged(Boolean newValue) {
        if (!Objects.equals(true, newValue)) {
            getModel().getStates().getBean().setSwivelWeight(null);
        }
    }

    private static class SetDtoSetUISaveContentEditUIAdapter extends SaveContentEditUIAdapter<SetDto, SetUI> implements WithClientUIContextApi {
        @Override
        public void adaptUi(DataSourceEditor dataSourceEditor, SetUI ui, boolean notPersisted, SetDto savedBean) {

            // reload trip data then default save
            TripUINavigationNode parentNode = (TripUINavigationNode) ui.getModel().getSource().upToReferenceNode(TripReference.class);

            getClientValidationContext().reset();
            parentNode.nodeChanged();
            super.adaptUi(dataSourceEditor, ui, notPersisted, savedBean);
        }
    }
}
