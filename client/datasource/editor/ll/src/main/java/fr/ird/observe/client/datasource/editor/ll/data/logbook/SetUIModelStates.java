package fr.ird.observe.client.datasource.editor.ll.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveRequest;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.ll.logbook.SetDto;
import fr.ird.observe.dto.data.ll.logbook.SetStubDto;
import fr.ird.observe.dto.form.Form;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@GenerateJavaBeanDefinition
public class SetUIModelStates extends GeneratedSetUIModelStates {

    private List<SetStubDto> copySetCandidates;
    private SetStubDto copyFirstSetCandidate;
    private SetStubDto copyLastSetCandidate;

    public SetUIModelStates(GeneratedSetUIModel model) {
        super(model);
        getBean().addPropertyChangeListener(SetDto.PROPERTY_SETTING_START_TIME_STAMP, e -> updateCopySetCandidate((Date) e.getNewValue()));
    }

    @Override
    protected void copyFormToBean(Form<SetDto> form) {
        copySetCandidates = form.getObject().getOtherSets().stream()
                .sorted(Comparator.comparing(SetStubDto::getSettingStartTimeStamp))
                .collect(Collectors.toList());
        updateCopySetCandidate(null);
        super.copyFormToBean(form);
    }

    public SetStubDto getCopyLastSetCandidate() {
        return copyLastSetCandidate;
    }

    public void setCopyLastSetCandidate(SetStubDto copyLastSetCandidate) {
        SetStubDto oldValue = getCopyLastSetCandidate();
        this.copyLastSetCandidate = copyLastSetCandidate;
        firePropertyChange("copyLastSetCandidate", oldValue, copyLastSetCandidate);
    }

    public SetStubDto getCopyFirstSetCandidate() {
        return copyFirstSetCandidate;
    }

    public void setCopyFirstSetCandidate(SetStubDto copyFirstSetCandidate) {
        SetStubDto oldValue = getCopyFirstSetCandidate();
        this.copyFirstSetCandidate = copyFirstSetCandidate;
        firePropertyChange("copyFirstSetCandidate", oldValue, copyFirstSetCandidate);
    }

    public SaveRequest<SetDto> toSaveRequestWithLastCopy() {
        return SaveRequest.create(setLastIdToCopy(), getSelectedId(), () -> getForm().getObject(), this::getBeanToSave, savePredicate());
    }

    public SaveRequest<SetDto> toSaveRequestWithFirstCopy() {
        return SaveRequest.create(setFirstIdToCopy(), getSelectedId(), () -> getForm().getObject(), this::getBeanToSave, savePredicate());
    }

    private ToolkitIdDtoBean setFirstIdToCopy() {
        return copyFirstSetCandidate == null ? null : copyFirstSetCandidate.toShortDto();
    }
    private ToolkitIdDtoBean setLastIdToCopy() {
        return copyLastSetCandidate == null ? null : copyLastSetCandidate.toShortDto();
    }

    private void updateCopySetCandidate(Date newValue) {
        if (isReadingMode()) {
            return;
        }
        setCopyFirstSetCandidate(null);
        setCopyLastSetCandidate(null);
        if (newValue == null) {
            return;
        }
        List<SetStubDto> collect = copySetCandidates.stream().filter(e -> e.getSettingStartTimeStamp().before(newValue)).collect(Collectors.toList());
        if (collect.isEmpty()) {
            return;
        }
        setCopyFirstSetCandidate(collect.get(0));
        setCopyLastSetCandidate(collect.get(collect.size() - 1));
    }
}
