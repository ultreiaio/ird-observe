package fr.ird.observe.client.datasource.editor.ll.data.logbook.sample;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveTreeAdapter;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.ActivitySampleUINavigationNode;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.ActivityUINavigationNode;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.SampleListUINavigationNode;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.SampleUINavigationNode;

/**
 * Created on 12/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public class ActivitySampleUIMoveTreeAdapterToTrip extends MoveTreeAdapter<ActivityUINavigationNode, SampleListUINavigationNode, SampleUINavigationNode> {

    public ActivitySampleUIMoveTreeAdapterToTrip(ActivitySampleUINavigationNode incomingNode) {
        super(incomingNode, e -> incomingNode.getParent());
    }

    @Override
    public SampleListUINavigationNode getNewParentNode(ActivityUINavigationNode oldParentNode, String newParentId) {
        return oldParentNode.getParent().getParent().getSampleListUINavigationNode();
    }

    @Override
    public SampleUINavigationNode getNewSelectedNodeFromNewParentNode(SampleListUINavigationNode newParentNode, String newId) {
        return newParentNode.getSampleUINavigationNode(newId);
    }

}
