package fr.ird.observe.client.datasource.editor.ll.data.logbook.sample;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRequestBuilderStepBuild;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRequestBuilderStepConfigure;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveTreeAdapter;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.ActivitySampleUI;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.SampleListUI;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.SampleListUIHandler;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.SampleUI;
import fr.ird.observe.client.datasource.editor.ll.data.logbook.SampleUIMoveTreeAdapter;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.dto.data.ll.logbook.ActivityDto;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.data.MoveRequest;
import io.ultreia.java4all.i18n.I18n;

import javax.swing.JOptionPane;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created on 08/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class SampleUIHelper {

    public static boolean toTrip(MoveRequest request) {
        return TripAware.class.isAssignableFrom(request.getParentTargetDtoType());
    }

    public static SampleMoveChoice movetoTrip(WithClientUIContextApi context) {
        if (context instanceof SampleListUIHandler && !((SampleListUIHandler) context).getUi().getModel().getStates().isOneSelectedData()) {
            int response = context.askToUser(
                    I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.target.title"),
                    I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.target.multiple.message"),
                    JOptionPane.QUESTION_MESSAGE, new String[]{
                            I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.target.trip"),
                            I18n.t("observe.ui.action.cancel")
                    }, 0);
            return response == 0 ? SampleMoveChoice.MOVE_TO_TRIP : SampleMoveChoice.CANCEL;
        } else {
            int response = context.askToUser(
                    I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.target.title"),
                    I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.target.message"),
                    JOptionPane.QUESTION_MESSAGE, new String[]{
                            I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.target.trip"),
                            I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.target.activity"),
                            I18n.t("observe.ui.action.cancel")
                    }, 0);
            switch (response) {
                case 0:
                    return SampleMoveChoice.MOVE_TO_TRIP;
                case 1:
                    return SampleMoveChoice.MOVE_TO_ACTIVITY;
                default:
                    return SampleMoveChoice.CANCEL;
            }
        }
    }

    public static Supplier<MoveRequestBuilderStepBuild> newSupplier(WithClientUIContextApi context, ObserveServicesProvider servicesProvider, Supplier<DataGroupByParameter> groupBySupplier, Supplier<MoveRequestBuilderStepConfigure> rootRequestBuilder, Supplier<MoveRequestBuilderStepConfigure> activityRequestBuilder) {
        return () -> {
            SampleMoveChoice moveToTrip = movetoTrip(context);
            switch (moveToTrip) {
                case CANCEL:
                    return null;
                case MOVE_TO_TRIP:
                    return rootRequestBuilder.get()
                            .setParentTargetDtoType(TripDto.class)
                            .setGroupByValue(groupBySupplier)
                            .setParentCandidates((groupBy, id) -> servicesProvider.getRootOpenableService().getBrothers(groupBy, id));
                case MOVE_TO_ACTIVITY:
                    return activityRequestBuilder.get()
                            .setParentTargetDtoType(ActivityDto.class)
                            .setGroupByValue(groupBySupplier)
                            .setAskNewParentTitle(I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.activity.title"))
                            .setAskNewParentMessage(I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.activity.message"))
                            .setParentCandidates((groupBy, id) -> servicesProvider.getLlCommonTripService().getSampleActivityParentCandidate(id, null));
            }
            throw new IllegalStateException();
        };
    }

    public static Supplier<MoveRequestBuilderStepBuild> newSupplier(ActivitySampleUI ui, Supplier<MoveRequestBuilderStepConfigure> rootRequestBuilder, Supplier<MoveRequestBuilderStepConfigure> activityRequestBuilder) {
        return () -> {
            WithClientUIContextApi context = ui.getHandler();
            ObserveServicesProvider servicesProvider = ui.getHandler();
            SampleMoveChoice moveToTrip = movetoTrip(context);
            switch (moveToTrip) {
                case CANCEL:
                    return null;
                case MOVE_TO_TRIP:
                    return rootRequestBuilder.get()
                            .setParentTargetDtoType(TripDto.class)
                            .setGroupByValue(ui.getModel()::getGroupByValue)
                            // parent candidates are all trip from the groupBy
                            .setParentCandidates((groupBy, id) -> servicesProvider.getRootOpenableService().getBrothers(groupBy, null));
                case MOVE_TO_ACTIVITY:
                    return activityRequestBuilder.get()
                            .setParentTargetDtoType(ActivityDto.class)
                            .setGroupByValue(ui.getModel()::getGroupByValue)
                            .setAskNewParentTitle(I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.activity.title"))
                            .setAskNewParentMessage(I18n.t("observe.data.ll.logbook.Sample.action.move.choose.parent.activity.message"))
                            .setParentCandidates((groupBy, id) -> servicesProvider.getLlCommonTripService().getSampleActivityParentCandidate(ui.getModel().getSource().getParent().getInitializer().getSelectedParentId(), id));
            }
            throw new IllegalStateException();
        };
    }

    public static Function<MoveRequest, ? extends MoveTreeAdapter<?, ?, ?>> create(ActivitySampleUI ui) {
        return r -> toTrip(r)
                ? new ActivitySampleUIMoveTreeAdapterToTrip(ui.getModel().getSource())
                : new ActivitySampleUIMoveTreeAdapter(ui.getModel().getSource());
    }

    public static Function<MoveRequest, ? extends MoveTreeAdapter<?, ?, ?>> create(SampleListUI ui) {
        return r -> toTrip(r)
                ? new SampleUIMoveTreeAdapter(ui.getModel().getSource())
                : new SampleUIMoveTreeAdapterToActivity(ui.getModel().getSource());
    }

    public static Function<MoveRequest, ? extends MoveTreeAdapter<?, ?, ?>> create(SampleUI ui) {
        return r -> toTrip(r)
                ? new SampleUIMoveTreeAdapter(ui.getModel().getSource())
                : new SampleUIMoveTreeAdapterToActivity(ui.getModel().getSource());
    }
}
