package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.reset.DefaultResetAdapter;
import fr.ird.observe.client.datasource.editor.api.content.actions.reset.ResetAction;
import fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes;
import fr.ird.observe.client.datasource.editor.ll.data.observation.branchline.SaveBranchline;
import fr.ird.observe.client.datasource.editor.ll.data.observation.branchline.SetDefaultTimerTimeOnBoard;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.CatchAcquisitionMode;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineReference;
import fr.ird.observe.dto.data.ll.observation.CatchDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ll.common.CatchFateReference;
import fr.ird.observe.services.service.SaveResultDto;
import io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.validator.swing.SwingValidator;

import javax.swing.AbstractAction;
import javax.swing.JToolBar;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 9/11/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class SetCatchUIHandler extends GeneratedSetCatchUIHandler {

    private static final Logger log = LogManager.getLogger(SetCatchUIHandler.class);
    private final PropertyChangeListener catchFateChanged;
    private final PropertyChangeListener branchlineChanged;
    private final PropertyChangeListener depredatedChanged;

    SetCatchUIHandler() {
        catchFateChanged = evt -> onCatchFateChanged((CatchFateReference) evt.getNewValue());
        branchlineChanged = evt -> onBranchlineChanged((BranchlineReference) evt.getNewValue());
        depredatedChanged = evt -> onDepredatedChanged((Boolean) evt.getNewValue(), (CatchDto) evt.getSource());
    }

    @Override
    protected Component getFocusComponentOnSelectedRow(SetCatchUI ui, boolean notPersisted, boolean newRow, CatchDto tableEditBean, CatchDto previousRowBean) {
        //FIXME Review this on Focus clean session
//        JComponent requestFocus;
//        if (newRow) {
//            requestFocus = ui.getNumber();
//        } else {
//            int acquisitionMode = tableEditBean.getAcquisitionMode();
//            CatchAcquisitionMode acquisitionModeEnum = CatchAcquisitionMode.valueOf(acquisitionMode);
//            if (acquisitionModeEnum.equals(CatchAcquisitionMode.GROUPED)) {
//                requestFocus = ui.getCount();
//            } else {
//                requestFocus = ui.getCatchHealthStatus();
//            }
//        }
        return ui.getNumber();
    }

    @Override
    public void onInit(SetCatchUI ui) {
        super.onInit(ui);
        // Place a new toolbar with set default Timer time on board action
        JToolBar toolBar = new JToolBar();
        toolBar.setOpaque(false);
        toolBar.setFloatable(false);
        toolBar.setBorderPainted(false);
        toolBar.add(ui.getSetDefaultTimerTimeOnBoard());
        ui.getTimerTimeOnBoard().getDateEditorContent().add(toolBar);
        UIInitHelper.setAction(ui.getTimer(), new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onTimerChanged(((BeanCheckBox) e.getSource()).isSelected());
            }
        });
        // At init time, let's disable timeSinceContact widget, because JAXX Binding does not work
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2777
        ui.getTimeSinceContact().setEnabled(false);
        SizeMeasureTableModel.onInit(ui);
        WeightMeasureTableModel.onInit(ui);

        updateBranchlineAccessibility(false);
        SetCatchUIModelStates states = ui.getModel().getStates();
        states.addPropertyChangeListener(SetCatchUIModelStates.PROPERTY_SHOW_INDIVIDUAL_TABS, evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            showIndividualTabs(newValue);
        });

        CatchDto bean = ui.getTableEditBean();
        bean.removePropertyChangeListener(CatchDto.PROPERTY_BRANCHLINE, branchlineChanged);
        bean.addPropertyChangeListener(CatchDto.PROPERTY_BRANCHLINE, branchlineChanged);
    }

    @Override
    public void initActions() {
        super.initActions();
        SetDefaultTimerTimeOnBoard.init(ui, ui.getSetDefaultTimerTimeOnBoard(), new SetDefaultTimerTimeOnBoard<SetCatchUI>() {
            @Override
            protected Date getHaulingStartTimeStamp(SetCatchUI ui) {
                return ui.getModel().getStates().getBean().getHaulingStartTimeStamp();
            }

            @Override
            protected BranchlineDto getBranchlineBean(SetCatchUI ui) {
                return ui.getBranchlineBean();
            }
        });
        SaveBranchline.init(ui, ui.getSaveBranchline(), new SaveBranchline<SetCatchUI>() {
            @Override
            protected void doActionPerformed(ActionEvent e, SetCatchUI ui) {
                SetCatchUIModel model = ui.getModel();
                String prefix = model.getPrefix();
                log.info(String.format("%sSaved modified branchline", prefix));
                BranchlineDto branchline = ui.getBranchlineBean();
                SaveResultDto saveResult;
                try {
                    saveResult = getServicesProvider().getLlCommonTripService().saveBranchline(branchline);
                } catch (ConcurrentModificationException ex) {
                    throw new RuntimeException(ex);
                }
                saveResult.toDto(branchline);

                // on recopie le last update car c'est le last update de Set qui est renvoyé.
                SetCatchUIModelStates states = model.getStates();
                boolean modified = states.isModified();
                states.getBean().setLastUpdateDate(saveResult.getLastUpdateDate());
                ui.getBranchlineValidator().setChanged(false);
                if (!modified) {
                    ui.getValidator().setChanged(false);
                    states.setModified(false);
                }
            }
        });
    }

    @Override
    protected void installResetAction() {
        super.installResetAction();
        ResetAction<SetCatchUI> action = ResetAction.prepareAction(new DefaultResetAdapter<>() {
            @Override
            public void onUpdate(SetCatchUI ui) {
                ui.getHandler().onBranchlineChanged(null);
                ui.getHandler().onBranchlineChanged(ui.getModel().getStates().getTableEditBean().getBranchline());
            }
        });
        action.setText(t("observe.data.ll.observation.Catch.action.reset.branchline"));
        action.setTooltipText(t("observe.data.ll.observation.Catch.action.reset.branchline.tip"));
        action.setKeyStroke(ObserveLLKeyStrokes.KEY_STROKE_RESET_BRANCHLINE);
        ResetAction.init(ui, ui.getResetBranchline(), action);
    }

    @Override
    public void startEditUI() {

        CatchDto bean = ui.getTableEditBean();
        bean.removePropertyChangeListener(CatchDto.PROPERTY_CATCH_FATE, catchFateChanged);
        bean.addPropertyChangeListener(CatchDto.PROPERTY_CATCH_FATE, catchFateChanged);

        bean.removePropertyChangeListener(CatchDto.PROPERTY_DEPREDATED, depredatedChanged);
        bean.addPropertyChangeListener(CatchDto.PROPERTY_DEPREDATED, depredatedChanged);

        super.startEditUI();
    }

    public void onBranchlineChanged(BranchlineReference newValue) {
        onBranchlineChanged(newValue, false);
    }

    public void onBranchlineChanged(BranchlineReference newValue, boolean force) {
        if (!force && getTableModel().isAdjusting()) {
            return;
        }
        BranchlineDto branchlineBean = ui.getModel().getStates().getBranchlineBean();
        String oldId = branchlineBean.getId();
        String newId = null;
        if (newValue != null) {
            newId = newValue.getId();
        }
        if (Objects.equals(oldId, newId)) {
            // avoid re-entrant code
            return;
        }

        SwingValidator<BranchlineDto> branchlineValidator = ui.getBranchlineValidator();
        SetCatchUIModel model = getModel();
        if (newValue == null) {
            log.info(String.format("%s Remove branchline", prefix));
            branchlineValidator.setBean(null);
            branchlineBean.clear();
        } else {
            log.info(String.format("%s Use branchline: %s", prefix, newValue.getId()));

            Form<BranchlineDto> form = getLlCommonTripService().loadBranchlineForm(newValue.getId());
            form.getObject().copy(branchlineBean);

            if (ui.getValidator().getBean() == null) {
                ui.getValidator().setBean(model.getStates().getBean());
            }
            branchlineValidator.setBean(branchlineBean);
        }
        updateBranchlineAccessibility(newValue != null);
        branchlineValidator.setChanged(false);
    }

    void updateBranchlineAccessibility(boolean withBranchline) {
        if (withBranchline) {
            ui.getMainTabbedPane().setComponentAt(5, ui.getBranchlinePanel());
        } else {
            ui.getMainTabbedPane().setComponentAt(5, ui.getNoBranchlineForm());
        }
    }

    void updateCatchAcquisitionMode(CatchAcquisitionMode newMode) {
        if (getTableModel().isAdjusting()) {
            return;
        }
        log.debug(String.format("%s Change CatchAcquisitionMode %s", prefix, newMode));
        if (newMode == null) {
            // mode null (cela peut arriver avec les bindings)
            return;
        }
        boolean createMode = ui.getTableModel().isCreate();
        CatchDto editBean = ui.getTableEditBean();
        switch (newMode) {
            case GROUPED:
                if (createMode) {
                    editBean.setCount(null);
                    editBean.setNumber(null);
                    editBean.setTagNumber(null);
                    editBean.setSection(null);
                    editBean.setBasket(null);
                    editBean.setBranchline(null);
                    editBean.setWeightMeasureMethod(ui.getModel().getStates().getDefaultWeightMeasureMethod());
                    editBean.setWeightMeasureType(ui.getModel().getStates().getDefaultWeightMeasureType());
                }
                break;

            case INDIVIDUAL:
                if (createMode) {
                    // on positionne le count à 1 (seule valeur possible)
                    editBean.setCount(1);
                    editBean.setWeightMeasureMethod(null);
                    editBean.setWeightMeasureType(null);
                    editBean.setTotalWeight(null);
                }
                break;
        }
        boolean isGrouped = CatchAcquisitionMode.GROUPED.equals(newMode);
        ui.getTotalWeight().setEnabled(isGrouped);
        ui.getWeightMeasureMethod().setEnabled(isGrouped);
        ui.getWeightMeasureType().setEnabled(isGrouped);
        ui.getCount().setEnabled(isGrouped);
        boolean isIndividual = CatchAcquisitionMode.INDIVIDUAL.equals(newMode);
        ui.getHookPosition().setEnabled(isIndividual);
        ui.getSection().setEnabled(isIndividual);
        ui.getBasket().setEnabled(isIndividual);
        ui.getBranchline().setEnabled(isIndividual);
        ui.getNumber().setEnabled(isIndividual);
        ui.getTagNumber().setEnabled(isIndividual);
        if (createMode) {
            // on propage le mode de saisie dans le bean
            editBean.setAcquisitionMode(newMode.ordinal());
        }
//        boolean showIndividualTabs = !createMode && isIndividual;
        boolean showIndividualTabs = isIndividual;
        getModel().getStates().setShowIndividualTabs(showIndividualTabs);
    }

    void updateShowIndividualTabs() {
//        boolean showIndividualTabs = !ui.getTableModel().isCreate() && ui.getTableEditBean().getAcquisitionMode() == CatchAcquisitionMode.INDIVIDUAL.ordinal();
        boolean showIndividualTabs = ui.getTableEditBean().getAcquisitionMode() == CatchAcquisitionMode.INDIVIDUAL.ordinal();
        getModel().getStates().setShowIndividualTabs(showIndividualTabs);
    }

    void onCatchFateChanged(CatchFateReference newValue) {
//        if (getTableModel().isAdjusting()) {
//            return;
//        }
        if (newValue == null || !ProtectedIdsLl.LL_OBSERVATION_CATCH_DISCARDED_CATCH_FATE_ID.equals(newValue.getId())) {
            // not discarded
            ui.getDiscardHealthStatus().setEnabled(false);
            ui.getHookWhenDiscarded().setEnabled(false);
            CatchDto tableEditBean = getModel().getStates().getTableEditBean();
            tableEditBean.setHookWhenDiscarded(null);
            tableEditBean.setDiscardHealthStatus(null);
        } else {
            // discarded
            ui.getDiscardHealthStatus().setEnabled(true);
            ui.getHookWhenDiscarded().setEnabled(true);
        }
    }

    void onDepredatedChanged(Boolean newValue, CatchDto tableEditBean) {
//        if (getTableModel().isAdjusting()) {
//            return;
//        }
        if (Objects.equals(true, newValue)) {
            // depredated
            ui.getBeatDiameter().setEnabled(true);
            ui.getPredator().setEnabled(true);
        } else {
            // not depredated
            ui.getBeatDiameter().setEnabled(false);
            ui.getPredator().setEnabled(false);
            tableEditBean.setBeatDiameter(null);
            tableEditBean.setPredator(null);
        }
    }

    void onTimerChanged(Boolean newValue) {
        if (Objects.equals(true, newValue)) {
            // with timer
            ui.getBranchlineBean().setTimeSinceContact(0);
        } else {
            // without timer
            ui.getBranchlineBean().setTimeSinceContact(null);
        }
    }

    private void showIndividualTabs(boolean newValue) {
        log.info(String.format("%s will show individuals tabs ?%s", prefix, newValue));
        ui.getFoodAndSexualTab().setEnabled(newValue);
        ui.getSizeMeasuresTab().setEnabled(newValue);
        ui.getWeightMeasuresTab().setEnabled(newValue);
        boolean enableInlineModels = newValue && (ui.getModel().getStates().isReadingMode() || ui.getModel().getStates().isEditing());
        getTableModel().inlineModels().keySet().forEach(i -> {
            i.setEditable(enableInlineModels);
            if (newValue && (!ui.getModel().getStates().isReadingMode() || ui.getModel().getStates().isEditing())) {
                i.validate();
            }
            if (!enableInlineModels) {
                int selectedRow = ui.getTableModel().getSelectedRow();
                if (selectedRow != -1) {
                    i.simpleClear();
                }
            }
        });
        if (!newValue && ui.getMainTabbedPane().getSelectedIndex() > 1) {
            // go back to first tab
            ui.getMainTabbedPane().setSelectedIndex(0);
        }
    }
}
