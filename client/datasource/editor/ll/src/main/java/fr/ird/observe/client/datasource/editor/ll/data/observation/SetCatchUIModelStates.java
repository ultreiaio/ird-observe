package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.ll.data.common.TripUINavigationNode;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.LonglinePositionHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.CatchAcquisitionMode;
import fr.ird.observe.dto.data.ll.common.TripReference;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.CatchDto;
import fr.ird.observe.dto.data.ll.observation.SetCatchDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.common.WeightMeasureMethodReference;
import fr.ird.observe.dto.referential.common.WeightMeasureTypeReference;
import fr.ird.observe.dto.referential.ll.common.CatchFateReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class SetCatchUIModelStates extends GeneratedSetCatchUIModelStates {
    public static final String PROPERTY_SHOW_INDIVIDUAL_TABS = "showIndividualTabs";
    /**
     * Branchline edit bean.
     */
    private final BranchlineDto branchlineBean;
    /**
     * Default acquisition mode to use.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2671">issue 2671</a>
     */
    private final CatchAcquisitionMode defaultAcquisitionMode;
    /**
     * Default weight measure method to use for new data.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2907">issue 2907</a>
     */
    private final WeightMeasureMethodReference defaultWeightMeasureMethod;
    /**
     * Default weight measure type to use for new data.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2932">issue 2932</a>
     */
    private final WeightMeasureTypeReference defaultWeightMeasureType;
    /**
     * Position helper.
     */
    protected LonglinePositionHelper<CatchDto> positionHelper;
    /**
     * flag to see the individual tabs (only on individual mode + not in create mode).
     */
    protected boolean showIndividualTabs;

    public SetCatchUIModelStates(GeneratedSetCatchUIModel model) {
        super(model);
        this.branchlineBean = new BranchlineDto();
        ObserveSwingDataSource mainDataSource = model.getSource().getContext().getMainDataSource();
        this.defaultWeightMeasureMethod = mainDataSource.getReferentialReferenceSet(WeightMeasureMethodReference.class).tryGetReferenceById(ProtectedIdsLl.LL_OBSERVATION_CATCH_DEFAULT_WEIGHT_MEASURE_METHOD_ID).orElseThrow(IllegalStateException::new);
        this.defaultWeightMeasureType = mainDataSource.getReferentialReferenceSet(WeightMeasureTypeReference.class).tryGetReferenceById(ProtectedIdsLl.LL_OBSERVATION_CATCH_DEFAULT_WEIGHT_MEASURE_TYPE_ID).orElseThrow(IllegalStateException::new);
        TripUINavigationNode tripNode = (TripUINavigationNode) model.getSource().upToReferenceNode(TripReference.class);
        TripReference tripReference = tripNode.getReference();
        String observationMethodId = tripReference.getObservationMethodId();
        this.defaultAcquisitionMode = ProtectedIdsLl.LL_OBSERVATION_GROUPED_OBSERVATION_METHOD_ID.equals(observationMethodId) ? CatchAcquisitionMode.GROUPED : CatchAcquisitionMode.INDIVIDUAL;
    }

    @Override
    public void init(ContentUI ui, DefaultUIInitializerResult initializerResult) {
        super.init(ui, initializerResult);
        SetCatchUI ui1 = (SetCatchUI) ui;
        positionHelper = new LonglinePositionHelper<>(ui1.getSection(), ui1.getBasket(), ui1.getBranchline(), ui1.getTableEditBean());
//        TripUINavigationNode tripNode = (TripUINavigationNode) ui1.getModel().getSource().upToReferenceNode(TripReference.class);
//        TripReference tripReference = tripNode.getReference();
//        String observationMethodId = tripReference.getObservationMethodId();
//        this.defaultAcquisitionMode = ProtectedIdsLl.LL_OBSERVATION_GROUPED_OBSERVATION_METHOD_ID.equals(observationMethodId) ? CatchAcquisitionMode.GROUPED : CatchAcquisitionMode.INDIVIDUAL;
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(CatchDto.PROPERTY_CATCH_FATE, ReferencesFilterHelper.newPredicateList(CatchFateReference::isObservation));
        referenceCache.addReferentialFilter(CatchDto.PROPERTY_SPECIES, ReferencesFilterHelper.newLlSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListLonglineObservationCatchId()));
        referenceCache.addReferentialFilter(CatchDto.PROPERTY_PREDATOR, ReferencesFilterHelper.newLlSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListLonglineDepredatorId()));
    }

    @Override
    protected void loadReferentialCacheOnOpenForm(Form<SetCatchDto> form) {
        super.loadReferentialCacheOnOpenForm(form);
        getReferenceCache().loadExtraReferentialReferenceSetsInModel(BranchlineDto.class);
    }

    @Override
    protected void copyFormToBean(Form<SetCatchDto> form) {
        positionHelper.initSections(form.getObject(), form.getObject().getCatches());
        super.copyFormToBean(form);
    }

    @Override
    public void initDefault(CatchDto newTableBean) {
        super.initDefault(newTableBean);
        newTableBean.setAcquisitionMode(defaultAcquisitionMode.ordinal());
        newTableBean.setWeightMeasureMethod(defaultWeightMeasureMethod);
        newTableBean.setWeightMeasureType(defaultWeightMeasureType);
    }

    public CatchAcquisitionMode getDefaultAcquisitionMode() {
        return defaultAcquisitionMode;
    }

    public WeightMeasureMethodReference getDefaultWeightMeasureMethod() {
        return defaultWeightMeasureMethod;
    }

    public WeightMeasureTypeReference getDefaultWeightMeasureType() {
        return defaultWeightMeasureType;
    }

    public BranchlineDto getBranchlineBean() {
        return branchlineBean;
    }

    public boolean isShowIndividualTabs() {
        return showIndividualTabs;
    }

    public void setShowIndividualTabs(boolean showIndividualTabs) {
        this.showIndividualTabs = showIndividualTabs;
        firePropertyChange(PROPERTY_SHOW_INDIVIDUAL_TABS, null, showIndividualTabs);
    }

    public void resetPosition(CatchDto dto) {
        positionHelper.resetPosition(dto);
    }
}
