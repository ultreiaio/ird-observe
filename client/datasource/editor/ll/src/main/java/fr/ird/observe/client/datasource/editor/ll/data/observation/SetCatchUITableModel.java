package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.table.EditableListProperty;
import fr.ird.observe.client.util.table.renderer.DecoratorTableRenderer;
import fr.ird.observe.dto.data.CatchAcquisitionMode;
import fr.ird.observe.dto.data.ll.observation.CatchDto;
import fr.ird.observe.dto.data.ll.observation.SizeMeasureDto;
import fr.ird.observe.dto.data.ll.observation.WeightMeasureDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JTable;
import javax.swing.table.TableColumnModel;
import java.util.Collection;
import java.util.List;

/**
 * Created on 12/4/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public class SetCatchUITableModel extends GeneratedSetCatchUITableModel {

    private static final Logger log = LogManager.getLogger(SetCatchUITableModel.class);
    protected final SizeMeasureTableModel sizeMeasuresTableModel;
    protected final WeightMeasureTableModel weightMeasuresTableModel;

    public SetCatchUITableModel(SetCatchUI ui) {
        super(ui);
        this.sizeMeasuresTableModel = new SizeMeasureTableModel(new EditableListProperty<>() {
            @Override
            public Collection<SizeMeasureDto> get() {
                return ui.getTableEditBean().getSizeMeasure();
            }

            @Override
            public void set(List<SizeMeasureDto> data) {
                ui.getTableEditBean().setSizeMeasure(data);
            }
        });
        this.weightMeasuresTableModel = new WeightMeasureTableModel(new EditableListProperty<>() {
            @Override
            public Collection<WeightMeasureDto> get() {
                return ui.getTableEditBean().getWeightMeasure();
            }

            @Override
            public void set(List<WeightMeasureDto> data) {
                ui.getTableEditBean().setWeightMeasure(data);
            }
        });
    }

    public SizeMeasureTableModel getSizeMeasuresTableModel() {
        return sizeMeasuresTableModel;
    }

    public WeightMeasureTableModel getWeightMeasuresTableModel() {
        return weightMeasuresTableModel;
    }

    @Override
    public void initTableUISize(JTable table) {
        super.initTableUISize(table);
        TableColumnModel columnModel = table.getColumnModel();
        ((DecoratorTableRenderer) columnModel.getColumn(2).getCellRenderer()).getDecorator().setIndex(1);
        ((DecoratorTableRenderer) columnModel.getColumn(3).getCellRenderer()).getDecorator().setIndex(1);
        ((DecoratorTableRenderer) columnModel.getColumn(4).getCellRenderer()).getDecorator().setIndex(1);
    }

    @Override
    protected void onBeforeResetRow(int row) {
        super.onBeforeResetRow(row);
        getContext().getHandler().onBranchlineChanged(null, true);
    }

    @Override
    protected void onAfterResetRow(int row) {
        getContext().getHandler().onBranchlineChanged(getModel().getStates().getTableEditBean().getBranchline(), true);
        getContext().getHandler().updateShowIndividualTabs();
    }

    @Override
    protected void onAfterLoadRowBeanToEdit(int editingRow, boolean newRow) {
        super.onAfterLoadRowBeanToEdit(editingRow, newRow);
        CatchDto tableEditBean = getModel().getStates().getTableEditBean();
        SetCatchUIHandler handler = getContext().getHandler();
        handler.onBranchlineChanged(tableEditBean.getBranchline(), true);
        handler.updateShowIndividualTabs();
        handler.onDepredatedChanged(tableEditBean.isDepredated(), tableEditBean);
        handler.onCatchFateChanged(tableEditBean.getCatchFate());
    }

    @Override
    protected void onSelectedRowChanged(SetCatchUI ui, int editingRow, CatchDto tableEditBean, CatchDto previousRowBean, boolean notPersisted, boolean newRow) {
        super.onSelectedRowChanged(ui, editingRow, tableEditBean, previousRowBean, notPersisted, newRow);
        SetCatchUIHandler handler = ui.getHandler();
        String prefix = getModel().getPrefix();
        log.info(String.format("%s Selected row changed: %d, create? %s", prefix, editingRow, notPersisted));
        CatchAcquisitionMode acquisitionModeEnum;
        if (newRow) {
            // go back to first pane
            ui.getMainTabbedPane().setSelectedIndex(0);
            acquisitionModeEnum = getModel().getStates().getDefaultAcquisitionMode();
//            ui.getPredator().setEnabled(false);
//            ui.getHookWhenDiscarded().setEnabled(false);
//            ui.getDiscardHealthStatus().setEnabled(false);
//            ui.getBeatDiameter().setEnabled(false);
        } else {
            int acquisitionMode = tableEditBean.getAcquisitionMode();
            acquisitionModeEnum = CatchAcquisitionMode.valueOf(acquisitionMode);
//            //FIXME Voir si pas besoin aussi de relancer les binding (catchFateChanged, branchlineChanged) ?
//            handler.onDepredatedChanged(tableEditBean.isDepredated(), tableEditBean);
        }
        ui.getAcquisitionModeGroup().setSelectedValue(null);
        ui.getAcquisitionModeGroup().setSelectedValue(acquisitionModeEnum);

        getModel().getStates().resetPosition(tableEditBean);
    }

    @Override
    protected void startEditTableEditBeanOnInlineModels() {
        if (getModel().getStates().isShowIndividualTabs()) {
            super.startEditTableEditBeanOnInlineModels();
        }
    }
}
