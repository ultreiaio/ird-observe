package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.reset.DefaultResetAdapter;
import fr.ird.observe.client.datasource.editor.api.content.actions.reset.ResetAction;
import fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.DeleteAll;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.GenerateAll;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.template.SectionTemplateTableModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import fr.ird.observe.dto.data.ll.observation.SectionTemplateDto;
import fr.ird.observe.dto.data.ll.observation.SetDetailCompositionDto;
import fr.ird.observe.dto.form.Form;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthFormat;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.TimeLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.ToolTipHighlighter;
import org.jdesktop.swingx.renderer.StringValue;
import org.nuiton.jaxx.runtime.swing.JVetoableTabbedPane;
import org.nuiton.jaxx.validator.swing.SwingValidator;

import javax.swing.JOptionPane;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 12/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public class SetDetailCompositionUIHandler extends GeneratedSetDetailCompositionUIHandler {

    private static final Logger log = LogManager.getLogger(SetDetailCompositionUIHandler.class);
    private static final TimeLog TIME_LOG = new TimeLog(SetDetailCompositionUIHandler.class,0,100);

    private final PropertyChangeListener modelCanGenerateChanged;
    private final JVetoableTabbedPane.ChangeSelectedIndex tabbedPaneWillChanged;

    SetDetailCompositionUIHandler() {
        modelCanGenerateChanged = evt -> {
            getModel().getStates().setSectionAdjusting(true);
            try {
                Boolean newValue = (Boolean) evt.getNewValue();
                onModelCanGenerateChanged(newValue);
            } finally {
                getModel().getStates().setSectionAdjusting(false);
            }
        };
        tabbedPaneWillChanged = this::onTabWillChanged;
    }

    @Override
    protected SetDetailCompositionUIInitializer createContentUIInitializer(SetDetailCompositionUI ui) {
        return new SetDetailCompositionUIInitializer(ui);
    }

    @Override
    public void onInit(SetDetailCompositionUI ui) {
        super.onInit(ui);
        List<String> formatsBuilder = new ArrayList<>(NauticalLengthFormat.values().length);
        for (NauticalLengthFormat format : NauticalLengthFormat.values()) {
            formatsBuilder.add(String.format("<code>%s</code> (%s)", format.getLabel(), format.getDescription()));
        }
        ui.getTemplateInformation().setText(I18n.t("observe.data.ll.observation.SetDetailComposition.templateInformation", String.join(", ",formatsBuilder)));
        ui.getTemplateInformation().setToolTipText(ui.getTemplateInformation().getText());
        SetDetailCompositionUIModel model = getModel();
        model.getStates().addPropertyChangeListener(SetDetailCompositionUIModelStates.PROPERTY_CAN_GENERATE, modelCanGenerateChanged);
        ui.getMainTabbedPane().setChangeSelectedIndex(tabbedPaneWillChanged);
    }

    @Override
    protected void installResetAction() {
        ResetAction.installAction(ui, ui.getReset(), new DefaultResetAdapter<>() {
            @Override
            public void onUpdate(SetDetailCompositionUI ui) {
                long t0 = TimeLog.getTime();
                ui.getModel().getStates().setResetEdit(true);
//                BranchlineDto branchline = ui.getBranchlinesTableModel().getSelectedRow();
                try {
//                    onBranchlineChanged(null);
                    ui.getSectionsTableModel().clear();
                    ui.getBasketsTableModel().clear();
                    ui.getBranchlinesTableModel().clear();
                } finally {
                    ui.getModel().getStates().setResetEdit(false);
                }
                TIME_LOG.log(t0,prefix + " Reset (clear step)" );
                long t1 = TimeLog.getTime();
                super.onUpdate(ui);
                TIME_LOG.log(t1,prefix + " Reset (stop and reload)" );
                // This will load other data (baskets, branchlines)
                ui.getSectionsTableModel().onSelectedSectionChanged(null, ui.getSectionsTableModel().getSelectedRow());
//                if (branchline != null && ui.getMainTabbedPane().getSelectedIndex() == 2) {
//                    onBranchlineChanged(branchline);
//                }
                TIME_LOG.log(t0,prefix + " Reset" );
            }
        });
        ResetAction<SetDetailCompositionUI> action = ResetAction.prepareAction(new DefaultResetAdapter<>() {
            @Override
            public void onUpdate(SetDetailCompositionUI ui) {
                ui.getHandler().onBranchlineChanged(null);
                ui.getHandler().onBranchlineChanged(ui.getModel().getStates().getBranchlinesTableModel().getSelectedRow());
            }
        });
        action.setText(t("observe.data.ll.observation.Catch.action.reset.branchline"));
        action.setTooltipText(t("observe.data.ll.observation.Catch.action.reset.branchline.tip"));
        action.setKeyStroke(ObserveLLKeyStrokes.KEY_STROKE_RESET_BRANCHLINE);
        ResetAction.init(ui, ui.getResetBranchline(), action);
        GenerateAll.init(ui, ui.getGenerateAll(), new GenerateAll());
        DeleteAll.init(ui, ui.getDeleteAll(), new DeleteAll());
    }

    @Override
    public void onOpenForm(Form<?> form) {
        UIHelper.cancelEditing(getUi().getSectionTemplatesTable());
        UIHelper.cancelEditing(getUi().getSectionsTable());

        SetDetailCompositionUIModel model = getModel();

        SetDetailCompositionDto bean = model.getStates().getBean();

        model.getStates().getBranchlinesTableModel().setUseTimer(Objects.equals(true, bean.isMonitored()));

        // by default, can generate if there is no section in database
        model.getStates().setCanGenerate(bean.isSectionEmpty());

        // TODO Use a cache of templates on setLongline (session scope)
        model.getStates().getSectionTemplatesTableModel().setData(new ArrayList<>());

//        BranchlineDto selectedRow = model.getStates().getBranchlinesTableModel().getSelectedRow();
//        if (ui.getMainTabbedPane().getSelectedIndex() == 2) {
//            onBranchlineChanged(selectedRow);
//        }
    }

    @Override
    public void onOpenAfterOpenModel() {
        super.onOpenAfterOpenModel();
        SetDetailCompositionUIModel model = getModel();
        model.getStates().setCompositionTabValid(true);
        List<SectionDto> section = new LinkedList<>(model.getStates().getBean().getSection());
        model.getStates().getSectionsTableModel().setData(section);
    }

    @Override
    public void onInstallValidators(IdDto editBean) {
        super.onInstallValidators(editBean);
        ui.getGenerateValidator().setBean(getModel().getStates());
        if (ui.getModel().getStates().isUpdatingMode() && ui.getMainTabbedPane().getSelectedIndex() == 2 && getModel().getStates().getBranchlinesTableModel().getSelectedRow() != null) {
            ui.getBranchlineDetailValidator().setBean(ui.getBranchlineBean());
        } else {
            ui.getBranchlineDetailValidator().setBean(null);
        }
    }

    @Override
    public void onUninstallValidators() {
        super.onUninstallValidators();
        ui.getGenerateValidator().setBean(null);
    }

    @Override
    public void startEditUI() {
        super.startEditUI();
        HighlightPredicate predicate = (renderer, adapter) -> {
            JXTable component = (JXTable) adapter.getComponent();
            SectionTemplateTableModel model = (SectionTemplateTableModel) component.getModel();
            int row = adapter.convertRowIndexToModel(adapter.row);
            SectionTemplateDto node = model.getData(row);
            if (Objects.requireNonNull(node).isDataEmpty()) {
                return false;
            }
            switch (adapter.convertColumnIndexToModel(adapter.column)) {
                case 0:
                    return !node.isIdValid();
                case 1:
                    return !node.isFloatlineLengthsValid();
            }
            return true;
        };
        ui.getSectionTemplatesTable().addHighlighter(new ColorHighlighter(predicate, getClientConfig().getFloatingObjectMaterialErrorColor(), Color.WHITE, getClientConfig().getFloatingObjectMaterialErrorColor(), Color.WHITE));
        ui.getSectionTemplatesTable().addHighlighter(new ToolTipHighlighter(predicate, (StringValue) value -> {
            int selectedColumn = ui.getSectionTemplatesTable().getSelectedColumn();
            String str = (String) value;
            boolean empty = Strings.isEmpty(str);
            switch (selectedColumn) {
                case 0:
                    return t("observe.data.ll.observation.SectionTemplate.validation.required.id");
                case 1:
                    return empty ? t("observe.data.ll.observation.SectionTemplate.validation.required.pattern") : t("observe.data.ll.observation.SectionTemplate.validation.invalid.pattern");
            }
            return null;
        }));
        ui.getModel().getStates().getValidationHelper().whenSectionChanged();
    }

    private void onModelCanGenerateChanged(Boolean canGenerate) {
        if (canGenerate) {
            if (getModel().getStates().getSectionTemplatesTableModel().isEmpty()) {
                // add an empty row
                getModel().getStates().getSectionTemplatesTableModel().addNewRow();
            }
        }
    }

    private boolean onTabWillChanged(int selectedIndex, int newIndex) {
        boolean result = true;
        if (getModel().getStates().isOpened() && newIndex > -1) {
            switch (selectedIndex) {
                case 0:
                    result = getModel().getStates().isGenerateTabValid();
                    break;
                case 1:
                    result = getModel().getStates().isCompositionTabValid();
                    break;
                case 2:
                    result = tryToQuitBranchlineTab();
                    break;
            }
        }
        return result;
    }

    @Override
    public void onMainTabChanged(int previousIndex, int selectedIndex) {
        switch (previousIndex) {
            case 0:
                // abort editing table
                UIHelper.cancelEditing(getUi().getSectionTemplatesTable());
                break;
            case 1:
                // abort editing tables
                UIHelper.cancelEditing(getUi().getSectionsTable());
                break;
            case 2:
                onBranchlineChanged(null);
                break;
        }
        switch (selectedIndex) {
            case 0:
                break;
            case 1:
                if (previousIndex == 0) {
                    if (getModel().getStates().isCanGenerate()) {
                        // update section templates list
                        List<SectionTemplateDto> sectionTemplates = getModel().getStates().getSectionTemplatesTableModel().getValidData();
                        getModel().getStates().getSectionsTableModel().setSectionTemplates(sectionTemplates.isEmpty() ? null : sectionTemplates);
                    } else {
                        getModel().getStates().getSectionsTableModel().setSectionTemplates(null);
                    }
                }
                break;
            case 2:
                BranchlineDto branchline = getModel().getStates().getBranchlinesTableModel().getSelectedRow();
                if (branchline != null) {
                    onBranchlineChanged(branchline);
                }
                break;
        }
    }

    public void onBranchlineChanged(BranchlineDto newValue) {
        boolean changed = ui.getValidator().isChanged() || ui.getModel().getStates().isModified();

        String oldId = null;
        BranchlineDto branchlineBean = ui.getModel().getStates().getBranchlineBean();
        if (branchlineBean != null) {
            oldId = branchlineBean.getId();
        }
        String newId = null;
        if (newValue != null) {
            newId = newValue.getId();
        }
        if (Objects.equals(oldId, newId)) {
            // avoid re-entrant code
            return;
        }

        SwingValidator<BranchlineDto> branchlineValidator = ui.getBranchlineDetailValidator();
        branchlineValidator.setBean(null);
        if (newValue == null) {
            log.info(String.format("%s Remove branchline", prefix));
            branchlineBean.clear();
        } else {
            log.info(String.format("%s Use branchline: %s", prefix, newValue.getId()));
            newValue.copy(branchlineBean);
            if (ui.getModel().getStates().isUpdatingMode()) {
                branchlineValidator.setBean(branchlineBean);
            }
        }
        updateBranchlineAccessibility(newValue != null);
        branchlineValidator.setChanged(false);
        ui.getValidator().setChanged(changed);
    }

    void updateBranchlineAccessibility(boolean withBranchline) {
        if (withBranchline) {
            ui.getMainTabbedPane().setComponentAt(2, ui.getBranchlinePanel());
        } else {
            ui.getMainTabbedPane().setComponentAt(2, ui.getNoBranchlineForm());
        }
    }

    public boolean tryToQuitBranchlineTab() {
        boolean canContinue;
        SetDetailCompositionUIModel model = getModel();
        SetDetailCompositionUIModelStates states = model.getStates();
        if (states.isEditing() && ui.getBranchlineDetailValidator().isChanged()) {
            canContinue = false;
            if (states.isBranchlineDetailTabValid()) {
                // le formulaire est valide, on demande a l'utilisateur s'il
                // veut la sauvegarder
                int response = askToUser(
                        I18n.t("observe.ui.title.need.confirm"),
                        I18n.t("observe.data.ll.observation.Branchline.message.modified"),
                        JOptionPane.WARNING_MESSAGE,
                        new Object[]{
                                I18n.t("observe.ui.choice.save"),
                                I18n.t("observe.ui.choice.doNotSave"),
                                I18n.t("observe.ui.choice.cancel")},
                        0);
                log.debug("response : " + response);
                switch (response) {
                    case JOptionPane.CLOSED_OPTION:
                    case 2:
                        break;
                    case 0:
                        // will save ui
                        // sauvegarde des modifications
                        doSaveBranchline();
                        canContinue = true;
                        break;
                    case 1:
                        // reset edit
                        ui.getHandler().getResetAction().doReset();
                        canContinue = true;
                        break;
                }
            } else {
                // le formulaire n'est pas valide, on ne peut que proposer la perte
                // des donnees car elles sont ne pas enregistrables
                int response = askToUser(
                        I18n.t("observe.ui.title.need.confirm"),
                        I18n.t("observe.data.ll.observation.Branchline.message.modified.but.invalid"),
                        JOptionPane.ERROR_MESSAGE,
                        new Object[]{
                                I18n.t("observe.ui.choice.continue"),
                                I18n.t("observe.ui.choice.cancel")},
                        0);
                log.debug("response : " + response);
                if (response == 0) {
                    // reset edit
                    ui.getHandler().getResetAction().doReset();
                    canContinue = true;
                }
            }
        } else {
            // pas en mode edition ou formulaire non modifié
            // on peut toujours quitter
            canContinue = true;
        }
        return canContinue;
    }

    public void doSaveBranchline() {
        BranchlineDto beanToSave = ui.getModel().getStates().getBranchlineBean();
        beanToSave.copy(ui.getModel().getStates().getBranchlinesTableModel().getSelectedRow());
        ui.getBranchlineDetailValidator().setChanged(false);
        ui.getBranchlineDetailValidator().setValid(true);
        ui.getValidator().setChanged(true);
        ui.getModel().getStates().getValidationHelper().whenBranchlineChanged();
    }

    void onTimerChanged(boolean newValue) {
        if (Objects.equals(true, newValue)) {
            // with timer
            ui.getBranchlineBean().setTimeSinceContact(0);
        } else {
            // without timer
            ui.getBranchlineBean().setTimeSinceContact(null);
        }
    }

    void onWeightedSnapChanged(boolean newValue) {
        if (Objects.equals(true, newValue)) {
            // with timer
            ui.getBranchlineBean().setSnapWeight(0F);
        } else {
            // without timer
            ui.getBranchlineBean().setSnapWeight(null);
        }
    }

    void onWeightedSwivelChanged(boolean newValue) {
        if (Objects.equals(true, newValue)) {
            // with timer
            ui.getBranchlineBean().setSwivelWeight(0F);
        } else {
            // without timer
            ui.getBranchlineBean().setSwivelWeight(null);
        }
    }
}
