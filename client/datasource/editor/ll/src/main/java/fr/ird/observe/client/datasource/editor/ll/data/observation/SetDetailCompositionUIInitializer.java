package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.datasource.editor.ll.data.observation.branchline.SaveBranchline;
import fr.ird.observe.client.datasource.editor.ll.data.observation.branchline.SetDefaultTimerTimeOnBoard;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.CompositionAutoSelectRowAndShowPopupAction;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.basket.BasketTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.basket.DeleteSelectedBasket;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.basket.InsertAfterSelectedBasket;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.basket.InsertBeforeSelectedBasket;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.branchline.BranchlineTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.branchline.DeleteSelectedBranchline;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.branchline.InsertAfterSelectedBranchline;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.branchline.InsertBeforeSelectedBranchline;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.section.DeleteSelectedSection;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.section.InsertAfterSelectedSection;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.section.InsertBeforeSelectedSection;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.section.SectionTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.template.DeleteSelectedSectionTemplate;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.template.InsertAfterSelectedSectionTemplate;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.template.InsertBeforeSelectedSectionTemplate;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.template.SectionTemplateTableModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ll.observation.BasketDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import fr.ird.observe.dto.data.ll.observation.SectionTemplateDto;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxCellEditor;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import javax.swing.AbstractAction;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Date;

/**
 * Created on 12/13/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.10
 */
public class SetDetailCompositionUIInitializer extends ContentSimpleUIInitializer<SetDetailCompositionUI> {

    SetDetailCompositionUIInitializer(SetDetailCompositionUI ui) {
        super(ui);
    }

    @Override
    public DefaultUIInitializerResult initUI() {
        DefaultUIInitializerResult initializerResult = super.initUI();
        initSectionTemplates(ui.getSectionTemplatesTable());
        initSections(ui.getSectionsTable());
        initBaskets(ui.getBasketsTable());
        initBranchlines(ui.getBranchlinesTable());
        initBranchline();
        return initializerResult;
    }

    protected void initSectionTemplates(EditableTable<SectionTemplateDto, SectionTemplateTableModel> table) {

        JXTableUtil.setI18nTableHeaderRenderer(table,
                                               SectionTemplateDto.class,
                                               SectionTemplateDto.PROPERTY_ID,
                                               SectionTemplateDto.PROPERTY_FLOATLINE_LENGTHS);

        table.installTableKeyListener();
//        table.installTableFocusListener();
        table.setCellSelectionEnabled(false);

        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.installSelectionListener();

        new CompositionAutoSelectRowAndShowPopupAction(ui,
                                                       table,
                                                       ui.getSectionTemplatesPopup(),
                                                       ui.getDeleteSelectedSectionTemplate(),
                                                       ui.getInsertBeforeSelectedSectionTemplate(),
                                                       ui.getInsertAfterSelectedSectionTemplate());

        DeleteSelectedSectionTemplate.init(ui, ui.getDeleteSelectedSectionTemplate(), new DeleteSelectedSectionTemplate());
        InsertBeforeSelectedSectionTemplate.init(ui, ui.getInsertBeforeSelectedSectionTemplate(), new InsertBeforeSelectedSectionTemplate());
        InsertAfterSelectedSectionTemplate.init(ui, ui.getInsertAfterSelectedSectionTemplate(), new InsertAfterSelectedSectionTemplate());
    }

    protected void initSections(EditableTable<SectionDto, SectionTableModel> table) {
        table.setSortable(false);
        table.setRowHeight(22);
        JXTableUtil.setI18nTableHeaderRenderer(table,
                                               SectionDto.class,
                                               SectionDto.PROPERTY_SETTING_IDENTIFIER,
                                               SectionDto.PROPERTY_HAULING_IDENTIFIER,
                                               SectionDto.PROPERTY_SECTION_TEMPLATE);

        JXTableUtil.initRenderers(table,
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newDecoratedRenderer(SectionTemplateDto.class));

        DecoratorService decoratorService = ui.getHandler().getDecoratorService();
        Decorator sectionTemplateDecorator = decoratorService.getDecoratorByType(SectionTemplateDto.class);
        FilterableComboBoxCellEditor editor = UIHelper.newCellEditor(Collections.emptyList(), sectionTemplateDecorator);
        table.getModel().setSectionTemplateEditor(editor);
        JXTableUtil.initEditors(table,
                                null,
                                NumberCellEditor.newIntegerColumnEditor(),
                                editor);

        table.installTableKeyListener();
//        table.installTableFocusListener();

        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.installSelectionListener();

        new CompositionAutoSelectRowAndShowPopupAction(ui,
                                                       table,
                                                       ui.getSectionsPopup(),
                                                       ui.getDeleteSelectedSection(),
                                                       ui.getInsertBeforeSelectedSection(),
                                                       ui.getInsertAfterSelectedSection());

        DeleteSelectedSection.init(ui, ui.getDeleteSelectedSection(), new DeleteSelectedSection());
        InsertBeforeSelectedSection.init(ui, ui.getInsertBeforeSelectedSection(), new InsertBeforeSelectedSection());
        InsertAfterSelectedSection.init(ui, ui.getInsertAfterSelectedSection(), new InsertAfterSelectedSection());
    }

    private void initBaskets(EditableTable<BasketDto, BasketTableModel> table) {
        table.setSortable(false);
        JXTableUtil.setI18nTableHeaderRenderer(table,
                                               BasketDto.class,
                                               BasketDto.PROPERTY_SETTING_IDENTIFIER,
                                               BasketDto.PROPERTY_HAULING_IDENTIFIER,
                                               BasketDto.PROPERTY_FLOATLINE1_LENGTH,
                                               BasketDto.PROPERTY_FLOATLINE2_LENGTH);

        JXTableUtil.initRenderers(table,
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newEmptyNumberTableCellRenderer());

        JXTableUtil.initEditors(table,
                                null,
                                NumberCellEditor.newIntegerColumnEditor(),
                                JXTableUtil.newFloat2ColumnEditor(),
                                JXTableUtil.newFloat2ColumnEditor());

        table.installTableKeyListener();
//        table.installTableFocusListener();

        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.installSelectionListener();

        new CompositionAutoSelectRowAndShowPopupAction(ui,
                                                       table,
                                                       ui.getBasketsPopup(),
                                                       ui.getDeleteSelectedBasket(),
                                                       ui.getInsertBeforeSelectedBasket(),
                                                       ui.getInsertAfterSelectedBasket());

        DeleteSelectedBasket.init(ui, ui.getDeleteSelectedBasket(), new DeleteSelectedBasket());
        InsertBeforeSelectedBasket.init(ui, ui.getInsertBeforeSelectedBasket(), new InsertBeforeSelectedBasket());
        InsertAfterSelectedBasket.init(ui, ui.getInsertAfterSelectedBasket(), new InsertAfterSelectedBasket());
    }

    private void initBranchlines(EditableTable<BranchlineDto, BranchlineTableModel> table) {
        table.setSortable(false);
        JXTableUtil.setI18nTableHeaderRenderer(table,
                                               BranchlineDto.class,
                                               BranchlineDto.PROPERTY_SETTING_IDENTIFIER,
                                               BranchlineDto.PROPERTY_HAULING_IDENTIFIER,
                                               BranchlineDto.PROPERTY_BRANCHLINE_LENGTH,
                                               BranchlineDto.PROPERTY_TRACELINE_LENGTH);

        JXTableUtil.initRenderers(table,
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newEmptyNumberTableCellRenderer());

        JXTableUtil.initEditors(table,
                                null,
                                NumberCellEditor.newIntegerColumnEditor(),
                                NumberCellEditor.newFloatColumnEditor(),
                                NumberCellEditor.newFloatColumnEditor());

        table.installTableKeyListener();
//        table.installTableFocusListener();

        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.installSelectionListener();

        new CompositionAutoSelectRowAndShowPopupAction(ui,
                                                       table,
                                                       ui.getBranchlinesPopup(),
                                                       ui.getDeleteSelectedBranchline(),
                                                       ui.getInsertBeforeSelectedBranchline(),
                                                       ui.getInsertAfterSelectedBranchline());

        DeleteSelectedBranchline.init(ui, ui.getDeleteSelectedBranchline(), new DeleteSelectedBranchline());
        InsertBeforeSelectedBranchline.init(ui, ui.getInsertBeforeSelectedBranchline(), new InsertBeforeSelectedBranchline());
        InsertAfterSelectedBranchline.init(ui, ui.getInsertAfterSelectedBranchline(), new InsertAfterSelectedBranchline());
    }

    private void initBranchline() {
        // Place a new toolbar with set default Timer time on board action
        JToolBar toolBar = new JToolBar();
        toolBar.setOpaque(false);
        toolBar.setFloatable(false);
        toolBar.setBorderPainted(false);
        toolBar.add(ui.getSetDefaultTimerTimeOnBoard());
        ui.getTimerTimeOnBoard().getDateEditorContent().add(toolBar);
        UIInitHelper.setAction(ui.getTimer(), new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.getHandler().onTimerChanged(((BeanCheckBox) e.getSource()).isSelected());
            }
        });
        UIInitHelper.setAction(ui.getWeightedSnap(), new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.getHandler().onWeightedSnapChanged(((BeanCheckBox) e.getSource()).isSelected());
            }
        });
        UIInitHelper.setAction(ui.getWeightedSwivel(), new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.getHandler().onWeightedSwivelChanged(((BeanCheckBox) e.getSource()).isSelected());
            }
        });
        SetDefaultTimerTimeOnBoard.init(ui, ui.getSetDefaultTimerTimeOnBoard(), new SetDefaultTimerTimeOnBoard<SetDetailCompositionUI>() {
            @Override
            protected Date getHaulingStartTimeStamp(SetDetailCompositionUI ui) {
                return ui.getModel().getStates().getBean().getHaulingStartTimeStamp();
            }

            @Override
            protected BranchlineDto getBranchlineBean(SetDetailCompositionUI ui) {
                return ui.getBranchlineBean();
            }
        });
        SaveBranchline.init(ui, ui.getSaveBranchline(), new SaveBranchline<SetDetailCompositionUI>() {
            @Override
            protected void doActionPerformed(ActionEvent e, SetDetailCompositionUI ui) {
                ui.getHandler().doSaveBranchline();
            }
        });
        ui.getHandler().updateBranchlineAccessibility(false);
        UIHelper.setLayerUI(ui.getBranchlineForm(), ui.getBranchlineBlockLayerUI());
        // At init time, let's disable timeSinceContact widget, because JAXX Binding does not work
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2777
        ui.getTimeSinceContact().setEnabled(false);
    }
}
