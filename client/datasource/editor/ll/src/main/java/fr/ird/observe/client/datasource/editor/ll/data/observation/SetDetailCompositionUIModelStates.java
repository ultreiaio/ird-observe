package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.SetDetailCompositionUIValidationHelper;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.basket.BasketTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.branchline.BranchlineTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.section.SectionTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.template.SectionTemplateTableModel;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.data.ll.observation.BasketDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import fr.ird.observe.dto.data.ll.observation.SectionTemplateDto;
import fr.ird.observe.dto.data.ll.observation.SetDetailCompositionDto;
import fr.ird.observe.dto.form.Form;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.i18n.I18n;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@GenerateJavaBeanDefinition
public class SetDetailCompositionUIModelStates extends GeneratedSetDetailCompositionUIModelStates {

    //For validation
    static {
        I18n.n("observe.data.ll.observation.SetDetailCompositionUI.generateTabValid");
    }

    public static final String PROPERTY_CAN_GENERATE = "canGenerate";
    public static final String PROPERTY_GENERATE_TAB_VALID = "generateTabValid";
    public static final String PROPERTY_COMPOSITION_TAB_VALID = "compositionTabValid";
    public static final String PROPERTY_BRANCHLINE_DETAIL_TAB_VALID = "branchlineDetailTabValid";

    protected final SectionTemplateTableModel sectionTemplatesTableModel;
    protected final SectionTableModel sectionsTableModel;
    protected final BasketTableModel basketsTableModel;
    protected final BranchlineTableModel branchlinesTableModel;
    /**
     * Branchline edit bean.
     */
    private final BranchlineDto branchlineBean;

    /**
     * To avoid section flush when selected section changes (used when deleting a section).
     */
    public boolean skipSavePreviousSelectedSection;
    /**
     * To avoid basket flush when selection basket changes (used when deleting a basket).
     */
    public boolean skipSavePreviousSelectedBasket;
    /**
     * To avoid branchline flush when selection branchline changes (used when deleting a branchline).
     */
    public boolean skipSavePreviousSelectedBranchline;
    /**
     * To avoid branchline flush when basket is adjusting (used when changing selected basket).
     */
    public boolean basketAdjusting;
    /**
     * To avoid basket flush when section is adjusting (used when changing selected section).
     */
    public boolean sectionAdjusting;
    protected boolean canGenerate;
    protected boolean generateTabValid;
    protected boolean compositionTabValid;
    protected boolean branchlineDetailTabValid;
    private SetDetailCompositionUIValidationHelper validationHelper;
    private SetDetailCompositionUI ui;

    public SetDetailCompositionUIModelStates(GeneratedSetDetailCompositionUIModel model) {
        super(model);
        this.sectionTemplatesTableModel = new SectionTemplateTableModel();
        this.sectionsTableModel = new SectionTableModel((SetDetailCompositionUIModel) model);
        this.basketsTableModel = new BasketTableModel((SetDetailCompositionUIModel) model);
        this.branchlinesTableModel = new BranchlineTableModel((SetDetailCompositionUIModel) model);
        this.branchlineBean = new BranchlineDto();
    }

    @Override
    public void init(ContentUI ui0, DefaultUIInitializerResult initializerResult) {
        super.init(ui0, initializerResult);
        ui = (SetDetailCompositionUI) ui0;
        validationHelper = new SetDetailCompositionUIValidationHelper(ui);
        getSectionTemplatesTableModel().init(ui);
        getSectionsTableModel().init();
        getBasketsTableModel().init(ui);
        getBranchlinesTableModel().init();
    }

    @Override
    protected void loadReferentialCacheOnOpenForm(Form<SetDetailCompositionDto> form) {
        super.loadReferentialCacheOnOpenForm(form);
        getReferenceCache().loadExtraReferentialReferenceSetsInModel(BranchlineDto.class);
    }

    public SectionTemplateTableModel getSectionTemplatesTableModel() {
        return sectionTemplatesTableModel;
    }

    public BasketTableModel getBasketsTableModel() {
        return basketsTableModel;
    }

    public SectionTableModel getSectionsTableModel() {
        return sectionsTableModel;
    }

    public BranchlineTableModel getBranchlinesTableModel() {
        return branchlinesTableModel;
    }

    public BranchlineDto getBranchlineBean() {
        return branchlineBean;
    }

    public boolean isHaulingDirectionSameAsSettings() {
        Boolean haulingDirectionSameAsSetting = getBean().getHaulingDirectionSameAsSetting();
        return haulingDirectionSameAsSetting != null && haulingDirectionSameAsSetting;
    }

    public boolean isGenerateHaulingIds() {
        return true;
    }

    public boolean isCanGenerate() {
        return canGenerate;
    }

    public void setCanGenerate(boolean canGenerate) {
        this.canGenerate = canGenerate;
        firePropertyChange(PROPERTY_CAN_GENERATE, null, canGenerate);
    }

    public boolean isBranchlineDetailTabValid() {
        return branchlineDetailTabValid;
    }

    public void setBranchlineDetailTabValid(boolean branchlineDetailTabValid) {
        this.branchlineDetailTabValid = branchlineDetailTabValid;
        firePropertyChange(PROPERTY_BRANCHLINE_DETAIL_TAB_VALID, null, branchlineDetailTabValid);
    }

    public boolean isCompositionTabValid() {
        return compositionTabValid;
    }

    public void setCompositionTabValid(boolean compositionTabValid) {
        this.compositionTabValid = compositionTabValid;
        firePropertyChange(PROPERTY_COMPOSITION_TAB_VALID, null, compositionTabValid);
    }

    @Override
    public SetDetailCompositionDto getBeanToSave() {
        SetDetailCompositionDto bean = getBean();
        SectionTableModel sectionsTableModel = getSectionsTableModel();
        SectionDto selectedSection = sectionsTableModel.getSelectedRow();
        if (selectedSection != null) {
            // flush selected section before save
            getBasketsTableModel().flushSection(selectedSection, getBranchlinesTableModel());
        }
        bean.autoTrim();
        return bean;
    }

    public boolean isSkipSavePreviousSelectedSection() {
        return skipSavePreviousSelectedSection;
    }

    public void setSkipSavePreviousSelectedSection(boolean skipSavePreviousSelectedSection) {
        this.skipSavePreviousSelectedSection = skipSavePreviousSelectedSection;
    }

    public boolean isSkipSavePreviousSelectedBasket() {
        return skipSavePreviousSelectedBasket;
    }

    public void setSkipSavePreviousSelectedBasket(boolean skipSavePreviousSelectedBasket) {
        this.skipSavePreviousSelectedBasket = skipSavePreviousSelectedBasket;
    }

    public boolean isSkipSavePreviousSelectedBranchline() {
        return skipSavePreviousSelectedBranchline;
    }

    public void setSkipSavePreviousSelectedBranchline(boolean skipSavePreviousSelectedBranchline) {
        this.skipSavePreviousSelectedBranchline = skipSavePreviousSelectedBranchline;
    }

    public boolean isBasketAdjusting() {
        return basketAdjusting;
    }

    public void setBasketAdjusting(boolean basketAdjusting) {
        this.basketAdjusting = basketAdjusting;
    }

    public boolean isSectionAdjusting() {
        return sectionAdjusting;
    }

    public void setSectionAdjusting(boolean sectionAdjusting) {
        this.sectionAdjusting = sectionAdjusting;
    }

    public boolean isGenerateTabValid() {
        return generateTabValid;
    }

    public void setGenerateTabValid(boolean generateTabValid) {
        this.generateTabValid = generateTabValid;
        firePropertyChange(PROPERTY_GENERATE_TAB_VALID, null, generateTabValid);
    }

    public void removeAllSections() {
        validationHelper.setObjectValueAdjusting(true);
        try {
            getSectionsTableModel().setData(new ArrayList<>());
            getBean().getSection().clear();
            setModified(true);
        } finally {
            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenSectionChanged();
        }
    }

    public void generateSections(SectionTemplateDto template, Integer nbSections, Integer basketsCount, Integer nbBranchlines) {
        boolean usingTemplate = template != null;
        validationHelper.setObjectValueAdjusting(true);
        try {
            SectionTableModel sectionsTableModel = getSectionsTableModel();
            BasketTableModel basketsTableModel = getBasketsTableModel();
            BranchlineTableModel branchlinesTableModel = getBranchlinesTableModel();
            List<SectionDto> sections = new ArrayList<>(nbSections);
            for (int sectionNumber = 0; sectionNumber < nbSections; sectionNumber++) {
                SectionDto section = sectionsTableModel.createNewRow();
                sections.add(section);
                for (int basketNumber = 0; basketNumber < basketsCount; basketNumber++) {
                    BasketDto basket = basketsTableModel.createNewRow();
                    section.getBasket().add(basket);
                    for (int branchlineNumber = 0; branchlineNumber < nbBranchlines; branchlineNumber++) {
                        BranchlineDto branchline = branchlinesTableModel.createNewRow();
                        basket.getBranchline().add(branchline);
                    }
                    LinkedList<BranchlineDto> branchlines = new LinkedList<>(basket.getBranchline());
                    branchlinesTableModel.rearrangeIds(branchlines);
                }
                List<BasketDto> baskets = new LinkedList<>(section.getBasket());
                basketsTableModel.rearrangeIds(baskets);
                if (usingTemplate) {
                    section.setSectionTemplate(template);
                    template.applyToBaskets(baskets);
                }
            }
            sectionsTableModel.rearrangeIds(sections);
            sectionsTableModel.setData(sections);
            getBean().getSection().addAll(sections);
            setModified(true);
        } finally {
            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenSectionChanged();
        }
    }

    public void addSection(boolean before) {
        validationHelper.setObjectValueAdjusting(true);
        try {
            SectionTableModel tableModel = getSectionsTableModel();
            SectionDto newSectionWithTemplate = tableModel.insert(before);
            SetDetailCompositionDto bean = getBean();
            List<SectionDto> sections = new ArrayList<>(bean.getSection());
            sections.add(tableModel.getSelectedRowIndex(), newSectionWithTemplate);
            bean.getSection().clear();
            bean.getSection().addAll(sections);
        } finally {
            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenSectionChanged();
        }
    }

    public void removeSection(SectionDto selectedSection) {
        validationHelper.setObjectValueAdjusting(true);
        setSkipSavePreviousSelectedSection(true);
        try {
            getBean().getSection().remove(selectedSection);
            getSectionsTableModel().removeSelectedRow();
        } finally {
            setSkipSavePreviousSelectedSection(false);
            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenSectionChanged();
        }
    }

    public SetDetailCompositionUIValidationHelper getValidationHelper() {
        return validationHelper;
    }

    public SetDetailCompositionUI getUi() {
        return ui;
    }
}
