package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;

/**
 * Created on 12/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
class SetGlobalCompositionUIHandler extends GeneratedSetGlobalCompositionUIHandler {

    @Override
    protected void onBeforeInit(SetGlobalCompositionUI ui, ContentUIInitializer<SetGlobalCompositionUI> initializer) {
        initializer.registerDependencies(
                SetFloatlinesCompositionUIModel.create(ui),
                SetBranchlinesCompositionUIModel.create(ui),
                SetHooksCompositionUIModel.create(ui),
                SetBaitsCompositionUIModel.create(ui));
    }

    @Override
    public void onInit(SetGlobalCompositionUI ui) {
        super.onInit(ui);
        getContentOpen().initTabUI(ui.getFloatlinesCompositionUI(), ui.getFloatlinesCompositionPanel(), 0, d -> ui.getModel().getStates().getBean().setFloatlinesComposition(d.getFloatlinesComposition()));
        getContentOpen().initTabUI(ui.getBranchlinesCompositionUI(), ui.getBranchlinesCompositionPanel(), 1, d -> ui.getModel().getStates().getBean().setBranchlinesComposition(d.getBranchlinesComposition()));
        getContentOpen().initTabUI(ui.getHooksCompositionUI(), ui.getHooksCompositionPanel(), 2, d -> ui.getModel().getStates().getBean().setHooksComposition(d.getHooksComposition()));
        getContentOpen().initTabUI(ui.getBaitsCompositionUI(), ui.getBaitsCompositionPanel(), 3, d -> ui.getModel().getStates().getBean().setBaitsComposition(d.getBaitsComposition()));
    }

    @Override
    public void onMainTabChanged(int previousIndex, int selectedIndex) {
        super.onMainTabChanged(previousIndex, selectedIndex);
        ui.getFloatlinesCompositionUI().getSelectToolbar().setVisible(false);
        ui.getBranchlinesCompositionUI().getSelectToolbar().setVisible(false);
        ui.getHooksCompositionUI().getSelectToolbar().setVisible(false);
        ui.getBaitsCompositionUI().getSelectToolbar().setVisible(false);
        ContentTableUI<?, ?, ?> subUi = null;
        switch (selectedIndex) {
            case 0:
                subUi = ui.getFloatlinesCompositionUI();
                break;
            case 1:
                subUi = ui.getBranchlinesCompositionUI();
                break;
            case 2:
                subUi = ui.getHooksCompositionUI();
                break;
            case 3:
                subUi = ui.getBaitsCompositionUI();
                break;
        }
        getContentOpen().updateConfigurePopup(subUi, true);
    }
}
