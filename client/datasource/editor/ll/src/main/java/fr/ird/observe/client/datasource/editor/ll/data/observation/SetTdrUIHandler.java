package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.init.UIInitHelper;
import io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;

/**
 * Created on 9/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class SetTdrUIHandler extends GeneratedSetTdrUIHandler {

    @Override
    public void onInit(SetTdrUI ui) {
        super.onInit(ui);
        UIInitHelper.setAction(ui.getEnableTimestamp(), new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onEnableTimestampChanged(((BeanCheckBox) e.getSource()).isSelected());
            }
        });
    }

    void onEnableTimestampChanged(boolean enableTimestamp) {
        SetTdrUIModelStates states = getModel().getStates();
        states.updateTableEditBeanTimestamps(enableTimestamp, true);
    }

}
