package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.LonglinePositionHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.data.ll.observation.SetTdrDto;
import fr.ird.observe.dto.data.ll.observation.TdrDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Date;

@GenerateJavaBeanDefinition
public class SetTdrUIModelStates extends GeneratedSetTdrUIModelStates {

    public static final String PROPERTY_ENABLE_TIMESTAMP = "enableTimestamp";
    protected LonglinePositionHelper<TdrDto> positionHelper;

    private boolean enableTimestamp;

    public SetTdrUIModelStates(GeneratedSetTdrUIModel model) {
        super(model);
    }

    @Override
    public void init(ContentUI ui, DefaultUIInitializerResult initializerResult) {
        super.init(ui, initializerResult);
        SetTdrUI ui1 = (SetTdrUI) ui;
        positionHelper = new LonglinePositionHelper<>(ui1.getSection(), ui1.getBasket(), ui1.getBranchline(), ui1.getTableEditBean());
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(TdrDto.PROPERTY_SPECIES, ReferencesFilterHelper.newLlSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListLonglineObservationCatchId()));
    }

    @Override
    protected void copyFormToBean(Form<SetTdrDto> form) {
        positionHelper.initSections(form.getObject(), form.getObject().getTdr());
        super.copyFormToBean(form);
    }

    public void resetPosition(TdrDto dto) {
        positionHelper.resetPosition(dto);
    }

    public boolean isEnableTimestamp() {
        return enableTimestamp;
    }

    public void setEnableTimestamp(boolean enableTimestamp) {
        boolean oldValue = this.enableTimestamp;
        this.enableTimestamp = enableTimestamp;
        firePropertyChange(PROPERTY_ENABLE_TIMESTAMP, oldValue, enableTimestamp);
    }

    public void updateTableEditBeanTimestamps(boolean enableTimestamp, boolean forceNewValue) {
        TdrDto tableEditBean = getTableEditBean();
        if (enableTimestamp) {
            if (forceNewValue) {
                // assign new default values from main bean
                Date timestamp = getBean().getSettingStartTimeStamp();
                setTimestamp(tableEditBean, timestamp);
            }
        } else {
            // remove timestamps
            setTimestamp(tableEditBean, null);
        }
        setEnableTimestamp(enableTimestamp);
    }

    private void setTimestamp(TdrDto bean, Date timestamp) {
        bean.setDeploymentStart(timestamp);
        bean.setDeploymentEnd(timestamp);
        bean.setFishingStart(timestamp);
        bean.setFishingEnd(timestamp);
    }
}
