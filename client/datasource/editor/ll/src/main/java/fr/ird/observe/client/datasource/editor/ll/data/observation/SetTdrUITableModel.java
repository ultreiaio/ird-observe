package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.observation.TdrDto;

class SetTdrUITableModel extends GeneratedSetTdrUITableModel {
    private static final long serialVersionUID = 1L;

    public SetTdrUITableModel(SetTdrUI ui) {
        super(ui);
    }

    @Override
    protected void onSelectedRowChanged(SetTdrUI ui, int editingRow, TdrDto tableEditBean, TdrDto previousRowBean, boolean notPersisted, boolean newRow) {
        boolean withTimestamp = tableEditBean.getFishingStart() != null;
        SetTdrUIModelStates states = ui.getModel().getStates();
        states.updateTableEditBeanTimestamps(withTimestamp, newRow);
        states.resetPosition(tableEditBean);
        super.onSelectedRowChanged(ui, editingRow, tableEditBean, previousRowBean, notPersisted, newRow);
    }

    @Override
    protected void onAfterResetRow(int row) {
        SetTdrUIModelStates states = getModel().getStates();
        TdrDto tableEditBean = states.getTableEditBean();
        boolean withTimestamp = tableEditBean.getFishingStart() != null;
        states.setEnableTimestamp(withTimestamp);
    }
}
