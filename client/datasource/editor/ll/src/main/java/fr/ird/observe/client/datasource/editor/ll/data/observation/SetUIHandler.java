package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.CopyCoordinate;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveAction;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.actions.SaveContentEditUIAdapter;
import fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes;
import fr.ird.observe.client.datasource.editor.ll.data.common.TripUINavigationNode;
import fr.ird.observe.dto.data.ll.common.TripReference;
import fr.ird.observe.dto.data.ll.observation.SetDto;
import io.ultreia.java4all.i18n.I18n;

import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * Created on 9/1/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class SetUIHandler extends GeneratedSetUIHandler {

    private final PropertyChangeListener shooterUsedChanged;
    private final PropertyChangeListener weightedSwivelChanged;
    private final PropertyChangeListener weightedSnapChanged;

    SetUIHandler() {
        shooterUsedChanged = evt -> onShooterUsedChanged((Boolean) evt.getNewValue());
        weightedSwivelChanged = evt -> onWeightedSwivelChanged((Boolean) evt.getNewValue());
        weightedSnapChanged = evt -> onWeightedSnapChanged((Boolean) evt.getNewValue());
    }

    //FIXME:Focus getHomeId/getSettingShape/getHaulingDirectionSameAsSetting/getHaulingDirectionSameAsSetting/

    @Override
    public void startEditUI() {

        SetUIModel model = getModel();
        SetDto bean = model.getStates().getBean();
        bean.removePropertyChangeListener(fr.ird.observe.dto.data.ll.logbook.SetDto.PROPERTY_SHOOTER_USED, shooterUsedChanged);
        bean.removePropertyChangeListener(fr.ird.observe.dto.data.ll.logbook.SetDto.PROPERTY_WEIGHTED_SNAP, weightedSnapChanged);
        bean.removePropertyChangeListener(fr.ird.observe.dto.data.ll.logbook.SetDto.PROPERTY_WEIGHTED_SWIVEL, weightedSwivelChanged);

        super.startEditUI();

        bean.addPropertyChangeListener(fr.ird.observe.dto.data.ll.logbook.SetDto.PROPERTY_SHOOTER_USED, shooterUsedChanged);
        bean.addPropertyChangeListener(fr.ird.observe.dto.data.ll.logbook.SetDto.PROPERTY_WEIGHTED_SNAP, weightedSnapChanged);
        bean.addPropertyChangeListener(fr.ird.observe.dto.data.ll.logbook.SetDto.PROPERTY_WEIGHTED_SWIVEL, weightedSwivelChanged);

        onShooterUsedChanged(bean.isShooterUsed());
        onWeightedSnapChanged(bean.isWeightedSnap());
        onWeightedSwivelChanged(bean.isWeightedSwivel());

        model.getStates().setModified(model.getStates().isCreatingMode());
    }

    @Override
    protected void installSaveAction() {
        SaveAction
                .create(ui, SetDto.class)
                .on(ui.getModel()::toSaveRequest)
                .call((r, d) -> getEditableService().save(r.getParentId(), d))
                .then(new SaveContentEditUIAdapter<>() {
                    @Override
                    public void adaptUi(DataSourceEditor dataSourceEditor, SetUI ui, boolean notPersisted, SetDto savedBean) {

                        // reload trip data then default save
                        TripUINavigationNode parentNode = (TripUINavigationNode) ui.getModel().getSource().upToReferenceNode(TripReference.class);

                        getClientValidationContext().reset();
                        parentNode.nodeChanged();
                        super.adaptUi(dataSourceEditor, ui, notPersisted, savedBean);
                    }
                })
                .install(ui.getSave());
    }

    @Override
    protected void installExtraActions() {
        super.installExtraActions();
        CopyCoordinate.install(I18n.t("observe.ui.action.copySettingStartCoordinate"),
                               ObserveLLKeyStrokes.KEY_STROKE_COPY_SETTING_START_COORDINATE,
                               ui,
                               ui.getSettingStart(),
                               ui.getSettingEnd(),
                               ui.getCopySettingStartCoordinate());
        CopyCoordinate.install(I18n.t("observe.ui.action.copySettingEndCoordinate"),
                               ObserveLLKeyStrokes.KEY_STROKE_COPY_SETTING_END_COORDINATE,
                               ui,
                               ui.getSettingEnd(),
                               ui.getHaulingStart(),
                               ui.getCopySettingEndCoordinate());
        CopyCoordinate.install(I18n.t("observe.ui.action.copyHaulingStartCoordinate"),
                               ObserveLLKeyStrokes.KEY_STROKE_COPY_HAULING_START_COORDINATE,
                               ui,
                               ui.getHaulingStart(),
                               ui.getHaulingEnd(),
                               ui.getCopyHaulingStartCoordinate());
    }

    private void onShooterUsedChanged(boolean newValue) {
        if (!Objects.equals(true, newValue)) {
            getModel().getStates().getBean().setShooterSpeed(null);
        }
    }

    private void onWeightedSnapChanged(boolean newValue) {
        if (!Objects.equals(true, newValue)) {
            getModel().getStates().getBean().setSnapWeight(null);
        }
    }

    private void onWeightedSwivelChanged(boolean newValue) {
        if (!Objects.equals(true, newValue)) {
            getModel().getStates().getBean().setSwivelWeight(null);
        }
    }

}
