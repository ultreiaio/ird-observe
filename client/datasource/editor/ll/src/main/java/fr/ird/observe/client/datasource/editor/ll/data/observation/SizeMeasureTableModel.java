package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableListProperty;
import fr.ird.observe.client.util.table.EditableTableModelWithCache;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.ll.observation.SizeMeasureDto;
import fr.ird.observe.dto.referential.common.SizeMeasureMethodReference;
import fr.ird.observe.dto.referential.common.SizeMeasureTypeReference;
import io.ultreia.java4all.decoration.Decorator;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created on 12/3/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public class SizeMeasureTableModel extends EditableTableModelWithCache<SizeMeasureDto> {

    private static final long serialVersionUID = 1L;
    /**
     * Default size measure method to use for new data.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2932">issue 2932</a>
     */
    private SizeMeasureMethodReference defaultSizeMeasureMethod;

    public SizeMeasureTableModel(EditableListProperty<SizeMeasureDto> listProperty) {
        super(listProperty);
    }

    public static void onInit(SetCatchUI ui) {
        EditableTable<SizeMeasureDto, SizeMeasureTableModel> table = ui.getSizeMeasuresTable();
        SizeMeasureTableModel tableModel = ui.getSizeMeasuresTableModel();
        ui.getTableModel().registerInlineModel(tableModel, table);
        table.setRowHeight(22);
        JXTableUtil.setI18nTableHeaderRenderer(table,
                                               SizeMeasureDto.class,
                                               SizeMeasureDto.PROPERTY_SIZE_MEASURE_TYPE,
                                               SizeMeasureDto.PROPERTY_SIZE,
                                               SizeMeasureDto.PROPERTY_SIZE_MEASURE_METHOD);

        SetCatchUIHandler handler = ui.getHandler();
        DecoratorService decoratorService = handler.getDecoratorService();
        Decorator sizeMeasureTypeDecorator = decoratorService.getDecoratorByType(SizeMeasureTypeReference.class);
        Decorator sizeMeasureMethodDecorator = decoratorService.getDecoratorByType(SizeMeasureMethodReference.class);
        JXTableUtil.initRenderers(table,
                                  JXTableUtil.newDecoratedRenderer(sizeMeasureTypeDecorator),
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newDecoratedRenderer(sizeMeasureMethodDecorator));

        List<SizeMeasureTypeReference> sizeMeasureTypes = handler.getReferentialReferences(SizeMeasureTypeReference.class);
        List<SizeMeasureMethodReference> sizeMeasureMethods = handler.getReferentialReferences(SizeMeasureMethodReference.class);
        tableModel.defaultSizeMeasureMethod = sizeMeasureMethods.stream().filter(r -> Objects.equals(r.getId(), ProtectedIdsLl.LL_OBSERVATION_SIZE_MEASURE_DEFAULT_SIZE_MEASURE_METHOD_ID)).findFirst().orElse(null);
        JXTableUtil.initEditors(table,
                                UIHelper.newCellEditor(sizeMeasureTypes, sizeMeasureTypeDecorator),
                                NumberCellEditor.newFloatColumnEditor(),
                                UIHelper.newCellEditor(sizeMeasureMethods, sizeMeasureMethodDecorator));

        table.init(ui, ui.getValidatorSizeMeasure());
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt0(SizeMeasureDto row, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return row.getSizeMeasureType();
            case 1:
                return row.getSize();
            case 2:
                return row.getSizeMeasureMethod();
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public boolean setValueAt0(SizeMeasureDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue = getValueAt0(row, rowIndex, columnIndex);
        switch (columnIndex) {
            case 0:
                row.setSizeMeasureType((SizeMeasureTypeReference) aValue);
                break;
            case 1:
                row.setSize((Float) aValue);
                break;
            case 2:
                row.setSizeMeasureMethod((SizeMeasureMethodReference) aValue);
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    @Override
    protected SizeMeasureDto createNewRow() {
        SizeMeasureDto result = SizeMeasureDto.newDto(new Date());
        result.setSizeMeasureMethod(defaultSizeMeasureMethod);
        return result;
    }

    @Override
    protected boolean isCanAddEmptyRow() {
        return false;
    }
}
