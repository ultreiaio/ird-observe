package fr.ird.observe.client.datasource.editor.ll.data.observation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableListProperty;
import fr.ird.observe.client.util.table.EditableTableModelWithCache;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.ll.observation.WeightMeasureDto;
import fr.ird.observe.dto.referential.common.WeightMeasureMethodReference;
import fr.ird.observe.dto.referential.common.WeightMeasureTypeReference;
import io.ultreia.java4all.decoration.Decorator;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created on 12/3/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public class WeightMeasureTableModel extends EditableTableModelWithCache<WeightMeasureDto> {

    private static final long serialVersionUID = 1L;
    /**
     * Default weight measure method to use for new data.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2932">issue 2932</a>
     */
    private WeightMeasureMethodReference defaultWeightMeasureMethod;

    public WeightMeasureTableModel(EditableListProperty<WeightMeasureDto> listProperty) {
        super(listProperty);
    }

    public static void onInit(SetCatchUI ui) {
        EditableTable<WeightMeasureDto, WeightMeasureTableModel> table = ui.getWeightMeasuresTable();
        WeightMeasureTableModel tableModel = ui.getWeightMeasuresTableModel();
        ui.getTableModel().registerInlineModel(tableModel, table);
        table.setRowHeight(22);
        JXTableUtil.setI18nTableHeaderRenderer(table,
                                               WeightMeasureDto.class,
                                               WeightMeasureDto.PROPERTY_WEIGHT_MEASURE_TYPE,
                                               WeightMeasureDto.PROPERTY_WEIGHT,
                                               WeightMeasureDto.PROPERTY_WEIGHT_MEASURE_METHOD);

        DecoratorService decoratorService = ui.getHandler().getDecoratorService();
        Decorator weightMeasureTypeDecorator = decoratorService.getDecoratorByType(WeightMeasureTypeReference.class);
        Decorator weightMeasureMethodDecorator = decoratorService.getDecoratorByType(WeightMeasureMethodReference.class);
        JXTableUtil.initRenderers(table,
                                  JXTableUtil.newDecoratedRenderer(weightMeasureTypeDecorator),
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newDecoratedRenderer(weightMeasureMethodDecorator));
        List<WeightMeasureTypeReference> weightMeasureTypes = ui.getHandler().getReferentialReferences(WeightMeasureTypeReference.class);
        List<WeightMeasureMethodReference> weightMeasureMethods = ui.getHandler().getReferentialReferences(WeightMeasureMethodReference.class);
        tableModel.defaultWeightMeasureMethod = weightMeasureMethods.stream().filter(r -> Objects.equals(r.getId(), ProtectedIdsLl.LL_OBSERVATION_WEIGHT_MEASURE_DEFAULT_WEIGHT_MEASURE_METHOD_ID)).findFirst().orElse(null);

        JXTableUtil.initEditors(table,
                                UIHelper.newCellEditor(weightMeasureTypes, weightMeasureTypeDecorator),
                                NumberCellEditor.newFloatColumnEditor(),
                                UIHelper.newCellEditor(weightMeasureMethods, weightMeasureMethodDecorator));
        table.init(ui, ui.getValidatorWeightMeasure());
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt0(WeightMeasureDto row, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return row.getWeightMeasureType();
            case 1:
                return row.getWeight();
            case 2:
                return row.getWeightMeasureMethod();
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public boolean setValueAt0(WeightMeasureDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue = getValueAt0(row, rowIndex, columnIndex);
        switch (columnIndex) {
            case 0:
                row.setWeightMeasureType((WeightMeasureTypeReference) aValue);
                break;
            case 1:
                row.setWeight((Float) aValue);
                break;
            case 2:
                row.setWeightMeasureMethod((WeightMeasureMethodReference) aValue);
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    @Override
    protected WeightMeasureDto createNewRow() {
        WeightMeasureDto result = WeightMeasureDto.newDto(new Date());
        result.setWeightMeasureMethod(defaultWeightMeasureMethod);
        return result;
    }

    @Override
    protected boolean isCanAddEmptyRow() {
        return false;
    }

}
