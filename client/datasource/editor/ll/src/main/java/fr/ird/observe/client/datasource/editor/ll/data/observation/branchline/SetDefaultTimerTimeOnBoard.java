package fr.ird.observe.client.datasource.editor.ll.data.observation.branchline;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;

import java.awt.event.ActionEvent;
import java.util.Date;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 15/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public abstract class SetDefaultTimerTimeOnBoard<U extends ContentUI> extends ContentUIActionSupport<U> {

    public SetDefaultTimerTimeOnBoard() {
        super("", t("observe.data.ll.observation.Branchline.setDefaultTimerTimeOnBoard.tip"), "combobox-reset2", ObserveLLKeyStrokes.KEY_STROKE_SET_DEFAULT_TIMER_TIME_ON_BOARD);
    }

    protected abstract Date getHaulingStartTimeStamp(U ui);

    protected abstract BranchlineDto getBranchlineBean(U ui);

    @Override
    protected void doActionPerformed(ActionEvent e, U ui) {
        BranchlineDto branchlineBean = getBranchlineBean(ui);
        Date haulingStartTimeStamp = getHaulingStartTimeStamp(ui);
        branchlineBean.setTimerTimeOnBoard(haulingStartTimeStamp);
    }
}
