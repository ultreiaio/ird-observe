package fr.ird.observe.client.datasource.editor.ll.data.observation.composition;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUIModelStates;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUI;
import fr.ird.observe.client.util.table.EditableTableModel;
import fr.ird.observe.client.util.table.popup.AutoSelectRowAndShowPopupActionSupport;
import fr.ird.observe.dto.data.InlineDataDto;
import fr.ird.observe.dto.data.ll.observation.SetDetailCompositionDto;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

/**
 * Created on 05/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class CompositionAutoSelectRowAndShowPopupAction extends AutoSelectRowAndShowPopupActionSupport {

    private final JMenuItem delete;
    private final JMenuItem insertBefore;
    private final JMenuItem insertAfter;
    private final ContentSimpleUIModelStates<SetDetailCompositionDto> states;

    public CompositionAutoSelectRowAndShowPopupAction(SetDetailCompositionUI ui, JTable table, JPopupMenu popup, JMenuItem delete, JMenuItem insertBefore, JMenuItem insertAfter) {
        super(table, popup);
        this.delete = delete;
        this.insertBefore = insertBefore;
        this.insertAfter = insertAfter;
        this.states = ui.getModel().getStates();
    }

    @Override
    protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {
        @SuppressWarnings("unchecked") EditableTableModel<InlineDataDto> model = (EditableTableModel<InlineDataDto>) getTable().getModel();
        boolean selectionNotEmpty = !model.isSelectionEmpty();
        boolean selectedRowIsNotEmpty = false;
        if (selectionNotEmpty) {
            InlineDataDto selectedData = model.getSelectedRow();
            selectedRowIsNotEmpty = model.isRowNotEmpty(selectedData);
        }

        boolean editable = !states.isReadingMode();
        boolean canDelete = computeCanDelete(editable, selectionNotEmpty, selectedRowIsNotEmpty);
        boolean canInsertBefore = computeCanInsertBefore(editable, selectionNotEmpty, selectedRowIsNotEmpty);
        boolean canInsertAfter = computeCanInsertAfter(editable, selectionNotEmpty, selectedRowIsNotEmpty);

        delete.setEnabled(canDelete);
        insertBefore.setEnabled(canInsertBefore);
        insertAfter.setEnabled(canInsertAfter);
    }

    protected boolean computeCanDelete(boolean editable, boolean selectionNotEmpty, boolean selectedRowIsNotEmpty) {
        boolean canDelete = false;
        if (editable && selectionNotEmpty) {
            // can delete only a non empty selected row
            canDelete = selectedRowIsNotEmpty;
        }
        return canDelete;
    }

    protected boolean computeCanInsertBefore(boolean editable, boolean selectionNotEmpty, boolean selectedRowIsNotEmpty) {
        boolean canInsertBefore = editable;
        if (editable && selectionNotEmpty) {
            // won't add before an empty row
            canInsertBefore = selectedRowIsNotEmpty;
        }
        return canInsertBefore;
    }

    protected boolean computeCanInsertAfter(boolean editable, boolean selectionNotEmpty, boolean selectedRowIsNotEmpty) {
        boolean canInsertAfter = editable;
        if (editable && selectionNotEmpty) {
            // won't after before an empty row
            canInsertAfter = selectedRowIsNotEmpty;
        }
        return canInsertAfter;
    }
}
