package fr.ird.observe.client.datasource.editor.ll.data.observation.composition;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModelStates;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.section.SectionTableModel;
import fr.ird.observe.dto.data.ll.observation.LonglineElementAware;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 10/07/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DeleteAll extends SetDetailCompositionUIActionSupport {

    private static final Logger log = LogManager.getLogger(DeleteAll.class);

    public DeleteAll() {
        super(t("observe.data.ll.observation.SetDetailComposition.action.deleteAllSections"), t("observe.data.ll.observation.SetDetailComposition.action.deleteAllSections.tip"), "content-delete-16", ObserveLLKeyStrokes.KEY_STROKE_DELETE_DATA_GLOBAL);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SetDetailCompositionUIModel model) {
        log.info("Trying to delete all sections.");
        SetDetailCompositionUIModelStates states = model.getStates();
        SectionTableModel sectionsTableModel = states.getSectionsTableModel();
        if (sectionsTableModel.isEmpty()) {
            // no section
            return;
        }
        boolean canDelete = sectionsTableModel.getNotEmptyData().stream().allMatch(LonglineElementAware::canDelete);
        if (!canDelete) {
            // there is some references, can't delete
            displayWarning(t("observe.data.ll.observation.SetDetailComposition.section.cant.delete.title"), t("observe.data.ll.observation.SetDetailComposition.section.cant.delete.message"));
            return;
        }
        int response = askToUser(t("observe.ui.title.delete"),
                               t("observe.data.ll.observation.SetDetailComposition.section.delete.message"),
                               JOptionPane.WARNING_MESSAGE,
                               new Object[]{t("observe.ui.choice.confirm.delete"),
                                       t("observe.ui.choice.cancel")},
                               1);
        if (response != 0) {
            // user cancel
            return;
        }
        log.info("Remove all sections.");
        states.removeAllSections();
    }

}
