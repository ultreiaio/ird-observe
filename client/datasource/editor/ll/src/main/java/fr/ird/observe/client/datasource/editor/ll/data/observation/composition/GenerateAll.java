package fr.ird.observe.client.datasource.editor.ll.data.observation.composition;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.ll.ObserveLLKeyStrokes;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModelStates;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.template.SectionTemplateTableModel;
import fr.ird.observe.dto.data.ll.observation.SectionTemplateDto;
import fr.ird.observe.dto.data.ll.observation.SetDetailCompositionDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 10/07/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GenerateAll extends SetDetailCompositionUIActionSupport {

    private static final Logger log = LogManager.getLogger(GenerateAll.class);

    public GenerateAll() {
        super(t("observe.data.ll.observation.SetDetailComposition.action.generateAllSections"), t("observe.data.ll.observation.SetDetailComposition.action.generateAllSections.tip"), "generate", ObserveLLKeyStrokes.KEY_STROKE_GENERATE);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SetDetailCompositionUIModel model) {
        log.info("Generate all sections.");
        SetDetailCompositionUIModelStates states = model.getStates();
        SetDetailCompositionDto bean = states.getBean();
        Integer nbSections = bean.getTotalSectionsCount();
        Integer basketsCount = bean.getBasketsPerSectionCount();
        Integer nbBranchlines = bean.getBranchlinesPerBasketCount();
        SectionTemplateDto template = null;
        SectionTemplateTableModel sectionTemplatesTableModel = states.getSectionTemplatesTableModel();
        List<SectionTemplateDto> sectionTemplates = sectionTemplatesTableModel.getNotEmptyData();
        if (sectionTemplates.size() == 1) {
            template = sectionTemplates.get(0);
            boolean compliantWithBasketCount = template.isCompliantWithBasketCount(basketsCount);
            if (!compliantWithBasketCount) {
                log.warn(String.format("sectionTemplate %s is not compliant with basketCount: %d", template, basketsCount));
                displayWarning(t("observe.data.ll.observation.SetDetailComposition.title.section.cant.use.firstTemplate"), t("observe.data.ll.observation.SetDetailComposition.firstTemplate.template.notCompliant.basketCount", template.getFloatlineLengths(), basketsCount));
                template = null;
            }
        }
        boolean usingTemplate = template != null;
        if (usingTemplate) {
            log.info(String.format("Will use sectionTemplate: %s", template));
        }
        states.generateSections(template, nbSections, basketsCount, nbBranchlines);
    }

}
