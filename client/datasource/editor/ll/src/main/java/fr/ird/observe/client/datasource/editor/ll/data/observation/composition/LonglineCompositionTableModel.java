package fr.ird.observe.client.datasource.editor.ll.data.observation.composition;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.table.EditableTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModel;
import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.data.ll.observation.LonglineElementAware;
import io.ultreia.java4all.bean.JavaBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created on 12/13/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.10
 */
public abstract class LonglineCompositionTableModel<D extends ObserveDto & JavaBean & LonglineElementAware> extends EditableTableModel<D> {

    private static final Logger log = LogManager.getLogger(LonglineCompositionTableModel.class);

    private static final long serialVersionUID = 1L;
    private final String prefix;
    private final SetDetailCompositionUIModel model;

    protected LonglineCompositionTableModel(SetDetailCompositionUIModel model) {
        super(false);
        this.model = model;
        prefix = model.getPrefix();
    }

    public SetDetailCompositionUIModel getModel() {
        return model;
    }

    protected String getLog(String message) {
        return prefix + message;
    }

    public void rearrangeIds(boolean fire) {
        rearrangeIds(getData());
        if (fire && !isEmpty()) {
            fireTableRowsUpdated(0, getRowCount() - 1);
        }
    }

    public void rearrangeIds(List<D> data) {
        if (data.isEmpty() || model.getStates().isResetEdit() || !model.getStates().isOpened()) {
            log.info("Skip rearrange ids for empty data or on reset action on {}" , this);
            return;
        }
        boolean generateHaulingIds = isGenerateHaulingIds();
        boolean haulingDirectionSameAsSettings = model.getStates().isHaulingDirectionSameAsSettings();
        int dataSize = data.size();
        log.info("Will rearrange ids for {} data on {} (generateHaulingIds ? {}, haulingDirectionSameAsSettings ? {})", dataSize, this, generateHaulingIds, haulingDirectionSameAsSettings);
        int index = 1;
        for (D d : data) {
            log.debug("SettingIdentifier : {}", index);
            d.setSettingIdentifier(index);
            if (generateHaulingIds) {
                int haulingId;
                if (haulingDirectionSameAsSettings) {
                    haulingId = index;
                } else {
                    haulingId = dataSize - index + 1;
                }
                log.debug("HaulingIdentifier : {}", haulingId);
                d.setHaulingIdentifier(haulingId);
            }
            index++;
        }
    }

    @Override
    public D addNewRow() {
        D row = super.addNewRow();
        validate();
        setModified(true);
        return row;
    }

    @Override
    public D insertAfterSelectedRow() {
        D row = super.insertAfterSelectedRow();
        validate();
        setModified(true);
        return row;
    }

    @Override
    public D insertBeforeSelectedRow() {
        D row = super.insertBeforeSelectedRow();
        validate();
        setModified(true);
        return row;
    }

    @Override
    public void removeData(int selectedRow) {
        super.removeData(selectedRow);
        validate();
        setModified(true);
    }

    protected boolean isGenerateHaulingIds() {
        return model.getStates().isGenerateHaulingIds();
    }

    @Override
    public void fireTableDataChanged() {
        // rearrange ids when data has changed
        rearrangeIds(false);
        super.fireTableDataChanged();
    }

    @Override
    public void fireTableRowsDeleted(int firstRow, int lastRow) {
        super.fireTableRowsDeleted(firstRow, lastRow);
        // must rearrange ids
        rearrangeIds(true);
    }

    @Override
    public void fireTableRowsInserted(int firstRow, int lastRow) {
        super.fireTableRowsInserted(firstRow, lastRow);
        // must rearrange ids
        rearrangeIds(true);
    }

    @Override
    public void removeSelectedRow() {
        super.removeSelectedRow();
        validate();
        setModified(true);
    }


    @Override
    public void setData(List<D> data) {
        super.setData(data);
        validate();
    }
}
