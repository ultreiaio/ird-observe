package fr.ird.observe.client.datasource.editor.ll.data.observation.composition;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.observation.BasketReference;
import fr.ird.observe.dto.data.ll.observation.BranchlineReference;
import fr.ird.observe.dto.data.ll.observation.CatchDto;
import fr.ird.observe.dto.data.ll.observation.LonglinePositionAware;
import fr.ird.observe.dto.data.ll.observation.LonglinePositionContainerAware;
import fr.ird.observe.dto.data.ll.observation.SectionReference;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 1/6/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.11
 */
public class LonglinePositionHelper<D extends LonglinePositionAware> {

    protected final FilterableComboBox<SectionReference> uiSection;
    protected final FilterableComboBox<BasketReference> uiBasket;
    protected final FilterableComboBox<BranchlineReference> uiBranchline;

    // To avoid any propagation when doing some works on locations
    protected boolean locationIsAdjusting;
    protected Collection<SectionReference> sectionUniverse;
    protected Collection<BasketReference> basketUniverse;
    protected Collection<BranchlineReference> branchlineUniverse;

    public LonglinePositionHelper(FilterableComboBox<SectionReference> uiSection,
                                  FilterableComboBox<BasketReference> uiBasket,
                                  FilterableComboBox<BranchlineReference> uiBranchline,
                                  D tableEditBean) {
        this.uiSection = uiSection;
        this.uiBasket = uiBasket;
        this.uiBranchline = uiBranchline;
        @SuppressWarnings("unchecked") PropertyChangeListener sectionChanged = evt -> onSectionChanged((SectionReference) evt.getNewValue(), (D) evt.getSource());
        tableEditBean.addPropertyChangeListener(CatchDto.PROPERTY_SECTION, sectionChanged);
        @SuppressWarnings("unchecked") PropertyChangeListener basketChanged = evt -> onBasketChanged((BasketReference) evt.getNewValue(), (D) evt.getSource());
        tableEditBean.addPropertyChangeListener(CatchDto.PROPERTY_BASKET, basketChanged);
    }

    public void initSections(LonglinePositionContainerAware positionSetDto, Collection<D> dtos) {

        this.sectionUniverse = positionSetDto.getSections();
        this.basketUniverse = positionSetDto.getBaskets();
        this.branchlineUniverse = positionSetDto.getBranchlines();
        for (D dto : dtos) {
            BranchlineReference branchline = dto.getBranchline();
            if (branchline != null) {
                BasketReference basket = getBasket(branchline);
                dto.setBasket(basket);
            }
            BasketReference basket = dto.getBasket();
            if (basket != null) {
                SectionReference section = getSection(basket);
                dto.setSection(section);
            }
        }
        uiSection.setData(getSections());
    }

    public void resetPosition(D dto) {
        SectionReference section = dto.getSection();
        BasketReference basket = dto.getBasket();
        BranchlineReference branchline = dto.getBranchline();
        uiBranchline.setSelectedItem(null);
        uiSection.setSelectedItem(null);
        uiBasket.setSelectedItem(null);
        if (section != null) {
            // reload section (basket and branchlines universe will then changed)
            uiSection.setSelectedItem(section);
        }
        if (basket != null) {
            // reload basket (branchlines universe will then changed)
            uiBasket.setSelectedItem(basket);
        }
        if (branchline != null) {
            // reload branchline
            uiBranchline.setSelectedItem(branchline);
        }
    }

    public void savePosition(List<D> dtoList) {
        for (D dto : dtoList) {
            if (dto.getBasket() != null) {
                // remove section
                dto.setSection(null);
            }
            if (dto.getBranchline() != null) {
                // remove basket
                dto.setBasket(null);
            }
        }
    }

    public List<SectionReference> getSections() {
        List<SectionReference> sections = new ArrayList<>();
        if (sectionUniverse != null) {
            sections.addAll(sectionUniverse);
        }
        return sections;
    }

    protected void onSectionChanged(SectionReference newValue, D dto) {
        locationIsAdjusting = true;
        try {
            BasketReference basket = dto.getBasket();
            BranchlineReference branchline = dto.getBranchline();
            // on désélectionne le panier
            dto.setBasket(null);
            // on désélectionne l'avançon
            dto.setBranchline(null);
            // on vide l'ensemble des paniers
            uiBasket.setData(Collections.emptyList());
            // on vide l'ensemble des avançons
            uiBranchline.setData(Collections.emptyList());
            if (newValue != null) {
                // une section est sélectionnée
                // on remplit uniquement les paniers de cette section
                List<BasketReference> baskets = getBaskets(newValue);
                uiBasket.setData(new ArrayList<>(baskets));
                if (basket != null && baskets.contains(basket)) {
                    // un panier est sélectionné
                    // on remplit uniquement les avançons du panier
                    List<BranchlineReference> branchlines = getBranchlines(basket);
                    uiBranchline.setData(new ArrayList<>(branchlines));
                    dto.setBasket(basket);
                    if (branchline != null && branchlines.contains(branchline)) {
                        // un avançon est sélectionné
                        dto.setBranchline(branchline);
                    }
                }
            }
        } finally {
            locationIsAdjusting = false;
        }
    }

    protected void onBasketChanged(BasketReference newValue, D dto) {
        if (!locationIsAdjusting) {
            BranchlineReference branchline = dto.getBranchline();
            // on désélectionne l'avançon
            dto.setBranchline(null);
            // on vide l'ensemble des avançons
            uiBranchline.setData(Collections.emptyList());
            if (newValue != null) {
                // un panier est sélectionné
                // on remplit uniquement les avançons des paniers
                List<BranchlineReference> branchlines = getBranchlines(newValue);
                uiBranchline.setData(branchlines);
                if (branchline != null && branchlines.contains(branchline)) {
                    // un avançon est sélectionné
                    dto.setBranchline(branchline);
                }
            }
        }
    }

    private SectionReference getSection(BasketReference basket) {
        String sectionId = basket.getParentId();
        return sectionUniverse.stream()
                .filter(s -> sectionId.equals(s.getId()))
                .findFirst()
                .orElse(null);
    }

    private BasketReference getBasket(BranchlineReference branchline) {
        String basketId = branchline.getParentId();
        return basketUniverse.stream()
                .filter(b -> basketId.equals(b.getId()))
                .findFirst()
                .orElse(null);
    }

    private List<BasketReference> getBaskets(SectionReference section) {
        return basketUniverse.stream()
                .filter(b -> section.getId().equals(b.getParentId()))
                .collect(Collectors.toList());
    }

    private List<BranchlineReference> getBranchlines(BasketReference basket) {
        return branchlineUniverse.stream()
                .filter(b -> basket.getId().equals(b.getParentId()))
                .collect(Collectors.toList());
    }

}
