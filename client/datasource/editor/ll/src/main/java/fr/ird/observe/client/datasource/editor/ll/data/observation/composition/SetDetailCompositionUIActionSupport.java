package fr.ird.observe.client.datasource.editor.ll.data.observation.composition;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUI;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModel;

import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

/**
 * Created on 11/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class SetDetailCompositionUIActionSupport extends ContentUIActionSupport<SetDetailCompositionUI> {
    protected SetDetailCompositionUIActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    protected abstract void doActionPerformed(ActionEvent e, SetDetailCompositionUIModel model);

    @Override
    protected void doActionPerformed(ActionEvent e, SetDetailCompositionUI ui) {
        doActionPerformed(e, ui.getModel());
    }
}
