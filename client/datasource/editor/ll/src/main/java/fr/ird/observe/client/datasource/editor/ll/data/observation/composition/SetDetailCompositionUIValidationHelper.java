package fr.ird.observe.client.datasource.editor.ll.data.observation.composition;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUI;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModel;
import fr.ird.observe.client.datasource.validation.ContentMessageTableModel;
import fr.ird.observe.dto.data.ll.observation.BasketDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.bean.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;

import javax.swing.JComponent;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 3/18/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.15
 */
public class SetDetailCompositionUIValidationHelper {

    private static final Logger log = LogManager.getLogger(SetDetailCompositionUIValidationHelper.class);

    private final SetDetailCompositionUIModel model;
    private final ContentMessageTableModel errorTableModel;
    private final SetDetailCompositionValidatorService validatorService;
    private final Set<BeanValidator<?>> validators;
    private boolean objectValueAdjusting;

    public SetDetailCompositionUIValidationHelper(SetDetailCompositionUI ui) {
        this.model = ui.getModel();
        this.errorTableModel = ui.getErrorTableModel();
        SwingValidator<SectionDto> sectionValidator = ui.getSectionValidator();
        SwingValidator<BasketDto> basketValidator = ui.getBasketValidator();
        SwingValidator<BranchlineDto> branchlineValidator = ui.getBranchlineValidator();
        this.validators = new LinkedHashSet<>(Arrays.asList(sectionValidator, basketValidator, branchlineValidator));
        Map<SwingValidator<?>, JComponent> validatorEditor = Map.of(
                sectionValidator, ui.getSectionsPane(),
                basketValidator, ui.getBasketsPane(),
                branchlineValidator, ui.getBranchlinesPane());
        this.validatorService = new SetDetailCompositionValidatorService(sectionValidator, basketValidator, branchlineValidator, validatorEditor);
    }

    public void whenSectionChanged() {
        if (!model.getStates().isOpened() || objectValueAdjusting) {
            return;
        }
        log.info("Rebuild messages, section model changes.");
        List<SectionDto> notEmptyData = model.getStates().getSectionsTableModel().getNotEmptyData();
        List<SwingValidatorMessage> messages = validatorService.validateSections(notEmptyData);
        removeOldMessages();
        errorTableModel.addMessages(messages);
        long errorMessagesCount = errorTableModel.getData().stream().filter(m -> m.getScope() == NuitonValidatorScope.ERROR && validators.contains(m.getValidator())).count();
        model.getStates().setCompositionTabValid(errorMessagesCount == 0);
    }

    public void whenBasketChanged() {
        if (!model.getStates().isOpened() || objectValueAdjusting) {
            return;
        }
        SectionDto selectedSection = model.getStates().getSectionsTableModel().getSelectedRow();
        if (selectedSection == null) {
            return;
        }
        log.info("Rebuild messages, basket model changes.");
        SetDetailCompositionValidationContext validationContext = validatorService.createSetDetailCompositionValidationContext();
        List<SwingValidatorMessage> messages;
        try {
            validatorService.validateSection(validationContext, selectedSection);
            messages = validationContext.getMessages();
            String contextLabel = validationContext.getContextLabel();
            removeOldMessages(contextLabel);
            errorTableModel.addMessages(messages);
        } finally {
            validatorService.destroySetDetailCompositionValidationContext(validationContext);
        }
        long errorMessagesCount = errorTableModel.getData().stream().filter(m -> m.getScope() == NuitonValidatorScope.ERROR && validators.contains(m.getValidator())).count();
        model.getStates().setCompositionTabValid(errorMessagesCount == 0);
    }

    public void whenBranchlineChanged() {
        if(!model.getStates().isOpened() || objectValueAdjusting) {
            return;
        }
        SectionDto selectedSection = model.getStates().getSectionsTableModel().getSelectedRow();
        if (selectedSection == null) {
            return;
        }
        BasketDto selectedBasket = model.getStates().getBasketsTableModel().getSelectedRow();
        if (selectedBasket == null) {
            return;
        }
        log.info("Rebuild messages, branchline model changes.");
        SetDetailCompositionValidationContext validationContext = validatorService.createSetDetailCompositionValidationContext();
        try {
            validatorService.validateSection(validationContext, selectedSection);
            List<SwingValidatorMessage> messages = validationContext.getMessages();
            String contextLabel = validationContext.getContextLabel();
            removeOldMessages(contextLabel);
            errorTableModel.addMessages(messages);
        } finally {
            validatorService.destroySetDetailCompositionValidationContext(validationContext);
        }
        long errorMessagesCount = errorTableModel.getData().stream().filter(m -> m.getScope() == NuitonValidatorScope.ERROR && validators.contains(m.getValidator())).count();
        model.getStates().setCompositionTabValid(errorMessagesCount == 0);
    }

    private void removeOldMessages() {
        errorTableModel.removeMessages(input -> {
            BeanValidator<?> validator = input.getValidator();
            return validators.contains(validator);
        });
    }

    private void removeOldMessages(String messageFieldPrefix) {
        errorTableModel.removeMessages(input -> {
            BeanValidator<?> validator = input.getValidator();
            return validators.contains(validator) && input.getField().startsWith(messageFieldPrefix);
        });
    }

    public void setObjectValueAdjusting(boolean objectValueAdjusting) {
        this.objectValueAdjusting = objectValueAdjusting;
    }

}
