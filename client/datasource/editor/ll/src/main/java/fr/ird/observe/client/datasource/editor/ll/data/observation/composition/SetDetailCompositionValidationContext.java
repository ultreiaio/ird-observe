package fr.ird.observe.client.datasource.editor.ll.data.observation.composition;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.ll.observation.BasketDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import io.ultreia.java4all.validation.bean.BeanValidatorEvent;
import io.ultreia.java4all.validation.bean.BeanValidatorListener;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;

import javax.swing.JComponent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SetDetailCompositionValidationContext implements BeanValidatorListener {

    private final List<SwingValidatorMessage> messages;
    private final Map<SwingValidator<?>, JComponent> validatorEditors;
    SectionDto section;
    BasketDto basket;
    BranchlineDto branchline;
    private String contextLabel;

    public SetDetailCompositionValidationContext(Map<SwingValidator<?>, JComponent> validatorEditors) {
        this.validatorEditors = validatorEditors;
        this.messages = new ArrayList<>();
    }

    public List<SwingValidatorMessage> getMessages() {
        return messages;
    }

    public void setSection(SectionDto section) {
        this.section = section;
        this.basket = null;
        this.branchline = null;
        updateContextLabel();
    }

    public void setBasket(BasketDto basket) {
        this.basket = basket;
        this.branchline = null;
        updateContextLabel();
    }

    public void setBranchline(BranchlineDto branchline) {
        this.branchline = branchline;
        updateContextLabel();
    }

    private void updateContextLabel() {
        contextLabel = "";
        if (section != null) {
            contextLabel += "S " + section.getSettingIdentifier();
            if (basket != null) {
                contextLabel += " Ba " + basket.getSettingIdentifier();
                if (branchline != null) {
                    contextLabel += " Br " + branchline.getSettingIdentifier();
                }
            }
        }
    }

    public String getContextLabel() {
        return contextLabel;
    }

    @Override
    public void onFieldChanged(BeanValidatorEvent<?> event) {
        String[] messagesToAdd = event.getMessagesToAdd();
        if (messagesToAdd != null) {
            String field = event.getField();
            NuitonValidatorScope scope = event.getScope();
            SwingValidator<?> validator = (SwingValidator<?>) event.getSource();
            for (String messageToAdd : messagesToAdd) {
                addMessage(validator, scope, field, messageToAdd);
            }
        }
    }

    public void addMessage(SwingValidator<?> validator, NuitonValidatorScope scope, String field, String messageToAdd) {
        String propertyLabel = I18nDecoratorHelper.getPropertyI18nKey(validator.getType(), field);
        SwingValidatorMessage message = new SwingValidatorMessage(
                validator,
                contextLabel + " - " + t(propertyLabel),
                messageToAdd,
                scope,
                validatorEditors.get(validator)
        );
        messages.add(message);
    }

}
