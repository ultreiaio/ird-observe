package fr.ird.observe.client.datasource.editor.ll.data.observation.composition;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.observation.BasketDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;

import javax.swing.JComponent;
import java.util.List;
import java.util.Map;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SetDetailCompositionValidatorService {

    private final SwingValidator<SectionDto> sectionValidator;
    private final SwingValidator<BasketDto> basketValidator;
    private final SwingValidator<BranchlineDto> branchlineValidator;
    private final Map<SwingValidator<?>, JComponent> validatorEditors;

    public SetDetailCompositionValidatorService(SwingValidator<SectionDto> sectionValidator,
                                                SwingValidator<BasketDto> basketValidator,
                                                SwingValidator<BranchlineDto> branchlineValidator,
                                                Map<SwingValidator<?>, JComponent> validatorEditors) {
        this.sectionValidator = sectionValidator;
        this.basketValidator = basketValidator;
        this.branchlineValidator = branchlineValidator;
        this.validatorEditors = validatorEditors;
    }

    public SetDetailCompositionValidationContext createSetDetailCompositionValidationContext() {
        SetDetailCompositionValidationContext validationContext = new SetDetailCompositionValidationContext(validatorEditors);
        addListener(validationContext);
        return validationContext;
    }

    public void destroySetDetailCompositionValidationContext(SetDetailCompositionValidationContext validationContext) {
        removeListener(validationContext);
    }

    public List<SwingValidatorMessage> validateSections(List<? extends SectionDto> sections) {
        SetDetailCompositionValidationContext validationContext = createSetDetailCompositionValidationContext();
        try {
            for (SectionDto section : sections) {
                validateSection(validationContext, section);
            }
        } finally {
            removeListener(validationContext);
        }
        return validationContext.getMessages();
    }

    public void validateSection(SetDetailCompositionValidationContext validationContext, SectionDto section) {
        validationContext.setSection(section);
        sectionValidator.setBean(null);
        sectionValidator.setBean(section);
        if (!section.isBasketEmpty()) {
            BasketDto previousBasket = null;
            for (BasketDto basket : section.getBasket()) {
                if (previousBasket != null) {
                    // validate previousBasket.floatline2Length = basket.floatline1Length
                    Float previousBasketFloatline2Length = previousBasket.getFloatline2Length();
                    Float basketFloatline1Length = basket.getFloatline1Length();
                    if (previousBasketFloatline2Length != null
                            && basketFloatline1Length != null
                            && Math.abs(previousBasketFloatline2Length - basketFloatline1Length) > 0.01f) {
                        validationContext.addMessage(basketValidator, NuitonValidatorScope.ERROR, "floatline2Length", t("observe.data.ll.observation.SetDetailComposition.basket.invalid.nextFloatline1Length", previousBasketFloatline2Length, basketFloatline1Length));
                    }
                }
                validateBasket(validationContext, basket);
                previousBasket = basket;
            }
        }
        validationContext.setBasket(null);
        validationContext.setBranchline(null);
    }

    private void validateBasket(SetDetailCompositionValidationContext validationContext, BasketDto basket) {
        validationContext.setBasket(basket);
        basketValidator.setBean(null);
        basketValidator.setBean(basket);
        if (!basket.isBranchlineEmpty()) {
            for (BranchlineDto branchline : basket.getBranchline()) {
                validateBranchline(validationContext, branchline);
            }
        }
        validationContext.setBranchline(null);
    }

    private void validateBranchline(SetDetailCompositionValidationContext validationContext, BranchlineDto branchline) {
        validationContext.setBranchline(branchline);
        branchlineValidator.setBean(null);
        branchlineValidator.setBean(branchline);
    }

    private void addListener(SetDetailCompositionValidationContext validationContext) {
        sectionValidator.addBeanValidatorListener(validationContext);
        basketValidator.addBeanValidatorListener(validationContext);
        branchlineValidator.addBeanValidatorListener(validationContext);
    }

    private void removeListener(SetDetailCompositionValidationContext validationContext) {
        sectionValidator.removeBeanValidatorListener(validationContext);
        basketValidator.removeBeanValidatorListener(validationContext);
        branchlineValidator.removeBeanValidatorListener(validationContext);
    }

}
