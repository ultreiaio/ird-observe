package fr.ird.observe.client.datasource.editor.ll.data.observation.composition.basket;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUI;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.LonglineCompositionTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.SetDetailCompositionUIValidationHelper;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.branchline.BranchlineTableModel;
import fr.ird.observe.dto.data.ll.observation.BasketDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import fr.ird.observe.dto.data.ll.observation.SectionTemplateDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.event.TableModelListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.10
 */
public class BasketTableModel extends LonglineCompositionTableModel<BasketDto> {
    private static final Logger log = LogManager.getLogger(BasketTableModel.class);
    private SetDetailCompositionUIValidationHelper validationHelper;

    public BasketTableModel(SetDetailCompositionUIModel model) {
        super(model);
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
            case 1:
                return Integer.class;
            case 2:
            case 3:
                return Float.class;
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public Object getValueAt0(BasketDto row, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return row.getSettingIdentifier();
            case 1:
                return row.getHaulingIdentifier();
            case 2:
                return row.getFloatline1Length();
            case 3:
                return row.getFloatline2Length();
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public boolean setValueAt0(BasketDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue = getValueAt0(row, rowIndex, columnIndex);
        switch (columnIndex) {
            case 0:
                row.setSettingIdentifier((Integer) aValue);
                break;
            case 1:
                row.setHaulingIdentifier((Integer) aValue);
                break;
            case 2:
                row.setFloatline1Length((Float) aValue);
                break;
            case 3:
                row.setFloatline2Length((Float) aValue);
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    public void init(SetDetailCompositionUI ui) {
        validationHelper = ui.getModel().getStates().getValidationHelper();

        PropertyChangeListener basketsTableModelModified = evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            onBasketsTableModelModified(newValue);
        };
        TableModelListener basketsTableModelChanged = e -> {
            BasketTableModel source = (BasketTableModel) e.getSource();
            onBasketsTableModelChanged(source.getData());
        };
        PropertyChangeListener selectedBasketChanged = evt -> {
            getModel().getStates().setBasketAdjusting(true);
            try {
                BasketDto previousValue = (BasketDto) evt.getOldValue();
                BasketDto newValue = (BasketDto) evt.getNewValue();
                onSelectedBasketChanged(previousValue, newValue);
            } finally {
                getModel().getStates().setBasketAdjusting(false);
            }
        };
        addPropertyChangeListener(BasketTableModel.MODIFIED_PROPERTY, basketsTableModelModified);
        addPropertyChangeListener(BasketTableModel.SELECTED_ROW_PROPERTY, selectedBasketChanged);
        addTableModelListener(basketsTableModelChanged);
    }

    public void flushSection(SectionDto section, BranchlineTableModel branchlineTableModel) {
        if (!isSelectionEmpty()) {
            // must flush back branchlines to selected basket
            BasketDto selectedBasket = getSelectedRow();
            branchlineTableModel.flushBasket(selectedBasket);
        }

        // flush bask baskets to the given section
        List<BasketDto> baskets = getNotEmptyData();
        section.setBasket(new LinkedHashSet<>(baskets));
        log.info(getLog(String.format("Flush baskets (%d) to his section: %s", baskets.size(), section)));
    }

    public void onBasketsTableModelModified(Boolean newValue) {
        if (getModel().getStates().isResetEdit()) {
            log.info("Skip (reset action) onBasketsTableModelModified");
            return;
        }
        if (newValue) {
            getModel().getStates().setModified(true);
        }
        validationHelper.whenBasketChanged();
    }

    public void onBasketsTableModelChanged(List<BasketDto> data) {
        if (getModel().getStates().isResetEdit()) {
            log.info("Skip (reset action) onBasketsTableModelChanged");
            return;
        }
        log.info("Baskets was changed, new size: {}", data.size());
    }

    public void onSelectedBasketChanged(BasketDto previousBasket, BasketDto newBasket) {
        if (getModel().getStates().isResetEdit()) {
            log.info("Skip (reset action) onSelectedBasketChanged");
            return;
        }
        log.info("New selected basket: {}", newBasket);
        if (previousBasket != null && !getModel().getStates().isSectionAdjusting() && !getModel().getStates().isSkipSavePreviousSelectedBasket()) {
            getModel().getStates().getBranchlinesTableModel().flushBasket(previousBasket);
        }
        List<BranchlineDto> branchlines = new ArrayList<>();
        if (newBasket != null && newBasket.getBranchline() != null) {
            branchlines.addAll(newBasket.getBranchline());
        }
        getModel().getStates().getBranchlinesTableModel().setData(branchlines);
    }

    @Override
    public BasketDto createNewRow() {
        BasketDto result = BasketDto.newDto(new Date());
        result.setNotUsed(true);
        return result;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean result = super.isCellEditable(rowIndex, columnIndex);
        if (result) {
            switch (columnIndex) {
                case 0:
                    // can never edit setting id
                    result = false;
                    break;
                case 1:
                    // can edit hauling id if and only if set has hauling breaks
                    result = !isGenerateHaulingIds();
                    break;
                case 2:
                case 3:
                    // can always change length ?
                    break;
                default:
                    throw new IllegalStateException("Can't come here");
            }
        }
        return result;
    }

    public void applySectionTemplate(SectionTemplateDto newTemplate) {
        newTemplate.applyToBaskets(getData());
        fireTableRowsUpdated(0, getRowCount() - 1);
        setModified(true);
    }

    public void addBranchline(boolean before) {
        validationHelper.setObjectValueAdjusting(true);
        try {
            BranchlineTableModel tableModel = getModel().getStates().getBranchlinesTableModel();
            BranchlineDto newBranchline = tableModel.insert(before);
            BasketDto selectedBasket = getModel().getStates().getBasketsTableModel().getSelectedRow();
            if (Objects.requireNonNull(selectedBasket).isBranchlineEmpty()) {
                selectedBasket.setBranchline(new LinkedHashSet<>());
            }
            List<BranchlineDto> branchlines = new ArrayList<>(selectedBasket.getBranchline());
            branchlines.add(tableModel.getSelectedRowIndex(), newBranchline);
            selectedBasket.getBranchline().clear();
            selectedBasket.getBranchline().addAll(branchlines);
        } finally {
            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenBasketChanged();
        }
    }

    public void removeBranchline(BranchlineDto selectedBranchline) {
        validationHelper.setObjectValueAdjusting(true);
        SetDetailCompositionUIModel model = getModel();
        model.getStates().setSkipSavePreviousSelectedBranchline(true);
        try {
            BasketDto selectedRow = getSelectedRow();
            Objects.requireNonNull(selectedRow).getBranchline().remove(selectedBranchline);
            model.getStates().getBranchlinesTableModel().removeSelectedRow();
        } finally {
            model.getStates().setSkipSavePreviousSelectedBranchline(false);
            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenBranchlineChanged();
        }

    }
}
