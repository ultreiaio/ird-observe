package fr.ird.observe.client.datasource.editor.ll.data.observation.composition.branchline;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.LonglineCompositionTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.SetDetailCompositionUIValidationHelper;
import fr.ird.observe.dto.data.ll.observation.BasketDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.event.TableModelListener;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.10
 */
public class BranchlineTableModel extends LonglineCompositionTableModel<BranchlineDto> {
    private static final Logger log = LogManager.getLogger(BranchlineTableModel.class);
    private boolean useTimer;
    private SetDetailCompositionUIValidationHelper validationHelper;

    public BranchlineTableModel(SetDetailCompositionUIModel model) {
        super(model);
    }
    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
            case 1:
                return Integer.class;
            case 2:
            case 3:
                return Float.class;
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public Object getValueAt0(BranchlineDto row,int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return row.getSettingIdentifier();
            case 1:
                return row.getHaulingIdentifier();
            case 2:
                return row.getBranchlineLength();
            case 3:
                return row.getTracelineLength();
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public boolean setValueAt0(BranchlineDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue=getValueAt0(row, rowIndex, columnIndex);
        switch (columnIndex) {
            case 0:
                row.setSettingIdentifier((Integer) aValue);
                break;
            case 1:
                row.setHaulingIdentifier((Integer) aValue);
                break;
            case 2:
                row.setBranchlineLength((Float) aValue);
                break;
            case 3:
                row.setTracelineLength((Float) aValue);
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    public void flushBasket(BasketDto basket) {
        if (!isSelectionEmpty()) {
            // must flush back details to selected branchline
            flushBranchline(getSelectedRow());
        }

        // flush bask branchlines to the given basket
        List<BranchlineDto> branchlines = getNotEmptyData();
        basket.setBranchline(new LinkedHashSet<>(branchlines));
        log.info(getLog(String.format("Flush branchlines (%d) to his basket: %s", branchlines.size(), basket)));
    }

    public void flushBranchline(BranchlineDto branchline) {
        log.info(getLog(String.format("Flush branchline details: %s", branchline)));
    }

    public void init() {
        validationHelper = getModel().getStates().getValidationHelper();
        PropertyChangeListener branchlinesTableModelModified = evt -> onBranchlinesTableModelModified((Boolean) evt.getNewValue());
        TableModelListener branchlinesTableModelChanged = e -> {
            BranchlineTableModel source = (BranchlineTableModel) e.getSource();
            onBranchlinesTableModelChanged(source.getData());
        };
        PropertyChangeListener selectedBranchlineChanged = evt -> {
            BranchlineDto previousValue = (BranchlineDto) evt.getOldValue();
            BranchlineDto newValue = (BranchlineDto) evt.getNewValue();
            onSelectedBranchlineChanged(previousValue, newValue);
        };

        addPropertyChangeListener(BranchlineTableModel.MODIFIED_PROPERTY, branchlinesTableModelModified);
        addPropertyChangeListener(BranchlineTableModel.SELECTED_ROW_PROPERTY, selectedBranchlineChanged);
        addTableModelListener(branchlinesTableModelChanged);
    }


    private void onSelectedBranchlineChanged(BranchlineDto previousBranchline, BranchlineDto newBranchline) {
        if (getModel().getStates().isResetEdit()) {
            log.info("Skip (reset action) onSelectedBranchlineChanged");
            return;
        }
        log.info("New selected branchline: {}", newBranchline);
        if (previousBranchline != null && !getModel().getStates().isBasketAdjusting() && !getModel().getStates().isSkipSavePreviousSelectedBranchline()) {
            // must flush back branchline detail to his row
            flushBranchline(previousBranchline);
        }
    }

    public void onBranchlinesTableModelModified(Boolean newValue) {
        if (getModel().getStates().isResetEdit()) {
            log.info("Skip (reset action) onBranchlinesTableModelModified");
            return;
        }
        if (newValue) {
            getModel().getStates().setModified(true);
        }
        validationHelper.whenBranchlineChanged();
    }

    private void onBranchlinesTableModelChanged(List<BranchlineDto> data) {
        if (getModel().getStates().isResetEdit()) {
            log.info("Skip (reset action) onBranchlinesTableModelChanged");
            return;
        }
        log.info("Branchlines was changed, new size: {}", data.size());
    }

    @Override
    public BranchlineDto createNewRow() {
        BranchlineDto branchline = BranchlineDto.newDto(new Date());
        branchline.setTimer(isUseTimer());
        branchline.setNotUsed(true);
        return branchline;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean result = super.isCellEditable(rowIndex, columnIndex);
        if (result) {
            switch (columnIndex) {
                case 0:
                    // can never edit setting id
                    result = false;
                    break;
                case 1:
                    // can edit hauling id if and only if set has hauling breaks
                    result = !isGenerateHaulingIds();
                    break;
                case 2:
                case 3:
                    // can always change length ?
                    break;
                default:
                    throw new IllegalStateException("Can't come here");
            }
        }
        return result;
    }

    public boolean isUseTimer() {
        return useTimer;
    }

    public void setUseTimer(boolean useTimer) {
        this.useTimer = useTimer;
    }
}
