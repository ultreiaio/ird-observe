package fr.ird.observe.client.datasource.editor.ll.data.observation.composition.section;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.edit.actions.DeleteEdit;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.SetDetailCompositionUIActionSupport;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 05/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class DeleteSelectedSection extends SetDetailCompositionUIActionSupport {

    private static final Logger log = LogManager.getLogger(DeleteSelectedSection.class);

    public DeleteSelectedSection() {
        super(t("observe.data.ll.observation.SetDetailComposition.action.deleteSelectedSection"), t("observe.data.ll.observation.SetDetailComposition.action.deleteSelectedSection.tip"), "delete", null);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SetDetailCompositionUIModel model) {
        SectionTableModel tableModel = model.getStates().getSectionsTableModel();
        SectionDto selectedSection = tableModel.getSelectedRow();
        if (selectedSection != null) {
            if (!selectedSection.canDelete()) {
                // there is some references, can't delete
                displayWarning(t("observe.data.ll.observation.SetDetailComposition.section.cant.delete.title"), t("observe.data.ll.observation.SetDetailComposition.section.cant.delete.message"));
                return;
            }
            boolean accept = DeleteEdit.confirmForEntityDelete(ui, selectedSection);
            if (!accept) {
                return;
            }
            log.info(getLog(String.format("Delete section: %s", selectedSection)));
            model.getStates().removeSection(selectedSection);
        }
    }
}
