package fr.ird.observe.client.datasource.editor.ll.data.observation.composition.section;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUIModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.LonglineCompositionTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.SetDetailCompositionUIValidationHelper;
import fr.ird.observe.client.datasource.editor.ll.data.observation.composition.basket.BasketTableModel;
import fr.ird.observe.dto.data.ll.observation.BasketDto;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import fr.ird.observe.dto.data.ll.observation.SectionTemplateDto;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxCellEditor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.event.TableModelListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.10
 */
public class SectionTableModel extends LonglineCompositionTableModel<SectionDto> {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger(SectionTableModel.class);
    private FilterableComboBoxCellEditor sectionTemplateEditor;
    private SetDetailCompositionUIValidationHelper validationHelper;
    /**
     * All section templates available (may be changed when selecting another section (to just get the possible compliant one (matching basket count)
     */
    private List<SectionTemplateDto> sectionTemplates;
    /**
     * Available section templates for the selected section (all are compliant with the section basket count)
     */
    private List<SectionTemplateDto> availableSectionTemplates;

    public SectionTableModel(SetDetailCompositionUIModel model) {
        super(model);
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
            case 1:
                return Integer.class;
            case 2:
                return SectionTemplateDto.class;
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public Object getValueAt0(SectionDto row, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return row.getSettingIdentifier();
            case 1:
                return row.getHaulingIdentifier();
            case 2:
                return row.getSectionTemplate();
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public boolean setValueAt0(SectionDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue = getValueAt0(row, rowIndex, columnIndex);
        switch (columnIndex) {
            case 0:
                row.setSettingIdentifier((Integer) aValue);
                break;
            case 1:
                row.setHaulingIdentifier((Integer) aValue);
                break;
            case 2:
                SectionTemplateDto sectionTemplate = (SectionTemplateDto) aValue;
                boolean changeTemplate = true;
                if (sectionTemplate != null) {
                    // check if can use this template
                    int basketsCount = row.getBasketSize();
                    boolean compliantWithBasketCount = sectionTemplate.isCompliantWithBasketCount(basketsCount);
                    if (!compliantWithBasketCount) {
                        // We can't use this value
                        log.warn("sectionTemplate {} is not compliant with basketCount: {}", sectionTemplate, basketsCount);
                        //TODO Send user a message
                        changeTemplate = false;
                    }
                }
                if (changeTemplate) {
                    SectionTemplateDto previousSectionTemplate = row.getSectionTemplate();
                    row.setSectionTemplate(sectionTemplate);
                    firePropertyChange(SectionDto.PROPERTY_SECTION_TEMPLATE, previousSectionTemplate, sectionTemplate);
                } else {
                    aValue = null;
                }
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    @Override
    public SectionDto createNewRow() {
        SectionDto delegate = SectionDto.newDto(new Date());
        delegate.setNotUsed(true);
        return delegate;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean result = super.isCellEditable(rowIndex, columnIndex);
        if (result) {
            switch (columnIndex) {
                case 0:
                    // can never edit setting id
                    result = false;
                    break;
                case 1:
                    // can edit hauling id if and only if set has hauling breaks
                    result = !isGenerateHaulingIds();
                    break;
                case 2:
                    // can change template if in generate mode
                    result = isCanUseTemplate();
                    break;
                default:
                    throw new IllegalStateException("Can't come here");
            }
        }
        return result;
    }

    protected boolean isCanUseTemplate() {
        return getModel().getStates().isCanGenerate() && availableSectionTemplates != null && !availableSectionTemplates.isEmpty();
    }

    public void init() {
        validationHelper = getModel().getStates().getValidationHelper();
        PropertyChangeListener selectedSectionChanged = evt -> {
            getModel().getStates().setSectionAdjusting(true);
            try {
                SectionDto previousValue = (SectionDto) evt.getOldValue();
                SectionDto newValue = (SectionDto) evt.getNewValue();
                onSelectedSectionChanged(previousValue, newValue);
            } finally {
                getModel().getStates().setSectionAdjusting(false);
            }
        };
        PropertyChangeListener sectionsTableModelModified = evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            onSectionsTableModelModified(newValue);
        };
        TableModelListener sectionsTableModelChanged = e -> {
            SectionTableModel source = (SectionTableModel) e.getSource();
            onSectionsTableModelChanged(source.getData());
        };
        PropertyChangeListener selectedSectionTemplateChanged = evt -> {
            SectionTemplateDto newValue = (SectionTemplateDto) evt.getNewValue();
            onSelectedSectionTemplateChanged(newValue);
        };
        addPropertyChangeListener(MODIFIED_PROPERTY, sectionsTableModelModified);
        addPropertyChangeListener(SELECTED_ROW_PROPERTY, selectedSectionChanged);
        addPropertyChangeListener(SectionDto.PROPERTY_SECTION_TEMPLATE, selectedSectionTemplateChanged);
        addTableModelListener(sectionsTableModelChanged);
    }

    public void onSectionsTableModelModified(Boolean newValue) {
        if (getModel().getStates().isResetEdit()) {
            log.info("Skip (reset action) onSectionsTableModelModified");
            return;
        }
        if (newValue) {
            getModel().getStates().setModified(true);
        }
        validationHelper.whenSectionChanged();
    }

    public void onSectionsTableModelChanged(List<SectionDto> data) {
        if (getModel().getStates().isResetEdit()) {
            log.info("Skip (reset action) onSectionsTableModelChanged");
            return;
        }
        log.info("Sections was changed, new size: {}", data.size());
        validationHelper.whenSectionChanged();
    }

    public void onSelectedSectionTemplateChanged(SectionTemplateDto newTemplate) {
        if (newTemplate != null) {
            SectionDto selectedSection = getSelectedRow();
            log.info("Will apply template: {} to section: {}", newTemplate, selectedSection);
            validationHelper.setObjectValueAdjusting(true);
            try {
                getModel().getStates().getBasketsTableModel().applySectionTemplate(newTemplate);
            } finally {
                validationHelper.setObjectValueAdjusting(false);
                validationHelper.whenSectionChanged();
            }
        }
    }

    public void onSelectedSectionChanged(SectionDto previousSection, SectionDto newSection) {
        if (getModel().getStates().isResetEdit()) {
            log.info("Skip (reset action) new selected section: {}", newSection);
            return;
        }
        log.info("New selected section: {}", newSection);
        validationHelper.setObjectValueAdjusting(true);
        try {
            if (previousSection != null && !getModel().getStates().isOpened() && !getModel().getStates().isSkipSavePreviousSelectedSection()) {
                // must flush back baskets to the previous section
                getModel().getStates().getBasketsTableModel().flushSection(previousSection, getModel().getStates().getBranchlinesTableModel());
            }
            List<BasketDto> baskets = newSection == null ? new ArrayList<>() : new ArrayList<>(newSection.getBasket());
            getModel().getStates().getBasketsTableModel().setData(baskets);
        } finally {
            validationHelper.setObjectValueAdjusting(false);
        }
        initSectionTemplates(newSection);
    }

    public void setSectionTemplateEditor(FilterableComboBoxCellEditor sectionTemplateEditor) {
        this.sectionTemplateEditor = sectionTemplateEditor;
    }

    public void addBasket(boolean before) {
        validationHelper.setObjectValueAdjusting(true);
        try {
            BasketTableModel tableModel = getModel().getStates().getBasketsTableModel();
            BasketDto newBasket = tableModel.insert(before);
            SectionDto selectedSection = getSelectedRow();
            if (Objects.requireNonNull(selectedSection).isBasketEmpty()) {
                selectedSection.setBasket(new LinkedHashSet<>());
            }
            List<BasketDto> baskets = new ArrayList<>(selectedSection.getBasket());
            baskets.add(tableModel.getSelectedRowIndex(), newBasket);
            selectedSection.getBasket().clear();
            selectedSection.getBasket().addAll(baskets);
        } finally {
            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenBasketChanged();
        }
    }

    public void removeBasket(BasketDto selectedBasket) {
        validationHelper.setObjectValueAdjusting(true);
        getModel().getStates().setSkipSavePreviousSelectedBasket(true);
        try {
            SectionDto selectedRow = getSelectedRow();
            Objects.requireNonNull(selectedRow).getBasket().remove(selectedBasket);
            getModel().getStates().getBasketsTableModel().removeSelectedRow();
        } finally {
            getModel().getStates().setSkipSavePreviousSelectedBasket(false);
            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenBasketChanged();
        }
    }

    public void setSectionTemplates(List<SectionTemplateDto> sectionTemplates) {
        this.sectionTemplates = sectionTemplates;
        SectionDto selectedRow = getSelectedRow();
        initSectionTemplates(selectedRow);
    }

    private void initSectionTemplates(SectionDto section) {
        if (sectionTemplates != null) {
            if (section == null) {
                availableSectionTemplates = Collections.emptyList();
            } else {
                int basketSize = section.getBasketSize();
                availableSectionTemplates = sectionTemplates.stream().filter(t -> t.isCompliantWithBasketCount(basketSize)).collect(Collectors.toList());
            }
            @SuppressWarnings("unchecked") FilterableComboBox<SectionTemplateDto> comboBox = (FilterableComboBox<SectionTemplateDto>) sectionTemplateEditor.getComponent();
            comboBox.setData(availableSectionTemplates);
            //2014-FIXME See why templates are not well reselect in cell editor
            //2014-FIXME See cell editor does not loose focus and empty selection when losing focus
        }
    }
}
