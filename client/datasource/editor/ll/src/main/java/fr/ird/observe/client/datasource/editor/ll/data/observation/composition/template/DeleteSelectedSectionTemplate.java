package fr.ird.observe.client.datasource.editor.ll.data.observation.composition.template;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUI;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ll.observation.SectionTemplateDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 05/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class DeleteSelectedSectionTemplate extends ContentUIActionSupport<SetDetailCompositionUI> {

    private static final Logger log = LogManager.getLogger(DeleteSelectedSectionTemplate.class);

    public DeleteSelectedSectionTemplate() {
        super(t("observe.data.ll.observation.SetDetailComposition.action.deleteSelectedSectionTemplate"), t("observe.data.ll.observation.SetDetailComposition.action.deleteSelectedSectionTemplate.tip"), "delete", null);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SetDetailCompositionUI ui) {
        SectionTemplateTableModel tableModel = ui.getSectionTemplatesTableModel();
        boolean selectionEmpty = tableModel.isSelectionEmpty();

        if (!selectionEmpty) {
            SectionTemplateDto data = tableModel.getSelectedRow();
            log.info(getLog(String.format("Delete section template: %s", data)));
            ObserveMainUI mainUI = getMainUI();
            int response = UIHelper.askUser(mainUI,
                                            t("observe.ui.title.delete"),
                                            t("observe.data.ll.observation.SetDetailComposition.sectionTemplate.delete"),
                                            JOptionPane.WARNING_MESSAGE,
                                            new Object[]{t("observe.ui.choice.confirm.delete"),
                                                    t("observe.ui.choice.cancel")},
                                            1);
            if (response != 0) {
                // user cancel
                return;
            }
            tableModel.removeSelectedRow();
        }
    }
}
