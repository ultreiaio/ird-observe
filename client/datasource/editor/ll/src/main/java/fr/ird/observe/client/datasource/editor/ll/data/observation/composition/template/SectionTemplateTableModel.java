package fr.ird.observe.client.datasource.editor.ll.data.observation.composition.template;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.table.EditableTableModel;
import fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUI;
import fr.ird.observe.dto.data.ll.observation.SectionTemplateDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.10
 */
public class SectionTemplateTableModel extends EditableTableModel<SectionTemplateDto> {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LogManager.getLogger(SectionTemplateTableModel.class);

    public SectionTemplateTableModel() {
        super(true);
    }

    public void init(SetDetailCompositionUI ui) {
        log.debug(String.format("Init from: %s", ui));
        PropertyChangeListener sectionTemplatesTableModelModified = evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            onSectionTemplatesTableModelModified(newValue);
        };
        addPropertyChangeListener(SectionTemplateTableModel.MODIFIED_PROPERTY, sectionTemplatesTableModelModified);
    }

    public void onSectionTemplatesTableModelModified(Boolean newValue) {
        // recompute table model valid state
        log.debug(String.format("New value: %s", newValue));
        validate();
    }

    @Override
    protected SectionTemplateDto createNewRow() {
        return new SectionTemplateDto();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt0(SectionTemplateDto row, int rowIndex, int columnIndex) {
        Object result;
        switch (columnIndex) {
            case 0:
                result = row.getId();
                break;
            case 1:
                result = row.getFloatlineLengths();
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return result;
    }

    @Override
    public boolean setValueAt0(SectionTemplateDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue=getValueAt0(row, rowIndex, columnIndex);
        switch (columnIndex) {
            case 0:
                row.setId((String) aValue);
                break;
            case 1:
                row.setFloatlineLengths((String) aValue);
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    @Override
    protected boolean computeValidState() {
        boolean newValidState = super.computeValidState();
        if (newValidState) {
            // check that we are using unique ids
            Set<String> ids = new HashSet<>();
            for (SectionTemplateDto sectionTemplate : getData()) {
                boolean add = ids.add(sectionTemplate.getId());
                if (!add) {
                    log.warn(String.format("Template ids (%s) are not unique.", sectionTemplate.getId()));
                    newValidState = false;
                    break;
                }
            }
        }
        return newValidState;
    }

    @Override
    public boolean isCanCreateNewRow(int rowIndex) {
        boolean canCreateNewRow = super.isCanCreateNewRow(rowIndex);
        if (canCreateNewRow) {
            // add a new row if and only if all rows are valid
            canCreateNewRow = computeValidState();
        }
        return canCreateNewRow;
    }
}
