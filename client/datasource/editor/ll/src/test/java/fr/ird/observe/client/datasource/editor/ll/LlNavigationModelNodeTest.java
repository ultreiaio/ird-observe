package fr.ird.observe.client.datasource.editor.ll;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: LL
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.NavigationModelNodeTestSupport;
import fr.ird.observe.spi.module.ll.BusinessModule;
import org.junit.Test;

/**
 * Created on 19/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class LlNavigationModelNodeTest extends NavigationModelNodeTestSupport {

    public LlNavigationModelNodeTest() {
        super(BusinessModule.get());
    }

    @Test
    public void createRootOpenableNode() {
        assertRootOpenableNode(new fr.ird.observe.client.datasource.editor.ll.data.common.TripUINavigationNode(), fr.ird.observe.dto.data.ll.common.TripDto.class, fr.ird.observe.dto.data.ll.common.TripReference.class, fr.ird.observe.client.datasource.editor.ll.data.common.TripUI.class);
    }

    @Test
    public void createOpenableNode() {
        assertOpenableNode(new fr.ird.observe.client.datasource.editor.ll.data.observation.ActivityUINavigationNode(), fr.ird.observe.dto.data.ll.observation.ActivityDto.class, fr.ird.observe.dto.data.ll.observation.ActivityReference.class, fr.ird.observe.client.datasource.editor.ll.data.observation.ActivityUI.class);
        assertOpenableNode(new fr.ird.observe.client.datasource.editor.ll.data.landing.LandingUINavigationNode(), fr.ird.observe.dto.data.ll.landing.LandingDto.class, fr.ird.observe.dto.data.ll.landing.LandingReference.class, fr.ird.observe.client.datasource.editor.ll.data.landing.LandingUI.class);
        assertOpenableNode(new fr.ird.observe.client.datasource.editor.ll.data.logbook.ActivityUINavigationNode(), fr.ird.observe.dto.data.ll.logbook.ActivityDto.class, fr.ird.observe.dto.data.ll.logbook.ActivityReference.class, fr.ird.observe.client.datasource.editor.ll.data.logbook.ActivityUI.class);
        assertOpenableNode(new fr.ird.observe.client.datasource.editor.ll.data.logbook.SampleUINavigationNode(), fr.ird.observe.dto.data.ll.logbook.SampleDto.class, fr.ird.observe.dto.data.ll.logbook.SampleReference.class, fr.ird.observe.client.datasource.editor.ll.data.logbook.SampleUI.class);
    }

    @Test
    public void createRootOpenableListNode() {
        assertRootOpenableListNode(new fr.ird.observe.client.datasource.editor.ll.data.common.TripListUINavigationNode(), fr.ird.observe.dto.data.ll.common.TripDto.class, fr.ird.observe.dto.data.ll.common.TripReference.class, fr.ird.observe.client.datasource.editor.ll.data.common.TripListUI.class);
    }

    @Test
    public void createOpenableListNode() {
        assertOpenableListNode(new fr.ird.observe.client.datasource.editor.ll.data.observation.ActivityListUINavigationNode(), fr.ird.observe.dto.data.ll.observation.ActivityDto.class, fr.ird.observe.dto.data.ll.observation.ActivityReference.class, fr.ird.observe.client.datasource.editor.ll.data.observation.ActivityListUI.class);
        assertOpenableListNode(new fr.ird.observe.client.datasource.editor.ll.data.landing.LandingListUINavigationNode(), fr.ird.observe.dto.data.ll.landing.LandingDto.class, fr.ird.observe.dto.data.ll.landing.LandingReference.class, fr.ird.observe.client.datasource.editor.ll.data.landing.LandingListUI.class);
        assertOpenableListNode(new fr.ird.observe.client.datasource.editor.ll.data.logbook.ActivityListUINavigationNode(), fr.ird.observe.dto.data.ll.logbook.ActivityDto.class, fr.ird.observe.dto.data.ll.logbook.ActivityReference.class, fr.ird.observe.client.datasource.editor.ll.data.logbook.ActivityListUI.class);
        assertOpenableListNode(new fr.ird.observe.client.datasource.editor.ll.data.logbook.SampleListUINavigationNode(), fr.ird.observe.dto.data.ll.logbook.SampleDto.class, fr.ird.observe.dto.data.ll.logbook.SampleReference.class, fr.ird.observe.client.datasource.editor.ll.data.logbook.SampleListUI.class);
    }

    @Test
    public void createEditableNode() {
        assertEditNode(new fr.ird.observe.client.datasource.editor.ll.data.observation.SetUINavigationNode(), fr.ird.observe.dto.data.ll.observation.SetDto.class, fr.ird.observe.dto.data.ll.observation.SetReference.class, fr.ird.observe.client.datasource.editor.ll.data.observation.SetUI.class);
        assertEditNode(new fr.ird.observe.client.datasource.editor.ll.data.logbook.ActivitySampleUINavigationNode(), fr.ird.observe.dto.data.ll.logbook.ActivitySampleDto.class, fr.ird.observe.dto.data.ll.logbook.ActivitySampleReference.class, fr.ird.observe.client.datasource.editor.ll.data.logbook.ActivitySampleUI.class);
        assertEditNode(new fr.ird.observe.client.datasource.editor.ll.data.logbook.SetUINavigationNode(), fr.ird.observe.dto.data.ll.logbook.SetDto.class, fr.ird.observe.dto.data.ll.logbook.SetReference.class, fr.ird.observe.client.datasource.editor.ll.data.logbook.SetUI.class);
    }

    @Test
    public void createSimpleNode() {
        assertSimpleNode(new fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUINavigationNode(), fr.ird.observe.dto.data.ll.observation.SetDetailCompositionDto.class, fr.ird.observe.client.datasource.editor.ll.data.observation.SetDetailCompositionUI.class);
        assertSimpleNode(new fr.ird.observe.client.datasource.editor.ll.data.observation.SetGlobalCompositionUINavigationNode(), fr.ird.observe.dto.data.ll.observation.SetGlobalCompositionDto.class, fr.ird.observe.client.datasource.editor.ll.data.observation.SetGlobalCompositionUI.class);
        assertSimpleNode(new fr.ird.observe.client.datasource.editor.ll.data.logbook.SetGlobalCompositionUINavigationNode(), fr.ird.observe.dto.data.ll.logbook.SetGlobalCompositionDto.class, fr.ird.observe.client.datasource.editor.ll.data.logbook.SetGlobalCompositionUI.class);
    }

    @Test
    public void createTableNode() {
        assertTableNode(new fr.ird.observe.client.datasource.editor.ll.data.common.TripGearUseFeaturesUINavigationNode(), fr.ird.observe.dto.data.ll.common.GearUseFeaturesDto.class, fr.ird.observe.client.datasource.editor.ll.data.common.TripGearUseFeaturesUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ll.data.observation.ActivityEncounterUINavigationNode(), fr.ird.observe.dto.data.ll.observation.EncounterDto.class, fr.ird.observe.client.datasource.editor.ll.data.observation.ActivityEncounterUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ll.data.observation.ActivitySensorUsedUINavigationNode(), fr.ird.observe.dto.data.ll.observation.SensorUsedDto.class, fr.ird.observe.client.datasource.editor.ll.data.observation.ActivitySensorUsedUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ll.data.observation.SetTdrUINavigationNode(), fr.ird.observe.dto.data.ll.observation.TdrDto.class, fr.ird.observe.client.datasource.editor.ll.data.observation.SetTdrUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ll.data.observation.SetCatchUINavigationNode(), fr.ird.observe.dto.data.ll.observation.CatchDto.class, fr.ird.observe.client.datasource.editor.ll.data.observation.SetCatchUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ll.data.logbook.SetCatchUINavigationNode(), fr.ird.observe.dto.data.ll.logbook.CatchDto.class, fr.ird.observe.client.datasource.editor.ll.data.logbook.SetCatchUI.class);
    }

}
