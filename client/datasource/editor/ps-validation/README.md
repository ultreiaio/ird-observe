# Abstract

This module exposes the data source editor implementation for the **Seine** business module.