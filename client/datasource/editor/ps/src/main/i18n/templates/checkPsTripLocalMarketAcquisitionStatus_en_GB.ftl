<#--
 #%L
 ObServe Client :: DataSource :: Editor :: PS
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<p>
  To acquire <i>${dtoLabel}</i> data, the trip form must first be completed:
</p>
<ul>
  <li>The <i>landing harbour</i> must be filled in</li>
  <li>This harbour must have packaging reference data associated with it</li>
  <li>The tab named <i>Logbook and associated data</i> must be properly filled in</li>
  <li>The field named <i>Local market collection</i> must be filled in with a value that indicates existence of data</li>
</ul>
<hr/>
<br/>
<p>
  Click on <b>Access to trip form</b> to fill in properly the form and save.
</p>
<br/>
<p>
  If <i>landing harbour</i> is not filled in,
<ul>
  <li>Its widget will be focused in the proper tab, to enable you to fill in it</li>
  <li>Once the landing harbour is provided, you need to fill in the tab named <i>Local market collection</i> in the proper tab</li>
</ul>
</p>
<p>
  <b>If <i>landing harbour</i> is filled in, but doesn't have packaging reference data associated associated with it, you will not be able to acquire local market data.</b>
</p>
<p>In other cases, the field named <i>Local market collection</i> is focused in the proper tab.
</p>
<br/>
<p>
  You can then acquire <i>${dtoLabel}</i> data.
</p>
</body>
</html>
