<#--
 #%L
 ObServe Client :: DataSource :: Editor :: PS
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<p>
  Pour pouvoir créer une donnée de type <i>${dtoLabel}</i>, le formulaire marée doit d'abord être complété :
</p>
<ul>
  <li>Le port d'arrivée doit être renseigné</li>
  <li>Ce port doit disposer d'un référentiel de conditionnements</li>
  <li>L'onglet <i>Livre de bord et données associées</i> doit être renseigné</li>
  <li>Le champ <i>Collecte du marché local</i> doit être renseigné avec une valeur attestant de l'existence de données</li>
</ul>
<hr/>
<br/>
<p>
  Cliquez sur <b>Accéder au formulaire marée</b> pour compléter ces données et enregistrer.
<p>
  <br/>
<p>
  Si le port d'arrivée n'est pas renseigné :
<ul>
  <li>Vous serez redirigé vers l'onglet « Caractéristiques générales », et le champs <i>Port d'arrivée</i> sera sélectionné pour vous permettre de le saisir.</li>
  <li>Une fois celui-ci renseigné, il vous faudra aller renseigner le champs <i>Collecte du marché local</i> sur l'onglet <i>Livre de bord et données associées</i></li>
</ul>
</p>
<p>
  <b>Si le port d'arrivée est renseigné mais que celui-ci ne dispose pas d'un référentiel de conditionnements, il ne sera pas possible de saisir des données de marché local.</b>
</p>
<p>Si le port d'arrivée est renseigné et dispose d'un référentiel de conditionnements, vous serez directement redirigé vers l'onglet <i>Livre de bord et données associées</i>, et le champ <i>Collecte du marché local</i> sera sélectionné pour vous permettre de le saisir.
</p>
<br/>
<p>Vous pourrez ensuite saisir les données du <i>${dtoLabel}</i>.</p>
</body>
</html>
