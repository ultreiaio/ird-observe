<#--
 #%L
 ObServe Client :: DataSource :: Editor :: PS
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<h1>#TODO</h1>
<p>
  To be able to acquire data of type <i>${dtoLabel}</i>, the trip form must first be completed:
</p>
<ul>
  <li>The <i>Local market wells sampling</i> must be filled with a value that indicate presence of data</li>
  <li>The <i>Logbook and associated data</i> tab must be properly filled</li>
</ul>
<hr/>
<br/>
<p>
  Click on <b>Access to trip form</b> to fill properly the form and save.
</p>
<br/>
<p>The <i>Local market wells sampling</i> widget will be focused in the proper tab.
</p>
<br/>
<p>
  You can then enter the data.
</p>
</body>
</html>
