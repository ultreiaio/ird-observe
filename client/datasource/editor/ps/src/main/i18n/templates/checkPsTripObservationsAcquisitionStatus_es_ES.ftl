<#--
 #%L
 ObServe Client :: DataSource :: Editor :: PS
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<h1>#TODO</h1>
<p>
  Para poder acceder al observaciones (<i>${dtoLabel}</i>), primero se debe completar el formulario de marea:
</p>
<ul>
  <li>La casilla <i>observaciones</i> debe estar marcada</li>
  <li>Se debe seleccionar la identidad de la persona que registra los datos en el <i>observaciones</i> (observador/a u
    otro/a)
  </li>
</ul>
<hr/>
<br/>
<p>
  Haga click en <b>Acceder al formulario de marea</b> para añadir la identidad de la persona que registra los datos y
  guardar.
</p>
<br/>
<p>
  La casilla <i>observaciones</i> se marcará automáticamente.
</p>
<br/>
<p>
  Podrá entonces introducir
  datos.
</p>
</body>
</html>
