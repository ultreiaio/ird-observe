<#--
 #%L
 ObServe Client :: DataSource :: Editor :: PS
 %%
 Copyright (C) 2008 - 2025 IRD, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<p>
  Pour pouvoir créer une donnée de type <i>${dtoLabel}</i>, le formulaire marée doit d'abord être complété :
</p>
<ul>
  <li>La valeur du champs <i>Échantillonnage des cuves d'espèces cibles</i> doit être renseignée avec une valeur reflétant l'existence de données</li>
  <li>L'onglet <i>Livre de bord et données associées</i> doit être renseigné</li>
</ul>
<hr/>
<br/>
<p>
  Cliquer sur <b>Accéder au formulaire marée</b> pour compléter ces données et enregistrer.
<p>
<br/>
<p> Vous serez redirigé vers l'onglet à renseigner et le champs <i>Échantillonnage des cuves d'espèces cibles</i> sera sélectionné.
</p>
<br/>
<p> Vous pourrez ensuite saisir la donnée souhaitée.</p>
</body>
</html>
