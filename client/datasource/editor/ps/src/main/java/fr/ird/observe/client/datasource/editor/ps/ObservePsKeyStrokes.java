package fr.ird.observe.client.datasource.editor.ps;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;

import javax.swing.KeyStroke;

/**
 * All keystrokes registered for Pure seine content.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
public class ObservePsKeyStrokes extends ObserveKeyStrokesEditorApi {

    public static final KeyStroke KEY_STROKE_COPY_FLOATING_OBJECT_PART_TO_RIGHT = KeyStroke.getKeyStroke("ctrl pressed R");
    public static final KeyStroke KEY_STROKE_COPY_FLOATING_OBJECT_PART_TO_LEFT = KeyStroke.getKeyStroke("ctrl pressed L");
    public static final KeyStroke KEY_STROKE_CREATE_ACTIVITY = KeyStroke.getKeyStroke("ctrl pressed U");
    public static final KeyStroke KEY_STROKE_COPY_COORDINATE = KeyStroke.getKeyStroke("ctrl pressed K");
    public static final KeyStroke KEY_STROKE_RESET_COMPUTED_VALUE = KeyStroke.getKeyStroke("ctrl pressed U");
}
