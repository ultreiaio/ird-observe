package fr.ird.observe.client.datasource.editor.ps;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.ps.data.observation.ActivityListUINavigationNode;
import fr.ird.observe.client.datasource.editor.ps.data.observation.ActivityUI;
import fr.ird.observe.client.datasource.editor.ps.data.observation.RouteUINavigationNode;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.navigation.id.CloseNodeCallback;
import fr.ird.observe.navigation.id.CloseNodeVetoException;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.id.ps.observation.RouteNode;
import io.ultreia.java4all.util.Dates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JOptionPane;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 28/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(CloseNodeCallback.class)
public class RouteCloseCallback implements CloseNodeCallback, WithClientUIContextApi {
    private static final Logger log = LogManager.getLogger(RouteCloseCallback.class);

    @Override
    public Class<? extends IdNode<?>> getNodeType() {
        return RouteNode.class;
    }

    @Override
    public int getPriority() {
        return 10;
    }

    @Override
    public void onIdNodeClosed(IdNode<?> node, boolean adjusting) throws CloseNodeVetoException {

        ObserveSwingDataSource mainDataSource = getDataSourcesManager().getMainDataSource();
        if (mainDataSource == null) {
            return;
        }

        boolean activityEndOfSearchFound = mainDataSource.getPsCommonTripService().isActivityEndOfSearchFound(Objects.requireNonNull(node.getId()));

        // on doit vérifier qu'il existe une activité de fin de veille (type activity vessel == 16)

        boolean mustAddFinVeille = !activityEndOfSearchFound;

        boolean createActivityEndOfSearching = false;

        if (mustAddFinVeille) {

            // on indique à l'utilisateur qu'il doit créer une activité de type 16

            int reponse = askToUser(
                    t("observe.ui.title.need.confirm"),
                    t("observe.data.ps.observation.Route.message.need.fin.veille.activity"),
                    JOptionPane.WARNING_MESSAGE,
                    new Object[]{
                            t("observe.ui.choice.cancel"),
                            t("observe.data.ps.observation.Route.choice.create.fin.veille.activity"),
                            t("observe.data.ps.observation.Route.choice.not.create.fin.veille.activity.and.continue"),
                    },
                    2);
            log.debug("response : " + reponse);

            switch (reponse) {
                case JOptionPane.CLOSED_OPTION:
                case 0:

                    // abandon operation
                    throw new CloseNodeVetoException(t(""), node);
                case 2:

                    // rien a faire
                    // on veut juste cloturer la route
                    break;
                case 1:

                    // creation de l'activity de fin de veille
                    // fermeture de l'activity de fin de veille
                    // fermeture de la route
                    createActivityEndOfSearching = true;
                    break;
            }
        }

        if (createActivityEndOfSearching) {

            // creation de l'action de fin de veille
            addActivityEndOfSearching(node);

            // on selection l'activity de fin de veille et on y reste
            // donc on ne continue pas la fermeture de la route
            throw new CloseNodeVetoException("Fermeture de la route abandonnée (création d'une activité de fin de veille en cours)", node);
        }
    }

    private void addActivityEndOfSearching(IdNode<?> node) {

        DataSourceEditor dataSourceEditor = (DataSourceEditor) getMainUI().getMainUIBodyContentManager().getCurrentBody().get();
        NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();

        RouteUINavigationNode routeNode = (RouteUINavigationNode) tree.getModel().getNodeFromModelNode(node);

        ActivityListUINavigationNode parentNode = Objects.requireNonNull(routeNode).getActivityListUINavigationNode();
        tree.selectSafeNode(parentNode);

        log.debug("PARENT NODE = " + parentNode);

        // on crée l'activité de fin de veille
        parentNode.addEmptyActivityUINavigationNode();

        // on récupère l'écran d'édition
        ActivityUI selectedUI = (ActivityUI) dataSourceEditor.getModel().getContent();

        // on recupère l'activité de fin de veille
        VesselActivityReference vesselActivitySeine = null;

        for (VesselActivityReference refVesselActivity : selectedUI.getVesselActivity().getModel().getData()) {
            if (VesselActivityReference.isEndOfSearchingOperation(refVesselActivity)) {
                vesselActivitySeine = refVesselActivity;
                break;
            }
        }

        // on la positionne sur le bean d'édition
        selectedUI.getBean().setVesselActivity(vesselActivitySeine);

        // on initialise la fin de veille a la dernière minute du jour
        selectedUI.getBean().setTime(Dates.getEndOfDay(Dates.createDate(0, 0, 0)));
    }

}
