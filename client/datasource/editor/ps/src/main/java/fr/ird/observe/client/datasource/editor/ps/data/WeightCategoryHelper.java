package fr.ird.observe.client.datasource.editor.ps.data;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIModelStates;
import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.dto.data.ps.logbook.CatchDto;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ps.common.WeightCategoryReference;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created on 20/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class WeightCategoryHelper {
    public static final String AVAILABLE_WEIGHT_CATEGORY = "availableWeightCategory";
    private static final Logger log = LogManager.getLogger(WeightCategoryHelper.class);
    final ContentTableUIModelStates<?, ?> states;
    final PropertyChangeListener speciesChanged;

    public WeightCategoryHelper(ContentTableUIModelStates<?, ?> states) {
        this.states = states;
        speciesChanged = evt -> onSpeciesChanged((SpeciesReference) evt.getNewValue());
    }

    public void install(FilterableComboBox<WeightCategoryReference> editor) {
        states.addPropertyChangeListener(AVAILABLE_WEIGHT_CATEGORY, evt -> {
            WeightCategoryReference previousWeightCategory = states.getTableEditBean().get("weightCategory");
            @SuppressWarnings("unchecked") List<WeightCategoryReference> newValue = (List<WeightCategoryReference>) evt.getNewValue();
            WeightCategoryReference newWeightCategory;
            if (newValue == null || !newValue.contains(previousWeightCategory)) {
                newWeightCategory = null;
            } else {
                newWeightCategory = previousWeightCategory;
            }
            log.info(String.format("Will use category %s, from %d category(ies)", newWeightCategory, newValue == null ? 0 : newValue.size()));
            editor.setData(newValue);
            editor.setSelectedItem(newWeightCategory);
        });
    }

    public void listenSpeciesChanged() {
        unListenSpeciesChanged();
        states.getTableEditBean().addPropertyChangeListener(TripDto.PROPERTY_SPECIES, speciesChanged);
    }

    public void unListenSpeciesChanged() {
        states.getTableEditBean().removePropertyChangeListener(TripDto.PROPERTY_SPECIES, speciesChanged);
    }

    protected void onSpeciesChanged(SpeciesReference species) {
        if (states.isReadingMode() || !states.isEditing()) {
            return;
        }
        List<WeightCategoryReference> allCategories = states.getReferenceCache().getReferentialReferences(CatchDto.PROPERTY_WEIGHT_CATEGORY);
        Predicate<WeightCategoryReference> predicate;
        if (species == null) {
            // get all categories with no species
            predicate = r -> r.getSpecies() == null;
        } else {
            // get all categories with no species, or with this species
            predicate = r -> r.getSpecies() == null || r.getSpecies().equals(species);
        }
        List<WeightCategoryReference> newCategories = allCategories.stream().filter(predicate).collect(Collectors.toList());
        log.info(String.format("For species: %s, found %d category(ies)", species, newCategories.size()));
        states.firePropertyChanged(AVAILABLE_WEIGHT_CATEGORY, newCategories);
    }
}
