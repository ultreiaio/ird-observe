package fr.ird.observe.client.datasource.editor.ps.data.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.TripMapPointType;
import org.apache.commons.lang3.time.DateUtils;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.styling.Style;
import org.locationtech.jts.geom.Coordinate;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 04/07/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
@AutoService(fr.ird.observe.dto.data.TripMapContentBuilder.class)
public class TripMapContentBuilder extends fr.ird.observe.client.datasource.editor.api.content.data.map.TripMapContentBuilderSupport {

    public static final String LINES_LAYER_NAME = "Lines";

    public TripMapContentBuilder() {
        super(new LinkedHashSet<>(Arrays.asList(
                TripMapPointType.psActivityObs,
                TripMapPointType.psActivityObsInHarbour,
                TripMapPointType.psActivityObsWithFreeSchoolType,
                TripMapPointType.psActivityObsWithObjectSchoolType,
                TripMapPointType.psActivityLogbook,
                TripMapPointType.psActivityLogbookWithSampling,
                TripMapPointType.psActivityLogbookInHarbour,
                TripMapPointType.psActivityLogbookWithFreeSchoolType,
                TripMapPointType.psActivityLogbookWithFreeSchoolTypeWithSampling,
                TripMapPointType.psActivityLogbookWithObjectSchoolType,
                TripMapPointType.psActivityLogbookWithObjectSchoolTypeWithSampling,
                TripMapPointType.psTransmittingBuoyLostLogbook
        )), createLineBuilder(true));
    }

    @Override
    public Set<String> getVariableNames() {
        Set<String> result = new LinkedHashSet<>(DEFAULT_VARIABLE_NAMES);
        result.add("mapPsStyleObservationsTextColor");
        result.add("mapPsStyleObservationsTextSize");
        result.add("mapPsStyleObservationsLineColor");
        result.add("mapPsStyleObservationsPointColor");
        result.add("mapPsStyleLogbookTextColor");
        result.add("mapPsStyleLogbookTextSize");
        result.add("mapPsStyleLogbookLineColor");
        result.add("mapPsStyleLogbookPointColor");
        result.add("mapPsStyleLogbookWithSamplingPointColor");
        result.add("mapPsStyleLogbookTransmittingBuoyLostPointColor");
        return result;
    }

    @Override
    public String getStyleFileName() {
        return "ps-style.xml";
    }

    @Override
    public boolean accept(TripMapDto tripMapDto) {
        return TripAware.isSeineId(tripMapDto.getTripId());
    }

    @Override
    public void addPoints(TripMapConfigDto tripMapConfig, List<TripMapPoint> tripMapPoints, Set<String> excludedFeatureNames) {
        if (tripMapConfig.isAddObservations()) {
            addPoints(tripMapPoints.stream().filter(f -> f.getType().isTrip() || f.getType().isObs()).collect(Collectors.toList()), OBSERVATION_POINTS_LAYER_NAME, n("observe.ui.datasource.editor.content.map.observation.points.not.valid"), excludedFeatureNames);
        }
        if (tripMapConfig.isAddLogbook()) {
            addPoints(tripMapPoints.stream().filter(f -> f.getType().isTrip() || f.getType().isLogbook()).collect(Collectors.toList()), LOGBOOK_POINTS_LAYER_NAME, n("observe.ui.datasource.editor.content.map.logbook.points.not.valid"), excludedFeatureNames);
        }
    }

    @Override
    public void addLines(TripMapConfigDto tripMapConfig, List<TripMapPoint> tripMapPoints, Set<String> excludedFeatureNames) {
        if (tripMapConfig.isAddObservations() && tripMapConfig.isAddObservationsTripSegment()) {
            addLines0(tripMapPoints.stream().filter(f -> f.getType().isTrip() || f.getType().isObs()).collect(Collectors.toList()),
                      "observationDay",
                      "observationNight",
                      true,
                      t("observe.ui.datasource.editor.content.map.legend.obs.tripDay"),
                      t("observe.ui.datasource.editor.content.map.legend.obs.tripBetweenTwoDays"), excludedFeatureNames);
        }
        if (tripMapConfig.isAddLogbook() && tripMapConfig.isAddLogbookTripSegment()) {
            addLines0(tripMapPoints.stream().filter(f -> f.getType().isTrip() || f.getType().isLogbook()).collect(Collectors.toList()),
                      "logbookSegment",
                      "logbookSegment",
                      false,
                      t("observe.ui.datasource.editor.content.map.legend.logbook.tripSegment"),
                      null, excludedFeatureNames);
        }
    }

    protected void addTripDay(DefaultFeatureCollection linesFeatures, String label, List<Coordinate> coordinatesByDay, TripMapPoint previousPoint, Set<String> excludedFeatureNames) {
        if (coordinatesByDay.size() > 1) {
            Coordinate[] coordinates = coordinatesByDay.toArray(new Coordinate[0]);
            addLine(linesFeatures, label, coordinates, previousPoint.getTime(), excludedFeatureNames);
        }
    }

    protected void addTripBetweenTwoDay(DefaultFeatureCollection linesFeatures, String label, TripMapPoint previousPoint, TripMapPoint point, boolean addTime, Set<String> excludedFeatureNames) {
        Coordinate[] coordinates = create(previousPoint, point);
        addLine(linesFeatures, label, coordinates, addTime ? previousPoint.getTime() : null, excludedFeatureNames);
    }

    protected void addLines0(List<TripMapPoint> tripMapPoints, String pointDay, String pointNight, boolean addDate, String pointDayLabel, String pointNightLabel, Set<String> excludedFeatureNames) {
        if (tripMapPoints.size() == 2 && tripMapPoints.stream().allMatch(t -> t.getType().isTrip())) {
            // Got only trip points, nothing to display then
            return;
        }

        DefaultFeatureCollection linesFeatures = new DefaultFeatureCollection();
        List<Coordinate> coordinatesByDay = new LinkedList<>();
        TripMapPoint previousPoint = null;
        for (TripMapPoint point : tripMapPoints) {
            if (!point.isValid() || !point.getType().isActivity()) {
                continue;
            }
            Coordinate coordinate = create(point);
            if (previousPoint != null && !DateUtils.isSameDay(previousPoint.getTime(), point.getTime())) {
                // changing day
                addTripDay(linesFeatures, pointDay, coordinatesByDay, previousPoint, excludedFeatureNames);
                addTripBetweenTwoDay(linesFeatures, pointNight, previousPoint, point, addDate, excludedFeatureNames);
                coordinatesByDay.clear();
            }
            coordinatesByDay.add(coordinate);
            previousPoint = point;
        }
        if (previousPoint != null) {
            addTripDay(linesFeatures, pointDay, coordinatesByDay, previousPoint, excludedFeatureNames);
        }
        Style styleLines = findStyle(styledLayerDescriptor, LINES_LAYER_NAME);
        if (!linesFeatures.isEmpty()) {
            Layer layerLines = new FeatureLayer(linesFeatures, styleLines, LINES_LAYER_NAME);
            mapContent.addLayer(layerLines);
        }
        addLineLegend(styleLines, pointDay, pointDayLabel, excludedFeatureNames);
        if (pointNightLabel != null) {
            addLineLegend(styleLines, pointNight, pointNightLabel, excludedFeatureNames);
        }
    }

}
