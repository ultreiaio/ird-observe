package fr.ird.observe.client.datasource.editor.ps.data.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.data.ps.common.TripDto;


/**
 * Created on 7/19/24.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
class TripUIHandler extends GeneratedTripUIHandler {

    @Override
    public void startEditUI() {
        TripDto bean = getModel().getStates().getBean();
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2729#note_1472547867
        if (bean.isPersisted() && bean.getActivitiesAcquisitionModeByTimeEnabled() == null) {
            // compute it
            boolean activitiesAcquisitionModeByTimeEnabled = getPsCommonTripService().isActivitiesAcquisitionModeByTimeEnabled(bean.getId());
            // set it to bean (will be used then by validation on field activitiesAcquisitionMode)
            bean.setActivitiesAcquisitionModeByTimeEnabled(activitiesAcquisitionModeByTimeEnabled);
        }
        super.startEditUI();
    }
}
