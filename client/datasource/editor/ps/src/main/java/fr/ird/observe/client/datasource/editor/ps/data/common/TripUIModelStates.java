package fr.ird.observe.client.datasource.editor.ps.data.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilter;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.common.DataQualityReference;
import fr.ird.observe.dto.referential.common.HarbourReference;
import fr.ird.observe.dto.referential.common.PersonReference;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.dto.referential.ps.common.AcquisitionStatusReference;
import fr.ird.observe.dto.referential.ps.common.ProgramReference;
import fr.ird.observe.dto.referential.ps.localmarket.PackagingReference;
import fr.ird.observe.dto.referential.ps.logbook.WellContentStatusReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.Dates;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;

import javax.swing.Icon;
import java.beans.PropertyChangeEvent;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@GenerateJavaBeanDefinition
public class TripUIModelStates extends GeneratedTripUIModelStates {
    public static final String PROPERTY_LOCALMARKET_POSSIBLE = "localmarketPossible";
    private final AcquisitionStatusReference defaultStatus;
    private final DataQualityReference defaultLogbookDataQuality;
    private final WellContentStatusReference defaultDepartureWellContentStatus;
    private final WellContentStatusReference defaultLandingWellContentStatus;
    private final Set<HarbourReference> harbourWithPackaging;
    private boolean localmarketPossible;

    public TripUIModelStates(GeneratedTripUIModel model) {
        super(model);
        TripDto bean = getBean();
        ObserveSwingDataSource mainDataSource = getReferenceCache().getDataSource();
        defaultLogbookDataQuality = mainDataSource.getReferentialReferenceSet(DataQualityReference.class).tryGetReferenceById(ProtectedIdsPs.PS_LOGBOOK_TRIP_DEFAULT_DATA_QUALITY_ID).orElseThrow(IllegalStateException::new);
        defaultStatus = mainDataSource.getReferentialReferenceSet(AcquisitionStatusReference.class).tryGetReferenceById(ProtectedIdsPs.PS_COMMON_DEFAULT_ACQUISITION_STATUS_ID).orElseThrow(IllegalStateException::new);
        ReferentialDtoReferenceSet<PackagingReference> packagingSet = mainDataSource.getReferentialReferenceSet(PackagingReference.class);
        harbourWithPackaging = packagingSet.stream().map(PackagingReference::getHarbour).collect(Collectors.toSet());
        ReferentialDtoReferenceSet<WellContentStatusReference> wellContentStatusReferenceSet = mainDataSource.getReferentialReferenceSet(WellContentStatusReference.class);
        defaultDepartureWellContentStatus = wellContentStatusReferenceSet.tryGetReferenceById(ProtectedIdsPs.PS_LOGBOOK_TRIP_DEFAULT_DEPARTURE_WELL_CONTENT_STATUS_ID).orElseThrow(IllegalStateException::new);
        defaultLandingWellContentStatus = wellContentStatusReferenceSet.tryGetReferenceById(ProtectedIdsPs.PS_LOGBOOK_TRIP_DEFAULT_LANDING_WELL_CONTENT_STATUS_ID).orElseThrow(IllegalStateException::new);
        bean.addPropertyChangeListener(TripDto.PROPERTY_LOGBOOK_ENABLED, this::onLogbooksEnabledChanged);
        bean.addPropertyChangeListener(TripDto.PROPERTY_LOGBOOK_COMMON_ENABLED, this::onLogbooksCommonEnabledChanged);
        bean.addPropertyChangeListener(TripDto.PROPERTY_OBSERVATIONS_ENABLED, this::onObservationsEnabledChanged);
        bean.addPropertyChangeListener(TripDto.PROPERTY_LOCALMARKET_ENABLED, this::onLocalmarketEnabledChanged);
        bean.addPropertyChangeListener(TripDto.PROPERTY_LANDING_ENABLED, this::onLandingEnabledChanged);
        bean.addPropertyChangeListener(TripDto.PROPERTY_LANDING_HARBOUR, this::onLandingHarbourChanged);
    }

    public static void fillReferenceCache(ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(TripDto.PROPERTY_CAPTAIN, ReferencesFilterHelper.newPredicateList(PersonReference::isCaptain));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_OBSERVER, ReferencesFilterHelper.newPredicateList(PersonReference::isObserver));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_OBSERVATIONS_DATA_ENTRY_OPERATOR, ReferencesFilterHelper.newPredicateList(PersonReference::isDataEntryOperator));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_LOGBOOK_DATA_ENTRY_OPERATOR, ReferencesFilterHelper.newPredicateList(PersonReference::isDataEntryOperator));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_VESSEL, ReferencesFilterHelper.<VesselReference>newPredicateList(v->v.getVesselType().isSeine()));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_OBSERVATIONS_ACQUISITION_STATUS, newAcquisitionStatusList(TripDto::isObservationsFilled, AcquisitionStatusReference::isObservation));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_LOGBOOK_ACQUISITION_STATUS, newAcquisitionStatusList(TripDto::isLogbookFilled, AcquisitionStatusReference::isLogbook));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_LANDING_ACQUISITION_STATUS, newAcquisitionStatusList(TripDto::isLandingFilled, AcquisitionStatusReference::isLanding));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_TARGET_WELLS_SAMPLING_ACQUISITION_STATUS, newAcquisitionStatusList(TripDto::isTargetWellsSamplingFilled, AcquisitionStatusReference::isTargetWellsSampling));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_LOCAL_MARKET_ACQUISITION_STATUS, newAcquisitionStatusList(TripDto::isLocalmarketFilled, AcquisitionStatusReference::isLocalMarket));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_LOCAL_MARKET_WELLS_SAMPLING_ACQUISITION_STATUS, newAcquisitionStatusList(TripDto::isLocalmarketWellsSamplingFilled, AcquisitionStatusReference::isLocalMarketWellsSampling));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_LOCAL_MARKET_SURVEY_SAMPLING_ACQUISITION_STATUS, newAcquisitionStatusList(TripDto::isLocalmarketSurveySamplingFilled, AcquisitionStatusReference::isLocalMarketSurveySampling));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_ADVANCED_SAMPLING_ACQUISITION_STATUS, newAcquisitionStatusList(TripDto::isAdvancedSamplingFilled, AcquisitionStatusReference::isAdvancedSampling));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_OBSERVATIONS_PROGRAM, ReferencesFilterHelper.newPredicateList(ProgramReference::isObservation));
        referenceCache.addReferentialFilter(TripDto.PROPERTY_LOGBOOK_PROGRAM, ReferencesFilterHelper.newPredicateList(ProgramReference::isLogbook));
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        fillReferenceCache( referenceCache);
    }

    @Override
    protected void copyFormToBean(Form<TripDto> form) {
        super.copyFormToBean(form);
        // set end date to current day if necessary
        if (isUpdatingMode() && getBean().getEndDate() == null) {
            Date date = Dates.getEndOfDay(new Date());
            getBean().setEndDate(date);
        }
    }

    @Override
    public TripDto getBeanToSave() {
        TripDto bean = super.getBeanToSave();
        //FIXME generate this in dot (tagValue day)
        // on force toujours la date a être sans heure, minute,...
        Date startDate = Dates.getDay(bean.getStartDate());
        bean.setStartDate(startDate);
        return bean;
    }

    public boolean isLocalmarketPossible() {
        return localmarketPossible;
    }

    public void setLocalmarketPossible(boolean localmarketPossible) {
        boolean oldValue = this.localmarketPossible;
        this.localmarketPossible = localmarketPossible;
        firePropertyChange(PROPERTY_LOCALMARKET_POSSIBLE, oldValue, localmarketPossible);
    }

    public Icon getLandingHarbourMessageIcon(boolean localmarketPossible) {
        if (localmarketPossible) {
            return SwingValidatorUtil.getInfoIcon();
        }
        return SwingValidatorUtil.getErrorIcon();
    }

    public String getLandingHarbourMessage(boolean localmarketPossible) {
        if (localmarketPossible) {
            return I18n.t("observe.data.ps.common.Trip.landingHarbour.localMarketPossible");
        }
        return I18n.t("observe.data.ps.common.Trip.landingHarbour.localMarketNotPossible");
    }

    public void setDefaultObservationValues(boolean reset) {
        TripDto bean = getBean();
        if (reset) {
            bean.setObservationsComment(null);
            bean.setObservationsDataEntryOperator(null);
            bean.setObservationsDataQuality(null);
            bean.setObservationsProgram(null);
            bean.setObserver(null);
        } else {
            //FIXME Something to set by default ?
        }
    }

    public void setDefaultLogbookValues(boolean reset) {
        TripDto bean = getBean();
        if (reset) {
            bean.setLoch(null);
            bean.setTimeAtSea(null);
            bean.setFishingTime(null);
            bean.setDepartureWellContentStatus(null);
            bean.setLandingWellContentStatus(null);
            bean.setLogbookProgram(null);
        } else {
            if (bean.getDepartureWellContentStatus() == null) {
                bean.setDepartureWellContentStatus(defaultDepartureWellContentStatus);
            }
            if (bean.getLandingWellContentStatus() == null) {
                bean.setLandingWellContentStatus(defaultLandingWellContentStatus);
            }
        }
    }

    public void setDefaultLocalmarketValues(boolean reset) {
        TripDto bean = getBean();
        if (reset) {
            bean.setLocalMarketTotalWeight(null);
            bean.setLocalMarketSurveySamplingAcquisitionStatus(defaultStatus);
            bean.setLocalMarketWellsSamplingAcquisitionStatus(defaultStatus);
        }
    }

    public void setDefaultLandingValues(boolean reset) {
        TripDto bean = getBean();
        if (reset) {
            bean.setLandingTotalWeight(null);
        }
    }

    public void setDefaultLogbookCommonValues(boolean reset) {
        TripDto bean = getBean();
        if (reset) {
            bean.setLogbookComment(null);
            bean.setLogbookDataEntryOperator(null);
            bean.setLogbookDataQuality(null);
        } else {
            if (bean.getLogbookDataQuality() == null) {
                bean.setLogbookDataQuality(defaultLogbookDataQuality);
            }
        }
    }

    private static ReferencesFilter<AcquisitionStatusReference> newAcquisitionStatusList(Predicate<TripDto> beanPredicate, Predicate<AcquisitionStatusReference> referencePredicate) {
        return (bean, incomingReferences) -> {
            boolean filterFiledEnabler = bean != null && beanPredicate.test((TripDto) bean);
            Stream<AcquisitionStatusReference> stream = incomingReferences.stream().filter(referencePredicate);
            if (filterFiledEnabler) {
                stream = stream.filter(AcquisitionStatusReference::isFieldEnabler);
            }
            return stream.collect(Collectors.toList());
        };
    }

    private void onLandingEnabledChanged(PropertyChangeEvent event) {
        if (!isOpened() || isReadingMode()) {
            return;
        }
        boolean reset = Objects.equals(Boolean.FALSE, event.getNewValue());
        setDefaultLandingValues(reset);
    }

    private void onLocalmarketEnabledChanged(PropertyChangeEvent event) {
        if (!isOpened() || isReadingMode()) {
            return;
        }
        boolean reset = Objects.equals(Boolean.FALSE, event.getNewValue());
        setDefaultLocalmarketValues(reset);
    }

    private void onLogbooksCommonEnabledChanged(PropertyChangeEvent event) {
        if (!isOpened() || isReadingMode()) {
            return;
        }
        boolean reset = Objects.equals(Boolean.FALSE, event.getNewValue());
        setDefaultLogbookCommonValues(reset);
    }

    private void onLandingHarbourChanged(PropertyChangeEvent event) {
        HarbourReference newValue = (HarbourReference) event.getNewValue();
        boolean localMarketPossible = newValue != null && harbourWithPackaging.contains(newValue);
        setLocalmarketPossible(localMarketPossible);
    }

    private void onLogbooksEnabledChanged(PropertyChangeEvent event) {
        if (!isOpened() || isReadingMode()) {
            return;
        }
        boolean reset = Objects.equals(Boolean.FALSE, event.getNewValue());
        setDefaultLogbookValues(reset);
    }

    private void onObservationsEnabledChanged(PropertyChangeEvent event) {
        if (!isOpened() || isReadingMode()) {
            return;
        }
        boolean reset = Objects.equals(Boolean.FALSE, event.getNewValue());
        setDefaultObservationValues(reset);
    }
}
