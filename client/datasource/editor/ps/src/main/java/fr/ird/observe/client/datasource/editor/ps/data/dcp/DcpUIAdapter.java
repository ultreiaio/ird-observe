package fr.ird.observe.client.datasource.editor.ps.data.dcp;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUIHandler;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.actions.AddFloatingObjectPreset;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.actions.CopyFloatingObjectPartToLeft;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.actions.CopyFloatingObjectPartToRight;
import fr.ird.observe.client.datasource.validation.ObserveSwingValidator;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ps.FloatingObjectAware;
import fr.ird.observe.dto.data.ps.TransmittingBuoyAware;
import fr.ird.observe.dto.data.ps.TypeTransmittingBuoyOperation;
import fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto;
import fr.ird.observe.dto.referential.common.CountryReference;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialDto;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialHierarchyDto;
import fr.ird.observe.dto.referential.ps.common.ObjectOperationReference;
import fr.ird.observe.dto.referential.ps.common.TransmittingBuoyTypeReference;
import fr.ird.observe.services.service.referential.ReferentialService;
import io.ultreia.java4all.jaxx.widgets.combobox.BeanEnumEditor;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.jdesktop.swingx.JXTitledPanel;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.swing.Table;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.text.BigTextEditor;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 10/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface DcpUIAdapter<T extends TransmittingBuoyAware> extends JAXXObject {

    ObserveSwingValidator<T> getValidatorBuoy1();

    ObserveSwingValidator<T> getValidatorBuoy2();

    T getTransmittingBuoy1();

    T getTransmittingBuoy2();

    JXTitledPanel getTransmittingBuoy1EditorPanel();

    JXTitledPanel getTransmittingBuoy2EditorPanel();

    BeanEnumEditor<TypeTransmittingBuoyOperation> getTypeOperation();

    JPanel getTransmittingBuoys();

    JLabel getNoBuoyEditor();

    FilterableComboBox<TransmittingBuoyTypeReference> getTransmittingBuoyType1();

    BigTextEditor getComment();

    JTabbedPane getMainTabbedPane();

    FloatingObjectPartsTreeTable getTable();

    FilterableComboBox<VesselReference> getVessel1();

    FilterableComboBox<VesselReference> getVessel2();

    FilterableComboBox<CountryReference> getCountry1();

    FilterableComboBox<CountryReference> getCountry2();

    JMenuItem getAddPreset();

    JButton getCopyFloatingObjectPartToLeft();

    JButton getCopyFloatingObjectPartToRight();

    ContentEditUIHandler<?, ?> getHandler();

    Table getTransmittingBuoy1Editor();

    JLabel getCoordinate1Label();

    CoordinatesEditor getCoordinate1();

    BigTextEditor getComment1();

    default void onOpenForm(DcpUIModelStates<?, ?> states) {
        states.onOpenForm(states.getReferenceCache(),
                          states.getBean(),
                          getTypeOperation(),
                          b -> getTable().openTable(states, b),
                          getTransmittingBuoy1(),
                          getTransmittingBuoy2());
    }

    default void onInit(DcpUIModelStates<?, ?> states, ReferentialService referentialService, DecoratorService decoratorService) {

        Set<ObjectMaterialDto> dtoList = referentialService.loadDtoList(ObjectMaterialDto.class);
        ObjectMaterialHierarchyDto detailedForm = ObjectMaterialHierarchyDto.build(dtoList);

        Map<String, ObjectMaterialDto> allMap = new TreeMap<>();
        detailedForm.getAllDto().forEach(s -> allMap.putIfAbsent(s.getId(), s));
        decoratorService.installDecorator(ObjectMaterialDto.class, allMap.values().stream());

        states.setReferentialMap(allMap);

        getTable().initTable(states, detailedForm);

        states.addPropertyChangeListener(DcpUIModelStates.PROPERTY_PARTS_MODIFIED, e -> {
            if (Objects.equals(e.getNewValue(), Boolean.TRUE)) {
                states.recomputeComputedValues();
            }
        });
        states.getBean().addPropertyChangeListener(FloatingObjectDto.PROPERTY_OBJECT_OPERATION, e -> states.updateMaterials(getTable().getTreeTableModel(), (ObjectOperationReference) e.getNewValue()));
        states.setFirstBuoyListener(new TransmittingBuoyListener<>(getTransmittingBuoy1(), getVessel1(), getCountry1()));
        states.setSecondBuoyListener(new TransmittingBuoyListener<>(getTransmittingBuoy2(), getVessel2(), getCountry2()));
    }

    default void installExtraActions() {
        AddFloatingObjectPreset.init((ContentUI) this, getAddPreset(), new AddFloatingObjectPreset());
        CopyFloatingObjectPartToRight.init((ContentUI) this, getCopyFloatingObjectPartToRight(), new CopyFloatingObjectPartToRight());
        CopyFloatingObjectPartToLeft.init((ContentUI) this, getCopyFloatingObjectPartToLeft(), new CopyFloatingObjectPartToLeft());
    }

    default void onOpenAfterOpenModel(DcpUIModelStates<?, ?> states) {
        processDataBinding("typeOperation.selectedItem");
        TypeTransmittingBuoyOperation typeOperation = getTypeOperation().getSelectedValue();
        changeTypeTransmittingBuoyOperation(states, typeOperation, false);
    }

    default void stopEditUI(DcpUIModelStates<?, ?> states) {
        if (getMainTabbedPane().getSelectedIndex() == 1) {
            getTable().editingCanceled(null);
        }
        getValidatorBuoy1().setBean(null);
        getValidatorBuoy2().setBean(null);
        states.removeFirstBuoyListener();
        states.removeSecondBuoyListener();
    }

    default void changeTypeTransmittingBuoyOperation(ItemEvent event, DcpUIModelStates<?, ?> states) {
        if (event.getStateChange() == ItemEvent.SELECTED && states.isEditing()) {
            changeTypeTransmittingBuoyOperation(states, getTypeOperation().getSelectedValue(), true);
            states.setModified(true);
        }
    }

    default void startEditUI(DcpUIModelStates<?, ?> states) {
        FloatingObjectAware bean = states.getBean();
        if (bean.getFirstBuoy() != null) {
            getValidatorBuoy1().setBean(getTransmittingBuoy1());
            states.addFirstBuoyListener();
            if (bean.getSecondBuoy() != null) {
                getValidatorBuoy2().setBean(getTransmittingBuoy2());
                states.addSecondBuoyListener();
            }
        }
        states.setModified(states.isCreatingMode());
    }

    default void changeTypeTransmittingBuoyOperation(DcpUIModelStates<?, ?> states, TypeTransmittingBuoyOperation typeOperation, boolean reset) {

        if (Objects.requireNonNull(typeOperation).isWithCoordinate()) {
            addTransmittingBuoyCoordinate();
        } else {
            removeTransmittingBuoyCoordinate();
        }
        FloatingObjectAware bean = states.getBean();
        boolean withBuoy = bean.isNotTransmittingBuoyEmpty();
        List<TransmittingBuoyAware> modelBuoyList = new ArrayList<>();
        if (!reset) {
            if (withBuoy) {
                modelBuoyList.addAll(bean.getTransmittingBuoy());
            }
        }

        if (withBuoy) {
            // always reset buoys of editBean to avoid side-effects
            bean.getTransmittingBuoy().clear();
        }

        // clean container
        JPanel editorPanel = getTransmittingBuoys();
        editorPanel.removeAll();

        if (states.isEditing()) {
            getValidatorBuoy1().setBean(null);
            getValidatorBuoy2().setBean(null);
            states.removeFirstBuoyListener();
            states.removeSecondBuoyListener();
        }

        String[] transmittingBuoyOperationCodes = typeOperation.getTransmittingBuoyOperationCodes();
        JComponent focusOwner;
        switch (typeOperation.getBuoyCount()) {
            case 0:
                // no buoy
                editorPanel.add(getNoBuoyEditor());
                focusOwner = getComment();
                break;
            case 1:
                // one buoy
                if (reset) {
                    states.bindEmptyBuoy(states.getReferenceCache(), bean, getTransmittingBuoy1(), states.getBuoyOperation(transmittingBuoyOperationCodes[0]));
                } else {
                    states.bindExistingBuoy(bean, getTransmittingBuoy1(), modelBuoyList.get(0));
                }
                editorPanel.add(getTransmittingBuoy1EditorPanel());
                if (states.isEditing()) {
                    getValidatorBuoy1().setBean(getTransmittingBuoy1());
                    states.addFirstBuoyListener();
                }
                focusOwner = getTransmittingBuoyType1();
                break;
            case 2:
                // two buoy
                if (reset) {
                    states.bindEmptyBuoy(states.getReferenceCache(), bean, getTransmittingBuoy1(), states.getBuoyOperation(transmittingBuoyOperationCodes[0]));
                    states.bindEmptyBuoy(states.getReferenceCache(), bean, getTransmittingBuoy2(), states.getBuoyOperation(transmittingBuoyOperationCodes[1]));
                } else {
                    states.bindExistingBuoy(bean, getTransmittingBuoy1(), modelBuoyList.get(0));
                    states.bindExistingBuoy(bean, getTransmittingBuoy2(), modelBuoyList.get(1));
                }
                editorPanel.add(getTransmittingBuoy1EditorPanel());
                editorPanel.add(getTransmittingBuoy2EditorPanel());
                if (states.isEditing()) {
                    getValidatorBuoy1().setBean(getTransmittingBuoy1());
                    getValidatorBuoy2().setBean(getTransmittingBuoy2());
                    states.addFirstBuoyListener();
                    states.addSecondBuoyListener();
                }
                focusOwner = getTransmittingBuoyType1();
                break;
            default:
                // can't happen
                throw new IllegalArgumentException(String.format("Can't have %d buoy count", typeOperation.getBuoyCount()));
        }
        //FIXME:Focus update zone focus owner
        JComponent finalFocusOwner = focusOwner;
        SwingUtilities.invokeLater(() -> {
            getHandler().getDataSourceEditor().getHandler().updateContentSizeForce(500);
            if (finalFocusOwner != null) {
                getHandler().setFocusZoneOwner(finalFocusOwner);
            }
        });
    }


    private void addTransmittingBuoyCoordinate() {
        Table transmittingBuoy1Editor = getTransmittingBuoy1Editor();
        if (transmittingBuoy1Editor.getComponentCount() != 13) {
            transmittingBuoy1Editor.add(getCoordinate1Label(), new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, 17, 1, new Insets(3, 3, 3, 3), 0, 0));
            transmittingBuoy1Editor.add(getCoordinate1(), new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
            transmittingBuoy1Editor.add(getComment1(), new GridBagConstraints(0, 6, 2, 1, 0.0, 1.0, 10, 1, new Insets(3, 3, 3, 3), 0, 0));
        }
    }

    private void removeTransmittingBuoyCoordinate() {
        getCoordinate1().reset();
        Table transmittingBuoy1Editor = getTransmittingBuoy1Editor();
        if (transmittingBuoy1Editor.getComponentCount() == 13) {
            transmittingBuoy1Editor.remove(12);
            transmittingBuoy1Editor.remove(11);
            transmittingBuoy1Editor.remove(10);
            transmittingBuoy1Editor.add(getComment1(), new GridBagConstraints(0, 5, 2, 1, 0.0, 1.0, 10, 1, new Insets(3, 3, 3, 3), 0, 0));
        }
    }

}
