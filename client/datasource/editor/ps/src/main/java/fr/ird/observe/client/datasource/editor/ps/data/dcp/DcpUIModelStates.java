package fr.ird.observe.client.datasource.editor.ps.data.dcp;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.DcpComputedValue;
import fr.ird.observe.dto.data.ps.FloatingObjectAware;
import fr.ird.observe.dto.data.ps.FloatingObjectPartAware;
import fr.ird.observe.dto.data.ps.TransmittingBuoyAware;
import fr.ird.observe.dto.data.ps.TypeTransmittingBuoyOperation;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.data.ps.logbook.TransmittingBuoyDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialDto;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialReference;
import fr.ird.observe.dto.referential.ps.common.ObjectOperationReference;
import fr.ird.observe.dto.referential.ps.common.TransmittingBuoyOperationReference;
import fr.ird.observe.dto.referential.ps.common.TransmittingBuoyOwnershipReference;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.combobox.BeanEnumEditor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * For common states on Dcp form.
 * <p>
 * Created on 07/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface DcpUIModelStates<D extends DataDto & FloatingObjectAware, P extends DataDto & FloatingObjectPartAware> extends WithClientUIContextApi, JavaBean {
    Logger log = LogManager.getLogger(DcpUIModelStates.class);
    String PROPERTY_PARTS_MODIFIED = "partsModified";
    String PROPERTY_REFERENCE = "reference";
    String PROPERTY_ARRIVING = "arriving";
    String PROPERTY_LEAVING = "leaving";

    static String booleanValue(DcpComputedValue value) {
        return value == null ? t("observe.Common.notComputed") : value.getLabel();
    }

    static String stringValue(String value) {
        return value == null ? t("observe.Common.notComputed") : value;
    }

    static boolean isBuoyDeployement(TransmittingBuoyOperationReference operation) {
        return operation != null && ProtectedIdsPs.PS_COMMON_TRANSMITTING_BUOY_OPERATION_POSE.equals(operation.getId());
    }

    void fireComputedValuesChanged();

    D getBean();

    Optional<ToolkitIdModifications> consolidate();

    TransmittingBuoyListener<?> getFirstBuoyListener();

    void setFirstBuoyListener(TransmittingBuoyListener<?> firstBuoyListener);

    TransmittingBuoyListener<?> getSecondBuoyListener();

    void setSecondBuoyListener(TransmittingBuoyListener<?> secondBuoyListener);

    default void recomputeComputedValues() {
        Optional<ToolkitIdModifications> result = consolidate();
        result.ifPresent(r -> {
            log.info(String.format("Flush %d consolidate floating object changes...", r.getModifications().size()));
            r.flushToBean(getBean());
            fireComputedValuesChanged();
        });
    }

    default FloatingObjectPreset toPreset() {
        FloatingObjectPreset preset = getBean().toPreset();
        Map<String, String> whenArriving = new LinkedHashMap<>();
        for (Map.Entry<ObjectMaterialDto, String> entry : getWhenArriving().entrySet()) {
            whenArriving.put(entry.getKey().getId(), entry.getValue());
        }
        preset.setWhenArrivingMaterials(whenArriving);
        Map<String, String> whenLeaving = new LinkedHashMap<>();
        for (Map.Entry<ObjectMaterialDto, String> entry : getWhenLeaving().entrySet()) {
            whenLeaving.put(entry.getKey().getId(), entry.getValue());
        }
        preset.setWhenLeavingMaterials(whenLeaving);
        return preset;
    }

    P newPart(Date now);

    default Set<P> toParts() {
        Set<P> result = new LinkedHashSet<>();
        Date now = new Date();
        boolean arriving = isArriving();
        boolean leaving = isLeaving();
        Map<ObjectMaterialDto, String> whenArriving = getWhenArriving();
        Map<ObjectMaterialDto, String> whenLeaving = getWhenLeaving();
        for (ObjectMaterialDto o : getAllWithData()) {
            String whenArriving1 = whenArriving.get(o);
            String whenLeaving1 = whenLeaving.get(o);
            if (whenArriving1 == null && whenLeaving1 == null) {
                // no more value for this material
                continue;
            }
            P partDto = newPart(now);
            partDto.setObjectMaterial(ObjectMaterialReference.of(getReferentialLocale(), o));
            if (arriving) {
                partDto.setWhenArriving(whenArriving1);
            }
            if (leaving) {
                partDto.setWhenLeaving(whenLeaving1);
            }
            result.add(partDto);
        }
        return result;
    }

    ReferentialLocale getReferentialLocale();

    void setPartsModified();

    default void reset() {
        getWhenArriving().clear();
        getWhenLeaving().clear();
        getBean().setMaterialsValid(true);
        getBean().setCanValidateMaterials(true);
        fireComputedValuesChanged();
    }

    Map<String, ObjectMaterialDto> getReferentialMap();

    void setReferentialMap(Map<String, ObjectMaterialDto> referentialMap);

    Map<String, TransmittingBuoyOperationReference> getBuoyOperationMap();

    void setBuoyOperationMap(Map<String, TransmittingBuoyOperationReference> buoyOperationMap);

    default TransmittingBuoyOperationReference getBuoyOperation(String code) {
        return getBuoyOperationMap().get(code);
    }

    Map<ObjectMaterialDto, String> getWhenArriving();

    Map<ObjectMaterialDto, String> getWhenLeaving();

    default Set<ObjectMaterialDto> getAllWithData() {
        Set<ObjectMaterialDto> all = new LinkedHashSet<>(getWhenArriving().keySet());
        all.addAll(getWhenLeaving().keySet());
        return all.stream().filter(ObjectMaterialDto::withData).collect(Collectors.toSet());
    }

    void setWhenArriving(String id, String value);

    void setWhenLeaving(String id, String value);

    Object getWhenArriving(String id);

    Object getWhenLeaving(String id);

    FloatingObjectPreset getReference();

    void setReference(FloatingObjectPreset reference);

    default String getComputedWhenArrivingBiodegradableValue() {
        return DcpUIModelStates.booleanValue(getBean().getComputedWhenArrivingBiodegradable());
    }

    default String getComputedWhenArrivingNonEntanglingValue() {
        return DcpUIModelStates.booleanValue(getBean().getComputedWhenArrivingNonEntangling());
    }

    default String getComputedWhenArrivingSimplifiedObjectTypeValue() {
        String computedSimplifiedObjectType = getBean().getComputedWhenArrivingSimplifiedObjectType();
        return DcpUIModelStates.stringValue(computedSimplifiedObjectType);
    }

    default String getComputedWhenLeavingBiodegradableValue() {
        return DcpUIModelStates.booleanValue(getBean().getComputedWhenLeavingBiodegradable());
    }

    default String getComputedWhenLeavingNonEntanglingValue() {
        return DcpUIModelStates.booleanValue(getBean().getComputedWhenLeavingNonEntangling());
    }

    default String getComputedWhenLeavingSimplifiedObjectTypeValue() {
        String computedSimplifiedObjectType = getBean().getComputedWhenLeavingSimplifiedObjectType();
        return DcpUIModelStates.stringValue(computedSimplifiedObjectType);
    }

    boolean isArriving();

    void setArriving(boolean arriving);

    boolean isLeaving();

    void setLeaving(boolean leaving);

    void setModified(boolean modified);

    default void setWhen0(String id, String value, Map<ObjectMaterialDto, String> map) {
        ObjectMaterialDto dto = Objects.requireNonNull(getReferentialMap().get(id));
        if (value == null) {
            map.remove(dto);
        } else {
            map.put(dto, value);
        }
    }

    default void setMaterialsValid(boolean materialsValid) {
        getBean().setMaterialsValid(materialsValid);
    }

    default void openTable() {
        getBean().setCanValidateMaterials(true);
        for (FloatingObjectPartAware p : getBean().getFloatingObjectPart()) {
            String objectMaterialId = p.getObjectMaterial().getId();
            String whenArriving = p.getWhenArriving();
            String whenLeaving = p.getWhenLeaving();
            if (whenArriving != null && !Objects.equals("false", whenArriving)) {
                setWhenArriving(objectMaterialId, whenArriving);
            }
            if (whenLeaving != null && !Objects.equals("false", whenLeaving)) {
                setWhenLeaving(objectMaterialId, whenLeaving);
            }
        }
    }

    default void bindExistingBuoy(FloatingObjectAware bean, TransmittingBuoyAware uiBuoy, TransmittingBuoyAware modelBuoy) {
        TransmittingBuoyOperationReference buoyOperation = modelBuoy.getTransmittingBuoyOperation();
        Optional.ofNullable(buoyOperation).ifPresent(getDecoratorService()::installDecorator);
        uiBuoy.setTransmittingBuoyOperation(null);
        uiBuoy.setTransmittingBuoyOperation(buoyOperation);
        @SuppressWarnings("unchecked") Collection<TransmittingBuoyAware> transmittingBuoy = (Collection<TransmittingBuoyAware>) bean.getTransmittingBuoy();
        transmittingBuoy.add(uiBuoy);
    }

    default void bindEmptyBuoy(ReferencesCache referencesCache, FloatingObjectAware bean, TransmittingBuoyAware uiBuoy, TransmittingBuoyOperationReference operation) {
        TransmittingBuoyDto.newDto(new Date()).copy(uiBuoy);
        uiBuoy.setTransmittingBuoyOperation(operation);
        if (DcpUIModelStates.isBuoyDeployement(operation)) {
            // operation de pose
            // toujours appartient au navire
            TransmittingBuoyOwnershipReference r = referencesCache.<TransmittingBuoyOwnershipReference>tryGetReferentialReferenceById(TransmittingBuoyDto.PROPERTY_TRANSMITTING_BUOY_OWNERSHIP, ProtectedIdsPs.PS_COMMON_TRANSMITTING_BUOY_OWNERSHIP_THIS_SHIP_ID).orElse(null);
            uiBuoy.setTransmittingBuoyOwnership(r);
        }
        @SuppressWarnings("unchecked") Collection<TransmittingBuoyAware> transmittingBuoy = (Collection<TransmittingBuoyAware>) bean.getTransmittingBuoy();
        transmittingBuoy.add(uiBuoy);
    }

    default void updateMaterials(FloatingObjectPartsTreeTableModel treeModel, ObjectOperationReference operation) {
        if (operation == null) {
            setArriving(false);
            setLeaving(false);
        } else {
            setArriving(operation.isWhenArriving());
            setLeaving(operation.isWhenLeaving());
        }
        treeModel.reset(true);
    }

    default void onOpenForm(ReferencesCache referencesCache,
                            FloatingObjectAware bean,
                            BeanEnumEditor<TypeTransmittingBuoyOperation> typeOperation,
                            Consumer<Boolean> openTable,
                            TransmittingBuoyAware ui1, TransmittingBuoyAware ui2) {

        FloatingObjectPreset floatingObjectReference = getReference();

        List<TransmittingBuoyOperationReference> referentialReferences = referencesCache.getReferentialReferences(TransmittingBuoyDto.PROPERTY_TRANSMITTING_BUOY_OPERATION);
        setBuoyOperationMap(Maps.uniqueIndex(referentialReferences, TransmittingBuoyOperationReference::getCode));

        TypeTransmittingBuoyOperation typeTransmittingBuoyOperation = bean.getTypeTransmittingBuoyOperation();
        typeOperation.setSelectedItem(typeTransmittingBuoyOperation);

        openTable.accept(bean.isPersisted() || floatingObjectReference != null);

        Optional.ofNullable(bean.getFirstBuoy()).ifPresent(b -> {
            b.copy(ui1);
            getFirstBuoyListener().apply();
        });
        Optional.ofNullable(bean.getSecondBuoy()).ifPresent(b -> {
            b.copy(ui2);
            getSecondBuoyListener().apply();
        });
        getBean().setCanValidateMaterials(true);
    }

    boolean isEditing();

    ReferencesCache getReferenceCache();

    boolean isCreatingMode();

    default void addFirstBuoyListener() {
        getFirstBuoyListener().install();
    }

    default void removeFirstBuoyListener() {
        getFirstBuoyListener().uninstall();
    }

    default void addSecondBuoyListener() {
        getSecondBuoyListener().install();
    }

    default void removeSecondBuoyListener() {
        getSecondBuoyListener().uninstall();
    }

}
