package fr.ird.observe.client.datasource.editor.ps.data.dcp;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialDto;
import org.jdesktop.swingx.tree.DefaultXTreeCellRenderer;

import javax.swing.JTree;
import java.awt.Component;

/**
 * Created by tchemit on 05/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.0
 */
public class FloatingObjectPartLegendTreeCellRenderer extends DefaultXTreeCellRenderer implements WithClientUIContextApi {

    public FloatingObjectPartLegendTreeCellRenderer() {
        super();
        setLeafIcon(null);
        setOpenIcon(null);
        setClosedIcon(null);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        FloatingObjectPartsTreeNode node = (FloatingObjectPartsTreeNode) value;
        ObjectMaterialDto valueAt = (ObjectMaterialDto) node.getValueAt(0);
        if (valueAt != null) {
            value = valueAt.toString();
            if (valueAt.withValidation()) {
                value += " -  " + valueAt.getValidation();
            }
        }
        Component result = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        result.setEnabled(node.isEnabled());
        return result;
    }
}
