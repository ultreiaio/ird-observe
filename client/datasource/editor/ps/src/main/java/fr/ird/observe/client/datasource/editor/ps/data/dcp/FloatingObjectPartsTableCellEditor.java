package fr.ird.observe.client.datasource.editor.ps.data.dcp;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.treetable.JRadioButtonCellEditor;
import org.jdesktop.swingx.JXTreeTable;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.util.EventObject;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by tchemit on 05/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.0
 */
public class FloatingObjectPartsTableCellEditor implements TableCellEditor {

    private final TableCellEditor booleanEditor;
    private final TableCellEditor stringEditor;
    private final TableCellEditor radioEditor;
    private final TableCellEditor intEditor;
    private final TableCellEditor floatEditor;
    private final Set<TableCellEditor> editors;
    private TableCellEditor editor;

    private static class IntegerCellEditor extends NumberCellEditor<Integer> {
        IntegerCellEditor() {
            super(Integer.class, false);
            numberEditor.setNumberPattern(UIHelper.INT_6_DIGITS_PATTERN);
        }
    }

    private static class FloatCellEditor extends NumberCellEditor<Float> {
        FloatCellEditor() {
            super(Float.class, false);
            numberEditor.setNumberPattern(UIHelper.DECIMAL1_PATTERN);
        }
    }

    public FloatingObjectPartsTableCellEditor(JXTreeTable table) {
        // register as a default editors to get correct integration with ui
        table.setDefaultEditor(boolean.class, new JRadioButtonCellEditor());
        table.setDefaultEditor(int.class, new IntegerCellEditor());
        table.setDefaultEditor(float.class, new FloatCellEditor());

        this.editors = new LinkedHashSet<>();
        editors.add(this.booleanEditor = table.getDefaultEditor(Boolean.class));
        editors.add(this.stringEditor = table.getDefaultEditor(Object.class));
        editors.add(this.radioEditor = table.getDefaultEditor(boolean.class));
        editors.add(this.intEditor = table.getDefaultEditor(int.class));
        editors.add(this.floatEditor = table.getDefaultEditor(float.class));
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        FloatingObjectPartsTreeNode node = (FloatingObjectPartsTreeNode) ((JXTreeTable) table).getPathForRow(row).getLastPathComponent();

        if (node.isBoolean()) {
            editor = node.isExclusive() ? radioEditor : booleanEditor;
        } else if (node.isText()) {
            editor = stringEditor;
        } else if (node.isFloat()) {
            editor = floatEditor;
            if (value != null) {
                value = Float.valueOf(value.toString());
            }
        } else if (node.isInteger()) {
            editor = intEditor;
            if (value != null) {
                value = Integer.valueOf(value.toString());
            }
        } else {
            throw new IllegalStateException("Can't manage type: " + node.getObjectMaterialType());
        }
        return editor.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    @Override
    public Object getCellEditorValue() {
        Object o = editor == null ? null : editor.getCellEditorValue();
        return o == null ? null : Objects.equals(false, o) ? null : String.valueOf(o);
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return editors.stream().anyMatch(e -> e.isCellEditable(anEvent));
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean stopCellEditing() {
        return editor != null && editor.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        if (editor != null) {
            editor.cancelCellEditing();
        }
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        editors.forEach(e -> e.addCellEditorListener(l));
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        editors.forEach(e -> e.removeCellEditorListener(l));
    }

}
