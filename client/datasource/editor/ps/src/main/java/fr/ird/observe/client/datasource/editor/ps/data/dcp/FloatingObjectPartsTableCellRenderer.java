package fr.ird.observe.client.datasource.editor.ps.data.dcp;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.treetable.JRadioButtonProvider;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.util.Objects;

/**
 * Created by tchemit on 05/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.0
 */
public class FloatingObjectPartsTableCellRenderer implements TableCellRenderer {

    // Render booleans (inclusive)
    private final TableCellRenderer booleanInclusiveRenderer;
    // Render booleans (exclusive)
    private final TableCellRenderer booleanExclusiveRenderer;
    // Render anything else
    private final TableCellRenderer objectRenderer;

    public FloatingObjectPartsTableCellRenderer(JXTreeTable table) {
        // register as a default renderer to get correct integration with ui
        table.setDefaultRenderer(boolean.class, new DefaultTableRenderer(new JRadioButtonProvider()));
        this.booleanInclusiveRenderer = table.getDefaultRenderer(Boolean.class);
        this.objectRenderer = table.getDefaultRenderer(Object.class);
        this.booleanExclusiveRenderer = table.getDefaultRenderer(boolean.class);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        FloatingObjectPartsTreeNode node = (FloatingObjectPartsTreeNode) ((JXTreeTable) table).getPathForRow(row).getLastPathComponent();
        Objects.requireNonNull(node);
        TableCellRenderer renderer = objectRenderer;
        Object newValue = value;
        boolean enabled = node.isEditable() && table.isCellEditable(row, column) && node.isEnabled(column);
        if (node.isBoolean()) {
            if (node.isColumnEditable(column)) {
                newValue = value == null ? null : Boolean.valueOf(String.valueOf(value));
                renderer = node.isExclusive() ? booleanExclusiveRenderer : booleanInclusiveRenderer;
            } else {
                newValue = "";
            }
        }
        Component component = renderer.getTableCellRendererComponent(table, newValue, isSelected, hasFocus, row, column);
        component.setEnabled(enabled || (!((JXTreeTable) table).isEditable() &&  node.isEnabled(column) && newValue!=null));
        return component;
    }
}
