package fr.ird.observe.client.datasource.editor.ps.data.dcp;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialHierarchyDto;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialTypeReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Created by tchemit on 30/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FloatingObjectPartsTreeNode extends AbstractMutableTreeTableNode implements Iterable<FloatingObjectPartsTreeNode> {

    private static final Logger log = LogManager.getLogger(FloatingObjectPartsTreeNode.class);

    //TODO Improve the design, we should not store anything in uiModel and separate leaving and arriving data
    private static class FloatingObjectPartsTreeNodeContext implements WithClientUIContextApi {

        // main model to get and store values
        private final DcpUIModelStates<?, ?> uiModelStates;
        // dto (null for root)
        private final ObjectMaterialHierarchyDto dto;
        // Is the node is enabled (from hierarchic value) ?
        private final boolean enabled;
        // Is this node editable ?
        private final boolean editable;
        // Is the node is exclusive (means only one value possible for him and his brothers) ?
        private final boolean exclusive;
        // Is the node use validation on his value ?
        private final boolean useValidation;
        // Referential locale to decorate
        private final ReferentialLocale referentialLocale;
        // Is this node (on arriving) need at least one child selected ? (if editable then node must be selected)
        private boolean needOneSelectionOnLeaving;
        // Is this node (on leaving) need at least one child selected ? (if editable then node must be selected)
        private boolean needOneSelectionOnArriving;
        // for a validation node (on arriving), is value valid (if no value then it is valid)
        private boolean valueValidOnLeaving = true;
        // for a validation node (on leaving), is value valid (if no value then it is valid)
        private boolean valueValidOnArriving = true;
        // Is the node valid (on arriving) column ?
        private boolean validWhenArriving = true;
        // Is the node valid (on leaving) column ?
        private boolean validWhenLeaving = true;
        // Set of brothers of this node that is in a exclusive group of node (only one value among all of them)
        private Set<FloatingObjectPartsTreeNode> companions;
        // Internal to store debug node text
        private String text;
        // Count of children selected (if none is selected then the node can be edited (from GUI if a child is selected then any parent node is not enabled) ?
        private int selectedChildCountOnArriving = 0;
        private int selectedChildCountOnLeaving = 0;

        FloatingObjectPartsTreeNodeContext(DcpUIModelStates<?, ?> uiModelStates) {
            this.uiModelStates = Objects.requireNonNull(uiModelStates);
            this.dto = null;
            this.enabled = true;
            this.editable = false;
            this.exclusive = false;
            this.useValidation = false;
            this.referentialLocale = getDecoratorService().getReferentialLocale();
        }

        FloatingObjectPartsTreeNodeContext(ObjectMaterialHierarchyDto dto, FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext parent) {
            this.uiModelStates = Objects.requireNonNull(parent).uiModelStates;
            this.dto = Objects.requireNonNull(dto);
            // enabled if parent is enabled and dto is enabled
            this.enabled = parent.enabled && dto.isEnabled();
            // editable if dto is selectable (we also make sure that the object material type is here too)
            this.editable = dto.getObjectMaterialType() != null;
            this.useValidation = enabled && editable && dto.withValidation();
            // exclusive if his parent requires it
            this.exclusive = parent.dto != null && !parent.dto.isChildrenMultiSelectable();
            this.referentialLocale = getDecoratorService().getReferentialLocale();
            log.debug(String.format("New node: %s %s - needOneSelection %b-%b - exclusive %s", dto.getCode(), dto.getLabel(referentialLocale), needOneSelectionOnArriving, needOneSelectionOnLeaving, exclusive));
        }

        Object getValueAt(int column) {
            switch (column) {
                case 0: // dto
                    return dto;
                case 1: // when arriving
                    return uiModelStates.getWhenArriving().get(dto);
                case 2: // when leaving
                    return uiModelStates.getWhenLeaving().get(dto);
            }
            throw new IllegalStateException();
        }

        void setValueAt(String value, int column) {
            text = null;
            switch (column) {
                case 1: // when arriving
                    uiModelStates.setWhenArriving(dto.getId(), value);
                    if (useValidation) {
                        valueValidOnArriving = dto.isValid(value);
                    }
                    return;
                case 2: // when leaving
                    uiModelStates.setWhenLeaving(dto.getId(), value);
                    if (useValidation) {
                        valueValidOnLeaving = dto.isValid(value);
                    }
                    return;
            }
            throw new IllegalStateException();
        }

        boolean isColumnEditable(int column) {
            switch (column) {
                case 1: // when arriving
                    return uiModelStates.isArriving();
                case 2: // when leaving
                    return uiModelStates.isLeaving();
            }
            throw new IllegalStateException();
        }

        boolean isValid(int column) {
            switch (column) {
                case 1: // when arriving
                    return validWhenArriving && (!useValidation || valueValidOnArriving);
                case 2: // when leaving
                    return validWhenLeaving && (!useValidation || valueValidOnLeaving);
            }
            throw new IllegalStateException();
        }

        Optional<FloatingObjectPartsTreeNode> getSelectedCompanion(int column) {
            return companions.stream().filter(n -> n.getValueAt(column) != null).findFirst();
        }

        public String getText() {
            if (text == null && dto != null) {
                text = String.format("%s [value: %s-%s] [valid: %s-%s]", dto.getLabel(referentialLocale), getValueAt(1), getValueAt(2), isValid(1), isValid(2));
            }
            return text;
        }

        void computeValidationValidState() {
            if (uiModelStates.isArriving()) {
                Object value = uiModelStates.getWhenArriving(dto.getId());
                valueValidOnArriving = dto.isValid(value);
            }
            if (uiModelStates.isLeaving()) {
                Object value = uiModelStates.getWhenLeaving(dto.getId());
                valueValidOnLeaving = dto.isValid(value);
            }
        }

        boolean isRealEnabledOnArriving() {
            return selectedChildCountOnArriving == 0;
        }

        void incSelectedChildCountOnArriving() {
            this.selectedChildCountOnArriving++;
        }

        void decSelectedChildCountOnArriving() {
            this.selectedChildCountOnArriving--;
        }

        boolean isRealEnabledOnLeaving() {
            return selectedChildCountOnLeaving == 0;
        }

        void incSelectedChildCountOnLeaving() {
            this.selectedChildCountOnLeaving++;
        }

        void decSelectedChildCountOnLeaving() {
            this.selectedChildCountOnLeaving--;
        }
    }

    static FloatingObjectPartsTreeNode createRoot(DcpUIModelStates<?, ?> model, ObjectMaterialHierarchyDto dto) {
        FloatingObjectPartsTreeNode root;
        FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext rootModel = new FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext(model);
        if (dto == null) {
            root = new FloatingObjectPartsTreeNode(rootModel);
        } else {
            FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext childModel = new FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext(dto, rootModel);
            root = new FloatingObjectPartsTreeNode(childModel);
            root.computeCompanions(null);
            root.setCompanions(null);
        }
        return root;
    }

    private FloatingObjectPartsTreeNode(FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext context) {
        super(context);
        if (context.dto != null) {
            for (ObjectMaterialHierarchyDto dto : context.dto.getChildren()) {
                FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext childContext = new FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext(dto, context);
                add(new FloatingObjectPartsTreeNode(childContext));
            }
        }
    }

    boolean isNotValid() {
        return !(isValid(1) && isValid(2));
    }

    @Override
    public String toString() {
        return getUserObject().getText();
    }

    @Override
    public FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext getUserObject() {
        return (FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext) super.getUserObject();
    }

    @Override
    public Object getValueAt(int column) {
        return getUserObject().getValueAt(column);
    }

    @Override
    public void setValueAt(Object aValue, int column) {
        String value = aValue == null ? null : String.valueOf(aValue).trim();
        if (Objects.equals("", value)) {
            value = null;
        }
        getUserObject().setValueAt(value, column);
    }

    @Override
    public boolean isEditable(int column) {
        return column > 0 && isEnabled(column) && getUserObject().editable && isColumnEditable(column);
    }

    boolean isColumnEditable(int column) {
        return getUserObject().isColumnEditable(column);
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public Iterator<FloatingObjectPartsTreeNode> iterator() {
        return (Iterator) (children == null ? Collections.emptyIterator() : children.iterator());
    }

    public boolean isValid(int column) {
        return getUserObject().isValid(column);
    }

    boolean isExclusive() {
        return getUserObject().exclusive;
    }

    public boolean isEditable() {
        return getUserObject().editable;
    }

    public boolean isEnabled() {
        return getUserObject().enabled;
    }

    public boolean isEnabled(int column) {
        boolean result = getUserObject().enabled;
        if (result) {
            if (column == 1) {
                result = isRealEnabledOnArriving();
            } else if (column == 2) {
                result = isRealEnabledOnLeaving();
            }
        }
        return result;
    }

    public String getId() {
        return getUserObject().dto.getId();
    }

    public boolean isBoolean() {
        return getUserObject().dto.isBoolean();
    }

    public boolean isText() {
        return getUserObject().dto.isText();
    }

    public boolean isInteger() {
        return getUserObject().dto.isInteger();
    }

    public boolean isFloat() {
        return getUserObject().dto.isFloat();
    }

    public boolean withValidation() {
        return getUserObject().dto.withValidation();
    }

    ObjectMaterialTypeReference getObjectMaterialType() {
        return getUserObject().dto.getObjectMaterialType();
    }

    Optional<FloatingObjectPartsTreeNode> getSelectedCompanion(int column) {
        return getUserObject().getSelectedCompanion(column);
    }

    void resetStates() {
        getUserObject().text = null;
        getUserObject().validWhenArriving = true;
        getUserObject().validWhenLeaving = true;
        getUserObject().needOneSelectionOnArriving = false;
        getUserObject().needOneSelectionOnLeaving = false;
    }

    @Override
    public FloatingObjectPartsTreeNode getParent() {
        return (FloatingObjectPartsTreeNode) super.getParent();
    }

    boolean isRealEnabledOnArriving() {
        return getUserObject().isRealEnabledOnArriving();
    }

    void setRealEnabledOnArriving(boolean realEnabled) {
        FloatingObjectPartsTreeNode parent = getParent();
        if (parent != null) {
            if (realEnabled) {
                parent.getUserObject().decSelectedChildCountOnArriving();
            } else {
                parent.getUserObject().incSelectedChildCountOnArriving();
            }
            parent.setRealEnabledOnArriving(realEnabled);
        }
    }

    boolean isRealEnabledOnLeaving() {
        return getUserObject().isRealEnabledOnLeaving();
    }

    void setRealEnabledOnLeaving(boolean realEnabled) {
        FloatingObjectPartsTreeNode parent = getParent();
        if (parent != null) {
            if (realEnabled) {
                parent.getUserObject().decSelectedChildCountOnLeaving();
            } else {
                parent.getUserObject().incSelectedChildCountOnLeaving();
            }
            parent.setRealEnabledOnLeaving(realEnabled);
        }
    }

    Set<FloatingObjectPartsTreeNode> getShell() {

        Set<FloatingObjectPartsTreeNode> result = new LinkedHashSet<>();
        getShell(result);
        return result;
    }

    private void getShell(Set<FloatingObjectPartsTreeNode> allNodesBuilder) {
        allNodesBuilder.add(this);
        for (FloatingObjectPartsTreeNode child : this) {
            child.getShell(allNodesBuilder);
        }
    }

    public boolean withMandatoryConstraintsOnChildren() {
        ObjectMaterialHierarchyDto userObject = getUserObject().dto;
        return userObject == null || userObject.isChildSelectionMandatory();
    }

    void computeNeedAtLeastOnSelectValidState(boolean whenArriving, boolean whenLeaving) {
        boolean needSelect = isEditable();
        FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext userObject = getUserObject();
        if (whenArriving) {
            userObject.needOneSelectionOnArriving = (!needSelect || withValue(1)) && isAtLeastOneSelected(1);
        }
        if (whenLeaving) {
            userObject.needOneSelectionOnLeaving = (!needSelect || withValue(2)) && isAtLeastOneSelected(2);
        }
        if (userObject.needOneSelectionOnArriving || userObject.needOneSelectionOnLeaving) {
            log.info("Validate node " + this);
        }

    }

    void computeMandatoryValidState(boolean whenArriving, boolean whenLeaving) {
        FloatingObjectPartsTreeNode.FloatingObjectPartsTreeNodeContext userObject = getUserObject();
        FloatingObjectPartsTreeNode parent = getParent();
        if (whenArriving) {
            if (parent != null && !parent.getUserObject().validWhenArriving) {
                userObject.validWhenArriving = false;
            } else {
                FloatingObjectPartsTreeNode selection = getFirstAncestorNeedOneSelection(1);
                userObject.validWhenArriving = selection.getUserObject().needOneSelectionOnArriving;
                if (selection.getParent() == null) {
                    userObject.validWhenArriving = "true".equals(selection.getUserObject().getValueAt(1))
                            || selection.isAtLeastOneSelected(1);
                }
            }
        }
        if (whenLeaving) {
            if (parent != null && !parent.getUserObject().validWhenLeaving) {
                userObject.validWhenLeaving = false;
            } else {
                FloatingObjectPartsTreeNode selection = getFirstAncestorNeedOneSelection(2);
                userObject.validWhenLeaving = selection.getUserObject().needOneSelectionOnLeaving;
                if (selection.getParent() == null) {
                    userObject.validWhenLeaving = "true".equals(selection.getUserObject().getValueAt(2))
                            || selection.isAtLeastOneSelected(2);
                }
            }
        }

        if (userObject.getValueAt(1) != null || userObject.getValueAt(2) != null) {
            log.info("Validate node " + this);
        }
    }

    void computeValidationValidState() {
        getUserObject().computeValidationValidState();
    }

    void resetRealEnabled() {
        getUserObject().selectedChildCountOnArriving = 0;
        getUserObject().selectedChildCountOnLeaving = 0;
    }

    private FloatingObjectPartsTreeNode getFirstAncestorNeedOneSelection(int column) {
        if (withMandatoryConstraintsOnChildren()) {
            if (parent == null) {
                return this;
            }
            if (withValue(column)) {
                return this;
            }
        }
        if (parent == null) {
            return this;
        }
        return getParent().getFirstAncestorNeedOneSelection(column);
    }

    private void setCompanions(Set<FloatingObjectPartsTreeNode> companions) {
        if (companions != null) {
            for (FloatingObjectPartsTreeNode child : this) {
                child.getUserObject().companions = companions;
            }
        }
    }

    private boolean withValue(int column) {
        return getValueAt(column) != null;
    }

    private boolean withValidValue(int column) {
        return withValue(column) && (column == 1 ? getUserObject().valueValidOnArriving : getUserObject().valueValidOnLeaving);
    }

    private boolean isAtLeastOneSelected(int column) {
        for (FloatingObjectPartsTreeNode child : this) {
            if (child.withValidValue(column)) {
                return true;
            }
            boolean result = child.isAtLeastOneSelected(column);
            if (result) {
                return true;
            }
        }
        return false;
    }

    private void computeCompanions(Set<FloatingObjectPartsTreeNode> companionsBuilder) {
        if (companionsBuilder != null) {
            companionsBuilder.add(this);
        }

        Set<FloatingObjectPartsTreeNode> childCompanionsBuilder = needCompanions() ? new LinkedHashSet<>() : null;
        for (FloatingObjectPartsTreeNode child : this) {
            child.computeCompanions(childCompanionsBuilder);
        }
        setCompanions(childCompanionsBuilder);
    }

    private boolean needCompanions() {
        ObjectMaterialHierarchyDto dto = getUserObject().dto;
        return dto != null && !dto.isChildrenMultiSelectable() && !isLeaf();
    }
}
