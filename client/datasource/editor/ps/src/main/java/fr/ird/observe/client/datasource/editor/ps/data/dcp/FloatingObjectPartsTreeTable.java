package fr.ird.observe.client.datasource.editor.ps.data.dcp;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.ps.ObservePsKeyStrokes;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialHierarchyDto;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.table.ColumnFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.Enumeration;

/**
 * Created by tchemit on 11/07/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FloatingObjectPartsTreeTable extends JXTreeTable {

    public FloatingObjectPartsTreeTable(FloatingObjectPartsTreeTableModel treeModel) {
        super(treeModel);
        setTreeCellRenderer(new FloatingObjectPartLegendTreeCellRenderer());
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setRootVisible(true);
    }

    @Override
    public FloatingObjectPartsTreeTableModel getTreeTableModel() {
        return (FloatingObjectPartsTreeTableModel) super.getTreeTableModel();
    }

    public void openTable(DcpUIModelStates<?, ?> model, boolean expandTree) {

        model.openTable();


        FloatingObjectPartsTreeTableModel treeTableModel = getTreeTableModel();
        treeTableModel.reset(true);
        treeTableModel.computeRealEnabledState();

        if (expandTree) {
            expandAll();
        } else {
            // expand first level nodes (except if they are disabled)
            FloatingObjectPartsTreeNode root = treeTableModel.getRoot();
            Enumeration<? extends MutableTreeTableNode> children = root.children();
            while (children.hasMoreElements()) {
                FloatingObjectPartsTreeNode mutableTreeTableNode = (FloatingObjectPartsTreeNode) children.nextElement();
                if (mutableTreeTableNode.isEnabled()) {
                    SwingUtilities.invokeLater(() -> expandPath(new TreePath(new Object[]{root, mutableTreeTableNode})));
                }
            }
        }
        // auto expand nodes when selected
        addTreeSelectionListener(e -> {
            int selectedRow = getSelectedRow();
            if (selectedRow != -1) {
                if (!isExpanded(selectedRow)) {
                    SwingUtilities.invokeLater(() -> expandRow(selectedRow));
                }
            }
        });
    }

    public void initTable(DcpUIModelStates<?, ?> model, ObjectMaterialHierarchyDto materials) {

        FloatingObjectPartsTreeTableModel treeModel = getTreeTableModel();
        treeModel.rebuildRootNode(materials);

        setColumnFactory(new ColumnFactory() {

            @Override
            public void configureColumnWidths(JXTable table, TableColumnExt columnExt) {
                if (columnExt.getModelIndex() > 0) {
                    columnExt.setMinWidth(100);
                    columnExt.setMaxWidth(100);
                    columnExt.setPreferredWidth(100);
                }
            }
        });
        treeModel.addTreeModelListener(new TreeModelListener() {
            @Override
            public void treeNodesChanged(TreeModelEvent e) {
                if (treeModel.isAdjusting()) {
                    return;
                }
                model.setModified(true);
                model.setPartsModified();
            }

            @Override
            public void treeNodesInserted(TreeModelEvent e) {

            }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) {

            }

            @Override
            public void treeStructureChanged(TreeModelEvent e) {

            }

        });

        setDefaultRenderer(Object.class, new FloatingObjectPartsTableCellRenderer(this));
        setDefaultEditor(Object.class, new FloatingObjectPartsTableCellEditor(this));
        addHighlighter(new ColorHighlighter((renderer, adapter) -> {
            JXTreeTable component = (JXTreeTable) adapter.getComponent();
            int row = adapter.convertRowIndexToModel(adapter.row);
            FloatingObjectPartsTreeNode node = (FloatingObjectPartsTreeNode) component.getPathForRow(row).getLastPathComponent();
            boolean valid1 = node.isValid(1);
            boolean valid2 = node.isValid(2);
            switch (adapter.convertRowIndexToModel(adapter.column)) {
                case 0:
                    return false;
                case 1:
                    return !valid1;
                case 2:
                    return !valid2;
            }
            return true;
        }, model.getClientConfig().getFloatingObjectMaterialErrorColor(), Color.WHITE));

        addHighlighter(new ColorHighlighter((renderer, adapter) -> {
            JXTreeTable component = (JXTreeTable) adapter.getComponent();
            int row = adapter.convertRowIndexToModel(adapter.row);
            FloatingObjectPartsTreeNode node = (FloatingObjectPartsTreeNode) component.getPathForRow(row).getLastPathComponent();
            boolean realEnabled1 = node.isRealEnabledOnArriving();
            boolean realEnabled2 = node.isRealEnabledOnLeaving();
            boolean nodeEditable = node.isEditable();
            switch (adapter.convertRowIndexToModel(adapter.column)) {
                case 0:
                    return false;
                case 1:
                    return model.isArriving() && nodeEditable && !realEnabled1;
                case 2:
                    return model.isLeaving() && nodeEditable && !realEnabled2;
            }
            return true;
        }, model.getClientConfig().getFloatingObjectMaterialNotEditableColor(), Color.BLACK));

        InputMap inputMap = getInputMap(WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = getActionMap();
        inputMap.put(ObserveKeyStrokesEditorApi.KEY_STROKE_EXPAND_TREE_TABLE_NODE, "expandNode");
        actionMap.put("expandNode", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = getSelectedRow();
                if (selectedRow != -1) {
                    if (!isExpanded(selectedRow)) {
                        SwingUtilities.invokeLater(() -> expandRow(selectedRow));
                    }
                }
            }
        });
        inputMap.put(ObservePsKeyStrokes.KEY_STROKE_COLLAPSE_TREE_TABLE_NODE, "collapseNode");
        actionMap.put("collapseNode", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = getSelectedRow();
                if (selectedRow != -1) {
                    if (!isCollapsed(selectedRow)) {
                        SwingUtilities.invokeLater(() -> collapseRow(selectedRow));
                    }
                }
            }
        });
    }
}
