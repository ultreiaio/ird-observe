package fr.ird.observe.client.datasource.editor.ps.data.dcp;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialHierarchyDto;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableNode;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 05/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.0
 */
public class FloatingObjectPartsTreeTableModel extends DefaultTreeTableModel {

    private static final Logger log = LogManager.getLogger(FloatingObjectPartsTreeTableModel.class);

    private final DcpUIModelStates<?, ?> uiModel;

    private Set<FloatingObjectPartsTreeNode> allNodes;
    private Set<FloatingObjectPartsTreeNode> needOneSelectionNodes;
    private Set<FloatingObjectPartsTreeNode> mandatoryNodes;
    private Set<FloatingObjectPartsTreeNode> validationNodes;
    private boolean adjusting;

    public FloatingObjectPartsTreeTableModel(DcpUIModelStates<?, ?> uiModel) {
        super(FloatingObjectPartsTreeNode.createRoot(uiModel, null), Arrays.asList(
                I18n.t("observe.Common.type.short"),
                I18n.t("observe.Common.whenArriving"),
                I18n.t("observe.Common.whenLeaving")));
        this.uiModel = uiModel;
    }

    @Override
    public FloatingObjectPartsTreeNode getRoot() {
        return (FloatingObjectPartsTreeNode) super.getRoot();
    }

    @Override
    public void setRoot(TreeTableNode root) {
        super.setRoot(root);
        this.allNodes = ((FloatingObjectPartsTreeNode) root).getShell();
        this.needOneSelectionNodes = new LinkedHashSet<>();
        this.mandatoryNodes = new LinkedHashSet<>();
        this.validationNodes = new LinkedHashSet<>();

        for (FloatingObjectPartsTreeNode node : allNodes) {
            if (!node.isEnabled()) {
                continue;
            }
            if (node.getParent()==null) {
                continue;
            }
            mandatoryNodes.add(node);
            if (node.withMandatoryConstraintsOnChildren()) {
                needOneSelectionNodes.add(node);
            }
            if (node.withValidation()) {
                validationNodes.add(node);
            }
        }
    }

    public void rebuildRootNode(ObjectMaterialHierarchyDto materials) {
        FloatingObjectPartsTreeNode root = FloatingObjectPartsTreeNode.createRoot(uiModel, materials);
        setRoot(root);
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case 0:
                return String.class;
            case 1:
            case 2:
                return Object.class;
        }
        throw new IllegalStateException();
    }

    @Override
    public void setValueAt(Object value, Object node, int column) {
        FloatingObjectPartsTreeNode treeNode = (FloatingObjectPartsTreeNode) node;
        Optional<FloatingObjectPartsTreeNode> previousNode = treeNode.isExclusive() && value != null ? treeNode.getSelectedCompanion(column) : Optional.empty();
        previousNode.ifPresent(p -> log.info("Previous selected node: " + p));
        super.setValueAt(value, node, column);
        previousNode.ifPresent(t -> super.setValueAt(null, t, column));
        boolean realEnabled = !"true".equals(value);
        log.info(String.format("Real enabled? %s", realEnabled));
        if (column == 1) {
            treeNode.setRealEnabledOnArriving(realEnabled);
        } else if (column == 2) {
            treeNode.setRealEnabledOnLeaving(realEnabled);
        }
        if (adjusting) {
            return;
        }
        reset(false);
        uiModel.setModified(true);
    }

    public void reset(boolean computeValidation) {
        allNodes.forEach(FloatingObjectPartsTreeNode::resetStates);
        if (computeValidation) {
            validationNodes.forEach(FloatingObjectPartsTreeNode::computeValidationValidState);
        }

        boolean whenArriving = uiModel.isArriving();
        boolean whenLeaving = uiModel.isLeaving();

        needOneSelectionNodes.forEach(n -> n.computeNeedAtLeastOnSelectValidState(whenArriving, whenLeaving));
        mandatoryNodes.forEach(n -> n.computeMandatoryValidState(whenArriving, whenLeaving));

        boolean notValid = allNodes.stream().anyMatch(FloatingObjectPartsTreeNode::isNotValid);
        uiModel.setMaterialsValid(!notValid);
    }

    public void computeRealEnabledState() {
        Set<String> whenArriving = uiModel.getWhenArriving().keySet().stream().map(IdDto::getId).collect(Collectors.toSet());
        Set<String> whenLeaving = uiModel.getWhenLeaving().keySet().stream().map(IdDto::getId).collect(Collectors.toSet());
        allNodes.forEach(FloatingObjectPartsTreeNode::resetRealEnabled);
        for (FloatingObjectPartsTreeNode node : allNodes) {
            if (whenArriving.contains(node.getId())) {
                node.setRealEnabledOnArriving(false);
            }
            if (whenLeaving.contains(node.getId())) {
                node.setRealEnabledOnLeaving(false);
            }
        }
    }

    public boolean isAdjusting() {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
        if (!adjusting) {
            reset(false);
            uiModel.setModified(true);
        }
    }
}
