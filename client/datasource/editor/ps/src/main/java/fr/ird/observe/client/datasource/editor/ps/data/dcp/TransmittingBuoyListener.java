package fr.ird.observe.client.datasource.editor.ps.data.dcp;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.TransmittingBuoyAware;
import fr.ird.observe.dto.data.ps.logbook.TransmittingBuoyDto;
import fr.ird.observe.dto.referential.common.CountryReference;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.dto.referential.ps.common.TransmittingBuoyOwnershipReference;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created at 24/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class TransmittingBuoyListener<T extends TransmittingBuoyAware> implements PropertyChangeListener {

    private final T buoy;
    private final FilterableComboBox<VesselReference> vessel;
    private final FilterableComboBox<CountryReference> country;

    /**
     * Flag to avoid re-entrant code
     */
    private boolean adjusting;

    public TransmittingBuoyListener(T buoy, FilterableComboBox<VesselReference> vessel, FilterableComboBox<CountryReference> country) {
        this.buoy = buoy;
        this.vessel = vessel;
        this.country = country;
    }

    public void install() {
        buoy.removePropertyChangeListener(this);
        buoy.addPropertyChangeListener(this);
        apply();
    }

    public void uninstall() {
        buoy.removePropertyChangeListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (adjusting) {
            return;
        }
        adjusting = true;

        try {
            switch (evt.getPropertyName()) {
                case TransmittingBuoyDto.PROPERTY_TRANSMITTING_BUOY_OWNERSHIP:
                    TransmittingBuoyOwnershipReference buoyOwnership = (TransmittingBuoyOwnershipReference) evt.getNewValue();
                    apply(buoyOwnership, buoy.getVessel());
                    break;
                case TransmittingBuoyDto.PROPERTY_VESSEL:
                    VesselReference vessel = (VesselReference) evt.getNewValue();
                    apply(buoy.getTransmittingBuoyOwnership(), vessel);
                    break;

            }
        } finally {
            adjusting = false;
        }
    }

    public void apply() {
        apply(buoy.getTransmittingBuoyOwnership(), buoy.getVessel());
    }

    private void apply(TransmittingBuoyOwnershipReference buoyOwnership, VesselReference vesselReference) {
        boolean enableVessel = true;
        boolean enableCountry = true;
        if (buoyOwnership != null) {
            switch (buoyOwnership.getId()) {
                case ProtectedIdsPs.PS_COMMON_TRANSMITTING_BUOY_OWNERSHIP_THIS_SHIP_ID:
                    enableVessel = false;
                    enableCountry = false;
                    break;
                case ProtectedIdsPs.PS_COMMON_TRANSMITTING_BUOY_OWNERSHIP_UNKNOWN_ID:
                    enableVessel = false;
                    break;
            }
        }
        if (enableVessel) {
            vessel.setEnabled(true);
            if (vesselReference != null) {
                enableCountry = false;
            }
        } else {
            buoy.setVessel(null);
            vessel.setEnabled(false);
        }
        if (enableCountry) {
            country.setEnabled(true);
        } else {
            buoy.setCountry(null);
            country.setEnabled(false);
        }
    }
}
