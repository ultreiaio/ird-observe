package fr.ird.observe.client.datasource.editor.ps.data.dcp.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.DcpUIModelStates;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.presets.FloatingObjectPresetUI;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;
import io.ultreia.java4all.validation.bean.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 30/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AddFloatingObjectPreset extends ContentUIActionSupport<ContentUI> implements ConfigureMenuAction<ContentUI> {

    private static final Logger log = LogManager.getLogger(AddFloatingObjectPreset.class);

    public AddFloatingObjectPreset() {
        super(t("observe.data.ps.dcp.FloatingObjectPreset.action.add"),
              t("observe.data.ps.dcp.FloatingObjectPreset.action.add.tip"), "add-preset",
              ObserveKeyStrokesEditorApi.KEY_STROKE_ADD_PRESET);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, ContentUI ui) {
        DcpUIModelStates<?, ?> states = getStates(ui);

        JAXXInitialContext initialContext = UIHelper.initialContext(ui, states.toPreset())
                                                    .add(getDecoratorService())
                                                    .add(getDataSourcesManager().getMainDataSource());
        FloatingObjectPresetUI configPanel = new FloatingObjectPresetUI(initialContext);
        int size = configPanel.getModel().getBean().getMaterialIds().size();
        configPanel.getMaterialsScrollPane().getViewport().setPreferredSize(new Dimension(200, Math.min(4, size) * 20));
        configPanel.getMessageScrollPane().getViewport().setPreferredSize(new Dimension(200, 60));
        String replaceText = t("observe.ui.choice.save");
        Object[] options = {replaceText, t("observe.ui.choice.cancel")};
        JOptionPane pane = new JOptionPane(configPanel, JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, options[0]);
        JButton saveButton = Objects.requireNonNull(UsageUIHandlerSupport.findButton(pane, replaceText));
        configPanel.getValidator().addPropertyChangeListener(BeanValidator.VALID_PROPERTY, e -> onValidChanged(saveButton, (Boolean) e.getNewValue()));
        onValidChanged(saveButton, configPanel.getValidator().isValid());

        saveButton.addFocusListener(new FocusAdapter() {

            boolean init;

            @Override
            public void focusGained(FocusEvent e) {
                if (!init) {
                    init = true;
                    log.info("Add initial focus");
                    SwingUtilities.invokeLater(configPanel.getLabel1().getTextEditor()::requestFocusInWindow);
                }
            }
        });
//        pane.putClientProperty(UIHelper.NO_PACK, true);
//        pane.putClientProperty(UIHelper.FIX_SIZE, true);
//        Dimension size = getMainUI().getSize();
//        Dimension thisSize = new Dimension((int) size.getWidth() - 10, (int) size.getHeight() - 10);
//        pane.setPreferredSize(thisSize);
        int response = askToUser(pane, t("observe.data.ps.dcp.FloatingObjectPreset.add.title"), options);

        if (response != 0) {
            // user cancel
            return;
        }

        // add preset
        FloatingObjectPreset model = configPanel.getModel().toBean();
        if (model.getBuoy1() != null) {
            model.setBuoy1(configPanel.getBuoy1().getModel().toBean());
        }
        if (model.getBuoy2() != null) {
            model.setBuoy2(configPanel.getBuoy2().getModel().toBean());
        }
        FloatingObjectPresetsStorage floatingObjectPresets = getFloatingObjectPresetsManager();
        floatingObjectPresets.add(model);
        states.setReference(model);
    }

    protected DcpUIModelStates<?, ?> getStates(ContentUI ui) {
        return (DcpUIModelStates<?, ?>) ui.getModel().getStates();
    }

    private void onValidChanged(JButton saveButton, Boolean newValue) {
        saveButton.setEnabled(Objects.equals(true, newValue));
    }
}
