package fr.ird.observe.client.datasource.editor.ps.data.dcp.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.ContentUIActionSupport;
import fr.ird.observe.client.datasource.editor.ps.ObservePsKeyStrokes;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.FloatingObjectPartsTreeNode;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.FloatingObjectPartsTreeTableModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.Enumeration;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 20/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CopyFloatingObjectPartToLeft extends ContentUIActionSupport<ContentUI> {

    private static final Logger log = LogManager.getLogger(CopyFloatingObjectPartToLeft.class);

    public CopyFloatingObjectPartToLeft() {
        super(t("observe.ui.action.copyFloatingObjectPartToLeft"),
              t("observe.ui.action.copyFloatingObjectPartToLeft.tip"), "copyToLeft", ObservePsKeyStrokes.KEY_STROKE_COPY_FLOATING_OBJECT_PART_TO_LEFT);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, ContentUI ui) {

        JXTreeTable table = (JXTreeTable) ui.getObjectById("table");
        log.info("Start action from " + table);
        getBusyModel().addTask("Copy floating object parts");
        try {
            FloatingObjectPartsTreeTableModel treeTableModel = (FloatingObjectPartsTreeTableModel) table.getTreeTableModel();
            FloatingObjectPartsTreeNode root = treeTableModel.getRoot();
            Enumeration<? extends MutableTreeTableNode> children = root.children();
            treeTableModel.setAdjusting(true);
            try {
                while (children.hasMoreElements()) {
                    FloatingObjectPartsTreeNode mutableTreeTableNode = (FloatingObjectPartsTreeNode) children.nextElement();
                    move(treeTableModel, mutableTreeTableNode);
                }
            } finally {
                treeTableModel.setAdjusting(false);
                treeTableModel.reset(true);
                treeTableModel.computeRealEnabledState();
            }
            SwingUtilities.invokeLater(table::revalidate);
        } finally {
            getBusyModel().popTask();
        }
    }

    private void move(DefaultTreeTableModel treeTableModel, FloatingObjectPartsTreeNode node) {

        String newValue = (String) node.getValueAt(2);
        treeTableModel.setValueAt(newValue, node, 1);

        for (FloatingObjectPartsTreeNode childNode : node) {
            move(treeTableModel, childNode);
        }

    }
}
