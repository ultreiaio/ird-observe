package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.dcp.FloatingObjectBuoyPreset;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/**
 * Created on 10/07/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
@GenerateJavaBeanDefinition
public class FloatingObjectBuoyPresetUIModel extends AbstractJavaBean {

    private final FloatingObjectBuoyPreset bean = new FloatingObjectBuoyPreset();
    private boolean keepCode = true;
    private boolean keepComment = true;
    private boolean keepTransmittingBuoyOwnershipId = true;
    private boolean keepTransmittingBuoyTypeId = true;
    private boolean keepTransmittingBuoyOperationId = true;
    private boolean keepCountryId = true;
    private boolean keepVesselId = true;

    public FloatingObjectBuoyPreset getBean() {
        return bean;
    }

    public boolean isKeepCode() {
        return keepCode;
    }

    public void setKeepCode(boolean keepCode) {
        boolean oldValue = isKeepCode();
        this.keepCode = keepCode;
        firePropertyChange("keepCode", oldValue, keepCode);
    }

    public boolean isKeepComment() {
        return keepComment;
    }

    public void setKeepComment(boolean keepComment) {
        boolean oldValue = isKeepComment();
        this.keepComment = keepComment;
        firePropertyChange("keepComment", oldValue, keepComment);
    }

    public boolean isKeepTransmittingBuoyOwnershipId() {
        return keepTransmittingBuoyOwnershipId;
    }

    public void setKeepTransmittingBuoyOwnershipId(boolean keepTransmittingBuoyOwnershipId) {
        boolean oldValue = isKeepTransmittingBuoyOwnershipId();
        this.keepTransmittingBuoyOwnershipId = keepTransmittingBuoyOwnershipId;
        firePropertyChange("keepTransmittingBuoyOwnershipId", oldValue, keepTransmittingBuoyOwnershipId);
    }

    public boolean isKeepTransmittingBuoyTypeId() {
        return keepTransmittingBuoyTypeId;
    }

    public void setKeepTransmittingBuoyTypeId(boolean keepTransmittingBuoyTypeId) {
        boolean oldValue = isKeepTransmittingBuoyTypeId();
        this.keepTransmittingBuoyTypeId = keepTransmittingBuoyTypeId;
        firePropertyChange("keepTransmittingBuoyTypeId", oldValue, keepTransmittingBuoyTypeId);
    }

    public boolean isKeepTransmittingBuoyOperationId() {
        return keepTransmittingBuoyOperationId;
    }

    public void setKeepTransmittingBuoyOperationId(boolean keepTransmittingBuoyOperationId) {
        boolean oldValue = isKeepTransmittingBuoyOperationId();
        this.keepTransmittingBuoyOperationId = keepTransmittingBuoyOperationId;
        firePropertyChange("keepTransmittingBuoyOperationId", oldValue, keepTransmittingBuoyOperationId);
    }

    public boolean isKeepCountryId() {
        return keepCountryId;
    }

    public void setKeepCountryId(boolean keepCountryId) {
        boolean oldValue = isKeepCountryId();
        this.keepCountryId = keepCountryId;
        firePropertyChange("keepCountryId", oldValue, keepCountryId);
    }

    public boolean isKeepVesselId() {
        return keepVesselId;
    }

    public void setKeepVesselId(boolean keepVesselId) {
        boolean oldValue = isKeepVesselId();
        this.keepVesselId = keepVesselId;
        firePropertyChange("keepVesselId", oldValue, keepVesselId);
    }

    public FloatingObjectBuoyPreset toBean() {
        FloatingObjectBuoyPreset result = new FloatingObjectBuoyPreset();
        bean.copy(result);
        if (!keepComment) {
            result.setComment(null);
        }
        if (!keepCode) {
            result.setCode(null);
        }
        if (!keepCountryId) {
            result.setCountryId(null);
        }
        if (!keepTransmittingBuoyOperationId) {
            result.setTransmittingBuoyOperationId(null);
        }
        if (!keepTransmittingBuoyOwnershipId) {
            result.setTransmittingBuoyOwnershipId(null);
        }
        if (!keepTransmittingBuoyTypeId) {
            result.setTransmittingBuoyTypeId(null);
        }
        if (!keepVesselId) {
            result.setVesselId(null);
        }
        return result;
    }
}
