/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

JLabel {
  i18nProperty:"";
}

#generalPanel {
  border:{new TitledBorder(t("observe.Common.generalTab") + "      ")};
}

#materialsPanel {
  border:{new TitledBorder(t("observe.data.ps.observation.FloatingObject.materialsTab") + "      ")};
}

#keepObjectOperation {
  selected:true;
  enabled:false;
  _skipBindingFromBean:true;
  _skipBindingToBean:true;
}

#objectOperation {
  _type:{ObjectOperationReference.class};
}

#supplyNameLabel {
  i18nProperty:supportVesselName;
}

#supplyName {
  enabled:{model.isKeepSupplyName()};
  text:{getStringValue(bean.getSupplyName())};
}

#label1 {
  text:{bean.getLabel1()};
}

#label2 {
  text:{bean.getLabel2()};
}

#label3 {
  text:{bean.getLabel3()};
}

#label4 {
  text:{bean.getLabel4()};
}

#label5 {
  text:{bean.getLabel5()};
}

#label6 {
  text:{bean.getLabel6()};
}

#label7 {
  text:{bean.getLabel7()};
}

#label8 {
  text:{bean.getLabel8()};
}

#buoy1 {
  border:{new TitledBorder(t("observe.Common.first.buoy") + "      ")};
}

#buoy2 {
  border:{new TitledBorder(t("observe.Common.second.buoy") + "      ")};
}

#noBuoyEditor {
  horizontalAlignment:"center";
  text:"observe.Common.noBuoy";
}

#errorTable {
  model:{model.getMessageTableModel()};
}

#formView {
  title: "observe.ui.view.form";
}

#messageView {
  title: "observe.ui.view.message";
  minimumSize: {new Dimension(200, 50)};
    maximumSize: {new Dimension(200, 100)};
}
