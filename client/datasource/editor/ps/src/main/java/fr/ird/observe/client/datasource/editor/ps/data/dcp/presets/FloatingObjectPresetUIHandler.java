package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.DisabledItemSelectionModel;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectBuoyPreset;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialReference;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 06/07/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class FloatingObjectPresetUIHandler implements UIHandler<FloatingObjectPresetUI> {

    @Override
    public void afterInit(FloatingObjectPresetUI ui) {
        FloatingObjectBuoyPresetUI buoy1 = ui.getBuoy1();
        buoy1.getValidator().setParentValidator(ui.getValidator());
        FloatingObjectBuoyPresetUI buoy2 = ui.getBuoy2();
        buoy2.getValidator().setParentValidator(ui.getValidator());

        ui.getMaterialsList().setFixedCellHeight(20);
        ui.getMaterialsList().setSelectionModel(new DisabledItemSelectionModel());

        FloatingObjectPreset floatingObjectPreset = ui.getContextValue(FloatingObjectPreset.class);
        FloatingObjectPresetUIModel model = ui.getModel();
        FloatingObjectPreset bean = model.getBean();
        floatingObjectPreset.copy(bean);

        FloatingObjectPresetUIInitializer<FloatingObjectPresetUI> initializer = new FloatingObjectPresetUIInitializer<>(ui);
        initializer.initUI();
        initializer.disabledIfNull(model, bean, "keepSupplyName");
        ui.getBuoys().removeAll();
        ui.setValidatorBean(bean);
        ui.getBuoys().add(ui.getNoBuoyEditor());

        Optional.ofNullable(floatingObjectPreset.getBuoy1()).ifPresent(t -> prepareBuoy(t, ui, buoy1));
        Optional.ofNullable(floatingObjectPreset.getBuoy2()).ifPresent(t -> prepareBuoy(t, ui, buoy2));

        DefaultListModel<String> materialsListModel = new DefaultListModel<>();
        List<String> materialsList = buildMaterialsList(initializer, ui.getBean());
        materialsList.forEach(materialsListModel::addElement);
        ui.getMaterialsList().setModel(materialsListModel);
        ui.getMaterialsList().setRequestFocusEnabled(false);
        ui.getMaterialsList().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);


    }

    public static List<String> buildMaterialsList(FloatingObjectPresetUIInitializer<?> initializer, FloatingObjectPreset model) {
        List<String> materials = new LinkedList<>();
        Map<String, String> whenArrivingMaterials = model.getWhenArrivingMaterials();
        if (whenArrivingMaterials == null) {
            whenArrivingMaterials = Map.of();
        }
        Map<String, String> whenLeavingMaterials = model.getWhenLeavingMaterials();
        if (whenLeavingMaterials == null) {
            whenLeavingMaterials = Map.of();
        }
        Set<String> ids = new LinkedHashSet<>(whenArrivingMaterials.keySet());
        ids.addAll(whenLeavingMaterials.keySet());
        for (String id : ids) {
            String label = initializer.decorate(ObjectMaterialReference.class, id);
            String values = "";
            String whenArrivingValue = whenArrivingMaterials.get(id);
            if (whenArrivingValue != null) {
                if ("true".equals(whenArrivingValue)) {
                    whenArrivingValue = t("boolean.true");
                }
                values += t("observe.Common.whenArriving") + " - " + whenArrivingValue;
            }
            String whenLeavingValue = whenLeavingMaterials.get(id);
            if (whenLeavingValue != null) {
                if (!values.isEmpty()) {
                    values += " - ";
                }
                if ("true".equals(whenLeavingValue)) {
                    whenLeavingValue = t("boolean.true");
                }
                values += t("observe.Common.whenLeaving") + " - " + whenLeavingValue;
            }
            materials.add(label + " ( " + values + " )");
        }
        return materials;
    }

    private void prepareBuoy(FloatingObjectBuoyPreset preset, FloatingObjectPresetUI ui, FloatingObjectBuoyPresetUI buoyUI) {
        FloatingObjectBuoyPreset bean = buoyUI.getBean();
        FloatingObjectBuoyPresetUIModel model = buoyUI.getModel();
        preset.copy(bean);
        ui.getBuoys().remove(ui.getNoBuoyEditor());
        ui.getBuoys().add(buoyUI);
        buoyUI.setValidatorBean(model.getBean());

        FloatingObjectPresetUIInitializer<FloatingObjectBuoyPresetUI> initializer = new FloatingObjectPresetUIInitializer<>(buoyUI);
        initializer.initUI();

        initializer.disabledIfNull(model, bean, "keepVesselId");
        initializer.disabledIfNull(model, bean, "keepCountryId");
        initializer.disabledIfNull(model, bean, "keepCode");
        initializer.disabledIfNull(model, bean, "keepComment");
        initializer.disabledIfNull(model, bean, "keepTransmittingBuoyOwnershipId");
        initializer.disabledIfNull(model, bean, "keepTransmittingBuoyTypeId");
        initializer.disabledIfNull(model, bean, "keepVesselId");
    }

}
