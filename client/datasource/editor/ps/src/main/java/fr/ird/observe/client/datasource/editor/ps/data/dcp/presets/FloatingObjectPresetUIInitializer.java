package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.validation.ObserveSwingValidator;
import fr.ird.observe.client.util.BeanLabel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializer;
import fr.ird.observe.client.util.init.DefaultUIInitializerContext;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox;
import io.ultreia.java4all.lang.Strings;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.validator.JAXXValidator;
import org.nuiton.jaxx.widgets.text.NormalTextEditor;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import java.awt.Component;
import java.beans.Introspector;
import java.util.Objects;

/**
 * Created on 13/07/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class FloatingObjectPresetUIInitializer<UI extends JComponent & JAXXObject & JAXXValidator> extends DefaultUIInitializer<UI> {

    public static final Class<?>[] MANAGED_TYPES = new Class<?>[]{
            BeanCheckBox.class, NormalTextEditor.class, ObserveSwingValidator.class, BeanLabel.class, JLabel.class
    };

    protected final DecoratorService decoratorService;
    private final ObserveSwingDataSource cache;

    public FloatingObjectPresetUIInitializer(UI ui) {
        super(ui, MANAGED_TYPES);
        this.decoratorService = Objects.requireNonNull(ui.getContextValue(DecoratorService.class));
        this.cache = ui.getContextValue(ObserveSwingDataSource.class);
    }

    @Override
    protected void initUI(DefaultUIInitializerContext<UI> initializerContext) {
        initializerContext
                .startFirstPass()
                .onComponents(BeanCheckBox.class, this::init)
                .onComponents(NormalTextEditor.class, this::init)
                .onComponents(ObserveSwingValidator.class, this::init)
                .onComponents(BeanLabel.class, this::init)
                .onComponents(JLabel.class, this::init)
                .onComponents(JTable.class, this::init)
                .startSecondPass();
    }


    protected void init(NormalTextEditor editor) {
        initializerContext.checkFirstPass();
        editor.init();
    }

    protected void init(JTable editor) {
        initializerContext.checkFirstPass();
        UIInitHelper.initErrorTable(editor);
        UIHelper.fixTableColumnWidth(editor, 0, 45);
        UIHelper.fixTableColumnWidth(editor, 1, 100);
    }

    private void init(JLabel editor) {
        String editorName = Strings.removeEnd(editor.getName(), "Label");
        Object objectById = ui.getObjectById(editorName);
        if (objectById instanceof JComponent) {
            editor.setLabelFor((Component) objectById);
        }
    }

    private void init(BeanLabel editor) {
        String name = editor.getName();
        JavaBean bean = (JavaBean) editor.getBean();
        @SuppressWarnings("unchecked") Class<? extends ReferentialDtoReference> type = (Class<? extends ReferentialDtoReference>) editor.getClientProperty("type");
        bean.addPropertyChangeListener(name+"Id", evt-> {
            String newValue = (String) evt.getNewValue();
            String text = newValue == null ? "" : decorate(type, newValue);
            editor.setText(text);
        });
        String id = bean.get(name + "Id");
        String text = id == null ? "" : decorate(type, id);
        editor.setText(text);
    }

    protected void init(ObserveSwingValidator<?> validator) {
        ui.listenValidatorContextNameAndRefreshFields(validator);
    }

    public <R extends ReferentialDtoReference> String decorate(Class<R> type, String id) {
        if (id == null) {
            return "";
        }
        if (cache == null) {
            return id;
        }
        R r = cache.getReferentialReferenceSet(type).tryGetReferenceById(id).orElseThrow(() -> new IllegalStateException(String.format("Could not find referential on type: %s with id: %s", type, id)));
        decoratorService.installDecorator(r);
        return r.toString();
    }

    public void disabledIfNull(JavaBean model, JavaBean bean, String uiProperty) {
        String beanProperty = Introspector.decapitalize(uiProperty.substring(4));
        Object value = bean.get(beanProperty);
        if (value == null || value.toString().trim().isEmpty()) {
            model.set(uiProperty, false);
            BeanCheckBox editor = (BeanCheckBox) ui.getObjectById(uiProperty);
            editor.setSelected(false);
            editor.setEnabled(false);
        }
    }
}

