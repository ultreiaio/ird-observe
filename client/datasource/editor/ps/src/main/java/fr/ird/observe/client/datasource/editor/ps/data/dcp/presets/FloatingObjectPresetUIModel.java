package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.validation.ContentMessageTableModel;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;

/**
 * Created on 10/07/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
@GenerateJavaBeanDefinition
public class FloatingObjectPresetUIModel extends AbstractJavaBean {

    private final ContentMessageTableModel messageTableModel = new ContentMessageTableModel();
    private final FloatingObjectPreset bean = new FloatingObjectPreset();
    private boolean keepSupplyName = true;

    public static FloatingObjectPresetUI init(ApplicationContext applicationContext, FloatingObjectPreset model) {
        return new FloatingObjectPresetUI(new JAXXInitialContext().add(applicationContext).add(model));
    }

    public ContentMessageTableModel getMessageTableModel() {
        return messageTableModel;
    }

    public FloatingObjectPreset getBean() {
        return bean;
    }

    public boolean isKeepSupplyName() {
        return keepSupplyName;
    }

    public void setKeepSupplyName(boolean keepSupplyName) {
        boolean oldValue = isKeepSupplyName();
        this.keepSupplyName = keepSupplyName;
        firePropertyChange("keepSupplyName", oldValue, keepSupplyName);
    }

    public FloatingObjectPreset toBean() {
        FloatingObjectPreset result = new FloatingObjectPreset();
        bean.copy(result);
        if (!keepSupplyName) {
            result.setSupplyName(null);
        }
        return result;
    }
}

