package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIOpenExecutor;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectBuoyPreset;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import io.ultreia.java4all.decoration.Decorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.Table;
import org.nuiton.jaxx.runtime.swing.renderer.DecoratorListCellRenderer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created at 20/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class FloatingObjectPresetsEditorUIHandler implements UIHandler<FloatingObjectPresetsEditorUI>, WithClientUIContextApi {
    private static final Logger log = LogManager.getLogger(FloatingObjectPresetsEditorUIHandler.class);
    private FloatingObjectPresetUIInitializer<FloatingObjectPresetsEditorUI> initializer;

    @Override
    public void beforeInit(FloatingObjectPresetsEditorUI ui) {
        initializer = new FloatingObjectPresetUIInitializer<>(ui);
        FloatingObjectPresetsEditorUIModel model = ui.getContextValue(FloatingObjectPresetsEditorUIModel.class);
        model.setInitializer(initializer);
    }

    @Override
    public void afterInit(FloatingObjectPresetsEditorUI ui) {
        initializer.initUI();
        FloatingObjectPresetsEditorUIModel model = ui.getModel();

        JList<FloatingObjectPreset> list = ui.getList();

        Decorator remoteDecorator = getDecoratorService().getDecoratorByType(FloatingObjectPreset.class);
        list.setFont(list.getFont().deriveFont(14f));
        list.setSelectionBackground(Color.LIGHT_GRAY);
        list.setSelectionForeground(Color.BLACK);
        list.setCellRenderer(new DecoratorListCellRenderer<>(remoteDecorator) {

            @Override
            protected Object decorateValue(Object value, int index) {
                String result = (String) super.decorateValue(value, index);
                FloatingObjectPreset v = (FloatingObjectPreset) value;
                String filenamePrefix = v.getFilenamePrefix();
                if (filenamePrefix.startsWith("0")) {
                    filenamePrefix = filenamePrefix.substring(1);
                }
                return String.format("<html><body><b>F%s</b> - %s</body></html>", filenamePrefix, result);
            }
        });

        FloatingObjectBuoyPresetUI buoy1 = ui.getBuoy1();
        FloatingObjectBuoyPresetUI buoy2 = ui.getBuoy2();

        initBuoy(ui, buoy1);
        initBuoy(ui, buoy2);

        JScrollPane scrollPane = ui.getMessageScrollPane();
        scrollPane.setPreferredSize(new Dimension(scrollPane.getPreferredSize().width, ui.getErrorTable().getRowHeight() * 6));
        ContentReferentialUIOpenExecutor.setMainI18nLanguage(ui, getClientConfig().getReferentialLocale());
        model.addPropertyChangeListener(FloatingObjectPresetsEditorUIModel.PROPERTY_SELECTED_PRESET, evt -> updateSelectedPreset(ui, (FloatingObjectPreset) evt.getNewValue()));
        model.addPropertyChangeListener(FloatingObjectPresetsEditorUIModel.PROPERTY_MODIFIED_ALL, evt -> updateModified(ui));
        model.addPropertyChangeListener(FloatingObjectPresetsEditorUIModel.PROPERTY_SIZE, evt -> updateSize(ui, (int) evt.getNewValue()));
        updateSize(ui, model.getSize());
        updateSelectedPreset(ui, list.getSelectedValue());
    }

    private void updateModified(FloatingObjectPresetsEditorUI ui) {
        List<String> monitorPropertyNames = ui.getModel().getMonitorPropertyNames();
        Set<String> modifiedProperties = ui.getModel().getModifiedProperties();
        for (String propertyName : monitorPropertyNames) {
            JLabel label = (JLabel) ui.getObjectById(propertyName + "Label");
            boolean modified = modifiedProperties.contains(propertyName);
            label.setForeground(modified ? Color.BLUE : Color.BLACK);
        }
    }


    private void updateSize(FloatingObjectPresetsEditorUI ui, int size) {
        CardLayout layout = (CardLayout) ui.getLayout();
        if (size == 0) {
            layout.show(ui, "noContent");
        } else {
            layout.show(ui, "withContent");
            ui.getList().setSelectedIndex(0);
        }
    }

    private void updateSelectedPreset(FloatingObjectPresetsEditorUI ui, FloatingObjectPreset selectedPreset) {
        log.info("Will display selected preset {}", selectedPreset == null ? null : selectedPreset.getLabel1());
        CardLayout layout = (CardLayout) ui.getContentPanel().getLayout();
        JButton delete = ui.getDelete();
        JButton goUp = ui.getGoUp();
        JButton goDown = ui.getGoDown();
        delete.setEnabled(false);
        goUp.setEnabled(false);
        goDown.setEnabled(false);

        if (selectedPreset == null || ui.getModel().isEmpty()) {
            layout.show(ui.getContentPanel(), "empty");
        } else {

            layout.show(ui.getContentPanel(), "form");

            ui.setValidatorBean(ui.getModel().getBean());
            int selectedIndex = ui.getList().getSelectedIndex();
            int size = ui.getModel().getListModel().size();
            delete.setEnabled(true);

            goUp.setEnabled(selectedIndex > 0);
            goDown.setEnabled(selectedIndex < size - 1);

            ui.getBuoys().removeAll();
            ui.getBuoys().add(ui.getNoBuoyEditor());

            FloatingObjectBuoyPresetUI buoy1 = ui.getBuoy1();
            FloatingObjectBuoyPresetUI buoy2 = ui.getBuoy2();

            Optional.ofNullable(selectedPreset.getBuoy1()).ifPresent(t -> prepareBuoy(t, ui, buoy1));
            Optional.ofNullable(selectedPreset.getBuoy2()).ifPresent(t -> prepareBuoy(t, ui, buoy2));

        }
        SwingUtilities.invokeLater(() -> ui.getParentContainer(FloatingObjectPresetsUI.class).repaint());
    }

    private void initBuoy(FloatingObjectPresetsEditorUI ui, FloatingObjectBuoyPresetUI buoyUI) {
        Table $Table0 = buoyUI.getContent();
        new FloatingObjectPresetUIInitializer<>(buoyUI).initUI();
        $Table0.removeAll();
        $Table0.add(buoyUI.transmittingBuoyOperationLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, 10, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(SwingUtil.boxComponentWithJxLayer(buoyUI.transmittingBuoyOperation), new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(buoyUI.transmittingBuoyTypeLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, 17, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(SwingUtil.boxComponentWithJxLayer(buoyUI.transmittingBuoyType), new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(buoyUI.transmittingBuoyOwnershipLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, 17, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(SwingUtil.boxComponentWithJxLayer(buoyUI.transmittingBuoyOwnership), new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(buoyUI.countryLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, 17, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(buoyUI.country, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(buoyUI.vesselLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, 17, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(buoyUI.vessel, new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(buoyUI.codeLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, 17, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(SwingUtil.boxComponentWithJxLayer(buoyUI.code), new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(buoyUI.commentLabel, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, 17, 1, new Insets(3, 3, 3, 3), 0, 0));
        $Table0.add(SwingUtil.boxComponentWithJxLayer(buoyUI.comment), new GridBagConstraints(1, 6, 1, 1, 1.0, 0.0, 13, 1, new Insets(3, 3, 3, 3), 0, 0));
    }

    private void prepareBuoy(FloatingObjectBuoyPreset preset, FloatingObjectPresetsEditorUI ui, FloatingObjectBuoyPresetUI buoyUI) {
        FloatingObjectBuoyPreset bean = buoyUI.getBean();
        FloatingObjectBuoyPresetUIModel model = buoyUI.getModel();
        preset.copy(bean);
        ui.getBuoys().remove(ui.getNoBuoyEditor());
        ui.getBuoys().add(buoyUI);
        buoyUI.setValidatorBean(model.getBean());
    }

}
