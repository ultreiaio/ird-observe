package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.validation.ContentMessageTableModel;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetStorage;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;
import fr.ird.observe.dto.referential.ReferentialLocale;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.DefaultListModel;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created at 20/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
@GenerateJavaBeanDefinition
public class FloatingObjectPresetsEditorUIModel extends AbstractJavaBean {

    public static final String PROPERTY_SELECTED_PRESET = "selectedPreset";
    public static final String PROPERTY_EMPTY = "empty";
    public static final String PROPERTY_MODIFIED = "modified";
    public static final String PROPERTY_MODIFIED_ALL = "modifiedAll";
    public static final String PROPERTY_SIZE = "size";
    private static final Logger log = LogManager.getLogger(FloatingObjectPresetsEditorUIModel.class);
    private final FloatingObjectPresetsStorage presetsStorage;
    private final ReferentialLocale referentialLocale;
    private final ObserveSwingDataSource dataSource;
    private final DefaultListModel<FloatingObjectPreset> listModel;
    private final ContentMessageTableModel messageTableModel = new ContentMessageTableModel();
    private final DefaultListModel<String> materialsListModel = new DefaultListModel<>();
    private final FloatingObjectPreset bean = new FloatingObjectPreset();
    private final Monitor monitor;
    private final PropertyChangeListener listenBean;
    private FloatingObjectPreset selectedPreset;
    private boolean modified;
    private FloatingObjectPresetUIInitializer<FloatingObjectPresetsEditorUI> initializer;

    public FloatingObjectPresetsEditorUIModel(FloatingObjectPresetsStorage presetsStorage, ReferentialLocale referentialLocale, ObserveSwingDataSource dataSource) {
        this.presetsStorage = Objects.requireNonNull(presetsStorage);
        this.referentialLocale = Objects.requireNonNull(referentialLocale);
        this.dataSource = dataSource;
        this.listModel = new DefaultListModel<>();
        this.monitor = new Monitor(FloatingObjectPreset.PROPERTY_LABEL1,
                                   FloatingObjectPreset.PROPERTY_LABEL2,
                                   FloatingObjectPreset.PROPERTY_LABEL3,
                                   FloatingObjectPreset.PROPERTY_LABEL4,
                                   FloatingObjectPreset.PROPERTY_LABEL5,
                                   FloatingObjectPreset.PROPERTY_LABEL6,
                                   FloatingObjectPreset.PROPERTY_LABEL7,
                                   FloatingObjectPreset.PROPERTY_LABEL8);
        this.listenBean = evt -> {
            if (getMonitorPropertyNames().contains(evt.getPropertyName())) {
                setModified(monitor.wasModified());
            }
        };
        updateListModel();
    }

    public ObserveSwingDataSource getDataSource() {
        return dataSource;
    }

    public boolean isWithDataSource() {
        return getDataSource() != null;
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public List<String> getMonitorPropertyNames() {
        return monitor.getPropertyNames();
    }

    public Set<String> getModifiedProperties() {
        return monitor.getModifiedProperties();
    }

    public DefaultListModel<String> getMaterialsListModel() {
        return materialsListModel;
    }

    public void updateListModel() {
        listModel.clear();
        presetsStorage.getPresets().forEach(listModel::addElement);
        firePropertyChange(PROPERTY_EMPTY, isEmpty());
        firePropertyChange(PROPERTY_SIZE, getSize());
    }

    public DefaultListModel<FloatingObjectPreset> getListModel() {
        return listModel;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        boolean oldValue = isModified();
        this.modified = modified;
        firePropertyChange(PROPERTY_MODIFIED, oldValue, modified);
        firePropertyChange(PROPERTY_MODIFIED_ALL, modified);
    }

    public int getSize() {
        return listModel.getSize();
    }

    public boolean isEmpty() {
        return listModel.isEmpty();
    }

    public FloatingObjectPresetsStorage getPresetsStorage() {
        return presetsStorage;
    }

    public List<FloatingObjectPreset> getPresets() {
        return new LinkedList<>(presetsStorage.getPresets());
    }

    public FloatingObjectPreset getSelectedPreset() {
        return selectedPreset;
    }

    public void setSelectedPreset(FloatingObjectPreset selectedPreset) {
        FloatingObjectPreset oldValue = getSelectedPreset();
        this.selectedPreset = selectedPreset;
        bean.removePropertyChangeListener(listenBean);
        if (selectedPreset == null) {
            monitor.setBean(null);
            bean.clear();
        } else {
            selectedPreset.copy(bean);
        }
        monitor.setBean(bean);
        if (selectedPreset != null) {
            bean.addPropertyChangeListener(listenBean);
        }
        materialsListModel.clear();
        List<String> materialsList = FloatingObjectPresetUIHandler.buildMaterialsList(initializer, bean);
        materialsList.forEach(materialsListModel::addElement);
        firePropertyChange(PROPERTY_SELECTED_PRESET, oldValue, selectedPreset);
        setModified(false);
    }

    public FloatingObjectPreset getBean() {
        return bean;
    }

    public ContentMessageTableModel getMessageTableModel() {
        return messageTableModel;
    }

    public void saveListModel() {
        Enumeration<FloatingObjectPreset> enumeration = getListModel().elements();
        int order = 0;
        Path directory = presetsStorage.getDirectory();
        Map<Path, FloatingObjectPreset> result = new LinkedHashMap<>();
        while (enumeration.hasMoreElements()) {
            FloatingObjectPreset nextElement = enumeration.nextElement();
            String fileName = nextElement.getFileName();
            String suffix = nextElement.getFilenameSuffix();
            String nextFilename = FloatingObjectPresetStorage.getNextFilename(++order, suffix);
            nextElement.setFileName(nextFilename);
            result.put(directory.resolve(nextFilename), nextElement);
            if (!Objects.equals(fileName, nextFilename)) {
                log.info("Change preset {} from filename {} to {}", nextElement.getLabel(referentialLocale), fileName, nextFilename);
            }
        }
        try {
            presetsStorage.store(result);
        } catch (IOException e) {
            throw new RuntimeException("Could not save presets", e);
        }
    }

    public void cancelEdit() {
        setSelectedPreset(selectedPreset);
    }

    public void saveEdit() {
        monitor.apply(selectedPreset, true);
        try {
            Path path = presetsStorage.getDirectory().resolve(selectedPreset.getFileName());
            log.info("Save preset {} to {}", selectedPreset.getLabel(referentialLocale), path);
            FloatingObjectPresetStorage.store(selectedPreset, path);
        } catch (IOException e) {
            throw new RuntimeException("Could not save preset", e);
        }
        setSelectedPreset(selectedPreset);
    }

    public void delete() {
        listModel.removeElement(selectedPreset);
        setSelectedPreset(null);
        saveListModel();
        updateListModel();
    }

    public void reset() {
        setSelectedPreset(null);
        presetsStorage.reset();
        updateListModel();
    }

    public void setInitializer(FloatingObjectPresetUIInitializer<FloatingObjectPresetsEditorUI> initializer) {
        this.initializer = initializer;
    }

    static class Monitor extends JavaBeanMonitor {
        public Monitor(String... propertyNames) {
            super(propertyNames);
        }

        List<String> getPropertyNames() {
            return this.propertyNames;
        }
    }
}
