package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.ObserveSwingApplicationContext;
import fr.ird.observe.client.datasource.api.ObserveDataSourcesManager;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.actions.ShowFloatingObjectPresetsUI;
import fr.ird.observe.client.main.ObserveMainUI;
import fr.ird.observe.client.main.body.MainUIBodyContent;
import io.ultreia.java4all.util.SingletonSupplier;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;

import javax.swing.JMenuItem;
import java.util.function.Supplier;

/**
 * Created at 20/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
@SuppressWarnings("rawtypes")
@AutoService(MainUIBodyContent.class)
public class FloatingObjectPresetsUIBodyContent extends MainUIBodyContent<FloatingObjectPresetsUI> {

    public FloatingObjectPresetsUIBodyContent() {
        super(6, FloatingObjectPresetsUI.class);
        setSupplier(SingletonSupplier.of(createSupplier()));
    }

    private Supplier<FloatingObjectPresetsUI> createSupplier() {
        return () -> {
            ObserveSwingApplicationContext applicationContext = (ObserveSwingApplicationContext) ObserveSwingApplicationContext.get();
            JAXXInitialContext initialContext = new JAXXInitialContext().add(applicationContext.getMainUI())
                                                                        .add(applicationContext.getDecoratorService())
                                                                        .add(applicationContext.getFloatingObjectPresetsManager());
            ObserveSwingDataSource mainDataSource = applicationContext.getDataSourcesManager().getMainDataSource();
            if (mainDataSource != null) {
                initialContext.add(mainDataSource);
            }
            return new FloatingObjectPresetsUI(initialContext);
        };
    }

    @Override
    public void install(ObserveMainUI mainUI) {
        super.install(mainUI);
        ObserveSwingApplicationContext applicationContext = (ObserveSwingApplicationContext) ObserveSwingApplicationContext.get();
        ObserveDataSourcesManager dataSourcesManager = applicationContext.getDataSourcesManager();
        JMenuItem editor = mainUI.getShowFloatingObjectPresets();
        dataSourcesManager.addPropertyChangeListener(ObserveDataSourcesManager.PROPERTY_MAIN_DATA_SOURCE, evt -> {
            ObserveSwingDataSource newValue = (ObserveSwingDataSource) evt.getNewValue();
            editor.setEnabled(newValue != null);
        });
        ShowFloatingObjectPresetsUI.init(mainUI, editor, new ShowFloatingObjectPresetsUI());
    }
}
