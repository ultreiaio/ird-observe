package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.SwingUtilities;

/**
 * Created at 20/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class FloatingObjectPresetsUIHandler implements UIHandler<FloatingObjectPresetsUI> {
    @Override
    public void beforeInit(FloatingObjectPresetsUI ui) {
        FloatingObjectPresetsStorage floatingObjectPresetsManager = ui.getContextValue(FloatingObjectPresetsStorage.class);
        DecoratorService decoratorService = ui.getContextValue(DecoratorService.class);
        ObserveSwingDataSource dataSource = ui.getContextValue(ObserveSwingDataSource.class);
        FloatingObjectPresetsUIModel model = new FloatingObjectPresetsUIModel(floatingObjectPresetsManager, decoratorService.getReferentialLocale(), dataSource);
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(FloatingObjectPresetsUI ui) {
        SwingUtilities.invokeLater(ui.getEditor().getList()::requestFocusInWindow);
    }
}
