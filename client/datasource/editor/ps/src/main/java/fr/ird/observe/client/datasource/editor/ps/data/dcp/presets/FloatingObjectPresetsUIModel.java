package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPresetsStorage;
import fr.ird.observe.dto.referential.ReferentialLocale;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Objects;

/**
 * Created at 20/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
@GenerateJavaBeanDefinition
public class FloatingObjectPresetsUIModel extends AbstractJavaBean {

    private final FloatingObjectPresetsStorage manager;
    private final ObserveSwingDataSource dataSource;
    private final FloatingObjectPresetsEditorUIModel presets;

    public FloatingObjectPresetsUIModel(FloatingObjectPresetsStorage manager, ReferentialLocale referentialLocale, ObserveSwingDataSource dataSource) {
        this.manager = Objects.requireNonNull(manager);
        this.dataSource = dataSource;
        this.presets = new FloatingObjectPresetsEditorUIModel(manager, referentialLocale, dataSource);
    }

    public FloatingObjectPresetsEditorUIModel getPresets() {
        return presets;
    }

    public FloatingObjectPresetsStorage getManager() {
        return manager;
    }

    public ObserveSwingDataSource getDataSource() {
        return dataSource;
    }

    public void reset() {
        presets.reset();
    }
}
