package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.ps.data.dcp.presets.FloatingObjectPresetsEditorUI;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.presets.FloatingObjectPresetsEditorUIModel;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import io.ultreia.java4all.decoration.Decorator;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created at 20/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class Delete extends FloatingObjectPresetsEditorUIActionSupport {

    public Delete() {
        super("", n("observe.ui.action.delete"), "delete", ObserveKeyStrokesSupport.KEY_STROKE_DELETE_DATA_GLOBAL);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, FloatingObjectPresetsEditorUI ui) {

        Decorator decorator = getDecoratorService().getDecoratorByType(FloatingObjectPreset.class);
        FloatingObjectPresetsEditorUIModel model = ui.getModel();
        FloatingObjectPreset configuration = model.getSelectedPreset();
        int response = askToUser(t("observe.data.ps.dcp.FloatingObjectPreset.delete.title"),
                                 t("observe.data.ps.dcp.FloatingObjectPreset.delete.message", decorator.decorate(configuration)),
                                 JOptionPane.QUESTION_MESSAGE,
                                 new Object[]{t("observe.ui.action.delete"), t("observe.ui.action.cancel")},
                                 0);
        boolean delete = response == 0;

        if (delete) {
            model.delete();
            SwingUtilities.invokeLater(ui.getList()::requestFocusInWindow);;
        }
    }
}
