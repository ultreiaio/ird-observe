package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.presets.FloatingObjectPresetsEditorUI;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.presets.FloatingObjectPresetsEditorUIModel;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created at 20/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class GoDown extends FloatingObjectPresetsEditorUIActionSupport {
    private static final Logger log = LogManager.getLogger(GoDown.class);

    public GoDown() {
        super("", n("observe.Common.action.move.down.tip"), "move-down", ObserveKeyStrokesEditorApi.KEY_STROKE_MOVE_DOWN_TABLE_ENTRY);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, FloatingObjectPresetsEditorUI ui) {
        FloatingObjectPresetsEditorUIModel model = ui.getModel();
        FloatingObjectPreset selectedPreset = model.getSelectedPreset();
        DefaultListModel<FloatingObjectPreset> listModel = model.getListModel();
        int oldIndex = listModel.indexOf(selectedPreset);
        int newIndex = oldIndex + 1;
        log.info("Will move down {} from {} to {}", selectedPreset, oldIndex, newIndex);
        listModel.remove(oldIndex);
        listModel.add(newIndex, selectedPreset);
        model.saveListModel();
        JList<FloatingObjectPreset> list = ui.getList();
        list.setSelectedIndex(newIndex);
        SwingUtilities.invokeLater(list::requestFocusInWindow);;
    }
}
