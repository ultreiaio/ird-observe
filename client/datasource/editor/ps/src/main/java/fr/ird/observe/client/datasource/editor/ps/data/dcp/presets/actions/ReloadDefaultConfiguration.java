package fr.ird.observe.client.datasource.editor.ps.data.dcp.presets.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.presets.FloatingObjectPresetsUI;
import fr.ird.observe.client.util.ObserveKeyStrokesSupport;
import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.io.IOException;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created at 21/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class ReloadDefaultConfiguration extends JComponentActionSupport<FloatingObjectPresetsUI> implements WithClientUIContextApi {

    public ReloadDefaultConfiguration() {
        super("", t("observe.ui.action.reloadDefaultConfiguration.tip"), "application-reload", ObserveKeyStrokesSupport.KEY_STROKE_RELOAD_DEFAULT_CONFIGURATION);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, FloatingObjectPresetsUI ui) {
        int response = askToUser(t("observe.data.ps.dcp.FloatingObjectPreset.reloadDefaultConfiguration.title"),
                                 t("observe.data.ps.dcp.FloatingObjectPreset.reloadDefaultConfiguration.message"),
                                 JOptionPane.QUESTION_MESSAGE,
                                 new Object[]{t("observe.ui.action.apply"), t("observe.ui.action.cancel")},
                                 0);
        boolean delete = response == 0;

        if (delete) {
            try {
                getClientConfig().reloadEmbeddedDcpPresets();
            } catch (IOException e) {
                throw new RuntimeException("Could not reload default floating object presets", e);
            }
            ui.getModel().reset();
            SwingUtilities.invokeLater(ui::repaint);
        }

    }
}
