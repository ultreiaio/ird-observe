package fr.ird.observe.client.datasource.editor.ps.data.landing;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.ps.data.WeightCategoryHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.landing.LandingDto;
import fr.ird.observe.dto.referential.ps.common.WeightCategoryReference;
import fr.ird.observe.dto.referential.ps.landing.FateReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;

/**
 * Created on 17/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripLandingUIModelStates extends GeneratedTripLandingUIModelStates {

    final WeightCategoryHelper weightCategoryHelper;
    private final FateReference defaultFate;

    public TripLandingUIModelStates(GeneratedTripLandingUIModel model) {
        super(model);
        this.weightCategoryHelper = new WeightCategoryHelper(this);
        ObserveSwingDataSource dataSource = model.getSource().getContext().getMainDataSource();
        defaultFate = dataSource.getReferentialReferenceSet(FateReference.class).tryGetReferenceById(ProtectedIdsPs.PS_LANDING_LANDING_PART_DEFAULT_FATE_ID).orElseThrow(IllegalStateException::new);
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        super.onAfterInitAddReferentialFilters(clientConfig, observeSelectModel, servicesProvider, referenceCache);
        referenceCache.addReferentialFilter(LandingDto.PROPERTY_SPECIES, ReferencesFilterHelper.newPsSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListSeineLandingId()));
        referenceCache.addReferentialFilter(LandingDto.PROPERTY_WEIGHT_CATEGORY, ReferencesFilterHelper.newPredicateList(WeightCategoryReference::isLanding));
    }

    @Override
    public void init(ContentUI ui, DefaultUIInitializerResult initializerResult) {
        super.init(ui, initializerResult);
        TripLandingUI realUi = (TripLandingUI) ui;
        weightCategoryHelper.install(realUi.getWeightCategory());
    }

    @Override
    public void initDefault(LandingDto newTableBean) {
        super.initDefault(newTableBean);
        newTableBean.setFate(defaultFate);
    }

}
