package fr.ird.observe.client.datasource.editor.ps.data.localmarket;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.data.ps.localmarket.SampleDto;
import fr.ird.observe.dto.data.ps.localmarket.WellIdDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ps.common.SampleTypeReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.List;

@GenerateJavaBeanDefinition
public class SampleUIModelStates extends GeneratedSampleUIModelStates {

    private final WellTableModel wellTableModel;

    public SampleUIModelStates(GeneratedSampleUIModel model) {
        super(model);
        this.wellTableModel = new WellTableModel(this);
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        super.onAfterInitAddReferentialFilters(clientConfig, observeSelectModel, servicesProvider, referenceCache);
        referenceCache.addReferentialFilter(SampleDto.PROPERTY_SAMPLE_TYPE, ReferencesFilterHelper.newPredicateList(SampleTypeReference::isLocalMarket));
    }

    @Override
    protected void copyFormToBean(Form<SampleDto> form) {
        // clear well ids table
        getWellTableModel().clear();
        super.copyFormToBean(form);
        List<WellIdDto> wells = getBean().toWellIds();
        getWellTableModel().setData(wells);
    }

    @Override
    public SampleDto getBeanToSave() {
        SampleDto bean = getBean();
        // on keep valid well ids
        bean.setWell(SampleDto.toIds(getWellTableModel().getValidData()));
        bean.autoTrim();
        return bean;
    }

    public WellTableModel getWellTableModel() {
        return wellTableModel;
    }
}
