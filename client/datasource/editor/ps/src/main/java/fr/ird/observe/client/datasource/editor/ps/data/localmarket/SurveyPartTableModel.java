package fr.ird.observe.client.datasource.editor.ps.data.localmarket;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableTableModel;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.dto.data.ps.localmarket.SurveyPartDto;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxCellEditor;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class SurveyPartTableModel extends EditableTableModel<SurveyPartDto> {

    private static final long serialVersionUID = 1L;
    private final SurveyUIModelStates states;
    private FilterableComboBoxCellEditor speciesCellEditor;

    public SurveyPartTableModel(SurveyUIModelStates states) {
        super(true);
        this.states = states;
    }

    public void init(SurveyUI ui) {
        EditableTable<SurveyPartDto, SurveyPartTableModel> table = ui.getSurveyPartTable();
        table.setRowHeight(22);
        JXTableUtil.setI18nTableHeaderRenderer(table, SurveyPartDto.class, "species", "proportion");
        Decorator decorator = ui.getHandler().getDecoratorService().getDecoratorByType(SpeciesReference.class);

        JXTableUtil.initRenderers(table, JXTableUtil.newDecoratedRenderer(decorator), JXTableUtil.newEmptyNumberTableCellRenderer());

        speciesCellEditor = UIHelper.newCellEditor(SpeciesReference.class, decorator);
        JXTableUtil.initEditors(table, speciesCellEditor, NumberCellEditor.newIntegerColumnEditor());

        table.init(ui, ui.getValidatorSurveyPart());
        UIHelper.fixTableColumnWidth(table, 1, 150);
    }

    public void copyFormToBean() {
        setData(states.getBean().getSurveyPart());
        List<SpeciesReference> references = states.getReferenceCache().getReferentialReferences(SurveyPartDto.PROPERTY_SPECIES);
        @SuppressWarnings("unchecked") FilterableComboBox<SpeciesReference> component = (FilterableComboBox<SpeciesReference>) speciesCellEditor.getComponent();
        component.setData(references);
    }

    @Override
    public void onModifiedChanged(SwingValidator<?> validator, Boolean newValue) {
        super.onModifiedChanged(validator, newValue);
        if (newValue) {
            List<SurveyPartDto> notEmptyData = getNotEmptyData();
            states.getBean().setSurveyPart(notEmptyData);
        }
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt0(SurveyPartDto row, int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return row.getSpecies();
        } else if (columnIndex == 1) {
            return row.getProportion();
        } else {
            throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public boolean setValueAt0(SurveyPartDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue = getValueAt0(row, rowIndex, columnIndex);
        if (columnIndex == 0) {
            row.setSpecies((SpeciesReference) aValue);
        } else if (columnIndex == 1) {
            row.setProportion((Integer) aValue);
        } else {
            throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    @Override
    protected SurveyPartDto createNewRow() {
        return SurveyPartDto.newDto(new Date());
    }
}
//{
//
//private static final long serialVersionUID=1L;
//
//public SurveyPartTableModel(EditableContentUI<SurveyDto> parentUi,SurveyPartUI ui,SurveyPartUIModel model){
//        super(parentUi,ui,model);
//        }
//
//@Override
//protected void onBeforeResetRow(int row){
//        super.onBeforeResetRow(row);
//        //FIXME Should review flow it seems it break cache logic, tableEditBean still keep this even after changing row
//        getBean().setSurveyPart(getData());
//        }
//
//        }
