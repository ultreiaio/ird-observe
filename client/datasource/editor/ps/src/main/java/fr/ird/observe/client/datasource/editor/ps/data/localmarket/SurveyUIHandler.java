package fr.ird.observe.client.datasource.editor.ps.data.localmarket;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.client.datasource.editor.api.content.ContentMode;
import fr.ird.observe.client.util.UIHelper;

public class SurveyUIHandler extends GeneratedSurveyUIHandler {

    @Override
    public void onInit(SurveyUI ui) {
        super.onInit(ui);
        ui.getSurveyPartTableModel().init(ui);
    }

    @Override
    public void stopEditUI() {
        UIHelper.cancelEditing(ui.getSurveyPartTable());
        super.stopEditUI();
    }

    @Override
    public void onModeChanged(ContentMode newMode) {
        if (newMode == ContentMode.CREATE && ui.getMainTabbedPane().getSelectedIndex() > 1) {
            // go back to first tab
            ui.getMainTabbedPane().setSelectedIndex(0);
        }
    }

}
