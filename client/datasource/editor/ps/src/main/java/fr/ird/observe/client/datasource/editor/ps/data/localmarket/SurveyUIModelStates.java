package fr.ird.observe.client.datasource.editor.ps.data.localmarket;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.data.ps.localmarket.BatchReference;
import fr.ird.observe.dto.data.ps.localmarket.SurveyDto;
import fr.ird.observe.dto.data.ps.localmarket.SurveyPartDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class SurveyUIModelStates extends GeneratedSurveyUIModelStates {

    protected final SurveyPartTableModel surveyPartTableModel;

    public SurveyUIModelStates(GeneratedSurveyUIModel model) {
        super(model);
        this.surveyPartTableModel = new SurveyPartTableModel(this);
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        super.onAfterInitAddReferentialFilters(clientConfig, observeSelectModel, servicesProvider, referenceCache);
        referenceCache.addReferentialFilter(SurveyPartDto.PROPERTY_SPECIES, ReferencesFilterHelper.newPsSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListSeineLocalmarketId()));
    }

    @Override
    protected void loadReferentialCacheOnOpenForm(Form<SurveyDto> form) {
        getReferenceCache().setDataReferenceSet(SurveyDto.PROPERTY_BATCHES, DataDtoReferenceSet.of(BatchReference.class, form.getObject().getAvailableBatches(), null));
        super.loadReferentialCacheOnOpenForm(form);
        getReferenceCache().loadExtraReferentialReferenceSetsInModel(SurveyPartDto.class);
    }

    @Override
    protected void copyFormToBean(Form<SurveyDto> form) {
        super.copyFormToBean(form);
        getSurveyPartTableModel().copyFormToBean();
    }

    @Override
    public SurveyDto getBeanToSave() {
        SurveyDto bean = getBean();
        bean.setSurveyPart(getSurveyPartTableModel().getValidData());
        bean.autoTrim();
        return bean;
    }

    public SurveyPartTableModel getSurveyPartTableModel() {
        return surveyPartTableModel;
    }

}
