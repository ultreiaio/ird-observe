package fr.ird.observe.client.datasource.editor.ps.data.localmarket;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilter;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.consolidation.data.ps.localmarket.BatchConsolidateRequest;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.localmarket.BatchDto;
import fr.ird.observe.dto.data.ps.localmarket.SurveyReference;
import fr.ird.observe.dto.data.ps.localmarket.TripBatchDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.referential.ps.localmarket.BuyerReference;
import fr.ird.observe.dto.referential.ps.localmarket.PackagingReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.data.ps.ConsolidateDataService;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.SwingUtilities;
import java.beans.PropertyChangeEvent;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@GenerateJavaBeanDefinition
public class TripBatchUIModelStates extends GeneratedTripBatchUIModelStates {
    public static final Set<String> COMPUTE_PROPERTIES = Set.of(BatchDto.PROPERTY_COUNT, BatchDto.PROPERTY_WEIGHT, BatchDto.PROPERTY_PACKAGING);
    private static final Logger log = LogManager.getLogger(TripBatchUIModelStates.class);
    private final ConsolidateDataService psConsolidateDataService;
    private TripDto trip;
    private boolean computing;

    public TripBatchUIModelStates(GeneratedTripBatchUIModel model) {
        super(model);
        psConsolidateDataService = model.getSource().getContext().getMainDataSource().getPsConsolidateDataService();
    }

    @Override
    public void open(ContentUIModel model) {
        super.open(model);
        ClientValidationContext context = getClientValidationContext();
        if (context.getSelectModel().getPs().getCommonTrip().isEnabled()) {
            trip = context.getCurrentPsCommonTrip();
        }
        getTableEditBean().addPropertyChangeListener(BatchDto.PROPERTY_WEIGHT, this::resetComputedValues);
        getTableEditBean().addPropertyChangeListener(this::recomputeComputedValues);
    }

    public void resetComputedValues(PropertyChangeEvent event) {
        if (computing) {
            return;
        }
        if (getTableModel().isCanConsolidate()) {
            getTableEditBean().setWeightComputedSource(null);
        }
    }

    public void recomputeComputedValues(PropertyChangeEvent event) {
        if (computing) {
            return;
        }
        String propertyName = event.getPropertyName();
        if (!COMPUTE_PROPERTIES.contains(propertyName) || !getTableModel().isCanConsolidate()) {
            return;
        }
        SwingUtilities.invokeLater(this::recomputeComputedValues);
    }

    public void recomputeComputedValues() {
        if (computing) {
            return;
        }
        computing = true;

        try {
            log.info("Will recompute some values...");
            BatchConsolidateRequest request = new BatchConsolidateRequest();
            request.setBatch(getTableEditBean());
            request.setOceanId(trip.getOcean().getId());
            request.setDate(trip.getEndDate());
            request.setFailIfLengthWeightParameterNotFound(false);
            ToolkitIdModifications result = psConsolidateDataService.consolidateLocalmarketBatch(request);
            if (result != null) {
                log.info("Flush consolidate batch changes...");
                result.flushToBean(getTableEditBean());
                Set<String> lengthWeightParameterNotFound = result.getWarnings();
                if (lengthWeightParameterNotFound != null) {
                    lengthWeightParameterNotFound.forEach(log::warn);
                }
            }
        } finally {
            computing = false;
        }
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        super.onAfterInitAddReferentialFilters(clientConfig, observeSelectModel, servicesProvider, referenceCache);
        referenceCache.addReferentialFilter(BatchDto.PROPERTY_SPECIES, ReferencesFilterHelper.newPsSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListSeineLocalmarketId()));
        referenceCache.addReferentialFilter(BatchDto.PROPERTY_PACKAGING, newPackagingList());
        referenceCache.addReferentialFilter(BatchDto.PROPERTY_BUYER, newBuyerList());
    }

    @Override
    protected void loadReferentialCacheOnOpenForm(Form<TripBatchDto> form) {
        getReferenceCache().setDataReferenceSet(BatchDto.PROPERTY_SURVEY, DataDtoReferenceSet.of(SurveyReference.class, form.getObject().getLocalmarketSurvey(), null));
        super.loadReferentialCacheOnOpenForm(form);
    }

    protected ReferencesFilter<PackagingReference> newPackagingList() {
        return (dto, incomingReferences) -> {
            LinkedHashSet<String> availablePackagingIds = ((TripBatchDto) dto).getAvailablePackagingIds();
            return incomingReferences.stream().filter(e -> availablePackagingIds.contains(e.getId())).collect(Collectors.toList());
        };
    }

    protected ReferencesFilter<BuyerReference> newBuyerList() {
        return (dto, incomingReferences) -> {
            LinkedHashSet<String> availableBuyerIds = ((TripBatchDto) dto).getAvailableBuyerIds();
            return incomingReferences.stream().filter(e -> availableBuyerIds.contains(e.getId())).collect(Collectors.toList());
        };
    }

}
