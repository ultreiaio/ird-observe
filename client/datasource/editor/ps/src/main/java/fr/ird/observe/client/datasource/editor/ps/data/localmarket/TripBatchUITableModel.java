package fr.ird.observe.client.datasource.editor.ps.data.localmarket;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ps.localmarket.BatchDto;

import javax.swing.JTable;

public class TripBatchUITableModel extends GeneratedTripBatchUITableModel {

    private static final long serialVersionUID = 1L;
    public static final String PROPERTY_CAN_CONSOLIDATE = "canConsolidate";

    private boolean canConsolidate;

    public TripBatchUITableModel(TripBatchUI ui) {
        super(ui);
    }

    @Override
    public void initTableUISize(JTable table) {
        super.initTableUISize(table);
        UIHelper.fixTableColumnWidth(table, 3, 75);
        UIHelper.fixTableColumnWidth(table, 4, 75);
    }

    @Override
    protected void stopEditTableEditBean(TripBatchUI ui, BatchDto tableEditBean) {
        setCanConsolidate(false);
        super.stopEditTableEditBean(ui, tableEditBean);
    }

    @Override
    protected void startEditTableEditBean(TripBatchUI ui, BatchDto tableEditBean) {
        super.startEditTableEditBean(ui, tableEditBean);
        setCanConsolidate(true);
    }

    public boolean isCanConsolidate() {
        return canConsolidate;
    }

    public void setCanConsolidate(boolean canConsolidate) {
        boolean oldValue = this.canConsolidate;
        this.canConsolidate = canConsolidate;
        firePropertyChange(PROPERTY_CAN_CONSOLIDATE, oldValue, canConsolidate);
    }
}
