package fr.ird.observe.client.datasource.editor.ps.data.localmarket;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.table.EditableTableModel;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.dto.data.ps.localmarket.SampleDto;
import fr.ird.observe.dto.data.ps.localmarket.WellIdDto;
import org.nuiton.jaxx.validator.swing.SwingValidator;

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 31/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class WellTableModel extends EditableTableModel<WellIdDto> {

    private static final long serialVersionUID = 1L;
    private final SampleUIModelStates states;

    public WellTableModel(SampleUIModelStates states) {
        super(true);
        this.states = states;
    }

    public void init(SampleUI ui) {

        EditableTable<WellIdDto, WellTableModel> table = ui.getWell();
        JXTableUtil.setI18nTableHeaderRenderer(table, WellIdDto.class, "well");

//        JXTableUtil.initRenderers(table, new DefaultTableCellRenderer());

        DefaultCellEditor editor = (DefaultCellEditor) table.getDefaultEditor(String.class);
        JTextField component = (JTextField) editor.getComponent();
        component.addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(AncestorEvent event) {
                SwingUtilities.invokeLater(() -> {
                    component.requestFocusInWindow();
                    component.selectAll();
                });
            }

            @Override
            public void ancestorRemoved(AncestorEvent event) {

            }

            @Override
            public void ancestorMoved(AncestorEvent event) {

            }
        });
        JXTableUtil.initEditors(table, editor);
        table.init(ui, ui.getValidatorMeasure());
    }

    @Override
    public void onModifiedChanged(SwingValidator<?> validator, Boolean newValue) {
        super.onModifiedChanged(validator, newValue);
        if (newValue) {
            Set<String> notEmptyData = SampleDto.toIds(getNotEmptyData());
            states.getBean().setWell(notEmptyData);
        }
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public Object getValueAt0(WellIdDto row, int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return row.getWell();
        } else {
            throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public boolean setValueAt0(WellIdDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue = getValueAt0(row, rowIndex, columnIndex);
        if (columnIndex == 0) {
            row.setWell((String) aValue);
        } else {
            throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    @Override
    protected WellIdDto createNewRow() {
        return new WellIdDto();
    }
}
