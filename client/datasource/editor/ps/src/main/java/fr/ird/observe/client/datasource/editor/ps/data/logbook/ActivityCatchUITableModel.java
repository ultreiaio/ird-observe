package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ps.logbook.CatchDto;
import fr.ird.observe.dto.referential.common.WeightMeasureMethodReference;

import javax.swing.JTable;
import java.beans.PropertyChangeListener;

class ActivityCatchUITableModel extends GeneratedActivityCatchUITableModel {
    private static final long serialVersionUID = 1L;

    final PropertyChangeListener weightChanged;

    public ActivityCatchUITableModel(ActivityCatchUI ui) {
        super(ui);
        weightChanged = evt -> {
            CatchDto source = (CatchDto) evt.getSource();
            Object newValue = evt.getNewValue();
            if (newValue != null && source.getWeightMeasureMethod() == null) {
                // set default weight measure method
                WeightMeasureMethodReference weightMeasureMethod = getModel().getStates().getDefaultWeightMeasureMethod();
                source.setWeightMeasureMethod(weightMeasureMethod);
            }
        };
    }

    @Override
    public void initTableUISize(JTable table) {
        super.initTableUISize(table);
        UIHelper.fixTableColumnWidth(table, 4, 55);
        UIHelper.fixTableColumnWidth(table, 5, 55);
        UIHelper.fixTableColumnWidth(table, 6, 55);
    }

    @Override
    protected void onSelectedRowChanged(ActivityCatchUI ui, int editingRow, CatchDto tableEditBean, CatchDto previousRowBean, boolean notPersisted, boolean newRow) {
        unListenTableEditBean();
        try {
            super.onSelectedRowChanged(ui, editingRow, tableEditBean, previousRowBean, notPersisted, newRow);
        } finally {
            listenTableEditBean();
        }
    }

    private void unListenTableEditBean() {
        getModel().getStates().weightCategoryHelper.unListenSpeciesChanged();
        getTableEditBean().removePropertyChangeListener(CatchDto.PROPERTY_WEIGHT, weightChanged);
    }

    private void listenTableEditBean() {
        getModel().getStates().weightCategoryHelper.listenSpeciesChanged();
        getTableEditBean().addPropertyChangeListener(CatchDto.PROPERTY_WEIGHT, weightChanged);
    }

}
