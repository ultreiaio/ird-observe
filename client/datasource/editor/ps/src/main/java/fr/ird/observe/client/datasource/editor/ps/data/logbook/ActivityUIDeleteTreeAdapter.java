package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeleteRequest;
import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeleteTreeAdapter;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationUI;

import java.util.Set;
import java.util.function.Function;

/**
 * When deleting some activities, we need to reload all activities of his route.
 * <p>
 * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2784">issue 2784</a>
 * <p>
 * Created at 15/09/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class ActivityUIDeleteTreeAdapter extends DeleteTreeAdapter<ActivityListUINavigationNode> {

    public static Function<DeleteRequest, ActivityUIDeleteTreeAdapter> create(ActivityListUI ui) {
        return r -> new ActivityUIDeleteTreeAdapter(ui.getModel().getSource());
    }

    public static Function<DeleteRequest, ActivityUIDeleteTreeAdapter> create(ActivityUI ui) {
        return r -> new ActivityUIDeleteTreeAdapter(ui.getModel().getSource());
    }

    public ActivityUIDeleteTreeAdapter(ActivityListUINavigationNode incomingNode) {
        super(incomingNode, e -> incomingNode);
    }

    public ActivityUIDeleteTreeAdapter(ActivityUINavigationNode incomingNode) {
        super(incomingNode, e -> incomingNode.getParent());
    }

    @Override
    public void adaptParentNode(NavigationUI navigationUI, ActivityListUINavigationNode parentNode, Set<String> ids) {
        // remove obsolete nodes
        removeChildren(navigationUI, parentNode, ids);
        // reload parent node data and all his children
        parentNode.reloadNodeDataAndChildren();
        // reload node data to the root (all lastUpdateDate has been updated)
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2786
        parentNode.getParent().reloadNodeDataToRoot();
    }
}
