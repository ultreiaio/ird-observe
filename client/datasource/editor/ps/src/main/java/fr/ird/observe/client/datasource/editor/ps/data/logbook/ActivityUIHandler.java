/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.ps.data.logbook;

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.CopyCoordinate;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveAction;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.open.actions.SaveContentOpenableUIAdapter;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.ps.ObservePsKeyStrokes;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityReference;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import io.ultreia.java4all.i18n.I18n;

import java.awt.event.ItemEvent;
import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
class ActivityUIHandler extends GeneratedActivityUIHandler {

    /**
     * To enable or not original coordinate on ActivityDto.originalDataModified flag.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2720">issue 2720</a>
     */
    private final PropertyChangeListener originalDataModifiedChanged;
    private final PropertyChangeListener fpaZoneChangedChanged;
    private final PropertyChangeListener vesselActivityChanged;

    public ActivityUIHandler() {
        originalDataModifiedChanged = evt -> onOriginalDataModifiedChanged((Boolean) evt.getNewValue());
        fpaZoneChangedChanged = evt -> onFpaZoneChanged((Boolean) evt.getNewValue());
        vesselActivityChanged = evt -> onVesselActivityChanged((VesselActivityReference) evt.getNewValue());
    }

    @Override
    public void onInit(ActivityUI ui) {
        super.onInit(ui);
        ui.getCoordinateHeader().remove(ui.getSkipMandatoryCoordinate());
    }

    @Override
    public void startEditUI() {
        ActivityDto bean = getModel().getStates().getBean();
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2729#note_1475767290
        if (bean.getMaxNumberAuthorized() == null) {
            // compute it
            bean.setMaxNumberAuthorized(getModel().getSource().getParent().getChildCount());
        }
        if (getModel().getStates().isCreatingMode()) {
            ui.getCoordinateHeader().add(ui.getSkipMandatoryCoordinate());
        }
        bean.removePropertyChangeListener(ActivityDto.PROPERTY_ORIGINAL_DATA_MODIFIED, originalDataModifiedChanged);
        bean.removePropertyChangeListener(ActivityDto.PROPERTY_CHANGED_ZONE_OPERATION, fpaZoneChangedChanged);
        bean.removePropertyChangeListener(ActivityDto.PROPERTY_VESSEL_ACTIVITY, vesselActivityChanged);
        onOriginalDataModifiedChanged(bean.isOriginalDataModified());
        onFpaZoneChanged(bean.isChangedZoneOperation());
        onVesselActivityChanged(bean.getVesselActivity());
        getModel().getStates().startEditUI();
        super.startEditUI();
        bean.addPropertyChangeListener(ActivityDto.PROPERTY_ORIGINAL_DATA_MODIFIED, originalDataModifiedChanged);
        bean.addPropertyChangeListener(ActivityDto.PROPERTY_CHANGED_ZONE_OPERATION, fpaZoneChangedChanged);
        bean.addPropertyChangeListener(ActivityDto.PROPERTY_VESSEL_ACTIVITY, vesselActivityChanged);
        TripDto trip = getClientValidationContext().getCurrentPsCommonTrip();
        getModel().getStates().setNumberEnabled(Objects.requireNonNull(trip.getActivitiesAcquisitionMode()).isByNumber());
    }

    @Override
    public void stopEditUI() {
        ui.getCoordinateHeader().remove(ui.getSkipMandatoryCoordinate());
        getModel().getStates().stopEditUI();
        super.stopEditUI();
    }

    @Override
    protected void installSaveAction() {
        SaveAction.create(ui, ActivityDto.class)
                .on(ui.getModel()::toSaveRequest)
                .call((r, d) -> getOpenableService().save(r.getParentId(), d))
                .then(new SaveContentOpenableUIAdapter<>(ActivityDto::isSkipMandatoryCoordinate, ActivityUI::getAddFloatingObject) {

                      @Override
                      protected void afterNodeUpdated(DataSourceEditor dataSourceEditor, ActivityUI ui, NavigationTree tree, ContentOpenableUINavigationNode node, boolean notPersisted, ActivityDto bean) {
                          ActivityReference reference = (ActivityReference) node.getReference();
                          bean.setStatistics(reference.statistics());
                          // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2729
                          // May be all activities nodes has changed (the number can have been recomputed)
                          ActivityListUINavigationNode activityListNode = ((ActivityUINavigationNode) node).getParent();
                          // must reload activity list node
                          activityListNode.reloadNodeData(false);
                          // remove all his children
                          activityListNode.removeAllChildren();
                          // rebuild them
                          activityListNode.getCapability().buildChildren();
                          // use now the new rebuild node
                          node = activityListNode.getActivityUINavigationNode(bean.getId());
                          super.afterNodeUpdated(dataSourceEditor, ui, tree, node, notPersisted, bean);
                      }
                  })
                  .install(ui.getSave());
        CopyCoordinate.install(I18n.t("observe.ui.action.copyCoordinate"),
                ObservePsKeyStrokes.KEY_STROKE_COPY_COORDINATE,
                ui,
                ui.getCoordinate(),
                ui.getCoordinateOriginal(),
                ui.getCopyFirstCoordinate());
    }

    private void onVesselActivityChanged(VesselActivityReference vesselActivity) {
        if (!getModel().getStates().isCreatingMode()) {
            return;
        }

        if (VesselActivityReference.isCoordinateRequiredForVesselActivity(vesselActivity)) {
            ui.getSkipMandatoryCoordinate().setEnabled(false);
            ui.getSkipMandatoryCoordinate().setSelected(false);
        } else {
            ui.getSkipMandatoryCoordinate().setEnabled(true);
        }
    }

    void onSkipMandatoryCoordinateChanged(ItemEvent newValue) {
        if (!getModel().getStates().isCreatingMode()) {
            return;
        }
        ui.getBean().setSkipMandatoryCoordinate(newValue.getStateChange() == ItemEvent.SELECTED);
    }

    private void onOriginalDataModifiedChanged(Boolean newValue) {

        ActivityUI ui = getUi();

        if (Objects.equals(true, newValue)) {

            // original data modified
            ui.getCoordinateOriginal().setEnabled(true);
            ui.getCopyFirstCoordinate().setEnabled(true);

        } else {

            // original data not modified
            ui.getCoordinateOriginal().getGlobalResetButton().doClick();
            ui.getCoordinateOriginal().setEnabled(false);
            ui.getCopyFirstCoordinate().setEnabled(false);
        }
    }

    void onFpaZoneChanged(boolean newValue) {
        ActivityDto bean = getModel().getStates().getBean();
        if (Objects.equals(true, newValue)) {
            bean.setCurrentFpaZone(null);
        } else {
            bean.setPreviousFpaZone(null);
            bean.setNextFpaZone(null);
        }
    }
}
