package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.logbook.ActivityDto;
import fr.ird.observe.dto.data.ps.observation.ActivityReference;
import fr.ird.observe.dto.data.ps.pairing.RoutePairingEngine;
import fr.ird.observe.dto.data.ps.pairing.RoutePairingDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ps.common.ObservedSystemReference;
import fr.ird.observe.dto.referential.ps.common.ReasonForNoFishingReference;
import fr.ird.observe.dto.referential.ps.common.ReasonForNullSetReference;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.dto.referential.ps.logbook.SetSuccessStatusReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.navigation.id.ps.Module;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@GenerateJavaBeanDefinition
public class ActivityUIModelStates extends GeneratedActivityUIModelStates {
    public static final String PROPERTY_ACTIVITY_OBS = "activityObs";
    public static final Set<String> PROPERTIES_FOR_UPDATE_ACTIVITIES_OBS_LIST = new LinkedHashSet<>(Arrays.asList(
            ActivityDto.PROPERTY_VESSEL_ACTIVITY,
            ActivityDto.PROPERTY_LATITUDE,
            ActivityDto.PROPERTY_LONGITUDE,
            ActivityDto.PROPERTY_QUADRANT,
            ActivityDto.PROPERTY_TIME));
    private static final Logger log = LogManager.getLogger(ActivityUIModelStates.class);
    private final SingletonSupplier<RoutePairingEngine> pairingContext;
    private List<ActivityReference> activityObs;
    private Map<String, ActivityReference> activityObsCache;
    private ReasonForNullSetReference defaultReasonForNullSet;
    private boolean numberEnabled;

    public ActivityUIModelStates(GeneratedActivityUIModel model) {
        super(model);
        Module psModule = getClientValidationContext().getSelectModel().getPs();
        ObserveSwingDataSource dataSource = getReferenceCache().getDataSource();

        this.pairingContext = SingletonSupplier.of(() -> {
            RoutePairingDto data = dataSource.getPsActivityPairingService().getRoutePairingDto(psModule.getCommonTrip().getId(), psModule.getLogbookRoute().getId());
            return data == null ? null : new RoutePairingEngine(data);
        }, false);
        getBean().addPropertyChangeListener(evt -> {
            String propertyName = evt.getPropertyName();
            if (!isOpened()) {
                return;
            }
            if (isReadingMode()) {
                return;
            }
            if (ActivityDto.PROPERTY_REASON_FOR_NO_FISHING.equals(propertyName)) {
                ReasonForNoFishingReference newValue = (ReasonForNoFishingReference) evt.getNewValue();
                setReasonForNoFishingDefaultValues(newValue);
                return;
            }
            if (ActivityDto.PROPERTY_SET_SUCCESS_STATUS.equals(propertyName)) {
                SetSuccessStatusReference newValue = (SetSuccessStatusReference) evt.getNewValue();
                setSetSuccessStatus(newValue);
                return;
            }
            if (ActivityDto.PROPERTY_SET_ENABLED.equals(propertyName)) {
                boolean newValue = (boolean) evt.getNewValue();
                setSetEnabled(newValue);
                return;
            }

            if (!PROPERTIES_FOR_UPDATE_ACTIVITIES_OBS_LIST.contains(propertyName)) {
                return;
            }
            pairingContext().ifPresent(this::updateActivityObsCandidates);
        });
    }

    @Override
    protected void copyFormToBean(Form<ActivityDto> form) {
        super.copyFormToBean(form);
        setReasonForNoFishingDefaultValues(getBean().getReasonForNoFishing());
        setSetSuccessStatus(getBean().getSetSuccessStatus());
        setSetEnabled(isSetEnabled());
    }

    public List<ActivityReference> getActivityObs() {
        return activityObs;
    }

    public void setActivityObs(List<ActivityReference> activityObs) {
        ActivityReference relatedObservedActivity = getBean().getRelatedObservedActivity();
        this.activityObs = Objects.requireNonNull(activityObs);
        activityObsCache = Maps.uniqueIndex(activityObs, ActivityReference::getId);
        boolean removeRelatedObservedActivity = relatedObservedActivity != null && !activityObs.contains(relatedObservedActivity);
        firePropertyChange(PROPERTY_ACTIVITY_OBS, null, activityObs);
        if (removeRelatedObservedActivity) {
            log.info(String.format("%s Removed not matching related observed activity: %s", getPrefix(), relatedObservedActivity));
            getBean().setRelatedObservedActivity(null);
        } else if (relatedObservedActivity != null) {
            //FIXME Check this does not alter model.modified ?
            ActivityReference activityReference = activityObsCache.get(relatedObservedActivity.getId());
            getBean().setRelatedObservedActivity(activityReference);
        }
    }

    public boolean isReasonForNoFishingNotFilled() {
        return getBean().getReasonForNoFishing() == null;
    }

    public boolean isSetEnabled() {
        return getBean().isSetEnabled();
    }

    public boolean isReasonForNullSetEnabled() {
        return isReasonForNoFishingNotFilled() && getBean().getSetSuccessStatus() != null && isReasonForNullSetEnabled(getBean().getSetSuccessStatus());
    }

    public boolean isNumberEnabled() {
        return numberEnabled;
    }

    public void setNumberEnabled(boolean numberEnabled) {
        this.numberEnabled = numberEnabled;
        firePropertyChange("numberEnabled", isNumberEnabled());
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(ActivityDto.PROPERTY_OBSERVED_SYSTEM, ReferencesFilterHelper.newPredicateList(ObservedSystemReference::isLogbook));
        referenceCache.addReferentialFilter(ActivityDto.PROPERTY_VESSEL_ACTIVITY, ReferencesFilterHelper.newPredicateList(VesselActivityReference::isLogbook));
    }

    @Override
    protected void loadReferentialCacheOnOpenForm(Form<ActivityDto> form) {
        super.loadReferentialCacheOnOpenForm(form);
        defaultReasonForNullSet = (ReasonForNullSetReference) getReferenceCache().tryGetReferentialReferenceById(ActivityDto.PROPERTY_REASON_FOR_NULL_SET, ProtectedIdsPs.PS_COMMON_REASON_FOR_NUL_SET_0).orElseThrow();
    }

    boolean isReasonForNullSetEnabled(SetSuccessStatusReference newValue) {
        return newValue != null && newValue.getId().equals(ProtectedIdsPs.PS_LOGBOOK_SET_SUCCESS_STATUS_0);
    }

    void setSetEnabled(boolean newValue) {
        ActivityDto bean = getBean();
        if (newValue) {
            if (isCreatingMode()) {
                bean.setSetCount(1);
            }
        } else {
            bean.setReasonForNoFishing(null);
            bean.setSetSuccessStatus(null);
            bean.setReasonForNullSet(null);
            bean.setSchoolType(null);
            bean.setTotalWeight(null);
            bean.setSetCount(null);
        }
        firePropertyChange("setEnabled", isSetEnabled());
    }

    void setReasonForNoFishingDefaultValues(ReasonForNoFishingReference newValue) {
        ActivityDto bean = getBean();
        if (newValue != null) {
            bean.setSetSuccessStatus(null);
            bean.setReasonForNullSet(null);
            bean.setSchoolType(null);
            bean.setTotalWeight(null);
            bean.setSetCount(0);
        } else {
            if (isCreatingMode()) {
                bean.setSetCount(1);
            }
        }
        firePropertyChange("reasonForNoFishingNotFilled", isReasonForNoFishingNotFilled());
    }

    void setSetSuccessStatus(SetSuccessStatusReference newValue) {
        boolean reasonForNullSetEnabled = isReasonForNullSetEnabled(newValue);
        if (!reasonForNullSetEnabled) {
            getBean().setReasonForNullSet(null);
        } else if (isCreatingMode()) {
            getBean().setReasonForNullSet(defaultReasonForNullSet);
        }
        firePropertyChange("reasonForNullSetEnabled", reasonForNullSetEnabled);
    }


    void updateActivityObsCandidates(RoutePairingEngine pairingContext) {
        log.info(String.format("%s Will update related observed activities...", getPrefix()));
        ActivityDto bean = getBean();
        String vesselActivityId = bean.getVesselActivityId();
        Date timeStamp = bean.getTimeStamp();

        List<ActivityReference> activityObs = pairingContext.getObservationActivitiesCandidates(vesselActivityId, timeStamp);
        log.info(String.format("%s Found %d related observed activities.", getPrefix(), activityObs.size()));
        setActivityObs(activityObs);
    }

    public void startEditUI() {
        ActivityDto bean = getBean();
        pairingContext().ifPresent(pairingContext -> {
            ActivityReference relatedObservedActivity = bean.getRelatedObservedActivity();
            updateActivityObsCandidates(pairingContext);
            if (relatedObservedActivity != null) {
                //FIXME Check this does not alter model.modified ?
                ActivityReference activityReference = activityObsCache.get(relatedObservedActivity.getId());
                getBean().setRelatedObservedActivity(activityReference);
            }
        });
    }

    public void stopEditUI() {
        activityObs = new ArrayList<>();
        firePropertyChange(PROPERTY_ACTIVITY_OBS, null, activityObs);
        pairingContext.clear();
    }

    private Optional<RoutePairingEngine> pairingContext() {
        return Optional.ofNullable(pairingContext.get());
    }
}
