<!--
  #%L
  ObServe Client :: DataSource :: Editor :: PS
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUI
    beanScope="bean" i18n="fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto" superGenericType='FloatingObjectDto, FloatingObjectUI'
    implements='fr.ird.observe.client.datasource.editor.ps.data.dcp.DcpUIAdapter&lt;TransmittingBuoyDto &gt;'>

  <style source="../dcp/FloatingObjectUICommon.jcss"/>

  <import>
    fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto
    fr.ird.observe.dto.data.ps.logbook.TransmittingBuoyDto
    fr.ird.observe.dto.data.ps.TypeTransmittingBuoyOperation
    fr.ird.observe.dto.referential.common.CountryReference
    fr.ird.observe.dto.referential.common.VesselReference
    fr.ird.observe.dto.referential.ps.common.TransmittingBuoyOperationReference
    fr.ird.observe.dto.referential.ps.common.TransmittingBuoyOwnershipReference
    fr.ird.observe.dto.referential.ps.common.TransmittingBuoyTypeReference
    fr.ird.observe.dto.referential.ps.common.ObjectOperationReference
    fr.ird.observe.client.datasource.editor.ps.data.dcp.DcpUIModelStates
    fr.ird.observe.client.datasource.editor.ps.data.dcp.FloatingObjectPartsTreeTable
    fr.ird.observe.client.datasource.editor.ps.data.dcp.FloatingObjectPartsTreeTableModel
    fr.ird.observe.client.datasource.editor.ps.data.dcp.FloatingObjectPartLegendTreeCellRenderer

    io.ultreia.java4all.jaxx.widgets.combobox.BeanEnumEditor
    io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox
    org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor
    org.nuiton.jaxx.widgets.text.BigTextEditor
    org.nuiton.jaxx.widgets.text.NormalTextEditor
    org.jdesktop.swingx.JXTitledPanel

    static fr.ird.observe.client.util.UIHelper.getStringValue
    static io.ultreia.java4all.i18n.I18n.n
  </import>

  <FloatingObjectUIModel id='model' constructorParams='@override:getNavigationSource(this)'/>
  <FloatingObjectDto id='bean'/>
  <FloatingObjectUIModelStates id='states'/>
  <TransmittingBuoyDto id='transmittingBuoy1' initializer='new TransmittingBuoyDto()'/>
  <TransmittingBuoyDto id='transmittingBuoy2' initializer='new TransmittingBuoyDto()'/>

  <BeanValidator id='validator' autoField='true' context='create' errorTableModel='{getErrorTableModel()}' beanClass='fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto'>
    <field name="materialsValid" component="tableScroll"/>
  </BeanValidator>

  <BeanValidator id='validatorBuoy1' beanClass='fr.ird.observe.dto.data.ps.logbook.TransmittingBuoyDto'
                 errorTableModel='{getErrorTableModel()}' parentValidator='{validator}' context='update'>
    <field name="transmittingBuoyType" component="transmittingBuoyType1"/>
    <field name="transmittingBuoyOperation" component="transmittingBuoy1EditorPanel"/>
    <field name="transmittingBuoyOwnership" component="transmittingBuoyOwnership1"/>
    <field name="code" component="code1"/>
    <field name="country" component="country1"/>
    <field name="vessel" component="vessel1"/>
    <field name='longitude' component='coordinate1'/>
    <field name='latitude' component='coordinate1'/>
    <field name='quadrant' component='coordinate1'/>
    <field name='comment' component='comment1'/>
  </BeanValidator>

  <BeanValidator id='validatorBuoy2' beanClass='fr.ird.observe.dto.data.ps.logbook.TransmittingBuoyDto'
                 errorTableModel='{getErrorTableModel()}' parentValidator='{validator}' context='update'>
    <field name="transmittingBuoyType" component="transmittingBuoyType2"/>
    <field name="transmittingBuoyOperation" component="transmittingBuoy2EditorPanel"/>
    <field name="transmittingBuoyOwnership" component="transmittingBuoyOwnership2"/>
    <field name="code" component="code2"/>
    <field name="country" component="country2"/>
    <field name="vessel" component="vessel2"/>
    <field name='comment' component='comment2'/>
  </BeanValidator>

  <JPanel id="contentBody" layout='{new BorderLayout()}'>
    <Table insets="0" fill="both" constraints='BorderLayout.CENTER'>
      <row>
        <cell anchor="north" weightx="1" weighty="1">
          <JTabbedPane id='mainTabbedPane'>
            <tab id='generalTab' i18nProperty="">
              <Table fill='both'>
                <row>
                  <cell anchor='west'>
                    <JLabel id='objectOperationLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1" fill="both">
                    <FilterableComboBox id='objectOperation' genericType='ObjectOperationReference'/>
                  </cell>
                </row>
                <row>
                  <cell anchor="west">
                    <JLabel id='supportVesselNameLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1" fill="both">
                    <NormalTextEditor id='supportVesselName'/>
                  </cell>
                </row>
                <row>
                  <cell columns='2' fill="both">
                    <Table id="computedPanel" fill="both" weightx="1">
                      <row>
                        <cell anchor="west">
                          <JLabel styleClass="skipI18n"/>
                        </cell>
                        <cell weightx="0.5" fill="both">
                          <JLabel text="observe.Common.whenArriving" styleClass="skipI18n"/>
                        </cell>
                        <cell weightx="0.5" fill="both">
                          <JLabel text="observe.Common.whenLeaving" styleClass="skipI18n"/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor="west">
                          <JLabel id='computedBiodegradableLabel'/>
                        </cell>
                        <cell weightx="0.5" fill="both">
                          <JLabel id='computedWhenArrivingBiodegradable' styleClass="skipI18n bold"/>
                        </cell>
                        <cell weightx="0.5" fill="both">
                          <JLabel id='computedWhenLeavingBiodegradable' styleClass="skipI18n bold"/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor="west">
                          <JLabel id='computedNonEntanglingLabel'/>
                        </cell>
                        <cell weightx="0.5" fill="both">
                          <JLabel id='computedWhenArrivingNonEntangling' styleClass="skipI18n bold"/>
                        </cell>
                        <cell weightx="0.5" fill="both">
                          <JLabel id='computedWhenLeavingNonEntangling' styleClass="skipI18n bold"/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor="west">
                          <JLabel id='computedSimplifiedObjectTypeLabel'/>
                        </cell>
                        <cell weightx="0.5" fill="both">
                          <JLabel id='computedWhenArrivingSimplifiedObjectType' styleClass="skipI18n bold"/>
                        </cell>
                        <cell weightx="0.5" fill="both">
                          <JLabel id='computedWhenLeavingSimplifiedObjectType' styleClass="skipI18n bold"/>
                        </cell>
                      </row>
                    </Table>
                  </cell>
                </row>
                <row>
                  <cell columns='2' fill="both" weighty="0.7">
                    <BigTextEditor id="comment"/>
                  </cell>
                </row>
              </Table>
            </tab>
            <tab id='materialsTab' i18nProperty="">
              <Table insets="0" fill="both">
                <row>
                  <cell weighty="1">
                    <JScrollPane id='tableScroll'>
                      <FloatingObjectPartsTreeTable id='table' constructorParams="new FloatingObjectPartsTreeTableModel(getStates())"/>
                    </JScrollPane>
                  </cell>
                </row>
                <row>
                  <cell weightx="1">
                    <JPanel layout="{new GridLayout(1,0)}">
                      <JButton id="copyFloatingObjectPartToRight"/>
                      <JButton id="copyFloatingObjectPartToLeft"/>
                    </JPanel>
                  </cell>
                </row>
              </Table>
            </tab>
            <tab id='buoysTab' i18nProperty="">
              <JPanel id="buoysPane" layout='{new BorderLayout()}'>
                <Table fill='both' constraints="BorderLayout.NORTH">
                  <row>
                    <cell anchor='west'>
                      <JLabel id='typeOperationLabel' styleClass="skipI18n"/>
                    </cell>
                    <cell anchor='east' weightx="1" fill="both">
                      <BeanEnumEditor id='typeOperation' genericType='TypeTransmittingBuoyOperation'
                                      constructorParams='TypeTransmittingBuoyOperation.class'
                                      onItemStateChanged='changeTypeTransmittingBuoyOperation(event,getStates());'/>
                    </cell>
                  </row>
                </Table>
                <JPanel id="transmittingBuoys" layout='{new GridLayout(1,0)}' constraints="BorderLayout.CENTER">
                  <JXTitledPanel id="transmittingBuoy1EditorPanel" contentContainer='{transmittingBuoy1Editor}'>
                    <Table id='transmittingBuoy1Editor' fill="both" beanScope="transmittingBuoy1">
                      <row>
                        <cell anchor='west'>
                          <JLabel id='transmittingBuoyType1Label' styleClass="skipI18n"/>
                        </cell>
                        <cell anchor='east' weightx="1" fill="both">
                          <FilterableComboBox id='transmittingBuoyType1' genericType='TransmittingBuoyTypeReference'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor='west'>
                          <JLabel id='transmittingBuoyOwnership1Label' styleClass="skipI18n"/>
                        </cell>
                        <cell anchor='east' weightx="1" fill="both">
                          <FilterableComboBox id='transmittingBuoyOwnership1' genericType='TransmittingBuoyOwnershipReference'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor='west'>
                          <JLabel id='vessel1Label'/>
                        </cell>
                        <cell anchor='east' weightx="1" fill="both">
                          <FilterableComboBox id='vessel1' genericType='VesselReference'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor='west'>
                          <JLabel id='country1Label'/>
                        </cell>
                        <cell anchor='east' weightx="1" fill="both">
                          <FilterableComboBox id='country1' genericType='CountryReference'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor='west'>
                          <JLabel id='code1Label' styleClass="skipI18n"/>
                        </cell>
                        <cell anchor='east' weightx="1" fill="both">
                          <NormalTextEditor id='code1'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor='west'>
                          <JLabel id='coordinate1Label' styleClass="skipI18n"/>
                        </cell>
                        <cell anchor='east' fill="both">
                          <CoordinatesEditor id='coordinate1'/>
                        </cell>
                      </row>
                      <row>
                        <cell columns='2' weighty="1" fill="both">
                          <BigTextEditor id='comment1'/>
                        </cell>
                      </row>
                    </Table>
                  </JXTitledPanel>
                  <JXTitledPanel id="transmittingBuoy2EditorPanel" contentContainer='{transmittingBuoy2Editor}'>
                    <Table id='transmittingBuoy2Editor' fill="both" beanScope="transmittingBuoy2">
                      <row>
                        <cell anchor='west'>
                          <JLabel id='transmittingBuoyType2Label' styleClass="skipI18n"/>
                        </cell>
                        <cell anchor='east' weightx="1" fill="both">
                          <FilterableComboBox id='transmittingBuoyType2' genericType='TransmittingBuoyTypeReference'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor='west'>
                          <JLabel id='transmittingBuoyOwnership2Label' styleClass="skipI18n"/>
                        </cell>
                        <cell anchor='east' weightx="1" fill="both">
                          <FilterableComboBox id='transmittingBuoyOwnership2' genericType='TransmittingBuoyOwnershipReference'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor='west'>
                          <JLabel id='vessel2Label'/>
                        </cell>
                        <cell anchor='east' weightx="1" fill="both">
                          <FilterableComboBox id='vessel2' genericType='VesselReference'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor='west'>
                          <JLabel id='country2Label'/>
                        </cell>
                        <cell anchor='east' weightx="1" fill="both">
                          <FilterableComboBox id='country2' genericType='CountryReference'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor='west'>
                          <JLabel id='code2Label' styleClass="skipI18n"/>
                        </cell>
                        <cell anchor='east' weightx="1" fill="both">
                          <NormalTextEditor id='code2'/>
                        </cell>
                      </row>
                      <row>
                        <cell columns='2' weighty="1" fill="both">
                          <BigTextEditor id='comment2'/>
                        </cell>
                      </row>
                    </Table>
                  </JXTitledPanel>
                  <JLabel id='noBuoyEditor' styleClass="skipI18n"/>
                </JPanel>
              </JPanel>
            </tab>
          </JTabbedPane>
        </cell>
      </row>
    </Table>
  </JPanel>
  <JMenuItem id="addPreset"/>
</fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUI>
