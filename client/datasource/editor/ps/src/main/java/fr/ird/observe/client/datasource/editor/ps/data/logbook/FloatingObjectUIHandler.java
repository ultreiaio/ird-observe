/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.ps.data.logbook;

import fr.ird.observe.dto.form.Form;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class FloatingObjectUIHandler extends GeneratedFloatingObjectUIHandler {

    @Override
    public void onInit(FloatingObjectUI ui) {
        super.onInit(ui);
        ui.onInit(ui.getModel().getStates(), getReferentialService(), getDecoratorService());
    }

    @Override
    protected void installExtraActions() {
        super.installExtraActions();
        ui.installExtraActions();
    }
//FIXME:Focus ui.getModel().getStates().isCreatingMode() ? getUi().getObjectOperation() : getUi().getSupportVesselName()

    @Override
    public void onOpenForm(Form<?> form) {
        ui.onOpenForm(ui.getModel().getStates());
    }

    @Override
    public void onOpenAfterOpenModel() {
        super.onOpenAfterOpenModel();
        ui.onOpenAfterOpenModel(ui.getModel().getStates());
    }

    @Override
    public void stopEditUI() {
        ui.stopEditUI(ui.getModel().getStates());
        super.stopEditUI();
    }

    @Override
    public void startEditUI() {
        super.startEditUI();
        ui.startEditUI(ui.getModel().getStates());
    }

}
