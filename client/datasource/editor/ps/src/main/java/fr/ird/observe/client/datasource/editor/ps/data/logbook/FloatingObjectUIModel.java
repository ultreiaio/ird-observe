package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.ObserveServicesProvider;

/**
 * Created on 9/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class FloatingObjectUIModel extends GeneratedFloatingObjectUIModel {

    private static FloatingObjectPreset reference;

    public static FloatingObjectPreset getReference() {
        return reference;
    }

    public static void setReference(FloatingObjectPreset reference) {
        FloatingObjectUIModel.reference = reference;
    }

    public FloatingObjectUIModel(FloatingObjectUINavigationNode source) {
        super(source);
    }

    @Override
    public void open() {
        FloatingObjectUIModelStates states = getStates();
        super.open();
        if (states.isCreatingMode()) {
            states.setReference(reference);
            reference = null;
        }
    }

    @Override
    public Form<FloatingObjectDto> preCreateForm(ObserveServicesProvider servicesProvider, String parentId) {
        return servicesProvider.getPsCommonTripService().preCreateLogbookFloatingObject(parentId, getStates().getReference());
    }
}
