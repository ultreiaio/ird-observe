package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.init.DefaultUIInitializer;
import fr.ird.observe.client.util.init.UIInitHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import io.ultreia.java4all.decoration.Decorator;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JTable;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 29/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class LightActivityUIHandler implements UIHandler<LightActivityUI> {

    @Override
    public void afterInit(LightActivityUI ui) {
        DefaultUIInitializer.doInit(ui);
        DecoratorService decoratorService = ui.getContextValue(DecoratorService.class);
        Decorator decorator = decoratorService.getDecoratorByType(VesselActivityReference.class);
        String entityLabel = t(I18nDecoratorHelper.getType(VesselActivityReference.class));
        ui.getVesselActivity().setPopupTitleText(t("observe.referential.Referential.type", entityLabel));
        LightActivityUIModel model = ui.getModel();
        ui.getVesselActivity().init(decorator, model.getVesselActivityUniverse());

        JTable errorTable = ui.getErrorTable();
        UIInitHelper.initErrorTable(errorTable);
        UIHelper.fixTableColumnWidth(errorTable, 0, 45);
        UIHelper.fixTableColumnWidth(errorTable, 1, 100);
        model.setDate(null);
        model.setTime(null);
    }
}
