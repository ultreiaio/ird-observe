package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.validation.ContentMessageTableModel;
import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Date;
import java.util.List;

/**
 * Created on 29/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@GenerateJavaBeanDefinition
public class LightActivityUIModel extends AbstractJavaBean implements ObserveDto {

    private final ContentMessageTableModel messageTableModel = new ContentMessageTableModel();
    private TripDto trip;
    private VesselActivityReference vesselActivity;
    private Date date = new Date(0);
    private Date time = new Date(0);
    private boolean valid;
    private List<VesselActivityReference> vesselActivityUniverse;

    public ContentMessageTableModel getMessageTableModel() {
        return messageTableModel;
    }

    public List<VesselActivityReference> getVesselActivityUniverse() {
        return vesselActivityUniverse;
    }

    public void setVesselActivityUniverse(List<VesselActivityReference> vesselActivityUniverse) {
        List<VesselActivityReference> oldValue = this.vesselActivityUniverse;
        this.vesselActivityUniverse = vesselActivityUniverse;
        firePropertyChange("vesselActivityUniverse", oldValue, vesselActivityUniverse);
    }

    public TripDto getTrip() {
        return trip;
    }

    public void setTrip(TripDto trip) {
        TripDto oldValue = this.trip;
        this.trip = trip;
        firePropertyChange("trip", oldValue, trip);
    }

    public VesselActivityReference getVesselActivity() {
        return vesselActivity;
    }

    public void setVesselActivity(VesselActivityReference vesselActivity) {
        VesselActivityReference oldValue = this.vesselActivity;
        this.vesselActivity = vesselActivity;
        firePropertyChange("vesselActivity", oldValue, vesselActivity);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        Date oldValue = this.date;
        this.date = date;
        firePropertyChange("date", oldValue, date);
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        Date oldValue = this.time;
        this.time = time;
        firePropertyChange("time", oldValue, time);
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        boolean oldValue = this.valid;
        this.valid = valid;
        firePropertyChange("valid", oldValue, valid);
    }

    public ActivityStubDto toActivity() {
        ActivityStubDto result = new ActivityStubDto();
        result.setTopiaCreateDate(new Date());
        result.setTime(time);
        result.setDate(date);
        result.setVesselActivity(vesselActivity);
        result.setNumber(1);
        return result;
    }
}
