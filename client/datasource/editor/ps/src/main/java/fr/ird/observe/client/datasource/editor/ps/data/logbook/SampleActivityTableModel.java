package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.actions.SampleActivityResetComputedValue;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableTableModel;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.dto.data.ps.logbook.ActivityReference;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.data.ps.logbook.SampleActivityDto;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxCellEditor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.AncestorEvent;
import java.awt.Component;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Created on 25/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleActivityTableModel extends EditableTableModel<SampleActivityDto> implements WithClientUIContextApi {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger(SampleActivityTableModel.class);

    private final SampleUIModelStates states;
    private List<ActivityStubDto> activities;
    private Decorator activityDecorator;

    public SampleActivityTableModel(SampleUIModelStates states) {
        super(true);
        this.states = states;
    }

    public void init(SampleUI ui) {
        EditableTable<SampleActivityDto, SampleActivityTableModel> table = ui.getSampleActivityTable();
        table.setRowHeight(22);

        JXTableUtil.setI18nTableHeaderRenderer(table, SampleActivityDto.class,
                                               SampleActivityDto.PROPERTY_ACTIVITY,
                                               SampleActivityDto.PROPERTY_WEIGHTED_WEIGHT,
                                               SampleActivityDto.PROPERTY_WEIGHTED_WEIGHT_COMPUTED);

        activityDecorator = ui.getHandler().getDecoratorService().getDecoratorByType(ActivityStubDto.class, ActivityReference.CLASSIFIER_WITH_ROUTE);
        JXTableUtil.initRenderers(table, JXTableUtil.newDecoratedRenderer(activityDecorator), JXTableUtil.newEmptyNumberTableCellRenderer(), JXTableUtil.newBooleanTableCellRenderer2());

        FilterableComboBoxCellEditor activityEditor = new FilterableComboBoxCellEditor(UIHelper.initCellEditor(ActivityStubDto.class, activityDecorator)) {
            @SuppressWarnings({"rawtypes", "unchecked"})
            @Override
            public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
                List availableActivities = getAvailableActivities(row);
                log.info(String.format("Will edit row %d: (universe %d - available: %d), current value: %s, ", row, activities.size(), availableActivities.size(), value));
                getComponent().setData(availableActivities);

                return super.getTableCellEditorComponent(table, value, isSelected, row, column);
            }
        };

        NumberCellEditor<Float> floatColumnEditor =  new NumberCellEditor<Float>(Float.class, false) {

            SampleActivityResetComputedValue action;
            @Override
            public void ancestorAdded(AncestorEvent event) {
                getAction();
                super.ancestorAdded(event);
            }

            public SampleActivityResetComputedValue getAction() {
                if (action==null) {
                    action = new SampleActivityResetComputedValue(SampleActivityTableModel.this, getNumberEditor(), new JButton());
                }
                return action;
            }

            @Override
            public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
                Component result = super.getTableCellEditorComponent(table, value, isSelected, row, column);
                SampleActivityDto bean = getData(row);
                getAction().init(bean, row);
                return result;
            }

            @Override
            public void ancestorRemoved(AncestorEvent event) {
                super.ancestorRemoved(event);
                getAction().exit();
            }
        };

        JXTableUtil.initEditors(table, activityEditor, floatColumnEditor, null);

        table.init(ui, ui.getValidatorMeasure());

        UIHelper.fixTableColumnWidth(table, 1, 150);
        UIHelper.fixTableColumnWidth(table, 2, 80);
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt0(SampleActivityDto row, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return row.getActivity();
            case 1:
                return row.getWeightedWeight();
            case 2:
                return row.isWeightedWeightComputed();
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public boolean setValueAt0(SampleActivityDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue = getValueAt0(row, rowIndex, columnIndex);
        switch (columnIndex) {
            case 0:
                row.setActivity((ActivityStubDto) aValue);
                break;
            case 1:
                row.setWeightedWeight((Float) aValue);
                if (!Objects.equals(oldValue, aValue)) {
                    boolean weightedWeightComputed = row.isWeightedWeightComputed();
                    // As a side effect we set that weighted weight is not computed
                    // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2053
                    row.setWeightedWeightComputed(false);
                    if (weightedWeightComputed) {
                        fireTableCellUpdated(rowIndex, 2);
                    }
                }
                break;
            case 2:
                if (!Objects.equals(oldValue, aValue)) {
                    row.setWeightedWeightComputed((Boolean) aValue);
                }
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean editable = super.isCellEditable(rowIndex, columnIndex);
        return editable && (columnIndex == 0 || columnIndex == 1);
    }

    @Override
    public SampleActivityDto createNewRow() {
        return SampleActivityDto.newDto(new Date());
    }

    public void initData(List<ActivityStubDto> activities, List<SampleActivityDto> data) {
        this.activities = activities;
        Map<String, ActivityStubDto> activitiesCache = new TreeMap<>(Maps.uniqueIndex(activities, ActivityStubDto::getTopiaId));
        for (SampleActivityDto dto : data) {
            ActivityStubDto activity = dto.getActivity();
            if (activity != null) {
                ActivityStubDto activityFromCache = activitiesCache.get(activity.getTopiaId());
                if (activityFromCache == null) {
                    // suspect activity... but still need to add it to universe
                    this.activities.add(activity);
                    activitiesCache.put(activity.getId(), activity);
                    activity.registerDecorator(activityDecorator);
                    continue;
                }
                // use cache activity (it is well decorated)
                dto.setActivity(activityFromCache);
            }
        }
        setData(data);
    }

    public List<ActivityStubDto> getActivities() {
        return activities;
    }

    public List<ActivityStubDto> getAvailableActivities(int row) {
        List<ActivityStubDto> newData = new LinkedList<>(getActivities());
        for (int i = 0, rowCount = getRowCount(); i < rowCount; i++) {
            if (i != row) {
                SampleActivityDto data = getData(i);
                if (data != null) {
                    ActivityStubDto activity = data.getActivity();
                    newData.remove(activity);
                }
            }
        }
        return newData;
    }

    @Override
    public void onModifiedChanged(SwingValidator<?> validator, Boolean newValue) {
        super.onModifiedChanged(validator, newValue);
        if (newValue) {
            //FIXME Should review flow it seems it break cache logic, tableEditBean still keep this even after changing row
            states.getBean().setSampleActivity(getValidData());
        }
    }
}

