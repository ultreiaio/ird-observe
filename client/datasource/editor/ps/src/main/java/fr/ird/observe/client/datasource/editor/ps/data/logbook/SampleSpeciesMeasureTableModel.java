package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.table.EditableListProperty;
import fr.ird.observe.client.util.table.EditableTableModelWithCache;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.dto.data.ps.logbook.SampleSpeciesMeasureDto;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import java.util.Date;
import java.util.Objects;

/**
 * Created on 29/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleSpeciesMeasureTableModel extends EditableTableModelWithCache<SampleSpeciesMeasureDto> {

    private static final long serialVersionUID = 1L;

    public static void onInit(SampleSpeciesUI ui) {

        EditableTable<SampleSpeciesMeasureDto, SampleSpeciesMeasureTableModel> table = ui.getSampleSpeciesMeasuresTable();
        ui.getTableModel().registerInlineModel(ui.getSampleSpeciesMeasuresTableModel(), table);

        JXTableUtil.setI18nTableHeaderRenderer(table, SampleSpeciesMeasureDto.class, "sizeClass", "count");

        JXTableUtil.initRenderers(table, JXTableUtil.newEmptyNumberTableCellRenderer(), JXTableUtil.newEmptyNumberTableCellRenderer());
        JXTableUtil.initEditors(table, NumberCellEditor.newFloatColumnEditor(), NumberCellEditor.newIntegerColumnEditor());

        table.init(ui, ui.getValidatorMeasure());
        JXTableUtil.setMinimumHeight(table.getRowHeight() * 15, table, ui.getSampleSpeciesMeasuresScrollPane());
    }

    public SampleSpeciesMeasureTableModel(EditableListProperty<SampleSpeciesMeasureDto> listProperty) {
        super(listProperty);
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt0(SampleSpeciesMeasureDto row, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return row.getSizeClass();
            case 1:
                return row.getCount();
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public boolean setValueAt0(SampleSpeciesMeasureDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue = getValueAt0(row, rowIndex, columnIndex);
        switch (columnIndex) {
            case 0:
                row.setSizeClass((Float) aValue);
                break;
            case 1:
                row.setCount((Integer) aValue);
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    @Override
    protected SampleSpeciesMeasureDto createNewRow() {
        return SampleSpeciesMeasureDto.newDto(new Date());
    }

    @Override
    public void onModifiedChanged(SwingValidator<?> validator, Boolean newValue) {
        super.onModifiedChanged(validator, newValue);
        if (newValue) {
            //FIXME Should review flow it seems it break cache logic, tableEditBean still keep this even after changing row
            listProperty().set(getValidData());
        }
    }
}
