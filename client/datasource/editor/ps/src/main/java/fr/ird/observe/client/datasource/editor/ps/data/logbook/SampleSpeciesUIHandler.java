package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.client.datasource.editor.api.content.data.sample.actions.ResetSizeMeasureType;
import fr.ird.observe.dto.data.ps.logbook.SampleSpeciesDto;
import fr.ird.observe.dto.referential.common.SizeMeasureTypeReference;
import fr.ird.observe.dto.referential.common.SpeciesReference;

import java.awt.Component;
import java.util.Optional;

/**
 * Created on 29/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleSpeciesUIHandler extends GeneratedSampleSpeciesUIHandler {

    @Override
    public void onInit(SampleSpeciesUI ui) {
        super.onInit(ui);
        ui.getSizeMeasureType().getToolbarRight().add(ui.getDefaultSizeMeasureType());
        ui.getTableEditBean().addPropertyChangeListener(SampleSpeciesDto.PROPERTY_SPECIES, evt -> onSpeciesChanged((SpeciesReference) evt.getNewValue()));
        SampleSpeciesMeasureTableModel.onInit(ui);
    }

    @Override
    public void initActions() {
        super.initActions();
        ResetSizeMeasureType.install(ui, ui.getDefaultSizeMeasureType(), ui.getSizeMeasureType());
    }

    @Override
    protected Component getFocusComponentOnSelectedRow(SampleSpeciesUI ui, boolean notPersisted, boolean newRow, SampleSpeciesDto tableEditBean, SampleSpeciesDto previousRowBean) {
        if (notPersisted || tableEditBean.getSpecies() == null) {
            return ui.getSpecies();
        }
        return ui.getSampleSpeciesMeasuresTable();
    }

    public void resetDefaultSizeMeasureType(boolean force) {
        ui.getSizeMeasureType().setSelectedItem(null);
        SampleSpeciesUIModel model = getModel();
        if (force || model.getStates().getTableEditBean().getSpecies() != null) {
            ui.getSizeMeasureType().setSelectedItem(model.getStates().getDefaultSizeMeasureType());
        }
    }

    private void onSpeciesChanged(SpeciesReference species) {
        SampleSpeciesUIModelStates states = getModel().getStates();
        if (states.isReadingMode()) {
            return;
        }
        if (!states.isEditing()) {
            return;
        }
        if (states.isResetEdit()) {
            return;
        }
        if (getTableModel().isAdjusting()) {
            return;
        }
        Optional<SizeMeasureTypeReference> sizeMeasureType = states.getSpeciesDefaultSizeMeasureType(species);
        states.setDefaultSizeMeasureType(sizeMeasureType.orElse(null));
        resetDefaultSizeMeasureType(false);
    }
}
