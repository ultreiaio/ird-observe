package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIModelStates;
import fr.ird.observe.dto.data.ps.logbook.SampleSpeciesDto;
import fr.ird.observe.dto.data.ps.observation.SampleMeasureDto;
import fr.ird.observe.dto.referential.common.SizeMeasureTypeReference;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created on 17/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@GenerateJavaBeanDefinition
public class SampleSpeciesUIModelStates extends GeneratedSampleSpeciesUIModelStates {

    private static final String PROPERTY_DEFAULT_SIZE_MEASURE_TYPE = "defaultSizeMeasureType";
    private static final Logger log = LogManager.getLogger(SampleSpeciesUIModelStates.class);

    private SizeMeasureTypeReference defaultSizeMeasureType;
    private Map<String, SizeMeasureTypeReference> sizeMeasureTypeReferenceMap;

    public SampleSpeciesUIModelStates(GeneratedSampleSpeciesUIModel model) {
        super(model);
        addPropertyChangeListener(ContentTableUIModelStates.PROPERTY_FORM, evt -> onFormLoaded());
    }

    protected void onFormLoaded() {
        log.info("On form loaded, load default measure types.");
        loadSizeMeasureTypes(getReferenceCache());
    }

    @Override
    public void initDefault(SampleSpeciesDto newTableBean) {
        super.initDefault(newTableBean);
        newTableBean.setSubSampleNumber(0);
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(SampleSpeciesDto.PROPERTY_SPECIES, ReferencesFilterHelper.newPsSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListSeineLogbookCatchId()));
        referenceCache.addReferentialFilter(SampleSpeciesDto.PROPERTY_SIZE_MEASURE_TYPE, ReferencesFilterHelper.newSubSizeMeasureList(clientConfig.getSeineLogbookSampleSizeMeasureTypeId()));
    }

    public SizeMeasureTypeReference getDefaultSizeMeasureType() {
        return defaultSizeMeasureType;
    }

    public void setDefaultSizeMeasureType(SizeMeasureTypeReference defaultSizeMeasureType) {
        SizeMeasureTypeReference oldValue = getDefaultSizeMeasureType();
        this.defaultSizeMeasureType = defaultSizeMeasureType;
        firePropertyChange(PROPERTY_DEFAULT_SIZE_MEASURE_TYPE, oldValue, defaultSizeMeasureType);
    }

    public void loadSizeMeasureTypes(ReferencesCache referenceCache) {
        List<SizeMeasureTypeReference> sizeMeasureTypeReferences = referenceCache.getReferentialReferences(SampleMeasureDto.PROPERTY_SIZE_MEASURE_TYPE);
        sizeMeasureTypeReferenceMap = Maps.uniqueIndex(sizeMeasureTypeReferences, SizeMeasureTypeReference::getId);
    }

    public Optional<SizeMeasureTypeReference> getSpeciesDefaultSizeMeasureType(SpeciesReference species) {
        SizeMeasureTypeReference result = null;
        if (species != null && species.getSizeMeasureTypeId() != null) {
            String sizeMeasureId = species.getSizeMeasureTypeId();
            result = sizeMeasureTypeReferenceMap.get(sizeMeasureId);
            log.info("Use as default size measure type: " + result);
        } else {
            log.info("No default size measure type defined (species is null, or no default size measure defined on it.");
        }
        return Optional.ofNullable(result);
    }
}

