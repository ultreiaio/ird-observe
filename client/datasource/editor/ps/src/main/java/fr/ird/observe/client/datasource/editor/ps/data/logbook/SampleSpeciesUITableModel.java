package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableListProperty;
import fr.ird.observe.dto.data.ps.logbook.SampleSpeciesDto;
import fr.ird.observe.dto.data.ps.logbook.SampleSpeciesMeasureDto;
import fr.ird.observe.dto.referential.common.SizeMeasureTypeReference;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JTable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Created on 29/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleSpeciesUITableModel extends GeneratedSampleSpeciesUITableModel {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger(SampleSpeciesUITableModel.class);
    protected final SampleSpeciesMeasureTableModel sampleSpeciesMeasuresTableModel;

    public SampleSpeciesUITableModel(SampleSpeciesUI ui) {
        super(ui);
        this.sampleSpeciesMeasuresTableModel = new SampleSpeciesMeasureTableModel(new EditableListProperty<>() {
            @Override
            public Collection<SampleSpeciesMeasureDto> get() {
                return ui.getTableEditBean().getSampleSpeciesMeasure();
            }

            @Override
            public void set(List<SampleSpeciesMeasureDto> data) {
                ui.getTableEditBean().setSampleSpeciesMeasure(data);
            }
        });
    }

    public SampleSpeciesMeasureTableModel getSampleSpeciesMeasuresTableModel() {
        return sampleSpeciesMeasuresTableModel;
    }

    @Override
    public void initTableUISize(JTable table) {
        super.initTableUISize(table);
        UIHelper.fixTableColumnWidth(table, 1, 75);
        UIHelper.fixTableColumnWidth(table, 4, 75);
        UIHelper.fixTableColumnWidth(table, 5, 75);
    }

    @Override
    protected void onBeforeResetRow(int row) {
        super.onBeforeResetRow(row);
        //FIXME Should review flow it seems it break cache logic, tableEditBean still keep this even after changing row
        getValueAt(row).setSampleSpeciesMeasure(sampleSpeciesMeasuresTableModel.getCacheForRow(row));
    }

    @Override
    protected void onSelectedRowChanged(SampleSpeciesUI ui, int editingRow, SampleSpeciesDto tableEditBean, SampleSpeciesDto previousRowBean, boolean notPersisted, boolean newRow) {
        SampleSpeciesUIModel sampleModel = getModel();

        SpeciesReference species = tableEditBean.getSpecies();
        log.info(String.format("%s (row %d) selected species %s", sampleModel.getPrefix(), editingRow, species));

        // get default size measure type
        Optional<SizeMeasureTypeReference> defaultSizeMeasureType = sampleModel.getStates().getSpeciesDefaultSizeMeasureType(species);

        // use default size measure type
        sampleModel.getStates().setDefaultSizeMeasureType(defaultSizeMeasureType.orElse(null));
        if (notPersisted && newRow && previousRowBean != null) {
            // use previous subSampleNumber
            tableEditBean.setSubSampleNumber(previousRowBean.getSubSampleNumber());
            // use previous start time and end time
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2718
            tableEditBean.setStartTime(previousRowBean.getStartTime());
            tableEditBean.setEndTime(previousRowBean.getEndTime());
        }
        super.onSelectedRowChanged(ui, editingRow, tableEditBean, previousRowBean, notPersisted, newRow);
    }
}
