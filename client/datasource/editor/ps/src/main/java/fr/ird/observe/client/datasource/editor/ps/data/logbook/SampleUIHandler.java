package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.client.datasource.editor.ps.data.logbook.actions.AddActivitiesFromWell;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ps.logbook.SampleDto;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

import static io.ultreia.java4all.i18n.I18n.t;

public class SampleUIHandler extends GeneratedSampleUIHandler {

    final VetoableChangeListener superSampleChanged;
    /**
     * Listen changes on weight properties and set property {@code SampleDto#weightsComputed} value to {@code false}.
     *
     * @since 9.4.0
     * @see <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2669">issue 2669</a>
     */
    final PropertyChangeListener weightsChanged;

    SampleUIHandler() {
        superSampleChanged = this::onSuperSampleChanged;
        weightsChanged = this::onWeightsChanged;
    }

    private void onWeightsChanged(PropertyChangeEvent event) {
        ((SampleDto) event.getSource()).setWeightsComputed(false);
    }

    @Override
    public void onInit(SampleUI ui) {
        super.onInit(ui);
        ui.getSampleActivityTableModel().init(ui);
        ui.getTotalWeight().setShowReset(false);
        ui.getSmallsWeight().setShowReset(false);
        ui.getBigsWeight().setShowReset(false);
    }

    @Override
    public void stopEditUI() {
        SampleUIModelStates states = getModel().getStates();
        states.removeVetoableChangeListener(SampleDto.PROPERTY_SUPER_SAMPLE, superSampleChanged);
        SampleDto bean = states.getBean();
        bean.removePropertyChangeListener(SampleDto.PROPERTY_SMALLS_WEIGHT, weightsChanged);
        bean.removePropertyChangeListener(SampleDto.PROPERTY_BIGS_WEIGHT, weightsChanged);
        bean.removePropertyChangeListener(SampleDto.PROPERTY_TOTAL_WEIGHT, weightsChanged);
        UIHelper.cancelEditing(ui.getSampleActivityTable());
        super.stopEditUI();
    }

    @Override
    public void startEditUI() {
        SampleUIModelStates states = getModel().getStates();
        states.addVetoableChangeListener(SampleDto.PROPERTY_SUPER_SAMPLE, superSampleChanged);
        SampleDto bean = states.getBean();
        bean.addPropertyChangeListener(SampleDto.PROPERTY_SMALLS_WEIGHT, weightsChanged);
        bean.addPropertyChangeListener(SampleDto.PROPERTY_BIGS_WEIGHT, weightsChanged);
        bean.addPropertyChangeListener(SampleDto.PROPERTY_TOTAL_WEIGHT, weightsChanged);
        super.startEditUI();
    }

    @Override
    protected void installMoveAction() {
        super.installMoveAction();
        // We install the action here to be at proper order in configure menu
        AddActivitiesFromWell.installAction(ui);
    }

    private void onSuperSampleChanged(PropertyChangeEvent event) throws PropertyVetoException {
        boolean oldValue = (boolean) event.getOldValue();
        boolean newValue = (boolean) event.getNewValue();
        SampleDto bean = getModel().getStates().getBean();
        if (oldValue && !newValue) {
            // Was superSample, become not superSample
            if (bean.isPersisted() && bean.getSampleSpeciesSubNumber() > 1) {
                // can't have more than one SampleSpecies
                String message = t("observe.data.ps.logbook.Sample.notSuperSample.with.moreThanOne.sampleSpecies.message");
                displayWarning(t("observe.data.ps.logbook.Sample.notSuperSample.with.moreThanOne.sampleSpecies.title"), message);
                throw new PropertyVetoException(message, event);
            }
        }
    }
}
