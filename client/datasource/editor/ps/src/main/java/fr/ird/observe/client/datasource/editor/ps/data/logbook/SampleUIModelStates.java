package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.data.ps.logbook.SampleDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.common.PersonReference;
import fr.ird.observe.dto.referential.ps.common.SampleTypeReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.beans.PropertyVetoException;
import java.util.List;
import java.util.Set;

@GenerateJavaBeanDefinition
public class SampleUIModelStates extends GeneratedSampleUIModelStates {

    private final SampleActivityTableModel sampleActivityTableModel;
    private final Set<String> availableWellInTrip;

    public SampleUIModelStates(GeneratedSampleUIModel model) {
        super(model);
        this.sampleActivityTableModel = new SampleActivityTableModel(this);
        String tripId = getClientValidationContext().getSelectModel().getPs().getCommonTrip().getId();
        this.availableWellInTrip = getReferenceCache().getDataSource().getPsCommonTripService().getLogbookWellIdsFromWellPlan(tripId);
    }

    public SampleActivityTableModel getSampleActivityTableModel() {
        return sampleActivityTableModel;
    }

    public boolean isWellInTrip(String well) {
        return availableWellInTrip.contains(well);
    }

    public boolean isSuperSample() {
        return getBean().isSuperSample();
    }

    public void setSuperSample(Boolean superSample) {
        boolean oldValue = isSuperSample();
        try {
            fireVetoableChange(SampleDto.PROPERTY_SUPER_SAMPLE, oldValue, superSample);
            getBean().setSuperSample(superSample);
            firePropertyChange(SampleDto.PROPERTY_SUPER_SAMPLE, oldValue, superSample);
        } catch (PropertyVetoException ignored) {
            firePropertyChange(SampleDto.PROPERTY_SUPER_SAMPLE, oldValue);
        }
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(SampleDto.PROPERTY_PERSON, ReferencesFilterHelper.newPredicateList(PersonReference::isPsSampler));
        referenceCache.addReferentialFilter(SampleDto.PROPERTY_SAMPLE_TYPE, ReferencesFilterHelper.newPredicateList(SampleTypeReference::isLogbook));
    }

    @Override
    protected void copyFormToBean(Form<SampleDto> form) {
        // clear well ids table
        getSampleActivityTableModel().clear();
        List<ActivityStubDto> activities = form.getObject().getActivity();
        super.copyFormToBean(form);
        getSampleActivityTableModel().initData(activities, getBean().getSampleActivity());
        firePropertyChange(SampleDto.PROPERTY_SUPER_SAMPLE, isSuperSample());
    }

    @Override
    public SampleDto getBeanToSave() {
        SampleDto bean = getBean();
        bean.setSampleActivity(getSampleActivityTableModel().getValidData());
        bean.autoTrim();
        return bean;
    }
}
