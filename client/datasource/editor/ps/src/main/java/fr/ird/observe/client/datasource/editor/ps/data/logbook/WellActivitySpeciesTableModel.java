package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.client.datasource.editor.api.content.ui.table.EditableTable;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableListProperty;
import fr.ird.observe.client.util.table.EditableTableModelWithCache;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.dto.data.ps.logbook.WellActivitySpeciesDto;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ps.common.WeightCategoryReference;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBoxCellEditor;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import javax.swing.event.AncestorEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 17/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public class WellActivitySpeciesTableModel extends EditableTableModelWithCache<WellActivitySpeciesDto> {

    private static final long serialVersionUID = 1L;
    private List<SpeciesReference> allSpecies = Collections.emptyList();
    private List<WeightCategoryReference> allWeightCategories = Collections.emptyList();
    private FilterableComboBox<SpeciesReference> speciesCellEditorComponent;
    private FilterableComboBox<WeightCategoryReference> weightCategoryCellEditorComponent;

    public static void onInit(WellActivityUI ui) {

        EditableTable<WellActivitySpeciesDto, WellActivitySpeciesTableModel> table = ui.getWellActivitySpeciesTable();
        WellActivitySpeciesTableModel wellActivitySpeciesTableModel = ui.getWellActivitySpeciesTableModel();
        ui.getTableModel().registerInlineModel(wellActivitySpeciesTableModel, table);
        table.setRowHeight(22);
        UIHelper.fixTableColumnWidth(table, 1, 100);
        UIHelper.fixTableColumnWidth(table, 2, 75);
        UIHelper.fixTableColumnWidth(table, 3, 75);
        JXTableUtil.setI18nTableHeaderRenderer(table,
                                               WellActivitySpeciesDto.class,
                                               WellActivitySpeciesDto.PROPERTY_SPECIES,
                                               WellActivitySpeciesDto.PROPERTY_WEIGHT_CATEGORY,
                                               WellActivitySpeciesDto.PROPERTY_WEIGHT,
                                               WellActivitySpeciesDto.PROPERTY_COUNT);

        Decorator speciesDecorator = ui.getHandler().getDecoratorService().getDecoratorByType(SpeciesReference.class);
        Decorator weightCategoryDecorator = ui.getHandler().getDecoratorService().getDecoratorByType(WeightCategoryReference.class);
        Decorator rowDecorator = ui.getHandler().getDecoratorService().getDecoratorByType(WellActivitySpeciesDto.class);

        JXTableUtil.initRenderers(table,
                                  JXTableUtil.newDecoratedRenderer(speciesDecorator),
                                  JXTableUtil.newDecoratedRenderer(weightCategoryDecorator),
                                  JXTableUtil.newEmptyNumberTableCellRenderer(),
                                  JXTableUtil.newEmptyNumberTableCellRenderer());

        wellActivitySpeciesTableModel.speciesCellEditorComponent = UIHelper.initCellEditor(SpeciesReference.class, speciesDecorator);
        wellActivitySpeciesTableModel.weightCategoryCellEditorComponent = UIHelper.initCellEditor(WeightCategoryReference.class, weightCategoryDecorator);
        wellActivitySpeciesTableModel.setRowDecorator(rowDecorator);
        FilterableComboBoxCellEditor speciesCellEditor = new FilterableComboBoxCellEditor(wellActivitySpeciesTableModel.speciesCellEditorComponent) {
            @Override
            public void ancestorAdded(AncestorEvent event) {
                wellActivitySpeciesTableModel.initSpeciesCellEditor();
                super.ancestorAdded(event);
            }
        };
        FilterableComboBoxCellEditor weightCategoryCellEditor = new FilterableComboBoxCellEditor(wellActivitySpeciesTableModel.weightCategoryCellEditorComponent) {
            @Override
            public void ancestorAdded(AncestorEvent event) {
                wellActivitySpeciesTableModel.initWeightCategoryCellEditor();
                super.ancestorAdded(event);
            }
        };
        JXTableUtil.initEditors(table,
                                speciesCellEditor,
                                weightCategoryCellEditor,
                                NumberCellEditor.newFloatColumnEditor(),
                                NumberCellEditor.newIntegerColumnEditor());

        table.init(ui, ui.getValidatorSpecies());
        JXTableUtil.setMinimumHeight(table.getRowHeight() * 8, table, ui.getWellActivitySpeciesScrollPane());
    }

    public WellActivitySpeciesTableModel(EditableListProperty<WellActivitySpeciesDto> listProperty) {
        super(listProperty);
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt0(WellActivitySpeciesDto row, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return row.getSpecies();
            case 1:
                return row.getWeightCategory();
            case 2:
                return row.getWeight();
            case 3:
                return row.getCount();
            default:
                throw new IllegalStateException("Can't come here");
        }
    }

    @Override
    public boolean setValueAt0(WellActivitySpeciesDto row, Object aValue, int rowIndex, int columnIndex) {
        Object oldValue = getValueAt0(row, rowIndex, columnIndex);
        switch (columnIndex) {
            case 0:
                row.setSpecies((SpeciesReference) aValue);
                break;
            case 1:
                row.setWeightCategory((WeightCategoryReference) aValue);
                break;
            case 2:
                row.setWeight((Float) aValue);
                break;
            case 3:
                row.setCount((Integer) aValue);
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return !Objects.equals(oldValue, aValue);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean editable = super.isCellEditable(rowIndex, columnIndex);
        if (editable) {
            switch (columnIndex) {
                case 0:
                    // always editable
                    break;
                case 1:
                    // Editable only if species is selected
                    editable = getValueAt(rowIndex, 0) != null;
                    break;
                case 2:
                case 3:
                    // Editable if species and weight category are selected
                    WellActivitySpeciesDto row = getData(rowIndex);
                    editable = row != null && row.getSpecies() != null && row.getWeightCategory() != null;
                    break;
                default:
                    throw new IllegalStateException("Can't come here");
            }
        }
        return editable;
    }

    @Override
    public void onModifiedChanged(SwingValidator<?> validator, Boolean newValue) {
        super.onModifiedChanged(validator, newValue);
        if (newValue) {
            //FIXME Should review flow it seems it break cache logic, tableEditBean still keep this even after changing row
            listProperty().set(getValidData());
        }
    }

    @Override
    protected WellActivitySpeciesDto createNewRow() {
        return WellActivitySpeciesDto.newDto(new Date());
    }

    public void setAllSpecies(List<SpeciesReference> allSpecies) {
        this.allSpecies = allSpecies;
    }

    public void setAllWeightCategories(List<WeightCategoryReference> allWeightCategories) {
        this.allWeightCategories = allWeightCategories;
    }

    private void initSpeciesCellEditor() {
        List<SpeciesReference> availableSpecies = new ArrayList<>(allSpecies);
        int selectedRowIndex = getSelectedRowIndex();
        int index = 0;
        int weightCategoriesSize = allWeightCategories.size();
        Multimap<SpeciesReference, WeightCategoryReference> used = ArrayListMultimap.create();
        for (WellActivitySpeciesDto row : getData()) {
            if (selectedRowIndex != index++) {
                SpeciesReference species = row.getSpecies();
                WeightCategoryReference weightCategory = row.getWeightCategory();
                if (species != null && weightCategory != null) {
                    used.put(species, weightCategory);
                }
            }
        }
        for (Map.Entry<SpeciesReference, Collection<WeightCategoryReference>> entry : used.asMap().entrySet()) {
            if (weightCategoriesSize == entry.getValue().size()) {
                // No more weight category possible for this species
                availableSpecies.remove(entry.getKey());
            }
        }
        speciesCellEditorComponent.setData(availableSpecies);
    }

    private void initWeightCategoryCellEditor() {
        List<WeightCategoryReference> availableWeightCategories = new ArrayList<>(allWeightCategories);
        int selectedRowIndex = getSelectedRowIndex();
        int index = 0;
        Multimap<SpeciesReference, WeightCategoryReference> used = ArrayListMultimap.create();
        SpeciesReference currentSpecies = null;
        WeightCategoryReference currentWeightCategory = null;
        for (WellActivitySpeciesDto row : getData()) {
            SpeciesReference species = row.getSpecies();
            WeightCategoryReference weightCategory = row.getWeightCategory();
            if (selectedRowIndex != index++) {
                if (species != null && weightCategory != null) {
                    used.put(species, weightCategory);
                }
            } else {
                currentSpecies = species;
                currentWeightCategory = weightCategory;
            }
        }
        if (currentSpecies == null) {
            availableWeightCategories.clear();
        } else {
            Collection<WeightCategoryReference> weightCategoriesUsed = used.get(currentSpecies);
            availableWeightCategories.removeAll(weightCategoriesUsed);
            //FIXME This should never happen...
            if (currentWeightCategory != null && !availableWeightCategories.contains(currentWeightCategory)) {
                availableWeightCategories.add(currentWeightCategory);
            }
        }
        weightCategoryCellEditorComponent.setData(availableWeightCategories);
    }
}

