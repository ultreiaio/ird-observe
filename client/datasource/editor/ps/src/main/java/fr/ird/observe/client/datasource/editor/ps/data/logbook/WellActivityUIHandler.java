package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.data.ps.logbook.WellActivityDto;

import javax.swing.JButton;
import java.awt.Component;

public class WellActivityUIHandler extends GeneratedWellActivityUIHandler {

    @Override
    public void onInit(WellActivityUI ui) {
        super.onInit(ui);
        JButton createActivity = ui.getCreateActivity();
        ui.getActivity().getToolbarRight().add(createActivity);
        createActivity.setFont(createActivity.getFont().deriveFont(10f));
        createActivity.setVisible(getClientConfig().isPsLogbookWellPlanCanCreateActivity());
        WellActivitySpeciesTableModel.onInit(ui);
    }

    @Override
    protected Component getFocusComponentOnSelectedRow(WellActivityUI ui, boolean notPersisted, boolean newRow, WellActivityDto tableEditBean, WellActivityDto previousRowBean) {
        if (notPersisted || tableEditBean.getActivity() == null) {
            return ui.getActivity();
        }
        return ui.getWellActivitySpeciesTable();
    }

    @Override
    protected void onTableModelChanged() {
        super.onTableModelChanged();
        getUi().getTable().invalidate();
    }
}
