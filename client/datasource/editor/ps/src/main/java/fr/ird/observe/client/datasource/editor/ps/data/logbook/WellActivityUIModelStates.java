package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.data.ps.logbook.WellActivityDto;
import fr.ird.observe.dto.data.ps.logbook.WellActivitySpeciesDto;
import fr.ird.observe.dto.data.ps.logbook.WellDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ps.common.WeightCategoryReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.decoration.Decorator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created on 13/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public class WellActivityUIModelStates extends GeneratedWellActivityUIModelStates {
    public static final String CREATE_PREFIX = "__create_";
    final Map<String, ActivityStubDto> activitiesCache;
    private final AtomicInteger fakeId = new AtomicInteger();
    private List<ActivityStubDto> activity;
    private Decorator activityDecorator;

    public WellActivityUIModelStates(GeneratedWellActivityUIModel model) {
        super(model);
        this.activitiesCache = new TreeMap<>();
    }

    public List<WellActivityDto> getPlanNewActivities(WellDto bean) {
        return bean.getWellActivity().stream().filter(d -> d.getActivity().getId().startsWith(CREATE_PREFIX)).collect(Collectors.toList());
    }

    @Override
    public void setValid(boolean valid) {
        super.setValid(valid);
    }

    public void setNewActivity(ActivityStubDto activity) {
        activity.setId(CREATE_PREFIX + fakeId);
        activitiesCache.put(activity.getTopiaId(), activity);
        setActivity(new ArrayList<>(activitiesCache.values()));
        getTableEditBean().setActivity(activity);
    }

    public List<ActivityStubDto> getActivity() {
        return activity;
    }

    public void setActivity(List<ActivityStubDto> activity) {
        List<ActivityStubDto> oldValue = this.activity;
        this.activity = activity;
        if (activity != null) {
            // set them in cache
            this.activitiesCache.putAll(Maps.uniqueIndex(activity, ActivityStubDto::getId));
        }
        firePropertyChange("activity", oldValue, activity);
    }

    @Override
    public void init(ContentUI ui, DefaultUIInitializerResult initializerResult) {
        super.init(ui, initializerResult);
        WellActivityUI realUi = (WellActivityUI) ui;
        activityDecorator = Objects.requireNonNull(realUi.getActivity().getConfig().getDecorator());
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        super.onAfterInitAddReferentialFilters(clientConfig, observeSelectModel, servicesProvider, referenceCache);
        referenceCache.addReferentialFilter(WellActivitySpeciesDto.PROPERTY_SPECIES, ReferencesFilterHelper.newPsSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListSeineLogbookWellPlanId()));
        referenceCache.addReferentialFilter(WellActivitySpeciesDto.PROPERTY_WEIGHT_CATEGORY, ReferencesFilterHelper.newPredicateList(WeightCategoryReference::isWellPlan));
    }

    @Override
    protected void loadReferentialCacheOnOpenForm(Form<WellDto> form) {
        getReferenceCache().loadExtraReferentialReferenceSetsInModel(WellActivitySpeciesDto.class);
        List<SpeciesReference> availableSpecies = getReferenceCache().getReferentialReferences(WellActivitySpeciesDto.PROPERTY_SPECIES);
        List<WeightCategoryReference> availableWeightCategories = getReferenceCache().getReferentialReferences(WellActivitySpeciesDto.PROPERTY_WEIGHT_CATEGORY);
        getTableModel().getWellActivitySpeciesTableModel().setAllSpecies(availableSpecies);
        getTableModel().getWellActivitySpeciesTableModel().setAllWeightCategories(availableWeightCategories);
    }

    @Override
    protected void copyFormToBean(Form<WellDto> form) {
        String tripId = getObserveSelectModel().getPs().getCommonTrip().getId();
        List<ActivityStubDto> activities = getReferenceCache().getDataSource().getPsCommonTripService().getLogbookWellPlanActivities(tripId);
        setActivity(activities);
        getTableModel().clearInlineModels();
        boolean cacheChanged = false;
        WellActivitySpeciesTableModel wellActivitySpeciesTableModel = getTableModel().getWellActivitySpeciesTableModel();
        for (WellActivityDto dto : form.getObject().getWellActivity()) {
            ActivityStubDto activity = dto.getActivity();
            if (activity != null) {
                ActivityStubDto activityFromCache = activitiesCache.get(activity.getTopiaId());
                if (activityFromCache == null) {
                    // suspect activity... but still need to add it to universe
                    activitiesCache.put(activity.getId(), activity);
                    activity.registerDecorator(activityDecorator);
                    cacheChanged = true;
                }
                // use cache activity (it is well decorated)
                dto.setActivity(activityFromCache);
            }
        }
        if (cacheChanged) {
            setActivity(new ArrayList<>(activitiesCache.values()));
        }
        form.getObject().copy(getBean());
        getBean().getWellActivity().forEach(r -> wellActivitySpeciesTableModel.addDecorator(r.getWellActivitySpecies()));
    }

}
