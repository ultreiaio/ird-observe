package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.EditableContentUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.EditableListProperty;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.data.ps.logbook.WellActivityDto;
import fr.ird.observe.dto.data.ps.logbook.WellActivitySpeciesDto;
import fr.ird.observe.dto.data.ps.logbook.WellDto;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;

import javax.swing.JTable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class WellActivityUITableModel extends GeneratedWellActivityUITableModel {

    private static final long serialVersionUID = 1L;
    private final WellActivitySpeciesTableModel wellActivitySpeciesTableModel;

    public WellActivityUITableModel(EditableContentUI<WellDto> parentUi, WellActivityUI ui, WellActivityUIModel model) {
        super(parentUi, ui, model);
        this.wellActivitySpeciesTableModel = new WellActivitySpeciesTableModel(new EditableListProperty<>() {
            @Override
            public Collection<WellActivitySpeciesDto> get() {
                return ui.getTableEditBean().getWellActivitySpecies();
            }

            @Override
            public void set(List<WellActivitySpeciesDto> data) {
                ui.getTableEditBean().setWellActivitySpecies(data);
            }
        });
        this.wellActivitySpeciesTableModel.setCanMoveRows(true);
    }

    public WellActivitySpeciesTableModel getWellActivitySpeciesTableModel() {
        return wellActivitySpeciesTableModel;
    }

    @Override
    public void initTableUISize(JTable table) {
        super.initTableUISize(table);
        UIHelper.fixTableColumnWidth(table, 1, 100);
    }

    @Override
    protected void onBeforeResetRow(int row) {
        super.onBeforeResetRow(row);
        getValueAt(row).setWellActivitySpecies(wellActivitySpeciesTableModel.getCacheForRow(row));
    }

    @Override
    protected void onAfterResetRow(int row) {
        super.onAfterResetRow(row);
        getValidator().doValidate();
        getValidator().setChanged(false);
        wellActivitySpeciesTableModel.setModified(false);
        fireTableDataChanged();
    }

    @Override
    protected void startEditTableEditBeanOnInlineModels() {
        super.startEditTableEditBeanOnInlineModels();
        wellActivitySpeciesTableModel.validate();
    }

    @Override
    protected void onSelectedRowChanged(WellActivityUI ui, int editingRow, WellActivityDto tableEditBean, WellActivityDto previousRowBean, boolean notPersisted, boolean newRow) {
        super.onSelectedRowChanged(ui, editingRow, tableEditBean, previousRowBean, notPersisted, newRow);
        if (ui.getModel().getStates().isReadingMode()) {
            return;
        }
        // Only offers activities not used
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2717
        FilterableComboBox<ActivityStubDto> editor = ui.getActivity();
        List<ActivityStubDto> columnValues = getColumnValues(0);
        columnValues.remove(tableEditBean.getActivity());
        List<ActivityStubDto> values = new LinkedList<>(ui.getModel().getStates().getActivity());
        values.removeAll(columnValues);
        editor.setData(values);
    }

}
