package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.collect.Maps;
import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.actions.open.ContentOpenWithValidator;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveAction;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUIOpenExecutor;
import fr.ird.observe.client.datasource.editor.api.content.data.open.actions.SaveContentOpenableUIAdapter;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.client.datasource.editor.ps.data.common.TripLogbookUINavigationNode;
import fr.ird.observe.client.datasource.editor.ps.data.common.TripUINavigationNode;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.actions.AddActivitiesFromSample;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.client.util.init.DefaultUIInitializerResult;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.data.ps.logbook.RouteDto;
import fr.ird.observe.dto.data.ps.logbook.RouteReference;
import fr.ird.observe.dto.data.ps.logbook.WellActivityDto;
import fr.ird.observe.dto.data.ps.logbook.WellDto;
import fr.ird.observe.dto.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.data.OpenableService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class WellUIHandler extends GeneratedWellUIHandler {
    private static final Logger log = LogManager.getLogger(WellUIHandler.class);

    @Override
    protected void onBeforeInit(WellUI ui, ContentUIInitializer<WellUI> initializer) {
        initializer.registerDependencies(WellActivityUIModel.create(ui));
    }

    @Override
    protected ContentOpenWithValidator<WellUI> createContentOpen(WellUI ui) {
        ContentOpenableUIOpenExecutor<WellDto, WellUI> executor = new ContentOpenableUIOpenExecutor<>();
        return new ContentOpenWithValidator<>(ui, executor, executor) {
            @Override
            public DefaultUIInitializerResult init(ContentUIInitializer<WellUI> initializer) {
                DefaultUIInitializerResult result = super.init(initializer);
                tabbedPaneValidator.getTabValidators().put(ui.getWellActivityTab(), ui.getValidator());
                String type = ObserveI18nDecoratorHelper.getType(WellActivityDto.class) + " - ";
                InputMap inputMap = ui.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
                ActionMap actionMap = ui.getActionMap();

                AddActivitiesFromSample.installAction(ui);
                prefixAction(ui.getWellActivityUI().getDeleteEntry(), type);
                prefixAction(ui.getWellActivityUI().getShowTechnicalInformations(), type);
                prefixAction(ui.getFillActivitiesFromSample(), type);
                registerInnerAction(ui.getWellActivityUI().getResetEntry(), inputMap, actionMap);
                registerInnerAction(ui.getWellActivityUI().getSaveEntry(), inputMap, actionMap);
                registerInnerAction(ui.getWellActivityUI().getSaveAndNewEntry(), inputMap, actionMap);

                addConfigureActions(ui, false);
                initTabUI(ui.getWellActivityUI(), ui.getWellActivityPanel(), 1, d -> ui.getModel().getStates().getBean().setWellActivity(d.getWellActivity()));
                getConfigureActions().remove(ui, ui.getFillActivitiesFromSample());
                // Big hack to put at the good place the ui.getFillActivitiesFromSample() action in the menu
                getConfigureActions().get(ui.getWellActivityUI()).remove(ui.getWellActivityUI().getShowTechnicalInformations());
                getConfigureActions().put(ui.getWellActivityUI(), ui.getFillActivitiesFromSample());
                getConfigureActions().put(ui.getWellActivityUI(), ui.getWellActivityUI().getShowTechnicalInformations());
                return result;
            }
        };
    }

    @Override
    public void onMainTabChanged(int previousIndex, int selectedIndex) {
        super.onMainTabChanged(previousIndex, selectedIndex);
        WellActivityUI subUi = ui.getWellActivityUI();
        boolean activityTab = selectedIndex == 1;
        subUi.getSelectToolbar().setVisible(activityTab && subUi.getTableModel().getRowCount() > 1);
        getContentOpen().updateConfigurePopup(ui, true);
        if (activityTab) {
            ui.getConfigurePopup().addSeparator();
            getContentOpen().updateConfigurePopup(subUi, false);
        }
    }

    @Override
    protected void installSaveAction() {
        List<WellActivityDto> wellPlanWithNewActivities = new LinkedList<>();
        ClientValidationContext validationContext = getClientValidationContext();
        TripDto trip = validationContext.getCurrentPsCommonTrip();
        SaveAction
                .create(ui, WellDto.class)
                .on(ui.getModel()::toSaveRequest)
                .call((r, d) -> save(d, trip, wellPlanWithNewActivities))
                .then(new SaveContentOpenableUIAdapter<>() {
                    @Override
                    public void adaptUi(DataSourceEditor dataSourceEditor, WellUI ui, boolean notPersisted, WellDto savedBean) {
                        if (wellPlanWithNewActivities.isEmpty()) {
                            // default save
                            super.adaptUi(dataSourceEditor, ui, notPersisted, savedBean);
                            return;
                        }

                        NavigationTree tree = dataSourceEditor.getNavigationUI().getTree();

                        // reload entire trip then default save
                        TripLogbookUINavigationNode parentNode = ui.getModel().getSource().getParent().getParent();

                        validationContext.reset();

                        TripUINavigationNode parent = parentNode.getParent();
                        parent.updateNode();
                        parentNode = parent.findChildByType(parentNode.getClass());
                        WellUINavigationNode node2 = parentNode.getWellListUINavigationNode().getWellUINavigationNode(savedBean.getId());
                        afterNodeUpdated(dataSourceEditor, ui, tree, node2, notPersisted, savedBean);
                    }
                })
                .install(ui.getSave());
    }

    private SaveResultDto save(WellDto beanToSave, TripDto trip, List<WellActivityDto> wellPlanWithNewActivities) throws ConcurrentModificationException {
        wellPlanWithNewActivities.addAll(getModel().getStates().getWellActivityUIModel().getStates().getPlanNewActivities(beanToSave));
        if (wellPlanWithNewActivities.isEmpty()) {
            // default save
            return getOpenableService().save(trip.getId(), beanToSave);
        }
        Map<Date, RouteReference> routeByDate = new TreeMap<>(Maps.uniqueIndex(trip.getRouteLogbook(), RouteReference::getDate));

        OpenableService openableService = getOpenableService();
        ReferentialLocale referentialLocale = getClientConfig().getReferentialLocale();
        // need to create all routes/activities
        for (WellActivityDto wellActivityDto : wellPlanWithNewActivities) {
            log.info("Will create activity for wellActivity: " + wellActivityDto);
            ActivityStubDto activity = wellActivityDto.getActivity();
            Date date = activity.getDate();
            RouteReference route = routeByDate.get(date);
            if (route == null) {

                log.info("Will create route: " + date);
                Form<RouteDto> routeDtoForm = openableService.preCreate(RouteDto.class, trip.getId());
                RouteDto dto = routeDtoForm.getObject();
                dto.setDate(date);
                SaveResultDto save = openableService.save(trip.getId(), dto);
                save.toDto(dto);
                beanToSave.setLastUpdateDate(save.getLastUpdateDate());
                routeByDate.put(date, route = dto.toReference(referentialLocale));
            }
            log.info("Will create activity: " + activity);

            Form<ActivityDto> dtoForm = openableService.preCreate(ActivityDto.class, route.getId());
            ActivityDto dto = dtoForm.getObject();
            dto.setTopiaCreateDate(activity.getTopiaCreateDate());
            dto.setDate(date);
            dto.setTime(activity.getTime());
            dto.setVesselActivity(activity.getVesselActivity());
            dto.setComment("Créée depuis le plan de cuve : " + beanToSave);
            SaveResultDto save = openableService.save(route.getId(), dto);
            save.toDto(dto);
            beanToSave.setLastUpdateDate(save.getLastUpdateDate());
            activity = dto.toStub();

            log.info("Set new activity to wellPlan: " + activity);
            wellActivityDto.setActivity(activity);
            wellActivityDto.setId(null);
            wellActivityDto.getWellActivitySpecies().forEach(t -> t.setId(null));
        }
        return getOpenableService().save(trip.getId(), beanToSave);
    }

}
