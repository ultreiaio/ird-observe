package fr.ird.observe.client.datasource.editor.ps.data.logbook;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.logbook.WellDto;
import fr.ird.observe.dto.form.Form;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class WellUIModelStates extends GeneratedWellUIModelStates {

    private WellActivityUIModel wellActivityUIModel;

    public WellUIModelStates(GeneratedWellUIModel model) {
        super(model);
    }

    public WellActivityUIModel getWellActivityUIModel() {
        return wellActivityUIModel;
    }

    public void setWellActivityUIModel(WellActivityUIModel wellActivityUIModel) {
        WellActivityUIModel oldValue = this.wellActivityUIModel;
        this.wellActivityUIModel = wellActivityUIModel;
        firePropertyChange("wellActivityUIModel", oldValue, wellActivityUIModel);
    }

    @Override
    public WellDto getBeanToSave() {
        WellDto bean = super.getBeanToSave();
        bean.setWellActivity(getWellActivityUIModel().getStates().getBeanToSave().getWellActivity());
        return bean;
    }

    @Override
    protected void loadReferentialCacheOnOpenForm(Form<WellDto> form) {
        super.loadReferentialCacheOnOpenForm(form);
        getWellActivityUIModel().getStates().loadReferentialCacheOnOpenForm(form);
    }

    @Override
    protected void copyFormToBean(Form<WellDto> form) {
        super.copyFormToBean(form);
        getWellActivityUIModel().getStates().copyFormToBean(form);
    }
}
