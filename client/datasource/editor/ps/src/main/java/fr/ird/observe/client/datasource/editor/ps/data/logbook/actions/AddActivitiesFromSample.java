package fr.ird.observe.client.datasource.editor.ps.data.logbook.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.data.open.actions.ContentOpenableUIActionSupport;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.WellActivityUI;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.WellActivityUIModel;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.WellActivityUITableModel;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.WellUI;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.WellUIModelStates;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.data.ps.logbook.WellActivityDto;
import fr.ird.observe.dto.data.ps.logbook.WellDto;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created at 01/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class AddActivitiesFromSample extends ContentOpenableUIActionSupport<WellDto, WellUI> implements ConfigureMenuAction<WellUI> {

    private static final Logger log = LogManager.getLogger(AddActivitiesFromSample.class);

    public AddActivitiesFromSample() {
        super(WellDto.class, I18n.n("observe.data.ps.logbook.Well.action.addActivitiesFromSample"), I18n.n("observe.data.ps.logbook.Well.action.addActivitiesFromSample.tip"), "generate", ObserveKeyStrokesEditorApi.KEY_STROKE_GENERATE);
    }

    public static void installAction(WellUI ui) {
        AddActivitiesFromSample action = new AddActivitiesFromSample();
        ui.getModel().getStates().addPropertyChangeListener(ContentOpenableUIModelStates.PROPERTY_MODE, action::updateEnableState);
        ui.getModel().getStates().getBean().addPropertyChangeListener(WellDto.PROPERTY_WELL, action::updateEnableState);
        ui.getModel().getStates().getBean().addPropertyChangeListener(WellDto.PROPERTY_WELL_ACTIVITY, action::updateEnableState);
        AddActivitiesFromSample.init(ui, ui.getFillActivitiesFromSample(), action);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, WellUI ui) {
        WellUIModelStates states = ui.getModel().getStates();
        WellDto bean = states.getBean();
        String well = bean.getWell();
        log.info("Will seek from activities for sample: {}", well);
        WellActivityUI wellActivityUI = ui.getWellActivityUI();
        WellActivityUITableModel tableModel = wellActivityUI.getTableModel();
        WellActivityUIModel model = wellActivityUI.getModel();
        List<ActivityStubDto> availableActivities = wellActivityUI.getModel().getStates().getActivity();
        Map<String, ActivityStubDto> activitiesCache = new TreeMap<>(Maps.uniqueIndex(availableActivities, ActivityStubDto::getTopiaId));
        String tripId = getClientValidationContext().getSelectModel().getPs().getCommonTrip().getId();
        Set<String> logbookWellActivityFromSample = getServicesProvider().getPsCommonTripService().createLogbookWellActivityFromSample(tripId, well);
        for (String activityId : logbookWellActivityFromSample) {
            log.info("Create Well activity for activity: {}", activityId);
            ActivityStubDto activity = activitiesCache.get(activityId);
            WellActivityDto wellActivityDto = model.newTableEditBean();
            wellActivityDto.setActivity(activity);
            tableModel.addNewEntry(wellActivityDto);
            wellActivityUI.getSaveEntry().setEnabled(true);
            try {
                wellActivityUI.getSaveEntry().doClick();
            } finally {
                wellActivityUI.getSaveEntry().setEnabled(false);
            }
        }
    }

    private void updateEnableState(PropertyChangeEvent evt) {
        WellUIModelStates states = ui.getModel().getStates();
        boolean readingMode = states.isReadingMode();
        String well = states.getBean().getWell();
        List<WellActivityDto> wellActivity = states.getBean().getWellActivity();
        setEnabled(!readingMode && well != null && !well.isEmpty() && wellActivity.isEmpty());
    }
}
