package fr.ird.observe.client.datasource.editor.ps.data.logbook.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.actions.ConfigureMenuAction;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.data.open.actions.ContentOpenableUIActionSupport;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.SampleActivityTableModel;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.SampleUI;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.SampleUIModelStates;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.data.ps.logbook.SampleActivityDto;
import fr.ird.observe.dto.data.ps.logbook.SampleDto;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created at 01/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class AddActivitiesFromWell extends ContentOpenableUIActionSupport<SampleDto, SampleUI> implements ConfigureMenuAction<SampleUI> {

    private static final Logger log = LogManager.getLogger(AddActivitiesFromWell.class);

    public AddActivitiesFromWell() {
        super(SampleDto.class, I18n.n("observe.data.ps.logbook.Sample.action.addActivitiesFromWell"), I18n.n("observe.data.ps.logbook.Sample.action.addActivitiesFromWell.tip"), "generate", ObserveKeyStrokesEditorApi.KEY_STROKE_GENERATE);
    }

    public static void installAction(SampleUI ui) {
        AddActivitiesFromWell action = new AddActivitiesFromWell();
        ui.getModel().getStates().addPropertyChangeListener(ContentOpenableUIModelStates.PROPERTY_MODE, action::updateEnableState);
        ui.getModel().getStates().getBean().addPropertyChangeListener(SampleDto.PROPERTY_WELL, action::updateEnableState);
        ui.getModel().getStates().getBean().addPropertyChangeListener(SampleDto.PROPERTY_SAMPLE_ACTIVITY, action::updateEnableState);
        AddActivitiesFromWell.init(ui, ui.getFillActivitiesFromWell(), action);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, SampleUI ui) {
        SampleUIModelStates states = ui.getModel().getStates();
        SampleDto bean = states.getBean();
        String well = bean.getWell();
        log.info("Will seek from activities from well: {}", well);
        SampleActivityTableModel tableModel = states.getSampleActivityTableModel();
        List<ActivityStubDto> availableActivities = tableModel.getActivities();
        Map<String, ActivityStubDto> activitiesCache = new TreeMap<>(Maps.uniqueIndex(availableActivities, ActivityStubDto::getTopiaId));
        String tripId = getClientValidationContext().getSelectModel().getPs().getCommonTrip().getId();
        Set<String> logbookSampleActivityFromWellPlan = getServicesProvider().getPsCommonTripService().createLogbookSampleActivityFromWellPlan(tripId, well);
        List<SampleActivityDto> sampleActivities = new LinkedList<>();
        for (String activityId  : logbookSampleActivityFromWellPlan) {
            log.info("Create Sample activity for activity: {}", activityId);
            ActivityStubDto activity = activitiesCache.get(activityId);
            SampleActivityDto sampleActivityDto = tableModel.createNewRow();
            sampleActivityDto.setActivity(activity);
            sampleActivityDto.setWeightedWeight(null);
            sampleActivityDto.setWeightedWeightComputed(false);
            sampleActivities.add(sampleActivityDto);
        }
        bean.setSampleActivity(sampleActivities);
        tableModel.setData(sampleActivities);
    }

    private void updateEnableState(PropertyChangeEvent evt) {
        SampleUIModelStates states = ui.getModel().getStates();
        boolean readingMode = states.isReadingMode();
        String well = states.getBean().getWell();
        List<SampleActivityDto> sampleActivity = states.getBean().getSampleActivity();
        setEnabled(!readingMode && well != null && !well.isEmpty() && states.isWellInTrip(well.trim()) && sampleActivity.isEmpty());
    }
}
