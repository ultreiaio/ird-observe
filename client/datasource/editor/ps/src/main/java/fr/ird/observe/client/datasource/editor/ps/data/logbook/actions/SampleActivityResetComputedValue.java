package fr.ird.observe.client.datasource.editor.ps.data.logbook.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.ps.data.logbook.SampleActivityTableModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ps.logbook.SampleActivityDto;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.number.NumberEditor;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 17/04/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public class SampleActivityResetComputedValue extends AbstractAction implements PropertyChangeListener {

    private final SampleActivityTableModel sampleActivityTableModel;
    private final NumberEditor delegate;
    private final JButton editor;
    private SampleActivityDto bean;
    private int row;

    public SampleActivityResetComputedValue(SampleActivityTableModel sampleActivityTableModel, NumberEditor delegate, JButton editor) {
        this.sampleActivityTableModel = sampleActivityTableModel;
        this.delegate = delegate;
        this.editor = Objects.requireNonNull(editor);
        putValue(ACTION_COMMAND_KEY, getClass().getName());
        putValue(SMALL_ICON, SwingUtil.getUIManagerActionIcon("data-calcule"));
        editor.setAction(this);
        editor.setDisabledIcon(UIHelper.getUIManagerActionIcon("data-observe"));
        delegate.getRightToolbar().add(editor);
        delegate.getRightToolbar().setVisible(true);
        delegate.getRightToolbar().remove(delegate.getShowPopUpButton());
    }

    public void init(SampleActivityDto bean, int row) {
        this.bean = bean;
        this.row = row;
        this.bean.removePropertyChangeListener(SampleActivityDto.PROPERTY_WEIGHTED_WEIGHT, this);
        this.bean.addPropertyChangeListener(SampleActivityDto.PROPERTY_WEIGHTED_WEIGHT, this);
        editor.setEnabled(bean.isWeightedWeightComputed());
        editor.setToolTipText(getToolTipText(bean.isWeightedWeightComputed()));
    }

    public void exit() {
        if (bean != null) {
            bean.removePropertyChangeListener(SampleActivityDto.PROPERTY_WEIGHTED_WEIGHT, this);
            bean = null;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        sampleActivityTableModel.setValueAt(false, row, 2);
        editor.setToolTipText(getToolTipText(bean.isWeightedWeightComputed()));
        editor.setEnabled(false);
        delegate.grabFocus();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Float newValue = (Float) evt.getNewValue();
        // when computed value change, let's update tooltipText
        editor.setToolTipText(getToolTipText(bean.isWeightedWeightComputed()));
        // can use this action only if there is a value in model
        editor.setEnabled(newValue != null);
    }

    protected String getToolTipText(boolean weightedWeightComputed) {
        String result;
        if (weightedWeightComputed) {
            result = t("observe.data.ps.logbook.SampleActivity.weightedWeightComputed.computed.tip");
        } else {
            result = t("observe.data.ps.logbook.SampleActivity.weightedWeightComputed.observed.tip");
        }
        return result;
    }
}
