package fr.ird.observe.client.datasource.editor.ps.data.logbook.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.open.actions.ContentOpenableUIActionSupport;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.SampleUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ps.logbook.SampleDto;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * To reset the three weights values on {@link SampleDto}.
 * <p>
 * Created at 11/12/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 * @see <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2957">issue 2957</a>
 */
public class SampleUIResetWeights extends ContentOpenableUIActionSupport<SampleDto, SampleUI> {

    public SampleUIResetWeights() {
        super(SampleDto.class, null, n("bean.action.reset.tip"), "combobox-reset", null);
    }

    @Override
    protected final void doActionPerformed(ActionEvent e, SampleUI ui) {
        ui.getModel().getStates().getBean().setTotalWeight(null);
        ui.getModel().getStates().getBean().setSmallsWeight(null);
        ui.getModel().getStates().getBean().setBigsWeight(null);
    }
}
