package fr.ird.observe.client.datasource.editor.ps.data.logbook.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.open.actions.ContentOpenableUIActionSupport;
import fr.ird.observe.client.datasource.editor.ps.ObservePsKeyStrokes;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.SampleUI;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ps.logbook.SampleDto;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created at 12/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
public class SampleUIResetWeightsComputed extends ContentOpenableUIActionSupport<SampleDto, SampleUI> {

    public SampleUIResetWeightsComputed() {
        super(SampleDto.class, null, null, "data-calcule", ObservePsKeyStrokes.KEY_STROKE_RESET_COMPUTED_VALUE);
    }

    @Override
    protected final void doActionPerformed(ActionEvent e, SampleUI ui) {
        ui.getModel().getStates().getBean().setWeightsComputed(false);
    }

    @Override
    public final void defaultInit(InputMap inputMap, ActionMap actionMap) {
        super.defaultInit(inputMap, actionMap);
        editor.setDisabledIcon(UIHelper.getUIManagerActionIcon("data-observe"));
        SampleDto bean = ui.getModel().getStates().getBean();
        bean.addPropertyChangeListener(SampleDto.PROPERTY_WEIGHTS_COMPUTED, evt -> {
            boolean newValue = (boolean) evt.getNewValue();
            updateAction(newValue);
        });
        updateAction(bean.isWeightsComputed());
    }

    protected String getToolTipText(boolean weightedWeightComputed) {
        String result;
        if (weightedWeightComputed) {
            result = t("observe.data.ps.logbook.Sample.weightsComputed.computed.tip");
        } else {
            result = t("observe.data.ps.logbook.Sample.weightsComputed.observed.tip");
        }
        return result;
    }

    protected void updateAction(boolean newValue) {
        // when computed value change, let's update tooltipText
        String toolTipText = getToolTipText(newValue);
        if (newValue) {
            String acceleratorStr = SwingUtil.keyStrokeToStr(getKeyStroke());
            toolTipText += acceleratorStr;
        }
        editor.setToolTipText(toolTipText);
        // can use this action only if there is a value in model
        editor.setEnabled(newValue);
    }
}
