package fr.ird.observe.client.datasource.editor.ps.data.logbook.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.ContentTableUIActionSupport;
import fr.ird.observe.client.datasource.editor.ps.ObservePsKeyStrokes;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.LightActivityUI;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.LightActivityUIModel;
import fr.ird.observe.client.datasource.editor.ps.data.logbook.WellActivityUI;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;
import fr.ird.observe.client.datasource.validation.ClientValidationContext;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import io.ultreia.java4all.validation.bean.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 29/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class WellActivityUICreateActivity extends ContentTableUIActionSupport<WellActivityUI> {

    private static final Logger log = LogManager.getLogger(WellActivityUICreateActivity.class);

    public WellActivityUICreateActivity() {
        super("", t("observe.data.ps.logbook.WellPlan.create.activity.tip"), "add", ObservePsKeyStrokes.KEY_STROKE_CREATE_ACTIVITY);
    }

    @Override
    protected void doActionPerformed(ActionEvent event, WellActivityUI ui) {

        ClientValidationContext context = getClientValidationContext();
        TripDto trip = context.getCurrentPsCommonTrip();
        LightActivityUIModel model = new LightActivityUIModel();
        model.setTrip(trip);
        List<VesselActivityReference> vesselActivityList = getDataSource().getReferentialService().getReferenceSet(VesselActivityReference.class, null).stream().filter(VesselActivityReference::isEnabled).collect(Collectors.toList());
        model.setVesselActivityUniverse(vesselActivityList);
        VesselActivityReference vesselActivity = vesselActivityList.stream().filter(v -> v.getId().equals(ProtectedIdsPs.PS_COMMON_VESSEL_ACTIVITY_ID_FOR_WELL_PLAN)).findFirst().orElseThrow();
        model.setVesselActivity(vesselActivity);
        JAXXInitialContext initialContext = UIHelper.initialContext(ui, model)
                                                    .add(getDecoratorService())
                                                    .add(getDataSourcesManager().getMainDataSource());
        LightActivityUI configPanel = new LightActivityUI(initialContext);

        String replaceText = t("observe.ui.choice.save");
        Object[] options = {replaceText, t("observe.ui.choice.cancel")};
        JOptionPane pane = new JOptionPane(configPanel, JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, options[0]);
        JButton saveButton = Objects.requireNonNull(UsageUIHandlerSupport.findButton(pane, replaceText));
        configPanel.getValidator().addPropertyChangeListener(BeanValidator.VALID_PROPERTY, e -> onValidChanged(saveButton, (Boolean) e.getNewValue()));
        onValidChanged(saveButton, configPanel.getModel().isValid());

        saveButton.addFocusListener(new FocusAdapter() {

            boolean init;

            @Override
            public void focusGained(FocusEvent e) {
                if (!init) {
                    init = true;
                    log.info("Add initial focus");
                    SwingUtilities.invokeLater(configPanel.getDate()::requestFocusInWindow);
                }
            }
        });
        pane.putClientProperty(UIHelper.NO_PACK, true);
        pane.putClientProperty(UIHelper.FIX_SIZE, true);
        pane.setPreferredSize(new Dimension(800, 400));
        int response = askToUser(pane, t("observe.data.ps.logbook.Activity.create.title"), options);

        if (response != 0) {
            // user cancel
            return;
        }
        ActivityStubDto activity = model.toActivity();
        ui.getStates().setNewActivity(activity);
    }

    private void onValidChanged(JButton saveButton, Boolean newValue) {
        saveButton.setEnabled(Objects.equals(true, newValue));
    }
}
