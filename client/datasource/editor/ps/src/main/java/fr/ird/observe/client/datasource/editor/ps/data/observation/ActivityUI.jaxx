<!--
  #%L
  ObServe Client :: DataSource :: Editor :: PS
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI
        beanScope="bean" i18n="fr.ird.observe.dto.data.ps.observation.ActivityDto" superGenericType='ActivityDto, ActivityUI'>

    <import>
      fr.ird.observe.dto.data.ps.observation.ActivityDto
      fr.ird.observe.dto.data.ps.observation.SetDto
      fr.ird.observe.dto.data.ps.observation.FloatingObjectDto
      fr.ird.observe.dto.referential.common.DataQualityReference
      fr.ird.observe.dto.referential.common.FpaZoneReference
      fr.ird.observe.dto.referential.ps.common.ObservedSystemReference
      fr.ird.observe.dto.referential.ps.common.VesselActivityReference
      fr.ird.observe.dto.referential.ps.observation.SurroundingActivityReference
      fr.ird.observe.dto.referential.ps.common.ReasonForNoFishingReference
      fr.ird.observe.dto.referential.ps.observation.DetectionModeReference
      fr.ird.observe.dto.referential.common.WindReference

      io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox
      io.ultreia.java4all.jaxx.widgets.list.DoubleList
      org.nuiton.jaxx.widgets.temperature.TemperatureFormat
      org.nuiton.jaxx.widgets.temperature.TemperatureEditor
      org.nuiton.jaxx.widgets.temperature.TemperatureEditorConfig
      org.nuiton.jaxx.widgets.datetime.TimeEditor
      org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor
      org.nuiton.jaxx.widgets.number.NumberEditor
      org.nuiton.jaxx.widgets.text.BigTextEditor
      org.nuiton.jaxx.widgets.text.NormalTextEditor

      static fr.ird.observe.client.util.UIHelper.getStringValue
      static io.ultreia.java4all.i18n.I18n.n
    </import>

    <ActivityUIModel id='model' constructorParams='@override:getNavigationSource(this)'/>
    <ActivityUIModelStates id='states'/>
    <ActivityDto id='bean'/>

    <BeanValidator id='validator' autoField='true' beanClass='fr.ird.observe.dto.data.ps.observation.ActivityDto'
                   errorTableModel='{getErrorTableModel()}' context='create'>
        <field name='longitude' component='coordinate'/>
        <field name='latitude' component='coordinate'/>
        <field name='quadrant' component='coordinate'/>
        <field name='floatingObjectEmpty' component='addFloatingObject'/>
    </BeanValidator>

    <JPanel id="contentBody" layout='{new BorderLayout()}'>
        <Table insets="0" fill="both" constraints='BorderLayout.CENTER'>
            <row>
                <cell anchor="north" weightx="1">
                    <JTabbedPane id='mainTabbedPane'>
                        <tab id='generalTab' i18nProperty="">
                            <Table fill="both">
                                <!-- heure observation  -->
                                <row>
                                    <cell columns="2" weightx="1">
                                        <TimeEditor id='time' styleClass="i18n"/>
                                    </cell>
                                </row>

                                <!--  position -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='coordinateLabel'/>
                                    </cell>
                                    <cell anchor='east' fill="both">
                                        <CoordinatesEditor id='coordinate'/>
                                    </cell>
                                </row>

                                <!-- activity vessel -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='vesselActivityLabel' styleClass="information"/>
                                    </cell>
                                    <cell anchor='east' weightx="1">
                                        <FilterableComboBox id='vesselActivity' genericType='VesselActivityReference'/>
                                    </cell>
                                </row>

                                <!--  activity environnante -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='surroundingActivityLabel'/>
                                    </cell>
                                    <cell anchor='east' weightx="1">
                                        <FilterableComboBox id='surroundingActivity' genericType='SurroundingActivityReference'/>
                                    </cell>
                                </row>

                                <!--  previousFpaZone -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='previousFpaZoneLabel'/>
                                    </cell>
                                    <cell anchor='east' weightx="1">
                                        <FilterableComboBox id='previousFpaZone' genericType='FpaZoneReference'/>
                                    </cell>
                                </row>

                                <!--  currentFpaZone -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='currentFpaZoneLabel'/>
                                    </cell>
                                    <cell anchor='east' weightx="1">
                                        <FilterableComboBox id='currentFpaZone' genericType='FpaZoneReference'/>
                                    </cell>
                                </row>

                                <!--  nextFpaZone -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='nextFpaZoneLabel'/>
                                    </cell>
                                    <cell anchor='east' weightx="1">
                                        <FilterableComboBox id='nextFpaZone' genericType='FpaZoneReference'/>
                                    </cell>
                                </row>

                                <!-- id ers  -->
                                <row>

                                    <cell anchor='west'>
                                        <JLabel id='ersIdLabel'/>
                                    </cell>
                                    <cell anchor='east' weightx="1" fill="both">
                                        <NormalTextEditor id='ersId'/>
                                    </cell>
                                </row>

                                <!-- data quality -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='dataQualityLabel'/>
                                    </cell>
                                    <cell anchor='east' weightx="1" fill="both">
                                        <FilterableComboBox id='dataQuality' genericType='DataQualityReference'/>
                                    </cell>
                                </row>

                            </Table>
                        </tab>

                        <tab id='measurementsTab' i18nProperty="">
                            <Table fill="both">
                                <!-- vitesse vessel -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='vesselSpeedLabel'/>
                                    </cell>
                                    <cell anchor='east' weightx="1" fill="both">
                                        <NumberEditor id='vesselSpeed' styleClass="float2"/>
                                    </cell>
                                </row>

                                <!-- température de surface -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='seaSurfaceTemperatureLabel'/>
                                    </cell>
                                    <cell anchor='east' fill="both">
                                        <TemperatureEditor id='seaSurfaceTemperature'/>
                                    </cell>
                                </row>

                                <!--  vent beaufort -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='windLabel'/>
                                    </cell>
                                    <cell anchor='east'>
                                        <FilterableComboBox id='wind' genericType='WindReference'/>
                                    </cell>
                                </row>

                                <!--  mode de détection -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='detectionModeLabel'/>
                                    </cell>
                                    <cell anchor='east'>
                                        <FilterableComboBox id='detectionMode' genericType='DetectionModeReference'/>
                                    </cell>
                                </row>

                                <!-- cause non coups senne  -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='reasonForNoFishingLabel'/>
                                    </cell>
                                    <cell anchor='east'>
                                        <FilterableComboBox id='reasonForNoFishing' genericType='ReasonForNoFishingReference'/>
                                    </cell>
                                </row>
                                <row>
                                    <cell columns="2" weighty="1">
                                        <JLabel styleClass="skipI18n"/>
                                    </cell>
                                </row>
                            </Table>
                        </tab>

                        <tab id="observedSystemTab" i18nProperty="">
                            <Table fill="both">
                                <row>
                                    <cell columns="2" fill="both" weighty="0.7">
                                        <!-- observed system -->
                                        <DoubleList id='observedSystem' genericType='ObservedSystemReference'/>
                                    </cell>
                                </row>
                                <!-- distance au système observe -->
                                <row>
                                    <cell anchor='west'>
                                        <JLabel id='observedSystemDistanceLabel'/>
                                    </cell>
                                    <cell weightx="1" fill="both">
                                        <NumberEditor id='observedSystemDistance' styleClass="float2"/>
                                    </cell>
                                </row>
                            </Table>
                        </tab>

                    </JTabbedPane>
                </cell>
            </row>

            <!--  comment -->
            <row>
                <cell columns='2' weighty="1">
                    <BigTextEditor id="comment"/>
                </cell>
            </row>
        </Table>
    </JPanel>

    <JMenuItem id='addSet'/>
    <JMenuItem id='addFloatingObject'/>
</fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUI>
