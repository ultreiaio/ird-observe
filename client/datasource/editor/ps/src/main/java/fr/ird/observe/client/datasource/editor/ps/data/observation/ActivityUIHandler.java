/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.ps.data.observation;

import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.NodeChildrenUpdate;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveAction;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.open.actions.SaveContentOpenableUIAdapter;
import fr.ird.observe.client.datasource.editor.api.navigation.NavigationTree;
import fr.ird.observe.dto.data.ps.observation.ActivityDto;
import fr.ird.observe.dto.data.ps.observation.ActivityReference;

import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ActivityUIHandler extends GeneratedActivityUIHandler {
    private final PropertyChangeListener fpaZoneChangedChanged;

    public ActivityUIHandler() {
        fpaZoneChangedChanged = evt -> onFpaZoneChanged((Boolean) evt.getNewValue());
    }

    @Override
    protected void installSaveAction() {
        SaveAction.create(ui, ActivityDto.class)
                  .on(ui.getModel()::toSaveRequest)
                  .call((r, d) -> getOpenableService().save(r.getParentId(), d))
                  .then(new SaveContentOpenableUIAdapter<>(ActivityDto::isStrongSetOperation, ActivityUI::getAddSet) {
                      @Override
                      protected void afterNodeUpdated(DataSourceEditor dataSourceEditor, ActivityUI ui, NavigationTree tree, ContentOpenableUINavigationNode node, boolean notPersisted, ActivityDto bean) {
                          ActivityReference reference = (ActivityReference) node.getReference();
                          bean.setStatistics(reference.statistics());
                          ActivityUINavigationCapability.fillAfterBuilder(NodeChildrenUpdate.afterUpdate(node, bean)).update(tree);
                          super.afterNodeUpdated(dataSourceEditor, ui, tree, node, notPersisted, bean);
                      }
                  })
                  .install(ui.getSave());
    }

    @Override
    public void startEditUI() {
        ActivityDto bean = getModel().getStates().getBean();
        bean.removePropertyChangeListener(ActivityDto.PROPERTY_CHANGED_ZONE_OPERATION, fpaZoneChangedChanged);
        onFpaZoneChanged(bean.isChangedZoneOperation());
        super.startEditUI();
        bean.addPropertyChangeListener(ActivityDto.PROPERTY_CHANGED_ZONE_OPERATION, fpaZoneChangedChanged);
    }

    void onFpaZoneChanged(boolean newValue) {
        ActivityDto bean = getModel().getStates().getBean();
        if (Objects.equals(true, newValue)) {
            bean.setCurrentFpaZone(null);
        } else {
            bean.setPreviousFpaZone(null);
            bean.setNextFpaZone(null);
        }
    }
}
