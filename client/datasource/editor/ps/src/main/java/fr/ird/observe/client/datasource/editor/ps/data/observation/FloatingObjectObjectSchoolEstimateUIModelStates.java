package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.observation.ObjectSchoolEstimateDto;
import fr.ird.observe.dto.referential.common.WeightMeasureMethodReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class FloatingObjectObjectSchoolEstimateUIModelStates extends GeneratedFloatingObjectObjectSchoolEstimateUIModelStates {

    private final WeightMeasureMethodReference defaultWeightMeasureMethod;

    public FloatingObjectObjectSchoolEstimateUIModelStates(GeneratedFloatingObjectObjectSchoolEstimateUIModel model) {
        super(model);
        ObserveSwingDataSource dataSource = model.getDataSourcesManager().getMainDataSource();
        defaultWeightMeasureMethod = dataSource.getReferentialReferenceSet(WeightMeasureMethodReference.class).tryGetReferenceById(ProtectedIdsPs.PS_OBSERVATION_SCHOOL_ESTIMATE_DEFAULT_WEIGHT_MEASURE_METHOD_ID).orElseThrow(IllegalStateException::new);
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(ObjectSchoolEstimateDto.PROPERTY_SPECIES, ReferencesFilterHelper.newPsSpeciesList(servicesProvider, observeSelectModel, clientConfig.getSpeciesListSeineObservationObjectSchoolEstimateId()));
    }

    @Override
    public void initDefault(ObjectSchoolEstimateDto newTableBean) {
        newTableBean.setWeightMeasureMethod(getDefaultWeightMeasureMethod());
    }

    public WeightMeasureMethodReference getDefaultWeightMeasureMethod() {
        return defaultWeightMeasureMethod;
    }

}
