package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.ps.data.dcp.DcpUIModelStates;
import fr.ird.observe.client.datasource.editor.ps.data.dcp.TransmittingBuoyListener;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeSpecializedRules;
import fr.ird.observe.consolidation.data.ps.observation.FloatingObjectConsolidateEngine;
import fr.ird.observe.consolidation.data.ps.observation.FloatingObjectConsolidateRequest;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectPartDto;
import fr.ird.observe.dto.data.ps.observation.TransmittingBuoyDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialDto;
import fr.ird.observe.dto.referential.ps.common.TransmittingBuoyOperationReference;
import fr.ird.observe.services.service.data.ps.ConsolidateDataService;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

@GenerateJavaBeanDefinition
public class FloatingObjectUIModelStates extends GeneratedFloatingObjectUIModelStates implements DcpUIModelStates<FloatingObjectDto, FloatingObjectPartDto> {

    static final String PROPERTY_PARTS_MODIFIED = "partsModified";
    static final String PROPERTY_REFERENCE = "reference";
    private static final Logger log = LogManager.getLogger(FloatingObjectUIModel.class);
    private static final String PROPERTY_ARRIVING = "arriving";
    private static final String PROPERTY_LEAVING = "leaving";
    private final ReferentialLocale referentialLocale;
    private final Map<ObjectMaterialDto, String> whenArriving;
    private final Map<ObjectMaterialDto, String> whenLeaving;
    private final Supplier<ConsolidateDataService> psConsolidateDataServiceSupplier;
    private final Supplier<SimplifiedObjectTypeSpecializedRules> simplifiedObjectTypeSpecializedRulesSupplier;
    private Map<String, ObjectMaterialDto> referentialMap;
    private Map<String, TransmittingBuoyOperationReference> buoyOperationMap;
    private FloatingObjectPreset reference;
    private boolean arriving;
    private boolean leaving;
    private FloatingObjectConsolidateEngine consolidateEngine;
    private TransmittingBuoyListener<?> firstBuoyListener;
    private TransmittingBuoyListener<?> secondBuoyListener;

    public FloatingObjectUIModelStates(GeneratedFloatingObjectUIModel model) {
        super(model);
        // To be able to validate materials (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2285)
        getBean().setCanValidateMaterials(true);
        this.referentialLocale = model.getDecoratorService().getReferentialLocale();
        this.psConsolidateDataServiceSupplier = SingletonSupplier.of(model.getSource().getContext().getMainDataSource()::getPsConsolidateDataService);
        this.whenArriving = new LinkedHashMap<>();
        this.whenLeaving = new LinkedHashMap<>();
        this.simplifiedObjectTypeSpecializedRulesSupplier = SingletonSupplier.of(model.getClientConfig()::getSimplifiedObjectTypeSpecializedRules);
    }

    @Override
    public TransmittingBuoyListener<?> getFirstBuoyListener() {
        return firstBuoyListener;
    }

    @Override
    public void setFirstBuoyListener(TransmittingBuoyListener<?> firstBuoyListener) {
        this.firstBuoyListener = firstBuoyListener;
    }

    @Override
    public TransmittingBuoyListener<?> getSecondBuoyListener() {
        return secondBuoyListener;
    }

    @Override
    public void setSecondBuoyListener(TransmittingBuoyListener<?> secondBuoyListener) {
        this.secondBuoyListener = secondBuoyListener;
    }

    @Override
    public Map<String, TransmittingBuoyOperationReference> getBuoyOperationMap() {
        return buoyOperationMap;
    }

    @Override
    public void setBuoyOperationMap(Map<String, TransmittingBuoyOperationReference> buoyOperationMap) {
        this.buoyOperationMap = buoyOperationMap;
    }

    @Override
    public FloatingObjectPartDto newPart(Date now) {
        return FloatingObjectPartDto.newDto(now);
    }

    @Override
    protected void copyFormToBean(Form<FloatingObjectDto> form) {
        super.copyFormToBean(form);
        reset();
    }

    @Override
    protected void loadReferentialCacheOnOpenForm(Form<FloatingObjectDto> form) {
        super.loadReferentialCacheOnOpenForm(form);
        getReferenceCache().loadExtraReferentialReferenceSetsInModel(TransmittingBuoyDto.class);
    }

    @Override
    public FloatingObjectDto getBeanToSave() {
        FloatingObjectDto bean = getBean();
        recomputeComputedValues();
        Set<FloatingObjectPartDto> parts = toParts();
        log.info(String.format("will persist %d part(s).", parts.size()));
        bean.setFloatingObjectPart(new LinkedHashSet<>(parts));
        bean.autoTrim();
        return bean;
    }

    @Override
    public void fireComputedValuesChanged() {
        firePropertyChange("computedWhenArrivingBiodegradableValue", getComputedWhenArrivingBiodegradableValue());
        firePropertyChange("computedWhenArrivingNonEntanglingValue", getComputedWhenArrivingNonEntanglingValue());
        firePropertyChange("computedWhenArrivingSimplifiedObjectTypeValue", getComputedWhenArrivingSimplifiedObjectTypeValue());
        firePropertyChange("computedWhenLeavingBiodegradableValue", getComputedWhenLeavingBiodegradableValue());
        firePropertyChange("computedWhenLeavingNonEntanglingValue", getComputedWhenLeavingNonEntanglingValue());
        firePropertyChange("computedWhenLeavingSimplifiedObjectTypeValue", getComputedWhenLeavingSimplifiedObjectTypeValue());
    }

    @Override
    public Map<ObjectMaterialDto, String> getWhenArriving() {
        return whenArriving;
    }

    @Override
    public Map<ObjectMaterialDto, String> getWhenLeaving() {
        return whenLeaving;
    }

    @Override
    public void setWhenArriving(String id, String value) {
        setWhen0(id, value, whenArriving);
    }

    @Override
    public void setWhenLeaving(String id, String value) {
        setWhen0(id, value, whenLeaving);
    }

    @Override
    public Object getWhenArriving(String id) {
        ObjectMaterialDto dto = Objects.requireNonNull(referentialMap.get(id));
        return whenArriving.get(dto);
    }

    @Override
    public Object getWhenLeaving(String id) {
        ObjectMaterialDto dto = Objects.requireNonNull(referentialMap.get(id));
        return whenLeaving.get(dto);
    }

    @Override
    public FloatingObjectPreset getReference() {
        return reference;
    }

    @Override
    public void setReference(FloatingObjectPreset reference) {
        this.reference = reference;
        firePropertyChange(PROPERTY_REFERENCE, null, reference);
    }

    @Override
    public boolean isArriving() {
        return arriving;
    }

    @Override
    public void setArriving(boolean arriving) {
        this.arriving = arriving;
        log.debug("setArriving: " + arriving);
        firePropertyChange(PROPERTY_ARRIVING, arriving);
    }

    @Override
    public boolean isLeaving() {
        return leaving;
    }

    @Override
    public void setLeaving(boolean leaving) {
        this.leaving = leaving;
        log.debug("setLeaving: " + leaving);
        firePropertyChange(PROPERTY_LEAVING, leaving);
    }

    @Override
    public Optional<ToolkitIdModifications> consolidate() {
        return getConsolidateEngine().consolidate(new FloatingObjectConsolidateRequest(getBean(), toParts()));
    }

    @Override
    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    @Override
    public void setPartsModified() {
        firePropertyChange(PROPERTY_PARTS_MODIFIED, true);
    }

    public FloatingObjectConsolidateEngine getConsolidateEngine() {
        if (consolidateEngine == null) {
            consolidateEngine = new FloatingObjectConsolidateEngine(psConsolidateDataServiceSupplier.get().newSimplifiedObjectTypeManager(simplifiedObjectTypeSpecializedRulesSupplier.get()), getDecoratorService());
        }
        return consolidateEngine;
    }

    @Override
    public Map<String, ObjectMaterialDto> getReferentialMap() {
        return referentialMap;
    }

    @Override
    public void setReferentialMap(Map<String, ObjectMaterialDto> referentialMap) {
        this.referentialMap = referentialMap;
    }

    @Override
    public String getComputedWhenArrivingBiodegradableValue() {
        return DcpUIModelStates.super.getComputedWhenArrivingBiodegradableValue();
    }

    @Override
    public String getComputedWhenArrivingNonEntanglingValue() {
        return DcpUIModelStates.super.getComputedWhenArrivingNonEntanglingValue();
    }

    @Override
    public String getComputedWhenArrivingSimplifiedObjectTypeValue() {
        return DcpUIModelStates.super.getComputedWhenArrivingSimplifiedObjectTypeValue();
    }

    @Override
    public String getComputedWhenLeavingBiodegradableValue() {
        return DcpUIModelStates.super.getComputedWhenLeavingBiodegradableValue();
    }

    @Override
    public String getComputedWhenLeavingNonEntanglingValue() {
        return DcpUIModelStates.super.getComputedWhenLeavingNonEntanglingValue();
    }

    @Override
    public String getComputedWhenLeavingSimplifiedObjectTypeValue() {
        return DcpUIModelStates.super.getComputedWhenLeavingSimplifiedObjectTypeValue();
    }
}
