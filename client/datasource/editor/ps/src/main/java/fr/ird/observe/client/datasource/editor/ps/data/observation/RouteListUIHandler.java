package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.client.datasource.editor.api.content.actions.move.layout.MoveLayoutAction;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.observation.RouteDto;

/**
 * Created on 19/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class RouteListUIHandler extends GeneratedRouteListUIHandler {

    @Override
    protected void installMoveAction() {
        super.installMoveAction();
        MoveLayoutAction
                .create(ui, RouteDto.class)
                .on(() -> ui.getModel().toMoveAllRequest().setParentCandidates((groupBy, parentId) -> getRootOpenableService().getBrothers(groupBy, parentId)))
                .call(r -> getRootOpenableService().moveLayout(TripDto.class, r))
                .then(RouteUIMoveAllTreeAdapter::create)
                .install(ui::getMoveAll);
    }
}
