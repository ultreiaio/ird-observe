/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.ps.data.observation;

import fr.ird.observe.client.datasource.editor.api.content.data.sample.actions.ResetSizeMeasureType;
import fr.ird.observe.client.datasource.editor.api.content.data.sample.actions.ResetWeightMeasureType;
import fr.ird.observe.dto.data.AcquisitionMode;
import fr.ird.observe.dto.data.ps.observation.SampleMeasureDto;
import fr.ird.observe.dto.referential.common.SizeMeasureTypeReference;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.common.WeightMeasureTypeReference;
import fr.ird.observe.dto.referential.ps.common.SpeciesFateReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Optional;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class SampleUIHandler extends GeneratedSampleUIHandler {
    private static final Logger log = LogManager.getLogger(SampleUIHandler.class);
    /**
     * Ecoute les modifications de la propriété {@link SampleMeasureDto#getWeight()},
     * et repasser alors le flag {@link SampleMeasureDto#isIsWeightComputed()} à
     * {@code false}.
     *
     * @since 3.0
     */
    private final PropertyChangeListener weightChanged;

    /**
     * Ecoute les modifications de la propriété {@link SampleMeasureDto#getLength()},
     * et repasser alors le flag {@link SampleMeasureDto#isIsLengthComputed()} à
     * {@code false}.
     *
     * @since 3.0
     */
    private final PropertyChangeListener lengthChanged;

    /**
     * Ecoute les modifications de la propriété {@link SampleMeasureDto#getLength()},
     * et repasser alors le flag {@link SampleMeasureDto#isIsLengthComputed()} à
     * {@code false}.
     *
     * @since 3.0
     */
    private final PropertyChangeListener speciesChanged;

    public SampleUIHandler() {
        weightChanged = evt -> onWeightChanged((Float) evt.getNewValue(), !ui.getStates().isResetEdit());
        lengthChanged = evt -> onLengthChanged((Float) evt.getNewValue(), !ui.getStates().isResetEdit());
        speciesChanged = evt -> onSpeciesChanged((SpeciesReference) evt.getNewValue());
    }

    @Override
    public void onInit(SampleUI ui) {
        super.onInit(ui);
        ui.getAcquisitionModeGroup().addChangeListener(this::onAcquisitionModeChanged);
        ui.getSizeMeasureType().getToolbarRight().add(ui.getDefaultSizeMeasureType());
        ui.getWeightMeasureType().getToolbarRight().add(ui.getDefaultWeightMeasureType());
        ui.getLength().getRightToolbar().add(ui.getLengthSourceInformation());
        ui.getWeight().getRightToolbar().add(ui.getWeightSourceInformation());
        ResetSizeMeasureType.install(ui, ui.getDefaultSizeMeasureType(), ui.getSizeMeasureType());
        ResetWeightMeasureType.install(ui, ui.getDefaultWeightMeasureType(), ui.getWeightMeasureType());
    }

    @Override
    protected Component getFocusComponentOnSelectedRow(SampleUI ui, boolean notPersisted, boolean newRow, SampleMeasureDto tableEditBean, SampleMeasureDto previousRowBean) {
        return getNewFormFocusOwner(notPersisted, previousRowBean != null);
    }

    public void onSelectedRowChanged(SampleMeasureDto tableEditBean, SampleMeasureDto previousRowBean, boolean notPersisted, boolean newRow) {

        onLengthChanged(tableEditBean.getLength(), false);
        onWeightChanged(tableEditBean.getWeight(), false);

        SampleUIModelStates states = getModel().getStates();

        SpeciesReference species = tableEditBean.getSpecies();
        log.info(String.format("%s selected species %s", prefix, species));

        Optional<SizeMeasureTypeReference> defaultSizeMeasureType;
        Optional<WeightMeasureTypeReference> defaultWeightMeasureType;

        Optional<SizeMeasureTypeReference> sizeMeasureType = Optional.empty();
        Optional<WeightMeasureTypeReference> weightMeasureType = Optional.empty();
        if (notPersisted) {
            boolean unsetSpecies = true;
            if (newRow) {
                // use default mode
                AcquisitionMode acquisitionMode = states.getDefaultAcquisitionMode();
                if (previousRowBean != null) {
                    //use previous row (species won't be empty)
                    unsetSpecies = false;

                    // get previous species
                    species = previousRowBean.getSpecies();
                    // get previous acquisition mode
                    acquisitionMode = AcquisitionMode.valueOf(previousRowBean.getAcquisitionMode());
                    // get previous size measure type
                    sizeMeasureType = Optional.ofNullable(previousRowBean.getSizeMeasureType());
                    // get previous weight measure type
                    weightMeasureType = Optional.ofNullable(previousRowBean.getWeightMeasureType());
                }
                updateAcquisitionMode0(acquisitionMode);
            }

            // get default size measure type
            defaultSizeMeasureType = states.getSpeciesDefaultSizeMeasureType(species);
            defaultWeightMeasureType = states.getSpeciesDefaultWeightMeasureType(species);

            if (unsetSpecies) {
                // unset species (this will not set again species in widget)
                species = null;
                sizeMeasureType = defaultSizeMeasureType;
                weightMeasureType = defaultWeightMeasureType;
            }

        } else {
            updateAcquisitionMode0(AcquisitionMode.valueOf(tableEditBean.getAcquisitionMode()));

            // get default size measure type
            defaultSizeMeasureType = states.getSpeciesDefaultSizeMeasureType(species);
            defaultWeightMeasureType = states.getSpeciesDefaultWeightMeasureType(species);

            species = null;
            sizeMeasureType = Optional.ofNullable(tableEditBean.getSizeMeasureType());
            weightMeasureType = Optional.ofNullable(tableEditBean.getWeightMeasureType());
        }

        // use default size measure type
        states.setDefaultSizeMeasureType(defaultSizeMeasureType.orElse(null));
        states.setDefaultWeightMeasureType(defaultWeightMeasureType.orElse(null));

        ui.getSizeMeasureType().setSelectedItem(null);
        sizeMeasureType.ifPresent(ui.getSizeMeasureType()::setSelectedItem);

        ui.getWeightMeasureType().setSelectedItem(null);
        weightMeasureType.ifPresent(ui.getWeightMeasureType()::setSelectedItem);

        // Not sure we need to reset any species list, if it never changes :):):)
//        List<SpeciesReference> availableSpecies = sampleModel.getReferenceProvider().getReferentialReferences("species");
//        ui.getSpecies().setData(availableSpecies);

        if (species != null) {
            log.info(String.format("%s Will set species : %s", prefix, species));
            ui.getSpecies().setSelectedItem(null);
            ui.getSpecies().setSelectedItem(species);
        }
        updateSpeciesFateUniverse(tableEditBean.getSpecies(), tableEditBean.getSpeciesFate());
    }

    protected void onSpeciesChanged(SpeciesReference species) {
        SampleUIModelStates states = getModel().getStates();
        Optional<SizeMeasureTypeReference> sizeMeasureType = states.getSpeciesDefaultSizeMeasureType(species);
        Optional<WeightMeasureTypeReference> weightMeasureType = states.getSpeciesDefaultWeightMeasureType(species);
        states.setDefaultSizeMeasureType(sizeMeasureType.orElse(null));
        states.setDefaultWeightMeasureType(weightMeasureType.orElse(null));
        resetDefaultSizeMeasureType(false);
        resetDefaultWeightMeasureType(false);
        updateSpeciesFateUniverse(species, states.getTableEditBean().getSpeciesFate());
    }

    protected void updateSpeciesFateUniverse(SpeciesReference species, SpeciesFateReference speciesFate) {
        ui.getSpeciesFate().setSelectedItem(null);
        if (species == null) {
            ui.getSpeciesFate().setEnabled(false);
            ui.getSpeciesFate().setData(List.of());
        } else {
            List<SpeciesFateReference> speciesFateReferences = getModel().getStates().getSpeciesFate(species);
            ui.getSpeciesFate().setData(speciesFateReferences);
            ui.getSpeciesFate().setEnabled(true);
            //FIXME Due to none sample migration on speciesFate, we can have some one not present in speciesFateReferences
            //FIXME Need a fix in migration probably
//            if (speciesFate != null && speciesFateReferences.contains(speciesFate)) {
            if (speciesFate != null) {
                ui.getSpeciesFate().setSelectedItem(speciesFate);
            }
        }
    }

    public void resetDefaultSizeMeasureType(boolean force) {
        ui.getSizeMeasureType().setSelectedItem(null);
        SampleUIModel model = getModel();
        if (force || model.getStates().getTableEditBean().getSpecies() != null) {
            ui.getSizeMeasureType().setSelectedItem(model.getStates().getDefaultSizeMeasureType());
        }
    }

    public void resetDefaultWeightMeasureType(boolean force) {
        ui.getWeightMeasureType().setSelectedItem(null);
        SampleUIModel model = getModel();
        if (force || (model.getStates().getTableEditBean().getSpecies() != null && model.getStates().getTableEditBean().getAcquisitionMode() == 1)) {
            ui.getWeightMeasureType().setSelectedItem(model.getStates().getDefaultWeightMeasureType());
        }
    }

    public void stopEditTableEditBean(SampleMeasureDto tableEditBean) {
        //FIXME Should we have to remove this all the time ? May be a adjusting Flag could do the trick
        tableEditBean.removePropertyChangeListener(SampleMeasureDto.PROPERTY_WEIGHT, weightChanged);
        tableEditBean.removePropertyChangeListener(SampleMeasureDto.PROPERTY_LENGTH, lengthChanged);
        tableEditBean.removePropertyChangeListener(SampleMeasureDto.PROPERTY_SPECIES, speciesChanged);
    }

    public void startEditTableEditBean(SampleMeasureDto tableEditBean) {
        tableEditBean.removePropertyChangeListener(SampleMeasureDto.PROPERTY_WEIGHT, weightChanged);
        tableEditBean.removePropertyChangeListener(SampleMeasureDto.PROPERTY_LENGTH, lengthChanged);
        tableEditBean.removePropertyChangeListener(SampleMeasureDto.PROPERTY_SPECIES, speciesChanged);
        tableEditBean.addPropertyChangeListener(SampleMeasureDto.PROPERTY_WEIGHT, weightChanged);
        tableEditBean.addPropertyChangeListener(SampleMeasureDto.PROPERTY_LENGTH, lengthChanged);
        tableEditBean.addPropertyChangeListener(SampleMeasureDto.PROPERTY_SPECIES, speciesChanged);
    }

    /**
     * Le mode de saisie a été mis à jour.
     *
     * @param event le nouveau de mode de saisie à utiliser
     * @since 1.8
     */
    public void onAcquisitionModeChanged(ChangeEvent event) {
        JAXXButtonGroup source = (JAXXButtonGroup) event.getSource();
        AcquisitionMode newMode = (AcquisitionMode) source.getSelectedValue();
        if (newMode == null) {
            // mode null (cela peut arriver avec les bindings)
            return;
        }
        boolean createMode = ui.getTableModel().isCreate();
        SampleMeasureDto editBean = ui.getModel().getStates().getTableEditBean();
        switch (newMode) {
            case number:
                onAcquisitionModeChangedToNumber(createMode, editBean);
                break;
            case individual:
                onAcquisitionModeChangedToIndividual(createMode, editBean);
                break;
        }
        if (createMode) {
            // on propage le mode de saisie dans le bean
            getModel().getStates().getTableEditBean().setAcquisitionMode(newMode.ordinal());
        }
    }

    protected void updateAcquisitionMode0(AcquisitionMode acquisitionMode) {
        JAXXButtonGroup acquisitionModeGroup = ui.getAcquisitionModeGroup();
        acquisitionModeGroup.setSelectedValue(null);
        acquisitionModeGroup.setSelectedValue(acquisitionMode);
    }

    protected void onWeightChanged(Float newValue, boolean realChange) {
        SampleMeasureDto tableEditBean = ui.getModel().getStates().getTableEditBean();
        if (realChange && tableEditBean.getAcquisitionMode() == 1) {
            tableEditBean.setIsWeightComputed(false);
        }
        if (newValue == null) {
            tableEditBean.setWeightMeasureMethod(null);
        }
    }

    protected void onLengthChanged(Float newValue, boolean realChange) {
        SampleMeasureDto tableEditBean = ui.getModel().getStates().getTableEditBean();
        if (realChange) {
            tableEditBean.setIsLengthComputed(false);
        }
        if (newValue == null) {
            tableEditBean.setSizeMeasureMethod(null);
        }
    }

    protected void onAcquisitionModeChangedToNumber(boolean createMode, SampleMeasureDto editBean) {
        // weight not enabled
        ui.getWeight().setEnabled(false);
        ui.getWeightMeasureMethod().setEnabled(false);
        // count enabled
        ui.getCount().setEnabled(true);
        if (createMode) {
            // delete any weight value
            editBean.setWeight(null);
            editBean.setWeightMeasureType(null);
            editBean.setWeightMeasureMethod(null);
            // delete count (force use to reset it)
            editBean.setCount(null);
        }
    }

    protected void onAcquisitionModeChangedToIndividual(boolean createMode, SampleMeasureDto editBean) {
        // weight enabled
        ui.getWeight().setEnabled(true);
        ui.getWeightMeasureMethod().setEnabled(true);
        // count not enabled (set to one)
        ui.getCount().setEnabled(false);
        if (createMode) {
            // Always set to one
            editBean.setCount(1);
            editBean.setWeightMeasureType(getModel().getStates().getDefaultWeightMeasureType());
        }
    }

    protected JComponent getNewFormFocusOwner(boolean notPersisted, boolean withPrevious) {
        if (notPersisted && !withPrevious) {
            return ui.getSpecies();
        }
        return ui.getLength();
    }

    public void resetIsWeightComputed() {
        ui.getModel().getStates().getTableEditBean().setIsWeightComputed(false);
        ui.getWeight().grabFocus();
    }

    public void resetIsLengthComputed() {
        ui.getModel().getStates().getTableEditBean().setIsLengthComputed(false);
        ui.getLength().grabFocus();
    }
}

