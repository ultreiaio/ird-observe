package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.client.datasource.editor.api.content.data.sample.SampleContentTableUIModelStates;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.AcquisitionMode;
import fr.ird.observe.dto.data.ps.observation.SampleDto;
import fr.ird.observe.dto.data.ps.observation.SampleMeasureDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ps.common.SpeciesFateReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import javax.swing.Icon;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

@GenerateJavaBeanDefinition
public class SampleUIModelStates extends SampleContentTableUIModelStates<SampleDto, SampleMeasureDto> {

    public static final String WEIGHT_COMPUTED_TIP = n("observe.data.ps.observation.Sample.weight.computed.tip");
    public static final String WEIGHT_OBSERVED_TIP = n("observe.data.ps.observation.Sample.weight.observed.tip");
    public static final String SIZE_COMPUTED_TIP = n("observe.data.ps.observation.Sample.length.computed.tip");
    public static final String SIZE_OBSERVED_TIP = n("observe.data.ps.observation.Sample.length.observed.tip");
    private final Icon computedIcon;
    private final Icon observedIcon;

    /**
     * Dictionary of species fate ids available per species id.
     */
    private Map<String, Set<String>> speciesFateBySpeciesMap;

    public SampleUIModelStates(GeneratedSampleUIModel model) {
        super(model, SampleDto.newDto(new java.util.Date()), SampleMeasureDto.newDto(new java.util.Date()), model.getSource().getInitializer().getSelectedId(), model.getSource().getInitializer().getScope().isStandalone());
        this.computedIcon = UIHelper.getUIManagerActionIcon("data-calcule");
        this.observedIcon = UIHelper.getUIManagerActionIcon("data-observe");
    }

    @Override
    public SampleUITableModel getTableModel() {
        return (SampleUITableModel) super.getTableModel();
    }

    @Override
    public AcquisitionMode getDefaultAcquisitionMode() {
        return AcquisitionMode.number;
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(SampleMeasureDto.PROPERTY_SPECIES, ReferencesFilterHelper.<SampleDto, SpeciesReference>newSubList(e -> e.getSpeciesFateBySpeciesMap().keySet()));
        referenceCache.addReferentialFilter(SampleMeasureDto.PROPERTY_SPECIES_FATE, ReferencesFilterHelper.<SampleDto, SpeciesFateReference>newSubList(
                e -> e.getSpeciesFateBySpeciesMap().values().stream().flatMap(Set::stream).collect(Collectors.toSet())));
    }

    @Override
    protected void copyFormToBean(Form<SampleDto> form) {
        this.speciesFateBySpeciesMap = form.getObject().getSpeciesFateBySpeciesMap();
        super.copyFormToBean(form);
    }

    @Override
    public SampleDto getBeanToSave() {
        SampleDto beanToSave = super.getBeanToSave();
        beanToSave.setId(getSelectedId());
        return beanToSave;
    }

    public String getWeightDataTip(boolean computed) {
        return computed ? t(WEIGHT_COMPUTED_TIP) : t(WEIGHT_OBSERVED_TIP);
    }

    public String getLengthDataTip(boolean computed) {
        return computed ? t(SIZE_COMPUTED_TIP) : t(SIZE_OBSERVED_TIP);
    }

    public Icon getSourceInformationIcon(boolean computed) {
        return computed ? computedIcon : observedIcon;
    }

    public List<SpeciesFateReference> getSpeciesFate(SpeciesReference speciesReference) {
        Set<String> availableSpeciesFateIds = speciesFateBySpeciesMap.get(speciesReference.getId());
        return getReferenceCache().<SpeciesFateReference>getReferentialReferenceSet(SampleMeasureDto.PROPERTY_SPECIES_FATE).subSet(availableSpeciesFateIds).collect(Collectors.toList());
    }
}
