package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ps.observation.SampleMeasureDto;

import javax.swing.JTable;

public class SampleUITableModel extends GeneratedSampleUITableModel {
    private static final long serialVersionUID = 1L;

    public SampleUITableModel(SampleUI ui) {
        super(ui);
    }

    @Override
    public void initTableUISize(JTable table) {
        super.initTableUISize(table);
        UIHelper.fixTableColumnWidth(table, 3, 65);
        UIHelper.fixTableColumnWidth(table, 4, 65);
        UIHelper.fixTableColumnWidth(table, 5, 65);
    }

    @Override
    protected void onSelectedRowChanged(SampleUI ui, int editingRow, SampleMeasureDto tableEditBean, SampleMeasureDto previousRowBean, boolean notPersisted, boolean newRow) {
        ui.getHandler().onSelectedRowChanged(tableEditBean, previousRowBean, notPersisted, newRow);
        super.onSelectedRowChanged(ui, editingRow, tableEditBean, previousRowBean, notPersisted, newRow);
    }

    @Override
    protected void stopEditTableEditBean(SampleUI ui, SampleMeasureDto tableEditBean) {
        super.stopEditTableEditBean(ui, tableEditBean);
        ui.getHandler().stopEditTableEditBean(tableEditBean);
    }

    @Override
    protected void startEditTableEditBean(SampleUI ui, SampleMeasureDto tableEditBean) {
        super.startEditTableEditBean(ui, tableEditBean);
        ui.getHandler().startEditTableEditBean(tableEditBean);
    }
}
