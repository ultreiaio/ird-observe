package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.client.datasource.editor.api.DataSourceEditor;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveAction;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.SaveContentTableUIAdapter;
import fr.ird.observe.dto.data.ps.observation.SetCatchDto;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.12
 */
public class SetCatchUIHandler extends GeneratedSetCatchUIHandler {
    @Override
    protected void installSaveAction() {
        SaveAction
                .create(ui, SetCatchDto.class)
                .on(ui.getModel()::toSaveRequest)
                .call((r, d) -> getContainerService().save(d))
                .then(new SaveContentTableUIAdapter<>() {
                    @Override
                    public void adaptUi(DataSourceEditor dataSourceEditor, SetCatchUI ui, boolean notPersisted, SetCatchDto savedBean) {
                        super.adaptUi(dataSourceEditor, ui, notPersisted, savedBean);
                        SetCatchUINavigationNode node = ui.getModel().getSource();
                        SetUINavigationNode parent = node.getParent();
                        parent.getSampleUINavigationNode().reloadNodeData();
                        parent.getSetNonTargetCatchReleaseUINavigationNode().reloadNodeData();
                    }
                })
                .install(ui.getSave());
    }
}
