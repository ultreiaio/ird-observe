package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.AskToDelete;
import fr.ird.observe.client.datasource.editor.api.content.data.table.TableSavePredicate;
import fr.ird.observe.dto.data.ps.observation.CatchDto;
import fr.ird.observe.dto.data.ps.observation.SetCatchDto;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import io.ultreia.java4all.i18n.I18n;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created on 23/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SetCatchUISavePredicate extends TableSavePredicate<SetCatchDto, CatchDto, SetCatchUIModelStates> {

    public SetCatchUISavePredicate(SetCatchUIModelStates states) {
        super(states);
    }

    @Override
    public boolean canSave(SetCatchUIModelStates states, SetCatchDto originalBean, SetCatchDto bean, List<CatchDto> data) {
        AskToDelete<SetCatchDto> ask = new AskToDelete<>(
                this::generateText,
                I18n.t("observe.data.ps.observation.Catch.message.will.delete.sub.data"));
        return ask.needDelete(originalBean, bean);
    }

    public List<String> generateText(SetCatchDto originalBean, SetCatchDto editBean) {
        Set<SpeciesReference> dataToDelete = originalBean.getRemovedUnsafeSpecies(editBean);
        if (!dataToDelete.isEmpty()) {
            // il existe des échantillon thon a supprimer on demande une confirmation
            List<String> result = new LinkedList<>();
            getDecoratorService().installDecorator(SpeciesReference.class, dataToDelete.stream());
            for (DtoReference species : dataToDelete) {
                result.add(species.toString());
            }
            return result;
        }
        return null;
    }
}
