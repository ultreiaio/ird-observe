package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.dto.data.ps.observation.CatchDto;
import fr.ird.observe.dto.referential.ps.common.SpeciesFateReference;

import javax.swing.JTable;
import java.beans.PropertyChangeListener;

import static io.ultreia.java4all.i18n.I18n.t;

class SetCatchUITableModel extends GeneratedSetCatchUITableModel {
    private static final long serialVersionUID = 1L;
    /**
     * Listen changes on property {@link CatchDto#getTotalCount()},
     * and set property {@link CatchDto#getTotalCountComputedSource()} value to {@code null}.
     *
     * @since 3.0
     */
    final PropertyChangeListener totalCountChanged;

    /**
     * Listen changes on property {@link CatchDto#getCatchWeight()},
     * and set property {@link CatchDto#getCatchWeightComputedSource()} value to {@code null}.
     *
     * @since 3.0
     */
    final PropertyChangeListener catchWeightChanged;

    /**
     * Listen changes on property {@link CatchDto#getMeanWeight()},
     * and set property {@link CatchDto#getMeanWeightComputedSource()} value to {@code null}.
     *
     * @since 3.0
     */
    final PropertyChangeListener meanWeightChanged;

    /**
     * Listen changes on property {@link CatchDto#getMeanLength()},
     * and set property {@link CatchDto#getMeanLengthComputedSource()} value to {@code null}.
     *
     * @since 3.0
     */
    final PropertyChangeListener meanLengthChanged;
    /**
     * Listen changes on property {@link CatchDto#getSpeciesFate()} ()},
     * and adapt ui.
     *
     * @since 3.0
     */
    final PropertyChangeListener speciesFateChanged;

    public SetCatchUITableModel(SetCatchUI ui) {
        super(ui);
        totalCountChanged = evt -> {
            CatchDto source = (CatchDto) evt.getSource();
            source.setTotalCountComputedSource(null);
        };
        catchWeightChanged = evt -> onWeightChanged((Float) evt.getNewValue(), !ui.getStates().isResetEdit());
        meanWeightChanged = evt -> {
            CatchDto source = (CatchDto) evt.getSource();
            source.setMeanWeightComputedSource(null);
        };
        meanLengthChanged = evt -> {
            CatchDto source = (CatchDto) evt.getSource();
            source.setMeanLengthComputedSource(null);
        };
        speciesFateChanged = evt -> {
            CatchDto source = (CatchDto) evt.getSource();
            SpeciesFateReference speciesFate = (SpeciesFateReference) evt.getNewValue();
            if (speciesFate == null || speciesFate.getDiscard() == null || !speciesFate.getDiscard()) {
                // not on discard, can edit well
                ui.getWell().setEnabled(true);
                // not on discard, can not edit reason for discard
                ui.getReasonForDiscard().setEnabled(false);
                source.setReasonForDiscard(null);
            } else {
                // on discard, reset well value and disabled it
                source.setWell(null);
                ui.getWell().setEnabled(false);
                // on discard, can edit reason for discard
                ui.getReasonForDiscard().setEnabled(true);
            }
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2526
            if (speciesFate==null || !speciesFate.isWeightRangeAllowed()) {
                source.setMinWeight(null);
                source.setMaxWeight(null);
                ui.getMinWeight().setEnabled(false);
                ui.getMaxWeight().setEnabled(false);
            } else {
                ui.getMinWeight().setEnabled(true);
                ui.getMaxWeight().setEnabled(true);
            }
        };
    }

    @Override
    public void initTableUISize(JTable table) {
        super.initTableUISize(table);
        // species
        // speciesFate
        // reasonForDiscard
        // catchWeight
        UIHelper.fixTableColumnWidth(table, 4, 55);
        // totalCount
        UIHelper.fixTableColumnWidth(table, 5, 55);
        // weightMeasureMethod
        // minWeight
        UIHelper.fixTableColumnWidth(table, 7, 50);
        // maxWeight
        UIHelper.fixTableColumnWidth(table, 8, 50);
        // meanWeight
        UIHelper.fixTableColumnWidth(table, 9, 50);
        // meanLength
        UIHelper.fixTableColumnWidth(table, 10, 50);
        // well
        UIHelper.fixTableColumnWidth(table, 11, 50);
        // informationSource
        // comment
        UIHelper.fixTableColumnWidth(table, 13, 50);
    }

    @Override
    protected void onSelectedRowChanged(SetCatchUI ui, int editingRow, CatchDto tableEditBean, CatchDto previousRowBean, boolean notPersisted, boolean newRow) {

        super.onSelectedRowChanged(ui, editingRow, tableEditBean, previousRowBean, notPersisted, newRow);

        tableEditBean.removePropertyChangeListener(CatchDto.PROPERTY_TOTAL_COUNT, totalCountChanged);
        tableEditBean.addPropertyChangeListener(CatchDto.PROPERTY_TOTAL_COUNT, totalCountChanged);

        tableEditBean.removePropertyChangeListener(CatchDto.PROPERTY_CATCH_WEIGHT, catchWeightChanged);
        tableEditBean.addPropertyChangeListener(CatchDto.PROPERTY_CATCH_WEIGHT, catchWeightChanged);

        tableEditBean.removePropertyChangeListener(CatchDto.PROPERTY_MEAN_WEIGHT, meanWeightChanged);
        tableEditBean.addPropertyChangeListener(CatchDto.PROPERTY_MEAN_WEIGHT, meanWeightChanged);

        tableEditBean.removePropertyChangeListener(CatchDto.PROPERTY_MEAN_LENGTH, meanLengthChanged);
        tableEditBean.addPropertyChangeListener(CatchDto.PROPERTY_MEAN_LENGTH, meanLengthChanged);

        tableEditBean.removePropertyChangeListener(CatchDto.PROPERTY_SPECIES_FATE, speciesFateChanged);
        tableEditBean.addPropertyChangeListener(CatchDto.PROPERTY_SPECIES_FATE, speciesFateChanged);
    }

    @Override
    protected String getDeleteExtraMessage(CatchDto bean) {
        if (bean.isHasSample() || bean.isHasRelease()) {
            int count = getBean().getUnsafeSpeciesUsageCount(bean.getSpecies());
            if (count == 1) {
                // while deleting this one, this species won't be available on sample
                return t("observe.data.ps.observation.Catch.message.will.delete.sub.data.for.species", bean.getSpecies());
            }
        }
        // No specific message
        return null;
    }

    protected void onWeightChanged(Float newValue, boolean realChange) {
        CatchDto tableEditBean = getTableEditBean();
        if (realChange) {
            tableEditBean.setCatchWeightComputedSource(null);
        }
        if (newValue == null) {
            tableEditBean.setWeightMeasureMethod(null);
        }
    }
}
