/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.ps.data.observation;

import fr.ird.observe.dto.data.AcquisitionMode;
import fr.ird.observe.dto.data.ps.observation.NonTargetCatchReleaseDto;
import fr.ird.observe.dto.referential.common.SpeciesGroupDto;
import fr.ird.observe.dto.referential.common.SpeciesGroupReleaseModeReference;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.SwingUtilities;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class SetNonTargetCatchReleaseUIHandler extends GeneratedSetNonTargetCatchReleaseUIHandler {

    private static final Logger log = LogManager.getLogger(SetNonTargetCatchReleaseUIHandler.class);

    final PropertyChangeListener speciesChanged;

    SetNonTargetCatchReleaseUIHandler() {
        speciesChanged = evt -> {
            if (getModel().getStates().isOpened() && getModel().getStates().isEditing() && !getTableModel().isAdjusting()) {
                SpeciesReference species = (SpeciesReference) evt.getNewValue();
                updateSpecies(species);
            }
        };
    }

    @Override
    public void onInit(SetNonTargetCatchReleaseUI ui) {
        super.onInit(ui);
        getTableModel().getTableEditBean().addPropertyChangeListener(NonTargetCatchReleaseDto.PROPERTY_SPECIES, speciesChanged);
    }

    /**
     * Le mode de saisie a été mis à jour.
     *
     * @param newMode le nouveau de mode de saisie à utiliser
     * @since 3.0
     */
    void updateModeSaisie(AcquisitionMode newMode) {
        log.debug(prefix + "Change mode saisie to " + newMode);
        if (newMode == null) {
            // mode null (cela peut arriver avec les bindings)
            return;
        }
        boolean createMode = ui.getTableModel().isCreate();
        NonTargetCatchReleaseDto editBean = ui.getTableEditBean();
        switch (newMode) {
            case number:
                // l'count est modifiable
                ui.getCount().setEnabled(true);
                if (createMode) {
                    // on supprime aussi l'count (pour forcer la saisie)
                    editBean.setCount(null);
                }
                break;
            case individual:
                // l'count n'est pas modifiable et est toujours de 1
                ui.getCount().setEnabled(false);
                if (createMode) {
                    // on positionne l'count à 1 (seule valeur possible)
                    editBean.setCount(1);
                }
                break;
        }
        if (createMode) {
            // on propage le mode de saisie dans le bean
            editBean.setAcquisitionMode(newMode.ordinal());
        }
    }

    void updateSpecies(SpeciesReference species) {
        FilterableComboBox<SpeciesGroupReleaseModeReference> speciesGroupReleaseModeEditor = getUi().getSpeciesGroupReleaseMode();
        if (species == null) {
            log.debug("ResetAction speciesGroupReleaseMode, there is no species selected.");
            speciesGroupReleaseModeEditor.setSelectedItem(null);
            speciesGroupReleaseModeEditor.setData(Collections.emptyList());
            return;
        }
        String speciesGroupId = species.getSpeciesGroupId();
        SpeciesGroupDto speciesGroupDto = Objects.requireNonNull(getModel().getStates().getSpeciesGroupDtoMap().get(speciesGroupId));
        log.debug(String.format("Will use speciesGroup: %s", speciesGroupDto));

        SpeciesGroupReleaseModeReference speciesGroupReleaseMode = getModel().getStates().getTableEditBean().getSpeciesGroupReleaseMode();
        List<SpeciesGroupReleaseModeReference> speciesGroupReleaseModeList = speciesGroupDto.getSpeciesGroupReleaseMode();
        speciesGroupReleaseModeEditor.setSelectedItem(null);
        speciesGroupReleaseModeEditor.setData(speciesGroupReleaseModeList);
        if (speciesGroupReleaseModeList.contains(speciesGroupReleaseMode)) {
            log.debug(String.format("Set back speciesGroupReleaseMode: %s", speciesGroupReleaseMode));
            speciesGroupReleaseModeEditor.setSelectedItem(speciesGroupReleaseMode);
        }
        SwingUtilities.invokeLater(ui::revalidate);
    }
}
