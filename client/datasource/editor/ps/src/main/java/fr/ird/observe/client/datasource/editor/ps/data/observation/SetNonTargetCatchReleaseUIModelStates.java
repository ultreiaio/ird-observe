package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.datasource.api.cache.ReferencesCache;
import fr.ird.observe.client.datasource.api.cache.ReferencesFilterHelper;
import fr.ird.observe.dto.data.AcquisitionMode;
import fr.ird.observe.dto.data.ps.observation.NonTargetCatchReleaseDto;
import fr.ird.observe.dto.data.ps.observation.SetNonTargetCatchReleaseDto;
import fr.ird.observe.dto.referential.common.SpeciesGroupDto;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Map;
import java.util.TreeMap;

@GenerateJavaBeanDefinition
public class SetNonTargetCatchReleaseUIModelStates extends GeneratedSetNonTargetCatchReleaseUIModelStates {
    private final Map<String, SpeciesGroupDto> speciesGroupDtoMap;

    public SetNonTargetCatchReleaseUIModelStates(GeneratedSetNonTargetCatchReleaseUIModel model) {
        super(model);
        this.speciesGroupDtoMap = new TreeMap<>();
        model.getSource().getContext().getMainDataSource().getReferentialService().loadDtoList(SpeciesGroupDto.class).stream().filter(SpeciesGroupDto::isNotSpeciesGroupReleaseModeEmpty).forEach(id -> speciesGroupDtoMap.put(id.getId(), id));
    }

    @Override
    public void onAfterInitAddReferentialFilters(ClientConfig clientConfig, Project observeSelectModel, ObserveServicesProvider servicesProvider, ReferencesCache referenceCache) {
        referenceCache.addReferentialFilter(NonTargetCatchReleaseDto.PROPERTY_SPECIES, ReferencesFilterHelper.<SetNonTargetCatchReleaseDto, SpeciesReference>newSubList(SetNonTargetCatchReleaseDto::getAvailableSpeciesIds));
    }

    public Map<String, SpeciesGroupDto> getSpeciesGroupDtoMap() {
        return speciesGroupDtoMap;
    }

    @Override
    public void initDefault(NonTargetCatchReleaseDto newTableBean) {
        newTableBean.setAcquisitionMode(AcquisitionMode.individual.ordinal());
    }
}
