package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.AcquisitionMode;
import fr.ird.observe.dto.data.ps.observation.NonTargetCatchReleaseDto;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ps.observation.NonTargetCatchReleaseStatusReference;

import java.util.List;

/**
 * Created on 12/4/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.0
 */
public class SetNonTargetCatchReleaseUITableModel extends GeneratedSetNonTargetCatchReleaseUITableModel {

    private static final long serialVersionUID = 1L;

    SetNonTargetCatchReleaseUITableModel(SetNonTargetCatchReleaseUI context) {
        super(context);
    }

    @Override
    protected void onSelectedRowChanged(SetNonTargetCatchReleaseUI ui, int editingRow, NonTargetCatchReleaseDto tableEditBean, NonTargetCatchReleaseDto previousRowBean, boolean notPersisted, boolean newRow) {
        List<SpeciesReference> availableSpecies = getModel().getStates().getReferenceCache().getReferentialReferences(NonTargetCatchReleaseDto.PROPERTY_SPECIES);

        int acquisitionMode = tableEditBean.getAcquisitionMode();
        AcquisitionMode enumValue = AcquisitionMode.valueOf(acquisitionMode);
        ui.getAcquisitionModeGroup().setSelectedValue(null);
        ui.getAcquisitionModeGroup().setSelectedValue(enumValue);

        ui.getSpecies().setData(availableSpecies);

        NonTargetCatchReleaseStatusReference status = tableEditBean.getStatus();

        //FIXME Never call the handler like this, modify model instead
        SetNonTargetCatchReleaseUIHandler handler = ui.getHandler();
        handler.updateSpecies(tableEditBean.getSpecies());
        tableEditBean.setStatus(status);

        //FIXME Check this is still working well, but still do not do this here...
//        tableEditBean.removePropertyChangeListener(NonTargetCatchReleaseDto.PROPERTY_SPECIES, handler.speciesChanged);
//        tableEditBean.addPropertyChangeListener(NonTargetCatchReleaseDto.PROPERTY_SPECIES, handler.speciesChanged);

        super.onSelectedRowChanged(ui, editingRow, tableEditBean, previousRowBean, notPersisted, newRow);
    }
}
