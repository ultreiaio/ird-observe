package fr.ird.observe.client.datasource.editor.ps.data.observation;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.ps.observation.SchoolEstimateDto;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.common.SpeciesReference;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class SetSchoolEstimateUITableModel extends GeneratedSetSchoolEstimateUITableModel {
    private static final long serialVersionUID = 1L;

    public SetSchoolEstimateUITableModel(SetSchoolEstimateUI ui) {
        super(ui);
    }

    @Override
    protected void onSelectedRowChanged(SetSchoolEstimateUI ui, int editingRow, SchoolEstimateDto tableEditBean, SchoolEstimateDto previousRowBean, boolean notPersisted, boolean newRow) {
        List<SpeciesReference> listSpeciesUsed = getColumnValues(0);
        Set<String> listSpeciesIdUsed = listSpeciesUsed.stream().map(ReferentialDtoReference::getId).collect(Collectors.toSet());
        List<SpeciesReference> references = getModel().getStates().getReferenceCache().getReferentialReferences(SchoolEstimateDto.PROPERTY_SPECIES);
        List<SpeciesReference> availableSpecies = ToolkitId.filterNotContains(references, listSpeciesIdUsed);

        super.onSelectedRowChanged(ui, editingRow, tableEditBean, previousRowBean, notPersisted, newRow);

        if (!newRow) {
            if (tableEditBean.getSpecies() != null) {
                availableSpecies.add(tableEditBean.getSpecies());
            }
        }
        ui.getSpecies().setData(availableSpecies);
    }
}
