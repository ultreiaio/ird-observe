/*
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client.datasource.editor.ps.data.observation;

import fr.ird.observe.dto.referential.ps.common.SchoolTypeReference;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class SetUIHandler extends GeneratedSetUIHandler {

    //FIXME:Focus startTime/schoolThickness

//    @Override
//    protected Form<SetDto> onOpenForm() {
//        return super.onOpenForm();
    //FIXME Voir si c'est toujours d'actualité? (si oui il faut déplacer dans le service metier)
    //FIXME C code dans le service, maintenant a tester!!!
//        RouteDto route = getPsObservationRouteService().loadDto(routeId);
//        SetDto bean = model.getBean();
//        Date time = bean.getStartTime();
//        Date date = route.getDate();
//        Date dateAndTime = Dates.getDateAndTime(date, time, false, false);
//        getUi().getStartTime().setDate(dateAndTime);
//    }

    String updateTypeValue(SchoolTypeReference schoolType) {
        if (schoolType == null) {
            return t("observe.data.ps.observation.Set.schoolType.not.fill");
        }
        getDecoratorService().installDecorator(schoolType);
        return schoolType.toString();
    }

}

