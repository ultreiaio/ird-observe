package fr.ird.observe.client.datasource.editor.ps.data.observation.actions;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.ResetComputedValue;
import fr.ird.observe.dto.data.ps.observation.CatchDto;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 09/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SetCatchUIMeanLengthComputedInformation extends ResetComputedValue {
    public SetCatchUIMeanLengthComputedInformation() {
        super(CatchDto.PROPERTY_MEAN_LENGTH, CatchDto.PROPERTY_MEAN_LENGTH_COMPUTED_SOURCE);
    }

    @Override
    protected String getToolTipText(Object computed) {
        String result;
        if (computed == null) {
            result = t("observe.data.ps.observation.Catch.meanLengthComputed.observed.tip");
        } else {
            result = t("observe.data.ps.observation.Catch.meanLengthComputed.computed.tip", computed.toString());
        }
        return result;
    }
}
