package fr.ird.observe.client.datasource.editor.ps.predicates;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewPredicate;
import fr.ird.observe.client.datasource.editor.api.content.data.TripActionHelper;
import fr.ird.observe.client.datasource.editor.ps.data.common.TripUI;
import fr.ird.observe.client.datasource.editor.ps.data.common.TripUINavigationNode;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.common.TripReference;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.nuiton.jaxx.runtime.swing.TabInfo;

import javax.swing.SwingUtilities;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created on 01/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class CheckTripSupport<U extends ContentUI, D extends DataDto> extends CreateNewPredicate<U, D, TripUI> {

    private final Predicate<TripReference> tripPredicate;
    private TripUINavigationNode tripNode;

    public CheckTripSupport(Set<Class<? extends DataDto>> acceptedTypes,
                            Predicate<TripReference> tripPredicate,
                            Function<TripUI, TabInfo> getTab,
                            Function<TripUI, FilterableComboBox<?>> getCombo) {
        super(acceptedTypes, TripUI.class, target -> SwingUtilities.invokeLater(() -> applyStrategy(target, getTab, getCombo)));
        this.tripPredicate = Objects.requireNonNull(tripPredicate);
    }

    public CheckTripSupport(Set<Class<? extends DataDto>> acceptedTypes,
                            Predicate<TripReference> tripPredicate,
                            Function<TripUI, TabInfo> getTab,
                            Function<TripUI, FilterableComboBox<?>> getCombo,
                            Predicate<TripUI> tripPredicate2,
                            Function<TripUI, TabInfo> getTab2,
                            Function<TripUI, FilterableComboBox<?>> getCombo2) {
        super(acceptedTypes, TripUI.class, target -> SwingUtilities.invokeLater(() -> {
            if (tripPredicate2.test(target)) {
                // the second predicate is ok, use the first behaviour
                applyStrategy(target, getTab, getCombo);
            } else {
                // the second predicate is ko, use the second behaviour
                applyStrategy(target, getTab2, getCombo2);
            }
        }));
        this.tripPredicate = Objects.requireNonNull(tripPredicate);
    }

    private static void applyStrategy(TripUI target,
                                      Function<TripUI, TabInfo> getTab,
                                      Function<TripUI, FilterableComboBox<?>> getCombo) {
        TabInfo tabInfo = getTab.apply(target);
        target.selectTab(tabInfo);
        FilterableComboBox<?> comboBox = getCombo.apply(target);
        SwingUtilities.invokeLater(comboBox.getCombobox()::requestFocusInWindow);
    }

    @Override
    public final boolean checkCanCreate(U source, Class<D> dtoType) {
        TripReference reference = (TripReference) getTripNode(source).getCapability().getReference();
        return tripPredicate.test(reference);
    }

    @Override
    public final boolean askUserToFix(U source, String dtoLabel, String message) {
        return TripActionHelper.askUserToFix(source, dtoLabel, message);
    }

    @Override
    public final TripUI changeContent(U source, Class<TripUI> targetType) {
        return TripActionHelper.changeContent(source, getTripNode(source), targetType);
    }

    private TripUINavigationNode getTripNode(U source) {
        if (tripNode == null) {
            tripNode = (TripUINavigationNode) source.getModel().getSource().upToReferenceNode(TripReference.class);
        }
        return tripNode;
    }
}
