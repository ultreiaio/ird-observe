package fr.ird.observe.client.datasource.editor.ps.predicates;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.WithClientUIContextApi;
import fr.ird.observe.client.datasource.editor.api.ObserveKeyStrokesEditorApi;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewPredicate;
import fr.ird.observe.client.datasource.editor.ps.ObservePsKeyStrokes;
import fr.ird.observe.client.datasource.editor.ps.data.common.TripUI;
import fr.ird.observe.client.datasource.usage.UsageUIHandlerSupport;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 09/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class UseFloatingObjectPresetSupport<U extends ContentUI, D extends DataDto> extends CreateNewPredicate<U, D, TripUI> {

    private final Consumer<FloatingObjectPreset> presetConsumer;

    public static class UsePresetAction extends AbstractAction implements WithClientUIContextApi {
        private static final Logger log = LogManager.getLogger(UsePresetAction.class);
        private final FloatingObjectPreset preset;
        private final KeyStroke keyStroke;
        private JOptionPane pane;

        public UsePresetAction(FloatingObjectPreset preset, int index) {
            this.preset = Objects.requireNonNull(preset);
            this.keyStroke = Objects.requireNonNull(KeyStroke.getKeyStroke("F" + index));
            putValue(UsePresetAction.class.getName(), getClass().getName() + "_" + index);
            putValue(NAME, getDecoratorService().getDecoratorByType(preset.getClass()).decorate(preset));
        }

        public void install(JButton parent, JOptionPane pane) {
            this.pane = pane;
            String actionMapKey = (String) getValue(UsePresetAction.class.getName());
            pane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(keyStroke, actionMapKey);
            pane.getActionMap().put(actionMapKey, this);
            ObservePsKeyStrokes.addKeyStroke(parent, keyStroke);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            log.info(String.format("Choose dcp preset: %s", preset.getLabel1()));
            pane.setValue(preset);
        }
    }

    protected UseFloatingObjectPresetSupport(Class<D> dtoType, Consumer<FloatingObjectPreset> presetConsumer) {
        super(Set.of(dtoType), TripUI.class, u -> {
        });
        this.presetConsumer = presetConsumer;
    }

    @Override
    public boolean askUserToFix(U source, String dtoLabel, String message) {
        Set<FloatingObjectPreset> presets = getFloatingObjectPresetsManager().getPresets();
        if (presets.isEmpty()) {
            // no preset, so nothing to ask, just create the dcp
            return true;
        }

        // ask user to choose a preset or to cancel
        JPanel userConfigs = new JPanel(new GridLayout(0, 2));

        String replaceText = ObserveKeyStrokesEditorApi.suffixTextWithKeyStroke(t("observe.ui.choice.dcp.default"), KeyStroke.getKeyStroke("pressed ENTER"));
        Object[] options = {replaceText};
        JOptionPane pane = new JOptionPane(userConfigs, JOptionPane.QUESTION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, options, options[0]);
        JButton jButton = Objects.requireNonNull(UsageUIHandlerSupport.findButton(pane, replaceText));
        Font font = jButton.getFont().deriveFont(18f);
        jButton.setFont(font.deriveFont(Font.ITALIC));
        int index = 0;
        for (FloatingObjectPreset preset : presets) {
            UsePresetAction a = new UsePresetAction(preset, ++index);
            JButton b = new JButton(a);
            b.setFont(font);
            userConfigs.add(b);
            a.install(b, pane);
        }
        askToUser(pane, message, options);
        Object value = pane.getValue();
        if (value instanceof FloatingObjectPreset) {
            // use accept a preset
            presetConsumer.accept((FloatingObjectPreset) value);
            return true;
        }
        // limit case user did not make a choice (just cancel dialog)
        // this means stop the create action
        return replaceText.equals(value);
    }

    @Override
    public final boolean checkCanCreate(U source, Class<D> dtoType) {
        // always reject create action to force going in askUserToFix method
        return false;
    }

    @Override
    public final TripUI changeContent(U source, Class<TripUI> targetType) {
        return null;
    }

    @Override
    public final String getMessage() {
        return t("observe.data.ps.dcp.FloatingObjectPreset.choose.title");
    }

    @Override
    protected boolean result(boolean checkCanCreate, boolean askUserToFix) {
        return askUserToFix;
    }
}
