package fr.ird.observe.client.datasource.editor.ps.predicates;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewPredicate;
import fr.ird.observe.client.datasource.editor.ps.data.observation.FloatingObjectUIModel;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;

/**
 * Ask user to use a floating object preset for observations.
 * <p>
 * Created on 09/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@AutoService(CreateNewPredicate.class)
@SuppressWarnings("rawtypes")
public class UseObservationFloatingObjectPreset<U extends ContentUI> extends UseFloatingObjectPresetSupport<U, FloatingObjectDto> {

    public UseObservationFloatingObjectPreset() {
        super(FloatingObjectDto.class, FloatingObjectUIModel::setReference);
    }

}
