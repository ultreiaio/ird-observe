package fr.ird.observe.client.datasource.editor.ps.referential.common;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.CreateReferential;
import fr.ird.observe.client.datasource.editor.api.content.referential.actions.ModifyReferential;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialDto;
import fr.ird.observe.dto.referential.ps.common.ObjectMaterialReference;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 06/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
class ObjectMaterialUIHandler extends ContentReferentialUIHandler<ObjectMaterialDto, ObjectMaterialReference, ObjectMaterialUI> implements UIHandler<ObjectMaterialUI> {

    @Override
    protected void installCreateAction() {
        CreateReferential.installAction(ui, new CreateReferential.DefaultConsumer<ObjectMaterialDto, ObjectMaterialReference, ObjectMaterialUI>().andThen(u -> setParentList()));
    }

    @Override
    protected void installModifyAction() {
        ModifyReferential.installAction(ui, new ModifyReferential.DefaultConsumer<ObjectMaterialDto, ObjectMaterialReference, ObjectMaterialUI>().andThen(u -> setParentList()));
    }

    private void setParentList() {
        Optional<Set<ObjectMaterialReference>> optionalReferenceSetDto = getModel().getReferenceCache().tryToGetReferentialReferenceSet(ObjectMaterialDto.PROPERTY_PARENT);

        if (optionalReferenceSetDto.isPresent()) {

            ObjectMaterialReference parent = getModel().getStates().getBean().getParent();
            List<ObjectMaterialReference> references = optionalReferenceSetDto.get().stream().filter(r -> !Objects.equals(r.getId(), Optional.ofNullable(parent).map(ReferentialDtoReference::getId).orElse(null))).collect(Collectors.toList());
            getUi().getParentCode().setData(references);
        }
    }

}
