<!--
  #%L
  ObServe Client :: DataSource :: Editor :: PS
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.editor.api.content.referential.ContentI18nReferentialUI beanScope="bean"
                                                                                          i18n="fr.ird.observe.dto.referential.ps.common.ObjectOperationDto"
                                                                                          superGenericType='ObjectOperationDto, ObjectOperationReference, ObjectOperationUI'>
  <import>
    fr.ird.observe.dto.referential.ps.common.ObjectOperationDto
    fr.ird.observe.dto.referential.ps.common.ObjectOperationReference

    io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox
  </import>
  <BeanValidator id='validator' autoField='true'
                 beanClass='fr.ird.observe.dto.referential.ps.common.ObjectOperationDto'
                 context='create' errorTableModel='{getErrorTableModel()}'/>
  <ObjectOperationUIModel id='model' constructorParams='@override:getNavigationSource(this)'/>
  <ObjectOperationDto id='bean'/>
  <Table id='editTable' forceOverride="3">
    <row>
      <cell anchor='west'>
        <JLabel id='atLeastOneSelectedLabel'/>
      </cell>
      <cell weightx="1" fill="both">
        <JPanel id="atLeastOneSelected" layout="{new GridLayout(0, 1)}" border='{BorderFactory.createLoweredBevelBorder()}'>
          <BeanCheckBox id='whenArriving' />
          <BeanCheckBox id='whenLeaving'/>
        </JPanel>
      </cell>
    </row>
  </Table>
</fr.ird.observe.client.datasource.editor.api.content.referential.ContentI18nReferentialUI>
