<!--
  #%L
  ObServe Client :: DataSource :: Editor :: PS
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.editor.api.content.referential.ContentI18nReferentialUI beanScope="bean"
                                                                                          i18n="fr.ird.observe.dto.referential.ps.common.ProgramDto"
                                                                                          superGenericType='ProgramDto, ProgramReference, ProgramUI'>
  <import>
    fr.ird.observe.dto.referential.ps.common.ProgramDto
    fr.ird.observe.dto.referential.ps.common.ProgramReference
    fr.ird.observe.dto.referential.common.OrganismReference

    io.ultreia.java4all.jaxx.widgets.choice.BeanCheckBox
    io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox
    org.nuiton.jaxx.widgets.datetime.DateEditor
    org.nuiton.jaxx.widgets.text.BigTextEditor

    static fr.ird.observe.client.util.UIHelper.getStringValue
  </import>

  <BeanValidator id='validator' autoField='true' context='create' beanClass='fr.ird.observe.dto.referential.ps.common.ProgramDto' errorTableModel='{getErrorTableModel()}'/>
  <ProgramUIModel id='model' constructorParams='@override:getNavigationSource(this)'/>
  <ProgramUIModelStates id='states'/>
  <ProgramDto id='bean'/>
  <Table id='editTable' fill="both" forceOverride="3">
    <row>
      <cell anchor='west'>
        <JLabel id='organismLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <FilterableComboBox id='organism' genericType='OrganismReference'/>
      </cell>
    </row>
    <row>
      <cell anchor='west'>
        <JLabel id='validityDateRangeLabel'/>
      </cell>
      <cell anchor='west'>
        <JPanel layout='{new GridLayout()}'>
          <DateEditor id='startDate'/>
          <DateEditor id='endDate'/>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell anchor='west'>
        <JLabel id='atLeastOneSelectedLabel'/>
      </cell>
      <cell weightx="1" fill="both">
        <JPanel id="atLeastOneSelected" layout="{new GridLayout(0, 1)}" border='{BorderFactory.createLoweredBevelBorder()}'>
          <BeanCheckBox id='observation' />
          <BeanCheckBox id='logbook'/>
        </JPanel>
      </cell>
    </row>
  </Table>
  <Table id='editMoreExtraTable' fill='both' insets="1">
    <row>
      <cell weightx="1" weighty="1" fill="both">
        <BigTextEditor id="comment"/>
      </cell>
    </row>
  </Table>
</fr.ird.observe.client.datasource.editor.api.content.referential.ContentI18nReferentialUI>
