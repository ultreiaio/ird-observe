<!--
  #%L
  ObServe Client :: DataSource :: Editor :: PS
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.client.datasource.editor.api.content.referential.ContentI18nReferentialUI beanScope="bean"
                                                                                          i18n="fr.ird.observe.dto.referential.ps.localmarket.BuyerDto"
                                                                                          superGenericType='BuyerDto, BuyerReference, BuyerUI'>

  <import>
    fr.ird.observe.dto.referential.ps.localmarket.BuyerDto
    fr.ird.observe.dto.referential.ps.localmarket.BuyerReference
    fr.ird.observe.dto.referential.common.HarbourReference

    org.nuiton.jaxx.widgets.text.BigTextEditor
    org.nuiton.jaxx.widgets.text.NormalTextEditor
    io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox
    static fr.ird.observe.client.util.UIHelper.getStringValue
  </import>
  <BeanValidator id='validator' autoField='true'
                 beanClass='fr.ird.observe.dto.referential.ps.localmarket.BuyerDto'
                 context='create' errorTableModel='{getErrorTableModel()}'/>
  <BuyerUIModel id='model' constructorParams='@override:getNavigationSource(this)'/>
  <BuyerDto id='bean'/>
  <Table id='editTable' forceOverride="3">
    <row>
      <cell anchor='west'>
        <JLabel id='harbourLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <FilterableComboBox id='harbour' genericType='HarbourReference'/>
      </cell>
    </row>
    <row>
      <cell anchor='west'>
        <JLabel id='emailLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <NormalTextEditor id='email'/>
      </cell>
    </row>
    <row>
      <cell anchor='west'>
        <JLabel id='phoneLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <NormalTextEditor id='phone'/>
      </cell>
    </row>
    <row>
      <cell weightx="1" weighty="1" fill="both" columns="2">
        <BigTextEditor id="address"/>
      </cell>
    </row>
  </Table>
</fr.ird.observe.client.datasource.editor.api.content.referential.ContentI18nReferentialUI>
