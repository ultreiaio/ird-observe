package fr.ird.observe.client.datasource.editor.ps;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: PS
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.NavigationModelNodeTestSupport;
import fr.ird.observe.dto.data.ps.observation.SampleMeasureDto;
import fr.ird.observe.spi.module.ps.BusinessModule;
import org.junit.Test;

/**
 * Created on 19/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class PsNavigationTreeNodeTest extends NavigationModelNodeTestSupport {

    public PsNavigationTreeNodeTest() {
        super(BusinessModule.get());
    }

    @Test
    public void createRootOpenableNode() {
        assertRootOpenableNode(new fr.ird.observe.client.datasource.editor.ps.data.common.TripUINavigationNode(), fr.ird.observe.dto.data.ps.common.TripDto.class, fr.ird.observe.dto.data.ps.common.TripReference.class, fr.ird.observe.client.datasource.editor.ps.data.common.TripUI.class);
    }

    @Test
    public void createOpenableNode() {
        assertOpenableNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.RouteUINavigationNode(), fr.ird.observe.dto.data.ps.observation.RouteDto.class, fr.ird.observe.dto.data.ps.observation.RouteReference.class, fr.ird.observe.client.datasource.editor.ps.data.observation.RouteUI.class);
        assertOpenableNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.ActivityUINavigationNode(), fr.ird.observe.dto.data.ps.observation.ActivityDto.class, fr.ird.observe.dto.data.ps.observation.ActivityReference.class, fr.ird.observe.client.datasource.editor.ps.data.observation.ActivityUI.class);
    }

    @Test
    public void createRootOpenableListNode() {
        assertRootOpenableListNode(new fr.ird.observe.client.datasource.editor.ps.data.common.TripListUINavigationNode(), fr.ird.observe.dto.data.ps.common.TripDto.class, fr.ird.observe.dto.data.ps.common.TripReference.class, fr.ird.observe.client.datasource.editor.ps.data.common.TripListUI.class);
    }

    @Test
    public void createOpenableListNode() {
        assertOpenableListNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.RouteListUINavigationNode(), fr.ird.observe.dto.data.ps.observation.RouteDto.class, fr.ird.observe.dto.data.ps.observation.RouteReference.class, fr.ird.observe.client.datasource.editor.ps.data.observation.RouteListUI.class);
        assertOpenableListNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.ActivityListUINavigationNode(), fr.ird.observe.dto.data.ps.observation.ActivityDto.class, fr.ird.observe.dto.data.ps.observation.ActivityReference.class, fr.ird.observe.client.datasource.editor.ps.data.observation.ActivityListUI.class);
    }

    @Test
    public void createEditableNode() {
        assertEditNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.SetUINavigationNode(), fr.ird.observe.dto.data.ps.observation.SetDto.class, fr.ird.observe.dto.data.ps.observation.SetReference.class, fr.ird.observe.client.datasource.editor.ps.data.observation.SetUI.class);
        assertEditNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.FloatingObjectUINavigationNode(), fr.ird.observe.dto.data.ps.observation.FloatingObjectDto.class, fr.ird.observe.dto.data.ps.observation.FloatingObjectReference.class, fr.ird.observe.client.datasource.editor.ps.data.observation.FloatingObjectUI.class);
    }

    @Test
    public void createLayoutNode() {
        assertLayoutNode(new fr.ird.observe.client.datasource.editor.ps.data.common.TripLocalmarketUINavigationNode(), fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class,  fr.ird.observe.client.datasource.editor.ps.data.common.TripLocalmarketUI.class);
        assertLayoutNode(new fr.ird.observe.client.datasource.editor.ps.data.common.TripLogbookUINavigationNode(), fr.ird.observe.dto.data.ps.common.TripLogbookDto.class,  fr.ird.observe.client.datasource.editor.ps.data.common.TripLogbookUI.class);
    }

    @Test
    public void createTableNode() {
        assertTableNode(new fr.ird.observe.client.datasource.editor.ps.data.common.TripGearUseFeaturesUINavigationNode(), fr.ird.observe.dto.data.ps.common.GearUseFeaturesDto.class, fr.ird.observe.client.datasource.editor.ps.data.common.TripGearUseFeaturesUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.SetSchoolEstimateUINavigationNode(), fr.ird.observe.dto.data.ps.observation.SchoolEstimateDto.class, fr.ird.observe.client.datasource.editor.ps.data.observation.SetSchoolEstimateUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.SampleUINavigationNode(), SampleMeasureDto.class, fr.ird.observe.client.datasource.editor.ps.data.observation.SampleUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.FloatingObjectObjectObservedSpeciesUINavigationNode(), fr.ird.observe.dto.data.ps.observation.ObjectObservedSpeciesDto.class, fr.ird.observe.client.datasource.editor.ps.data.observation.FloatingObjectObjectObservedSpeciesUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.FloatingObjectObjectSchoolEstimateUINavigationNode(), fr.ird.observe.dto.data.ps.observation.ObjectSchoolEstimateDto.class, fr.ird.observe.client.datasource.editor.ps.data.observation.FloatingObjectObjectSchoolEstimateUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.SetNonTargetCatchReleaseUINavigationNode(), fr.ird.observe.dto.data.ps.observation.NonTargetCatchReleaseDto.class, fr.ird.observe.client.datasource.editor.ps.data.observation.SetNonTargetCatchReleaseUI.class);
        assertTableNode(new fr.ird.observe.client.datasource.editor.ps.data.observation.SetCatchUINavigationNode(), fr.ird.observe.dto.data.ps.observation.CatchDto.class, fr.ird.observe.client.datasource.editor.ps.data.observation.SetCatchUI.class);
    }

}
