package fr.ird.observe.client.datasource.editor.spi;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.ContentNodeType;
import fr.ird.observe.client.datasource.editor.spi.content.ContentUIDescriptor;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.util.SortedProperties;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 06/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class DetectCapabilities extends UiMojoRunnable {

    public Map<String, String> parentRelations;
    public List<String> missingParents;
    public SortedProperties rootCapabilities;
    protected String fileContent;
    private DetectContentFiles files;

    public Path getDirectoryToScan(Path sourceDirectory, Path baseDirectory, String directory) {
        String fqn = getClass().getPackage().getName().replace(".spi", "");
        return sourceDirectory.resolve(fqn.replaceAll("\\.", File.separator))
                .resolve(baseDirectory.toFile().getName())
                .resolve(directory);
    }

    public void setFiles(DetectContentFiles files) {
        this.files = files;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(files);
    }

    @Override
    public void run() {

        parentRelations = new LinkedHashMap<>();
        missingParents = new LinkedList<>();

        Path directoryToScan = getDirectoryToScan(sourceDirectory, baseDirectory, "data").getParent();

        Path capabilitiesFile = sourceDirectory.getParent().resolve("capabilities").resolve(sourceDirectory.relativize(directoryToScan)).resolve("Root.capabilities");

        rootCapabilities = ContentUIDescriptor.loadCapabilityFile(capabilitiesFile);
        rootCapabilities.values().forEach(c -> parentRelations.put(c.toString(), RootNavigationNode.class.getName()));
        for (ContentUIDescriptor descriptor : files.DESCRIPTORS) {

            Map<String, SortedProperties> capabilities = createCapabilities(descriptor, sourceDirectory);
            int size = capabilities.size();
            if (size == 0) {
                getLog().debug(String.format("[%s] No capability file detected.", descriptor.getClass().getName()));
            } else {
                getLog().info(String.format("[%s] %d capability file(s) detected.", descriptor.getClass().getName(), size));
            }
            descriptor.setCapabilities(capabilities);
        }
        missingParents.removeAll(parentRelations.keySet());
        if (!missingParents.isEmpty()) {
            for (Iterator<String> iterator = missingParents.iterator(); iterator.hasNext(); ) {
                String nodeName = iterator.next();
                if (rootCapabilities.contains(nodeName)) {
                    iterator.remove();
                    parentRelations.put(nodeName, RootNavigationNode.class.getName());
                    continue;
                }
                if (tryParentNodeCandidate(iterator, nodeName, nodeName.replace("PartUI", "UI"))) {
                    continue;
                }
                if (tryParentNodeCandidate(iterator, nodeName, nodeName.replace("SampleSpeciesUI", "SampleUI"))) {
                    continue;
                }
                if (tryParentNodeCandidate(iterator, nodeName, nodeName.replaceAll("Set(.+)CompositionUI", "SetGlobalCompositionUI"))) {
                    continue;
                }
                if (tryParentNodeCandidate(iterator, nodeName, nodeName.replace("BranchlineUI", "SetCatchUI"))) {
                    continue;
                }
                if (tryParentNodeCandidate(iterator, nodeName, nodeName.replace("WellActivityUI", "WellUI"))) {
                    continue;
                }
                getLog().error("Missing parent relation for: " + nodeName);
            }
            if (!missingParents.isEmpty()) {
                getLog().error("Missing parent relations detected: " + String.join("\n", missingParents));
            }
        }
        ObserveBusinessProject businessProject = ObserveBusinessProject.get();
        for (ContentUIDescriptor descriptor : files.DESCRIPTORS) {
            Map<String, SortedProperties> capabilities = descriptor.geCapabilities();
            Map<String, List<CapabilityDescriptor>> descriptors = new LinkedHashMap<>();
            descriptor.setCapabilitiesDescriptor(descriptors);
            if (capabilities.isEmpty()) {
                continue;
            }
            for (Map.Entry<String, SortedProperties> entry : capabilities.entrySet()) {
                List<CapabilityDescriptor> capabilityDescriptors = initCapabilities(businessProject, entry.getValue());
                descriptors.put(entry.getKey(), capabilityDescriptors);
            }
        }
    }

    protected boolean tryParentNodeCandidate(Iterator<String> iterator, String nodeName, String parentNodeNameCandidate) {
        if (parentRelations.containsKey(parentNodeNameCandidate)) {
            iterator.remove();
            parentRelations.put(nodeName, parentNodeNameCandidate);
            return true;
        }
        return false;
    }

    protected Map<String, SortedProperties> createCapabilities(ContentUIDescriptor descriptor, Path sourceDirectory) {
        Map<String, SortedProperties> result = new LinkedHashMap<>();
        Map<Path, String> filesContent = files.getFiles(descriptor.getClass());
        for (Path path : filesContent.keySet()) {
            Pair<String, SortedProperties> pair = descriptor.acceptCapability(sourceDirectory, path);
            String nodeName = pair.getKey();
            SortedProperties capabilities = pair.getValue();
            if (capabilities != null) {
                getLog().debug(String.format("Found capabilities for: %s (%d)", nodeName, capabilities.size()));
                result.put(nodeName, capabilities);
                if (capabilities.isEmpty()) {
                    missingParents.add(nodeName);
                }
                for (String dependencyNodeName : capabilities.stringPropertyNames()) {
                    String property = capabilities.getProperty(dependencyNodeName);
                    parentRelations.put(property, nodeName);
                    missingParents.remove(property);
                }
            } else {
                getLog().debug(String.format("No capabilities for: %s", nodeName));
                missingParents.add(nodeName);
            }
        }
        return result;
    }

    protected List<CapabilityDescriptor> initCapabilities(ObserveBusinessProject businessProject, SortedProperties capabilities) {
        List<CapabilityDescriptor> capabilityDescriptors = new LinkedList<>();
        Set<String> methodNames = new LinkedHashSet<>();
        Set<String> methodNamesToFix = new LinkedHashSet<>();
        for (String key : capabilities.stringPropertyNames()) {
            String nodeTypeName = capabilities.getProperty(key);
            int lastIndex = nodeTypeName.lastIndexOf(".");
            String nodeTypePackage = nodeTypeName.substring(0, lastIndex);
            String nodeTypeSimpleName = nodeTypeName.substring(lastIndex + 1);
            String childrenJaxxUiPath = (nodeTypePackage + "." + nodeTypeSimpleName.replace("NavigationNode", "")).replaceAll("\\.", java.io.File.separator) + ".jaxx";
            ContentNodeType contentNodeType = ContentNodeType.valueOf(files, childrenJaxxUiPath);
            CapabilityDescriptor capabilityDescriptor = CapabilityDescriptor.parse(businessProject, key, nodeTypeName, contentNodeType);
            capabilityDescriptors.add(capabilityDescriptor);
            String methodName = capabilityDescriptor.getMethodName();
            if (nodeTypePackage.contains(".ps")) {
                switch (methodName) {
                    case "RouteListUINavigationNode":
                        if (nodeTypePackage.endsWith(".logbook")) {
                            capabilityDescriptor.setMethodName(methodName = methodName.replace("Route", "RouteLogbook"));
                        }
                        if (nodeTypePackage.endsWith(".observation")) {
                            capabilityDescriptor.setMethodName(methodName = methodName.replace("Route", "RouteObs"));
                        }
                    case "SampleListUINavigationNode":
                        if (nodeTypePackage.endsWith(".localmarket")) {
                            capabilityDescriptor.setMethodName(methodName = "Localmarket" + methodName);
                        }
                        break;
                    case "SurveyListUINavigationNode":
                        if (nodeTypePackage.endsWith(".localmarket")) {
                            capabilityDescriptor.setMethodName(methodName = "Localmarket" + methodName);
                        }
                        break;
                }
            }
            if (nodeTypePackage.contains(".ll")) {
                if ("ActivityListUINavigationNode".equals(methodName)) {
                    if (nodeTypePackage.endsWith(".logbook")) {
                        capabilityDescriptor.setMethodName(methodName = methodName.replace("Activity", "ActivityLogbook"));
                    }
                    if (nodeTypePackage.endsWith(".observation")) {
                        capabilityDescriptor.setMethodName(methodName = methodName.replace("Activity", "ActivityObs"));
                    }
                }
            }
            if (!methodNames.add(methodName)) {
                methodNamesToFix.add(methodName);
            }
        }
        if (!methodNamesToFix.isEmpty()) {
            throw new IllegalStateException("Found some ambiguity with methods, you need to fix them: " + methodNamesToFix);
        }
        return capabilityDescriptors;
    }
}
