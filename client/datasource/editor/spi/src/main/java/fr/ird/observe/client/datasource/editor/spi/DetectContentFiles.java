package fr.ird.observe.client.datasource.editor.spi;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.ContentUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.data.edit.ContentEditUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.data.layout.ContentLayoutUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.data.list.ContentListUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.data.openable.ContentOpenableUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.data.rlist.ContentRootListUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.data.ropen.ContentRootOpenableUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.data.simple.ContentSimpleUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.data.table.ContentTableUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.referential.ContentReferentialUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.referential.ReferentialHomeUIDescriptor;
import io.ultreia.java4all.util.SortedProperties;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created on 06/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class DetectContentFiles extends UiMojoRunnable {

    public final List<ContentUIDescriptor> DESCRIPTORS =
            List.of(
                    new ContentLayoutUIDescriptor(),
                    new ContentEditUIDescriptor(),
                    new ContentRootOpenableUIDescriptor(),
                    new ContentOpenableUIDescriptor(),
                    new ContentRootListUIDescriptor(),
                    new ContentListUIDescriptor(),
                    new ContentTableUIDescriptor(),
                    new ContentSimpleUIDescriptor(),
                    new ContentReferentialUIDescriptor(),
                    new ReferentialHomeUIDescriptor());
    protected String fileContent;

    public Path getDirectoryToScan(Path sourceDirectory, Path baseDirectory, String directory) {
        String fqn = getClass().getPackage().getName().replace(".spi", "");
        return sourceDirectory.resolve(fqn.replaceAll("\\.", File.separator))
                .resolve(baseDirectory.toFile().getName())
                .resolve(directory);
    }

    @Override
    public void run() {
        for (ContentUIDescriptor descriptor : DESCRIPTORS) {
            Path directoryToScan = getDirectoryToScan(sourceDirectory, baseDirectory, descriptor.getDirectory());
            Map<Path, String> filesContent = createDefaultFilesContent(descriptor, directoryToScan, descriptor.getExtensionToScan(), sourceDirectory, targetDirectory);
            int size = filesContent.size();
            if (size == 0) {
                getLog().debug(String.format("[%s] No file detected.", descriptor.getClass().getName()));
            } else {
                getLog().info(String.format("[%s] %d file(s) detected.", descriptor.getClass().getName(), size));
            }
            descriptor.setFilesContent(filesContent);
        }
    }

    protected Map<Path, String> createDefaultFilesContent(ContentUIDescriptor descriptor, Path directoryToScan, String extensionToScan, Path sourceDirectory, Path targetDirectory) {
        Map<Path, String> result = new LinkedHashMap<>();
        if (Files.notExists(directoryToScan)) {
            return result;
        }
        try (Stream<Path> stream = Files.find(directoryToScan, 2, (p, b) -> p.toFile().getName().endsWith(extensionToScan))) {
            stream.map(p -> descriptor.acceptPath(sourceDirectory, targetDirectory, p)).filter(Objects::nonNull).forEach(p -> result.put(p.getKey(), p.getValue()));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't find ui from: %s", directoryToScan), e);
        }
        return result;
    }

    public Map<Path, String> getFiles(Class<? extends ContentUIDescriptor> descriptorClass) {
        return DESCRIPTORS.stream().filter(d -> descriptorClass.equals(d.getClass())).findFirst().map(ContentUIDescriptor::getFilesContent).orElseThrow(IllegalStateException::new);
    }

    public Map<String, SortedProperties> getCapabilities(Class<? extends ContentUIDescriptor> descriptorClass) {
        return DESCRIPTORS.stream().filter(d -> descriptorClass.equals(d.getClass())).findFirst().map(ContentUIDescriptor::geCapabilities).orElseThrow(IllegalStateException::new);
    }

    public Map<String, List<CapabilityDescriptor>> getCapabilitiesDescriptor(Class<? extends ContentUIDescriptor> descriptorClass) {
        return DESCRIPTORS.stream().filter(d -> descriptorClass.equals(d.getClass())).findFirst().map(ContentUIDescriptor::getCapabilitiesDescriptor).orElseThrow(IllegalStateException::new);
    }

    public boolean isContentEditUI(String path) {
        return isContentUI(getFiles(ContentEditUIDescriptor.class), path);
    }

    public boolean isContentSimpleUI(String path) {
        return isContentUI(getFiles(ContentSimpleUIDescriptor.class), path);
    }

    public boolean isContentLayoutUI(String path) {
        return isContentUI(getFiles(ContentLayoutUIDescriptor.class), path);
    }

    public boolean isContentTableUI(String path) {
        return isContentUI(getFiles(ContentTableUIDescriptor.class), path);
    }

    public boolean isContentOpenableUI(String path) {
        return isContentUI(getFiles(ContentOpenableUIDescriptor.class), path);
    }

    public boolean isContentRootOpenableUI(String path) {
        return isContentUI(getFiles(ContentRootOpenableUIDescriptor.class), path);
    }

    public boolean isContentListUI(String path) {
        return isContentUI(getFiles(ContentListUIDescriptor.class), path);
    }

    public boolean isContentRootListUI(String path) {
        return isContentUI(getFiles(ContentRootListUIDescriptor.class), path);
    }

    public boolean isContentReferentialUI(String path) {
        return isContentUI(getFiles(ContentReferentialUIDescriptor.class), path);
    }

    public boolean isContentReferentialHomeUI(String path) {
        return path.endsWith("ReferentialHomeUI.jaxx") && isContentUI(getFiles(ReferentialHomeUIDescriptor.class), path);
    }

    private boolean isContentUI(Map<Path, String> files, String path) {
        return files.keySet().stream().anyMatch(p -> p.toString().endsWith(path));
    }
}
