package fr.ird.observe.client.datasource.editor.spi;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.spi.content.ContentUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateJavaFileSupport;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.util.SortedProperties;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created on 06/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentUI extends UiMojoRunnable {
    protected Set<String> iconPaths;

    @Override
    public void run() {
        DetectContentFiles detectFiles = new DetectContentFiles();
        detectFiles.prepare(this);

        detectFiles.init();
        detectFiles.run();

        DetectCapabilities detectCapabilities = new DetectCapabilities();
        detectCapabilities.prepare(this);

        detectCapabilities.setFiles(detectFiles);

        detectCapabilities.init();
        detectCapabilities.run();

        iconPaths = new LinkedHashSet<>();
        for (ContentUIDescriptor descriptor : detectFiles.DESCRIPTORS) {
            if (descriptor.isEmpty()) {
                continue;
            }
            for (Class<? extends GenerateJavaFileSupport> generatorType : descriptor.getGeneratorTypes()) {
                GenerateJavaFileSupport generator = Objects2.newInstance(generatorType);
                generator.setIconPaths(iconPaths);
                generator.setFiles(detectFiles);
                generator.setDetectCapabilities(detectCapabilities);
                generator.setDescriptor(descriptor);
                generator.prepare(this);
                generator.init();
                generator.run();
            }
        }

        String moduleName = baseDirectory.toFile().getName().toLowerCase();

        Path uiPropertiesPath = sourceDirectory.getParent().resolve("resources").resolve("observe-ui-navigation-" + moduleName + ".properties");
        Path iconRootPath = uiPropertiesPath.getParent()
                .resolve("icons")
                .resolve("navigation")
                .resolve("default");
        SortedProperties oldContent = new SortedProperties();
        if (Files.exists(uiPropertiesPath)) {
            try (BufferedReader reader = Files.newBufferedReader(uiPropertiesPath)) {
                oldContent.load(reader);
            } catch (IOException e) {
                throw new IllegalStateException("Can't load file: " + uiPropertiesPath, e);
            }
        }

        SortedProperties newContent = new SortedProperties();

        List<String> cleanIconPaths = new LinkedList<>(iconPaths);
        cleanIconPaths.sort(String::compareTo);
        cleanIconPaths.forEach(key -> {
            String newKey = "icon." + key;
            String newSmallKey = newKey + "-small";
            String keyPath = key.replace("navigation.", "").replaceAll("\\.", File.separator);
            String value = keyPath + "_24x24.png";
            String smallValue = keyPath + "_16x16.png";
            newContent.put(newKey, "${ui.navigation.icons.path}/" + value);
            newContent.put(newSmallKey, "${ui.navigation.icons.path}/" + smallValue);

            Path iconPath = iconRootPath.resolve(value);
            if (Files.notExists(iconPath)) {
                getLog().error(String.format("Can't find icon at: %s", iconPath));
            }
        });
        newContent.putAll(oldContent);

        if (!isForce() && oldContent.keySet().equals(newContent.keySet())) {
            getLog().info(String.format("No change on ui properties files (%s).", uiPropertiesPath));
            return;
        }
        try {
            if (Files.notExists(uiPropertiesPath.getParent())) {
                Files.createDirectories(uiPropertiesPath.getParent());
            }
            try (BufferedWriter writer = Files.newBufferedWriter(uiPropertiesPath)) {
                getLog().warn(String.format("Modify ui properties files (%s).", uiPropertiesPath));
                newContent.store(writer, "Generated by " + getClass().getName());
            }
        } catch (IOException e) {
            throw new IllegalStateException("Can't write file: " + uiPropertiesPath);
        }
    }

}
