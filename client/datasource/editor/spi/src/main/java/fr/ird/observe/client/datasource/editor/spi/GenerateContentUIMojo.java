package fr.ird.observe.client.datasource.editor.spi;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.toolkit.maven.plugin.ExecuteRunnerMojoSupport;
import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;
import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.topia.persistence.TagValues;

import java.io.IOException;

/**
 * Created on 19/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@Mojo(name = "generate-content-ui", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyCollection = ResolutionScope.COMPILE)
public class GenerateContentUIMojo extends ExecuteRunnerMojoSupport {

    /**
     * Model name (used to obtain model tag values).
     */
    @Parameter(property = "model.name", required = true)
    private String modelName;

    private TagValues dtoTagValues;
    private TagValues persistenceTagValues;

    @Override
    protected void doAction(I18nKeySet getterFile, ClassLoader classLoader) throws IOException {
        ProjectPackagesDefinition projectPackagesDefinition = ProjectPackagesDefinition.of(classLoader);
        dtoTagValues = TagValues.dto(modelName, ProjectPackagesDefinition::cleanType);
        persistenceTagValues = TagValues.persistence(modelName, projectPackagesDefinition::dtoToEntity);
        super.doAction(getterFile, classLoader);
    }

    @Override
    protected MojoRunnable createRunner() {
        GenerateContentUI runner = new GenerateContentUI();
        runner.setSourceDirectory(getSourceRoot().toPath());
        runner.setTargetDirectory(getTargetRoot().toPath());
        runner.setBaseDirectory(getProject().getBasedir().toPath());
        runner.setDtoTagValues(dtoTagValues);
        runner.setPersistenceTagValues(persistenceTagValues);
        return runner;
    }
}

