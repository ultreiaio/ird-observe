package fr.ird.observe.client.datasource.editor.spi;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;
import org.nuiton.topia.persistence.TagValues;

import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 19/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class UiMojoRunnable extends MojoRunnable {

    protected Path sourceDirectory;
    protected Path targetDirectory;
    protected Path baseDirectory;

    protected TagValues dtoTagValues;
    protected TagValues persistenceTagValues;

    public void setDtoTagValues(TagValues dtoTagValues) {
        this.dtoTagValues = Objects.requireNonNull(dtoTagValues);
    }

    public void setPersistenceTagValues(TagValues persistenceTagValues) {
        this.persistenceTagValues = Objects.requireNonNull(persistenceTagValues);
    }

    public void setSourceDirectory(Path sourceDirectory) {
        this.sourceDirectory = sourceDirectory;
    }

    public void setTargetDirectory(Path targetDirectory) {
        this.targetDirectory = targetDirectory;
    }

    public void setBaseDirectory(Path baseDirectory) {
        this.baseDirectory = baseDirectory;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(baseDirectory);
        Objects.requireNonNull(sourceDirectory);
        Objects.requireNonNull(targetDirectory);
        Objects.requireNonNull(dtoTagValues);
        Objects.requireNonNull(persistenceTagValues);
    }

    public Path getSourceDirectory() {
        return sourceDirectory;
    }

    public Path getTargetDirectory() {
        return targetDirectory;
    }

    public Path getBaseDirectory() {
        return baseDirectory;
    }

    public TagValues getDtoTagValues() {
        return dtoTagValues;
    }

    public TagValues getPersistenceTagValues() {
        return persistenceTagValues;
    }

    public void prepare(UiMojoRunnable parent) {
        setBaseDirectory(parent.getBaseDirectory());
        setSourceDirectory(parent.getSourceDirectory());
        setTargetDirectory(parent.getTargetDirectory());
        setTemporaryPath(parent.getTemporaryPath());
        setLog(parent.getLog());
        setGetterFile(parent.getGetterFile());
        setForce(parent.isForce());
        setDtoTagValues(parent.getDtoTagValues());
        setPersistenceTagValues(parent.getPersistenceTagValues());
    }
}
