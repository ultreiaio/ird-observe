package fr.ird.observe.client.datasource.editor.spi.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewRootOpenableUI;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.LayoutAware;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.spi.module.BusinessProject;
import io.ultreia.java4all.lang.Strings;

import java.beans.Introspector;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 15/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class CapabilityDescriptor {

    public static final String NEW_NODE_METHOD_WITH_REFERENCE = "" +
            "    public %1$s.%2$s new%3$s(%4$s.%5$s reference) {\n" +
            "        return %1$s.%2$s.create(() -> %6$s, reference);\n" +
            "    }\n\n";
    public static final String NEW_EMPTY_NODE_METHOD_WITH_REFERENCE = "" +
            "    public %1$s.%2$s newEmpty%3$s(ReferentialLocale referentialLocale) {\n" +
            "        %4$s.%5$s reference = new %4$s.%5$s();\n" +
            "        return %1$s.%2$s.create(() -> %6$s, reference);\n" +
            "    }\n\n";
    public static final String NEW_NODE_METHOD0 = "" +
            "    public %1$s.%2$s new%3$s() {\n" +
            "        return %1$s.%2$s.create();\n" +
            "    }\n\n";
    public static final String NEW_NODE_METHOD = "" +
            "    public %1$s.%2$s new%3$s() {\n" +
            "        return %1$s.%2$s.create(() -> %4$s);\n" +
            "    }\n\n";
    public static final String NEW_NODE_METHOD_WITH_INITIAL_COUNT = "" +
            "    public %1$s.%2$s new%3$s() {\n" +
            "        return %1$s.%2$s.create(() -> %4$s, %4$s.%5$s());\n" +
            "    }\n\n";
    public static final String ADD_NODE_METHOD_WITH_REFERENCE = "" +
            "    public %4$s.%5$s add%1$s(%2$s.%3$sReference reference) {\n" +
            "        %4$s.%5$s newNode = new%1$s(reference);\n" +
            "        add(newNode);\n" +
            "        return newNode;\n" +
            "    }\n\n";
    public static final String ADD_EMPTY_NODE_METHOD_WITH_REFERENCE = "" +
            "    public %4$s.%5$s addEmpty%1$s() {\n" +
            "        %4$s.%5$s newNode = newEmpty%1$s(getContext().getReferentialLocale());\n" +
            "        add(newNode);\n" +
            "        return newNode;\n" +
            "    }\n\n";
    public static final String GET_NODE_METHOD = "" +
            "    public %1$s.%2$s get%3$s() {\n" +
            "        return (%1$s.%2$s) findChildByType(%1$s.%2$s.class);\n" +
            "    }\n\n";
    public static final String GET_NODE_METHOD_WITH_REFERENCE = "" +
            "    public %1$s.%2$s get%3$s(%4$s.%5$sReference reference) {\n" +
            "        return findChildByType(%1$s.%2$s.class, reference.getId());\n" +
            "    }\n\n" +
            "    public %1$s.%2$s get%3$s(String id) {\n" +
            "        return findChildByType(%1$s.%2$s.class, id);\n" +
            "    }\n\n";
    public static final String ADD_NODE_METHOD = "" +
            "    public %2$s.%3$s add%1$s() {\n" +
            "        %2$s.%3$s newNode = new%1$s();\n" +
            "        add(newNode);\n" +
            "        return newNode;\n" +
            "    }\n\n";
    public static final String ADD_CHILD_REFERENCE_NODE = "" +
            "        %1$s.%2$sReference optional%2$sReference = node.getReference().get%2$s();\n" +
            "        if (optional%2$sReference != null) {\n" +
            "            node.add%3$s(optional%2$sReference);\n" +
            "        }\n";
    public static final String ADD_MULTIPLE_CHILD_REFERENCE_NODE = "" +
            "        java.util.Collection<%1$s.%2$sReference> optional%2$sCollection = node.%4$s;\n" +
            "        if (optional%2$sCollection != null) {\n" +
            "            optional%2$sCollection.forEach(node::add%3$s);\n" +
            "        }\n";
    public static final String GET_POSITION_NODE = "" +
            "        if (reference instanceof %1$s.%2$sReference) {\n" +
            "            return actualPosition;\n" +
            "        }\n" +
            "        if (getNode().getReference().get%2$s() != null) {\n" +
            "            actualPosition++;\n" +
            "        }\n";
    public static final String GET_STATIC_POSITION_NODE = "" +
            "        actualPosition++;\n";
    public static final String GET_STATIC_WITH_PREDICATE_POSITION_NODE = "" +
            "        if (getNode().getReference().%1$s()) {\n" +
            "            actualPosition++;\n" +
            "        }\n";
    public static final String GET_MULTIPLE_POSITION_NODE = "" +
            "        if (reference instanceof %1$s.%2$sReference) {\n" +
            "            return actualPosition + getNode().getChildrenPosition((%1$s.%2$sReference) reference, %3$s);\n" +
            "        }\n";
    public static final String INSTALL_CREATE_NEW_TABLE_ACTION = "" +
            "        installCreateNewTableEntryAction(%1$s.class);\n";
    public static final String INSTALL_CREATE_NEW_EDIT_ACTION = "" +
            "        installCreateNewEditableAction(%1$sDto.class, getModel().getSource()::addEmpty%2$s);\n";
    public static final String INSTALL_CREATE_NEW_ACTION = "" +
            "        installCreateNewOpenableAction(getModel().getSource().getParent()::addEmpty%1$s);\n";
    public static final String INSTALL_CREATE_NEW_ROOT_ACTION = "" +
            "        installCreateNewOpenableAction(getModel().getSource().getParent()::addEmpty%1$s);\n";
    public static final String INSTALL_CREATE_NEW_ACTION_LIST = "" +
            "        installCreateNewOpenableAction(getModel().getSource()::addEmpty%1$s);\n";
    public static final String INSTALL_CREATE_NEW_ROOT_ACTION_LIST = "" +
            "        installCreateNewOpenableAction(getModel().getSource()::addEmpty%1$s);\n";
    public static final String INSTALL_CREATE_NEW_ACTION2 = "" +
            "        installCreateNewOpenableAction(%1$s.class, getModel().getSource()::addEmpty%2$s);\n";
    public static final String INSTALL_CREATE_NEW_ROOT_ACTION2 = "" +
            "        installCreateNewOpenableAction(%1$s.class, getModel().getSource()::addEmpty%2$s);\n";
    public static final String INSTALL_CREATE_NEW_ACTION2_LIST = "" +
            "        installCreateNewOpenableAction(%1$s.class, () -> getModel().getSource().get%2$s().addEmpty%3$s());\n";
    public static final String INSTALL_CREATE_NEW_ROOT_ACTION2_LIST = "" +
            "        installCreateNewRootOpenableAction(%1$s.class, () -> getModel().getSource().get%2$s().addEmpty%3$s());\n";
    private final ContentNodeType contentNodeType;
    private final CapacityNodeType capacityNodeType;
    private final String nodeTypeName;
    private final String optionalPredicate;
    private final Class<? extends IdDto> optionalDtoType;
    private final Class<? extends DtoReference> optionalReferenceType;
    private final String nodeTypePackage;
    private final String nodeTypeSimpleName;
    private String methodName;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static CapabilityDescriptor parse(BusinessProject businessProject, String key, String nodeTypeName, ContentNodeType contentNodeType) {
        CapacityNodeType capacityNodeType = CapacityNodeType.parseKey(key);
        String optionalPredicate = capacityNodeType.getPredicate(key);
        Class<? extends IdDto> optionalDtoType = contentNodeType.getDto(businessProject, nodeTypeName);
        Class<? extends DtoReference> optionalReferenceType = capacityNodeType.getReference(businessProject, nodeTypeName);
        if (optionalReferenceType == null && LayoutAware.class.isAssignableFrom(optionalDtoType)) {
            optionalReferenceType = businessProject.getMapping().getReferenceType((Class) optionalDtoType);
        }
        return new CapabilityDescriptor(contentNodeType, capacityNodeType, nodeTypeName, optionalPredicate, optionalDtoType, optionalReferenceType);
    }

    public static String generateAddMainNewOpenAction(ContentNodeType contentNodeType, String nodeTypeSimpleName) {
        if (contentNodeType == ContentNodeType.OPEN) {
            return String.format(INSTALL_CREATE_NEW_ACTION, nodeTypeSimpleName);
        } else if (contentNodeType == ContentNodeType.ROPEN) {
            return String.format(INSTALL_CREATE_NEW_ROOT_ACTION, nodeTypeSimpleName);
        } else if (contentNodeType == ContentNodeType.LIST) {
            return String.format(INSTALL_CREATE_NEW_ACTION_LIST, nodeTypeSimpleName);
        } else if (contentNodeType == ContentNodeType.RLIST) {
            return String.format(INSTALL_CREATE_NEW_ROOT_ACTION_LIST, nodeTypeSimpleName);
        }
        return "";
    }

    public CapabilityDescriptor(ContentNodeType contentNodeType, CapacityNodeType capacityNodeType, String nodeTypeName, String optionalPredicate, Class<? extends IdDto> optionalDtoType, Class<? extends DtoReference> optionalReferenceType) {
        this.contentNodeType = Objects.requireNonNull(contentNodeType);
        this.capacityNodeType = Objects.requireNonNull(capacityNodeType);
        this.nodeTypeName = Objects.requireNonNull(nodeTypeName);
        this.optionalDtoType = optionalDtoType;
        int lastIndex = nodeTypeName.lastIndexOf(".");
        nodeTypePackage = nodeTypeName.substring(0, lastIndex);
        nodeTypeSimpleName = nodeTypeName.substring(lastIndex + 1);
        this.methodName = nodeTypeSimpleName;
        this.optionalReferenceType = optionalReferenceType;
        this.optionalPredicate = optionalPredicate;
    }

    public ContentNodeType getNodeType() {
        return contentNodeType;
    }

    public ContentNodeType getContentNodeType() {
        return contentNodeType;
    }

    public CapacityNodeType getCapacityNodeType() {
        return capacityNodeType;
    }

    public String getNodeTypeName() {
        return nodeTypeName;
    }

    public String getNodeTypePackage() {
        return nodeTypePackage;
    }

    public String getNodeTypeSimpleName() {
        return nodeTypeSimpleName;
    }

    public String getMethodName() {
        return methodName;
    }

    public Class<? extends DtoReference> getOptionalReferenceType() {
        return optionalReferenceType;
    }

    public Class<? extends IdDto> getOptionalDtoType() {
        return optionalDtoType;
    }

    public String getOptionalReferenceTypeCleanSimpleName() {
        return optionalReferenceType.getSimpleName().replace("Reference", "");
    }

    public Optional<String> getOptionalPredicate() {
        return Optional.ofNullable(optionalPredicate);
    }

    public void setMethodName(String methodName) {
        this.methodName = Objects.requireNonNull(methodName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CapabilityDescriptor)) return false;
        CapabilityDescriptor that = (CapabilityDescriptor) o;
        return nodeTypeName.equals(that.nodeTypeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodeTypeName);
    }

    public String generateNewNodeMethod(ContentUIDescriptor descriptor) {
        String referenceProperty = descriptor.getReferenceProperty();
        switch (contentNodeType) {
            case REFERENTIAL:
                return String.format(NEW_NODE_METHOD0, nodeTypePackage, nodeTypeSimpleName, methodName);
            case RLIST:
                return String.format(NEW_NODE_METHOD_WITH_INITIAL_COUNT, nodeTypePackage, nodeTypeSimpleName, methodName, referenceProperty, "get" + Strings.capitalize(methodName.replace("ListUINavigationNode", "StatValue")));
            case LIST:
                return String.format(NEW_NODE_METHOD_WITH_INITIAL_COUNT, nodeTypePackage, nodeTypeSimpleName, methodName, referenceProperty, "get" + Strings.capitalize(methodName.replace("ListUINavigationNode", "StatValue")));
            case SIMPLE:
            case LAYOUT:
            case TABLE:
                return String.format(NEW_NODE_METHOD, nodeTypePackage, nodeTypeSimpleName, methodName, referenceProperty);
            case EDIT:
            case ROPEN:
            case OPEN:
                return String.format(NEW_NODE_METHOD_WITH_REFERENCE, nodeTypePackage, nodeTypeSimpleName, methodName, getOptionalReferenceTypePackage(), optionalReferenceType.getSimpleName(), referenceProperty);
            default:
                throw new IllegalStateException("Unexpected value: " + contentNodeType);
        }
    }

    public String generateNewEmptyNodeMethod(ContentUIDescriptor descriptor) {
        String referenceProperty = descriptor.getReferenceProperty();
        switch (contentNodeType) {
            case REFERENTIAL:
            case LIST:
            case RLIST:
            case SIMPLE:
            case LAYOUT:
            case TABLE:
                return "";
            case EDIT:
            case ROPEN:
            case OPEN:
                return String.format(NEW_EMPTY_NODE_METHOD_WITH_REFERENCE, nodeTypePackage, nodeTypeSimpleName, methodName, getOptionalReferenceTypePackage(), optionalReferenceType.getSimpleName(), referenceProperty);
            default:
                throw new IllegalStateException("Unexpected value: " + contentNodeType);
        }
    }

    public String generateAddNodeMethod() {
        switch (contentNodeType) {
            case LIST:
            case RLIST:
            case REFERENTIAL:
            case SIMPLE:
            case LAYOUT:
            case TABLE:
                return String.format(ADD_NODE_METHOD, methodName, nodeTypePackage, nodeTypeSimpleName);
            case EDIT:
            case ROPEN:
            case OPEN:
                return String.format(ADD_NODE_METHOD_WITH_REFERENCE, methodName, getOptionalReferenceTypePackage(), getOptionalReferenceTypeCleanSimpleName(), nodeTypePackage, nodeTypeSimpleName);
            default:
                throw new IllegalStateException("Unexpected value: " + contentNodeType);
        }
    }

    public String generateAddEmptyNodeMethod() {
        switch (contentNodeType) {
            case LIST:
            case RLIST:
            case REFERENTIAL:
            case SIMPLE:
            case LAYOUT:
            case TABLE:
                return "";
            case EDIT:
            case ROPEN:
            case OPEN:
                return String.format(ADD_EMPTY_NODE_METHOD_WITH_REFERENCE, methodName, getOptionalReferenceTypePackage(), getOptionalReferenceTypeCleanSimpleName(), nodeTypePackage, nodeTypeSimpleName);
            default:
                throw new IllegalStateException("Unexpected value: " + contentNodeType);
        }
    }

    public String generateGetNodeMethod() {
        switch (capacityNodeType) {
            case STATIC:
            case STATIC_WITH_PREDICATE:
            case REFERENCE:
                return String.format(GET_NODE_METHOD, getNodeTypePackage(), getNodeTypeSimpleName(), methodName);
            case REFERENCE_LIST:
                return String.format(GET_NODE_METHOD_WITH_REFERENCE, getNodeTypePackage(), getNodeTypeSimpleName(), methodName, getOptionalReferenceTypePackage(), getOptionalReferenceTypeCleanSimpleName());
            default:
                throw new IllegalStateException("Unexpected value: " + this.contentNodeType);
        }
    }

    public String generateCapabilityAddNodeMethod(ContentNodeType contentNodeType) {

        switch (capacityNodeType) {
            case STATIC:
                return String.format("        node.add%1$s();\n", methodName);
            case STATIC_WITH_PREDICATE:
                String beanPropertyName = "is" + getOptionalPredicate().orElseThrow();
                return String.format("" +
                                             "        if (node.getReference().%1$s()) {\n" +
                                             "            node.add%2$s();\n" +
                                             "        }\n", beanPropertyName, methodName);
            case REFERENCE:
                return String.format(ADD_CHILD_REFERENCE_NODE, getOptionalReferenceTypePackage(), getOptionalReferenceTypeCleanSimpleName(), methodName);
            case REFERENCE_LIST:
                String getter;
                if (contentNodeType == ContentNodeType.LIST || contentNodeType == ContentNodeType.RLIST) {
                    getter = "getReferences()";
                } else {
                    getter = "getReference().get" + getOptionalReferenceTypeCleanSimpleName() + "()";
                }
                return String.format(ADD_MULTIPLE_CHILD_REFERENCE_NODE, getOptionalReferenceTypePackage(), getOptionalReferenceTypeCleanSimpleName(), methodName, getter);
            default:
                throw new IllegalStateException("Unexpected value: " + this.contentNodeType);
        }
    }


    public String generateCapabilityOnBeanToUpdateUpdateNodeMethod(Class<?> mainDtoType, Set<String> availableProperties) {
        switch (capacityNodeType) {
            case STATIC_WITH_PREDICATE: {
                String beanPropertyName = getOptionalPredicate().orElseThrow();
                return String.format("                .onPredicate(%1$s::is%2$s, %3$s.class)\n", mainDtoType.getSimpleName(), beanPropertyName, getNodeTypeSimpleName());
            }
            case REFERENCE_LIST:
            case REFERENCE: {
                String beanPropertyName = methodName.replace("UINavigationNode", "Enabled");
                if (availableProperties.contains(Introspector.decapitalize(beanPropertyName))) {
                    return String.format("                .onPredicate(%1$s::is%2$s, %3$s.class)\n", mainDtoType.getSimpleName(), beanPropertyName, getNodeTypeSimpleName());
                }
            }
            default:
                return null;
        }
    }
    public String generateCapabilityOnUpdatedBeanNodeUpdateNodeMethod(Class<?> mainDtoType, Set<String> availableProperties) {
        switch (capacityNodeType) {
            case STATIC_WITH_PREDICATE: {
                String beanPropertyName = getOptionalPredicate().orElseThrow();
                return String.format("                .onPredicate(%1$s::is%2$s, %3$s.class)\n", mainDtoType.getSimpleName(), beanPropertyName, getNodeTypeSimpleName());
            }
            case REFERENCE_LIST:
            case REFERENCE: {
                String beanPropertyName = methodName.replace("UINavigationNode", "Enabled");
                if (availableProperties.contains(Introspector.decapitalize(beanPropertyName))) {
                    return String.format("                .onPredicate(d-> d.is%1$s() && d.get%2$sStatValue() > 0, %3$s.class)\n", beanPropertyName, Strings.removeEnd(beanPropertyName,"Enabled"), getNodeTypeSimpleName());
                }
            }
            default:
                return null;
        }
    }

    protected String getOptionalReferenceTypePackage() {
        return optionalReferenceType.getPackage().getName();
    }

    public String generateCapabilityGetPosition(String comparator) {
        switch (capacityNodeType) {
            case STATIC:
                return GET_STATIC_POSITION_NODE;
            case STATIC_WITH_PREDICATE:
                String beanPropertyName = "is" + getOptionalPredicate().orElseThrow();
                return String.format(GET_STATIC_WITH_PREDICATE_POSITION_NODE, beanPropertyName);
            case REFERENCE:
                return String.format(GET_POSITION_NODE, getOptionalReferenceTypePackage(), getOptionalReferenceTypeCleanSimpleName());
            case REFERENCE_LIST:
                if (comparator == null) {
                    comparator = String.format("Comparator.comparing(%1$s.%2$sReference::getTopiaCreateDate)", getOptionalReferenceTypePackage(), getOptionalReferenceTypeCleanSimpleName());
                }
                return String.format(GET_MULTIPLE_POSITION_NODE, getOptionalReferenceTypePackage(), getOptionalReferenceTypeCleanSimpleName(), comparator);
        }
        return "";
    }

    public String generateAddNewAction(List<String> imports) {
        Class<? extends IdDto> dtoType = getOptionalDtoType();

        String childNodeSimpleName = nodeTypeSimpleName.replace("ListUI", "UI");
        switch (getContentNodeType()) {
            case ROPEN:
                imports.add(CreateNewRootOpenableUI.class.getName());
                return String.format(INSTALL_CREATE_NEW_ROOT_ACTION2, getNodeTypeName(), childNodeSimpleName);
            case OPEN:
                imports.add(CreateNewOpenableUI.class.getName());
                return String.format(INSTALL_CREATE_NEW_ACTION2, getNodeTypeName(), childNodeSimpleName);
            case RLIST:
                imports.add(CreateNewRootOpenableUI.class.getName());
                return String.format(INSTALL_CREATE_NEW_ROOT_ACTION2_LIST, getNodeTypeName(), methodName, childNodeSimpleName);
            case LIST:
                imports.add(CreateNewOpenableUI.class.getName());
                return String.format(INSTALL_CREATE_NEW_ACTION2_LIST, getNodeTypeName(), methodName, childNodeSimpleName);
            case TABLE:
                return String.format(INSTALL_CREATE_NEW_TABLE_ACTION, getNodeTypeName());
            case EDIT:
                if (dtoType != null) {
                    imports.add(dtoType.getName());
                    return String.format(INSTALL_CREATE_NEW_EDIT_ACTION, dtoType.getSimpleName().replace("Dto", ""), getNodeTypeSimpleName());
                }
            case LAYOUT:
            case SIMPLE:
            case ROOT:
            case REFERENTIAL_HOME:
            case REFERENTIAL:
                break;
        }
        return "";
    }

    public String generatePanelAction(int index) {
        switch (getContentNodeType()) {
            case LIST:
            case TABLE:
                return String.format("        initGotoAction( ui.getGoto%1$s(),  source -> getModel().getSource().get%2$s(), %3$d);\n", methodName.replace("UINavigationNode", ""), methodName, index);
        }
        return "";
    }
}
