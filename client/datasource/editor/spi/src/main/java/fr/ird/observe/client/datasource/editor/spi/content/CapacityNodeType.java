package fr.ird.observe.client.datasource.editor.spi.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.lang.Strings;

/**
 * Created on 15/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public enum CapacityNodeType {
    STATIC(false),
    STATIC_WITH_PREDICATE(false) {
        @Override
        public String getPredicate(String key) {
            return Strings.capitalize(key.substring(key.indexOf('_') + 1, key.length() - 2));
        }
    },
    REFERENCE(true) {
        @Override
        public Class<? extends DtoReference> getReference(BusinessProject businessProject, String key) {
            return getReference0(businessProject, key);
        }
    },
    REFERENCE_LIST(true) {
        @Override
        public Class<? extends DtoReference> getReference(BusinessProject businessProject, String key) {
            return getReference0(businessProject, key);
        }
    };

    private final boolean canCreateNode;

    public static CapacityNodeType parseKey(String key) {
        if (Strings.isNumeric(key)) {
            return STATIC;
        }
        if (key.endsWith("??")) {
            return STATIC_WITH_PREDICATE;
        }
        if (key.endsWith("?")) {
            return REFERENCE;
        }
        if (key.endsWith("*")) {
            return REFERENCE_LIST;
        }
        throw new IllegalStateException(String.format("Can't find type for key: %s", key));
    }

    CapacityNodeType(boolean canCreateNode) {
        this.canCreateNode = canCreateNode;
    }

    public String getPredicate(String key) {
        return null;
    }

    public Class<? extends DtoReference> getReference(BusinessProject businessProject, String nodeTypeName) {
        return null;
    }

    Class<? extends DtoReference> getReference0(BusinessProject businessProject, String nodeTypeName) {
        String nodeTypePackage = nodeTypeName.substring(0, nodeTypeName.lastIndexOf("."));
        BusinessModule childBusinessModule = businessProject.getBusinessModule(nodeTypePackage);
        String childReferenceName = nodeTypeName
                .replace("UINavigationNode", "")
                .replace("fr.ird.observe.client.datasource.editor", "fr.ird.observe.dto")
                .replace(childBusinessModule.getName() + ".", "")
                .replace("referential.", "referential." + childBusinessModule.getName() + ".")
                .replace("data.", "data." + childBusinessModule.getName() + ".");

        String referenceTypePackage = childReferenceName.substring(0, childReferenceName.lastIndexOf('.'));
        String referenceTypeSimpleName = childReferenceName.substring(childReferenceName.lastIndexOf('.') + 1) + "Reference";
        return Objects2.forName(referenceTypePackage + "." + referenceTypeSimpleName);
    }

    public boolean canCreateNode() {
        return canCreateNode;
    }
}
