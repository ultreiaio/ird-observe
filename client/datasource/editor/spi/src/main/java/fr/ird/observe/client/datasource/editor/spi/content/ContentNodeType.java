package fr.ird.observe.client.datasource.editor.spi.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIModel;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModel;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUIModel;
import fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationContext;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.client.datasource.editor.spi.DetectContentFiles;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import io.ultreia.java4all.lang.Objects2;

/**
 * Created on 15/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public enum ContentNodeType {
    ROOT(RootNavigationNode.class, RootNavigationContext.class, null) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return false;
        }
    },
    LAYOUT(ContentLayoutUINavigationNode.class, ContentLayoutUINavigationContext.class, ContentLayoutUIModel.class) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return detectContentFiles.isContentLayoutUI(jaxxUiPath);
        }
    },
    SIMPLE(ContentSimpleUINavigationNode.class, ContentSimpleUINavigationContext.class, ContentSimpleUIModel.class) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return detectContentFiles.isContentSimpleUI(jaxxUiPath);
        }
    }, TABLE(ContentTableUINavigationNode.class, ContentTableUINavigationContext.class, ContentTableUIModel.class) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return detectContentFiles.isContentTableUI(jaxxUiPath);
        }
    },
    EDIT(ContentEditUINavigationNode.class, ContentEditUINavigationContext.class, ContentEditUIModel.class) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return detectContentFiles.isContentEditUI(jaxxUiPath);
        }
    },
    OPEN(ContentOpenableUINavigationNode.class, ContentOpenableUINavigationContext.class, ContentOpenableUIModel.class) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return detectContentFiles.isContentOpenableUI(jaxxUiPath);
        }

//        @Override
//        public Class<? extends IdDto> getDto(BusinessProject businessProject, String nodeTypeName) {
//            return getDto0(businessProject, nodeTypeName);
//        }
    },
    ROPEN(ContentRootOpenableUINavigationNode.class, ContentRootOpenableUINavigationContext.class, ContentRootOpenableUIModel.class) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return detectContentFiles.isContentRootOpenableUI(jaxxUiPath);
        }

//        @Override
//        public Class<? extends IdDto> getDto(BusinessProject businessProject, String nodeTypeName) {
//            return getDto0(businessProject, nodeTypeName);
//        }
    },
    LIST(ContentListUINavigationNode.class, ContentListUINavigationContext.class, ContentListUIModel.class) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return detectContentFiles.isContentListUI(jaxxUiPath);
        }

//        @Override
//        public Class<? extends IdDto> getDto(BusinessProject businessProject, String nodeTypeName) {
//            return getDto0(businessProject, nodeTypeName);
//        }
    }, RLIST(ContentRootListUINavigationNode.class, ContentRootListUINavigationContext.class, ContentRootListUIModel.class) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return detectContentFiles.isContentRootListUI(jaxxUiPath);
        }

//        @Override
//        public Class<? extends IdDto> getDto(BusinessProject businessProject, String nodeTypeName) {
//            return getDto0(businessProject, nodeTypeName);
//        }
    },
    REFERENTIAL_HOME(ReferentialHomeUINavigationNode.class, ReferentialHomeUINavigationContext.class, ReferentialHomeUIModel.class) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return detectContentFiles.isContentReferentialHomeUI(jaxxUiPath);
        }
    },
    REFERENTIAL(ContentReferentialUINavigationNode.class, ContentReferentialUINavigationContext.class, ContentReferentialUIModel.class) {
        @Override
        public boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath) {
            return detectContentFiles.isContentReferentialUI(jaxxUiPath);
        }
    };

    private final Class<? extends NavigationNode> nodeType;
    private final Class<? extends NavigationContext<?>> contextType;
    private final Class<? extends ContentUIModel> uiModelType;

    public static ContentNodeType valueOf(DetectContentFiles detectContentFiles, String jaxxUiPath) {
        for (ContentNodeType value : values()) {
            if (value.accept(detectContentFiles, jaxxUiPath)) {
                return value;
            }
        }
        throw new IllegalStateException("Can't find content node type for path: " + jaxxUiPath);
    }

    @SuppressWarnings("unchecked")
    ContentNodeType(Class<? extends NavigationNode> nodeType, Class<?> contextType, Class<? extends ContentUIModel> uiModelType) {
        this.nodeType = nodeType;
        this.contextType = (Class<? extends NavigationContext<?>>) contextType;
        this.uiModelType = uiModelType;
    }

    public abstract boolean accept(DetectContentFiles detectContentFiles, String jaxxUiPath);

    public Class<? extends NavigationNode> getNodeType() {
        return nodeType;
    }

    public Class<? extends NavigationContext<?>> getNodeContextType() {
        return contextType;
    }

    public Class<? extends ContentUIModel> getUiModelType() {
        return uiModelType;
    }


    public Class<? extends IdDto> getDto(BusinessProject businessProject, String nodeTypeName) {
        return getDto0(businessProject, nodeTypeName);
    }

    Class<? extends IdDto> getDto0(BusinessProject businessProject, String nodeTypeName) {
        String nodeTypePackage = nodeTypeName.substring(0, nodeTypeName.lastIndexOf("."));
        BusinessModule childBusinessModule = businessProject.getBusinessModule(nodeTypePackage);
        String childReferenceName = nodeTypeName
                .replace("ListUI", "UI")
                .replace("UINavigationNode", "")
                .replace("fr.ird.observe.client.datasource.editor", "fr.ird.observe.dto")
                .replace(childBusinessModule.getName() + ".", "")
                .replace("referential.", "referential." + childBusinessModule.getName() + ".")
                .replace("data.", "data." + childBusinessModule.getName() + ".");

        String referenceTypePackage = childReferenceName.substring(0, childReferenceName.lastIndexOf('.'));
        String referenceTypeSimpleName = childReferenceName.substring(childReferenceName.lastIndexOf('.') + 1) + "Dto";
        return Objects2.forName(referenceTypePackage + "." + referenceTypeSimpleName);
    }

}
