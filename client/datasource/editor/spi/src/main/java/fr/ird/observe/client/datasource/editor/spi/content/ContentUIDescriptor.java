package fr.ird.observe.client.datasource.editor.spi.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.datasource.security.Permission;
import io.ultreia.java4all.util.SortedProperties;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Created on 06/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public interface ContentUIDescriptor {

    static SortedProperties loadCapabilityFile(Path capabilitiesFile) {
        SortedProperties capabilities = new SortedProperties();
        if (Files.exists(capabilitiesFile)) {
            try (BufferedReader reader = Files.newBufferedReader(capabilitiesFile)) {
                capabilities.load(reader);
            } catch (IOException e) {
                throw new IllegalStateException("Can't load capabilities file at: " + capabilitiesFile, e);
            }
        }
        return capabilities;
    }

    String getExtensionToScan();

    String getDirectory();

    Class<? extends ContentUI> getUiType();

    ContentNodeType getContentNodeType();

    List<Class<? extends GenerateJavaFileSupport>> getGeneratorTypes();

    Map<Path, String> getFilesContent();

    void setFilesContent(Map<Path, String> filesContent);

    Map<String, SortedProperties> geCapabilities();

    void setCapabilities(Map<String, SortedProperties> capabilities);

    Map<String, List<CapabilityDescriptor>> getCapabilitiesDescriptor();

    void setCapabilitiesDescriptor(Map<String, List<CapabilityDescriptor>> capabilities);

    default String cleanClassName(String className) {
        return className.replace(getJavaFileSuffix(), "").replace("Generated", "");
    }

    default String getJavaFileSuffix() {
        String simpleName = getClass().getSimpleName();
        int indexOf = simpleName.indexOf("UI");
        return simpleName.substring(indexOf + 2);
    }

    default Pair<Path, String> acceptPath(Path sourceDirectory, Path targetDirectory, Path uiPath) {
        try {
            String fileContent = new String(Files.readAllBytes(uiPath));
            if (!fileContent.contains("<" + getUiType().getName())) {
                return null;
            }
            return Pair.of(uiPath, fileContent);
        } catch (Exception e) {
            throw new IllegalStateException(String.format("Can't load jaxx file: %s", uiPath), e);
        }
    }

    default SortedProperties loadCapabilityFile(Path capabilitiesFile, String packageName, String cleanClassName) {
        return loadCapabilityFile(capabilitiesFile);
    }

    default Pair<String, SortedProperties> acceptCapability(Path sourceDirectory, Path uiPath) {

        String packageName = sourceDirectory.relativize(uiPath.getParent()).toString().replace(".jaxx", "").replaceAll(File.separator, ".");
        String className = uiPath.toFile().getName().replace(".jaxx", "");
        String cleanClassName = cleanClassName(className);
        String nodeName = packageName + "." + cleanClassName + "NavigationNode";
//        Path capabilitiesFile = uiPath.getParent().resolve(cleanClassName + ".capabilities");
        Path capabilitiesFile = sourceDirectory.getParent().resolve("capabilities").resolve(sourceDirectory.relativize(uiPath)).getParent().resolve(cleanClassName + ".capabilities");
        SortedProperties capabilities = loadCapabilityFile(capabilitiesFile, packageName, cleanClassName);
        if (capabilities.isEmpty()) {
            return Pair.of(nodeName, null);
        }
        return Pair.of(nodeName, capabilities);
    }

    default boolean isEmpty() {
        return getFilesContent().isEmpty();
    }

    default String getReferenceProperty() {
        return "getReference()";
    }

    default boolean isData() {
        return getDirectory().equals("data");
    }

    default boolean isReferential() {
        return getDirectory().equals("referential");
    }

    default Permission getPermission() {
        if (isData()) {
            return Permission.WRITE_DATA;
        }
        if (isReferential()) {
            return Permission.WRITE_REFERENTIAL;
        }
        return Permission.NONE;
    }
}
