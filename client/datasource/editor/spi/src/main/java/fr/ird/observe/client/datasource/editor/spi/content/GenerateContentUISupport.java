package fr.ird.observe.client.datasource.editor.spi.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.spi.content.data.layout.ContentLayoutUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.data.openable.ContentOpenableUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.data.ropen.ContentRootOpenableUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIHandlerHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIModelHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIModelStatesHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationCapabilityHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationContextHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationHandlerHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationNodeHelper;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created on 31/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class GenerateContentUISupport extends GenerateJavaFileSupport {

    public String nodeHandlerName;
    public String nodeContextName;
    public String nodeCapabilityName;
    protected Path sourceDirectory;
    protected Path targetDirectory;
    protected String iconPath;
    protected ContentUIHandlerHelper uiHandlerHelper = new ContentUIHandlerHelper(this);
    protected ContentUIModelHelper uiModelHelper = new ContentUIModelHelper(this);
    protected ContentUINavigationContextHelper uiNavigationContextHelper = new ContentUINavigationContextHelper(this);
    protected ContentUINavigationNodeHelper uiNavigationNodeHelper = new ContentUINavigationNodeHelper(this);
    protected ContentUINavigationCapabilityHelper uiNavigationCapabilityHelper = new ContentUINavigationCapabilityHelper(this);
    protected ContentUINavigationHandlerHelper uiNavigationHandlerHelper = new ContentUINavigationHandlerHelper(this);
    protected ContentUIModelStatesHelper uiModelStatesHelper = new ContentUIModelStatesHelper(this);

    public abstract Class<? extends ContentUI> getUiType();

    protected abstract String generateConcreteContent(Path path, String packageName, String namePrefix);

    @Override
    public final String getDirectory() {
        return "data";
    }

    @Override
    public String getExtensionToScan() {
        return "UI.jaxx";
    }

    protected void init(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        this.sourceDirectory = sourceDirectory;
        this.targetDirectory = targetDirectory;
        this.iconPath = null;
        this.packageName = packageName;
        this.className = path.toFile().getName().replace(".java", "");
        this.cleanClassName = cleanClassName(className);
        this.nodeHandlerName = cleanClassName + "NavigationHandler";
        this.nodeContextName = cleanClassName + "NavigationContext";
        this.nodeCapabilityName = cleanClassName + "NavigationCapability";
        this.businessModule = businessProject.getBusinessModule(packageName);
        this.businessSubModule = businessProject.getBusinessSubModule(businessModule, packageName);
        this.iconPath = generateIconPath(cleanClassName);
        this.i18nMapping.clear();
        initCapabilities();
    }

    protected final String generateAbstractContent(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        init(sourceDirectory, targetDirectory, path, packageName, namePrefix);
        return generateAbstractContent0(sourceDirectory, targetDirectory, path, packageName, namePrefix);
    }

    protected String generateAbstractContent0(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        return null;
    }

    protected final Path getAbstractFilePath(Path uiPath, String namePrefix) {
        return uiPath.getParent().resolve("Generated" + namePrefix + getJavaFileSuffix() + ".java");
    }

    protected Path getConcreteFilePath(Path uiPath, String namePrefix) {
        return uiPath.getParent().resolve(namePrefix + getJavaFileSuffix() + ".java");
    }

    @Override
    protected void processPath(Path sourceDirectory, Path targetDirectory, Path uiPath, String fileContent) {

        String packageName = sourceDirectory.relativize(uiPath.getParent()).toString().replace(".jaxx", "").replaceAll(File.separator, ".");

        String namePrefix = uiPath.toFile().getName().replace(".jaxx", "");

        Path abstractModelPath = getAbstractFilePath(uiPath, namePrefix);
        if (Files.notExists(abstractModelPath)) {
            boolean generated = generateAbstractFile(sourceDirectory, targetDirectory, targetDirectory.resolve(sourceDirectory.relativize(abstractModelPath)), packageName, namePrefix);
            if (generated) {
                generatedFileCount++;
            }
        }

        Path concreteModelPath = getConcreteFilePath(uiPath, namePrefix);
        boolean exists = Files.exists(concreteModelPath);
        if (alwaysGenerateConcreteFile() && exists) {
            throw new IllegalStateException("You can not override class: " + concreteModelPath);
        }
        if (!exists || force()) {
            boolean generated = generateConcreteFile(relativizePath(concreteModelPath), packageName, namePrefix);
            if (generated) {
                generatedFileCount++;
            }
        }
    }

    protected boolean force() {
        return getClass().getSimpleName().endsWith("NavigationScope");
    }

    protected Path relativizePath(Path concreteModelPath) {
        if (concreteModelPath.toFile().getName().endsWith(".scope")) {
            return concreteModelPath;
        }
        return targetDirectory.resolve(sourceDirectory.relativize(concreteModelPath));
    }

    //FIXME Move this in descriptor
    protected final boolean alwaysGenerateConcreteFile() {
        return !getClass().getSimpleName().endsWith("ContentEditUIModel")
                && !getClass().getSimpleName().endsWith("EditUIHandler")
                && !getClass().getSimpleName().endsWith("EditUIModelStates")
                && !getClass().getSimpleName().endsWith("OpenableUIHandler")
                && !getClass().getSimpleName().endsWith("DeleteTreeAdapter")
                && !getClass().getSimpleName().endsWith("OpenableUIMoveTreeAdapter")
                && !getClass().getSimpleName().endsWith("OpenableUIModelStates")
                && !getClass().getSimpleName().endsWith("ListUIModel")
                && !getClass().getSimpleName().endsWith("ListUIModelStates")
                && !getClass().getSimpleName().endsWith("ListUIHandler")
                && !getClass().getSimpleName().endsWith("ListUINavigationCapability")
                && !getClass().getSimpleName().endsWith("SimpleUIHandler")
                && !getClass().getSimpleName().endsWith("SimpleUIModelStates")
                && !getClass().getSimpleName().endsWith("NavigationScope")
                && !getClass().getSimpleName().endsWith("NavigationHandler")
                && !getClass().getSimpleName().endsWith("TableUIModel")
                && !getClass().getSimpleName().endsWith("TableUIHandler")
                && !getClass().getSimpleName().endsWith("TableUITableModel")
                && !getClass().getSimpleName().endsWith("TableUIModelStates")
                ;
    }

    protected boolean generateAbstractFile(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        String content = generateAbstractContent(sourceDirectory, targetDirectory, path, packageName, namePrefix);
        if (content != null) {
            generate(path, packageName, content);
            return true;
        }
        return false;
    }

    protected boolean generateConcreteFile(Path path, String packageName, String namePrefix) {
        String content = generateConcreteContent(path, packageName, namePrefix);
        if (content != null) {
            generate(path, packageName, content);
            return true;
        }
        return false;
    }

    protected String generate(String template, Object... parameters) {
        return generate0(getClass(), template, packageName, new LinkedList<>(), parameters);
    }

    protected String generate(String template, List<String> imports, Object... parameters) {
        return generate0(getClass(), template, packageName, imports, parameters);
    }

    public CapabilityDescriptor getChildrenCapabilityDescriptor(String nodeType) {
        return getCapabilityDescriptor(ContentOpenableUIDescriptor.class, nodeType)
                .or(() -> getCapabilityDescriptor(ContentRootOpenableUIDescriptor.class, nodeType))
                .or(() -> getCapabilityDescriptor(ContentLayoutUIDescriptor.class, nodeType))
                .orElse(null);
    }

    public Optional<CapabilityDescriptor> getCapabilityDescriptor(Class<? extends ContentUIDescriptor> descriptorType, String nodeType) {
        Map<String, List<CapabilityDescriptor>> capabilitiesDescriptor = files.getCapabilitiesDescriptor(descriptorType);
        for (Map.Entry<String, List<CapabilityDescriptor>> entry : capabilitiesDescriptor.entrySet()) {
            for (CapabilityDescriptor capabilityDescriptor : entry.getValue()) {
                if (nodeType.equals(capabilityDescriptor.getNodeTypeName())) {
                    return Optional.of(capabilityDescriptor);
                }
            }
        }
        return Optional.empty();
    }

}
