package fr.ird.observe.client.datasource.editor.spi.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.GsonBuilder;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScopes;
import fr.ird.observe.client.datasource.editor.spi.DetectCapabilities;
import fr.ird.observe.client.datasource.editor.spi.DetectContentFiles;
import fr.ird.observe.client.datasource.editor.spi.UiMojoRunnable;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducerProvider;
import io.ultreia.java4all.util.SortedProperties;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class GenerateJavaFileSupport extends UiMojoRunnable {
    public static final String HEADER = "package %1$s;\n\n" +
            "%2$s\n\n" +
            "import javax.annotation.Generated;\n\n" +
            "@Generated(value = \"%3$s\", date = \"%4$s\")\n";
    protected final Map<String, String> i18nMapping = new LinkedHashMap<>();
    public String fileContent;
    public String packageName;
    public String cleanClassName;
    public boolean notStandalone;
    public ObserveBusinessProject businessProject;
    public String parentNodeName;
    public DetectContentFiles files;
    public Map<String, List<CapabilityDescriptor>> loadedCapabilitiesDescriptor;
    public List<CapabilityDescriptor> capabilitiesDescriptor;
    public SortedProperties capabilities;
    public NavigationScopeBuilder.Builder scopeBuilder;
    public DetectCapabilities detectCapabilities;
    public Project editModel;
    public Project selectModel;
    protected int generatedFileCount = 0;
    protected Path navigationScopePath;
    protected BeanPropertyI18nKeyProducerProvider beanPropertyI18nKeyProducerProvider;
    protected Set<String> iconPaths;
    protected BusinessModule businessModule;
    protected BusinessSubModule businessSubModule;
    protected Map<String, SortedProperties> loadedCapabilities;
    protected String className;
    private ContentUIDescriptor descriptor;

    public static String generate0(Class<?> templateType, String template, String packageName, List<String> imports, Object... parameters) {
        List<Object> realParameters = new LinkedList<>();
        for (Object parameter : parameters) {
            if (parameter == null) {
                continue;
            }
            if (parameter instanceof Class) {
                Class<?> aClass = (Class<?>) parameter;
                imports.add(aClass.getName());
                realParameters.add(aClass.getSimpleName().replace("Dto", ""));
            } else {
                realParameters.add(parameter.toString());
            }
        }
        String header = String.format(HEADER, packageName, new LinkedHashSet<>(imports).stream().map(s -> "import " + s + ";").collect(Collectors.joining("\n")), templateType.getName(), new Date());
        String content = String.format(template, realParameters.toArray(new Object[0]));
        return header + content;
    }

    public abstract String getExtensionToScan();

    public abstract String getDirectory();

    protected abstract Class<? extends ContentUIDescriptor> getDescriptorType();

    protected abstract void processPath(Path sourceDirectory, Path targetDirectory, Path uiPath, String fileContent);

    public void setFiles(DetectContentFiles files) {
        this.files = files;
    }

    public void setDetectCapabilities(DetectCapabilities detectCapabilities) {
        this.detectCapabilities = detectCapabilities;
    }

    public void generate(Path path, String packageName, String content) {
        log.debug(String.format("Will generate %s.%s", packageName, path.toFile().getName()));
        try {
            if (Files.notExists(path.getParent())) {
                Files.createDirectories(path.getParent());
            }
            log.debug(String.format("Write:\n%s", content));
            Files.write(path, content.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't write at: %s", path), e);
        }
    }

    protected String generateIconPath(String cleanClassName) {
        String suffix = cleanClassName.replace("UI", "");
        return String.format("navigation.%s.%s.%s.%s", businessModule.getName().toLowerCase(), getDirectory(), businessSubModule.getName().toLowerCase(), suffix);
    }

    protected String serializeDescriptor(NavigationScopeDescriptor descriptor) {
        if (descriptor.getIconPath() == null) {
            getLog().warn("No icon path found for: " + descriptor);
        }
        iconPaths.add(descriptor.getIconPath());
        return new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create().toJson(descriptor);
    }

    protected final String getJavaFileSuffix() {
        String simpleName = getClass().getSimpleName();
        int indexOf = simpleName.indexOf("UI");
        return simpleName.substring(indexOf + 2);
    }

    protected Path getNavigationScope(Path uiPath, String namePrefix) {
        Path sourcePath = uiPath.getParent().resolve(NavigationScopes.getScopeFilename(namePrefix + "NavigationNode"));
        navigationScopePath = sourceDirectory.getParent().resolve("resources").resolve(sourceDirectory.relativize(sourcePath));
        return navigationScopePath;
    }

    public boolean isNotStandalone(String fileContent) {
        return fileContent.contains("implements='fr.ird.observe.client.datasource.editor.api.content.NotStandaloneContentUI");
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(files);
        Objects.requireNonNull(descriptor);
        Objects.requireNonNull(detectCapabilities);
        Objects.requireNonNull(iconPaths);
    }

    @Override
    public void run() {
        beanPropertyI18nKeyProducerProvider = I18nDecoratorHelper.get();
        businessProject = ObserveBusinessProject.get();
        editModel = new Project();
        selectModel = new Project();
        Map<Path, String> filesContent = files.getFiles(getDescriptorType());
        loadedCapabilities = files.getCapabilities(getDescriptorType());
        loadedCapabilitiesDescriptor = files.getCapabilitiesDescriptor(getDescriptorType());
        filesContent.entrySet().stream().filter(p -> acceptPath(p.getValue())).forEach(p -> processPath(sourceDirectory, targetDirectory, p.getKey(), p.getValue()));
        if (generatedFileCount > 0) {
            log.info(String.format("[%s] Generate %d file(s).", getClass().getName(), generatedFileCount));
        } else {
            log.warn(String.format("[%s] No file generated.", getClass().getName()));
        }
    }


    protected void initCapabilities() {
        if (loadedCapabilities != null) {
            String nodeName = packageName + "." + cleanClassName + "NavigationNode";
            parentNodeName = detectCapabilities.parentRelations.get(nodeName);
            capabilities = loadedCapabilities.get(nodeName);
            capabilitiesDescriptor = loadedCapabilitiesDescriptor.get(nodeName);
        }
        if (capabilities == null) {
            capabilities = new SortedProperties();
        }
    }

    protected void addI118nProperty(Class<?> type, String property) {
        String key = beanPropertyI18nKeyProducerProvider.getDefaultLabelsBuilder().getI18nPropertyKey(type, property);
        registerI18nKey(property, key);
    }

    protected void registerI18nKey(String property, String key) {
        getterFile.addKey(key);
        i18nMapping.put(property, key);
    }

    public final boolean acceptPath(String fileContent) {
        this.fileContent = fileContent;
        this.notStandalone = isNotStandalone(fileContent);
        return true;
    }

    protected String cleanClassName(String className) {
        return className.replace(getJavaFileSuffix(), "").replace("Generated", "");
    }

    public void setIconPaths(Set<String> iconPaths) {
        this.iconPaths = iconPaths;
    }

    public void setDescriptor(ContentUIDescriptor descriptor) {
        this.descriptor = descriptor;
    }

    public ContentUIDescriptor getDescriptor() {
        return descriptor;
    }
}
