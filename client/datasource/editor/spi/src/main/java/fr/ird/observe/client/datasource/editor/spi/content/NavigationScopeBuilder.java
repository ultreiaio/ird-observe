package fr.ird.observe.client.datasource.editor.spi.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.stats.WithStatistics;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessReferentialPackage;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.bean.definition.JavaBeanPropertyDefinition;
import io.ultreia.java4all.lang.Objects2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.Introspector;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 20/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class NavigationScopeBuilder {

    private static final Logger log = LogManager.getLogger(Builder.class);
    public final Class<?> acceptedClass;
    protected final GenerateJavaFileSupport generator;

    public static String of(Class<?> typeMirror) {
        return typeMirror == null ? null : typeMirror.getName();
    }

    public static Map<String, String> of(Map<Class<? extends BusinessDto>, String> nodeChildTypes) {
        if (nodeChildTypes == null) {
            return null;
        }
        Map<String, String> builder = new LinkedHashMap<>();
        for (Map.Entry<Class<? extends BusinessDto>, String> entry : nodeChildTypes.entrySet()) {
            builder.put(of(entry.getKey()), entry.getValue());
        }
        return builder;
    }

    protected NavigationScopeBuilder(Class<?> acceptedClass, GenerateJavaFileSupport generator) {
        this.acceptedClass = acceptedClass;
        this.generator = generator;
    }

    public abstract Builder createBuilder(String contentUiType, String nodeType);

    public void prepareBuild(Builder builder) {
    }

    @SuppressWarnings("unused")
    public Class<?> getAcceptedClass() {
        return acceptedClass;
    }

    protected Builder newBuilder(String contentUiType, String node) {
        return new Builder(generator.packageName + "." + contentUiType, generator.packageName + "." + node, generator.selectModel, generator.editModel);
    }

    @SuppressWarnings("UnusedReturnValue")
    public class Builder {
        public final String nodeType;
        public final String nodeTypePackage;
        public final Project selectModel;
        public final Project editModel;
        public final String contentUiType;
        public final Map<String, String> typesBuilder;
        public final Map<String, String> propertiesBuilder;
        public Class<? extends BusinessDto> dtoType;
        public Class<? extends BusinessDto> mainDtoType;
        public Class<? extends BusinessDto> globalDtoType;
        public Class<? extends BusinessDto> parentDtoType;
        public IdNode<?> selectNode;
        public IdNode<?> editNode;
        public Class<? extends DtoReference> dtoReferenceType;
        public Class<? extends DtoReference> parentDtoReferenceType;
        public Class<?> nodeDataType;
        public String nodeChildType;
        public Map<Class<? extends BusinessDto>, String> nodeChildTypes;
        public BusinessModule module;
        public BusinessSubModule subModule;
        public String decoratorClassifier;
        public String childrenPropertyName;
        public String showDataPropertyName;
        public String emptyNodePropertyName;
        public String initialCountPropertyName;
        public boolean isEditNode;
        public boolean isSelectNode;
        public boolean subNode;
        public boolean noAutoLoadNode;

        public Builder(String contentUiType, String nodeType, Project selectModel, Project editModel) {
            this.nodeType = nodeType;
            this.contentUiType = contentUiType;
            String nodeTypeSimpleName = getSimpleName(nodeType);
            this.nodeTypePackage = nodeType.substring(0, nodeType.length() - nodeTypeSimpleName.length() - 1);
            this.editModel = Objects.requireNonNull(editModel);
            this.selectModel = Objects.requireNonNull(selectModel);
            this.typesBuilder = new LinkedHashMap<>();
            this.propertiesBuilder = new LinkedHashMap<>();
        }

        public NavigationScopeDescriptor build(Map<String, String> i18nMapping, String iconPath) {
            if (nodeDataType == null) {
                throw new IllegalStateException("No nodeDataType set for node scope: " + nodeType);
            }
            registerType(NavigationScope.TYPE_UI_NODE_DATA, nodeDataType);
            prepareBuild(this);
            addProperty(NavigationScope.PROPERTY_UI_ICON_PATH, iconPath);
            if (decoratorClassifier != null) {
                addProperty(NavigationScope.PROPERTY_UI_DECORATOR_CLASSIFIER, decoratorClassifier);
            }

            if (childrenPropertyName != null) {
                addProperty(NavigationScope.PROPERTY_MODEL_CHILDREN_PROPERTY_NAME, childrenPropertyName);
            }
            if (showDataPropertyName != null) {
                addProperty(NavigationScope.PROPERTY_MODEL_SHOW_DATA_PROPERTY_NAME, showDataPropertyName);
            }
            if (emptyNodePropertyName != null) {
                addProperty(NavigationScope.PROPERTY_MODEL_EMPTY_NODE_PROPERTY_NAME, emptyNodePropertyName);
            }
            if (initialCountPropertyName != null) {
                addProperty(NavigationScope.PROPERTY_MODEL_INITIAL_COUNT_PROPERTY_NAME, initialCountPropertyName);
            }
            if (subNode) {
                addProperty(NavigationScope.PROPERTY_UI_SUB_NODE, "true");
            }
            if (noAutoLoadNode) {
                addProperty(NavigationScope.PROPERTY_UI_NO_AUTO_LOAD, "true");
            }
            if (module != null) {
                Class<? extends BusinessModule> moduleType = module.getClass();
                registerType(NavigationScope.TYPE_NAVIGATION_MODULE, moduleType);
            }
            if (subModule != null) {
                Class<? extends BusinessSubModule> subModuleType = subModule.getClass();
                registerType(NavigationScope.TYPE_NAVIGATION_SUB_MODULE, subModuleType);
            }
            if (isSelectNode) {
                addProperty(NavigationScope.PROPERTY_NAVIGATION_SELECT_NODE, "true");
            }
            if (isEditNode) {
                addProperty(NavigationScope.PROPERTY_NAVIGATION_EDIT_NODE, "true");
            }
            if (selectNode != null) {
                registerType(NavigationScope.TYPE_NAVIGATION_SELECT_NODE, selectNode);
            }
            if (editNode != null) {
                registerType(NavigationScope.TYPE_NAVIGATION_EDIT_NODE, editNode);
            }
            registerType(NavigationScope.TYPE_UI_CONTENT, contentUiType);
            registerType(NavigationScope.TYPE_UI_NODE, nodeType);
            if (generator.parentNodeName != null) {
                registerType(NavigationScope.TYPE_UI_NODE_PARENT, generator.parentNodeName);
            }
            if (nodeChildType != null) {
                registerType(NavigationScope.TYPE_UI_NODE_CHILD, nodeChildType);
            }
            Permission permission = generator.getDescriptor().getPermission();
            addProperty(NavigationScope.PROPERTY_MODEL_PERMISSION, permission);
            return new NavigationScopeDescriptor(of(nodeChildTypes), typesBuilder, i18nMapping, propertiesBuilder);
        }

        public void registerType(String name, Object type) {
            if (Objects.requireNonNull(type) instanceof String) {
                registerType(name, (String) type);
                return;
            }
            if (!(type instanceof Class)) {
                type = type.getClass();
            }
            type = ((Class<?>) type).getName();
            registerType(name, (String) type);
        }

        protected void registerType(String name, String type) {
            typesBuilder.put(name, type);
        }

        public Builder setMainDtoTypeFromDtoType() {
            Class<? extends BusinessDto> mainDtoType = getMainDtoType(dtoType);
            return setMainDtoType(mainDtoType);
        }

        public Builder setMainDtoTypeFromGlobalDtoType() {
            Class<? extends BusinessDto> mainDtoType = getMainDtoType(globalDtoType);
            return setMainDtoType(mainDtoType);
        }

        public Builder setParentDtoTypeFromEditNodeParentType() {
            IdNode<?> parent = editNode.getParent();
            if (parent != null) {
                Class<? extends BusinessDto> parentDtoType = parent.getType();
                Class<? extends DtoReference> parentDtoReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(parentDtoType);
                return setParentDtoType(parentDtoType).setParentDtoReferenceType(parentDtoReferenceType);
            }
            Class<? extends BusinessDto> parentDtoType = guessParentDtoTypeFromFullyQualifiedName(editNode.getType().getName());
            Class<? extends DtoReference> parentDtoReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(parentDtoType);
            return setParentDtoType(parentDtoType).setParentDtoReferenceType(parentDtoReferenceType);
        }

        public Builder setParentDtoTypeFromSelectNodeParentType() {
            IdNode<?> parent = selectNode.getParent();
            if (parent != null) {
                Class<? extends BusinessDto> parentDtoType = parent.getType();
                Class<? extends DtoReference> parentDtoReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(parentDtoType);
                return setParentDtoType(parentDtoType).setParentDtoReferenceType(parentDtoReferenceType);
            }
            Class<? extends BusinessDto> parentDtoType = guessParentDtoTypeFromFullyQualifiedName(selectNode.getType().getName());
            Class<? extends DtoReference> parentDtoReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(parentDtoType);
            return setParentDtoType(parentDtoType).setParentDtoReferenceType(parentDtoReferenceType);
        }

        public Builder setParentDtoTypeFromSelectNodeType() {
            if (selectNode != null) {
                Class<? extends BusinessDto> parentDtoType = selectNode.getType();
                Class<? extends DtoReference> parentDtoReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(parentDtoType);
                return setParentDtoType(parentDtoType).setParentDtoReferenceType(parentDtoReferenceType);
            }
            Class<? extends BusinessDto> parentDtoType = guessParentDtoTypeFromFullyQualifiedName(editNode.getType().getName());
            Class<? extends DtoReference> parentDtoReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(parentDtoType);
            return setParentDtoType(parentDtoType).setParentDtoReferenceType(parentDtoReferenceType);
        }

        public Builder setSelectNodeFromNodeType() {
            Class<? extends IdNode<?>> selectNodeType = guessSelectNode();
            IdNode<?> selectNode = selectModel.forNodeType(selectNodeType).orElseThrow(IllegalStateException::new);
            setSelectNode(selectNode);
            editNode = editModel.forNodeType(selectNode.getParent().getClass()).orElseThrow(IllegalStateException::new);
            return this;
        }

        public Builder setSelectNodeFromMainDtoType(String selectTypeName) {
            Class<? extends BusinessDto> mainDtoType;
            if (selectTypeName == null) {
                mainDtoType = this.mainDtoType;
            } else {
                Class<BusinessDto> dtoType = forName0(selectTypeName);
                mainDtoType = ObserveBusinessProject.get().getMapping().getMainDtoType(dtoType);
            }
            IdNode<? extends BusinessDto> selectNode = getSelectNodeFromDtoType(mainDtoType);
            return setSelectNode(selectNode);
        }

        public Builder setSelectNodeFromEditNode() {
            IdNode<? extends BusinessDto> selectNode = selectModel.forNavigationNode(editNode).orElseThrow();
            return setSelectNode(selectNode);
        }

        public Builder setSelectNodeFromEditNodeParent() {
            IdNode<? extends BusinessDto> selectNode = selectModel.forNavigationNode(editNode).orElseThrow();
            return setSelectNode(selectNode.getParent());
        }

        public Builder setEditNodeFromDtoType() {
            IdNode<? extends BusinessDto> editNode = editModel.forDtoType(dtoType).orElseThrow();
            return setEditNode(editNode);
        }

        public Builder setDtoReferenceTypeFromDtoType() {
            Class<? extends DtoReference> dtoReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(dtoType);
            setDtoReferenceType(dtoReferenceType);
            return this;
        }

        public Builder setDtoReferenceTypeFromMainDtoType() {
            Class<? extends DtoReference> dtoReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(mainDtoType);
            setDtoReferenceType(dtoReferenceType);
            return this;
        }

        public Builder setDtoReferenceTypeFromGlobalDtoType() {
            Class<? extends DtoReference> dtoReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(globalDtoType);
            setDtoReferenceType(dtoReferenceType);
            return this;
        }

        public Builder setDtoTypeFromNodeType() {
            String fqn = nodeType;
            if (fqn.contains(".data")) {
                fqn = fqn.replace(nodeTypePackage, subModule.getDataPackageName());
            } else {
                fqn = fqn.replace(nodeTypePackage, subModule.getReferentialPackageName());
            }
            Class<? extends BusinessDto> dtoType = guessDtoTypeFromFullyQualifiedName(fqn);
            setDtoType(dtoType);
            return this;
        }

        public Builder setModule() {
            String packageName = nodeTypePackage;
            BusinessModule module = ObserveBusinessProject.get().getBusinessModule(packageName);
            BusinessSubModule subModule = ObserveBusinessProject.get().getBusinessSubModule(module, packageName);
            setModule(module);
            setSubModule(subModule);
            return this;
        }

        public Builder setSubNode() {
            this.subNode = true;
            return this;
        }

        public Builder setNoAutoLoadNode() {
            this.noAutoLoadNode = true;
            return this;
        }

        public Builder setSelectNode() {
            this.isSelectNode = true;
            return this;
        }

        public Builder setEditNode() {
            this.isEditNode = true;
            return this;
        }

        public Builder setNodeChildType() {
            String fqn = nodeType.replace("List", "");
            setNodeChildType(fqn);
            return this;
        }

        public Builder setChildrenPropertyName(JavaBeanDefinition javaBeanDefinition) {
            String childrenPropertyName = guessChildPropertyName(javaBeanDefinition, globalDtoType, dtoType);
            return setChildrenPropertyName(childrenPropertyName)
                    .computeInitialCountPropertyName(childrenPropertyName)
                    .computeShowDataPropertyName(childrenPropertyName);
        }

        public Builder setReferentialNodeChildTypesFromSubModule() {
            Map<Class<? extends BusinessDto>, String> builder = new LinkedHashMap<>();

            BusinessReferentialPackage referentialPackage = subModule.getReferentialPackage().orElseThrow();
            String packageName = referentialPackage.getPackageName();
            List<Class<? extends ReferentialDto>> types = new ArrayList<>(referentialPackage.getTypes());
            for (Class<? extends ReferentialDto> referentialType : types) {
                String fqn = nodeTypePackage + "." + referentialType.getName().substring(packageName.length() + 1).replace("Dto", "UINavigationNode");
                builder.put(referentialType, fqn);
            }
            setNodeChildTypes(builder);
            return this;
        }

        public Builder setChildrenPropertyName(String childrenPropertyName) {
            this.childrenPropertyName = childrenPropertyName;
            return this;
        }

        public Builder computeInitialCountPropertyName(String propertyName) {
            if (propertyName == null) {
                return this;
            }
            JavaBeanDefinition javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(parentDtoReferenceType).orElseThrow();
            String initialCountPropertyName = guessInitialCountPropertyName(javaBeanDefinition, propertyName);
            return setInitialCountPropertyName(initialCountPropertyName);
        }

        public Builder computeShowDataPropertyName(String propertyName) {
            if (propertyName == null) {
                return this;
            }
            JavaBeanDefinition javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(parentDtoReferenceType).orElseThrow();
            String showDataPropertyName = guessShowDataPropertyName(javaBeanDefinition, propertyName);
            return setShowDataPropertyName(showDataPropertyName);
        }

        public Builder computePredicateDataPropertyName(String propertyName) {
            if (propertyName == null) {
                return this;
            }
            propertyName = Introspector.decapitalize(propertyName);
            JavaBeanDefinition javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(parentDtoReferenceType).orElseThrow();
            String showDataPropertyName = guessPredicatePropertyName(javaBeanDefinition, propertyName);
            return setShowDataPropertyName(showDataPropertyName);
        }

        public Builder setShowDataPropertyName(String showDataPropertyName) {
            this.showDataPropertyName = showDataPropertyName;
            return this;
        }

        public Builder setInitialCountPropertyName(String initialCountPropertyName) {
            this.initialCountPropertyName = initialCountPropertyName;
            return this;
        }

        public Builder isString() {
            return setNodeDataType(String.class);
        }

        public Builder setNodeDataTypeFromDtoType() {
            return setNodeDataType(dtoType);
        }

        public Builder setDecoratorClassifierFromDtoType() {
            return setDecoratorClassifier(WithStatistics.class.isAssignableFrom(dtoType) ? DecoratorService.WITH_STATS_CLASSIFIER : null);
        }

        public Builder setNodeDataTypeFromDtoReferenceType() {
            return setNodeDataType(dtoReferenceType);
        }

        public Builder setNodeDataType(Class<?> nodeDataType) {
            this.nodeDataType = nodeDataType;
            return this;
        }

        public Builder setDecoratorClassifier(String decoratorClassifier) {
            this.decoratorClassifier = decoratorClassifier;
            return this;
        }

        public Builder setParentDtoType(Class<? extends BusinessDto> parentDtoType) {
            this.parentDtoType = parentDtoType;
            return this;
        }

        protected Builder setSelectNode(IdNode<?> selectNode) {
            this.selectNode = selectNode;
            if (editNode == null) {
                this.editNode = getEditNodeFromSelectNode();
            }
            return this;
        }

        public Builder setEditNode(IdNode<?> editNode) {
            this.editNode = editNode;
            return this;
        }

        protected Builder setMainDtoType(Class<? extends BusinessDto> mainDtoType) {
            this.mainDtoType = mainDtoType;
            return this;
        }

        protected Builder setDtoReferenceType(Class<? extends DtoReference> dtoReferenceType) {
            this.dtoReferenceType = dtoReferenceType;
            return this;
        }

        public Builder setNodeChildType(String nodeChildType) {
            this.nodeChildType = nodeChildType;
            return this;
        }

        public Builder setNodeChildTypes(Map<Class<? extends BusinessDto>, String> nodeChildTypes) {
            this.nodeChildTypes = nodeChildTypes;
            return this;
        }

        public Builder setGlobalDtoType(Class<? extends BusinessDto> globalDtoType) {
            this.globalDtoType = globalDtoType;
            return this;
        }

        public Builder setModule(BusinessModule module) {
            this.module = module;
            return this;
        }

        public Builder setSubModule(BusinessSubModule subModule) {
            this.subModule = subModule;
            return this;
        }

        public Builder setDtoType(Class<? extends BusinessDto> dtoType) {
            this.dtoType = dtoType;
            return this;
        }

        public Builder setParentDtoReferenceType(Class<? extends DtoReference> parentDtoReferenceType) {
            this.parentDtoReferenceType = parentDtoReferenceType;
            return this;
        }

        protected <D extends BusinessDto> IdNode<D> getSelectNodeFromDtoType(Class<D> dtoType) {
            return selectModel.forDtoType(dtoType).orElseThrow();
        }

        protected IdNode<?> getEditNodeFromSelectNode() {
            IdNode<?> editNode = editModel.forNavigationNode(selectNode).orElseThrow();
            if (editNode.isEditable()) {
                return editNode;
            }
            //FIXME Should test until parent is editable (for now this is ok, but later we will have this case)
            return editNode.getParent();
        }

        protected Class<? extends BusinessDto> guessDtoTypeFromFullyQualifiedName(String incomingFullyQualifiedName) {
            String fullyQualifiedName = incomingFullyQualifiedName
                    .replace("ListUI", "UI")
                    .replace("UINavigationNode", "Dto")
                    .replace("editor.api", "editor")
                    .replace("fr.ird.observe.client.datasource.editor.content.referential.", "fr.ird.observe.dto.referential.")
                    .replace("fr.ird.observe.client.datasource.editor.content.", "fr.ird.observe.dto.");
            return forName0(fullyQualifiedName);
        }

        protected Class<? extends BusinessDto> guessParentDtoTypeFromFullyQualifiedName(String incomingFullyQualifiedName) {
            String fullyQualifiedName = incomingFullyQualifiedName
                    .replace(".TripDto", ".ProgramDto");
            return forName0(fullyQualifiedName);
        }

        protected Class<? extends BusinessDto> getMainDtoType(Class<? extends BusinessDto> dtoType) {
            return ObserveBusinessProject.get().getMapping().getMainDtoType(dtoType);
        }

        protected Class<? extends IdNode<?>> guessSelectNode() {
            String fullyQualifiedName = nodeType
                    .replace("UINavigationNode", "Node")
                    .replace("editor.api", "editor")
                    .replace(".data.", ".")
                    .replace("fr.ird.observe.client.datasource.editor.", "fr.ird.observe.navigation.id.");
            return forName0(fullyQualifiedName);
        }

        protected String guessShowDataPropertyName(JavaBeanDefinition javaBeanDefinition, String propertyName) {
            String wantedPropertyName = propertyName + "Enabled";
            Optional<JavaBeanPropertyDefinition<?, ?>> sizeProperty = javaBeanDefinition.readProperties().filter(p -> Objects.equals(p.propertyName(), wantedPropertyName)).findAny();
            return sizeProperty.isPresent() ? wantedPropertyName : null;
        }

        protected String guessPredicatePropertyName(JavaBeanDefinition javaBeanDefinition, String propertyName) {
            Optional<JavaBeanPropertyDefinition<?, ?>> sizeProperty = javaBeanDefinition.readProperties().filter(p -> Objects.equals(p.propertyName(), propertyName)).findAny();
            return sizeProperty.isPresent() ? propertyName : null;
        }

        protected String guessInitialCountPropertyName(JavaBeanDefinition javaBeanDefinition, String propertyName) {
            //FIXME Remove any Size weak properties and use only Stat API
            Set<String> wantedPropertyNames = Set.of(propertyName + "Size", propertyName + "StatValue");
            Optional<JavaBeanPropertyDefinition<?, ?>> sizeProperty = javaBeanDefinition.readProperties().filter(p -> wantedPropertyNames.contains(p.propertyName())).findAny();
            return sizeProperty.map(JavaBeanPropertyDefinition::propertyName).orElse(null);
        }

        public String guessEnabledPropertyName(JavaBeanDefinition javaBeanDefinition, Class<?> globalDtoType, Class<?> dtoType) {
            Set<JavaBeanPropertyDefinition<?, ?>> candidates = javaBeanDefinition.readProperties().filter(p -> p.propertyName().endsWith("Enabled") && boolean.class.equals(p.type())).collect(Collectors.toSet());
            if (candidates.isEmpty()) {
                log.warn(String.format("Can't find candidate property for type: %s in: %s", dtoType, globalDtoType));
                return null;
            }

            JavaBeanPropertyDefinition<?, ?> candidate = null;
            if (candidates.size() == 1) {
                candidate = candidates.iterator().next();
            } else {
                for (JavaBeanPropertyDefinition<?, ?> candidate0 : candidates) {
                    String propertyName = candidate0.propertyName().replace("Enabled", "");
                    if (propertyName.startsWith("localmarket")) {
                        propertyName = propertyName.substring(11);
                    }
                    if (dtoType.getSimpleName().toLowerCase().equalsIgnoreCase(propertyName + "Dto")) {
                        candidate = candidate0;
                        break;
                    }
                    if (dtoType.getSimpleName().toLowerCase().equalsIgnoreCase(propertyName + "Reference")) {
                        candidate = candidate0;
                        break;
                    }
                    if (propertyName.endsWith("es") && dtoType.getSimpleName().toLowerCase().equalsIgnoreCase(propertyName.substring(0, propertyName.length() - 2) + "Dto")) {
                        candidate = candidate0;
                        break;
                    }
                }
            }
            if (candidate == null) {
                log.warn(String.format("Can't find candidate property for type: %s in: %s", dtoType, globalDtoType));
                return null;
            }
            return candidate.propertyName();
        }


        public String guessChildPropertyName(JavaBeanDefinition javaBeanDefinition, Class<?> globalDtoType, Class<?> dtoType) {
            Set<JavaBeanPropertyDefinition<?, ?>> candidates = javaBeanDefinition.readAndWriteProperties().filter(p -> !Objects.equals(p.propertyName(), ContainerDto.PROPERTY_CHILDREN) && Collection.class.isAssignableFrom(p.type())).collect(Collectors.toSet());
            if (candidates.isEmpty()) {
                throw new IllegalStateException(String.format("Can't find candidate property for type: %s in: %s", dtoType, globalDtoType));
            }

            JavaBeanPropertyDefinition<?, ?> candidate = null;
            if (candidates.size() == 1) {
                candidate = candidates.iterator().next();
            } else {
                for (JavaBeanPropertyDefinition<?, ?> candidate0 : candidates) {
                    String propertyName = candidate0.propertyName();
                    if (propertyName.startsWith("localmarket")) {
                        propertyName = propertyName.substring(11);
                    }
                    if (dtoType.getSimpleName().toLowerCase().equalsIgnoreCase(propertyName + "Dto")) {
                        candidate = candidate0;
                        break;
                    }
                    if (propertyName.endsWith("es") && dtoType.getSimpleName().toLowerCase().equalsIgnoreCase(propertyName.substring(0, propertyName.length() - 2) + "Dto")) {
                        candidate = candidate0;
                        break;
                    }
                }
            }
            if (candidate == null) {
                throw new IllegalStateException(String.format("More than one candidate properties for type: %s in: %s %s", dtoType, globalDtoType, candidates.stream().map(JavaBeanPropertyDefinition::propertyName).collect(Collectors.toList())));
            }
            return candidate.propertyName();
        }

        public <T> Class<T> forName0(String fqn) {
            return Objects2.forName(fqn);
        }

        public String getSimpleName(String fullyQualifiedName) {
            return fullyQualifiedName.substring(fullyQualifiedName.lastIndexOf(".") + 1);
        }

        protected void addProperty(String propertyName, Object value) {
            propertiesBuilder.put(propertyName, Objects.requireNonNull(value).toString());
        }

        public void setNotStandalone() {
            addProperty(NavigationScope.PROPERTY_MODEL_STANDALONE, "false");
        }
    }
}
