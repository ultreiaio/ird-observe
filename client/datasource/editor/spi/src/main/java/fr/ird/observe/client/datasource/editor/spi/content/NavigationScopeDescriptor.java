package fr.ird.observe.client.datasource.editor.spi.content;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 20/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class NavigationScopeDescriptor {

    private final Map<String, String> properties;
    private final Map<String, String> i18nMapping;
    private final Map<String, String> types;
    private final Map<String, String> nodeChildTypes;

    protected static Map<String, String> getMap(Map<String, String> map) {
        return map == null || map.isEmpty() ? null : new TreeMap<>(map);
    }

    public NavigationScopeDescriptor(Map<String, String> nodeChildTypes,
                                     Map<String, String> types,
                                     Map<String, String> i18nMapping,
                                     Map<String, String> properties) {
        this.types = getMap(types);
        this.properties = getMap(properties);
        this.nodeChildTypes = getMap(nodeChildTypes);
        this.i18nMapping = getMap(i18nMapping);
    }

    public String getIconPath() {
        return properties.get(NavigationScope.PROPERTY_UI_ICON_PATH);
    }

    @Override
    public String toString() {
        return com.google.common.base.MoreObjects.toStringHelper(this)
                .omitNullValues()
                .add("properties", properties)
                .add("types", types)
                .add("i18nMapping", i18nMapping)
                .add("nodeChildTypes", nodeChildTypes)
                .toString();
    }
}
