package fr.ird.observe.client.datasource.editor.spi.content.data.edit;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUINavigationCapability;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationCapabilityHelper;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentEditUINavigationCapability extends GenerateContentEditUISupport {

    public static final String ABSTRACT_MODEL_WITH_CHILDREN = "" +
            "public class Generated%1$sNavigationCapability extends ContentEditUINavigationCapability<%1$sNavigationNode> implements ContainerCapability<%1$sNavigationNode> {\n\n" +
            "%2$s" +
            "    public Generated%1$sNavigationCapability(%1$sNavigationNode node) {\n" +
            "        super(node);\n" +
            "    }\n\n" +
            "%3$s" +
            "}\n";
    public static final String ABSTRACT_MODEL = "" +
            "public class Generated%1$sNavigationCapability extends ContentEditUINavigationCapability<%1$sNavigationNode> {\n\n" +
            "    public Generated%1$sNavigationCapability(%1$sNavigationNode node) {\n" +
            "        super(node);\n" +
            "    }\n\n" +
            "}\n";

    @Override
    protected String generateAbstractContent0(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        imports.add(ContentEditUINavigationCapability.class.getName());
        uiNavigationCapabilityHelper.useContainerCapabilities(imports);
        uiNavigationCapabilityHelper.useReferenceContainerCapabilities(imports);
        if (capabilitiesDescriptor != null) {
            return generate(ABSTRACT_MODEL_WITH_CHILDREN, imports, cleanClassName,
                            uiNavigationCapabilityHelper.generateStaticFields(),
                            ContentUINavigationCapabilityHelper.GET_NODE_TYPES + uiNavigationCapabilityHelper.generateCreateNodeMethods());

        } else {
            return generate(ABSTRACT_MODEL, imports, cleanClassName);
        }
    }

    @Override
    protected void init(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        super.init(sourceDirectory, targetDirectory, path, packageName, namePrefix);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        return uiNavigationCapabilityHelper.generateNavigationCapabilityConcreteContent();
    }
}
