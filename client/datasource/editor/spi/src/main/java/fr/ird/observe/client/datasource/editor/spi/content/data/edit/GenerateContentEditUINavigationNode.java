package fr.ird.observe.client.datasource.editor.spi.content.data.edit;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUINavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUINavigationNode;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationContextHelper;
import fr.ird.observe.dto.reference.DtoReference;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 06/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentEditUINavigationNode extends GenerateContentEditUISupport {

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        imports.add(ContentEditUINavigationInitializer.class.getName());
        imports.add(ContentEditUINavigationNode.class.getName());
        imports.add(referenceType.getName());
        imports.add(DtoReference.class.getName());
        String context = uiNavigationNodeHelper.generateCreateNodeEdit(imports, referenceType.getSimpleName());
        context += uiNavigationNodeHelper.generateCreateContextWithCapability();
        context += uiNavigationNodeHelper.generateParentNode();
        context += uiNavigationNodeHelper.generateCreateNodeMethods(imports);
        context += ContentUINavigationContextHelper.generateGetReference(imports, referenceType);
        context += ContentUINavigationContextHelper.generateGetParentReference(imports, parentReferenceType);
//        context += ContentUINavigationContextHelper.generateGetEditableServiceSpi(imports, dtoType, referenceType, serviceType);
        return uiNavigationNodeHelper.generateContent(imports, context);
    }

}
