package fr.ird.observe.client.datasource.editor.spi.content.data.edit;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.edit.ContentEditUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeBuilder;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeDescriptor;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;

import java.nio.file.Path;
import java.util.Optional;

/**
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentEditUINavigationScope extends GenerateContentEditUISupport {

    public static class Builder extends NavigationScopeBuilder {

        private final GenerateContentEditUISupport generator;

        public Builder(GenerateContentEditUISupport generator) {
            super(ContentEditUINavigationNode.class, generator);
            this.generator = generator;
        }

        @Override
        public Builder createBuilder(String contentUiType, String nodeType) {
            return newBuilder(contentUiType, nodeType)
                    .setModule()
                    .setSelectNode()
                    .setDtoType(generator.dtoType)
                    .setDecoratorClassifierFromDtoType()
                    .setDtoReferenceTypeFromDtoType()
                    .setMainDtoTypeFromDtoType()
                    .setSelectNodeFromNodeType()
                    .setParentDtoTypeFromSelectNodeParentType()
                    .setNodeDataTypeFromDtoReferenceType();
        }

        @Override
        public void prepareBuild(Builder builder) {
            builder.registerType(NavigationScope.TYPE_MODEL_PARENT, builder.parentDtoType);
            builder.registerType(NavigationScope.TYPE_MODEL_PARENT_REFERENCE, builder.parentDtoReferenceType);
            builder.registerType(NavigationScope.TYPE_MODEL_MAIN, builder.dtoType);
            builder.registerType(NavigationScope.TYPE_MODEL_MAIN_REFERENCE, builder.dtoReferenceType);

            JavaBeanDefinition javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(builder.parentDtoReferenceType).orElseThrow(IllegalStateException::new);
            String propertyName = builder.guessEnabledPropertyName(javaBeanDefinition, builder.parentDtoReferenceType, builder.dtoReferenceType);
            if (propertyName != null) {
                builder.setShowDataPropertyName(propertyName);
            }
            if (builder.showDataPropertyName == null) {
                CapabilityDescriptor capabilityDescriptor = generator.getChildrenCapabilityDescriptor(builder.nodeType);
                if (capabilityDescriptor != null) {
                    Optional<String> optionalPredicate = capabilityDescriptor.getOptionalPredicate();
                    optionalPredicate.ifPresent(builder::computePredicateDataPropertyName);
                }
            }
        }
    }

    @Override
    protected Path getConcreteFilePath(Path uiPath, String namePrefix) {
        return getNavigationScope(uiPath, namePrefix);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        addI118nProperty(dtoType, "type");
        addI118nProperty(dtoType, "title");
        addI118nProperty(dtoType, "navigation.unsaved");
        addI118nProperty(dtoType, "action.add");
        addI118nProperty(dtoType, "action.add.tip");
        NavigationScopeDescriptor descriptor = scopeBuilder.build(i18nMapping, iconPath);
        return serializeDescriptor(descriptor);
    }
}
