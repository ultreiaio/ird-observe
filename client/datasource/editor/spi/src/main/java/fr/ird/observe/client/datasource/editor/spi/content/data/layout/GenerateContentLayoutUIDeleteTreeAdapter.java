package fr.ird.observe.client.datasource.editor.spi.content.data.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeleteLayoutTreeAdapter;
import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeleteRequest;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

/**
 * Created on 13/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public class GenerateContentLayoutUIDeleteTreeAdapter extends GenerateContentLayoutUISupport {

    private static final String CONTENT = "" +
            "public class %1$sDeleteTreeAdapter extends DeleteLayoutTreeAdapter<%2$s> {\n\n" +
            "    public static Function<DeleteRequest, %1$sDeleteTreeAdapter> create(%1$s ui) {\n" +
            "        return r -> new %1$sDeleteTreeAdapter(ui.getModel().getSource());\n" +
            "    }\n\n" +
            "    public %1$sDeleteTreeAdapter(%1$sNavigationNode incomingNode) {\n" +
            "        super(incomingNode, incomingNode::getParent);\n" +
            "    }\n\n" +
            "}\n";

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        imports.add(DeleteRequest.class.getName());
        imports.add(DeleteLayoutTreeAdapter.class.getName());
        imports.add(Function.class.getName());
        imports.add(parentNodeName);
        String parentNodeName = this.parentNodeName.substring(this.parentNodeName.lastIndexOf(".") + 1);
        return generate(CONTENT, imports, namePrefix, parentNodeName);
    }
}
