package fr.ird.observe.client.datasource.editor.spi.content.data.layout;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.layout.ContentLayoutUIModelStates;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIModelStatesHelper;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 08/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public class GenerateContentLayoutUIModelStates extends GenerateContentLayoutUISupport {
    private static final String GENERATED_CONSTRUCTOR = "" +
            "    public Generated%1$sUIModelStates(Generated%1$sUIModel model) {\n" +
            "        super(model, %1$sDto.newDto(new java.util.Date()), model.getSource().getInitializer().getSelectedId());\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public final %1$sUINavigationNode source() {\n" +
            "        return (%1$sUINavigationNode) super.source();\n" +
            "    }\n\n";
    private static final String CONSTRUCTOR = "" +
            "    public %1$sUIModelStates(Generated%1$sUIModel model) {\n" +
            "        super(model);\n" +
            "    }\n\n";


    @Override
    protected String generateAbstractContent0(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        imports.add(ContentLayoutUIModelStates.class.getName());
        imports.add(dtoType.getName());
        String context = String.format(GENERATED_CONSTRUCTOR, dtoType.getSimpleName().replace("Dto", ""));
        context += ContentUIModelStatesHelper.addSavePredicate(sourceDirectory, targetDirectory, path, namePrefix);
        return uiModelStatesHelper.generateGeneratedContent(imports, "ContentLayoutUIModelStates<" + dtoType.getSimpleName() + ">",
                                                            context);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        return uiModelStatesHelper.generateContent(imports, String.format(CONSTRUCTOR, dtoType.getSimpleName().replace("Dto", "")));
    }
}
