package fr.ird.observe.client.datasource.editor.spi.content.data.list;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUI;
import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.ContentNodeType;
import fr.ird.observe.client.datasource.editor.spi.content.ContentUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateJavaFileSupport;
import io.ultreia.java4all.util.SortedProperties;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Created on 06/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentListUIDescriptor implements ContentUIDescriptor {
    private static Map<Path, String> filesContent;
    private static Map<String, SortedProperties> capabilities;
    private static Map<String, List<CapabilityDescriptor>> capabilitiesDescriptor;

    @Override
    public Map<Path, String> getFilesContent() {
        return filesContent;
    }

    @Override
    public void setFilesContent(Map<Path, String> filesContent) {
        ContentListUIDescriptor.filesContent = filesContent;
    }

    @Override
    public Map<String, SortedProperties> geCapabilities() {
        return capabilities;
    }

    @Override
    public void setCapabilities(Map<String, SortedProperties> capabilities) {
        ContentListUIDescriptor.capabilities = capabilities;
    }

    @Override
    public SortedProperties loadCapabilityFile(Path capabilitiesFile, String packageName, String cleanClassName) {
        SortedProperties capabilities = new SortedProperties();
        capabilities.put("0*", packageName + "." + cleanClassName.replace("ListUI", "UINavigationNode"));
        return capabilities;
    }

    @Override
    public String getExtensionToScan() {
        return "ListUI.jaxx";
    }

    @Override
    public String getDirectory() {
        return "data";
    }

    @Override
    public Class<? extends ContentUI> getUiType() {
        return ContentListUI.class;
    }

    @Override
    public ContentNodeType getContentNodeType() {
        return ContentNodeType.LIST;
    }

    @Override
    public List<Class<? extends GenerateJavaFileSupport>> getGeneratorTypes() {
        return List.of(GenerateContentListUIModel.class,
                                GenerateContentListUINavigationCapability.class,
                                GenerateContentListUINavigationHandler.class,
                                GenerateContentListUINavigationContext.class,
                                GenerateContentListUINavigationNode.class,
                                GenerateContentListUINavigationScope.class,
                                GenerateContentListUIHandler.class,
                                GenerateContentListUIModelStates.class
        );
    }

    @Override
    public Map<String, List<CapabilityDescriptor>> getCapabilitiesDescriptor() {
        return capabilitiesDescriptor;
    }

    @Override
    public void setCapabilitiesDescriptor(Map<String, List<CapabilityDescriptor>> capabilities) {
        capabilitiesDescriptor = capabilities;
    }

    @Override
    public String getReferenceProperty() {
        return "getParentReference()";
    }
}
