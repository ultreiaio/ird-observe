package fr.ird.observe.client.datasource.editor.spi.content.data.list;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.list.ContentListUINavigationCapability;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationCapabilityHelper;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentListUINavigationCapability extends GenerateContentListUISupport {
    public static final String ABSTRACT_MODEL = "" +
            "public class Generated%1$sNavigationCapability extends ContentListUINavigationCapability<%1$sNavigationNode> {\n\n" +
            "%2$s" +
            "    public Generated%1$sNavigationCapability(%1$sNavigationNode node) {\n" +
            "        super(node);\n" +
            "    }\n\n" +
            "%3$s" +
            "}\n";
    public static final String CREATE_CHILD_NODE = "" +
            "    @Override\n" +
            "    public %1$sNavigationNode createChildNode(DtoReference reference) {\n" +
            "        return getNode().new%1$sNavigationNode((%2$s) reference);\n" +
            "    }\n\n";

    @Override
    protected String generateAbstractContent0(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        imports.add(ContentListUINavigationCapability.class.getName());
        uiNavigationCapabilityHelper.useContainerCapabilities(imports);
        uiNavigationCapabilityHelper.useReferenceContainerCapabilities(imports);
        String extraMethods = ContentUINavigationCapabilityHelper.GET_NODE_TYPES + ContentUINavigationCapabilityHelper.GET_TYPES;
        extraMethods += uiNavigationCapabilityHelper.generateCreateNodeMethods();
        extraMethods += uiNavigationCapabilityHelper.generateGetPositionNodeMethod("getNode().getInitializer().getReferenceComparator()");
        extraMethods += String.format(CREATE_CHILD_NODE, cleanClassName.replace("ListUI", "UI"), referenceType.getName());
        return generate(ABSTRACT_MODEL, imports, cleanClassName,
                        uiNavigationCapabilityHelper.generateStaticFields(),
                        extraMethods);
    }

    @Override
    protected void init(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        super.init(sourceDirectory, targetDirectory, path, packageName, namePrefix);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        return uiNavigationCapabilityHelper.generateNavigationCapabilityConcreteContent();
    }

}
