package fr.ird.observe.client.datasource.editor.spi.content.data.openable;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveTreeAdapter;
import fr.ird.observe.services.service.data.MoveRequest;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

/**
 * Created on 13/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public class GenerateContentOpenableUIMoveTreeAdapter extends GenerateContentOpenableUISupport {

    private static final String CONTENT = "" +
            "public class %1$sUIMoveTreeAdapter extends MoveTreeAdapter<%1$sListUINavigationNode, %1$sListUINavigationNode, %1$sUINavigationNode> {\n" +
            "\n" +
            "    public static Function<MoveRequest, %1$sUIMoveTreeAdapter> create(%1$sListUI ui) {\n" +
            "        return r -> new %1$sUIMoveTreeAdapter(ui.getModel().getSource());\n" +
            "    }\n" +
            "\n" +
            "    public static Function<MoveRequest, %1$sUIMoveTreeAdapter> create(%1$sUI ui) {\n" +
            "        return r -> new %1$sUIMoveTreeAdapter(ui.getModel().getSource());\n" +
            "    }\n\n" +
            "    public %1$sUIMoveTreeAdapter(%1$sListUINavigationNode incomingNode) {\n" +
            "        super(incomingNode, e -> incomingNode);\n" +
            "    }\n\n" +
            "    public %1$sUIMoveTreeAdapter(%1$sUINavigationNode incomingNode) {\n" +
            "        super(incomingNode, e -> incomingNode.getParent());\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public %1$sListUINavigationNode getNewParentNode(%1$sListUINavigationNode oldParentNode, String newParentId) {\n" +
            "        return oldParentNode.%2$s(newParentId);\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public %1$sUINavigationNode getNewSelectedNodeFromNewParentNode(%1$sListUINavigationNode newParentNode, String newIdToSelect) {\n" +
            "        return newParentNode.get%1$sUINavigationNode(newIdToSelect);\n" +
            "    }\n\n" +
            "}\n";

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        imports.add(Set.class.getName());
        imports.add(MoveTreeAdapter.class.getName());
        imports.add(MoveRequest.class.getName());
        imports.add(Function.class.getName());
        String siblingMethod = "findParentSibling";
        if (scopeBuilder.selectNode.getParent() == null) {
            siblingMethod = "findSibling";
        }
        return generate(CONTENT, imports, dtoNamePrefix, siblingMethod);
    }
}
