package fr.ird.observe.client.datasource.editor.spi.content.data.openable;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.open.ContentOpenableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeBuilder;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeDescriptor;
import fr.ird.observe.dto.reference.DataDtoReference;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.bean.definition.JavaBeanPropertyDefinition;

import java.nio.file.Path;

/**
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentOpenableUINavigationScope extends GenerateContentOpenableUISupport {

    public static class Builder extends NavigationScopeBuilder {

        private final GenerateContentOpenableUISupport generator;

        public Builder(GenerateContentOpenableUISupport generator) {
            super(ContentOpenableUINavigationNode.class, generator);
            this.generator = generator;
        }

        @Override
        public Builder createBuilder(String contentUiType, String nodeType) {
            return newBuilder(contentUiType, nodeType)
                    .setModule()
                    .setSelectNode()
                    .setEditNode()
                    .setDtoType(generator.dtoType)
                    .setDtoReferenceTypeFromDtoType()
                    .setNodeDataTypeFromDtoReferenceType()
                    .setDecoratorClassifierFromDtoType()
                    .setEditNodeFromDtoType()
                    .setSelectNodeFromEditNode()
                    .setParentDtoTypeFromEditNodeParentType();
        }

        @Override
        public void prepareBuild(Builder builder) {
            JavaBeanDefinition javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(builder.dtoReferenceType).orElseThrow();
            @SuppressWarnings("unchecked") JavaBeanPropertyDefinition<DataDtoReference, ?> propertyDefinition = (JavaBeanPropertyDefinition<DataDtoReference, ?>) javaBeanDefinition.properties().get("emptyNode");
            if (propertyDefinition != null && propertyDefinition.canRead()) {
                builder.emptyNodePropertyName = propertyDefinition.propertyName();
            }
            builder.registerType(NavigationScope.TYPE_MODEL_PARENT, builder.parentDtoType);
            builder.registerType(NavigationScope.TYPE_MODEL_PARENT_REFERENCE, builder.parentDtoReferenceType);
            builder.registerType(NavigationScope.TYPE_MODEL_MAIN, builder.dtoType);
            builder.registerType(NavigationScope.TYPE_MODEL_MAIN_REFERENCE, builder.dtoReferenceType);
        }
    }

    @Override
    protected Path getConcreteFilePath(Path uiPath, String namePrefix) {
        return getNavigationScope(uiPath, namePrefix);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        addI118nProperty(dtoType, "type");
        addI118nProperty(dtoType, "title");
        addI118nProperty(dtoType, "message.not.open");
        addI118nProperty(dtoType, "navigation.unsaved");
        addI118nProperty(dtoType, "action.create");
        addI118nProperty(dtoType, "action.move");
        addI118nProperty(dtoType, "action.move.tip");
        addI118nProperty(dtoType, "action.move.choose.parent.title");
        addI118nProperty(dtoType, "action.move.choose.parent.message");
        NavigationScopeDescriptor descriptor = scopeBuilder.build(i18nMapping, iconPath);
        return serializeDescriptor(descriptor);
    }
}
