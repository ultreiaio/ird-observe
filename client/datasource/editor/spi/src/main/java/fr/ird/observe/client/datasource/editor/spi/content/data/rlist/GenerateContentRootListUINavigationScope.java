package fr.ird.observe.client.datasource.editor.spi.content.data.rlist;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeBuilder;

import java.beans.Introspector;
import java.nio.file.Path;

/**
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentRootListUINavigationScope extends GenerateContentRootListUISupport {

    public static class Builder extends NavigationScopeBuilder {

        private final GenerateContentRootListUISupport generator;

        public Builder(GenerateContentRootListUISupport generator) {
            super(ContentRootListUINavigationNode.class, generator);
            this.generator = generator;
        }

        @Override
        public Builder createBuilder(String contentUiType, String nodeType) {
            return newBuilder(contentUiType, nodeType)
                    .setModule()
                    .setNodeChildType()
                    .setDtoType(generator.dtoType)
                    .setDtoReferenceTypeFromDtoType()
                    .setEditNodeFromDtoType()
                    .setSelectNodeFromEditNodeParent()
                    .setNodeDataTypeFromDtoType()
                    .setDecoratorClassifierFromDtoType();
        }

        @Override
        public void prepareBuild(Builder builder) {
            if (builder.selectNode == null) {
                builder.setEditNode();
            }
            CapabilityDescriptor capabilityDescriptor = generator.getChildrenCapabilityDescriptor(builder.nodeType);
            if (capabilityDescriptor != null) {
                String propertyName = Introspector.decapitalize(capabilityDescriptor.getMethodName().replace("RootListUINavigationNode", ""));
                builder.computeInitialCountPropertyName(propertyName).computeShowDataPropertyName(propertyName);
            }
            builder.registerType(NavigationScope.TYPE_MODEL_MAIN, builder.dtoType);
            builder.registerType(NavigationScope.TYPE_MODEL_MAIN_REFERENCE, builder.dtoReferenceType);
        }
    }

    @Override
    protected Path getConcreteFilePath(Path uiPath, String namePrefix) {
        return getNavigationScope(uiPath, namePrefix);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        addI118nProperty(dtoType, "type");
        addI118nProperty(dtoType, "list.title");
        addI118nProperty(dtoType, "action.create");
        addI118nProperty(dtoType, "list.navigation.node");
        addI118nProperty(dtoType, "root.list.message.none");
        return serializeDescriptor(scopeBuilder.build(i18nMapping, iconPath));
    }
}
