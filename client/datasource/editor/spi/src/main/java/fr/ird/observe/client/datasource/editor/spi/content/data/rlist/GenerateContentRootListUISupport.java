package fr.ird.observe.client.datasource.editor.spi.content.data.rlist;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.data.rlist.ContentRootListUI;
import fr.ird.observe.client.datasource.editor.spi.content.ContentUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DtoReference;

import java.nio.file.Path;

/**
 * Created on 31/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class GenerateContentRootListUISupport extends GenerateContentUISupport {
    String dtoPackage;
    String dtoNamePrefix;
    String dtoFullyQualifiedName;
    Class<? extends RootOpenableDto> dtoType;
    Class<? extends DtoReference> referenceType;

    @Override
    protected Class<? extends ContentUIDescriptor> getDescriptorType() {
        return ContentRootListUIDescriptor.class;
    }

    @Override
    public final String getExtensionToScan() {
        return "ListUI.jaxx";
    }

    @Override
    public Class<? extends ContentUI> getUiType() {
        return ContentRootListUI.class;
    }

    @SuppressWarnings({"unchecked"})
    protected void init(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        super.init(sourceDirectory, targetDirectory, path, packageName, namePrefix);
        dtoPackage = businessSubModule.getDataPackageName();
        dtoNamePrefix = namePrefix.replace("ListUI", "");
        dtoFullyQualifiedName = dtoPackage + "." + dtoNamePrefix + "Dto";
        try {
            dtoType = (Class<? extends RootOpenableDto>) Thread.currentThread().getContextClassLoader().loadClass(dtoFullyQualifiedName);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(String.format("Can't find dto type: %s", dtoFullyQualifiedName), e);
        }
        scopeBuilder = new GenerateContentRootListUINavigationScope.Builder(this).createBuilder(cleanClassName, cleanClassName + "NavigationNode");
        referenceType = scopeBuilder.dtoReferenceType;
    }
}
