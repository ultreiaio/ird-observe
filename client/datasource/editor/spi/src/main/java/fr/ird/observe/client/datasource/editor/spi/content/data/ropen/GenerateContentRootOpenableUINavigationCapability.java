package fr.ird.observe.client.datasource.editor.spi.content.data.ropen;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.ropen.ContentRootOpenableUINavigationCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationHandler;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationCapabilityHelper;
import fr.ird.observe.dto.reference.DtoReference;

import java.nio.file.Path;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 05/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentRootOpenableUINavigationCapability extends GenerateContentRootOpenableUISupport {

    public static final String ABSTRACT_MODEL = "" +
            "public class Generated%1$sNavigationCapability extends ContentRootOpenableUINavigationCapability<%1$sNavigationNode> %2$s {\n\n" +
            "%3$s" +
            "    public Generated%1$sNavigationCapability(%1$sNavigationNode node) {\n" +
            "        super(node);\n" +
            "    }\n\n" +
            "%4$s" +
            "}\n";

    @Override
    protected String generateAbstractContent0(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {

        List<String> imports = new LinkedList<>();
        imports.add(ContentRootOpenableUINavigationCapability.class.getName());

        boolean addReferenceContainerCapability = uiNavigationCapabilityHelper.useReferenceContainerCapabilities(imports);
        boolean useContainerCapabilities = uiNavigationCapabilityHelper.useContainerCapabilities(imports);
        boolean withCapabilities = useContainerCapabilities || addReferenceContainerCapability;

        String extraMethods = "";
        String implementsState = "";
        String staticsField = "";
        if (withCapabilities) {
            List<String> contracts = new LinkedList<>();
            extraMethods += ContentUINavigationCapabilityHelper.GET_NODE_TYPES;
            if (addReferenceContainerCapability) {
                contracts.add(" ReferenceContainerCapability<" + cleanClassName + "NavigationNode>");
                extraMethods += "\n" + ContentUINavigationCapabilityHelper.GET_TYPES;
                extraMethods += ContentUINavigationCapabilityHelper.ACCEPT_TYPES;
                extraMethods += uiNavigationCapabilityHelper.generateCreateChildNodeMethod();
                extraMethods += uiNavigationCapabilityHelper.generateGetPositionNodeMethod(null);
            } else {
                imports.add(DtoReference.class.getName());
                imports.add(Comparator.class.getName());
                imports.add(NavigationHandler.class.getName());
            }
            if (useContainerCapabilities) {
                contracts.add(" ContainerCapability<" + cleanClassName + "NavigationNode>");
            }
            extraMethods += uiNavigationCapabilityHelper.generateCreateNodeMethods();
            extraMethods += uiNavigationCapabilityHelper.generateNodeUpdaterMethod(imports);
            staticsField = uiNavigationCapabilityHelper.generateStaticFields();
            implementsState = " implements " + String.join(",", contracts);
        }
        return generate(ABSTRACT_MODEL,
                        imports,
                        cleanClassName,
                        implementsState,
                        staticsField,
                        extraMethods);

    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        return uiNavigationCapabilityHelper.generateNavigationCapabilityConcreteContent();
    }

}
