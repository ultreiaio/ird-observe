package fr.ird.observe.client.datasource.editor.spi.content.data.simple;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIModelHelper;

import java.nio.file.Path;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentSimpleUIModel extends GenerateContentSimpleUISupport {
    public static final String CREATE_MODEL = "" +
            "    public static %1$sModel create(NavigationNode parentSource, java.util.function.Supplier<%2$s> parentReference) {\n" +
            "        %1$sNavigationNode node = %1$sNavigationNode.create(parentReference);\n" +
            "        node.setParent(parentSource);\n" +
            "        return new %1$sModel(node);\n" +
            "    }\n\n";

    public static final String TO_SAVE_REQUEST = "" +
            "    @Override\n" +
            "    public SaveRequest<%1$s> toSaveRequest() {\n" +
            "        return SaveRequest.create(getStates().getSelectedId(), () -> getStates().getForm().getObject(), getStates()::getBeanToSave, getStates().savePredicate());\n" +
            "    }\n\n";

    //FIXME Generate only concrete
    @Override
    protected String generateAbstractContent0(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        String extraMethods = "";
        extraMethods += String.format(CREATE_MODEL, cleanClassName, parentReferenceType.getName());
        extraMethods += ContentUIModelHelper.generateStates(cleanClassName);
        return uiModelHelper.generateModelGeneratedContent(dtoType, null, extraMethods, String.format(TO_SAVE_REQUEST, dtoType.getSimpleName()));
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        return uiModelHelper.generateModelConcreteContent();
    }
}
