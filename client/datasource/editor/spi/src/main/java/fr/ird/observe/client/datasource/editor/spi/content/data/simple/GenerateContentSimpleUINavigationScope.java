package fr.ird.observe.client.datasource.editor.spi.content.data.simple;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeBuilder;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeDescriptor;

import java.nio.file.Path;

/**
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentSimpleUINavigationScope extends GenerateContentSimpleUISupport {

    public static class Builder extends NavigationScopeBuilder {

        private final GenerateContentSimpleUISupport generator;

        public Builder(GenerateContentSimpleUISupport generator) {
            super(ContentSimpleUINavigationNode.class, generator);
            this.generator = generator;
        }

        @Override
        public Builder createBuilder(String contentUiType, String nodeType) {
            String parentNode = generator.detectCapabilities.parentRelations.get(generator.packageName + "." + nodeType).replace("UINavigationNode", "Dto");
            String selectTypeName = parentNode.replace(generator.packageName, generator.dtoPackage);

            if (selectTypeName.startsWith(parentNode)) {
                // parent node is not in this package
                selectTypeName = null;
            }
            Builder builder = newBuilder(contentUiType, nodeType)
                    .setModule()
                    .setDtoTypeFromNodeType()
                    .setMainDtoTypeFromDtoType()
                    .setDtoReferenceTypeFromMainDtoType()
                    .setSelectNodeFromMainDtoType(selectTypeName)
                    .setParentDtoTypeFromSelectNodeType()
                    .setNodeDataTypeFromDtoType();
            if (selectTypeName != null) {
                builder.setSubNode();
            }
            return builder;
        }

        @Override
        public void prepareBuild(Builder builder) {
            builder.registerType(NavigationScope.TYPE_MODEL_MAIN, builder.dtoType);
            builder.registerType(NavigationScope.TYPE_MODEL_PARENT, builder.parentDtoType);
            builder.registerType(NavigationScope.TYPE_MODEL_PARENT_REFERENCE, builder.parentDtoReferenceType);
        }
    }

    @Override
    protected Path getConcreteFilePath(Path uiPath, String namePrefix) {
        return getNavigationScope(uiPath, namePrefix);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        addI118nProperty(dtoType, "type");
        NavigationScopeDescriptor descriptor = scopeBuilder.build(i18nMapping, iconPath);
        return serializeDescriptor(descriptor);
    }
}
