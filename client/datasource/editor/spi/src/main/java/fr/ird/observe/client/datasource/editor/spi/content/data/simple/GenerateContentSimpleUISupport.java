package fr.ird.observe.client.datasource.editor.spi.content.data.simple;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUI;
import fr.ird.observe.client.datasource.editor.spi.content.ContentUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.reference.DtoReference;

import java.nio.file.Path;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class GenerateContentSimpleUISupport extends GenerateContentUISupport {

    String dtoPackage;
    String dtoNamePrefix;
    String dtoFullyQualifiedName;
    Class<? extends SimpleDto> dtoType;
    Class<? extends DtoReference> parentReferenceType;

    @Override
    protected Class<? extends ContentUIDescriptor> getDescriptorType() {
        return ContentSimpleUIDescriptor.class;
    }

    @Override
    public Class<? extends ContentUI> getUiType() {
        return ContentSimpleUI.class;
    }

    @SuppressWarnings("unchecked")
    protected void init(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        super.init(sourceDirectory, targetDirectory, path, packageName, namePrefix);
        dtoPackage = businessSubModule.getDataPackageName();
        dtoNamePrefix = namePrefix.replace("UI", "");
        dtoFullyQualifiedName = dtoPackage + "." + dtoNamePrefix + "Dto";
        try {
            dtoType = (Class<? extends SimpleDto>) Thread.currentThread().getContextClassLoader().loadClass(dtoFullyQualifiedName);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(String.format("Can't find dto type: %s", dtoFullyQualifiedName), e);
        }
        scopeBuilder = new GenerateContentSimpleUINavigationScope.Builder(this).createBuilder(cleanClassName, cleanClassName + "NavigationNode");
        parentReferenceType = scopeBuilder.parentDtoReferenceType;
    }

}
