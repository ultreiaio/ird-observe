package fr.ird.observe.client.datasource.editor.spi.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUIInitializer;
import fr.ird.observe.client.datasource.editor.api.content.EditableContentUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.layout.MoveLayoutAction;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.ContentSimpleUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.data.table.NotStandaloneContentTableUIHandler;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIHandlerHelper;
import fr.ird.observe.dto.data.UsingLayout;
import fr.ird.observe.dto.data.WithProportion;
import fr.ird.observe.services.service.data.MoveLayoutRequest;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentTableUIHandler extends GenerateContentTableUISupport {

    public static final String ABSTRACT_MODEL = "" +
            "public abstract class Generated%1$sHandler extends %2$s<%3$sDto, %4$sDto, %1$s> implements UIHandler<%1$s> {\n\n" +
            "    @Override\n" +
            "    protected %1$sTableModel getTableModel() {\n" +
            "        return (%1$sTableModel) super.getTableModel();\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public %1$sModel getModel() {\n" +
            "        return (%1$sModel) super.getModel();\n" +
            "    }\n\n" +
            "%5$s" +
            "}\n";
    public static final String NOT_STANDALONE_METHODS = "" +
            "    @Override\n" +
            "    protected void onBeforeInit(%1$s ui, ContentUIInitializer<%1$s> initializer) {\n" +
            "        EditableContentUI<%2$s> parentUI = initializer.getParentEditUI();\n" +
            "        %1$sModel model = parentUI.getContextValue(%1$sModel.class);\n" +
            "        initializer.registerDependencies(model, new %1$sTableModel(parentUI, ui, model));\n" +
            "    }\n\n";
    public static final String REBUILD_PROPORTION_SUM_METHODS = "" +
            "    @Override\n" +
            "    protected void onTableModelChanged() {\n" +
            "        super.onTableModelChanged();\n" +
            "        // when model change in table, let's recompute the proportion sum\n" +
            "        getModel().getStates().getBean().rebuild%1$sProportionSum();\n" +
            "    }\n\n";

    @Override
    protected String generateAbstractContent0(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        Class<?> superClass = ContentTableUIHandler.class;
        String extraMethods = "";
        List<String> imports = new LinkedList<>();
        if (notStandalone) {
            superClass = NotStandaloneContentTableUIHandler.class;
            imports.add(scopeBuilder.globalDtoType.getName());
            imports.add(EditableContentUI.class.getName());
            imports.add(ContentUIInitializer.class.getName());
            extraMethods += String.format(NOT_STANDALONE_METHODS, cleanClassName, scopeBuilder.globalDtoType.getSimpleName());
        }
        extraMethods += ContentUIHandlerHelper.generateSaveAction(imports, this);
        if (WithProportion.class.isAssignableFrom(dtoType)) {
            extraMethods += String.format(REBUILD_PROPORTION_SUM_METHODS, dtoType.getSimpleName().replace("Dto", ""));
        }
        if (UsingLayout.class.isAssignableFrom(globalDtoType)) {
            imports.add(parentDtoType.getName());
            imports.add(MoveLayoutAction.class.getName());
            imports.add(MoveLayoutRequest.class.getName());
            extraMethods += String.format(ContentUIHandlerHelper.INSTALL_MOVE_LAYOUT_ACTION, parentDtoType.getSimpleName(), globalDtoType.getSimpleName(), cleanClassName);
        }
        imports.add(ContentSimpleUIHandler.class.getName());
        imports.add(UIHandler.class.getName());
        return generate(ABSTRACT_MODEL, imports, cleanClassName, superClass, globalDtoType, dtoType, extraMethods);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        return uiHandlerHelper.generateHandlerConcreteContent();
    }
}
