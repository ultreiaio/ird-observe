package fr.ird.observe.client.datasource.editor.spi.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.move.layout.MoveLayoutRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.layout.MoveLayoutRequestBuilderStepConfigure;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveRequest;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUIModel;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIModelHelper;
import fr.ird.observe.dto.data.UsingLayout;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.services.ObserveServicesProvider;

import java.nio.file.Path;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentTableUIModel extends GenerateContentTableUISupport {
    public static final String UI_MODEL_GENERATED2 = "" +
            "public abstract class Generated%1$sModel extends %2$s<%3$sDto, %4$sDto> {\n\n" +
            "    public Generated%1$sModel(%1$sNavigationNode source) {\n" +
            "        super(source);\n" +
            "    }\n\n" +
            "%5$s" +
            "}\n";
    public static final String CREATE_MODEL = "" +
            "    public static %1$sModel create(%2$s parentUI) {\n" +
            "        %2$sNavigationNode parentSource = %2$s.getNavigationSource(parentUI);\n" +
            "        %1$sNavigationNode node = %1$sNavigationNode.create(parentSource::getParentReference);\n" +
            "        node.setParent(parentSource);\n" +
            "        return new %1$sModel(node);\n" +
            "    }\n\n";
    public static final String UI_MODEL_CONCRETE2 = "" +
            "public class %1$sModel extends Generated%1$sModel {\n\n" +
            "    public %1$sModel(%1$sNavigationNode source) {\n" +
            "        super(source);\n" +
            "    }\n\n" +
            "}\n";
    public static final String NEW_TABLE_BEAN = "" +
            "    @Override\n" +
            "    public %1$s newTableEditBean() {\n" +
            "        %1$s result = new %1$s();\n" +
            "        result.setTopiaCreateDate(new Date());\n" +
            "        getStates().initDefault(result);\n" +
            "        return result;\n" +
            "    }\n\n";

    public static final String TO_SAVE_REQUEST = "" +
            "    @Override\n" +
            "    public final SaveRequest<%1$s> toSaveRequest() {\n" +
            "        return SaveRequest.create(getStates().getSelectedId(), () -> getStates().getForm().getObject(), getStates()::getBeanToSave, getStates().savePredicate());\n" +
            "    }\n\n";

    @Override
    protected String generateAbstractContent0(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        String extraMethods = "";
        String loadFormMethod;
        if (notStandalone) {
            extraMethods += String.format(CREATE_MODEL, cleanClassName, globalDtoType.getSimpleName().replace("Dto", "") + "UI");
            imports.add(DtoReference.class.getName());
            loadFormMethod = String.format(ContentUIModelHelper.LOAD_EMPTY_FORM, globalDtoType.getSimpleName());
        } else {
            loadFormMethod = String.format(ContentUIModelHelper.LOAD_FORM, globalDtoType.getSimpleName(), "getContainerService");
        }
        extraMethods += ContentUIModelHelper.generateStates(cleanClassName);
        extraMethods += String.format(TO_SAVE_REQUEST, globalDtoType.getSimpleName());
        extraMethods += uiModelHelper.generateGetSource(imports, globalDtoType);
        if (UsingLayout.class.isAssignableFrom(globalDtoType)) {
            imports.add(MoveLayoutRequestBuilder.class.getName());
            imports.add(MoveLayoutRequestBuilderStepConfigure.class.getName());
            extraMethods += String.format(ContentUIModelHelper.TO_MOVE_LAYOUT_REQUEST, globalDtoType.getSimpleName());
        }
        imports.add(SaveRequest.class.getName());
        imports.add(ObserveServicesProvider.class.getName());
        imports.add(Form.class.getName());

        extraMethods += loadFormMethod;

        imports.add(Date.class.getName());
        extraMethods += String.format(NEW_TABLE_BEAN, dtoType.getSimpleName());
        return generate(UI_MODEL_GENERATED2, imports, cleanClassName, ContentTableUIModel.class, globalDtoType, dtoType, extraMethods);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        return generate(UI_MODEL_CONCRETE2, imports, cleanClassName, globalDtoType);
    }
}
