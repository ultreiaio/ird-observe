package fr.ird.observe.client.datasource.editor.spi.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.move.layout.MoveLayoutTreeAdapter;
import fr.ird.observe.dto.data.UsingLayout;
import fr.ird.observe.services.service.data.MoveLayoutRequest;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

/**
 * Created on 02/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class GenerateContentTableUIMoveTreeAdapter extends GenerateContentTableUISupport {

    private static final String CONTENT = "" +
            "public class %1$sMoveTreeAdapter extends MoveLayoutTreeAdapter<%2$s, %3$s> {\n\n" +
            "    public static Function<MoveLayoutRequest, %1$sMoveTreeAdapter> create(%1$s ui) {\n" +
            "        return r -> new %1$sMoveTreeAdapter(ui.getModel().getSource());\n" +
            "    }\n\n" +
            "    public %1$sMoveTreeAdapter(%3$s incomingNode) {\n" +
            "        super(incomingNode::getParent);\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public final %3$s getNodeToSelect(%2$s parentNode) {\n" +
            "        return parentNode.get%3$s();\n" +
            "    }\n\n" +
            "}\n";

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        if (skip()) {
            return null;
        }
        List<String> imports = new LinkedList<>();
        imports.add(Set.class.getName());
        imports.add(MoveLayoutTreeAdapter.class.getName());
        imports.add(MoveLayoutRequest.class.getName());
        imports.add(Function.class.getName());
        imports.add(parentNodeName);
        String parentNodeName = this.parentNodeName.substring(this.parentNodeName.lastIndexOf(".") + 1);
        String nodeName = namePrefix + "NavigationNode";
        return generate(CONTENT, imports, namePrefix, parentNodeName, nodeName);
    }

    protected boolean skip() {
        return !UsingLayout.class.isAssignableFrom(scopeBuilder.globalDtoType);
    }
}
