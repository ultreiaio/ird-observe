package fr.ird.observe.client.datasource.editor.spi.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUINavigationHandler;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentTableUINavigationHandler extends GenerateContentTableUISupport {

    public static final String GENERATED_MODEL = "" +
            "public class Generated%1$sNavigationHandler extends ContentTableUINavigationHandler<%1$sNavigationNode> {\n\n" +
            "%2$s" +
            "}\n";

    public static final String GENERATED_STANDALONE_MODEL = "" +
            "@SuppressWarnings({\"unchecked\", \"rawtypes\"})\n" +
            "public class Generated%1$sNavigationHandler extends ContentTableUINavigationHandler {\n\n" +
            "%2$s" +
            "}\n";

    @Override
    protected String generateAbstractContent0(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        imports.add(ContentTableUINavigationHandler.class.getName());
        nodeCapabilityName = null;
        String extraMethods = uiNavigationHandlerHelper.generateGeneratedConstructor();
        extraMethods += uiNavigationHandlerHelper.generateGetters(imports);
        if (notStandalone) {
            return generate(GENERATED_STANDALONE_MODEL, imports, cleanClassName, extraMethods);
        }
        return generate(GENERATED_MODEL, imports, cleanClassName, extraMethods);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        return uiNavigationHandlerHelper.generateNavigationHandlerConcreteContent();
    }

}
