package fr.ird.observe.client.datasource.editor.spi.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUINavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUINavigationNode;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationContextHelper;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 06/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentTableUINavigationNode extends GenerateContentTableUISupport {

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        List<String> imports = new LinkedList<>();
        imports.add(ContentTableUINavigationInitializer.class.getName());
        imports.add(ContentTableUINavigationNode.class.getName());
        String context = uiNavigationNodeHelper.generateCreateNodeContentTable(imports);
        context += uiNavigationNodeHelper.generateCreateContextWithoutCapability();
        context += uiNavigationNodeHelper.generateParentNode();
        context += uiNavigationNodeHelper.generateCreateNodeMethods(imports);
        if (notStandalone) {
            context += ContentUINavigationContextHelper.generateGetParentReference(imports, parentReferenceType);
        }
        return uiNavigationNodeHelper.generateContent(imports, context);
    }

}
