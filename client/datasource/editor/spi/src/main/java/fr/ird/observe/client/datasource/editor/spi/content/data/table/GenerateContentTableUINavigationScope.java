package fr.ird.observe.client.datasource.editor.spi.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeBuilder;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeDescriptor;
import fr.ird.observe.dto.DtoToReference;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.spi.module.ObserveBusinessProject;

import java.nio.file.Path;
import java.util.Optional;

/**
 * Created on 30/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentTableUINavigationScope extends GenerateContentTableUISupport {

    public static class Builder extends NavigationScopeBuilder {
        private final GenerateContentTableUISupport generator;

        public Builder(GenerateContentTableUISupport generator) {
            super(ContentTableUINavigationNode.class, generator);
            this.generator = generator;
        }

        @Override
        public Builder createBuilder(String contentUiType, String nodeType) {
            String parentNode = generator.detectCapabilities.parentRelations.get(generator.packageName + "." + nodeType).replace("UINavigationNode", "Dto");
            String selectTypeName = parentNode.replace(generator.packageName, generator.dtoPackage);
            if (selectTypeName.startsWith(parentNode)) {
                // parent node is not in this package
                selectTypeName = null;
            }
            return newBuilder(contentUiType, nodeType)
                    .setModule()
                    .setNoAutoLoadNode() // we still don't have count of data in parent reference, so can't autoload it
                    .setGlobalDtoType(generator.globalDtoType)
                    .setDtoReferenceTypeFromGlobalDtoType()
                    .setDtoType(generator.dtoType)
                    .setNodeDataTypeFromDtoType()
                    .setMainDtoTypeFromGlobalDtoType()
                    .setSelectNodeFromMainDtoType(selectTypeName)
                    .setParentDtoTypeFromSelectNodeType()
                    .setChildrenPropertyName(generator.globalDtoTypeJavaBeanDefinition);
        }

        @Override
        public void prepareBuild(Builder builder) {

            if (generator.notStandalone) {
                builder.setNotStandalone();
                builder.isSelectNode = false;
                builder.isEditNode = false;
                builder.selectNode = null;
                builder.editNode = null;
                generator.parentNodeName = null;
            } else {
                if (!OpenableDto.class.isAssignableFrom(builder.parentDtoType) && !RootOpenableDto.class.isAssignableFrom(builder.parentDtoType)) {
                    builder.setSubNode();
                }
                if (builder.showDataPropertyName == null) {
                    CapabilityDescriptor capabilityDescriptor = generator.getChildrenCapabilityDescriptor(builder.nodeType);
                    if (capabilityDescriptor != null) {
                        Optional<String> optionalPredicate = capabilityDescriptor.getOptionalPredicate();
                        optionalPredicate.ifPresent(builder::computePredicateDataPropertyName);
                    }
                }
            }
            builder.registerType(NavigationScope.TYPE_MODEL_PARENT, builder.mainDtoType);
            if (DtoToReference.class.isAssignableFrom(builder.mainDtoType)) {
                Class<DtoReference> parentReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(builder.mainDtoType);
                builder.registerType(NavigationScope.TYPE_MODEL_PARENT_REFERENCE, parentReferenceType);
            }
            builder.registerType(NavigationScope.TYPE_MODEL_MAIN, builder.globalDtoType);
            if (builder.dtoReferenceType != null) {
                builder.registerType(NavigationScope.TYPE_MODEL_MAIN_REFERENCE, builder.dtoReferenceType);
            }
            builder.registerType(NavigationScope.TYPE_MODEL_CHILD, builder.dtoType);
            Class<DtoReference> childReferenceType = ObserveBusinessProject.get().getMapping().getReferenceType(builder.dtoType);
            builder.registerType(NavigationScope.TYPE_MODEL_CHILD_REFERENCE, childReferenceType);
        }
    }

    @Override
    protected Path getConcreteFilePath(Path uiPath, String namePrefix) {
        return getNavigationScope(uiPath, namePrefix);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        addI18n();
        NavigationScopeDescriptor descriptor = scopeBuilder.build(i18nMapping, iconPath);
        return serializeDescriptor(descriptor);
    }

    protected void addI18n() {
        addI118nProperty(dtoType, "action.save");
        addI118nProperty(dtoType, "action.create");
        addI118nProperty(dtoType, "action.save.tip");
        if (notStandalone) {
            return;
        }
        addI118nProperty(dtoType, "type");
        addI118nProperty(dtoType, "title");
    }
}
