package fr.ird.observe.client.datasource.editor.spi.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUI;
import fr.ird.observe.client.datasource.editor.spi.content.ContentUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.reference.DtoReference;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.lang.Objects2;

import java.nio.file.Path;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class GenerateContentTableUISupport extends GenerateContentUISupport {

    String globalDtoPackage;
    String globalDtoNamePrefix;
    String globalDtoFullyQualifiedName;
    String dtoPackage;
    String dtoNamePrefix;
    Class<? extends ContainerDto<?>> globalDtoType;
    Class<? extends BusinessDto> dtoType;
    Class<? extends BusinessDto> parentDtoType;
    Class<? extends DtoReference> parentReferenceType;
    JavaBeanDefinition globalDtoTypeJavaBeanDefinition;

    @Override
    public Class<? extends ContentUI> getUiType() {
        return ContentTableUI.class;
    }

    @Override
    protected Class<? extends ContentUIDescriptor> getDescriptorType() {
        return ContentTableUIDescriptor.class;
    }

    @SuppressWarnings("unchecked")
    protected void init(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {
        super.init(sourceDirectory, targetDirectory, path, packageName, namePrefix);
        globalDtoPackage = businessSubModule.getDataPackageName();
        globalDtoNamePrefix = namePrefix.replace("UI", "");
        globalDtoFullyQualifiedName = globalDtoPackage + "." + globalDtoNamePrefix + "Dto";

        try {
            globalDtoType = (Class<? extends ContainerDto<?>>) Thread.currentThread().getContextClassLoader().loadClass(globalDtoFullyQualifiedName);
            dtoType = businessProject.getMapping().getDtoToFormDtoMapping().get(globalDtoType);
            if (dtoType == null) {
                // can happen if table has no referential
                dtoType = globalDtoType;
            }
            if (dtoType.equals(globalDtoType)) {
                if (globalDtoNamePrefix.endsWith("Part")) {
                    globalDtoNamePrefix = globalDtoNamePrefix.substring(0, globalDtoNamePrefix.length() - 4);
                    globalDtoFullyQualifiedName = globalDtoPackage + "." + globalDtoNamePrefix + "Dto";
                    globalDtoType = Objects2.forName(globalDtoFullyQualifiedName);
                    dtoType = Objects2.forName(globalDtoFullyQualifiedName.replace("Dto", "PartDto"));
                } else if (globalDtoNamePrefix.endsWith("SampleSpecies")) {
                    globalDtoNamePrefix = "SampleSampleSpecies";
                    globalDtoFullyQualifiedName = globalDtoPackage + "." + globalDtoNamePrefix + "Dto";
                    globalDtoType = Objects2.forName(globalDtoFullyQualifiedName);
                    dtoType = Objects2.forName(globalDtoFullyQualifiedName.replace("SampleSampleSpeciesDto", "SampleSpeciesDto"));
                } else if (globalDtoNamePrefix.endsWith("WellActivity")) {
                    globalDtoNamePrefix = "Well";
                    globalDtoFullyQualifiedName = globalDtoPackage + "." + globalDtoNamePrefix + "Dto";
                    globalDtoType = Objects2.forName(globalDtoFullyQualifiedName);
//                    dtoType = Objects2.forName(globalDtoFullyQualifiedName.replace("WellActivity", "Well"));
                } else {
                    throw new IllegalStateException("Can't get dtoType for: " + globalDtoType);
                }
            }
        } catch (ClassNotFoundException e) {
            if (globalDtoNamePrefix.startsWith("Set") && globalDtoNamePrefix.endsWith("Composition")) {
                globalDtoNamePrefix = "SetGlobalComposition";
                globalDtoFullyQualifiedName = globalDtoPackage + "." + globalDtoNamePrefix + "Dto";
                globalDtoType = Objects2.forName(globalDtoFullyQualifiedName);
                dtoType = Objects2.forName(globalDtoPackage + "." + namePrefix.substring(3).replace("UI", "") + "Dto");
            } else {
                throw new IllegalStateException(String.format("Can't find dto type: %s", globalDtoFullyQualifiedName), e);
            }
        }
        globalDtoTypeJavaBeanDefinition = JavaBeanDefinitionStore.getDefinition(globalDtoType).orElseThrow(IllegalStateException::new);
        dtoPackage = dtoType.getPackage().getName();
        dtoNamePrefix = dtoType.getSimpleName();
        scopeBuilder = new GenerateContentTableUINavigationScope.Builder(this).createBuilder(cleanClassName, cleanClassName + "NavigationNode");
        parentDtoType = scopeBuilder.parentDtoType;
        parentReferenceType = scopeBuilder.parentDtoReferenceType;
        if (notStandalone) {
            this.iconPath = generateIconPath(dtoNamePrefix.replace("Dto", ""));
        }
    }

}
