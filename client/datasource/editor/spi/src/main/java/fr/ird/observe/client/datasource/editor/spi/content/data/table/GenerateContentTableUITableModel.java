package fr.ird.observe.client.datasource.editor.spi.content.data.table;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.EditableContentUI;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableMeta;
import fr.ird.observe.client.datasource.editor.api.content.data.table.ContentTableUITableModel;
import fr.ird.observe.client.util.UIHelper;
import fr.ird.observe.client.util.table.JXTableUtil;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.WithIndex;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.toolkit.templates.ToolkitTagValues;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.bean.definition.JavaBeanPropertyDefinition;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducer;
import io.ultreia.java4all.lang.Strings;
import org.jdesktop.swingx.JXTable;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.topia.templates.TopiaHibernateTagValues;

import javax.swing.JTable;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentTableUITableModel extends GenerateContentTableUISupport {

    public static final String STANDALONE_ABSTRACT_MODEL = "" +
            "public abstract class Generated%1$sTableModel extends ContentTableUITableModel<%2$sDto, %3$sDto, %1$s> {\n\n" +
            "    private static final long serialVersionUID = 1L;\n\n" +
            "    protected final EditableContentUI<%2$sDto> parentUI;\n\n" +
            "    public Generated%1$sTableModel(EditableContentUI<%2$sDto> parentUI, %1$s ui, %1$sModel model) {\n" +
            "        super(ui, model);\n" +
            "        this.parentUI = parentUI;\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    protected final SwingValidator<%2$sDto> getParentValidator() {\n" +
            "        return parentUI.getValidator();\n" +
            "    }\n\n" +
            "%4$s" +
            "}\n";
    public static final String ABSTRACT_MODEL = "" +
            "public abstract class Generated%1$sTableModel extends ContentTableUITableModel<%2$sDto, %3$sDto, %1$s> {\n\n" +
            "    private static final long serialVersionUID = 1L;\n\n" +
            "    public Generated%1$sTableModel(%1$s ui) {\n" +
            "        super(ui, ui.getModel());\n" +
            "    }\n\n" +
            "%4$s" +
            "}";
    public static final String STANDALONE_CONCRETE_MODEL = "" +
            "public class %1$sTableModel extends Generated%1$sTableModel {\n\n" +
            "    private static final long serialVersionUID = 1L;\n\n" +
            "    public %1$sTableModel(EditableContentUI<%2$sDto> parentUi, %1$s ui, %1$sModel model) {\n" +
            "        super(parentUi, ui, model);\n" +
            "    }\n\n" +
            "}";
    public static final String CONCRETE_MODEL = "" +
            "public class %1$sTableModel extends Generated%1$sTableModel {\n\n" +
            "    private static final long serialVersionUID = 1L;\n\n" +
            "    public %1$sTableModel(%1$s ui) {\n" +
            "        super(ui);\n" +
            "    }\n\n" +
            "}";

    public static final String META = "" +
            "    @Override\n" +
            "    protected final List<ContentTableMeta<%1$s>> createMetas() {\n" +
            "        return %2$s;\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    protected final %3$sUIModel getModel() {\n" +
            "        return (%3$sUIModel) super.getModel();\n" +
            "    }\n\n";
    public static final String I18N = "" +
            "    @Override\n" +
            "    public final void initTableUI(JXTable table) {\n\n" +
            "        JXTableUtil.setI18nTableHeaderRenderer(\n" +
            "                table,\n" +
            "                %1$s.class,\n" +
            "                metas.stream().map(ContentTableMeta::getName).toArray(length -> new String[length]));\n\n" +
            "%2$s\n" +
            "        initTableUISize(table);\n" +
            "    }\n\n";
    public static final String INIT_TABLE_SIZE = "" +
            "    @Override\n" +
            "    public void initTableUISize(JTable table) {\n" +
            "        UIHelper.fixTableColumnWidth(table, 0, 55);\n"+
            "    }\n\n";

    private BeanPropertyI18nKeyProducer labelsBuilder;

    @Override
    public void run() {
        labelsBuilder = I18nDecoratorHelper.get().getDefaultLabelsBuilder();
        super.run();
    }

    @Override
    protected String generateAbstractContent0(Path sourceDirectory, Path targetDirectory, Path path, String packageName, String namePrefix) {

        Set<JavaBeanPropertyDefinition<?, ?>> candidates = globalDtoTypeJavaBeanDefinition.readAndWriteProperties().filter(p -> Collection.class.isAssignableFrom(p.type())).collect(Collectors.toSet());
        if (candidates.isEmpty()) {
            throw new IllegalStateException(String.format("Can't find candidate property in: %s", globalDtoType));
        }

        JavaBeanDefinition javaBeanDefinition2 = JavaBeanDefinitionStore.getDefinition(dtoType).orElseThrow(IllegalStateException::new);

        String tagValue = dtoTagValues.getClassTagValue(ToolkitTagValues.Store.containerChildDataDtoProperties.getName(), dtoType);
        String[] propertyNames = Objects.requireNonNull(tagValue, String.format("Can't find dto tagValue containerChildDataDtoProperties for type: %s", dtoType.getName())).split(("\\s*,\\s*"));

        StringBuilder metasBuilder = new StringBuilder("List.of(");
        StringBuilder rendererBuilder = new StringBuilder("        JXTableUtil.initRenderers(table");
        boolean withIndex = WithIndex.class.isAssignableFrom(dtoType);
        if (withIndex) {
            // add a first meta with the index
            String propertyName = WithIndex.PROPERTY_INDEX;
            String propertySuffix = Strings.convertToConstantName(propertyName);
            metasBuilder.append(String.format("\n                ContentTableUITableModel.newTableMeta(%1$s.class, %1$s.PROPERTY_%2$s)", dtoNamePrefix, propertySuffix));
            rendererBuilder.append("\n                , JXTableUtil.newEmptyNumberTableCellRenderer(component -> component.setEnabled(false))");
            String textKey = labelsBuilder.getI18nPropertyKey(dtoType, propertyName + ".short");
            String tipKey = labelsBuilder.getI18nPropertyKey(dtoType, propertyName);
            getterFile.addKey(textKey);
            getterFile.addKey(tipKey);
            metasBuilder.append(",");
        }
        int length = propertyNames.length;
        for (int i = 0; i < length; i++) {
            String propertyName = propertyNames[i];
            String propertySuffix = Strings.convertToConstantName(propertyName);
            metasBuilder.append(String.format("\n                ContentTableUITableModel.newTableMeta(%1$s.class, %1$s.PROPERTY_%2$s)", dtoNamePrefix, propertySuffix));

            Class<?> propertyType = javaBeanDefinition2.property(propertyName).type();
            if (Number.class.isAssignableFrom(propertyType)) {
                rendererBuilder.append("\n                , JXTableUtil.newEmptyNumberTableCellRenderer()");
            } else if (String.class.isAssignableFrom(propertyType)) {
                rendererBuilder.append("\n                , JXTableUtil.newStringTableCellRenderer(10)");
            } else if (ReferentialDtoReference.class.isAssignableFrom(propertyType)) {
                String classifier = String.format("(String) getContext().get%s().getClientProperty(fr.ird.observe.client.util.init.UIInitHelper.DECORATOR_CLASSIFIER)", Strings.capitalize(propertyName));
                rendererBuilder.append(String.format("\n                , JXTableUtil.newDecoratedRenderer(%s.class, %s)", propertyType.getName(), classifier));
            } else if (DataDtoReference.class.isAssignableFrom(propertyType)) {
                String classifier = String.format("(String) getContext().get%s().getClientProperty(fr.ird.observe.client.util.init.UIInitHelper.DECORATOR_CLASSIFIER)", Strings.capitalize(propertyName));
                rendererBuilder.append(String.format("\n                , JXTableUtil.newDecoratedRenderer(%s.class, %s)", propertyType.getName(), classifier));
            } else if (DataDto.class.isAssignableFrom(propertyType)) {
                String classifier = String.format("(String) getContext().get%s().getClientProperty(fr.ird.observe.client.util.init.UIInitHelper.DECORATOR_CLASSIFIER)", Strings.capitalize(propertyName));
                rendererBuilder.append(String.format("\n                , JXTableUtil.newDecoratedRenderer(%s.class, %s)", propertyType.getName(), classifier));
            } else if (Collection.class.isAssignableFrom(propertyType)) {
                rendererBuilder.append("\n                , JXTableUtil.newCollectionTableCellRenderer()");
            } else if (boolean.class.isAssignableFrom(propertyType)) {
                rendererBuilder.append("\n                ,table.getDefaultRenderer(Boolean.class)");
            } else if (Boolean.class.isAssignableFrom(propertyType)) {
                rendererBuilder.append("\n                , JXTableUtil.newBooleanTableCellRenderer2()");
            } else if (int.class.isAssignableFrom(propertyType)) {
                rendererBuilder.append(String.format("\n                , JXTableUtil.newEnumTableCellRenderer(fr.ird.observe.dto.data.Catch%s.class)", Strings.capitalize(propertyName)));
            } else if (Date.class.isAssignableFrom(propertyType)) {
                String dateFlavor = persistenceTagValues.getAttributeTagValue(TopiaHibernateTagValues.Store.hibernateAttributeType.getName() + ".java.util.Date", dtoType, propertyName);
                if (dateFlavor==null) {
                    throw new IllegalStateException("Can't find hibernateAttributeType.java.util.Date tag value on "+dtoType.getName()+"#"+propertyName);
                }
                switch (dateFlavor) {
                    case "time":
                        rendererBuilder.append("\n                , JXTableUtil.newTimeTableCellRenderer()");
                        break;
                    case "date":
                        rendererBuilder.append("\n                , JXTableUtil.newDateTableCellRenderer()");
                break;
                    default:
                        // on time stamp
                        rendererBuilder.append("\n                , JXTableUtil.newTimestampTableCellRenderer()");
                }
            }
            else {
                rendererBuilder.append("\n                , table.getDefaultRenderer(Boolean.class)");
            }
            String textKey = labelsBuilder.getI18nPropertyKey(dtoType, propertyName + ".short");
            String tipKey = labelsBuilder.getI18nPropertyKey(dtoType, propertyName);
            getterFile.addKey(textKey);
            getterFile.addKey(tipKey);
            if (i < length - 1) {
                metasBuilder.append(",");
            }
        }
        metasBuilder.append(")");
        rendererBuilder.append(");");
        String metas = String.format(META, dtoNamePrefix, metasBuilder, namePrefix.replace("UI", ""));
//        if (withIndex) {
//            rendererBuilder.append("\n        setTableSortable(table);");
//        }
        String i18n = String.format(I18N, dtoType.getName(), rendererBuilder);
        String initTableSize = withIndex?INIT_TABLE_SIZE:"";

        List<String> imports = new LinkedList<>();
        imports.add(ContentTableMeta.class.getName());
        imports.add(ContentTableUITableModel.class.getName());
        imports.add(JXTableUtil.class.getName());
        imports.add(I18n.class.getName());
        imports.add(List.class.getName());
        imports.add(JXTable.class.getName());
        if (withIndex) {
            imports.add(JTable.class.getName());
            imports.add(UIHelper.class.getName());

        }
        if (notStandalone) {
            imports.add(EditableContentUI.class.getName());
            imports.add(SwingValidator.class.getName());
            return generate(STANDALONE_ABSTRACT_MODEL, imports, cleanClassName, globalDtoType, dtoType, metas + i18n + initTableSize);
        }
        return generate(ABSTRACT_MODEL, imports, cleanClassName, globalDtoType, dtoType, metas + i18n + initTableSize);
    }

    @Override
    protected String generateConcreteContent(Path path, String packageName, String namePrefix) {
        if (notStandalone) {
            List<String> imports = new LinkedList<>();
            imports.add(EditableContentUI.class.getName());
            return generate(STANDALONE_CONCRETE_MODEL, imports, cleanClassName, globalDtoType);
        }
        return generate(CONCRETE_MODEL, cleanClassName);
    }
}
