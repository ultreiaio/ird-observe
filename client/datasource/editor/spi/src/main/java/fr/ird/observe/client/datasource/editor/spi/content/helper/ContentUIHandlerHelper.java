package fr.ird.observe.client.datasource.editor.spi.content.helper;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.create.CreateNewRootOpenableUI;
import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeleteAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.layout.MoveLayoutAction;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveAction;
import fr.ird.observe.client.datasource.editor.api.content.data.TripActionHelper;
import fr.ird.observe.client.datasource.editor.api.content.data.TripUIHelper;
import fr.ird.observe.client.datasource.editor.api.content.data.edit.actions.SaveContentEditUIAdapter;
import fr.ird.observe.client.datasource.editor.api.content.data.open.actions.SaveContentOpenableUIAdapter;
import fr.ird.observe.client.datasource.editor.api.content.data.ropen.actions.SaveContentRootOpenableUIAdapter;
import fr.ird.observe.client.datasource.editor.api.content.data.simple.actions.SaveContentSimpleUIAdapter;
import fr.ird.observe.client.datasource.editor.api.content.data.table.actions.SaveContentTableUIAdapter;
import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.ContentNodeType;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateJavaFileSupport;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.data.SkipSimpleMove;
import fr.ird.observe.dto.data.UsingLayout;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.data.DeleteLayoutRequest;
import fr.ird.observe.services.service.data.MoveLayoutRequest;
import fr.ird.observe.services.service.data.TripAwareService;
import io.ultreia.java4all.lang.Strings;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.util.LinkedList;
import java.util.List;

/**
 * Created on 08/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentUIHandlerHelper extends ContentUIHelperSupport {

    public static final String UI_HANDLER_CONCRETE = "" +
            "public class %1$sHandler extends Generated%1$sHandler {\n\n" +
            "}\n";
    public static final String UI_HANDLER_GENERATED = "" +
            "public abstract class Generated%1$sHandler extends %2$s<%3$sDto, %1$s> implements UIHandler<%1$s>{\n\n" +
            "%4$s" +
            "}\n";
    public static final String UI_HANDLER_GENERATED2 = "" +
            "public abstract class Generated%1$sHandler extends %2$s<%3$sDto, %4$s, %1$s> implements UIHandler<%1$s>{\n\n" +
            "%5$s" +
            "}\n";
    public static final String INSTALL_MOVE_SIMPLE_ACTION = "" +
            "    @Override\n" +
            "    protected void installMoveAction() {\n" +
            "        MoveAction\n" +
            "                .create(ui)\n" +
            "                .on(() -> ui.getModel().toMoveRequest().setParentCandidates((groupBy, parentId) -> %1$s))\n" +
            "                .call((r ,newParentId, ids) -> getOpenableService().move(%2$s.class, r))\n" +
            "                .then(%3$sMoveTreeAdapter::create)\n" +
            "                .install(ui::getMove);\n" +
            "    }\n\n";
    public static final String INSTALL_MOVE_LAYOUT_ACTION = "" +
            "    @Override\n" +
            "    protected final void installMoveAction() {\n" +
            "        MoveLayoutAction\n" +
            "                .create(ui)\n" +
            "                .on(() -> ui.getModel().toMoveRequest().setParentCandidates((groupBy, parentId) -> getRootOpenableService().getBrothers(groupBy, parentId)))\n" +
            "                .call(r -> getRootOpenableService().moveLayout(%1$s.class, r))\n" +
            "                .then(%3$sMoveTreeAdapter::create)\n" +
            "                .install(ui::getMoveAll);\n" +
            "    }\n\n";
    public static final String INSTALL_DELETE_SIMPLE_ACTION = "" +
            "    @Override\n" +
            "    protected void installDeleteAction() {\n" +
            "        DeleteAction\n" +
            "                .create(ui)\n" +
            "                .on(ui.getModel()::toDeleteRequest)\n" +
            "                .call(r -> id -> %1$s().delete(%2$s.class, id))\n" +
            "                .then(%3$sDeleteTreeAdapter::create)\n" +
            "                .install(ui::getDelete);\n" +
            "    }\n\n";
    public static final String INSTALL_DELETE_LAYOUT_ACTION = "" +
            "    @Override\n" +
            "    protected final void installDeleteAction() {\n" +
            "        DeleteAction\n" +
            "                .create(ui)\n" +
            "                .on(ui.getModel()::toDeleteRequest)\n" +
            "                .call(r -> id -> getRootOpenableService().deleteLayout(%1$s.class, new DeleteLayoutRequest(%2$s.class, id)))\n" +
            "                .then(%3$sDeleteTreeAdapter::create)\n" +
            "                .install(ui::getDelete);\n" +
            "    }\n\n";
    public static final String INSTALL_DELETE_OPENABLE_ACTION = "" +
            "    @Override\n" +
            "    protected void installDeleteAction() {\n" +
            "        DeleteAction\n" +
            "                .create(ui)\n" +
            "                .on(() -> ui.getModel().toDeleteRequest().setOpenableServiceSupplier(this::getOpenableService).setDeleteExtraMessageSupplier(this::computeDeleteDependenciesExtraMessage))\n" +
            "                .call(r -> id -> %1$s().delete(%2$s.class, id))\n" +
            "                .then(%3$sDeleteTreeAdapter::create)\n" +
            "                .install(ui::getDelete);\n" +
            "    }\n\n";
    public static final String INSTALL_DELETE_ROOT_OPENABLE_ACTION = "" +
            "    @Override\n" +
            "    protected void installDeleteAction() {\n" +
            "        DeleteAction\n" +
            "                .create(ui)\n" +
            "                .on(() -> ui.getModel().toDeleteRequest().setOpenableServiceSupplier(this::getOpenableService))\n" +
            "                .call(r -> id -> %1$s().delete(%2$s.class, id))\n" +
            "                .then(%3$sDeleteTreeAdapter::create)\n" +
            "                .install(ui::getDelete);\n" +
            "    }\n\n";
    public static final String INSTALL_MOVE_ACTION = "" +
            "    @Override\n" +
            "    protected void installMoveAction() {\n" +
            "        MoveAction\n" +
            "                .create(ui)\n" +
            "                .on(() -> ui.getModel().toMoveRequest().setParentCandidates((groupBy, parentId) -> %1$s))\n" +
            "                .call((r ,newParentId, ids) -> getOpenableService().move(%2$s.class, r))\n" +
            "                .then(%3$sMoveTreeAdapter::create)\n" +
            "                .install(ui::getMove);\n" +
            "    }\n\n";
    public static final String INSTALL_DELETE_LIST_ACTION = "" +
            "    @Override\n" +
            "    protected void installDeleteAction() {\n" +
            "        DeleteAction\n" +
            "                .create(ui)\n" +
            "                .on(() -> ui.getModel().toDeleteRequest().setOpenableServiceSupplier(this::getOpenableService).setDeleteExtraMessageSupplier(this::computeDeleteDependenciesExtraMessage))\n" +
            "                .call(r -> id -> getOpenableService().delete(%1$s.class, id))\n" +
            "                .then(%2$sDeleteTreeAdapter::create)\n" +
            "                .install(ui::getDelete);\n" +
            "    }\n\n";
    public static final String INSTALL_DELETE_ROOT_LIST_ACTION = "" +
            "    @Override\n" +
            "    protected void installDeleteAction() {\n" +
            "        DeleteAction\n" +
            "                .create(ui)\n" +
            "                .on(() -> ui.getModel().toDeleteRequest().setOpenableServiceSupplier(this::getOpenableService))\n" +
            "                .call(r -> id -> getRootOpenableService().delete(%1$s.class, id))\n" +
            "                .then(%2$sDeleteTreeAdapter::create)\n" +
            "                .install(ui::getDelete);\n" +
            "    }\n\n";
    public static final String INSTALL_CREATE_NEW_ACTION_METHOD = "" +
            "    @Override\n" +
            "    protected final void installCreateNewAction() {\n" +
            "%2$s" +
            "    }\n\n";
    public static final String INSTALL_PANELS_ACTION_METHOD = "" +
            "    @Override\n" +
            "    protected final void installPanelsAction() {\n" +
            "%2$s" +
            "    }\n\n";
    public static final String INSTALL_SAVE_ACTION_METHOD = "" +
            "    @Override\n" +
            "    protected void installSaveAction() {\n" +
            "        SaveAction\n" +
            "                .create(ui, %1$s.class)\n" +
            "                .on(ui.getModel()::toSaveRequest)\n" +
            "                .call((r, d) -> %2$s)\n" +
            "                .then(new %3$s<>())\n" +
            "                .install(ui.getSave());\n" +
            "    }\n\n";
    public static final String INSTALL_EMPTY_SAVE_ACTION_METHOD = "" +
            "    @Override\n" +
            "    protected void installSaveAction() {\n" +
            "    }\n\n";
    public static final String ON_END_OPEN_UI = "" +
            "    @Override\n" +
            "    public final void onEndOpenUI() {\n" +
            "        TripActionHelper.disableCreateActionIfProgramDisabled(ui, ui.getModel().getSource().getParentReference());\n" +
            "        super.onEndOpenUI();\n" +
            "    }\n\n";
    public static final String INSTALL_MOVE_ROOT_ACTION = "" +
            "    @Override\n" +
            "    protected final void installMoveAction() {\n" +
            "        MoveLayoutAction\n" +
            "                .create(ui)\n" +
            "                .on(() -> ui.getModel().toMoveRequest().setParentCandidates((groupBy, parentId) -> getRootOpenableService().getBrothers(groupBy, parentId)))\n" +
            "                .call(r -> getRootOpenableService().moveLayout(%1$s.class, r))\n" +
            "                .then(%2$sMoveTreeAdapter::create)\n" +
            "                .install(ui::getMoveAll);\n" +
            "    }\n\n";

    public static String generateCreateNewAction(List<String> imports, GenerateJavaFileSupport generator) {
        StringBuilder createChildrenMethodBuilder = new StringBuilder();
        IdNode<?> editNode = generator.scopeBuilder.editNode;
        boolean onLayout = generator.getDescriptor().getContentNodeType() == ContentNodeType.LAYOUT;
        if (generator.getDescriptor().getContentNodeType() == ContentNodeType.OPEN) {
            Class<? extends IdDto> dtoType = generator.scopeBuilder.dtoType;
            String mainMethod = CapabilityDescriptor.generateAddMainNewOpenAction(ContentNodeType.OPEN, dtoType.getSimpleName().replace("Dto", "UINavigationNode"));
            createChildrenMethodBuilder.append(mainMethod);
        } else if (generator.getDescriptor().getContentNodeType() == ContentNodeType.ROPEN) {
            Class<? extends IdDto> dtoType = generator.scopeBuilder.dtoType;
            String mainMethod = CapabilityDescriptor.generateAddMainNewOpenAction(ContentNodeType.ROPEN, dtoType.getSimpleName().replace("Dto", "UINavigationNode"));
            createChildrenMethodBuilder.append(mainMethod);
        }
        if (generator.capabilitiesDescriptor != null) {
            for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
                Class<? extends IdDto> optionalDtoType = capabilityDescriptor.getOptionalDtoType();
                if (!onLayout && editNode != null && editNode.getType().equals(optionalDtoType)) {
                    continue;
                }
                String method = capabilityDescriptor.generateAddNewAction(imports);
                createChildrenMethodBuilder.append(method);
            }
        }
        return String.format(INSTALL_CREATE_NEW_ACTION_METHOD, generator.cleanClassName, createChildrenMethodBuilder);
    }

    public static String generateInitPanelsAction(GenerateJavaFileSupport generator) {
        StringBuilder createChildrenMethodBuilder = new StringBuilder();
        int size = generator.capabilitiesDescriptor.size();
        int index = 1;
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            String method = capabilityDescriptor.generatePanelAction(index);
            createChildrenMethodBuilder.append(method);
            index += size;
        }
        return String.format(INSTALL_PANELS_ACTION_METHOD, generator.cleanClassName, createChildrenMethodBuilder);
    }

    public static String generateSaveAction(List<String> imports, GenerateJavaFileSupport generator) {
        String serviceCall;
        Class<? extends IdDto> dtoType;
        Class<?> adapterType;
        switch (generator.getDescriptor().getContentNodeType()) {
            case SIMPLE:
                dtoType = generator.scopeBuilder.dtoType;
                adapterType = SaveContentSimpleUIAdapter.class;
                serviceCall = "getSimpleService().save(d)";
                break;
            case TABLE:
                if (generator.notStandalone) {
                    return INSTALL_EMPTY_SAVE_ACTION_METHOD;
                }
                dtoType = generator.scopeBuilder.globalDtoType;
                adapterType = SaveContentTableUIAdapter.class;
                serviceCall = "getContainerService().save(d)";
                break;
            case EDIT:
                dtoType = generator.scopeBuilder.dtoType;
                adapterType = SaveContentEditUIAdapter.class;
                serviceCall = "getEditableService().save(r.getParentId(), d)";
                break;
            case OPEN:
                dtoType = generator.scopeBuilder.dtoType;
                adapterType = SaveContentOpenableUIAdapter.class;
                serviceCall = "getOpenableService().save(r.getParentId(), d)";
                break;
            case ROPEN:
                dtoType = generator.scopeBuilder.dtoType;
                adapterType = SaveContentRootOpenableUIAdapter.class;
                serviceCall = "getRootOpenableService().save(d)";
                break;
            default:
                return "";
        }
        imports.add(dtoType.getName());
        imports.add(SaveAction.class.getName());
        imports.add(adapterType.getName());
        return String.format(INSTALL_SAVE_ACTION_METHOD, dtoType.getSimpleName(), serviceCall, adapterType.getSimpleName());
    }

    public static String generateListCreateNewAction(List<String> imports, GenerateJavaFileSupport generator) {
        StringBuilder createChildrenMethodBuilder = new StringBuilder();
        IdNode<?> editNode = generator.scopeBuilder.editNode;
        String mainMethod = CapabilityDescriptor.generateAddMainNewOpenAction(ContentNodeType.LIST, editNode.getType().getSimpleName().replace("Dto", "UINavigationNode"));
        createChildrenMethodBuilder.append(mainMethod);
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            if (editNode.getType().equals(capabilityDescriptor.getOptionalDtoType())) {
                continue;
            }
            String addNodeMethod = capabilityDescriptor.generateAddNewAction(imports);
            createChildrenMethodBuilder.append(addNodeMethod);
        }
        return String.format(INSTALL_CREATE_NEW_ACTION_METHOD, generator.cleanClassName, createChildrenMethodBuilder);
    }

    public static String generateRootListCreateNewAction(List<String> imports, GenerateJavaFileSupport generator) {
        StringBuilder createChildrenMethodBuilder = new StringBuilder();
        IdNode<?> editNode = generator.scopeBuilder.editNode;
        String mainMethod = CapabilityDescriptor.generateAddMainNewOpenAction(ContentNodeType.RLIST, editNode.getType().getSimpleName().replace("Dto", "UINavigationNode"));
        createChildrenMethodBuilder.append(mainMethod);
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            if (editNode.getType().equals(capabilityDescriptor.getOptionalDtoType())) {
                continue;
            }
            String addNodeMethod = capabilityDescriptor.generateAddNewAction(imports);
            createChildrenMethodBuilder.append(addNodeMethod);
        }
        return String.format(INSTALL_CREATE_NEW_ACTION_METHOD, generator.cleanClassName, createChildrenMethodBuilder);
    }

    public ContentUIHandlerHelper(GenerateContentUISupport generator) {
        super(generator);
    }

    public String generateHandlerGeneratedContent(Class<?> uiHandler, Class<?> dtoType, Class<?> extraType) {
        List<String> imports = new LinkedList<>();
        imports.add(UIHandler.class.getName());
        String extraMethods = generateGetModel();
        IdNode<?> editNode = generator.scopeBuilder.editNode;

        imports.add(DeleteAction.class.getName());

        boolean useRoot = editNode != null && editNode.getLevel() == 0;
        if (useRoot) {
            extraMethods += ON_END_OPEN_UI;
            imports.add(CreateNewRootOpenableUI.class.getName());
            imports.add(TripActionHelper.class.getName());
            extraMethods += generateRootListCreateNewAction(imports, generator);
            extraMethods += String.format(INSTALL_DELETE_ROOT_LIST_ACTION, dtoType.getSimpleName(), generator.cleanClassName.replace("List", ""));
        } else {
            Class<? extends BusinessDto> parentDtoType = generator.scopeBuilder.parentDtoType;
            imports.add(parentDtoType.getName());
            String moveMethodName = editNode != null && editNode.getLevel() == 1 ? "getRootOpenableService().getBrothers(groupBy, parentId)" :
                    "getOpenableService().getBrothers(" + parentDtoType.getSimpleName() + ".class, parentId)";
            imports.add(MoveAction.class.getName());
            imports.add(MoveRequestBuilder.class.getName());
            imports.add(CreateNewOpenableUI.class.getName());
            imports.add(TripActionHelper.class.getName());
            extraMethods += generateListCreateNewAction(imports, generator);
            if (!SkipSimpleMove.class.isAssignableFrom(dtoType)) {
                extraMethods += String.format(INSTALL_MOVE_ACTION, moveMethodName, dtoType.getSimpleName(), generator.cleanClassName.replace("List", ""));
            }
            extraMethods += String.format(INSTALL_DELETE_LIST_ACTION, dtoType.getSimpleName(), generator.cleanClassName.replace("List", ""));
        }
        return generate(ContentUIHandlerHelper.UI_HANDLER_GENERATED2, imports, generator.cleanClassName, uiHandler, dtoType, extraType, extraMethods);
    }

    public String generateHandlerGeneratedContent(Class<?> uiHandler, Class<?> dtoType) {
        List<String> imports = new LinkedList<>();
        imports.add(UIHandler.class.getName());
        String extraMethods = generateGetModel();
        IdNode<?> editNode = generator.scopeBuilder.editNode;
        boolean addSave = false;
        boolean addDelete = false;
        Class<? extends BusinessDto> parentDtoType = generator.scopeBuilder.parentDtoType;
        String cleanClassName = generator.cleanClassName;
        String dtoTypeSimpleName = dtoType.getSimpleName();
        switch (generator.getDescriptor().getContentNodeType()) {
            case EDIT:
                addDelete = true;
                extraMethods += String.format(INSTALL_DELETE_SIMPLE_ACTION, "getEditableService", dtoTypeSimpleName, cleanClassName);
                break;
            case OPEN:
                addDelete = true;
                extraMethods += String.format(INSTALL_DELETE_OPENABLE_ACTION, "getOpenableService", dtoTypeSimpleName, cleanClassName);
                if (!SkipSimpleMove.class.isAssignableFrom(dtoType)) {
                    String moveMethodName = editNode.getLevel() == 1 ? "getRootOpenableService().getBrothers(groupBy, parentId)" :
                            "getOpenableService().getBrothers(" + parentDtoType.getSimpleName() + ".class, parentId)";
                    imports.add(parentDtoType.getName());
                    imports.add(MoveAction.class.getName());
                    imports.add(MoveRequestBuilder.class.getName());
                    extraMethods += String.format(INSTALL_MOVE_SIMPLE_ACTION, moveMethodName, dtoTypeSimpleName, cleanClassName);
                }
                break;
            case ROPEN:
                addDelete = true;

                imports.add(TripUIHelper.class.getName());
                imports.add(Form.class.getName());
                imports.add(ObserveServicesProvider.class.getName());

                String moduleName = generator.scopeBuilder.editNode.getModule().getName();
                String subModuleName = generator.scopeBuilder.editNode.getSubModule().getName();
                String serviceName = cleanClassName.replace("UI", "") + "Service";
                imports.add(TripAwareService.class.getPackageName() + "." + moduleName + "." + subModuleName + "." + serviceName);
                String serviceMethod = "get" + Strings.capitalize(moduleName) + Strings.capitalize(subModuleName) + serviceName;
                extraMethods = extraMethods +
                        String.format("" +
                                              "    protected %2$sHelper<%1$s, %2$s> uiHelper;\n\n" +
                                              "    @Override\n" +
                                              "    public void onInit(%2$s ui) {\n" +
                                              "        super.onInit(ui);\n" +
                                              "        uiHelper = new %2$sHelper<>(ui, prefix) {\n" +
                                              "            @Override\n" +
                                              "            protected %4$s getService(ObserveServicesProvider servicesProvider) {\n" +
                                              "                return servicesProvider.%3$s();\n" +
                                              "            }\n" +
                                              "        };\n" +
                                              "        uiHelper.installMap(getModel().getStates().getBean());\n" +
                                              "    }\n\n" +
                                              "    @Override\n" +
                                              "    public void onOpenForm(Form<?> form) {\n" +
                                              "        uiHelper.onOpenModel(getModel().getStates().getBean());\n" +
                                              "        super.onOpenForm(form);\n" +
                                              "    }\n\n" +
                                              "    @Override\n" +
                                              "    public void onEndOpenUI() {\n" +
                                              "        uiHelper.onOpened();\n" +
                                              "    }\n\n",
                                      dtoTypeSimpleName,
                                      cleanClassName,
                                      serviceMethod,
                                      serviceName);
                extraMethods += String.format(INSTALL_DELETE_ROOT_OPENABLE_ACTION, "getRootOpenableService", dtoTypeSimpleName, cleanClassName);
                if (UsingLayout.class.isAssignableFrom(dtoType)) {
                    imports.add(MoveLayoutAction.class.getName());
                    imports.add(MoveLayoutRequest.class.getName());
                    extraMethods += String.format(INSTALL_MOVE_ROOT_ACTION, dtoTypeSimpleName, cleanClassName);
                }
                break;
            case LAYOUT:
                addDelete = true;
                imports.add(DeleteLayoutRequest.class.getName());
                imports.add(MoveLayoutAction.class.getName());
                imports.add(MoveLayoutRequest.class.getName());
                imports.add(parentDtoType.getName());
                extraMethods += String.format(INSTALL_DELETE_LAYOUT_ACTION, parentDtoType.getSimpleName(), dtoTypeSimpleName, cleanClassName);
                extraMethods += String.format(INSTALL_MOVE_LAYOUT_ACTION, parentDtoType.getSimpleName(), dtoTypeSimpleName, cleanClassName);
                extraMethods += generateInitPanelsAction(generator);
                break;
            case SIMPLE:
                addSave = true;
                break;
        }
        if (addDelete) {
            addSave = true;
            imports.add(DeleteAction.class.getName());
            extraMethods += generateCreateNewAction(imports, generator);
        }
        if (addSave) {
            extraMethods += generateSaveAction(imports, generator);
        }
        return generate(ContentUIHandlerHelper.UI_HANDLER_GENERATED, imports, cleanClassName, uiHandler, dtoType, extraMethods);
    }

    public String generateHandlerConcreteContent() {
        return generate(ContentUIHandlerHelper.UI_HANDLER_CONCRETE, generator.cleanClassName);
    }

    public String generateGetModel() {
        return String.format(ContentUINavigationCapabilityHelper.GET_MODEL, generator.cleanClassName);
    }

}
