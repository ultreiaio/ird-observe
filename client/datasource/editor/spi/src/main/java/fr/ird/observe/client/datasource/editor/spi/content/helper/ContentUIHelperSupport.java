package fr.ird.observe.client.datasource.editor.spi.content.helper;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateJavaFileSupport;

import java.util.LinkedList;
import java.util.List;

/**
 * Created on 08/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ContentUIHelperSupport {

    final GenerateContentUISupport generator;

    public ContentUIHelperSupport(GenerateContentUISupport generator) {
        this.generator = generator;
    }

    protected String generate(String template, Object... parameters) {
        return GenerateJavaFileSupport.generate0(generator.getClass(), template, generator.packageName, new LinkedList<>(), parameters);
    }

    protected String generate(String template, List<String> imports, Object... parameters) {
        return GenerateJavaFileSupport.generate0(generator.getClass(), template, generator.packageName, imports, parameters);
    }

}
