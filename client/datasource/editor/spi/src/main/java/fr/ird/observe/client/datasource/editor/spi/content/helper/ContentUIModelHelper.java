package fr.ird.observe.client.datasource.editor.spi.content.helper;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.delete.DeleteRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRequestBuilderStepConfigure;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.MoveRootRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.layout.MoveLayoutRequestBuilder;
import fr.ird.observe.client.datasource.editor.api.content.actions.move.layout.MoveLayoutRequestBuilderStepConfigure;
import fr.ird.observe.client.datasource.editor.api.content.actions.save.SaveRequest;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.UsingLayout;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.services.ObserveServicesProvider;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created on 08/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentUIModelHelper extends ContentUIHelperSupport {
    public static final String UI_MODEL_CONCRETE = "" +
            "public class %1$sModel extends Generated%1$sModel {\n\n" +
            "    public %1$sModel(%1$sNavigationNode source) {\n" +
            "        super(source);\n" +
            "    }\n\n" +
            "}\n";
    public static final String UI_MODEL_GENERATED = "" +
            "public abstract class Generated%1$sModel extends %2$s<%3$s> {\n\n" +
            "    public Generated%1$sModel(%1$sNavigationNode source) {\n" +
            "        super(source);\n" +
            "    }\n\n" +
            "%4$s" +
            "}\n";
    public static final String GET_SOURCE = "" +
            "    @Override\n" +
            "    public %1$sNavigationNode getSource() {\n" +
            "        return (%1$sNavigationNode) super.getSource();\n" +
            "    }\n\n";
    public static final String STATES = "" +
            "    @Override\n" +
            "    public %1$s getStates() {\n" +
            "        return (%1$s) super.getStates();\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    protected %1$s createStates() {\n" +
            "        return new %1$s(this);\n" +
            "    }\n\n";
    public static final String STATES2 = "" +
            "    @Override\n" +
            "    public %1$s getStates() {\n" +
            "        return (%1$s) super.getStates();\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    protected %1$s createStates() {\n" +
            "        return new %1$s();\n" +
            "    }\n\n";
    public static final String LOAD_FORM = "" +
            "    @Override\n" +
            "    public Form<%1$s> loadForm(ObserveServicesProvider servicesProvider, String selectedId) {\n" +
            "        return servicesProvider.%2$s().loadForm(%1$s.class, selectedId);\n" +
            "    }\n\n";
    public static final String TO_DELETE_REQUEST = "" +
            "    @Override\n" +
            "    public DeleteRequestBuilder.StepSetExtra toDeleteRequest() {\n" +
            "        return DeleteRequestBuilder\n" +
            "                .create(%1$s.class, getStates().getBeanLabel());\n" +
            "    }\n\n";
    public static final String TO_DELETE_REQUEST_WITH_EDIT = "" +
            "    @Override\n" +
            "    public DeleteRequestBuilder.StepSetExtra toDeleteRequest() {\n" +
            "        return DeleteRequestBuilder\n" +
            "                .create(%1$s.class, getStates().getBeanLabel())\n" +
            "                .setEditNode(getSource().getInitializer().getEditNode());\n" +
            "    }\n\n";
    public static final String TO_DELETE_REQUEST_MULTIPLE = "" +
            "    @Override\n" +
            "    public DeleteRequestBuilder.StepSetExtra toDeleteRequest() {\n" +
            "        return DeleteRequestBuilder\n" +
            "                .create(%1$s.class, getStates().getSelectedDatasLabel())\n" +
            "                .setEditNode(getSource().getInitializer().getEditNode());\n" +
            "    }\n\n";
    public static final String LOAD_EMPTY_FORM = "" +
            "    @Override\n" +
            "    public Form<%1$s> loadForm(ObserveServicesProvider servicesProvider, String selectedId) {\n" +
            "        return null;\n" +
            "    }\n\n";
    public static final String TO_MOVE_REQUEST = "" +
            "    @Override\n" +
            "    public MoveRequestBuilderStepConfigure toMoveRequest() {\n" +
            "        return MoveRequestBuilder\n" +
            "                .create(%1$s.class, getSource().getParent().getParentReference().toLabel(), getStates().getBeanLabel())\n" +
            "                .setEditNode(getSource().getInitializer().getEditNode())\n" +
            "                .setGroupByValue(this::getGroupByValue);\n" +
            "    }\n\n";
    public static final String TO_MOVE_REQUEST_ROOT = "" +
            "    @Override\n" +
            "    public MoveRequestBuilderStepConfigure toMoveRequest() {\n" +
            "        return MoveRootRequestBuilder\n" +
            "                .create(%1$s.class, getSource().getParent().getParentReference().toLabel(), getStates().getBeanLabel())\n" +
            "                .setEditNode(getSource().getInitializer().getEditNode())\n" +
            "                .setGroupByValue(this::getGroupByValue);\n" +
            "    }\n\n";
    public static final String TO_MOVE_LAYOUT_REQUEST = "" +
            "    public final MoveLayoutRequestBuilderStepConfigure toMoveRequest() {\n" +
            "        return MoveLayoutRequestBuilder\n" +
            "                .create(%1$s.class, getSource().getParent().getReference().toShortDto())\n" +
            "                .setEditNode(getSource().getInitializer().getEditNode())\n" +
            "                .setGroupByValue(this::getGroupByValue)\n" +
            "                .setAvailableLayoutType(%1$s.class);\n" +
            "    }\n\n";

    public static final String TO_MOVE_REQUEST_LAYOUT_ROOT = "" +
            "    public MoveLayoutRequestBuilderStepConfigure toMoveRequest() {\n" +
            "        return MoveLayoutRequestBuilder\n" +
            "                .create(%1$s.class, getStates().getBeanLabel())\n" +
            "                .setEditNode(getSource().getInitializer().getEditNode())\n" +
            "                .setGroupByValue(this::getGroupByValue)\n" +
            "                .setAvailableLayoutTypes(List.of(%2$s));\n" +
            "    }\n\n";
    public static final String TO_MOVE_REQUEST_MULTIPLE = "" +
            "    @Override\n" +
            "    public MoveRequestBuilderStepConfigure toMoveRequest() {\n" +
            "        return MoveRequestBuilder\n" +
            "                .create(%1$s.class, getStates().selectedParent(), getStates().getSelectedDatasLabel())\n" +
            "                .setEditNode(getSource().getInitializer().getEditNode())\n" +
            "                .setGroupByValue(this::getGroupByValue);\n" +
            "    }\n\n";
    public static final String TO_MOVE_REQUEST_MULTIPLE_ROOT = "" +
            "    @Override\n" +
            "    public MoveRequestBuilderStepConfigure toMoveRequest() {\n" +
            "        return MoveRootRequestBuilder\n" +
            "                .create(%1$s.class, getStates().selectedParent(), getStates().getSelectedDatasLabel())\n" +
            "                .setEditNode(getSource().getInitializer().getEditNode())\n" +
            "                .setGroupByValue(this::getGroupByValue);\n" +
            "    }\n\n";
    public static final String CREATE_FORM = "" +
            "    @Override\n" +
            "    public Form<%1$s> preCreateForm(ObserveServicesProvider servicesProvider, String parentId) {\n" +
            "        return servicesProvider.%2$s().preCreate(%1$s.class, parentId);\n" +
            "    }\n\n";
    public static final String CREATE_ROOT_FORM = "" +
            "    @Override\n" +
            "    public Form<%1$s> preCreateForm(ObserveServicesProvider servicesProvider, String parentId) {\n" +
            "        return servicesProvider.%2$s().preCreate(%1$s.class, getGroupByValue());\n" +
            "    }\n\n";

    public static String generateSimpleGetSource(String position) {
        return String.format(ContentUIModelHelper.GET_SOURCE, position);
    }

    public static String generateStates(String cleanClassName) {
        return String.format(STATES, cleanClassName + "ModelStates");
    }

    public static String generateStates2(String cleanClassName) {
        return String.format(STATES2, cleanClassName + "ModelStates");
    }

    public ContentUIModelHelper(GenerateContentUISupport generator) {
        super(generator);
    }

    public String generateModelGeneratedContent(Class<?> dtoType, Class<?> referenceType, String extraMethods, String save) {
        List<String> imports = new LinkedList<>();
        if (!extraMethods.isEmpty()) {
            imports.add(DtoReference.class.getName());
            imports.add(NavigationNode.class.getName());
        }
        extraMethods += generateGetSource(imports, dtoType);
        if (referenceType != null) {
            imports.add(referenceType.getName());
        }
        if (save != null) {
            imports.add(SaveRequest.class.getName());
            extraMethods += save;
        }
        Class<? extends BusinessDto> parentDtoType = generator.scopeBuilder.parentDtoType;
        String generics = dtoType.getSimpleName();
        boolean parentIsRoot = parentDtoType != null && RootOpenableDto.class.isAssignableFrom(parentDtoType);
        switch (generator.getDescriptor().getContentNodeType()) {
            case LAYOUT:
                imports.add(ObserveServicesProvider.class.getName());
                imports.add(Form.class.getName());
                imports.add(MoveLayoutRequestBuilder.class.getName());
                imports.add(MoveLayoutRequestBuilderStepConfigure.class.getName());
                imports.add(DeleteRequestBuilder.class.getName());
                imports.add(generator.scopeBuilder.parentDtoReferenceType.getName());
                imports.add(Objects.requireNonNull(parentDtoType).getName());
                extraMethods += String.format(LOAD_FORM, dtoType.getSimpleName(), "getSimpleService");
                extraMethods += String.format(TO_DELETE_REQUEST, dtoType.getSimpleName());
                extraMethods += String.format(TO_MOVE_LAYOUT_REQUEST, dtoType.getSimpleName());
                break;
            case SIMPLE:
                imports.add(ObserveServicesProvider.class.getName());
                imports.add(Form.class.getName());
                extraMethods += String.format(LOAD_FORM, dtoType.getSimpleName(), "getSimpleService");
                break;
            case EDIT:
                imports.add(ObserveServicesProvider.class.getName());
                imports.add(Form.class.getName());
                imports.add(DeleteRequestBuilder.class.getName());
                extraMethods += String.format(LOAD_FORM, dtoType.getSimpleName(), "getEditableService");
                extraMethods += String.format(CREATE_FORM, dtoType.getSimpleName(), "getEditableService");
                extraMethods += String.format(TO_DELETE_REQUEST, dtoType.getSimpleName());
                break;
            case OPEN:
                imports.add(ObserveServicesProvider.class.getName());
                imports.add(Form.class.getName());
                imports.add(DeleteRequestBuilder.class.getName());
                imports.add(MoveRequestBuilderStepConfigure.class.getName());

                imports.add(generator.scopeBuilder.parentDtoReferenceType.getName());
                extraMethods += String.format(LOAD_FORM, dtoType.getSimpleName(), "getOpenableService");
                extraMethods += String.format(CREATE_FORM, dtoType.getSimpleName(), "getOpenableService");
                extraMethods += String.format(TO_DELETE_REQUEST_WITH_EDIT, dtoType.getSimpleName());

                if (parentIsRoot) {
                    imports.add(MoveRootRequestBuilder.class.getName());
                    extraMethods += String.format(TO_MOVE_REQUEST_ROOT, dtoType.getSimpleName());
                } else {
                    imports.add(MoveRequestBuilder.class.getName());
                    extraMethods += String.format(TO_MOVE_REQUEST, dtoType.getSimpleName());
                }
                break;
            case ROPEN:
                imports.add(ObserveServicesProvider.class.getName());
                imports.add(Form.class.getName());
                imports.add(DeleteRequestBuilder.class.getName());
                extraMethods += String.format(LOAD_FORM, dtoType.getSimpleName(), "getRootOpenableService");
                extraMethods += String.format(CREATE_ROOT_FORM, dtoType.getSimpleName(), "getRootOpenableService");
                extraMethods += String.format(TO_DELETE_REQUEST_WITH_EDIT, dtoType.getSimpleName());
                if (UsingLayout.class.isAssignableFrom(dtoType)) {
                    imports.add(MoveLayoutRequestBuilder.class.getName());
                    imports.add(List.class.getName());
                    StringBuilder types = new StringBuilder();
                    for (CapabilityDescriptor descriptor : generator.capabilitiesDescriptor) {
                        switch (descriptor.getContentNodeType()) {
                            case LAYOUT:
                            case TABLE:
                            case LIST:
                                types.append(String.format(", %s.class", descriptor.getOptionalDtoType().getName()));
                                break;
                        }
                    }
                    imports.add(MoveLayoutRequestBuilderStepConfigure.class.getName());
                    extraMethods += String.format(TO_MOVE_REQUEST_LAYOUT_ROOT, dtoType.getSimpleName(), types.substring(2));
                }
                break;
            case LIST:
                imports.add(MoveRequestBuilderStepConfigure.class.getName());
                imports.add(DeleteRequestBuilder.class.getName());
                imports.add(generator.scopeBuilder.parentDtoReferenceType.getName());
                imports.add(Objects.requireNonNull(parentDtoType).getName());
                extraMethods += String.format(TO_DELETE_REQUEST_MULTIPLE, dtoType.getSimpleName());
                if (parentIsRoot) {
                    imports.add(MoveRootRequestBuilder.class.getName());
                    extraMethods += String.format(TO_MOVE_REQUEST_MULTIPLE_ROOT, dtoType.getSimpleName());
                } else {
                    imports.add(MoveRequestBuilder.class.getName());
                    extraMethods += String.format(TO_MOVE_REQUEST_MULTIPLE, dtoType.getSimpleName());
                }
                generics = Objects.requireNonNull(referenceType).getSimpleName();
                break;
            case RLIST:
                imports.add(DeleteRequestBuilder.class.getName());
                extraMethods += String.format(TO_DELETE_REQUEST_MULTIPLE, dtoType.getSimpleName());
                generics = Objects.requireNonNull(referenceType).getSimpleName();
                break;
        }
        return generate(UI_MODEL_GENERATED, imports, generator.cleanClassName, generator.getDescriptor().getContentNodeType().getUiModelType(), generics, extraMethods);
    }

    public String generateModelConcreteContent() {
        List<String> imports = new LinkedList<>();
        return generate(UI_MODEL_CONCRETE, imports, generator.cleanClassName);
    }

    public String generateGetSource(List<String> imports, Class<?> extraType) {
        String result = "";
        if (generator.notStandalone) {
            result += "    @SuppressWarnings({\"unchecked\", \"rawtypes\"})\n";
        }
        imports.add(extraType.getName());
        result += String.format(GET_SOURCE, generator.cleanClassName);
        return result;
    }
}
