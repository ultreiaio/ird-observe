package fr.ird.observe.client.datasource.editor.spi.content.helper;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Created on 08/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public class ContentUIModelStatesHelper extends ContentUIHelperSupport {

    public static final String GENERATED_CONTENT = "" +
            "public class %1$sModelStates extends %2$s {\n\n" +
            "%3$s" +
            "}\n";
    public static final String CONTENT = "" +
            "@GenerateJavaBeanDefinition\n" +
            "public class %1$sModelStates extends Generated%1$sModelStates {\n\n" +
            "%2$s" +
            "}\n";

    public ContentUIModelStatesHelper(GenerateContentUISupport generator) {
        super(generator);
    }

    public String generateGeneratedContent(List<String> imports, String superClass, String context) {
        return generate(GENERATED_CONTENT, imports, "Generated" + generator.cleanClassName, superClass, context);
    }

    public String generateContent(List<String> imports, String context) {
        imports.add(GenerateJavaBeanDefinition.class.getName());
        return generate(CONTENT, imports, generator.cleanClassName, context);
    }

    public static String addSavePredicate(Path sourceDirectory, Path targetDirectory, Path path, String namePrefix) {
        Path sourcePath = sourceDirectory.resolve(targetDirectory.relativize(path));
        String savePredicateClassName = namePrefix + "SavePredicate";
        Path sourceSavePredicatePath = sourcePath.getParent().resolve(savePredicateClassName + ".java");
        if (Files.exists(sourceSavePredicatePath)) {
            return String.format("" +
                                         "    @Override\n" +
                                         "    public final %1$s savePredicate() {\n" +
                                         "        return new %1$s((%2$sModelStates)this);\n" +
                                         "    }\n\n", savePredicateClassName, namePrefix);
        }
        return "";
    }
}
