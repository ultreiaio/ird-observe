package fr.ird.observe.client.datasource.editor.spi.content.helper;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.actions.save.NodeChildrenUpdate;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationHandler;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ContainerCapability;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.ReferenceContainerCapability;
import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.ContentNodeType;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateJavaFileSupport;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.reference.DtoReference;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.bean.definition.JavaBeanPropertyDefinition;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 08/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentUINavigationCapabilityHelper extends ContentUIHelperSupport {
    public static final String NAVIGATION_CAPABILITY_CONCRETE = "" +
            "public class %1$sNavigationCapability extends Generated%1$sNavigationCapability {\n\n" +
            "    public %1$sNavigationCapability(%1$sNavigationNode node) {\n" +
            "        super(node);\n" +
            "    }\n\n" +
            "}\n";
    public static final String ON_BEAN_TO_UPDATE_NODE_METHOD = "" +
            "    public static NodeChildrenUpdate fillBeforeBuilder(NodeChildrenUpdate.BuilderOnBeanToUpdate<%1$s> builder) {\n" +
            "        return builder\n" +
            "%2$s" +
            "                .build();\n" +
            "    }\n\n";
    public static final String ON_UPDATED_BEAN_NODE_METHOD = "" +
            "    public static NodeChildrenUpdate fillAfterBuilder(NodeChildrenUpdate.BuilderOnUpdatedBean<%1$s> builder) {\n" +
            "        return builder\n" +
            "%2$s" +
            "                .build();\n" +
            "    }\n\n";

    public static final String CREATE_CHILDREN_METHOD = "" +
            "    @Override\n" +
            "    public void buildChildren() {\n" +
            "        %1$sNavigationNode node = getNode();\n" +
            "        if (node.getInitializer().isNotPersisted()) {\n" +
            "            return;\n" +
            "        }\n" +
            "%2$s" +
            "    }\n\n";
    public static final String CREATE_CHILDREN_METHOD2 = "" +
            "    @Override\n" +
            "    public void buildChildren() {\n" +
            "        %1$sNavigationNode node = getNode();\n" +
            "%2$s" +
            "    }\n\n";

    public static final String GET_TYPES = "" +
            "    @Override\n" +
            "    public List<Class<? extends DtoReference>> getAcceptedTypes() {\n" +
            "        return ACCEPTED_TYPES;\n" +
            "    }\n\n";
    public static final String ACCEPT_TYPES = "" +
            "    @Override\n" +
            "    public boolean acceptChildReferenceType(Class<? extends DtoReference> dtoReferenceType) {\n" +
            "        return getAcceptedTypes().contains(dtoReferenceType);\n" +
            "    }\n\n";
    public static final String STATIC_NODE_TYPES_FIELD = "    public static final List<Class<? extends NavigationNode>> ACCEPTED_NODE_TYPES = List.of(%s);\n\n";
    public static final String STATIC_REFERENCES_TYPES_FIELD = "    public static final List<Class<? extends DtoReference>> ACCEPTED_TYPES = List.of(%s);\n\n";
    public static final String GET_MODEL = "" +
            "    @Override\n" +
            "    public %1$sModel getModel() {\n" +
            "        return (%1$sModel) super.getModel();\n" +
            "    }\n\n";
    public static final String GET_NODE_TYPES = "" +
            "    @Override\n" +
            "    public List<Class<? extends NavigationNode>> getAcceptedNodeTypes() {\n" +
            "        return ACCEPTED_NODE_TYPES;\n" +
            "    }\n\n";
    public static final String GET_NODE_POSITION_METHOD = "" +
            "    @Override\n" +
            "    public int getNodePosition(DtoReference reference) {\n" +
            "        if (reference.isNotPersisted()) {\n" +
            "            // not persisted, always at bottom\n" +
            "            return getNode().getChildCount();\n" +
            "        }\n" +
            "        int actualPosition = 0;\n" +
            "%2$s" +
            "        throw new IllegalStateException(\"Can't get node position for reference: \" + reference);\n" +
            "    }\n\n";
    public static final String CREATE_CHILD = "" +
            "        if (reference instanceof %1$s.%2$sReference) {\n" +
            "            return getNode().new%3$s((%1$s.%2$sReference) reference);\n" +
            "        }\n";
    public static final String CREATE_CHILD_NODE_METHOD = "" +
            "    @Override\n" +
            "    public NavigationNode createChildNode(DtoReference reference) {\n" +
            "%2$s" +
            "        throw new IllegalStateException(\"Can't create node with reference: \" + reference);\n" +
            "    }\n\n";

    public static String generateCreateNodeMethods(GenerateJavaFileSupport generator) {
        if (generator.capabilitiesDescriptor == null) {
            return "";
        }
        StringBuilder createChildrenMethodBuilder = new StringBuilder();
        ContentNodeType contentNodeType = generator.getDescriptor().getContentNodeType();
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            String addNodeMethod = capabilityDescriptor.generateCapabilityAddNodeMethod(contentNodeType);
            createChildrenMethodBuilder.append(addNodeMethod);
        }
        String template = CREATE_CHILDREN_METHOD;
        if (contentNodeType == ContentNodeType.LIST || contentNodeType == ContentNodeType.RLIST) {
            template = CREATE_CHILDREN_METHOD2;
        }
        return String.format(template, generator.cleanClassName, createChildrenMethodBuilder);
    }

    public static String generateOnBeanToUpdateNodeUpdaterMethod(GenerateJavaFileSupport generator, List<String> imports) {
        if (generator.capabilitiesDescriptor == null) {
            return "";
        }
        Class<? extends BusinessDto> dtoType = generator.scopeBuilder.dtoType;
        StringBuilder createChildrenMethodBuilder = new StringBuilder();
        JavaBeanDefinition javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(dtoType).orElseThrow();
        Set<String> availableProperties = javaBeanDefinition.readProperties().map(JavaBeanPropertyDefinition::propertyName).collect(Collectors.toSet());
        boolean used = false;
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            String addNodeMethod = capabilityDescriptor.generateCapabilityOnBeanToUpdateUpdateNodeMethod(dtoType, availableProperties);
            if (addNodeMethod != null) {
                used = true;
                createChildrenMethodBuilder.append(addNodeMethod);
            }
        }
        if (used) {
            imports.add(NodeChildrenUpdate.class.getName());
            imports.add(dtoType.getName());
            return String.format(ON_BEAN_TO_UPDATE_NODE_METHOD, dtoType.getSimpleName(), createChildrenMethodBuilder);

        }
        return "";
    }

    public static String generateOnUpdatedBeanNodeUpdaterMethod(GenerateJavaFileSupport generator, List<String> imports) {
        if (generator.capabilitiesDescriptor == null) {
            return "";
        }
        Class<? extends BusinessDto> dtoType = generator.scopeBuilder.dtoType;
        StringBuilder createChildrenMethodBuilder = new StringBuilder();
        JavaBeanDefinition javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(dtoType).orElseThrow();
        Set<String> availableProperties = javaBeanDefinition.readProperties().map(JavaBeanPropertyDefinition::propertyName).collect(Collectors.toSet());
        boolean used = false;
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            String addNodeMethod = capabilityDescriptor.generateCapabilityOnUpdatedBeanNodeUpdateNodeMethod(dtoType, availableProperties);
            if (addNodeMethod != null) {
                used = true;
                createChildrenMethodBuilder.append(addNodeMethod);
            }
        }
        if (used) {
            imports.add(NodeChildrenUpdate.class.getName());
            imports.add(dtoType.getName());
            return String.format(ON_UPDATED_BEAN_NODE_METHOD, dtoType.getSimpleName(), createChildrenMethodBuilder);

        }
        return "";
    }

    public static String generateAcceptedNodeTypes(GenerateJavaFileSupport generator) {
        StringBuilder acceptedNodeTypes = new StringBuilder();
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            acceptedNodeTypes.append(",\n            ").append(capabilityDescriptor.getNodeTypeName()).append(".class");
        }
        if (acceptedNodeTypes.length() > 0) {
            return String.format(STATIC_NODE_TYPES_FIELD, acceptedNodeTypes.substring(1));
        }
        return "";
    }

    public static String generateAcceptedTypes(GenerateJavaFileSupport generator) {
        StringBuilder acceptedTypes = new StringBuilder();
        boolean onLayout = generator.getDescriptor().getContentNodeType() == ContentNodeType.LAYOUT;
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            switch (capabilityDescriptor.getCapacityNodeType()) {
                case STATIC:
                case STATIC_WITH_PREDICATE:
                    if (onLayout) {
                        if (capabilityDescriptor.getContentNodeType() == ContentNodeType.LIST) {
                            @SuppressWarnings({"unchecked", "rawtypes"}) Class<?> referenceType = generator.businessProject.getMapping().getReferenceType((Class) capabilityDescriptor.getOptionalDtoType());
                            acceptedTypes.append(",\n            ").append(referenceType.getName()).append(".class");
                        }
                    }
                    break;
                case REFERENCE:
                case REFERENCE_LIST:
                    acceptedTypes.append(",\n            ").append(capabilityDescriptor.getOptionalReferenceType().getName()).append(".class");
                    break;
            }
        }
        if (acceptedTypes.length() > 0) {
            return String.format(STATIC_REFERENCES_TYPES_FIELD, acceptedTypes.substring(1));
        }
        return "";
    }

    public ContentUINavigationCapabilityHelper(GenerateContentUISupport generator) {
        super(generator);
    }

    public String generateStaticFields() {
        return generateAcceptedNodeTypes(generator) + generateAcceptedTypes(generator);
    }

    public String generateCreateNodeMethods() {
        return generateCreateNodeMethods(generator);
    }

    public String generateNodeUpdaterMethod(List<String> imports) {
        return generateOnBeanToUpdateNodeUpdaterMethod(generator, imports) + generateOnUpdatedBeanNodeUpdaterMethod(generator, imports);
    }

    public String generateGetModel() {
        return String.format(GET_MODEL, generator.cleanClassName);
    }

    public String generateNavigationCapabilityConcreteContent() {
        return generate(NAVIGATION_CAPABILITY_CONCRETE, generator.cleanClassName);
    }

    public boolean useContainerCapabilities(List<String> imports) {
        if (generator.capabilitiesDescriptor == null) {
            return false;
        }
        boolean useStaticCapabilities = false;
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            switch (capabilityDescriptor.getCapacityNodeType()) {
                case STATIC_WITH_PREDICATE:
                case STATIC:
                    useStaticCapabilities = true;
                    break;
                case REFERENCE:
                    imports.add(DtoReference.class.getName());
                    break;
                case REFERENCE_LIST:
                    break;
            }
        }
        imports.add(NavigationNode.class.getName());
        imports.add(List.class.getName());
        imports.add(ContainerCapability.class.getName());
        return useStaticCapabilities;
    }

    public boolean useReferenceContainerCapabilities(List<String> imports) {
        if (generator.capabilitiesDescriptor == null) {
            return false;
        }
        boolean result = false;
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            switch (capabilityDescriptor.getCapacityNodeType()) {
                case STATIC:
                case STATIC_WITH_PREDICATE:
                    break;
                case REFERENCE:
                case REFERENCE_LIST:
                    result = true;
                    break;
            }
        }
        if (result) {
            imports.add(DtoReference.class.getName());
            imports.add(Comparator.class.getName());
            imports.add(NavigationHandler.class.getName());
            imports.add(ReferenceContainerCapability.class.getName());
        }
        return result;
    }

    public String generateGetPositionNodeMethod(String comparator) {
        StringBuilder nodePositionBuilder = new StringBuilder();
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            nodePositionBuilder.append(capabilityDescriptor.generateCapabilityGetPosition(comparator));
        }
        return String.format(GET_NODE_POSITION_METHOD, generator.cleanClassName, nodePositionBuilder);
    }

    public String generateCreateChildNodeMethod() {
        StringBuilder createChildNode = new StringBuilder();
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            switch (capabilityDescriptor.getCapacityNodeType()) {
                case STATIC:
                case STATIC_WITH_PREDICATE:
                    continue;
                case REFERENCE_LIST:
                case REFERENCE:
                    switch (capabilityDescriptor.getContentNodeType()) {
                        case LIST:
                        case RLIST:
                        case REFERENTIAL_HOME:
                        case REFERENTIAL:
                        case ROOT:
                        case SIMPLE:
                        case TABLE:
                            continue;
                        case EDIT:
                        case OPEN:
                        case ROPEN:
                            createChildNode.append(String.format(CREATE_CHILD, capabilityDescriptor.getOptionalReferenceType().getPackage().getName(), capabilityDescriptor.getOptionalReferenceTypeCleanSimpleName(), capabilityDescriptor.getMethodName()));
                            break;
                    }
                    break;
            }
        }
        return String.format(CREATE_CHILD_NODE_METHOD, generator.cleanClassName, createChildNode);
    }

}
