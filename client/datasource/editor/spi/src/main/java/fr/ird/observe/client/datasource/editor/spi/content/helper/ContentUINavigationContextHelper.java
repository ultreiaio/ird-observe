package fr.ird.observe.client.datasource.editor.spi.content.helper;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationContext;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;

import java.util.LinkedList;
import java.util.List;

/**
 * Created on 08/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentUINavigationContextHelper extends ContentUIHelperSupport {

    public static final String NAVIGATION_CONTEXT_GENERATED = "" +
            "public class Generated%1$sNavigationContext extends %2$s {\n\n" +
            "%3$s" +
            "}\n";

    public static final String NAVIGATION_CONTEXT_CONCRETE = "" +
            "public class %1$sNavigationContext extends Generated%1$sNavigationContext {\n\n" +
            "}\n";
    public static final String GET_REFERENCE = "" +
            "    @Override\n" +
            "    public final %1$s get%2$s() {\n" +
            "        return (%1$s) super.get%2$s();\n" +
            "    }\n\n";
    public static final String GET_UNCHECKED_REFERENCE = "" +
            "    @SuppressWarnings(\"unchecked\")\n" +
            "    @Override\n" +
            "    public final %1$s get%2$s() {\n" +
            "        return (%1$s) super.get%2$s();\n" +
            "    }\n\n";
    public static final String GET_SIBLING = "" +
            "    @Override\n" +
            "    public %1$sNavigationNode findSibling(String siblingId) {\n" +
            "        return (%1$sNavigationNode) super.findSibling(siblingId);\n" +
            "    }\n\n";
    public static final String GET_PARENT_SIBLING = "" +
            "    @Override\n" +
            "    public %1$sNavigationNode findParentSibling(String siblingId) {\n" +
            "        return (%1$sNavigationNode) super.findParentSibling(siblingId);\n" +
            "    }\n\n";
    public static final String ADD_CHILDREN = "" +
            "    @Override\n" +
            "    public %1$sUINavigationNode addMissingChildren(String newSelectedId) {\n" +
            "        return (%1$sUINavigationNode) super.addMissingChildren(newSelectedId);\n" +
            "    }\n\n";

    public static String generateGetReference(List<String> imports, Class<?> referenceType) {
        imports.add(referenceType.getName());
        return String.format(GET_REFERENCE, referenceType.getSimpleName(), "Reference");
    }

    public static String generateGetReferences(List<String> imports, Class<?> referenceType) {
        imports.add(referenceType.getName());
        imports.add(List.class.getName());
        return String.format(GET_UNCHECKED_REFERENCE, "List<" + referenceType.getSimpleName() + ">", "References");
    }

    public static String generateGetParentReference(List<String> imports, Class<?> referenceType) {
        imports.add(referenceType.getName());
        return String.format(GET_REFERENCE, referenceType.getSimpleName(), "ParentReference");
    }

    public static String generateGetParentReference(String referenceType) {
        return String.format(GET_REFERENCE, referenceType, "ParentReference");
    }

    public static String generateAddChildren(String name) {
        return String.format(ADD_CHILDREN, name);
    }

    public static String generateGetSibling(String name) {
        return String.format(GET_SIBLING, name);
    }

    public static String generateGetParentSibling(String name) {
        return String.format(GET_PARENT_SIBLING, name);
    }

    public ContentUINavigationContextHelper(GenerateContentUISupport generator) {
        super(generator);
    }

    public String generateNavigationContextConcreteContent() {
        return generate(NAVIGATION_CONTEXT_CONCRETE, generator.cleanClassName);
    }

    public String generateNavigationContextGeneratedContent() {
        return generateNavigationContextGeneratedContent(new LinkedList<>(), "");
    }

    public String generateNavigationContextGeneratedContent(List<String> imports, String extra) {
        Class<? extends NavigationContext<?>> nodeContextType = generator.getDescriptor().getContentNodeType().getNodeContextType();
        return generate(NAVIGATION_CONTEXT_GENERATED, imports, generator.cleanClassName, nodeContextType, extra);
    }

}
