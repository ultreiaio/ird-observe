package fr.ird.observe.client.datasource.editor.spi.content.helper;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.NullCapability;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;

import java.util.List;

/**
 * Created on 08/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentUINavigationHandlerHelper extends ContentUIHelperSupport {
    public static final String CONSTRUCTOR = "" +
            "    public Generated%1$sNavigationHandler(%1$sNavigationNode node) {\n" +
            "        super(node);\n" +
            "    }\n\n";
    public static final String NAVIGATION_HANDLER_CONCRETE = "" +
            "public class %1$sNavigationHandler extends Generated%1$sNavigationHandler {\n\n" +
            "%2$s" +
            "}\n";
    public static final String GET_NODE_POSITION = "" +
            "    @Override\n" +
            "    public final int getNodePosition(fr.ird.observe.dto.reference.DtoReference reference) {\n" +
            "        return getNode().getCapability().getNodePosition(reference);\n" +
            "    }\n\n";
    public static final String BUILD_CHILDREN = "" +
            "    @Override\n" +
            "    public final void buildChildren() {\n" +
            "        getNode().getCapability().buildChildren();\n" +
            "    }\n\n";
    public static final String GET_CONTEXT = "" +
            "    @Override\n" +
            "    public final %1$s getContext() {\n" +
            "        return getNode().getContext();\n" +
            "    }\n\n";
    public static final String GET_NODE = "" +
            "    @Override\n" +
            "    public final %1$sNavigationNode getNode() {\n" +
            "        return (%1$sNavigationNode) super.getNode();\n" +
            "    }\n\n";
    public static final String GET_CAPABILITY = "" +
            "    @Override\n" +
            "    public final %1$s getCapability() {\n" +
            "        return getNode().getCapability();\n" +
            "    }\n\n";
    public static final String GET_NULL_CAPABILITY = "" +
            "    @Override\n" +
            "    public final %1$s getCapability() {\n" +
            "        return getNode().getCapability();\n" +
            "    }\n\n";

    public static String generateConstructor(String cleanClassName) {
        return generateGeneratedConstructor(cleanClassName).replace("public Generated", "public ");
    }

    public static String generateGeneratedConstructor(String cleanClassName) {
        return String.format(CONSTRUCTOR, cleanClassName);
    }

    public static String generateGetters(String nodeContextName, String nodeCapabilityName) {
        String result = "" + String.format(GET_CONTEXT, nodeContextName);
        if (nodeCapabilityName == null) {
            result += String.format(GET_NULL_CAPABILITY, "NullCapability<?>");
        } else {

            result += String.format(GET_CAPABILITY, nodeCapabilityName);
        }
        return result;
    }

    public ContentUINavigationHandlerHelper(GenerateContentUISupport generator) {
        super(generator);
    }

    public String generateGetters(List<String> imports) {
        String result = "";
        if (generator.notStandalone) {
            result += "    @SuppressWarnings({\"unchecked\", \"rawtypes\"})\n";
            result += String.format(GET_NODE, generator.cleanClassName);
        }
        result += String.format(GET_CONTEXT, generator.nodeContextName);
        if (generator.nodeCapabilityName == null) {
            imports.add(NullCapability.class.getName());
            result += String.format(GET_CAPABILITY, "NullCapability<?>");
        } else {
            result += String.format(GET_CAPABILITY, generator.nodeCapabilityName);
        }
        return result;
    }

    public String generateGeneratedConstructor() {
        return generateGeneratedConstructor(generator.cleanClassName);
    }

    public String generateConstructor() {
        return generateConstructor(generator.cleanClassName);
    }

    public String generateNavigationHandlerConcreteContent() {
        return generate(NAVIGATION_HANDLER_CONCRETE, generator.cleanClassName, generateConstructor());
    }
}
