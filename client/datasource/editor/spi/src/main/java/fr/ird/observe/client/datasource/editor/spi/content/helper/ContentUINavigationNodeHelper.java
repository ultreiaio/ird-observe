package fr.ird.observe.client.datasource.editor.spi.content.helper;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateContentUISupport;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateJavaFileSupport;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.services.service.data.EmptyChildrenDataReferenceSet;
import fr.ird.observe.services.service.data.RootEmptyChildrenDataReferenceSet;
import fr.ird.observe.spi.module.ObserveBusinessProject;

import java.util.List;
import java.util.Objects;

/**
 * Created on 08/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ContentUINavigationNodeHelper extends ContentUIHelperSupport {

    public static final String CREATE_NODE_CONTENT_REFERENTIAL = "" +
            "    public static %1$s create(long initialCount) {\n" +
            "        return init(new %1$s(), initialCount);\n" +
            "    }\n\n";
    public static final String CREATE_NODE_HOME_REFERENTIAL = "" +
            "    public static %1$s create(Map<Class<? extends ReferentialDto>, Long> referentialCountMap) {\n" +
            "        return init(new %1$s(), referentialCountMap);\n" +
            "    }\n\n";
    public static final String CREATE_NODE_SIMPLE = "" +
            "    public static %1$s create(java.util.function.Supplier<%2$s> parentReference) {\n" +
            "        return init(new %1$s(), parentReference);\n" +
            "    }\n\n";
    public static final String CREATE_NODE_CONTENT_LAYOUT = "" +
            "    public static %1$s create(java.util.function.Supplier<%2$s> parentReference) {\n" +
            "        %1$s node = new %1$s();\n" +
            "        return init(node, parentReference);\n" +
            "    }\n\n";
    public static final String CREATE_NODE_CONTENT_LIST = "" +
            "    public static %1$s create(java.util.function.Supplier<%2$s> parentReference, long initialCount) {\n" +
            "        %1$s node = new %1$s();\n" +
            "        return init(node, parentReference, fr.ird.observe.services.service.data.OpenableService.create(%3$s.class, %4$s.class, parentReference.get().getId(), (int) initialCount, node.getContext().getBusinessProject(), node.getContext().getServicesProvider()::getOpenableService, node.getContext().getDecoratorService()));\n" +
            "    }\n\n";
    public static final String CREATE_NODE_CONTENT_ROOT_LIST = "" +
            "    public static %1$s create(%2$s groupByDto, String groupByFlavor) {\n" +
            "        %1$s node = new %1$s();\n" +
            "        return init(node, groupByDto, fr.ird.observe.services.service.data.RootOpenableService.create(%3$s.class, %4$s.class, groupByDto, groupByFlavor, node.getContext().getBusinessProject(), node.getContext().getServicesProvider()::getRootOpenableService, node.getContext().getDecoratorService()));\n" +
            "    }\n\n";
    public static final String CREATE_NODE2 = "" +
            "    public static %1$s create(java.util.function.Supplier<%3$s> parentReference, %2$s reference) {\n" +
            "        return init(new %1$s(), parentReference, reference);\n" +
            "    }\n\n";
    public static final String CREATE_NODE3 = "" +
            "    public static %1$s create(java.util.function.Supplier<%3$s> parentReference, %2$s reference) {\n" +
            "        return init(new %1$s(), parentReference, reference);\n" +
            "    }\n\n";
    public static final String CREATE_CONTEXT = "" +
            "    @Override\n" +
            "    public %1$s getContext() {\n" +
            "        return (%1$s) super.getContext();\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    protected %1$s createContext() {\n" +
            "        return new %1$s();\n" +
            "    }\n\n";
    public static final String CREATE_HANDLER = "" +
            "    @Override\n" +
            "    public %1$s getHandler() {\n" +
            "        return (%1$s) super.getHandler();\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    protected %1$s createHandler() {\n" +
            "        return new %1$s(this);\n" +
            "    }\n\n";
    public static final String GET_PARENT = "" +
            "    @Override\n" +
            "    public final %1$s getParent() {\n" +
            "        return (%1$s) super.getParent();\n" +
            "    }\n\n";
    public static final String CREATE_CAPABILITY = "" +
            "    @Override\n" +
            "    public %1$s getCapability() {\n" +
            "        return (%1$s) super.getCapability();\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    protected %1$s createCapability() {\n" +
            "        return new %1$s(this);\n" +
            "    }\n\n";
    private static final String CONTENT = "" +
            "@AutoService(NavigationNode.class)\n" +
            "public class %1$sNavigationNode extends %2$s {\n\n" +
            "%3$s" +
            "}\n";

    public static String generateCreateContext(String nodeContextName, String nodeHandlerName, String nodeCapabilityName) {
        String result = "";
        if (nodeContextName != null) {
            result += String.format(CREATE_CONTEXT, nodeContextName);
        }
        if (nodeHandlerName != null) {
            result += String.format(CREATE_HANDLER, nodeHandlerName);
        }
        if (nodeCapabilityName != null) {
            result += String.format(CREATE_CAPABILITY, nodeCapabilityName);
        }
        return result;
    }

    public static String generateCreateContext2(String nodeContextName, String nodeHandlerName, String nodeCapabilityName) {
        String result = "";
        if (nodeContextName != null) {
            result += "    @SuppressWarnings({\"unchecked\", \"rawtypes\"})\n";
            result += String.format(CREATE_CONTEXT, nodeContextName);
        }
        if (nodeHandlerName != null) {
            result += "    @SuppressWarnings({\"unchecked\", \"rawtypes\"})\n";
            result += String.format(CREATE_HANDLER, nodeHandlerName);
        }
        if (nodeCapabilityName != null) {
            result += "    @SuppressWarnings({\"unchecked\", \"rawtypes\"})\n";
            result += String.format(CREATE_CAPABILITY, nodeCapabilityName);
        }
        return result;
    }

    public static String generateParentNode(String parentNodeName) {
        return parentNodeName == null ? "" : String.format(GET_PARENT, parentNodeName);
    }

    public static String generateCreateNode4(String nodeHandlerName) {
        return String.format(CREATE_NODE_HOME_REFERENTIAL, nodeHandlerName.replace("Handler", "Node"));
    }

    public static String generateCreateNodeContentReferential(String nodeHandlerName) {
        return String.format(CREATE_NODE_CONTENT_REFERENTIAL, nodeHandlerName.replace("Handler", "Node"));
    }

    public static String generateCreateNode2(String nodeHandlerName, String referenceType, String parentType) {
        return String.format(CREATE_NODE2, nodeHandlerName.replace("Handler", "Node"), referenceType, parentType);
    }

    public static String generateCreateNode3(String nodeHandlerName, String referenceType, String parentType) {
        return String.format(CREATE_NODE3, nodeHandlerName.replace("Handler", "Node"), referenceType, parentType);
    }

    public static String generateCreateNodeMethods(GenerateJavaFileSupport generator, List<String> imports) {
        if (generator.capabilitiesDescriptor == null) {
            return "";
        }
        StringBuilder createChildrenMethodBuilder = new StringBuilder();
        StringBuilder addChildrenMethodBuilder = new StringBuilder();
        StringBuilder getChildrenMethodBuilder = new StringBuilder();
        for (CapabilityDescriptor capabilityDescriptor : generator.capabilitiesDescriptor) {
            String newNodeMethod = capabilityDescriptor.generateNewNodeMethod(generator.getDescriptor());
            createChildrenMethodBuilder.append(newNodeMethod);
            String addNodeMethod = capabilityDescriptor.generateAddNodeMethod();
            addChildrenMethodBuilder.append(addNodeMethod);
            String getNodeMethod = capabilityDescriptor.generateGetNodeMethod();
            getChildrenMethodBuilder.append(getNodeMethod);
            if (capabilityDescriptor.getCapacityNodeType().canCreateNode()) {
                imports.add(ReferentialLocale.class.getName());
                String newEmptyNodeMethod = capabilityDescriptor.generateNewEmptyNodeMethod(generator.getDescriptor());
                createChildrenMethodBuilder.append(newEmptyNodeMethod);
                String addEmptyNodeMethod = capabilityDescriptor.generateAddEmptyNodeMethod();
                addChildrenMethodBuilder.append(addEmptyNodeMethod);
            }
        }
        return createChildrenMethodBuilder.toString()
                + addChildrenMethodBuilder
                + getChildrenMethodBuilder;
    }

    public ContentUINavigationNodeHelper(GenerateContentUISupport generator) {
        super(generator);
    }

    public String generateParentNode() {
        if (generator.notStandalone) {
            return "";
        }
        String parentNodeName = generator.parentNodeName;
        if (parentNodeName == null) {
            generator.getLog().error("Found a node without parent: " + generator.cleanClassName);
            parentNodeName = RootNavigationNode.class.getName();
        }
        return String.format(GET_PARENT, parentNodeName);
    }

    public String generateCreateContextWithCapability() {
        if (generator.notStandalone) {
            return generateCreateContext2(Objects.requireNonNull(generator.nodeHandlerName).replace("Handler", "Context"), generator.nodeHandlerName, generator.nodeHandlerName.replace("Handler", "Capability"));
        }
        return generateCreateContext(Objects.requireNonNull(generator.nodeHandlerName).replace("Handler", "Context"), generator.nodeHandlerName, generator.nodeHandlerName.replace("Handler", "Capability"));
    }

    public String generateCreateContextWithoutCapability() {
        if (generator.notStandalone) {
            return generateCreateContext2(Objects.requireNonNull(generator.nodeHandlerName).replace("Handler", "Context"), generator.nodeHandlerName, null);
        }
        return generateCreateContext(Objects.requireNonNull(generator.nodeHandlerName).replace("Handler", "Context"), generator.nodeHandlerName, null);
    }

    public String generateCreateNodeLayout(List<String> imports) {
        imports.add(DtoReference.class.getName());
        imports.add(generator.scopeBuilder.parentDtoReferenceType.getName());
        imports.add(AutoService.class.getName());
        imports.add(NavigationNode.class.getName());
        return String.format(CREATE_NODE_CONTENT_LAYOUT, generator.nodeHandlerName.replace("Handler", "Node"),
                             generator.scopeBuilder.parentDtoReferenceType.getSimpleName());
    }

    public String generateCreateNodeSimple(List<String> imports) {
        imports.add(DtoReference.class.getName());
        imports.add(AutoService.class.getName());
        imports.add(NavigationNode.class.getName());
        return String.format(CREATE_NODE_SIMPLE, generator.nodeHandlerName.replace("Handler", "Node"),
                             generator.scopeBuilder.parentDtoReferenceType.getSimpleName());
    }

    public String generateCreateNodeContentTable(List<String> imports) {
        imports.add(AutoService.class.getName());
        imports.add(NavigationNode.class.getName());
        if (generator.notStandalone) {
            imports.add(DtoReference.class.getName());
            return String.format(CREATE_NODE_SIMPLE, generator.nodeHandlerName.replace("Handler", "Node"),
                                 "? extends " + DtoReference.class.getSimpleName());
        }
        imports.add(generator.scopeBuilder.parentDtoReferenceType.getName());
        return String.format(CREATE_NODE_SIMPLE, generator.nodeHandlerName.replace("Handler", "Node"),
                             generator.scopeBuilder.parentDtoReferenceType.getSimpleName());
    }

    public String generateCreateNodeContentList(List<String> imports) {
        imports.add(generator.scopeBuilder.parentDtoReferenceType.getName());
        imports.add(generator.scopeBuilder.dtoType.getName());
        imports.add(generator.scopeBuilder.dtoReferenceType.getName());
        imports.add(AutoService.class.getName());
        imports.add(NavigationNode.class.getName());
        imports.add(EmptyChildrenDataReferenceSet.class.getName());
        imports.add(ObserveBusinessProject.class.getName());
        return String.format(CREATE_NODE_CONTENT_LIST,
                             generator.nodeHandlerName.replace("Handler", "Node"),
                             generator.scopeBuilder.parentDtoReferenceType.getSimpleName(),
                             generator.scopeBuilder.dtoType.getSimpleName(),
                             generator.scopeBuilder.dtoReferenceType.getSimpleName());
    }

    public String generateCreateNodeContentRootList(List<String> imports) {
        imports.add(generator.scopeBuilder.dtoType.getName());
        imports.add(generator.scopeBuilder.dtoReferenceType.getName());
        imports.add(AutoService.class.getName());
        imports.add(NavigationNode.class.getName());
        imports.add(ObserveBusinessProject.class.getName());
        imports.add(DataGroupByDto.class.getName());
        imports.add(RootEmptyChildrenDataReferenceSet.class.getName());
        return String.format(CREATE_NODE_CONTENT_ROOT_LIST,
                             generator.nodeHandlerName.replace("Handler", "Node"),
                             DataGroupByDto.class.getSimpleName() + "<?>",
                             generator.scopeBuilder.dtoType.getSimpleName(),
                             generator.scopeBuilder.dtoReferenceType.getSimpleName());
    }

    public String generateCreateNodeOpenable(List<String> imports, String referenceType) {
        imports.add(DtoReference.class.getName());
        imports.add(AutoService.class.getName());
        imports.add(NavigationNode.class.getName());
        return generateCreateNode2(generator.nodeHandlerName, referenceType, generator.scopeBuilder.parentDtoReferenceType.getSimpleName());
    }

    public String generateCreateNodeRootOpenable(List<String> imports, String referenceType) {
        imports.add(DtoReference.class.getName());
        imports.add(DataGroupByDto.class.getName());
        imports.add(AutoService.class.getName());
        imports.add(NavigationNode.class.getName());
        return generateCreateNode3(generator.nodeHandlerName, referenceType, String.format("%s<%s>", DataGroupByDto.class.getSimpleName(), referenceType.replace("Reference", "Dto")));
    }

    public String generateCreateNodeEdit(List<String> imports, String referenceType) {
        imports.add(DtoReference.class.getName());
        imports.add(AutoService.class.getName());
        imports.add(NavigationNode.class.getName());
        return generateCreateNode2(generator.nodeHandlerName, referenceType, generator.scopeBuilder.parentDtoReferenceType.getSimpleName());
    }

    public String generateCreateNodeMethods(List<String> imports) {
        return generateCreateNodeMethods(generator, imports);
    }

    public String generateContent(List<String> imports, String context) {
        Class<? extends NavigationNode> nodeType = generator.getDescriptor().getContentNodeType().getNodeType();
        return generate(CONTENT, imports, generator.cleanClassName, nodeType, context);
    }
}
