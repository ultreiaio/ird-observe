package fr.ird.observe.client.datasource.editor.spi.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIHandler;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModel;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUIModelStates;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUINavigationContext;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUINavigationHandler;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUINavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationScope;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.capability.NullCapability;
import fr.ird.observe.client.datasource.editor.spi.content.ContentUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateJavaFileSupport;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeBuilder;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIModelHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIModelStatesHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationContextHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationHandlerHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationNodeHelper;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.lang.Objects2;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateContentReferentialUI extends GenerateJavaFileSupport {

    public static final String MODEL = "" +
            "public class %1$sUIModel extends ContentReferentialUIModel<%1$sDto, %1$sReference> {\n\n" +
            "    public %1$sUIModel(%1$sUINavigationNode source) {\n" +
            "        super(source);\n" +
            "    }\n\n" +
            "%2$s" +
            "}\n";
    public static final String HANDLER = "" +
            "class %1$sUIHandler extends ContentReferentialUIHandler<%1$sDto, %1$sReference, %1$sUI> implements UIHandler<%1$sUI> {\n\n" +
            "}\n";
    public static final String NAVIGATION_CONTEXT = "" +
            "public class %1$sUINavigationContext extends ContentReferentialUINavigationContext {\n\n" +
            "}\n";
    public static final String NAVIGATION_HANDLER = "" +
            "public class %1$sUINavigationHandler extends ContentReferentialUINavigationHandler<%1$sUINavigationNode> {\n\n" +
            "%2$s" +
            "}\n";
    private static final String NAVIGATION_NODE = "" +
            "@AutoService(NavigationNode.class)\n" +
            "public class %1$sUINavigationNode extends ContentReferentialUINavigationNode {\n\n" +
            "%2$s" +
            "}\n";
    private static final String MODEL_STATES_CONSTRUCTOR = "" +
            "    public %1$sUIModelStates(%1$sUIModel model) {\n" +
            "        super(model, %1$sDto.newDto(new java.util.Date()));\n" +
            "    }\n\n";
    public static final String TO_REFERENCE_METHOD = "    @Override\n" +
            "    public %s toReference(%s bean, ReferentialLocale referentialLocale) {\n" +
            "        return bean.toReference(referentialLocale);\n" +
            "    }\n\n";
    private String namePrefix;
    private String dtoNamePrefix;
    private String dtoPackage;
    private String nodeHandlerName;
    private Class<? extends ReferentialDto> dtoType;
    private Class<? extends ReferentialDtoReference> referenceType;
    private String nodeContextName;

    @Override
    protected Class<? extends ContentUIDescriptor> getDescriptorType() {
        return ContentReferentialUIDescriptor.class;
    }

    @Override
    public String getExtensionToScan() {
        return "UI.jaxx";
    }

    @Override
    public String getDirectory() {
        return "referential";
    }

    private final DecoratorService decoratorService = new DecoratorService(ReferentialLocale.FR);

    @Override
    public void run() {
        editModel = new Project();
        selectModel = new Project();
        super.run();
    }

    protected void processPath(Path sourceDirectory, Path targetDirectory, Path uiPath, String fileContent) {
        packageName = sourceDirectory.relativize(uiPath.getParent()).toString().replace(".jaxx", "").replaceAll(File.separator, ".");
        namePrefix = uiPath.toFile().getName().replace(".jaxx", "");
        this.className = uiPath.toFile().getName().replace(".jaxx", "");
        this.cleanClassName = cleanClassName(className);
        businessModule = ObserveBusinessProject.get().getBusinessModule(packageName);
        businessSubModule = ObserveBusinessProject.get().getBusinessSubModule(businessModule, packageName);
        parentNodeName = packageName + ".ReferentialHomeUINavigationNode";
        dtoPackage = businessSubModule.getReferentialPackageName();
        dtoNamePrefix = namePrefix.replace("UI", "");
        nodeHandlerName = dtoNamePrefix + "UINavigationHandler";
        nodeContextName = nodeHandlerName.replace("Handler", "Context");
        dtoType = Objects2.forName(dtoPackage + "." + dtoNamePrefix + "Dto");
        referenceType = Objects2.forName(dtoPackage + "." + dtoNamePrefix + "Reference");
        initCapabilities();
        scopeBuilder = new Builder().createBuilder(dtoNamePrefix + "UI", dtoNamePrefix + "UINavigationNode");
        doGenerate(uiPath, "ModelStates", this::generateUIModelStates);
        doGenerate(uiPath, "Model", this::generateUIModel);
        doGenerate(uiPath, "Handler", this::generateUIHandler);
        doGenerate(uiPath, "NavigationContext", this::generateNavigationContext);
        doGenerate(uiPath, "NavigationHandler", this::generateNavigationHandler);
        doGenerate(uiPath, "NavigationNode", this::generateNavigationNode);
        Path scopePath = getNavigationScope(uiPath, namePrefix);
        generateNavigationScope(scopePath);
    }

    private void doGenerate(Path uiPath, String suffix, BiConsumer<Path, List<String>> generator) {
        Path uiHandlerPath = uiPath.getParent().resolve(namePrefix + suffix + ".java");
        if (Files.notExists(uiHandlerPath)) {
            Path file = targetDirectory.resolve(sourceDirectory.relativize(uiHandlerPath));
            List<String> imports = new LinkedList<>();
            imports.add(dtoType.getName());
            imports.add(referenceType.getName());
            generator.accept(file, imports);
        }
    }

    private void generateUIModelStates(Path path, List<String> imports) {
        imports.add(GenerateJavaBeanDefinition.class.getName());
        imports.add(ContentReferentialUIModelStates.class.getName());
        imports.add(dtoType.getName());
        imports.add(referenceType.getName());
        imports.add(ReferentialLocale.class.getName());
        String extraMethods = String.format(MODEL_STATES_CONSTRUCTOR, dtoNamePrefix);
        extraMethods += String.format(TO_REFERENCE_METHOD, referenceType.getSimpleName(), dtoType.getSimpleName());
        extraMethods += ContentUIModelStatesHelper.addSavePredicate(sourceDirectory, targetDirectory, path, namePrefix);
        String content = generate(ContentUIModelStatesHelper.GENERATED_CONTENT, imports, cleanClassName, String.format("ContentReferentialUIModelStates<%s, %s>", dtoType.getSimpleName(), referenceType.getSimpleName()), extraMethods);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateUIModel(Path path, List<String> imports) {
        imports.add(ContentReferentialUIModel.class.getName());
        String extraMethods = ContentUIModelHelper.generateSimpleGetSource(dtoNamePrefix + "UI");
        extraMethods += ContentUIModelHelper.generateStates(cleanClassName);
        String content = generate(MODEL, imports, dtoNamePrefix, extraMethods);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateUIHandler(Path path, List<String> imports) {
        imports.add(ContentReferentialUIHandler.class.getName());
        imports.add(UIHandler.class.getName());
        String content = generate(HANDLER, imports, dtoNamePrefix);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateNavigationContext(Path path, List<String> imports) {
        imports.add(ContentReferentialUINavigationContext.class.getName());
        String content = generate(NAVIGATION_CONTEXT, imports, dtoNamePrefix);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateNavigationHandler(Path path, List<String> imports) {
        imports.add(ContentReferentialUINavigationHandler.class.getName());
        imports.add(NullCapability.class.getName());
        String extraMethods = ContentUINavigationHandlerHelper.generateConstructor(dtoNamePrefix + "UI");
        extraMethods += ContentUINavigationHandlerHelper.generateGetters(nodeContextName, null);
        String content = generate(NAVIGATION_HANDLER, imports, dtoNamePrefix, extraMethods);

        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateNavigationNode(Path path, List<String> imports) {
        imports.add(ContentReferentialUINavigationInitializer.class.getName());
        imports.add(ContentReferentialUINavigationNode.class.getName());
        imports.add(NavigationNode.class.getName());
        imports.add(AutoService.class.getName());
        String context = ContentUINavigationNodeHelper.generateCreateNodeContentReferential(nodeHandlerName) + ContentUINavigationNodeHelper.generateCreateContext(nodeContextName, nodeHandlerName, null);
        context += ContentUINavigationNodeHelper.generateParentNode(parentNodeName);
        context += ContentUINavigationNodeHelper.generateCreateNodeMethods(this, imports);
        context += ContentUINavigationContextHelper.generateGetReferences(imports, referenceType);
        String content = generate(NAVIGATION_NODE, imports, dtoNamePrefix, context);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateNavigationScope(Path path) {
        String iconPath = generateIconPath("Home");
        addI118nProperty(dtoType, "type");
        NavigationScopeDescriptor descriptor = scopeBuilder.build(i18nMapping, iconPath);
        String content = serializeDescriptor(descriptor);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    protected String generate(String template, Object... parameters) {
        return generate0(getClass(), template, packageName, new LinkedList<>(), parameters);
    }

    protected String generate(String template, List<String> imports, Object... parameters) {
        return generate0(getClass(), template, packageName, imports, parameters);
    }

    public class Builder extends NavigationScopeBuilder {


        public Builder() {
            super(ContentReferentialUINavigationNode.class, GenerateContentReferentialUI.this);
        }

        @Override
        public Builder createBuilder(String contentUiType, String nodeType) {
            boolean longDecoratorDefinition = decoratorService.optionalDecoratorByType(dtoType, DecoratorService.LONG_CLASSIFIER).isPresent();
            return newBuilder(contentUiType, nodeType)
                    .setModule()
                    .setNoAutoLoadNode() // don't want to load all referential at init time
                    .setDtoType(Objects2.forName(dtoPackage + "." + dtoNamePrefix + "Dto"))
                    .setDtoReferenceTypeFromDtoType()
                    .setNodeDataTypeFromDtoType()
                    .setDecoratorClassifier(longDecoratorDefinition ? DecoratorService.LONG_CLASSIFIER : null);
        }

        @Override
        public void prepareBuild(Builder builder) {
            builder.registerType(NavigationScope.TYPE_MODEL_MAIN, builder.dtoType);
            builder.registerType(NavigationScope.TYPE_MODEL_MAIN_REFERENCE, builder.dtoReferenceType);
        }
    }

}
