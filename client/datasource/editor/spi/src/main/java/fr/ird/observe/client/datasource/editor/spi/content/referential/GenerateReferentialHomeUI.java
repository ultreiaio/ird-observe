package fr.ird.observe.client.datasource.editor.spi.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUINavigationInitializer;
import fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUINavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.NavigationNode;
import fr.ird.observe.client.datasource.editor.api.navigation.tree.root.RootNavigationNode;
import fr.ird.observe.client.datasource.editor.spi.content.ContentUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateJavaFileSupport;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeBuilder;
import fr.ird.observe.client.datasource.editor.spi.content.NavigationScopeDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIModelHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUIModelStatesHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationHandlerHelper;
import fr.ird.observe.client.datasource.editor.spi.content.helper.ContentUINavigationNodeHelper;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * Created on 28/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class GenerateReferentialHomeUI extends GenerateJavaFileSupport {

    public static final String UI = "" +
            "public class ReferentialHomeUI extends fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUI {\n\n" +
            "    public ReferentialHomeUI(JAXXContext param0) {\n" +
            "        super(((JAXXInitialContext) param0).add(new ReferentialHomeUIModel(getNavigationSource(param0))));\n" +
            "    }\n" +
            "}\n";
    public static final String MODEL = "" +
            "public class ReferentialHomeUIModel extends fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUIModel {\n\n" +
            "    public ReferentialHomeUIModel(ReferentialHomeUINavigationNode source) {\n" +
            "        super(source);\n" +
            "    }\n" +
            "%1$s" +
            "}\n";
    public static final String NAVIGATION_CONTEXT = "" +
            "public class ReferentialHomeUINavigationContext extends fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUINavigationContext {\n\n" +
            "}\n";
    public static final String NAVIGATION_HANDLER = "" +
            "public class ReferentialHomeUINavigationHandler extends fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUINavigationHandler<ReferentialHomeUINavigationNode> {\n\n" +
            "    public ReferentialHomeUINavigationHandler(ReferentialHomeUINavigationNode node) {\n" +
            "        super(node);\n" +
            "    }\n\n" +
            "%1$s" +
            "}\n";
    private static final String NAVIGATION_CAPABILITY = "" +
            "public class ReferentialHomeUINavigationCapability extends fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUINavigationCapability<ReferentialHomeUINavigationNode> {\n\n" +
            "    public ReferentialHomeUINavigationCapability(ReferentialHomeUINavigationNode node) {\n" +
            "        super(node);\n" +
            "    }\n\n" +
            "}\n";
    private static final String NAVIGATION_NODE = "" +
            "@AutoService(NavigationNode.class)\n" +
            "public class ReferentialHomeUINavigationNode extends fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUINavigationNode {\n\n" +
            "%1$s" +
            "}\n";

    private Set<Path> parentPaths;
    private String nodeHandlerName;
    private String nodeContextName;
    private String nodeCapabilityName;

    @Override
    protected Class<? extends ContentUIDescriptor> getDescriptorType() {
        return ReferentialHomeUIDescriptor.class;
    }

    @Override
    public String getExtensionToScan() {
        return "UI.jaxx";
    }

    @Override
    public String getDirectory() {
        return "referential";
    }

    @Override
    public void run() {
        parentPaths = new LinkedHashSet<>();
        super.run();
    }

    @Override
    protected void processPath(Path sourceDirectory, Path targetDirectory, Path uiPath, String fileContent) {
        packageName = sourceDirectory.relativize(uiPath.getParent()).toString().replace(".jaxx", "").replaceAll(File.separator, ".");
        if (parentPaths.add(uiPath.getParent())) {
            businessModule = ObserveBusinessProject.get().getBusinessModule(packageName);
            businessSubModule = businessProject.getBusinessSubModule(businessModule, packageName);
            this.className = ReferentialHomeUI.class.getSimpleName();
            this.cleanClassName = cleanClassName(className);

            nodeHandlerName = "ReferentialHomeUINavigationHandler";
            nodeContextName = nodeHandlerName.replace("Handler", "Context");
            nodeCapabilityName = nodeHandlerName.replace("Handler", "Capability");
            parentNodeName = RootNavigationNode.class.getName();
            initCapabilities();
            scopeBuilder = new Builder().createBuilder("ReferentialHomeUI", "ReferentialHomeUINavigationNode");
            doGenerate(uiPath, "ModelStates", this::generateUIModelStates);
            doGenerate(uiPath, "Model", this::generateUIModel);
            doGenerate(uiPath, "", this::generateUI);
            doGenerate(uiPath, "NavigationContext", this::generateNavigationContext);
            doGenerate(uiPath, "NavigationCapability", this::generateNavigationCapability);
            doGenerate(uiPath, "NavigationHandler", this::generateNavigationHandler);
            doGenerate(uiPath, "NavigationNode", this::generateNavigationNode);

            Path scopePath = getNavigationScope(uiPath, "ReferentialHomeUI");
            generateNavigationScope(scopePath);
        }
    }

    private void generateNavigationScope(Path path) {
        String iconPath = generateIconPath("Home");
        NavigationScopeDescriptor descriptor = scopeBuilder.build(i18nMapping, iconPath);
        String content = serializeDescriptor(descriptor);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void doGenerate(Path uiPath, String suffix, BiConsumer<Path, List<String>> generator) {
        Path uiHandlerPath = uiPath.getParent().resolve("ReferentialHomeUI" + suffix + ".java");
        if (Files.notExists(uiHandlerPath)) {
            Path file = targetDirectory.resolve(sourceDirectory.relativize(uiHandlerPath));
            List<String> imports = new LinkedList<>();
            generator.accept(file, imports);
        }
    }

    private void generateUI(Path path, List<String> imports) {
        imports.add(JAXXContext.class.getName());
        imports.add(JAXXInitialContext.class.getName());
        String content = generate(UI, imports);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateUIModelStates(Path path, List<String> imports) {
        imports.add(GenerateJavaBeanDefinition.class.getName());
        String content = generate(ContentUIModelStatesHelper.GENERATED_CONTENT, imports, cleanClassName, "fr.ird.observe.client.datasource.editor.api.content.referential.ReferentialHomeUIModelStates", "");
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateUIModel(Path path, List<String> imports) {
        String extraMethods = ContentUIModelHelper.generateSimpleGetSource("ReferentialHomeUI");
        extraMethods += ContentUIModelHelper.generateStates2(cleanClassName);
        String content = generate(MODEL, imports, extraMethods);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateNavigationContext(Path path, List<String> imports) {
        String content = generate(NAVIGATION_CONTEXT, imports);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateNavigationHandler(Path path, List<String> imports) {
        String content = generate(NAVIGATION_HANDLER, imports, ContentUINavigationHandlerHelper.generateGetters(nodeContextName, nodeCapabilityName));
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateNavigationCapability(Path path, List<String> imports) {
        String content = generate(NAVIGATION_CAPABILITY, imports);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    private void generateNavigationNode(Path path, List<String> imports) {
        String context = ContentUINavigationNodeHelper.generateCreateNode4(nodeHandlerName) + ContentUINavigationNodeHelper.generateCreateContext(nodeContextName, nodeHandlerName, nodeCapabilityName);
        context += ContentUINavigationNodeHelper.generateParentNode(parentNodeName);
//        context += ContentUINavigationNodeHelper.generateCreateNodeMethods(this, imports);
        imports.add(NavigationNode.class.getName());
        imports.add(ReferentialDto.class.getName());
        imports.add(Map.class.getName());
        imports.add(AutoService.class.getName());
        imports.add(ContentReferentialUINavigationInitializer.class.getName());
        String content = generate(NAVIGATION_NODE, imports, context);
        generate(path, packageName, content);
        generatedFileCount++;
    }

    protected String generate(String template, Object... parameters) {
        return generate0(getClass(), template, packageName, new LinkedList<>(), parameters);
    }

    protected String generate(String template, List<String> imports, Object... parameters) {
        return generate0(getClass(), template, packageName, imports, parameters);
    }

    public class Builder extends NavigationScopeBuilder {

        public Builder() {
            super(ReferentialHomeUINavigationNode.class, GenerateReferentialHomeUI.this);
        }

        @Override
        public Builder createBuilder(String contentUiType, String nodeType) {
            return newBuilder(contentUiType, nodeType)
                    .isString()
                    .setModule()
                    .setReferentialNodeChildTypesFromSubModule();
        }
    }
}
