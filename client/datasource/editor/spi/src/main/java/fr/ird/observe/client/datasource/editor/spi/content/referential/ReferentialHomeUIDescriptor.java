package fr.ird.observe.client.datasource.editor.spi.content.referential;

/*-
 * #%L
 * ObServe Client :: DataSource :: Editor :: SPI
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.editor.api.content.ContentUI;
import fr.ird.observe.client.datasource.editor.api.content.referential.ContentReferentialUI;
import fr.ird.observe.client.datasource.editor.spi.content.CapabilityDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.ContentNodeType;
import fr.ird.observe.client.datasource.editor.spi.content.ContentUIDescriptor;
import fr.ird.observe.client.datasource.editor.spi.content.GenerateJavaFileSupport;
import io.ultreia.java4all.util.SortedProperties;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Created on 06/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ReferentialHomeUIDescriptor implements ContentUIDescriptor {
    private static Map<Path, String> filesContent;
    private static Map<String, SortedProperties> capabilities;
    private static Map<String, List<CapabilityDescriptor>> capabilitiesDescriptor;

    @Override
    public String getExtensionToScan() {
        return "UI.jaxx";
    }

    @Override
    public String getDirectory() {
        return "referential";
    }

    @Override
    public Class<? extends ContentUI> getUiType() {
        return ContentReferentialUI.class;
    }

    @Override
    public ContentNodeType getContentNodeType() {
        return ContentNodeType.REFERENTIAL_HOME;
    }

    @Override
    public List<Class<? extends GenerateJavaFileSupport>> getGeneratorTypes() {
        return List.of(GenerateReferentialHomeUI.class);
    }

    public Map<Path, String> getFilesContent() {
        return filesContent;
    }

    @Override
    public void setFilesContent(Map<Path, String> filesContent) {
        ReferentialHomeUIDescriptor.filesContent = filesContent;
    }

    @Override
    public Map<String, SortedProperties> geCapabilities() {
        return capabilities;
    }

    @Override
    public void setCapabilities(Map<String, SortedProperties> capabilities) {
        ReferentialHomeUIDescriptor.capabilities = capabilities;
    }

    @Override
    public SortedProperties loadCapabilityFile(Path capabilitiesFile, String packageName, String cleanClassName) {
        SortedProperties result = new SortedProperties();
        int i = 0;
        String subModuleName = capabilitiesFile.getParent().toFile().getName();
        for (Path path : filesContent.keySet()) {
            if (!path.getParent().toFile().getName().equals(subModuleName)) {
                continue;
            }
            String className = path.toFile().getName().replace(".jaxx", "");
            String childNodeName = packageName + "." + cleanClassName(className) + "NavigationNode";
            result.put("" + (i++), childNodeName);
        }
        return result;
    }

    @Override
    public Pair<Path, String> acceptPath(Path sourceDirectory, Path targetDirectory, Path uiPath) {
        try {
            String fileContent = new String(Files.readAllBytes(uiPath));
            return Pair.of(uiPath, fileContent);
        } catch (Exception e) {
            throw new IllegalStateException(String.format("Can't load jaxx file: %s", uiPath), e);
        }
    }

    @Override
    public Pair<String, SortedProperties> acceptCapability(Path sourceDirectory, Path uiPath) {
        String packageName = sourceDirectory.relativize(uiPath.getParent()).toString().replace(".jaxx", "").replaceAll(File.separator, ".");
        String nodeName = packageName + ".ReferentialHomeUINavigationNode";
        Path capabilitiesFile = sourceDirectory.getParent().resolve("capabilities").resolve(sourceDirectory.relativize(uiPath)).getParent().resolve("ReferentialHomeUI" + ".capabilities");
        SortedProperties result = loadCapabilityFile(capabilitiesFile, packageName, "ReferentialHomeUI");
        return Pair.of(nodeName, result);
    }

    @Override
    public Map<String, List<CapabilityDescriptor>> getCapabilitiesDescriptor() {
        return capabilitiesDescriptor;
    }

    @Override
    public void setCapabilitiesDescriptor(Map<String, List<CapabilityDescriptor>> capabilities) {
        capabilitiesDescriptor = capabilities;
    }
}
