
Pour créer une nouvelle base
----------------------------

./create.sh

ou

./create.bat


Pour mettre à jour une base
----------------------------

./update.sh

ou

./update.bat

Pour mettre à jour la sécurité sur une base
-------------------------------------------

./update-security.sh

ou

./update-security.bat

Pour vider une base
-------------------

./drop.sh

ou

./drop.bat

Pour appliquer des scripts supplémentaires
------------------------------------------

Les rajouter (si besoin dans le répertoirer extra)

Lancer la commande

./apply-extra.sh
