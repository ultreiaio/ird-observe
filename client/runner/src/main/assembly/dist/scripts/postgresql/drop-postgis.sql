---
-- #%L
-- ObServe Client :: Runner
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER table common.Harbour DROP COLUMN the_geom CASCADE;
ALTER TABLE ps_observation.activity DROP COLUMN the_geom CASCADE;
ALTER TABLE ps_observation.transmittingbuoy DROP COLUMN the_geom CASCADE;
ALTER TABLE ps_logbook.activity DROP COLUMN the_geom CASCADE;
ALTER TABLE ps_logbook.activity DROP COLUMN the_geom_original CASCADE;
ALTER TABLE ps_logbook.transmittingbuoy DROP COLUMN the_geom CASCADE;
ALTER TABLE ll_observation.activity DROP COLUMN the_geom CASCADE;
ALTER TABLE ll_observation.set DROP COLUMN the_geom_settingstart CASCADE;
ALTER TABLE ll_observation.set DROP COLUMN the_geom_settingend CASCADE;
ALTER TABLE ll_observation.set DROP COLUMN the_geom_haulingstart CASCADE;
ALTER TABLE ll_observation.set DROP COLUMN the_geom_haulingend CASCADE;
ALTER TABLE ll_logbook.activity DROP COLUMN the_geom CASCADE;
ALTER TABLE ll_logbook.set DROP COLUMN the_geom_settingstart CASCADE;
ALTER TABLE ll_logbook.set DROP COLUMN the_geom_settingend CASCADE;
ALTER TABLE ll_logbook.set DROP COLUMN the_geom_haulingstart CASCADE;
ALTER TABLE ll_logbook.set DROP COLUMN the_geom_haulingend CASCADE;
ALTER TABLE ll_landing.landing DROP COLUMN the_geom CASCADE;
