---
-- #%L
-- ObServe Client :: Runner
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

--
-- Function and trigger to compute a postgis column on INSERT or UPDATE using default column names.
--
-- latitude, longitude and the_geom
--
CREATE OR REPLACE FUNCTION sync_the_geom_default() RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    IF (NEW.latitude IS NULL OR NEW.longitude IS NULL) THEN
        RAISE NOTICE 'No latitude or longitude, can not compute postgis field for id % ', NEW.topiaId;
        NEW.the_geom := NULL;
        return NEW;
    END IF;
    IF (TG_OP = 'UPDATE' AND NEW.latitude = OLD.latitude AND NEW.longitude = OLD.longitude)
    THEN
        return NEW;
    END IF;
    RAISE NOTICE 'Will compute the_geom for %.% % - latitude % and longitude %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.latitude, NEW.longitude;
    NEW.the_geom := public.ST_SetSRID(public.ST_MakePoint(NEW.longitude, NEW.latitude), 4326);
    RAISE NOTICE 'Computed for %.% % latitude % and longitude %, the_geom %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.latitude, NEW.longitude, NEW.the_geom;
    RETURN NEW;
END
$$ LANGUAGE 'plpgsql';

--
-- Function and trigger to compute a postgis column on INSERT or UPDATE using original column names.
--
-- latitudeOriginal, longitudeOriginal and the_geom_original
--
CREATE OR REPLACE FUNCTION sync_the_geom_original() RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    IF (NEW.latitudeOriginal IS NULL OR NEW.longitudeOriginal IS NULL) THEN
        RAISE NOTICE 'No latitudeOriginal or longitudeOriginal, can not compute postgis field for id % ', NEW.topiaId;
        NEW.the_geom_original := NULL;
        return NEW;
    END IF;
    IF (TG_OP = 'UPDATE' AND NEW.latitudeOriginal = OLD.latitudeOriginal AND
        NEW.longitudeOriginal = OLD.longitudeOriginal)
    THEN
        return NEW;
    END IF;
    RAISE NOTICE 'Will compute the_geom for %.% % - latitudeOriginal % and longitudeOriginal %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.latitudeOriginal, NEW.longitudeOriginal;
    NEW.the_geom_original := public.ST_SetSRID(public.ST_MakePoint(NEW.longitudeOriginal, NEW.latitudeOriginal), 4326);
    RAISE NOTICE 'Computed for %.% % latitudeOriginal % and longitudeOriginal %, the_geom_original %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.latitudeOriginal, NEW.longitudeOriginal, NEW.the_geom_original;
    RETURN NEW;
END
$$ LANGUAGE 'plpgsql';


--
-- Function and trigger to compute a postgis column on INSERT or UPDATE using setting start column names.
--
-- settingStartLatitude, settingStartLongitude and the_geom_settingStart
--
CREATE OR REPLACE FUNCTION sync_the_geom_settingStart() RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    IF (NEW.settingStartLatitude IS NULL OR NEW.settingStartLongitude IS NULL) THEN
        RAISE NOTICE 'No settingStartLatitude or settingStartLongitude, can not compute postgis field for id % ', NEW.topiaId;
        NEW.the_geom_settingStart := NULL;
        return NEW;
    END IF;
    IF (TG_OP = 'UPDATE' AND NEW.settingStartLatitude = OLD.settingStartLatitude AND
        NEW.settingStartLongitude = OLD.settingStartLongitude)
    THEN
        return NEW;
    END IF;
    RAISE NOTICE 'Will compute the_geom for %.% % - settingStartLatitude % and settingStartLongitude %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.settingStartLatitude, NEW.settingStartLongitude;
    NEW.the_geom_settingStart := public.ST_SetSRID(public.ST_MakePoint(NEW.settingStartLongitude, NEW.settingStartLatitude), 4326);
    RAISE NOTICE 'Computed for %.% % settingStartLatitude % and settingStartLongitude %, the_geom_settingStart %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.settingStartLatitude, NEW.settingStartLongitude, NEW.the_geom_settingStart;
    RETURN NEW;
END
$$ LANGUAGE 'plpgsql';

--
-- Function and trigger to compute a postgis column on INSERT or UPDATE using setting end column names.
--
-- settingEndLatitude, settingEndLongitude and the_geom_settingEnd
--
CREATE OR REPLACE FUNCTION sync_the_geom_settingEnd() RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    IF (NEW.settingEndLatitude IS NULL OR NEW.settingEndLongitude IS NULL) THEN
        RAISE NOTICE 'No settingEndLatitude or settingEndLongitude, can not compute postgis field for id % ', NEW.topiaId;
        NEW.the_geom_settingEnd := NULL;
        return NEW;
    END IF;
    IF (TG_OP = 'UPDATE' AND NEW.settingEndLatitude = OLD.settingEndLatitude AND
        NEW.settingEndLongitude = OLD.settingEndLongitude)
    THEN
        return NEW;
    END IF;
    RAISE NOTICE 'Will compute the_geom for %.% % - settingEndLatitude % and settingEndLongitude %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.settingEndLatitude, NEW.settingEndLongitude;
    NEW.the_geom_settingEnd := public.ST_SetSRID(public.ST_MakePoint(NEW.settingEndLongitude, NEW.settingEndLatitude), 4326);
    RAISE NOTICE 'Computed for %.% % settingEndLatitude % and settingEndLongitude %, the_geom_settingEnd %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.settingEndLatitude, NEW.settingEndLongitude, NEW.the_geom_settingEnd;
    RETURN NEW;
END
$$ LANGUAGE 'plpgsql';

--
-- Function and trigger to compute a postgis column on INSERT or UPDATE using hauling start column names.
--
-- haulingStartLatitude, haulingStartLongitude and the_geom_haulingStart
--
CREATE OR REPLACE FUNCTION sync_the_geom_haulingStart() RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    IF (NEW.haulingStartLatitude IS NULL OR NEW.haulingStartLongitude IS NULL) THEN
        RAISE NOTICE 'No haulingStartLatitude or haulingStartLongitude, can not compute postgis field for id % ', NEW.topiaId;
        NEW.the_geom_haulingStart := NULL;
        return NEW;
    END IF;
    IF (TG_OP = 'UPDATE' AND NEW.haulingStartLatitude = OLD.haulingStartLatitude AND
        NEW.haulingStartLongitude = OLD.haulingStartLongitude)
    THEN
        return NEW;
    END IF;
    RAISE NOTICE 'Will compute the_geom for %.% % - haulingStartLatitude % and haulingStartLongitude %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.haulingStartLatitude, NEW.haulingStartLongitude;
    NEW.the_geom_haulingStart := public.ST_SetSRID(public.ST_MakePoint(NEW.haulingStartLongitude, NEW.haulingStartLatitude), 4326);
    RAISE NOTICE 'Computed for %.% % haulingStartLatitude % and haulingStartLongitude %, the_geom_haulingStart %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.haulingStartLatitude, NEW.haulingStartLongitude, NEW.the_geom_haulingStart;
    RETURN NEW;
END
$$ LANGUAGE 'plpgsql';

--
-- Function and trigger to compute a postgis column on INSERT or UPDATE using hauling end column names.
--
-- haulingEndLatitude, haulingEndLongitude and the_geom_haulingEnd
--
CREATE OR REPLACE FUNCTION sync_the_geom_haulingEnd() RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    IF (NEW.haulingEndLatitude IS NULL OR NEW.haulingEndLongitude IS NULL) THEN
        RAISE NOTICE 'No haulingEndLatitude or haulingEndLongitude, can not compute postgis field for id % ', NEW.topiaId;
        NEW.the_geom_haulingEnd := NULL;
        return NEW;
    END IF;
    IF (TG_OP = 'UPDATE' AND NEW.haulingEndLatitude = OLD.haulingEndLatitude AND
        NEW.haulingEndLongitude = OLD.haulingEndLongitude)
    THEN
        return NEW;
    END IF;
    RAISE NOTICE 'Will compute the_geom for %.% % - haulingEndLatitude % and haulingEndLongitude %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.haulingEndLatitude, NEW.haulingEndLongitude;
    NEW.the_geom_haulingEnd := public.ST_SetSRID(public.ST_MakePoint(NEW.haulingEndLongitude, NEW.haulingEndLatitude), 4326);
    RAISE NOTICE 'Computed for %.% % haulingEndLatitude % and haulingEndLongitude %, the_geom_haulingEnd %', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.haulingEndLatitude, NEW.haulingEndLongitude, NEW.the_geom_haulingEnd;
    RETURN NEW;
END
$$ LANGUAGE 'plpgsql';
