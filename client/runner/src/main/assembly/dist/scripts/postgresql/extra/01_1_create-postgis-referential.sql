---
-- #%L
-- ObServe Client :: Runner
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE common.harbour ADD COLUMN the_geom geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_harbour_gist ON common.harbour USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_common_harbour_the_geom ON common.harbour;
CREATE TRIGGER tr_sync_common_harbour_the_geom BEFORE INSERT OR UPDATE OF latitude, longitude ON common.harbour FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_default();
UPDATE common.harbour SET the_geom = public.ST_SetSRID(public.ST_MakePoint(longitude,latitude), 4326) WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND the_geom IS NULL;
