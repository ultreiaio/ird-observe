---
-- #%L
-- ObServe Client :: Runner
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

ALTER TABLE ps_observation.activity ADD COLUMN the_geom geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ps_observation_activity_gist ON ps_observation.activity USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_ps_observation_activity_the_geom ON ps_observation.activity;
CREATE TRIGGER tr_sync_ps_observation_activity_the_geom BEFORE INSERT OR UPDATE OF latitude, longitude ON ps_observation.activity FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_default();
UPDATE ps_observation.activity SET the_geom = public.ST_SetSRID(public.ST_MakePoint(longitude,latitude), 4326) WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND the_geom IS NULL;

ALTER TABLE ps_observation.transmittingbuoy ADD COLUMN the_geom geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ps_observation_transmittingbuoy_gist ON ps_observation.TransmittingBuoy USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_ps_observation_transmittingbuoy_the_geom ON ps_observation.TransmittingBuoy;
CREATE TRIGGER tr_sync_ps_observation_transmittingbuoy_the_geom BEFORE INSERT OR UPDATE OF latitude, longitude ON ps_observation.TransmittingBuoy FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_default();
UPDATE ps_observation.TransmittingBuoy SET the_geom = public.ST_SetSRID(public.ST_MakePoint(longitude, latitude), 4326) WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND the_geom IS NULL;


ALTER TABLE ps_logbook.activity ADD COLUMN the_geom geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ps_logbook_activity_gist ON ps_logbook.activity USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_ps_logbook_activity_the_geom ON ps_logbook.activity;
CREATE TRIGGER tr_sync_ps_logbook_activity_the_geom BEFORE INSERT OR UPDATE OF latitude, longitude ON ps_logbook.activity FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_default();
UPDATE ps_logbook.activity SET the_geom = public.ST_SetSRID(public.ST_MakePoint(longitude,latitude), 4326) WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND the_geom IS NULL;

ALTER TABLE ps_logbook.activity ADD COLUMN the_geom_original geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ps_logbook_activity_original_gist ON ps_logbook.activity USING GIST (the_geom_original);
DROP TRIGGER IF EXISTS tr_sync_ps_logbook_activity_the_geom_original ON ps_logbook.activity;
CREATE TRIGGER tr_sync_ps_logbook_activity_the_geom_original BEFORE INSERT OR UPDATE OF latitudeOriginal, longitudeOriginal ON ps_logbook.activity FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_original();
UPDATE ps_logbook.activity SET the_geom_original = public.ST_SetSRID(public.ST_MakePoint(latitudeOriginal, longitudeOriginal), 4326) WHERE latitudeOriginal IS NOT NULL AND longitudeOriginal IS NOT NULL AND the_geom_original IS NULL;

ALTER TABLE ps_logbook.transmittingbuoy ADD COLUMN the_geom geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ps_logbook_transmittingbuoy_gist ON ps_logbook.TransmittingBuoy USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_ps_logbook_transmittingbuoy_the_geom ON ps_logbook.TransmittingBuoy;
CREATE TRIGGER tr_sync_ps_logbook_transmittingbuoy_the_geom BEFORE INSERT OR UPDATE OF latitude, longitude ON ps_logbook.TransmittingBuoy FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_default();
UPDATE ps_logbook.TransmittingBuoy SET the_geom = public.ST_SetSRID(public.ST_MakePoint(longitude, latitude), 4326) WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND the_geom IS NULL;
