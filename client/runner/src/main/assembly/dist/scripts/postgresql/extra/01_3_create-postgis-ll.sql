---
-- #%L
-- ObServe Client :: Runner
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

ALTER TABLE ll_observation.activity ADD COLUMN the_geom geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ll_observation_activity_gist ON ll_observation.activity USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_ll_observation_activity_the_geom ON ll_observation.activity;
CREATE TRIGGER tr_sync_ll_observation_activity_the_geom BEFORE INSERT OR UPDATE OF latitude, longitude ON ll_observation.activity FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_default();
UPDATE ll_observation.activity SET the_geom = public.ST_SetSRID(public.ST_MakePoint(longitude, latitude), 4326) WHERE the_geom IS NULL;

ALTER TABLE ll_observation.set ADD COLUMN the_geom_settingstart geometry(Point, 4326);
DROP INDEX IF EXISTS idx_ll_observation_set_gipublic.ST_settingstart;
CREATE INDEX IF NOT EXISTS idx_ll_observation_set_gipublic.ST_settingstart ON ll_observation.set USING GIST (the_geom_settingstart);
DROP TRIGGER IF EXISTS tr_sync_ll_observation_set_the_geom_settingstart ON ll_observation.set;
CREATE TRIGGER tr_sync_ll_observation_set_the_geom_settingstart BEFORE INSERT OR UPDATE OF settingStartLatitude, settingStartLongitude ON ll_observation.set FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_settingStart();
UPDATE ll_observation.set SET the_geom_settingstart = public.ST_SetSRID(public.ST_MakePoint(settingStartLongitude, settingStartLatitude), 4326) WHERE the_geom_settingstart IS NULL;

ALTER TABLE ll_observation.set ADD COLUMN the_geom_settingend geometry(Point, 4326);
DROP INDEX IF EXISTS idx_ll_observation_set_gipublic.ST_settingend;
CREATE INDEX IF NOT EXISTS idx_ll_observation_set_gipublic.ST_settingend ON ll_observation.set USING GIST (the_geom_settingend);
DROP TRIGGER IF EXISTS tr_sync_ll_observation_set_the_geom_settingend ON ll_observation.set;
CREATE TRIGGER tr_sync_ll_observation_set_the_geom_settingend BEFORE INSERT OR UPDATE OF settingEndLatitude, settingEndLongitude ON ll_observation.set FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_settingEnd();
UPDATE ll_observation.set SET the_geom_settingend = public.ST_SetSRID(public.ST_MakePoint(settingEndLongitude, settingEndLatitude), 4326) WHERE the_geom_settingend IS NULL;

ALTER TABLE ll_observation.set ADD COLUMN the_geom_haulingstart geometry(Point, 4326);
DROP INDEX IF EXISTS idx_ll_observation_set_gipublic.ST_haulingstart;
CREATE INDEX IF NOT EXISTS idx_ll_observation_set_gipublic.ST_haulingstart ON ll_observation.set USING GIST (the_geom_haulingstart);
DROP TRIGGER IF EXISTS tr_sync_ll_observation_set_the_geom_haulingstart ON ll_observation.set;
CREATE TRIGGER tr_sync_ll_observation_set_the_geom_haulingstart BEFORE INSERT OR UPDATE OF haulingStartLatitude, haulingStartLongitude ON ll_observation.set FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_haulingStart();
UPDATE ll_observation.set SET the_geom_haulingstart = public.ST_SetSRID(public.ST_MakePoint(haulingStartLongitude, haulingStartLatitude), 4326) WHERE the_geom_haulingstart IS NULL;

ALTER TABLE ll_observation.set ADD COLUMN the_geom_haulingend geometry(Point, 4326);
DROP INDEX IF EXISTS idx_ll_observation_set_gipublic.ST_haulingend;
CREATE INDEX IF NOT EXISTS idx_ll_observation_set_gipublic.ST_haulingend ON ll_observation.set USING GIST (the_geom_haulingend);
DROP TRIGGER IF EXISTS tr_sync_ll_observation_set_the_geom_haulingend ON ll_observation.set;
CREATE TRIGGER tr_sync_ll_observation_set_the_geom_haulingend BEFORE INSERT OR UPDATE OF haulingEndLatitude, haulingEndLongitude ON ll_observation.set FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_haulingEnd();
UPDATE ll_observation.set SET the_geom_haulingend = public.ST_SetSRID(public.ST_MakePoint(haulingEndLongitude, haulingEndLatitude), 4326) WHERE the_geom_haulingend IS NULL;

ALTER TABLE ll_logbook.activity ADD COLUMN the_geom geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ll_logbook_activity_gist ON ll_logbook.activity USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_ll_logbook_activity_the_geom ON ll_logbook.activity;
CREATE TRIGGER tr_sync_ll_logbook_activity_the_geom BEFORE INSERT OR UPDATE OF latitude, longitude ON ll_logbook.activity FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_default();
UPDATE ll_logbook.activity SET the_geom = public.ST_SetSRID(public.ST_MakePoint(longitude, latitude), 4326) WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND the_geom IS NULL;

ALTER TABLE ll_logbook.set ADD COLUMN the_geom_settingstart geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ll_logbook_set_gipublic.ST_settingstart ON ll_logbook.set USING GIST (the_geom_settingstart);
DROP TRIGGER IF EXISTS tr_sync_ll_logbook_set_the_geom_settingstart ON ll_logbook.set;
CREATE TRIGGER tr_sync_ll_logbook_set_the_geom_settingstart BEFORE INSERT OR UPDATE OF settingStartLatitude, settingStartLongitude ON ll_logbook.set FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_settingStart();
UPDATE ll_logbook.set SET the_geom_settingstart = public.ST_SetSRID(public.ST_MakePoint(settingStartLongitude, settingStartLatitude), 4326) WHERE settingStartLongitude IS NOT NULL AND settingStartLatitude IS NOT NULL AND the_geom_settingstart IS NULL;

ALTER TABLE ll_logbook.set ADD COLUMN the_geom_settingend geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ll_logbook_set_gipublic.ST_settingend ON ll_logbook.set USING GIST (the_geom_settingend);
DROP TRIGGER IF EXISTS tr_sync_ll_logbook_set_the_geom_settingend ON ll_logbook.set;
CREATE TRIGGER tr_sync_ll_logbook_set_the_geom_settingend BEFORE INSERT OR UPDATE OF settingEndLatitude, settingEndLongitude ON ll_logbook.set FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_settingEnd();
UPDATE ll_logbook.set SET the_geom_settingend = public.ST_SetSRID(public.ST_MakePoint(settingEndLongitude, settingEndLatitude), 4326) WHERE settingEndLongitude IS NOT NULL AND settingEndLatitude IS NOT NULL AND the_geom_settingend IS NULL;

ALTER TABLE ll_logbook.set ADD COLUMN the_geom_haulingstart geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ll_logbook_set_gipublic.ST_haulingstart ON ll_logbook.set USING GIST (the_geom_haulingstart);
DROP TRIGGER IF EXISTS tr_sync_ll_logbook_set_the_geom_haulingstart ON ll_logbook.set;
CREATE TRIGGER tr_sync_ll_logbook_set_the_geom_haulingstart BEFORE INSERT OR UPDATE OF haulingStartLatitude, haulingStartLongitude ON ll_logbook.set FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_haulingStart();
UPDATE ll_logbook.set SET the_geom_haulingstart = public.ST_SetSRID(public.ST_MakePoint(haulingStartLongitude, haulingStartLatitude), 4326) WHERE haulingStartLongitude IS NOT NULL AND haulingStartLatitude IS NOT NULL AND the_geom_haulingstart IS NULL;

ALTER TABLE ll_logbook.set ADD COLUMN the_geom_haulingend geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ll_logbook_set_gipublic.ST_haulingend ON ll_logbook.set USING GIST (the_geom_haulingend);
DROP TRIGGER IF EXISTS tr_sync_ll_logbook_set_the_geom_haulingend ON ll_logbook.set;
CREATE TRIGGER tr_sync_ll_logbook_set_the_geom_haulingend BEFORE INSERT OR UPDATE OF haulingEndLatitude, haulingEndLongitude ON ll_logbook.set FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_haulingEnd();
UPDATE ll_logbook.set SET the_geom_haulingend = public.ST_SetSRID(public.ST_MakePoint(haulingEndLongitude, haulingEndLatitude), 4326) WHERE haulingEndLongitude IS NOT NULL AND haulingEndLatitude IS NOT NULL AND the_geom_haulingend IS NULL;

ALTER TABLE ll_landing.landing ADD COLUMN the_geom geometry(Point, 4326);
CREATE INDEX IF NOT EXISTS idx_ll_landing_landing_gist ON ll_landing.landing USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_ll_landing_landing_the_geom ON ll_landing.landing;
CREATE TRIGGER tr_sync_ll_landing_landing_the_geom BEFORE INSERT OR UPDATE OF latitude, longitude ON ll_landing.landing FOR EACH ROW EXECUTE PROCEDURE sync_the_geom_default();
UPDATE ll_landing.landing SET the_geom = public.ST_SetSRID(public.ST_MakePoint(longitude, latitude), 4326) WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND the_geom IS NULL;
