Pour mettre à jour une base via serveur
---------------------------------------

./update.sh

ou

./update.bat

Pour mettre à jour la sécurité sur une base via serveur
-------------------------------------------------------

./update-security.sh

ou

./update-security.bat

Pour vider une base via serveur
-------------------------------

./drop.sh

ou

./drop.bat
