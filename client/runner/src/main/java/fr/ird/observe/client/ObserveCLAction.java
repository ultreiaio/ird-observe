/*
 * #%L
 * ObServe Client :: Runner
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client;

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.configuration.ClientConfigAction;
import fr.ird.observe.client.configuration.ClientConfigOption;
import fr.ird.observe.client.datasource.actions.AdminStep;
import fr.ird.observe.client.datasource.editor.api.wizard.ObstunaAdminAction;
import fr.ird.observe.client.datasource.editor.api.wizard.launchers.RemoteUILauncher;
import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.application.context.spi.GenerateApplicationComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.context.DefaultJAXXContext;
import org.nuiton.jaxx.runtime.swing.application.ActionWorker;

import java.io.Console;
import java.util.Arrays;
import java.util.EnumSet;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Les actions appellables via {@link RunObserve}.
 * <p>
 * Consulter la classe {@link ClientConfigAction} pour connaitre les actions
 * possibles.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see ClientConfig
 * @see RunObserve
 * @since 1.0
 */
@SuppressWarnings("unused")

@GenerateApplicationComponent(name = "ObServe Client actions")
public class ObserveCLAction implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(ObserveCLAction.class);

    /**
     * disable main ui.
     */
    private void disableMainUI() {
        log.debug(this);
        getClientConfig().setDisplayMainUI(false);
    }

    @SuppressWarnings("unused")
    public void help() {
        disableMainUI();

        StringBuilder out = new StringBuilder();

        out.append(t("observe.runner.message.help.usage", getClientConfig().getVersion()));
        out.append('\n');
        out.append("Options (set with --option <key> <value>:");
        out.append('\n');
        for (ClientConfigOption o : ClientConfigOption.values()) {

            out.append("\t");
            out.append(o.getKey());
            out.append("(");
            out.append(o.getDefaultValue());
            out.append(") :");
            out.append(t(o.getDescription()));
            out.append('\n');
        }

        out.append("Actions:");
        out.append('\n');
        for (ClientConfigAction a : ClientConfigAction.values()) {
            out.append("\t");
            out.append(Arrays.toString(a.getAliases()));
            out.append("(");
            out.append(a.getAction());
            out.append("):");
            out.append(t(a.getDescription()));
            out.append('\n');
        }
        Console cons;

        if ((cons = System.console()) != null) {
            cons.printf(out.toString());
        }
    }

    public void configure() {
        disableMainUI();
        //FIXME:BodyContent Make this works again
//        ApplicationContext context = ApplicationContext.get();

//        ObserveMainUIHandler handler = getMainUI().getHandler();
//
//        Runnable runnable = createRunnable(handler, "showConfig", context);
//
//        launchAction(t("observe.runner.action.showConfig.title"), runnable);
    }

    public void launchAdminUI(String operationName) {

        disableMainUI();

        EnumSet<AdminStep> operations = AdminStep.getOperations();

        AdminStep operation = AdminStep.valueOfIgnoreCase(operationName);
        if (operation == null) {
            if (log.isErrorEnabled()) {
                log.error(operationName + " is not a known admin operation.");
                log.error("Use one of these ones : " + operations);
            }
            return;
        }

        if (!operation.isOperation()) {
            if (log.isErrorEnabled()) {
                log.error(operation + " is not a admin operation(just a step in wizard).");
                log.error("Use one of these ones : " + operations);
            }
            return;
        }

//        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
        JAXXContext context = new DefaultJAXXContext();

        //FIXME:BodyContent Make this works again
        throw new IllegalStateException("To be back (dead since 02/11/2019)");
//        AdminUILauncher launcher = AdminUILauncher.newLauncher(context, operation);
//
//        Runnable runnable = createRunnable(launcher, "start");
//
//        launchAction(t(operation.getTitle()), runnable);
    }

    public void launchRemoteAdminUI(String operationName) throws InterruptedException {
        launchAdminUI(operationName, DataSourceConnectMode.REMOTE);
    }

    public void launchServerAdminUI(String operationName) throws InterruptedException {
        launchAdminUI(operationName, DataSourceConnectMode.SERVER);
    }

    //FIXME Review how to do this
    public void launchH2ServerMode() {

        if (!getClientConfig().isLocalStorageExist()) {
            log.error("Local database does not exist.");
        }
//        else {

//            ObserveSwingApplicationContext.get().setH2ServerMode(true);
//        }
    }

    public void createId(String className, int nbId) {

        disableMainUI();

//        Class<?> klazz = null;

        //FIXME
//        List<ObserveEntityEnum> enums = Lists.newArrayList(Entities.ALL_ENTITIES);
//        enums.remove(ObserveEntityEnum.CommentableEntity);
//        enums.remove(ObserveEntityEnum.OpenableEntity);
//        enums.remove(ObserveEntityEnum.I18nReferenceEntity);
//        enums.remove(ObserveEntityEnum.ReferenceEntity);
//
//        for (ObserveEntityEnum e : enums) {
//            if (className.equals(e.name())) {
//                klazz = e.getContract();
//                break;
//            }
//        }
//
//        if (klazz == null) {
//
//            Collections.sort(enums, new Comparator<ObserveEntityEnum>() {
//                @Override
//                public int compare(ObserveEntityEnum observeEntityEnum, ObserveEntityEnum observeEntityEnum2) {
//                    return observeEntityEnum.name().compareTo(observeEntityEnum2.name());
//                }
//            });
//            if (log.isErrorEnabled()) {
//                log.error(className + " not found! availables names :\n\t" + Joiner.on("\n\t").join(enums));
//            }
//            return;
//        }
//
//        for (int i = 0; i < nbId; i++) {
//            String topiaId = TopiaId.create(klazz);
//            System.out.println(topiaId);
//        }
    }

    private void launchAdminUI(String operationName, DataSourceConnectMode connectMode) throws InterruptedException {

        disableMainUI();

        EnumSet<ObstunaAdminAction> operations = EnumSet.allOf(ObstunaAdminAction.class);

        ObstunaAdminAction operation = ObstunaAdminAction.valueOfIgnoreCase(operationName);
        if (operation == null) {
            if (log.isErrorEnabled()) {
                log.error(operationName + " is not a known datasource admin operation.");
                log.error("Use one of these ones : " + operations);
            }
            return;
        }

        if (operation == ObstunaAdminAction.UPDATE || operation == ObstunaAdminAction.CREATE) {
            getClientConfig().setObstunaCanMigrate(true);
        }
        JAXXContext context = new DefaultJAXXContext();
        RemoteUILauncher launcher = operation.newLauncher(context, null, connectMode);

        launchAction(t(launcher.getTitle()), launcher::start);
    }

    private void launchAction(String title, Runnable target) throws InterruptedException {
        ObserveActionExecutor executor = (ObserveActionExecutor) RunObserve.getRunner().getActionExecutor();

        CommandLineActionWorker action = new CommandLineActionWorker(title, target);
        executor.addAction(action);

        // on attends la fin de l'opération
        ApplicationContext context = ApplicationContext.get();
        log.debug("Lock main context " + context);
        context.lock();
    }

    /**
     * Un worker pour les opération longues d'administration.
     *
     * @author Tony Chemit - dev@tchemit.fr
     * @since 1.4
     */
    public class CommandLineActionWorker extends ActionWorker<Void, String> {

        CommandLineActionWorker(String actionLabel, Runnable target) {
            super(actionLabel);
            setTarget(target);
        }

        public ObserveCLAction getAction() {
            return ObserveCLAction.this;
        }
    }
}
