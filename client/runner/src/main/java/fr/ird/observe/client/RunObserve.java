/*
 * #%L
 * ObServe Client :: Runner
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.client;

import fr.ird.observe.client.datasource.actions.AdminActionWorkerListener;
import fr.ird.observe.client.datasource.editor.api.RunObserveListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.application.ApplicationRunner;
import org.nuiton.jaxx.runtime.swing.application.event.ActionExecutorListener;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

/**
 * Le lanceur de l'application ObServe.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class RunObserve extends ApplicationRunner implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(RunObserve.class);

    static {
        //FIXME This does not work with Log4j 2.14.0
        System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");
        java.util.logging.LogManager.getLogManager().reset();
        java.util.logging.LogManager.getLogManager().getLogger("").setLevel(Level.SEVERE);
    }

    public static void main(String... args) {
        log.info(String.format("ObServe client launch at %s args: %s", new Date(), Arrays.toString(args)));
        new RunObserve(args).launch();
    }

    public static RunObserve getRunner() {
        return (RunObserve) ApplicationRunner.getRunner();
    }

    public RunObserve(String... args) {
        super(args);
        List<ActionExecutorListener> executorListeners = List.of(new CommandLineActionListener(), new AdminActionWorkerListener());
        setMainListener(new RunObserveListener(executorListeners));
    }

}
