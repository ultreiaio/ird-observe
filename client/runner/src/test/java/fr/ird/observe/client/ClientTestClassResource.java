package fr.ird.observe.client;

/*
 * #%L
 * ObServe Client :: Runner
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.client.configuration.ClientConfigApplicationComponent;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.test.DataSourcesForTestManager;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestClassResourceSupport;
import fr.ird.observe.test.TestConfig;
import fr.ird.observe.test.spi.DatabaseClassifier;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.boot.DefaultI18nBootLoader;
import io.ultreia.java4all.i18n.runtime.boot.UserI18nBootLoader;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.Description;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Locale;

/**
 * Created on 18/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ClientTestClassResource extends TestClassResourceSupport {

    private static final Logger log = LogManager.getLogger(ClientTestClassResource.class);

    private final DataSourcesForTestManager dataSourcesForTestManager;
    private ObserveSwingApplicationContext applicationContext;

    public ClientTestClassResource() {
        this(DatabaseClassifier.DEFAULT);
    }

    public ClientTestClassResource(DatabaseClassifier classifier) {
        super(classifier);
        dataSourcesForTestManager = new DataSourcesForTestManager();
    }

    public DataSourcesForTestManager getDataSourcesForTestManager() {
        return dataSourcesForTestManager;
    }

    public ObserveSwingApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public ObserveSwingDataSource createDataSourceConfiguration(Version dbVersion, String dbName, File targetPath, String login, char[] password) throws DataSourceCreateWithNoReferentialImportException, IOException, IncompatibleDataSourceCreateConfigurationException, DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        ObserveDataSourceConfigurationTopiaH2 sharedDatabaseConfiguration = dataSourcesForTestManager.createSharedDataSourceConfigurationH2(dbVersion, dbName, login, password);
        File sharedDatabaseFile = sharedDatabaseConfiguration.getDatabaseFile();
        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration;
        ObserveSwingDataSource observeSwingDataSource;
        boolean sharedDatabaseExist = sharedDatabaseFile.exists();
        if (!sharedDatabaseExist) {
            log.info(String.format("Create shared database: %s/%s to %s", dbVersion.toString(), dbName, sharedDatabaseFile));
            SqlScript script = dataSourcesForTestManager.getCache(dbVersion, dbName);
            observeSwingDataSource = ClientUIContextApplicationComponent.value().getDataSourcesManager().newDataSource(sharedDatabaseConfiguration);
            observeSwingDataSource.createFromDump(script);
        }
        if (targetPath == null) {
            dataSourceConfiguration = sharedDatabaseConfiguration;
            observeSwingDataSource = ClientUIContextApplicationComponent.value().getDataSourcesManager().newDataSource(sharedDatabaseConfiguration);
        } else {
            // Use a copy
            dataSourceConfiguration = dataSourcesForTestManager.createDataSourceConfigurationH2(targetPath, dbVersion, dbName, login, password);
            File databaseFileTarget = dataSourceConfiguration.getDatabaseFile();
            log.info(String.format("Copy database: %s/%s to %s", dbVersion.toString(), dbName, databaseFileTarget));
            Files.createDirectories(databaseFileTarget.toPath().getParent());
            Files.copy(sharedDatabaseFile.toPath(), databaseFileTarget.toPath());
            observeSwingDataSource = ClientUIContextApplicationComponent.value().getDataSourcesManager().newDataSource(sharedDatabaseConfiguration);
        }
        dataSourceConfiguration.setModelVersion(ObserveTestConfiguration.getModelVersion());
        if (!observeSwingDataSource.isOpen()) {
            observeSwingDataSource.open();
        }
        return observeSwingDataSource;
    }

    @Override
    protected void before(Description description) throws Throwable {
        super.before(description);
        if (ApplicationContext.isInit()) {
            ApplicationContext.get().close();
        }
        ClientConfig config = ClientConfig.forTest();
        config.setDataDirectory(getTestDirectory());
        config.initConfig();
        ClientConfig.initI18n(config);
        applicationContext = ObserveSwingApplicationContext.init(config, TestConfig.class);
        I18n.init(new UserI18nBootLoader(ClientConfigApplicationComponent.value().getI18nDirectory().toPath(), new DefaultI18nBootLoader(I18nConfiguration.createDefaultConfiguration())), Locale.FRANCE);
    }

    protected void after(Description description) throws IOException {
        super.after(description);
        getApplicationContext().close();
    }
}
