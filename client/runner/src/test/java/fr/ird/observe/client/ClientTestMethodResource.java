package fr.ird.observe.client;

/*
 * #%L
 * ObServe Client :: Runner
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestMethodResourceSupportWrite;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import org.junit.runner.Description;

import java.io.File;
import java.util.Objects;


/**
 * Created on 18/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ClientTestMethodResource extends TestMethodResourceSupportWrite<ClientTestClassResource> {

    private ObserveSwingDataSource dataSource;

    public ClientTestMethodResource(ClientTestClassResource clientTestClassResource) {
        super(clientTestClassResource);
    }

    public ObserveSwingDataSource getDataSource() {
        return dataSource;
    }

    @Override
    protected void before(Description description) throws Throwable {

        super.before(description);

        Objects.requireNonNull(getDbName(), "Pas de nom de base spécifié");
        Objects.requireNonNull(getDbVersion(), "Pas de version de base spécifié");
        Objects.requireNonNull(getLogin(), "Pas de login spécifié");
        Objects.requireNonNull(getPassword(), "Pas de password spécifié");

        CopyDatabaseConfiguration copyDatabaseConfiguration = ObserveTestConfiguration.getCopyDatabaseConfigurationAnnotation(testClassMethod, testClassResource.getClassifier());
        boolean useSharedDatabase = copyDatabaseConfiguration == null;

        File databasePath = useSharedDatabase ? null : getTestDirectory().toPath().resolve("localDb").toFile();

        dataSource = testClassResource.createDataSourceConfiguration(getDbVersion(), getDbName(), databasePath, getLogin(), getPassword());

    }

}
