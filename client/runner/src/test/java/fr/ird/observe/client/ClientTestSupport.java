package fr.ird.observe.client;

/*-
 * #%L
 * ObServe Client :: Runner
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import org.junit.ClassRule;
import org.junit.Rule;

/**
 * Created on 26/12/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@DatabaseVersionConfiguration(ObserveTestConfiguration.MODEL_VERSION)
@DatabaseLoginConfiguration()
@DatabasePasswordConfiguration(ObserveTestConfiguration.H2_PASSWORD)
public abstract class ClientTestSupport {

    @ClassRule
    public static final ClientTestClassResource TEST_CLASS_RESOURCE = new ClientTestClassResource();

    @Rule
    public final ClientTestMethodResource testMethodResource = new ClientTestMethodResource(TEST_CLASS_RESOURCE);

}
