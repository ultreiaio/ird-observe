package fr.ird.observe.client;

/*-
 * #%L
 * ObServe Client :: Runner
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.client.datasource.actions.validate.ValidateModel;
import fr.ird.observe.client.datasource.actions.validate.ValidateModelTemplate;
import fr.ird.observe.client.datasource.actions.validate.ValidationModelMode;
import fr.ird.observe.client.datasource.api.ObserveSwingDataSource;
import fr.ird.observe.dto.validation.DtoValidationContext;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.validation.api.request.DataValidationRequest;
import fr.ird.observe.validation.api.result.ValidationResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by tchemit on 14/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@DatabaseNameConfiguration(DatabaseName.data)
public class ObserveTextGeneratorTest extends ClientTestSupport implements WithClientUIContextApi {

    private static final Logger log = LogManager.getLogger(ObserveTextGeneratorTest.class);

    @Test
    public void getValidationReport() {

        ObserveSwingDataSource dataSource = testMethodResource.getDataSource();

        String llCommonTripId = ObserveFixtures.getLlCommonTripId();
        String psCommonTripId = ObserveFixtures.getPsCommonTripId();
        ValidationRequestConfiguration configuration = getClientConfig().toValidationRequestConfiguration();
        DataValidationRequest request = new DataValidationRequest();
        request.setDataIds(new LinkedHashSet<>(List.of(psCommonTripId/*, llCommonTripId*/)));
        request.setScopes(new LinkedHashSet<>(Arrays.asList(NuitonValidatorScope.ERROR, NuitonValidatorScope.WARNING)));
        request.setValidationContext(DtoValidationContext.UPDATE_VALIDATION_CONTEXT);
        configuration.setValidationSpeedEnable(true);
        configuration.setValidationSpeedMaxValue(30f);
        configuration.setValidationLengthWeightEnable(true);

        ValidationResult result = dataSource.getValidateService().validateData(configuration, request);

        ValidateModel model = new ValidateModel();
        model.setDecoratorService(getDecoratorService());
        model.setContextName(request.getValidationContext());
        model.addScope(NuitonValidatorScope.ERROR);
        model.addScope(NuitonValidatorScope.WARNING);
        model.setContextName(request.getValidationContext());
        model.setValidationSpeedMaxValue(configuration.getValidationSpeedMaxValue());
        model.setModelMode(ValidationModelMode.DATA);
        model.setValidationResult(result);
        String content = ValidateModelTemplate.generate(model);
        Assert.assertNotNull(content);
        log.debug(content);
    }


}
