package fr.ird.observe.client.datasource.editor.ll.data.observation.composition;

/*
 * #%L
 * ObServe Client :: Runner
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.client.ObserveSwingApplicationContext;
import fr.ird.observe.client.configuration.ClientConfig;
import fr.ird.observe.dto.data.ll.observation.BasketDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import fr.ird.observe.test.TestConfig;
import fr.ird.observe.test.ToolkitFixtures;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.config.ArgumentsParserException;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.boot.DefaultI18nBootLoader;
import io.ultreia.java4all.validation.bean.BeanValidatorMessage;
import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.api.SimpleNuitonValidationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created on 3/18/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.15
 */
public class SetDetailCompositionValidatorServiceTest {

    private static final Logger log = LogManager.getLogger(SetDetailCompositionValidatorServiceTest.class);

    protected SetDetailCompositionValidatorService service;

    @Before
    public void setUp() throws IOException, ArgumentsParserException {

        if (ApplicationContext.isInit()) {
            ApplicationContext.get().close();
        }
        File testDirectory = ToolkitFixtures.getTestBasedir(SetDetailCompositionValidatorServiceTest.class).toPath().getParent().resolve(SetDetailCompositionValidatorServiceTest.class.getName()).toFile();
        ClientConfig config = ClientConfig.forTest();
        config.setDataDirectory(testDirectory);
        config.initConfig();

        if (!Files.exists(config.getTemplatesDirectory().toPath())) {
            Files.createDirectories(config.getTemplatesDirectory().toPath());
        }

        ObserveSwingApplicationContext.init(config, TestConfig.class);

        NuitonValidatorProvider defaultProvider = NuitonValidatorProviders.newProvider(NuitonValidatorProviders.getDefaultFactoryName(), new SimpleNuitonValidationContext(Locale.FRANCE));
        SwingValidator<SectionDto> sectionValidator = new SwingValidator<>(defaultProvider, SectionDto.class, "update", NuitonValidatorScope.values());
        SwingValidator<BasketDto> basketValidator = new SwingValidator<>(defaultProvider, BasketDto.class, "update", NuitonValidatorScope.values());
        SwingValidator<BranchlineDto> branchlineValidator = new SwingValidator<>(defaultProvider, BranchlineDto.class, "update", NuitonValidatorScope.values());

        service = new SetDetailCompositionValidatorService(sectionValidator, basketValidator, branchlineValidator, Collections.emptyMap());
        I18n.init(new DefaultI18nBootLoader(I18nConfiguration.createDefaultConfiguration()), Locale.FRANCE);
    }

    @Test
    public void testValidateSections() {

        List<SectionDto> sections = new ArrayList<>();
        {
            SectionDto section = new SectionDto();
            section.setId("0");
            sections.add(section);
        }
        {
            SectionDto section = new SectionDto();
            section.setId("1");
            sections.add(section);
        }
        {
            // 2 sections, without settingIdentifier
            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 2);
        }
        {
            // 2 sections, with settingIdentifier
            sections.get(0).setSettingIdentifier(1);
            sections.get(1).setSettingIdentifier(2);

            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 0);
        }
        { // 2 sections with baskets without settingIdentifier
            {
                BasketDto basket = new BasketDto();
                basket.setId("00");
                sections.get(0).getBasket().add(basket);
            }
            {
                BasketDto basket = new BasketDto();
                basket.setId("01");
                sections.get(0).getBasket().add(basket);
            }
            {
                BasketDto basket = new BasketDto();
                basket.setId("10");
                sections.get(1).getBasket().add(basket);
            }
            {
                BasketDto basket = new BasketDto();
                basket.setId("11");
                sections.get(1).getBasket().add(basket);
            }
            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 4);
        }
        { // 2 sections with baskets with settingIdentifier

            Iterables.get(sections.get(0).getBasket(), 0).setSettingIdentifier(1);
            Iterables.get(sections.get(0).getBasket(), 1).setSettingIdentifier(2);
            Iterables.get(sections.get(1).getBasket(), 0).setSettingIdentifier(1);
            Iterables.get(sections.get(1).getBasket(), 1).setSettingIdentifier(2);

            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 0);
        }
        { // Section 1 - Basket 1 mistmatch floatline1Length with Section 1 - Basket 2

            Iterables.get(sections.get(0).getBasket(), 0).setFloatline1Length(1f);
            Iterables.get(sections.get(0).getBasket(), 0).setFloatline2Length(2f);
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline1Length(1f); // should be 2
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline2Length(6f);
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline1Length(6f);
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline2Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline1Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline2Length(12f);

            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 1);
        }
        { // OK d'ont check if last floatline has same length of first floatline for next section

            Iterables.get(sections.get(0).getBasket(), 0).setFloatline1Length(1f);
            Iterables.get(sections.get(0).getBasket(), 0).setFloatline2Length(2f);
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline1Length(2f);
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline2Length(6f);
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline1Length(10f); // Should be 6
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline2Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline1Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline2Length(12f);

            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 0);
        }
        { // Ok

            Iterables.get(sections.get(0).getBasket(), 0).setFloatline1Length(1f);
            Iterables.get(sections.get(0).getBasket(), 0).setFloatline2Length(2f);
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline1Length(2f);
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline2Length(10f);
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline1Length(10f);
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline2Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline1Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline2Length(12f);

            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 0);
        }
    }

    protected void assertMessages(List<SwingValidatorMessage> messages, int expectedNbMessages) {
        StringBuilder builder = new StringBuilder();
        for (BeanValidatorMessage<?> message : messages) {
            builder.append("\n").append(message.getScope()).append(" - ").append(message.getField()).append(" - ").append(message.getI18nError(message.getMessage()));
        }
        if (log.isInfoEnabled()) {
            log.info(builder.toString());
        }
        Assert.assertEquals("Should have found " + expectedNbMessages + " messages, but found " + messages.size(), expectedNbMessages, messages.size());
    }
}
