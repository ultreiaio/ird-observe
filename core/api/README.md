# Abstract

This module is the pom of the **Core API** part of the project: means everything that can be exposed by a 
service (so everything except persistence stuff).