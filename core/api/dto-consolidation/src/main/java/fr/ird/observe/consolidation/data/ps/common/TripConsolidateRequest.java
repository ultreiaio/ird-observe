package fr.ird.observe.consolidation.data.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.data.ps.logbook.SampleActivityDto;
import fr.ird.observe.dto.data.ps.logbook.SampleDto;
import io.ultreia.java4all.util.json.JsonAware;

/**
 * Request to consolidate a trip.
 * <p>
 * Created on 28/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TripConsolidateRequest implements JsonAware {
    /**
     * Id of trip.
     */
    private String tripId;
    /**
     * To throw exception if RTP not found.
     */
    private boolean failIfLengthWeightParameterNotFound;
    /**
     * To throw exception if RTT not found.
     */
    private boolean failIfLengthLengthParameterNotFound;
    /**
     * Species list id used to filter species for {@link SampleActivityDto#getWeightedWeight()}.
     */
    private String speciesListForLogbookSampleActivityWeightedWeight;
    /**
     * Species list id used to filter species for {@link SampleDto#getBigsWeight()}, {@link SampleDto#getSmallsWeight()},
     * {@link SampleDto#getTotalWeight()} .
     */
    private String speciesListForLogbookSampleWeights;

    public boolean isFailIfLengthWeightParameterNotFound() {
        return failIfLengthWeightParameterNotFound;
    }

    public void setFailIfLengthWeightParameterNotFound(boolean failIfLengthWeightParameterNotFound) {
        this.failIfLengthWeightParameterNotFound = failIfLengthWeightParameterNotFound;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public boolean isFailIfLengthLengthParameterNotFound() {
        return failIfLengthLengthParameterNotFound;
    }

    public void setFailIfLengthLengthParameterNotFound(boolean failIfLengthLengthParameterNotFound) {
        this.failIfLengthLengthParameterNotFound = failIfLengthLengthParameterNotFound;
    }

    public String getSpeciesListForLogbookSampleActivityWeightedWeight() {
        return speciesListForLogbookSampleActivityWeightedWeight;
    }

    public void setSpeciesListForLogbookSampleActivityWeightedWeight(String speciesListForLogbookSampleActivityWeightedWeight) {
        this.speciesListForLogbookSampleActivityWeightedWeight = speciesListForLogbookSampleActivityWeightedWeight;
    }

    public String getSpeciesListForLogbookSampleWeights() {
        return speciesListForLogbookSampleWeights;
    }

    public void setSpeciesListForLogbookSampleWeights(String speciesListForLogbookSampleWeights) {
        this.speciesListForLogbookSampleWeights = speciesListForLogbookSampleWeights;
    }
}
