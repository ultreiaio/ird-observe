package fr.ird.observe.consolidation.data.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.ToolkitIdModifications;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.Set;

/**
 * Consolidation result for a given trip (if no consolidation modification found then this object won't be created).
 * <p>
 * Created on 28/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TripConsolidateResult implements JsonAware {

    /**
     * Id of trip.
     */
    protected final String tripId;
    /**
     * Label of trip.
     */
    protected final String tripLabel;
    /**
     * Observation activities results.
     */
    protected final Set<fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult> activityObservationResults;
    /**
     * Logbook activities results.
     */
    protected final Set<fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateResult> activityLogbookResults;
    /**
     * Logbook sample results.
     */
    private final Set<fr.ird.observe.consolidation.data.ps.logbook.SampleConsolidateResult> logbookSampleResults;
    /**
     * Local market batches results.
     */
    protected final Set<ToolkitIdModifications> localmarketBatchResults;

    public TripConsolidateResult(String tripId,
                                 String tripLabel,
                                 Set<fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult> activityObservationResults,
                                 Set<fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateResult> activityLogbookResults,
                                 Set<fr.ird.observe.consolidation.data.ps.logbook.SampleConsolidateResult> logbookSampleResults,
                                 Set<ToolkitIdModifications> localmarketBatchResults) {
        this.tripId = tripId;
        this.tripLabel = tripLabel;
        this.activityObservationResults = activityObservationResults;
        this.activityLogbookResults = activityLogbookResults;
        this.logbookSampleResults = logbookSampleResults;
        this.localmarketBatchResults = localmarketBatchResults;
    }

    public String getTripId() {
        return tripId;
    }

    public String getTripLabel() {
        return tripLabel;
    }

    public Set<fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult> getActivityObservationResults() {
        return activityObservationResults;
    }

    public Set<fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateResult> getActivityLogbookResults() {
        return activityLogbookResults;
    }

    public Set<fr.ird.observe.consolidation.data.ps.logbook.SampleConsolidateResult> getLogbookSampleResults() {
        return logbookSampleResults;
    }

    public Set<ToolkitIdModifications> getLocalmarketBatchResults() {
        return localmarketBatchResults;
    }

    public boolean withModifications() {
        return activityObservationResults.stream().anyMatch(fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult::withModifications) ||
                activityLogbookResults.stream().anyMatch(fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateResult::withModifications) ||
                logbookSampleResults.stream().anyMatch(fr.ird.observe.consolidation.data.ps.logbook.SampleConsolidateResult::withModifications) ||
                localmarketBatchResults.stream().anyMatch(ToolkitIdModifications::withModifications);
    }

    public boolean withWarnings() {
        return activityObservationResults.stream().anyMatch(fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult::withWarnings) ||
                activityLogbookResults.stream().anyMatch(fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateResult::withWarnings) ||
                logbookSampleResults.stream().anyMatch(fr.ird.observe.consolidation.data.ps.logbook.SampleConsolidateResult::withWarnings) ||
                localmarketBatchResults.stream().anyMatch(ToolkitIdModifications::withWarnings);
    }
}
