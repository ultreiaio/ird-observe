package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.FloatingObjectAware;
import fr.ird.observe.dto.data.ps.FloatingObjectPartAware;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;

import java.util.List;

/**
 * Created on 11/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.28
 */
public interface CleanComputedValuesAction<F extends DataDto & FloatingObjectAware,
        P extends DataDto & FloatingObjectPartAware,
        R extends FloatingObjectConsolidateRequestSupport<F, P>,
        C extends FloatingObjectConsolidateContextSupport<F, P, R>> extends AtomicConsolidateAction<C, F> {

    final class WhenArriving<F extends DataDto & FloatingObjectAware,
            P extends DataDto & FloatingObjectPartAware,
            R extends FloatingObjectConsolidateRequestSupport<F, P>,
            C extends FloatingObjectConsolidateContextSupport<F, P, R>> implements CleanComputedValuesAction<F, P, R, C> {

        @Override
        public List<String> fieldNames() {
            return List.of(FloatingObjectDto.PROPERTY_COMPUTED_WHEN_ARRIVING_BIODEGRADABLE, FloatingObjectDto.PROPERTY_COMPUTED_WHEN_ARRIVING_NON_ENTANGLING, FloatingObjectDto.PROPERTY_COMPUTED_WHEN_ARRIVING_SIMPLIFIED_OBJECT_TYPE);
        }

        @Override
        public boolean test(C c, F f) {
            return !f.getObjectOperation().isWhenArriving();
        }

        @Override
        public void accept(C c, F f) {
            f.setComputedWhenArrivingBiodegradable(null);
            f.setComputedWhenArrivingNonEntangling(null);
            f.setComputedWhenArrivingSimplifiedObjectType(null);
        }
    }

    final class WhenLeaving<F extends DataDto & FloatingObjectAware,
            P extends DataDto & FloatingObjectPartAware,
            R extends FloatingObjectConsolidateRequestSupport<F, P>,
            C extends FloatingObjectConsolidateContextSupport<F, P, R>> implements CleanComputedValuesAction<F, P, R, C> {

        @Override
        public List<String> fieldNames() {
            return List.of(FloatingObjectDto.PROPERTY_COMPUTED_WHEN_LEAVING_BIODEGRADABLE, FloatingObjectDto.PROPERTY_COMPUTED_WHEN_LEAVING_NON_ENTANGLING, FloatingObjectDto.PROPERTY_COMPUTED_WHEN_LEAVING_SIMPLIFIED_OBJECT_TYPE);
        }

        @Override
        public boolean test(C c, F f) {
            return !f.getObjectOperation().isWhenLeaving();
        }

        @Override
        public void accept(C c, F f) {
            f.setComputedWhenLeavingBiodegradable(null);
            f.setComputedWhenLeavingNonEntangling(null);
            f.setComputedWhenLeavingSimplifiedObjectType(null);
        }
    }
}
