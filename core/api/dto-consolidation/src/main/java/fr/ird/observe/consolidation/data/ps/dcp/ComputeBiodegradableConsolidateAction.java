package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.DcpComputedValue;
import fr.ird.observe.dto.data.ps.FloatingObjectAware;
import fr.ird.observe.dto.data.ps.FloatingObjectPartAware;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;

import java.util.List;
import java.util.Set;

/**
 * Created on 24/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public interface ComputeBiodegradableConsolidateAction<F extends DataDto & FloatingObjectAware,
        P extends DataDto & FloatingObjectPartAware,
        R extends FloatingObjectConsolidateRequestSupport<F, P>,
        C extends FloatingObjectConsolidateContextSupport<F, P, R>> extends FloatingObjectConsolidateAction<DcpComputedValue, F, P, R, C> {

    @Override
    default DcpComputedValue computeValue(C context, R request, Set<P> parts) {
        if (parts.isEmpty()) {
            return null;
        }
        DcpComputedValue result = DcpComputedValue.UNKNOWN;
        for (P part : parts) {
            Boolean biodegradable = part.getObjectMaterial().getBiodegradable();
            if (biodegradable == null) {
                continue;
            }
            if (!biodegradable) {
                result = DcpComputedValue.FALSE;
                break;
            }
            result = DcpComputedValue.TRUE;
        }
        return result;
    }

    final class WhenArriving<F extends DataDto & FloatingObjectAware,
            P extends DataDto & FloatingObjectPartAware,
            R extends FloatingObjectConsolidateRequestSupport<F, P>,
            C extends FloatingObjectConsolidateContextSupport<F, P, R>> implements ComputeBiodegradableConsolidateAction<F, P, R, C>, WhenArrivingConsolidateAction<DcpComputedValue, F, P, R, C> {

        @Override
        public List<String> fieldNames() {
            return List.of(FloatingObjectDto.PROPERTY_COMPUTED_WHEN_ARRIVING_BIODEGRADABLE);
        }

        @Override
        public void setValue(F floatingObject, DcpComputedValue computedValue) {
            floatingObject.setComputedWhenArrivingBiodegradable(computedValue);
        }
    }

    final class WhenLeaving<F extends DataDto & FloatingObjectAware,
            P extends DataDto & FloatingObjectPartAware,
            R extends FloatingObjectConsolidateRequestSupport<F, P>,
            C extends FloatingObjectConsolidateContextSupport<F, P, R>> implements ComputeBiodegradableConsolidateAction<F, P, R, C>, WhenLeavingConsolidateAction<DcpComputedValue, F, P, R, C> {

        @Override
        public List<String> fieldNames() {
            return List.of(FloatingObjectDto.PROPERTY_COMPUTED_WHEN_LEAVING_BIODEGRADABLE);
        }

        @Override
        public void setValue(F floatingObject, DcpComputedValue computedValue) {
            floatingObject.setComputedWhenLeavingBiodegradable(computedValue);
        }
    }
}
