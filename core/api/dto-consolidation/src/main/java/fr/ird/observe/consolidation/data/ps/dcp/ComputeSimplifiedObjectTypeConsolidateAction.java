package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.FloatingObjectAware;
import fr.ird.observe.dto.data.ps.FloatingObjectPartAware;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 24/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public interface ComputeSimplifiedObjectTypeConsolidateAction<F extends DataDto & FloatingObjectAware,
        P extends DataDto & FloatingObjectPartAware,
        R extends FloatingObjectConsolidateRequestSupport<F, P>,
        C extends FloatingObjectConsolidateContextSupport<F, P, R>> extends FloatingObjectConsolidateAction<String, F, P, R, C> {

    @Override
    default String computeValue(C context, R request, Set<P> parts) {
        if (parts.isEmpty()) {
            return null;
        }
        Set<String> objectMaterialIds = parts.stream().map(p -> p.getObjectMaterial().getId()).collect(Collectors.toSet());
        return context.getSimplifiedObjectTypeManager().getStandardCode(objectMaterialIds);
    }

    final class WhenArriving<F extends DataDto & FloatingObjectAware,
            P extends DataDto & FloatingObjectPartAware,
            R extends FloatingObjectConsolidateRequestSupport<F, P>,
            C extends FloatingObjectConsolidateContextSupport<F, P, R>> implements ComputeSimplifiedObjectTypeConsolidateAction<F, P, R, C>, WhenArrivingConsolidateAction<String, F, P, R, C> {

        @Override
        public List<String> fieldNames() {
            return List.of(FloatingObjectDto.PROPERTY_COMPUTED_WHEN_ARRIVING_SIMPLIFIED_OBJECT_TYPE);
        }

        @Override
        public void setValue(F floatingObject, String computedValue) {
            floatingObject.setComputedWhenArrivingSimplifiedObjectType(computedValue);
        }
    }

    final class WhenLeaving<F extends DataDto & FloatingObjectAware,
            P extends DataDto & FloatingObjectPartAware,
            R extends FloatingObjectConsolidateRequestSupport<F, P>,
            C extends FloatingObjectConsolidateContextSupport<F, P, R>> implements ComputeSimplifiedObjectTypeConsolidateAction<F, P, R, C>, WhenLeavingConsolidateAction<String, F, P, R, C> {

        @Override
        public List<String> fieldNames() {
            return List.of(FloatingObjectDto.PROPERTY_COMPUTED_WHEN_LEAVING_SIMPLIFIED_OBJECT_TYPE);
        }

        @Override
        public void setValue(F floatingObject, String computedValue) {
            floatingObject.setComputedWhenLeavingSimplifiedObjectType(computedValue);
        }
    }
}
