package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.FloatingObjectAware;
import fr.ird.observe.dto.data.ps.FloatingObjectPartAware;

import java.util.Set;

/**
 * Created on 09/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.27
 */
public interface FloatingObjectConsolidateAction<
        V,
        F extends DataDto & FloatingObjectAware,
        P extends DataDto & FloatingObjectPartAware,
        R extends FloatingObjectConsolidateRequestSupport<F, P>,
        C extends FloatingObjectConsolidateContextSupport<F, P, R>> extends AtomicConsolidateAction<C, F> {

    Set<P> getParts(R request);

    void setValue(F floatingObject, V computedValue);

    V computeValue(C context, R request, Set<P> parts);

    @Override
    default void accept(C context, F floatingObject) {
        R request = context.getRequest();
        Set<P> parts = getParts(request);
        if (parts.isEmpty()) {
            return;
        }
        V computedValue = computeValue(context, request, parts);
        setValue(floatingObject, computedValue);
    }

    interface WhenArrivingConsolidateAction<
            V,
            F extends DataDto & FloatingObjectAware,
            P extends DataDto & FloatingObjectPartAware,
            R extends FloatingObjectConsolidateRequestSupport<F, P>,
            C extends FloatingObjectConsolidateContextSupport<F, P, R>> extends FloatingObjectConsolidateAction<V, F, P, R, C> {

        @Override
        default boolean test(C context, F floatingObject) {
            return floatingObject.getObjectOperation().isWhenArriving();
        }

        @Override
        default Set<P> getParts(R request) {
            return request.getWhenArriving();
        }
    }

    interface WhenLeavingConsolidateAction<
            V,
            F extends DataDto & FloatingObjectAware,
            P extends DataDto & FloatingObjectPartAware,
            R extends FloatingObjectConsolidateRequestSupport<F, P>,
            C extends FloatingObjectConsolidateContextSupport<F, P, R>> extends FloatingObjectConsolidateAction<V, F, P, R, C> {

        @Override
        default boolean test(C context, F floatingObject) {
            return floatingObject.getObjectOperation().isWhenLeaving();
        }

        @Override
        default Set<P> getParts(R request) {
            return request.getWhenLeaving();
        }
    }
}
