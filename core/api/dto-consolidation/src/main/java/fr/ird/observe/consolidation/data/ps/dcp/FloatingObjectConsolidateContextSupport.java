package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.ConsolidateContext;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.FloatingObjectAware;
import fr.ird.observe.dto.data.ps.FloatingObjectPartAware;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;

import java.util.Objects;
import java.util.Optional;

/**
 * Created on 21/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0
 */
public abstract class FloatingObjectConsolidateContextSupport<
        F extends DataDto & FloatingObjectAware,
        P extends DataDto & FloatingObjectPartAware,
        R extends FloatingObjectConsolidateRequestSupport<F, P>
        > implements ConsolidateContext<F> {

    private final SimplifiedObjectTypeManager simplifiedObjectTypeManager;
    private final JavaBeanMonitor monitor;
    private final Decorator decorator;
    private final Class<F> dataType;

    private F floatingObject;

    private R request;

    public FloatingObjectConsolidateContextSupport(SimplifiedObjectTypeManager simplifiedObjectTypeManager, JavaBeanMonitor monitor, Class<F> dataType, Decorator decorator) {
        this.simplifiedObjectTypeManager = simplifiedObjectTypeManager;
        this.monitor = Objects.requireNonNull(monitor);
        this.decorator = Objects.requireNonNull(decorator);
        this.dataType = dataType;
    }

    public void watch(R request) {
        this.request = Objects.requireNonNull(request);
        this.floatingObject = request.getFloatingObject();
        monitor.setBean(floatingObject);
    }

    public Optional<ToolkitIdModifications> build() {
        if (monitor.wasModified()) {
            floatingObject.registerDecorator(decorator);
            Optional<ToolkitIdModifications> result = monitor.toModifications(modifications -> new ToolkitIdModifications(floatingObject, modifications, null));
            result.ifPresent(m -> m.reset(floatingObject));
            return result;
        }
        return Optional.empty();
    }

    public void clear() {
        this.request = null;
        this.floatingObject = null;
        monitor.setBean(null);
    }

    @Override
    public JavaBeanMonitor monitor() {
        return monitor;
    }

    @Override
    public Class<F> dataType() {
        return dataType;
    }

    public SimplifiedObjectTypeManager getSimplifiedObjectTypeManager() {
        return simplifiedObjectTypeManager;
    }

    public R getRequest() {
        return request;
    }
}
