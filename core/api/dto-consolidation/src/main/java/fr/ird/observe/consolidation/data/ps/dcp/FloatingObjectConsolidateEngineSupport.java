package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.FloatingObjectAware;
import fr.ird.observe.dto.data.ps.FloatingObjectPartAware;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;

import java.util.Optional;

/**
 * Created on 24/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public abstract class FloatingObjectConsolidateEngineSupport<
        F extends DataDto & FloatingObjectAware,
        P extends DataDto & FloatingObjectPartAware,
        R extends FloatingObjectConsolidateRequestSupport<F, P>,
        C extends FloatingObjectConsolidateContextSupport<F, P, R>> {

    private final SimplifiedObjectTypeManager simplifiedObjectTypeManager;
    private final JavaBeanMonitor monitor;
    private final Decorator decorator;
    private final C context;

    protected FloatingObjectConsolidateEngineSupport(SimplifiedObjectTypeManager simplifiedObjectTypeManager, Class<F> dataType, DecoratorService decoratorService) {
        this.simplifiedObjectTypeManager = simplifiedObjectTypeManager;
        this.monitor = new JavaBeanMonitor(
                FloatingObjectDto.PROPERTY_COMPUTED_WHEN_ARRIVING_BIODEGRADABLE,
                FloatingObjectDto.PROPERTY_COMPUTED_WHEN_ARRIVING_NON_ENTANGLING,
                FloatingObjectDto.PROPERTY_COMPUTED_WHEN_ARRIVING_SIMPLIFIED_OBJECT_TYPE,
                FloatingObjectDto.PROPERTY_COMPUTED_WHEN_LEAVING_BIODEGRADABLE,
                FloatingObjectDto.PROPERTY_COMPUTED_WHEN_LEAVING_NON_ENTANGLING,
                FloatingObjectDto.PROPERTY_COMPUTED_WHEN_LEAVING_SIMPLIFIED_OBJECT_TYPE
        );
        this.decorator = decoratorService.getDecoratorByType(dataType);
        this.context = createContext();
    }

    protected abstract C createContext();

    public final Optional<ToolkitIdModifications> consolidate(R request) {

        F floatingObject = request.getFloatingObject();

        context.watch(request);

        try {
            cleanComputedValuesWhenArrivingAction().execute(context, floatingObject);
            if (floatingObject.getObjectOperation().isWhenArriving()) {
                computeSimplifiedObjectTypeWhenArrivingAction().execute(context, floatingObject);
                computeNonEntanglingWhenArrivingAction().execute(context, floatingObject);
                computeBiodegradableWhenArrivingAction().execute(context, floatingObject);
            }
            cleanComputedValuesWhenLeavingAction().execute(context, floatingObject);
            if (floatingObject.getObjectOperation().isWhenLeaving()) {
                computeSimplifiedObjectTypeWhenLeavingAction().execute(context, floatingObject);
                computeNonEntanglingWhenLeavingAction().execute(context, floatingObject);
                computeBiodegradableWhenLeavingAction().execute(context, floatingObject);
            }
            return context.build();
        } finally {
            context.clear();
        }
    }

    protected abstract AtomicConsolidateAction<C, F> computeSimplifiedObjectTypeWhenArrivingAction();

    protected abstract AtomicConsolidateAction<C, F> computeSimplifiedObjectTypeWhenLeavingAction();

    protected abstract AtomicConsolidateAction<C, F> computeBiodegradableWhenArrivingAction();

    protected abstract AtomicConsolidateAction<C, F> computeBiodegradableWhenLeavingAction();

    protected abstract AtomicConsolidateAction<C, F> computeNonEntanglingWhenArrivingAction();

    protected abstract AtomicConsolidateAction<C, F> computeNonEntanglingWhenLeavingAction();

    protected abstract AtomicConsolidateAction<C, F> cleanComputedValuesWhenArrivingAction();

    protected abstract AtomicConsolidateAction<C, F> cleanComputedValuesWhenLeavingAction();

    public SimplifiedObjectTypeManager getSimplifiedObjectTypeManager() {
        return simplifiedObjectTypeManager;
    }

    public JavaBeanMonitor getMonitor() {
        return monitor;
    }

    public Decorator getDecorator() {
        return decorator;
    }
}
