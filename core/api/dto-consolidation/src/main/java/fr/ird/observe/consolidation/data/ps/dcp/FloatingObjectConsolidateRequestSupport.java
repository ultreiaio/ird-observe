package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.FloatingObjectAware;
import fr.ird.observe.dto.data.ps.FloatingObjectPartAware;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 24/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public abstract class FloatingObjectConsolidateRequestSupport<F extends DataDto & FloatingObjectAware, P extends DataDto & FloatingObjectPartAware> implements JsonAware {

    private final F floatingObject;
    private final Set<P> whenArriving;
    private final Set<P> whenLeaving;
    private final boolean needClean;

    public FloatingObjectConsolidateRequestSupport(F floatingObject, Set<P> floatingObjectParts) {
        this.floatingObject = Objects.requireNonNull(floatingObject);
        Set<P> whenArrivingBuilder = new LinkedHashSet<>();
        Set<P> whenLeavingBuilder = new LinkedHashSet<>();
        boolean whenArriving = floatingObject.getObjectOperation().isWhenArriving();
        boolean whenLeaving = floatingObject.getObjectOperation().isWhenLeaving();
        boolean needClean = false;
        for (P floatingObjectPart : floatingObjectParts) {
            if (floatingObjectPart.getWhenArriving() != null) {
                if (whenArriving) {
                    whenArrivingBuilder.add(floatingObjectPart);
                } else {
                    needClean = true;
                }
            }
            if (floatingObjectPart.getWhenLeaving() != null) {
                if (whenLeaving) {
                    whenLeavingBuilder.add(floatingObjectPart);
                } else {
                    needClean = true;
                }
            }
        }
        this.needClean = needClean;
        this.whenArriving = whenArrivingBuilder;
        this.whenLeaving = whenLeavingBuilder;
    }

    public F getFloatingObject() {
        return floatingObject;
    }

    public Set<P> getWhenArriving() {
        return whenArriving;
    }

    public Set<P> getWhenLeaving() {
        return whenLeaving;
    }

    public boolean isNeedClean() {
        return needClean;
    }
}
