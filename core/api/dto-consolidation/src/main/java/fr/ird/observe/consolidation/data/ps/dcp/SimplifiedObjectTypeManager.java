package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Created by tchemit on 02/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.0
 */
public class SimplifiedObjectTypeManager implements ObserveDto {

    private static final Logger log = LogManager.getLogger(SimplifiedObjectTypeManager.class);

    // For each object material, get his simplified object type node
    private final Map<String, SimplifiedObjectTypeNode> mapping;
    private final Map<Set<SimplifiedObjectTypeNode>, String> specializedMapping;

    public SimplifiedObjectTypeManager(Map<String, SimplifiedObjectTypeNode> mapping, SimplifiedObjectTypeSpecializedRules specializedRules) {
        this.mapping = mapping;
        this.specializedMapping = specializedRules.toNode(mapping);
    }

    public SimplifiedObjectTypeNode getNodeForStandardCode(String standardCode) {
        return mapping.values().stream().filter(n -> Objects.equals(n.getStandardCode(), standardCode)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public String getStandardCode(Set<String> objectMaterialIds) {
        Set<SimplifiedObjectTypeNode> availableNodes = new LinkedHashSet<>();
        for (String objectMaterialId : objectMaterialIds) {
            SimplifiedObjectTypeNode node = mapping.get(objectMaterialId);
            while (node != null && Objects.equals("", node.getStandardCode())) {
                log.warn(String.format("For id: %s, found a node with empty standard code: %s", objectMaterialId, node));
                node = node.getParent();
            }
            availableNodes.add(Objects.requireNonNull(node, "Can't find a node with a none empty standard code for id: " + objectMaterialId));
        }

        List<SimplifiedObjectTypeNode> mainNodes = new LinkedList<>(availableNodes);
        // get highest level first
        mainNodes.sort(Comparator.comparing(SimplifiedObjectTypeNode::getLevel).reversed());

        // remove from available nodes, all nodes in the path of other ones
        for (SimplifiedObjectTypeNode availableNode : availableNodes) {
            if (mainNodes.contains(availableNode)) {
                mainNodes.removeAll(availableNode.getPathsToRoot(false));
            }
        }

        // try to get a specialized match
        Optional<String> optionalSpecializedStandardCode = specializedMapping.entrySet().stream().filter(e -> e.getKey().size() == mainNodes.size() && mainNodes.containsAll(e.getKey())).findFirst().map(Map.Entry::getValue);
        if (optionalSpecializedStandardCode.isPresent()) {
            return optionalSpecializedStandardCode.get();
        }

        // now each node is on a different path, get the shared common ancestor
        Iterator<SimplifiedObjectTypeNode> iterator = mainNodes.iterator();
        SimplifiedObjectTypeNode result = iterator.next();
        while (iterator.hasNext()) {
            result = result.getSharedAncestor(iterator.next());
        }
        Objects.requireNonNull(result);
        return result.getStandardCode();
    }

}
