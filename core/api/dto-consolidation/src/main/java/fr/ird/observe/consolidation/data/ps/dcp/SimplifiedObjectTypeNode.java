package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by tchemit on 02/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SimplifiedObjectTypeNode {

    private final int level;
    private final String id;
    private final String standardCode;
    private final SimplifiedObjectTypeNode parent;

    public SimplifiedObjectTypeNode(String id, String standardCode, SimplifiedObjectTypeNode parent) {
        this.id = id;
        this.standardCode = standardCode;
        this.parent = parent;
        this.level = (parent == null ? 0 : 1 + parent.getLevel());
    }

    public String getId() {
        return id;
    }

    public String getStandardCode() {
        return standardCode;
    }

    public SimplifiedObjectTypeNode getParent() {
        return parent;
    }

    public int getLevel() {
        return level;
    }

    public SimplifiedObjectTypeNode getSharedAncestor(SimplifiedObjectTypeNode other) {
        List<SimplifiedObjectTypeNode> pathsToRoot = getPathsToRoot(true);
        List<SimplifiedObjectTypeNode> otherPathsToRoot = other.getPathsToRoot(true);
        List<SimplifiedObjectTypeNode> result = new LinkedList<>(pathsToRoot);
        result.retainAll(otherPathsToRoot);

        // get the most precise on common paths (if it exists)
        return result.isEmpty() ? pathsToRoot.get(pathsToRoot.size() - 1) : result.get(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimplifiedObjectTypeNode that = (SimplifiedObjectTypeNode) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public List<SimplifiedObjectTypeNode> getPathsToRoot(boolean includeMe) {
        List<SimplifiedObjectTypeNode> result = new LinkedList<>();
        if (includeMe) {
            result.add(this);
        }
        if (parent != null) {
            parent.getPathsToRoot0(result);
        }
        return result;
    }

    @Override
    public String toString() {
//        return String.format("SimplifiedObjectTypeNode{level=%d, id='%s', standardCode='%s'}", level, id, standardCode);
        return String.format("SimplifiedObjectTypeNode{level=%d, standardCode='%s'}", level, standardCode);
    }

    private void getPathsToRoot0(List<SimplifiedObjectTypeNode> result) {
        result.add(this);
        if (parent != null) {
            parent.getPathsToRoot0(result);
        }
    }
}
