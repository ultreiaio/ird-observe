package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.SortedProperties;
import io.ultreia.java4all.util.json.JsonAware;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.2.1
 */
public class SimplifiedObjectTypeSpecializedRules implements JsonAware {

    private static final Logger log = LogManager.getLogger(SimplifiedObjectTypeSpecializedRules.class);
    private final Map<Set<String>, String> rules;

    public static SimplifiedObjectTypeSpecializedRules of(File definition) {
        try {
            return of(definition.toURI().toURL());
        } catch (IOException e) {
            throw new RuntimeException("Can't load", e);
        }
    }

    public static SimplifiedObjectTypeSpecializedRules of(URL definition) {
        try {
            return new SimplifiedObjectTypeSpecializedRules(definition);
        } catch (IOException e) {
            throw new RuntimeException("Can't load", e);
        }
    }

    protected SimplifiedObjectTypeSpecializedRules(URL definition) throws IOException {
        SortedProperties properties = new SortedProperties();
        this.rules = new LinkedHashMap<>();
        try (InputStream in = Objects.requireNonNull(definition).openStream()) {
            properties.load(in);
        }
        for (String ruleKey : properties.stringPropertyNames()) {
            Set<String> ruleEntry = new LinkedHashSet<>(Arrays.asList(ruleKey.split("\\s*,\\s*")));
            String ruleValue = properties.getProperty(ruleKey);
            rules.put(ruleEntry, ruleValue.trim());
        }
    }

    public Map<Set<String>, String> getRules() {
        return rules;
    }

    public Map<Set<SimplifiedObjectTypeNode>, String> toNode(Map<String, SimplifiedObjectTypeNode> mapping) {
        Map<String, SimplifiedObjectTypeNode> standardCodeMapping = new TreeMap<>();
        mapping.values().forEach(s -> standardCodeMapping.put(s.getStandardCode(), s));
        Map<SimplifiedObjectTypeNode, Set<SimplifiedObjectTypeNode>> dependencies = new LinkedHashMap<>();
        for (SimplifiedObjectTypeNode node : mapping.values()) {
            if ("".equals(node.getStandardCode())) {
                continue;
            }
            for (SimplifiedObjectTypeNode parent : node.getPathsToRoot(false)) {
                Set<SimplifiedObjectTypeNode> dependenciesForParent = dependencies.computeIfAbsent(parent, e -> new LinkedHashSet<>());
                dependenciesForParent.add(node);
            }
        }

        Map<Set<SimplifiedObjectTypeNode>, String> specializedMappingBuilder = new LinkedHashMap<>();
        for (Map.Entry<Set<String>, String> entry : rules.entrySet()) {
            Set<SimplifiedObjectTypeNode> tuple = new LinkedHashSet<>();
            for (String s : entry.getKey()) {
                SimplifiedObjectTypeNode simplifiedObjectTypeNode = Objects.requireNonNull(standardCodeMapping.get(s), String.format("can't find simplified object type node form standard code: %s", s));
                tuple.add(simplifiedObjectTypeNode);
            }
            String standardCode = entry.getValue();
            int currentSize = specializedMappingBuilder.size();
            addInheritance(specializedMappingBuilder, standardCodeMapping, tuple, standardCode, dependencies);
            int newSize = specializedMappingBuilder.size();
            log.debug(String.format("Add %d inheritance rules for main rule: %s", newSize - currentSize, tuple));

        }
        log.info(String.format("Found %d rules (from %d main rules).", specializedMappingBuilder.size(), rules.size()));
        return specializedMappingBuilder;
    }

    private void addInheritance(Map<Set<SimplifiedObjectTypeNode>, String> specializedMappingBuilder, Map<String, SimplifiedObjectTypeNode> standardCodeMapping, Set<SimplifiedObjectTypeNode> tuple, String standardCode, Map<SimplifiedObjectTypeNode, Set<SimplifiedObjectTypeNode>> dependencies) {
        log.debug(String.format("Add inheritance for standard code: %s on tuple: %s", standardCode, tuple));
        int size = tuple.size();
        @SuppressWarnings("unchecked") List<SimplifiedObjectTypeNode>[] dependenciesTuples = (List<SimplifiedObjectTypeNode>[]) Array.newInstance(List.class, size);
        int index = 0;
        for (SimplifiedObjectTypeNode simplifiedObjectTypeNode : tuple) {
            List<SimplifiedObjectTypeNode> build = new LinkedList<>();
            build.add(simplifiedObjectTypeNode);
            build.addAll(dependencies.getOrDefault(simplifiedObjectTypeNode, new LinkedHashSet<>()));
            dependenciesTuples[index++] = build;
        }
        int[] sizeTuples = new int[size];
        index = 0;
        long count = 1;
        for (List<SimplifiedObjectTypeNode> dependenciesTuple : dependenciesTuples) {
            count *= (sizeTuples[index++] = dependenciesTuple.size());
        }
        int[] indexTuples = new int[size];
        // this will be incremented at first next loop
        Arrays.fill(indexTuples, 0);
        indexTuples[0] = -1;
        for (long currentIndex = 0; currentIndex < count; currentIndex++) {
            promote(0, size, indexTuples, sizeTuples);

            Set<SimplifiedObjectTypeNode> currentTuple = new LinkedHashSet<>();
            for (int i = 0; i < size; i++) {
                int indexTuple = indexTuples[i];
                currentTuple.add(dependenciesTuples[i].get(indexTuple));
            }
            boolean keepRule = true;
            if (specializedMappingBuilder.containsKey(currentTuple)) {
                keepRule = false;
                String existingStandardCode = specializedMappingBuilder.get(currentTuple);
                if (!Objects.equals(standardCode, existingStandardCode)) {
                    SimplifiedObjectTypeNode thisNode = standardCodeMapping.get(standardCode);
                    SimplifiedObjectTypeNode otherNode = standardCodeMapping.get(existingStandardCode);
                    if (thisNode.getLevel() < otherNode.getLevel()) {
                        log.trace(String.format("Mismatch standard code (found: %s, wants to set: %s) - reject it since less specialized for tuple: %s", existingStandardCode, standardCode, currentTuple));
                    } else {
                        keepRule = true;
                        log.trace(String.format("Mismatch standard code (found: %s, wants to set: %s) - accept it more specialized for tuple: %s", existingStandardCode, standardCode, currentTuple));
                    }
                }
            }
            if (keepRule) {
                log.debug(String.format("[%d/%d] Set standard code: %s for tuple: %s", currentIndex + 1, count, standardCode, Arrays.toString(indexTuples)));
                specializedMappingBuilder.put(currentTuple, standardCode);
            } else {
                log.debug(String.format("[%d/%d] Reject set standard code: %s for tuple: %s", currentIndex + 1, count, standardCode, Arrays.toString(indexTuples)));
            }
        }

    }

    private void promote(int i, int size, int[] indexTuples, int[] sizeTuples) {
        indexTuples[i] += 1;
        if (indexTuples[i] == sizeTuples[i]) {
            // promote level i
            indexTuples[i] = 0;
            if (i + 1 < size) {
                promote(i + 1, size, indexTuples, sizeTuples);
            }
        }
    }
}
