package fr.ird.observe.consolidation.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.localmarket.BatchDto;
import fr.ird.observe.dto.data.ps.localmarket.BatchWeightComputedValueSource;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ps.localmarket.BatchWeightTypeReference;
import io.ultreia.java4all.lang.Numbers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created on 24/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public enum BatchConsolidateActions implements AtomicConsolidateAction<BatchConsolidateContext, BatchDto> {

    ComputeWeightFromSpeciesMeanWeight() {
        @Override
        public boolean test(BatchConsolidateContext context, BatchDto dto) {
            if (dto.getCount() == null) {
                // this method requires count
                return false;
            }
            BatchWeightTypeReference batchWeightType = dto.getPackaging().getBatchWeightType();
            return batchWeightType != null && ProtectedIdsPs.PS_LOCAL_MARKET_BATCH_WEIGHT_TYPE_SINGLE_SPECIES_INDIVIDUAL.equals(batchWeightType.getId());
        }

        @Override
        public void accept(BatchConsolidateContext context, BatchDto dto) {

            // espèce unité
            // P = poids moyen de l'espèce * batch.count / 1000
            // ocean = océan de débarquement de la marée = ps_common.trip.ocean
            //lengthweightparameter.startdate <= trip.enddate <= lengthweightparameter.enddate
            //sex = 0 - indéterminé (fr.ird.referential.common.Sex#1239832686121#0.0)
            //sizemeasuretype = type de mesure par défaut de l'espèce = common.species.sizemeasuretype
            // Si aucun poids moyen n'est trouvé P = NULL.

            SpeciesReference species = dto.getSpecies();
            String sizeMeasureTypeId = species.getSizeMeasureTypeId();
            BatchConsolidateRequest request = context.getRequest();
            String oceanId = request.getOceanId();
            Date date = request.getDate();
            boolean failIfLengthWeightParameterNotFound = request.isFailIfLengthWeightParameterNotFound();
            Optional<Float> optionalMeanWeight = context.getRtpMeanWeightFinder().get(species.getId(),
                                                                                      "fr.ird.referential.common.Sex#1239832686121#0.0",
                                                                                      oceanId,
                                                                                      sizeMeasureTypeId,
                                                                                      date,
                                                                                      failIfLengthWeightParameterNotFound,
                                                                                      context::registerLengthWeightParameterNotFound);
            if (optionalMeanWeight.isPresent()) {
                Float meanWeight = optionalMeanWeight.get();
                dto.setWeight(Numbers.roundFourDigits(meanWeight * dto.getCount() / 1000));
                dto.setWeightComputedSource(BatchWeightComputedValueSource.fromSpeciesMeanWeight);
            } else {
                dto.setWeight(null);
                dto.setWeightComputedSource(null);
            }
        }
    },
    ComputeWeightFromPackagingMeanWeight() {
        @Override
        public boolean test(BatchConsolidateContext context, BatchDto dto) {
            if (dto.getCount() == null) {
                // this method requires count
                return false;
            }
            BatchWeightTypeReference batchWeightType = dto.getPackaging().getBatchWeightType();
            return batchWeightType != null && ProtectedIdsPs.PS_LOCAL_MARKET_BATCH_WEIGHT_TYPE_PACKAGING_NOT_WEIGHTED.equals(batchWeightType.getId());
        }

        @Override
        public void accept(BatchConsolidateContext context, BatchDto dto) {
            //  Paquet/Conditionnement non pesé
            //  P = batch.packaging.meanWeight * batch.count (ou null si pas de packaging.meanWeight)
            Float meanWeight = dto.getPackaging().getMeanWeight();
            if (meanWeight == null) {
                dto.setWeight(null);
                dto.setWeightComputedSource(null);
            } else {
                dto.setWeight(Numbers.roundFourDigits(dto.getCount() * meanWeight));
                dto.setWeightComputedSource(BatchWeightComputedValueSource.fromPackagingMeanWeight);
            }
        }
    },
    ResetWeightValues() {
        @Override
        public boolean test(BatchConsolidateContext context, BatchDto dto) {
            return true;
        }

        @Override
        public void accept(BatchConsolidateContext context, BatchDto dto) {
            dto.setWeight(null);
            dto.setWeightComputedSource(null);
        }
    };

    @Override
    public List<String> fieldNames() {
        return List.of(BatchDto.PROPERTY_WEIGHT, BatchDto.PROPERTY_WEIGHT_COMPUTED_SOURCE);
    }

    @Override
    public String toString() {
        return "ps.localmarket." + BatchConsolidateActions.class.getSimpleName() + "." + name();
    }
}
