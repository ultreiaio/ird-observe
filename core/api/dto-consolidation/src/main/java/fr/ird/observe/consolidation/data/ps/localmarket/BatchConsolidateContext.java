package fr.ird.observe.consolidation.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.ConsolidateContext;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.ps.localmarket.BatchDto;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 21/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class BatchConsolidateContext implements ConsolidateContext<BatchDto> {
    private static final Logger log = LogManager.getLogger(BatchConsolidateContext.class);

    private final GetOptionalRtpMeanWeight rtpMeanWeightFinder;
    private final JavaBeanMonitor monitor;
    private final Decorator decorator;
    private BatchConsolidateRequest request;
    private BatchDto batch;
    private String lengthWeightParameterNotFound;

    public BatchConsolidateContext(GetOptionalRtpMeanWeight rtpMeanWeightFinder, JavaBeanMonitor monitor, Decorator decorator) {
        this.rtpMeanWeightFinder = rtpMeanWeightFinder;
        this.monitor = Objects.requireNonNull(monitor);
        this.decorator = decorator;
    }

    public void watch(BatchConsolidateRequest request) {
        this.request = Objects.requireNonNull(request);
        this.batch = Objects.requireNonNull(request.getBatch());
        monitor.setBean(batch);
        batch.setWeight(null);
        batch.setWeightComputedSource(null);
    }

    public Optional<ToolkitIdModifications> build() {
        if (monitor.wasModified()) {
            batch.registerDecorator(decorator);
            Optional<ToolkitIdModifications> result = monitor.toModifications(modifications -> new ToolkitIdModifications(batch, modifications, lengthWeightParameterNotFound == null ? null : Set.of(lengthWeightParameterNotFound)));
            result.ifPresent(m -> m.reset(batch));
            return result;
        } else if (lengthWeightParameterNotFound != null) {
            return Optional.of(new ToolkitIdModifications(batch, Set.of(), Set.of(lengthWeightParameterNotFound)));
        }
        return Optional.empty();
    }

    public void registerLengthWeightParameterNotFound(String lengthWeightParameterNotFound) {
        this.lengthWeightParameterNotFound = lengthWeightParameterNotFound;
        if (log.isWarnEnabled()) {
            log.warn(lengthWeightParameterNotFound);
        }
    }

    public BatchConsolidateRequest getRequest() {
        return request;
    }

    public GetOptionalRtpMeanWeight getRtpMeanWeightFinder() {
        return rtpMeanWeightFinder;
    }

    public void clear() {
        this.batch = null;
        this.request = null;
        monitor.setBean(null);
    }

    @Override
    public JavaBeanMonitor monitor() {
        return monitor;
    }

    @Override
    public Class<BatchDto> dataType() {
        return BatchDto.class;
    }
}
