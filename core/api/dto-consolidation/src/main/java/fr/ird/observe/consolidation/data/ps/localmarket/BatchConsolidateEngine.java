package fr.ird.observe.consolidation.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.ps.localmarket.BatchDto;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

/**
 * Created on 21/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class BatchConsolidateEngine {

    private static final Logger log = LogManager.getLogger(BatchConsolidateEngine.class);

    private final BatchConsolidateContext context;

    public BatchConsolidateEngine(GetOptionalRtpMeanWeight rtpMeanWeightFinder, DecoratorService decoratorService) {
        JavaBeanMonitor monitor = new JavaBeanMonitor(BatchDto.PROPERTY_WEIGHT, BatchDto.PROPERTY_WEIGHT_COMPUTED_SOURCE);
        this.context = new BatchConsolidateContext(rtpMeanWeightFinder, monitor, decoratorService.getDecoratorByType(BatchDto.class));
    }

    public Optional<ToolkitIdModifications> consolidate(BatchConsolidateRequest request) {
        BatchDto dto = request.getBatch();

        // the only thing we can consolidate is batch.weight
        if (dto.getWeight() != null && dto.getWeightComputedSource() == null) {
            // the value was set by user, do not change any thing
            return Optional.empty();
        }

        Optional<ToolkitIdModifications> result = consolidate(context, request, dto);
        result.ifPresent(r -> {
            if (r.withModifications()) {
                log.info(String.format("Found some modifications on batch: %s", request.getBatch().getId()));
            }
            if (r.withWarnings()) {
                log.warn(String.format("Found some warnings on batch: %s", request.getBatch().getId()));
            }
        });
        return result;
    }

    private Optional<ToolkitIdModifications> consolidate(BatchConsolidateContext context, BatchConsolidateRequest request, BatchDto dto) {
        context.watch(request);
        try {
            if (BatchConsolidateActions.ComputeWeightFromSpeciesMeanWeight.execute(context, dto)) {
                return context.build();
            }
            if (BatchConsolidateActions.ComputeWeightFromPackagingMeanWeight.execute(context, dto)) {
                return context.build();
            }
            BatchConsolidateActions.ResetWeightValues.execute(context, dto);
            return context.build();
        } finally {
            context.clear();
        }
    }
}

