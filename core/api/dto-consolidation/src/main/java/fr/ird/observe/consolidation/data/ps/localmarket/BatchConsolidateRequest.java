package fr.ird.observe.consolidation.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.localmarket.BatchDto;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.Date;

/**
 * Request to consolidate a local market batch.
 * <p>
 * Created on 21/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class BatchConsolidateRequest implements JsonAware {

    /**
     * Batch to consolidate.
     */
    private BatchDto batch;
    /**
     * Ocean id of the trip.
     */
    private String oceanId;
    /**
     * End date of the trip.
     */
    private Date date;
    /**
     * To throw exception if RTP not found.
     */
    private boolean failIfLengthWeightParameterNotFound;

    public boolean isFailIfLengthWeightParameterNotFound() {
        return failIfLengthWeightParameterNotFound;
    }

    public void setFailIfLengthWeightParameterNotFound(boolean failIfLengthWeightParameterNotFound) {
        this.failIfLengthWeightParameterNotFound = failIfLengthWeightParameterNotFound;
    }

    public BatchDto getBatch() {
        return batch;
    }

    public void setBatch(BatchDto batch) {
        this.batch = batch;
    }

    public String getOceanId() {
        return oceanId;
    }

    public void setOceanId(String oceanId) {
        this.oceanId = oceanId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
