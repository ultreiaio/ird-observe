package fr.ird.observe.consolidation.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created on 21/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface GetOptionalRtpMeanWeight {

    Optional<Float> get(String speciesId,
                        String sexId,
                        String oceanId,
                        String sizeMeasureTypeId,
                        Date date,
                        boolean failIfLengthWeightParameterNotFound,
                        Consumer<String> fallbackIfNotFound);

}
