package fr.ird.observe.consolidation.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdModifications;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.Set;

/**
 * Created on 12/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.28
 */
public class ActivityConsolidateResult implements JsonAware {

    /**
     * Id of activity.
     */
    protected String activityId;

    /**
     * Label of activity.
     */
    private String activityLabel;

    /**
     * All modification on floating objects.
     */
    private Set<ToolkitIdModifications> floatingObjectModifications;

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }

    public Set<ToolkitIdModifications> getFloatingObjectModifications() {
        return floatingObjectModifications;
    }

    public void setFloatingObjectModifications(Set<ToolkitIdModifications> floatingObjectModifications) {
        this.floatingObjectModifications = floatingObjectModifications;
    }

    public boolean withModifications() {
        if (getFloatingObjectModifications() != null) {
            for (ToolkitIdModifications modifications : floatingObjectModifications) {
                if (modifications.withModifications()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean withWarnings() {
        if (getFloatingObjectModifications() != null) {
            for (ToolkitIdModifications modifications : floatingObjectModifications) {
                if (modifications.withWarnings()) {
                    return true;
                }
            }
        }
        return false;
    }
}

