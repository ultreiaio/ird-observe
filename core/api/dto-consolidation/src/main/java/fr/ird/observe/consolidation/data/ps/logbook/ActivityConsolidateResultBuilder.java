package fr.ird.observe.consolidation.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdModifications;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 12/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.28
 */
public class ActivityConsolidateResultBuilder {
    private final Set<ToolkitIdModifications> floatingObjectModificationBuilder = new LinkedHashSet<>();
    private final String activityLabel;
    private final String activityId;

    public ActivityConsolidateResultBuilder(String activityId, String activityLabel) {
        this.activityId = activityId;
        this.activityLabel = activityLabel;
    }

    public Optional<ActivityConsolidateResult> build() {
        boolean noModification = floatingObjectModificationBuilder.isEmpty();

        if (noModification) {
            return Optional.empty();
        }
        ActivityConsolidateResult result = new ActivityConsolidateResult();
        result.setActivityId(activityId);
        result.setActivityLabel(activityLabel);
        result.setFloatingObjectModifications(floatingObjectModificationBuilder);
        return Optional.of(result);
    }

    public void flushFloatingObjectModification(ToolkitIdModifications modifications) {
        floatingObjectModificationBuilder.add(modifications);
    }
}
