package fr.ird.observe.consolidation.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.consolidation.data.ps.dcp.CleanComputedValuesAction;
import fr.ird.observe.consolidation.data.ps.dcp.ComputeBiodegradableConsolidateAction;
import fr.ird.observe.consolidation.data.ps.dcp.ComputeNonEntanglingConsolidateAction;
import fr.ird.observe.consolidation.data.ps.dcp.ComputeSimplifiedObjectTypeConsolidateAction;
import fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto;

import java.util.List;

/**
 * Created on 09/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.27
 */
public enum FloatingObjectConsolidateActions implements AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> {

    CleanComputedValuesWhenArriving(new CleanComputedValuesAction.WhenArriving<>()),
    CleanComputedValuesWhenLeaving(new CleanComputedValuesAction.WhenLeaving<>()),
    ComputeBiodegradableWhenArriving(new ComputeBiodegradableConsolidateAction.WhenArriving<>()),
    ComputeBiodegradableWhenLeaving(new ComputeBiodegradableConsolidateAction.WhenLeaving<>()),
    ComputeNonEntanglingWhenArriving(new ComputeNonEntanglingConsolidateAction.WhenArriving<>()),
    ComputeNonEntanglingWhenLeaving(new ComputeNonEntanglingConsolidateAction.WhenLeaving<>()),
    ComputeSimplifiedObjectTypeWhenArriving(new ComputeSimplifiedObjectTypeConsolidateAction.WhenArriving<>()),
    ComputeSimplifiedObjectTypeWhenLeaving(new ComputeSimplifiedObjectTypeConsolidateAction.WhenLeaving<>());

    private final AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> action;

    FloatingObjectConsolidateActions(AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> action) {
        this.action = action;
    }

    @Override
    public List<String> fieldNames() {
        return action.fieldNames();
    }

    @Override
    public void accept(FloatingObjectConsolidateContext floatingObjectConsolidateContext, FloatingObjectDto floatingObjectDto) {
        action.accept(floatingObjectConsolidateContext, floatingObjectDto);
    }

    @Override
    public boolean test(FloatingObjectConsolidateContext floatingObjectConsolidateContext, FloatingObjectDto floatingObjectDto) {
        return action.test(floatingObjectConsolidateContext, floatingObjectDto);
    }

    @Override
    public String toString() {
        return "ps.logbook." + FloatingObjectConsolidateActions.class.getSimpleName() + "." + name();
    }
}
