package fr.ird.observe.consolidation.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.data.ps.dcp.FloatingObjectConsolidateContextSupport;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeManager;
import fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto;
import fr.ird.observe.dto.data.ps.logbook.FloatingObjectPartDto;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;

/**
 * Created on 24/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class FloatingObjectConsolidateContext extends FloatingObjectConsolidateContextSupport<FloatingObjectDto, FloatingObjectPartDto, FloatingObjectConsolidateRequest> {

    public FloatingObjectConsolidateContext(SimplifiedObjectTypeManager simplifiedObjectTypeManager, JavaBeanMonitor monitor, Decorator decorator) {
        super(simplifiedObjectTypeManager, monitor, FloatingObjectDto.class, decorator);
    }
}
