package fr.ird.observe.consolidation.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdModifications;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.Set;

/**
 * Created at 12/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
public class SampleConsolidateResult implements JsonAware {

    /**
     * Id of sample.
     */
    protected String sampleId;

    /**
     * Label of sample.
     */
    private String sampleLabel;


    /**
     * All modifications on the sample (can be null if there is no modification on it).
     */
    private ToolkitIdModifications modifications;
    /**
     * All modification on sample activities.
     */
    private Set<ToolkitIdModifications> sampleActivityModifications;

    public String getSampleId() {
        return sampleId;
    }

    public void setSampleId(String sampleId) {
        this.sampleId = sampleId;
    }

    public String getSampleLabel() {
        return sampleLabel;
    }

    public void setSampleLabel(String sampleLabel) {
        this.sampleLabel = sampleLabel;
    }

    public ToolkitIdModifications getModifications() {
        return modifications;
    }

    public void setModifications(ToolkitIdModifications modifications) {
        this.modifications = modifications;
    }

    public Set<ToolkitIdModifications> getSampleActivityModifications() {
        return sampleActivityModifications;
    }

    public void setSampleActivityModifications(Set<ToolkitIdModifications> sampleActivityModifications) {
        this.sampleActivityModifications = sampleActivityModifications;
    }

    public boolean withModifications() {
        if (getModifications()!=null) {
            if (getModifications().withModifications()) {
                return true;
            }
        }
        if (getSampleActivityModifications() != null) {
            for (ToolkitIdModifications modifications : getSampleActivityModifications()) {
                if (modifications.withModifications()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean withWarnings() {
        if (getModifications()!=null) {
            if( getModifications().withWarnings()) {
                return true;
            }
        }
        if (getSampleActivityModifications() != null) {
            for (ToolkitIdModifications modifications : getSampleActivityModifications()) {
                if (modifications.withWarnings()) {
                    return true;
                }
            }
        }
        return false;
    }
}

