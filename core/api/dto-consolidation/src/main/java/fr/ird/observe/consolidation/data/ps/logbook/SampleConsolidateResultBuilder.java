package fr.ird.observe.consolidation.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdModifications;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created at 12/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
public class SampleConsolidateResultBuilder {
    private final Set<ToolkitIdModifications> sampleActivityModificationBuilder = new LinkedHashSet<>();
    private final String sampleLabel;
    private final String sampleId;
    private ToolkitIdModifications modifications;

    public SampleConsolidateResultBuilder(String sampleId, String sampleLabel) {
        this.sampleId = sampleId;
        this.sampleLabel = sampleLabel;
    }

    public Optional<SampleConsolidateResult> build() {
        boolean noModification = sampleActivityModificationBuilder.isEmpty() && modifications == null;
        if (noModification) {
            return Optional.empty();
        }
        SampleConsolidateResult result = new SampleConsolidateResult();
        result.setSampleId(sampleId);
        result.setSampleLabel(sampleLabel);
        result.setModifications(modifications);
        result.setSampleActivityModifications(sampleActivityModificationBuilder);
        return Optional.of(result);
    }

    public void flushModification(ToolkitIdModifications modifications) {
        this.modifications = modifications;
    }

    public void flushSampleActivityModification(ToolkitIdModifications modifications) {
        sampleActivityModificationBuilder.add(modifications);
    }

}

