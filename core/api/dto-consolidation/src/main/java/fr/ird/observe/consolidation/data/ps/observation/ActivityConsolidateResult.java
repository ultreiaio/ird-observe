package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.ToolkitIdModifications;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.Set;

/**
 * Pour retourner le résultat de la consolidation d'une activité de type Seine.
 * <p>
 * Un tel objet est créé uniquement si des modifications on été effectuée sur l'activité.
 * <p>
 * Created on 28/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ActivityConsolidateResult implements JsonAware {

    /**
     * Id of activity.
     */
    protected String activityId;

    /**
     * Label of activity.
     */
    private String activityLabel;

    /**
     * All modifications on the set (can be null if activity has no set, or there is no modification on it).
     */
    private ToolkitIdModifications setModifications;
    /**
     * All modification on samples.
     */
    private Set<ToolkitIdModifications> sampleMeasureModifications;
    /**
     * All modification on catches.
     */
    private Set<ToolkitIdModifications> catchModifications;
    /**
     * All modification on floating objects.
     */
    private Set<ToolkitIdModifications> floatingObjectModifications;

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }

    public Set<ToolkitIdModifications> getSampleMeasureModifications() {
        return sampleMeasureModifications;
    }

    public void setSampleMeasureModifications(Set<ToolkitIdModifications> sampleMeasureModifications) {
        this.sampleMeasureModifications = sampleMeasureModifications;
    }

    public Set<ToolkitIdModifications> getCatchModifications() {
        return catchModifications;
    }

    public void setCatchModifications(Set<ToolkitIdModifications> catchModifications) {
        this.catchModifications = catchModifications;
    }

    public Set<ToolkitIdModifications> getFloatingObjectModifications() {
        return floatingObjectModifications;
    }

    public void setFloatingObjectModifications(Set<ToolkitIdModifications> floatingObjectModifications) {
        this.floatingObjectModifications = floatingObjectModifications;
    }

    public ToolkitIdModifications getSetModifications() {
        return setModifications;
    }

    public void setSetModifications(ToolkitIdModifications setModifications) {
        this.setModifications = setModifications;
    }

    public boolean withModifications() {
        boolean result = getSetModifications() != null && setModifications.withModifications();
        if (!result) {
            if (getFloatingObjectModifications() != null) {
                for (ToolkitIdModifications modifications : floatingObjectModifications) {
                    if (modifications.withModifications()) {
                        return true;
                    }
                }
            }
            if (getCatchModifications() != null) {
                for (ToolkitIdModifications modifications : catchModifications) {
                    if (modifications.withModifications()) {
                        return true;
                    }
                }
            }
            if (getSampleMeasureModifications() != null) {
                for (ToolkitIdModifications modifications : sampleMeasureModifications) {
                    if (modifications.withModifications()) {
                        return true;
                    }
                }
            }
        }
        return result;
    }

    public boolean withWarnings() {
        boolean result = setModifications != null && setModifications.withWarnings();
        if (!result) {
            if (floatingObjectModifications != null) {
                for (ToolkitIdModifications modifications : floatingObjectModifications) {
                    if (modifications.withWarnings()) {
                        return true;
                    }
                }
            }
            if (catchModifications != null) {
                for (ToolkitIdModifications modifications : catchModifications) {
                    if (modifications.withWarnings()) {
                        return true;
                    }
                }
            }
            if (sampleMeasureModifications != null) {
                for (ToolkitIdModifications modifications : sampleMeasureModifications) {
                    if (modifications.withWarnings()) {
                        return true;
                    }
                }
            }
        }
        return result;
    }
}
