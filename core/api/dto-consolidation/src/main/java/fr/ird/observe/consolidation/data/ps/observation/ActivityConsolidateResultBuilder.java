package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdModifications;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * To build Activity consolidate result.
 * <p>
 * Created on 29/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ActivityConsolidateResultBuilder {

    private final Set<ToolkitIdModifications> floatingObjectModificationBuilder = new LinkedHashSet<>();
    private final Set<ToolkitIdModifications> catchModificationBuilder = new LinkedHashSet<>();
    private final Set<ToolkitIdModifications> sampleMeasureModificationBuilder = new LinkedHashSet<>();
    private ToolkitIdModifications setModifications;
    private final String activityLabel;
    private final String activityId;

    public ActivityConsolidateResultBuilder(String activityId, String activityLabel) {
        this.activityId = activityId;
        this.activityLabel = activityLabel;
    }

    public Optional<ActivityConsolidateResult> build() {
        boolean noModification = sampleMeasureModificationBuilder.isEmpty()
                && catchModificationBuilder.isEmpty()
                && floatingObjectModificationBuilder.isEmpty()
                && setModifications == null;

        if (noModification) {
            return Optional.empty();
        }
        ActivityConsolidateResult result = new ActivityConsolidateResult();
        result.setActivityId(activityId);
        result.setActivityLabel(activityLabel);
        result.setSetModifications(setModifications);
        result.setSampleMeasureModifications(sampleMeasureModificationBuilder);
        result.setCatchModifications(catchModificationBuilder);
        result.setFloatingObjectModifications(floatingObjectModificationBuilder);
        return Optional.of(result);
    }

    public void flushSetModification(ToolkitIdModifications modifications) {
        this.setModifications = modifications;
    }

    public void flushFloatingObjectModification(ToolkitIdModifications modifications) {
        floatingObjectModificationBuilder.add(modifications);
    }

    public void flushSampleMeasureModification(ToolkitIdModifications modifications) {
        sampleMeasureModificationBuilder.add(modifications);
    }

    public void flushCatchModification(ToolkitIdModifications modifications) {
        catchModificationBuilder.add(modifications);
    }

}
