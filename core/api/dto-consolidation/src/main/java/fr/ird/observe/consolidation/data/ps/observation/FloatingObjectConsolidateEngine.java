package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.consolidation.data.ps.dcp.FloatingObjectConsolidateEngineSupport;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeManager;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectPartDto;

/**
 * Created on 24/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class FloatingObjectConsolidateEngine extends FloatingObjectConsolidateEngineSupport<FloatingObjectDto, FloatingObjectPartDto, FloatingObjectConsolidateRequest, FloatingObjectConsolidateContext> {

    public FloatingObjectConsolidateEngine(SimplifiedObjectTypeManager simplifiedObjectTypeManager, DecoratorService decoratorService) {
        super(simplifiedObjectTypeManager, FloatingObjectDto.class, decoratorService);
    }

    @Override
    protected FloatingObjectConsolidateContext createContext() {
        return new FloatingObjectConsolidateContext(getSimplifiedObjectTypeManager(), getMonitor(), getDecorator());
    }

    @Override
    protected AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> cleanComputedValuesWhenArrivingAction() {
        return FloatingObjectConsolidateActions.CleanComputedValuesWhenArriving;
    }

    @Override
    protected AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> cleanComputedValuesWhenLeavingAction() {
        return FloatingObjectConsolidateActions.CleanComputedValuesWhenLeaving;
    }

    @Override
    protected AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> computeSimplifiedObjectTypeWhenArrivingAction() {
        return FloatingObjectConsolidateActions.ComputeSimplifiedObjectTypeWhenArriving;
    }

    @Override
    protected AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> computeSimplifiedObjectTypeWhenLeavingAction() {
        return FloatingObjectConsolidateActions.ComputeSimplifiedObjectTypeWhenLeaving;
    }

    @Override
    protected AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> computeBiodegradableWhenArrivingAction() {
        return FloatingObjectConsolidateActions.ComputeBiodegradableWhenArriving;
    }

    @Override
    protected AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> computeBiodegradableWhenLeavingAction() {
        return FloatingObjectConsolidateActions.ComputeBiodegradableWhenLeaving;
    }

    @Override
    protected AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> computeNonEntanglingWhenArrivingAction() {
        return FloatingObjectConsolidateActions.ComputeNonEntanglingWhenArriving;
    }

    @Override
    protected AtomicConsolidateAction<FloatingObjectConsolidateContext, FloatingObjectDto> computeNonEntanglingWhenLeavingAction() {
        return FloatingObjectConsolidateActions.ComputeNonEntanglingWhenLeaving;
    }
}
