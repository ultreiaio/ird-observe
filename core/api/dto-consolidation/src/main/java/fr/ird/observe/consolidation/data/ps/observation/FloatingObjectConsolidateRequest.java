package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.data.ps.dcp.FloatingObjectConsolidateRequestSupport;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectPartDto;

import java.util.Set;

/**
 * Created by tchemit on 04/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FloatingObjectConsolidateRequest extends FloatingObjectConsolidateRequestSupport<FloatingObjectDto, FloatingObjectPartDto> {

    public FloatingObjectConsolidateRequest(FloatingObjectDto floatingObject, Set<FloatingObjectPartDto> floatingObjectParts) {
        super(floatingObject, floatingObjectParts);
    }

}
