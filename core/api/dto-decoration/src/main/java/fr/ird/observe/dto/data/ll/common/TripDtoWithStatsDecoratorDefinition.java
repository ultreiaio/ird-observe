package fr.ird.observe.dto.data.ll.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.decoration.ObserveDtoDecoratorRenderer;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.i18n.I18n;
import java.util.Locale;
import java.util.Optional;
import javax.annotation.Generated;

@SuppressWarnings("rawtypes")
@AutoService(DecoratorDefinition.class)
@Generated(value = "io.ultreia.java4all.decoration.spi.DecoratorDefinitionJavaFileBuilder")
public final class TripDtoWithStatsDecoratorDefinition extends DecoratorDefinition<TripDto, ObserveDtoDecoratorRenderer<TripDto>> {

    public TripDtoWithStatsDecoratorDefinition() {
        super(TripDto.class, "WithStats", "##", " - ", new ObserveDtoDecoratorRenderer<>(TripDto.class), "startDate", "endDate", "vessel", "vesselCode", "gearUseFeaturesStat", "activityObsStat", "observationsActivitySetStat", "activityLogbookStat", "logbookActivitySetStat", "logbookActivitySampleStat", "sampleStat", "landingStat");
    }

    @Override
    public final String decorateContext(Locale locale, ObserveDtoDecoratorRenderer<TripDto> renderer, TripDto source, int index) {
        switch (index) {
            case 0:
              return renderer.date(locale, source.getStartDate());
            case 1:
              return renderer.date(locale, source.getEndDate());
            case 2:
              return renderer.label(locale, source.getVessel());
            case 3:
              return renderer.onNullValue("vesselCode", locale, source.getVesselCode());
            case 4:
              return " ( " + renderer.stat(locale, source.getGearUseFeaturesStat());
            case 5:
              return renderer.stat(locale, source.getActivityObsStat());
            case 6:
              return renderer.stat(locale, source.getObservationsActivitySetStat());
            case 7:
              return renderer.stat(locale, source.getActivityLogbookStat());
            case 8:
              return renderer.stat(locale, source.getLogbookActivitySetStat());
            case 9:
              return renderer.stat(locale, source.getLogbookActivitySampleStat());
            case 10:
              return renderer.stat(locale, source.getSampleStat());
            case 11:
              return renderer.stat(locale, source.getLandingStat()) + " )";
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    @Override
    public final String decorate(Locale locale, Object source, String rendererSeparator, int index) {
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);            default:
                throw new IllegalStateException("No index with value: " + index);
        }

    }

}
