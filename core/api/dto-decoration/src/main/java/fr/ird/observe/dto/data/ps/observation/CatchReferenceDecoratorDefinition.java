package fr.ird.observe.dto.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.decoration.ObserveDtoReferenceDecoratorRenderer;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.i18n.I18n;
import java.util.Locale;
import java.util.Optional;
import javax.annotation.Generated;

@SuppressWarnings("rawtypes")
@AutoService(DecoratorDefinition.class)
@Generated(value = "io.ultreia.java4all.decoration.spi.DecoratorDefinitionJavaFileBuilder")
public final class CatchReferenceDecoratorDefinition extends DecoratorDefinition<CatchReference, ObserveDtoReferenceDecoratorRenderer<CatchReference>> {

    public CatchReferenceDecoratorDefinition() {
        super(CatchReference.class, null, "##", " - ", new ObserveDtoReferenceDecoratorRenderer<>(CatchReference.class), "species", "speciesFateLabel");
    }

    @Override
    public final String decorateContext(Locale locale, ObserveDtoReferenceDecoratorRenderer<CatchReference> renderer, CatchReference source, int index) {
        switch (index) {
            case 0:
              return renderer.speciesLabel(locale, source.getSpecies());
            case 1:
              return renderer.onNullValue("speciesFateLabel", locale, source.getSpeciesFateLabel());
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    @Override
    public final String decorate(Locale locale, Object source, String rendererSeparator, int index) {
        return decoratorForContextCountEquals2(locale, source, rendererSeparator, index);
    }

}
