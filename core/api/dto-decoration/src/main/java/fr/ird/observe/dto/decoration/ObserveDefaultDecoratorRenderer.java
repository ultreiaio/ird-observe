package fr.ird.observe.dto.decoration;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.WithStartEndDate;
import fr.ird.observe.dto.referential.WindAware;
import io.ultreia.java4all.i18n.I18n;

import java.util.Locale;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 19/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface ObserveDefaultDecoratorRenderer {

    default String speciesLabel(String faoCode, String scientificLabel, String label) {
        if ("xx".equals(scientificLabel)) {
            return String.format("%s - %s", faoCode, label);
        }
        return String.format("%s - %s - %s", faoCode, scientificLabel, label);
    }

    default String speciesSimpleLabel(String scientificLabel, String label) {
        if ("xx".equals(scientificLabel)) {
            return String.format("%s", label);
        }
        return String.format("%s (%s)", scientificLabel, label);
    }

    default String fieldEnabler(Locale locale, boolean fieldEnabler) {
        return fieldEnabler ? I18n.l(locale, "observe.referential.ps.common.AcquisitionStatus.fieldEnabled") : I18n.l(locale, "observe.referential.ps.common.AcquisitionStatus.fieldDisabled");
    }

    default String booleanValue(Locale locale, boolean booleanValue) {
        return booleanValue ? I18n.l(locale, "boolean.true") : I18n.l(locale, "boolean.false");
    }

    default String speciesFateDiscard(Locale locale, Boolean discard) {
        String value;
        if (discard == null) {
            value = t("boolean.null");
        } else {
            value = discard ? t("boolean.true") : t("boolean.false");
        }
        return I18n.l(locale, "observe.referential.ps.common.SpeciesFate.discardLabel", value);
    }

    default String speciesFateWeightRangeAllowed(Locale locale, boolean weightRangeAllowed) {
        String value = weightRangeAllowed ? t("boolean.true") : t("boolean.false");
        return I18n.l(locale, "observe.referential.ps.common.SpeciesFate.weightRangeAllowedLabel", value);
    }

    String onNullValue(Locale locale, Object value);

    default String speedRange(Locale locale, WindAware source) {
        return source == null ? onNullValue(locale, null) : source.getSpeedRange(locale);
    }

    default String swellHeight(Locale locale, WindAware source) {
        return source == null ? onNullValue(locale, null) : source.getSwellHeight(locale);
    }

    default String validityRangeLabel(Locale locale, WithStartEndDate source) {
        return source == null ? onNullValue(locale, null) : source.getValidityRangeLabel(locale);
    }
}
