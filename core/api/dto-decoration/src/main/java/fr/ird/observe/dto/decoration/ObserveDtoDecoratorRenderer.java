package fr.ird.observe.dto.decoration;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DtoDecoratorRenderer;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.WithStartEndDate;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import io.ultreia.java4all.i18n.I18n;

import java.util.Locale;

/**
 * Created on 19/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveDtoDecoratorRenderer<O extends BusinessDto> extends DtoDecoratorRenderer<O> implements ObserveDefaultDecoratorRenderer {

    public ObserveDtoDecoratorRenderer(Class<O> type) {
        super(type);
        setCodeProperties(ObserveI18nDecoratorHelper.CODE_PROPERTIES);
    }

    @Override
    public String onNullValue(String propertyName, Locale locale, Object value) {
        if (propertyName.equals(WithStartEndDate.PROPERTY_VALIDITY_RANGE_LABEL)) {
            return I18n.t("observe.Common.validityRangeLabel") + ' ' + value;
        }
        return super.onNullValue(propertyName, locale, value);
    }

    public String speciesLabel(Locale locale, SpeciesReference species) {
        if (species == null) {
            return onNullValue(locale, null);
        }
        String faoCode = species.getFaoCode();
        String scientificLabel = species.getScientificLabel();
        String label = label(locale, species);
        return speciesLabel(faoCode, scientificLabel, label);
    }

    public String speciesSimpleLabel(Locale locale, SpeciesReference species) {
        if (species == null) {
            return onNullValue(locale, null);
        }
        String scientificLabel = species.getScientificLabel();
        String label = label(locale, species);
        return speciesSimpleLabel(scientificLabel, label);
    }
}
