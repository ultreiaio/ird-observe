package fr.ird.observe.dto.decoration;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ObserveI18nLabelsBuilder;
import fr.ird.observe.spi.module.BusinessProjectI18nHelper;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducer;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducerProvider;

import java.util.Set;

/**
 * Created on 03/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
@AutoService(BeanPropertyI18nKeyProducerProvider.class)
public class ObserveI18nDecoratorHelper extends I18nDecoratorHelper {

    public static final Set<String> CODE_PROPERTIES = Set.of("code", "vesselCode", "faoCode");
    private ObserveI18nLabelsBuilder labelsBuilder;

    public ObserveI18nDecoratorHelper() {
        BusinessProjectI18nHelper.setI18nKeyPrefix(getCommonPrefix());
    }

    @Override
    public BeanPropertyI18nKeyProducer getDefaultLabelsBuilder() {
        return labelsBuilder == null ? labelsBuilder = new ObserveI18nLabelsBuilder() : labelsBuilder;
    }

    @Override
    public String getCommonPrefix() {
        return getDefaultLabelsBuilder().getCommonPrefix();
    }
}
