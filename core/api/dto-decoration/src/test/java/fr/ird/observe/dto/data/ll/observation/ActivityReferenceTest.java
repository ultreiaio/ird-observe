package fr.ird.observe.dto.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.util.Dates;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * Created on 02/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ActivityReferenceTest {

    @Test
    public void testSort() {
        ActivityReference r0 = new ActivityReference();
        r0.setTimeStamp(Dates.createDate(12, 11, 2016));

        ActivityReference r1 = new ActivityReference();
        r1.setTimeStamp(Dates.createDate(1, 4, 2015));

        ActivityReference r2 = new ActivityReference();
        r2.setTimeStamp(Dates.createDate(3, 5, 2017));

        List<ActivityReference> list = new LinkedList<>();
        list.add(r0);
        list.add(r1);
        list.add(r2);
        Decorator decorator = DecoratorService.provider().decorator(Locale.FRANCE, ActivityReference.class);
        r0.registerDecorator(decorator);
        r1.registerDecorator(decorator);
        r2.registerDecorator(decorator);

        List<ActivityReference> list2 = new LinkedList<>(list);

        decorator.sort(list2, 0);

        Assert.assertEquals(List.of(r1, r0, r2), list2);
        decorator.sort(list2, 0, true);
        Assert.assertEquals(List.of(r2, r0, r1), list2);
    }

}
