package fr.ird.observe.dto.data.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.VesselReference;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.util.Dates;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * Created on 02/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripDtoTest {

    @Test
    public void testSort() {
        TripDto r0 = new TripDto();
        r0.setStartDate(Dates.createDate(12, 11, 2016));
        r0.setEndDate(Dates.createDate(3, 1, 2017));
        VesselReference vessel = new VesselReference();
        vessel.setLabel("Vessel");
        r0.setVessel(vessel);

        TripDto r1 = new TripDto();
        r1.setStartDate(Dates.createDate(1, 4, 2017));
        r1.setEndDate(Dates.createDate(9, 5, 2017));
        r1.setVessel(vessel);

        TripDto r2 = new TripDto();
        r2.setStartDate(Dates.createDate(3, 5, 2017));
        r2.setEndDate(Dates.createDate(8, 5, 2017));
        r2.setVessel(vessel);

        List<TripDto> list = new LinkedList<>();
        list.add(r0);
        list.add(r1);
        list.add(r2);
        Decorator decorator = DecoratorService.provider().decorator(Locale.FRANCE, TripDto.class);
        r0.registerDecorator(decorator);
        r1.registerDecorator(decorator);
        r2.registerDecorator(decorator);

        List<TripDto> list2 = new LinkedList<>(list);

        decorator.sort(list2, 0);

        Assert.assertEquals(list, list2);
        decorator.sort(list2, 0, true);
        Assert.assertEquals(List.of(r2, r1, r0), list2);

        decorator.sort(list2, 1);
        Assert.assertEquals(List.of(r0, r2, r1), list2);

        decorator.sort(list2, 1, true);
        Assert.assertEquals(List.of(r1, r2, r0), list2);

        decorator.setIndex(0);
    }

    @Test
    public void testSortToolkitLabel() {
        TripDto r0 = new TripDto();
        r0.setStartDate(Dates.createDate(12, 11, 2016));
        r0.setEndDate(Dates.createDate(3, 1, 2017));
        VesselReference vessel = new VesselReference();
        vessel.setLabel("Vessel");
        r0.setVessel(vessel);

        TripDto r1 = new TripDto();
        r1.setStartDate(Dates.createDate(1, 4, 2017));
        r1.setEndDate(Dates.createDate(9, 5, 2017));
        r1.setVessel(vessel);

        TripDto r2 = new TripDto();
        r2.setStartDate(Dates.createDate(3, 5, 2017));
        r2.setEndDate(Dates.createDate(8, 5, 2017));
        r2.setVessel(vessel);

        Decorator decorator = DecoratorService.provider().decorator(Locale.FRANCE, TripDto.class);
        r0.registerDecorator(decorator);
        r1.registerDecorator(decorator);
        r2.registerDecorator(decorator);

        ToolkitIdLabel l0 = r0.toLabel();
        ToolkitIdLabel l1 = r1.toLabel();
        ToolkitIdLabel l2 = r2.toLabel();

        List<ToolkitIdLabel> list = new LinkedList<>();
        list.add(l0);
        list.add(l1);
        list.add(l2);

        Decorator decoratorLabel = new DecoratorService(ReferentialLocale.FR).getToolkitIdLabelDecoratorByType(TripDto.class);
        l0.registerDecorator(decoratorLabel);
        l1.registerDecorator(decoratorLabel);
        l2.registerDecorator(decoratorLabel);

        decoratorLabel.sort(list, 0);

        Assert.assertEquals(List.of(l0, l1, l2), list);
        decoratorLabel.sort(list, 0, true);
        Assert.assertEquals(List.of(l2, l1, l0), list);
    }
}
