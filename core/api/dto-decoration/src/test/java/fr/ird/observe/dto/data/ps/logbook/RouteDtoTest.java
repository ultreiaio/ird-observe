package fr.ird.observe.dto.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialLocale;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.util.Dates;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * Created on 02/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class RouteDtoTest {

    @Test
    public void testSort() {
        RouteDto r0 = new RouteDto();
        r0.setDate(Dates.createDate(12, 11, 2016));

        RouteDto r1 = new RouteDto();
        r1.setDate(Dates.createDate(1, 4, 2017));

        RouteDto r2 = new RouteDto();
        r2.setDate(Dates.createDate(3, 5, 2017));

        List<RouteDto> list = new LinkedList<>();
        list.add(r0);
        list.add(r1);
        list.add(r2);
        Decorator decorator = DecoratorService.provider().decorator(Locale.FRANCE, RouteDto.class);
        r0.registerDecorator(decorator);
        r1.registerDecorator(decorator);
        r2.registerDecorator(decorator);

        List<RouteDto> list2 = new LinkedList<>(list);

        decorator.sort(list2, 0);

        Assert.assertEquals(list, list2);
        decorator.sort(list2, 0, true);
        Assert.assertEquals(List.of(r2, r1, r0), list2);
    }

    @Test
    public void testSortToolkitLabel() {
        RouteDto r0 = new RouteDto();
        r0.setDate(Dates.createDate(12, 11, 2016));

        RouteDto r1 = new RouteDto();
        r1.setDate(Dates.createDate(1, 4, 2017));

        RouteDto r2 = new RouteDto();
        r2.setDate(Dates.createDate(3, 5, 2017));

        Decorator decorator = DecoratorService.provider().decorator(Locale.FRANCE, RouteDto.class);
        r0.registerDecorator(decorator);
        r1.registerDecorator(decorator);
        r2.registerDecorator(decorator);

        ToolkitIdLabel l0 = r0.toLabel();
        ToolkitIdLabel l1 = r1.toLabel();
        ToolkitIdLabel l2 = r2.toLabel();

        List<ToolkitIdLabel> list = new LinkedList<>();
        list.add(l0);
        list.add(l1);
        list.add(l2);

        Decorator decoratorLabel = new DecoratorService(ReferentialLocale.FR).getToolkitIdLabelDecoratorByType(RouteDto.class);
        l0.registerDecorator(decoratorLabel);
        l1.registerDecorator(decoratorLabel);
        l2.registerDecorator(decoratorLabel);

        decoratorLabel.sort(list, 0);

        Assert.assertEquals(List.of(l0, l1, l2), list);
        decoratorLabel.sort(list, 0, true);
        Assert.assertEquals(List.of(l2, l1, l0), list);
    }
}
