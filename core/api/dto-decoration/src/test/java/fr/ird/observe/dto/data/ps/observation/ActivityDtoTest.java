package fr.ird.observe.dto.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialLocale;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.util.Dates;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * Created on 02/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ActivityDtoTest {

    private ActivityDto r0;
    private ActivityDto r1;
    private ActivityDto r2;
    private Decorator decorator;

    @Before
    public void setUp() throws Exception {
        r0 = new ActivityDto();
        r0.setTime(Dates.createDate(0, 25, 8, 0, 0, 0));

        r1 = new ActivityDto();
        r1.setTime(Dates.createDate(0, 45, 15, 0, 0, 0));

        r2 = new ActivityDto();
        r2.setTime(Dates.createDate(0, 12, 12, 0, 0, 0));

        decorator = DecoratorService.provider().decorator(Locale.FRANCE, ActivityDto.class);
        r0.registerDecorator(decorator);
        r1.registerDecorator(decorator);
        r2.registerDecorator(decorator);
    }

    @Test
    public void testSort() {

        List<ActivityDto> list = new LinkedList<>();
        list.add(r0);
        list.add(r1);
        list.add(r2);

        decorator.sort(list, 0);
        Assert.assertEquals(List.of(r0, r2, r1), list);

        decorator.sort(list, 0, true);
        Assert.assertEquals(List.of(r1, r2, r0), list);
    }

    @Test
    public void testSortToolkitLabel() {

        ToolkitIdLabel l0 = r0.toLabel();
        ToolkitIdLabel l1 = r1.toLabel();
        ToolkitIdLabel l2 = r2.toLabel();

        List<ToolkitIdLabel> list = new LinkedList<>();
        list.add(l0);
        list.add(l1);
        list.add(l2);

        Decorator decoratorLabel = new DecoratorService(ReferentialLocale.FR).getToolkitIdLabelDecoratorByType(ActivityDto.class);
        l0.registerDecorator(decoratorLabel);
        l1.registerDecorator(decoratorLabel);
        l2.registerDecorator(decoratorLabel);

        decoratorLabel.sort(list, 0);

        Assert.assertEquals(List.of(l0, l2, l1), list);
        decoratorLabel.sort(list, 0, true);
        Assert.assertEquals(List.of(l1, l2, l0), list);
    }

}
