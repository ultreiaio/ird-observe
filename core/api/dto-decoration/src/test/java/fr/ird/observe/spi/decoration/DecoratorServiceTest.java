package fr.ird.observe.spi.decoration;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.lang.Objects2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * Created on 06/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class DecoratorServiceTest {

    private static final Logger log = LogManager.getLogger(DecoratorServiceTest.class);

    private DecoratorService decoratorService;
    private ObserveBusinessProject businessProject;

    @Before
    public void setUp() {
        businessProject = ObserveBusinessProject.get();
        decoratorService = new DecoratorService(ReferentialLocale.FR);
    }

    @Test
    public void getReferentialReferenceDecorator() {
        businessProject.getReferentialTypes().forEach(this::testReferential);
    }

    @Test
    public void getDataReferenceDecorator() {
        testData(fr.ird.observe.dto.data.ps.observation.ActivityDto.class);
        testData(fr.ird.observe.dto.data.ll.observation.ActivityDto.class);
        businessProject.getDataTypes().forEach(this::testData);
    }

    @Test
    public void testGroupBy() {
        businessProject.getRootOpenableDataTypes().forEach(this::testGroupBy);
    }

    private <D extends ReferentialDto, R extends ReferentialDtoReference> void testReferential(Class<D> dtoType) {
        Class<R> referenceType = ObserveBusinessProject.get().getMapping().getReferenceType(dtoType);
        test(dtoType, referenceType);
    }

    private <D extends DataDto, R extends DataDtoReference> void testData(Class<D> dtoType) {
        Class<R> referenceType = ObserveBusinessProject.get().getMapping().getReferenceType(dtoType);
        if (referenceType == null) {
            return;
        }
        test(dtoType, referenceType);
    }

    private <D extends RootOpenableDto> void testGroupBy(Class<D> dtoType) {
        List<DataGroupByDtoDefinition<D, ?>> definitions = businessProject.getDataGroupByDtoDefinitions(dtoType);
        Assert.assertNotNull("could not find groupBy definitions for:" + dtoType.getName(), definitions);
        for (DataGroupByDtoDefinition<D, ?> definition : definitions) {
            log.info(definition.getDefinitionLabel());
            Decorator decorator = decoratorService.getDecoratorByType(definition.getContainerType());
            Assert.assertNotNull("could not find decorator for groupBy definitions :" + dtoType.getName(), decorator);
        }
    }

    private <D extends BusinessDto, R extends DtoReference> void test(Class<D> dtoType, Class<R> referenceType) {
        Decorator referenceDecorator = decoratorService.getDecoratorByType(referenceType);
        if (referenceType != null) {
            Assert.assertNotNull("could not find reference decorator for " + dtoType.getName(), referenceDecorator);
        }
        Decorator decorator = decoratorService.getDecoratorByType(dtoType);
        Assert.assertNotNull("could not find reference decorator for " + dtoType.getName(), decorator);

        log.debug(String.format("Testing on %s", dtoType.getName()));

        D dto = BusinessDto.newDto(dtoType, new Date());
        decoratorService.installDecorator(dto);
        decorate(decorator, dto);
        if (referenceType != null) {
            R reference = Objects2.newInstance(referenceType);
            reference.fromDtoReferenceAware(dto);
            decoratorService.installDecorator(reference);
            decorate(referenceDecorator, reference);
            ToolkitIdLabel toLabel = dto.toLabel();

            Decorator toLabelDecorator = decoratorService.getDecoratorByType(ToolkitIdLabel.class, dtoType.getName());
            decoratorService.installToolkitIdLabelDecorator(dtoType, toLabel);
            decorate(toLabelDecorator, toLabel);
        }
    }

    private void decorate(Decorator d, ToolkitId o) {
        String fromDecorator = d.decorate(o);
        String fromToString = o.toString();
        log.trace(String.format("fromDecorator: %s, fromToString: %s", fromDecorator, fromToString));
        Assert.assertNotNull(fromDecorator);
        Assert.assertFalse(fromDecorator.isEmpty());
        Assert.assertEquals(fromDecorator, fromToString);
    }

}
