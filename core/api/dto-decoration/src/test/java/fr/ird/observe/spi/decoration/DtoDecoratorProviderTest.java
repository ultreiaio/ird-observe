package fr.ird.observe.spi.decoration;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveApiFixtures;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.common.SexDto;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created on 18/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class DtoDecoratorProviderTest {

    private DecoratorProvider provider;

    @Before
    public void setUp() throws Exception {
        provider = DecoratorProvider.get();
    }

    @After
    public void tearDown() {
        provider.close();
    }

    @Test
    public void init() {
        ObserveApiFixtures.assertFixture("DECORATOR_DEFINITIONS_COUNT", ObserveApiFixtures.DECORATOR_DEFINITIONS_COUNT, provider.definitions().size());
    }

    @Test
    public void decorator() {
        assertEquals(0, provider.decorators().size());
        Decorator actual = provider.decorator(Locale.FRANCE, SexDto.class);
        assertNotNull(actual);
        assertEquals(1, provider.decorators().size());
        Decorator actual2 = provider.decorator(Locale.FRANCE, SexDto.class);
        assertEquals(actual, actual2);
        assertEquals(1, provider.decorators().size());
        actual2 = provider.decorator(Locale.UK, SexDto.class);
        assertNotEquals(actual, actual2);
        assertEquals(2, provider.decorators().size());
    }

    @Test(expected = IllegalStateException.class)
    public void badDecorator() {
        provider.decorator(Locale.FRANCE, SexDto.class, "Taiste");
    }

    @Test(expected = IllegalStateException.class)
    public void badDecorator2() {
        provider.decorator(Locale.FRANCE, ReferentialDto.class);
    }

    @Test
    public void testSexDto() {
        DecoratorProvider provider = DecoratorProvider.get();
        Decorator decorator = provider.decorator(Locale.FRANCE, SexDto.class);
        assertNotNull(decorator);
        SexDto dto = new SexDto();
        String actual = decorator.decorate(dto);
        System.out.println(actual);
        dto.setCode("code");
        dto.setLabel2("Label FR");
        dto.setLabel1("Label UK");
        actual = decorator.decorate(dto);
        System.out.println("FR:" + actual);
        actual = provider.decorator(Locale.UK, SexDto.class).decorate(dto);
        System.out.println("UK:" + actual);
        actual = provider.decorator(new Locale("es", "ES"), SexDto.class).decorate(dto);
        System.out.println("ES:" + actual);
    }
}
