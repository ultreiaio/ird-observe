package fr.ird.observe.spi.module;

/*-
 * #%L
 * ObServe Core :: API :: Dto Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveApiFixtures;
import fr.ird.observe.dto.DtoToReference;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.ps.observation.SchoolEstimateDto;
import fr.ird.observe.dto.data.ps.observation.SetSchoolEstimateDto;
import fr.ird.observe.dto.form.FormDefinition;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReferenceDefinition;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.test.TestSupportWithConfig;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created on 03/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class ObserveBusinessProjectTest extends TestSupportWithConfig {

    private static final Logger log = LogManager.getLogger(ObserveBusinessProjectTest.class);
    private ObserveBusinessProject businessProject;

    @Before
    public void setUp() throws Exception {
        businessProject = ObserveBusinessProject.get();
    }

    @Test
    public void getModule() {
        assertBusinessModule("fr.ird.observe.client.datasource.editor.ll.data.logbook", "ll", "logbook");
        assertBusinessModule("fr.ird.observe.client.datasource.editor.ll.data.common", "ll", "common");
        assertBusinessModule("fr.ird.observe.client.datasource.editor.ps.data", "ps", null);
        assertBusinessModule("fr.ird.observe.client.datasource.editor.ps.data.common", "ps", "common");
        assertBusinessModule("fr.ird.observe.client.datasource.editor.common.data", "common", null);
        assertBusinessModule("fr.ird.observe.client.datasource.editor.common.referential.common", "common", "common");
    }

    @Test
    public void getGroupByDefinitions() {
        {
            List<DataGroupByDtoDefinition<fr.ird.observe.dto.data.ll.common.TripDto, ?>> definitions = businessProject.getDataGroupByDtoDefinitions(fr.ird.observe.dto.data.ll.common.TripDto.class);
            Assert.assertNotNull(definitions);
            Assert.assertFalse(definitions.isEmpty());

            List<DataGroupByDtoDefinition<?, ?>> definitions2 = businessProject.getLlBusinessModule().getDataGroupByDtoDefinitions();
            Assert.assertNotNull(definitions2);
            Assert.assertFalse(definitions2.isEmpty());
            int expectedCount = ObserveApiFixtures.getIntegerProperty("DataGroupByDtoToDefinitionMapping.count.ll");
            ObserveApiFixtures.assertFixture("DataGroupByDtoToDefinitionMapping.count.ll", expectedCount, definitions2.size());
        }
        {
            List<DataGroupByDtoDefinition<fr.ird.observe.dto.data.ps.common.TripDto, ?>> definitions = businessProject.getDataGroupByDtoDefinitions(fr.ird.observe.dto.data.ps.common.TripDto.class);
            Assert.assertNotNull(definitions);
            Assert.assertFalse(definitions.isEmpty());
            List<DataGroupByDtoDefinition<?, ?>> definitions2 = businessProject.getPsBusinessModule().getDataGroupByDtoDefinitions();
            Assert.assertNotNull(definitions2);
            Assert.assertFalse(definitions2.isEmpty());
            int expectedCount = ObserveApiFixtures.getIntegerProperty("DataGroupByDtoToDefinitionMapping.count.ps");
            ObserveApiFixtures.assertFixture("DataGroupByDtoToDefinitionMapping.count.ps", expectedCount, definitions2.size());
        }
    }

    @Test
    public void getGroupByDefinition() {
        Assert.assertNotNull(businessProject.getDataGroupByDtoDefinition("dataLlCommonTripGroupByLogbookProgram"));
        Assert.assertNotNull(businessProject.getDataGroupByDtoDefinition("dataLlCommonTripGroupByObservationsProgram"));
        Assert.assertNotNull(businessProject.getDataGroupByDtoDefinition("dataPsCommonTripGroupByLogbookProgram"));
        Assert.assertNotNull(businessProject.getDataGroupByDtoDefinition("dataPsCommonTripGroupByObservationsProgram"));

        Assert.assertNull(businessProject.getDataGroupByDtoDefinition("dataPsCommonTripGroupByObservationsProgram" + System.nanoTime()));

    }

    private void assertBusinessModule(String packageName, String moduleName, String subModuleName) {
        BusinessModule businessModule = businessProject.getBusinessModule(packageName);
        Assert.assertNotNull(businessModule);
        Assert.assertNotNull(businessModule.getName());
        Assert.assertEquals(moduleName, businessModule.getName());
        if (subModuleName != null) {
            BusinessSubModule businessSubModule = businessProject.getBusinessSubModule(businessModule, packageName);
            Assert.assertNotNull(businessSubModule);
            Assert.assertNotNull(businessSubModule.getName());
            Assert.assertEquals(subModuleName, businessSubModule.getName());
        }
    }

    @Test
    public void fromReferentialDto() {
        AtomicInteger formCount = new AtomicInteger();
        for (Class<? extends ReferentialDto> dtoType : businessProject.getReferentialTypes()) {
            fromReferentialDto((Class) dtoType, formCount);
        }
        Assert.assertEquals("dto.REFERENTIAL_FORM_COUNT bad value", ObserveApiFixtures.REFERENTIAL_FORM_COUNT, formCount.intValue());
    }

    @Test
    public void groupByDecoration() {
        for (Class<? extends RootOpenableDto> dtoType : businessProject.getRootOpenableDataTypes()) {
            groupByDecoration(dtoType);
        }
    }

    private <D extends RootOpenableDto> void groupByDecoration(Class<D> dtoType) {
        List<DataGroupByDtoDefinition<D, ?>> definitions = businessProject.getDataGroupByDtoDefinitions(dtoType);
        Assert.assertNotNull("could not find groupBy definitions for:" + dtoType.getName(), definitions);
        for (DataGroupByDtoDefinition<D, ?> definition : definitions) {
            log.info(definition.getDefinitionLabel());
        }
    }

    @Test
    public void fromReferentialForm() {
        AtomicInteger formCount = new AtomicInteger();
        for (Class<? extends ReferentialDto> dtoType : businessProject.getReferentialTypes()) {
            fromReferentialForm(dtoType, formCount);
        }
        Assert.assertEquals("dto.REFERENTIAL_FORM_COUNT bad value", ObserveApiFixtures.REFERENTIAL_FORM_COUNT, formCount.intValue());
    }

    @Test
    public void referentialJavaBeanDefinition() {
        for (Class<? extends ReferentialDto> dtoType : businessProject.getReferentialTypes()) {
            Assert.assertTrue("Can't find JavaBeanDefinition for type:" + dtoType.getName(), JavaBeanDefinitionStore.getDefinition(dtoType).isPresent());
        }
    }

    @Test
    public void dataJavaBeanDefinition() {
        for (Class<? extends DataDto> dtoType : businessProject.getDataTypes()) {
            Assert.assertTrue("Can't find JavaBeanDefinition for type:" + dtoType.getName(), JavaBeanDefinitionStore.getDefinition(dtoType).isPresent());
        }
    }

    @Test
    public void fromDataDto() {
        AtomicInteger referenceCount = new AtomicInteger();
        for (Class<? extends DataDto> dtoType : businessProject.getDataTypes()) {
            fromDataDto((Class) dtoType, referenceCount);
        }
        Assert.assertEquals("global.REFERENCE_DATA_COUNT bad value", ObserveApiFixtures.REFERENCE_DATA_COUNT, referenceCount.intValue());
    }

    @Test
    public void fromDataForm() {
        AtomicInteger formCount = new AtomicInteger();
        for (Class<? extends DataDto> dtoType : businessProject.getDataTypes()) {
            fromDataForm(dtoType, formCount);
        }
        Assert.assertEquals("dto.DATA_FORM_COUNT bad value", ObserveApiFixtures.DATA_FORM_COUNT, formCount.intValue());
        FormDefinition<SchoolEstimateDto> spi = businessProject.<SchoolEstimateDto>getOptionalFormDefinition(SetSchoolEstimateDto.class).orElse(null);
        Assert.assertNotNull(spi);
    }

    private <D extends ReferentialDto & DtoToReference<R>, R extends ReferentialDtoReference> void fromReferentialDto(Class<D> dtoType, AtomicInteger formCount) {
        Optional<DtoReferenceDefinition<D, R>> optionalReferenceDefinition = businessProject.getOptionalReferenceDefinition(dtoType);
        Assert.assertTrue(optionalReferenceDefinition.isPresent());
        DtoReferenceDefinition<D, R> dtoReferenceDefinition = optionalReferenceDefinition.get();
        Class<R> referenceType = dtoReferenceDefinition.getType();
        Assert.assertNotNull(referenceType);
        formCount.incrementAndGet();
    }

    private <D extends ReferentialDto> void fromReferentialForm(Class<D> dtoType, AtomicInteger formCount) {
        FormDefinition<D> formDefinition = businessProject.<D>getOptionalFormDefinition(dtoType).orElse(null);
        Assert.assertNotNull(formDefinition);
        Assert.assertEquals(dtoType, formDefinition.getType());
        formCount.incrementAndGet();
    }

    private <D extends DataDto & DtoToReference<R>, R extends DataDtoReference> void fromDataDto(Class<D> dtoType, AtomicInteger referenceCount) {
        Class<R> referenceType = businessProject.getMapping().getReferenceType(dtoType);

        if (Objects.equals(businessProject.getMapping().getDtoToMainDtoClassMapping().get(dtoType), dtoType)) {
            Assert.assertNotNull("could not find type context for:" + dtoType.getName(), dtoType);
        } else {
            if (referenceType == null) {
                return;
            }
        }
        DtoReferenceDefinition<D, R> referenceDefinition = businessProject.<D, R>getOptionalReferenceDefinition(dtoType).orElse(null);
        boolean expectedNull;
        if (referenceType != null) {
            referenceCount.incrementAndGet();
            expectedNull = false;
        } else {
            expectedNull = true;
        }
        Assert.assertEquals("reference definition should be null for type: " + dtoType + " but was " + referenceDefinition, expectedNull, referenceDefinition == null);
    }


    private <D extends DataDto, R extends DataDto> void fromDataForm(Class<D> dtoType, AtomicInteger formCount) {
        FormDefinition<R> formDefinition = businessProject.<R>getOptionalFormDefinition(dtoType).orElse(null);
        if (formDefinition != null) {
            if (formDefinition.getType().equals(dtoType)) {
                formCount.incrementAndGet();
            }
        }
    }
}
