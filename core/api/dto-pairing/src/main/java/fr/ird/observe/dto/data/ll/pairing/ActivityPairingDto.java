package fr.ird.observe.dto.data.ll.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.observation.ActivityReference;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.List;
import java.util.Objects;

/**
 * Contains all stuff used to perform one activity paring action.
 * <p>
 * Created at 19/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ActivityPairingDto implements JsonAware {

    private final List<ActivityReference> observationActivities;

    public ActivityPairingDto(List<ActivityReference> observationActivities) {
        this.observationActivities = Objects.requireNonNull(observationActivities);
    }

    public List<ActivityReference> getObservationActivities() {
        return observationActivities;
    }


}
