package fr.ird.observe.dto.data.ll.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.observation.ActivityReference;
import io.ultreia.java4all.util.Dates;
import org.apache.commons.lang3.time.DateUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Engine to perform one activity pairing.
 * <p>
 * Created at 19/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ActivityPairingEngine {

    private final ActivityPairingDto data;

    public ActivityPairingEngine(ActivityPairingDto data) {
        this.data = Objects.requireNonNull(data);
    }

    public List<fr.ird.observe.dto.data.ll.observation.ActivityReference> getObservationActivitiesCandidates(fr.ird.observe.dto.data.ll.LlActivityAware logbookActivity) {
        String vesselActivityId = logbookActivity.getVesselActivityId();
        Date date = logbookActivity.getDate();
        if (vesselActivityId == null || date == null) {
            return Collections.emptyList();
        }
        long minDate = Dates.getYesterday(date).getTime();
        long maxDate = DateUtils.addDays(Dates.getEndOfDay(date), 1).getTime();
        return data.getObservationActivities().stream().filter(e -> filterDate(e, vesselActivityId, minDate, maxDate)).collect(Collectors.toList());
    }

    public List<ActivityPairingResult> computeResult(List<fr.ird.observe.dto.data.ll.logbook.ActivityReference> logbookActivities) {
        List<ActivityPairingResult> result = new LinkedList<>();
        for (fr.ird.observe.dto.data.ll.logbook.ActivityReference logbookActivity : logbookActivities) {
            ActivityPairingResult activityPairingResult = computeResult(logbookActivity);
            if (activityPairingResult != null) {
                result.add(activityPairingResult);
            }
        }
        return result;
    }

    private ActivityPairingResult computeResult(fr.ird.observe.dto.data.ll.logbook.ActivityReference logbookActivity) {
        List<ActivityReference> observationActivities = getObservationActivitiesCandidates(logbookActivity);
        List<ActivityPairingResultItem> itemBuilder = new ArrayList<>(observationActivities.size());
        for (ActivityReference observationActivity : observationActivities) {
            itemBuilder.add(new ActivityPairingResultItem(logbookActivity, observationActivity));
        }
        itemBuilder.sort(ActivityPairingResult.COMPARATOR);
        if (itemBuilder.isEmpty() && logbookActivity.getRelatedObservedActivity() == null) {
            // skip this activity, no data to set at all
            return null;
        }
        return new ActivityPairingResult(logbookActivity, itemBuilder);
    }

    private boolean filterDate(fr.ird.observe.dto.data.ll.observation.ActivityReference ref, String vesselActivityId, long minDate, long maxDate) {
        long date = ref.getDate().getTime();
        return vesselActivityId.equals(ref.getVesselActivityId()) && minDate <= date && date <= maxDate;
    }
}
