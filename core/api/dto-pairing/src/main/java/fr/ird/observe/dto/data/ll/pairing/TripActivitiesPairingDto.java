package fr.ird.observe.dto.data.ll.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.common.TripReference;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.List;

/**
 * Contains all the stuff to perform a trip activities pairing action.
 * <p>
 * Created at 19/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class TripActivitiesPairingDto implements JsonAware {
    private final TripReference trip;
    private final List<fr.ird.observe.dto.data.ll.logbook.ActivityReference> logbookActivities;
    private final List<fr.ird.observe.dto.data.ll.observation.ActivityReference> observationActivities;

    public TripActivitiesPairingDto(TripReference trip,
                                    List<fr.ird.observe.dto.data.ll.logbook.ActivityReference> logbookActivities,
                                    List<fr.ird.observe.dto.data.ll.observation.ActivityReference> observationActivities) {
        this.trip = trip;
        this.logbookActivities = logbookActivities;
        this.observationActivities = observationActivities;
    }

    public TripReference getTrip() {
        return trip;
    }

    public ActivityPairingDto toActivityPairingDto() {
        return new ActivityPairingDto(getObservationActivities());
    }

    public List<fr.ird.observe.dto.data.ll.observation.ActivityReference> getObservationActivities() {
        return observationActivities;
    }

    public List<fr.ird.observe.dto.data.ll.logbook.ActivityReference> getLogbookActivities() {
        return logbookActivities;
    }
}
