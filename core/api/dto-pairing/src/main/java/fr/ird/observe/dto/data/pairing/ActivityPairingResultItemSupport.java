package fr.ird.observe.dto.data.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.data.ActivityAware;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.lang.Numbers;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 20/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ActivityPairingResultItemSupport<O extends ActivityAware> extends AbstractJavaBean implements ObserveDto, Decorated {

    private final O observationActivity;
    private final long computedTime;
    private final float computedDistance;
    protected transient Decorator decorator;
    private String computedTimeStr;

    protected ActivityPairingResultItemSupport(ActivityAware activityLogbook, O observationActivity) {
        Date logbookTimeStamp = Objects.requireNonNull(activityLogbook).getTimeStamp();
        if (logbookTimeStamp == null) {
            logbookTimeStamp = Objects.requireNonNull(activityLogbook.getDate());
        }
        Date obsTimeStamp = Objects.requireNonNull(observationActivity).getTimeStamp();
        this.observationActivity = observationActivity;
        this.computedTime = TimeUnit.MINUTES.convert(logbookTimeStamp.getTime() - obsTimeStamp.getTime(), TimeUnit.MILLISECONDS);
        this.computedDistance = Numbers.roundTwoDigits((float) activityLogbook.getGPSPoint().getDistanceInKm(observationActivity.getGPSPoint()));
    }

    @Override
    public final Optional<Decorator> decorator() {
        return Optional.ofNullable(decorator);
    }

    @Override
    public final void registerDecorator(Decorator decorator) {
        this.decorator = Objects.requireNonNull(decorator);
    }

    public final O getObservationActivity() {
        return observationActivity;
    }

    public final long getComputedTime() {
        return computedTime;
    }

    public final float getComputedDistance() {
        return computedDistance;
    }

    public final String getComputedTimeStr() {
        if (computedTimeStr == null) {
            if (computedTime == 0) {
                return computedTimeStr = t("observe.data.pairing.ActivityPairingResultItem.computedTimeEquals");
            }
            if (computedTime < 0) {
                computedTimeStr = t("observe.data.pairing.ActivityPairingResultItem.computedTimeAfter") + " - ";
            } else {
                computedTimeStr = t("observe.data.pairing.ActivityPairingResultItem.computedTimeBefore") + " - ";
            }
            long days = TimeUnit.MINUTES.toDays(computedTime);
            long hours = TimeUnit.MINUTES.toHours(computedTime) % 24;
            long minutes = TimeUnit.MINUTES.toMinutes(computedTime) % 60;
            computedTimeStr += String.format("%s:%s:%s", Math.abs(days), Math.abs(hours), Math.abs(minutes));
        }
        return computedTimeStr;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityPairingResultItemSupport<?> that = (ActivityPairingResultItemSupport<?>) o;
        return Objects.equals(observationActivity, that.observationActivity);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(observationActivity);
    }

    @Override
    public final String toString() {
        if (decorator == null) {
            return super.toString();
        }
        return decorate();
    }
}
