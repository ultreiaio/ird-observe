package fr.ird.observe.dto.data.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.data.ActivityAware;

import java.util.List;
import java.util.Objects;

/**
 * Created on 20/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ActivityPairingResultSupport<ALogbook extends ActivityAware, AObservation extends ActivityAware, AResultItem extends ActivityPairingResultItemSupport<AObservation>> implements ObserveDto {

    private final ALogbook activityLogbook;
    private final List<AResultItem> items;
    private final AResultItem selectedRelatedObservedActivity;

    public ActivityPairingResultSupport(ALogbook activityLogbook, List<AResultItem> items, AResultItem selectedRelatedObservedActivity) {
        this.activityLogbook = Objects.requireNonNull(activityLogbook);
        this.items = Objects.requireNonNull(items);
        this.selectedRelatedObservedActivity = selectedRelatedObservedActivity;
    }

    public final AResultItem getSelectedRelatedObservedActivity() {
        return selectedRelatedObservedActivity;
    }

    public final ALogbook getActivityLogbook() {
        return activityLogbook;
    }

    public final AObservation getActivityObservation(AResultItem item) {
        return item.getObservationActivity();
    }

    public final List<AResultItem> getItems() {
        return items;
    }
}
