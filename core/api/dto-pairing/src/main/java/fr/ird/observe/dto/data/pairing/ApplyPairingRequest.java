package fr.ird.observe.dto.data.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.json.JsonAware;

import java.util.TreeMap;

/**
 * Created on 20/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ApplyPairingRequest implements JsonAware {

    private final TreeMap<String, String> mapping;

    public ApplyPairingRequest(TreeMap<String, String> mapping) {
        this.mapping = mapping;
    }

    public TreeMap<String, String> getMapping() {
        return mapping;
    }

    public int size() {
        return mapping.size();
    }
}
