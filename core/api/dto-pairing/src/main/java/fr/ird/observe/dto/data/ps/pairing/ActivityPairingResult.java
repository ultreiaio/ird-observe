package fr.ird.observe.dto.data.ps.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.pairing.ActivityPairingResultSupport;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Comparator;
import java.util.List;

/**
 * Created by tchemit on 15/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
public class ActivityPairingResult extends ActivityPairingResultSupport<fr.ird.observe.dto.data.ps.logbook.ActivityReference, fr.ird.observe.dto.data.ps.observation.ActivityReference, ActivityPairingResultItem> {

    public static final Comparator<ActivityPairingResultItem> COMPARATOR = Comparator.comparingDouble(ActivityPairingResultItem::getComputedDistance).thenComparingDouble(ActivityPairingResultItem::getComputedTime);

    public static ActivityPairingResultItem getSelectedRelatedObservedActivity(fr.ird.observe.dto.data.ps.logbook.ActivityReference activityLogbook, List<ActivityPairingResultItem> items) {
        fr.ird.observe.dto.data.ps.observation.ActivityReference relatedObservedActivity = activityLogbook.getRelatedObservedActivity();
        return relatedObservedActivity == null
                ? null
                : items.stream().filter(s -> relatedObservedActivity.equals(s.getObservationActivity()))
                .findFirst()
                .orElse(new ActivityPairingResultItem(activityLogbook, relatedObservedActivity));
    }

    public ActivityPairingResult(fr.ird.observe.dto.data.ps.logbook.ActivityReference activityLogbook, List<ActivityPairingResultItem> items) {
        super(activityLogbook, items, getSelectedRelatedObservedActivity(activityLogbook, items));
    }

}
