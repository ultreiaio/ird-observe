package fr.ird.observe.dto.data.ps.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityReference;
import fr.ird.observe.dto.data.ps.observation.RouteReference;
import io.ultreia.java4all.util.Dates;
import org.apache.commons.lang3.time.DateUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Engine to perform activities pairing at route level.
 * <p>
 * Created on 21/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class RoutePairingEngine implements ObserveDto {

    private final RoutePairingDto data;
    private String lastRequest;
    private List<fr.ird.observe.dto.data.ps.observation.ActivityReference> candidates;

    public RoutePairingEngine(RoutePairingDto data) {
        this.data = data;
    }

    public List<fr.ird.observe.dto.data.ps.observation.ActivityReference> getObservationActivitiesCandidates(String vesselActivityId, Date date) {
        if (date == null || vesselActivityId == null) {
            return Collections.emptyList();
        }
        if (!Objects.equals(vesselActivityId, lastRequest)) {
            boolean reloadCandidates = updateLastRequest(vesselActivityId);
            if (reloadCandidates) {
                reloadCandidates(vesselActivityId);
            }
        }
        return new ArrayList<>(candidates);
    }

    public List<ActivityPairingResult> computeResult(List<ActivityReference> logbookActivities) {
        List<ActivityPairingResult> result = new LinkedList<>();

        for (ActivityReference logbookActivity : logbookActivities) {
            ActivityPairingResult activityPairingResult = computeResult(logbookActivity);
            if (activityPairingResult != null) {
                // only add if there is some data
                result.add(activityPairingResult);
            }
        }
        return result;
    }

    public Stream<RouteReference> getObservationRoutesCandidates() {
        Date date = data.getLogbookRoute().getDate();
        long minRouteDate = DateUtils.addDays(Dates.getDay(date), -1).getTime();
        long maxRouteDate = DateUtils.addDays(Dates.getEndOfDay(date), 1).getTime();
        return data.getObservationRoutes().keySet().stream().filter(r -> filterDate(r.getDate(), minRouteDate, maxRouteDate));
    }

    public Stream<fr.ird.observe.dto.data.ps.observation.ActivityReference> getObservationRouteActivities(fr.ird.observe.dto.data.ps.observation.RouteReference route, String vesselActivityId) {
        return data.getObservationRoutes().get(route).stream().filter(a -> vesselActivityId.equals(a.getVesselActivityId()));
    }

    private ActivityPairingResult computeResult(ActivityReference logbookActivity) {
        List<fr.ird.observe.dto.data.ps.observation.ActivityReference> observationActivities = getObservationActivitiesCandidates(logbookActivity.getVesselActivityId(), logbookActivity.getDate());
        List<ActivityPairingResultItem> itemBuilder = new ArrayList<>(observationActivities.size());
        for (fr.ird.observe.dto.data.ps.observation.ActivityReference observationActivity : observationActivities) {
            itemBuilder.add(new ActivityPairingResultItem(logbookActivity, observationActivity));
        }
        if (itemBuilder.isEmpty() && logbookActivity.getRelatedObservedActivity() == null) {
            // skip this activity, no data to set at all
            return null;
        }
        itemBuilder.sort(ActivityPairingResult.COMPARATOR);
        return new ActivityPairingResult(logbookActivity, itemBuilder);
    }

    private boolean updateLastRequest(String vesselActivityId) {
        if (lastRequest != null) {
            if (candidates != null && !Objects.equals(lastRequest, vesselActivityId)) {
                candidates = null;
            }
        }
        if (candidates == null) {
            return true;
        }
        lastRequest = vesselActivityId;
        return false;
    }

    private void reloadCandidates(String vesselActivityId) {
        List<fr.ird.observe.dto.data.ps.observation.ActivityReference> result = new LinkedList<>();
        getObservationRoutesCandidates().forEach(r -> {
            Date routeDate = r.getDate();
            getObservationRouteActivities(r, vesselActivityId).forEach(a -> {
                a.setDate(routeDate);
                result.add(a);
            });
        });
        candidates = result;
    }

    private boolean filterDate(Date date, long minDate, long maxDate) {
        long time = date.getTime();
        return minDate <= time && time <= maxDate;
    }

}
