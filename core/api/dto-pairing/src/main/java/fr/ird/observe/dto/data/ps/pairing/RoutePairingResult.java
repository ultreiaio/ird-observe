package fr.ird.observe.dto.data.ps.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.data.ps.logbook.RouteReference;

import java.util.List;

/**
 * Created on 24/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public class RoutePairingResult implements ObserveDto {

    private final RouteReference route;
    private final List<ActivityPairingResult> items;

    public RoutePairingResult(RouteReference route, List<ActivityPairingResult> items) {
        this.route = route;
        this.items = items;
    }

    public RouteReference getRoute() {
        return route;
    }

    public List<ActivityPairingResult> getItems() {
        return items;
    }
}
