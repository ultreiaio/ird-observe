package fr.ird.observe.dto.data.ps.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.common.TripReference;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Contains all data needed to perform activities pairing action on one trip.
 * <p>
 * Created at 18/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class TripActivitiesPairingDto implements JsonAware {
    private final TripReference trip;
    private final Map<fr.ird.observe.dto.data.ps.logbook.RouteReference, List<fr.ird.observe.dto.data.ps.logbook.ActivityReference>> logbookRoutes;
    private final Map<fr.ird.observe.dto.data.ps.observation.RouteReference, List<fr.ird.observe.dto.data.ps.observation.ActivityReference>> observationRoutes;

    public TripActivitiesPairingDto(TripReference trip,
                                    Map<fr.ird.observe.dto.data.ps.logbook.RouteReference, List<fr.ird.observe.dto.data.ps.logbook.ActivityReference>> logbookRoutes,
                                    Map<fr.ird.observe.dto.data.ps.observation.RouteReference, List<fr.ird.observe.dto.data.ps.observation.ActivityReference>> observationRoutes) {
        this.trip = trip;
        this.logbookRoutes = logbookRoutes;
        this.observationRoutes = observationRoutes;
    }

    public TripReference getTrip() {
        return trip;
    }

    public RoutePairingDto toRoutePairingDto(fr.ird.observe.dto.data.ps.logbook.RouteReference routeLogbook) {
        return new RoutePairingDto(routeLogbook, observationRoutes);
    }

    public Set<fr.ird.observe.dto.data.ps.logbook.RouteReference> getLogbookRoutes() {
        return logbookRoutes.keySet();
    }

    public List<fr.ird.observe.dto.data.ps.logbook.ActivityReference> getLogbookRouteActivities(fr.ird.observe.dto.data.ps.logbook.RouteReference route) {
        return logbookRoutes.get(route);
    }
}
