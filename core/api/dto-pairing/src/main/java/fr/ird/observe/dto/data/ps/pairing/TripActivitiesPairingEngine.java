package fr.ird.observe.dto.data.ps.pairing;

/*-
 * #%L
 * ObServe Core :: API :: Dto Pairing
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityReference;
import fr.ird.observe.dto.data.ps.logbook.RouteReference;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Engine to perform activities pairing at trip level.
 * <p>
 * Created on 21/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripActivitiesPairingEngine implements ObserveDto {

    private final TripActivitiesPairingDto data;

    public TripActivitiesPairingEngine(TripActivitiesPairingDto data) {
        this.data = Objects.requireNonNull(data);
    }

    public TripActivitiesPairingResult computeResult() {
        List<RoutePairingResult> routeBuilder = new LinkedList<>();
        for (RouteReference routeLogbook : data.getLogbookRoutes()) {
            RoutePairingResult routeResult = computeRouteResult(routeLogbook);
            if (routeResult != null) {
                // only add if there is some data
                routeBuilder.add(routeResult);
            }
        }
        return new TripActivitiesPairingResult(data.getTrip(), routeBuilder);
    }

    private RoutePairingResult computeRouteResult(RouteReference logbookRoute) {
        RoutePairingEngine routePairingEngine = new RoutePairingEngine(data.toRoutePairingDto(logbookRoute));
        List<ActivityReference> logbookActivities = data.getLogbookRouteActivities(logbookRoute);
        List<ActivityPairingResult> result = routePairingEngine.computeResult(logbookActivities);

        if (!result.isEmpty()) {
            // only add if there is some data
            return new RoutePairingResult(logbookRoute, result);
        }
        return null;
    }

}
