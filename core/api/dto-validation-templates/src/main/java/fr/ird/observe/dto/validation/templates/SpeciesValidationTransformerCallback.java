package fr.ird.observe.dto.validation.templates;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation Templates
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.toolkit.templates.validation.ValidationTransformerCallback;
import fr.ird.observe.toolkit.templates.validation.ValidatorTransformer;
import fr.ird.observe.validation.ObserveValidationTagValues;
import io.ultreia.java4all.validation.api.NuitonValidatorFileInfo;
import org.nuiton.eugene.java.BeanTransformerContext;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.tagvalue.ObjectModelTagValuesStore;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created at 09/10/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
@AutoService(ValidationTransformerCallback.class)
public class SpeciesValidationTransformerCallback implements ValidationTransformerCallback {
    private static final String SPECIES_WEIGHT_FIELD_TEMPLATE = ValidatorTransformer.loadTemplate(SpeciesValidationTransformerCallback.class, "SPECIES_WEIGHT_FIELD_TEMPLATE");
    private static final String SPECIES_LENGTH_FIELD_TEMPLATE = ValidatorTransformer.loadTemplate(SpeciesValidationTransformerCallback.class, "SPECIES_LENGTH_FIELD_TEMPLATE");
    private final ObserveValidationTagValues observeValidationTagValues = new ObserveValidationTagValues();

    @Override
    public void prepareBean(ValidatorTransformer transformer,
                            BeanTransformerContext context,
                            ProjectPackagesDefinition def,
                            ObjectModel model,
                            ObjectModelClass beanClass,
                            Collection<ObjectModelAttribute> attributes,
                            List<NuitonValidatorFileInfo> userValidators,
                            Class<?> dtoClazz) {

        ObjectModelTagValuesStore tagValuesStore = model.getTagValuesStore();

        Map<String, String> speciesWeight = ValidatorTransformer.getStringProperties(beanClass, attributes, (c, a) -> observeValidationTagValues.getSpeciesWeight(tagValuesStore, c, a));
        Map<String, String> speciesLength = ValidatorTransformer.getStringProperties(beanClass, attributes, (c, a) -> observeValidationTagValues.getSpeciesLength(tagValuesStore, c, a));

        addSpecies(transformer, speciesLength, SPECIES_LENGTH_FIELD_TEMPLATE);
        addSpecies(transformer, speciesWeight, SPECIES_WEIGHT_FIELD_TEMPLATE);
    }

    private void addSpecies(ValidatorTransformer transformer, Map<String, String> fields, String template) {
        for (Map.Entry<String, String> entry : fields.entrySet()) {
            String value = entry.getValue();
            boolean warning = value.startsWith(":");
            if (warning) {
                value = value.substring(1);
            }
            String field = entry.getKey();
            String content = String.format(template, field, value, ValidatorTransformer.removeVariables(value));
            if (warning) {
                transformer.addWarningValidator(field, content);
            } else {
                transformer.addErrorValidator(field, content);
            }
        }
    }
}
