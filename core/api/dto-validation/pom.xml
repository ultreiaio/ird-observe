<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  ObServe Core :: API :: Dto Validation
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>fr.ird.observe</groupId>
    <artifactId>core-api</artifactId>
    <version>9.4.0-M3-SNAPSHOT</version>
  </parent>
  <artifactId>core-api-dto-validation</artifactId>
  <name>ObServe Core :: API :: Dto Validation</name>
  <description>ObServe Core API Dto Validation module</description>
  <properties>
    <eugene.templates>fr.ird.observe.toolkit.templates.GenerateDtoValidation</eugene.templates>
  </properties>
  <dependencies>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>core-api-dto</artifactId>
      <version>${project.version}</version>
    </dependency>
    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
    </dependency>
    <dependency>
      <groupId>fr.ird.observe</groupId>
      <artifactId>toolkit-api</artifactId>
    </dependency>
    <dependency>
      <groupId>fr.ird.observe</groupId>
      <artifactId>toolkit-api-decoration</artifactId>
    </dependency>
    <dependency>
      <groupId>fr.ird.observe</groupId>
      <artifactId>toolkit-api-validation</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all</groupId>
      <artifactId>java-bean</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all</groupId>
      <artifactId>java-lang</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all</groupId>
      <artifactId>java-util</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all.decorator</groupId>
      <artifactId>decorator-api</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all.i18n</groupId>
      <artifactId>i18n-runtime</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all.validation</groupId>
      <artifactId>api</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all.validation.impl</groupId>
      <artifactId>java</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all.validation.impl</groupId>
      <artifactId>java-validator</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api</artifactId>
    </dependency>
    <dependency>
      <groupId>com.google.auto.service</groupId>
      <artifactId>auto-service</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.google.auto.service</groupId>
      <artifactId>auto-service-annotations</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
      <scope>provided</scope>
    </dependency>
  </dependencies>
  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>io.ultreia.java4all.eugene</groupId>
          <artifactId>eugene-maven-plugin</artifactId>
          <dependencies>
            <dependency>
              <groupId>${project.groupId}</groupId>
              <artifactId>core-api-dto-decoration</artifactId>
              <version>${project.version}</version>
            </dependency>
          </dependencies>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <plugin>
        <groupId>fr.ird.observe</groupId>
        <artifactId>toolkit-maven-plugin</artifactId>
        <dependencies>
          <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>model</artifactId>
            <version>${project.version}</version>
          </dependency>
        </dependencies>
        <executions>
          <execution>
            <id>generate</id>
            <goals>
              <goal>generate-dto-validation-context</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</project>
