package fr.ird.observe.dto.validation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.referential.common.SpeciesDto;
import fr.ird.observe.dto.referential.ps.common.ObservedSystemReference;
import fr.ird.observe.navigation.id.Project;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Validation context support.
 * <p>
 * The validation context is injected in the validation stack.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
@SuppressWarnings("unused")
public abstract class DtoValidationContext extends GeneratedDtoValidationContextSupport {

    public static final String CURRENT_LL_LOGBOOK_CATCH_SPECIES_ID = "currentLlLogbookCatchSpeciesId";
    private List<ObservedSystemReference> psCommonObservedSystems;
    private List<String> currentLlLogbookCatchSpeciesId;
    private String psWellRegex;
    private String llWellRegex;

    protected DtoValidationContext(ValidationRequestConfiguration configuration, DecoratorService decoratorService, Project selectModel) {
        super(configuration, decoratorService, selectModel);
    }

    protected abstract Supplier<List<ObservedSystemReference>> psCommonObservedSystemSupplier();

    protected abstract Supplier<List<String>> currentLlLogbookCatchSpeciesIdSupplier();

    protected abstract Function<String, SpeciesDto> speciesFunction();

    public String getPsWellRegex() {
        return psWellRegex == null ? psWellRegex = psWellRegexSupplier().get() : psWellRegex;
    }

    public String getLlWellRegex() {
        return llWellRegex == null ? llWellRegex = llWellRegexSupplier().get() : llWellRegex;
    }

    public List<String> getCurrentLlLogbookCatchSpeciesId() {
        return currentLlLogbookCatchSpeciesId == null ? currentLlLogbookCatchSpeciesId = currentLlLogbookCatchSpeciesIdSupplier().get() : currentLlLogbookCatchSpeciesId;
    }

    protected Supplier<String> psWellRegexSupplier() {
        return () -> Optional.ofNullable(getCurrentPsCommonTrip().getVessel().getWellRegex()).orElse(".*");
    }

    protected Supplier<String> llWellRegexSupplier() {
        return () -> Optional.ofNullable(getCurrentLlCommonTrip().getVessel().getWellRegex()).orElse(".*");
    }

    @Override
    public ValidationRequestConfiguration getConfiguration() {
        return (ValidationRequestConfiguration) super.getConfiguration();
    }

    @Override
    public void reset() {
        super.reset();
        this.psCommonObservedSystems = null;
        this.psWellRegex = null;
        this.llWellRegex = null;
        this.currentLlLogbookCatchSpeciesId = null;
    }

    public final List<ObservedSystemReference> getPsCommonObservedSystems() {
        return psCommonObservedSystems == null ? psCommonObservedSystems = psCommonObservedSystemSupplier().get() : psCommonObservedSystems;
    }

    public final SpeciesDto getSpecies(String speciesId) {
        return getDto(speciesFunction(), speciesId);
    }

    //FIXME Improve this?
    public abstract TripValidationHelper getLlCommonTripService();

    //FIXME Improve this?
    public abstract TripValidationHelper getPsCommonTripService();
}
