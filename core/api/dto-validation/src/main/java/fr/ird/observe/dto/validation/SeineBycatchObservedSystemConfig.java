package fr.ird.observe.dto.validation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;

import java.util.Collection;

/**
 * Created by tchemit on 03/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SeineBycatchObservedSystemConfig {

    private ArrayListMultimap<String, String> data;

    public Collection<String> getRequiredObservedSystemBySpeciesId(String speciesId) {
        return data.get(speciesId);
    }

    public ArrayListMultimap<String, String> getData() {
        return data;
    }

    public void setData(ArrayListMultimap<String, String> data) {
        this.data = data;
    }
}
