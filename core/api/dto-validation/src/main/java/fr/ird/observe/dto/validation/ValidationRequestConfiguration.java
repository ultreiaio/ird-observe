package fr.ird.observe.dto.validation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.validation.api.request.ValidationRequestConfigurationSupport;

/**
 * La requète de validation des données.
 * <p>
 * Created on 02/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ValidationRequestConfiguration extends ValidationRequestConfigurationSupport {

    private boolean validationSpeedEnable;
    private boolean validationLengthWeightEnable;
    private Float validationSpeedMaxValue;
    private SeineBycatchObservedSystemConfig seineBycatchObservedSystemConfig;

    public boolean isValidationSpeedEnable() {
        return validationSpeedEnable;
    }

    public void setValidationSpeedEnable(boolean validationSpeedEnable) {
        this.validationSpeedEnable = validationSpeedEnable;
    }

    public boolean isValidationLengthWeightEnable() {
        return validationLengthWeightEnable;
    }

    public void setValidationLengthWeightEnable(boolean validationLengthWeightEnable) {
        this.validationLengthWeightEnable = validationLengthWeightEnable;
    }

    public Float getValidationSpeedMaxValue() {
        return validationSpeedMaxValue;
    }

    public void setValidationSpeedMaxValue(Float validationSpeedMaxValue) {
        this.validationSpeedMaxValue = validationSpeedMaxValue;
    }

    public SeineBycatchObservedSystemConfig getSeineBycatchObservedSystemConfig() {
        return seineBycatchObservedSystemConfig;
    }

    public void setSeineBycatchObservedSystemConfig(SeineBycatchObservedSystemConfig seineBycatchObservedSystemConfig) {
        this.seineBycatchObservedSystemConfig = seineBycatchObservedSystemConfig;
    }
}
