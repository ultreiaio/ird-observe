package fr.ird.observe.dto.validation.validator;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.validation.DtoValidationContext;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.FieldValidatorFunction;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldExpressionValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * <!-- START SNIPPET: javadoc -->
 * Ce validateur vérifie qu'une espèce est bien les tailles ou
 * les poids définis par les bornes de l'espèce :
 * <ul>
 * <li>minLength</li>
 * <li>maxLength</li>
 * <li>minWeight</li>
 * <li>maxWeight</li>
 * </ul>
 * <p>
 * Lorsqu'il s'agit d'une espèce faune, si aucune borne n'est trouvée, alors
 * on se base sur les bornes définis dans son groupe d'espèce (si il est défini).
 * <p>
 * Le paramètre {@link #ratio} permet de spécifier une marge à appliquer sur
 * les bornes, il s'agit d'un pourcentage décimal.
 * <p>
 * Example : si ratio = 10, alors on utilise les bornes suivantes :
 * <pre>
 * bMin -10% et bMax + 10%
 * </pre>
 * <!-- END SNIPPET: javadoc -->
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public abstract class AbstractSpeciesFieldDtoValidator<O extends JavaBean> extends FieldExpressionValidator<O, Number> {
    private static final Logger log = LogManager.getLogger(AbstractSpeciesFieldDtoValidator.class);
    /**
     * Field name where to find species value.
     */
    private final String speciesField;
    /**
     * Ration to apply to range of species value.
     */
    private final Float ratio;
    /**
     * FIXME!!!
     * If set to not {@code null}, then will be enable only if {@link #validationLengthWeightEnable} is set the exact same value.
     * Otherwise will always try to validate.
     *
     * @see #validationLengthWeightEnable
     * @see #shouldValidateFromEnable(DtoValidationContext, JavaBean)
     */
    private Boolean enable;
    /**
     * Loaded from validation context.
     * Permits to enable or not this validator.
     *
     * @see #enable
     * @see #shouldValidateFromEnable(DtoValidationContext, JavaBean)
     */
    private transient Boolean validationLengthWeightEnable;
    /**
     * Bound found from species
     *
     * @see SpeciesReference
     */
    private Bound bound;
    /**
     * Computed bound with {@link #ratio} applied to {@link #bound}.
     */
    private Bound computedBound;

    public AbstractSpeciesFieldDtoValidator(String fieldName,
                                            Function<O, Number> fieldFunction,
                                            String messageKey,
                                            MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder,
                                            BiFunction<O, NuitonValidationContext, Boolean> skipFunction,
                                            FieldValidatorFunction<O, ? super NuitonValidationContext, ? super FieldExpressionValidator<O, ?>, Boolean> expressionFunction,
                                            String speciesField,
                                            Float ratio) {
        super(fieldName, fieldFunction, messageKey, messageBuilder, skipFunction, expressionFunction);
        this.speciesField = speciesField;
        this.ratio = ratio;
    }

    protected abstract Float getBoundMin(SpeciesReference species);

    protected abstract Float getBoundMax(SpeciesReference species);

    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
//        if (ratio == null) {
//            throw new ValidationException("No parameter 'ratio' filled");
//        }
//
//        String fieldName = getFieldName();
//        if (fieldName == null) {
//            throw new ValidationException("No parameter 'fieldName' filled");
//        }
//
//        if (speciesField == null) {
//            throw new ValidationException("No parameter 'speciesField' filled");
//        }

        boolean shouldValidate = shouldValidate((DtoValidationContext) validationContext, object);

        if (!shouldValidate) {
            return;
        }

        // data to validate
        Number value = getField(object);
        Float data = value == null ? null : Float.valueOf(String.valueOf(value));

        if (data == null) {
            // no data found, can not validate
            log.debug("No data found, skip validate.");
            return;
        }

        log.debug(String.format("data to validate : %s", data));

        SpeciesReference speciesRef = object.get(speciesField);

        if (speciesRef == null) {

            // no species found, can not validate
            log.debug("No species found, skip validate.");
            return;
        }

        log.debug(String.format("Species to validate : %s", speciesRef));

        bound = getBound(speciesRef);

        log.debug(String.format("Species Bound to validate : %s", bound));

        if (bound == null) {

            // no bound found, can not validate
            log.debug("No species bound found, skip validate.");
            return;
        }

        computedBound = bound.applyRatio(ratio);
        log.debug(String.format("Bound with ratio  : %s", computedBound));

        boolean valid = computedBound.validate(data);

        if (!valid) {
            addMessage(validationContext, messagesCollector, getMessage(object, validationContext));
        }
    }

    public void setEnable(String enable) {
        this.enable = Boolean.parseBoolean(Objects.requireNonNull(enable));
    }

    public Float getMin() {
        return computedBound == null ? null : Numbers.roundFourDigits(computedBound.getMin());
    }

    public Float getMax() {
        return computedBound == null ? null : Numbers.roundFourDigits(computedBound.getMax());
    }

    private Boolean getValidationLengthWeightEnable(DtoValidationContext validationContext, Object object) {
        if (validationLengthWeightEnable == null) {
            validationLengthWeightEnable = validationContext.getConfiguration().isValidationLengthWeightEnable();
        }
        return validationLengthWeightEnable;
    }

    private boolean shouldValidate(DtoValidationContext validationContext, O object) {
        boolean shouldValidate = shouldValidateFromEnable(validationContext, object);
        if (!shouldValidate) {
            log.debug("Skip speed validation from 'configuration.validationLengthWeightEnable'");
        } else {
            shouldValidate = evaluateExpressionParameter(object, validationContext);

            if (!shouldValidate) {
                log.debug("Skip speed validation from 'expression'");
            }
        }
        return shouldValidate;
    }

    private boolean shouldValidateFromEnable(DtoValidationContext validationContext, O object) {
        boolean answer = true;
        if (this.enable != null) {
            Boolean enable = getValidationLengthWeightEnable(validationContext, object);
            answer = Objects.equals(this.enable, enable);
        }
        return answer;
    }

    private Bound getBound(SpeciesReference species) {

        Float min = getBoundMin(species);
        Float max = getBoundMax(species);

        if (min == null || min == 0 || max == null || max == 0) {
            // one of range is missing, can not use this bound
            return null;
        }
        return new Bound(min, max);
    }

    public static class Bound {

        private final float min;

        private final float max;

        Bound(float min, float max) {
            this.min = min;
            this.max = max;
        }

        public float getMin() {
            return min;
        }

        public float getMax() {
            return max;
        }

        boolean validate(float value) {
            return min <= value && value <= max;
        }

        Bound applyRatio(float ratio) {
            float delta = min / 100 * ratio;
            float min = this.min - delta;
            if (min < 0) {
                min = 0f;
            }
            delta = max / 100 * ratio;
            float max = this.max + delta;
            return new Bound(min, max);
        }

        @Override
        public String toString() {
            return super.toString() + '<' + min + ',' + max + '>';
        }
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends FieldExpressionValidator.Generator {
        public Generator() {
            super(AbstractSpeciesFieldDtoValidator.class);
        }

        @Override
        public boolean accept(Class<?> validationType) {
            return getValidatorType().isAssignableFrom(validationType);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            result.add(ExpressionResolver.escapeString(validatorDefinition.getOptionalParameter("speciesField").orElse("species")));
            result.add(escapeFloat(validatorDefinition.getParameter("ratio")));
        }
    }

}
