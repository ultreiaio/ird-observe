package fr.ird.observe.dto.validation.validator;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.WithStartEndDate;
import fr.ird.observe.dto.validation.DtoValidationContext;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.SkipableFieldValidatorSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created on 1/23/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0.1
 */
public class ObserveLengthFormulaCollectionUniqueKeyDtoValidator<O extends WithStartEndDate & JavaBean, F> extends SkipableFieldValidatorSupport<O, F> {

    private static final Logger log = LogManager.getLogger(ObserveLengthFormulaCollectionUniqueKeyDtoValidator.class);

    private final List<String> keys;

    public ObserveLengthFormulaCollectionUniqueKeyDtoValidator(String fieldName,
                                                               Function<O, F> fieldFunction,
                                                               String messageKey,
                                                               MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder,
                                                               BiFunction<O, NuitonValidationContext, Boolean> skipFunction,
                                                               String keys) {
        super(fieldName, fieldFunction, messageKey, messageBuilder, skipFunction);
        this.keys = List.of(keys.split("\\s*,\\s*"));
    }

    public List<String> getKeys() {
        return keys;
    }


    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {

        if (keys == null || keys.isEmpty()) {
            throw new IllegalStateException("keys not defined");
        }

        @SuppressWarnings("unchecked") Collection<O> col = (Collection<O>) ((DtoValidationContext) validationContext).getEditingReferentielList();

        log.debug("collection found : " + col);
        log.debug("againstBean = " + object);

        boolean answer = true;

        Date startDate = object.getStartDate();
        Date endDate = object.getEndDate();

        String idToExclude = ((IdDto) object).getId();
        List<O> challenges = new LinkedList<>();
        for (O o : col) {

            String id = ((IdDto) o).getId();
            if (Objects.equals(idToExclude, id)) {
                // Do not treat again object
                continue;
            }

            if (equals(object, o)) {
                // same common key
                challenges.add(o);
                break;
            }
        }

        if (challenges.isEmpty()) {
            // no other entries with same common key
            return;
        }

        Iterator<O> iterator = challenges.iterator();
        while (answer && iterator.hasNext()) {
            WithStartEndDate challenge = iterator.next();
            Date challengeStartDate = challenge.getStartDate();
            Date challengeEndDate = challenge.getEndDate();

            int min = WithStartEndDate.START_DATE_COMPARATOR.compare(startDate, challengeStartDate);
            if (min == 0) {
                // same lower bound
                answer = false;
                continue;
            }
            if (min < 0) {
                // only valid if check upper bound is lower than challenge min bound
                answer = WithStartEndDate.END_DATE_COMPARATOR.compare(endDate, challengeStartDate) < 0;
                continue;
            }
            // check min is after challenge min
            answer = WithStartEndDate.END_DATE_COMPARATOR.compare(startDate, challengeEndDate) > 0;
        }

        if (!answer) {
            addMessage(validationContext, messagesCollector, getMessage(object, validationContext));
        }
    }

    protected boolean equals(O o1, O o2) {
        boolean equals = true;

        for (String key : keys) {

            Object property1 = o1.get(key);
            Object property2 = o2.get(key);

            if (property1 instanceof Date) {
                property1 = ((Date) property1).getTime();
            }
            if (property2 instanceof Date) {
                property2 = ((Date) property2).getTime();
            }

            equals = Objects.equals(property1, property2);

            if (!equals) {
                break;
            }
        }
        return equals;
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends SkipableFieldValidatorSupport.GeneratorSupport {
        public Generator() {
            super(ObserveLengthFormulaCollectionUniqueKeyDtoValidator.class);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            result.add(ExpressionResolver.escapeString(validatorDefinition.getParameter("keys")));
        }
    }

}
