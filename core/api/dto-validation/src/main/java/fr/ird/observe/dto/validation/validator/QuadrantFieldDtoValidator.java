package fr.ird.observe.dto.validation.validator;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.referential.common.OceanReference;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.List;
import java.util.function.Function;

/**
 * Created on 02/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class QuadrantFieldDtoValidator<O> extends FieldValidatorSupport<O, Integer> {

    private final Function<NuitonValidationContext, OceanReference> ocean;

    public QuadrantFieldDtoValidator(String fieldName, Function<O, Integer> fieldFunction, Function<NuitonValidationContext, OceanReference> ocean) {
        super(fieldName, fieldFunction);
        this.ocean = ocean;
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {

        Integer quadrant = getField(object);
        if (quadrant == null) {
            return;
        }
        OceanReference ocean = this.ocean.apply(validationContext);
        if (ocean == null) {
            return;
        }
        switch (quadrant) {
            case 1:
                if (!ocean.isNorthEastAllowed()) {
                    addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.quadrant.1.invalid"), ocean.getLabel());
                    return;
                }
                break;
            case 2:
                if (!ocean.isSouthEastAllowed()) {
                    addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.quadrant.2.invalid"), ocean.getLabel());
                    return;
                }
                break;
            case 3:
                if (!ocean.isSouthWestAllowed()) {
                    addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.quadrant.3.invalid"), ocean.getLabel());
                    return;
                }
                break;
            case 4:
                if (!ocean.isNorthWestAllowed()) {
                    addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.quadrant.4.invalid"), ocean.getLabel());
                    return;
                }
                break;
        }
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(QuadrantFieldDtoValidator.class, true, true, false, false);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            String ocean = validatorDefinition.getParameter("ocean");
            ExpressionResolver resolver = validationContextResolver(validationContextType);
            result.add("c -> " + resolver.resolve(ocean));
        }
    }
}
