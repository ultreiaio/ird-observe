package fr.ird.observe.dto.validation.validator;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.common.SpeciesReference;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.FieldValidatorFunction;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.validator.FieldExpressionValidator;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Validateur sur le weight d'une species.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class SpeciesWeightFieldDtoValidator<O extends JavaBean> extends AbstractSpeciesFieldDtoValidator<O> {
    public SpeciesWeightFieldDtoValidator(String fieldName,
                                          Function<O, Number> fieldFunction,
                                          String messageKey,
                                          MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder,
                                          BiFunction<O, NuitonValidationContext, Boolean> skipFunction,
                                          FieldValidatorFunction<O, ? super NuitonValidationContext, ? super FieldExpressionValidator<O, ?>, Boolean> expressionFunction,
                                          String speciesField,
                                          Float ratio) {
        super(fieldName, fieldFunction, messageKey, messageBuilder, skipFunction, expressionFunction, speciesField, ratio);
    }

    @Override
    protected Float getBoundMin(SpeciesReference species) {
        return species.getMinWeight();
    }

    @Override
    protected Float getBoundMax(SpeciesReference species) {
        return species.getMaxWeight();
    }

}
