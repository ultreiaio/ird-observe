package fr.ird.observe.dto.validation.validator.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.dto.validation.DtoValidationContext;
import fr.ird.observe.dto.validation.TripValidationHelper;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.Date;

/**
 * Created on 28/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public abstract class TripVesselValidatorSupport<O extends TripAware> extends FieldValidatorSupport<O, VesselReference> {
    public TripVesselValidatorSupport() {
        super("vessel", TripAware::getVessel);
    }

    protected void validate(TripValidationHelper tripService, O object, DtoValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        String id = object.getId();
        if (id == null) {
            // object not persisted, no ide, can't do the math
            return;
        }
        VesselReference vessel = getField(object);

        if (vessel == null) {
            // no vessel, can't validate
            return;
        }
        Date startDate = object.getStartDate();
        Date endDate = object.getEndDate();
        int matchingTrips = tripService.getMatchingTripsVesselWithinDateRange(id, vessel.getId(), startDate, endDate);
        if (matchingTrips > 0) {
            addMessage(validationContext, messagesCollector, I18n.n("observe.data.Trip.validation.vessel.overlap"));
        }
    }
}
