package fr.ird.observe.dto.validation.validator.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.data.WellIdAware;
import fr.ird.observe.validation.ValidationContextSupport;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * Created on 06/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public class WellIdFieldValidator<O extends WellIdAware> extends FieldValidatorSupport<O, String> {

    private final Function<ValidationContextSupport, String> patternFunction;

    public WellIdFieldValidator(Function<ValidationContextSupport, String> patternFunction) {
        super("well", WellIdAware::getWell);
        this.patternFunction = patternFunction;
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        if (object == null) {
            return;
        }
        String pattern = patternFunction.apply((ValidationContextSupport) validationContext);
        Pattern regex = Pattern.compile(pattern);

        String well = getField(object);
        if (well != null && !well.isBlank()) {
            if (!regex.matcher(well).find()) {
                addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.well.id.not.valid"), pattern);
            }
        }
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(WellIdFieldValidator.class, false, false, false, false);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            addPatternFunction(key, validatorDefinition, validationContextType, result);
        }

        protected void addPatternFunction(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, List<String> result) throws NoSuchMethodException {
            String pattern = validatorDefinition.getParameter("patternProperty").trim();
            ExpressionResolver resolver = beanAndValidationContextResolver(key.getBeanType(), validationContextType);
            result.add("c -> " + resolver.resolve(pattern));
        }
    }
}
