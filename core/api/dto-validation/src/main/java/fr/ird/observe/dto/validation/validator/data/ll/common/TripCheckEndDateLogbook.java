package fr.ird.observe.dto.validation.validator.data.ll.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.dto.data.ll.logbook.ActivityStubDto;
import fr.ird.observe.validation.validator.collection.CollectionFieldValidationSupport;
import fr.ird.observe.validation.validator.collection.CollectionValidationWalkerContext;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.Dates;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;

import java.util.Collection;
import java.util.Date;

/**
 * Created at 18/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class TripCheckEndDateLogbook extends CollectionFieldValidationSupport<TripDto, Date, ActivityStubDto> {
    public TripCheckEndDateLogbook() {
        super(TripDto.PROPERTY_END_DATE, TripDto::getEndDate, null);
    }

    @Override
    protected Collection<ActivityStubDto> getCollection(NuitonValidationContext validationContext, TripDto object) {
        return object.getActivityLogbook();
    }

    @Override
    protected boolean validateEntry(CollectionValidationWalkerContext<TripDto, ActivityStubDto> context, ActivityStubDto current) {
        return Dates.getDay(current.getStartTimeStamp()).getTime() <= context.getContainer().getEndDate().getTime() &&
                (current.getEndTimeStamp() ==null || Dates.getDay(current.getEndTimeStamp()).getTime() <= context.getContainer().getEndDate().getTime());
    }

    @Override
    protected void addError(NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector, CollectionValidationWalkerContext<TripDto, ActivityStubDto> context, TripDto object) {
        addMessage(validationContext, messagesCollector, I18n.n("observe.data.ll.common.Trip.validation.invalid.endDate.logbook"), context.getHumanIndex());
    }
}
