package fr.ird.observe.dto.validation.validator.data.ll.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.logbook.ActivityDto;
import fr.ird.observe.dto.data.ll.logbook.SampleDto;
import fr.ird.observe.dto.data.ll.logbook.SamplePartDto;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.validation.DtoValidationContext;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.validator.SkipableFieldValidatorSupport;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 28/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public abstract class CheckSamplePartSpeciesSupport<O> extends SkipableFieldValidatorSupport<O, List<SamplePartDto>> {

    public CheckSamplePartSpeciesSupport(Function<O, List<SamplePartDto>> fieldFunction) {
        super(SampleDto.PROPERTY_SAMPLE_PART, fieldFunction, null);
    }

    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        List<SamplePartDto> sampleParts = getField(object);
        Set<SpeciesReference> sampleSpecies = sampleParts.stream().map(SamplePartDto::getSpecies).filter(Objects::nonNull).collect(Collectors.toSet());
        if (sampleSpecies.isEmpty()) {
            return;
        }
        ActivityDto activity = ((DtoValidationContext) validationContext).getCurrentLlLogbookActivity();
        Collection<String> catchSpeciesIds;
        String messageKey;
        if (activity == null) {
            // Trip scope
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2314
            catchSpeciesIds = ((DtoValidationContext) validationContext).getCurrentLlLogbookCatchSpeciesId();
            messageKey = I18n.n("observe.data.Trip.validation.sample.species");
        } else {
            // Activity scope
            catchSpeciesIds = activity.getCatchSpeciesIds();
            messageKey = I18n.n("observe.data.ll.logbook.Activity.validation.sample.species");
        }
        Set<String> badSpecies = new LinkedHashSet<>();
        for (SpeciesReference aSampleSpecies : sampleSpecies) {
            if (!catchSpeciesIds.contains(aSampleSpecies.getId())) {
                badSpecies.add(aSampleSpecies.getFaoCode());
            }
        }
        if (!badSpecies.isEmpty()) {
            addMessage(validationContext, messagesCollector, messageKey, badSpecies);
        }
    }
}

