package fr.ird.observe.dto.validation.validator.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ll.observation.ActivityReference;
import fr.ird.observe.dto.data.ll.observation.SetDto;
import fr.ird.observe.dto.data.ll.observation.SetStubDto;
import fr.ird.observe.validation.ValidationContextSupport;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.Objects;
import java.util.Optional;

/**
 * Created on 12/7/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.9
 */
public class SetCheckUniqueNumber extends FieldValidatorSupport<SetDto, Integer> {

    public SetCheckUniqueNumber() {
        super(SetDto.PROPERTY_HOME_ID, SetDto::getNumber);
    }

    @Override
    public void validate(SetDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        Integer number = object.getNumber();
        if (number != null) {
            Optional<SetStubDto> sameNumberSetOptional = object.getOtherSets()
                                                               .stream()
                                                               .filter(t -> Objects.equals(t.getNumber(), number))
                                                               .findFirst();
            if (sameNumberSetOptional.isPresent()) {
                ActivityReference activity = sameNumberSetOptional.get().getActivity();
                DecoratorService decoratorService = ((ValidationContextSupport) validationContext).getDecoratorService();
                Decorator decorator = decoratorService.getDecoratorByType(ActivityReference.class);
                String duplicatedActivity = decorator.decorate(activity);
                addMessage(validationContext, messagesCollector, I18n.n("observe.data.ll.Set.validation.duplicated.number"), duplicatedActivity);
            }
        }
    }
}
