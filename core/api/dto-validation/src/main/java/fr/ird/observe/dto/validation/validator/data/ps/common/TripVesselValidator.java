package fr.ird.observe.dto.validation.validator.data.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.validation.DtoValidationContext;
import fr.ird.observe.dto.validation.TripValidationHelper;
import fr.ird.observe.dto.validation.validator.data.TripVesselValidatorSupport;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;

/**
 * Created on 28/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class TripVesselValidator extends TripVesselValidatorSupport<TripDto> {
    public TripVesselValidator() {
        super();
    }

    @Override
    public void validate(TripDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        TripValidationHelper tripService = ((DtoValidationContext) validationContext).getPsCommonTripService();
        validate(tripService, object, (DtoValidationContext) validationContext, messagesCollector);
    }
}
