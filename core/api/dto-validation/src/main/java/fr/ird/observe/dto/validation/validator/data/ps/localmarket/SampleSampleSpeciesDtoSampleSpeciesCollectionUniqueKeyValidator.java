package fr.ird.observe.dto.validation.validator.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.localmarket.SampleSampleSpeciesDto;
import fr.ird.observe.dto.data.ps.localmarket.SampleSpeciesDto;
import fr.ird.observe.validation.validator.collection.AbstractCollectionUniqueKeyValidator;
import io.ultreia.java4all.i18n.I18n;
import javax.annotation.Generated;

@Generated(value = "fr.ird.observe.toolkit.templates.validation.GenerateCollectionUniqueKeyValidators")
public class SampleSampleSpeciesDtoSampleSpeciesCollectionUniqueKeyValidator extends AbstractCollectionUniqueKeyValidator<SampleSampleSpeciesDto, SampleSpeciesDto> {

    public SampleSampleSpeciesDtoSampleSpeciesCollectionUniqueKeyValidator() {
        super("sampleSpecies", SampleSampleSpeciesDto::getSampleSpecies, I18n.n("observe.data.ps.localmarket.SampleSampleSpecies.sampleSpecies.validation.uniqueKey"));
    }

    @Override
    protected java.lang.String computeUniqueKey(fr.ird.observe.dto.data.ps.localmarket.SampleSpeciesDto collectionValue) {
        StringBuilder builder = new StringBuilder();
        addComponentValue(builder, collectionValue.getSpecies());
        addComponentValue(builder, collectionValue.getSizeMeasureType());
        return builder.substring(1);
    }

} //SampleSampleSpeciesDtoSampleSpeciesCollectionUniqueKeyValidator
