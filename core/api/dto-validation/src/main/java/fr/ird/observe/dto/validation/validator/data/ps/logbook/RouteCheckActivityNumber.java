package fr.ird.observe.dto.validation.validator.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.logbook.ActivityDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.data.ps.logbook.RouteDto;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Check that the {@link ActivityDto#getNumber()} follows a sequence starting at {@code 1}.
 * <p>
 * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2783">issue 2783</a>
 * <p>
 * Created at 14/09/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class RouteCheckActivityNumber extends FieldValidatorSupport<RouteDto, Collection<ActivityStubDto>> {

    public RouteCheckActivityNumber() {
        super(RouteDto.PROPERTY_ACTIVITY, RouteDto::getActivity);
    }

    @Override
    public void validate(RouteDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        Collection<ActivityStubDto> activities = getField(object);
        List<Integer> actualNumberOrder = activities.stream().map(ActivityStubDto::getNumber).sorted().collect(Collectors.toList());
        List<Integer> expectedNumberOrder = IntStream.rangeClosed(1, activities.size()).boxed().collect(Collectors.toList());
        if (!actualNumberOrder.equals(expectedNumberOrder)) {
            addMessage(validationContext, messagesCollector, I18n.n("observe.data.ps.logbook.Route.activity.validation.badNumberSequence"), expectedNumberOrder, actualNumberOrder);
        }
    }
}
