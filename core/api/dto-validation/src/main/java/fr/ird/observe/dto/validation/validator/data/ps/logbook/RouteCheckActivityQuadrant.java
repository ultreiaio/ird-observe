package fr.ird.observe.dto.validation.validator.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.data.ps.logbook.RouteDto;
import fr.ird.observe.dto.referential.common.OceanReference;
import fr.ird.observe.dto.validation.DtoValidationContext;
import fr.ird.observe.validation.validator.collection.CollectionFieldValidationSupport;
import fr.ird.observe.validation.validator.collection.CollectionValidationWalkerContext;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;

import java.util.Collection;
import java.util.Objects;

/**
 * Created on 11/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class RouteCheckActivityQuadrant extends CollectionFieldValidationSupport<RouteDto, Collection<ActivityStubDto>, ActivityStubDto> {

    private String oceanCode;

    public RouteCheckActivityQuadrant() {
        super(RouteDto.PROPERTY_ACTIVITY, RouteDto::getActivity, null);
    }

    @Override
    protected void validateWhenNotSkip(RouteDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        TripDto trip = ((DtoValidationContext) validationContext).getCurrentPsCommonTrip();
        OceanReference ocean = trip.getOcean();
        if (ocean == null) {
            return;
        }
        oceanCode = ocean.getCode();
        super.validateWhenNotSkip(object, validationContext, messagesCollector);
    }

    @Override
    protected Collection<ActivityStubDto> getCollection(NuitonValidationContext validationContext, RouteDto object) {
        return getField(object);
    }

    @Override
    protected boolean processEntry(CollectionValidationWalkerContext<RouteDto, ActivityStubDto> context, ActivityStubDto current) {
        return !current.skipValidate();
    }

    @Override
    protected boolean validateEntry(CollectionValidationWalkerContext<RouteDto, ActivityStubDto> context, ActivityStubDto current) {
        return (Objects.equals(oceanCode, "3"))
                || (Objects.equals(oceanCode, "1"))
                || (Objects.equals(oceanCode, "2") && (current.getLongitude() >= 0));
    }

    @Override
    protected void addError(NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector, CollectionValidationWalkerContext<RouteDto, ActivityStubDto> context, RouteDto object) {
        addMessage(validationContext, messagesCollector, I18n.n("observe.data.ps.Route.validation.invalid.quadrant"), context.getHumanIndex(), oceanCode);
    }

}
