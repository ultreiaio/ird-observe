package fr.ird.observe.dto.validation.validator.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ps.observation.ActivityDto;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ps.common.ObservedSystemReference;
import fr.ird.observe.dto.validation.DtoValidationContext;
import fr.ird.observe.dto.validation.SeineBycatchObservedSystemConfig;
import fr.ird.observe.validation.ValidationContextSupport;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by tchemit on 15/06/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ActivityCheckObservedSystem extends FieldValidatorSupport<ActivityDto, Collection<ObservedSystemReference>> {

    public ActivityCheckObservedSystem() {
        super(ActivityDto.PROPERTY_OBSERVED_SYSTEM, ActivityDto::getObservedSystem);
    }

    @Override
    public void validate(ActivityDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        if (object == null) {
            return;
        }
        if (!object.getNonTargetCatchSpecies().isEmpty()) {
            List<ObservedSystemReference> observedSystemReferences = ((DtoValidationContext) validationContext).getPsCommonObservedSystems();
            Map<String, ObservedSystemReference> map = Maps.uniqueIndex(observedSystemReferences, ObservedSystemReference::getId);
            SeineBycatchObservedSystemConfig seineBycatchObservedSystemConfig = ((DtoValidationContext) validationContext).getConfiguration().getSeineBycatchObservedSystemConfig();
            if (seineBycatchObservedSystemConfig == null) {
                //FIXME Coming from public API, need to add default configuration on server config ?
                return;
            }
            DecoratorService decoratorService = ((ValidationContextSupport) validationContext).getDecoratorService();
            Decorator decorator = decoratorService.getDecoratorByType(ObservedSystemReference.class);
            Set<String> labels = new LinkedHashSet<>();
            for (SpeciesReference speciesReference : object.getNonTargetCatchSpecies()) {
                Collection<String> requiredObservedSystemBySpeciesId = seineBycatchObservedSystemConfig.getRequiredObservedSystemBySpeciesId(speciesReference.getTopiaId());
                if (!requiredObservedSystemBySpeciesId.isEmpty()) {
                    // must check that at least one of those observed system are in
                    boolean found = false;
                    for (ObservedSystemReference observedSystemReference : object.getObservedSystem()) {
                        if (requiredObservedSystemBySpeciesId.contains(observedSystemReference.getTopiaId())) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        for (String observedSystemId : requiredObservedSystemBySpeciesId) {
                            ObservedSystemReference ref = map.get(observedSystemId);
                            labels.add(decorator.decorate(ref));
                        }
                    }
                }
            }
            if (!labels.isEmpty()) {
                // there is some missing system observed
                addMessage(validationContext, messagesCollector, I18n.n("observe.data.ps.observation.Activity.validation.required.observedSystem.for.catches"), labels);

            }
        }
    }
}
