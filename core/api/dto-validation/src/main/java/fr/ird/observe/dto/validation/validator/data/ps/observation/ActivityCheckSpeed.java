package fr.ird.observe.dto.validation.validator.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.GPSPoint;
import fr.ird.observe.dto.data.ps.observation.ActivityDto;
import fr.ird.observe.dto.data.ps.observation.ActivityStubDto;
import fr.ird.observe.dto.data.ps.observation.RouteDto;
import fr.ird.observe.dto.validation.DtoValidationContext;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ActivityCheckSpeed extends FieldValidatorSupport<ActivityDto, Object> {

    private final boolean enable;

    public ActivityCheckSpeed(String fieldName, boolean enable) {
        super(fieldName, null);
        this.enable = enable;
    }

    static GPSPoint newPoint(RouteDto route, ActivityDto activity) {
        return GPSPoint.newPoint(route.getDate(), activity.getTime(), activity.getLatitude(), activity.getLongitude());
    }

    static GPSPoint newPoint(RouteDto route, ActivityStubDto activity) {
        return GPSPoint.newPoint(route.getDate(), activity.getTime(), activity.getLatitude(), activity.getLongitude());
    }

    @Override
    public void validate(ActivityDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        if (object == null) {
            return;
        }
        boolean enable = ((DtoValidationContext) validationContext).getConfiguration().isValidationSpeedEnable();
        if (this.enable != enable) {
            return;
        }
        Float speed = ((DtoValidationContext) validationContext).getConfiguration().getValidationSpeedMaxValue();
        if (object.getTime() == null) {
            return;
        }
        if (object.getLatitude() == null || object.getLongitude() == null) {
            return;
        }
        RouteDto route = ((DtoValidationContext) validationContext).getCurrentPsObservationRoute();
        if (route.getDate() == null) {
            return;
        }
        ActivityStubDto previousActivity;
        if (object.isNotPersisted()) {
            // since there is no id, can't use the later method, must find out by our self previous activity
            List<ActivityStubDto> activitySeine = new LinkedList<>(route.getActivity());
            ActivityStubDto activityStub = new ActivityStubDto();
            activityStub.setTime(object.getTime());
            activitySeine.add(activityStub);
            activitySeine.sort(Comparator.comparing(ActivityStubDto::getTimeSecond));
            int index = activitySeine.indexOf(activityStub);
            if (index > 0) {
                previousActivity = activitySeine.get(index - 1);
            } else {
                previousActivity = null;
            }
        } else {
            previousActivity = route.getPreviousActivity(object.getId());
        }
        if (previousActivity == null) {
            return;
        }
        if (previousActivity.getLatitude() == null || previousActivity.getLongitude() == null) {
            return;
        }
        GPSPoint currentPoint = newPoint(route, object);
        GPSPoint previousPoint = newPoint(route, previousActivity);
        float computedSpeed = previousPoint.getSpeed(currentPoint);
        boolean b = computedSpeed <= speed;
        if (!b) {
            addMessage(validationContext, messagesCollector, I18n.n("observe.data.ps.observation.Activity.validation.speed.bound"), computedSpeed, speed);
        }
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(ActivityCheckSpeed.class, true, false, false, false);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            result.add(validatorDefinition.getParameter("enabled"));
        }
    }
}
