package fr.ird.observe.dto.validation.validator.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ps.observation.ActivityDto;
import fr.ird.observe.dto.data.ps.observation.CatchDto;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ps.common.ObservedSystemReference;
import fr.ird.observe.dto.validation.DtoValidationContext;
import fr.ird.observe.dto.validation.SeineBycatchObservedSystemConfig;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by tchemit on 03/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CatchCheckObservedSystem extends FieldValidatorSupport<CatchDto, SpeciesReference> {

    public CatchCheckObservedSystem() {
        super(CatchDto.PROPERTY_SPECIES, CatchDto::getSpecies);
    }

    @Override
    public void validate(CatchDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {

        ActivityDto activitySeineDto = ((DtoValidationContext) validationContext).getCurrentPsObservationActivity();
        SpeciesReference speciesReference = getField(object);

        if (speciesReference == null) {
            return;
        }
        SeineBycatchObservedSystemConfig seineBycatchObservedSystemConfig = ((DtoValidationContext) validationContext).getConfiguration().getSeineBycatchObservedSystemConfig();
        if (seineBycatchObservedSystemConfig == null) {
            //FIXME Coming from public API, need to add default configuration on server config ?
            return;
        }
        Collection<String> requiredObservedSystemBySpeciesId = seineBycatchObservedSystemConfig.getRequiredObservedSystemBySpeciesId(speciesReference.getTopiaId());
        if (!requiredObservedSystemBySpeciesId.isEmpty()) {
            // must check that at least one of those observed system are in
            boolean found = false;
            for (ObservedSystemReference observedSystemReference : activitySeineDto.getObservedSystem()) {
                if (requiredObservedSystemBySpeciesId.contains(observedSystemReference.getTopiaId())) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                List<ObservedSystemReference> observedSystems = ((DtoValidationContext) validationContext).getPsCommonObservedSystems();
                DecoratorService decoratorService = ((DtoValidationContext) validationContext).getDecoratorService();
                Map<String, ObservedSystemReference> map = Maps.uniqueIndex(observedSystems, ObservedSystemReference::getId);
                Decorator decorator = decoratorService.getDecoratorByType(ObservedSystemReference.class);
                Set<String> labels = new LinkedHashSet<>();
                for (String observedSystemId : requiredObservedSystemBySpeciesId) {
                    ObservedSystemReference ref = map.get(observedSystemId);
                    labels.add(decorator.decorate(ref));
                }
                addMessage(validationContext, messagesCollector, I18n.n("observe.data.ps.observation.Catch.validation.missing.observedSystem"), labels);
            }
        }
    }
}
