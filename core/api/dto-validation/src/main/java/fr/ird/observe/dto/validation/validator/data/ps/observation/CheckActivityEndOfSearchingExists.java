package fr.ird.observe.dto.validation.validator.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.observation.ActivityDto;
import fr.ird.observe.dto.data.ps.observation.RouteDto;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.dto.validation.DtoValidationContext;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class CheckActivityEndOfSearchingExists extends FieldValidatorSupport<ActivityDto, VesselActivityReference> {

    private static final Logger log = LogManager.getLogger(CheckActivityEndOfSearchingExists.class);

    public CheckActivityEndOfSearchingExists() {
        super(ActivityDto.PROPERTY_VESSEL_ACTIVITY, ActivityDto::getVesselActivity, I18n.n("observe.data.ps.observation.Route.validation.activityEndOfSearching.notAvailable"), null);
    }

    @Override
    public void validate(ActivityDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        // on vérifie qu'il n'existe pas d'activité de fin de veille
        if (!object.isActivityEndOfSearching()) {
            // rien a valider (on est pas sur une activité de fin de veille)
            return;
        }
        // l'activité est une activité de fin de veille
        // on doit vérifier qu'il n'existe pas déjà une autre activité de fin de veille
        RouteDto route = ((DtoValidationContext) validationContext).getCurrentPsObservationRoute();
        if (route == null) {
            log.warn("COULD NOT FIND DATA CONTEXT! [currentPsObservationRoute]");
            return;
        }
        boolean detected = route.isActivityFindDeVeilleFound();
        boolean valid = !detected;
        if (!valid) {
            addMessage(validationContext, messagesCollector, getMessage(object, validationContext));
        }
    }
}
