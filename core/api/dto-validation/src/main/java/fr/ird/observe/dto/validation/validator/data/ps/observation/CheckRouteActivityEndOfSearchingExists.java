package fr.ird.observe.dto.validation.validator.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.observation.ActivityStubDto;
import fr.ird.observe.dto.data.ps.observation.RouteDto;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.Collection;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class CheckRouteActivityEndOfSearchingExists extends FieldValidatorSupport<RouteDto, Collection<ActivityStubDto>> {

    public CheckRouteActivityEndOfSearchingExists() {
        super(RouteDto.PROPERTY_ACTIVITY, RouteDto::getActivity, I18n.n("observe.data.ps.observation.Route.validation.missing.activityEndOfSearching"), null);
    }

    @Override
    public void validate(RouteDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        // on vérifie qu'il existe bien une activity de fin de veille parmi les activités de la route
        boolean detected = object.isActivityFindDeVeilleFound();
        if (!detected) {
            addMessage(validationContext, messagesCollector, getMessage(object, validationContext));
        }
    }
}
