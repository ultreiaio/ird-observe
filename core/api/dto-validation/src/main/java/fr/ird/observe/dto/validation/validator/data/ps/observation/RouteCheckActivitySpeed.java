package fr.ird.observe.dto.validation.validator.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.GPSPoint;
import fr.ird.observe.dto.data.ps.observation.ActivityStubDto;
import fr.ird.observe.dto.data.ps.observation.RouteDto;
import fr.ird.observe.dto.validation.DtoValidationContext;
import fr.ird.observe.validation.validator.collection.CollectionFieldValidationSupport;
import fr.ird.observe.validation.validator.collection.CollectionValidationWalkerContext;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;
import io.ultreia.java4all.validation.impl.java.validator.SkipableFieldValidatorSupport;

import java.util.Collection;
import java.util.List;

import static fr.ird.observe.dto.validation.validator.data.ps.observation.ActivityCheckSpeed.newPoint;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class RouteCheckActivitySpeed extends FieldValidatorSupport<RouteDto, Collection<ActivityStubDto>> {

    private final boolean enable;
    private CollectionFieldValidationSupport<RouteDto, Collection<ActivityStubDto>, ActivityStubDto> delegate;
    private Float speed;

    public RouteCheckActivitySpeed(boolean enable) {
        super(RouteDto.PROPERTY_ACTIVITY, RouteDto::getActivity);
        this.enable = enable;
    }

    public Float getSpeed() {
        return speed;
    }

    public boolean isEnable() {
        return enable;
    }

    private CollectionFieldValidationSupport<RouteDto, Collection<ActivityStubDto>, ActivityStubDto> getDelegate(DtoValidationContext validationContext, RouteDto route) {
        if (delegate == null) {
            delegate = new CollectionFieldValidationSupport<RouteDto, Collection<ActivityStubDto>, ActivityStubDto>(getFieldName(), RouteDto::getActivity, null) {
                private String invalidActivity;
                private Float computedSpeed;


                @Override
                protected Collection<ActivityStubDto> getCollection(NuitonValidationContext validationContext, RouteDto object) {
                    return getField(object);
                }

                @Override
                protected boolean processEntry(CollectionValidationWalkerContext<RouteDto, ActivityStubDto> context, ActivityStubDto current) {
                    return !current.skipValidate();
                }

                @Override
                protected boolean validateEntry(CollectionValidationWalkerContext<RouteDto, ActivityStubDto> c, ActivityStubDto current) {
                    ActivityStubDto previousActivity = c.getPrevious();
                    ActivityStubDto currentActivity = c.getCurrent();
                    if (previousActivity == null) {
                        return true;
                    }
                    if (previousActivity.getLatitude() == null || previousActivity.getLongitude() == null) {
                        return true;
                    }
                    if (currentActivity.getLongitude() == null || currentActivity.getLatitude() == null) {
                        return true;
                    }
                    if (route.getDate() == null) {
                        return true;
                    }
                    GPSPoint previousPoint = newPoint(route, previousActivity);
                    GPSPoint currentPoint = newPoint(route, currentActivity);
                    float computedSpeed = previousPoint.getSpeed(currentPoint);
                    boolean valid = computedSpeed <= speed;
                    if (!valid) {
                        this.computedSpeed = computedSpeed;
                        this.invalidActivity = validationContext.getDecoratorService().getDecoratorByType(ActivityStubDto.class).decorate(currentActivity);
                    }
                    return valid;
                }

                @Override
                protected void addError(NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector, CollectionValidationWalkerContext<RouteDto, ActivityStubDto> context, RouteDto object) {
                    addMessage(validationContext, messagesCollector, I18n.n("observe.data.ps.observation.Activity.validation.speed.bound.inter"), getSpeed(), getInvalidActivity(), getComputedSpeed());
                }

                @Override
                public void validateWhenNotSkip(RouteDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
                    invalidActivity = null;
                    computedSpeed = null;
                    super.validateWhenNotSkip(object, validationContext, messagesCollector);
                }

                public String getInvalidActivity() {
                    return invalidActivity;
                }

                public float getComputedSpeed() {
                    return computedSpeed;
                }

            };

//            delegate.setCollectionFieldName(RouteDto.PROPERTY_ACTIVITY);
//            delegate.setMode(CollectionFieldExpressionValidator.Mode.ALL);
//            delegate.setValueStack(stack);
//            delegate.setUseSensitiveContext(true);
//            delegate.setExpressionForFirst(null);
//            delegate.setExpressionForLast(null);
//            delegate.setFieldName(getFieldName());
//            delegate.setExpression("true");
//            delegate.setMessageKey(getMessageKey());
//            delegate.setDefaultMessage(getDefaultMessage());
//            delegate.setValidatorContext(getValidatorContext());
        }
        return delegate;
    }

    @Override
    public void validate(RouteDto object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        if (object == null) {
            return;
        }
        boolean enable = ((DtoValidationContext) validationContext).getConfiguration().isValidationSpeedEnable();
        if (this.enable != enable) {
            return;
        }
        speed = ((DtoValidationContext) validationContext).getConfiguration().getValidationSpeedMaxValue();
        getDelegate((DtoValidationContext) validationContext, object).validate(object, validationContext, messagesCollector);
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends SkipableFieldValidatorSupport.GeneratorSupport {
        public Generator() {
            super(RouteCheckActivitySpeed.class, false, false, false, false, false);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            result.add(validatorDefinition.getOptionalParameter("enabled").orElse("false"));
        }
    }

}
