package fr.ird.observe.dto.validation.validator.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.observation.ActivityStubDto;
import fr.ird.observe.dto.data.ps.observation.RouteDto;
import fr.ird.observe.validation.validator.collection.CollectionFieldValidationSupport;
import fr.ird.observe.validation.validator.collection.CollectionValidationWalkerContext;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;

import java.util.Collection;

/**
 * Created on 09/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class RouteCheckActivityTime extends CollectionFieldValidationSupport<RouteDto, Long, ActivityStubDto> {

    public RouteCheckActivityTime() {
        super(RouteDto.PROPERTY_ACTIVITY, null, null);
    }

    @Override
    protected Collection<ActivityStubDto> getCollection(NuitonValidationContext validationContext, RouteDto dto) {
        return dto.getActivity();
    }

    @Override
    protected boolean validateEntry(CollectionValidationWalkerContext<RouteDto, ActivityStubDto> context, ActivityStubDto current) {
        ActivityStubDto previous = context.getPrevious();
        return previous == null || previous.getTime().getTime() <= current.getTime().getTime();
    }

    @Override
    protected void addError(NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector, CollectionValidationWalkerContext<RouteDto, ActivityStubDto> context, RouteDto object) {
        addMessage(validationContext, messagesCollector, I18n.n("observe.data.ps.Route.validation.invalid.time"), context.getHumanIndex());
    }
}
