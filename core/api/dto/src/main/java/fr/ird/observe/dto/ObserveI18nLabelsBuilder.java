package fr.ird.observe.dto;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.GearUseFeaturesAware;
import fr.ird.observe.dto.data.GearUseFeaturesMeasurementAware;
import fr.ird.observe.dto.data.InlineDataDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.data.WellIdAware;
import fr.ird.observe.dto.data.WellIdsAware;
import fr.ird.observe.dto.data.WithDataFile;
import fr.ird.observe.dto.data.WithIndex;
import fr.ird.observe.dto.data.WithProportion;
import fr.ird.observe.dto.data.WithSimpleComment;
import fr.ird.observe.dto.data.ll.SetGlobalCompositionAware;
import fr.ird.observe.dto.data.ll.observation.LonglineElementAware;
import fr.ird.observe.dto.data.ll.observation.LonglinePositionAware;
import fr.ird.observe.dto.referential.I18nReferentialDto;
import fr.ird.observe.dto.referential.MinMaxWeightAware;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.WithFormula;
import fr.ird.observe.dto.referential.WithI18n;
import fr.ird.observe.dto.referential.WithSpeciesFaoCode;
import fr.ird.observe.dto.referential.common.SpeciesDto;
import fr.ird.observe.dto.referential.common.VesselSizeCategoryDto;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducerSupport;
import io.ultreia.java4all.i18n.spi.bean.RegisterI18nLabel;
import io.ultreia.java4all.i18n.spi.bean.RegisterI18nLabels;
import io.ultreia.java4all.lang.Strings;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * To render fields in select widgets.
 * <p>
 * Created by tchemit on 24/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.0
 */
@RegisterI18nLabels({
        @RegisterI18nLabel(target = CommonDto.class, offers = {
                "acquisitionMode", "acquisitionMode", "acquisitionMode.short",
                "action.create.tip", "action.creating", "action.delete", "action.delete.entry.tip", "action.delete.tip", "action.editable", "action.goto.data", "action.goto.open",
                "action.goto.selected", "action.goto.selected.tip", "action.goto.tip", "action.move.bottom", "action.move.bottom.tip", "action.move.down", "action.move.down.tip",
                "action.move.top", "action.move.top.tip", "action.move.up", "action.move.up.tip",
                "action.notEditable", "action.reading", "action.reset", "action.reset.new.entry.tip",
                "action.reset.tip", "action.save.and.next", "action.save.and.next.tip", "action.save.entry.tip",
                "action.select.first.tip", "action.select.last.tip", "action.select.next.tip", "action.select.previous.tip", "action.updating", "activity.available",
                "activity.comment", "activity.selected", "baitHaulingStatus", "baitSettingStatus", "baitSettingStatus.short", "baitType", "baitType.short",
                "baitsComposition", "baitsComposition.validation.uniqueKey", "baitsCompositionProportionSum",
                "basket", "basket.short", "basketLineLength", "basketsPerSectionCount", "batches.available",
                "batches.selected", "beatDiameter", "branchline", "branchline.short", "branchlineLength",
                "branchlineLength.short", "branchlinesComposition", "branchlinesComposition.validation.uniqueKey", "branchlinesCompositionProportionSum", "branchlinesPerBasketCount", "brokerageCompany", "captain",
                "catchFate", "catchFate.short", "catchHealthStatus", "catchHealthStatus.short", "categoryMax",
                "categoryMax.short", "categoryMin", "categoryMin.short", "comment", "comment.short",
                "computedBiodegradable","computedWhenArrivingBiodegradable","computedWhenLeavingBiodegradable",
                "computedNonEntangling","computedWhenArrivingNonEntangling","computedWhenLeavingNonEntangling",
                "computedSimplifiedObjectType","computedWhenArrivingSimplifiedObjectType","computedWhenLeavingSimplifiedObjectType",
                "computedValues", "conservation", "coordinate", "countDepredated", "countDepredated.short",
                "country", "createDate", "currentDirection", "currentSpeed", "dataQuality", "departureHarbour", "deploymentEnd", "deploymentStart",
                "depredated", "depredated.short", "depredatedProportion", "depthRecorder", "description", "destination", "destination.short", "directory", "discardHealthStatus",
                "discardHealthStatus.short", "distance", "distance.short", "enabled", "encounterType", "encounterType.short", "endDate", "endTime",
                "endTimeStamp", "endTimestamp", "ersId", "fate", "fate.short", "fateVessel", "fateVessel.short", "file", "first.buoy",
                "fishingEnd", "fishingEndDepth", "fishingStart", "fishingStartDepth", "floatingObject.comment", //                "action.save.tip",
                "floatline1Length", "floatline1Length.short", "floatline2Length", "floatline2Length.short",
                "floatlinesComposition", "floatlinesComposition.validation.uniqueKey",
                "floatlinesCompositionProportionSum", "formsUrl", "fpaZone", "nextFpaZone", "previousFpaZone", "currentFpaZone", "gearCharacteristic",
                "allowedGearCharacteristic.available", "allowedGearCharacteristic.selected", "defaultGearCharacteristic.available", "defaultGearCharacteristic.selected",
                "gearCharacteristic.short", "gearUseFeatures", "gearUseFeaturesMeasurement", "generalComment", "generalTab", "global.progression.description", "gonadeWeight", "harbour", "haulingBreaks", "haulingDirectionSameAsSetting", "haulingEndLatitude", "haulingEndLongitude", "haulingEndQuadrant",
                "haulingEndTimeStamp", "haulingEndDate", "haulingIdentifier", "haulingIdentifier.short", "haulingIdentifier.short", "haulingStartLatitude", "haulingStartLongitude", "haulingStartQuadrant", "haulingStartTimeStamp", "haulingStartDate", "homeId",
                "homeId.short", "hookLost",
                "hookOffset", "hookOffset.short", "hookPosition", "hookPosition", "hookSize", "hookSize.short", "hookType", "hookType.short", "hookWhenDiscarded",
                "hooksComposition", "hooksComposition.validation.uniqueKey", "hooksCompositionProportionSum", "id", "id.short", "individualSize", "individualSize.short", "individualWeight", "individualWeight.short", "informationSource", "itemHorizontalPosition",
                "itemVerticalPosition", "label", "label1", "label2", "label3", "label4", "label5", "label6", "label7", "label8", "labels", "landingHarbour", "lastUpdateDate", "latitude",
                "landing", "localMarket", "logbook", "observation", "wellPlan",
                "latitudeOriginal", "lengthBetweenBranchlines", "sizeMeasureMethod", "sizeMeasureMethod.short", "lightsticksColor", "lightsticksPerBasketCount", "lightsticksType", "lightsticksUsed", "lineType", "lineType.short",
                "list.message.none", "locationOnLongline", "logbookComment", "logbookDataEntryOperator", "logbookDataQuality", "longitude", "longitudeOriginal", "maturityStatus",
                "maxDepthTargeted", "maxFishingDepth", "meanDeploymentDepth", "meanFishingDepth",
                "measuredCount", "measuredCount.short", "measurementValue", "measurementValue.short", "medianDeploymentDepth", "medianFishingDepth", "message.cantWriteData", "messages", "minFishingDepth",
                "mitigationType", "mitigationType.available", "mitigationType.selected", "needComment", "no.coordinate", "no.unit", "noBuoy", "noOfCrewMembers",
                "noOfDays", "nocode", "none", "notComputed", "objectMaterial",
                "objectOperation", "observationsComment", "observationsDataEntryOperator", "observationsDataQuality", "observedSystem",
                "observedSystem.available", "observedSystem.selected", "observedSystemDistance",
                "observer", "ocean", "ocean.available", "ocean.selected",
                "onBoardProcessing", "origin", "otherTab", "packaging", "packaging.short", "photoReferences", "sampleReferences", "predator", "predator.available", "predator.selected", "processingCompany",
                "program", "proportion", "proportion.short", "proportionSum", "psSampler.available", "psSampler.selected", "quadrant", "quadrantOriginal", "reasonForNoFishing", "reasonForNullSet", "relatedObservedActivity", "reportsUrl", "root.list.message.none", "sampleQuality", "sampleSpeciesMeasure", "sampleType", "saveFile.overwrite",
                "saveFile.overwrite.cancel", "saveFile.overwrite.ok", "saveFile.overwrite.title", "schoolType", "seaSurfaceTemperature", "second.buoy", "section", "section.short",
                "sensorBrand", "sensorBrand.short", "sensorDataFormat", "sensorDataFormat.short", "sensorSerialNo", "sensorSerialNo.short", "sensorType", "sensorType.short", "serialNo", "serialNo.short", "settingEndLatitude", "settingEndLongitude", "settingEndQuadrant",
                "settingEndTimeStamp", "settingEndDate", "settingIdentifier", "settingIdentifier.short", "settingIdentifier.short", "settingShape", "settingStartLatitude", "settingStartLongitude", "settingStartQuadrant", "settingStartTimeStamp", "settingStartDate",
                "settingVesselSpeed", "sex", "sex.short", "shippingCompany", "shooterSpeed", "shooterUsed", "sizeClass", "sizeClass.short", "sizeMeasureType", "sizeMeasureType.short", "snapWeight", "species", "species.available", "species.selected", "species.short", "speciesAndMeasureTypes",
                "speciesFaoCode", "speciesFate", "speciesGroupReleaseMode.available", "speciesGroupReleaseMode.selected", "speciesLabel",
                "startDate", "startTime", "startTimeStamp", "startTimestamp", "status", "step.description", "stomachFullness",
                "stomachFullness", "storage.not.valid",
                "superSample", "supportVesselName",
                "survey", "swivelWeight", "tagNumber", "technicalInformation", "timeBetweenHooks", "timeSinceContact", "timeStamp",
                "timer", "timerTimeOnBoard", "topType", "topType.short", "totalBasketsCount", "totalHooksCount", "totalLightsticksCount", "totalLineLength", "totalSectionsCount", "traceCutOff",
                "tracelineLength", "tracelineLength.short", "tracelineType", "tracelineType.short", "transmittingBuoyOperation", "transmittingBuoyOwnership", "transmittingBuoyType", "type.short", "undefined",
                "unknown", "uri", "usage.label.and.count", "usedInTrip", "usedInTrip.short", "validation.field.tip",
                "validation.message.tip", "validation.scope.tip", "validityRangeLabel", "version",
                "vessel", "vesselActivity", "vesselCode", "vesselSpeed", "weightCategory",
                "weightCategory.short", "weightMeasureMethod", "weightMeasureMethod.short", "weightMeasureType", "weightMeasureType.short", "weightedSnap",
                "weightedSwivel", "well", "well.short", "whenArriving", "whenLeaving", "wind",
                "windDirection",
                "latitude.validation.required",
                "logbookDataEntryOperator.validation.required",
                "logbookDataQuality.validation.required",
                "longitude.validation.required",
                "observationsDataEntryOperator.validation.required",
                "observer.validation.required",
                "quadrant.validation.required",
                "sizeMeasureType.validation.required",
                "weightMeasureMethod.validation.required",
                "weightMeasureType.validation.required",
                WithIndex.PROPERTY_INDEX, WithIndex.PROPERTY_INDEX + ".short"}),
        @RegisterI18nLabel(target = IdDto.class, properties = {"id", "lastUpdateDate", "version", "createDate"}),
        @RegisterI18nLabel(target = DataGroupByDto.class, properties = {"filterText", "count"}),
        @RegisterI18nLabel(target = BusinessDto.class, properties = {"homeId"}),
        @RegisterI18nLabel(target = OpenableDto.class, offers = {"action.close.tip", "action.create.tip", "action.editable.tip", "action.notEditable.tip", "action.reopen.tip", "action.update.tip"}),
        @RegisterI18nLabel(target = WithStartEndDate.class, properties = {"startDate", "endDate", "validityRangeLabel"}),
        @RegisterI18nLabel(target = WithSpeciesFaoCode.class, properties = {"speciesFaoCode", "speciesLabel"}),
        @RegisterI18nLabel(target = WithDataFile.class, properties = {"dataLocation", "dataLocation.tip", "data", "hasData.short", "hasData", "action.deleteDataFile", "action.deleteDataFile.tip", "action.exportDataFile", "action.exportDataFile.filter", "action.exportDataFile.tip", "action.importData", "action.importDataFile", "action.importDataFile.tip", "action.replace.data.file.message", "choose.title.exportDataFile", "choose.title.importData", "delete.data.file.message"}),
        @RegisterI18nLabel(target = LonglineElementAware.class, properties = {"haulingIdentifier", "haulingIdentifier.short", "settingIdentifier", "settingIdentifier.short"}),
        @RegisterI18nLabel(target = InlineDataDto.class, offers = {"action.add", "action.add.tip", "action.delete", "action.delete.message", "action.delete.tip"}),
        @RegisterI18nLabel(target = WithI18n.class, properties = {"label1", "label2", "label3", "label4", "label5", "label6", "label7", "label8"}),
        @RegisterI18nLabel(target = WellIdAware.class, properties = {"well"}),
        @RegisterI18nLabel(target = WellIdsAware.class, properties = {"well"}),
        @RegisterI18nLabel(target = WithSimpleComment.class, properties = {"comment"}),
        @RegisterI18nLabel(target = WithIndex.class, properties = {WithIndex.PROPERTY_INDEX, WithIndex.PROPERTY_INDEX+".short"}),
        @RegisterI18nLabel(target = WithProportion.class, properties = {"proportion", "proportion.short", "proportionSum"}),
        @RegisterI18nLabel(target = GearUseFeaturesAware.class, properties = {"action.create", "action.save", "action.save.tip", "definitionTab", "gear", "gear.short", "gearUseFeaturesMeasurement", "number", "number.short",
                "title", "type", "usedInTrip", "usedInTrip.short"}),
        @RegisterI18nLabel(target = GearUseFeaturesMeasurementAware.class, properties = {"gearCharacteristic", "gearCharacteristic.short", "measurementValue", "measurementValue.short", "type"}),
        @RegisterI18nLabel(target = LonglinePositionAware.class, properties = {"basket", "basket.short", "branchline", "branchline.short", "section", "section.short"}),
        @RegisterI18nLabel(target = ReferentialDto.class, properties = {"codeAndHomeId", "code", "status", "uri", "enabled", "needComment"},
                offers = {"action.back.to.list", "action.back.to.list.tip", "action.changeId.choose.new.id", "action.changeId.choose.new.id.title", "action.changeId.tip", "action.create", "action.delete.tip", "action.detail", "action.modify", "action.openType", "action.show.unique.keys.tip", "action.show.usages.tip", "characteristics", "disabled", "obsolete"}),
        @RegisterI18nLabel(target = I18nReferentialDto.class),
        @RegisterI18nLabel(target = MinMaxWeightAware.class, offers = {"minWeight", "maxWeight", "minMaxWeight", "minMeanMaxWeight"}),
        @RegisterI18nLabel(target = TripAware.class, properties = {"vesselCode"}, offers = {"bulkModify.availableValues", "bulkModify.availableCriteria", "bulkModify.title","action.bulkModify", "action.create", "choice.go.to.trip", "list.navigation.node", "list.title", "logbookTab", "mapTab", "message.not.open", "navigation.unsaved", "observationsTab", "logbookProgram", "observationsProgram", "title", "title.can.not.create.trip.sub.data", "type", "logbookProgram.validation.required", "observationsProgram.validation.required"}),
        @RegisterI18nLabel(target = WithFormula.class, offers = {"equation", "coefficients", "coefficientsInformation", "source"}),
        @RegisterI18nLabel(target = SetGlobalCompositionAware.class, properties = {
                SetGlobalCompositionAware.PROPERTY_BAITS_COMPOSITION, SetGlobalCompositionAware.PROPERTY_BRANCHLINES_COMPOSITION, SetGlobalCompositionAware.PROPERTY_FLOATLINES_COMPOSITION, SetGlobalCompositionAware.PROPERTY_HOOKS_COMPOSITION, SetGlobalCompositionAware.PROPERTY_BAITS_COMPOSITION_PROPORTION_SUM, SetGlobalCompositionAware.PROPERTY_BRANCHLINES_COMPOSITION_PROPORTION_SUM, SetGlobalCompositionAware.PROPERTY_FLOATLINES_COMPOSITION_PROPORTION_SUM, SetGlobalCompositionAware.PROPERTY_HOOKS_COMPOSITION_PROPORTION_SUM,
                "baitsComposition.validation.uniqueKey", "branchlinesComposition.validation.uniqueKey", "floatlinesComposition.validation.uniqueKey", "hooksComposition.validation.uniqueKey"})
})
public class
ObserveI18nLabelsBuilder extends BeanPropertyI18nKeyProducerSupport {

    public static final String OBSERVE_COMMON_PREFIX = "observe.";
    private static final List<String> SKIP_LABEL = Arrays.asList(
            "speciesLabel",
            "species/" + SpeciesDto.PROPERTY_SCIENTIFIC_LABEL,
            SpeciesDto.PROPERTY_SCIENTIFIC_LABEL,
            VesselSizeCategoryDto.PROPERTY_CAPACITY_LABEL,
            VesselSizeCategoryDto.PROPERTY_GAUGE_LABEL,
            WithStartEndDate.PROPERTY_VALIDITY_RANGE_LABEL,
            WithStartEndDate.PROPERTY_START_END_DATE_LABEL);

    static {
        n("boolean.false");
        n("boolean.true");
        n("observe.Common.size");
    }

    //FIXME I18n
    public static String getCodeLabel(String code) {
        return code == null ? " Aucun code" : code;
    }

    //FIXME I18n
    public static String getUnitLabel(boolean isBoolean, String unit) {
        return isBoolean ? "" : (" [ " + (unit == null ? "pas d'unité" : unit) + " ]");
    }

    //FIXME I18n
    public static Pair<Boolean, String> getMeasurementValue(String measurementValue) {
        boolean isBoolean = false;
        if ("true".equals(measurementValue)) {
            measurementValue = "Oui";
            isBoolean = true;
        } else if ("false".equals(measurementValue)) {
            measurementValue = "Non";
            isBoolean = true;
        }
        return Pair.of(isBoolean, measurementValue);
    }

    public ObserveI18nLabelsBuilder() {
        super(OBSERVE_COMMON_PREFIX, "Observe", Thread.currentThread().getContextClassLoader());
    }

    @Override
    protected Map<String, String> createPropertyKeyMapping() {
        return ImmutableMap.<String, String>builder()
                .put("sizeMeasureTypeCode", "sizeMeasureType")
                .put("inputSizeMeasureTypeCode", "inputSizeMeasureType")
                .put("outputSizeMeasureTypeCode", "outputSizeMeasureType")
                .put("computedTimeStr", "computedTime")
                .put("coordinateStr", "coordinate")
                .put("species/" + SpeciesDto.PROPERTY_SCIENTIFIC_LABEL, "species")
                .build();
    }

    @Override
    public String getPropertyMapping(String property) {
        int i = Objects.requireNonNull(property).indexOf("::");
        if (i > -1) {
            property = property.substring(0, i);
        }
        if (property.endsWith("Label") && !SKIP_LABEL.contains(property)) {
            property = Strings.removeEnd(property, "Label");
        }
        return super.getPropertyMapping(property);
    }

}
