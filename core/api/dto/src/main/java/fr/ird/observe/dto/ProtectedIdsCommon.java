package fr.ird.observe.dto;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 29/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
//FIXME-PROTECTED-ID
public interface ProtectedIdsCommon {
    String COMMON_GEAR_CHARACTERISTIC_TYPE_INTEGER_UNSIGNED = "fr.ird.referential.common.GearCharacteristicType#1239832686123#0.5";
    String COMMON_GEAR_CHARACTERISTIC_TYPE_INTEGER_SIGNED = "fr.ird.referential.common.GearCharacteristicType#1239832686123#0.3";
    String COMMON_GEAR_CHARACTERISTIC_TYPE_FLOAT_UNSIGNED = "fr.ird.referential.common.GearCharacteristicType#1239832686123#0.6";
    String COMMON_GEAR_CHARACTERISTIC_TYPE_FLOAT_SIGNED = "fr.ird.referential.common.GearCharacteristicType#1239832686123#0.4";
    String COMMON_GEAR_CHARACTERISTIC_TYPE_BOOLEAN = "fr.ird.referential.common.GearCharacteristicType#1239832686123#0.2";
    String COMMON_GEAR_CHARACTERISTIC_TYPE_TEXT = "fr.ird.referential.common.GearCharacteristicType#1239832686123#0.1";
    String COMMON_GEAR_CHARACTERISTIC_TYPE_DATE = "fr.ird.referential.common.GearCharacteristicType#1464000000000#0.7";
    String COMMON_GEAR_CHARACTERISTIC_TYPE_LIST = "fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8";
    String COMMON_OCEAN_ATLANTIC = "fr.ird.referential.common.Ocean#1239832686151#0.17595105505051245";
    String COMMON_OCEAN_INDIAN = "fr.ird.referential.common.Ocean#1239832686152#0.8325731048817705";
}
