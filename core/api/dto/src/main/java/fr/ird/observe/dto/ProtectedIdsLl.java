package fr.ird.observe.dto;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 29/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
//FIXME-PROTECTED-ID
public interface ProtectedIdsLl {
    String LL_COMMON_ACTIVITY_INTERACTION_ID = "fr.ird.referential.ll.common.VesselActivity#1239832686138#0.4";
    String LL_COMMON_ACTIVITY_STATION_ID = "fr.ird.referential.ll.common.VesselActivity#1239832686138#0.3";
    String LL_COMMON_ACTIVITY_HARBOUR_ID = "fr.ird.referential.ll.common.VesselActivity#666#03";

    String LL_OBSERVATION_GROUPED_OBSERVATION_METHOD_ID = "fr.ird.referential.ll.common.ObservationMethod#1239832686136#0.1";
    String LL_OBSERVATION_CATCH_DEFAULT_WEIGHT_MEASURE_METHOD_ID = "fr.ird.referential.common.WeightMeasureMethod#666#03";
    String LL_OBSERVATION_CATCH_DEFAULT_WEIGHT_MEASURE_TYPE_ID = "fr.ird.referential.common.WeightMeasureType#1726219730662#0.7553113855315937";
    String LL_OBSERVATION_CATCH_DISCARDED_CATCH_FATE_ID = "fr.ird.referential.ll.common.CatchFate#1239832686125#0.3";
    String LL_OBSERVATION_SIZE_MEASURE_DEFAULT_SIZE_MEASURE_METHOD_ID = "fr.ird.referential.common.SizeMeasureMethod#666#01";
    String LL_OBSERVATION_WEIGHT_MEASURE_DEFAULT_WEIGHT_MEASURE_METHOD_ID = "fr.ird.referential.common.WeightMeasureMethod#1726219878250#0.4217724493500721";
    String LL_LOGBOOK_ACTIVITY_DEFAULT_DATA_QUALITY_ID = "fr.ird.referential.common.DataQuality#0#1";
    String LL_LOGBOOK_LANDING_PART_DEFAULT_DATA_QUALITY_ID = "fr.ird.referential.common.DataQuality#0#5";
    String LL_LOGBOOK_ACTIVITY_DEFAULT_SETTING_SHAPE_ID = "fr.ird.referential.ll.common.SettingShape#1433499465088#0.892811479745433";
    String LL_LOGBOOK_BRANCHLINE_COMPOSITION_TOP_TYPE_ID = "fr.ird.referential.ll.common.LineType#1239832686157#0.9";
    String LL_LOGBOOK_BRANCHLINE_COMPOSITION_TRACELINE_TYPE_ID = "fr.ird.referential.ll.common.LineType#1239832686157#0.9";
    String LL_LOGBOOK_CATCH_DISCARDED_CATCH_FATE_ID = "fr.ird.referential.ll.common.CatchFate#1239832686125#0.3";
    String LL_LOGBOOK_CATCH_LONGLINE_DEFAULT_CATCH_HEALTH_STATUS_ID = "fr.ird.referential.ll.common.HealthStatus#1239832686128#0.4";
    String LL_LOGBOOK_SAMPLE_PART_DEFAULT_SIZE_MEASURE_METHOD_ID = "fr.ird.referential.common.SizeMeasureMethod#666#01";
    String LL_LOGBOOK_SAMPLE_PART_DEFAULT_WEIGHT_MEASURE_METHOD_ID = "fr.ird.referential.common.WeightMeasureMethod#666#02";
    String LL_LOGBOOK_SAMPLE_PART_DEFAULT_SEX_ID = "fr.ird.referential.common.Sex#1239832686122#0.5";
    String LL_LOGBOOK_FLOATLINES_COMPOSITION_LINE_TYPE_ID = "fr.ird.referential.ll.common.LineType#1239832686157#0.9";
    String LL_LOGBOOK_HOOKS_COMPOSITION_TYPE_ID = "fr.ird.referential.ll.common.HookType#1433499457247#0.796845980919898";
    String LL_LANDING_TRANSSHIPMENT_VESSEL_ID = "fr.ird.referential.common.Vessel#1308214144311#0.8813727202637713";
    String LL_LANDING_TRANSSHIPMENT_HARBOUR_ID = "fr.ird.referential.common.Harbour#11#0.38";
}
