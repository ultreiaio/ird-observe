package fr.ird.observe.dto;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Set;

/**
 * Created on 29/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
//FIXME-PROTECTED-ID
public interface ProtectedIdsPs {
    String PS_COMMON_TRANSMITTING_BUOY_OWNERSHIP_THIS_SHIP_ID = "fr.ird.referential.ps.common.TransmittingBuoyOwnership#0#3";
    String PS_COMMON_TRANSMITTING_BUOY_OWNERSHIP_UNKNOWN_ID = "fr.ird.referential.ps.common.TransmittingBuoyOwnership#0#0";
    String PS_COMMON_SCHOOL_TYPE_UNDEFINED_ID = "fr.ird.referential.ps.common.SchoolType#0#0";
    String PS_COMMON_SCHOOL_TYPE_FREE_ID = "fr.ird.referential.ps.common.SchoolType#0#2";
    String PS_COMMON_SCHOOL_TYPE_OBJECT_ID = "fr.ird.referential.ps.common.SchoolType#0#1";
    String PS_COMMON_VESSEL_ACTIVITY_ID_FOR_WELL_PLAN = "fr.ird.referential.ps.common.VesselActivity#1464000000000#32";
    String PS_COMMON_VESSEL_ACTIVITY_ID_FOR_END_OF_SEARCHING = "fr.ird.referential.ps.common.VesselActivity#1239832675372#0.21399033380125898";

    //FIXME No more used, find out why? 2021-11-02
    Set<String> PS_COMMON_VESSEL_ACTIVITY_FOR_OBSERVED_SYSTEM = Set.of(
            "fr.ird.referential.ps.common.VesselActivity#1239832675349#0.363119635949572", // At harbour
            "fr.ird.referential.ps.common.VesselActivity#1239832675370#0.495613158646268", // Drifting due to mechanical problems
            "fr.ird.referential.ps.common.VesselActivity#1239832675372#0.21399033380125898" // end of searching
    );
    /**
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2663">issue 2663</a>
     */
    Set<String> PS_COMMON_TRANSMITTING_BUOY_WITH_COORDINATES = Set.of(
            "fr.ird.referential.ps.common.TransmittingBuoyOperation#1464000000000#5", // End of use
            "fr.ird.referential.ps.common.TransmittingBuoyOperation#1464000000000#4" // End of signal
    );
    Set<String> PS_COMMON_LD1_SIZE_MEASURE_TYPE_ID = Set.of("fr.ird.referential.common.SizeMeasureType#1433499466774#0.529249255312607", "LD1");
    Set<String> PS_COMMON_LF_SIZE_MEASURE_TYPE_ID = Set.of("fr.ird.referential.common.SizeMeasureType#1433499465700#0.0902433863375336", "LF");
    String PS_COMMON_TRANSMITTING_BUOY_OPERATION_POSE = "fr.ird.referential.ps.common.TransmittingBuoyOperation#1239832686238#0.4755624782839416";
    String PS_COMMON_OBJECT_MATERIAL_TYPE_FLOAT = "fr.ird.referential.ps.common.ObjectMaterialType#0#1";
    String PS_COMMON_OBJECT_MATERIAL_TYPE_INTEGER = "fr.ird.referential.ps.common.ObjectMaterialType#0#2";
    String PS_COMMON_OBJECT_MATERIAL_TYPE_TEXT = "fr.ird.referential.ps.common.ObjectMaterialType#0#3";
    String PS_COMMON_OBJECT_MATERIAL_TYPE_BOOLEAN = "fr.ird.referential.ps.common.ObjectMaterialType#0#0";
    String PS_COMMON_REASON_FOR_NUL_SET_0 = "fr.ird.referential.ps.common.ReasonForNullSet#1239832683529#0.10743661058036036";
    String PS_COMMON_ACTIVITY_HARBOUR_ID = "fr.ird.referential.ps.common.VesselActivity#1239832675349#0.363119635949572";
    String PS_COMMON_DEFAULT_ACQUISITION_STATUS_ID = "fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#099";
    String PS_COMMON_DONE_ACQUISITION_STATUS_ID = "fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#001";
    String PS_LOGBOOK_DEFAULT_WELL_CONTENT_STATUS_ID = "fr.ird.referential.ps.logbook.WellContentStatus#1464000000000#03";
    String PS_OBSERVATION_FOB_OBSERVED_SYSTEM = "fr.ird.referential.ps.common.ObservedSystem#0#1.2";
    Set<String> PS_OBSERVATION_FOB_OBSERVED_SYSTEM_EXCLUDE_OPERATIONS = Set.of("fr.ird.referential.ps.common.ObjectOperation#0#10", "fr.ird.referential.ps.common.ObjectOperation#0#1");
    String PS_OBSERVATION_SCHOOL_ESTIMATE_DEFAULT_WEIGHT_MEASURE_METHOD_ID = "fr.ird.referential.common.WeightMeasureMethod#666#04";
    String PS_OBSERVATION_KEPT_TARGET_CATCH_DEFAULT_WEIGHT_MEASURE_METHOD_ID = "fr.ird.referential.common.WeightMeasureMethod#666#01";
    String PS_LOGBOOK_TRIP_DEFAULT_DATA_QUALITY_ID = "fr.ird.referential.common.DataQuality#0#5";
    String PS_LOGBOOK_TRIP_DEFAULT_DEPARTURE_WELL_CONTENT_STATUS_ID = "fr.ird.referential.ps.logbook.WellContentStatus#1464000000000#03";
    String PS_LOGBOOK_TRIP_DEFAULT_LANDING_WELL_CONTENT_STATUS_ID = "fr.ird.referential.ps.logbook.WellContentStatus#1464000000000#03";
    String PS_LOGBOOK_FOB_OBSERVED_SYSTEM = "fr.ird.referential.ps.common.ObservedSystem#0#1.2";
    String PS_LOGBOOK_SET_SUCCESS_STATUS_0 = "fr.ird.referential.ps.logbook.SetSuccessStatus#1464000000000#00";
    Set<String> PS_LOGBOOK_FOB_OBSERVED_SYSTEM_EXCLUDE_OPERATIONS = Set.of("fr.ird.referential.ps.common.ObjectOperation#0#10", "fr.ird.referential.ps.common.ObjectOperation#0#1");
    String PS_LANDING_LANDING_PART_DEFAULT_FATE_ID = "fr.ird.referential.ps.landing.Fate#1464000000000#01";
    String PS_LOGBOOK_ACTIVITY_DEFAULT_WEIGHT_MEASURE_METHOD_ID = "fr.ird.referential.common.WeightMeasureMethod#666#03";
    String PS_LOGBOOK_ACTIVITY_DEFAULT_INFORMATION_SOURCE_ID = "fr.ird.referential.ps.logbook.InformationSource#1464000000000#03";
    String PS_LOGBOOK_CATCH_DEFAULT_WEIGHT_MEASURE_METHOD_ID = "fr.ird.referential.common.WeightMeasureMethod#666#03";
    String PS_LOGBOOK_SAMPLE_DEFAULT_SAMPLE_TYPE_ID = "fr.ird.referential.ps.common.SampleType#1464000000000#01";
    String PS_LOGBOOK_SAMPLE_DEFAULT_SAMPLE_QUALITY_ID = "fr.ird.referential.ps.logbook.SampleQuality#1464000000000#01";
    Set<String> PS_LOGBOOK_CATCH_SPECIES_FATES = Set.of("fr.ird.referential.ps.common.SpeciesFate#1239832683619#0.5722739932065866", "fr.ird.referential.ps.common.SpeciesFate#1501492537510#0.9210847837998154");
    String PS_LOGBOOK_WEIGHT_CATEGORY_SMALLS_WEIGHT_ID = "fr.ird.referential.ps.common.WeightCategory#1464000000000#00001";
    String PS_LOGBOOK_WEIGHT_CATEGORY_BIGS_WEIGHT_ID = "fr.ird.referential.ps.common.WeightCategory#1464000000000#00002";

    String PS_LOCAL_MARKET_SAMPLE_DEFAULT_SAMPLE_TYPE_ID = "fr.ird.referential.ps.common.SampleType#1464000000000#01";
    String PS_LOCAL_MARKET_BATCH_WEIGHT_TYPE_WEIGHING = "fr.ird.referential.ps.localmarket.BatchWeightType#1464000000000#01";
    String PS_LOCAL_MARKET_BATCH_WEIGHT_TYPE_SINGLE_SPECIES_INDIVIDUAL = "fr.ird.referential.ps.localmarket.BatchWeightType#1464000000000#02";
    String PS_LOCAL_MARKET_BATCH_WEIGHT_TYPE_PACKAGING_NOT_WEIGHTED = "fr.ird.referential.ps.localmarket.BatchWeightType#1464000000000#03";
}
