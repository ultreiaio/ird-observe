package fr.ird.observe.dto;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Ordering;

import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 10/07/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface WithStartEndDate extends DtoAndReferenceAware {

    Comparator<Date> START_DATE_COMPARATOR = Ordering.natural().nullsFirst();
    Comparator<Date> END_DATE_COMPARATOR = Ordering.natural().nullsLast();

    String PROPERTY_VALIDITY_RANGE_LABEL = "validityRangeLabel";
    String PROPERTY_START_END_DATE_LABEL = "startEndDateLabel";

    Date getStartDate();

    Date getEndDate();

    default String getValidityRangeLabel(Locale locale) {
        Date startDate = getStartDate();
        Date endDate = getEndDate();
        if (startDate == null && endDate == null) {
            return t("observe.Common.undefined");
        }
        if (startDate != null && endDate != null) {
            StringBuilder result = new StringBuilder();
            I18nDecoratorHelper.getDateLabel(locale, result, startDate);
            result.append(" - ");
            I18nDecoratorHelper.getDateLabel(locale, result, endDate);
            return result.toString();
        }
        if (startDate != null) {
            StringBuilder result = new StringBuilder(" > ");
            I18nDecoratorHelper.getDateLabel(locale, result, startDate);
            return result.toString();
        }
        StringBuilder result = new StringBuilder(" < ");
        I18nDecoratorHelper.getDateLabel(locale, result, endDate);
        return result.toString();
    }

    default boolean acceptDate(Date date) {
        if (date == null) {
            return false;
        }
        Date startDate = getStartDate();
        if (startDate != null && date.before(startDate)) {
            return false;
        }
        Date endDate = getEndDate();
        if (endDate != null && date.after(endDate)) {
            return false;
        }
        return true;
    }
}
