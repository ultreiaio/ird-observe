package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

/**
 * Pour définir le mode saisie d'un échantillon.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.8
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN)
public enum AcquisitionMode {
    /**
     * Le mode de saisie par défaut.
     * <p>
     * L'observateur entre un effectif et une classe de taille, il ne peut pas
     * saisir le poids (celui sera déduit par calcul).
     */
    number,

    /**
     * Le mode de saisie par individu.
     * <p>
     * L'observateur saisie les poids individuel, il ne peut pas saisir
     * d'effectif, l'effectif est de 1.
     */
    individual;

    public static AcquisitionMode valueOf(int ordinal) throws IllegalArgumentException {
        for (AcquisitionMode o : values()) {
            if (o.ordinal() == ordinal) {
                return o;
            }
        }
        throw new IllegalArgumentException("could not find a " + AcquisitionMode.class.getSimpleName() + " value for ordinal " + ordinal);
    }

    @Override
    public String toString() {
        return AcquisitionModeI18n.getLabel(this);
    }
}
