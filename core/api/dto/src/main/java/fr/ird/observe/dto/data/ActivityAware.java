package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.DtoAndReferenceAware;
import fr.ird.observe.dto.GPSPoint;
import fr.ird.observe.dto.ToolkitId;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.util.Dates;

import java.util.Date;
import java.util.Optional;

/**
 * Created by tchemit on 15/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface ActivityAware extends DtoAndReferenceAware, ToolkitId, JavaBean {

    String CLASSIFIER_WITH_ROUTE = "WithRoute";
    String PROPERTY_HAS_SET = "hasSet";
    String PROPERTY_SET_ENABLED = "setEnabled";

    static GPSPoint newGPSPoint(ActivityAware a) {
        GPSPoint gpsPoint = new GPSPoint();
        gpsPoint.setTime(a.getTimeStamp());
        gpsPoint.setLatitude(a.getLatitude());
        gpsPoint.setLongitude(a.getLongitude());
        return gpsPoint;
    }

    GPSPoint getGPSPoint();

    Date getTimeStamp();

    Float getLatitude();

    Float getLongitude();

    ToolkitId getVesselActivity();

    default String getVesselActivityId() {
        return Optional.ofNullable(getVesselActivity()).map(ToolkitId::getId).orElse(null);
    }

    boolean isSetEnabled();

    default boolean isFloatingObjectEnabled() {
        return false;
    }

    default boolean isCatchesEnabled() {
        return isSetEnabled();
    }

    default Date getDate() {
        return getTimeStamp() == null ? null : Dates.getDay(getTimeStamp());
    }

}
