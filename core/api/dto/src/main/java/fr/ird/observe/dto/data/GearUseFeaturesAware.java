package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.referential.common.GearReference;
import io.ultreia.java4all.bean.JavaBean;

import java.util.List;

/**
 * Created on 01/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface GearUseFeaturesAware extends ObserveDto, JavaBean, ToolkitId {

    String getComment();

    Integer getNumber();

    Boolean getUsedInTrip();

    GearReference getGear();

    boolean isGearUseFeaturesMeasurementEmpty();

    boolean isNotGearUseFeaturesMeasurementEmpty();

    int getGearUseFeaturesMeasurementSize();

    List<? extends GearUseFeaturesMeasurementAware> getGearUseFeaturesMeasurement();

    default void setGear(GearReference gear) {
        // by default do nothing
    }

    default void setComment(String comment) {
        // by default do nothing
    }

    default void setNumber(Integer number) {
        // by default do nothing
    }

    default void setUsedInTrip(Boolean usedInTrip) {
        // by default do nothing
    }

    default void sortMeasurements() {
        getGearUseFeaturesMeasurement().sort(GearUseFeaturesMeasurementAware.GEAR_USE_FEATURES_MEASUREMENT_COMPARATOR);
    }
}
