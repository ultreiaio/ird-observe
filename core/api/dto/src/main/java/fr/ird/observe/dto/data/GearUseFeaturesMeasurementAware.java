package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.common.GearCharacteristicListItemReference;
import fr.ird.observe.dto.referential.common.GearCharacteristicReference;
import fr.ird.observe.dto.referential.common.GearDto;
import io.ultreia.java4all.lang.Strings;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created on 01/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface GearUseFeaturesMeasurementAware extends InlineDataDto {

    Comparator<GearUseFeaturesMeasurementAware> GEAR_USE_FEATURES_MEASUREMENT_COMPARATOR = Comparator.comparing(m -> Strings.leftPad(m.getGearCharacteristic().getCode(), 6, "0"));

    static <M extends GearUseFeaturesMeasurementAware> List<M> getDefaultGearUseFeaturesMeasurement(GearDto gear, Supplier<M> supplier) {
        return gear.getDefaultGearCharacteristic().stream()
                   .map(input -> {
                       M measurementDto = supplier.get();
                       if (input.getGearCharacteristicType().isBoolean()) {
                           measurementDto.setMeasurementValue(Boolean.FALSE.toString());
                       }
                       measurementDto.setGearCharacteristic(input);
                       return measurementDto;
                   })
                   .sorted(GearUseFeaturesMeasurementAware.GEAR_USE_FEATURES_MEASUREMENT_COMPARATOR)
                   .collect(Collectors.toList());
    }

    static boolean updateGearCharacteristic(GearUseFeaturesMeasurementAware measure, GearCharacteristicReference value) {
        GearCharacteristicReference oldValue = measure.getGearCharacteristic();
        measure.setGearCharacteristic(value);
        boolean valueChanged = !Objects.equals(oldValue, value);
        if (valueChanged) {
            if (value != null && measure.getGearCharacteristic().getGearCharacteristicType().isBoolean()) {
                // force false value by default
                measure.setMeasurementValue("false");
            } else {
                // force to null value
                measure.setMeasurementValue(null);
            }
        }
        return valueChanged;
    }

    static boolean updateMeasurementValue(GearUseFeaturesMeasurementAware measure, Object aValue) {
        String oldValue = measure.getMeasurementValue();
        String value;
        if (aValue == null) {
            value = null;
        } else {
            if (measure.getGearCharacteristic().getGearCharacteristicType().isDate()) {
                value = String.valueOf(((Date) aValue).getTime());
            } else if (measure.getGearCharacteristic().getGearCharacteristicType().isList()) {
                value = ((GearCharacteristicListItemReference) aValue).getId();
            } else {
                value = String.valueOf(aValue).trim();
            }
            if (value.isBlank()) {
                value = null;
            }
        }
        measure.setMeasurementValue(value);
        return !Objects.equals(oldValue, value);
    }

    String getMeasurementValue();

    GearCharacteristicReference getGearCharacteristic();

    void setMeasurementValue(String measurementValue);

    void setGearCharacteristic(GearCharacteristicReference gearCharacteristic);
}
