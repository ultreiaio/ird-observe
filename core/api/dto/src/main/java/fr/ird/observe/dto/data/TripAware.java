package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.WithStartEndDate;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.dto.referential.ps.common.AcquisitionStatusReference;
import io.ultreia.java4all.bean.JavaBean;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 06/01/2019.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface TripAware extends WithStartEndDate, ToolkitId, JavaBean {

    String PROPERTY_OBSERVATIONS_AVAILABILITY = "observationsAvailability";
    String PROPERTY_LOGBOOK_AVAILABILITY = "logbookAvailability";

    static <T extends TripAware> List<T> sort(List<T> data) {
        return data
                .stream()
                .sorted(Comparator.comparing(TripAware::getStartDate)
                                .thenComparing(TripAware::getEndDate)
                                .thenComparing(TripAware::getVesselLabel))
                .collect(Collectors.toList());
    }

    static boolean isSeineId(String id) {
        return id.contains(".ps.");
    }

    static boolean isLonglineId(String id) {
        return id.contains(".ll.");
    }

    static boolean isFieldEnabled(AcquisitionStatusReference acquisitionStatus) {
        return Optional.ofNullable(acquisitionStatus).map(AcquisitionStatusReference::isFieldEnabler).orElse(false);
    }

    void setId(String id);

    VesselReference getVessel();

    ReferentialDtoReference getObservationsProgram();

    ReferentialDtoReference getLogbookProgram();

    boolean isObservationsAvailability();

    boolean isLogbookAvailability();

    default String getVesselLabel() {
        return getVessel() == null ? null : getVessel().getLabel();
    }

    default String getVesselCode() {
        return getVessel() == null ? null : getVessel().getCode();
    }

    default void setEndDate(Date endDate) {
        // not used for references
    }
}
