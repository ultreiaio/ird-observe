package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Configuration used to build a {@code TripMapUI}.
 * <p>
 * Created on 01/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public interface TripMapConfig {

    String getMapDateFormat();

    Set<TripMapContentBuilder> getTripMapContentBuilders();

    File getMapDirectory();

    Color getMapBackgroundColor();

    List<File> getMapLayerFiles();

    Map<String, String> getVariables(TripMapContentBuilder builder);

    File getTemporaryDirectory();

}
