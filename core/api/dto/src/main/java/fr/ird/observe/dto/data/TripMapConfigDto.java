package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.StringJoiner;

/**
 * Created on 30/06/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
@GenerateJavaBeanDefinition
public class TripMapConfigDto extends AbstractJavaBean implements JsonAware {

    public static final String PROPERTY_TRIP_ID = "tripId";
    public static final String PROPERTY_ADD_OBSERVATIONS = "addObservations";
    public static final String PROPERTY_ADD_OBSERVATIONS_TRIP_SEGMENT = "addObservationsTripSegment";
    public static final String PROPERTY_ADD_LOGBOOK = "addLogbook";
    public static final String PROPERTY_ADD_LOGBOOK_TRIP_SEGMENT = "addLogbookTripSegment";
    private String tripId;
    private boolean addObservations;
    private boolean addLogbook;
    private boolean addLogbookTripSegment;
    private boolean addObservationsTripSegment;

    public String getTripId() {
        return tripId;
    }

    public boolean isAddObservations() {
        return addObservations;
    }

    public boolean isAddLogbook() {
        return addLogbook;
    }

    public void setTripId(String tripId) {
        String oldValue = getTripId();
        this.tripId = tripId;
        firePropertyChange(PROPERTY_TRIP_ID, oldValue, tripId);
    }

    public void setAddObservations(boolean addObservations) {
        boolean oldValue = isAddObservations();
        this.addObservations = addObservations;
        firePropertyChange(PROPERTY_ADD_OBSERVATIONS, oldValue, addObservations);
    }

    public void setAddLogbook(boolean addLogbook) {
        boolean oldValue = isAddLogbook();
        this.addLogbook = addLogbook;
        firePropertyChange(PROPERTY_ADD_LOGBOOK, oldValue, addLogbook);
    }

    public boolean isAddLogbookTripSegment() {
        return addLogbookTripSegment;
    }

    public void setAddLogbookTripSegment(boolean addLogbookTripSegment) {
        boolean oldValue = isAddLogbookTripSegment();
        this.addLogbookTripSegment = addLogbookTripSegment;
        firePropertyChange(PROPERTY_ADD_LOGBOOK_TRIP_SEGMENT, oldValue, addLogbookTripSegment);
    }

    public boolean isAddObservationsTripSegment() {
        return addObservationsTripSegment;
    }

    public void setAddObservationsTripSegment(boolean addObservationsTripSegment) {
        boolean oldValue = isAddObservationsTripSegment();
        this.addObservationsTripSegment = addObservationsTripSegment;
        firePropertyChange(PROPERTY_ADD_OBSERVATIONS_TRIP_SEGMENT, oldValue, addObservationsTripSegment);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", TripMapConfigDto.class.getSimpleName() + "[", "]")
                .add("tripId='" + tripId + "'")
                .add("addObservations=" + addObservations)
                .add("addLogbook=" + addLogbook)
                .toString();
    }
}
