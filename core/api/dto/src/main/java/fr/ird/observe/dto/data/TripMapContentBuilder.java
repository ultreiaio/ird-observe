package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

/**
 * Created on 02/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public interface TripMapContentBuilder {

    Set<String> getVariableNames();

    String getStyleFileName();

    boolean accept(TripMapDto tripMapDto);

    void addLines(TripMapConfigDto tripMapConfig, List<TripMapPoint> tripMapPoints, Set<String> excludedFeatureNames) throws Exception;

    void addPoints(TripMapConfigDto tripMapConfig, List<TripMapPoint> tripMapPoints, Set<String> excludedFeatureNames) throws Exception;

    File getStyleFile(Path mapDirectory);

    void reset();

    void addLayer(File layerFile) throws IOException;

    void setStyledLayerDescriptor(File styleFile) throws FileNotFoundException;
}
