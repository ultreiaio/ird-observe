package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.LinkedHashSet;

@GenerateJavaBeanDefinition
public class TripMapDto extends AbstractJavaBean implements ObserveDto {

    public static final String PROPERTY_TRIP_ID = "tripId";
    public static final String PROPERTY_POINTS = "points";

    private String tripId;
    private LinkedHashSet<TripMapPoint> points;

    public static TripMapDto of(String tripId, LinkedHashSet<TripMapPoint> points) {
        TripMapDto tripMapDto = new TripMapDto();
        tripMapDto.setTripId(tripId);
        tripMapDto.setPoints(points);
        return tripMapDto;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        String oldValue = getTripId();
        this.tripId = tripId;
        firePropertyChange(PROPERTY_TRIP_ID, oldValue, tripId);
    }

    public boolean isPointsEmpty() {
        return points == null || points.isEmpty();
    }

    public int sizePoints() {
        return points == null ? 0 : points.size();
    }

    public LinkedHashSet<TripMapPoint> getPoints() {
        if (points == null) {
            points = new LinkedHashSet<>();
        }
        return points;
    }

    public void setPoints(LinkedHashSet<TripMapPoint> points) {
        LinkedHashSet<TripMapPoint> oldValue = getPoints();
        this.points = points;
        firePropertyChange(PROPERTY_POINTS, oldValue, points);
    }

}
