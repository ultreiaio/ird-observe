package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;

import java.util.Date;
import java.util.Objects;

public class TripMapPoint implements ObserveDto {

    protected Date time;
    protected Float latitude;
    protected Float longitude;
    protected TripMapPointType type;
    protected boolean valid;

    public static TripMapPoint of(TripMapPointType type, Date time, Float latitude, Float longitude) {
        TripMapPoint result = new TripMapPoint();
        result.setType(Objects.requireNonNull(type));
        result.setTime(time);
        result.setLatitude(latitude);
        result.setLongitude(longitude);
        result.setValid(time != null && latitude != null && longitude != null);
        return result;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public TripMapPointType getType() {
        return type;
    }

    public void setType(TripMapPointType type) {
        this.type = type;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
