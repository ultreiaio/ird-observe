package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

/**
 * <b>Attention : les noms des constantes sont utilisées dans la feuille de style de rendu. Si on change les noms ici,
 * il faut ré-impacter dans les fichiers ps-style.xml et ll-style.xml</b>
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN)
public enum TripMapPointType {

    psTripDepartureHarbour,
    psTripLandingHarbour,

    psActivityObs,
    psActivityObsInHarbour,
    psActivityObsWithFreeSchoolType,
    psActivityObsWithObjectSchoolType,

    psActivityLogbook,
    psActivityLogbookWithSampling,
    psActivityLogbookInHarbour,
    psActivityLogbookWithFreeSchoolType,
    psActivityLogbookWithFreeSchoolTypeWithSampling,
    psActivityLogbookWithObjectSchoolType,
    psActivityLogbookWithObjectSchoolTypeWithSampling,
    psTransmittingBuoyLostLogbook,

    llTripDepartureHarbour,
    llTripLandingHarbour,

    llActivityObs,
    llActivityObsInHarbour,
    llActivityObsWithSettingStart,
    llActivityObsWithSettingEnd,
    llActivityObsWithHaulingStart,
    llActivityObsWithHaulingEnd,

    llActivityLogbook,
    llActivityLogbookInHarbour,
    llActivityLogbookWithSettingStart,
    llActivityLogbookWithSettingEnd,
    llActivityLogbookWithHaulingStart,
    llActivityLogbookWithHaulingEnd;

    private final boolean seine;
    private final boolean longline;
    private final boolean trip;
    private final boolean activity;
    private final boolean obs;
    private final boolean logbook;

    TripMapPointType() {
        this.seine = name().startsWith("ps");
        this.longline = name().startsWith("ll");
        this.trip = name().contains("Trip");
        this.activity = name().contains("Activity");
        this.obs = name().contains("Obs");
        this.logbook = name().contains("Logbook");
    }

    public boolean isSeine() {
        return seine;
    }

    public boolean isLongline() {
        return longline;
    }

    public boolean isTrip() {
        return trip;
    }

    public boolean isActivity() {
        return activity;
    }

    public boolean isObs() {
        return obs;
    }

    public boolean isLogbook() {
        return logbook;
    }

    public String getLabel() {
        return TripMapPointTypeI18n.getLabel(this);
    }
}
