package fr.ird.observe.dto.data;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import io.ultreia.java4all.bean.JavaBean;

/**
 * To be able to validate once any data with a {@code well} id.
 * <p>
 * Created on 06/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.12
 */
public interface WellIdAware extends JavaBean, ObserveDto {

    static String getWellLabel(String well) {
        if (well != null && well.length() == 2) {
            well = " " + well;
        }
        return well;
    }

    String getWell();

    default String getWellLabel() {
        return getWellLabel(getWell());
    }

}
