package fr.ird.observe.dto.data.ll;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.GPSPoint;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.ActivityAware;
import fr.ird.observe.dto.referential.ll.common.VesselActivityReference;

/**
 * Created on 02/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface LlActivityAware extends ActivityAware {
    @Override
    VesselActivityReference getVesselActivity();

    @Override
    default GPSPoint getGPSPoint() {
        return ActivityAware.newGPSPoint(this);
    }

    default boolean isSensorUsedEnabled() {
        return isSetEnabled() || (getVesselActivity() != null && ProtectedIdsLl.LL_COMMON_ACTIVITY_STATION_ID.equals(getVesselActivity().getId()));
    }

    default boolean isEncounterEnabled() {
        return isSetEnabled() || (getVesselActivity() != null && ProtectedIdsLl.LL_COMMON_ACTIVITY_INTERACTION_ID.equals(getVesselActivity().getId()));
    }

    @Override
    default boolean isSetEnabled() {
        return getVesselActivity() != null && getVesselActivity().isAllowSet();
    }

}
