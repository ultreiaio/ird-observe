package fr.ird.observe.dto.data.ll;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.data.WithProportion;
import io.ultreia.java4all.bean.JavaBean;

import java.util.Collection;

/**
 * Created on 17/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface SetGlobalCompositionAware extends ObserveDto, JavaBean {

    String PROPERTY_BAITS_COMPOSITION = "baitsComposition";
    String PROPERTY_FLOATLINES_COMPOSITION = "floatlinesComposition";
    String PROPERTY_HOOKS_COMPOSITION = "hooksComposition";
    String PROPERTY_BRANCHLINES_COMPOSITION = "branchlinesComposition";

    String PROPERTY_FLOATLINES_COMPOSITION_PROPORTION_SUM = "floatlinesCompositionProportionSum";
    String PROPERTY_BRANCHLINES_COMPOSITION_PROPORTION_SUM = "branchlinesCompositionProportionSum";
    String PROPERTY_HOOKS_COMPOSITION_PROPORTION_SUM = "hooksCompositionProportionSum";
    String PROPERTY_BAITS_COMPOSITION_PROPORTION_SUM = "baitsCompositionProportionSum";

    Collection<? extends WithProportion> getFloatlinesComposition();

    Collection<? extends WithProportion> getBaitsComposition();

    Collection<? extends WithProportion> getBranchlinesComposition();

    Collection<? extends WithProportion> getHooksComposition();

    void fireSumChanged(String propertyName, int sum);

    default int getFloatlinesCompositionProportionSum() {
        return WithProportion.sumProportions(getFloatlinesComposition());
    }

    default int getBranchlinesCompositionProportionSum() {
        return WithProportion.sumProportions(getBranchlinesComposition());
    }

    default int getHooksCompositionProportionSum() {
        return WithProportion.sumProportions(getHooksComposition());
    }

    default int getBaitsCompositionProportionSum() {
        return WithProportion.sumProportions(getBaitsComposition());
    }

    default void setFloatlinesCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        fireSumChanged(PROPERTY_FLOATLINES_COMPOSITION_PROPORTION_SUM, sum);
    }

    default void setBranchlinesCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        fireSumChanged(PROPERTY_BRANCHLINES_COMPOSITION_PROPORTION_SUM, sum);
    }

    default void setHooksCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        fireSumChanged(PROPERTY_HOOKS_COMPOSITION_PROPORTION_SUM, sum);
    }

    default void setBaitsCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        fireSumChanged(PROPERTY_BAITS_COMPOSITION_PROPORTION_SUM, sum);
    }

    default void rebuildFloatlinesCompositionProportionSum() {
        setFloatlinesCompositionProportionSum(getFloatlinesCompositionProportionSum());
    }

    default void rebuildBranchlinesCompositionProportionSum() {
        setBranchlinesCompositionProportionSum(getBranchlinesCompositionProportionSum());
    }

    default void rebuildHooksCompositionProportionSum() {
        setHooksCompositionProportionSum(getHooksCompositionProportionSum());
    }

    default void rebuildBaitsCompositionProportionSum() {
        setBaitsCompositionProportionSum(getBaitsCompositionProportionSum());
    }

}
