package fr.ird.observe.dto.data.ll.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ll.common.VesselActivityReference;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Date;

@GenerateJavaBeanDefinition
public class ActivityDto extends GeneratedActivityDto {

    public boolean isHasSet() {
        return getSet() != null;
    }

    public boolean isHasActivitySample() {
        return getActivitySample() != null;
    }

    @Override
    public void setSet(SetReference set) {
        super.setSet(set);
        firePropertyChange(PROPERTY_HAS_SET, isHasSet());
    }

    @Override
    public void setActivitySample(ActivitySampleReference activitySample) {
        super.setActivitySample(activitySample);
        firePropertyChange("hasActivitySample", isHasActivitySample());
    }

    @Override
    public Date getTimeStamp() {
        return getStartTimeStamp();
    }

    @Override
    public void setVesselActivity(VesselActivityReference vesselActivity) {
        boolean oldValueSetEnabled = isSetEnabled();
        super.setVesselActivity(vesselActivity);
        firePropertyChange(PROPERTY_SET_ENABLED, oldValueSetEnabled, isSetEnabled());
    }

    //FIXME See why we don't use it anymore, look nice
//    public SampleReference toNewSampleReference(ReferentialLocale referentialLocale) {
//        SampleDto sampleDto = SampleDto.newDto(new Date());
//        sampleDto.setTimeStamp(getStartTimeStamp());
//        sampleDto.setLatitude(getLatitude());
//        sampleDto.setLongitude(getLongitude());
//        return sampleDto.toReference(referentialLocale);
//    }

}
