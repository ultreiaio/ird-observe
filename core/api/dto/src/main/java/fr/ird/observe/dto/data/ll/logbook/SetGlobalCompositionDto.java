package fr.ird.observe.dto.data.ll.logbook;

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
@GenerateJavaBeanDefinition
public class SetGlobalCompositionDto extends GeneratedSetGlobalCompositionDto  {

    @Override
    public void fireSumChanged(String propertyName, int sum) {
        firePropertyChange(propertyName, -1, sum);
    }

    public void removeIds() {
        setId(null);
        getBaitsComposition().forEach(e -> e.setId(null));
        getBranchlinesComposition().forEach(e -> e.setId(null));
        getFloatlinesComposition().forEach(e -> e.setId(null));
        getHooksComposition().forEach(e -> e.setId(null));
    }
}
