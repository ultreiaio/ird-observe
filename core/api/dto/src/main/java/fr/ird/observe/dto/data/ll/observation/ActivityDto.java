package fr.ird.observe.dto.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ll.common.VesselActivityReference;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class ActivityDto extends GeneratedActivityDto {

    public boolean isHasSet() {
        return getSet() != null;
    }

    @Override
    public void setSet(SetReference set) {
        super.setSet(set);
        firePropertyChange(PROPERTY_HAS_SET, null, isHasSet());
    }

    @Override
    public void setVesselActivity(VesselActivityReference vesselActivity) {
        boolean oldValueSetEnabled = isSetEnabled();
        super.setVesselActivity(vesselActivity);
        firePropertyChange(PROPERTY_SET_ENABLED, oldValueSetEnabled, isSetEnabled());
    }

}
