package fr.ird.observe.dto.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.GPSPoint;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class ActivityReference extends GeneratedActivityReference {

    private transient GPSPoint gpsPoint;

    @Override
    public GPSPoint getGPSPoint() {
        if (gpsPoint == null) {
            gpsPoint = super.getGPSPoint();
        }
        return gpsPoint;
    }

    public final boolean isEmptyNode() {
        return !isSetEnabled() && !isSensorUsedEnabled() && !isEncounterEnabled();
    }

    @Override
    public void removeStatistics() {
        super.removeStatistics();
        if (getSet() != null) {
            getSet().removeStatistics();
        }
    }
}
