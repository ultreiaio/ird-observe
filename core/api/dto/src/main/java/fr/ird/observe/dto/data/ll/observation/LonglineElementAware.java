package fr.ird.observe.dto.data.ll.observation;

/*
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.DtoAndReferenceAware;
import fr.ird.observe.dto.DtoParentAware;
import fr.ird.observe.dto.data.InlineDataDto;

/**
 * Place this contract on any element involved in longline definition.
 * <p>
 * Created on 12/13/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see SectionDto
 * @see BasketDto
 * @see BranchlineDto
 * @since 3.10
 */
public interface LonglineElementAware extends DtoAndReferenceAware, DtoParentAware, InlineDataDto {

    String ONLY_HAULING_IDENTIFIER = "OnlyHaulingIdentifier";

    void setSettingIdentifier(Integer settingIdentifier);

    Integer getSettingIdentifier();

    void setHaulingIdentifier(Integer haulingIdentifier);

    Integer getHaulingIdentifier();

    boolean isNotUsed();

    @Override
    default boolean isDataEmpty() {
        return getSettingIdentifier() == null;
    }

    default boolean canDelete() {
        return !isPersisted() || isNotUsed();
    }
}
