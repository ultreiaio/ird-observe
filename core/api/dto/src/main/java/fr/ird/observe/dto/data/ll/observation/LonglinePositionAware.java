package fr.ird.observe.dto.data.ll.observation;

/*
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.reference.DtoReferenceAware;

/**
 * Place this contract on any data that use a position on a longline.
 * Created on 1/6/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see CatchDto
 * @see TdrDto
 * @since 3.11
 */
public interface LonglinePositionAware extends ObserveDto, DtoReferenceAware {

    String PROPERTY_SECTION = "section";
    String PROPERTY_BASKET = "basket";
    String PROPERTY_BRANCHLINE = "branchline";

    SectionReference getSection();

    BasketReference getBasket();

    BranchlineReference getBranchline();

    void setSection(SectionReference section);

    void setBasket(BasketReference basket);

    void setBranchline(BranchlineReference branchline);

}
