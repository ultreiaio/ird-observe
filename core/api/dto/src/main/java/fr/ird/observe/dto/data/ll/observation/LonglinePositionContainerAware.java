package fr.ird.observe.dto.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.DtoReferenceAware;

import java.util.Collection;

/**
 * Place this contract on any dto rto get the full longline definition and be able to set exact position on a longline.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see SetCatchDto
 * @see SetTdrDto
 */
public interface LonglinePositionContainerAware extends DtoReferenceAware {

    boolean isSectionsEmpty();

    int getSectionsSize();

    Collection<SectionReference> getSections();

    void setSections(Collection<SectionReference> sections);

    boolean isBasketsEmpty();

    int getBasketsSize();

    Collection<BasketReference> getBaskets();

    void setBaskets(Collection<BasketReference> baskets);

    boolean isBranchlinesEmpty();

    int getBranchlinesSize();

    Collection<BranchlineReference> getBranchlines();

    void setBranchlines(Collection<BranchlineReference> branchlines);
}
