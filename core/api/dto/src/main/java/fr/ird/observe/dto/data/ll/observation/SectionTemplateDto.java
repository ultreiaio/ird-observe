package fr.ird.observe.dto.data.ll.observation;

/*
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthFormat;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.lang.Strings;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.9
 * FIXME Use validator
 */
@GenerateJavaBeanDefinition
public class SectionTemplateDto extends GeneratedSectionTemplateDto {

    protected static final Pattern FLOATLINE_LENGTHS_PATTERN = Pattern.compile("(\\d+)(\\.\\d*)?(/(\\d+)(\\.\\d*)?)+:(m|km|nm|ftm)");

    public boolean isIdValid() {
        return Strings.isNotEmpty(id);
    }

    public boolean isValid() {
        return !isDataEmpty() && isFloatlineLengthsValid();
    }

    public boolean isFloatlineLengthsValid() {
        if (floatlineLengths == null) {
            return false;
        }
        Matcher matcher = FLOATLINE_LENGTHS_PATTERN.matcher(floatlineLengths);
        return matcher.matches();
    }

    public io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthFormat getUnitFormat() {
        String text = floatlineLengths.substring(floatlineLengths.indexOf(":") + 1);
        return NauticalLengthFormat.valueOf(text.toUpperCase());
    }

    public List<Float> getFloatlineLengthsAsList() {
        String[] parts = floatlineLengths.substring(0, floatlineLengths.indexOf(":")).split("/");
        List<Float> newLengths = new ArrayList<>(parts.length);
        for (String part : parts) {
            float aFloat = Numbers.roundTwoDigits(Float.valueOf(part));
            newLengths.add(aFloat);
        }
        return newLengths;
    }

    public boolean isCompliantWithBasketCount(int basketsCount) {
        if (!isFloatlineLengthsValid()) {
            throw new IllegalStateException("floatline lengths is not valid");
        }
        List<Float> floatlineLengthsAsList = getFloatlineLengthsAsList();
        return basketsCount + 1 == floatlineLengthsAsList.size();
    }

    public void applyToBaskets(List<BasketDto> baskets) {
        if (!isFloatlineLengthsValid()) {
            throw new IllegalStateException("floatline lengths is not valid");
        }
        Objects.requireNonNull(baskets);
        if (baskets.isEmpty()) {
            throw new IllegalStateException("basket can't be empty");
        }
        if (!isCompliantWithBasketCount(baskets.size())) {
            throw new IllegalStateException("basket size is not compliant with basket count");
        }
        NauticalLengthFormat unitFormat = getUnitFormat();
        List<Float> floatlineLengthsAsList = getFloatlineLengthsAsList();
        Iterator<Float> lengthsIterator = floatlineLengthsAsList.iterator();
        float floatline1;
        float floatline2;
        Iterator<BasketDto> basketIterator = baskets.iterator();
        {
            // on first basket, using the two first lengths
            floatline1 = Numbers.roundTwoDigits(unitFormat.convert(NauticalLengthFormat.M, lengthsIterator.next()));
            floatline2 = Numbers.roundTwoDigits(unitFormat.convert(NauticalLengthFormat.M,lengthsIterator.next()));
            BasketDto basket = basketIterator.next();
            basket.setFloatline1Length(floatline1);
            basket.setFloatline2Length(floatline2);
        }
        while (basketIterator.hasNext()) {
            // floatline1 is previous floatline2
            floatline1 = floatline2;
            floatline2 = Numbers.roundTwoDigits(unitFormat.convert(NauticalLengthFormat.M,lengthsIterator.next()));
            BasketDto basket = basketIterator.next();
            basket.setFloatline1Length(floatline1);
            basket.setFloatline2Length(floatline2);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SectionTemplateDto)) return false;
        if (!super.equals(o)) return false;
        SectionTemplateDto that = (SectionTemplateDto) o;
        return Objects.equals(floatlineLengths, that.floatlineLengths);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), floatlineLengths);
    }

    @Override
    public String toString() {
        return Decorated.toString(this, e ->
                new StringJoiner(", ", SectionTemplateDto.class.getSimpleName() + "[", "]")
                        .add("id='" + id + "'")
                        .add("floatlineLengths='" + floatlineLengths + "'")
                        .toString());
    }
}
