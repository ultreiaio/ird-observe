package fr.ird.observe.dto.data.ps;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.data.ps.logbook.ActivityDto;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

/**
 * Introduce to characterise how activities field {@code number} and {@code time} are managed and how to deal with
 * new activities when changing one of those fields.
 * <p>
 * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2729">issue 2729</a>
 * <p>
 * Created on 17/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN)
public enum ActivitiesAcquisitionMode {
    /**
     * In this case, all activities are driven by {@link ActivityDto#getNumber()},
     * {@link ActivityDto#getTime()} is then not mandatory and the field {@link ActivityDto#getNumber()} is editable.
     */
    BY_NUMBER,
    /**
     * In this case, all activities are driven by {@link ActivityDto#getTime()}, which is then mandatory, the field
     * {@link ActivityDto#getNumber()} is then not editable and always recomputed if necessary.
     */
    BY_TIME;

    public boolean isByNumber() {
        return this == BY_NUMBER;
    }

    @Override
    public String toString() {
        return ActivitiesAcquisitionModeI18n.getLabel(this);
    }
}
