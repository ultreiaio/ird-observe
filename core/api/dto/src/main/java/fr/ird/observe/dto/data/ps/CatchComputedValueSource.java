package fr.ird.observe.dto.data.ps;

/*
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

/**
 * Pour définir comment a été calculé une donnée d'une discarded faune.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN)
public enum CatchComputedValueSource {

    /**
     * Donnée directement calculée à partir d'autre données de la capture.
     *
     * @since 3.0
     */
    fromData,
    /**
     * Donnée calculée à partir de l'échantillon pour la même espèce et la même calée.
     *
     * @since 3.0
     */
    fromSample,
    /**
     * Donnée calculée à partir du référentiel.
     *
     * @since 3.0
     */
    fromReferentiel;

    @Override
    public String toString() {
        return CatchComputedValueSourceI18n.getLabel(this);
    }
}
