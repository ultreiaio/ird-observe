package fr.ird.observe.dto.data.ps;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.WithSimpleComment;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.referential.ps.common.ObjectOperationReference;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 10/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface FloatingObjectAware extends ToolkitId, WithSimpleComment {

    ObjectOperationReference getObjectOperation();

    void setObjectOperation(ObjectOperationReference objectOperation);

    String getSupportVesselName();

    void setSupportVesselName(String supportVesselName);

    DcpComputedValue getComputedWhenArrivingBiodegradable();

    void setComputedWhenArrivingBiodegradable(DcpComputedValue computedWhenArrivingBiodegradable);

    DcpComputedValue getComputedWhenArrivingNonEntangling();

    void setComputedWhenArrivingNonEntangling(DcpComputedValue computedWhenArrivingNonEntangling);

    String getComputedWhenArrivingSimplifiedObjectType();

    void setComputedWhenArrivingSimplifiedObjectType(String computedWhenArrivingSimplifiedObjectType);

    DcpComputedValue getComputedWhenLeavingBiodegradable();

    void setComputedWhenLeavingBiodegradable(DcpComputedValue computedWhenLeavingBiodegradable);

    DcpComputedValue getComputedWhenLeavingNonEntangling();

    void setComputedWhenLeavingNonEntangling(DcpComputedValue computedWhenLeavingNonEntangling);

    String getComputedWhenLeavingSimplifiedObjectType();

    void setComputedWhenLeavingSimplifiedObjectType(String computedWhenLeavingSimplifiedObjectType);

    boolean isMaterialsValid();

    void setMaterialsValid(boolean materialsValid);

    boolean isCanValidateMaterials();

    public void setCanValidateMaterials(boolean canValidateMaterials);

    default TransmittingBuoyAware getTransmittingBuoy(int index) {
        return getTransmittingBuoy().get(index);
    }

    boolean isTransmittingBuoyEmpty();

    boolean isNotTransmittingBuoyEmpty();

    int getTransmittingBuoySize();

    List<? extends TransmittingBuoyAware> getTransmittingBuoy();

    boolean isFloatingObjectPartEmpty();

    boolean isNotFloatingObjectPartEmpty();

    int getFloatingObjectPartSize();

    Collection<? extends FloatingObjectPartAware> getFloatingObjectPart();

    /**
     * Récupère le type d'objectOperation effectué sur les balises du DCP.
     *
     * @return l'ordinal de l'énumeration {@link TypeTransmittingBuoyOperation}
     * @see TypeTransmittingBuoyOperation
     */
    default TypeTransmittingBuoyOperation getTypeTransmittingBuoyOperation() {
        return TypeTransmittingBuoyOperation.guessTransmittingBuoyOperation(getTransmittingBuoy().stream().map(TransmittingBuoyAware::getTransmittingBuoyOperation).collect(Collectors.toList()));
    }

    default TransmittingBuoyAware getFirstBuoy() {
        Collection<? extends TransmittingBuoyAware> transmittingBuoy = getTransmittingBuoy();
        return transmittingBuoy == null || transmittingBuoy.size() == 0 ? null : getTransmittingBuoy(0);
    }

    default TransmittingBuoyAware getSecondBuoy() {
        Collection<? extends TransmittingBuoyAware> transmittingBuoy = getTransmittingBuoy();
        return transmittingBuoy == null || transmittingBuoy.size() < 2 ? null : getTransmittingBuoy(1);
    }

    default FloatingObjectPreset toPreset() {
        FloatingObjectPreset result = new FloatingObjectPreset();
        ObjectOperationReference objectOperation = getObjectOperation();
        if (objectOperation != null) {
            result.setObjectOperationId(objectOperation.getId());
        }
        result.setSupplyName(getSupportVesselName());
        TransmittingBuoyAware firstBuoy = getFirstBuoy();
        if (firstBuoy != null) {
            result.setBuoy1(firstBuoy.toPreset());
        }
        TransmittingBuoyAware secondBuoy = getSecondBuoy();
        if (secondBuoy != null) {
            result.setBuoy2(secondBuoy.toPreset());
        }
        return result;
    }
}
