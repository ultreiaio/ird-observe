package fr.ird.observe.dto.data.ps;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.GPSPoint;
import fr.ird.observe.dto.data.ActivityAware;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import io.ultreia.java4all.util.Dates;

import java.util.Date;

/**
 * Created on 02/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface PsActivityAware extends ActivityAware {
    String PROPERTY_CATCHES_ENABLED = "catchesEnabled";
    String PROPERTY_FLOATING_OBJECT_ENABLED = "floatingObjectEnabled";
    String PROPERTY_CHANGED_ZONE_OPERATION = "changedZoneOperation";

    /**
     * Use this method to get a none null date to compare activities.
     *
     * @return time or topiaCreateDate if time is null.
     */
    default Date getTimeOrTopiaCreateDate() {
        Date time = getTime();
        return time == null ? getTopiaCreateDate() : time;
    }

    @Override
    default boolean isSetEnabled() {
        return getVesselActivity() != null && getVesselActivity().isAllowSet();
    }

    @Override
    default boolean isFloatingObjectEnabled() {
        return getVesselActivity() != null && getVesselActivity().isAllowFad();
    }

    default boolean isActivityEndOfSearching() {
        return VesselActivityReference.isEndOfSearchingOperation(getVesselActivity());
    }

    default boolean isChangedZoneOperation() {
        return getVesselActivity() != null && getVesselActivity().isAllowFpaZoneChange();
    }

    @Override
    VesselActivityReference getVesselActivity();

    @Override
    default GPSPoint getGPSPoint() {
        return ActivityAware.newGPSPoint(this);
    }

    @Override
    default Date getTimeStamp() {
        if (getDate() == null || getTime() == null) {
            return null;
        }
        return Dates.getDateAndTime(getDate(), getTime(), true, false);
    }

    Date getTime();

    Date getDate();

}
