package fr.ird.observe.dto.data.ps;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.WithSimpleComment;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectBuoyPreset;
import fr.ird.observe.dto.referential.common.CountryReference;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.dto.referential.ps.common.TransmittingBuoyOperationReference;
import fr.ird.observe.dto.referential.ps.common.TransmittingBuoyOwnershipReference;
import fr.ird.observe.dto.referential.ps.common.TransmittingBuoyTypeReference;

/**
 * Created on 10/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface TransmittingBuoyAware extends ToolkitId, WithSimpleComment {

    String getCode();

    void setCode(String code);

    TransmittingBuoyOwnershipReference getTransmittingBuoyOwnership();

    void setTransmittingBuoyOwnership(TransmittingBuoyOwnershipReference transmittingBuoyOwnership);

    TransmittingBuoyTypeReference getTransmittingBuoyType();

    void setTransmittingBuoyType(TransmittingBuoyTypeReference transmittingBuoyType);

    TransmittingBuoyOperationReference getTransmittingBuoyOperation();

    void setTransmittingBuoyOperation(TransmittingBuoyOperationReference transmittingBuoyOperation);

    CountryReference getCountry();

    void setCountry(CountryReference country);

    VesselReference getVessel();

    void setVessel(VesselReference vessel);

    Float getLatitude();

    void setLatitude(Float latitude);

    Float getLongitude();

    void setLongitude(Float longitude);

    Integer getQuadrant();

    void setQuadrant(Integer quadrant);

    default FloatingObjectBuoyPreset toPreset() {
        FloatingObjectBuoyPreset result = new FloatingObjectBuoyPreset();
        result.setCode(getCode());
        result.setComment(getComment());
        CountryReference country = getCountry();
        if (country != null) {
            result.setCountryId(country.getId());
        }
        VesselReference vessel = getVessel();
        if (vessel != null) {
            result.setVesselId(vessel.getId());
        }
        TransmittingBuoyOperationReference transmittingBuoyOperation = getTransmittingBuoyOperation();
        if (transmittingBuoyOperation != null) {
            result.setTransmittingBuoyOperationId(transmittingBuoyOperation.getId());
        }
        TransmittingBuoyOwnershipReference transmittingBuoyOwnership = getTransmittingBuoyOwnership();
        if (transmittingBuoyOwnership != null) {
            result.setTransmittingBuoyOwnershipId(transmittingBuoyOwnership.getId());
        }
        TransmittingBuoyTypeReference transmittingBuoyType = getTransmittingBuoyType();
        if (transmittingBuoyType != null) {
            result.setTransmittingBuoyTypeId(transmittingBuoyType.getId());
        }
        return result;
    }
}
