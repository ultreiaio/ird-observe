/*
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.dto.data.ps;

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.referential.ps.common.TransmittingBuoyOperationReference;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN)
public enum TypeTransmittingBuoyOperation {
    // no buoy
    noBuoy(false),
    // one buoy
    visit(false, "1"),
    grab(false, "2"),
    posing(false, "3"),
    lost(true, "4"),
    endOfUse(true, "5"),
    unknown(false, "99"),
    // two buoy
    grabAndChange(false, "2", "3");
    /**
     * TransmittingBuoyOperation codes associated to this type.
     */
    private final String[] transmittingBuoyOperationCodes;
    /**
     * Number of buoy authorized for this type.
     */
    private final int buoyCount;
    /**
     * Use coordinate?
     */
    private final boolean withCoordinate;

    public static TypeTransmittingBuoyOperation guessTransmittingBuoyOperation(List<TransmittingBuoyOperationReference> transmittingBuoy) {
        int transmittingBuoyCount = transmittingBuoy.size();
        switch (transmittingBuoyCount) {
            case 0:
                // no buoy
                return TypeTransmittingBuoyOperation.noBuoy;
            case 1:
                // one buoy
                TransmittingBuoyOperationReference operation = transmittingBuoy.iterator().next();
                String code = operation.getCode();
                for (TypeTransmittingBuoyOperation typeTransmittingBuoyOperation : TypeTransmittingBuoyOperation.values()) {
                    if (typeTransmittingBuoyOperation.getBuoyCount() == 1 && typeTransmittingBuoyOperation.getTransmittingBuoyOperationCodes()[0].equals(code)) {
                        return typeTransmittingBuoyOperation;
                    }
                }
                Set<String> availableCodes = new TreeSet<>();
                for (TypeTransmittingBuoyOperation typeTransmittingBuoyOperation : TypeTransmittingBuoyOperation.values()) {
                    if (typeTransmittingBuoyOperation.getBuoyCount() == 1) {
                        availableCodes.add(typeTransmittingBuoyOperation.getTransmittingBuoyOperationCodes()[0]);
                    }
                }
                throw new IllegalStateException(String.format("When having a single buoy, TransmittingBuoyOperation code must be among: %s, but was %s", availableCodes, code));
            case 2:
                //  two buoy
                return TypeTransmittingBuoyOperation.grabAndChange;
            default:
                throw new IllegalStateException(String.format("A floating object, can only have 0, 1 or 2 buoy(s) but found %d buoys", transmittingBuoyCount));
        }
    }

    TypeTransmittingBuoyOperation(boolean withCoordinate, String... transmittingBuoyOperationCodes) {
        this.transmittingBuoyOperationCodes = transmittingBuoyOperationCodes;
        this.withCoordinate = withCoordinate;
        this.buoyCount = transmittingBuoyOperationCodes.length;
    }

    public boolean isWithCoordinate() {
        return withCoordinate;
    }

    public String[] getTransmittingBuoyOperationCodes() {
        return transmittingBuoyOperationCodes;
    }

    public int getBuoyCount() {
        return buoyCount;
    }

    public String getLabel() {
        return TypeTransmittingBuoyOperationI18n.getLabel(this);
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
