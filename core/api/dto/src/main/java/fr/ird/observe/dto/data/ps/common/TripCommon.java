package fr.ird.observe.dto.data.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.referential.ps.common.AcquisitionStatusReference;
import io.ultreia.java4all.bean.JavaBean;

/**
 * Created on 27/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface TripCommon extends TripAware, JavaBean, TripLocalmarketCommon, TripLogbookCommon {

    String PROPERTY_OBSERVATIONS_ENABLED = "observationsEnabled";
    String PROPERTY_LANDING_ENABLED = "landingEnabled";
    String PROPERTY_ADVANCED_SAMPLING_ENABLED = "advancedSamplingEnabled";

    boolean isObservationsFilled();

    boolean isLandingFilled();

    boolean isAdvancedSamplingFilled();

    AcquisitionStatusReference getObservationsAcquisitionStatus();

    AcquisitionStatusReference getLandingAcquisitionStatus();

    AcquisitionStatusReference getAdvancedSamplingAcquisitionStatus();

    default boolean isObservationsAvailability() {
        return isObservationsEnabled();
    }

    default boolean isObservationsEnabled() {
        return TripAware.isFieldEnabled(getObservationsAcquisitionStatus());
    }

    @Override
    default boolean isLogbookAvailability() {
        return isLogbookEnabled();
    }

    default boolean isLandingEnabled() {
        return TripAware.isFieldEnabled(getLandingAcquisitionStatus());
    }

    default boolean isAdvancedSamplingEnabled() {
        return TripAware.isFieldEnabled(getAdvancedSamplingAcquisitionStatus());
    }

    default boolean isLogbookCommonEnabled() {
        return isLogbookEnabled()
                || isTargetWellsSamplingEnabled()
                || isLandingEnabled()
                || isLocalmarketEnabled()
                || isLocalmarketWellsSamplingEnabled()
                || isLocalmarketSurveySamplingEnabled()
                || isAdvancedSamplingEnabled();
    }

    //
    // short-cuts for ui
    //

    default boolean isRouteObsEnabled() {
        return isObservationsEnabled();
    }
}
