package fr.ird.observe.dto.data.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.referential.ps.common.AcquisitionStatusReference;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.i18n.spi.bean.RegisterI18nLabel;

/**
 * Created on 29/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@RegisterI18nLabel(target = TripLocalmarketCommon.class, properties = {"localMarketAcquisitionStatus", "localMarketSurveySamplingAcquisitionStatus", "localMarketWellsSamplingAcquisitionStatus"})
public interface TripLocalmarketCommon extends ToolkitId, JavaBean {

    String PROPERTY_LOCALMARKET_ENABLED = "localmarketEnabled";
    String PROPERTY_LOCALMARKET_WELLS_SAMPLING_ENABLED = "localmarketWellsSamplingEnabled";
    String PROPERTY_LOCALMARKET_SURVEY_SAMPLING_ENABLED = "localmarketSurveySamplingEnabled";

    AcquisitionStatusReference getLocalMarketAcquisitionStatus();

    AcquisitionStatusReference getLocalMarketWellsSamplingAcquisitionStatus();

    AcquisitionStatusReference getLocalMarketSurveySamplingAcquisitionStatus();

    boolean isLocalmarketFilled();

    boolean isLocalmarketWellsSamplingFilled();

    boolean isLocalmarketSurveySamplingFilled();

    default boolean isLocalmarketWellsSamplingEnabled() {
        return TripAware.isFieldEnabled(getLocalMarketWellsSamplingAcquisitionStatus());
    }

    default boolean isLocalmarketSurveySamplingEnabled() {
        return TripAware.isFieldEnabled(getLocalMarketSurveySamplingAcquisitionStatus());
    }

    //
    // short-cuts for ui
    //

    default boolean isLocalmarketSurveyEnabled() {
        return isLocalmarketSurveySamplingEnabled();
    }

    default boolean isLocalmarketBatchEnabled() {
        return isLocalmarketEnabled();
    }

    default boolean isLocalmarketEnabled() {
        return TripAware.isFieldEnabled(getLocalMarketAcquisitionStatus());
    }

    default boolean isLocalmarketSampleEnabled() {
        return isLocalmarketWellsSamplingEnabled();
    }
}
