package fr.ird.observe.dto.data.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.referential.ps.common.AcquisitionStatusReference;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.i18n.spi.bean.RegisterI18nLabel;

/**
 * Created on 29/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@RegisterI18nLabel(target = TripLogbookCommon.class, properties = {"logbookAcquisitionStatus", "targetWellsSamplingAcquisitionStatus"})
public interface TripLogbookCommon extends ToolkitId, JavaBean {

    String PROPERTY_LOGBOOK_ENABLED = "logbookEnabled";
    String PROPERTY_LOGBOOK_COMMON_ENABLED = "logbookCommonEnabled";
    String PROPERTY_TARGET_WELLS_SAMPLING_ENABLED = "targetWellsSamplingEnabled";

//    int getRouteLogbookSize();

//    int getSampleSize();

//    int getWellPlanSize();

    boolean isLogbookFilled();

    boolean isTargetWellsSamplingFilled();

    AcquisitionStatusReference getLogbookAcquisitionStatus();

    AcquisitionStatusReference getTargetWellsSamplingAcquisitionStatus();

    default boolean isLogbookEnabled() {
        return TripAware.isFieldEnabled(getLogbookAcquisitionStatus());
    }

    default boolean isTargetWellsSamplingEnabled() {
        return TripAware.isFieldEnabled(getTargetWellsSamplingAcquisitionStatus());
    }

    //
    // short-cuts for ui
    //

    default boolean isWellPlanEnabled() {
        return isTargetWellsSamplingEnabled();
    }

    default boolean isWellEnabled() {
        return isTargetWellsSamplingEnabled();
    }

    default boolean isRouteLogbookEnabled() {
        return isLogbookEnabled();
    }

    default boolean isSampleEnabled() {
        return isLogbookEnabled();
    }
}
