package fr.ird.observe.dto.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/**
 * Created on 10/06/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
@GenerateJavaBeanDefinition
public class FloatingObjectBuoyPreset extends AbstractJavaBean implements ObserveDto {

    public static final String PROPERTY_CODE = "code";
    public static final String PROPERTY_COMMENT = "comment";
    public static final String PROPERTY_TRANSMITTING_BUOY_OWNERSHIP_ID = "transmittingBuoyOwnershipId";
    public static final String PROPERTY_TRANSMITTING_BUOY_TYPE_ID = "transmittingBuoyTypeId";
    public static final String PROPERTY_TRANSMITTING_BUOY_OPERATION_ID = "transmittingBuoyOperationId";
    public static final String PROPERTY_COUNTRY_ID = "countryId";
    public static final String PROPERTY_VESSEL_ID = "vesselId";

    private String code;
    private String comment;
    private String transmittingBuoyOwnershipId;
    private String transmittingBuoyTypeId;
    private String transmittingBuoyOperationId;
    private String vesselId;
    private String countryId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        String oldValue = getCode();
        this.code = code;
        firePropertyChange(PROPERTY_CODE, oldValue, code);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        String oldValue = getComment();
        this.comment = comment;
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    public String getVesselId() {
        return vesselId;
    }

    public void setVesselId(String vesselId) {
        String oldValue = getVesselId();
        this.vesselId = vesselId;
        firePropertyChange(PROPERTY_VESSEL_ID, oldValue, vesselId);
    }

    public String getTransmittingBuoyOwnershipId() {
        return transmittingBuoyOwnershipId;
    }

    public void setTransmittingBuoyOwnershipId(String transmittingBuoyOwnershipId) {
        String oldValue = getTransmittingBuoyOwnershipId();
        this.transmittingBuoyOwnershipId = transmittingBuoyOwnershipId;
        firePropertyChange(PROPERTY_TRANSMITTING_BUOY_OWNERSHIP_ID, oldValue, transmittingBuoyOwnershipId);
    }

    public String getTransmittingBuoyTypeId() {
        return transmittingBuoyTypeId;
    }

    public void setTransmittingBuoyTypeId(String transmittingBuoyTypeId) {
        String oldValue = getTransmittingBuoyTypeId();
        this.transmittingBuoyTypeId = transmittingBuoyTypeId;
        firePropertyChange(PROPERTY_TRANSMITTING_BUOY_TYPE_ID, oldValue, transmittingBuoyTypeId);
    }

    public String getTransmittingBuoyOperationId() {
        return transmittingBuoyOperationId;
    }

    public void setTransmittingBuoyOperationId(String transmittingBuoyOperationId) {
        String oldValue = getTransmittingBuoyOperationId();
        this.transmittingBuoyOperationId = transmittingBuoyOperationId;
        firePropertyChange(PROPERTY_TRANSMITTING_BUOY_OPERATION_ID, oldValue, transmittingBuoyOperationId);
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        String oldValue = getCountryId();
        this.countryId = countryId;
        firePropertyChange(PROPERTY_COUNTRY_ID, oldValue, countryId);
    }
}
