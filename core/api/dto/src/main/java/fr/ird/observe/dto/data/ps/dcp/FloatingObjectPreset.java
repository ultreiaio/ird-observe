package fr.ird.observe.dto.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.WithI18n;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.i18n.spi.bean.RegisterI18nLabel;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 10/06/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0
 */
@GenerateJavaBeanDefinition
@RegisterI18nLabel(target = FloatingObjectPreset.class, properties = {
        "action.add", "action.add.tip", "add.title", "choose.title",
        "label1", "label2", "label3", "label4", "label5", "label6", "label7", "label8", "objectOperation", "supplyName", "supportVesselName"})
public class FloatingObjectPreset extends AbstractJavaBean implements JsonAware, WithI18n {

    public static final String PROPERTY_SUPPLY_NAME = "supplyName";
    public static final String PROPERTY_OBJECT_OPERATION_ID = "objectOperationId";
    public static final String PROPERTY_BUOY_1 = "buoy1";
    public static final String PROPERTY_BUOY_2 = "buoy2";
    public static final String PROPERTY_WHEN_LEAVING_MATERIALS = "whenLeavingMaterials";
    public static final String PROPERTY_WHEN_ARRIVING_MATERIALS = "whenArrivingMaterials";
    public static final String PROPERTY_FILE_NAME = "fileName";
    private String label1;
    private String label2;
    private String label3;
    private String label4;
    private String label5;
    private String label6;
    private String label7;
    private String label8;
    private String objectOperationId;
    private String supplyName;
    private FloatingObjectBuoyPreset buoy1;
    private FloatingObjectBuoyPreset buoy2;
    private Map<String, String> whenArrivingMaterials;
    private Map<String, String> whenLeavingMaterials;
    private transient String fileName;

    @Override
    public String getLabel1() {
        return label1;
    }

    @Override
    public void setLabel1(String label1) {
        String oldValue = getLabel1();
        this.label1 = label1;
        firePropertyChange(PROPERTY_LABEL1, oldValue, label1);
    }

    @Override
    public String getLabel2() {
        return label2;
    }

    @Override
    public void setLabel2(String label2) {
        String oldValue = getLabel2();
        this.label2 = label2;
        firePropertyChange(PROPERTY_LABEL2, oldValue, label2);
    }

    @Override
    public String getLabel3() {
        return label3;
    }

    @Override
    public void setLabel3(String label3) {
        String oldValue = getLabel3();
        this.label3 = label3;
        firePropertyChange(PROPERTY_LABEL3, oldValue, label3);
    }

    @Override
    public String getLabel4() {
        return label4;
    }

    @Override
    public void setLabel4(String label4) {
        String oldValue = getLabel4();
        this.label4 = label4;
        firePropertyChange(PROPERTY_LABEL4, oldValue, label4);
    }

    @Override
    public String getLabel5() {
        return label5;
    }

    @Override
    public void setLabel5(String label5) {
        String oldValue = getLabel5();
        this.label5 = label5;
        firePropertyChange(PROPERTY_LABEL5, oldValue, label5);
    }

    @Override
    public String getLabel6() {
        return label6;
    }

    @Override
    public void setLabel6(String label6) {
        String oldValue = getLabel6();
        this.label6 = label6;
        firePropertyChange(PROPERTY_LABEL6, oldValue, label6);
    }

    @Override
    public String getLabel7() {
        return label7;
    }

    @Override
    public void setLabel7(String label7) {
        String oldValue = getLabel7();
        this.label7 = label7;
        firePropertyChange(PROPERTY_LABEL7, oldValue, label7);
    }

    @Override
    public String getLabel8() {
        return label8;
    }

    @Override
    public void setLabel8(String label8) {
        String oldValue = getLabel8();
        this.label8 = label8;
        firePropertyChange(PROPERTY_LABEL8, oldValue, label8);
    }

    public String getObjectOperationId() {
        return objectOperationId;
    }

    public void setObjectOperationId(String objectOperationId) {
        String oldValue = getObjectOperationId();
        this.objectOperationId = objectOperationId;
        firePropertyChange(PROPERTY_OBJECT_OPERATION_ID, oldValue, objectOperationId);
    }

    public String getSupplyName() {
        return supplyName;
    }

    public void setSupplyName(String supplyName) {
        String oldValue = getSupplyName();
        this.supplyName = supplyName;
        firePropertyChange(PROPERTY_SUPPLY_NAME, oldValue, supplyName);
    }

    public FloatingObjectBuoyPreset getBuoy1() {
        return buoy1;
    }

    public void setBuoy1(FloatingObjectBuoyPreset buoy1) {
        FloatingObjectBuoyPreset oldValue = getBuoy1();
        this.buoy1 = buoy1;
        firePropertyChange(PROPERTY_BUOY_1, oldValue, buoy1);
    }

    public FloatingObjectBuoyPreset getBuoy2() {
        return buoy2;
    }

    public void setBuoy2(FloatingObjectBuoyPreset buoy2) {
        FloatingObjectBuoyPreset oldValue = getBuoy2();
        this.buoy2 = buoy2;
        firePropertyChange(PROPERTY_BUOY_2, oldValue, buoy2);
    }

    public Map<String, String> getWhenArrivingMaterials() {
        return whenArrivingMaterials;
    }

    public void setWhenArrivingMaterials(Map<String, String> whenArrivingMaterials) {
        Map<String, String> oldValue = getWhenArrivingMaterials();
        this.whenArrivingMaterials = whenArrivingMaterials == null ? null : new TreeMap<>(whenArrivingMaterials);
        firePropertyChange(PROPERTY_WHEN_ARRIVING_MATERIALS, oldValue, whenArrivingMaterials);
    }

    public Map<String, String> getWhenLeavingMaterials() {
        return whenLeavingMaterials;
    }

    public void setWhenLeavingMaterials(Map<String, String> whenLeavingMaterials) {
        Map<String, String> oldValue = getWhenLeavingMaterials();
        this.whenLeavingMaterials = whenLeavingMaterials == null ? null : new TreeMap<>(whenLeavingMaterials);
        firePropertyChange(PROPERTY_WHEN_LEAVING_MATERIALS, oldValue, whenLeavingMaterials);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        String oldValue = this.fileName;
        this.fileName = fileName;
        firePropertyChange(PROPERTY_FILE_NAME, oldValue, fileName);
    }

    public String getFilenamePrefix() {
        if (fileName == null) {
            return null;
        }
        return fileName.substring(0, fileName.indexOf("-"));
    }


    public String getFilenameSuffix() {
        if (fileName == null) {
            return null;
        }
        return fileName.substring(fileName.indexOf("-"));
    }

    public Set<String> getIds() {
        List<String> result = new LinkedList<>();
        addIds(result, whenArrivingMaterials);
        addIds(result, whenLeavingMaterials);
        addId(result, objectOperationId);
        addIds(result, buoy1);
        addIds(result, buoy2);
        return new LinkedHashSet<>(result);
    }

    public Set<String> getMaterialIds() {
        List<String> result = new LinkedList<>();
        addIds(result, whenArrivingMaterials);
        addIds(result, whenLeavingMaterials);
        return new LinkedHashSet<>(result);
    }

    private void addIds(List<String> builder, Map<String, String> buoy) {
        if (buoy != null) {
            builder.addAll(buoy.keySet());
        }
    }

    private void addIds(List<String> builder, FloatingObjectBuoyPreset buoy) {
        if (buoy != null) {
            addId(builder, buoy.getCountryId());
            addId(builder, buoy.getTransmittingBuoyOwnershipId());
            addId(builder, buoy.getTransmittingBuoyOperationId());
            addId(builder, buoy.getTransmittingBuoyTypeId());
        }
    }

    private void addId(List<String> builder, String id) {
        if (id != null) {
            builder.add(id);
        }
    }
}
