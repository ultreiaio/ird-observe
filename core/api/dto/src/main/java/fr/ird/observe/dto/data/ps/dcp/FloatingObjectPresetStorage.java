package fr.ird.observe.dto.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 10/06/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class FloatingObjectPresetStorage {

    private static final Logger log = LogManager.getLogger(FloatingObjectPresetStorage.class);

    public static Path getNextFilename(Path directory) throws IOException {
        long nbFiles = Files.walk(directory).filter(f -> !Files.isDirectory(f) && f.toFile().getName().endsWith(".yml")).count();
        String result = getNextFilename(++nbFiles,"-preset.yml");
        while (Files.exists(directory.resolve(result))) {
            result = getNextFilename(++nbFiles,"-preset.yml");
        }
        return directory.resolve(result);
    }

    public static String getNextFilename(long nbFiles, String suffix) {
        return (nbFiles > 9 ? nbFiles + "" : "0" + nbFiles) + suffix;
    }

    public static Map<Path, FloatingObjectPreset> loadAll(Path directory) throws IOException {
        Map<Path, FloatingObjectPreset> result = new TreeMap<>();
        Files.walk(directory).filter(f -> !Files.isDirectory(f) && f.toFile().getName().endsWith(".yml")).forEach(f -> result.put(f, load(f)));
        return Collections.unmodifiableMap(result);
    }

    public static FloatingObjectPreset load(Path file) {
        log.info("Loading floating object preset from: {}", file);
        try (Reader fileReader = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
            try (YamlReader reader = new YamlReader(fileReader, createConfig())) {
                FloatingObjectPreset result = reader.read(FloatingObjectPreset.class);
                result.setFileName(file.toFile().getName());
                return result;
            }
        } catch (Exception e) {
            throw new RuntimeException(String.format("Could not read floating object preset from: %s", file), e);
        }
    }

    public static void store(FloatingObjectPreset preset, Path file) throws IOException {

        log.info("Store floating object preset to: {}", file);

        if (Files.notExists(file.getParent())) {
            Files.createDirectories(file.getParent());
        }
        try (BufferedWriter writer = Files.newBufferedWriter(file, StandardCharsets.UTF_8)) {
            try (YamlWriter yamlWriter = new YamlWriter(writer, createConfig())) {
                preset.setFileName(null);
                yamlWriter.write(preset);
            } finally {
                preset.setFileName(file.toFile().getName());
            }
        } catch (Exception e) {
            throw new IOException(String.format("Could not write floating object preset to: %s", file), e);
        }

    }

    private static YamlConfig createConfig() {
        YamlConfig yamlConfig = new YamlConfig();
        yamlConfig.writeConfig.setIndentSize(2);
        yamlConfig.writeConfig.setKeepBeanPropertyOrder(true);
        yamlConfig.writeConfig.setWriteRootTags(false);
        return yamlConfig;
    }
}
