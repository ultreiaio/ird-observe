package fr.ird.observe.dto.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 10/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class FloatingObjectPresetsStorage {

    private static final Logger log = LogManager.getLogger(FloatingObjectPresetsStorage.class);
    private final Path directory;
    private Map<Path, FloatingObjectPreset> presetsMap;

    public FloatingObjectPresetsStorage(Path directory) {
        this.directory = directory;
    }

    public void add(FloatingObjectPreset preset) {
        try {
            Path nextFilename = FloatingObjectPresetStorage.getNextFilename(directory);
            Path file = directory.resolve(nextFilename);
            log.info("Adding {} dcp preset to: {}", directory.toFile().getName(), file);
            FloatingObjectPresetStorage.store(Objects.requireNonNull(preset), file);
            presetsMap.put(file, preset);
        } catch (IOException e) {
            throw new RuntimeException("Can't add dcp preset", e);
        }
    }

    public int size() {
        return getPresets().size();
    }

    public Set<FloatingObjectPreset> getPresets() {
        return new LinkedHashSet<>(getPresetsMap().values());
    }

    public Map<Path, FloatingObjectPreset> getPresetsMap() {
        if (presetsMap == null) {
            log.info("Loading ps dcp presets... from {}", directory);
            try {
                presetsMap = FloatingObjectPresetStorage.loadAll(directory);
                log.info("Load {} ps dcp presets.", presetsMap.size());
            } catch (IOException e) {
                log.error("Can't load presets", e);
                presetsMap = new TreeMap<>();
            }
        }
        return presetsMap;
    }

    public void setPresetsMap(Map<Path, FloatingObjectPreset> psObservationPresets) {
        this.presetsMap = psObservationPresets;
        log.info("Set {} ps dcp presets.", psObservationPresets.size());
    }

    public void reset() {
        presetsMap = null;
    }

    public Path getDirectory() {
        return directory;
    }

    public void store(Map<Path, FloatingObjectPreset> presetsMap) throws IOException {
        Map<Path, FloatingObjectPreset> oldPresetsMap = getPresetsMap();
        for (Path path : oldPresetsMap.keySet()) {
            if (!presetsMap.containsKey(path)) {
                log.warn("Delete old preset {}", path);
                Files.delete(path);
            }
        }
        this.presetsMap = presetsMap;
        for (Map.Entry<Path, FloatingObjectPreset> entry : presetsMap.entrySet()) {
            Path path = entry.getKey();
            if (!oldPresetsMap.containsKey(path)) {
                log.warn("Store preset {}", path);
                FloatingObjectPresetStorage.store(entry.getValue(), path);
            }
        }
    }
}
