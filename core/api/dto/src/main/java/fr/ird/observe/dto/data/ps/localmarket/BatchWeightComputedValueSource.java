package fr.ird.observe.dto.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

/**
 * Pour décrire comment la donnée calculée a été construite.
 * <p>
 * Created on 22/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN)
public enum BatchWeightComputedValueSource {
    /**
     * Donnée directement du poids moyen d'un espèce.
     */
    fromSpeciesMeanWeight,
    /**
     * Donnée calculée à partir du poids moyen d'un type de conditionnement.
     */
    fromPackagingMeanWeight;

    public String getLabel() {
        return BatchWeightComputedValueSourceI18n.getLabel(this);
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
