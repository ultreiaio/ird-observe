package fr.ird.observe.dto.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.WithProportion;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.List;

/**
 * Created on 18/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@GenerateJavaBeanDefinition
public class SurveyDto extends GeneratedSurveyDto {

    public int getSurveyPartProportionSum() {
        return WithProportion.sumProportions(getSurveyPart());
    }

    @Override
    public void setSurveyPart(List<SurveyPartDto> surveyPart) {
        this.surveyPart = surveyPart;
        // fires with old value at null, otherwise list could be equals (only topiaId is used for equals)
        firePropertyChange(PROPERTY_SURVEY_PART, null, surveyPart);
        rebuildSurveyPartProportionSum();
    }

    public void setSurveyPartProportionSum(int proportionSum) {
        firePropertyChange(PROPERTY_SURVEY_PART_PROPORTION_SUM, 0, proportionSum);
    }

    public void rebuildSurveyPartProportionSum() {
        setSurveyPartProportionSum(getSurveyPartProportionSum());
    }
}
