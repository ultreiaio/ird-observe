package fr.ird.observe.dto.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.util.Dates;

import java.util.Date;

/**
 * Created on 23/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@GenerateJavaBeanDefinition
public class ActivityDto extends GeneratedActivityDto {

    public boolean isCoordinateRequiredForVesselActivity() {
        return VesselActivityReference.isCoordinateRequiredForVesselActivity(getVesselActivity());
    }

    @Override
    public void setVesselActivity(VesselActivityReference vesselActivity) {
        boolean oldValueSetEnabled = isSetEnabled();
        boolean oldValueChangedZoneOperation = isChangedZoneOperation();
        boolean oldValueCatchesEnabled = isCatchesEnabled();
        boolean oldValueFloatingObjectEnabled = isFloatingObjectEnabled();
        super.setVesselActivity(vesselActivity);
        firePropertyChange(PROPERTY_SET_ENABLED, oldValueSetEnabled, isSetEnabled());
        firePropertyChange(PROPERTY_CATCHES_ENABLED, oldValueCatchesEnabled, isCatchesEnabled());
        firePropertyChange(PROPERTY_FLOATING_OBJECT_ENABLED, oldValueFloatingObjectEnabled, isFloatingObjectEnabled());
        firePropertyChange(PROPERTY_CHANGED_ZONE_OPERATION, oldValueChangedZoneOperation, isChangedZoneOperation());
    }

    @Override
    public void setTime(Date time) {
        if (date == null) {
            date = time == null ? null : Dates.getDay(time);
        }
        super.setTime(time == null ? null : Dates.getTime(time, false, false));
    }

    public ActivityStubDto toStub() {
        ActivityStubDto result = new ActivityStubDto();
        result.setId(getTopiaId());
        result.setTopiaVersion(getTopiaVersion());
        result.setTopiaCreateDate(getTopiaCreateDate());
        result.setLastUpdateDate(getLastUpdateDate());

        result.setTime(getTime());
        result.setLatitude(getLatitude());
        result.setLongitude(getLongitude());
        result.setNumber(getNumber());
        result.setSeaSurfaceTemperature(getSeaSurfaceTemperature());
        result.setVesselActivity(getVesselActivity());
        return result;
    }
}
