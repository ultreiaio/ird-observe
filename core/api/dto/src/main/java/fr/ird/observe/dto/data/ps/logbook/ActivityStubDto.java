package fr.ird.observe.dto.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.Dates;

import java.util.Date;

/**
 * Created on 23/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@GenerateJavaBeanDefinition
public class ActivityStubDto extends GeneratedActivityStubDto {

    static {
        I18n.n("observe.data.ps.logbook.ActivityStub.type");
    }

    public Date getTimeSecond() {
        return Dates.getTime(time, false, false);
    }

    /**
     * Use this method to get a none null date to compare activities.
     *
     * @return time or topiaCreateDate if time is null.
     */
    public Date getTimeOrTopiaCreateDate() {
        Date time = getTime();
        return time == null ? getTopiaCreateDate() : time;
    }

    public boolean skipValidate() {
        return getLatitude() == null || getLongitude() == null;
    }


    @Override
    public void setTime(Date time) {
        if (date == null) {
            date = time == null ? null : Dates.getDay(time);
        }
        super.setTime(time == null ? null : Dates.getTime(time, false, false));
    }

}
