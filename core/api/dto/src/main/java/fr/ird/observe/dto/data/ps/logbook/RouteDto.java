package fr.ird.observe.dto.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.util.Dates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.Objects;

/**
 * Created on 23/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("unused")
@GenerateJavaBeanDefinition
public class RouteDto extends GeneratedRouteDto {

    private static final Logger log = LogManager.getLogger(RouteDto.class);

    public boolean isTimeAvailable(String activityId, Date timeOrTopiaCreateDate) {
        Date currentTime = Dates.getTime(timeOrTopiaCreateDate, false, false);
        return getActivity().stream()
                .noneMatch(activity -> Objects.equals(currentTime, activity.getTimeOrTopiaCreateDate()) && !Objects.equals(activityId, activity.getId()));
    }

    public ActivityStubDto getPreviousActivity(String activitySeineId) {
        ActivityStubDto previous = null;
        if (activitySeineId != null) {
            int currentPosition = Iterables.indexOf(getActivity(), ActivityStubDto.newIdPredicate(activitySeineId)::test);
            if (currentPosition >= 1) {
                previous = Iterables.get(getActivity(), currentPosition - 1);
            }
        }
        if (previous != null) {
            log.debug("previous activity " + previous.getTime());
        } else {
            log.debug("no previous activity for " + activity);
        }
        return previous;
    }
}
