package fr.ird.observe.dto.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.List;


/**
 * Created on 23/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
@GenerateJavaBeanDefinition
public class ActivityReference extends GeneratedActivityReference {

    private List<FloatingObjectReference> floatingObject;

    public List<FloatingObjectReference> getFloatingObject() {
        return floatingObject;
    }

    public void setFloatingObject(List<FloatingObjectReference> floatingObject) {
        this.floatingObject = floatingObject;
    }

    public final boolean isEmptyNode() {
        return getSet() == null && (floatingObject == null || floatingObject.isEmpty());
    }

    @Override
    public void removeStatistics() {
        super.removeStatistics();
        if (getSet() != null) {
            getSet().removeStatistics();
        }
        if (floatingObject != null) {
            floatingObject.forEach(FloatingObjectReference::removeStatistics);
        }
    }
}
