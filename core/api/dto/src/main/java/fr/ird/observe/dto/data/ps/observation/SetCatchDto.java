package fr.ird.observe.dto.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.common.SpeciesReference;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@GenerateJavaBeanDefinition
public class SetCatchDto extends GeneratedSetCatchDto {

    /**
     * Get the set of unsafe species no more used in the {@code incoming} dto.
     *
     * @param incoming the dto to compare this
     * @return the set of unsafe species no more used in incoming dto
     */
    public Set<SpeciesReference> getRemovedUnsafeSpecies(SetCatchDto incoming) {
        // Get all unsafe species
        Set<SpeciesReference> allUnsafeSpecies = getUnsafeSpecies();
        // Get incoming used unsafe species
        Set<SpeciesReference> incomingUsedUnsafeSpecies = incoming.getUnsafeSpecies();
        // Remove for all the used
        allUnsafeSpecies.removeAll(incomingUsedUnsafeSpecies);
        // This will return the set of no more used unsafe species
        return allUnsafeSpecies;
    }

    /**
     * @return set of unsafe species used in catches
     */
    public Set<SpeciesReference> getUnsafeSpecies() {
        return getCatches().stream().filter(c -> c.isHasSample() || c.isHasRelease()).map(CatchDto::getSpecies).collect(Collectors.toSet());
    }

    /**
     * @param species species to test
     * @return count of usage of this species in catches
     */
    public int getUnsafeSpeciesUsageCount(SpeciesReference species) {
        return (int) getCatches().stream().filter(d -> Objects.equals(species, d.getSpecies())).count();
    }
}
