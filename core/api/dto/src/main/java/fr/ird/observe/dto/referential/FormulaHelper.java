package fr.ird.observe.dto.referential;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveUtil;
import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 05/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class FormulaHelper {

    public static final String VARIABLE_WEIGHT = "P";
    public static final String VARIABLE_LENGTH = "L";
    public static final String VARIABLE_INPUT = "I";
    public static final String VARIABLE_OUTPUT = "O";
    public static final String COEFFICIENT_B = "b";
    private static final Logger log = LogManager.getLogger(FormulaHelper.class);
    private static final Pattern COEFFICIENTS_PATTERN = Pattern.compile("(.+)=(.+)");
    private static final String VARIABLE_X = "x";
    private static ScriptEngine scriptEngine;

    private static ScriptEngine getScriptEngine() {
        if (scriptEngine == null) {
            scriptEngine = ObserveUtil.getScriptEngine();
        }
        return scriptEngine;
    }

    public static Map<String, Double> getCoefficientValues(WithFormula formula) {
        Map<String, Double> result = new TreeMap<>();
        String coefficients = formula.getCoefficients();
        if (coefficients != null) {
            for (String coefficientDef : coefficients.split(":")) {
                Matcher matcher = COEFFICIENTS_PATTERN.matcher(coefficientDef.trim());
                log.debug("constant to test = " + coefficientDef);
                if (matcher.matches()) {
                    String key = matcher.group(1);
                    String val = matcher.group(2);
                    try {
                        Double d = Double.valueOf(val);
                        result.put(key, d);
                        log.debug("detects coefficient " + key + '=' + val);
                    } catch (NumberFormatException e) {
                        // pas pu recupere le count...
                        log.warn("could not parse double " + val + " for coefficient " + key, e);
                    }
                }
            }
        }
        return result;
    }

    public static boolean validateRelation(WithFormula withFormula, String formula, String variable) {
        boolean result = false;
        if (Strings.isNotEmpty(formula)) {
            Map<String, Double> coefficientValues = withFormula.getCoefficientValues();
            ScriptEngine engine = getScriptEngine();
            Bindings bindings = engine.createBindings();
            addBindings(coefficientValues, bindings, variable, 1);
            try {
                engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
                Number o = (Number) engine.eval("parseFloat(" + formula + ")");
                log.debug("evaluation ok : " + formula + " (" + variable + "=1) = " + o);
                result = true;
            } catch (Exception e) {
                log.error("evaluation ko : " + formula + ", reason : " + e.getMessage(), e);
            }
        }
        return result;
    }

    public static boolean validateObjectMaterialValidation(String relation, Object value) {
        boolean result = false;
        if (Strings.isNotEmpty(relation)) {
            ScriptEngine engine = getScriptEngine();
            Bindings bindings = engine.createBindings();
            bindings.put(FormulaHelper.VARIABLE_X, value);
            try {
                engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
                Boolean o = (Boolean) engine.eval(relation);
                log.debug(String.format("evaluation ok : %s (%s=%s) = %s", relation, FormulaHelper.VARIABLE_X, value, o));
                result = o != null && o;
            } catch (Exception e) {
                log.error("evaluation ko : " + relation + ", reason : " + e.getMessage(), e);
            }
        }
        return result;
    }

    public static boolean validateObjectMaterialValidationSyntax(String relation, Object value) {
        if (Strings.isNotEmpty(relation)) {
            ScriptEngine engine = getScriptEngine();
            Bindings bindings = engine.createBindings();
            bindings.put(FormulaHelper.VARIABLE_X, value);
            try {
                engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
                Boolean o = (Boolean) engine.eval(relation);
                log.debug(String.format("evaluation ok : %s (%s=%s) = %s", relation, FormulaHelper.VARIABLE_X, value, o));
                return true;
            } catch (Exception e) {
                log.error(String.format("evaluation ko : %s, reason : %s", relation, e.getMessage()), e);
                return false;
            }
        }
        return false;
    }

    public static Float computeValue(WithFormula withFormula, String formula, String coefficientName, String variableName, float data, Function<Float,Float> toRoundResult) {
        if (coefficientName != null) {
            Double b = withFormula.getCoefficientValue(coefficientName);
            if (b == 0) {
                // ce cas limite ne permet pas de calculer la taille a partir du weight
                return null;
            }
        }
        Float o = computeValue(withFormula, formula, variableName, data);
        if (o != null) {
            o = toRoundResult.apply(o);
        }
        return o;
    }

    private static Float computeValue(WithFormula withFormula, String formula, String variable, float data) {
        Map<String, Double> coefficientValues = withFormula.getCoefficientValues();
        ScriptEngine engine = getScriptEngine();
        Bindings bindings = engine.createBindings();
        addBindings(coefficientValues, bindings, variable, data);
        engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
        Number o = null;
        try {
            o = (Number) engine.eval("parseFloat(" + formula + ")");
        } catch (ScriptException e) {
            log.error("Could not compute value from " + formula, e);
        }
        return o == null ? null : o.floatValue();
    }

    private static void addBindings(Map<String, Double> coefficientValues, Bindings bindings, String variable, float data) {
        for (Map.Entry<String, Double> entry : coefficientValues.entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue();
            bindings.put(key, value);
            log.debug("add constant " + key + '=' + value);
        }
        bindings.put(variable, data);
    }

}
