package fr.ird.observe.dto.referential;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.WithStartEndDate;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 22/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public interface WithFormula extends WithStartEndDate, WithSpeciesFaoCode {

    String PROPERTY_OCEAN = "ocean";
    String PROPERTY_SPECIES = "species";
    String PROPERTY_SEX = "sex";
    String PROPERTY_START_DATE = "startDate";
    String PROPERTY_END_DATE = "endDate";
    String PROPERTY_COEFFICIENTS = "coefficients";
    String PROPERTY_SOURCE = "source";
    Comparator<WithFormula> FORMULA_SUPPORT_START_DATE_COMPARATOR = Comparator.comparing(WithStartEndDate::getStartDate, WithStartEndDate.START_DATE_COMPARATOR);
    Comparator<WithFormula> FORMULA_SUPPORT_END_DATE_COMPARATOR = Comparator.comparing(WithStartEndDate::getEndDate, WithStartEndDate.END_DATE_COMPARATOR);
    Comparator<WithFormula> FORMULA_SUPPORT_COMPARATOR = FORMULA_SUPPORT_START_DATE_COMPARATOR.thenComparing(FORMULA_SUPPORT_END_DATE_COMPARATOR);

    static <D extends WithFormula> void sort(List<D> list) {
        list.sort(FORMULA_SUPPORT_COMPARATOR);
    }

    String getCoefficients();

    void setCoefficients(String coefficients);

    Map<String, Double> getCoefficientValues();

    String getFormulaOneVariableName();

    String getFormulaTwoVariableName();

    void setStartDate(Date startDate);

    void setEndDate(Date endDate);

    String getSource();

    void setSource(String source);

    default Set<String> getCoefficientNames() {
        return getCoefficientValues().keySet();
    }

    String getFormulaOne();

    String getFormulaTwo();

    default void revalidateFormulaOne() {
        boolean result = FormulaHelper.validateRelation(this, getFormulaOne(), getFormulaOneVariableName());
        setFormulaOneValid(result);
    }

    boolean isFormulaOneValid();

    void setFormulaOneValid(boolean formulaOneValid);

    boolean isFormulaTwoValid();

    void setFormulaTwoValid(boolean formulaTwoValid);

    default void revalidateFormulaTwo() {
        boolean result = FormulaHelper.validateRelation(this, getFormulaTwo(), getFormulaTwoVariableName());
        setFormulaTwoValid(result);
    }

    default Double getCoefficientValue(String coefficientName) {
        return getCoefficientValues().get(coefficientName);
    }

    default Float computeFromFormulaOne(float data) {
        // only used for entities
        return null;
    }

    default Float computeFromFormulaTwo(float data) {
        // only used for entities
        return null;
    }

}
