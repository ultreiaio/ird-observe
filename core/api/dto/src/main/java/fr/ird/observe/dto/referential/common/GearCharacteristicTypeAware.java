package fr.ird.observe.dto.referential.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.DtoAndReferenceAware;
import fr.ird.observe.dto.ProtectedIdsCommon;
import fr.ird.observe.dto.reference.DtoReferenceAware;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4
 */
public interface GearCharacteristicTypeAware extends DtoReferenceAware, DtoAndReferenceAware {

    Logger log = LogManager.getLogger(GearCharacteristicTypeAware.class);

    Set<String> INTEGER_IDS = new LinkedHashSet<>(Arrays.asList(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_INTEGER_UNSIGNED, ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_INTEGER_SIGNED));
    Set<String> FLOAT_IDS = new LinkedHashSet<>(Arrays.asList(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_FLOAT_UNSIGNED, ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_FLOAT_SIGNED));
    Set<String> BOOLEAN_IDS = Collections.singleton(ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_BOOLEAN);

    default boolean isBoolean() {
        return BOOLEAN_IDS.contains(getTopiaId());
    }

    default boolean isInteger() {
        return INTEGER_IDS.contains(getTopiaId());
    }

    default boolean isFloat() {
        return FLOAT_IDS.contains(getTopiaId());
    }

    default boolean isDate() {
        return ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_DATE.equals(getTopiaId());
    }

    default boolean isList() {
        return ProtectedIdsCommon.COMMON_GEAR_CHARACTERISTIC_TYPE_LIST.equals(getTopiaId());
    }

    default Object getTypeValue(Object value, Map<String, GearCharacteristicListItemReference> gearCharacteristicListItemsById) {
        if (value != null && !value.toString().isEmpty()) {
            if (isBoolean()) {
                value = Boolean.valueOf(value.toString());
            } else if (isInteger()) {
                value = Float.valueOf(value.toString()).intValue();
            } else if (isFloat()) {
                value = Float.valueOf(value.toString());
            } else if (isDate()) {
                value = Date.from(Instant.ofEpochMilli(Long.parseLong(value.toString())));
            } else if (isList()) {
                value = gearCharacteristicListItemsById.get(value.toString());
            }
        }
        return value;
    }

    default Optional<Object> getOptionalTypeValue(Object value, Map<String, GearCharacteristicListItemReference> gearCharacteristicListItemsById) {
        try {
            return Optional.ofNullable(getTypeValue(value, gearCharacteristicListItemsById));
        } catch (Exception e) {
            log.error(String.format("Could not get editor value from: %s", value), e);
            return Optional.empty();
        }
    }
}
