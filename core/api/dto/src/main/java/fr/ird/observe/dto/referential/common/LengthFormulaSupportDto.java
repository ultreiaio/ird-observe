package fr.ird.observe.dto.referential.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.FormulaHelper;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Map;

@GenerateJavaBeanDefinition
public abstract class LengthFormulaSupportDto extends GeneratedLengthFormulaSupportDto {

    private Map<String, Double> coefficientsValues;
    public String getOceanLabel() {
        return ocean == null ? null : ocean.getLabel();
    }

    public String getSexLabel() {
        return sex == null ? null : sex.getLabel();
    }

    @Override
    public String getSpeciesFaoCode() {
        return getSpecies() == null ? null : getSpecies().getFaoCode();
    }

    @Override
    public SpeciesReference getSpeciesLabel() {
        return (SpeciesReference) super.getSpeciesLabel();
    }

    @Override
    public final void setCoefficients(String coefficients) {
        super.setCoefficients(coefficients);
        this.coefficientsValues = null;
        if (coefficients != null) {
            revalidateFormulaOne();
            revalidateFormulaTwo();
        }
    }

    @Override
    public final Map<String, Double> getCoefficientValues() {
        if (coefficientsValues == null) {
            coefficientsValues = FormulaHelper.getCoefficientValues(this);
        }
        return coefficientsValues;
    }

}
