package fr.ird.observe.dto.referential.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.FormulaHelper;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class LengthLengthParameterDto extends GeneratedLengthLengthParameterDto {

    @Override
    public String getFormulaOneVariableName() {
        return FormulaHelper.VARIABLE_INPUT;
    }

    @Override
    public String getFormulaTwoVariableName() {
        return FormulaHelper.VARIABLE_OUTPUT;
    }

    @Override
    public String getFormulaOne() {
        return getInputOutputFormula();
    }

    @Override
    public String getFormulaTwo() {
        return getOutputInputFormula();
    }

    @Override
    public void setInputOutputFormula(String inputOutputFormula) {
        super.setInputOutputFormula(inputOutputFormula);
        revalidateFormulaOne();
    }

    @Override
    public void setOutputInputFormula(String outputInputFormula) {
        super.setOutputInputFormula(outputInputFormula);
        revalidateFormulaTwo();
    }

    @Override
    public boolean isFormulaOneValid() {
        return isInputOutputFormulaValid();
    }

    @Override
    public void setFormulaOneValid(boolean formulaOneValid) {
        setInputOutputFormulaValid(formulaOneValid);
    }

    @Override
    public boolean isFormulaTwoValid() {
        return isOutputInputFormulaValid();
    }

    @Override
    public void setFormulaTwoValid(boolean formulaTwoValid) {
        setOutputInputFormulaValid(formulaTwoValid);
    }
}
