package fr.ird.observe.dto.referential.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/**
 * Created on 18/10/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.14
 */
@GenerateJavaBeanDefinition
public class LengthLengthParameterReference extends GeneratedLengthLengthParameterReference {

    @Override
    public String getSpeciesFaoCode() {
        return getSpecies() == null ? null : getSpecies().getFaoCode();
    }

    @Override
    public SpeciesReference getSpeciesLabel() {
        return (SpeciesReference) super.getSpeciesLabel();
    }
}
