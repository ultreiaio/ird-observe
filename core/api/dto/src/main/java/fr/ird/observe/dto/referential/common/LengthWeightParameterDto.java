package fr.ird.observe.dto.referential.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.FormulaHelper;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class LengthWeightParameterDto extends GeneratedLengthWeightParameterDto {

    @Override
    public void setLengthWeightFormula(String value) {
        super.setLengthWeightFormula(value);
        revalidateFormulaOne();
    }

    @Override
    public void setWeightLengthFormula(String value) {
        super.setWeightLengthFormula(value);
        revalidateFormulaTwo();
    }

    @Override
    public String getFormulaOne() {
        return getLengthWeightFormula();
    }

    @Override
    public String getFormulaTwo() {
        return getWeightLengthFormula();
    }

    @Override
    public String getFormulaOneVariableName() {
        return FormulaHelper.VARIABLE_LENGTH;
    }

    @Override
    public String getFormulaTwoVariableName() {
        return FormulaHelper.VARIABLE_WEIGHT;
    }

    @Override
    public boolean isFormulaOneValid() {
        return isLengthWeightFormulaValid();
    }

    @Override
    public void setFormulaOneValid(boolean formulaOneValid) {
        setLengthWeightFormulaValid(formulaOneValid);
    }

    @Override
    public boolean isFormulaTwoValid() {
        return isWeightLengthFormulaValid();
    }

    @Override
    public void setFormulaTwoValid(boolean formulaTwoValid) {
        setWeightLengthFormulaValid(formulaTwoValid);
    }
}
