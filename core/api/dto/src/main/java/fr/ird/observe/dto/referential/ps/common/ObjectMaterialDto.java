package fr.ird.observe.dto.referential.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.referential.FormulaHelper;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class ObjectMaterialDto extends GeneratedObjectMaterialDto {

    public boolean isBoolean() {
        return objectMaterialType != null && ProtectedIdsPs.PS_COMMON_OBJECT_MATERIAL_TYPE_BOOLEAN.equals(objectMaterialType.getId());
    }

    public boolean isText() {
        return objectMaterialType != null && ProtectedIdsPs.PS_COMMON_OBJECT_MATERIAL_TYPE_TEXT.equals(objectMaterialType.getId());
    }

    public boolean isInteger() {
        return objectMaterialType != null && ProtectedIdsPs.PS_COMMON_OBJECT_MATERIAL_TYPE_INTEGER.equals(objectMaterialType.getId());
    }

    public boolean isFloat() {
        return objectMaterialType != null && ProtectedIdsPs.PS_COMMON_OBJECT_MATERIAL_TYPE_FLOAT.equals(objectMaterialType.getId());
    }

    /**
     * @return {@code true} if code is compliant with his associated parent code, {@code false} otherwise.
     * @see #getParent()
     * @see #getCode()
     */
    public boolean isCodeCompliantWithParent() {
        ObjectMaterialReference parent = getParent();
        String code = getCode();
        if (parent == null) {
            // If no parent, then accept anything here
            return true;
        }
        if (code == null || code.trim().isEmpty()) {
            // If a parent is defined, code can not be null nor empty
            return false;
        }
        String parentCode = parent.getCode();
        if (parentCode == null || parentCode.trim().isEmpty()) {
            // if parent code is null or empty, then accept any code (even if we might only accept code without any «-» inside it)
            return true;
        }
        // In any other cases, code must starts with «parentCode-»
        return code.startsWith(parentCode + "-");
    }

    @Override
    public void setValidation(String validation) {
        super.setValidation(validation);
        Object value = "10";
        if (isBoolean()) {
            value = Boolean.TRUE;
        } else if (isInteger()) {
            value = 10;
        } else if (isFloat()) {
            value = 10f;
        }
        boolean result = FormulaHelper.validateObjectMaterialValidationSyntax(validation, value);
        setValidationValid(result);
    }

    public boolean withValidation() {
        return validation != null && !validation.isEmpty();
    }

    /**
     * @return {code true} if node contains no data to persist, {@code false} otherwise.
     */
    public boolean withData() {
        ObjectMaterialReference parent = getParent();
        return parent != null && parent.getParentId() != null;
    }

}
