package fr.ird.observe.dto.referential.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.dto.referential.FormulaHelper;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by tchemit on 29/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
public class ObjectMaterialHierarchyDto extends ObjectMaterialDto {

    private final List<ObjectMaterialHierarchyDto> children;

    public static class Builder {

        private final List<ObjectMaterialDto> dtoList;

        public Builder(Collection<ObjectMaterialDto> dtoList) {
            this.dtoList = new ArrayList<>(dtoList);

        }

        public ObjectMaterialHierarchyDto build() {
            dtoList.sort(Comparator.comparing(ObjectMaterialDto::getCode));
            return getObjectMaterialHierarchyList(dtoList).get(0);
        }


        public List<ObjectMaterialHierarchyDto> getObjectMaterialHierarchyList(List<ObjectMaterialDto> objectMaterials) {
            Multimap<String, ObjectMaterialDto> childrenByParent = ArrayListMultimap.create();
            objectMaterials.forEach(o -> childrenByParent.put(Optional.ofNullable(o.getParent()).map(ObjectMaterialReference::getId).orElse("Yo"), o));
            Collection<ObjectMaterialDto> topLevelMaterials = childrenByParent.get("Yo");
            List<ObjectMaterialHierarchyDto> result = new LinkedList<>();
            Set<String> idsDone = new TreeSet<>();
            for (ObjectMaterialDto topLevelMaterial : topLevelMaterials) {
                ObjectMaterialHierarchyDto hierarchyDto = fillHierarchyDto(childrenByParent, topLevelMaterial, idsDone);
                result.add(hierarchyDto);
            }
            result.sort(Comparator.comparing(ObjectMaterialDto::getCode));
            return result;
        }

        private ObjectMaterialHierarchyDto fillHierarchyDto(Multimap<String, ObjectMaterialDto> childrenByParent, ObjectMaterialDto topLevelMaterial, Set<String> idsDone) {
            Collection<ObjectMaterialDto> childrenEntities = childrenByParent.get(topLevelMaterial.getId());
            String code = topLevelMaterial.getCode();
            int level = code == null || code.isEmpty() ? 0 : code.split("-").length;
            Map<Integer, ObjectMaterialDto> orderMap = new TreeMap<>();
            for (ObjectMaterialDto child : childrenEntities) {
                String code1 = child.getCode().split("-")[level];
                orderMap.put(Integer.valueOf(code1), child);
            }
            List<ObjectMaterialDto> orderedChildren = new LinkedList<>();
            orderMap.forEach((k, v) -> orderedChildren.add(v));
            ObjectMaterialHierarchyDto hierarchyDto = new ObjectMaterialHierarchyDto(topLevelMaterial);
            boolean add = idsDone.add(hierarchyDto.getId());
            if (add) {
                for (ObjectMaterialDto child : orderedChildren) {
                    ObjectMaterialHierarchyDto hierarchyDto1 = fillHierarchyDto(childrenByParent, child, idsDone);
                    hierarchyDto.addChild(hierarchyDto1);
                }
            }
            return hierarchyDto;
        }
    }

    public static ObjectMaterialHierarchyDto build(Collection<ObjectMaterialDto> dtoList) {
        return new Builder(dtoList).build();
    }

    public ObjectMaterialHierarchyDto(ObjectMaterialDto root) {
        root.copy(this);
        this.children = new LinkedList<>();
    }

    public List<ObjectMaterialHierarchyDto> getChildren() {
        return children;
    }

    public void addChild(ObjectMaterialHierarchyDto hierarchyDto1) {
        children.add(hierarchyDto1);
    }

    public Set<ObjectMaterialHierarchyDto> getAllDto() {
        Set<ObjectMaterialHierarchyDto> result = new LinkedHashSet<>();
        getAllDto(this, result);
        return result;
    }

    public boolean isValid(Object value) {
        if (value == null) {
            return true;
        }
        if (isText()) {
            return FormulaHelper.validateObjectMaterialValidation(validation, value);
        } else if (isInteger()) {
            return FormulaHelper.validateObjectMaterialValidation(validation, Integer.valueOf(value.toString()));
        } else if (isFloat()) {
            return FormulaHelper.validateObjectMaterialValidation(validation, Float.valueOf(value.toString()));
        }
        throw new IllegalStateException("Can't validate this dto... " + this);
    }

    private void getAllDto(ObjectMaterialHierarchyDto hierarchyDto, Set<ObjectMaterialHierarchyDto> result) {
        result.add(hierarchyDto);
        hierarchyDto.getChildren().forEach(c -> getAllDto(c, result));
    }
}
