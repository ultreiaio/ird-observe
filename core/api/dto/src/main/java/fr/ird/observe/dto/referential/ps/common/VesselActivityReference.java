package fr.ird.observe.dto.referential.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.ToolkitId;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public class VesselActivityReference extends GeneratedVesselActivityReference {

    //FIXME No more used, find out why? 2021-11-02
    public static boolean isObservedSystemOperation(ToolkitId id) {
        return id != null && ProtectedIdsPs.PS_COMMON_VESSEL_ACTIVITY_FOR_OBSERVED_SYSTEM.contains(id.getId());
    }

    public static boolean isEndOfSearchingOperation(ToolkitId id) {
        return id != null && ProtectedIdsPs.PS_COMMON_VESSEL_ACTIVITY_ID_FOR_END_OF_SEARCHING.equals(id.getId());
    }

    public static boolean isCoordinateRequiredForVesselActivity(VesselActivityReference reference) {
        return reference == null || reference.isAllowSet() || !reference.isAllowFad();
    }
}
