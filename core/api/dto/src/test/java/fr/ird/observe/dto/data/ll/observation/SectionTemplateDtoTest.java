package fr.ird.observe.dto.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 23/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.2
 */
public class SectionTemplateDtoTest {

    @Test
    public void testIsFloatlineLengthsValid() {

        SectionTemplateDto sectionTemplate = new SectionTemplateDto();

        sectionTemplate.setFloatlineLengths("1.2");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2//");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/a");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/1/");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/1");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/1:");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/1:a");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/1:m");
        Assert.assertTrue(sectionTemplate.isFloatlineLengthsValid());
        sectionTemplate.setFloatlineLengths("1.2/1:km");
        Assert.assertTrue(sectionTemplate.isFloatlineLengthsValid());
        sectionTemplate.setFloatlineLengths("1.2/1:nm");
        Assert.assertTrue(sectionTemplate.isFloatlineLengthsValid());
        sectionTemplate.setFloatlineLengths("1.2/1:ftm");
        Assert.assertTrue(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/1.0");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/1.0:m");
        Assert.assertTrue(sectionTemplate.isFloatlineLengthsValid());
        sectionTemplate.setFloatlineLengths("1.2/1.0:km");
        Assert.assertTrue(sectionTemplate.isFloatlineLengthsValid());
        sectionTemplate.setFloatlineLengths("1.2/1.0:nm");
        Assert.assertTrue(sectionTemplate.isFloatlineLengthsValid());
        sectionTemplate.setFloatlineLengths("1.2/1.0:ftm");
        Assert.assertTrue(sectionTemplate.isFloatlineLengthsValid());
    }

    @Test
    public void testIsCompliantWithBasketCount() {
        {
            // with 2 values -> required 1 basket
            SectionTemplateDto sectionTemplate = new SectionTemplateDto();
            sectionTemplate.setFloatlineLengths("1/2:m");
            Assert.assertFalse(sectionTemplate.isCompliantWithBasketCount(0));
            Assert.assertTrue(sectionTemplate.isCompliantWithBasketCount(1));
            Assert.assertFalse(sectionTemplate.isCompliantWithBasketCount(2));
            Assert.assertFalse(sectionTemplate.isCompliantWithBasketCount(3));
        }
        {
            // with 3 values -> required 2 baskets
            SectionTemplateDto sectionTemplate = new SectionTemplateDto();
            sectionTemplate.setFloatlineLengths("1/2/3:km");
            Assert.assertFalse(sectionTemplate.isCompliantWithBasketCount(0));
            Assert.assertFalse(sectionTemplate.isCompliantWithBasketCount(1));
            Assert.assertTrue(sectionTemplate.isCompliantWithBasketCount(2));
            Assert.assertFalse(sectionTemplate.isCompliantWithBasketCount(3));
        }
        {
            // with 4 values -> required 3 baskets
            SectionTemplateDto sectionTemplate = new SectionTemplateDto();
            sectionTemplate.setFloatlineLengths("1/2/3/4:nm");
            Assert.assertFalse(sectionTemplate.isCompliantWithBasketCount(0));
            Assert.assertFalse(sectionTemplate.isCompliantWithBasketCount(1));
            Assert.assertFalse(sectionTemplate.isCompliantWithBasketCount(2));
            Assert.assertTrue(sectionTemplate.isCompliantWithBasketCount(3));
        }
    }

    @Test
    public void testApplyToBaskets() {
        {
            // with 1/2 -> (1,2) (m)
            SectionTemplateDto sectionTemplate = new SectionTemplateDto();
            sectionTemplate.setFloatlineLengths("1.1/2.2:m");
            List<BasketDto> baskets = new ArrayList<>();
            baskets.add(new BasketDto());
            sectionTemplate.applyToBaskets(baskets);
            assertBasketsFloatlinesValues(baskets, Pair.of(1.1f, 2.2f));
        }
        {
            // with 1/2 -> (1,2) (km)
            SectionTemplateDto sectionTemplate = new SectionTemplateDto();
            sectionTemplate.setFloatlineLengths("1.1/2.2:km");
            List<BasketDto> baskets = new ArrayList<>();
            baskets.add(new BasketDto());
            sectionTemplate.applyToBaskets(baskets);
            assertBasketsFloatlinesValues(baskets, Pair.of(1100f, 2200f));
        }
        {
            // with 1/2/3 -> (1,2), (2,3) (m)
            SectionTemplateDto sectionTemplate = new SectionTemplateDto();
            sectionTemplate.setFloatlineLengths("1.1/2.2/3.3:m");
            List<BasketDto> baskets = new ArrayList<>();
            baskets.add(new BasketDto());
            baskets.add(new BasketDto());
            sectionTemplate.applyToBaskets(baskets);
            assertBasketsFloatlinesValues(baskets, Pair.of(1.1f, 2.2f), Pair.of(2.2f, 3.3f));
        }
        {
            // with 1/2/3/4 -> (1,2), (2,3), (3,4) (m)
            SectionTemplateDto sectionTemplate = new SectionTemplateDto();
            sectionTemplate.setFloatlineLengths("1.1/2.2/3.3/4.4:m");
            List<BasketDto> baskets = new ArrayList<>();
            baskets.add(new BasketDto());
            baskets.add(new BasketDto());
            baskets.add(new BasketDto());
            sectionTemplate.applyToBaskets(baskets);
            assertBasketsFloatlinesValues(baskets, Pair.of(1.1f, 2.2f), Pair.of(2.2f, 3.3f), Pair.of(3.3f, 4.4f));
        }
    }

    @SafeVarargs
    protected final void assertBasketsFloatlinesValues(List<BasketDto> baskets, Pair<Float, Float>... expectedFloatlinesLengths) {
        for (int i = 0; i < expectedFloatlinesLengths.length; i++) {
            Pair<Float, Float> expectedValue = expectedFloatlinesLengths[i];
            BasketDto basket = baskets.get(i);
            Assert.assertEquals(expectedValue.getLeft(), basket.getFloatline1Length());
            Assert.assertEquals(expectedValue.getRight(), basket.getFloatline2Length());
        }
    }
}
