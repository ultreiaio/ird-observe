package fr.ird.observe.dto.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.ToolkitFixtures;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Map;

/**
 * Created on 10/06/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8
 */
public class FloatingObjectPresetStorageTest {

    private static final String STORE_CONTENT = "label1: label english\n" +
            "label2: label french\n" +
            "label3: label spanish\n" +
            "objectOperationId: fr.ird.ObjectOperation.1\n" +
            "supplyName: supply name 1\n" +
            "buoy1: \n" +
            "  code: code 1\n" +
            "  comment: brand 1\n" +
            "  transmittingBuoyOwnershipId: fr.ird.TransmittingBuoyOwnership.1\n" +
            "  transmittingBuoyTypeId: fr.ird.TransmittingBuoyType.1\n" +
            "  transmittingBuoyOperationId: fr.ird.TransmittingBuoyOperation.1\n" +
            "  vesselId: fr.ird.Vessel.1\n" +
            "  countryId: fr.ird.Country.1\n" +
            "buoy2: \n" +
            "  code: code 2\n" +
            "  comment: brand 2\n" +
            "  transmittingBuoyOwnershipId: fr.ird.TransmittingBuoyOwnership.2\n" +
            "  transmittingBuoyTypeId: fr.ird.TransmittingBuoyType.2\n" +
            "  transmittingBuoyOperationId: fr.ird.TransmittingBuoyOperation.2\n" +
            "  vesselId: fr.ird.Vessel.2\n" +
            "  countryId: fr.ird.Country.2\n" +
            "whenArrivingMaterials: !java.util.TreeMap\n" +
            "  fr.ird.ObjectMaterial.1: true\n" +
            "  fr.ird.ObjectMaterial.2: 1.2\n" +
            "whenLeavingMaterials: !java.util.TreeMap\n" +
            "  fr.ird.ObjectMaterial.2: false\n" +
            "  fr.ird.ObjectMaterial.4: 1.4\n";
    private Path testBasedir;

    @Before
    public void setUp() throws Exception {

        testBasedir = ToolkitFixtures.getTestBasedir(FloatingObjectPresetStorageTest.class).toPath();
    }

    @Test
    public void loadAll() throws IOException {
        Path directory = ToolkitFixtures.getBasedir().toPath()
                .resolve("src")
                .resolve("test")
                .resolve("resources")
                .resolve("fr")
                .resolve("ird")
                .resolve("observe")
                .resolve("dto")
                .resolve("data")
                .resolve("ps")
                .resolve("dcp");

        Assert.assertTrue(Files.exists(directory));

        Map<Path, FloatingObjectPreset> presets = FloatingObjectPresetStorage.loadAll(directory);
        Assert.assertNotNull(presets);
        Assert.assertEquals(2, presets.size());

        Iterator<Map.Entry<Path, FloatingObjectPreset>> iterator = presets.entrySet().iterator();
        {
            Map.Entry<Path, FloatingObjectPreset> presetEntry = iterator.next();
            assertPreset(presetEntry.getKey(), presetEntry.getValue(), "01_preset.yml", "supply name 1");

        }
        {
            Map.Entry<Path, FloatingObjectPreset> presetEntry = iterator.next();
            assertPreset(presetEntry.getKey(), presetEntry.getValue(), "02_preset.yml", "supply name 2");
        }
    }


    @Test
    public void load() throws IOException {

        Path file = ToolkitFixtures.getBasedir().toPath()
                .resolve("src")
                .resolve("test")
                .resolve("resources")
                .resolve("fr")
                .resolve("ird")
                .resolve("observe")
                .resolve("dto")
                .resolve("data")
                .resolve("ps")
                .resolve("dcp")
                .resolve("01_preset.yml");

        Assert.assertTrue(Files.exists(file));

        FloatingObjectPreset preset = FloatingObjectPresetStorage.load(file);

        Path target = testBasedir.resolve("01_load.yml");
        Assert.assertTrue(Files.notExists(target));
        FloatingObjectPresetStorage.store(preset, target);
        Assert.assertTrue(Files.exists(target));

        String actual = Files.readString(target, StandardCharsets.UTF_8);
        Assert.assertEquals(STORE_CONTENT, actual);

    }

    @Test
    public void store() throws IOException {

        FloatingObjectPreset preset = new FloatingObjectPreset();
        preset.setLabel1("label english");
        preset.setLabel2("label french");
        preset.setLabel3("label spanish");
        preset.setSupplyName("supply name 1");
        preset.setObjectOperationId("fr.ird.ObjectOperation.1");
        {
            FloatingObjectBuoyPreset buoyPreset = new FloatingObjectBuoyPreset();
            buoyPreset.setComment("brand 1");
            buoyPreset.setCode("code 1");
            buoyPreset.setCountryId("fr.ird.Country.1");
            buoyPreset.setTransmittingBuoyOwnershipId("fr.ird.TransmittingBuoyOwnership.1");
            buoyPreset.setTransmittingBuoyOperationId("fr.ird.TransmittingBuoyOperation.1");
            buoyPreset.setTransmittingBuoyTypeId("fr.ird.TransmittingBuoyType.1");
            buoyPreset.setVesselId("fr.ird.Vessel.1");
            preset.setBuoy1(buoyPreset);
        }
        {
            FloatingObjectBuoyPreset buoyPreset = new FloatingObjectBuoyPreset();
            buoyPreset.setComment("brand 2");
            buoyPreset.setCode("code 2");
            buoyPreset.setCountryId("fr.ird.Country.2");
            buoyPreset.setTransmittingBuoyOwnershipId("fr.ird.TransmittingBuoyOwnership.2");
            buoyPreset.setTransmittingBuoyOperationId("fr.ird.TransmittingBuoyOperation.2");
            buoyPreset.setTransmittingBuoyTypeId("fr.ird.TransmittingBuoyType.2");
            buoyPreset.setVesselId("fr.ird.Vessel.2");
            preset.setBuoy2(buoyPreset);
        }
        preset.setWhenArrivingMaterials(Map.of("fr.ird.ObjectMaterial.1", "true", "fr.ird.ObjectMaterial.2", "1.2"));
        preset.setWhenLeavingMaterials(Map.of("fr.ird.ObjectMaterial.2", "false", "fr.ird.ObjectMaterial.4", "1.4"));

        Path target = testBasedir.resolve("01_store.yml");
        Assert.assertTrue(Files.notExists(target));
        FloatingObjectPresetStorage.store(preset, target);
        Assert.assertTrue(Files.exists(target));

        String actual = Files.readString(target, StandardCharsets.UTF_8);

        Assert.assertEquals(STORE_CONTENT, actual);

    }

    @Test
    public void getNextFilename() throws IOException {
        Path directory = ToolkitFixtures.getBasedir().toPath()
                .resolve("src")
                .resolve("test")
                .resolve("resources")
                .resolve("fr")
                .resolve("ird")
                .resolve("observe")
                .resolve("dto")
                .resolve("data")
                .resolve("ps")
                .resolve("dcp");

        Assert.assertTrue(Files.exists(directory));
        Path nextFilename = FloatingObjectPresetStorage.getNextFilename(directory);
        Assert.assertEquals("03-preset.yml", nextFilename.getFileName().toString());
    }

    private void assertPreset(Path path, FloatingObjectPreset preset, String exceptedFilename, String name) {
        Assert.assertEquals(exceptedFilename, path.toFile().getName());
        Assert.assertEquals(name, preset.getSupplyName());
    }
}
