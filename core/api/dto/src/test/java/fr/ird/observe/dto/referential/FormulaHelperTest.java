package fr.ird.observe.dto.referential;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.CommonDto;
import io.ultreia.java4all.lang.Numbers;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.Map;

/**
 * Created on 05/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class FormulaHelperTest {

    @Test
    public void testComputeValue() {

        WithFormula formula = new WithFormula() {
            @Override
            public String getSpeciesFaoCode() {
                return null;
            }

            @Override
            public CommonDto getSpecies() {
                return null;
            }

            @Override
            public Date getStartDate() {
                return null;
            }

            @Override
            public Date getEndDate() {
                return null;
            }

            @Override
            public String getCoefficients() {
                return "a=3.8e-5:b=2.78 ";
            }

            @Override
            public void setCoefficients(String coefficients) {

            }

            @Override
            public Double getCoefficientValue(String coefficientName) {
                return getCoefficientValues().get(coefficientName);
            }

            @Override
            public Map<String, Double> getCoefficientValues() {
                return Map.of("a", 3.8e-5, "b", 2.78);
            }

            @Override
            public String getFormulaOneVariableName() {
                return null;
            }

            @Override
            public String getFormulaTwoVariableName() {
                return null;
            }

            @Override
            public void setStartDate(Date startDate) {

            }

            @Override
            public void setEndDate(Date endDate) {

            }

            @Override
            public String getSource() {
                return null;
            }

            @Override
            public void setSource(String source) {

            }

            @Override
            public String getFormulaOne() {
                return null;
            }

            @Override
            public String getFormulaTwo() {
                return null;
            }

            @Override
            public boolean isFormulaOneValid() {
                return false;
            }

            @Override
            public void setFormulaOneValid(boolean formulaOneValid) {

            }

            @Override
            public boolean isFormulaTwoValid() {
                return false;
            }

            @Override
            public void setFormulaTwoValid(boolean formulaTwoValid) {

            }
        };

        Float weight = FormulaHelper.computeValue(formula, "a * Math.pow(L, b)", null, "L", 84.0f, Numbers::roundThreeDigits);
        Assert.assertNotNull(weight);

        float excepted = (float) (Math.pow(84.0, 2.78) * 3.8e-5);
        Assert.assertEquals(excepted, weight, 3);
    }
}
