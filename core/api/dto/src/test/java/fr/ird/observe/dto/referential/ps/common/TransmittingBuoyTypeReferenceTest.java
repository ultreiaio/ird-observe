package fr.ird.observe.dto.referential.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 24/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class TransmittingBuoyTypeReferenceTest {

    @Test
    public void isCodeSyntaxValid() {
        TransmittingBuoyTypeReference reference = new TransmittingBuoyTypeReference();
        reference.setRegex("[0-9]{5,6}");

        Assert.assertTrue(reference.isCodeSyntaxValid(null));
        Assert.assertTrue(reference.isCodeSyntaxValid(""));
        Assert.assertFalse(reference.isCodeSyntaxValid("abc"));
        Assert.assertFalse(reference.isCodeSyntaxValid("a1"));
        Assert.assertFalse(reference.isCodeSyntaxValid("1!"));
        Assert.assertFalse(reference.isCodeSyntaxValid("1"));
        Assert.assertFalse(reference.isCodeSyntaxValid("12"));
        Assert.assertFalse(reference.isCodeSyntaxValid("123"));
        Assert.assertFalse(reference.isCodeSyntaxValid("1234"));
        Assert.assertTrue(reference.isCodeSyntaxValid("12345"));
        Assert.assertTrue(reference.isCodeSyntaxValid("123456"));
        Assert.assertFalse(reference.isCodeSyntaxValid("1234567"));
    }
}
