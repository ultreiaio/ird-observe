package fr.ird.observe.navigation.id;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.id.ps.common.TripNode;
import fr.ird.observe.navigation.id.ps.observation.ActivityNode;
import fr.ird.observe.navigation.id.ps.observation.RouteNode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by tchemit on 29/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class IdProjectManagerTest {

    private Project editModel;
    private Project selectModel;
    private IdProjectManager idProjectManager;
    private TripNode editTrip;
    private RouteNode editRouteObs;
    private ActivityNode editActivityObs;
    private TripNode selectTrip;
    private RouteNode selectRouteObs;
    private ActivityNode selectActivityObs;

    @Before
    public void setUp() {
        editModel = new Project();
        selectModel = new Project();
        idProjectManager = new IdProjectManager(editModel);
        editTrip = editModel.getPs().getCommonTrip();
        editRouteObs = editModel.getPs().getObservationRoute();
        editActivityObs = editModel.getPs().getObservationActivity();

        selectTrip = selectModel.getPs().getCommonTrip();
        selectRouteObs = selectModel.getPs().getObservationRoute();
        selectActivityObs = selectModel.getPs().getObservationActivity();
    }

    @Test
    public void testOpenSeine() throws CloseNodeVetoException {

        initEditModel("T1", "R1", null);
        initSelectModel("T1", null, null);

        assertSeineOpenRequest(idProjectManager.createOpenIdNodeRequest(editModel, editRouteObs, selectRouteObs, "R2"),
                               1, 1,
                               "T1", "R2", null);

        initEditModel("T1", "R2", null);
        initSelectModel("T2", null, null);

        assertSeineOpenRequest(idProjectManager.createOpenIdNodeRequest(editModel, editRouteObs, selectRouteObs, "R3"),
                               2, 2,
                               "T2", "R3", null);

        initEditModel("T1", "R2", "A3");
        initSelectModel(null, null, null);

        assertSeineOpenRequest(idProjectManager.createOpenIdNodeRequest(editModel, editTrip, selectTrip, "T2"),
                               3, 1,
                               "T2", null, null);

        initEditModel("T10", "R2", "A6");
        initSelectModel("T20", "r4", "A5");

        assertSeineOpenRequest(idProjectManager.createOpenIdNodeRequest(editModel, editActivityObs, selectActivityObs, "A5"),
                               3, 3,
                               "T20", "r4", "A5");

        initEditModel("T10", null, null);
        initSelectModel("T20", "r4", "A5");

        assertSeineOpenRequest(idProjectManager.createOpenIdNodeRequest(editModel, editActivityObs, selectActivityObs, "A5"),
                               1, 3,
                               "T20", "r4", "A5");

        initEditModel("T10", "R6", "A7");
        initSelectModel(null, null, null);

        assertSeineOpenRequest(idProjectManager.createOpenIdNodeRequest(editModel, editTrip, selectTrip, "T6"),
                               3, 1,
                               "T6", null, null);

        initEditModel("T10", null, null);
        initSelectModel("T10", "R4", null);

        assertSeineOpenRequest(idProjectManager.createOpenIdNodeRequest(editModel, editActivityObs, selectActivityObs, "A6"),
                               0, 2,
                               "T10", "R4", "A6");

        // change brothers

        initEditModel(null, null, null);
        initSelectModel(null, null, null);

        initEditModel("T10", null, null);
        initSelectModel(null, null, null);

        assertSeineOpenRequest(idProjectManager.createOpenIdNodeRequest(editModel, editTrip, selectTrip, "T11"),
                               1, 1,
                               "T11", null, null);

        initEditModel("T10", "R1", null);
        initSelectModel("T10", null, null);

        assertSeineOpenRequest(idProjectManager.createOpenIdNodeRequest(editModel, editRouteObs, selectRouteObs, "R2"),
                               1, 1,
                               "T10", "R2", null);

        initEditModel("T10", "R1", "A1");
        initSelectModel("T10", "R1", null);

        assertSeineOpenRequest(idProjectManager.createOpenIdNodeRequest(editModel, editActivityObs, selectActivityObs, "A2"),
                               1, 1,
                               "T10", "R1", "A2");
    }

    @Test
    public void testCloseSeine() throws CloseNodeVetoException {

        testCloseSeineRequest(editActivityObs,
                              1,
                              "T10", "R6");

        testCloseSeineRequest(editRouteObs,
                              2,
                              "T10", null);

        testCloseSeineRequest(editTrip,
                              3,
                              null, null);

    }

    private void initSelectModel(String selectTrip, String selectRoute, String selectActivity) {
        selectModel.clearModel();
        this.selectTrip.setId(selectTrip);
        this.selectRouteObs.setId(selectRoute);
        this.selectActivityObs.setId(selectActivity);

    }

    private void initEditModel(String editTrip, String editRoute, String editActivity) {
        editModel.clearModel();
        this.editTrip.setId(editTrip);
        this.editRouteObs.setId(editRoute);
        this.editActivityObs.setId(editActivity);
    }

    private void assertSeineOpenRequest(OpenNodeRequest request, int expectedNodesToCloses, int expectedNodesToOpen,
                                        String expectedTrip, String expectedRoute, String expectedActivity) throws CloseNodeVetoException {


        Assert.assertEquals("bad expectedNodesToCloses", expectedNodesToCloses, request.getNodesToClose().size());
        Assert.assertEquals("bad expectedNodesToOpen", expectedNodesToOpen, request.getNodesToOpen().size());

        idProjectManager.applyOpenIdNodeRequest(request);

        Assert.assertEquals(expectedTrip, editTrip.getId());
        Assert.assertEquals(expectedRoute, editRouteObs.getId());
        Assert.assertEquals(expectedActivity, editActivityObs.getId());
    }

    private void testCloseSeineRequest(IdNode<?> editNode, int expectedNodesToCloses, String expectedTrip, String expectedRoute) throws CloseNodeVetoException {

        initEditModel("T10", "R6", "A7");

        CloseNodeRequest request = idProjectManager.createCloseIdNodeRequest(editNode);

        Assert.assertEquals("bad expectedNodesToCloses", expectedNodesToCloses, request.getNodesToClose().size());

        idProjectManager.applyCloseIdNodeRequest(request);

        Assert.assertEquals("bad expectedTrip", expectedTrip, editTrip.getId());
        Assert.assertEquals("bad expectedRoute", expectedRoute, editRouteObs.getId());
        Assert.assertNull(editActivityObs.getId());
    }

}
