package fr.ird.observe.navigation.id;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 02/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0-RC-2
 */
public class RootNodeTest {

    @Test
    public void getNodes() {
        RootNode root = new RootNode();
        Assert.assertEquals(2, root.getChildren().size());
        Assert.assertNotNull(root.getPsTrip());
        Assert.assertNotNull(root.getLlTrip());
    }
}
