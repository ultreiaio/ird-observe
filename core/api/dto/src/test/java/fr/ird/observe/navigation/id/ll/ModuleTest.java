package fr.ird.observe.navigation.id.ll;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.id.ll.common.TripNode;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by tchemit on 26/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ModuleTest {

    private Module model;

    @Before
    public void setUp() {
        model = new Module();
    }

    @Test
    public void testInit() {
        Assert.assertNotNull(model);
        IdNode<?> root = model.getRoot();
        Assert.assertNotNull(root);
        Assert.assertEquals(TripNode.class, root.getClass());
        Assert.assertNotNull(model.getCommonTrip());
        Assert.assertNotNull(model.getObservationActivity());
        Assert.assertNotNull(model.getLogbookActivity());
        Assert.assertNotNull(model.getLandingLanding());
        Assert.assertNotNull(model.getLogbookSample());
    }

    @Test
    public void testGson() {
        model.getCommonTrip().setId("fr.ird.observe.entities.longline.TripLongline#yo.ya");
        Gson gson = new DtoGsonSupplier(true).get();
        String expected = "{\n" +
                "  \"type\": \"fr.ird.observe.navigation.id.ll.Module\",\n" +
                "  \"ids\": {\n" +
                "    \"fr.ird.observe.navigation.id.ll.common.TripNode\": \"fr.ird.observe.entities.longline.TripLongline#yo.ya\"\n" +
                "  }\n" +
                "}";
        String actual = gson.toJson(model);
        Assert.assertEquals(expected, actual);
        Module actualModel = gson.fromJson(actual, Module.class);
        Assert.assertNotNull(actualModel);
        Assert.assertEquals(model, actualModel);
    }

    @Test
    public void testEnabled() {
        Assert.assertFalse(model.isEnabled());
        model.getCommonTrip().setId("fr.ird.observe.entities.longline.TripLongline#yo.ya");
        Assert.assertTrue(model.isEnabled());
        model.getCommonTrip().setId(null);
        Assert.assertFalse(model.isEnabled());
    }

}
