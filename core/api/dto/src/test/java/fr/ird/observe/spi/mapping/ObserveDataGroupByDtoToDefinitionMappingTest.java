package fr.ird.observe.spi.mapping;

/*-
 * #%L
 * ObServe Core :: API :: Dto
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveApiFixtures;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created on 05/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveDataGroupByDtoToDefinitionMappingTest {

    @Test
    public void get() {
        ObserveApiFixtures.assertMapping(ObserveDataGroupByDtoToDefinitionMapping::get, "DataGroupByDtoToDefinitionMapping.count");
    }

    @Test
    public void getGroupByDefinitions() {
        ObserveBusinessProject businessProject = ObserveBusinessProject.get();
        ObserveDataGroupByDtoToDefinitionMapping mapping = ObserveDataGroupByDtoToDefinitionMapping.get();
        {
            List<DataGroupByDtoDefinition<fr.ird.observe.dto.data.ll.common.TripDto, ?>> definitions = mapping.getDefinitions(fr.ird.observe.dto.data.ll.common.TripDto.class);
            Assert.assertNotNull(definitions);
            Assert.assertFalse(definitions.isEmpty());

            List<DataGroupByDtoDefinition<?, ?>> definitions2 = mapping.getDefinitions(businessProject.getLlBusinessModule());
            Assert.assertNotNull(definitions2);
            Assert.assertFalse(definitions2.isEmpty());
            int expectedCount = ObserveApiFixtures.getIntegerProperty("DataGroupByDtoToDefinitionMapping.count.ll");
            ObserveApiFixtures.assertFixture("DataGroupByDtoToDefinitionMapping.count.ll", expectedCount, definitions2.size());
        }
        {
            List<DataGroupByDtoDefinition<fr.ird.observe.dto.data.ps.common.TripDto, ?>> definitions = mapping.getDefinitions(fr.ird.observe.dto.data.ps.common.TripDto.class);
            Assert.assertNotNull(definitions);
            Assert.assertFalse(definitions.isEmpty());
            List<DataGroupByDtoDefinition<?, ?>> definitions2 = mapping.getDefinitions(businessProject.getPsBusinessModule());
            Assert.assertNotNull(definitions2);
            Assert.assertFalse(definitions2.isEmpty());
            int expectedCount = ObserveApiFixtures.getIntegerProperty("DataGroupByDtoToDefinitionMapping.count.ps");
            ObserveApiFixtures.assertFixture("DataGroupByDtoToDefinitionMapping.count.ps", expectedCount, definitions2.size());
        }
    }

    @Test
    public void getGroupByDefinition() {
        ObserveDataGroupByDtoToDefinitionMapping mapping = ObserveDataGroupByDtoToDefinitionMapping.get();
        Assert.assertNotNull(mapping.getDefinition(fr.ird.observe.dto.data.ll.common.TripGroupByLogbookProgramDtoDefinition.NAME));
        Assert.assertNotNull(mapping.getDefinition(fr.ird.observe.dto.data.ll.common.TripGroupByObservationsProgramDtoDefinition.NAME));
        Assert.assertNotNull(mapping.getDefinition(fr.ird.observe.dto.data.ps.common.TripGroupByLogbookProgramDtoDefinition.NAME));
        Assert.assertNotNull(mapping.getDefinition(fr.ird.observe.dto.data.ps.common.TripGroupByObservationsProgramDtoDefinition.NAME));
        Assert.assertNull(mapping.getDefinition(fr.ird.observe.dto.data.ps.common.TripGroupByObservationsProgramDtoDefinition.NAME + System.nanoTime()));
    }
}
