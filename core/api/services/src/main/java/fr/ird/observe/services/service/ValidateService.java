package fr.ird.observe.services.service;

/*
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.validation.api.request.DataValidationRequest;
import fr.ird.observe.validation.api.request.ReferentialValidationRequest;
import fr.ird.observe.validation.api.result.ValidationResult;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
@Service
public interface ValidateService extends ObserveService {

    @Post
    @MethodCredential(Permission.READ_REFERENTIAL)
    ValidationResult validateReferential(ValidationRequestConfiguration configuration, ReferentialValidationRequest request);

    @MethodCredential(Permission.READ_ALL)
    @Post
    ValidationResult validateData(ValidationRequestConfiguration configuration, DataValidationRequest request);

}
