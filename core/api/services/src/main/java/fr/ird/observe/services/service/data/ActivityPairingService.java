package fr.ird.observe.services.service.data;

/*-
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.data.pairing.ApplyPairingRequest;
import fr.ird.observe.dto.data.pairing.TripPairingResultSupport;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.ObserveService;
import io.ultreia.java4all.http.spi.Internal;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Write;

/**
 * Created on 30/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
@Internal
public interface ActivityPairingService<T extends TripAware, I extends ObserveDto, R extends TripPairingResultSupport<T, I>> extends ObserveService {

    @MethodCredential(Permission.READ_ALL)
    @Post
    R computePairing(String tripId);

    @MethodCredential(Permission.READ_ALL_AND_WRITE_DATA)
    @Post
    @Write
    void applyPairing(ApplyPairingRequest activityMapping);
}
