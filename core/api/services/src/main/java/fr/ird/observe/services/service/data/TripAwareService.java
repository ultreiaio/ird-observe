package fr.ird.observe.services.service.data;

/*-
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.validation.TripValidationHelper;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.ObserveService;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Internal;
import io.ultreia.java4all.http.spi.Nullable;

import java.util.Date;
import java.util.Set;

/**
 * Created by tchemit on 06/01/2019.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Internal
public interface TripAwareService extends ObserveService, TripValidationHelper {

    @Get
    @MethodCredential(Permission.READ_DATA)
    TripMapDto getTripMap(TripMapConfigDto config);

    @Get
    @MethodCredential(Permission.READ_DATA)
    @Override
    int getMatchingTripsVesselWithinDateRange(String tripId, String vesselId, Date startDate, Date endDate);

    @Get
    @MethodCredential(Permission.READ_DATA)
    ReferentialDtoReferenceSet<SpeciesReference> getSpeciesByListAndTrip(@Nullable String tripId, String speciesListId);

    @Get
    @MethodCredential(Permission.READ_DATA)
    Set<String> getAllTripIds();

}
