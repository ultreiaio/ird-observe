package fr.ird.observe.services.service.data.ll;

/*-
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.data.ll.common.TripReference;
import fr.ird.observe.dto.data.ll.pairing.ActivityPairingDto;
import fr.ird.observe.dto.data.ll.pairing.ActivityPairingResult;
import fr.ird.observe.dto.data.ll.pairing.TripActivitiesPairingDto;
import fr.ird.observe.dto.data.ll.pairing.TripActivitiesPairingResult;
import fr.ird.observe.services.service.MethodCredential;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Service;

/**
 * Created by tchemit on 20/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Service
public interface ActivityPairingService extends fr.ird.observe.services.service.data.ActivityPairingService<TripReference, ActivityPairingResult, TripActivitiesPairingResult> {

    @Get
    @MethodCredential(Permission.READ_DATA)
    TripActivitiesPairingDto getTripActivitiesPairingDto(String tripId);

    @Get
    @MethodCredential(Permission.READ_DATA)
    ActivityPairingDto getActivityPairingDto(String tripId);
}
