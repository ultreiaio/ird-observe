package fr.ird.observe.services.service.data.ll.common;

/*-
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.ll.logbook.SetDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.data.MoveRequest;
import fr.ird.observe.services.service.data.TripAwareService;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.http.spi.Write;

import java.util.List;
import java.util.Set;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@Service
public interface TripService extends TripAwareService {

    @MethodCredential(Permission.WRITE_DATA)
    @Write
    @Post
    SaveResultDto saveAndCopyProperties(String activityId, String setToCopyId, SetDto dto) throws ConcurrentModificationException;

    @Get
    @MethodCredential(Permission.READ_DATA)
    Form<BranchlineDto> loadBranchlineForm(String branchlineId);

    @MethodCredential(Permission.WRITE_DATA)
    @Write
    @Post
    SaveResultDto saveBranchline(BranchlineDto dto) throws ConcurrentModificationException;

    /**
     * Get all activities candidates (excluding optional activity id) for a sample move.
     *
     * @param tripId     where to get activities
     * @param activityId optional activity id to exclude (used when moving a sample from activity to activity)
     * @return available activities for the sample move
     */
    @Get
    @MethodCredential(Permission.READ_DATA)
    List<ToolkitIdLabel> getSampleActivityParentCandidate(String tripId, @Nullable String activityId);

    @Get
    @MethodCredential(Permission.READ_DATA)
    List<String> getLogbookCatchSpeciesIds(String tripId);

    //FIXME Use a common MoveService
    @Write
    @Post
    @MethodCredential(Permission.WRITE_DATA)
    Set<String> moveActivitySample(MoveRequest request) throws ConcurrentModificationException;
}
