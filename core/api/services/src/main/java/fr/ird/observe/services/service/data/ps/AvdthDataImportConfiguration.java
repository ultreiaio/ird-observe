package fr.ird.observe.services.service.data.ps;

/*-
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.referential.ReferentialLocale;
import io.ultreia.java4all.util.json.JsonAware;

import java.nio.file.Path;
import java.util.Date;

/**
 * Created on 28/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class AvdthDataImportConfiguration implements JsonAware {
    /**
     * Timestamp to use to create new data.
     */
    private final Date now;
    /**
     * Id prefix to use to create new data.
     */
    private final long idTimestamp;
    /**
     * Program id where to attach imported trip.
     */
    private final String programId;
    /**
     * Ocean id where to attach imported trip.
     */
    private final String oceanId;
    /**
     * Referential locale used.
     */
    private final ReferentialLocale referentialLocale;
    /**
     * Where to write temporary sql exports.
     */
    private final Path scriptTemporaryDirectory;

    /**
     * Should we import data even if prepare step is not valid? (mostly used in tests).
     */
    private final boolean forceImport;
    /**
     * Should we load imported data in data source? (mostly used in tests).
     */
    private final boolean importInDatasource;
    /**
     * Location of file to import.
     */
    private final Path avdthFile;
    /**
     * Location of file to export.
     */
    private final Path exportFile;
    /**
     * Progression model to notify ui.
     */
    private final ProgressionModel progressionModel;

    public AvdthDataImportConfiguration(Date now,
                                        long idTimestamp,
                                        String programId,
                                        String oceanId,
                                        ReferentialLocale referentialLocale,
                                        Path scriptTemporaryDirectory,
                                        boolean forceImport,
                                        boolean importInDatasource,
                                        Path avdthFile,
                                        Path exportFile,
                                        ProgressionModel progressionModel) {
        this.now = now;
        this.idTimestamp = idTimestamp;
        this.programId = programId;
        this.oceanId = oceanId;
        this.referentialLocale = referentialLocale;
        this.scriptTemporaryDirectory = scriptTemporaryDirectory;
        this.forceImport = forceImport;
        this.importInDatasource = importInDatasource;
        this.avdthFile = avdthFile;
        this.exportFile = exportFile;
        this.progressionModel = progressionModel;
    }

    public ProgressionModel getProgressionModel() {
        return progressionModel;
    }

    public boolean isForceImport() {
        return forceImport;
    }

    public Path getAvdthFile() {
        return avdthFile;
    }

    public Path getExportFile() {
        return exportFile;
    }

    public boolean isImportInDatasource() {
        return importInDatasource;
    }

    public Path getScriptTemporaryDirectory() {
        return scriptTemporaryDirectory;
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public Date getNow() {
        return now;
    }

    public long getIdTimestamp() {
        return idTimestamp;
    }

    public String getProgramId() {
        return programId;
    }

    public String getOceanId() {
        return oceanId;
    }

    public AvdthReferentialImportConfiguration toReferentialImportConfiguration() {
        return new AvdthReferentialImportConfiguration(
                getNow(),
                getReferentialLocale(),
                getAvdthFile(),
                exportFile.getParent().resolve(exportFile.toFile().getName().replace("export", "export-referential")),
                getProgressionModel());
    }

    public void incrementsProgression(String message) {
        int value = progressionModel.getValue() + 1;
        progressionModel.setMessage(String.format("%5d/%5d - %s", value, progressionModel.getMaximum(), message));
    }
}
