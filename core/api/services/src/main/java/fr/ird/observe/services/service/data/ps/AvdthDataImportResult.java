package fr.ird.observe.services.service.data.ps;

/*-
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import io.ultreia.java4all.util.sql.SqlScript;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

/**
 * Created on 29/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("unused")
public class AvdthDataImportResult implements ObserveDto {
    private final Map<String, Integer> countResult;
    private final Map<String, Integer> readCountResult;
    private final Map<String, Integer> readResult;
    private final Map<String, Integer> notReadResult;
    private final Map<String, Integer> exportResult;

    /**
     * Import configuration.
     */
    private transient final AvdthDataImportConfiguration configuration;
    /**
     * Path of export path.
     */
    private transient final Path sqlResultPath;
    /**
     * Location of messages.
     */
    private transient final Path messageFile;
    /**
     * Message count.
     */
    private final int messageCount;
    /**
     * Content of export.
     */
    private SqlScript sqlResultContent;
    /**
     * Messages content.
     */
    private String messages;

    public AvdthDataImportResult(AvdthDataImportConfiguration configuration,
                                 Path sqlResultPath,
                                 Path messageFile,
                                 int messageCount,
                                 Map<String, Integer> countResult,
                                 Map<String, Integer> readCountResult,
                                 Map<String, Integer> readResult,
                                 Map<String, Integer> notReadResult,
                                 Map<String, Integer> exportResult) {
        this.messageFile = messageFile;
        this.messageCount = messageCount;
        this.countResult = countResult;
        this.readCountResult = readCountResult;
        this.readResult = readResult;
        this.configuration = configuration;
        this.notReadResult = notReadResult;
        this.exportResult = exportResult;
        this.sqlResultPath = sqlResultPath;
    }

    public Path getMessageFile() {
        return messageFile;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public Map<String, Integer> getReadCountResult() {
        return readCountResult;
    }

    public AvdthDataImportConfiguration getConfiguration() {
        return configuration;
    }

    public Map<String, Integer> getCountResult() {
        return countResult;
    }

    public Map<String, Integer> getReadResult() {
        return readResult;
    }

    public Map<String, Integer> getNotReadResult() {
        return notReadResult;
    }

    public SqlScript getSqlResultContent() {
        return sqlResultContent == null ? sqlResultContent = SqlScript.of(sqlResultPath) : sqlResultContent;
    }

    public Map<String, Integer> getExportResult() {
        return exportResult;
    }

    public Path getSqlResultPath() {
        return sqlResultPath;
    }

    public boolean isValid() {
        return notReadResult.isEmpty() && messageCount == 0;
    }

    public String getMessages() {
        if (messages == null) {
            try {
                messages = Files.readString(getMessageFile());
            } catch (IOException e) {
                throw new IllegalStateException("Can't read messages from " + messageFile, e);
            }
        }
        return messages;
    }
}
