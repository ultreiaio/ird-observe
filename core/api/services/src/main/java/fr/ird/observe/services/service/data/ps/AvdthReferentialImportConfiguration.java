package fr.ird.observe.services.service.data.ps;

/*-
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.referential.ReferentialLocale;

import java.nio.file.Path;
import java.util.Date;

/**
 * Created on 29/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class AvdthReferentialImportConfiguration implements ObserveDto {
    /**
     * Timestamp to use to create new data.
     */
    private final Date now;

    /**
     * Referential locale used.
     */
    private final ReferentialLocale referentialLocale;

    /**
     * Location of file to import.
     */
    private final Path avdthFile;
    /**
     * Location of file to export.
     */
    private final Path exportFile;
    /**
     * Progression model.
     */
    private final ProgressionModel progressionModel;

    public AvdthReferentialImportConfiguration(Date now, ReferentialLocale referentialLocale, Path avdthFile, Path exportFile, ProgressionModel progressionModel) {
        this.now = now;
        this.referentialLocale = referentialLocale;
        this.avdthFile = avdthFile;
        this.exportFile = exportFile;
        this.progressionModel = progressionModel;
    }

    public Path getAvdthFile() {
        return avdthFile;
    }

    public Path getExportFile() {
        return exportFile;
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public Date getNow() {
        return now;
    }

    public void incrementsProgression(String message) {
        int value = progressionModel.getValue() + 1;
        progressionModel.setMessage(String.format("%5d/%5d - %s", value, progressionModel.getMaximum(), message));
    }
}
