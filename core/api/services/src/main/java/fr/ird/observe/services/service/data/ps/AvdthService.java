package fr.ird.observe.services.service.data.ps;

/*-
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.ObserveService;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Service;

/**
 * Created on 21/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@Service
public interface AvdthService extends ObserveService {

    /**
     * Load avdth data from the given configuration, generate export sql script and optionally import them in the datasource.
     *
     * @param configuration configuration of import
     * @return result of import.
     */
    @Get
    @MethodCredential(Permission.READ_ALL_AND_WRITE_DATA)
    AvdthDataImportResult importData(AvdthDataImportConfiguration configuration) throws MissingReferentialException;

}
