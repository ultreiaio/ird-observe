package fr.ird.observe.services.service.data.ps;

/*
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.data.ps.common.TripConsolidateRequest;
import fr.ird.observe.consolidation.data.ps.common.TripConsolidateResult;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeManager;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeSpecializedRules;
import fr.ird.observe.consolidation.data.ps.localmarket.BatchConsolidateRequest;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.referential.common.DuplicateLengthLengthParameterException;
import fr.ird.observe.dto.referential.common.DuplicateLengthWeightParameterException;
import fr.ird.observe.dto.referential.common.LengthLengthParameterNotFoundException;
import fr.ird.observe.dto.referential.common.LengthWeightParameterNotFoundException;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.ObserveService;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.http.spi.Write;

/**
 * Created on 28/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Service
public interface ConsolidateDataService extends ObserveService {

    @Post(timeOut = 30)
    @Write
    @MethodCredential(Permission.WRITE_DATA)
    TripConsolidateResult consolidateTrip(SimplifiedObjectTypeSpecializedRules rules, TripConsolidateRequest request) throws DuplicateLengthLengthParameterException, DuplicateLengthWeightParameterException, LengthWeightParameterNotFoundException, LengthLengthParameterNotFoundException;

    @Post
    @MethodCredential(Permission.READ_DATA)
    ToolkitIdModifications consolidateLocalmarketBatch(BatchConsolidateRequest request) throws DuplicateLengthWeightParameterException, LengthWeightParameterNotFoundException;

    @Get
    @MethodCredential(Permission.READ_REFERENTIAL)
    SimplifiedObjectTypeManager newSimplifiedObjectTypeManager(SimplifiedObjectTypeSpecializedRules rules);

}
