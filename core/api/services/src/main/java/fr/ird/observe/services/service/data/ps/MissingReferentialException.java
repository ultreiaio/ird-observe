package fr.ird.observe.services.service.data.ps;

/*-
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.spi.json.ThrowableAdapterSupport;
import io.ultreia.java4all.util.json.JsonAdapter;

/**
 * When some referential are missing from AVDTH.
 * <p>
 * Created on 01/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class MissingReferentialException extends Exception {

    public MissingReferentialException(String message) {
        super(message);
    }

    @AutoService(JsonAdapter.class)
    public static class GsonAdapter extends ThrowableAdapterSupport<MissingReferentialException> {

        @Override
        public MissingReferentialException create(JsonObject json, String message, JsonDeserializationContext context) throws JsonParseException {
            return new MissingReferentialException(message);
        }

        @Override
        public Class<?> type() {
            return MissingReferentialException.class;
        }
    }
}
