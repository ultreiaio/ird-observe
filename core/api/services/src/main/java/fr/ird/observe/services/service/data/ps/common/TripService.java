package fr.ird.observe.services.service.data.ps.common;

/*-
 * #%L
 * ObServe Core :: API :: Services
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.data.ps.common.TripLocalmarketDto;
import fr.ird.observe.dto.data.ps.common.TripLogbookDto;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.data.TripAwareService;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;

import java.util.List;
import java.util.Set;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@Service
public interface TripService extends TripAwareService {

    @Get
    @MethodCredential(Permission.READ_DATA)
    Form<TripLogbookDto> loadLogbookForm(String tripId);

    @Get
    @MethodCredential(Permission.READ_DATA)
    Form<TripLocalmarketDto> loadLocalmarketForm(String tripId);

    @Get
    @MethodCredential(Permission.READ_DATA)
    List<ActivityStubDto> getLogbookWellPlanActivities(String tripId);

    /**
     * @param tripId selected trip id
     * @return the set of {@code Well#well} used in the well plan of selected trip
     */
    @Get
    @MethodCredential(Permission.READ_DATA)
    Set<String> getLogbookWellIdsFromWellPlan(String tripId);

    /**
     * @param tripId selected trip id
     * @param wellId selected well id
     * @return the set of activity ids found in well plan for selected trip and well
     */
    @Post
    @MethodCredential(Permission.READ_DATA)
    Set<String> createLogbookSampleActivityFromWellPlan(String tripId, String wellId);

    /**
     * @param tripId selected trip id
     * @return the set of {@code Well#well} used in the sample of the selected trip
     */
    @Get
    @MethodCredential(Permission.READ_DATA)
    Set<String> getLogbookWellIdsFromSample(String tripId);

    /**
     * @param tripId selected trip id
     * @param wellId selected well id
     * @return the set of activity ids found in sample for selected trip and well
     */
    @Post
    @MethodCredential(Permission.READ_DATA)
    Set<String> createLogbookWellActivityFromSample(String tripId, String wellId);

    @Get
    @MethodCredential(Permission.READ_DATA)
    boolean isActivityEndOfSearchFound(String routeId);

    @Get
    @MethodCredential(Permission.READ_DATA)
    boolean isActivitiesAcquisitionModeByTimeEnabled(String tripId);

    @Get
    @MethodCredential(Permission.WRITE_DATA)
    Form<fr.ird.observe.dto.data.ps.observation.FloatingObjectDto> preCreateObservationFloatingObject(String activityId, @Nullable FloatingObjectPreset floatingObjectPreset);

    @Get
    @MethodCredential(Permission.WRITE_DATA)
    Form<fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto> preCreateLogbookFloatingObject(String activityId, @Nullable FloatingObjectPreset floatingObjectPreset);
}
