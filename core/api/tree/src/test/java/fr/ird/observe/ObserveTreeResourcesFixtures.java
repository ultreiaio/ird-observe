package fr.ird.observe;

/*-
 * #%L
 * ObServe Core :: API :: Tree
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.VerticalNavigationHelper;
import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModel;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.spi.navigation.tree.ToolkitTreeNodeBuilder;
import fr.ird.observe.test.ObserveFixtures;
import org.junit.Assert;

import java.nio.file.Path;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 28/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.2.0
 */
public class ObserveTreeResourcesFixtures extends ObserveFixtures {

    protected static final Map<String, String> STORE = loadFixturesMap("api-tree");

    public static void assertLoadPath(Supplier<ToolkitTreeNode> rootNode, String path, int expectedLevel) {
        ToolkitTreeNode actual = new ToolkitTreeNodeBuilder().load(rootNode.get(), path == null ? null : Path.of(path));
        Assert.assertNotNull(actual);
        Assert.assertEquals(expectedLevel, actual.getLevel());
    }

    public static Map<String, String> getProperties() {
        return STORE;
    }

    public static void assertFixture(String fixtureName, Object expected, Object actual) {
        Assert.assertEquals("Could not find fixtures " + fixtureName, expected, actual);
    }

    public static String getProperty(String propertyName) {
        return getProperties().get(propertyName);
    }

    public static int getIntegerProperty(String propertyName) {
        String property = Objects.requireNonNull(getProperty(propertyName), String.format("%s Can not find fixture: %s", ObserveTreeResourcesFixtures.class.getName(), propertyName));
        return Integer.parseInt(property);
    }

    public static <N extends ToolkitTreeNode> N assertModel(Supplier<N> rootNodeSupplier, Gson gson, Map<String, ToolkitTreeNodeStates> mapping, int exceptedRootCount) {
        N rootNode = rootNodeSupplier.get();
        new ToolkitTreeNodeBuilder().load(rootNode, mapping);
        if (!ObserveFixtures.WITH_ASSERT) {
            ToolkitTreeFlatModel result = new ToolkitTreeFlatModel(mapping, exceptedRootCount);
            System.out.println(gson.toJson(result));
        }
        Assert.assertEquals(exceptedRootCount, rootNode.getChildCount());
        for (String path : mapping.keySet()) {
            ToolkitTreeNode node = VerticalNavigationHelper.findPath0(rootNode, path);
            Assert.assertNotNull(String.format("Can't find node %s", path), node);
            Assert.assertEquals(path, node.getNodePath().toString());
            Map<String, Object> states = mapping.get(path).getStates();
            if (states != null) {
                ToolkitTreeNodeBean bean = node.getUserObject();
                for (String state : states.keySet()) {
                    Assert.assertEquals(states.get(state), bean.getState(state));
                }
            }
        }
        return rootNode;
    }
}
