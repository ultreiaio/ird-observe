package fr.ird.observe.navigation.tree.navigation;

/*-
 * #%L
 * ObServe Core :: API :: Tree
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.dto.data.ll.common.TripGroupByObservationsProgramDto;
import fr.ird.observe.dto.data.ll.common.TripReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.referential.ll.common.ProgramDto;
import fr.ird.observe.navigation.tree.navigation.bean.RootOpenFilterNavigationTreeNodeBean;
import fr.ird.observe.navigation.tree.navigation.bean.RootOpenNavigationTreeNodeBean;
import fr.ird.observe.navigation.tree.navigation.data.ll.common.GearUseFeaturesNavigationTreeNode;
import fr.ird.observe.navigation.tree.navigation.data.ll.common.TripGroupByObservationsProgramNavigationTreeNode;
import fr.ird.observe.navigation.tree.navigation.data.ll.common.TripNavigationTreeNode;
import fr.ird.observe.navigation.tree.navigation.referential.common.CountryNavigationTreeNode;
import fr.ird.observe.navigation.tree.navigation.referential.common.ReferentialPackageNavigationTreeNode;
import org.junit.Assert;
import org.junit.Test;

import java.util.function.Function;

/**
 * Created on 06/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0-RC-2
 */
public class RootNavigationTreeNodeTest {

    public static void setData(RootOpenNavigationTreeNodeBean bean, DtoReference data) {
        RootOpenNavigationTreeNodeBean.STATE_ID.setValue(bean, data == null ? null : data.getId());
        RootOpenNavigationTreeNodeBean.STATE_TEXT.setValue(bean, data == null ? null : data.toString());
    }

    public static void setData(RootOpenFilterNavigationTreeNodeBean bean, DataGroupByDto<?> data) {
        RootOpenFilterNavigationTreeNodeBean.STATE_ID.setValue(bean, data == null ? null : data.getId());
        RootOpenFilterNavigationTreeNodeBean.STATE_TEXT.setValue(bean, data == null ? null : data.toString());
    }

    @Test
    public void create() {
        RootNavigationTreeNode root = new RootNavigationTreeNode();
        Assert.assertNotNull(root);
        ReferentialPackageNavigationTreeNode commonCommon = root.getReferentialCommon();
        Assert.assertNull(commonCommon);
        commonCommon = root.findPath("referentialCommon");
        Assert.assertNull(commonCommon);
        root.add(new ReferentialPackageNavigationTreeNode());
        commonCommon = root.getReferentialCommon();
        Assert.assertNotNull(commonCommon);
        commonCommon = root.findPath("referentialCommon");
        Assert.assertNotNull(commonCommon);

        Assert.assertNull(root.findPath("referentialCommon/country"));
        Assert.assertNull(commonCommon.findPath("country"));
        Assert.assertNull(commonCommon.getCountry());
        commonCommon.add(new CountryNavigationTreeNode());
        Assert.assertNotNull(root.findPath("referentialCommon/country"));
        Assert.assertNotNull(commonCommon.findPath("country"));
        Assert.assertNotNull(commonCommon.getCountry());

        ToolkitIdDtoBean programId = ToolkitIdDtoBean.of(ProgramDto.class, "program");
        ToolkitIdDtoBean tripId = ToolkitIdDtoBean.of(TripDto.class, "trip");

        assertNodeNull(root, root, p -> p.getDataLlCommonTripGroupByObservationsProgram(programId), "dataLlCommonTripGroupByObservationsProgram~program");

        TripGroupByObservationsProgramNavigationTreeNode newProgram = new TripGroupByObservationsProgramNavigationTreeNode();
        root.add(newProgram);
        // still null since newChild has no id
        assertNodeNull(root, root, p -> p.getDataLlCommonTripGroupByObservationsProgram(programId), "dataLlCommonTripGroupByObservationsProgram~program");

        TripGroupByObservationsProgramDto programReference = new TripGroupByObservationsProgramDto();
        programReference.setFilterValue(programId.getId());

        setData(newProgram.getUserObject(), programReference);
        TripGroupByObservationsProgramNavigationTreeNode programLl = assertNodeNotNull(root, root, p -> p.getDataLlCommonTripGroupByObservationsProgram(programId), "dataLlCommonTripGroupByObservationsProgram~program");

        assertNodeNull(root, programLl, p -> p.getChildren(tripId), "dataLlCommonTripGroupByObservationsProgram~program/trip");
        Assert.assertNull(programLl.findPath("trip"));

        TripNavigationTreeNode newTrip = new TripNavigationTreeNode();
        programLl.add(newTrip);

        assertNodeNull(root, programLl, p -> p.getChildren(tripId), "dataLlCommonTripGroupByObservationsProgram~program/trip");
        Assert.assertNull(programLl.findPath("trip"));

        root.add(newTrip);
        assertNodeNull(root, root, p -> p.findChildById(tripId.getId()), "dataLlCommonTrip~trip");
        Assert.assertNull(root.findPath("dataLlCommonTrip~trip"));

        TripReference tripReference = new TripReference();
        tripReference.setId(tripId.getId());
        setData(newTrip.getUserObject(), tripReference);

        assertNodeNotNull(root, root, p -> p.findChildByType(TripNavigationTreeNode.class, tripId.getId()), "dataLlCommonTrip~trip");
        Assert.assertNotNull(root.findPath("dataLlCommonTrip~trip"));

        programLl.add(newTrip);
        TripNavigationTreeNode tripNode = assertNodeNotNull(root, programLl, p -> p.getChildren(tripId), "dataLlCommonTripGroupByObservationsProgram~program/trip");
        Assert.assertNotNull(programLl.findPath("trip"));

        assertNodeNull(root, tripNode, TripNavigationTreeNode::getGearUseFeatures, "dataLlCommonTripGroupByObservationsProgram~program/trip/gearUseFeatures");
        Assert.assertNull(tripNode.findPath("gearUseFeatures"));

        tripNode.add(new GearUseFeaturesNavigationTreeNode());
        assertNodeNotNull(root, tripNode, TripNavigationTreeNode::getGearUseFeatures, "dataLlCommonTripGroupByObservationsProgram~program/trip/gearUseFeatures");
        Assert.assertNotNull(tripNode.findPath("gearUseFeatures"));
        Assert.assertNotNull(programLl.findPath("trip/gearUseFeatures"));

        Assert.assertNull(root.findPath("dataLlCommonTrip~trip/gearUseFeatures"));
        root.add(newTrip);
        Assert.assertNotNull(root.findPath("dataLlCommonTrip~trip/gearUseFeatures"));
    }

    protected <N extends NavigationTreeNode> void assertNodeNull(NavigationTreeNode root, N parent, Function<N, NavigationTreeNode> toChildNode, String rootPath) {
        NavigationTreeNode tripNode = toChildNode.apply(parent);
        Assert.assertNull(tripNode);
        tripNode = root.findPath(rootPath);
        Assert.assertNull(tripNode);
    }

    protected <N extends NavigationTreeNode, M extends NavigationTreeNode> M assertNodeNotNull(NavigationTreeNode root, N parent, Function<N, M> toChildNode, String rootPath) {
        M tripNode = toChildNode.apply(parent);
        Assert.assertNotNull(tripNode);
        tripNode = root.findPath(rootPath);
        Assert.assertNotNull(tripNode);
        return tripNode;
    }
}
