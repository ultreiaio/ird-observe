package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * ObServe Core :: API :: Tree
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.ll.common.TripGroupByObservationsProgramDto;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.referential.ps.common.ProgramDto;
import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModel;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.navigation.tree.selection.bean.RootOpenFilterSelectionTreeNodeBean;
import fr.ird.observe.navigation.tree.selection.data.ll.common.TripGroupByObservationsProgramSelectionTreeNode;
import fr.ird.observe.navigation.tree.selection.data.ps.common.TripSelectionTreeNode;
import fr.ird.observe.navigation.tree.selection.referential.common.ReferentialPackageSelectionTreeNode;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import fr.ird.observe.spi.navigation.tree.ToolkitTreeNodeBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 06/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0-RC-2
 */
public class RootSelectionTreeNodeTest {

    public static void setData(RootOpenFilterSelectionTreeNodeBean bean, DataGroupByDto<?> data) {
        RootOpenFilterSelectionTreeNodeBean.STATE_ID.setValue(bean, data == null ? null : data.getId());
        RootOpenFilterSelectionTreeNodeBean.STATE_TEXT.setValue(bean, data == null ? null : data.toString());
    }

    @Test
    public void create() {
        RootSelectionTreeNode root = new RootSelectionTreeNode();
        Assert.assertNotNull(root);
        ReferentialPackageSelectionTreeNode commonCommon = root.getReferentialCommon();
        Assert.assertNull(commonCommon);
        root.add(new ReferentialPackageSelectionTreeNode());
        commonCommon = root.getReferentialCommon();
        Assert.assertNotNull(commonCommon);

        ToolkitIdDtoBean programId = ToolkitIdDtoBean.of(ProgramDto.class, "yo");
        TripGroupByObservationsProgramSelectionTreeNode programLl = root.getDataLlCommonTripGroupByObservationsProgram(programId);
        Assert.assertNull(programLl);
        TripGroupByObservationsProgramSelectionTreeNode newChild = new TripGroupByObservationsProgramSelectionTreeNode();
        root.add(newChild);
        programLl = root.getDataLlCommonTripGroupByObservationsProgram(programId);
        // still null since newChild has no id
        Assert.assertNull(programLl);

        TripGroupByObservationsProgramDto programReference = new TripGroupByObservationsProgramDto();
        programReference.setId(programId.getId());
        programReference.setFilterValue(programId.getId());
        setData(newChild.getUserObject(), programReference);
        programLl = root.getDataLlCommonTripGroupByObservationsProgram(programId);
        Assert.assertNotNull(programLl);

    }

    @Test
    public void testSelect() throws IOException {
        Gson gson = new DtoGsonSupplier(true).get();
        URL resource = getClass().getClassLoader().getResource("navigationTree.json");
        RootSelectionTreeNode node;
        try (InputStreamReader stream = new InputStreamReader(Objects.requireNonNull(resource).openStream(), StandardCharsets.UTF_8)) {
            ToolkitTreeFlatModel result = gson.fromJson(stream, ToolkitTreeFlatModel.class);
            Map<String, ToolkitTreeNodeStates> mapping = result.getMapping();
            node = new ToolkitTreeNodeBuilder().load(new RootSelectionTreeNode(), mapping);
        }
        Assert.assertNotNull(node);
        fr.ird.observe.navigation.tree.selection.data.ps.common.TripGroupByObservationsProgramSelectionTreeNode psProgramNode = node.getDataPsCommonTripGroupByObservationsProgram(ToolkitIdDtoBean.of(ProgramDto.class, "fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444"));
        Assert.assertNotNull(psProgramNode);
        TripSelectionTreeNode psTripNode = psProgramNode.getChildren(ToolkitIdDtoBean.of(TripDto.class, "fr.ird.data.ps.common.Trip#1343641056380#0.32424601977176837"));
        Assert.assertNotNull(psTripNode);
        Assert.assertTrue(node.getUserObject().isNotSelected());
        Assert.assertTrue(psProgramNode.getUserObject().isNotSelected());
        Assert.assertTrue(psTripNode.getUserObject().isNotSelected());
        psTripNode.setSelected(true);
        Assert.assertTrue(psTripNode.getUserObject().isSelected());
        Assert.assertTrue(psProgramNode.getUserObject().isPartialSelected());
        Assert.assertTrue(node.getUserObject().isNotSelected());
        psTripNode.setSelected(false);
        Assert.assertTrue(node.getUserObject().isNotSelected());
        Assert.assertTrue(psProgramNode.getUserObject().isNotSelected());
        Assert.assertTrue(psTripNode.getUserObject().isNotSelected());
        psProgramNode.setSelected(true);
        Assert.assertTrue(psTripNode.getUserObject().isSelected());
        Assert.assertTrue(psProgramNode.getUserObject().isSelected());
        psProgramNode.setSelected(false);
        Assert.assertTrue(node.getUserObject().isNotSelected());
        Assert.assertTrue(psProgramNode.getUserObject().isNotSelected());

    }
}
