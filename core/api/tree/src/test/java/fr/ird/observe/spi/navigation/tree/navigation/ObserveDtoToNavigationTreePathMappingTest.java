package fr.ird.observe.spi.navigation.tree.navigation;

/*-
 * #%L
 * ObServe Core :: API :: Tree
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveTreeResourcesFixtures;
import org.junit.Test;

/**
 * Created on 08/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveDtoToNavigationTreePathMappingTest {

    @Test
    public void size() {
        ObserveDtoToNavigationTreePathMapping mapping = ObserveDtoToNavigationTreePathMapping.get();
        if (!ObserveTreeResourcesFixtures.WITH_ASSERT) {
            String print = mapping.print();
            System.out.println(print);
        }
        ObserveTreeResourcesFixtures.assertFixture("NavigationMapping.count", ObserveTreeResourcesFixtures.getIntegerProperty("NavigationMapping.count"), mapping.size());
    }
}
