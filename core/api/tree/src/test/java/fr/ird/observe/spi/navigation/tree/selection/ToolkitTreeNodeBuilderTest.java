package fr.ird.observe.spi.navigation.tree.selection;

/*-
 * #%L
 * ObServe Core :: API :: Tree
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.ObserveTreeResourcesFixtures;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.referential.ps.common.ProgramDto;
import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModel;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.navigation.tree.selection.RootSelectionTreeNode;
import fr.ird.observe.navigation.tree.selection.data.ps.common.TripGroupByObservationsProgramSelectionTreeNode;
import fr.ird.observe.navigation.tree.selection.data.ps.common.TripSelectionTreeNode;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 06/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0-RC-2
 */
public class ToolkitTreeNodeBuilderTest {

    public static final String content = "{\n" +
            "  \"mapping\": {\n" +
            "    \"dataPsCommonTripGroupByObservationsProgram~program1\": {\n" +
            "      \"count\": 1\n" +
            "    },\n" +
            "    \"dataPsCommonTripGroupByObservationsProgram~program1/trip1\": {\n" +
            "    },\n" +
            "    \"dataPsCommonTripGroupByObservationsProgram~program2\": {\n" +
            "      \"count\": 2\n" +
            "    },\n" +
            "    \"dataPsCommonTripGroupByObservationsProgram~program2/trip2\": {\n" +
            "    },\n" +
            "    \"referentialCommon\": {},\n" +
            "    \"referentialCommon/country\": {\n" +
            "      \"count\": 1\n" +
            "    },\n" +
            "    \"referentialPsCommon\": {},\n" +
            "    \"referentialPsCommon/program\": {\n" +
            "      \"count\": 2\n" +
            "    },\n" +
            "    \"referentialPsCommon/schoolType\": {\n" +
            "      \"count\": 3\n" +
            "    }\n" +
            "  }\n" +
            "}";

    @Test
    public void create() {
        Gson gson = new DtoGsonSupplier(true).get();

        Map<String, ToolkitTreeNodeStates> mapping = new LinkedHashMap<>();
        mapping.put("dataPsCommonTripGroupByObservationsProgram~program1", ToolkitTreeNodeStates.of("count", 1));
        mapping.put("dataPsCommonTripGroupByObservationsProgram~program1/trip1", ToolkitTreeNodeStates.of());
        mapping.put("dataPsCommonTripGroupByObservationsProgram~program2", ToolkitTreeNodeStates.of("count", 2));
        mapping.put("dataPsCommonTripGroupByObservationsProgram~program2/trip2", ToolkitTreeNodeStates.of());
        mapping.put("referentialCommon", ToolkitTreeNodeStates.of());
        mapping.put("referentialCommon/country", ToolkitTreeNodeStates.of("count", 1));
        mapping.put("referentialPsCommon", ToolkitTreeNodeStates.of());
        mapping.put("referentialPsCommon/program", ToolkitTreeNodeStates.of("count", 2));
        mapping.put("referentialPsCommon/schoolType", ToolkitTreeNodeStates.of("count", 3));
        assertModel(gson, mapping);
    }

    @Test
    public void fromResult() {
        Gson gson = new DtoGsonSupplier(true).get();
        ToolkitTreeFlatModel result = gson.fromJson(content, ToolkitTreeFlatModel.class);
        Map<String, ToolkitTreeNodeStates> mapping = result.getMapping();
        assertModel(gson, mapping);
    }

    @Test
    public void fromPath() {
        ObserveTreeResourcesFixtures.assertLoadPath(RootSelectionTreeNode::new, null, 0);
        ObserveTreeResourcesFixtures.assertLoadPath(RootSelectionTreeNode::new, "dataPsCommonTripGroupByObservationsProgram~program1", 1);
        ObserveTreeResourcesFixtures.assertLoadPath(RootSelectionTreeNode::new, "dataPsCommonTripGroupByObservationsProgram~program1/trip2", 2);
    }

    private void assertModel(Gson gson, Map<String, ToolkitTreeNodeStates> mapping) {

        RootSelectionTreeNode rootNode = ObserveTreeResourcesFixtures.assertModel(RootSelectionTreeNode::new, gson, mapping, 4);

        Assert.assertNotNull(rootNode.getReferentialCommon());
        Assert.assertNotNull(rootNode.getReferentialCommon().getCountry());
        Assert.assertEquals(1, rootNode.getReferentialCommon().getCountry().getUserObject().getCount());
        Assert.assertNotNull(rootNode.getReferentialPsCommon().getProgram());
        Assert.assertEquals(2, rootNode.getReferentialPsCommon().getProgram().getUserObject().getCount());
        Assert.assertNull(rootNode.getReferentialCommon().getVessel());
        Assert.assertNull(rootNode.getReferentialLlCommon());
        Assert.assertNotNull(rootNode.getReferentialPsCommon());
        Assert.assertNotNull(rootNode.getReferentialPsCommon().getSchoolType());
        Assert.assertEquals(3, rootNode.getReferentialPsCommon().getSchoolType().getUserObject().getCount());

        TripGroupByObservationsProgramSelectionTreeNode program1 = rootNode.getDataPsCommonTripGroupByObservationsProgram(ToolkitIdDtoBean.of(ProgramDto.class, "program1"));
        Assert.assertNotNull(program1);
        TripSelectionTreeNode trip1 = program1.getChildren(ToolkitIdDtoBean.of(TripDto.class, "trip1"));
        Assert.assertNotNull(trip1);
    }
}
