package fr.ird.observe.spi.validation;

/*-
 * #%L
 * ObServe Core :: API :: Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.dto.referential.common.SpeciesDto;
import fr.ird.observe.dto.referential.ps.common.ObservedSystemReference;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by tchemit on 06/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ServiceValidationContext extends GeneratedServiceValidationContext {

    public ServiceValidationContext(ValidationRequestConfiguration configuration, DecoratorService decoratorService, Project selectModel, ObserveServicesProvider servicesProvider) {
        super(configuration, decoratorService, selectModel, servicesProvider);
    }

    @Override
    protected Supplier<List<ObservedSystemReference>> psCommonObservedSystemSupplier() {
        return () -> getServicesProvider().getReferentialService().getReferenceSet(ObservedSystemReference.class, null).toList();
    }

    @Override
    protected Supplier<List<String>> currentLlLogbookCatchSpeciesIdSupplier() {
        return () -> {
            TripDto dto = getCurrentLlCommonTrip();
            return dto == null ? List.of() : getServicesProvider().getLlCommonTripService().getLogbookCatchSpeciesIds(dto.getId());
        };
    }

    @Override
    protected Function<String, SpeciesDto> speciesFunction() {
        return function();
    }

    @Override
    public fr.ird.observe.services.service.data.ll.common.TripService getLlCommonTripService() {
        return getServicesProvider().getLlCommonTripService();
    }

    @Override
    public fr.ird.observe.services.service.data.ps.common.TripService getPsCommonTripService() {
        return getServicesProvider().getPsCommonTripService();
    }
}
