package fr.ird.observe;

/*-
 * #%L
 * ObServe Core :: API :: Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.ObserveFixtures;

import java.util.Map;

/**
 * Created on 28/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.2.0
 */
public class ObserveApiValidationFixtures extends ObserveFixtures {

    protected static final Map<String, String> STORE = loadFixturesMap("api-validation");
    public static final int VALIDATOR_MAP_DATA_COUNT = Integer.parseInt(STORE.get("VALIDATOR_MAP_DATA_COUNT"));
    public static final int VALIDATOR_MAP_CREATE_DATA_COUNT = VALIDATOR_MAP_DATA_COUNT / 2;
    public static final int VALIDATOR_MAP_UPDATE_DATA_COUNT = VALIDATOR_MAP_DATA_COUNT / 2;
    public static final int VALIDATORS_REFERENTIAL_COUNT = Integer.parseInt(STORE.get("VALIDATORS_REFERENTIAL_COUNT"));
    public static final int VALIDATOR_UPDATE_REFERENTIAL_COUNT = VALIDATORS_REFERENTIAL_COUNT / 2;
    public static final int VALIDATOR_CREATE_REFERENTIAL_COUNT = VALIDATORS_REFERENTIAL_COUNT / 2;
    public static final int VALIDATORS_DATA_COUNT = Integer.parseInt(STORE.get("VALIDATORS_DATA_COUNT"));
    public static final int VALIDATOR_UPDATE_EXTRA_CREATE_DATA_COUNT = Integer.parseInt(STORE.get("VALIDATOR_UPDATE_EXTRA_CREATE_DATA_COUNT"));
    public static final int VALIDATOR_CREATE_DATA_COUNT = (VALIDATORS_DATA_COUNT-VALIDATOR_UPDATE_EXTRA_CREATE_DATA_COUNT) / 2;
    public static final int VALIDATOR_UPDATE_DATA_COUNT = (VALIDATORS_DATA_COUNT-VALIDATOR_UPDATE_EXTRA_CREATE_DATA_COUNT) / 2;
    public static final int VALIDATORS_COUNT = VALIDATORS_REFERENTIAL_COUNT + VALIDATORS_DATA_COUNT;

    public static Map<String, String> getProperties() {
        return STORE;
    }

    public static String getProperty(String propertyName) {
        return getProperties().get(propertyName);
    }
}
