package fr.ird.observe.dto.validation;

/*-
 * #%L
 * ObServe Core :: API :: Validation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveApiValidationFixtures;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.test.TestSupportWithConfig;
import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.api.SimpleNuitonValidationContext;
import io.ultreia.java4all.validation.bean.NuitonValidatorsMap;
import io.ultreia.java4all.validation.impl.java.NuitonValidatorProviderFactoryImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by tchemit on 03/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ValidatorMapBuilderTest extends TestSupportWithConfig {

    private static final List<NuitonValidatorScope> SCOPES = Arrays.asList(NuitonValidatorScope.values());

    @Test
    public void forDataDto() {

        NuitonValidatorProvider provider = NuitonValidatorProviders.newProvider(NuitonValidatorProviderFactoryImpl.PROVIDER_NAME, new SimpleNuitonValidationContext(Locale.FRANCE));
        {
            NuitonValidatorsMap.Builder builder = NuitonValidatorsMap.create(provider, ObserveBusinessProject.get().getMapping().getMainDataClasses(), "create", SCOPES);
            NuitonValidatorsMap validatorsMap = builder.build();
            Assert.assertEquals(ObserveApiValidationFixtures.VALIDATOR_MAP_CREATE_DATA_COUNT, validatorsMap.size());
        }
        {
            NuitonValidatorsMap.Builder builder = NuitonValidatorsMap.create(provider, ObserveBusinessProject.get().getMapping().getMainDataClasses(), "update", SCOPES);
            NuitonValidatorsMap validatorsMap = builder.build();
            Assert.assertEquals(ObserveApiValidationFixtures.VALIDATOR_MAP_UPDATE_DATA_COUNT, validatorsMap.size());
        }
    }

    @Test
    public void forReferentialDto() {
        NuitonValidatorProvider provider = NuitonValidatorProviders.newProvider(NuitonValidatorProviderFactoryImpl.PROVIDER_NAME, new SimpleNuitonValidationContext(Locale.FRANCE));
        {
            NuitonValidatorsMap.Builder builder = NuitonValidatorsMap.create(provider, ObserveBusinessProject.get().getReferentialTypes(), "create", SCOPES);
            NuitonValidatorsMap validatorsMap = builder.build();
            Assert.assertEquals(ObserveApiValidationFixtures.VALIDATOR_CREATE_REFERENTIAL_COUNT, validatorsMap.size());
        }
        NuitonValidatorsMap.Builder builder = NuitonValidatorsMap.create(provider, ObserveBusinessProject.get().getReferentialTypes(), "update", SCOPES);
        NuitonValidatorsMap validatorsMap = builder.build();
        Assert.assertEquals(ObserveApiValidationFixtures.VALIDATOR_UPDATE_REFERENTIAL_COUNT, validatorsMap.size());
    }

}
