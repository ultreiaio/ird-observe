package fr.ird.observe.persistence.avdth;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.persistence.avdth.data.DataQuery;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.util.LinkedHashSet;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * Created on 26/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class AvdthQueries {
    private static final Logger log = LogManager.getLogger(AvdthQueries.class);

    public static AvdthQueries INSTANCE = new AvdthQueries();
    private final Set<DataQueryDefinition> dataQueries;
    private final Set<ReferentialQueryDefinition> referentialQueries;

    public static AvdthQueries get() {
        return INSTANCE == null ? INSTANCE = new AvdthQueries() : INSTANCE;
    }

    public static <T extends ReferentialQueryDefinition> ReferentialQuery referentialReader(Class<T> descriptorType, Connection conn) {
        return new ReferentialQuery(conn, get().referentialQuery(descriptorType));
    }

    public static <T extends DataQueryDefinition> DataQuery dataTableReader(Class<T> descriptorType, Connection conn) {
        return new DataQuery(conn, get().dataQuery(descriptorType));
    }

    private AvdthQueries() {
        log.info(String.format("Initializing... %s", this));
        dataQueries = new LinkedHashSet<>();
        referentialQueries = new LinkedHashSet<>();
        for (DataQueryDefinition tableDescriptor : ServiceLoader.load(DataQueryDefinition.class)) {
            dataQueries.add(tableDescriptor);
        }
        for (ReferentialQueryDefinition tableDescriptor : ServiceLoader.load(ReferentialQueryDefinition.class)) {
            referentialQueries.add(tableDescriptor);
        }
        log.info(String.format("Initialization done %s, found %d referential, %d data queries.", this, referentialQueries.size(), dataQueries.size()));
    }

    protected <T extends DataQueryDefinition> DataQueryDefinition dataQuery(Class<T> descriptorType) {
        for (DataQueryDefinition descriptor : dataQueries) {
            if (descriptor.getClass().equals(descriptorType)) {
                return descriptor;
            }
        }
        throw new IllegalStateException(String.format("Can't find data query definition: %s", descriptorType.getName()));
    }

    protected <T extends ReferentialQueryDefinition> ReferentialQueryDefinition referentialQuery(Class<T> descriptorType) {
        for (ReferentialQueryDefinition descriptor : referentialQueries) {
            if (descriptor.getClass().equals(descriptorType)) {
                return descriptor;
            }
        }
        throw new IllegalStateException(String.format("Can't find referential query definition: %s", descriptorType.getName()));
    }
}
