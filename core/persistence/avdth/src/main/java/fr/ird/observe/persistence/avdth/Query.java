package fr.ird.observe.persistence.avdth;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.TimeLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * To read one avdth (only once).
 * <p>
 * Created on 26/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class Query implements Iterator<ResultSet>, Closeable {
    public static final String JDBC_URL = "jdbc:ucanaccess://%s;openExclusive=true;immediatelyReleaseResources=true";
    static final Logger log = LogManager.getLogger(Query.class);
    /**
     * Size of the primary key.
     */
    protected final int primaryKeySize;
    private final String query;
    private final PreparedStatement preparedStatement;
    private final ResultSet resultSet;
    private final QueryDefinition descriptor;
    /**
     * To count number of row read.
     */
    private int count = 0;
    private List<Object> lastPrimaryKey;
    private List<Object> primaryKey;

    public static Connection create(File avdthFile, TimeLog timeLog) throws SQLException {
        long t0 = TimeLog.getTime();
        try {
            return DriverManager.getConnection(String.format(JDBC_URL, avdthFile.getAbsolutePath()));
//            return DriverManager.getConnection(String.format("jdbc:ucanaccess://%s;memory=false", avdthFile.getAbsolutePath()));
        } finally {
            timeLog.log(t0, "Open connection on " + avdthFile.getName());
        }
    }

    public static List<Object> getPrimaryKey(ResultSet resultSet, int primaryKeyBegin, int primaryKeyEnd) throws SQLException {
        List<Object> builder = new LinkedList<>();
        for (int i = primaryKeyBegin; i <= primaryKeyEnd; i++) {
            builder.add(resultSet.getObject(i));
        }
        return builder;
    }

    public static String primaryKeyString(List<Object> primaryKey) {
        return primaryKey.stream().map(Object::toString).collect(Collectors.joining("-"));
    }

    public Query(Connection conn, QueryDefinition descriptor) {
        this.descriptor = descriptor;
        this.primaryKeySize = this.descriptor.getPrimaryKeySize();
        this.query = descriptor.getQuery();
        log.debug(String.format("Will load: %s", query));
        this.preparedStatement = createPreparedStatement(conn, query);
        this.resultSet = createResultSet(preparedStatement);
    }

    public QueryDefinition getDescriptor() {
        return descriptor;
    }

    public final void setPrimaryKey(List<Object> primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String from() {
        return descriptor.from();
    }

    @Override
    public final boolean hasNext() {
        boolean hasNext;
        try {
            hasNext = resultSet.next();
        } catch (SQLException e) {
            throw new IllegalStateException("Can't get resultSet.next()", e);
        }
        if (!hasNext) {
            // no more row
            return false;
        }
        if (primaryKey == null) {
            // no filter on primary key
            return true;
        }
        try {
            List<Object> newPrimaryKey = getPrimaryKey(primaryKey.size());
            if (compareKeys(newPrimaryKey)) {
                // this row compliant with the given primary key
                return true;
            }
        } catch (SQLException e) {
            throw new IllegalStateException("Can't get getPrimaryKey()", e);
        }
        // this row is not compliant with the given primary key, go to previous row
        try {
            resultSet.previous();
        } catch (SQLException e) {
            throw new IllegalStateException("Can't get resultSet.previous()", e);
        }
        // no row matching
        return false;
    }

    @Override
    public final ResultSet next() {
        count++;
        try {
            lastPrimaryKey = getPrimaryKey(primaryKeySize);
        } catch (SQLException e) {
            throw new IllegalStateException("Can't get getPrimaryKey()", e);
        }
        return resultSet;
    }

    public final int count() {
        return count;
    }

    public final List<Object> lastPrimaryKey() {
        return lastPrimaryKey;
    }

    @Override
    public final void close() {
        log.debug(String.format("Will close: %s", query));
        try {
            resultSet.close();
        } catch (SQLException e) {
            log.error("Can't close resultSet", e);
        }
        try {
            preparedStatement.close();
        } catch (SQLException e) {
            log.error("Can't close preparedStatement", e);
        }
    }

    protected boolean compareKeys(List<Object> newPrimaryKey) {
        return primaryKey.equals(newPrimaryKey);
    }

    protected PreparedStatement createPreparedStatement(Connection conn, String query) {
        try {
            return Objects.requireNonNull(conn).prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        } catch (SQLException e) {
            throw new IllegalStateException("Can't create preparedStatement", e);
        }
    }

    protected ResultSet createResultSet(PreparedStatement preparedStatement) {
        try {
            return preparedStatement.executeQuery();
        } catch (SQLException e) {
            throw new IllegalStateException("Can't create resultSet", e);
        }
    }

    public List<Object> getPrimaryKey(int primaryKeySize) throws SQLException {
        List<Object> builder = new LinkedList<>();
        for (int i = 1; i <= primaryKeySize; i++) {
            builder.add(resultSet.getObject(i));
        }
        return builder;
    }

}
