package fr.ird.observe.persistence.avdth;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created on 26/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class QueryDefinition {

    private static final Logger log = LogManager.getLogger(QueryDefinition.class);

    /**
     * Size of the primary key.
     */
    protected final int primaryKeySize;
    /**
     * Query to execute.
     */
    private final String query;

    public QueryDefinition(int primaryKeySize) {
        this.query = createQuery();
        this.primaryKeySize = primaryKeySize;
        log.info(String.format("Detected query [%-35s]: %s", getClass().getSimpleName(), query));
    }

    public String getQuery() {
        return query;
    }

    public int getPrimaryKeySize() {
        return primaryKeySize;
    }

    public abstract String select();

    public abstract String from();

    public String innerJoin() {
        return null;
    }

    public String where() {
        return null;
    }

    public String orderBy() {
        return null;
    }

    public String mainTable() {
        String result = from();
        int indexOf = result.indexOf("JOIN");
        if (indexOf == -1) {
            return result;
        }
        result = result.substring(indexOf + 5);
        indexOf = result.indexOf(" ");
        result = result.substring(0, indexOf).trim();
        return result;
    }

    protected String createQuery() {
        String queryBuilder = "SELECT " + select() + " FROM " + from();
        if (innerJoin() != null) {
            queryBuilder += " INNER JOIN " + innerJoin();
        }
        if (where() != null) {
            queryBuilder += " WHERE " + where();
        }
        if (orderBy() != null) {
            queryBuilder += " ORDER BY " + orderBy();
        }
        return queryBuilder;
    }
}
