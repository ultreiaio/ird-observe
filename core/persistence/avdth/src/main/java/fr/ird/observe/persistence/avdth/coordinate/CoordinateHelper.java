package fr.ird.observe.persistence.avdth.coordinate;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Helper to deal with coordinates.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.2
 */
public class CoordinateHelper {

    /**
     * Calcule la valeur signée de la longitude à partir du {@code quadrant} et
     * de la valeur absolue de la {@code longitude}.
     *
     * @param quadrant  la valeur du quandrant (peut être null)
     * @param longitude la longitude décimale (peut être null)
     * @return la valeur signée de la longitude ou {@code null} si l'une des
     * deux données d'entrée est {@code null}.
     * @since 1.2
     */
    public static Float getSignedLongitude(Integer quadrant, Float longitude) {
        if (longitude == null) {
            return null;
        }
        if (quadrant == null) {

            // cas special ou pas encore de quadrant positionne, on conserve
            // juste la valeur de la longitude sans rien faire d'autre
            return longitude;
        }
        int result;
        switch (quadrant) {
            case 1:
            case 2:
                result = 1;
                break;
            default:
                result = -1;
        }
        return result * longitude;
    }

    /**
     * Calcule la valeur signée de la latitude à partir du {@code quadrant} et
     * de la valeur absolue de la {@code latitude}.
     *
     * @param quadrant la valeur du quandrant (peut être null)
     * @param latitude la longitude décimale (peut être null)
     * @return la valeur signée de la latitude ou {@code null} si l'une des
     * deux données d'entrée est {@code null}.
     * @since 1.2
     */
    public static Float getSignedLatitude(Integer quadrant, Float latitude) {
        if (latitude == null) {
            return null;
        }
        if (quadrant == null) {

            // cas special ou pas encore de quadrant positionne, on conserve
            // juste la valeur de la latitude sans rien faire d'autre
            return latitude;
        }
        int result;
        switch (quadrant) {
            case 1:
            case 4:
                result = 1;
                break;
            default:
                result = -1;
        }
        return result * latitude;
    }

}
