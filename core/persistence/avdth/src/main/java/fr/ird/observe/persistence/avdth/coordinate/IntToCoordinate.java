package fr.ird.observe.persistence.avdth.coordinate;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Un type pour l'import access qui permet de récupérer un coordonnee decimale, à
 * partir d'un {@code int}.
 * <p/>
 * Les deux derniers de l'int sont les minutes et optionnellement les premiers sont des degrès.
 * <p/>
 * Exemples:
 * Voici les valeurs utilisées :
 * <ul>
 * <li>00 = 0°0'</li>
 * <li>59 = 0°59'</li>
 * <li>100 = 1°0'</li>
 * <li>159 = 1°59'</li>
 * <li>1159 = 11°59'</li>
 * </ul>
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.2
 */
public class IntToCoordinate {

    protected final Integer intValue;

    public IntToCoordinate(Integer intValue) {
        this.intValue = intValue;
    }

    public Float getFloatValue() {
        if (intValue == null) {
            return null;
        }
        String strValue = (intValue + "").trim();
        int degree = 0;
        int minute = 0;
        switch (strValue.length()) {
            case 1:
            case 2:
                minute = intValue;
                break;
            case 3:
                degree = Integer.parseInt(strValue.charAt(0) + "");
                minute = Integer.parseInt(strValue.substring(1));
                break;
            case 4:
                degree = Integer.parseInt(strValue.substring(0, 2));
                minute = Integer.parseInt(strValue.substring(2));
                break;
            case 5:
                degree = Integer.parseInt(strValue.substring(0, 3));
                minute = Integer.parseInt(strValue.substring(3));
        }
        SexagecimalPosition position = SexagecimalPosition.valueOf(degree, minute, 0);
        return position.toDecimal();
    }
}
