package fr.ird.observe.persistence.avdth.coordinate;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.Objects;

/**
 * Le modèle d'une position au format sexagecimal (degre - minute - seconde).
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.2
 */
public class SexagecimalPosition implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer degree;
    private Integer minute;
    private Integer second;

    /**
     * Methode statique de fabrique de position a partir d'une valeur du format
     * decimal.
     * <p/>
     * Note : Si la valeur (au format decimal) vaut <code>null</code>, alors on
     * reinitialise les composants de la position a <code>null</code> et la
     * methode {@link #isNull()} vaudra alors {@code true}.
     *
     * @param decimal la valeur au format decimal
     * @return une nouvelle instance de position convertie
     */
    public static SexagecimalPosition valueOf(Float decimal) {
        SexagecimalPosition r = new SexagecimalPosition();
        r.update(decimal);
        return r;
    }

    /**
     * Methode statique de fabrique de position a partir d'une valeur du format
     * degre-minute-seconde.
     *
     * @param d la valeur des degres
     * @param m la valeur des minutes
     * @param s la valeur des secondes
     * @return une nouvelle instance de position convertie
     */
    public static SexagecimalPosition valueOf(int d, int m, int s) {
        SexagecimalPosition r = new SexagecimalPosition();
        r.setDegree(d);
        r.setMinute(m);
        r.setSecond(s);
        return r;
    }

    private SexagecimalPosition() {
        // no instance
    }

    /**
     * @return {@code true} si aucune composante n'est renseigné, {@code false} autrement.
     */
    public boolean isNull() {
        return degree == null && minute == null && second == null;
    }

    /**
     * Mets a jour les composants de la position a partir d'une valeur decimal.
     * <p/>
     * Note : Si la valeur (au format decimal) vaut <code>null</code>, alors on
     * reinitialise les composants de la position a <code>null</code> et la
     * methode {@link #isNull()} vaudra alors {@code true}.
     *
     * @param decimal la valeur decimale a convertir (qui peut etre nulle).
     */
    public void update(Float decimal) {
        Integer d = null;
        Integer m = null;
        Integer s = null;
        if (decimal != null) {
            int remain = 0;

            d = (int) (Math.round(decimal + 0.5) - 1);
            m = 0;
            s = 0;
            decimal = 60 * (decimal - d);
            if (decimal > 0) {
                m = (int) (Math.round(decimal + 0.5) - 1);
                decimal = 60 * (decimal - m);
                if (decimal > 0) {
                    s = (int) (Math.round(decimal + 0.5) - 1);
                    remain = (int) (10 * (decimal - s));
                }
            }
            if (remain > 9) {
                s++;
//                remain = 0;
            }
            if (s == 60) {
                m++;
                s = 0;
            }
            if (m == 60) {
                d++;
                m = 0;
            }
        }
        degree = d;
        minute = m;
        second = s;
    }

    public Float toDecimal() {
        if (isNull()) {
            return null;
        }
        int d = degree == null ? 0 : degree;
        int m = minute == null ? 0 : minute;
        int s = second == null ? 0 : second;
        float result = (float) d;
        if (m > 0) {
            result += (float) m / 60;
            if (s == 0) {
                result += 0.5f / 3600;
            }
        }
        if (s > 0) {
            result += ((float) s + 0.5f) / 3600;
        }
        return result;
    }

    public Integer getDegree() {
        return degree;
    }

    public void setDegree(Integer degree) {
        this.degree = degree;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getSecond() {
        return second;
    }

    public void setSecond(Integer second) {
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SexagecimalPosition that = (SexagecimalPosition) o;
        return Objects.equals(degree, that.getDegree()) &&
                Objects.equals(minute, that.getMinute()) &&
                Objects.equals(second, that.getSecond());
    }

    @Override
    public int hashCode() {
        return Objects.hash(degree, minute, second);
    }
}
