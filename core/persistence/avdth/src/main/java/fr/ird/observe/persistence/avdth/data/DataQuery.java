package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.persistence.avdth.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 10/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class DataQuery extends Query {

    private static final Logger log = LogManager.getLogger(DataQuery.class);

    /**
     * Count of row in table (without any join).
     */
    private int tableCount;
    /**
     * Count of effective read row in table (may be with a join).
     */
    private int tableReadCount;
    /**
     * List of missing primary keys.
     */
    private List<Object[]> missingPrimaryKeys;

    public DataQuery(Connection conn, DataQueryDefinition descriptor) {
        super(conn, descriptor);
    }

    @Override
    public DataQueryDefinition getDescriptor() {
        return (DataQueryDefinition) super.getDescriptor();
    }

    public int tableCount() {
        return tableCount;
    }

    public int tableReadCount() {
        return tableReadCount;
    }

    public void count(Connection conn) {
        DataQueryDefinition descriptor = getDescriptor();
        tableCount = count(conn, descriptor.createAllCountQuery());
        tableReadCount = count(conn, descriptor.createReadCountQuery());
        log.info(String.format("Count [%20s]: %6d / %6d", descriptor.from(), tableReadCount(), tableCount()));
        if (tableCount > tableReadCount) {
            missingPrimaryKeys = collectPrimaryKeys(conn, descriptor.createMissingQuery());
        }
    }

    public List<Object[]> getMissingPrimaryKeys() {
        return missingPrimaryKeys;
    }

    protected int count(Connection conn, String countQuery) {
        try (PreparedStatement countStatement = createPreparedStatement(conn, countQuery)) {
            try (ResultSet resultSet = createResultSet(countStatement)) {
                resultSet.next();
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(String.format("Can't count table with query [%s]", countQuery), e);
        }
    }

    protected List<Object[]> collectPrimaryKeys(Connection conn, String countQuery) {
        List<Object[]> result = new LinkedList<>();
        try (PreparedStatement countStatement = createPreparedStatement(conn, countQuery)) {
            try (ResultSet resultSet = createResultSet(countStatement)) {
                while (resultSet.next()) {
                    List<Object> primaryKey = Query.getPrimaryKey(resultSet, 1, primaryKeySize);
                    result.add(primaryKey.toArray(new Object[0]));
                }
                resultSet.next();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(String.format("Can't count table with query [%s]", countQuery), e);
        }
        return result;
    }
}
