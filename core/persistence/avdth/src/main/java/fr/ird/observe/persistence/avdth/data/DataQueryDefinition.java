package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.persistence.avdth.QueryDefinition;

/**
 * Created on 26/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class DataQueryDefinition extends QueryDefinition {

    public DataQueryDefinition(int primaryKeySize) {
        super(primaryKeySize);
    }

    public String createAllCountQuery() {
        return "SELECT COUNT(*) FROM " + from();
    }

    public String createReadCountQuery() {
        String result = "SELECT COUNT(*) FROM " + from();
        if (innerJoin() != null) {
            result += " INNER JOIN " + innerJoin();
        }
        return result;
    }

    public String createMissingQuery() {
        String result = String.format("SELECT %1$s.* FROM %1$s", from());
//        if (innerJoin() != null) {
//            result += " INNER JOIN " + innerJoin();
//        }
        if (whereMissing() != null) {
            result += " WHERE " + whereMissing();
        }
        if (orderBy() != null) {
            result += " ORDER BY " + orderBy();
        }
        return result;
    }

    protected String whereMissing() {
        return null;
    }


}
