package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.persistence.avdth.coordinate.CoordinateHelper;
import fr.ird.observe.persistence.avdth.coordinate.IntToCoordinate;
import fr.ird.observe.services.service.data.ps.AvdthDataImportConfiguration;
import fr.ird.observe.spi.context.DtoEntityContext;
import org.apache.commons.lang3.mutable.MutableInt;
import org.nuiton.topia.persistence.TopiaIdFactoryForBulkSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

/**
 * Created on 03/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class DataReader<E extends DataEntity> {
    /**
     * Configuration of import (with also import context).
     */
    private final ImportEngine context;
    /**
     * Internal topia Id factory.
     */
    private final TopiaIdFactoryForBulkSupport topiaIdFactory;
    /**
     * Internal counter of created entities of this type.
     */
    private int count = 0;

    public DataReader(ImportEngine context) {
        this.context = Objects.requireNonNull(context);
        long idTimestamp = context.getConfiguration().getIdTimestamp();
        this.topiaIdFactory = context.getPersistenceApplicationContext().newIdFactoryForBulk(idTimestamp);
    }

    public ImportEngine context() {
        return context;
    }

    public abstract E read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException;

    public final int getCount() {
        return count;
    }

    protected final Float toLatitudeCoordinate(int quadrant, int coordinate) {
        return CoordinateHelper.getSignedLatitude(quadrant, new IntToCoordinate(coordinate).getFloatValue());
    }

    protected final Float toLongitudeCoordinate(int quadrant, int coordinate) {
        return CoordinateHelper.getSignedLongitude(quadrant, new IntToCoordinate(coordinate).getFloatValue());
    }

    protected final <EE extends DataEntity> EE newEntity(DtoEntityContext<?, ?, EE, ?> spi) {
        String id = topiaIdFactory.newTopiaId(spi.toEntityType());
        count++;
        return spi.newEntity(id);
    }

    protected final <EE extends DataEntity> EE newEntity(DtoEntityContext<?, ?, EE, ?> spi, MutableInt count) {
        String id = topiaIdFactory.newTopiaId(spi.toEntityType());
        count.increment();
        return spi.newEntity(id);
    }

    protected final AvdthDataImportConfiguration getContext() {
        return context.getConfiguration();
    }

    protected final void addMessage(String message) {
        context.addMessage(message);
    }
}

