package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.persistence.avdth.data.common.LandingReader;
import fr.ird.observe.persistence.avdth.data.common.TripReader;
import fr.ird.observe.persistence.avdth.data.localmarket.BatchReader;
import fr.ird.observe.persistence.avdth.data.localmarket.SampleReader;
import fr.ird.observe.persistence.avdth.data.localmarket.SampleSpeciesMeasureReader;
import fr.ird.observe.persistence.avdth.data.localmarket.SampleSpeciesReader;
import fr.ird.observe.persistence.avdth.data.localmarket.SurveyReader;
import fr.ird.observe.persistence.avdth.data.logbook.ActivityReader;
import fr.ird.observe.persistence.avdth.data.logbook.CatchReader;
import fr.ird.observe.persistence.avdth.data.logbook.SampleActivityReader;
import fr.ird.observe.persistence.avdth.data.logbook.WellActivityReader;
import fr.ird.observe.persistence.avdth.data.logbook.WellReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 29/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class DataTables implements Closeable {
    private static final Logger log = LogManager.getLogger(DataTables.class);
    protected final List<DataQuery> tableReaders;
    final DataQuery tripTableReader;
    final DataQuery activityTableReader;
    final DataQuery activityObservedSystemTableReader;
    final DataQuery catchTableReader;
    final DataQuery sampleTableReader;
    final DataQuery sampleActivityTableReader;
    final DataQuery sampleSpeciesTableReader;
    final DataQuery sampleSpeciesMeasureTableReader;
    final DataQuery localMarketSampleTableReader;
    final DataQuery localMarketSampleSpeciesTableReader;
    final DataQuery localMarketSampleSpeciesMeasureTableReader;
    final DataQuery localMarketSampleWellTableReader;
    final DataQuery localMarketBatchTableReader;
    final DataQuery localMarketSurveyTableReader;
    final DataQuery wellTableReader;
    final DataQuery wellActivityTableReader;
    final DataQuery landingTableReader;

    public DataTables(Connection conn) {
        this.tableReaders = new LinkedList<>();
        tableReaders.add(tripTableReader = AvdthQueries.dataTableReader(TripReader.TripTableReader.class, conn));
        tableReaders.add(activityTableReader = AvdthQueries.dataTableReader(ActivityReader.ActivityTableReader.class, conn));
        tableReaders.add(activityObservedSystemTableReader = AvdthQueries.dataTableReader(ActivityReader.ActivityObservedSystemTableReader.class, conn));
        tableReaders.add(catchTableReader = AvdthQueries.dataTableReader(CatchReader.CatchTableReader.class, conn));
        tableReaders.add(sampleTableReader = AvdthQueries.dataTableReader(fr.ird.observe.persistence.avdth.data.logbook.SampleReader.SampleTableReader.class, conn));
        tableReaders.add(sampleActivityTableReader = AvdthQueries.dataTableReader(SampleActivityReader.SampleActivityTableReader.class, conn));
        tableReaders.add(sampleSpeciesTableReader = AvdthQueries.dataTableReader(fr.ird.observe.persistence.avdth.data.logbook.SampleSpeciesReader.SampleSpeciesTableReader.class, conn));
        tableReaders.add(sampleSpeciesMeasureTableReader = AvdthQueries.dataTableReader(fr.ird.observe.persistence.avdth.data.logbook.SampleSpeciesMeasureReader.SampleSpeciesMeasureTableReader.class, conn));
        tableReaders.add(wellTableReader = AvdthQueries.dataTableReader(WellReader.WellTableReader.class, conn));
        tableReaders.add(wellActivityTableReader = AvdthQueries.dataTableReader(WellActivityReader.WellActivityTableReader.class, conn));
        tableReaders.add(landingTableReader = AvdthQueries.dataTableReader(LandingReader.LandingTableReader.class, conn));
        tableReaders.add(localMarketSampleTableReader = AvdthQueries.dataTableReader(SampleReader.SampleTableReader.class, conn));
        tableReaders.add(localMarketSampleWellTableReader = AvdthQueries.dataTableReader(SampleReader.SampleWellTableReader.class, conn));
        tableReaders.add(localMarketSampleSpeciesTableReader = AvdthQueries.dataTableReader(SampleSpeciesReader.SampleSpeciesTableReader.class, conn));
        tableReaders.add(localMarketSampleSpeciesMeasureTableReader = AvdthQueries.dataTableReader(SampleSpeciesMeasureReader.SampleSpeciesMeasureTableReader.class, conn));
        tableReaders.add(localMarketBatchTableReader = AvdthQueries.dataTableReader(BatchReader.BatchTableReader.class, conn));
        tableReaders.add(localMarketSurveyTableReader = AvdthQueries.dataTableReader(SurveyReader.SurveyTableReader.class, conn));
    }

    @Override
    public void close() {
        for (Query query : tableReaders) {
            try {
                query.close();
            } catch (Exception e) {
                log.error(e);
            }
        }
        tableReaders.clear();
    }

    public Map<String, Integer> getTablesCount() {
        Map<String, Integer> result = new TreeMap<>();
        tableReaders.forEach(tableReader -> result.put(tableReader.from(), tableReader.tableCount()));
        return result;
    }

    public Map<String, Integer> getTablesRead() {
        Map<String, Integer> result = new TreeMap<>();
        tableReaders.forEach(tableReader -> result.put(tableReader.from(), tableReader.count()));
        return result;
    }

    public Map<String, Integer> getTablesReadCount() {
        Map<String, Integer> result = new TreeMap<>();
        tableReaders.forEach(tableReader -> result.put(tableReader.from(), tableReader.tableReadCount()));
        return result;
    }
}
