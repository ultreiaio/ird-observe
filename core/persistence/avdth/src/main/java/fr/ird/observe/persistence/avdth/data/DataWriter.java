package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.SqlHelper;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.spi.SqlConversions;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;

/**
 * Created on 28/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class DataWriter<E extends DataEntity, R extends DataReader<E>> {

    /**
     * to collect bad species found in readers.
     */
    protected final Set<String> badSpecies = new TreeSet<>();
    /**
     * Configuration of import (with also import context).
     */
    private final ImportEngine context;
    /**
     * Data reader.
     */
    private final R reader;
    /**
     * Current writer.
     */
    private SqlScriptWriter writer;
    /**
     * to format in sql code comment.
     */
    private final Function<String, String> commentFormat;


    public static String roundFloat2(Float comment) {
        if (comment == null) {
            return "NULL";
        }
        return "" + Numbers.roundTwoDigits(comment);
    }

    public static String roundFloat4(Float comment) {
        if (comment == null) {
            return "NULL";
        }
        return "" + Numbers.roundFourDigits(comment);
    }

    protected String escapeComment(String comment) {
        return commentFormat.apply(comment);
    }

    public DataWriter(ImportEngine context, R reader) {
        this.context = Objects.requireNonNull(context);
        this.reader = reader;
        this.commentFormat = SqlHelper.escapeComment(context.getPersistenceApplicationContext().getConfiguration().isPostgresConfiguration());
    }

    public ImportDataContext dataContext() {
        return context.context;
    }

    public abstract void write(E entity, String parentId, int parentCount);

    public E intercept(ResultSet resultSet, String parentId) throws SQLException {
        return intercept(resultSet, parentId, 0);
    }

    public E intercept(ResultSet resultSet, String parentId, int parentCount) throws SQLException {
        E entity = getReader().read(dataContext(), resultSet);
        write(entity, parentId, parentCount);
        return entity;
    }

    public final R getReader() {
        return reader;
    }

    public final Set<String> getBadSpecies() {
        return badSpecies;
    }

    public void flush() {
        flush(entityType(), getReader().getCount());
    }

    public void toResult(Map<String, Integer> resultBuilder) {
        toResult(resultBuilder, entityType(), getReader().getCount());
    }

    public SqlScriptWriter writer() {
        return writer;
    }

    public void setWriter(SqlScriptWriter writer) {
        this.writer = writer;
    }

    protected final void toResult(Map<String, Integer> resultBuilder, Class<?> entityType, int count) {
        resultBuilder.put(entityType.getName(), count);
    }

    protected final void flush(Class<?> entityType, int count) {
        if (count > 0) {
            writer.writeSql(String.format("UPDATE common.LastUpdateDate SET lastUpdateDate = '%s'::timestamp WHERE type = '%s';", timestamp(), entityType.getName()));
        }
    }

    protected final String timestamp() {
        return SqlConversions.timestamp(new Date());
    }

    protected final Class<?> entityType() {
        return ObserveUtil.getFirstType(this);
    }
}
