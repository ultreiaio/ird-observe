package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.data.ps.logbook.Route;
import fr.ird.observe.entities.referential.ps.common.ObservedSystem;
import fr.ird.observe.persistence.avdth.data.logbook.FloatingObjectReader;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * To keep some data while import (used to create new data if not found).
 * <p>
 * Created on 28/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ImportDataContext extends ImportReferentialContext {

    private final Set<ObservedSystem> observedSystems = new LinkedHashSet<>();
    private final Map<String, String> activityPkToId = new TreeMap<>();
    private final Map<String, String> routeDateToId = new TreeMap<>();
    private Trip trip;
    private Route route;
    private Activity activity;
    /**
     * Collected avdth observed system code from the ACT_ASSOC.
     *
     * @see fr.ird.observe.persistence.avdth.data.logbook.ActivityReader.ActivityObservedSystemTableReader
     */
    private Set<String> avdthObservedSystemCodes;
    private boolean canCreateActivity;

    public boolean isCanCreateActivity() {
        return canCreateActivity;
    }

    public Map<String, String> getRouteDateToId() {
        return routeDateToId;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
        route = null;
        activity = null;
        canCreateActivity = false;
        routeDateToId.clear();
        activityPkToId.clear();
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
        routeDateToId.put(route.getDate().toString(), route.getTopiaId());
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity, String activityPk) {
        this.activity = activity;
        activityPkToId.put(activityPk, activity.getTopiaId());
    }

    public Map<String, String> getActivityPkToId() {
        return activityPkToId;
    }

    public Set<String> getAvdthObservedSystemCodes() {
        return avdthObservedSystemCodes;
    }

    public Set<ObservedSystem> getObservedSystems() {
        return observedSystems;
    }

    public void setAvdthObservedSystemCodes(Set<String> avdthObservedSystemCodes) {
        this.avdthObservedSystemCodes = avdthObservedSystemCodes;
    }

    public void addDefaultObservedSystemOrSanitizeFloatingObjectOnes(Activity entity) {
        // Remove any observed system from code 21 to 25, and at last if one of them has been found
        // add (if not already present the observed system 20, this case should never happen, but just in case...)
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2548
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2961

        boolean floatingObjectCreatedByObservedSystem = avdthObservedSystemCodes.stream().anyMatch(FloatingObjectReader.OBSERVED_SYTEM_CODES_WITH_DCP::contains);
        if (floatingObjectCreatedByObservedSystem) {
            boolean observedSystem20Found = false;
            for (ObservedSystem observedSystem : entity.getObservedSystem()) {
                if ("20".equals(observedSystem.getCode())) {
                    observedSystem20Found = true;
                }
            }
            if (!observedSystem20Found) {
                // add the Observed system 20
                entity.addObservedSystem(getObservedSystem20());
                return;
            }
        }
        if (entity.isObservedSystemEmpty()) {
            // add no observed system
            entity.addObservedSystem(getObservedSystem0());
        }
    }

    public void computeCanCreateActivity() {
        canCreateActivity = activityPkToId.isEmpty();
    }
}
