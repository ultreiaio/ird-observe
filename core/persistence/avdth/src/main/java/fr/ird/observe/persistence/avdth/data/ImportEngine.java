package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.data.ps.logbook.Route;
import fr.ird.observe.persistence.avdth.AvdthReferentialBuilder;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.persistence.avdth.data.common.LandingReader;
import fr.ird.observe.persistence.avdth.data.common.LandingWriter;
import fr.ird.observe.persistence.avdth.data.common.TripReader;
import fr.ird.observe.persistence.avdth.data.common.TripWriter;
import fr.ird.observe.persistence.avdth.data.localmarket.BatchReader;
import fr.ird.observe.persistence.avdth.data.localmarket.BatchWriter;
import fr.ird.observe.persistence.avdth.data.localmarket.SampleReader;
import fr.ird.observe.persistence.avdth.data.localmarket.SampleSpeciesMeasureWriter;
import fr.ird.observe.persistence.avdth.data.localmarket.SampleSpeciesWriter;
import fr.ird.observe.persistence.avdth.data.localmarket.SampleWriter;
import fr.ird.observe.persistence.avdth.data.localmarket.SurveyReader;
import fr.ird.observe.persistence.avdth.data.localmarket.SurveyWriter;
import fr.ird.observe.persistence.avdth.data.logbook.ActivityReader;
import fr.ird.observe.persistence.avdth.data.logbook.ActivityWriter;
import fr.ird.observe.persistence.avdth.data.logbook.CatchReader;
import fr.ird.observe.persistence.avdth.data.logbook.CatchWriter;
import fr.ird.observe.persistence.avdth.data.logbook.FloatingObjectReader;
import fr.ird.observe.persistence.avdth.data.logbook.FloatingObjectWriter;
import fr.ird.observe.persistence.avdth.data.logbook.RouteReader;
import fr.ird.observe.persistence.avdth.data.logbook.RouteWriter;
import fr.ird.observe.persistence.avdth.data.logbook.SampleSpeciesMeasureReader;
import fr.ird.observe.persistence.avdth.data.logbook.SampleSpeciesReader;
import fr.ird.observe.persistence.avdth.data.logbook.WellActivityReader;
import fr.ird.observe.persistence.avdth.data.logbook.WellActivityWriter;
import fr.ird.observe.persistence.avdth.data.logbook.WellReader;
import fr.ird.observe.persistence.avdth.data.logbook.WellWriter;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.SpeciesCache;
import fr.ird.observe.services.service.data.ps.AvdthDataImportConfiguration;
import fr.ird.observe.services.service.data.ps.AvdthDataImportResult;
import fr.ird.observe.services.service.data.ps.MissingReferentialException;
import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 03/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ImportEngine implements Closeable {

    private static final Logger log = LogManager.getLogger(ImportEngine.class);
    public static final TimeLog timeLog = new TimeLog(ImportEngine.class, 100, 200);

    /**
     * Import configuration.
     */
    protected final AvdthDataImportConfiguration configuration;
    /**
     * Set of temporary files (one for each trip).
     */
    protected final Set<Path> temporaryFiles;
    /**
     * Persistence application context.
     */
    protected final ObserveTopiaApplicationContext persistenceApplicationContext;
    /**
     * Where to store messages.
     */
    protected final Path messageFile;
    /**
     * Connection to avdth database.
     */
    protected final Connection conn;
    protected final Set<DataWriter<?, ?>> writers;
    protected final Set<DataReader<?>> readers;
    final ImportDataContext context;
    final TripReader tripReader;
    final RouteReader routeReader;
    final ActivityReader activityReader;
    final FloatingObjectReader floatingObjectReader;
    final CatchReader catchReader;
    final fr.ird.observe.persistence.avdth.data.logbook.SampleReader sampleReader;
    final fr.ird.observe.persistence.avdth.data.logbook.SampleActivityReader sampleActivityReader;
    final SampleSpeciesReader sampleSpeciesReader;
    final SampleSpeciesMeasureReader sampleSpeciesMeasureReader;
    final SampleReader localMarketSampleReader;
    final fr.ird.observe.persistence.avdth.data.localmarket.SampleSpeciesReader localMarketSampleSpeciesReader;
    final fr.ird.observe.persistence.avdth.data.localmarket.SampleSpeciesMeasureReader localMarketSampleSpeciesMeasureReader;
    final BatchReader localMarketBatchReader;
    final SurveyReader localMarketSurveyReader;
    final WellReader wellReader;
    final WellActivityReader wellActivityReader;
    final LandingReader landingReader;
    final TripWriter tripWriter;
    final RouteWriter routeWriter;
    final ActivityWriter activityWriter;
    final CatchWriter catchWriter;
    final FloatingObjectWriter floatingObjectWriter;
    final fr.ird.observe.persistence.avdth.data.logbook.SampleWriter sampleWriter;
    final fr.ird.observe.persistence.avdth.data.logbook.SampleActivityWriter sampleActivityWriter;
    final fr.ird.observe.persistence.avdth.data.logbook.SampleSpeciesWriter sampleSpeciesWriter;
    final fr.ird.observe.persistence.avdth.data.logbook.SampleSpeciesMeasureWriter sampleSpeciesMeasureWriter;
    final SampleWriter localMarketSampleWriter;
    final SampleSpeciesWriter localMarketSampleSpeciesWriter;
    final SampleSpeciesMeasureWriter localMarketSampleSpeciesMeasureWriter;
    final BatchWriter localMarketBatchWriter;
    final SurveyWriter localMarketSurveyWriter;
    final WellWriter wellWriter;
    final WellActivityWriter wellActivityWriter;
    final LandingWriter landingWriter;
    /**
     * Loaded avdth referential.
     */
    protected AvdthReferentialImportResult referential;
    /**
     * The last generated export file.
     */
    protected Path lastFile;
    /**
     * messages writer.
     */
    protected BufferedWriter messageWriter;
    /**
     * Table count.
     */
    protected Map<String, Integer> tablesCount;
    /**
     * Table read count. (This may be different that {@link #tablesCount} since for some tables we use inner join to get
     * safe data).
     */
    protected Map<String, Integer> tablesReadCount;
    /**
     * Effective table read count (in read method)..
     */
    protected Map<String, Integer> tablesRead;
    /**
     * Table not read count (difference between {@link #tablesCount} and {@link #tablesReadCount}).
     */
    protected Map<String, Integer> notReadResult;
    /**
     * Message count.
     */
    protected int messageCount = 0;

    public static AvdthDataImportResult run(AvdthDataImportConfiguration configuration, ObserveTopiaApplicationContext applicationContext) throws IOException, SQLException, MissingReferentialException {
        try (ImportEngine context = new ImportEngineExecution(configuration, applicationContext)) {
            context.prepare();
            context.read();
            context.flush();
            return context.toResult();
        } finally {
            ObserveUtil.cleanMemory();
        }
    }

    public ImportEngine(AvdthDataImportConfiguration context, ObserveTopiaApplicationContext persistenceApplicationContext) throws SQLException, IOException {
        configuration = Objects.requireNonNull(context);
        this.context = new ImportDataContext();
        this.persistenceApplicationContext = Objects.requireNonNull(persistenceApplicationContext);
        this.temporaryFiles = new LinkedHashSet<>();

        readers = new LinkedHashSet<>();
        readers.add(tripReader = new TripReader(this));
        readers.add(routeReader = new RouteReader(this));
        readers.add(activityReader = new ActivityReader(this));
        readers.add(floatingObjectReader = new FloatingObjectReader(this));
        readers.add(catchReader = new CatchReader(this));
        readers.add(sampleReader = new fr.ird.observe.persistence.avdth.data.logbook.SampleReader(this));
        readers.add(sampleActivityReader = new fr.ird.observe.persistence.avdth.data.logbook.SampleActivityReader(this));
        readers.add(sampleSpeciesReader = new SampleSpeciesReader(this));
        readers.add(sampleSpeciesMeasureReader = new SampleSpeciesMeasureReader(this));
        readers.add(wellReader = new WellReader(this));
        readers.add(wellActivityReader = new WellActivityReader(this));
        readers.add(landingReader = new LandingReader(this));
        readers.add(localMarketSampleReader = new SampleReader(this));
        readers.add(localMarketSampleSpeciesReader = new fr.ird.observe.persistence.avdth.data.localmarket.SampleSpeciesReader(this));
        readers.add(localMarketSampleSpeciesMeasureReader = new fr.ird.observe.persistence.avdth.data.localmarket.SampleSpeciesMeasureReader(this));
        readers.add(localMarketBatchReader = new BatchReader(this));
        readers.add(localMarketSurveyReader = new SurveyReader(this));

        writers = new LinkedHashSet<>();
        writers.add(activityWriter = new ActivityWriter(this, activityReader));
        writers.add(catchWriter = new CatchWriter(this, catchReader));
        writers.add(tripWriter = new TripWriter(this, tripReader));
        writers.add(routeWriter = new RouteWriter(this, routeReader));
        writers.add(sampleWriter = new fr.ird.observe.persistence.avdth.data.logbook.SampleWriter(this, sampleReader));
        writers.add(sampleActivityWriter = new fr.ird.observe.persistence.avdth.data.logbook.SampleActivityWriter(this, sampleActivityReader));
        writers.add(sampleSpeciesWriter = new fr.ird.observe.persistence.avdth.data.logbook.SampleSpeciesWriter(this, sampleSpeciesReader));
        writers.add(sampleSpeciesMeasureWriter = new fr.ird.observe.persistence.avdth.data.logbook.SampleSpeciesMeasureWriter(this, sampleSpeciesMeasureReader));
        writers.add(wellWriter = new WellWriter(this, wellReader));
        writers.add(wellActivityWriter = new WellActivityWriter(this, wellActivityReader));
        writers.add(landingWriter = new LandingWriter(this, landingReader));
        writers.add(localMarketSampleWriter = new SampleWriter(this, localMarketSampleReader));
        writers.add(localMarketSampleSpeciesWriter = new SampleSpeciesWriter(this, localMarketSampleSpeciesReader));
        writers.add(localMarketSampleSpeciesMeasureWriter = new SampleSpeciesMeasureWriter(this, localMarketSampleSpeciesMeasureReader));
        writers.add(localMarketBatchWriter = new BatchWriter(this, localMarketBatchReader));
        writers.add(localMarketSurveyWriter = new SurveyWriter(this, localMarketSurveyReader));
        writers.add(floatingObjectWriter = new FloatingObjectWriter(this, floatingObjectReader));

        configuration.getProgressionModel().setMaximum(
                /* connection */ 2 +
                                         /* prepare */  2 * AvdthReferentialBuilder.STEP_COUNT +
                                         /* prepare count data tables (one step by table reader) (there is one more dataReader than reader) */(2 + readers.size()) +
                                         /* prepare read data tables (two step by writers) */ writers.size() * 2 +
                                         /* read */  1
        );
        configuration.getProgressionModel().setValue(0);
        configuration.incrementsProgression("Connexion à la base AVDTH en cours...");
        this.conn = Query.create(configuration.getAvdthFile().toFile(), timeLog);
        configuration.incrementsProgression("Connexion à la base AVDTH terminée.");
        if (Files.notExists(configuration.getScriptTemporaryDirectory())) {
            Files.createDirectories(configuration.getScriptTemporaryDirectory());
        }
        this.messageFile = configuration.getScriptTemporaryDirectory().resolve("messages.txt");
        this.messageWriter = Files.newBufferedWriter(messageFile);
        timeLog.reset();
    }

    public ImportEngine(ImportEngine parent) {
        this.configuration = Objects.requireNonNull(parent).configuration;
        this.context = parent.context;
        this.persistenceApplicationContext = parent.persistenceApplicationContext;
        this.temporaryFiles = parent.temporaryFiles;

        this.conn = parent.conn;
        this.referential = parent.referential;
        this.lastFile = parent.lastFile;
        this.notReadResult = parent.notReadResult;

        this.messageFile = parent.messageFile;
        this.messageWriter = parent.messageWriter;

        tripReader = parent.tripReader;
        routeReader = parent.routeReader;
        activityReader = parent.activityReader;
        floatingObjectReader = parent.floatingObjectReader;
        catchReader = parent.catchReader;
        sampleReader = parent.sampleReader;
        sampleActivityReader = parent.sampleActivityReader;
        sampleSpeciesReader = parent.sampleSpeciesReader;
        sampleSpeciesMeasureReader = parent.sampleSpeciesMeasureReader;
        wellReader = parent.wellReader;
        wellActivityReader = parent.wellActivityReader;
        landingReader = parent.landingReader;
        localMarketSampleReader = parent.localMarketSampleReader;
        localMarketSampleSpeciesReader = parent.localMarketSampleSpeciesReader;
        localMarketSampleSpeciesMeasureReader = parent.localMarketSampleSpeciesMeasureReader;
        localMarketBatchReader = parent.localMarketBatchReader;
        localMarketSurveyReader = parent.localMarketSurveyReader;
        activityWriter = parent.activityWriter;
        catchWriter = parent.catchWriter;
        tripWriter = parent.tripWriter;
        routeWriter = parent.routeWriter;
        sampleWriter = parent.sampleWriter;
        sampleActivityWriter = parent.sampleActivityWriter;
        sampleSpeciesWriter = parent.sampleSpeciesWriter;
        sampleSpeciesMeasureWriter = parent.sampleSpeciesMeasureWriter;
        wellWriter = parent.wellWriter;
        wellActivityWriter = parent.wellActivityWriter;
        landingWriter = parent.landingWriter;
        localMarketSampleWriter = parent.localMarketSampleWriter;
        localMarketSampleSpeciesWriter = parent.localMarketSampleSpeciesWriter;
        localMarketSampleSpeciesMeasureWriter = parent.localMarketSampleSpeciesMeasureWriter;
        localMarketBatchWriter = parent.localMarketBatchWriter;
        localMarketSurveyWriter = parent.localMarketSurveyWriter;
        floatingObjectWriter = parent.floatingObjectWriter;
        tablesCount = parent.tablesCount;
        tablesRead = parent.tablesRead;
        tablesReadCount = parent.tablesReadCount;
        messageCount = parent.messageCount;
        writers = parent.writers;
        readers = parent.readers;
    }

    public Map<String, Integer> getExportResult() {
        Map<String, Integer> result = new TreeMap<>();
        writers.forEach(writer -> writer.toResult(result));
        return result;
    }

    public void flush() throws IOException {
        messageWriter.flush();
        long time0 = TimeLog.getTime();
        try (SqlScriptWriter ignored = newWriter("update-lastUpdateTable")) {
            writers.forEach(DataWriter::flush);
        }
        timeLog.log(time0, "Export trip", "All");
        try (OutputStream wbc = Files.newOutputStream(configuration.getExportFile(), StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {
            for (Path temporaryFile : getTemporaryFiles()) {
                Files.copy(temporaryFile, wbc);
            }
        }
        log.info(String.format("Export to\n%s", configuration.getExportFile()));
        timeLog.log(time0, "Export trip", "All");
    }

    public AvdthDataImportResult toResult() {
        return new AvdthDataImportResult(configuration,
                                         configuration.getExportFile(),
                                         getMessageFile(),
                                         getMessageCount(),
                                         tablesCount == null ? Map.of() : tablesCount,
                                         tablesReadCount == null ? Map.of() : tablesReadCount,
                                         tablesRead == null ? Map.of() : tablesRead,
                                         notReadResult == null ? Map.of() : notReadResult,
                                         getExportResult());
    }

    @Override
    public void close() {
        try {
            conn.close();
        } catch (Exception e) {
            log.error(e);
        }
        try {
            messageWriter.close();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public Connection getConn() {
        return conn;
    }

    public AvdthDataImportConfiguration getConfiguration() {
        return configuration;
    }

    public Path getLastFile() {
        return lastFile;
    }

    public void setReferential(AvdthReferentialImportResult referential) {
        this.referential = referential;
    }

    public AvdthReferentialImportResult getReferential() {
        return referential;
    }

    public SpeciesCache getSpeciesCache() {
        return getReferential().getSpeciesCache();
    }

    public Path getMessageFile() {
        return messageFile;
    }

    public SqlScriptWriter newWriter(String name) {
        lastFile = configuration.getScriptTemporaryDirectory().resolve(name + ".sql");
        temporaryFiles.add(lastFile);
        SqlScriptWriter writer = SqlScriptWriter.of(lastFile);
        writers.forEach(w -> w.setWriter(writer));
        return writer;
    }

    public void addMessage(String message) {
        try {
            messageCount++;
            messageWriter.newLine();
            messageWriter.write(message);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't write message: %s", message), e);
        }
    }

    public int getMessageCount() {
        return messageCount;
    }

    public Set<Path> getTemporaryFiles() {
        return temporaryFiles;
    }

    public ObserveTopiaApplicationContext getPersistenceApplicationContext() {
        return persistenceApplicationContext;
    }

    public void flush(String primaryKeyString, SqlScriptWriter sqlWriter) throws IOException {
        long time0 = TimeLog.getTime();
        sqlWriter.flush();
        timeLog.log(time0, "Export trip", primaryKeyString);
        if (configuration.isImportInDatasource()) {
            time0 = TimeLog.getTime();
            try (ObserveTopiaPersistenceContext persistenceContext = getPersistenceApplicationContext().newPersistenceContext()) {
                persistenceContext.executeSqlScript(SqlScript.of(getLastFile()));
                persistenceContext.commit();
            }
            timeLog.log(time0, "Persist trip", primaryKeyString);
        }
    }

    public void prepare() throws IOException, SQLException, MissingReferentialException {
        long time0 = TimeLog.getTime();
        AvdthReferentialImportResult referential = new AvdthReferentialBuilder(configuration.toReferentialImportConfiguration()).build(conn, getPersistenceApplicationContext());
        setReferential(referential);
        timeLog.log(time0, "Load referential");
        Set<String> missingReferentialMessages = referential.getMissingReferentialMessages();
        if (!missingReferentialMessages.isEmpty()) {
            StringBuilder builder = new StringBuilder(String.format("Il manque %d référentiel(s) dans ObServe :\n", missingReferentialMessages.size()));
            missingReferentialMessages.forEach(m -> builder.append(m).append("\n"));
            String message = builder.toString();
            throw new MissingReferentialException(message);
        }
        configuration.incrementsProgression("Préparation des réferentiels");

        context.prepare(this, referential);

        time0 = TimeLog.getTime();
        try (DataTables tables = new DataTables(getConn())) {
            tables.tableReaders.forEach(this::countTable);
            tablesCount = tables.getTablesCount();
            tablesReadCount = tables.getTablesReadCount();
            this.notReadResult = new LinkedHashMap<>();
            this.tablesReadCount.forEach((k, v) -> {
                int notRead = tablesCount.get(k) - v;
                if (notRead > 0) {
                    notReadResult.put(k, notRead);
                }
            });
            timeLog.log(time0, "Count data");
        }
    }

    public abstract void read(DataTables tables);

    protected boolean isPrepareNotValid() {
        return !notReadResult.isEmpty() || messageCount > 0;
    }

    public void read() {
        if (!configuration.isForceImport() && isPrepareNotValid()) {
            log.warn("Can't perform read step, prepare step is not valid.");
            return;
        }
        long time0 = TimeLog.getTime();
        try (DataTables tables = new DataTables(getConn())) {
            @SuppressWarnings("SpellCheckingInspection") int tripCount = tablesReadCount.get("MAREE");
            getConfiguration().incrementsProgression(String.format("%d marée(s) detectée(s).", tripCount));
            getConfiguration().getProgressionModel().setValue(0);
            getConfiguration().getProgressionModel().setMaximum(tripCount);
            read(tables);
            timeLog.log(time0, "Load data");
        }
        Set<String> badSpecies = catchWriter.getBadSpecies();
        if (!badSpecies.isEmpty()) {
            log.error(String.format("Need to add %d species (for catch):\n\t %s", badSpecies.size(), String.join("\n\t", badSpecies)));
        }
        badSpecies = sampleSpeciesWriter.getBadSpecies();
        if (!badSpecies.isEmpty()) {
            log.error(String.format("Need to add %d species (for logbook sampleSpeciesMeasure):\n\t %s", badSpecies.size(), String.join("\n\t", badSpecies)));
        }
        badSpecies = localMarketSampleSpeciesWriter.getBadSpecies();
        if (!badSpecies.isEmpty()) {
            log.error(String.format("Need to add %d species (for local market sampleSpeciesMeasure):\n\t %s", badSpecies.size(), String.join("\n\t", badSpecies)));
        }
        badSpecies = localMarketBatchWriter.getBadSpecies();
        if (!badSpecies.isEmpty()) {
            log.error(String.format("Need to add %d species (for local market batch):\n\t %s", badSpecies.size(), String.join("\n\t", badSpecies)));
        }
        badSpecies = localMarketSurveyWriter.getBadSpecies();
        if (!badSpecies.isEmpty()) {
            log.error(String.format("Need to add %d species (for local market survey):\n\t %s", badSpecies.size(), String.join("\n\t", badSpecies)));
        }
        badSpecies = wellWriter.getBadSpecies();
        if (!badSpecies.isEmpty()) {
            log.error(String.format("Need to add %d species (for wellPlan):\n\t %s", badSpecies.size(), String.join("\n\t", badSpecies)));
        }
        badSpecies = landingWriter.getBadSpecies();
        if (!badSpecies.isEmpty()) {
            log.error(String.format("Need to add %d species (for landing):\n\t %s", badSpecies.size(), String.join("\n\t", badSpecies)));
        }
    }

    protected void countTable(DataQuery table) {
        getConfiguration().incrementsProgression(String.format("Lecture de la table %s en cours...", table.from()));
        table.count(getConn());
        int tableCount = table.tableCount();
        int tableReadCount = table.tableReadCount();
        if (tableCount > tableReadCount) {
            // missing some rows
            addMessage(String.format("ERROR: Sur la table %s, il existe %d ligne(s) non lue(s).", table.from(), tableCount - tableReadCount));
            List<Object[]> missingPrimaryKeys = Objects.requireNonNull(table.getMissingPrimaryKeys());
            int index = 1;
            int size = missingPrimaryKeys.size();
            for (Object[] primaryKey : missingPrimaryKeys) {
                addMessage(String.format("%5d/%5d - %s", (index++), size, Arrays.toString(primaryKey)));
            }
        }
        getConfiguration().incrementsProgression(String.format("Lecture de la table %s terminée (%d entrée(s) existantes - %d entrée(s) lue(s))", table.from(), tableCount, tableReadCount));
    }

    public Activity createMissingActivityForSampleActivity(ResultSet resultSet, List<Object> activityPrimaryKey, String activityPk) throws SQLException {
        Date routeDate = (Date) activityPrimaryKey.get(0);
        int activityNumber = (int) activityPrimaryKey.get(1);
        String routeId = context.getRouteDateToId().get(routeDate + "");
        if (routeId == null) {
            // create route
            Route route = routeReader.create(routeDate);
            log.debug(String.format("Create new route (for sample): %s", routeDate));
            routeWriter.write(route, context.getTrip().getTopiaId(), 0);
            context.setRoute(route);
            routeId = route.getTopiaId();
        }
        Activity activity = activityReader.createFromSampleActivity(context, resultSet, activityNumber);
        log.debug(String.format("Create new activity (for sample): %s-%s", routeDate, activityNumber));
        activityWriter.write(activity, routeId, 0);
        activityWriter.writeObservedSystems(activity);

        context.setActivity(activity, activityPk);
        return activity;
    }

    protected void fixLogbookAcquisitionStatus(Trip trip, String tripId) {
        if (!trip.isLogbookFilled() && !context.getActivityPkToId().isEmpty()) {
            // fix logbookAcquisitionStatus
            trip.setLogbookAcquisitionStatus(context.getAcquisitionStatus1());
            tripWriter.writeAcquisitionStatus(tripId, Trip.PROPERTY_LOGBOOK_ACQUISITION_STATUS);
        }
    }

}
