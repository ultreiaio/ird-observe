package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.data.ps.localmarket.Survey;
import fr.ird.observe.entities.data.ps.localmarket.SurveyPart;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.data.ps.logbook.Route;
import fr.ird.observe.entities.data.ps.logbook.Sample;
import fr.ird.observe.entities.data.ps.logbook.SampleActivity;
import fr.ird.observe.entities.data.ps.logbook.SampleSpecies;
import fr.ird.observe.entities.data.ps.logbook.Well;
import fr.ird.observe.entities.data.ps.logbook.WellActivity;
import fr.ird.observe.entities.data.ps.logbook.WellActivitySpecies;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.persistence.avdth.data.common.TripReader;
import fr.ird.observe.persistence.avdth.data.logbook.ActivityReader;
import fr.ird.observe.services.service.data.ps.AvdthDataImportConfiguration;
import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * Created on 30/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ImportEngineExecution extends ImportEngine {
    private static final Logger log = LogManager.getLogger(ImportEngineExecution.class);

    private DataTables tables;

    public ImportEngineExecution(AvdthDataImportConfiguration context, ObserveTopiaApplicationContext persistenceApplicationContext) throws SQLException, IOException {
        super(context, persistenceApplicationContext);
    }

    @Override
    public void read(DataTables tables) {
        this.tables = tables;
        while (tables.tripTableReader.hasNext()) {
            ResultSet tripRow = tables.tripTableReader.next();
            List<Object> tripPk = tables.tripTableReader.lastPrimaryKey();
            String primaryKeyString = TripReader.lastPrimaryKeyString(tripPk);
            getConfiguration().incrementsProgression(String.format("Lecture de la marée: %s", tripPk));
            long time0 = TimeLog.getTime();
            try (SqlScriptWriter sqlWriter = newWriter(primaryKeyString)) {

                Trip trip = tripWriter.intercept(tripRow, null);
                context.setTrip(trip);
                String tripId = trip.getTopiaId();

                // landing
                loadLanding(tripId, tripPk);

                // route - activity - dcp - catch
                loadRoute(tripId, tripPk);

                fixLogbookAcquisitionStatus(trip, tripId);
                context.computeCanCreateActivity();

                // sample
                loadSample(tripId, tripPk);

                // wellPlan
                loadWellPlan(tripId, tripPk);

                if (context.isCanCreateActivity()) {
                    // maybe some new activities?
                    fixLogbookAcquisitionStatus(trip, tripId);
                }

                // local market sample
                loadLocalmarketSample(tripId, tripPk);

                // local market batch
                loadLocalmarketBatch(tripId, tripPk);

                // local market survey
                loadLocalmarketSurvey(tripId, tripPk);

                timeLog.log(time0, "Read trip", primaryKeyString);
                flush(primaryKeyString, sqlWriter);
            } catch (SQLException | IOException e) {
                throw new IllegalStateException(e);
            }
        }
        tablesRead = tables.getTablesRead();
    }

    protected void loadLanding(String tripId, List<Object> tripPk) throws SQLException {
        int landingCount = 0;
        tables.landingTableReader.setPrimaryKey(tripPk);
        while (tables.landingTableReader.hasNext()) {
            ResultSet landingRow = tables.landingTableReader.next();
            landingWriter.intercept(landingRow, tripId, landingCount++);
        }
        if (landingCount > 0) {
            // found some landing, update landingAcquisitionStatus
            tripWriter.writeAcquisitionStatus(tripId, Trip.PROPERTY_LANDING_ACQUISITION_STATUS);
        }
    }

    protected void loadRoute(String tripId, List<Object> tripPk) throws SQLException {
        tables.activityTableReader.setPrimaryKey(tripPk);
        while (tables.activityTableReader.hasNext()) {
            ResultSet activityRow = tables.activityTableReader.next();
            Route route = routeWriter.intercept(activityRow, tripId);
            context.setRoute(route);
            List<Object> routePk = ActivityReader.getRoutePk(activityRow);
            loadActivity(route, activityRow, routePk);

            routeWriter.writeExtra(route);

            // search next route
            tables.activityTableReader.setPrimaryKey(tripPk);
        }
    }

    protected void loadActivity(Route route, ResultSet routeRow, List<Object> routePk) throws SQLException {

        String routeId = route.getTopiaId();
        // set route pk, so we will get all activities on this day
        tables.activityTableReader.setPrimaryKey(routePk);
        // the route row is the very first activity row
        ResultSet activityRow = routeRow;
        while (activityRow != null) {

            // load observed systems for this activity
            Set<String> observedSystemCodes = activityReader.readObservedSystem(context, tables.activityObservedSystemTableReader, ActivityReader.getActivityPk(activityRow));
            context.setAvdthObservedSystemCodes(observedSystemCodes);

            // load activity
            Activity activity = activityWriter.intercept(activityRow, routeId, 0);
            String activityId = activity.getTopiaId();
            String activityPk = Query.primaryKeyString(ActivityReader.getSimpleActivityPk(activityRow));
            context.setActivity(activity, activityPk);

            // load optional floating object
            floatingObjectWriter.intercept(activityRow, activityId);

            // add extra to route (fishingTime, timeAtSea)
            routeReader.readExtra(route, activityRow);

            // load activity catches
            loadCatch(activity, activityId, tables.activityTableReader.lastPrimaryKey());

            // add default observed system (if none found) and sanitize some observed system coming from DCP
            context.addDefaultObservedSystemOrSanitizeFloatingObjectOnes(activity);
            // clean this data context state
            context.setAvdthObservedSystemCodes(null);
            // write activity observed systems
            activityWriter.writeObservedSystems(activity);
            // while loading Floating objects, we may have change the vessel activity of the activity
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2612
            activityWriter.updateVesselActivity(activity);

            // get another activity ?
            if (tables.activityTableReader.hasNext()) {
                // still got an activity on this day
                activityRow = tables.activityTableReader.next();
            } else {
                // no more activity to read
                activityRow = null;
            }
        }
    }

    protected void loadCatch(Activity activity, String activityId, List<Object> activityPrimaryKey) throws SQLException {
        int catchesCount = 0;
        tables.catchTableReader.setPrimaryKey(activityPrimaryKey);
        boolean baitsFishingOnly = false;
        while (tables.catchTableReader.hasNext()) {
            ResultSet catchRow = tables.catchTableReader.next();
            int speciesCode = catchRow.getInt(6);
            if (speciesCode >= 700 && speciesCode < 720) {
                baitsFishingOnly = true;
            }
            catchWriter.intercept(catchRow, activityId, catchesCount++);
        }
        if (baitsFishingOnly) {
            // See #1883
            context.addBaitOnlyObservedSystem(activity);
        }
    }

    protected void loadSample(String tripId, List<Object> tripPk) throws SQLException {
        tables.sampleTableReader.setPrimaryKey(tripPk);
        while (tables.sampleTableReader.hasNext()) {
            ResultSet sampleRow = tables.sampleTableReader.next();
            List<Object> samplePk = tables.sampleTableReader.lastPrimaryKey();
            Sample sample = sampleWriter.intercept(sampleRow, tripId);
            loadSampleSpecies(sample.getTopiaId(), samplePk);
        }
    }

    protected void loadSampleSpecies(String sampleId, List<Object> samplePk) throws SQLException {
        tables.sampleSpeciesTableReader.setPrimaryKey(samplePk);
        int sampleCount = 0;
        while (tables.sampleSpeciesTableReader.hasNext()) {
            ResultSet sampleSpeciesRow = tables.sampleSpeciesTableReader.next();
            List<Object> sampleSpeciesPk = tables.sampleSpeciesTableReader.lastPrimaryKey();
            SampleSpecies sampleSpecies = sampleSpeciesWriter.intercept(sampleSpeciesRow, sampleId, sampleCount++);
            loadSampleSpeciesMeasure(sampleSpecies.getTopiaId(), sampleSpeciesPk);
            loadSampleActivity(sampleId, samplePk);
        }
    }

    protected void loadSampleSpeciesMeasure(String sampleSpeciesId, List<Object> sampleSpeciesPk) throws SQLException {
        tables.sampleSpeciesMeasureTableReader.setPrimaryKey(sampleSpeciesPk);
        while (tables.sampleSpeciesMeasureTableReader.hasNext()) {
            ResultSet sampleSpeciesMeasureRow = tables.sampleSpeciesMeasureTableReader.next();
            sampleSpeciesMeasureWriter.intercept(sampleSpeciesMeasureRow, sampleSpeciesId);
        }
    }

    protected void loadSampleActivity(String sampleId, List<Object> samplePk) throws SQLException {
        tables.sampleActivityTableReader.setPrimaryKey(samplePk);
        while (tables.sampleActivityTableReader.hasNext()) {
            ResultSet sampleSetRow = tables.sampleActivityTableReader.next();
            SampleActivity sampleActivity = sampleActivityReader.read(context, sampleSetRow);
            if (sampleActivity != null) {
                sampleActivityWriter.write(sampleActivity, sampleId, 0);
            }
        }
    }

    protected void loadWellPlan(String tripId, List<Object> tripPk) throws SQLException {
        tables.wellTableReader.setPrimaryKey(tripPk);
        boolean withWell = false;
        while (tables.wellTableReader.hasNext()) {
            ResultSet wellRow = tables.wellTableReader.next();
            Well well = wellWriter.intercept(wellRow, tripId);
            String wellId = well.getTopiaId();
            withWell = true;
            tables.wellActivityTableReader.setPrimaryKey(tables.wellTableReader.lastPrimaryKey());
            while (tables.wellActivityTableReader.hasNext()) {
                ResultSet wellActivityRow = tables.wellActivityTableReader.next();
                WellActivity wellActivity = wellActivityReader.read(context, wellActivityRow);
                if (wellActivity != null) {
                    // get the wellActivitySpecies on this row
                    WellActivitySpecies wellActivitySpecies = wellActivityReader.readExtra(context, wellActivityRow);
                    wellActivity.addWellActivitySpecies(wellActivitySpecies);
                    // to stay on this wellActivity (and get other next wellActivitySpecies)
                    tables.wellActivityTableReader.setPrimaryKey(tables.wellActivityTableReader.getPrimaryKey(6));
                    while (tables.wellActivityTableReader.hasNext()) {
                        wellActivitySpecies = wellActivityReader.readExtra(context, wellActivityRow);
                        wellActivity.addWellActivitySpecies(wellActivitySpecies);
                    }
                    wellActivityWriter.write(wellActivity, wellId, 0);
                    // to get other next wellActivity on this well
                    tables.wellActivityTableReader.setPrimaryKey(tables.wellTableReader.lastPrimaryKey());
                }
            }
        }
        if (withWell) {
            // found some well, update targetWellsSamplingAcquisitionStatus
            tripWriter.writeAcquisitionStatus(tripId, Trip.PROPERTY_TARGET_WELLS_SAMPLING_ACQUISITION_STATUS);
        }
    }

    protected void loadLocalmarketSample(String tripId, List<Object> tripPk) throws SQLException {
        tables.localMarketSampleTableReader.setPrimaryKey(tripPk);
        int sampleCount = 0;
        while (tables.localMarketSampleTableReader.hasNext()) {
            ResultSet sampleRow = tables.localMarketSampleTableReader.next();
            List<Object> samplePk = tables.localMarketSampleTableReader.lastPrimaryKey();
            fr.ird.observe.entities.data.ps.localmarket.Sample sample = localMarketSampleWriter.intercept(sampleRow, tripId);
            boolean foundOne = loadLocalmarketSampleSpecies(sample.getTopiaId(), samplePk);
            if (!foundOne) {
                log.warn(String.format("Remove not used sample: %s", samplePk));
                localMarketSampleWriter.delete(sample.getTopiaId());
            } else {
                sampleCount++;
            }
        }
        if (sampleCount > 0) {
            // found some local market sample, update localMarketWellsSamplingAcquisitionStatus
            tripWriter.writeAcquisitionStatus(tripId, Trip.PROPERTY_LOCAL_MARKET_WELLS_SAMPLING_ACQUISITION_STATUS);
        }
    }

    protected boolean loadLocalmarketSampleSpecies(String sampleId, List<Object> samplePk) throws SQLException {
        tables.localMarketSampleSpeciesTableReader.setPrimaryKey(samplePk);
        boolean foundOne = false;
        while (tables.localMarketSampleSpeciesTableReader.hasNext()) {
            ResultSet sampleSpeciesRow = tables.localMarketSampleSpeciesTableReader.next();
            List<Object> sampleSpeciesPk = tables.localMarketSampleSpeciesTableReader.lastPrimaryKey();
            fr.ird.observe.entities.data.ps.localmarket.SampleSpecies sampleSpecies = localMarketSampleSpeciesWriter.intercept(sampleSpeciesRow, sampleId);
            loadLocalmarketSampleSpeciesMeasure(sampleSpecies.getTopiaId(), sampleSpeciesPk);
            loadLocalmarketSampleWell(sampleId, samplePk);
            foundOne = true;
        }
        return foundOne;
    }

    protected void loadLocalmarketSampleWell(String sampleId, List<Object> samplePk) throws SQLException {
        tables.localMarketSampleWellTableReader.setPrimaryKey(samplePk);
        while (tables.localMarketSampleWellTableReader.hasNext()) {
            ResultSet sampleWellRow = tables.localMarketSampleWellTableReader.next();
            localMarketSampleWriter.writeExtra(sampleId, sampleWellRow);
        }
    }

    protected void loadLocalmarketSampleSpeciesMeasure(String sampleSpeciesId, List<Object> sampleSpeciesPk) throws SQLException {
        tables.localMarketSampleSpeciesMeasureTableReader.setPrimaryKey(sampleSpeciesPk);
        while (tables.localMarketSampleSpeciesMeasureTableReader.hasNext()) {
            ResultSet sampleSpeciesMeasureRow = tables.localMarketSampleSpeciesMeasureTableReader.next();
            localMarketSampleSpeciesMeasureWriter.intercept(sampleSpeciesMeasureRow, sampleSpeciesId);
        }
    }

    protected void loadLocalmarketBatch(String tripId, List<Object> tripPk) throws SQLException {
        tables.localMarketBatchTableReader.setPrimaryKey(tripPk);
        boolean withBatch = false;
        while (tables.localMarketBatchTableReader.hasNext()) {
            ResultSet batchRow = tables.localMarketBatchTableReader.next();
            localMarketBatchWriter.intercept(batchRow, tripId);
            withBatch = true;
        }
        if (withBatch) {
            // found some local market batch, update localMarketAcquisitionStatus
            tripWriter.writeAcquisitionStatus(tripId, Trip.PROPERTY_LOCAL_MARKET_ACQUISITION_STATUS);
        }
    }

    protected void loadLocalmarketSurvey(String tripId, List<Object> tripPk) throws SQLException {
        tables.localMarketSurveyTableReader.setPrimaryKey(tripPk);
        boolean withSurvey = false;
        while (tables.localMarketSurveyTableReader.hasNext()) {
            // found a new survey on this trip
            withSurvey = true;
            ResultSet surveyRow = tables.localMarketSurveyTableReader.next();
            Survey survey = localMarketSurveyWriter.intercept(surveyRow, tripId);
            String surveyId = survey.getTopiaId();
            // let's seek now all row with our primary key on survey
            tables.localMarketSurveyTableReader.setPrimaryKey(tables.localMarketSurveyTableReader.lastPrimaryKey());
            do {
                SurveyPart surveyPart = localMarketSurveyReader.readExtra(context, surveyRow);
                localMarketSurveyWriter.writeExtra(surveyPart, surveyId);
                if (tables.localMarketSurveyTableReader.hasNext()) {
                    // still a part to register
                    surveyRow = tables.localMarketSurveyTableReader.next();
                } else {
                    // no more part, search back to tripPk
                    tables.localMarketSurveyTableReader.setPrimaryKey(tripPk);
                    break;
                }
            } while (true);
        }
        if (withSurvey) {
            // found some local market survey, update localMarketSurveySamplingAcquisitionStatus
            tripWriter.writeAcquisitionStatus(tripId, Trip.PROPERTY_LOCAL_MARKET_SURVEY_SAMPLING_ACQUISITION_STATUS);
        }
    }
}
