package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.referential.common.DataQuality;
import fr.ird.observe.entities.referential.common.FpaZone;
import fr.ird.observe.entities.referential.common.Harbour;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.Person;
import fr.ird.observe.entities.referential.common.SizeMeasureType;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.common.Vessel;
import fr.ird.observe.entities.referential.common.WeightMeasureMethod;
import fr.ird.observe.entities.referential.common.Wind;
import fr.ird.observe.entities.referential.common.WindImpl;
import fr.ird.observe.entities.referential.ps.common.AcquisitionStatus;
import fr.ird.observe.entities.referential.ps.common.ObjectMaterial;
import fr.ird.observe.entities.referential.ps.common.ObjectOperation;
import fr.ird.observe.entities.referential.ps.common.ObservedSystem;
import fr.ird.observe.entities.referential.ps.common.Program;
import fr.ird.observe.entities.referential.ps.common.ReasonForNullSet;
import fr.ird.observe.entities.referential.ps.common.SampleType;
import fr.ird.observe.entities.referential.ps.common.SchoolType;
import fr.ird.observe.entities.referential.ps.common.SpeciesFate;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOperation;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOwnership;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyType;
import fr.ird.observe.entities.referential.ps.common.VesselActivity;
import fr.ird.observe.entities.referential.ps.common.WeightCategory;
import fr.ird.observe.entities.referential.ps.landing.Destination;
import fr.ird.observe.entities.referential.ps.landing.Fate;
import fr.ird.observe.entities.referential.ps.localmarket.Packaging;
import fr.ird.observe.entities.referential.ps.logbook.SampleQuality;
import fr.ird.observe.entities.referential.ps.logbook.SetSuccessStatus;
import fr.ird.observe.entities.referential.ps.logbook.WellContentStatus;
import fr.ird.observe.entities.referential.ps.logbook.WellSamplingConformity;
import fr.ird.observe.entities.referential.ps.logbook.WellSamplingStatus;
import fr.ird.observe.persistence.avdth.data.logbook.WellReader;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.SpeciesCache;
import fr.ird.observe.persistence.avdth.referential.interceptors.SchoolTypeInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.VesselActivityInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.WeightCategoryInterceptor;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Contains all referential used in import and all the logic of them.
 * <p>
 * Created on 28/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ImportReferentialContext {

    private Map<String, WeightCategory> landingCategories;
    private Map<String, Destination> destinations;
    private Map<String, WeightCategory> wellCategories;
    private Map<String, WellSamplingConformity> wellSamplingConformity;
    private Map<String, WellSamplingStatus> wellSamplingStatus;
    private Map<String, WeightCategory> catchCategories;
    private Map<String, Harbour> harbour;
    private Map<String, Vessel> vessel;
    private Map<String, WellContentStatus> wellContentStatus;
    private Map<String, VesselActivity> vesselActivity;
    private Map<String, SchoolType> schoolType;
    private Map<String, FpaZone> fpaZone;
    private List<Wind> wind;
    private Map<String, ObservedSystem> observedSystem;
    private Map<String, ObjectOperation> objectOperation;
    private Map<String, TransmittingBuoyOperation> transmittingBuoyOperation;
    private Map<String, TransmittingBuoyOwnership> transmittingBuoyOwnership;
    private Map<String, TransmittingBuoyType> transmittingBuoyType;
    private Map<String, SampleQuality> sampleQuality;
    private Map<String, SampleType> sampleType;
    private Program tripLogbookProgram;
    private Ocean tripOcean;
    private Map<String, Packaging> packaging;
    private SampleType localmarketSampleType;
    private WeightMeasureMethod weightMeasureMethod;
    private Person dataEntryOperator;
    private SpeciesFate speciesFate6;
    private SpeciesFate speciesFate11;
    private SizeMeasureType sizeMeasureTypeFL;
    private SizeMeasureType sizeMeasureTypeDL;
    private DataQuality dataQualityA;
    private DataQuality dataQualityE;
    private Fate fateProcessed;
    private Fate fateDiscarded;
    private Fate fateSoldOnLocalMarket;
    private Fate fateTransshipment;
    private AcquisitionStatus acquisitionStatus1;
    private AcquisitionStatus acquisitionStatus999;
    private TransmittingBuoyType transmittingBuoyType98;
    private TransmittingBuoyOperation transmittingBuoyOperation1;
    private ObjectMaterial objectMaterialFOB;
    private ObjectMaterial objectMaterialNLOG;
    private ObjectMaterial objectMaterialDFAD;
    private ObjectMaterial objectMaterialALOG;
    private ObjectMaterial objectMaterialAFAD;
    private ObjectMaterial objectMaterialBiodegradable;
    private ObjectMaterial objectMaterialAlone;
    private ObjectMaterial objectMaterialLOG;
    private ObjectMaterial objectMaterialFALOG;
    private ObjectMaterial objectMaterialANLOG;
    private ObjectMaterial objectMaterialHALOG;
    private ObjectMaterial objectMaterialVNLOG;
    private ObjectMaterial objectMaterialCarrion;
    private ObservedSystem observedSystem0;
    private SpeciesCache speciesCache;
    private ObservedSystem observedSystem20;
    private ObservedSystem observedSystem102;
    private ObservedSystem observedSystem103;
    private ObservedSystem observedSystem110;
    private VesselActivity vesselActivity52;
    private VesselActivity vesselActivity101;
    private VesselActivity vesselActivity102;
    private SchoolType schoolType0;
    private SchoolType schoolType1;
    private SchoolType schoolType2;
    private SetSuccessStatus setSuccessStatus0;
    private SetSuccessStatus setSuccessStatus1;
    private SetSuccessStatus setSuccessStatus2;
    private ReasonForNullSet reasonForNullSet0;

    public void prepare(ImportEngine importEngine, AvdthReferentialImportResult referential) {

        // common
        speciesCache = referential.getSpeciesCache();
        speciesCache.init(Maps.uniqueIndex(referential.getSpecies(), Species::getFaoCode));

        Map<String, SizeMeasureType> sizeMeasureType = Maps.uniqueIndex(referential.getSizeMeasureType(), SizeMeasureType::getId);
        sizeMeasureTypeFL = sizeMeasureType.get("fr.ird.referential.common.SizeMeasureType#1433499465700#0.0902433863375336"); // FL
        sizeMeasureTypeDL = sizeMeasureType.get("fr.ird.referential.common.SizeMeasureType#1433499466774#0.529249255312607"); // PD1

        Map<String, DataQuality> dataQuality = Maps.uniqueIndex(referential.getDataQuality(), DataQuality::getId);
        dataQualityA = dataQuality.get("fr.ird.referential.common.DataQuality#0#1"); // A
        dataQualityE = dataQuality.get("fr.ird.referential.common.DataQuality#0#5"); // E

        // trip
        Map<String, Ocean> ocean = Maps.uniqueIndex(referential.getOcean(), Ocean::getId);
        String oceanId = importEngine.getConfiguration().getOceanId();
        if (tripOcean == null) {
            tripOcean = Objects.requireNonNull(ocean.get(oceanId), String.format("Can't find ocean with id: %s", oceanId));
        }
        Map<String, Program> program = Maps.uniqueIndex(referential.getProgram(), Program::getId);
        String programId = importEngine.getConfiguration().getProgramId();
        if (tripLogbookProgram == null) {
            tripLogbookProgram = Objects.requireNonNull(program.get(programId), String.format("Can't find program with id: %s", programId));
        }
        harbour = Maps.uniqueIndex(referential.getHarbour(), Harbour::getCode);
        vessel = Maps.uniqueIndex(referential.getVessel(), Vessel::getCode);
        wellContentStatus = Maps.uniqueIndex(referential.getWellContentStatus(), WellContentStatus::getCode);

        Map<String, AcquisitionStatus> acquisitionStatus = Maps.uniqueIndex(referential.getAcquisitionStatus(), AcquisitionStatus::getId);
        acquisitionStatus1 = acquisitionStatus.get("fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#001"); // 1
        acquisitionStatus999 = acquisitionStatus.get("fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#999"); //99
        // landing
        landingCategories = Maps.uniqueIndex(referential.getWeightCategory(), WeightCategory::getCode);

        Map<String, Fate> fates = Maps.uniqueIndex(referential.getFate(), Fate::getCode);
        fateProcessed = fates.get("1");
        fateDiscarded = fates.get("2");
        fateSoldOnLocalMarket = fates.get("3");
        fateTransshipment = fates.get("4");

        destinations = Maps.uniqueIndex(referential.getDestination(), Destination::getCode);

        // activity
        vesselActivity = Maps.uniqueIndex(referential.getVesselActivity(), VesselActivity::getCode);
        vesselActivity52 = vesselActivity.get("52");
        vesselActivity101 = vesselActivity.get("101");
        vesselActivity102 = vesselActivity.get("102");
        schoolType = Maps.uniqueIndex(referential.getSchoolType(), SchoolType::getCode);
        schoolType0 = schoolType.get("0");
        schoolType1 = schoolType.get("1");
        schoolType2 = schoolType.get("2");
        ImmutableMap<String, SetSuccessStatus> setSuccessStatus = Maps.uniqueIndex(referential.getSetSuccessStatus(), SetSuccessStatus::getCode);
        setSuccessStatus0 = setSuccessStatus.get("0");
        setSuccessStatus1 = setSuccessStatus.get("1");
        setSuccessStatus2 = setSuccessStatus.get("2");
        ImmutableMap<String, ReasonForNullSet> reasonForNullSet = Maps.uniqueIndex(referential.getReasonForNullSet(), ReasonForNullSet::getCode);
        reasonForNullSet0 = reasonForNullSet.get("0");


        fpaZone = Maps.uniqueIndex(referential.getFpaZone(), FpaZone::getHomeId);
        wind = referential.getWind();
        observedSystem = Maps.uniqueIndex(referential.getObservedSystem(), ObservedSystem::getCode);
        observedSystem0 = observedSystem.get("0");
        observedSystem20 = observedSystem.get("20");
        observedSystem102 = observedSystem.get("102");
        observedSystem103 = observedSystem.get("103");
        observedSystem110 = observedSystem.get("110");

        // dcp
        objectOperation = Maps.uniqueIndex(referential.getObjectOperation(), ObjectOperation::getCode);
        Map<String, ObjectMaterial> objectMaterial = Maps.uniqueIndex(referential.getObjectMaterial(), ObjectMaterial::getTopiaId);
        transmittingBuoyOperation = Maps.uniqueIndex(referential.getTransmittingBuoyOperation(), TransmittingBuoyOperation::getCode);
        transmittingBuoyOperation1 = transmittingBuoyOperation.get("1");
        transmittingBuoyOwnership = Maps.uniqueIndex(referential.getTransmittingBuoyOwnership(), TransmittingBuoyOwnership::getCode);
        transmittingBuoyType = Maps.uniqueIndex(referential.getTransmittingBuoyType(), TransmittingBuoyType::getCode);
        transmittingBuoyType98 = transmittingBuoyType.get("98");
        objectMaterialFOB = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#1.3");
        objectMaterialNLOG = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.51");
        objectMaterialCarrion = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.57");
        objectMaterialDFAD = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.2");
        objectMaterialALOG = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.59");
        objectMaterialAFAD = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.46");
        objectMaterialLOG = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.50");
        objectMaterialFALOG = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.63");
        objectMaterialHALOG = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.67");
        objectMaterialANLOG = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.56");
        objectMaterialVNLOG = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.52");
        objectMaterialAlone = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#1561561977652#0.5876332198776647");
        objectMaterialBiodegradable = objectMaterial.get("fr.ird.referential.ps.common.ObjectMaterial#0#0.69");

        // catch
        catchCategories = new TreeMap<>();
        for (WeightCategory category : referential.getWeightCategory()) {
            String categoryCode = category.getCode();
            if (categoryCode != null && categoryCode.startsWith("C-")) {
                catchCategories.put(categoryCode, category);
            }
        }
        Map<String, SpeciesFate> speciesFate = Maps.uniqueIndex(referential.getSpeciesFate(), SpeciesFate::getCode);
        speciesFate6 = speciesFate.get("6");
        speciesFate11 = speciesFate.get("11");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2092
        weightMeasureMethod = Maps.uniqueIndex(referential.getWeightMeasureMethod(), WeightMeasureMethod::getTopiaId).get("fr.ird.referential.common.WeightMeasureMethod#666#03");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2097
        dataEntryOperator = referential.getPerson().stream().filter(p -> p.getTopiaId().equals("fr.ird.referential.common.Person#1254317601353#0.6617065204572095")).findFirst().orElseThrow();

        // logbook sample
        sampleQuality = Maps.uniqueIndex(referential.getSampleQuality(), SampleQuality::getCode);
        sampleType = Maps.uniqueIndex(referential.getSampleType(), SampleType::getCode);

        // wellPlan
        wellCategories = new TreeMap<>();
        for (WeightCategory category : referential.getWeightCategory()) {
            String categoryCode = category.getCode();
            if (categoryCode != null && categoryCode.startsWith("W-")) {
                wellCategories.put(categoryCode, category);
            }
        }
        wellSamplingConformity = new TreeMap<>();
        for (WellSamplingConformity conformity : referential.getWellSamplingConformity()) {
            String code = conformity.getCode();
            if (WellReader.conformityMapping.containsValue(code)) {
                WellReader.conformityMapping.forEach((k, v) -> {
                    if (v.equals(code)) {
                        this.wellSamplingConformity.put(k, conformity);
                    }
                });
            }
        }
        wellSamplingStatus = new TreeMap<>();
        for (WellSamplingStatus status : referential.getWellSamplingStatus()) {
            String code = status.getCode();
            if (WellReader.statusMapping.containsValue(code)) {
                WellReader.statusMapping.forEach((k, v) -> {
                    if (v.equals(code)) {
                        wellSamplingStatus.put(k, status);
                    }
                });
            }
        }

        // localmarket batch
        packaging = Maps.uniqueIndex(referential.getPackaging(), Packaging::getCode);

        // localmarket sample
        localmarketSampleType = Objects.requireNonNull(Maps.uniqueIndex(referential.getSampleType(), SampleType::getCode).get("9"));
    }

    public SetSuccessStatus getSetSuccessStatus0() {
        return setSuccessStatus0;
    }

    public SetSuccessStatus getSetSuccessStatus1() {
        return setSuccessStatus1;
    }

    public SetSuccessStatus getSetSuccessStatus2() {
        return setSuccessStatus2;
    }

    public ReasonForNullSet getReasonForNullSet0() {
        return reasonForNullSet0;
    }

    public ObjectMaterial getObjectMaterialFOB() {
        return objectMaterialFOB;
    }

    public ObjectMaterial getObjectMaterialNLOG() {
        return objectMaterialNLOG;
    }

    public ObjectMaterial getObjectMaterialCarrion() {
        return objectMaterialCarrion;
    }

    public ObjectMaterial getObjectMaterialDFAD() {
        return objectMaterialDFAD;
    }

    public ObjectMaterial getObjectMaterialALOG() {
        return objectMaterialALOG;
    }

    public ObjectMaterial getObjectMaterialAFAD() {
        return objectMaterialAFAD;
    }

    public ObjectMaterial getObjectMaterialLOG() {
        return objectMaterialLOG;
    }

    public ObjectMaterial getObjectMaterialFALOG() {
        return objectMaterialFALOG;
    }

    public ObjectMaterial getObjectMaterialHALOG() {
        return objectMaterialHALOG;
    }

    public ObjectMaterial getObjectMaterialVNLOG() {
        return objectMaterialVNLOG;
    }

    public ObjectMaterial getObjectMaterialBiodegradable() {
        return objectMaterialBiodegradable;
    }

    public ObjectMaterial getObjectMaterialAlone() {
        return objectMaterialAlone;
    }

    public ObjectMaterial getObjectMaterialANLOG() {
        return objectMaterialANLOG;
    }

    public SampleType getLocalmarketSampleType() {
        return localmarketSampleType;
    }

    public Program getTripLogbookProgram() {
        return tripLogbookProgram;
    }

    public Ocean getTripOcean() {
        return tripOcean;
    }

    public SchoolType getSchoolType0() {
        return schoolType0;
    }

    public SchoolType getSchoolType1() {
        return schoolType1;
    }

    public SchoolType getSchoolType2() {
        return schoolType2;
    }

    public boolean isSpeciesDiscarded(String speciesCode) {
        return speciesCode.equals("8") || (speciesCode.startsWith("8") && speciesCode.length() == 3);
    }

    public SpeciesFate getSpeciesFate(String speciesCode) {
        if (isSpeciesDiscarded(speciesCode)) {
            // all species in avdth with code 8 or 8xx are discard
            return speciesFate11;
        }
        // otherwise use kept
        return speciesFate6;
    }


    public WeightMeasureMethod getWeightMeasureMethod() {
        return weightMeasureMethod;
    }

    public Person getDataEntryOperator() {
        return dataEntryOperator;
    }

    public final Species getSpecies(String originalSpeciesCode) {
        return speciesCache.translateSpecies(originalSpeciesCode);
    }

    public final SizeMeasureType getSizeMeasureType(int ldLf) {
        if (ldLf == 2) {
            return sizeMeasureTypeFL;
        }
        return sizeMeasureTypeDL;
    }

    public Packaging getPackaging(String code) {
        return packaging.get(code);
    }

    public WeightCategory getCatchWeightCategory(Species species, String weightCategoryCode) {
        return WeightCategoryInterceptor.getCatchWeightCategory(catchCategories, species, weightCategoryCode);
    }

    public WeightCategory getLandingWeightCategory(Species species, String weightCategoryCode) {
        return WeightCategoryInterceptor.getLandingWeightCategory(landingCategories, species, weightCategoryCode);
    }

    public WellSamplingConformity getWellSamplingConformity(String fate) {
        return Objects.requireNonNull(wellSamplingConformity.get(fate), "can't find wellSamplingConformity with code: " + fate);
    }

    public WellSamplingStatus getWellSamplingStatus(String fate) {
        return Objects.requireNonNull(wellSamplingStatus.get(fate), "can't find wellSamplingStatus with code: " + fate);
    }

    public SampleQuality getSampleQuality(String code) {
        return sampleQuality.get(code);
    }

    public SampleType getSampleType(String code) {
        return sampleType.get(code);
    }

    public TransmittingBuoyType getTransmittingBuoyType98() {
        return transmittingBuoyType98;
    }

    public TransmittingBuoyOperation getTransmittingBuoyOperation1() {
        return transmittingBuoyOperation1;
    }

    public Fate getFateProcessed() {
        return fateProcessed;
    }

    public Fate getFateDiscarded() {
        return fateDiscarded;
    }

    public Fate getFateSoldOnLocalMarket() {
        return fateSoldOnLocalMarket;
    }

    public Fate getFateTransshipment() {
        return fateTransshipment;
    }

    public Harbour getHarbour(String code) {
        return Objects.requireNonNull(harbour.get(code), String.format("Can't find Harbour code with code: %s", code));
    }

    public Vessel getVessel(String vesselCode) {
        return Objects.requireNonNull(vessel.get(vesselCode), String.format("Can't find vessel code with code: %s", vesselCode));
    }

    public DataQuality getDataQualityA() {
        return dataQualityA;
    }

    public DataQuality getDataQualityE() {
        return dataQualityE;
    }

    public AcquisitionStatus getAcquisitionStatus1() {
        return acquisitionStatus1;
    }

    public AcquisitionStatus getAcquisitionStatus999() {
        return acquisitionStatus999;
    }

    public WellContentStatus getWellContentStatus(String code) {
        if (code.equals("0")) {
            code = "2";
        }
        return wellContentStatus.get(code);
    }

    public Destination getDestination(String code) {
        if (code == null) {
            return null;
        }
        Destination destination = destinations.get(code);
        return Objects.requireNonNull(destination, String.format("Can't find destination for code: %s.", code));
    }

    public ObservedSystem getObservedSystem0() {
        return observedSystem0;
    }

    public ObservedSystem getObservedSystem102() {
        return observedSystem102;
    }

    public ObservedSystem getObservedSystem103() {
        return observedSystem103;
    }

    public ObservedSystem getObservedSystem110() {
        return observedSystem110;
    }

    public ObservedSystem getObservedSystem20() {
        return observedSystem20;
    }

    public ObservedSystem getObservedSystem(String observedSystemCode) {
        ObservedSystem result = observedSystem.get(observedSystemCode);
        return Objects.requireNonNull(result, String.format("Can't find observedSystem with code: %s from ad-hoc AVDTH to ObServe mapping", observedSystemCode));
    }

    public Wind getWind(int windSpeed) {
        for (Wind wind : wind) {
            if (WindImpl.accept(wind, windSpeed)) {
                return wind;
            }
        }
        throw new IllegalStateException(String.format("Can't find wind with speed: %s", windSpeed));
    }

    public VesselActivity getVesselActivity(String code) {
        String observeVesselActivityCode = VesselActivityInterceptor.getObserveCode(code);
        VesselActivity result = vesselActivity.get(observeVesselActivityCode);
        return Objects.requireNonNull(result, String.format("Can't find vessel activity wth code: %s", code));
    }

    public SchoolType getSchoolType(String code) {
        String schoolTypeCode = SchoolTypeInterceptor.CODE_MAPPING.get(code);
        return schoolType.get(schoolTypeCode);
    }

    public FpaZone getFpaZone(String code) {
        return code == null ? null : fpaZone.get(code);
    }

    public void addBaitOnlyObservedSystem(Activity entity) {
        entity.addObservedSystem(getObservedSystem102());
    }

    public ObjectOperation getObjectOperation(String objectOperationCode) {
        return objectOperation.get(objectOperationCode);
    }

    public TransmittingBuoyOperation getTransmittingBuoyOperation(String transmittingBuoyTypeCode) {
        if (transmittingBuoyTypeCode == null) {
            return null;
        }
        return transmittingBuoyOperation.get(transmittingBuoyTypeCode);
    }

    public TransmittingBuoyType getTransmittingBuoyType(String transmittingBuoyTypeCode) {
        TransmittingBuoyType result = transmittingBuoyType.get(transmittingBuoyTypeCode);
        return Objects.requireNonNull(result, String.format("Can't find TransmittingBuoyType with code: %s", transmittingBuoyTypeCode));
    }

    public TransmittingBuoyOwnership getTransmittingBuoyOwnership(String transmittingBuoyOwnershipCode) {
        if (transmittingBuoyOwnershipCode == null) {
            return null;
        }
        return transmittingBuoyOwnership.get(transmittingBuoyOwnershipCode);
    }

    public WeightCategory getWellWeightCategory(String weightCategoryCode) {
        String categoryCode = "W-" + weightCategoryCode;
        WeightCategory weightCategory = wellCategories.get(categoryCode);
        return Objects.requireNonNull(weightCategory, String.format("Can't find weight category for code: %s", categoryCode));
    }


    public VesselActivity getVesselActivity52() {
        return vesselActivity52;
    }

    public VesselActivity getVesselActivity101() {
        return vesselActivity101;
    }

    public VesselActivity getVesselActivity102() {
        return vesselActivity102;
    }
}
