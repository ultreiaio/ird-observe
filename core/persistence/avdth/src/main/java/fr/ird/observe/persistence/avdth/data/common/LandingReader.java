package fr.ird.observe.persistence.avdth.data.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.landing.Landing;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.ps.common.WeightCategory;
import fr.ird.observe.entities.referential.ps.landing.Destination;
import fr.ird.observe.entities.referential.ps.landing.Fate;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 02/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class LandingReader extends DataReader<Landing> {

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(DataQueryDefinition.class)
    public static class LandingTableReader extends DataQueryDefinition {

        /**
         * create table LOT_COM (
         * 1 C_BAT SMALLINT not null,
         * 2 D_DBQ TIMESTAMP not null,
         * 3 N_LOT INTEGER identity,
         * 4 C_ESP SMALLINT not null,
         * 5 C_CAT_C SMALLINT not null,
         * 6 V_POIDS_LC NUMERIC(100,7) not null,
         * 7 C_DEST SMALLINT
         * constraint LOT_COM_LOT_COM_DESTIN_LOT_COM references LOT_COM_DESTIN on update cascade,
         * primary key (C_BAT, D_DBQ, N_LOT),
         * constraint LOT_COM_CFK_CAT_COM_R_LOT_COM foreign key (C_ESP, C_CAT_C) references LOT_COM_DESTIN (C_ESP, C_CAT_C),
         * constraint LOT_COM_CFK_MAREE_R_LOT_COM foreign key (C_BAT, D_DBQ) references MAREE on update cascade );
         */
        public LandingTableReader() {
            super(3);
        }

        @Override
        public String select() {
            return  /* LOT_COM_01 */ " LOT_COM.C_BAT," +
                    /* LOT_COM_02 */ " LOT_COM.D_DBQ," +
                    /* LOT_COM_03 */ " LOT_COM.N_LOT," +
                    /* LOT_COM_04 */ " LOT_COM.C_ESP," +
                    /* LOT_COM_05 */ " LOT_COM.C_CAT_C," +
                    /* LOT_COM_05 */ " LOT_COM.V_POIDS_LC," +
                    /* LOT_COM_07 */ " LOT_COM.C_DEST";
        }

        @Override
        public String from() {
            return "LOT_COM";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_LOT, C_ESP, C_CAT_C";
        }
    }

    public LandingReader(ImportEngine context) {
        super(context);
    }

    @Override
    public Landing read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        Landing entity = newEntity(Landing.SPI);
        entity.setDate(resultSet.getDate(2));
        entity.setHomeId(resultSet.getString(3));
        String originalSpeciesCode = resultSet.getString(4);
        Species species = dataContext.getSpecies(originalSpeciesCode);
        entity.setSpecies(species);
        String weightCategoryCode = resultSet.getString(5);
        WeightCategory weightCategory = dataContext.getLandingWeightCategory(species, weightCategoryCode);
        entity.setWeightCategory(weightCategory);
        entity.setWeight(resultSet.getFloat(6));
        String destinationCode = resultSet.getString(7);
        Destination destination = dataContext.getDestination(destinationCode);
        entity.setDestination(destination);
        Fate fate = getFate(dataContext, originalSpeciesCode, destinationCode);
        entity.setFate(fate);
        return entity;
    }

    private Fate getFate(ImportDataContext dataContext, String originalSpeciesCode, String avdthDestinationCode) {
        if (avdthDestinationCode != null) {
            int code = Integer.parseInt(avdthDestinationCode);
            if (isDestinationCodeToTransshipment(code)) {
                return dataContext.getFateTransshipment();
            }
            if (isDestinationCodeToSoldOnLocalMarket(code)) {
                return dataContext.getFateSoldOnLocalMarket();
            }
        }
        if (dataContext.isSpeciesDiscarded(originalSpeciesCode)) {
            return dataContext.getFateDiscarded();
        }
        return dataContext.getFateProcessed();
    }

    private boolean isDestinationCodeToTransshipment(int code) {
        return code == 3 || code == 4;
    }

    private boolean isDestinationCodeToSoldOnLocalMarket(int code) {
        return code == 18 || code == 28 || (code >= 40 && code <= 61) || (code >= 64 && code <= 65);
    }

}

