package fr.ird.observe.persistence.avdth.data.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.data.ps.ActivitiesAcquisitionMode;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.referential.common.Harbour;
import fr.ird.observe.entities.referential.common.Vessel;
import fr.ird.observe.entities.referential.ps.common.AcquisitionStatus;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;
import fr.ird.observe.spi.SqlConversions;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

/**
 * Created on 24/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripReader extends DataReader<Trip> {

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(DataQueryDefinition.class)
    public static class TripTableReader extends DataQueryDefinition {

        /**
         * create table MAREE (
         * C_BAT         SMALLINT                  not null constraint MAREE_CFK_BATEAU_R_MAREE references BATEAU,
         * D_DBQ         TIMESTAMP                 not null,
         * C_PORT_DBQ    SMALLINT                  not null constraint MAREE_PORTMAREE1 references PORT on update cascade,
         * V_TEMPS_M     SMALLINT        default 0 not null,
         * V_TEMPS_P     SMALLINT        default 0 not null,
         * V_POIDS_DBQ   NUMERIC(100, 7) default 0 not null,
         * V_POIDS_FP    NUMERIC(100, 7) default 0 not null,
         * F_ENQ         SMALLINT        default 1 not null,
         * C_PORT_DEP    SMALLINT                  not null constraint MAREE_PORTMAREE references PORT on update cascade,
         * D_DEPART      TIMESTAMP,
         * F_CAL_VID     SMALLINT        default 1 not null,
         * V_LOCH        SMALLINT,
         * C_ZONE_GEO    SMALLINT constraint MAREE_CFK_ZONE_GEO_R_MAREE references ZONE_GEO,
         * L_COM_M       VARCHAR(255),
         * C_ID_ERS      VARCHAR(64),
         * C_TOPIAID     VARCHAR(255),
         * F_CAL_VID_DPT SMALLINT        default 1 not null,
         * primary key (C_BAT, D_DBQ));
         */
        public TripTableReader() {
            super(2);
        }

        @Override
        public String select() {
            return  /* MAREE_01 */" C_BAT," +
                    /* MAREE_02 */ " D_DBQ," +
                    /* MAREE_03 */ " C_PORT_DBQ," +
                    /* MAREE_04 */ " V_TEMPS_M," +
                    /* MAREE_05 */ " V_TEMPS_P," +
                    /* MAREE_06 */ " V_POIDS_DBQ," +
                    /* MAREE_07 */ " V_POIDS_FP," +
                    /* MAREE_08 */ " F_ENQ," +
                    /* MAREE_09 */ " C_PORT_DEP," +
                    /* MAREE_10 */ " D_DEPART," +
                    /* MAREE_11 */ " F_CAL_VID," +
                    /* MAREE_12 */ " V_LOCH," +
                    /* MAREE_13 */ " L_COM_M," +
                    /* MAREE_14 */ " C_ID_ERS," +
                    /* MAREE_15 */ " F_CAL_VID_DPT";
        }

        @Override
        public String from() {
            return "MAREE";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ";
        }
    }

    public static String lastPrimaryKeyString(List<Object> pk) {
        return pk.get(0) + "-" + SqlConversions.day((java.util.Date) pk.get(1));
    }

    public TripReader(ImportEngine context) {
        super(context);
    }

    @Override
    public Trip read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        Trip entity = newEntity(Trip.SPI);
        AcquisitionStatus notFilledAcquisitionStatus = dataContext.getAcquisitionStatus999();
        entity.setObservationsAcquisitionStatus(notFilledAcquisitionStatus);
        entity.setTargetWellsSamplingAcquisitionStatus(notFilledAcquisitionStatus);
        entity.setLandingAcquisitionStatus(notFilledAcquisitionStatus);
        entity.setLocalMarketAcquisitionStatus(notFilledAcquisitionStatus);
        entity.setLocalMarketWellsSamplingAcquisitionStatus(notFilledAcquisitionStatus);
        entity.setLocalMarketSurveySamplingAcquisitionStatus(notFilledAcquisitionStatus);
        entity.setAdvancedSamplingAcquisitionStatus(notFilledAcquisitionStatus);
        entity.setHistoricalData(false);
        entity.setLogbookProgram(dataContext.getTripLogbookProgram());
        entity.setOcean(dataContext.getTripOcean());
        String vesselCode = resultSet.getString(1);
        Vessel vessel = dataContext.getVessel(vesselCode);
        entity.setVessel(vessel);
        Date endDate = resultSet.getDate(2);
        entity.setEndDate(Objects.requireNonNull(endDate));
        String landingHarbourCode = resultSet.getString(3);
        Harbour landingHarbour = dataContext.getHarbour(landingHarbourCode);
        entity.setLandingHarbour(landingHarbour);
        entity.setTimeAtSea(resultSet.getInt(4));
        entity.setFishingTime(resultSet.getInt(5));
        entity.setLandingTotalWeight(resultSet.getFloat(6));
        entity.setLocalMarketTotalWeight(resultSet.getFloat(7));
        boolean activityAvailability = resultSet.getInt(8) > 0;
        entity.setLogbookAcquisitionStatus(activityAvailability ? dataContext.getAcquisitionStatus1() : notFilledAcquisitionStatus);
        String departureHarbourCode = resultSet.getString(9);
        Harbour departureHarbour = dataContext.getHarbour(departureHarbourCode);
        entity.setDepartureHarbour(departureHarbour);
        Date startDate = resultSet.getDate(10);
        entity.setStartDate(Objects.requireNonNull(startDate));
        entity.setLandingWellContentStatus(dataContext.getWellContentStatus(resultSet.getString(11)));
        entity.setLoch(resultSet.getInt(12));
        String logbookComment = resultSet.getString(13);
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2923
        if (logbookComment != null) {
            logbookComment = logbookComment.trim();
            if (!logbookComment.isEmpty()) {
                entity.setLogbookComment(logbookComment);
            }
        }
        entity.setErsId(resultSet.getString(14));
        entity.setDepartureWellContentStatus(dataContext.getWellContentStatus(resultSet.getString(15)));
        entity.setLogbookDataQuality(dataContext.getDataQualityA());
        entity.setLogbookDataEntryOperator(dataContext.getDataEntryOperator());
        entity.setActivitiesAcquisitionMode(ActivitiesAcquisitionMode.BY_NUMBER);
        return entity;
    }
}
