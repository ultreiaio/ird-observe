package fr.ird.observe.persistence.avdth.data.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.persistence.avdth.data.DataWriter;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import static fr.ird.observe.spi.SqlConversions.day;
import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toId;

/**
 * Created on 24/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripWriter extends DataWriter<Trip, TripReader> {

    public TripWriter(ImportEngine context, TripReader reader) {
        super(context, reader);
    }

    @Override
    public void write(Trip entity, String parentId, int parentCount) {

        String id = entity.getTopiaId();
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_common.trip" +
                                           /*  1 */      "(topiaId," +
                                           /*  2 */      " topiaVersion," +
                                           /*  3 */      " topiaCreateDate," +
                                           /*  4 */      " lastUpdateDate," +
                                           /*  5 */      " startDate," +
                                           /*  6 */      " endDate," +
                                           /*  7 */      " ersId," +
                                           /*  8 */      " ocean," +
                                           /*  9 */      " vessel," +
                                           /* 10 */      " logbookProgram," +
                                           /* 11 */      " departureHarbour," +
                                           /* 12 */      " landingHarbour," +
                                           /* 13 */      " logbookComment," +
                                           /* 14 */      " timeAtSea," +
                                           /* 15 */      " fishingTime," +
                                           /* 16 */      " loch," +
                                           /* 17 */      " landingTotalWeight," +
                                           /* 18 */      " localMarketTotalWeight," +
                                           /* 19 */      " observationsAcquisitionStatus," +
                                           /* 20 */      " logbookAcquisitionStatus," +
                                           /* 21 */      " departureWellContentStatus," +
                                           /* 22 */      " landingWellContentStatus," +
                                           /* 23 */      " historicalData," +
                                           /* 24 */      " targetWellsSamplingAcquisitionStatus," +
                                           /* 25 */      " landingAcquisitionStatus," +
                                           /* 26 */      " localMarketAcquisitionStatus," +
                                           /* 27 */      " localMarketWellsSamplingAcquisitionStatus," +
                                           /* 28 */      " localMarketSurveySamplingAcquisitionStatus," +
                                           /* 29 */      " advancedSamplingAcquisitionStatus," +
                                           /* 30 */      " logbookDataQuality," +
                                           /* 31 */      " logbookDataEntryOperator," +
                                           /* 32 */      " activitiesAcquisitionMode)" +
                                           " VALUES (" +
                                           /*  1 */         " %1$s," +
                                           /*  2 */         " 0," +
                                           /*  3 */         " '%2$s'::timestamp," +
                                           /*  4 */         " '%3$s'::timestamp," +
                                           /*  5 */         " '%4$s'::date," +
                                           /*  6 */         " '%5$s'::date," +
                                           /*  7 */         " %6$s," +
                                           /*  8 */         " %7$s," +
                                           /*  9 */         " %8$s," +
                                           /* 10 */         " %9$s," +
                                           /* 11 */         " %10$s," +
                                           /* 12 */         " %11$s," +
                                           /* 13 */         " %12$s," +
                                           /* 14 */         " %13$s," +
                                           /* 15 */         " %14$s," +
                                           /* 16 */         " %15$s," +
                                           /* 17 */         " %16$s," +
                                           /* 18 */         " %17$s," +
                                           /* 19 */         " %18$s," +
                                           /* 20 */         " %19$s," +
                                           /* 21 */         " %20$s," +
                                           /* 22 */         " %21$s," +
                                           /* 23 */         " %22$s," +
                                           /* 24 */         " %23$s," +
                                           /* 25 */         " %24$s," +
                                           /* 26 */         " %25$s," +
                                           /* 27 */         " %26$s," +
                                           /* 28 */         " %27$s," +
                                           /* 29 */         " %28$s," +
                                           /* 30 */         " %29$s," +
                                           /* 31 */         " %30$s," +
                                           /* 32 */         " %31$s,);",
                /*  1 */          escapeString(id),
                /*  2 */
                /*  3 */          timestamp,
                /*  4 */          timestamp,
                /*  5 */          day(entity.getStartDate()),
                /*  6 */          day(entity.getEndDate()),
                /*  7 */          escapeString(entity.getErsId()),
                /*  8 */          toId(entity.getOcean()),
                /*  9 */          toId(entity.getVessel()),
                /* 10 */          toId(entity.getLogbookProgram()),
                /* 11 */          toId(entity.getDepartureHarbour()),
                /* 12 */          toId(entity.getLandingHarbour()),
                /* 13 */          escapeComment(entity.getLogbookComment()),
                /* 14 */          entity.getTimeAtSea(),
                /* 15 */          entity.getFishingTime(),
                /* 16 */          entity.getLoch(),
                /* 17 */          roundFloat4(entity.getLandingTotalWeight()),
                /* 18 */          roundFloat4(entity.getLocalMarketTotalWeight()),
                /* 19 */          toId(entity.getObservationsAcquisitionStatus()),
                /* 20 */          toId(entity.getLogbookAcquisitionStatus()),
                /* 21 */          toId(entity.getDepartureWellContentStatus()),
                /* 22 */          toId(entity.getLandingWellContentStatus()),
                /* 23 */          entity.isHistoricalData(),
                /* 24 */          toId(entity.getTargetWellsSamplingAcquisitionStatus()),
                /* 25 */          toId(entity.getLandingAcquisitionStatus()),
                /* 26 */          toId(entity.getLocalMarketAcquisitionStatus()),
                /* 27 */          toId(entity.getLocalMarketWellsSamplingAcquisitionStatus()),
                /* 28 */          toId(entity.getLocalMarketSurveySamplingAcquisitionStatus()),
                /* 29 */          toId(entity.getLogbookAcquisitionStatus()),
                /* 30 */          toId(entity.getLogbookDataQuality()),
                /* 31 */          toId(entity.getLogbookDataEntryOperator()),
                /* 32 */          escapeString(entity.getActivitiesAcquisitionMode().name())
        );
        writer().writeSql(sql);
    }

    public void writeAcquisitionStatus(String tripId, String acquisitionStatusProperty) {
        String sql = String.format("UPDATE ps_common.trip SET %1$s = %2$s WHERE topiaId = '%3$s';",
                                   acquisitionStatusProperty,
                                   toId(dataContext().getAcquisitionStatus1()),
                                   tripId);
        writer().writeSql(sql);
    }
}
