package fr.ird.observe.persistence.avdth.data.localmarket;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.localmarket.Batch;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.ps.localmarket.Packaging;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 20/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class BatchReader extends DataReader<Batch> {

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(DataQueryDefinition.class)
    public static class BatchTableReader extends DataQueryDefinition {

        /**
         * create table FP_LOT
         * (
         * C_BAT        SMALLINT     not null,
         * D_DBQ        TIMESTAMP    not null,
         * N_LOT        VARCHAR(255) not null,
         * C_ESP        SMALLINT     not null constraint FP_LOT_ESPECEFP_LOT references ESPECE on update cascade on delete cascade,
         * C_COND       SMALLINT     not null constraint FP_LOT_FP_CONDITIONNEMENTFP_LOT references FP_CONDITIONNEMENT on update cascade,
         * N_UNIT       NUMERIC(100, 2),
         * V_POIDS_PESE NUMERIC(100, 3),
         * L_ORIGINE    VARCHAR(255),
         * L_COM        VARCHAR(255),
         * primary key (C_BAT, D_DBQ, N_LOT),
         * constraint FP_LOT_MAREEFP_LOT foreign key (C_BAT, D_DBQ) references MAREE on update cascade on delete cascade
         * );
         */
        public BatchTableReader() {
            super(3);
        }

        @Override
        public String select() {
            return  /* FP_LOT_01 */   " FP_LOT.C_BAT," +
                    /* FP_LOT_02 */   " FP_LOT.D_DBQ," +
                    /* FP_LOT_03 */   " FP_LOT.N_LOT," +
                    /* FP_LOT_04 */   " FP_LOT.C_ESP," +
                    /* FP_LOT_05 */   " FP_LOT.C_COND," +
                    /* FP_LOT_06 */   " FP_LOT.N_UNIT," +
                    /* FP_LOT_07 */   " FP_LOT.V_POIDS_PESE," +
                    /* FP_LOT_08 */   " FP_LOT.L_ORIGINE," +
                    /* FP_LOT_09 */   " FP_LOT.L_COM";
        }

        @Override
        public String from() {
            return "FP_LOT";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_LOT, C_ESP";
        }
    }

    public BatchReader(ImportEngine context) {
        super(context);
    }

    @Override
    public Batch read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        Batch entity = newEntity(Batch.SPI);
        String homeId = resultSet.getString(3);
        entity.setHomeId(homeId);
        String originalSpeciesCode = resultSet.getString(4);
        Species species = dataContext.getSpecies(originalSpeciesCode);
        entity.setSpecies(species);
        Object codeCond = resultSet.getObject(5);
        Packaging packaging = dataContext.getPackaging(codeCond + "");
        entity.setPackaging(packaging);
        Object count = resultSet.getObject(6);
        if (count != null) {
            entity.setCount(Float.valueOf(count + "").intValue());
        }
        Object weight = resultSet.getObject(7);
        if (weight != null) {
            entity.setWeight(Float.valueOf(weight + ""));
        }
        String origin = resultSet.getString(8);
        entity.setOrigin(origin);
        String comment = resultSet.getString(9);
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2923
        if (comment != null) {
            comment = comment.trim();
            if (!comment.isEmpty()) {
                entity.setComment(comment);
            }
        }
        return entity;
    }
}
