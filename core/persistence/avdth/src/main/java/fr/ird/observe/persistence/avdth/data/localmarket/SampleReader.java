package fr.ird.observe.persistence.avdth.data.localmarket;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.localmarket.Sample;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 30/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleReader extends DataReader<Sample> {

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(DataQueryDefinition.class)
    public static class SampleTableReader extends DataQueryDefinition {

        /**
         * create table FP_ECHANTILLON
         * (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * N_ECH VARCHAR(255) not null,
         * D_ECH TIMESTAMP,
         * C_BAT_REEL SMALLINT constraint FP_ECHANTILLON_BATEAUFP_ECHANTILLON references BATEAU on update cascade,
         * primary key (C_BAT, D_DBQ, N_ECH);
         */
        public SampleTableReader() {
            super(3);
        }

        @Override
        public String select() {
            return  /* FP_ECHANTILLON_01 */   " C_BAT," +
                    /* FP_ECHANTILLON_02 */   " D_DBQ," +
                    /* FP_ECHANTILLON_03 */   " N_ECH," +
                    /* FP_ECHANTILLON_04 */   " D_ECH," +
                    /* FP_ECHANTILLON_05 */   " C_BAT_REEL";
        }

        @Override
        public String from() {
            return "FP_ECHANTILLON";
        }

        @Override
        public String innerJoin() {
            return "MAREE ON MAREE.C_BAT = FP_ECHANTILLON.C_BAT AND MAREE.D_DBQ = FP_ECHANTILLON.D_DBQ";
        }

        @Override
        protected String whereMissing() {
            return " NOT EXISTS( SELECT 'x' FROM MAREE WHERE MAREE.C_BAT = FP_ECHANTILLON.C_BAT AND MAREE.D_DBQ = FP_ECHANTILLON.D_DBQ )";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_ECH";
        }
    }

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(DataQueryDefinition.class)
    public static class SampleWellTableReader extends DataQueryDefinition {

        /**
         * create table FP_ECH_CUVE (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * N_ECH VARCHAR(255) not null,
         * N_CUVE SMALLINT not null,
         * C_POSITION SMALLINT not null,
         * primary key (C_BAT, D_DBQ, N_ECH, N_CUVE, C_POSITION),
         * constraint FP_ECH_CUVE_FP_ECHANTILLONFP_ECH_CUVE foreign key (C_BAT, D_DBQ, N_ECH) references FP_ECHANTILLON on update cascade on delete cascade);
         */
        public SampleWellTableReader() {
            super(5);
        }

        @Override
        public String select() {
            return  /* FP_ECH_CUVE_01 */   " C_BAT," +
                    /* FP_ECH_CUVE_02 */   " D_DBQ," +
                    /* FP_ECH_CUVE_03 */   " N_ECH," +
                    /* FP_ECH_CUVE_04 */   " N_CUVE," +
                    /* FP_ECH_CUVE_05 */   " C_POSITION";
        }

        @Override
        public String from() {
            return "FP_ECH_CUVE";
        }

        @Override
        public String innerJoin() {
            return "MAREE ON MAREE.C_BAT = FP_ECH_CUVE.C_BAT AND MAREE.D_DBQ = FP_ECH_CUVE.D_DBQ";
        }

        @Override
        protected String whereMissing() {
            return " NOT EXISTS( SELECT 'x' FROM MAREE WHERE MAREE.C_BAT = FP_ECH_CUVE.C_BAT AND MAREE.D_DBQ = FP_ECH_CUVE.D_DBQ )";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_ECH, N_CUVE, C_POSITION";
        }
    }

    public SampleReader(ImportEngine context) {
        super(context);
    }

    @Override
    public Sample read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        Sample entity = newEntity(Sample.SPI);
        entity.setNumber(resultSet.getString(3));
        entity.setDate(resultSet.getDate(4));
        entity.setSampleType(dataContext.getLocalmarketSampleType());
        return entity;
    }
}

