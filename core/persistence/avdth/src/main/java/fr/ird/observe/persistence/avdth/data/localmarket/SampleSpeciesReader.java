package fr.ird.observe.persistence.avdth.data.localmarket;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.localmarket.SampleSpecies;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 30/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleSpeciesReader extends DataReader<SampleSpecies> {

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(DataQueryDefinition.class)
    public static class SampleSpeciesTableReader extends DataQueryDefinition {

        /**
         * create table FP_ECH_ESP
         * (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * N_ECH VARCHAR(255) not null,
         * C_ESP SMALLINT not null,
         * F_LDLF SMALLINT not null,
         * V_NB_MES SMALLINT not null,
         * primary key (C_BAT, D_DBQ, N_ECH, C_ESP, F_LDLF),
         * constraint FP_ECH_ESP_FP_ECHANTILLONFP_ECH_ESP foreign key (C_BAT, D_DBQ, N_ECH) references FP_ECHANTILLON on update cascade on delete cascade);
         */
        public SampleSpeciesTableReader() {
            super(5);
        }

        @Override
        public String select() {
            return  /* FP_ECH_ESP_01 */   " FP_ECH_ESP.C_BAT," +
                    /* FP_ECH_ESP_02 */   " FP_ECH_ESP.D_DBQ," +
                    /* FP_ECH_ESP_03 */   " FP_ECH_ESP.N_ECH," +
                    /* FP_ECH_ESP_04 */   " FP_ECH_ESP.C_ESP," +
                    /* FP_ECH_ESP_05 */   " FP_ECH_ESP.F_LDLF," +
                    /* FP_ECH_ESP_06 */   " FP_ECH_ESP.V_NB_MES";
        }

        @Override
        public String from() {
            return "FP_ECH_ESP";
        }

        @Override
        public String innerJoin() {
            return "MAREE ON MAREE.C_BAT = FP_ECH_ESP.C_BAT AND MAREE.D_DBQ = FP_ECH_ESP.D_DBQ";
        }

        @Override
        protected String whereMissing() {
            return " NOT EXISTS( SELECT 'x' FROM MAREE WHERE MAREE.C_BAT = FP_ECH_ESP.C_BAT AND MAREE.D_DBQ = FP_ECH_ESP.D_DBQ )";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_ECH, C_ESP, F_LDLF";
        }
    }

    public SampleSpeciesReader(ImportEngine context) {
        super(context);
    }

    @Override
    public SampleSpecies read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        SampleSpecies entity = newEntity(SampleSpecies.SPI);
        String originalSpeciesCode = resultSet.getString(4);
        Species species = dataContext.getSpecies(originalSpeciesCode);
        entity.setSpecies(species);
        int ldLf = resultSet.getInt(5);
        entity.setSizeMeasureType(dataContext.getSizeMeasureType(ldLf));
        Object measureCount = resultSet.getObject(6);
        if (measureCount != null) {
            entity.setMeasuredCount(Integer.valueOf(measureCount.toString()));
        }
        return entity;
    }
}
