package fr.ird.observe.persistence.avdth.data.localmarket;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.localmarket.Survey;
import fr.ird.observe.entities.data.ps.localmarket.SurveyPart;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.persistence.avdth.data.DataWriter;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.util.Map;

import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toDate;
import static fr.ird.observe.spi.SqlConversions.toId;

/**
 * Created on 20/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SurveyWriter extends DataWriter<Survey, SurveyReader> {

    public SurveyWriter(ImportEngine context, SurveyReader reader) {
        super(context, reader);
    }

    @Override
    public void write(Survey entity, String parentId, int parentCount) {
        String id = entity.getTopiaId();
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_localmarket.Survey" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " date," +
                                           /*  6 */  " number," +
                                           /*  7 */  " comment," +
                                           /*  8 */  " trip)" +
                                           " VALUES (" +
                                           /*  1 */       " %1$s," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s," +
                                           /*  7 */       " %6$s," +
                                           /*  8 */       " %7$s);",
                /*  1 */  escapeString(id),
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  toDate(entity.getDate()),
                /*  6 */  entity.getNumber(),
                /*  7 */  escapeComment(entity.getComment()),
                /*  8 */  escapeString(parentId));
        writer().writeSql(sql);
    }

    @Override
    public void flush() {
        super.flush();
        flush(SurveyPart.class, getReader().getSurveyPartCount());
    }

    @Override
    public void toResult(Map<String, Integer> resultBuilder) {
        super.toResult(resultBuilder);
        toResult(resultBuilder, SurveyPart.class, getReader().getSurveyPartCount());
    }

    public void writeExtra(SurveyPart entity, String parentId) {
        Species species = entity.getSpecies();
        if (species.isNotPersisted()) {
            badSpecies.add(species.getCode() + "-" + species.getLabel2());
            return;
        }
        String id = entity.getTopiaId();
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_localmarket.SurveyPart" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " homeId," +
                                           /*  6 */  " species," +
                                           /*  7 */  " proportion," +
                                           /*  8 */  " survey)" +
                                           " VALUES (" +
                                           /*  1 */       " %1$s," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s," +
                                           /*  7 */       " %6$s," +
                                           /*  8 */       " %7$s);",
                /*  1 */  escapeString(id),
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  escapeString(entity.getHomeId()),
                /*  6 */  toId(entity.getSpecies()),
                /*  7 */  entity.getProportion(),
                /*  8 */  escapeString(parentId));
        writer().writeSql(sql);
    }
}

