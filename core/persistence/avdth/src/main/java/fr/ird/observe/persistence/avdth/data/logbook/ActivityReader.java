package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.referential.common.Wind;
import fr.ird.observe.entities.referential.ps.common.ObservedSystem;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.persistence.avdth.data.DataQuery;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;
import fr.ird.observe.persistence.avdth.referential.interceptors.ObservedSystemInterceptor;
import io.ultreia.java4all.util.Dates;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 25/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */

@SuppressWarnings("SpellCheckingInspection")
public class ActivityReader extends DataReader<Activity> {

    @AutoService(DataQueryDefinition.class)
    public static class ActivityTableReader extends DataQueryDefinition {

        /**
         * create table ACTIVITE (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * D_ACT TIMESTAMP not null,
         * N_ACT SMALLINT not null,
         * H_ACT SMALLINT,
         * M_ACT INTEGER,
         * C_OCEA SMALLINT not null constraint ACTIVITE_OCEANACTIVITE references OCEAN on update cascade,
         * Q_ACT SMALLINT not null,
         * V_LAT SMALLINT not null,
         * V_LON SMALLINT not null,
         * V_TMER SMALLINT not null,
         * V_TPEC SMALLINT not null,
         * C_OPERA SMALLINT not null constraint ACTIVITE_CFK_OPERA_R_ACTIVITE references OPERA,
         * V_NB_OP SMALLINT not null,
         * C_TBANC SMALLINT not null constraint ACTIVITE_CFK_TYPE_BANC_R_ACTIVITE references TYPE_BANC,
         * F_DON_ORG SMALLINT default 1 not null,
         * F_POS_COR SMALLINT default 0 not null,
         * F_POS_VMS_D SMALLINT default 9 not null,
         * F_CUVE_C SMALLINT default 9 not null,
         * F_OBS SMALLINT default 0 not null,
         * F_EXPERT SMALLINT default 9 not null,
         * V_POIDS_CAP NUMERIC(100,7),
         * V_TEMP_S NUMERIC(100,7),
         * V_COUR_DIR SMALLINT,
         * V_COUR_VIT NUMERIC(100,7),
         * V_VENT_DIR SMALLINT,
         * V_VENT_VIT NUMERIC(100,7),
         * C_TYP_OBJET SMALLINT constraint ACTIVITE_TYPE_OBJETACTIVITE references TYPE_OBJET,
         * F_DCP_ECO INTEGER,
         * F_PROP_BALISE INTEGER,
         * C_TYP_BALISE SMALLINT constraint ACTIVITE_TYPE_BALISEACTIVITE1 references TYPE_BALISE on update cascade on delete cascade,
         * V_ID_BALISE VARCHAR(64),
         * V_POIDS_ESTIM_DCP NUMERIC default 0,
         * L_COM_A VARCHAR(255),
         * C_Z_FPA SMALLINT constraint ACTIVITE_ZONE_FPAACTIVITE references ZONE_FPA,
         * primary key (C_BAT, D_DBQ, D_ACT, N_ACT),
         * constraint ACTIVITE_CFK_MAREE_R_ACTIVITE foreign key (C_BAT, D_DBQ) references OCEAN (C_BAT, D_DBQ) on update cascade);
         */
        public ActivityTableReader() {
            super(4);
        }

        @Override
        public String select() {
            return  /* ACTIVITE_01 */ " C_BAT," +
                    /* ACTIVITE_02 */ " D_DBQ," +
                    /* ACTIVITE_03 */ " D_ACT," +
                    /* ACTIVITE_04 */ " N_ACT," +
                    /* ACTIVITE_05 */ " H_ACT," +
                    /* ACTIVITE_06 */ " M_ACT," +
                    /* ACTIVITE_07 */ " C_OCEA," +
                    /* ACTIVITE_08 */ " Q_ACT," +
                    /* ACTIVITE_09 */ " V_LAT," +
                    /* ACTIVITE_10 */ " V_LON," +
                    /* ACTIVITE_11 */ " V_TMER," +
                    /* ACTIVITE_12 */ " V_TPEC," +
                    /* ACTIVITE_13 */ " C_OPERA," +
                    /* ACTIVITE_14 */ " V_NB_OP," +
                    /* ACTIVITE_15 */ " C_TBANC," +
                    /* ACTIVITE_16 */ " F_DON_ORG," +
                    /* ACTIVITE_17 */ " F_POS_COR," +
                    /* ACTIVITE_18 */ " F_POS_VMS_D," +
                    /* ACTIVITE_19 */ " F_CUVE_C," +
                    /* ACTIVITE_20 */ " F_OBS," +
                    /* ACTIVITE_21 */ " F_EXPERT," +
                    /* ACTIVITE_22 */ " V_POIDS_CAP," +
                    /* ACTIVITE_23 */ " V_TEMP_S," +
                    /* ACTIVITE_24 */ " V_COUR_DIR," +
                    /* ACTIVITE_25 */ " V_COUR_VIT," +
                    /* ACTIVITE_26 */ " V_VENT_DIR," +
                    /* ACTIVITE_27 */ " V_VENT_VIT," +
                    /* ACTIVITE_28 */ " C_TYP_OBJET," +
                    /* ACTIVITE_29 */ " F_DCP_ECO," +
                    /* ACTIVITE_30 */ " F_PROP_BALISE," +
                    /* ACTIVITE_31 */ " C_TYP_BALISE," +
                    /* ACTIVITE_32 */ " V_ID_BALISE," +
                    /* ACTIVITE_33 */ " V_POIDS_ESTIM_DCP," +
                    /* ACTIVITE_34 */ " L_COM_A," +
                    /* ACTIVITE_35 */ " C_Z_FPA";
        }

        @Override
        public String from() {
            return "ACTIVITE";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, D_ACT, N_ACT";
        }
    }

    @AutoService(DataQueryDefinition.class)
    public static class ActivityObservedSystemTableReader extends DataQueryDefinition {

        /**
         * create table ACT_ASSOC (
         * 1 C_BAT SMALLINT not null,
         * 2 D_DBQ TIMESTAMP not null,
         * 3 D_ACT TIMESTAMP not null,
         * 4 N_ACT SMALLINT not null,
         * 5 N_ASSOC INTEGER identity,
         * 6 C_ASSOC SMALLINT not null constraint ACT_ASSOC_FK_ASSOC_R_ACT_ASSOC references ASSOC,
         * primary key (C_BAT, D_DBQ, D_ACT, N_ACT, N_ASSOC),
         * constraint ACT_ASSOC_FK_ACTIVITE_R_ACT_ASSOC foreign key (C_BAT, D_DBQ, D_ACT, N_ACT) references ASSOC (C_BAT, D_DBQ, D_ACT, N_ACT) on update cascade );
         */
        public ActivityObservedSystemTableReader() {
            super(4);
        }

        @Override
        public String select() {
            return  /*  ACT_ASSOC_01 */ " C_BAT," +
                    /*  ACT_ASSOC_02 */ " D_DBQ," +
                    /*  ACT_ASSOC_03 */ " D_ACT," +
                    /*  ACT_ASSOC_04 */ " N_ACT," +
                    /*  ACT_ASSOC_05 */ " N_ASSOC," +
                    /*  ACT_ASSOC_06 */ " C_ASSOC";
        }

        @Override
        public String from() {
            return "ACT_ASSOC";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, D_ACT, N_ACT, N_ASSOC";
        }

    }

    public static List<Object> getRoutePk(ResultSet activityRow) throws SQLException {
        return Query.getPrimaryKey(activityRow, 1, 3);
    }

    public static List<Object> getActivityPk(ResultSet activityRow) throws SQLException {
        return Query.getPrimaryKey(activityRow, 1, 4);
    }

    public static List<Object> getSimpleActivityPk(ResultSet activityRow) throws SQLException {
        return Query.getPrimaryKey(activityRow, 3, 4);
    }

    public ActivityReader(ImportEngine context) {
        super(context);
    }

    @Override
    public Activity read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        Activity entity = newEntity(Activity.SPI);
        Set<ObservedSystem> observedSystems = dataContext.getObservedSystems();
        if (!observedSystems.isEmpty()) {
            entity.addAllObservedSystem(observedSystems);
            // as soon as entity was flush, let's clear internal states
            observedSystems.clear();
        }
        entity.setNumber(resultSet.getInt(4));
        Integer timeHour = (Integer) resultSet.getObject(5);
        Integer timeMinute = (Integer) resultSet.getObject(6);
        if (timeHour != null && timeMinute != null) {
            Date time = Dates.createDate(0, timeMinute, timeHour, 0, 0, 0);
            entity.setTime(time);
        }
        // 7- No ocean
        loadCoordinate(resultSet, 8, entity);
        // 11 - V_TMER on route
        // 12 - V_TPEC on route
        String vesselActivityCode = resultSet.getString(13);
        Object setCountObject = resultSet.getObject(14);
        Integer setCount = setCountObject == null ? null : ((Number) setCountObject).intValue();
        String schoolTypeCode = resultSet.getString(15);
        switch (vesselActivityCode) {
            case "0":
                // SetSuccessStatus=0 + ReasonForNullSet=0 (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2091)
                entity.setSetSuccessStatus(dataContext.getSetSuccessStatus0());
                entity.setReasonForNullSet(dataContext.getReasonForNullSet0());
                // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2610
                entity.setSetCount(setCount);
                // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2485
                entity.setSchoolType(dataContext.getSchoolType(schoolTypeCode));
                break;
            case "1":
                // SetSuccessStatus=1 (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2091)
                entity.setSetSuccessStatus(dataContext.getSetSuccessStatus1());
                // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2610
                entity.setSetCount(setCount);
                // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2485
                entity.setSchoolType(dataContext.getSchoolType(schoolTypeCode));
                break;
            case "2":
                // SetSuccessStatus=2 (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2091)
                entity.setSetSuccessStatus(dataContext.getSetSuccessStatus2());
                // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2610
                entity.setSetCount(setCount);
                // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2485
                entity.setSchoolType(dataContext.getSchoolType(schoolTypeCode));
                break;
            case "14": // (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2429)
                // SetSuccessStatus=2
                entity.setSetSuccessStatus(dataContext.getSetSuccessStatus2());
                // Add Observed system 110
                entity.addObservedSystem(dataContext.getObservedSystem110());
                // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2610
                entity.setSetCount(setCount);
                // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2485
                entity.setSchoolType(dataContext.getSchoolType(schoolTypeCode));
                break;
        }
        entity.setVesselActivity(dataContext.getVesselActivity(vesselActivityCode));
        Object fDonOrg = resultSet.getObject(16);
        if (fDonOrg != null) {
            boolean originalDataModified = Objects.equals(fDonOrg + "", "0");
            entity.setOriginalDataModified(originalDataModified);
        }
        Object fPosCor = resultSet.getObject(17);
        if (fPosCor != null) {
            boolean positionCorrected = Objects.equals(fDonOrg + "", "9");
            entity.setPositionCorrected(positionCorrected);
        }
        Object vmsPos = resultSet.getObject(18);
        if (vmsPos != null) {
            boolean vmsDivergent = Objects.equals(vmsPos + "", "1");
            entity.setVmsDivergent(vmsDivergent);
        }
        // Not used 19 - F_CUVE_C
        // Not used 20 - F_OBS
        // Not used 21 - F_EXPERT
        entity.setDataQuality(dataContext.getDataQualityA());
        Object totalWeight = resultSet.getObject(22);
        if (totalWeight != null) {
            entity.setTotalWeight(((Number) totalWeight).floatValue());
        }
        Object seaSurfaceTemperature = resultSet.getObject(23);
        if (seaSurfaceTemperature != null) {
            entity.setSeaSurfaceTemperature(((Number) seaSurfaceTemperature).floatValue());
        }
        Object currentDirection = resultSet.getObject(24);
        if (currentDirection != null) {
            entity.setCurrentDirection(((Number) currentDirection).intValue());
        }
        Object currentSpeed = resultSet.getObject(25);
        if (currentSpeed != null) {
            entity.setCurrentSpeed(((Number) currentSpeed).floatValue());
        }
        Object windDirection = resultSet.getObject(26);
        if (windDirection != null) {
            entity.setWindDirection(((Number) windDirection).intValue());
        }
        Object windSpeed = resultSet.getObject(27);
        if (windSpeed != null) {
            Wind wind = dataContext.getWind(((Number) windSpeed).intValue());
            entity.setWind(wind);
        }
        String comment = resultSet.getString(34);
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2923
        if (comment != null) {
            comment = comment.trim();
            if (!comment.isEmpty()) {
                entity.setComment(comment);
            }
        }
        String fpaZoneCode = resultSet.getString(35);
        entity.setCurrentFpaZone(dataContext.getFpaZone(fpaZoneCode));
        return entity;
    }

    public Activity createFromSampleActivity(ImportDataContext dataContext, ResultSet resultSet, int activityNumber) throws SQLException {
        Activity entity = newEntity(Activity.SPI);

        entity.setNumber(activityNumber);
        entity.setVesselActivity(dataContext.getVesselActivity52());
        entity.setComment("Generated from AVDTH ECH_CALEE record");
        entity.setSetCount(1);
        loadCoordinate(resultSet, 7, entity);
        String schoolTypeCode = resultSet.getString(10);
        switch (schoolTypeCode) {
            case "1":
                entity.addObservedSystem(dataContext.getObservedSystem20());
                entity.setSchoolType(dataContext.getSchoolType1());
                break;
            case "2":
                entity.setSchoolType(dataContext.getSchoolType2());
                break;
            case "3":
                entity.setSchoolType(dataContext.getSchoolType0());
                break;
        }
        entity.addObservedSystem(dataContext.getObservedSystem103());
        entity.setDataQuality(dataContext.getDataQualityE());

        return entity;
    }

    public Set<String> readObservedSystem(ImportDataContext dataContext, DataQuery activityObservedSystemTableReader, List<Object> activityPrimaryKey) throws SQLException {
        Set<String> observedSystemCodes = new LinkedHashSet<>();
        Set<ObservedSystem> observedSystems = dataContext.getObservedSystems();
        observedSystems.clear();
        activityObservedSystemTableReader.setPrimaryKey(activityPrimaryKey);
        while (activityObservedSystemTableReader.hasNext()) {
            ResultSet activityObservedSystemRow = activityObservedSystemTableReader.next();
            String avdthSystemCode = activityObservedSystemRow.getString(6);
            observedSystemCodes.add(avdthSystemCode);
            String observeSystemCode = ObservedSystemInterceptor.CODE_MAPPING.get(avdthSystemCode);
            if (observeSystemCode == null) {
                throw new IllegalStateException(String.format("Can't find observedSystem with code: %s from ad-hoc AVDTH to ObServe mapping", avdthSystemCode));
            }
            ObservedSystem observedSystem = dataContext.getObservedSystem(observeSystemCode);
            observedSystems.add(observedSystem);
        }
        return observedSystemCodes;
    }

    private void loadCoordinate(ResultSet resultSet, int columnIndex, Activity entity) throws SQLException {
        int quadrant = resultSet.getInt(columnIndex);
        int latitude = resultSet.getInt(columnIndex + 1);
        int longitude = resultSet.getInt(columnIndex + 2);
        entity.setLatitude(toLatitudeCoordinate(quadrant, latitude));
        entity.setLongitude(toLongitudeCoordinate(quadrant, longitude));
    }

}
