package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.referential.ps.common.ObservedSystem;
import fr.ird.observe.persistence.avdth.data.DataWriter;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.util.Map;

import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toId;
import static fr.ird.observe.spi.SqlConversions.toTime;

/**
 * Created on 25/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */

public class ActivityWriter extends DataWriter<Activity, ActivityReader> {

    private int observedSystemCount;

    public ActivityWriter(ImportEngine context, ActivityReader reader) {
        super(context, reader);
    }

    @Override
    public void write(Activity entity, String parentId, int parentCount) {
        String id = entity.getTopiaId();
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_logbook.activity" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " homeId," +
                                           /*  6 */  " comment," +
                                           /*  7 */  " time," +
                                           /*  8 */  " latitude," +
                                           /*  9 */  " longitude," +
                                           /* 10 */  " number," +
                                           /* 11 */  " setCount," +
                                           /* 12 */  " originalDataModified," +
                                           /* 13 */  " vmsDivergent," +
                                           /* 14 */  " positionCorrected," +
                                           /* 15 */  " seaSurfaceTemperature," +
                                           /* 16 */  " windDirection," +
                                           /* 17 */  " vesselActivity," +
                                           /* 18 */  " wind," +
                                           /* 19 */  " totalWeight," +
                                           /* 20 */  " currentSpeed," +
                                           /* 21 */  " currentDirection," +
                                           /* 22 */  " schoolType," +
                                           /* 23 */  " relatedObservedActivity," +
                                           /* 24 */  " currentFpaZone," +
                                           /* 25 */  " dataQuality," +
                                           /* 26 */  " informationSource," +
                                           /* 27 */  " setSuccessStatus," +
                                           /* 28 */  " reasonForNullSet," +
                                           /* 29 */  " route)" +
                                           " VALUES (" +
                                           /*  1 */       " %1$s," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s," +
                                           /*  7 */       " %6$s," +
                                           /*  8 */       " %7$s," +
                                           /*  9 */       " %8$s," +
                                           /* 10 */       " %9$s," +
                                           /* 11 */       " %10$s," +
                                           /* 12 */       " %11$s," +
                                           /* 13 */       " %12$s," +
                                           /* 14 */       " %13$s," +
                                           /* 15 */       " %14$s," +
                                           /* 16 */       " %15$s," +
                                           /* 17 */       " %16$s," +
                                           /* 18 */       " %17$s," +
                                           /* 19 */       " %18$s," +
                                           /* 20 */       " %19$s," +
                                           /* 21 */       " %20$s," +
                                           /* 22 */       " %21$s," +
                                           /* 23 */       " %22$s," +
                                           /* 24 */       " %23$s," +
                                           /* 25 */       " %24$s," +
                                           /* 26 */       " %25$s," +
                                           /* 27 */       " %26$s," +
                                           /* 28 */       " %27$s," +
                                           /* 29 */       " %28$s" +
                                           ");",

                /*  1 */  escapeString(id),
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  entity.getHomeId(),
                /*  6 */  escapeComment(entity.getComment()),
                /*  7 */  toTime(entity.getTime()),
                /*  8 */  roundFloat4(entity.getLatitude()),
                /*  9 */  roundFloat4(entity.getLongitude()),
                /* 10 */  entity.getNumber(),
                /* 11 */  entity.getSetCount(),
                /* 12 */  entity.isOriginalDataModified(),
                /* 13 */  entity.isVmsDivergent(),
                /* 14 */  entity.isPositionCorrected(),
                /* 15 */  roundFloat2(entity.getSeaSurfaceTemperature()),
                /* 16 */  entity.getWindDirection(),
                /* 17 */  toId(entity.getVesselActivity()),
                /* 18 */  toId(entity.getWind()),
                /* 19 */  roundFloat4(entity.getTotalWeight()),
                /* 20 */  entity.getCurrentSpeed(),
                /* 21 */  entity.getCurrentDirection(),
                /* 22 */  toId(entity.getSchoolType()),
                /* 23 */  toId(entity.getRelatedObservedActivity()),
                /* 24 */  toId(entity.getCurrentFpaZone()),
                /* 25 */  toId(entity.getDataQuality()),
                /* 26 */  escapeString("fr.ird.referential.ps.logbook.InformationSource#1464000000000#01"), // Migration-AVDTH
                /* 27 */  toId(entity.getSetSuccessStatus()),
                /* 28 */  toId(entity.getReasonForNullSet()),
                /* 29 */  escapeString(parentId)
        );
        writer().writeSql(sql);
    }

    public void writeObservedSystems(Activity entity) {
        if (entity.isObservedSystemNotEmpty()) {
            String id = entity.getId();
            for (ObservedSystem observedSystem : entity.getObservedSystem()) {
                String sql = String.format("INSERT INTO ps_logbook.activity_observedSystem(activity, observedSystem) VALUES (%1$s, %2$s);",
                                           escapeString(id),
                                           escapeString(observedSystem.getId()));
                writer().writeSql(sql);
                observedSystemCount++;
            }
        }
    }

    /**
     * To update the activity sql code if vessel activity was changed while floating object import.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2612">Issue 2612</a>
     *
     * @param entity activity to process
     */
    public void updateVesselActivity(Activity entity) {
        if (FloatingObjectReader.CHANGED_VESSEL_ACTIVITY_CODE_BY_FLOATING_OBJECT.containsValue(entity.getVesselActivity().getCode())) {
            String id = entity.getId();
            String vesselActivityId = entity.getVesselActivity().getId();
            String sql = String.format("UPDATE ps_logbook.activity SET vesselActivity = %1$s WHERE topiaId = %2$s;",
                                       escapeString(vesselActivityId),
                                       escapeString(id));
            writer().writeSql(sql);
        }
    }

    @Override
    public void toResult(Map<String, Integer> resultBuilder) {
        super.toResult(resultBuilder);
        toResult(resultBuilder, ObservedSystem.class, observedSystemCount);
    }
}
