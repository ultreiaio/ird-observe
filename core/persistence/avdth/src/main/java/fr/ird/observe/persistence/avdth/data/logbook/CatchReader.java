package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.logbook.Catch;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.ps.common.WeightCategory;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 26/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class CatchReader extends DataReader<Catch> {

    @AutoService(DataQueryDefinition.class)
    public static class CatchTableReader extends DataQueryDefinition {
        /**
         * create table CAPT_ELEM (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * D_ACT TIMESTAMP not null,
         * N_ACT SMALLINT not null,
         * N_CAPT INTEGER identity,
         * C_ESP SMALLINT not null,
         * C_CAT_T SMALLINT not null,
         * V_POIDS_CAPT NUMERIC(100,7) not null,
         * primary key (C_BAT, D_DBQ, D_ACT, N_ACT, N_CAPT),
         * constraint CAPT_ELEM_FK_ACTIVITE_R_CAPT_ELEM foreign key (C_BAT, D_DBQ, D_ACT, N_ACT) references CAT_TAILLE (C_BAT, D_DBQ, D_ACT, N_ACT) on update cascade,
         * constraint CAPT_ELEM_FK_CAT_TAIL_R_CAPT_ELEM foreign key (C_ESP, C_CAT_T) references CAT_TAILLE );
         */
        public CatchTableReader() {
            super(4);
        }

        @Override
        public String select() {
            return  /* CAPT_ELEM_01 */   " CAPT_ELEM.C_BAT," +
                    /* CAPT_ELEM_02 */   " CAPT_ELEM.D_DBQ," +
                    /* CAPT_ELEM_03 */   " CAPT_ELEM.D_ACT," +
                    /* CAPT_ELEM_04 */   " CAPT_ELEM.N_ACT," +
                    /* CAPT_ELEM_05 */   " CAPT_ELEM.N_CAPT," +
                    /* CAPT_ELEM_06 */   " CAPT_ELEM.C_ESP," +
                    /* CAPT_ELEM_07 */   " CAPT_ELEM.C_CAT_T," +
                    /* CAPT_ELEM_08 */   " CAPT_ELEM.V_POIDS_CAPT";
        }

        @Override
        public String from() {
            return "CAPT_ELEM";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, D_ACT, N_ACT, N_CAPT";
        }
    }

    public CatchReader(ImportEngine context) {
        super(context);
    }

    @Override
    public Catch read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        Catch entity = newEntity(Catch.SPI);
        entity.setHomeId(resultSet.getString(5));
        String originalSpeciesCode = resultSet.getString(6);
        Species species = dataContext.getSpecies(originalSpeciesCode);
        entity.setSpecies(species);

        String weightCategoryCode = resultSet.getString(7);
        WeightCategory weightCategory = dataContext.getCatchWeightCategory(species, weightCategoryCode);
        entity.setWeightCategory(weightCategory);

        entity.setSpeciesFate(dataContext.getSpeciesFate(originalSpeciesCode));
        entity.setWeight(resultSet.getFloat(8));
        entity.setWeightMeasureMethod(dataContext.getWeightMeasureMethod());
        entity.setWell(null);
        entity.setComment(null);
        return entity;
    }
}
