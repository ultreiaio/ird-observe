package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.logbook.Catch;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.persistence.avdth.data.DataWriter;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toId;

/**
 * Created on 26/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class CatchWriter extends DataWriter<Catch, CatchReader> {

    public CatchWriter(ImportEngine context, CatchReader reader) {
        super(context, reader);
    }

    @Override
    public void write(Catch entity, String parentId, int parentCount) {
        Species species = entity.getSpecies();
        if (species.isNotPersisted()) {
            badSpecies.add(species.getCode() + "-" + species.getLabel2());
            return;
        }
        String id = entity.getTopiaId();
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_logbook.catch" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " homeId," +
                                           /*  6 */  " species," +
                                           /*  7 */  " speciesFate," +
                                           /*  8 */  " weightCategory," +
                                           /*  9 */  " weight," +
                                           /* 10 */  " weightMeasureMethod, " +
                                           /* 11 */  " well," +
                                           /* 12 */  " comment," +
                                           /* 13 */  " activity," +
                                           /* 14 */  " activity_idx)" +
                                           " VALUES (" +
                                           /*  1 */       " '%1$s'," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s," +
                                           /*  7 */       " %6$s," +
                                           /*  8 */       " %7$s," +
                                           /*  9 */       " %8$s," +
                                           /* 10 */       " %9$s," +
                                           /* 11 */       " %10$s," +
                                           /* 12 */       " %11$s," +
                                           /* 13 */       " %12$s," +
                                           /* 14 */       " %13$s);",
                /*  1 */  id,
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  entity.getHomeId(),
                /*  6 */  toId(species),
                /*  7 */  toId(entity.getSpeciesFate()),
                /*  8 */  toId(entity.getWeightCategory()),
                /*  9 */  roundFloat4(entity.getWeight()),
                /* 10 */  toId(entity.getWeightMeasureMethod()),
                /* 11 */  escapeString(entity.getWell()),
                /* 12 */  escapeComment(entity.getComment()),
                /* 13 */  escapeString(parentId),
                /* 14 */  parentCount
        );
        writer().writeSql(sql);
    }
}
