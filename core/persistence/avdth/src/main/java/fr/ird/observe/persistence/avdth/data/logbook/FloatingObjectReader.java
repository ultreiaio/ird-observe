package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.data.ps.logbook.FloatingObject;
import fr.ird.observe.entities.data.ps.logbook.FloatingObjectPart;
import fr.ird.observe.entities.data.ps.logbook.TransmittingBuoy;
import fr.ird.observe.entities.referential.ps.common.ObjectMaterial;
import fr.ird.observe.entities.referential.ps.common.ObjectOperation;
import fr.ird.observe.entities.referential.ps.common.ObservedSystem;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOperation;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOwnership;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyType;
import fr.ird.observe.entities.referential.ps.common.VesselActivity;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created on 30/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class FloatingObjectReader extends DataReader<FloatingObject> {
    /**
     * To map ObServe vessel activity when in Avdth we found a Floating object to persist.
     * <p>
     * Keys are current ObServe vessel activity code used, values are the real vessel activity code to use (which allow fad)
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2612">Issue 2612</a>
     */
    public static final Map<String, String> CHANGED_VESSEL_ACTIVITY_CODE_BY_FLOATING_OBJECT = Map.of("1", "101",
                                                                                                     "2", "102");
    private static final Logger log = LogManager.getLogger(FloatingObjectReader.class);
    /**
     * To get FloatingObject.objectOperation code from avdth ACTIVITE.C_OPERA.
     */
    public static final Map<String, String> DCP_OBJECT_OPERATION_CODE_MAPPING = ImmutableMap.<String, String>builder()
            .put("0", "99")
            .put("1", "99")
            .put("2", "99")
            .put("3", "99")
            .put("4", "99")
            .put("5", "1")
            .put("6", "4")
            .put("7", "99")
            .put("8", "99")
            .put("9", "99")
            .put("10", "99")
            .put("12", "99")
            .put("13", "99")
            .put("14", "99")
            .put("15", "99")
            .put("22", "4")
            .put("23", "1")
            .put("24", "4")
            .put("25", "2")
            .put("26", "2")
            .put("29", "2")
            .put("30", "2")
            .put("31", "8")
            .put("32", "8")
            .put("33", "11")
            .put("34", "99")
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2568
            .put("40", "11")
            .put("41", "11")
            .build();
    /**
     * To get TransmittingBuoy.TransmittingBuoyOperation code from avdth ACTIVITE.C_OPERA code.
     * <p>
     * If for a given {@code ACTIVITE.C_OPERA}, no value is found here, this means that there is no buoy to create.
     */
    public static final Map<String, String> TRANSMITTING_BUOY_OPERATION_CODE_MAPPING = ImmutableMap.<String, String>builder()
            .put("0", "99") // Inconnu         | Inconnu
            .put("1", "99") // Inconnu         | Inconnu
            .put("2", "99") // Inconnu         | Inconnu
            .put("3", "99") // Inconnu         | Inconnu
            .put("4", "99") // Inconnu         | Inconnu
            .put("5", "3")  // Mise à l'eau    | null ou 3 Mise à l'eau
            .put("6", "2")  // Retrait         | null ou 2 Récupération
            .put("14", "99")// Inconnu         | Inconnu
            .put("22", "99") // Normalement il ne devrait pas y avoir de balise
            .put("23", "3") // Mise à l'eau    | 3 Mise à l'eau
            .put("24", "2") // Retrait         | 2 Récupération
            .put("25", "3") // Visite          | 3 Mise à l'eau
            .put("26", "2") // Visite          | 2 Récupération
            .put("29", "1") // Visite          | 1 Visite
            .put("30", "99") // Inconnu        | Inconnu
            .put("31", "99") // Modif ou renfor| Inconnu
            .put("32", "3") // Modif ou renfor | 3 Mise à l'eau
            .put("33", "4") // Perte           | 4 Perte signal
            .put("34", "2") // Retrait         | 2 Récupération
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2568
            .put("40", "4") // Perte           | 4 Perte signal
            .put("41", "5") // Arret commandé de la transmission  | 5 Fin d'utilisation
            .build();
    /**
     * To get TransmittingBuoy.TransmittingBuoyType code from avdth ACTIVITE.C_TYP_BALISE code.
     */
    public static final Map<String, String> TRANSMITTING_BUOY_TYPE_CODE_MAPPING = ImmutableMap.<String, String>builder()
            .put("1", "1")
            .put("2", "2")
            .put("3", "3")
            .put("4", "4")
            .put("5", "5")
            .put("6", "6")
            .put("7", "7")
            .put("8", "8")
            .put("9", "9")
            .put("10", "10")
            .put("11", "90")
            .put("12", "21")
            .put("13", "22")
            .put("14", "23")
            .put("15", "24")
            .put("16", "25")
            .put("17", "26")
            .put("18", "27")
            .put("19", "28")
            .put("20", "41")
            .put("21", "42")
            .put("22", "43")
            .put("23", "44")
            .put("24", "45")
            .put("25", "46")
            .put("26", "47")
            .put("27", "100")
            .put("28", "61")
            .put("29", "62")
            .put("30", "63")
            .put("31", "64")
            .put("32", "65")
            .put("33", "66")
            .put("34", "67")
            .put("35", "68")
            .put("36", "69")
            .put("37", "70")
            .put("38", "71")
            .put("39", "3")
            .put("40", "91")
            .put("98", "99")
            .put("99", "99")
            .build();
    /**
     * To get TransmittingBuoy.transmittingBuoyOwnership code from avdth ACTIVITE.F_PROP_BALISE code.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2569">issue 2569</a>
     */
    public static final Map<String, String> TRANSMITTING_BUOY_OWNERSHIP_CODE_MAPPING = Map.of(
            "1", "0",
            "2", "3");
    /**
     * To get associated observedSystem code from avdth ACTIVITE.C_TYP_OBJET code.
     */
    public static final Map<String, String> OBSERVED_SYSTEM_FROM_OBJECT_OPERATION_CODE_MAPPING = ImmutableMap.<String, String>builder()
            .put("1", "20")
            .put("2", "20")
            .put("3", "20")
            .put("4", "18")
            .put("5", "12")
            .put("6", "11")
            .put("7", "9")
            .put("9", "20")
            .put("10", "20")
            .put("11", "20")
            .put("12", "20")
            .put("13", "20")
            .build();
    /**
     * Observed system from {@code ACT_ASSOC} which can create a Floating object.
     */
    public static final Set<String> OBSERVED_SYTEM_CODES_WITH_DCP = Set.of("20", "21", "22", "23", "24", "25", "81");

    private final MutableInt floatingObjectPartCount = new MutableInt();
    private final MutableInt transmittingBuoyCount = new MutableInt();

    private final Map<String, MutableInt> badVesselActivityCodes = new TreeMap<>();

    public FloatingObjectReader(ImportEngine context) {
        super(context);
    }

    @Override
    public FloatingObject read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {

        // Get the current activity where to add the optional floating object
        Activity activity = dataContext.getActivity();

        // Get his vessel activity
        VesselActivity vesselActivity = activity.getVesselActivity();

        // Avdth observed system codes associated to the current activity (need them to compute ex nihilo floating object)
        Set<String> avdthObservedSystemCodes = dataContext.getAvdthObservedSystemCodes();

        // We need to get the original vessel activity code from AVDTH (to compute some mapping)
        String vesselActivityCode = resultSet.getString(13);

        // Is the floating object can be created by the observed systems?
        boolean floatingObjectCreatedByObservedSystem = avdthObservedSystemCodes.stream().anyMatch(OBSERVED_SYTEM_CODES_WITH_DCP::contains);

        // Is the current vessel activity accept to create Floating object?
        boolean vesselActivityAllowFad = vesselActivity.isAllowFad();

        // Get object type code
        String objectTypeCode = resultSet.getString(28);
        if (objectTypeCode == null) {
            // If null, then consider it as not found in avdth
            objectTypeCode = "999";
        }

        // Is floating object exists in avdth?
        boolean floatingObjectExistsInAvdth = !objectTypeCode.equals("999");

        String buoyTypeCode = resultSet.getString(31);
        if (buoyTypeCode == null) {
            // If null, then consider it as not found in avdth
            buoyTypeCode = "999";
        }
        // IS buoy exists in avdth?
        boolean buoyExistsInAvdth = !"999".equals(buoyTypeCode);

        if (!floatingObjectExistsInAvdth && !buoyExistsInAvdth && !floatingObjectCreatedByObservedSystem) {
            // If Floating object does not exist in avdth,
            // neither the buoy exist in avdth,
            // neither need to be created by observed system,
            // Do not create the floating object (no log is required here: we are on an activity with no floating object)
            return null;
        }

        if (!vesselActivityAllowFad) {

            String observeVesselActivityCode = activity.getVesselActivity().getCode();
            String realObserveVesselActivityCode = CHANGED_VESSEL_ACTIVITY_CODE_BY_FLOATING_OBJECT.get(observeVesselActivityCode);
            if (realObserveVesselActivityCode == null) {

                // No extra mapping found for vessel activity, we will not import the floating object

                List<Object> activityPrimaryKey = ActivityReader.getActivityPk(resultSet);
                String activityPk = Query.primaryKeyString(activityPrimaryKey);
                String message = String.format("For AVDTH activity %s (ACTIVITE.C_OPERA %s), the ObServe vessel activity (code: %s, label: %s) does not allow FAD, we will not import it... (floatingObjectExistsInAvdth? %b, buoyExistsInAvdth? %b, floatingObjectCreatedByObservedSystem? %b).",
                                               activityPk,
                                               vesselActivityCode,
                                               vesselActivity.getCode(),
                                               vesselActivity.getLabel1(),
                                               floatingObjectExistsInAvdth,
                                               buoyExistsInAvdth,
                                               floatingObjectCreatedByObservedSystem);
                log.warn(message);
                if (floatingObjectExistsInAvdth || buoyExistsInAvdth) {
                    // this will reject import (except if force import option is set on)
                    addMessage(message);
                    badVesselActivityCodes.computeIfAbsent(vesselActivityCode, MutableInt::new).increment();
                }

                // do not create the floating object
                return null;
            }

            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2612
            switch (realObserveVesselActivityCode) {
                case "101":
                    // Use vessel activity 101
                    activity.setVesselActivity(dataContext.getVesselActivity101());
                    break;
                case "102":
                    // Use vessel activity 102
                    activity.setVesselActivity(dataContext.getVesselActivity102());
                    break;
            }
        }

        FloatingObject entity = newEntity(FloatingObject.SPI);

        // Get floating object operation
        ObjectOperation objectOperation = getObjectOperationByVesselActivityCode(dataContext, vesselActivityCode);
        entity.setObjectOperation(Objects.requireNonNull(objectOperation, String.format("No mapping for FloatingObject.objectOperation from ACTIVITE.C_OPERA: %s", vesselActivityCode)));

        ObservedSystem observedSystem = getObservedSystem(dataContext, objectOperation, objectTypeCode, floatingObjectExistsInAvdth, buoyExistsInAvdth);
        if (observedSystem != null) {
            activity.addObservedSystem(observedSystem);
        }
        boolean addBuoyFromObservedSystem = addFloatingObjectMaterials(dataContext,
                                                                       resultSet,
                                                                       vesselActivityCode,
                                                                       objectTypeCode,
                                                                       avdthObservedSystemCodes,
                                                                       objectOperation,
                                                                       entity,
                                                                       floatingObjectExistsInAvdth,
                                                                       buoyExistsInAvdth);

        TransmittingBuoy transmittingBuoy = null;
        if (buoyExistsInAvdth) {
            transmittingBuoy = getBuoyFromAvdthActivity(dataContext, resultSet, buoyTypeCode, vesselActivityCode, objectOperation, activity);
        } else if (addBuoyFromObservedSystem) {
            transmittingBuoy = getBuoyFromObservedSystem(dataContext);
        }
        if (transmittingBuoy != null) {
            // only add buoy if buoy type known
            entity.addTransmittingBuoy(transmittingBuoy);
        }
        return entity;
    }

    private boolean addFloatingObjectMaterials(ImportDataContext dataContext,
                                               ResultSet resultSet,
                                               String vesselActivityCode,
                                               String objectTypeCode,
                                               Set<String> avdthObservedSystemCodes,
                                               ObjectOperation objectOperation,
                                               FloatingObject entity,
                                               boolean floatingObjectExistsInAvdth,
                                               boolean buoyExistsInAvdth) throws SQLException {
        boolean whenArriving = objectOperation.isWhenArriving();
        boolean whenLeaving = objectOperation.isWhenLeaving();

        // to add default material if there is a declared buoy (but no floating object declared)
        // and no object material created by ObservedSystem, we will then add a standalone buoy
        boolean addDefaultObjectMaterialOnUnknownObjectType = true;
        // should create a buoy from ObservedSystem
        boolean addBuoyFromObservedSystem = false;
        Set<String> objectMaterialUsed = new TreeSet<>();

        if (floatingObjectExistsInAvdth) {
            Object dcpEcoCode = resultSet.getObject(29);
            if (dcpEcoCode != null && Objects.equals(2, dcpEcoCode)) {
                // add material 4-1 (Biodegradable materials)
                addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialBiodegradable(), whenArriving, whenLeaving, entity);
            }
        }

        if (avdthObservedSystemCodes.contains("20")) {
            // add ObjectMaterial FOB
            addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialFOB(), whenArriving, whenLeaving, entity);
            addDefaultObjectMaterialOnUnknownObjectType = false;
        }
        if (avdthObservedSystemCodes.contains("21")) {
            // add ObjectMaterial 2-1-1 VNLOG
            addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialVNLOG(), whenArriving, whenLeaving, entity);
            addDefaultObjectMaterialOnUnknownObjectType = false;
        }
        if (avdthObservedSystemCodes.contains("22")) {
            // add ObjectMaterial 2-1-1 VNLOG + 1-1 DFAD
            addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialVNLOG(), whenArriving, whenLeaving, entity);
            addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialDFAD(), whenArriving, whenLeaving, entity);
            addDefaultObjectMaterialOnUnknownObjectType = false;
            addBuoyFromObservedSystem = true;
        }
        if (avdthObservedSystemCodes.contains("23")) {
            // add ObjectMaterial 2-2 ALOG
            addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialALOG(), whenArriving, whenLeaving, entity);
            addDefaultObjectMaterialOnUnknownObjectType = false;
        }
        if (avdthObservedSystemCodes.contains("24")) {
            // add ObjectMaterial 1-1 DFAD
            addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialDFAD(), whenArriving, whenLeaving, entity);
            addDefaultObjectMaterialOnUnknownObjectType = false;
            addBuoyFromObservedSystem = true;
        }
        if (avdthObservedSystemCodes.contains("25")) {
            // add ObjectMaterial 1-2 AFAD
            addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialAFAD(), whenArriving, whenLeaving, entity);
            addDefaultObjectMaterialOnUnknownObjectType = false;
        }
        if (avdthObservedSystemCodes.contains("81")) {
            // add ObjectMaterial 2-1-2-1 Carrion
            addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialCarrion(), whenArriving, whenLeaving, entity);
            addDefaultObjectMaterialOnUnknownObjectType = false;
        }
        if (vesselActivityCode.equals("34")) {
            // add ObjectMaterial 2-2-4-4
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2910
            addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialAlone(), whenArriving, whenLeaving, entity);
            addDefaultObjectMaterialOnUnknownObjectType = false;
        }
        if (floatingObjectExistsInAvdth) {
            // simple mapping
            ObjectMaterial objectMaterial = getObjectMaterialByObjectTypeCode(dataContext, objectTypeCode);
            if (objectMaterial != null) {
                addObjectMaterial(vesselActivityCode, objectMaterialUsed, objectMaterial, whenArriving, whenLeaving, entity);
            }
        } else {
            if (buoyExistsInAvdth && addDefaultObjectMaterialOnUnknownObjectType) {
                // add ObjectMaterial 2-2-4-4
                addObjectMaterial(vesselActivityCode, objectMaterialUsed, dataContext.getObjectMaterialAlone(), whenArriving, whenLeaving, entity);
            }
        }
        return addBuoyFromObservedSystem;
    }

    private TransmittingBuoy getBuoyFromAvdthActivity(ImportDataContext dataContext, ResultSet resultSet, String buoyTypeCode, String vesselActivityCode, ObjectOperation objectOperation, Activity activity) throws SQLException {
        TransmittingBuoyOperation transmittingBuoyOperation = getTransmittingBuoyOperationByVesselActivityCode(dataContext, vesselActivityCode);
        if (transmittingBuoyOperation == null) {
            List<Object> activityPrimaryKey = ActivityReader.getActivityPk(resultSet);
            String activityPk = Query.primaryKeyString(activityPrimaryKey);
            // Reject import, since we can not find the buoy operation from ACTIVITE.C_OPERA
            addMessage(String.format("For AVDTH activity: %s, could not find transmittingBuoyOperation from ACTIVITE.C_OPERA: %s", activityPk, vesselActivityCode));
            return null;
        }
        TransmittingBuoyType transmittingBuoyType = getTransmittingBuoyTypeByBuoyTypeCode(dataContext, buoyTypeCode);
        String buoyOwnershipCode = resultSet.getString(30);
        String buoyId = resultSet.getString(32);
        TransmittingBuoy transmittingBuoy = newEntity(TransmittingBuoy.SPI, transmittingBuoyCount);
        transmittingBuoy.setTransmittingBuoyOperation(transmittingBuoyOperation);
        transmittingBuoy.setTransmittingBuoyType(transmittingBuoyType);
        transmittingBuoy.setCode(buoyId);
        boolean addCoordinate = "11".equals(objectOperation.getCode());
        if (addCoordinate) {
            transmittingBuoy.setLatitude(activity.getLatitude());
            transmittingBuoy.setLongitude(activity.getLongitude());
        }
        TransmittingBuoyOwnership transmittingBuoyOwnership = getTransmittingBuoyOwnership(dataContext, buoyOwnershipCode);
        transmittingBuoy.setTransmittingBuoyOwnership(transmittingBuoyOwnership);
        return transmittingBuoy;
    }

    private TransmittingBuoy getBuoyFromObservedSystem(ImportDataContext dataContext) {
        // only add buoy if observed system ask for it (22 and 24)
        TransmittingBuoyType transmittingBuoyType = dataContext.getTransmittingBuoyType98();
        TransmittingBuoyOperation transmittingBuoyOperation = dataContext.getTransmittingBuoyOperation1();
        TransmittingBuoy transmittingBuoy = newEntity(TransmittingBuoy.SPI, transmittingBuoyCount);
        transmittingBuoy.setTransmittingBuoyOperation(transmittingBuoyOperation);
        transmittingBuoy.setTransmittingBuoyType(transmittingBuoyType);
        transmittingBuoy.setCode(null);
        return transmittingBuoy;
    }

    private void addObjectMaterial(String vesselActivityCode, Set<String> objectMaterialUsed, ObjectMaterial objectMaterial, boolean whenArriving, boolean whenLeaving, FloatingObject floatingObject) {
        if (!whenArriving && !whenLeaving) {
            throw new IllegalStateException(String.format("Can't have not whenArriving and not whenLeaving from ACTIVITE.C_OPERA: %s", vesselActivityCode));
        }
        if (!objectMaterialUsed.add(objectMaterial.getTopiaId())) {
            // already used
            return;
        }
        FloatingObjectPart floatingObjectPart = newEntity(FloatingObjectPart.SPI, floatingObjectPartCount);
        floatingObjectPart.setWhenArriving("" + whenArriving);
        floatingObjectPart.setWhenLeaving("" + whenLeaving);
        floatingObjectPart.setObjectMaterial(Objects.requireNonNull(objectMaterial));
        floatingObject.addFloatingObjectPart(floatingObjectPart);
    }

    public int getTransmittingBuoyCount() {
        return transmittingBuoyCount.intValue();
    }

    public int getFloatingObjectPartCount() {
        return floatingObjectPartCount.intValue();
    }

    public Map<String, MutableInt> getBadVesselActivityCodes() {
        return badVesselActivityCodes;
    }

    private ObjectOperation getObjectOperationByVesselActivityCode(ImportDataContext dataContext, String vesselActivityCode) {
        String objectOperationCode = DCP_OBJECT_OPERATION_CODE_MAPPING.get(vesselActivityCode);
        return dataContext.getObjectOperation(Objects.requireNonNull(objectOperationCode, String.format("Can't find objectOperation with ACTIVITE.C_OPERA: %s", vesselActivityCode)));
    }

    private ObservedSystem getObservedSystem(ImportDataContext dataContext,
                                             ObjectOperation objectOperation,
                                             String objectTypeCode,
                                             boolean floatingObjectExistsInAvdth,
                                             boolean buoyExistsInAvdth) {
        if (!floatingObjectExistsInAvdth) {
            if (buoyExistsInAvdth && !objectOperation.getCode().equals("1")) {
                return dataContext.getObservedSystem20();
            }
            return null;
        }
        String observedSystemCode = OBSERVED_SYSTEM_FROM_OBJECT_OPERATION_CODE_MAPPING.get(objectTypeCode);
        return dataContext.getObservedSystem(observedSystemCode);
    }

    private TransmittingBuoyOwnership getTransmittingBuoyOwnership(ImportDataContext dataContext, String buoyOwnershipCode) {
        if (buoyOwnershipCode == null) {
            return null;
        }
        String transmittingBuoyOwnershipCode = TRANSMITTING_BUOY_OWNERSHIP_CODE_MAPPING.get(buoyOwnershipCode);
        if (transmittingBuoyOwnershipCode == null) {
            return null;
        }
        return dataContext.getTransmittingBuoyOwnership(transmittingBuoyOwnershipCode);
    }

    private TransmittingBuoyOperation getTransmittingBuoyOperationByVesselActivityCode(ImportDataContext dataContext, String vesselActivityCode) {
        String transmittingBuoyOperationCode = TRANSMITTING_BUOY_OPERATION_CODE_MAPPING.get(vesselActivityCode);
        if (transmittingBuoyOperationCode == null) {
            return null;
        }
        return dataContext.getTransmittingBuoyOperation(transmittingBuoyOperationCode);
    }


    private TransmittingBuoyType getTransmittingBuoyTypeByBuoyTypeCode(ImportDataContext dataContext, String buoyTypeCode) {
        String transmittingBuoyTypeCode = TRANSMITTING_BUOY_TYPE_CODE_MAPPING.get(buoyTypeCode);
        if (transmittingBuoyTypeCode == null) {
            transmittingBuoyTypeCode = "99";
        }
        TransmittingBuoyType transmittingBuoyType = dataContext.getTransmittingBuoyType(transmittingBuoyTypeCode);
        return Objects.requireNonNull(transmittingBuoyType, String.format("Can't find transmittingBuoyType with code: %s", buoyTypeCode));
    }

    /**
     * To get objectMaterial associated by a DCP (or null) from avdth ACTIVITE.C_TYP_OBJET code.
     *
     * @param objectTypeCode ACTIVITE.C_TYP_OBJET
     * @return material or null
     */
    private ObjectMaterial getObjectMaterialByObjectTypeCode(ImportDataContext dataContext, String objectTypeCode) {
        switch (objectTypeCode) {
            case "1":
                return dataContext.getObjectMaterialAFAD();
            case "2":
                return dataContext.getObjectMaterialDFAD();
            case "3":
                return dataContext.getObjectMaterialLOG();
            case "9":
                return dataContext.getObjectMaterialFOB();
            case "10":
                return dataContext.getObjectMaterialFALOG();
            case "11":
                return dataContext.getObjectMaterialHALOG();
            case "12":
                return dataContext.getObjectMaterialANLOG();
            case "13":
                return dataContext.getObjectMaterialVNLOG();
        }
        return null;
    }
}
