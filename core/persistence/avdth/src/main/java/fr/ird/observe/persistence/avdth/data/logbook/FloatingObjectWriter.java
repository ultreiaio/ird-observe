package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.logbook.FloatingObject;
import fr.ird.observe.entities.data.ps.logbook.FloatingObjectPart;
import fr.ird.observe.entities.data.ps.logbook.TransmittingBuoy;
import fr.ird.observe.persistence.avdth.data.DataWriter;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.util.Map;

import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toId;

/**
 * Created on 30/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class FloatingObjectWriter extends DataWriter<FloatingObject, FloatingObjectReader> {

    public FloatingObjectWriter(ImportEngine context, FloatingObjectReader reader) {
        super(context, reader);
    }

    @Override
    public void write(FloatingObject entity, String parentId, int parentCount) {
        if (entity == null) {
            // maybe we did not found a dcp on the activity row, can happen
            return;
        }
        String id = entity.getTopiaId();
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_logbook.floatingObject" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " homeId," +
                                           /*  6 */  " comment," +
                                           /*  7 */  " objectOperation," +
                                           /*  8 */  " supportVesselName," +
                                           /*  9 */  " activity)" +
                                           " VALUES (" +
                                           /*  1 */       " %1$s," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s," +
                                           /*  7 */       " %6$s," +
                                           /*  8 */       " %7$s," +
                                           /*  9 */       " %8$s" +
                                           ");",
                /*  1 */  escapeString(id),
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  escapeString(entity.getHomeId()),
                /*  6 */  escapeComment(entity.getComment()),
                /*  7 */  toId(entity.getObjectOperation()),
                /*  8 */  escapeString(entity.getSupportVesselName()),
                /*  9 */  escapeString(parentId)
        );
        writer().writeSql(sql);
        if (entity.isFloatingObjectPartNotEmpty()) {
            entity.getFloatingObjectPart().forEach(floatingObjectPart -> write(floatingObjectPart, id));
        }
        if (entity.isTransmittingBuoyNotEmpty()) {
            entity.getTransmittingBuoy().forEach(transmittingBuoy -> write(transmittingBuoy, id));
        }
    }

    protected void write(FloatingObjectPart floatingObjectPart, String floatingObjectId) {
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_logbook.floatingObjectPart" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " homeId," +
                                           /*  6 */  " whenArriving," +
                                           /*  7 */  " whenLeaving," +
                                           /*  8 */  " objectMaterial," +
                                           /*  9 */  " floatingObject)" +
                                           " VALUES (" +
                                           /*  1 */       " %1$s," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s," +
                                           /*  7 */       " %6$s," +
                                           /*  8 */       " %7$s," +
                                           /*  9 */       " %8$s" +
                                           ");",
                /*  1 */  escapeString(floatingObjectPart.getTopiaId()),
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  escapeString(floatingObjectPart.getHomeId()),
                /*  6 */  escapeString(floatingObjectPart.getWhenArriving()),
                /*  7 */  escapeString(floatingObjectPart.getWhenLeaving()),
                /*  8 */  toId(floatingObjectPart.getObjectMaterial()),
                /* 11 */  escapeString(floatingObjectId)
        );
        writer().writeSql(sql);
    }

    protected void write(TransmittingBuoy transmittingBuoy, String floatingObjectId) {
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_logbook.transmittingBuoy" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " homeId," +
                                           /*  6 */  " comment," +
                                           /*  7 */  " code," +
                                           /*  8 */  " transmittingBuoyOwnership," +
                                           /*  9 */  " transmittingBuoyType," +
                                           /* 10 */  " transmittingBuoyOperation," +
                                           /* 11 */  " country," +
                                           /* 12 */  " vessel," +
                                           /* 13 */  " latitude," +
                                           /* 14 */  " longitude," +
                                           /* 15 */  " floatingObject)" +
                                           " VALUES (" +
                                           /*  1 */       " %1$s," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s," +
                                           /*  7 */       " %6$s," +
                                           /*  8 */       " %7$s," +
                                           /*  9 */       " %8$s," +
                                           /* 10 */       " %9$s," +
                                           /* 11 */       " %10$s," +
                                           /* 12 */       " %11$s," +
                                           /* 13 */       " %12$s," +
                                           /* 14 */       " %13$s," +
                                           /* 15 */       " %14$s" +
                                           ");",
                /*  1 */  escapeString(transmittingBuoy.getTopiaId()),
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  escapeString(transmittingBuoy.getHomeId()),
                /*  6 */  escapeComment(transmittingBuoy.getComment()),
                /*  7 */  escapeString(transmittingBuoy.getCode()),
                /*  8 */  toId(transmittingBuoy.getTransmittingBuoyOwnership()),
                /*  9 */  toId(transmittingBuoy.getTransmittingBuoyType()),
                /* 10 */  toId(transmittingBuoy.getTransmittingBuoyOperation()),
                /* 11 */  toId(transmittingBuoy.getCountry()),
                /* 12 */  toId(transmittingBuoy.getVessel()),
                /* 13 */  roundFloat4(transmittingBuoy.getLatitude()),
                /* 14 */  roundFloat4(transmittingBuoy.getLongitude()),
                /* 15 */  escapeString(floatingObjectId)
        );
        writer().writeSql(sql);
    }

    @Override
    public void flush() {
        super.flush();
        flush(FloatingObjectPart.class, getReader().getFloatingObjectPartCount());
        flush(TransmittingBuoy.class, getReader().getTransmittingBuoyCount());
    }

    @Override
    public void toResult(Map<String, Integer> resultBuilder) {
        super.toResult(resultBuilder);
        toResult(resultBuilder, FloatingObjectPart.class, getReader().getFloatingObjectPartCount());
        toResult(resultBuilder, TransmittingBuoy.class, getReader().getTransmittingBuoyCount());
        getReader().getBadVesselActivityCodes().forEach((k,v)-> resultBuilder.put(String.format("Bad Vessel activity used to build FAD from ACTIVITE.C_OPERA %s used but this vessel activity does not allow fad in ObServe.", k), v.getValue()));
    }
}

