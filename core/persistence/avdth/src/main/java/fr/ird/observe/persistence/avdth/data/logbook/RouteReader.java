package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.logbook.Route;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created on 25/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class RouteReader extends DataReader<Route> {

    public RouteReader(ImportEngine context) {
        super(context);
    }

    @Override
    public Route read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        Route entity = newEntity(Route.SPI);
        entity.setDate(resultSet.getDate(3));
        entity.setTimeAtSea(0);
        entity.setFishingTime(0);
        return entity;
    }

    public Route create(Date date) throws SQLException {
        Route entity = newEntity(Route.SPI);
        entity.setDate(date);
        entity.setTimeAtSea(0);
        entity.setFishingTime(0);
        return entity;
    }

    public void readExtra(Route entity, ResultSet resultSet) throws SQLException {
        Object timeAtSea = resultSet.getObject(11);
        if (timeAtSea != null) {
            entity.setTimeAtSea(entity.getTimeAtSea() + (int) timeAtSea);
        }
        Object fishingTime = resultSet.getObject(12);
        if (fishingTime != null) {
            entity.setFishingTime(entity.getFishingTime() + (int) fishingTime);
        }
    }
}
