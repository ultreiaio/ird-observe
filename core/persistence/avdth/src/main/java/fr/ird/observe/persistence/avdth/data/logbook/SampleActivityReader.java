package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.data.ps.logbook.ActivityImpl;
import fr.ird.observe.entities.data.ps.logbook.SampleActivity;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created on 30/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleActivityReader extends DataReader<SampleActivity> {

    private static final Logger log = LogManager.getLogger(SampleActivityReader.class);

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(DataQueryDefinition.class)
    public static class SampleActivityTableReader extends DataQueryDefinition {

        /**
         * create table ECH_CALEE (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * N_ECH SMALLINT not null,
         * D_ACT TIMESTAMP not null,
         * N_ACT SMALLINT not null,
         * C_ZONE_GEO SMALLINT constraint ECH_CALEE_FK_ZONE_GEO_R_ECH_CALEE references ZONE_GEO,
         * Q_ACT SMALLINT,
         * V_LAT SMALLINT,
         * V_LON SMALLINT,
         * C_TBANC SMALLINT not null,
         * V_POND NUMERIC(100,7) not null,
         * primary key (C_BAT, D_DBQ, N_ECH, D_ACT, N_ACT),
         * constraint ECH_CALEE_FK_ECHANTIL_R_ECH_CALEE foreign key (C_BAT, D_DBQ, N_ECH) references ECHANTILLON on update cascade );
         */
        public SampleActivityTableReader() {
            super(3);
        }

        @Override
        public String select() {
            return  /*  ECH_CALEE_01 */ " C_BAT," +
                    /*  ECH_CALEE_02 */ " D_DBQ," +
                    /*  ECH_CALEE_03 */ " N_ECH," +
                    /*  ECH_CALEE_04 */ " D_ACT," +
                    /*  ECH_CALEE_05 */ " N_ACT," +
                    /*  ECH_CALEE_06 */ " C_ZONE_GEO," +
                    /*  ECH_CALEE_07 */ " Q_ACT," +
                    /*  ECH_CALEE_08 */ " V_LAT," +
                    /*  ECH_CALEE_09 */ " V_LON," +
                    /*  ECH_CALEE_10 */ " C_TBANC," +
                    /*  ECH_CALEE_11 */ " V_POND";
        }

        @Override
        public String from() {
            return "ECH_CALEE";
        }

        @Override
        public String innerJoin() {
            return "MAREE ON MAREE.C_BAT = ECH_CALEE.C_BAT AND MAREE.D_DBQ = ECH_CALEE.D_DBQ";
        }

        @Override
        protected String whereMissing() {
            return " NOT EXISTS( SELECT 'x' FROM MAREE WHERE MAREE.C_BAT = ECH_CALEE.C_BAT AND MAREE.D_DBQ = ECH_CALEE.D_DBQ )";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_ECH, D_ACT, N_ACT";
        }
    }

    public static List<Object> getActivityPk(ResultSet sampleSetRow) throws SQLException {
        LinkedList<Object> result = new LinkedList<>();
        result.add(sampleSetRow.getObject(4));
        result.add(sampleSetRow.getObject(5));
        return result;
    }

    public SampleActivityReader(ImportEngine context) {
        super(context);
    }

    @Override
    public SampleActivity read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        List<Object> activityPrimaryKey = getActivityPk(resultSet);
        String activityPk = Query.primaryKeyString(activityPrimaryKey);
        Map<String, String> activityPkToId = dataContext.getActivityPkToId();
        Activity activity;
        String activityId = activityPkToId.get(activityPk);
        if (activityId == null) {
            String samplePk = Query.primaryKeyString(Query.getPrimaryKey(resultSet, 1, 3));
            if (!dataContext.isCanCreateActivity()) {
                String message = String.format("Sample [%20s], not allowed to create missing activity and could not find Activity [%20s]", samplePk, activityPk);
                log.debug(message);
                addMessage(message);
                return null;
            }
            // only on this case we can create missing route and activity (See #2070)
            log.debug(String.format("Sample [%20s], missing SampleActivity.activity %s, will create it.", samplePk, activityPrimaryKey));
            activity = context().createMissingActivityForSampleActivity(resultSet, activityPrimaryKey, activityPk);
        } else {
            activity = new ActivityImpl();
            activity.setTopiaId(activityId);
        }
        SampleActivity entity = newEntity(SampleActivity.SPI);
        float weightedWeight = resultSet.getFloat(11);
        entity.setWeightedWeight(weightedWeight);

        entity.setActivity(activity);
        return entity;
    }
}

