package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.logbook.Sample;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 26/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleReader extends DataReader<Sample> {

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(DataQueryDefinition.class)
    public static class SampleTableReader extends DataQueryDefinition {

        /**
         * create table ECHANTILLON (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * N_ECH SMALLINT not null,
         * C_QUAL_ECH SMALLINT default 9 not null constraint ECHANTILLON_FK_QUAL_ECH_R_ECHANTILLON references QUAL_ECH,
         * C_TYP_ECH SMALLINT default 1 not null constraint ECHANTILLON_FK_TYPE_ECH_R_ECHANTILLON references TYPE_ECHANT,
         * C_PORT_DBQ SMALLINT not null,
         * N_CUVE SMALLINT not null,
         * F_POS_CUVE SMALLINT not null,
         * F_S_ECH SMALLINT default 0 not null,
         * V_POIDS_M10 NUMERIC(100,7) not null,
         * V_POIDS_P10 NUMERIC(100,7) not null,
         * V_POIDS_ECH NUMERIC(100,7) not null,
         * primary key (C_BAT, D_DBQ, N_ECH) );
         */
        public SampleTableReader() {
            super(3);
        }

        @Override
        public String select() {
            return  /* ECHANTILLON_01 */   " C_BAT," +
                    /* ECHANTILLON_02 */   " D_DBQ," +
                    /* ECHANTILLON_03 */   " N_ECH," +
                    /* ECHANTILLON_04 */   " C_QUAL_ECH," +
                    /* ECHANTILLON_05 */   " C_TYP_ECH," +
                    /* ECHANTILLON_06 */   " C_PORT_DBQ," +
                    /* ECHANTILLON_07 */   " N_CUVE," +
                    /* ECHANTILLON_08 */   " F_POS_CUVE," +
                    /* ECHANTILLON_09 */   " F_S_ECH," +
                    /* ECHANTILLON_10 */   " V_POIDS_M10," +
                    /* ECHANTILLON_11 */   " V_POIDS_P10," +
                    /* ECHANTILLON_12 */   " V_POIDS_ECH";
        }

        @Override
        public String from() {
            return "ECHANTILLON";
        }

        @Override
        public String innerJoin() {
            return "MAREE ON MAREE.C_BAT = ECHANTILLON.C_BAT AND MAREE.D_DBQ = ECHANTILLON.D_DBQ";
        }

        @Override
        protected String whereMissing() {
            return " NOT EXISTS( SELECT 'x' FROM MAREE WHERE MAREE.C_BAT = ECHANTILLON.C_BAT AND MAREE.D_DBQ = ECHANTILLON.D_DBQ )";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_ECH, N_CUVE, F_POS_CUVE";
        }
    }

    public SampleReader(ImportEngine context) {
        super(context);
    }

    @Override
    public Sample read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        Sample entity = newEntity(Sample.SPI);
        entity.setNumber(resultSet.getInt(3));
        entity.setSampleQuality(dataContext.getSampleQuality(resultSet.getString(4)));
        entity.setSampleType(dataContext.getSampleType(resultSet.getString(5)));

        String wellId = WellReader.getWellId(resultSet, 7);
        entity.setWell(wellId);
        entity.setSuperSample(resultSet.getInt(9) == 1);
        entity.setSmallsWeight(((Number) resultSet.getObject(10)).floatValue());
        entity.setBigsWeight(((Number) resultSet.getObject(11)).floatValue());
        entity.setTotalWeight(((Number) resultSet.getObject(12)).floatValue());
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2918
        boolean smallsWeightNull = isNull(entity.getSmallsWeight());
        boolean bigsWeightNull = isNull(entity.getBigsWeight());
        boolean totalWeightNull = isNull(entity.getTotalWeight());
        if (smallsWeightNull && bigsWeightNull && totalWeightNull) {
            entity.setSmallsWeight(null);
            entity.setBigsWeight(null);
            entity.setTotalWeight(0f);
            return entity;
        }
        if (!smallsWeightNull || !bigsWeightNull) {
            if (totalWeightNull) {
                entity.setTotalWeight(null);
            }
            if (smallsWeightNull) {
                entity.setSmallsWeight(0f);
            }
            if (bigsWeightNull) {
                entity.setBigsWeight(0f);
            }
            return entity;
        }
        // now we have smallsWeightNull && bigsWeightNull
        entity.setSmallsWeight(null);
        entity.setBigsWeight(null);
        return entity;
    }

    private static boolean isNull(Float value) {
        return Math.abs(value) < 0.00001f;
    }
}
