package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.logbook.SampleSpeciesMeasure;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 26/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleSpeciesMeasureReader extends DataReader<SampleSpeciesMeasure> {

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(DataQueryDefinition.class)
    public static class SampleSpeciesMeasureTableReader extends DataQueryDefinition {

        /**
         * create table ECH_FREQT
         * (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * N_ECH SMALLINT not null,
         * N_S_ECH SMALLINT default 0 not null,
         * C_ESP SMALLINT not null,
         * F_LDLF SMALLINT not null,
         * V_LONG NUMERIC(100,7) not null,
         * V_EFF SMALLINT not null,
         * primary key (C_BAT, D_DBQ, N_ECH, N_S_ECH, C_ESP, F_LDLF, V_LONG),
         * constraint ECH_FREQT_FK_ECH_ESP_R_ECH_FREQT foreign key (C_BAT, D_DBQ, N_ECH, N_S_ECH, C_ESP, F_LDLF) references ECH_ESP on update cascade on delete cascade );
         */
        public SampleSpeciesMeasureTableReader() {
            super(7);
        }

        @Override
        public String select() {
            return  /* ECH_FREQT_01 */   " C_BAT," +
                    /* ECH_FREQT_02 */   " D_DBQ," +
                    /* ECH_FREQT_03 */   " N_ECH," +
                    /* ECH_FREQT_04 */   " N_S_ECH," +
                    /* ECH_FREQT_05 */   " C_ESP," +
                    /* ECH_FREQT_06 */   " F_LDLF," +
                    /* ECH_FREQT_07 */   " V_LONG," +
                    /* ECH_FREQT_08 */   " V_EFF";
        }

        @Override
        public String from() {
            return "ECH_FREQT";
        }

        @Override
        public String innerJoin() {
            return "MAREE ON MAREE.C_BAT = ECH_FREQT.C_BAT AND MAREE.D_DBQ = ECH_FREQT.D_DBQ";
        }

        @Override
        protected String whereMissing() {
            return " NOT EXISTS( SELECT 'x' FROM MAREE WHERE MAREE.C_BAT = ECH_FREQT.C_BAT AND MAREE.D_DBQ = ECH_FREQT.D_DBQ )";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_ECH, N_S_ECH, C_ESP, F_LDLF, V_LONG";
        }
    }

    public SampleSpeciesMeasureReader(ImportEngine context) {
        super(context);
    }

    @Override
    public SampleSpeciesMeasure read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        SampleSpeciesMeasure entity = newEntity(SampleSpeciesMeasure.SPI);
        entity.setSizeClass(resultSet.getFloat(7));
        entity.setCount(resultSet.getInt(8));
        return entity;
    }
}
