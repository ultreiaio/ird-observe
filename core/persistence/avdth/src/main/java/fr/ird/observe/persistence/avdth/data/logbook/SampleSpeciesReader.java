package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.logbook.SampleSpecies;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 26/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleSpeciesReader extends DataReader<SampleSpecies> {

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(DataQueryDefinition.class)
    public static class SampleSpeciesTableReader extends DataQueryDefinition {

        /**
         * create table ECH_ESP (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * N_ECH SMALLINT not null,
         * N_S_ECH SMALLINT default 0 not null,
         * C_ESP SMALLINT not null constraint ECH_ESP_ESPECEECH_ESP references ESPECE on update cascade on delete cascade,
         * F_LDLF SMALLINT not null,
         * V_NB_MES SMALLINT not null,
         * V_NB_TOT SMALLINT not null,
         * primary key (C_BAT, D_DBQ, N_ECH, N_S_ECH, C_ESP, F_LDLF),
         * constraint ECH_ESP_CFK_ECHANTILLON_R_ECH_ESP foreign key (C_BAT, D_DBQ, N_ECH) references ECHANTILLON on update cascade );
         */
        public SampleSpeciesTableReader() {
            super(6);
        }

        @Override
        public String select() {
            return  /* ECH_ESP_01 */   " C_BAT," +
                    /* ECH_ESP_02 */   " D_DBQ," +
                    /* ECH_ESP_03 */   " N_ECH," +
                    /* ECH_ESP_04 */   " N_S_ECH," +
                    /* ECH_ESP_05 */   " C_ESP," +
                    /* ECH_ESP_06 */   " F_LDLF," +
                    /* ECH_ESP_07 */   " V_NB_MES," +
                    /* ECH_ESP_08 */   " V_NB_TOT";
        }

        @Override
        public String from() {
            return "ECH_ESP";
        }

        @Override
        public String innerJoin() {
            return "MAREE ON MAREE.C_BAT = ECH_ESP.C_BAT AND MAREE.D_DBQ = ECH_ESP.D_DBQ";
        }


        @Override
        protected String whereMissing() {
            return " NOT EXISTS( SELECT 'x' FROM MAREE WHERE MAREE.C_BAT = ECH_ESP.C_BAT AND MAREE.D_DBQ = ECH_ESP.D_DBQ )";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_ECH, N_S_ECH, C_ESP, F_LDLF";
        }
    }

    public SampleSpeciesReader(ImportEngine context) {
        super(context);
    }

    @Override
    public SampleSpecies read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        SampleSpecies entity = newEntity(SampleSpecies.SPI);
        //FIXME
//        entity.setComment(resultSet.getString(1));
        //FIXME
//        entity.setStartTime(resultSet.getString(1));
        //FIXME
//        entity.setEndTime(resultSet.getString(1));
        entity.setSubSampleNumber(resultSet.getInt(4));
        String originalSpeciesCode = resultSet.getString(5);
        Species species = dataContext.getSpecies(originalSpeciesCode);
        entity.setSpecies(species);
        int ldLf = resultSet.getInt(6);
        entity.setSizeMeasureType(dataContext.getSizeMeasureType(ldLf));
        Object measureCount = resultSet.getObject(7);
        if (measureCount != null) {
            entity.setMeasuredCount(Integer.valueOf(measureCount.toString()));
        }
        Object totalCount = resultSet.getObject(8);
        if (totalCount != null) {
            entity.setTotalCount(Integer.valueOf(totalCount.toString()));
        }
        return entity;
    }
}
