package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.logbook.SampleSpecies;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.persistence.avdth.data.DataWriter;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toId;
import static fr.ird.observe.spi.SqlConversions.toTime;

/**
 * Created on 26/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleSpeciesWriter extends DataWriter<SampleSpecies, SampleSpeciesReader> {

    public SampleSpeciesWriter(ImportEngine context, SampleSpeciesReader reader) {
        super(context, reader);
    }

    @Override
    public void write(SampleSpecies entity, String parentId, int parentCount) {
        Species species = entity.getSpecies();
        if (species.isNotPersisted()) {
            badSpecies.add(species.getCode() + "-" + species.getLabel2());
            return;
        }
        String id = entity.getTopiaId();
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_logbook.sampleSpecies" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " comment," +
                                           /*  6 */  " startTime," +
                                           /*  7 */  " endTime," +
                                           /*  8 */  " subSampleNumber," +
                                           /*  9 */  " measuredCount," +
                                           /* 10 */  " totalCount," +
                                           /* 11 */  " species," +
                                           /* 12 */  " sizeMeasureType," +
                                           /* 13 */  " sample," +
                                           /* 14 */  " sample_idx)" +
                                           " VALUES (" +
                                           /*  1 */       " %1$s," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s," +
                                           /*  7 */       " %6$s," +
                                           /*  8 */       " %7$s," +
                                           /*  9 */       " %8$s," +
                                           /* 10 */       " %9$s," +
                                           /* 11 */       " %10$s," +
                                           /* 12 */       " %11$s," +
                                           /* 13 */       " %12$s," +
                                           /* 14 */       " %13$s);",
                /*  1 */  escapeString(id),
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  escapeComment(entity.getComment()),
                /*  6 */  toTime(entity.getStartTime()),
                /*  7 */  toTime(entity.getEndTime()),
                /*  8 */  entity.getSubSampleNumber(),
                /*  9 */  entity.getMeasuredCount(),
                /* 10 */  entity.getTotalCount(),
                /* 11 */  toId(entity.getSpecies()),
                /* 12 */  toId(entity.getSizeMeasureType()),
                /* 13 */  escapeString(parentId),
                /* 14 */  parentCount);
        writer().writeSql(sql);
    }
}
