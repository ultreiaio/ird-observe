package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.data.ps.logbook.ActivityImpl;
import fr.ird.observe.entities.data.ps.logbook.WellActivity;
import fr.ird.observe.entities.data.ps.logbook.WellActivitySpecies;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.ps.common.WeightCategory;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created on 16/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public class WellActivityReader extends DataReader<WellActivity> {

    private static final Logger log = LogManager.getLogger(WellActivityReader.class);

    public static List<Object> getActivityPk(ResultSet wellSetRow) throws SQLException {
        LinkedList<Object> result = new LinkedList<>();
        result.add(wellSetRow.getObject(5));
        result.add(wellSetRow.getObject(6));
        return result;
    }

    @AutoService(DataQueryDefinition.class)
    public static class WellActivityTableReader extends DataQueryDefinition {

        /**
         * create table CUVE_CALEE (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * N_CUVE SMALLINT not null,
         * F_POS_CUVE SMALLINT not null,
         * N_CALESP INTEGER identity,
         * D_ACT TIMESTAMP,
         * N_ACT SMALLINT not null,
         * C_ESP SMALLINT not null constraint CUVE_CALEE_ESPECECUVE_CALEE references ESPECE on update cascade on delete cascade,
         * C_CAT_POIDS SMALLINT not null constraint CUVE_CALEE_FK_CAT_POID_R_CUVE_CALEE references CAT_POIDS,
         * V_POIDS NUMERIC(100,7),
         * V_NB SMALLINT,
         * primary key (C_BAT, D_DBQ, N_CUVE, F_POS_CUVE, N_CALESP),
         * constraint CUVE_CALEE_FK_CUVE_R_CUVE_CALEE foreign key (C_BAT, D_DBQ, N_CUVE, F_POS_CUVE) references ESPECE (C_BAT, D_DBQ, N_CUVE, F_POS_CUVE) on update cascade );
         */
        public WellActivityTableReader() {
            super(4);
        }

        @Override
        public String select() {
            return  /* CUVE_CALEE_01 */ " CUVE_CALEE.C_BAT," +
                    /* CUVE_CALEE_02 */ " CUVE_CALEE.D_DBQ," +
                    /* CUVE_CALEE_03 */ " CUVE_CALEE.N_CUVE," +
                    /* CUVE_CALEE_04 */ " CUVE_CALEE.F_POS_CUVE," +
                    /* CUVE_CALEE_05 */ " CUVE_CALEE.D_ACT," +
                    /* CUVE_CALEE_06 */ " CUVE_CALEE.N_ACT," +
                    /* CUVE_CALEE_07 */ " CUVE_CALEE.N_CALESP," +
                    /* CUVE_CALEE_08 */ " CUVE_CALEE.C_ESP," +
                    /* CUVE_CALEE_09 */ " CUVE_CALEE.C_CAT_POIDS," +
                    /* CUVE_CALEE_10 */ " CUVE_CALEE.V_POIDS," +
                    /* CUVE_CALEE_11 */ " CUVE_CALEE.V_NB";
        }

        @Override
        public String from() {
            return "CUVE_CALEE";
        }

        // Use a join since it does not exist on AVDTH see #1883
        @Override
        public String innerJoin() {
            return "MAREE ON MAREE.C_BAT = CUVE_CALEE.C_BAT AND MAREE.D_DBQ = CUVE_CALEE.D_DBQ" +
                    " INNER JOIN ACTIVITE ON ACTIVITE.C_BAT = CUVE_CALEE.C_BAT AND ACTIVITE.D_DBQ = CUVE_CALEE.D_DBQ AND ACTIVITE.D_ACT = CUVE_CALEE.D_ACT AND ACTIVITE.N_ACT = CUVE_CALEE.N_ACT";
        }

        @Override
        protected String whereMissing() {
            return " NOT EXISTS( SELECT 'x' FROM ACTIVITE WHERE ACTIVITE.C_BAT = CUVE_CALEE.C_BAT AND ACTIVITE.D_DBQ = CUVE_CALEE.D_DBQ AND ACTIVITE.D_ACT = CUVE_CALEE.D_ACT AND ACTIVITE.N_ACT = CUVE_CALEE.N_ACT )";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_CUVE, F_POS_CUVE, D_ACT, N_ACT, N_CALESP";
        }
    }

    private final MutableInt wellActivitySpeciesCount = new MutableInt();

    public WellActivityReader(ImportEngine context) {
        super(context);
    }

    @Override
    public WellActivity read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        WellActivity entity = newEntity(WellActivity.SPI);
        List<Object> activityPrimaryKey = getActivityPk(resultSet);
        String activityPk = Query.primaryKeyString(activityPrimaryKey);
        Map<String, String> activityPkToId = dataContext.getActivityPkToId();
        Activity activity;
        String activityId = activityPkToId.get(activityPk);
        if (activityId == null) {
            String wellActivityPk = Query.primaryKeyString(Query.getPrimaryKey(resultSet, 1, 3));
            String message = String.format("WellActivity [%20s], not allowed to create missing activity and could not find Activity [%20s]", wellActivityPk, activityPk);
            log.debug(message);
            addMessage(message);
            return null;
        } else {
            activity = new ActivityImpl();
            activity.setTopiaId(activityId);
        }
        entity.setActivity(activity);
        return entity;
    }

    public WellActivitySpecies readExtra(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        WellActivitySpecies entity = newEntity(WellActivitySpecies.SPI, wellActivitySpeciesCount);
        entity.setSetSpeciesNumber(resultSet.getInt(7));
        String originalSpeciesCode = resultSet.getString(8);
        Species species = dataContext.getSpecies(originalSpeciesCode);
        entity.setSpecies(species);
        String weightCategoryCode = resultSet.getString(9);
        WeightCategory weightCategory = dataContext.getWellWeightCategory(weightCategoryCode);
        entity.setWeightCategory(weightCategory);
        entity.setWeight(resultSet.getFloat(10));
        entity.setCount(resultSet.getInt(11));
        return entity;
    }

    public int getWellActivitySpeciesCount() {
        return wellActivitySpeciesCount.intValue();
    }
}
