package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.logbook.WellActivity;
import fr.ird.observe.entities.data.ps.logbook.WellActivitySpecies;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.persistence.avdth.data.DataWriter;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.util.Map;

import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toId;

/**
 * Created on 16/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public class WellActivityWriter extends DataWriter<WellActivity, WellActivityReader> {

    public WellActivityWriter(ImportEngine context, WellActivityReader reader) {
        super(context, reader);
    }

    @Override
    public void write(WellActivity entity, String parentId, int parentCount) {
        String id = entity.getTopiaId();
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_logbook.WellActivity" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " activity," +
                                           /*  6 */  " well)" +
                                           " VALUES (" +
                                           /*  1 */       " '%1$s'," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s);",
                /*  1 */  id,
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  toId(entity.getActivity()),
                /*  6 */  escapeString(parentId)
        );
        writer().writeSql(sql);
        int wellActivitySpeciesCount = 0;
        for (WellActivitySpecies wellActivitySpecies : entity.getWellActivitySpecies()) {
            writeExtra(wellActivitySpecies, timestamp, id, wellActivitySpeciesCount++);
        }
    }

    @Override
    public void flush() {
        super.flush();
        flush(WellActivitySpecies.class, getReader().getWellActivitySpeciesCount());
    }

    @Override
    public void toResult(Map<String, Integer> resultBuilder) {
        super.toResult(resultBuilder);
        toResult(resultBuilder, WellActivitySpecies.class, getReader().getWellActivitySpeciesCount());
    }

    private void writeExtra(WellActivitySpecies wellActivitySpecies, String timestamp, String parentId, int parentCount) {
        Species species = wellActivitySpecies.getSpecies();
        if (species.isNotPersisted()) {
            badSpecies.add(species.getCode() + "-" + species.getLabel2());
            return;
        }

        String sql = String.format("INSERT INTO ps_logbook.wellActivitySpecies" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " species," +
                                           /*  6 */  " weightCategory," +
                                           /*  7 */  " weight," +
                                           /*  8 */  " wellActivity," +
                                           /*  9 */  " wellActivity_idx)" +
                                           " VALUES (" +
                                           /*  1 */       " '%1$s'," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s," +
                                           /*  7 */       " %6$s," +
                                           /*  8 */       " %7$s," +
                                           /*  9 */       " %8$s);",
                /*  1 */  wellActivitySpecies.getTopiaId(),
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  toId(species),
                /*  6 */  toId(wellActivitySpecies.getWeightCategory()),
                /*  7 */  roundFloat4(wellActivitySpecies.getWeight()),
                /*  8 */  escapeString(parentId),
                /*  9 */  parentCount
        );
        writer().writeSql(sql);
    }

}
