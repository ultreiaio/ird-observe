package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableMap;
import fr.ird.observe.entities.data.ps.logbook.Well;
import fr.ird.observe.entities.referential.ps.logbook.WellSamplingConformity;
import fr.ird.observe.entities.referential.ps.logbook.WellSamplingStatus;
import fr.ird.observe.persistence.avdth.data.DataQueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataReader;
import fr.ird.observe.persistence.avdth.data.ImportDataContext;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 16/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public class WellReader extends DataReader<Well> {

    public static final Map<String, String> conformityMapping = ImmutableMap.<String, String>builder()
            .put("1", "9")
            .put("2", "9")
            .put("3", "9")
            .put("4", "9")
            .put("5", "9")
            .put("6", "9")
            .put("7", "9")
            .put("8", "9")
            .put("9", "9")
            .put("10", "1")
            .put("11", "1")
            .put("12", "1")
            .put("13", "1")
            .put("14", "1")
            .put("15", "1")
            .put("16", "1")
            .put("17", "1")
            .put("20", "2")
            .put("21", "2")
            .put("22", "2")
            .build();
    public static final Map<String, String> statusMapping = ImmutableMap.<String, String>builder()
            .put("1", "9")
            .put("2", "9")
            .put("3", "9")
            .put("4", "9")
            .put("5", "9")
            .put("6", "9")
            .put("7", "9")
            .put("8", "9")
            .put("9", "9")
            .put("10", "1")
            .put("11", "2")
            .put("12", "3")
            .put("13", "4")
            .put("14", "5")
            .put("15", "6")
            .put("16", "7")
            .put("17", "8")
            .put("20", "10")
            .put("21", "11")
            .put("22", "12")
            .build();

    @AutoService(DataQueryDefinition.class)
    public static class WellTableReader extends DataQueryDefinition {

        public static final Map<String, String> WELL_POSITION_MAPPING = Map.of(
                "1", "B",
                "2", "T",
                "3", "A"
        );

        /**
         * CUVE (
         * C_BAT SMALLINT not null,
         * D_DBQ TIMESTAMP not null,
         * N_CUVE SMALLINT not null,
         * F_POS_CUVE SMALLINT not null,
         * C_DEST SMALLINT not null constraint CUVE_CFK_DESTIN_R_CUVE references DESTIN,
         * primary key (C_BAT, D_DBQ, N_CUVE, F_POS_CUVE) );
         */
        public WellTableReader() {
            super(4);
        }

        @Override
        public String select() {
            return  /* CUVE_01 */ " C_BAT," +
                    /* CUVE_02 */ " D_DBQ," +
                    /* CUVE_03 */ " N_CUVE," +
                    /* CUVE_04 */ " F_POS_CUVE," +
                    /* CUVE_05 */ " C_DEST";
        }

        @Override
        public String from() {
            return "CUVE";
        }

        @Override
        public String innerJoin() {
            return "MAREE ON MAREE.C_BAT = CUVE.C_BAT AND MAREE.D_DBQ = CUVE.D_DBQ";
        }

        @Override
        protected String whereMissing() {
            return " NOT EXISTS( SELECT 'x' FROM MAREE WHERE MAREE.C_BAT = CUVE.C_BAT AND MAREE.D_DBQ = CUVE.D_DBQ )";
        }

        @Override
        public String orderBy() {
            return "C_BAT, D_DBQ, N_CUVE, F_POS_CUVE, C_DEST";
        }
    }

    public static String getWellId(ResultSet resultSet, int position) throws SQLException {
        String wellNumber = resultSet.getString(position);
        String wellPosition = resultSet.getString(position + 1);
        return wellNumber + Objects.requireNonNull(WellTableReader.WELL_POSITION_MAPPING.get(wellPosition));
    }

    public WellReader(ImportEngine context) {
        super(context);
    }

    @Override
    public Well read(ImportDataContext dataContext, ResultSet resultSet) throws SQLException {
        Well entity = newEntity(Well.SPI);
        String wellId = getWellId(resultSet, 3);
        entity.setWell(wellId);
        String fate = resultSet.getString(5);
        WellSamplingConformity wellSamplingConformity = dataContext.getWellSamplingConformity(fate);
        entity.setWellSamplingConformity(wellSamplingConformity);
        WellSamplingStatus wellSamplingStatus = dataContext.getWellSamplingStatus(fate);
        entity.setWellSamplingStatus(wellSamplingStatus);
        return entity;
    }

}
