package fr.ird.observe.persistence.avdth.data.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.logbook.Well;
import fr.ird.observe.persistence.avdth.data.DataWriter;
import fr.ird.observe.persistence.avdth.data.ImportEngine;

import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toId;
/**
 * Created on 16/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public class WellWriter extends DataWriter<Well, WellReader> {

    public WellWriter(ImportEngine context, WellReader reader) {
        super(context, reader);
    }

    @Override
    public void write(Well entity, String parentId, int parentCount) {
        String id = entity.getTopiaId();
        String timestamp = timestamp();
        String sql = String.format("INSERT INTO ps_logbook.Well" +
                                           /*  1 */ " (topiaId," +
                                           /*  2 */  " topiaVersion," +
                                           /*  3 */  " topiaCreateDate," +
                                           /*  4 */  " lastUpdateDate," +
                                           /*  5 */  " well," +
                                           /*  6 */  " wellSamplingConformity," +
                                           /*  7 */  " wellSamplingStatus," +
                                           /*  8 */  " trip)" +
                                           " VALUES (" +
                                           /*  1 */       " '%1$s'," +
                                           /*  2 */       " 0," +
                                           /*  3 */       " '%2$s'::timestamp," +
                                           /*  4 */       " '%3$s'::timestamp," +
                                           /*  5 */       " %4$s," +
                                           /*  6 */       " %5$s," +
                                           /*  7 */       " %6$s," +
                                           /*  8 */       " %7$s);",
                /*  1 */  id,
                /*  2 */
                /*  3 */  timestamp,
                /*  4 */  timestamp,
                /*  5 */  escapeString(entity.getWell()),
                /*  6 */  toId(entity.getWellSamplingConformity()),
                /*  7 */  toId(entity.getWellSamplingStatus()),
                /*  8 */  escapeString(parentId)
        );
        writer().writeSql(sql);
    }

}
