package fr.ird.observe.persistence.avdth.referential;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.entities.referential.common.DataQuality;
import fr.ird.observe.entities.referential.common.SizeMeasureMethod;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.Person;
import fr.ird.observe.entities.referential.common.SizeMeasureType;
import fr.ird.observe.entities.referential.common.WeightMeasureMethod;
import fr.ird.observe.entities.referential.common.Wind;
import fr.ird.observe.entities.referential.ps.common.AcquisitionStatus;
import fr.ird.observe.entities.referential.ps.common.ObjectMaterial;
import fr.ird.observe.entities.referential.ps.common.ObjectOperation;
import fr.ird.observe.entities.referential.ps.common.ObservedSystem;
import fr.ird.observe.entities.referential.ps.common.Program;
import fr.ird.observe.entities.referential.ps.common.ReasonForNoFishing;
import fr.ird.observe.entities.referential.ps.common.ReasonForNullSet;
import fr.ird.observe.entities.referential.ps.common.SampleType;
import fr.ird.observe.entities.referential.ps.common.SchoolType;
import fr.ird.observe.entities.referential.ps.common.SpeciesFate;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOperation;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOwnership;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyType;
import fr.ird.observe.entities.referential.ps.common.VesselActivity;
import fr.ird.observe.entities.referential.ps.common.WeightCategory;
import fr.ird.observe.entities.referential.ps.landing.Fate;
import fr.ird.observe.entities.referential.ps.logbook.SetSuccessStatus;
import fr.ird.observe.entities.referential.ps.logbook.WellContentStatus;
import fr.ird.observe.entities.referential.ps.logbook.WellSamplingConformity;
import fr.ird.observe.entities.referential.ps.logbook.WellSamplingStatus;
import fr.ird.observe.persistence.avdth.referential.interceptors.DestinationInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.FpaZoneInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.HarbourInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.ObservedSystemInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.OceanInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.PackagingInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.SampleQualityInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.SampleTypeInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.SchoolTypeInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.SpeciesInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.TransmittingBuoyTypeInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.VesseInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.VesselActivityInterceptor;
import fr.ird.observe.persistence.avdth.referential.interceptors.WeightCategoryInterceptor;
import fr.ird.observe.services.service.data.ps.AvdthReferentialImportConfiguration;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created on 29/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class AvdthReferentialImportContext implements Closeable {
    private static final Logger log = LogManager.getLogger(AvdthReferentialImportContext.class);

    /**
     * Persistence application context.
     */
    protected final ObserveTopiaApplicationContext persistenceApplicationContext;
    /**
     * Import configuration.
     */
    final AvdthReferentialImportConfiguration configuration;
    /**
     * Connection to avdth database.
     */
    final Connection conn;
    /**
     * Decorator service.
     */
    private final DecoratorService decoratorService;
    /**
     * Result.
     */
    protected AvdthReferentialImportResult result;
    private SpeciesCache speciesCache;

    public AvdthReferentialImportContext(AvdthReferentialImportConfiguration configuration, Connection conn, ObserveTopiaApplicationContext persistenceApplicationContext) {
        this.configuration = Objects.requireNonNull(configuration);
        this.decoratorService = new DecoratorService(configuration.getReferentialLocale());
        this.persistenceApplicationContext = Objects.requireNonNull(persistenceApplicationContext);
        this.conn = Objects.requireNonNull(conn);
    }

    public DecoratorService getDecoratorService() {
        return decoratorService;
    }

    public Connection getConn() {
        return conn;
    }

    public AvdthReferentialImportConfiguration getConfiguration() {
        return configuration;
    }

    public ObserveTopiaApplicationContext getPersistenceApplicationContext() {
        return persistenceApplicationContext;
    }

    @Override
    public void close() throws IOException {

    }

    public void prepare() throws IOException {
        if (Files.notExists(configuration.getExportFile().getParent())) {
            Files.createDirectories(configuration.getExportFile().getParent());
        }
        result = new AvdthReferentialImportResult();
        result.setSpecies(new LinkedList<>());
        result.setHarbour(new LinkedList<>());
        try (ObserveTopiaPersistenceContext persistenceContext = getPersistenceApplicationContext().newPersistenceContext()) {
            prepare(Ocean.SPI, persistenceContext, result::setOcean);
            prepare(VesselActivity.SPI, persistenceContext, result::setVesselActivity);
            prepare(SchoolType.SPI, persistenceContext, result::setSchoolType);
            prepare(WellContentStatus.SPI, persistenceContext, result::setWellContentStatus);
            prepare(Program.SPI, persistenceContext, result::setProgram);
            prepare(DataQuality.SPI, persistenceContext, result::setDataQuality);
            prepare(Wind.SPI, persistenceContext, result::setWind);
            prepare(SpeciesFate.SPI, persistenceContext, result::setSpeciesFate);
            prepare(WeightMeasureMethod.SPI, persistenceContext, result::setWeightMeasureMethod);
            prepare(SizeMeasureMethod.SPI, persistenceContext, result::setSizeMeasureMethod);
            prepare(SizeMeasureType.SPI, persistenceContext, result::setSizeMeasureType);
            prepare(WeightCategory.SPI, persistenceContext, result::setWeightCategory);
            prepare(WellSamplingConformity.SPI, persistenceContext, result::setWellSamplingConformity);
            prepare(WellSamplingStatus.SPI, persistenceContext, result::setWellSamplingStatus);
            prepare(Fate.SPI, persistenceContext, result::setFate);
            prepare(ObjectOperation.SPI, persistenceContext, result::setObjectOperation);
            prepare(SampleType.SPI, persistenceContext, result::setSampleType);
            prepare(ObjectMaterial.SPI, persistenceContext, result::setObjectMaterial);
            prepare(ObservedSystem.SPI, persistenceContext, result::setObservedSystem);
            prepare(TransmittingBuoyOperation.SPI, persistenceContext, result::setTransmittingBuoyOperation);
            prepare(TransmittingBuoyType.SPI, persistenceContext, result::setTransmittingBuoyType);
            prepare(TransmittingBuoyOwnership.SPI, persistenceContext, result::setTransmittingBuoyOwnership);
            prepare(AcquisitionStatus.SPI, persistenceContext, result::setAcquisitionStatus);
            prepare(Person.SPI, persistenceContext, result::setPerson);
            prepare(ReasonForNoFishing.SPI, persistenceContext, result::setReasonForNoFishing);
            prepare(ReasonForNullSet.SPI, persistenceContext, result::setReasonForNullSet);
            prepare(SetSuccessStatus.SPI, persistenceContext, result::setSetSuccessStatus);
        }
    }

    protected <E extends ReferentialEntity> void prepare(ReferentialDtoEntityContext<?, ?, E, ?> spi, ObserveTopiaPersistenceContext persistenceContext, Consumer<List<E>> consumer) {
        configuration.incrementsProgression(String.format("Chargement du référentiel %s en cours...", spi.toEntityType().getName()));
        List<E> entities = spi.loadEntities(persistenceContext);
        consumer.accept(entities);
        configuration.incrementsProgression(String.format("Chargement du référentiel %s terminé (%d entrée(s)).", spi.toEntityType().getName(), entities.size()));
    }

    public int read() throws SQLException, IOException {
        log.info(String.format("Export referential to\n%s", configuration.getExportFile()));
        speciesCache = SpeciesCache.load(conn);
        int referentialCount = 0;
        try (ObserveTopiaPersistenceContext persistenceContext = persistenceApplicationContext.newPersistenceContext()) {
            try (SqlScriptWriter sqlWriter = SqlScriptWriter.of(configuration.getExportFile())) {
                referentialCount += new DestinationInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new FpaZoneInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new HarbourInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new SpeciesInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new WeightCategoryInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new ObservedSystemInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new OceanInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new PackagingInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new SampleQualityInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new SampleTypeInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new SchoolTypeInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new TransmittingBuoyTypeInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new VesseInterceptor(this).process(sqlWriter, conn, persistenceContext);
                referentialCount += new VesselActivityInterceptor(this).process(sqlWriter, conn, persistenceContext);
//                referentialCount += writers.getSpeciesFateInterceptor().process(sqlWriter, conn, persistenceContext, (r, i) -> r.setSpeciesFate(i.getLoaded()));
                log.info(String.format("Loaded %d referential(s).", referentialCount));
                sqlWriter.flush();
            }
        }
        return referentialCount;
    }

    public AvdthReferentialImportResult toResult(int referentialCount) {
        result.setReferentialCount(referentialCount);
        result.setSpeciesCache(speciesCache);
        return result;
    }

    public AvdthReferentialImportResult getResult() {
        return result;
    }

    public SpeciesCache getSpeciesCache() {
        return speciesCache;
    }
}
