package fr.ird.observe.persistence.avdth.referential;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referential.common.DataQuality;
import fr.ird.observe.entities.referential.common.FpaZone;
import fr.ird.observe.entities.referential.common.Harbour;
import fr.ird.observe.entities.referential.common.SizeMeasureMethod;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.Person;
import fr.ird.observe.entities.referential.common.SizeMeasureType;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.common.Vessel;
import fr.ird.observe.entities.referential.common.WeightMeasureMethod;
import fr.ird.observe.entities.referential.common.Wind;
import fr.ird.observe.entities.referential.ps.common.AcquisitionStatus;
import fr.ird.observe.entities.referential.ps.common.ObjectMaterial;
import fr.ird.observe.entities.referential.ps.common.ObjectOperation;
import fr.ird.observe.entities.referential.ps.common.ObservedSystem;
import fr.ird.observe.entities.referential.ps.common.Program;
import fr.ird.observe.entities.referential.ps.common.ReasonForNoFishing;
import fr.ird.observe.entities.referential.ps.common.ReasonForNullSet;
import fr.ird.observe.entities.referential.ps.common.SampleType;
import fr.ird.observe.entities.referential.ps.common.SchoolType;
import fr.ird.observe.entities.referential.ps.common.SpeciesFate;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOperation;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOwnership;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyType;
import fr.ird.observe.entities.referential.ps.common.VesselActivity;
import fr.ird.observe.entities.referential.ps.common.WeightCategory;
import fr.ird.observe.entities.referential.ps.landing.Destination;
import fr.ird.observe.entities.referential.ps.landing.Fate;
import fr.ird.observe.entities.referential.ps.localmarket.Packaging;
import fr.ird.observe.entities.referential.ps.logbook.SampleQuality;
import fr.ird.observe.entities.referential.ps.logbook.SetSuccessStatus;
import fr.ird.observe.entities.referential.ps.logbook.WellContentStatus;
import fr.ird.observe.entities.referential.ps.logbook.WellSamplingConformity;
import fr.ird.observe.entities.referential.ps.logbook.WellSamplingStatus;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 29/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class AvdthReferentialImportResult {

    private final Set<String> missingReferentialMessages = new LinkedHashSet<>();
    private int referentialCount;
    private List<SizeMeasureMethod> sizeMeasureMethod;
    private List<WeightMeasureMethod> weightMeasureMethod;
    private List<Program> program;
    private List<Person> person;
    private List<Vessel> vessel;
    private List<Wind> wind;
    private List<Ocean> ocean;
    private List<VesselActivity> vesselActivity;
    private List<Harbour> harbour;
    private List<SchoolType> schoolType;
    private List<TransmittingBuoyType> transmittingBuoyType;
    private List<FpaZone> fpaZone;
    private List<WeightCategory> weightCategory;
    private List<Species> species;
    private List<SizeMeasureType> sizeMeasureType;
    private List<SpeciesFate> speciesFate;
    private List<SampleType> sampleType;
    private List<SampleQuality> sampleQuality;
    private List<Fate> fate;
    private List<Destination> destination;
    private List<ObservedSystem> observedSystem;
    private List<Packaging> packaging;
    private List<WellContentStatus> wellContentStatus;
    private List<DataQuality> dataQuality;
    private List<WellSamplingConformity> wellSamplingConformity;
    private List<WellSamplingStatus> wellSamplingStatus;
    private List<ObjectOperation> objectOperation;
    private List<ObjectMaterial> objectMaterial;
    private List<TransmittingBuoyOperation> transmittingBuoyOperation;
    private List<TransmittingBuoyOwnership> transmittingBuoyOwnership;
    private List<AcquisitionStatus> acquisitionStatus;
    private List<ReasonForNoFishing> reasonForNoFishing;
    private List<ReasonForNullSet> reasonForNullSet;
    private List<SetSuccessStatus> setSuccessStatus;
    private SpeciesCache speciesCache;

    public List<ReasonForNoFishing> getReasonForNoFishing() {
        return reasonForNoFishing;
    }

    public void setReasonForNoFishing(List<ReasonForNoFishing> reasonForNoFishing) {
        this.reasonForNoFishing = reasonForNoFishing;
    }

    public List<ReasonForNullSet> getReasonForNullSet() {
        return reasonForNullSet;
    }

    public void setReasonForNullSet(List<ReasonForNullSet> reasonForNullSet) {
        this.reasonForNullSet = reasonForNullSet;
    }

    public List<SetSuccessStatus> getSetSuccessStatus() {
        return setSuccessStatus;
    }

    public void setSetSuccessStatus(List<SetSuccessStatus> setSuccessStatus) {
        this.setSuccessStatus = setSuccessStatus;
    }

    public List<Person> getPerson() {
        return person;
    }

    public void setPerson(List<Person> person) {
        this.person = person;
    }

    public SpeciesCache getSpeciesCache() {
        return speciesCache;
    }

    public void setSpeciesCache(SpeciesCache speciesCache) {
        this.speciesCache = speciesCache;
    }

    public void setReferentialCount(int referentialCount) {
        this.referentialCount = referentialCount;
    }

    public int getReferentialCount() {
        return referentialCount;
    }

    public List<AcquisitionStatus> getAcquisitionStatus() {
        return acquisitionStatus;
    }

    public void setAcquisitionStatus(List<AcquisitionStatus> acquisitionStatus) {
        this.acquisitionStatus = acquisitionStatus;
    }

    public List<TransmittingBuoyOperation> getTransmittingBuoyOperation() {
        return transmittingBuoyOperation;
    }

    public void setTransmittingBuoyOperation(List<TransmittingBuoyOperation> transmittingBuoyOperation) {
        this.transmittingBuoyOperation = transmittingBuoyOperation;
    }

    public List<ObjectMaterial> getObjectMaterial() {
        return objectMaterial;
    }

    public void setObjectMaterial(List<ObjectMaterial> objectMaterial) {
        this.objectMaterial = objectMaterial;
    }

    public List<ObjectOperation> getObjectOperation() {
        return objectOperation;
    }

    public void setObjectOperation(List<ObjectOperation> objectOperation) {
        this.objectOperation = objectOperation;
    }

    public List<Destination> getDestination() {
        return destination;
    }

    public void setDestination(List<Destination> destination) {
        this.destination = destination;
    }

    public List<WellSamplingConformity> getWellSamplingConformity() {
        return wellSamplingConformity;
    }

    public void setWellSamplingConformity(List<WellSamplingConformity> wellSamplingConformity) {
        this.wellSamplingConformity = wellSamplingConformity;
    }

    public List<WellSamplingStatus> getWellSamplingStatus() {
        return wellSamplingStatus;
    }

    public void setWellSamplingStatus(List<WellSamplingStatus> wellSamplingStatus) {
        this.wellSamplingStatus = wellSamplingStatus;
    }

    public List<SizeMeasureType> getSizeMeasureType() {
        return sizeMeasureType;
    }

    public void setSizeMeasureType(List<SizeMeasureType> sizeMeasureType) {
        this.sizeMeasureType = sizeMeasureType;
    }

    public List<SizeMeasureMethod> getSizeMeasureMethod() {
        return sizeMeasureMethod;
    }

    public void setSizeMeasureMethod(List<SizeMeasureMethod> sizeMeasureMethod) {
        this.sizeMeasureMethod = sizeMeasureMethod;
    }

    public List<WeightMeasureMethod> getWeightMeasureMethod() {
        return weightMeasureMethod;
    }

    public void setWeightMeasureMethod(List<WeightMeasureMethod> weightMeasureMethod) {
        this.weightMeasureMethod = weightMeasureMethod;
    }

    public List<SpeciesFate> getSpeciesFate() {
        return speciesFate;
    }

    public void setSpeciesFate(List<SpeciesFate> speciesFate) {
        this.speciesFate = speciesFate;
    }

    public List<Wind> getWind() {
        return wind;
    }

    public void setWind(List<Wind> wind) {
        this.wind = wind;
    }

    public List<DataQuality> getDataQuality() {
        return dataQuality;
    }

    public void setDataQuality(List<DataQuality> dataQuality) {
        this.dataQuality = dataQuality;
    }

    public List<Program> getProgram() {
        return program;
    }

    public void setProgram(List<Program> program) {
        this.program = program;
    }

    public List<WellContentStatus> getWellContentStatus() {
        return wellContentStatus;
    }

    public void setWellContentStatus(List<WellContentStatus> wellContentStatus) {
        this.wellContentStatus = wellContentStatus;
    }

    public List<Vessel> getVessel() {
        return vessel;
    }

    public void setVessel(List<Vessel> vessel) {
        this.vessel = vessel;
    }

    public List<Ocean> getOcean() {
        return ocean;
    }

    public void setOcean(List<Ocean> ocean) {
        this.ocean = ocean;
    }

    public List<VesselActivity> getVesselActivity() {
        return vesselActivity;
    }

    public void setVesselActivity(List<VesselActivity> vesselActivity) {
        this.vesselActivity = vesselActivity;
    }

    public List<Harbour> getHarbour() {
        return harbour;
    }

    public void setHarbour(List<Harbour> harbour) {
        this.harbour = harbour;
    }

    public List<SchoolType> getSchoolType() {
        return schoolType;
    }

    public void setSchoolType(List<SchoolType> schoolType) {
        this.schoolType = schoolType;
    }

    public List<TransmittingBuoyType> getTransmittingBuoyType() {
        return transmittingBuoyType;
    }

    public void setTransmittingBuoyType(List<TransmittingBuoyType> transmittingBuoyType) {
        this.transmittingBuoyType = transmittingBuoyType;
    }

    public List<TransmittingBuoyOwnership> getTransmittingBuoyOwnership() {
        return transmittingBuoyOwnership;
    }

    public void setTransmittingBuoyOwnership(List<TransmittingBuoyOwnership> transmittingBuoyOwnership) {
        this.transmittingBuoyOwnership = transmittingBuoyOwnership;
    }

    public List<FpaZone> getFpaZone() {
        return fpaZone;
    }

    public void setFpaZone(List<FpaZone> fpaZone) {
        this.fpaZone = fpaZone;
    }

    public List<WeightCategory> getWeightCategory() {
        return weightCategory;
    }

    public void setWeightCategory(List<WeightCategory> weightCategory) {
        this.weightCategory = weightCategory;
    }

    public List<Species> getSpecies() {
        return species;
    }

    public void setSpecies(List<Species> species) {
        this.species = species;
    }

    public List<SampleType> getSampleType() {
        return sampleType;
    }

    public void setSampleType(List<SampleType> sampleType) {
        this.sampleType = sampleType;
    }

    public List<SampleQuality> getSampleQuality() {
        return sampleQuality;
    }

    public void setSampleQuality(List<SampleQuality> sampleQuality) {
        this.sampleQuality = sampleQuality;
    }

    public List<Fate> getFate() {
        return fate;
    }

    public void setFate(List<Fate> fate) {
        this.fate = fate;
    }

    public List<ObservedSystem> getObservedSystem() {
        return observedSystem;
    }

    public void setObservedSystem(List<ObservedSystem> observedSystem) {
        this.observedSystem = observedSystem;
    }

    public List<Packaging> getPackaging() {
        return packaging;
    }

    public void setPackaging(List<Packaging> packaging) {
        this.packaging = packaging;
    }

    public void addMissingReferential(String message) {
        missingReferentialMessages.add(message);
    }

    public Set<String> getMissingReferentialMessages() {
        return missingReferentialMessages;
    }
}
