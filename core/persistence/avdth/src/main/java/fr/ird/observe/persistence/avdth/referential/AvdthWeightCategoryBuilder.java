package fr.ird.observe.persistence.avdth.referential;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.data.ps.AvdthReferentialImportConfiguration;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created on 04/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class AvdthWeightCategoryBuilder {
    private final AvdthReferentialImportConfiguration configuration;

    public AvdthWeightCategoryBuilder(AvdthReferentialImportConfiguration configuration) {
        this.configuration = configuration;
    }

    public AvdthReferentialImportResult build(Connection conn, ObserveTopiaApplicationContext topiaApplicationContext) throws IOException, SQLException {
        AvdthWeightCategoryBuilderContext context = new AvdthWeightCategoryBuilderContext(configuration, conn, topiaApplicationContext);
        context.prepare();
        int read = context.read();
        return context.toResult(read);
    }

}
