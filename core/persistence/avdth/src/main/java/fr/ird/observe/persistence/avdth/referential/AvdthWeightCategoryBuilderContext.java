package fr.ird.observe.persistence.avdth.referential;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.data.ps.AvdthReferentialImportConfiguration;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.ps.common.WeightCategory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * Created on 04/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class AvdthWeightCategoryBuilderContext extends AvdthReferentialImportContext {
    private static final Logger log = LogManager.getLogger(AvdthWeightCategoryBuilderContext.class);

    public AvdthWeightCategoryBuilderContext(AvdthReferentialImportConfiguration configuration, Connection conn, ObserveTopiaApplicationContext persistenceApplicationContext) {
        super(configuration, conn, persistenceApplicationContext);
    }

    @Override
    public void close() throws IOException {

    }

    public void prepare() throws IOException {
        if (Files.notExists(configuration.getExportFile().getParent())) {
            Files.createDirectories(configuration.getExportFile().getParent());
        }
        result = new AvdthReferentialImportResult();
        result.setSpecies(new LinkedList<>());
        result.setWeightCategory(new LinkedList<>());
        try (ObserveTopiaPersistenceContext persistenceContext = getPersistenceApplicationContext().newPersistenceContext()) {
            prepare(Species.SPI, persistenceContext, result::setSpecies);
            prepare(WeightCategory.SPI, persistenceContext, result::setWeightCategory);
        }
    }

    public int read() throws SQLException, IOException {
        int referentialCount = 0;

        try (ObserveTopiaPersistenceContext persistenceContext = persistenceApplicationContext.newPersistenceContext()) {
            try (SqlScriptWriter sqlWriter = SqlScriptWriter.of(configuration.getExportFile())) {
                log.info(String.format("Export referential to\n%s", configuration.getExportFile()));
                referentialCount += new WeightCategoryExtractor(this).process(sqlWriter, conn, persistenceContext);
                log.info(String.format("Loaded %d referential(s).", referentialCount));
                sqlWriter.flush();
            }
        }
        return referentialCount;
    }

    public AvdthReferentialImportResult toResult(int referentialCount) {
        result.setReferentialCount(referentialCount);
        return result;
    }

    public AvdthReferentialImportResult getResult() {
        return result;
    }
}
