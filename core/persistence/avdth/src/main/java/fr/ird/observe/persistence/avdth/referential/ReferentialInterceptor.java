package fr.ird.observe.persistence.avdth.referential;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.spi.SqlConversions;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.function.Function;

/**
 * Created on 21/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class ReferentialInterceptor<E extends ReferentialEntity, S extends ReferentialDtoEntityContext<?, ?, E, ?>> {

    private static final Logger log = LogManager.getLogger(ReferentialInterceptor.class);
    /**
     * Main spi associated.
     */
    protected final S spi;
    /**
     * Loaded in this injector.
     */
    protected final List<E> loaded;
    /**
     * Import context.
     */
    protected final AvdthReferentialImportContext context;

    protected ReferentialInterceptor(AvdthReferentialImportContext context, S spi) {
        this.context = Objects.requireNonNull(context);
        this.spi = spi;
        this.loaded = new LinkedList<>();
    }

    public abstract int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException;

    public int process(SqlScriptWriter sqlWriter, Query query, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        context.getConfiguration().incrementsProgression(String.format("Lecture du référentiel %s en cours...", query.from()));
        int count = read(persistenceContext, query, sqlWriter);
        context.getConfiguration().incrementsProgression(String.format("Lecture du référentiel %s terminée (%d entrée(s)).", query.from(), count));
        consume(context.getResult(), getLoaded());
        return count;
    }

    public abstract void consume(AvdthReferentialImportResult result, List<E> newList);

    protected Map<String, E> existingIndex(ObserveTopiaPersistenceContext persistenceContext) {
        List<E> existing = spi.getDao(persistenceContext).findAll();
        return Maps.uniqueIndex(existing, ReferentialEntity::getCode);
    }

    public E read(ResultSet resultSet, Map<String, E> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        String code = resultSet.getString(1);
        return read(existingIndex, code);
    }

    protected E read(Map<String, E> existingIndex, String code) {
        E existing = existingIndex.get(code);
        E result = spi.newEntity(context.getConfiguration().getNow());
        if (existing != null) {
            existing.copy(result);
        }
        result.setCode(code);
        return result;
    }

    public List<E> getLoaded() {
        return loaded;
    }

    public int read(ObserveTopiaPersistenceContext persistenceContext, Query query, SqlScriptWriter sqlWriter) throws SQLException {
        loaded.clear();
        String prefix = String.format("%-35s", query.getDescriptor().getClass().getSimpleName());
        Map<String, E> existingIndex = existingIndex(persistenceContext);
        List<E> notPersisted = new LinkedList<>();
        while (query.hasNext()) {
            ResultSet resultSet = query.next();
            E entity = read(resultSet, existingIndex, sqlWriter);
            if (entity != null) {
                loaded.add(entity);
                if (entity.isNotPersisted()) {
                    notPersisted.add(entity);
                    String content = toString(resultSet);
                    log.warn(String.format("%s - Missing referential: %s.", prefix, content));
                    String format = String.format("Un référentiel (de type %s) non présent dans ObServe a été détecté (table %s) : %s", spi.toEntityType().getSimpleName(), query.getDescriptor().mainTable(), content);
                    context.getResult().addMissingReferential(format);
                }
            }
        }
        log.info(String.format("%s - Loaded: %d %s", prefix, loaded.size(), spi.toEntityType().getSimpleName()));

        if (!notPersisted.isEmpty()) {
            log.warn(String.format("%s - Need to create %d new entries...", prefix, notPersisted.size()));
        }
        return loaded.size();
    }

    protected final String timestamp() {
        return SqlConversions.timestamp(new Date());
    }

    protected void mergeNewReferential(AvdthReferentialImportResult result, List<E> newList, Function<AvdthReferentialImportResult, List<E>> getter, Function<E, String> toCode) {
        List<E> existing = getter.apply(result);
        Map<String, E> byCode = new TreeMap<>(Maps.uniqueIndex(existing, toCode::apply));
        newList.stream().filter(entity -> !byCode.containsKey(toCode.apply(entity))).forEach(existing::add);
    }

    private String toString(ResultSet resultSet) throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        Map<String, String> result = new LinkedHashMap<>(columnCount);
        for (int i = 1; i <= columnCount; i++) {
            String columnName = metaData.getColumnLabel(i);
            Object columnValue = resultSet.getObject(i);
            result.put(columnName, String.valueOf(columnValue));
        }
        return result.toString();
    }
}
