package fr.ird.observe.persistence.avdth.referential;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableMap;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Cache of species from avdth.
 * <p>
 * Created on 28/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SpeciesCache {

    private static final Logger log = LogManager.getLogger(SpeciesCache.class);
    /**
     * From avdth species code to fao code.
     */
    private static final Map<String, String> SPECIES_CODE_TO_FAO_CODE = ImmutableMap
            .<String, String>builder()
            .put("8", "TUN")  // 8-DSC
            .put("9", "TUN")  // 9-MIX
            .put("34", "BUM") // 34-BLZ
            .put("40", "TUS") // 40-BGT
            .put("43", "RAV*") // 43-RAV
            .put("99", "XXX*")// 99-OTH

            .put("701", "PIL") // 701-?
            .put("702", "CLP") // 702-?
            .put("703", "JAX") // 703-?
            .put("704", "PAX") // 704-?
            .put("705", "ANE") // 705-?
            .put("706", "CLP") // 706-?
            .put("707", "BOG") // 707-?
            .put("708", "CLP") // 708-?
            .put("709", "MZZ") // 709-?

            .put("801", "YFT") // 801 → YFT (force it for old avdth databases)
            .put("802", "SKJ") // 802 → SKJ (force it for old avdth databases)
            .put("803", "BET") // 803 → BET (force it for old avdth databases)
            .put("804", "ALB") // 804 → ALB (force it for old avdth databases)
            .put("805", "LTA") // 805 → LTA (force it for old avdth databases)
            .put("806", "FRI") // 806 → FRI (force it for old avdth databases)
            .put("807", "SKH") // 807 → SKH (force it for old avdth databases)
            .put("809", "TUN") // 809-MIX
            .put("810", "KAW") // 810 → KAW (force it for old avdth databases)
            .put("811", "LOT") // 811 → LOT (force it for old avdth databases)
            .put("843", "RAV*")// 843 → RAV*
            .put("899", "XXX*")// 899-OTH
            .build();

    private final TreeMap<String, String> codeToFaoCode;
    private final TreeMap<String, String> codeToLabel;
    private Map<String, Species> speciesByFaoCode;

    /**
     * Get all species with their faoCode and label.
     */
    @AutoService(ReferentialQueryDefinition.class)
    public static class SpeciesTable extends ReferentialQueryDefinition {

        public SpeciesTable() {
            super(1);
        }

        @Override
        public String select() {
            return "ESPECE.C_ESP, ESPECE.C_ESP_3L, ESPECE.L_ESP";
        }

        @Override
        public String from() {
            return "ESPECE";
        }
    }

    public static SpeciesCache load(Connection connexion) throws SQLException {
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(SpeciesTable.class, connexion)) {
            return load(tableReader);
        }
    }

    public static SpeciesCache load(Query query) throws SQLException {
        String prefix = String.format("%-35s", SpeciesTable.class.getSimpleName());
        TreeMap<String, String> codeToFaoCode = new TreeMap<>();
        TreeMap<String, String> codeToLabel = new TreeMap<>();
        while (query.hasNext()) {
            ResultSet resultSet = query.next();
            String code = Objects.requireNonNull(resultSet.getString(1));
            String originalFaoCode = resultSet.getString(2);
            String faoCode = translateFaoCode(code, originalFaoCode);
            String label = resultSet.getString(3);
            codeToFaoCode.put(code, faoCode);
            codeToLabel.put(code, label);
        }
        log.info(String.format("%s - Loaded: %d avdth species.", prefix, codeToFaoCode.size()));
        return new SpeciesCache(codeToFaoCode, codeToLabel);
    }

    public static Species translateSpecies(Map<String, Species> speciesByFaoCode, String originalSpeciesCode, String originalFaoCode) {
        Species species = translateUnsafeSpecies(speciesByFaoCode, originalSpeciesCode, originalFaoCode);
        return Objects.requireNonNull(species, String.format("Can't find species with faoCode = '%s' (C_ESP = '%s')", originalFaoCode, originalSpeciesCode));
    }

    public static Species translateUnsafeSpecies(Map<String, Species> speciesByFaoCode, String originalSpeciesCode, String originalFaoCode) {
        String faoCode = translateFaoCode(originalSpeciesCode, originalFaoCode);
        Objects.requireNonNull(faoCode, String.format("Can't find faoCode for C_ESP = '%s'", originalSpeciesCode));
        return speciesByFaoCode.get(faoCode);
    }

    public static String translateFaoCode(String originalSpeciesCode, String originalFaoCode) {
        String faoCode = originalFaoCode;
        if (originalFaoCode.length() == 4 && originalFaoCode.startsWith("D")) {
            faoCode = originalFaoCode.substring(1);
        }
        return SPECIES_CODE_TO_FAO_CODE.getOrDefault(originalSpeciesCode, faoCode);
    }

    protected SpeciesCache(TreeMap<String, String> codeToFaoCode, TreeMap<String, String> codeToLabel) {
        this.codeToFaoCode = codeToFaoCode;
        this.codeToLabel = codeToLabel;
    }

    public void init(Map<String, Species> speciesByFaoCode) {
        this.speciesByFaoCode = speciesByFaoCode;
        codeToFaoCode.entrySet().removeIf(next -> !speciesByFaoCode.containsKey(next.getValue()));
        codeToLabel.entrySet().removeIf(next -> !codeToFaoCode.containsKey(next.getKey()));
        String prefix = String.format("%-35s", SpeciesTable.class.getSimpleName());
        log.info(String.format("%s - Init with %d ObServe species (and %d for Avdth).", prefix, speciesByFaoCode.size(), codeToFaoCode.size()));
    }

    public Species translateSpecies(String originalSpeciesCode) {
        Species species = translateUnsafeSpecies(originalSpeciesCode);
        return Objects.requireNonNull(species, String.format("Can't find species (C_ESP = '%s')", originalSpeciesCode));
    }

    public Species translateUnsafeSpecies(String originalSpeciesCode) {
        String faoCode = codeToFaoCode.get(originalSpeciesCode);
        Objects.requireNonNull(faoCode, String.format("Can't find faoCode for C_ESP = '%s'", originalSpeciesCode));
        return speciesByFaoCode.get(faoCode);
    }

    public String faoCode(String code) {
        return codeToFaoCode.get(code);
    }

    public String label(String code) {
        return codeToLabel.get(code);
    }
}
