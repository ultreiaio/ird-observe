package fr.ird.observe.persistence.avdth.referential;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.Maps;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.ps.common.WeightCategory;
import fr.ird.observe.entities.referential.ps.common.WeightCategorySpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.persistence.avdth.QueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataWriter;
import fr.ird.observe.persistence.avdth.referential.interceptors.WeightCategoryInterceptor;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static fr.ird.observe.spi.SqlConversions.toId;

/**
 * To extract weight categories.
 * <p>
 * Created on 04/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class WeightCategoryExtractor extends ReferentialInterceptor<WeightCategory, WeightCategorySpi> {
    /**
     * Internal cache of created entities, keep it static to reuse if for more than one import...
     */
    private final static Map<String, WeightCategory> createdEntities = new TreeMap<>();

    private Map<String, Species> speciesByFaoCode;
    private long landingCount = 0;
    private long catchCount = 1000;
    private Class<? extends QueryDefinition> tableDescriptorClass;

    @AutoService(ReferentialQueryDefinition.class)
    public static class LandingWeightCategoryTable extends ReferentialQueryDefinition {

        public LandingWeightCategoryTable() {
            super(1);
        }

        @Override
        public String select() {
            return  /* 1 */" CAT_COM.C_ESP," +
                    /* 2 */" ESPECE.C_ESP_3L," +
                    /* 3 */" CAT_COM.C_CAT_C," +
                    /* 4 */" CAT_COM.L_CC_SOV," +
                    /* 5 */" CAT_COM.L_CC_STAR," +
                    /* 6 */" CAT_COM.STATUT";
        }

        @Override
        public String from() {
            return "CAT_COM";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON ESPECE.C_ESP = CAT_COM.C_ESP";
        }

        @Override
        public String orderBy() {
            return "CAT_COM.C_ESP, CAT_COM.C_CAT_C";
        }
    }

    @AutoService(ReferentialQueryDefinition.class)
    public static class CatchWeightCategoryTable extends ReferentialQueryDefinition {

        public CatchWeightCategoryTable() {
            super(1);
        }

        @Override
        public String select() {
            return  /* 1 */" CAT_TAILLE.C_ESP," +
                    /* 2 */" ESPECE.C_ESP_3L," +
                    /* 3 */" CAT_TAILLE.C_CAT_T," +
                    /* 4 */" CAT_TAILLE.L_CAT_T," +
                    /* 5 */" CAT_TAILLE.V_POIDS_M," +
                    /* 6 */" CAT_TAILLE.STATUT";
        }

        @Override
        public String from() {
            return "CAT_TAILLE";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON ESPECE.C_ESP = CAT_TAILLE.C_ESP";
        }

        @Override
        public String orderBy() {
            return "CAT_TAILLE.C_ESP, CAT_TAILLE.C_CAT_T";
        }
    }

    public WeightCategoryExtractor(AvdthWeightCategoryBuilderContext context) {
        super(context, WeightCategory.SPI);
    }

    @Override
    protected Map<String, WeightCategory> existingIndex(ObserveTopiaPersistenceContext persistenceContext) {
        speciesByFaoCode = Maps.uniqueIndex(context.getResult().getSpecies(), Species::getFaoCode);
        List<WeightCategory> existing = spi.loadEntities(persistenceContext);
        landingCount += existing.stream().filter(d -> d.getCode().startsWith("L-")).count();
        catchCount += existing.stream().filter(d -> d.getCode().startsWith("C-")).count();
        return Maps.uniqueIndex(existing.stream().filter(d -> d.getCode().contains("-")).collect(Collectors.toList()), ReferentialEntity::getCode);
    }

    @Override
    public WeightCategory read(ResultSet resultSet, Map<String, WeightCategory> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        WeightCategory entity = super.read(resultSet, existingIndex, sqlWriter);
        if (entity.isNotPersisted()) {

            if (tableDescriptorClass.equals(CatchWeightCategoryTable.class)) {
                String originalSpeciesCode = resultSet.getString(1);
                String originalFaoCode = resultSet.getString(2);
                String categoryCode = resultSet.getString(3);
                String label = resultSet.getString(4).replaceAll("\n", " ");
                if (label.contains("cat. ?")) {
                    return null;
                }
                if (label.contains("Sardine ?")) {
                    return null;
                }
                Species species = SpeciesCache.translateSpecies(speciesByFaoCode, originalSpeciesCode, originalFaoCode);
                if (WeightCategoryInterceptor.rejectCatchWeightCategory(species, categoryCode)) {
                    return null;
                }

                String code = WeightCategoryInterceptor.getCatchWeightCategoryCode(species, categoryCode);
                if (existingIndex.containsKey(code)) {
                    return null;
                }
                if (createdEntities.containsKey(code)) {
                    return null;
                }
                // fill it from avdth
                entity.setSpecies(species);
                entity.setLogbook(true);
                entity.setCode(code);
                entity.setLabel1(label);
                entity.setLabel2(label);
                entity.setLabel3(label);
                Object meanWeight = resultSet.getObject(5);
                if (meanWeight != null) {
                    entity.setMeanWeight(Float.valueOf(meanWeight.toString()));
                }
                boolean statusCode = resultSet.getBoolean(6);
                entity.setStatus(statusCode ? ReferenceStatus.enabled : ReferenceStatus.disabled);
                write(++catchCount, sqlWriter, entity);

            } else if (tableDescriptorClass.equals(LandingWeightCategoryTable.class)) {
                String originalSpeciesCode = resultSet.getString(1);
                String originalFaoCode = resultSet.getString(2);
                String categoryCode = resultSet.getString(3);
                Species species = SpeciesCache.translateSpecies(speciesByFaoCode, originalSpeciesCode, originalFaoCode);
                if (WeightCategoryInterceptor.rejectLandingWeightCategory(categoryCode)) {
                    return null;
                }
                String code = WeightCategoryInterceptor.getLandingWeightCategoryCode(species, categoryCode);
                if (existingIndex.containsKey(code)) {
                    return null;
                }
                if (createdEntities.containsKey(code)) {
                    return null;
                }
                // fill it from avdth
                entity.setLanding(true);
                entity.setSpecies(species);
                entity.setCode(code);
                String sovLabel = resultSet.getString(4);
                String startLabel = resultSet.getString(5);
                String label = String.format("[SOVETCO: %s] - [STARKIST: %s]", sovLabel, startLabel);
                entity.setLabel1(label);
                entity.setLabel2(label);
                entity.setLabel3(label);
                boolean statusCode = resultSet.getBoolean(6);
                entity.setStatus(statusCode ? ReferenceStatus.enabled : ReferenceStatus.disabled);
                write(++landingCount, sqlWriter, entity);
            }
        }
        return entity;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        int result = 0;
        try (Query query = AvdthQueries.referentialReader(LandingWeightCategoryTable.class, connexion)) {
            result += process(sqlWriter, query, persistenceContext);
        }
        try (Query query = AvdthQueries.referentialReader(CatchWeightCategoryTable.class, connexion)) {
            result += process(sqlWriter, query, persistenceContext);
        }
        return result;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Query query, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        this.tableDescriptorClass = query.getDescriptor().getClass();
        return super.process(sqlWriter, query, persistenceContext);
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<WeightCategory> newList) {
        List<WeightCategory> existing = result.getWeightCategory();
        Map<String, WeightCategory> byCode = new TreeMap<>(Maps.uniqueIndex(existing, WeightCategory::getCode));
        newList.stream().filter(entity -> !byCode.containsKey(entity.getCode())).forEach(existing::add);
    }

    private void write(long count, SqlScriptWriter sqlWriter, WeightCategory entity) {
        String id = Strings.leftPad("" + count, 3, '0');
        entity.setTopiaId("fr.ird.referential.ps.common.WeightCategory#${REFERENTIAL_PREFIX}" + id);
        String sql = String.format(
                "INSERT INTO ps_common.WeightCategory(" +
                        /*  1 */      "topiaId, " +
                        /*  2 */      "topiaVersion, " +
                        /*  3 */      "code, " +
                        /*  4 */      "status, " +
                        /*  5 */      "topiaCreateDate, " +
                        /*  6 */      "lastUpdateDate, " +
                        /*  7 */      "needComment, " +
                        /*  8 */      "species, " +
                        /*  9 */      "label1, " +
                        /* 10 */      "label2, " +
                        /* 11 */      "label3, " +
                        /* 12 */      "minWeight, " +
                        /* 13 */      "meanWeight, " +
                        /* 14 */      "maxWeight, " +
                        /* 15 */      "landing ," +
                        /* 16 */      "logbook ," +
                        /* 17 */      "wellPlan" +
                        ") VALUES " +
                        "(" +
                        /*  1 */ " '%1$s'," +
                        /*  2 */ " 0," +
                        /*  3 */ " '%2$s'," +
                        /*  4 */ " %3$s," +
                        /*  5 */ " ${CURRENT_DATE}," +
                        /*  6 */ " ${CURRENT_TIMESTAMP}," +
                        /*  7 */ " %4$s," +
                        /*  8 */ " %5$s," +
                        /*  9 */ " '%6$s'," +
                        /* 10 */ " '%6$s'," +
                        /* 11 */ " '%6$s'," +
                        /* 12 */ " %7$s," +
                        /* 13 */ " %8$s," +
                        /* 14 */ " %9$s," +
                        /* 15 */ " %10$s," +
                        /* 16 */ " %11$s," +
                        /* 17 */ " %12$s" +
                        ");",
                /*  1 */  entity.getTopiaId(),
                /*  2 */
                /*  3 */  entity.getCode(),
                /*  4 */  entity.getStatus().ordinal(),
                /*  5 */
                /*  6 */
                /*  7 */  entity.isNeedComment(),
                /*  8 */  toId(entity.getSpecies()),
                /*  9, 10, 11 */  entity.getLabel2().replaceAll("'", "''''"),
                /* 12 */  DataWriter.roundFloat4(entity.getMinWeight()),
                /* 13 */  DataWriter.roundFloat4(entity.getMeanWeight()),
                /* 14 */  DataWriter.roundFloat4(entity.getMaxWeight()),
                /* 15 */  entity.isLanding(),
                /* 16 */  entity.isLogbook(),
                /* 17 */  entity.isWellPlan()
        );
        sqlWriter.writeSql(sql);
        createdEntities.put(entity.getCode(), entity);
    }
}
