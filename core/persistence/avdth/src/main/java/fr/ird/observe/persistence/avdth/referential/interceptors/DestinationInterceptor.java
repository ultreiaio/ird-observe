package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ps.landing.Destination;
import fr.ird.observe.entities.referential.ps.landing.DestinationSpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class DestinationInterceptor extends ReferentialInterceptor<Destination, DestinationSpi> {
    @AutoService(ReferentialQueryDefinition.class)
    public static class DestinationTable extends ReferentialQueryDefinition {
        /**
         * create table LOT_COM_DESTIN (
         * C_DEST SMALLINT not null primary key,
         * L_DEST VARCHAR(255),
         * STATUT BOOLEAN default 1 );
         */
        public DestinationTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(LOT_COM.C_DEST), LOT_COM_DESTIN.L_DEST";
        }

        @Override
        public String from() {
            return "LOT_COM INNER JOIN LOT_COM_DESTIN ON LOT_COM.C_DEST = LOT_COM_DESTIN.C_DEST";
        }

        @Override
        public String orderBy() {
            return "LOT_COM.C_DEST";
        }
    }

    public DestinationInterceptor(AvdthReferentialImportContext context) {
        super(context, Destination.SPI);
    }

    @Override
    public Destination read(ResultSet resultSet, Map<String, Destination> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        Destination entity = super.read(resultSet, existingIndex, sqlWriter);
        if (entity.isNotPersisted()) {
            // fill it from avdth
            entity.setStatus(ReferenceStatus.enabled);
            // labels
            String label = resultSet.getString(2);
            entity.setLabel1(label + " TODO");
            entity.setLabel2(label);
            entity.setLabel3(label + " TODO");
        }
        return entity;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(DestinationTable.class, connexion)) {
            return process(sqlWriter, tableReader, persistenceContext);
        }
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<Destination> newList) {
        result.setDestination(newList);
    }
}
