package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.Maps;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.common.FpaZone;
import fr.ird.observe.entities.referential.common.FpaZoneSpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class FpaZoneInterceptor extends ReferentialInterceptor<FpaZone, FpaZoneSpi> {

    @AutoService(ReferentialQueryDefinition.class)
    public static class FpaZoneTable extends ReferentialQueryDefinition {

        /**
         * create table ZONE_FPA (
         * C_Z_FPA  SMALLINT not null primary key,
         * L_Z_FPA  VARCHAR(255),
         * C_PAYS   SMALLINT constraint ZONE_FPA_PAYSZONE_FPA references PAYS,
         * C_SUBDIV VARCHAR(3),
         * STATUT   BOOLEAN default 1);
         */
        public FpaZoneTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(ACTIVITE.C_Z_FPA), ZONE_FPA.L_Z_FPA";
        }

        @Override
        public String from() {
            return "ACTIVITE INNER JOIN ZONE_FPA ON ACTIVITE.C_Z_FPA = ZONE_FPA.C_Z_FPA";
        }

        @Override
        public String orderBy() {
            return "ACTIVITE.C_Z_FPA";
        }
    }

    public FpaZoneInterceptor(AvdthReferentialImportContext context) {
        super(context, FpaZone.SPI);
    }

    @Override
    protected Map<String, FpaZone> existingIndex(ObserveTopiaPersistenceContext persistenceContext) {
        List<FpaZone> existing = spi.loadEntities(persistenceContext).stream().filter(r -> r.getHomeId() != null).collect(Collectors.toList());
        return Maps.uniqueIndex(existing, FpaZone::getHomeId);
    }

    @Override
    public FpaZone read(ResultSet resultSet, Map<String, FpaZone> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        FpaZone entity = super.read(resultSet, existingIndex, sqlWriter);
        if (entity.isNotPersisted()) {
            // fill it from avdth
            entity.setStatus(ReferenceStatus.enabled);
            // labels
            String label = resultSet.getString(2);
            entity.setLabel1(label + " TODO");
            entity.setLabel2(label);
            entity.setLabel3(label + " TODO");
        }
        return entity;
    }

    @Override
    protected FpaZone read(Map<String, FpaZone> existingIndex, String code) {
        FpaZone entity = super.read(existingIndex, code);
        if (entity.isNotPersisted()) {
            entity.setLabel2(entity.getHomeId());
            entity.setCode(null);
        }
        return entity;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(FpaZoneTable.class, connexion)) {
            return process(sqlWriter, tableReader, persistenceContext);
        }
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<FpaZone> newList) {
        result.setFpaZone(newList);
    }
}
