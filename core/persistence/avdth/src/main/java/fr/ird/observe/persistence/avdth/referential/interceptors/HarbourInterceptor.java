package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.common.Harbour;
import fr.ird.observe.entities.referential.common.HarbourSpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class HarbourInterceptor extends ReferentialInterceptor<Harbour, HarbourSpi> {

    @AutoService(ReferentialQueryDefinition.class)
    public static class DepartureHarbourTable extends ReferentialQueryDefinition {

        /**
         * create table PORT (
         * C_PORT   SMALLINT    not null primary key,
         * L_PORT   VARCHAR(64) not null,
         * V_LAT_P  NUMERIC(100, 7),
         * V_LON_P  NUMERIC(100, 7),
         * L_COM_P  VARCHAR(64),
         * C_LOCODE VARCHAR(15),
         * C_PAYS   SMALLINT constraint PORT_PAYSPORT references PAYS on update cascade );
         */

        public DepartureHarbourTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(MAREE.C_PORT_DEP) AS C_PORT," +
                    " PORT.L_PORT," +
                    " PORT.C_LOCODE," +
                    " PORT.V_LAT_P," +
                    " PORT.V_LON_P ";
        }

        @Override
        public String from() {
            return "MAREE INNER JOIN PORT ON MAREE.C_PORT_DEP = PORT.C_PORT";
        }

        @Override
        public String orderBy() {
            return "MAREE.C_PORT_DEP";
        }
    }

    @AutoService(ReferentialQueryDefinition.class)
    public static class LandingHarbourTable extends ReferentialQueryDefinition {

        /**
         * create table PORT (
         * C_PORT   SMALLINT    not null primary key,
         * L_PORT   VARCHAR(64) not null,
         * V_LAT_P  NUMERIC(100, 7),
         * V_LON_P  NUMERIC(100, 7),
         * L_COM_P  VARCHAR(64),
         * C_LOCODE VARCHAR(15),
         * C_PAYS   SMALLINT constraint PORT_PAYSPORT references PAYS on update cascade );
         */

        public LandingHarbourTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(MAREE.C_PORT_DBQ) AS C_PORT," +
                    " PORT.L_PORT," +
                    " PORT.C_LOCODE," +
                    " PORT.V_LAT_P," +
                    " PORT.V_LON_P ";
        }

        @Override
        public String from() {
            return "MAREE INNER JOIN PORT ON MAREE.C_PORT_DBQ = PORT.C_PORT";
        }

        @Override
        public String orderBy() {
            return "MAREE.C_PORT_DBQ";
        }
    }

    public HarbourInterceptor(AvdthReferentialImportContext context) {
        super(context, Harbour.SPI);
    }

    @Override
    public Harbour read(ResultSet resultSet, Map<String, Harbour> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        Harbour entity = super.read(resultSet, existingIndex, sqlWriter);
        float latitude = resultSet.getFloat(4);
        float longitude = resultSet.getFloat(5);
        if (entity.isNotPersisted()) {
            // fill it from avdth
            entity.setStatus(ReferenceStatus.enabled);
            // labels
            String label = resultSet.getString(2);
            entity.setLabel1(label + " TODO");
            entity.setLabel2(label);
            entity.setLabel3(label + " TODO");
            entity.setLocode(resultSet.getString(3));
            entity.setLatitude(latitude);
            entity.setLongitude(longitude);
        }
        return entity;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        int result = 0;
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(DepartureHarbourTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(LandingHarbourTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        return result;
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<Harbour> newList) {
        mergeNewReferential(result, newList, AvdthReferentialImportResult::getHarbour, Harbour::getCode);
    }
}
