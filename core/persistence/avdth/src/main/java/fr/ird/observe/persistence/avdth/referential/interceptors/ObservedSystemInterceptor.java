package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ps.common.ObservedSystem;
import fr.ird.observe.entities.referential.ps.common.ObservedSystemSpi;
import fr.ird.observe.entities.referential.ps.common.SchoolType;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObservedSystemInterceptor extends ReferentialInterceptor<ObservedSystem, ObservedSystemSpi> {
    /**
     * From avdth code to observe code.
     */
    public static final Map<String, String> CODE_MAPPING = ImmutableMap.<String, String>builder()
            .put("0", "0")
            .put("10", "18")
            .put("11", "23")
            .put("12", "24")
            .put("13", "25")
            .put("14", "26")
            .put("20", "20")
            .put("21", "20")
            .put("22", "20")
            .put("23", "20")
            .put("24", "20")
            .put("25", "20")
            .put("26", "17")
            .put("27", "27")
            .put("28", "28")
            .put("29", "31")
            .put("30", "4")
            .put("31", "4")
            .put("32", "32")
            .put("33", "33")
            .put("34", "34")
            .put("35", "35")
            .put("36", "36")
            .put("37", "37")
            .put("38", "38")
            .put("40", "40")
            .put("41", "41")
            .put("42", "42")
            .put("43", "43")
            .put("50", "50")
            .put("51", "11")
            .put("52", "10")
            .put("53", "53")
            .put("60", "12")
            .put("61", "13")
            .put("62", "62")
            .put("70", "70")
            .put("71", "71")
            .put("72", "72")
            .put("73", "73")
            .put("74", "74")
            .put("80", "80")
            .put("81", "81")
            .put("90", "90")
            .put("91", "91")
            .put("92", "92")
            .put("93", "93")
            .put("94", "94")
            .put("95", "95")
            .put("96", "96")
            .put("97", "97")
            .put("98", "98")
            .put("99", "101")
            .build();
    /**
     * Internal cache of created entities, keep it static to reuse if for more than one import...
     */
    private final static Map<String, ObservedSystem> createdEntities = new TreeMap<>();
    /**
     * Internal counter to simulate id on none persisted entries.
     */
    protected static int count = 2000;
    private Map<String, SchoolType> schoolTypesById;

    @AutoService(ReferentialQueryDefinition.class)
    public static class ObservedSystemTable extends ReferentialQueryDefinition {

        /**
         * create table ASSOC (
         * C_ASSOC   SMALLINT     not null primary key,
         * L_ASSOC   VARCHAR(255) not null,
         * C_ASSOC_R SMALLINT     not null,
         * C_ASSOC_G SMALLINT     not null );
         */
        public ObservedSystemTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(ACT_ASSOC.C_ASSOC), ASSOC.L_ASSOC, ASSOC.C_ASSOC_G";
        }

        @Override
        public String from() {
            return "ACT_ASSOC INNER JOIN ASSOC ON ACT_ASSOC.C_ASSOC = ASSOC.C_ASSOC";
        }

        @Override
        public String orderBy() {
            return "ACT_ASSOC.C_ASSOC";
        }
    }

    public ObservedSystemInterceptor(AvdthReferentialImportContext context) {
        super(context, ObservedSystem.SPI);
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(ObservedSystemTable.class, connexion)) {
            return process(sqlWriter, tableReader, persistenceContext);
        }
    }

    @Override
    protected Map<String, ObservedSystem> existingIndex(ObserveTopiaPersistenceContext persistenceContext) {
        schoolTypesById = Maps.uniqueIndex(SchoolType.SPI.loadEntities(persistenceContext), SchoolType::getTopiaId);
        return super.existingIndex(persistenceContext);
    }

    @Override
    protected ObservedSystem read(Map<String, ObservedSystem> existingIndex, String code) {
        String realCode = CODE_MAPPING.getOrDefault(code, code);
        ObservedSystem entity = super.read(existingIndex, realCode);
        entity.setCode(code);
        return entity;
    }

    @Override
    public ObservedSystem read(ResultSet resultSet, Map<String, ObservedSystem> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        ObservedSystem entity = super.read(resultSet, existingIndex, sqlWriter);
        if (entity.isNotPersisted()) {
            String code = entity.getCode();
            if (createdEntities.containsKey(code)) {
                return createdEntities.get(code);
            }
            // fill it from avdth
            entity.setStatus(ReferenceStatus.enabled);
            entity.setObservation(false);
            entity.setLogbook(true);
            // labels
            String label = resultSet.getString(2);
            entity.setLabel1(label + " TODO");
            entity.setLabel2(label);
            entity.setLabel3(label + " TODO");
            // schoolType
            String avdthCode = resultSet.getString(3);
            String schoolTypeId = SchoolTypeInterceptor.CODE_TO_ID_MAPPING.get(avdthCode);
            SchoolType schoolType = schoolTypesById.get(Objects.requireNonNull(schoolTypeId));
            entity.setSchoolType(Objects.requireNonNull(schoolType));
            String id = Strings.leftPad("" + (count++), 4, '0');
            String sql = String.format("INSERT INTO ps_common.ObservedSystem(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, schoolType, observation, logbook, label1, label2, label3) VALUES ('fr.ird.referential.common.Harbour#${REFERENTIAL_PREFIX}%1$s'  , 0, '%2$s' , 1, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, '%3$s'   , %4$s  , %5$s, '%6$s TODO', '%6$s', '%6$s TODO');",
                                       id, code, schoolTypeId, false, true, label.replaceAll("'", "''''"));
            sqlWriter.writeSql(sql);
            createdEntities.put(code, entity);
        }
        return entity;
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<ObservedSystem> newList) {
        mergeNewReferential(result, newList, AvdthReferentialImportResult::getObservedSystem, ObservedSystem::getCode);
    }
}
