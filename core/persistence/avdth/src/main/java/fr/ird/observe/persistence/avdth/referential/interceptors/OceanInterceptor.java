package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.OceanSpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class OceanInterceptor extends ReferentialInterceptor<Ocean, OceanSpi> {
    /**
     * avdth code to observe code.
     */
    public static final Set<String> CODE_TO_SKIP = new LinkedHashSet<>(Arrays.asList("4", "5"));
    /**
     * Internal cache of created entities, keep it static to reuse if for more than one import...
     */
    private final static Map<String, Ocean> createdEntities = new TreeMap<>();
    /**
     * Internal counter to simulate id on none persisted entries.
     */
    protected static int count = 0;

    @AutoService(ReferentialQueryDefinition.class)
    public static class OceanTable extends ReferentialQueryDefinition {
        /**
         * create table OCEAN (
         * C_OCEA SMALLINT    not null primary key,
         * L_OCEA VARCHAR(64) not null );
         */
        public OceanTable() {
            super(1);
        }

        @Override
        public String select() {
            return " DISTINCT(ACTIVITE.C_OCEA)," +
                    " OCEAN.L_OCEA";
        }

        @Override
        public String from() {
            return "ACTIVITE INNER JOIN OCEAN ON ACTIVITE.C_OCEA = OCEAN.C_OCEA";
        }

        @Override
        public String orderBy() {
            return "ACTIVITE.C_OCEA";
        }
    }

    public OceanInterceptor(AvdthReferentialImportContext context) {
        super(context, Ocean.SPI);
    }

    @Override
    protected Ocean read(Map<String, Ocean> existingIndex, String code) {
        if (CODE_TO_SKIP.contains(code)) {
            return null;
        }
        return super.read(existingIndex, code);
    }

    @Override
    public Ocean read(ResultSet resultSet, Map<String, Ocean> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        Ocean entity = super.read(resultSet, existingIndex, sqlWriter);
        if (entity == null) {
            return null;
        }
        if (entity.isNotPersisted()) {
            String code = entity.getCode();
            if (createdEntities.containsKey(code)) {
                return createdEntities.get(code);
            }
            // fill it from avdth
            entity.setStatus(ReferenceStatus.enabled);
            // labels
            String label = resultSet.getString(2);
            entity.setLabel1(label + " TODO");
            entity.setLabel2(label);
            entity.setLabel3(label + " TODO");

            String id = Strings.leftPad("" + (count++), 1, '0');
            String sql = String.format("INSERT INTO common.Ocean(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, label1, label2, label3) VALUES " +
                                               "('fr.ird.referential.common.Ocean#${REFERENTIAL_PREFIX}%1$s', 0, '%2$s' , %3$s, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, '%4$s TODO', '%4$s', '%4$s TODO');",
                                       id, code, entity.getStatus().ordinal(), label.replaceAll("'", "''''"));
            sqlWriter.writeSql(sql);
            createdEntities.put(code, entity);
        }
        return entity;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(OceanTable.class, connexion)) {
            return process(sqlWriter, tableReader, persistenceContext);
        }
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<Ocean> newList) {
        mergeNewReferential(result, newList, AvdthReferentialImportResult::getOcean, Ocean::getCode);
    }
}
