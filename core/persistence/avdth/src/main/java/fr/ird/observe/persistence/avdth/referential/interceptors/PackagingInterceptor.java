package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.Maps;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.common.Harbour;
import fr.ird.observe.entities.referential.ps.localmarket.BatchComposition;
import fr.ird.observe.entities.referential.ps.localmarket.BatchWeightType;
import fr.ird.observe.entities.referential.ps.localmarket.Packaging;
import fr.ird.observe.entities.referential.ps.localmarket.PackagingSpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import static fr.ird.observe.spi.SqlConversions.day;
import static fr.ird.observe.spi.SqlConversions.toId;
/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class PackagingInterceptor extends ReferentialInterceptor<Packaging, PackagingSpi> {
    /**
     * Internal cache of created entities, keep it static to reuse if for more than one import...
     */
    private final static Map<String, Packaging> createdEntities = new TreeMap<>();
    /**
     * Internal counter to simulate id on none persisted entries.
     */
    protected static int count = 0;

    private final Map<String, String> codePackagingToBatchCompositionId =
            Map.of(
                    "1", "fr.ird.referential.ps.localmarket.BatchComposition#1464000000000#01",
                    "2", "fr.ird.referential.ps.localmarket.BatchComposition#1464000000000#02",
                    "3", "fr.ird.referential.ps.localmarket.BatchComposition#1464000000000#02",
                    "4", "fr.ird.referential.ps.localmarket.BatchComposition#1464000000000#02"
            );
    private final Map<String, String> codePackagingToBatchWeightTypeId = Map.of(
            "1", "fr.ird.referential.ps.localmarket.BatchWeightType#1464000000000#01",
            "2", "fr.ird.referential.ps.localmarket.BatchWeightType#1464000000000#01",
            "3", "fr.ird.referential.ps.localmarket.BatchWeightType#1464000000000#02",
            "4", "fr.ird.referential.ps.localmarket.BatchWeightType#1464000000000#03"
    );
    private Map<String, BatchComposition> batchCompositionById;
    private Map<String, BatchWeightType> batchWeightTypeById;
    private Map<String, Harbour> harbourByCode;

    @AutoService(ReferentialQueryDefinition.class)
    public static class PackagingTable extends ReferentialQueryDefinition {

        /**
         * create table FP_CONDITIONNEMENT (
         * C_COND     SMALLINT     not null primary key,
         * C_PORT     SMALLINT     not null constraint FP_CONDITIONNEMENT_PORTFP_CONDITIONNEMENT references PORT,
         * D_DEBUT    TIMESTAMP,
         * D_FIN      TIMESTAMP,
         * L_COND     VARCHAR(255) not null,
         * C_TYP_COND SMALLINT     not null constraint FP_CONDITIONNEMENT_FP_TYPE_CONDFP_CONDITIONNEMENT references FP_TYPE_COND on update cascade,
         * V_POIDS    NUMERIC(100, 3) );
         */
        public PackagingTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(FP_LOT.C_COND), FP_CONDITIONNEMENT.L_COND, FP_CONDITIONNEMENT.C_PORT, FP_CONDITIONNEMENT.D_DEBUT, FP_CONDITIONNEMENT.D_FIN, FP_CONDITIONNEMENT.C_TYP_COND, FP_CONDITIONNEMENT.V_POIDS";
        }

        @Override
        public String from() {
            return "FP_LOT INNER JOIN FP_CONDITIONNEMENT ON FP_CONDITIONNEMENT.C_COND = FP_LOT.C_COND";
        }

        @Override
        public String orderBy() {
            return "FP_LOT.C_COND";
        }
    }

    public PackagingInterceptor(AvdthReferentialImportContext context) {
        super(context, Packaging.SPI);
    }

    @Override
    protected Map<String, Packaging> existingIndex(ObserveTopiaPersistenceContext persistenceContext) {
        batchCompositionById = Maps.uniqueIndex(BatchComposition.SPI.loadEntities(persistenceContext), BatchComposition::getTopiaId);
        batchWeightTypeById = Maps.uniqueIndex(BatchWeightType.SPI.loadEntities(persistenceContext), BatchWeightType::getTopiaId);
        harbourByCode = Maps.uniqueIndex(Harbour.SPI.loadEntities(persistenceContext), Harbour::getCode);
        return super.existingIndex(persistenceContext);
    }

    @Override
    public Packaging read(ResultSet resultSet, Map<String, Packaging> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        Packaging entity = super.read(resultSet, existingIndex, sqlWriter);
        if (entity.isNotPersisted()) {
            // fill it from avdth
            entity.setStatus(ReferenceStatus.enabled);
            String code = entity.getCode();
            if (createdEntities.containsKey(code)) {
                return createdEntities.get(code);
            }
            // labels
            String label = resultSet.getString(2);
            entity.setLabel1(label + " TODO");
            entity.setLabel2(label);
            entity.setLabel3(label + " TODO");
            String codeHarbour = resultSet.getString(3);
            Harbour harbour = harbourByCode.get(codeHarbour);
            entity.setHarbour(Objects.requireNonNull(harbour));
            Date startDate = resultSet.getDate(4);
            entity.setStartDate(startDate);
            Date endDate = resultSet.getDate(5);
            entity.setEndDate(endDate);
            String codePackaging = resultSet.getString(6);
            String compositionId = codePackagingToBatchCompositionId.get(codePackaging);
            String weightTypeId = codePackagingToBatchWeightTypeId.get(codePackaging);
            BatchComposition batchComposition = batchCompositionById.get(Objects.requireNonNull(compositionId));
            entity.setBatchComposition(Objects.requireNonNull(batchComposition));
            BatchWeightType batchWeightType = batchWeightTypeById.get(Objects.requireNonNull(weightTypeId));
            entity.setBatchWeightType(Objects.requireNonNull(batchWeightType));
            Float weight = resultSet.getFloat(7);
            entity.setMeanWeight(weight);

            String id = Strings.leftPad("" + (count++), 3, '0');
            String sql = String.format("INSERT INTO ps_localmarket.Packaging(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, startDate, endDate, meanWeight, harbour, batchComposition, batchWeightType, label1, label2, label3) VALUES " +
                                               "('fr.ird.referential.ps.localmarket.Packaging#${REFERENTIAL_PREFIX}%1$s', 0, '%2$s' , %3$s, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, %4$s, %5$s, %6$s,%7$s,%8$s,%9$s, '%10$s TODO', '%10$s', '%10$s TODO');",
                                       id,
                                       code,
                                       entity.getStatus().ordinal(),
                                       day(entity.getStartDate()),
                                       day(entity.getEndDate()),
                                       entity.getMeanWeight(),
                                       toId(entity.getHarbour()),
                                       toId(entity.getBatchComposition()),
                                       toId(entity.getBatchWeightType()),
                                       label.replaceAll("'", "''''"));
            sqlWriter.writeSql(sql);
            createdEntities.put(code, entity);
        }
        return entity;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(PackagingTable.class, connexion)) {
            return process(sqlWriter, tableReader, persistenceContext);
        }
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<Packaging> newList) {
        result.setPackaging(newList);
    }
}
