package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ps.common.SampleType;
import fr.ird.observe.entities.referential.ps.common.SampleTypeSpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class SampleTypeInterceptor extends ReferentialInterceptor<SampleType, SampleTypeSpi> {

    @AutoService(ReferentialQueryDefinition.class)
    public static class SampleTypeTable extends ReferentialQueryDefinition {
        /**
         * create table TYPE_ECHANT (
         * C_TYP_ECH SMALLINT default 1 not null primary key,
         * L_TYP_ECH VARCHAR(64)        not null );
         */
        public SampleTypeTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(ECHANTILLON.C_TYP_ECH)";
        }


        @Override
        public String from() {
            return "ECHANTILLON";
        }

        @Override
        public String innerJoin() {
            return "TYPE_ECHANT ON ECHANTILLON.C_TYP_ECH = TYPE_ECHANT.C_TYP_ECH";
        }

        @Override
        public String orderBy() {
            return "ECHANTILLON.C_TYP_ECH";
        }
    }

    public SampleTypeInterceptor(AvdthReferentialImportContext context) {
        super(context, SampleType.SPI);
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(SampleTypeTable.class, connexion)) {
            return process(sqlWriter, tableReader, persistenceContext);
        }
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<SampleType> newList) {
        mergeNewReferential(result, newList, AvdthReferentialImportResult::getSampleType, SampleType::getCode);
    }
}
