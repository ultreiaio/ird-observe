package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ps.common.SchoolType;
import fr.ird.observe.entities.referential.ps.common.SchoolTypeSpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class SchoolTypeInterceptor extends ReferentialInterceptor<SchoolType, SchoolTypeSpi> {
    /**
     * From avdth to observe code.
     */
    public static final Map<String, String> CODE_MAPPING = Map.of(
            "1", "1",
            "2", "2",
            "3", "0");
    /**
     * From code to topiaId.
     */
    public static final Map<String, String> CODE_TO_ID_MAPPING = Map.of(
            "1", "fr.ird.referential.ps.common.SchoolType#0#1",
            "2", "fr.ird.referential.ps.common.SchoolType#0#2",
            "3", "fr.ird.referential.ps.common.SchoolType#0#0");

    @AutoService(ReferentialQueryDefinition.class)
    public static class SchoolTypeTable extends ReferentialQueryDefinition {

        /**
         * create table TYPE_BANC (
         * C_TBANC   SMALLINT    not null primary key,
         * L_TBANC4L VARCHAR(4)  not null,
         * L_TBANC   VARCHAR(64) not null );
         */
        public SchoolTypeTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(ACTIVITE.C_TBANC)";
        }

        @Override
        public String from() {
            return "ACTIVITE INNER JOIN TYPE_BANC ON ACTIVITE.C_TBANC = TYPE_BANC.C_TBANC";
        }

        @Override
        public String orderBy() {
            return "ACTIVITE.C_TBANC";
        }
    }

    public SchoolTypeInterceptor(AvdthReferentialImportContext context) {
        super(context, SchoolType.SPI);
    }

    @Override
    protected SchoolType read(Map<String, SchoolType> existingIndex, String code) {
        String realCode = CODE_MAPPING.get(code);
        return super.read(existingIndex, Objects.requireNonNull(realCode));
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(SchoolTypeTable.class, connexion)) {
            return process(sqlWriter, tableReader, persistenceContext);
        }
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<SchoolType> newList) {
        mergeNewReferential(result, newList, AvdthReferentialImportResult::getSchoolType, SchoolType::getCode);
    }
}
