package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.Maps;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.common.SpeciesSpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import fr.ird.observe.persistence.avdth.referential.SpeciesCache;
import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class SpeciesInterceptor extends ReferentialInterceptor<Species, SpeciesSpi> {

    private static final Logger log = LogManager.getLogger(SpeciesInterceptor.class);
    /**
     * Internal cache of created entities, keep it static to reuse if for more than one import...
     */
    private static final Map<String, Species> createdEntities = new TreeMap<>();
    /**
     * Internal counter to simulate id on none persisted entries.
     */
    protected static int count = 10000;


    /**
     * Get all species used in catches.
     */
    @AutoService(ReferentialQueryDefinition.class)
    public static class CatchSpeciesTable extends ReferentialQueryDefinition {

        public CatchSpeciesTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(CAPT_ELEM.C_ESP), ESPECE.C_ESP_3L, ESPECE.L_ESP";
        }

        @Override
        public String from() {
            return "CAPT_ELEM";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON CAPT_ELEM.C_ESP = ESPECE.C_ESP";
        }

        @Override
        public String orderBy() {
            return "CAPT_ELEM.C_ESP";
        }
    }

    /**
     * Get all species used in landing.
     */
    @AutoService(ReferentialQueryDefinition.class)
    public static class LandingSpeciesTable extends ReferentialQueryDefinition {

        public LandingSpeciesTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(LOT_COM.C_ESP), ESPECE.C_ESP_3L, ESPECE.L_ESP";
        }

        @Override
        public String from() {
            return "LOT_COM";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON LOT_COM.C_ESP = ESPECE.C_ESP";
        }

        @Override
        public String orderBy() {
            return "LOT_COM.C_ESP";
        }
    }

    /**
     * Get all species used in samples.
     */
    @AutoService(ReferentialQueryDefinition.class)
    public static class SampleSpeciesTable extends ReferentialQueryDefinition {

        public SampleSpeciesTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(ECH_ESP.C_ESP), ESPECE.C_ESP_3L, ESPECE.L_ESP";
        }

        @Override
        public String from() {
            return "ECH_ESP";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON ECH_ESP.C_ESP = ESPECE.C_ESP";
        }

        @Override
        public String orderBy() {
            return "ECH_ESP.C_ESP";
        }
    }

    /**
     * Get all species used in well plans.
     */
    @AutoService(ReferentialQueryDefinition.class)
    public static class WellPlanSpeciesTable extends ReferentialQueryDefinition {

        public WellPlanSpeciesTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(CUVE_CALEE.C_ESP), ESPECE.C_ESP_3L, ESPECE.L_ESP";
        }

        @Override
        public String from() {
            return "CUVE_CALEE";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON CUVE_CALEE.C_ESP = ESPECE.C_ESP";
        }

        @Override
        public String orderBy() {
            return "CUVE_CALEE.C_ESP";
        }
    }

    /**
     * Get all species used in local market batches.
     */
    @AutoService(ReferentialQueryDefinition.class)
    public static class LocalMarketBatchSpeciesTable extends ReferentialQueryDefinition {

        public LocalMarketBatchSpeciesTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(FP_LOT.C_ESP), ESPECE.C_ESP_3L, ESPECE.L_ESP";
        }

        @Override
        public String from() {
            return "FP_LOT";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON FP_LOT.C_ESP = ESPECE.C_ESP";
        }

        @Override
        public String orderBy() {
            return "FP_LOT.C_ESP";
        }
    }

    /**
     * Get all species used in local market surveys.
     */
    @AutoService(ReferentialQueryDefinition.class)
    public static class LocalMarketSurveySpeciesTable extends ReferentialQueryDefinition {

        public LocalMarketSurveySpeciesTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(FP_SONDAGE.C_ESP), ESPECE.C_ESP_3L, ESPECE.L_ESP";
        }

        @Override
        public String from() {
            return "FP_SONDAGE";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON FP_SONDAGE.C_ESP = ESPECE.C_ESP";
        }

        @Override
        public String orderBy() {
            return "FP_SONDAGE.C_ESP";
        }
    }

    @AutoService(ReferentialQueryDefinition.class)
    public static class LocalMarketSampleSpeciesTable extends ReferentialQueryDefinition {

        public LocalMarketSampleSpeciesTable() {
            super(1);
        }

        @Override
        public String select() {
            return "DISTINCT(FP_ECH_ESP.C_ESP), ESPECE.C_ESP_3L, ESPECE.L_ESP";
        }

        @Override
        public String from() {
            return "FP_ECH_ESP";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON FP_ECH_ESP.C_ESP = ESPECE.C_ESP";
        }

        @Override
        public String orderBy() {
            return "FP_ECH_ESP.C_ESP";
        }
    }

    public SpeciesInterceptor(AvdthReferentialImportContext context) {
        super(context, Species.SPI);
    }

    @Override
    protected Map<String, Species> existingIndex(ObserveTopiaPersistenceContext persistenceContext) {
        List<Species> existing = Species.SPI.loadEntities(persistenceContext);
        return Maps.uniqueIndex(existing, Species::getFaoCode);
    }

    @Override
    public Species read(ResultSet resultSet, Map<String, Species> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        String originalSpeciesCode = resultSet.getString(1);
        String originalFaoCode = resultSet.getString(2);
        String label = resultSet.getString(3);
        // try by species code and fao code
        Species source = SpeciesCache.translateUnsafeSpecies(existingIndex, originalSpeciesCode, originalFaoCode);
        if (source != null) {
            return source;
        }
        // try by created
        source = createdEntities.get(originalSpeciesCode);
        if (source != null) {
            return source;
        }
        String translatedFaoCode = SpeciesCache.translateFaoCode(originalSpeciesCode, originalFaoCode);
        // try by created (on faoCode)
        source = createdEntities.values().stream().filter(e->translatedFaoCode.equals(e.getFaoCode())).findAny().orElse(null);
        if (source != null) {
            return source;
        }
        Species entity = spi.newEntity(context.getConfiguration().getNow());
        log.warn(String.format("Need to create new species: %s-%s", originalSpeciesCode, translatedFaoCode));
        entity.setFaoCode(translatedFaoCode);
        entity.setStatus(ReferenceStatus.enabled);

        entity.setLabel1(label + " TODO");
        entity.setLabel2(label);
        entity.setLabel3(label + " TODO");

        entity.setCode(originalSpeciesCode);

        if (entity.isNotPersisted()) {
            String id = Strings.leftPad("" + (count++), 5, '0');
            String sql = String.format("INSERT INTO common.Species(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, faoCode, label1, label2, label3) VALUES " +
                                               "('fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}%1$s', 0, '%2$s' , %3$s, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, '%4$s', '%5$s TODO', '%5$s', '%5$s TODO');",
                                       id, originalSpeciesCode, entity.getStatus().ordinal(), entity.getFaoCode(), label.replaceAll("'", "''''"));
            sqlWriter.writeSql(sql);
            createdEntities.put(originalSpeciesCode, entity);
        }
        return entity;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        int result = 0;
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(CatchSpeciesTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(SampleSpeciesTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(LandingSpeciesTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(WellPlanSpeciesTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(LocalMarketBatchSpeciesTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(LocalMarketSurveySpeciesTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(LocalMarketSampleSpeciesTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        return result;
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<Species> newList) {
        List<Species> existing = new LinkedList<>(new LinkedHashSet<>(result.getSpecies()));
        Map<String, Species> speciesByFaoCode = new TreeMap<>(Maps.uniqueIndex(existing, Species::getFaoCode));
        newList.stream().filter(species -> !speciesByFaoCode.containsKey(species.getFaoCode())).forEach(existing::add);
        result.setSpecies(existing);
    }
}
