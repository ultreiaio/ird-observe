package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableMap;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyType;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyTypeSpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TransmittingBuoyTypeInterceptor extends ReferentialInterceptor<TransmittingBuoyType, TransmittingBuoyTypeSpi> {
    /**
     * From avdth to observe code.
     */
    public static final Map<String, String> CODE_MAPPING = ImmutableMap.<String, String>builder()
            .put("11", "90")
            .put("12", "21")
            .put("13", "22")
            .put("14", "23")
            .put("15", "24")
            .put("16", "25")
            .put("17", "26")
            .put("18", "27")
            .put("19", "28")
            .put("20", "41")
            .put("21", "42")
            .put("22", "43")
            .put("23", "44")
            .put("24", "45")
            .put("25", "46")
            .put("26", "47")
            .put("27", "100")
            .put("28", "61")
            .put("29", "62")
            .put("30", "63")
            .put("31", "64")
            .put("32", "65")
            .put("33", "66")
            .put("34", "67")
            .put("35", "68")
            .put("36", "69")
            .put("37", "70")
            .put("38", "71")
            .put("39", "3")
            .put("40", "91")
            .put("98", "99")
            .build();

    @SuppressWarnings("SpellCheckingInspection")
    @AutoService(ReferentialQueryDefinition.class)
    public static class TransmittingBuoyTypeTable extends ReferentialQueryDefinition {

        /**
         * create table TYPE_BALISE (
         * C_TYP_BALISE SMALLINT     not null primary key,
         * C_TOPIAID    VARCHAR(255),
         * L_TYP_BALISE VARCHAR(255) not null,
         * STATUT       BOOLEAN default 1 );
         */
        public TransmittingBuoyTypeTable() {
            super(1);

        }

        @Override
        public String select() {
            return "DISTINCT(ACTIVITE.C_TYP_BALISE), TYPE_BALISE.L_TYP_BALISE";
        }

        @Override
        public String from() {
            return "ACTIVITE INNER JOIN TYPE_BALISE ON ACTIVITE.C_TYP_BALISE = TYPE_BALISE.C_TYP_BALISE";
        }

        @Override
        public String orderBy() {
            return "ACTIVITE.C_TYP_BALISE";
        }
    }

    public TransmittingBuoyTypeInterceptor(AvdthReferentialImportContext context) {
        super(context, TransmittingBuoyType.SPI);
    }

    @Override
    public TransmittingBuoyType read(ResultSet resultSet, Map<String, TransmittingBuoyType> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        TransmittingBuoyType entity = super.read(resultSet, existingIndex, sqlWriter);
        if (entity.isNotPersisted()) {
            // fill it from avdth
            entity.setStatus(ReferenceStatus.enabled);
            // labels
            String label = resultSet.getString(2);
            entity.setLabel1(label + " TODO");
            entity.setLabel2(label);
            entity.setLabel3(label + " TODO");
        }
        return entity;
    }

    @Override
    protected TransmittingBuoyType read(Map<String, TransmittingBuoyType> existingIndex, String code) {
        String realCode = CODE_MAPPING.getOrDefault(code, code);
        TransmittingBuoyType entity = super.read(existingIndex, realCode);
        entity.setCode(code);
        return entity;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(TransmittingBuoyTypeTable.class, connexion)) {
            return process(sqlWriter, tableReader, persistenceContext);
        }
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<TransmittingBuoyType> newList) {
        mergeNewReferential(result, newList, AvdthReferentialImportResult::getTransmittingBuoyType, TransmittingBuoyType::getCode);
    }
}
