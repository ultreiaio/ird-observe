package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.common.Vessel;
import fr.ird.observe.entities.referential.common.VesselSpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 22/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class VesseInterceptor extends ReferentialInterceptor<Vessel, VesselSpi> {
    /**
     * Internal cache of created entities, keep it static to reuse if for more than one import...
     */
    private final static Map<String, Vessel> createdEntities = new TreeMap<>();
    /**
     * Internal counter to simulate id on none persisted entries.
     */
    protected static int count = 8000;

    @AutoService(ReferentialQueryDefinition.class)
    public static class VesseTable extends ReferentialQueryDefinition {
        /**
         * create table BATEAU
         * ( C_BAT SMALLINT not null primary key,
         * C_PAYS SMALLINT not null constraint BATEAU_CFK_PAYS_R_BATEAU references PAYS,
         * C_TYP_B SMALLINT not null constraint BATEAU_CFK_TYPE_BATEAU_R_BATEAU references TYPE_BATEAU,
         * C_CAT_B SMALLINT not null constraint BATEAU_CFK_CAT_BATEAU_R_BATEAU references CAT_BATEAU,
         * C_QUILLE SMALLINT,
         * C_GEN INTEGER,
         * C_FLOTTE SMALLINT,
         * AN_SERV SMALLINT,
         * D_CHGT_PAV TIMESTAMP,
         * V_L_HT NUMERIC(100,7),
         * L_PP NUMERIC(100,7),
         * V_CT_M3 NUMERIC(100,7),
         * V_P_CV NUMERIC(100,7),
         * V_CHANT NUMERIC(100,7),
         * V_MAX_RECH NUMERIC(100,7),
         * V_MAX NUMERIC(100,7),
         * L_BAT VARCHAR(64) not null,
         * L_COM_B VARCHAR(255),
         * D_DEBUT TIMESTAMP,
         * D_FIN TIMESTAMP,
         * C_IMMAT_NAT VARCHAR(127),
         * C_IMMAT_COMM VARCHAR(127),
         * C_IMMAT_FAO VARCHAR(127),
         * STATUT BOOLEAN default 0 );
         */
        public VesseTable() {
            super(1);
        }

        @Override
        public String select() {
            return " DISTINCT(MAREE.C_BAT)," +
                    " BATEAU.L_BAT";
        }

        @Override
        public String from() {
            return "MAREE INNER JOIN BATEAU ON MAREE.C_BAT = BATEAU.C_BAT";
        }

        @Override
        public String orderBy() {
            return "MAREE.C_BAT";
        }
    }

    public VesseInterceptor(AvdthReferentialImportContext context) {
        super(context, Vessel.SPI);
    }

    @Override
    public Vessel read(ResultSet resultSet, Map<String, Vessel> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        Vessel entity = super.read(resultSet, existingIndex, sqlWriter);
        if (entity.isNotPersisted()) {
            // fill it from avdth
            entity.setStatus(ReferenceStatus.enabled);
            String code = entity.getCode();
            if (createdEntities.containsKey(code)) {
                return createdEntities.get(code);
            }
            // labels
            String label = resultSet.getString(2);
            entity.setLabel1(label + " TODO");
            entity.setLabel2(label);
            entity.setLabel3(label + " TODO");
            String id = Strings.leftPad("" + (count++), 4, '0');
            String sql = String.format("INSERT INTO common.Vessel(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, label1, label2, label3) VALUES " +
                                               "('fr.ird.referential.common.Vessel#${REFERENTIAL_PREFIX}%1$s', 0, '%2$s' , %3$s, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, '%4$s TODO', '%4$s', '%4$s TODO');",
                                       id, code, entity.getStatus().ordinal(), label.replaceAll("'", "''''"));
            sqlWriter.writeSql(sql);
            createdEntities.put(code, entity);
        }
        return entity;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(VesseTable.class, connexion)) {
            return process(sqlWriter, tableReader, persistenceContext);
        }
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<Vessel> newList) {
        result.setVessel(newList);
    }
}
