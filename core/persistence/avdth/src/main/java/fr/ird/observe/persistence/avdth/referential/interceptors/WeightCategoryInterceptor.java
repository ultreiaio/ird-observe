package fr.ird.observe.persistence.avdth.referential.interceptors;

/*-
 * #%L
 * ObServe Core :: Persistence :: Avdth
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.Maps;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.ps.common.WeightCategory;
import fr.ird.observe.entities.referential.ps.common.WeightCategorySpi;
import fr.ird.observe.persistence.avdth.AvdthQueries;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.persistence.avdth.QueryDefinition;
import fr.ird.observe.persistence.avdth.data.DataWriter;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportContext;
import fr.ird.observe.persistence.avdth.referential.AvdthReferentialImportResult;
import fr.ird.observe.persistence.avdth.referential.ReferentialInterceptor;
import fr.ird.observe.persistence.avdth.referential.ReferentialQuery;
import fr.ird.observe.persistence.avdth.referential.ReferentialQueryDefinition;
import fr.ird.observe.persistence.avdth.referential.SpeciesCache;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static fr.ird.observe.spi.SqlConversions.toId;

/**
 * Created on 04/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class WeightCategoryInterceptor extends ReferentialInterceptor<WeightCategory, WeightCategorySpi> {

    public static final Set<String> SKIP_FAO_CODE = Set.of(
            "TUS",
            "TUN",
            "PIL",
            "CLP",
            "JAX",
            "PAX",
            "ANE",
            "BOG",
            "MZZ");
    /**
     * Internal cache of created entities, keep it static to reuse if for more than one import...
     */
    private final static Map<String, WeightCategory> createdEntities = new TreeMap<>();
    /**
     * Internal counter to simulate id on none persisted entries.
     */
    protected static int count = 0;
    private Map<String, Species> speciesByFaoCode;
    private Class<? extends QueryDefinition> tableDescriptorClass;

    @AutoService(ReferentialQueryDefinition.class)
    public static class LandingWeightCategoryTable extends ReferentialQueryDefinition {

        public LandingWeightCategoryTable() {
            super(1);
        }

        @Override
        public String select() {
            return  /* 1 */" DISTINCT(LOT_COM.C_ESP || '-' || LOT_COM.C_CAT_C) AS CODE," +
                    /* 2 */" LOT_COM.C_ESP," +
                    /* 3 */" ESPECE.C_ESP_3L," +
                    /* 4 */" LOT_COM.C_CAT_C," +
                    /* 5 */" CAT_COM.L_CC_SOV," +
                    /* 6 */" CAT_COM.L_CC_STAR," +
                    /* 7 */" CAT_COM.STATUT";
        }

        @Override
        public String from() {
            return "LOT_COM INNER JOIN CAT_COM ON LOT_COM.C_CAT_C = CAT_COM.C_CAT_C AND LOT_COM.C_ESP = CAT_COM.C_ESP";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON ESPECE.C_ESP = CAT_COM.C_ESP";
        }

        @Override
        public String orderBy() {
            return "LOT_COM.C_ESP, LOT_COM.C_CAT_C";
        }
    }

    @AutoService(ReferentialQueryDefinition.class)
    public static class CatchWeightCategoryTable extends ReferentialQueryDefinition {

        public CatchWeightCategoryTable() {
            super(1);
        }

        @Override
        public String select() {
            return  /* 1 */" DISTINCT('C-' || CAT_TAILLE.C_ESP || '-' || CAT_TAILLE.C_CAT_T) AS CODE," +
                    /* 2 */" CAT_TAILLE.C_ESP," +
                    /* 3 */" ESPECE.C_ESP_3L," +
                    /* 4 */" CAT_TAILLE.C_CAT_T," +
                    /* 5 */" CAT_TAILLE.L_CAT_T," +
                    /* 6 */" CAT_TAILLE.V_POIDS_M," +
                    /* 7 */" CAT_TAILLE.STATUT";
        }

        @Override
        public String from() {
            return "CAPT_ELEM INNER JOIN CAT_TAILLE ON CAPT_ELEM.C_CAT_T = CAT_TAILLE.C_CAT_T AND CAPT_ELEM.C_ESP = CAT_TAILLE.C_ESP";
        }

        @Override
        public String innerJoin() {
            return "ESPECE ON ESPECE.C_ESP = CAT_TAILLE.C_ESP";
        }

        @Override
        public String orderBy() {
            return "CAT_TAILLE.C_ESP, CAT_TAILLE.C_CAT_T";
        }
    }

    @AutoService(ReferentialQueryDefinition.class)
    public static class WellPlanWeightCategoryTable extends ReferentialQueryDefinition {

        public WellPlanWeightCategoryTable() {
            super(1);
        }

        @Override
        public String select() {
            return  /* 1 */" 'W-' || CAT_POIDS.C_CAT_POIDS AS CODE," +
                    /* 2 */" CAT_POIDS.C_CAT_POIDS," +
                    /* 3 */" CAT_POIDS.L_CAT_POIDS";
        }

        @Override
        public String from() {
            return "CAT_POIDS";
        }

        @Override
        public String orderBy() {
            return "CAT_POIDS.C_CAT_POIDS";
        }
    }

    public static boolean rejectCatchWeightCategory(Species species, String weightCategoryCode) {
        if (weightCategoryCode.equals("9")) {
            return true;
        }
        return SKIP_FAO_CODE.contains(species.getFaoCode());
    }

    public static boolean rejectLandingWeightCategory(String weightCategoryCode) {
        return weightCategoryCode.equals("9");
    }

    public static String getCatchWeightCategoryCode(Species species, String weightCategoryCode) {
        return "C-" + species.getFaoCode() + "-" + weightCategoryCode;
    }

    public static String getLandingWeightCategoryCode(Species species, String weightCategoryCode) {
        return "L-" + species.getFaoCode() + "-" + weightCategoryCode;
    }

    public static WeightCategory getCatchWeightCategory(Map<String, WeightCategory> categories, Species species, String weightCategoryCode) {
        if (rejectCatchWeightCategory(species, weightCategoryCode)) {
            return null;
        }
        String categoryCode = getCatchWeightCategoryCode(species, weightCategoryCode);
        WeightCategory weightCategory = categories.get(categoryCode);
        return Objects.requireNonNull(weightCategory, "Can't find weight category for code: " + categoryCode);
    }

    public static WeightCategory getLandingWeightCategory(Map<String, WeightCategory> categories, Species species, String weightCategoryCode) {
        if (rejectLandingWeightCategory(weightCategoryCode)) {
            return null;
        }
        String categoryCode = getLandingWeightCategoryCode(species, weightCategoryCode);
        WeightCategory weightCategory = categories.get(categoryCode);
        return Objects.requireNonNull(weightCategory, "Can't find weight category for code: " + categoryCode);
    }

    public WeightCategoryInterceptor(AvdthReferentialImportContext context) {
        super(context, WeightCategory.SPI);
    }

    @Override
    protected Map<String, WeightCategory> existingIndex(ObserveTopiaPersistenceContext persistenceContext) {
        speciesByFaoCode = Maps.uniqueIndex(context.getResult().getSpecies(), Species::getFaoCode);
        List<WeightCategory> existing = spi.loadEntities(persistenceContext);
        return Maps.uniqueIndex(existing.stream().filter(d -> d.getCode().contains("-")).collect(Collectors.toList()), ReferentialEntity::getCode);
    }

    @Override
    public WeightCategory read(ResultSet resultSet, Map<String, WeightCategory> existingIndex, SqlScriptWriter sqlWriter) throws SQLException {
        WeightCategory entity = super.read(resultSet, existingIndex, sqlWriter);
        if (entity.isNotPersisted()) {

            if (tableDescriptorClass.equals(WellPlanWeightCategoryTable.class)) {

                String code = "W-" + resultSet.getString(1);
                if (existingIndex.containsKey(code)) {
                    return existingIndex.get(code);
                }
                if (createdEntities.containsKey(code)) {
                    return createdEntities.get(code);
                }
                // fill it from avdth
                entity.setStatus(ReferenceStatus.enabled);
                entity.setWellPlan(true);

                entity.setCode(code);
                // labels
                String label = resultSet.getString(2);
                entity.setLabel1(label + " TODO");
                entity.setLabel2(label);
                entity.setLabel3(label + " TODO");

                write(sqlWriter, entity);

            } else if (tableDescriptorClass.equals(CatchWeightCategoryTable.class)) {
                String originalSpeciesCode = resultSet.getString(2);
                String originalFaoCode = resultSet.getString(3);
                String categoryCode = resultSet.getString(4);
                // species
                Species species = SpeciesCache.translateSpecies(speciesByFaoCode, originalSpeciesCode, originalFaoCode);
                if (rejectCatchWeightCategory(species, categoryCode)) {
                    return null;
                }

                // labels
                String label = resultSet.getString(5).replaceAll("\n", " ");
                if (label.contains("cat. ?")) {
                    return null;
                }
                if (label.contains("Sardine ?")) {
                    return null;
                }
                // fill it from avdth
                entity.setLogbook(true);
                entity.setSpecies(species);

                String code = getCatchWeightCategoryCode(species, categoryCode);

                if (existingIndex.containsKey(code)) {
                    return existingIndex.get(code);
                }
                if (createdEntities.containsKey(code)) {
                    return createdEntities.get(code);
                }
                entity.setCode(code);
                entity.setLabel1(label + " TODO");
                entity.setLabel2(label);
                entity.setLabel3(label + " TODO");
                Object meanWeight = resultSet.getObject(6);
                if (meanWeight != null) {
                    entity.setMeanWeight(Float.valueOf(meanWeight.toString()));
                }
                // status
                boolean statusCode = resultSet.getBoolean(7);
                entity.setStatus(statusCode ? ReferenceStatus.enabled : ReferenceStatus.disabled);
                write(sqlWriter, entity);

            } else if (tableDescriptorClass.equals(LandingWeightCategoryTable.class)) {
                entity.setLanding(true);
                String originalSpeciesCode = resultSet.getString(2);
                String originalFaoCode = resultSet.getString(3);
                String categoryCode = resultSet.getString(4);
                // species
                Species species = SpeciesCache.translateSpecies(speciesByFaoCode, originalSpeciesCode, originalFaoCode);
                entity.setSpecies(species);
                if (rejectLandingWeightCategory(categoryCode)) {
                    return null;
                }
                String code = getLandingWeightCategoryCode(species, categoryCode);
                entity.setCode(code);
                if (existingIndex.containsKey(code)) {
                    return existingIndex.get(code);
                }
                if (createdEntities.containsKey(code)) {
                    return createdEntities.get(code);
                }
                // fill it from avdth

                // labels
                String sovLabel = resultSet.getString(5);
                String startLabel = resultSet.getString(6);
                String label = String.format("[SOVETCO: %s] - [STARKIST: %s]", sovLabel, startLabel);
                entity.setLabel1(label + " TODO");
                entity.setLabel2(label);
                entity.setLabel3(label + " TODO");
                // status
                boolean statusCode = resultSet.getBoolean(7);
                entity.setStatus(statusCode ? ReferenceStatus.enabled : ReferenceStatus.disabled);
                write(sqlWriter, entity);
            }
        }
        return entity;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Connection connexion, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        int result = 0;
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(LandingWeightCategoryTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(CatchWeightCategoryTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        try (ReferentialQuery tableReader = AvdthQueries.referentialReader(WellPlanWeightCategoryTable.class, connexion)) {
            result += process(sqlWriter, tableReader, persistenceContext);
        }
        return result;
    }

    @Override
    public int process(SqlScriptWriter sqlWriter, Query query, ObserveTopiaPersistenceContext persistenceContext) throws SQLException {
        this.tableDescriptorClass = query.getDescriptor().getClass();
        return super.process(sqlWriter, query, persistenceContext);
    }

    @Override
    public void consume(AvdthReferentialImportResult result, List<WeightCategory> newList) {
        mergeNewReferential(result, newList, AvdthReferentialImportResult::getWeightCategory, WeightCategory::getCode);
    }

    private void write(SqlScriptWriter sqlWriter, WeightCategory entity) {
        String id = Strings.leftPad("fr.ird.observe.entities.referential.ps.common.WeightCategory#666#" + (count++), 5, '0');
        entity.setTopiaId(id);
        String timestamp = timestamp();
        String sql = String.format(
                "INSERT INTO ps_common.WeightCategory(" +
                        /*  1 */      "topiaId, " +
                        /*  2 */      "topiaVersion, " +
                        /*  3 */      "code, " +
                        /*  4 */      "status, " +
                        /*  5 */      "topiaCreateDate, " +
                        /*  6 */      "lastUpdateDate, " +
                        /*  7 */      "needComment, " +
                        /*  8 */      "species, " +
                        /*  9 */      "label1, " +
                        /* 10 */      "label2, " +
                        /* 11 */      "label3," +
                        /* 12 */      "minWeight," +
                        /* 13 */      "meanWeight," +
                        /* 14 */      "maxWeight," +
                        /* 15 */      "landing," +
                        /* 16 */      "logbook," +
                        /* 17 */      "wellPlan" +
                        ") VALUES " +
                        "(" +
                        /*  1 */ " '%1$s'," +
                        /*  2 */ " 0," +
                        /*  3 */ " '%2$s'," +
                        /*  4 */ " %3$s," +
                        /*  5 */ " '%4$s'::timestamp," +
                        /*  6 */ " '%5$s'::timestamp," +
                        /*  7 */ " %6$s," +
                        /*  8 */ " %7$s," +
                        /*  9 */ " '%8$s TODO'," +
                        /* 10 */ " '%8$s'," +
                        /* 11 */ " '%8$s TODO'," +
                        /* 12 */ " %9$s," +
                        /* 13 */ " %10$s," +
                        /* 14 */ " %11$s," +
                        /* 15 */ " %12$s," +
                        /* 16 */ " %13$s," +
                        /* 17 */ " %14$s" +
                        ");",
                /*  1 */  id,
                /*  2 */
                /*  3 */  entity.getCode(),
                /*  4 */  entity.getStatus(),
                /*  5 */  timestamp,
                /*  6 */  timestamp,
                /*  7 */  entity.isNeedComment(),
                /*  8 */  toId(entity.getSpecies()),
                /*  9, 10, 11 */  entity.getLabel1().replaceAll("'", "''''"),
                /* 12 */  DataWriter.roundFloat4(entity.getMinWeight()),
                /* 13 */  DataWriter.roundFloat4(entity.getMeanWeight()),
                /* 14 */  DataWriter.roundFloat4(entity.getMaxWeight()),
                /* 15 */  entity.isLanding(),
                /* 16 */  entity.isLogbook(),
                /* 17 */  entity.isWellPlan()
        );
        sqlWriter.writeSql(sql);
        createdEntities.put(entity.getCode(), entity);
    }
}
