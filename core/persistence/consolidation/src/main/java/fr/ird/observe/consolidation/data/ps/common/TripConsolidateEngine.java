package fr.ird.observe.consolidation.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeManager;
import fr.ird.observe.consolidation.data.ps.localmarket.GetOptionalRtpMeanWeightImpl;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.ps.localmarket.BatchDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.ObserveTopiaDaoSupplier;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.data.ps.localmarket.Batch;
import fr.ird.observe.entities.referential.common.LengthLengthParameterCache;
import fr.ird.observe.entities.referential.common.LengthWeightParameterCache;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.common.SpeciesList;
import fr.ird.observe.spi.consolidation.ToolkitIdModificationsToSql;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;

import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 17/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripConsolidateEngine {

    private static final Logger log = LogManager.getLogger(TripConsolidateEngine.class);
    private final ServiceContext context;
    private final fr.ird.observe.consolidation.data.ps.localmarket.BatchConsolidateEngine localmarketBatchEngine;
    private final fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateEngine observationActivityConsolidateEngine;
    private final fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateEngine logbookActivityConsolidateEngine;
    private final fr.ird.observe.consolidation.data.ps.logbook.SampleConsolidateEngine logbookSampleConsolidateEngine;

    public TripConsolidateEngine(ServiceContext context, SimplifiedObjectTypeManager simplifiedObjectTypeManager, TripConsolidateRequest request) {
        this.context = context;
        LengthWeightParameterCache lengthWeightParameterCache = new LengthWeightParameterCache(context.getApplicationLocale(), context.getDecoratorService(), (ObserveTopiaDaoSupplier) context.getTopiaPersistenceContext());
        LengthLengthParameterCache lengthLengthParameterCache = new LengthLengthParameterCache(context.getApplicationLocale(), context.getDecoratorService(), (ObserveTopiaDaoSupplier) context.getTopiaPersistenceContext());
        this.observationActivityConsolidateEngine = new fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateEngine(context,
                                                                                                                                   simplifiedObjectTypeManager,
                                                                                                                                   lengthWeightParameterCache,
                                                                                                                                   lengthLengthParameterCache);
        this.logbookActivityConsolidateEngine = new fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateEngine(context, simplifiedObjectTypeManager);
        String sampleActivitySpeciesListId = Objects.requireNonNull(request.getSpeciesListForLogbookSampleActivityWeightedWeight());
        String sampleSpeciesListId = Objects.requireNonNull(request.getSpeciesListForLogbookSampleWeights());
        Set<String> speciesListForLogbookSampleActivityWeightedWeight = SpeciesList.loadEntity(context, sampleActivitySpeciesListId).getSpecies().stream().map(Species::getId).collect(Collectors.toSet());
        Set<String> speciesListForLogbookSampleWeights = SpeciesList.loadEntity(context, sampleSpeciesListId).getSpecies().stream().map(Species::getId).collect(Collectors.toSet());
        this.logbookSampleConsolidateEngine = new fr.ird.observe.consolidation.data.ps.logbook.SampleConsolidateEngine(context.getDecoratorService(), speciesListForLogbookSampleWeights, speciesListForLogbookSampleActivityWeightedWeight);
        this.localmarketBatchEngine = new fr.ird.observe.consolidation.data.ps.localmarket.BatchConsolidateEngine(new GetOptionalRtpMeanWeightImpl(context, lengthWeightParameterCache), context.getDecoratorService());
    }

    public Optional<TripConsolidateResult> consolidate(TripConsolidateRequest request) {
        boolean failIfLengthWeightParameterNotFound = request.isFailIfLengthWeightParameterNotFound();
        boolean failIfLengthLengthParameterNotFound = request.isFailIfLengthLengthParameterNotFound();

        String tripId = request.getTripId();
        DecoratorService decoratorService = context.getDecoratorService();
        log.info("Start consolidate trip: {}", tripId);

        Trip trip = Trip.loadEntity(context, tripId);

        Set<fr.ird.observe.consolidation.data.ps.logbook.SampleConsolidateResult> logbookSampleResults = logbookSampleConsolidateEngine.consolidateTrip(trip);
        Set<ToolkitIdModifications> localmarketBatchResults = consolidateLocalmarketBatches(trip, failIfLengthWeightParameterNotFound);

        Set<fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult> observationActivityConsolidateResults = consolidateObservationActivities(trip, failIfLengthWeightParameterNotFound, failIfLengthLengthParameterNotFound);
        Set<fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateResult> logbookActivityConsolidateResults = consolidateLogbookActivities(trip);

        if (logbookSampleResults.isEmpty() && observationActivityConsolidateResults.isEmpty() && localmarketBatchResults.isEmpty() && logbookActivityConsolidateResults.isEmpty()) {
            // no modification, nor warning on this trip
            return Optional.empty();
        }
        decoratorService.installDecorator(Trip.class, trip);
        String tripLabel = trip.toString();
        TripConsolidateResult result = new TripConsolidateResult(tripId, tripLabel, observationActivityConsolidateResults, logbookActivityConsolidateResults, logbookSampleResults, localmarketBatchResults);
        boolean withModifications = result.withModifications();
        if (withModifications) {
            log.info("Found some modifications on trip: {} - {}", tripId, result.getTripLabel());
        }
        if (result.withWarnings()) {
            log.info("Found some warnings on trip: {} - {}", tripId, result.getTripLabel());
        }
        // clear hibernate session (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2843)
        context.getTopiaPersistenceContext().getHibernateSupport().clearSession();
        if (withModifications) {
            // generate sql script
            Path scriptPath = context.getTemporaryDirectoryRoot().resolve(String.format("TripConsolidateResult%d.sql", System.nanoTime()));
            log.info("Produce TripConsolidateResult sql script to: {}", scriptPath);
            SqlScript script = toSql(scriptPath, result, context.getTopiaApplicationContext().getModel());
            // consume sql script
            context.getTopiaPersistenceContext().executeSqlScript(script);
        }
        return Optional.of(result);
    }

    public SqlScript toSql(Path scriptPath, TripConsolidateResult result, TopiaEntitySqlModel entitySqlModel) {
        try (ToolkitIdModificationsToSql toolkitIdModificationsToSql = new ToolkitIdModificationsToSql(scriptPath, context.timestampNow(), entitySqlModel)) {
            observationActivityConsolidateEngine.toSql(result.getActivityObservationResults(), toolkitIdModificationsToSql);
            logbookActivityConsolidateEngine.toSql(result.getActivityLogbookResults(), toolkitIdModificationsToSql);
            logbookSampleConsolidateEngine.toSql(result.getLogbookSampleResults(), toolkitIdModificationsToSql);
            toolkitIdModificationsToSql.toSql(Batch.SPI, result.getLocalmarketBatchResults());
            return toolkitIdModificationsToSql.build();
        } catch (IOException e) {
            throw new IllegalStateException("Could not produce sql script to " + scriptPath, e);
        }
    }

    private Set<ToolkitIdModifications> consolidateLocalmarketBatches(Trip trip, boolean failIfLengthWeightParameterNotFound) {
        ReferentialLocale referenceLocale = context.getReferentialLocale();
        Set<ToolkitIdModifications> result = new LinkedHashSet<>();
        int batchIndex = 0;
        int batchMax = trip.getLocalmarketBatchSize();
        fr.ird.observe.consolidation.data.ps.localmarket.BatchConsolidateRequest batchRequest = new fr.ird.observe.consolidation.data.ps.localmarket.BatchConsolidateRequest();
        batchRequest.setFailIfLengthWeightParameterNotFound(failIfLengthWeightParameterNotFound);
        batchRequest.setOceanId(trip.getOcean().getId());
        batchRequest.setDate(trip.getEndDate());

        for (Batch batch : trip.getLocalmarketBatch()) {

            String batchPrefix = String.format("Batch [%s/%s] ", ++batchIndex, batchMax);
            log.debug("{} Start consolidate batch: {}", batchPrefix, batch.getTopiaId());

            BatchDto batchDto = Batch.toDto(referenceLocale, batch);
            batchRequest.setBatch(batchDto);

            Optional<ToolkitIdModifications> batchResult = localmarketBatchEngine.consolidate(batchRequest);
            batchResult.ifPresent(r -> {
                // add to result
                result.add(r);
                // flush back to entity
                r.flushToBean(batch);
            });
        }
        return result;
    }

    private Set<fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult> consolidateObservationActivities(Trip trip, boolean failIfLengthWeightParameterNotFound, boolean failIfLengthLengthParameterNotFound) {
        Set<fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult> result = new LinkedHashSet<>();
        int routeIndex = 0;
        int routeMax = trip.getRouteObsSize();
        Decorator routeDecorator = context.getDecoratorService().getDecoratorByType(fr.ird.observe.entities.data.ps.observation.Route.class);
        for (fr.ird.observe.entities.data.ps.observation.Route route : trip.getRouteObs()) {
            route.registerDecorator(routeDecorator);
            String routePrefix = String.format("Observations - Route %s [%s/%s] ", route, ++routeIndex, routeMax);
            log.debug("{} Start consolidate route: {}", routePrefix, route.getTopiaId());
            int activityIndex = 0;
            int activityMax = route.getActivitySize();
            for (fr.ird.observe.entities.data.ps.observation.Activity activity : route.getActivity()) {

                String activityPrefix = String.format("%s - Activity [%s/%s] ", routePrefix, ++activityIndex, activityMax);

                if (activity.getSetSize() == 0 && activity.isFloatingObjectEmpty()) {
                    log.debug("{} Skip activity (No set nor floating object found): {}", activityPrefix, activity.getTopiaId());
                    continue;
                }
                Optional<fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult> activityResult = observationActivityConsolidateEngine.consolidateActivity(trip,
                                                                                                                                                                               route,
                                                                                                                                                                               activity,
                                                                                                                                                                               activityPrefix,
                                                                                                                                                                               failIfLengthWeightParameterNotFound,
                                                                                                                                                                               failIfLengthLengthParameterNotFound);
                activityResult.ifPresent(result::add);
            }
        }
        return result;
    }

    private Set<fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateResult> consolidateLogbookActivities(Trip trip) {
        Set<fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateResult> result = new LinkedHashSet<>();
        int routeIndex = 0;
        int routeMax = trip.getRouteLogbookSize();
        Decorator routeDecorator = context.getDecoratorService().getDecoratorByType(fr.ird.observe.entities.data.ps.logbook.Route.class);
        for (fr.ird.observe.entities.data.ps.logbook.Route route : trip.getRouteLogbook()) {
            route.registerDecorator(routeDecorator);
            String routePrefix = String.format("Logbook - Route %s [%s/%s] ", route, ++routeIndex, routeMax);
            log.debug("{} Start consolidate route: {}", routePrefix, route.getTopiaId());
            int activityIndex = 0;
            int activityMax = route.getActivitySize();
            for (fr.ird.observe.entities.data.ps.logbook.Activity activity : route.getActivity()) {

                String activityPrefix = String.format("%s - Activity [%s/%s] ", routePrefix, ++activityIndex, activityMax);

                if (activity.isFloatingObjectEmpty()) {
                    log.debug("{} Skip activity (No floating object found): {}", activityPrefix, activity.getTopiaId());
                    continue;
                }
                Optional<fr.ird.observe.consolidation.data.ps.logbook.ActivityConsolidateResult> activityResult = logbookActivityConsolidateEngine.consolidateActivity(trip,
                                                                                                                                                                       route,
                                                                                                                                                                       activity,
                                                                                                                                                                       activityPrefix);
                activityResult.ifPresent(result::add);
            }
        }
        return result;
    }
}
