package fr.ird.observe.consolidation.data.ps.dcp;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ps.common.ObjectMaterial;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created on 18/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public class SimplifiedObjectTypeManagerFactory {

    public static SimplifiedObjectTypeManager create(ObserveTopiaPersistenceContext persistenceContext, SimplifiedObjectTypeSpecializedRules simplifiedObjectTypeSpecializedRules) {
        List<ObjectMaterial> materials = persistenceContext.getPsCommonObjectMaterialDao().findAll();
        Map<String, SimplifiedObjectTypeNode> mappingBuilder = createMappingBuilder(materials);
        return new SimplifiedObjectTypeManager(mappingBuilder, simplifiedObjectTypeSpecializedRules);
    }

    private static Map<String, SimplifiedObjectTypeNode> createMappingBuilder(List<ObjectMaterial> materials) {
        TreeMap<String, SimplifiedObjectTypeNode> result = new TreeMap<>();
        java.util.Set<ObjectMaterial> materialsDone = new LinkedHashSet<>();
        java.util.Set<ObjectMaterial> materialsTodo = new LinkedHashSet<>(materials);
        while (!materialsTodo.isEmpty()) {
            java.util.Set<ObjectMaterial> currentRoundMaterials;
            if (materialsDone.isEmpty()) {
                // first round
                currentRoundMaterials = materialsTodo.stream().filter(f -> f.getParent() == null).collect(Collectors.toSet());
            } else {
                currentRoundMaterials = materialsTodo.stream().filter(f -> materialsDone.contains(f.getParent())).collect(Collectors.toSet());
            }
            materialsDone.addAll(currentRoundMaterials);
            materialsTodo.removeAll(currentRoundMaterials);
            for (ObjectMaterial currentRoundMaterial : currentRoundMaterials) {
                ObjectMaterial simplifiedObjectMaterial = currentRoundMaterial.getSimplifiedObjectMaterial();
                SimplifiedObjectTypeNode node;
                if (Objects.equals(currentRoundMaterial, simplifiedObjectMaterial)) {
                    // new node
                    ObjectMaterial simplifiedParentObjectMaterial = currentRoundMaterial.getSimplifiedParentObjectMaterial();
                    SimplifiedObjectTypeNode parentNode = simplifiedParentObjectMaterial == null ? null : result.get(simplifiedParentObjectMaterial.getTopiaId());
                    node = new SimplifiedObjectTypeNode(currentRoundMaterial.getTopiaId(), currentRoundMaterial.getStandardCode(), parentNode);
                } else {
                    if (simplifiedObjectMaterial == null) {
                        simplifiedObjectMaterial = currentRoundMaterial;
                    }
                    // simplified object material is a parent, so must have been process in previous round
                    Objects.requireNonNull(simplifiedObjectMaterial);
                    node = result.get(simplifiedObjectMaterial.getTopiaId());
                    if (node == null) {
                        node = new SimplifiedObjectTypeNode(currentRoundMaterial.getTopiaId(), currentRoundMaterial.getStandardCode(), null);
                    }
                    Objects.requireNonNull(node);
                }
                result.put(currentRoundMaterial.getTopiaId(), node);
            }
        }
        return result;
    }
}
