package fr.ird.observe.consolidation.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.referential.common.LengthWeightParameterNotFoundException;
import fr.ird.observe.entities.referential.common.LengthWeightParameter;
import fr.ird.observe.entities.referential.common.LengthWeightParameterCache;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.Sex;
import fr.ird.observe.entities.referential.common.SizeMeasureType;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.i18n.I18n;

import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import static io.ultreia.java4all.i18n.I18n.l;

/**
 * Created on 22/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class GetOptionalRtpMeanWeightImpl implements GetOptionalRtpMeanWeight {
    private final ImmutableMap<String, Species> speciesById;
    private final ImmutableMap<String, Sex> sexById;
    private final ImmutableMap<String, SizeMeasureType> sizeMeasureTypeById;
    private final ImmutableMap<String, Ocean> oceanById;
    private final Locale locale;
    private final LengthWeightParameterCache lengthWeightParameterCache;
    private final DecoratorService decoratorService;

    public GetOptionalRtpMeanWeightImpl(ServiceContext context, LengthWeightParameterCache lengthWeightParameterCache) {
        this.decoratorService = context.getDecoratorService();
        speciesById = Maps.uniqueIndex(Species.loadEntities(context), Species::getTopiaId);
        sexById = Maps.uniqueIndex(Sex.loadEntities(context), Sex::getTopiaId);
        sizeMeasureTypeById = Maps.uniqueIndex(SizeMeasureType.loadEntities(context), SizeMeasureType::getTopiaId);
        oceanById = Maps.uniqueIndex(Ocean.loadEntities(context), Ocean::getTopiaId);
        this.locale = context.getApplicationLocale();
        this.lengthWeightParameterCache = lengthWeightParameterCache;
    }

    public static void addWarning(DecoratorService decoratorService, Locale locale, String i18nMessage, Species species, Sex sex, Ocean ocean, Date date, Consumer<String> fallbackIfNotFound) {
        decoratorService.installDecorator(Species.class, species);
        decoratorService.installDecorator(Ocean.class, ocean);
        if (sex != null) {
            decoratorService.installDecorator(Sex.class, sex);
        }
        String speciesLabel = species.toString();
        String oceanLabel = ocean.toString();
        String sexLabel = sex == null ? l(locale, "observe.service.actions.consolidate.noSex") : sex.toString();
        String message = l(locale, i18nMessage, speciesLabel, oceanLabel, sexLabel, date);
        fallbackIfNotFound.accept(message);
    }

    @Override
    public Optional<Float> get(String speciesId, String sexId, String oceanId, String sizeMeasureTypeId, Date date, boolean failIfLengthWeightParameterNotFound, Consumer<String> fallbackIfNotFound) {
        Species species = Objects.requireNonNull(speciesById.get(speciesId));
        Sex sex = sexById.get(sexId);
        Ocean ocean = Objects.requireNonNull(oceanById.get(oceanId));
        SizeMeasureType sizeMeasureType = sizeMeasureTypeById.get(sizeMeasureTypeId);
        try {
            return lengthWeightParameterCache.find(ocean, date, species, sex, sizeMeasureType).map(LengthWeightParameter::getMeanWeight);

        } catch (LengthWeightParameterNotFoundException e) {
            if (failIfLengthWeightParameterNotFound) {
                throw e;
            }
            addWarning(decoratorService, locale, I18n.n("observe.service.actions.consolidate.lengthWeightParameterNotFound"), species, sex, ocean, date, fallbackIfNotFound);
            return Optional.empty();
        }
    }
}
