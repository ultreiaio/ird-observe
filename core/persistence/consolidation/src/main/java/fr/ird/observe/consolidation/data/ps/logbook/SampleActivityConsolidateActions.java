package fr.ird.observe.consolidation.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.data.ps.logbook.SampleActivity;

import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 14/04/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public enum SampleActivityConsolidateActions implements AtomicConsolidateAction<SampleActivityConsolidateContext, SampleActivity> {

    /**
     * Compute {@link SampleActivity#getWeightedWeight()}.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2053">issue 2053</a>
     */
    ComputeWeightedWeight() {
        @Override
        public List<String> fieldNames() {
            return List.of(SampleActivity.PROPERTY_WEIGHTED_WEIGHT, SampleActivity.PROPERTY_WEIGHTED_WEIGHT_COMPUTED);
        }

        @Override
        public boolean test(SampleActivityConsolidateContext context, SampleActivity datum) {
            return datum.isWeightedWeightComputed() || datum.getWeightedWeight() == null;
        }

        @Override
        public void accept(SampleActivityConsolidateContext context, SampleActivity datum) {
            String wellId = context.getSampleWell();
            Set<String> speciesList = context.getAcceptedSpeciesIds();
            Activity activity = datum.getActivity();

            // Get total weight of the set which is this well
            double w1 = context.sumOnTripWell(w -> w.getTotalWeight(activity, speciesList, Set.of(wellId)));
            if (w1 == 0) {
                // limit case if the well was not found in trip well plan
                log.warn("Well {} not found for activity {} in trip well plan.", wellId, activity.getTopiaId());
                datum.setWeightedWeight(null);
                datum.setWeightedWeightComputed(false);
                return;
            }
            // Get total weight of the set which are in well that has been sampled
            double w2 = context.sumOnTripWell(w -> w.getTotalWeight(activity, speciesList, context.getSampledWellIds()));
            // Get total weight of the set in any well
            double wT = context.sumOnTripWell(w -> w.getTotalWeight(activity, speciesList));
            // the weighted weight is the formula: weightedWeight = (w1/w2)*wT
            float weightedWeight = (float) ((w1 / w2) * wT);
            Float oldWeightedWeight = datum.getWeightedWeight();
            datum.setWeightedWeight(weightedWeight);
            if (!Objects.equals(oldWeightedWeight, datum.getWeightedWeight())) {
                log.info("Well {} , W1: {}, W2: {}, WT: {} → WW: {} (old value: {})", wellId, w1, w2, wT, datum.getWeightedWeight(), oldWeightedWeight);
            }
            datum.setWeightedWeightComputed(true);
        }
    };

    @Override
    public String toString() {
        return "ps.logbook." + SampleActivityConsolidateActions.class.getSimpleName() + "." + name();
    }
}
