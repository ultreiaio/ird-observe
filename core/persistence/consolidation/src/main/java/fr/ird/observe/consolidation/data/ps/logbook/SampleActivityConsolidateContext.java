package fr.ird.observe.consolidation.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.ConsolidateContext;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.data.ps.logbook.Sample;
import fr.ird.observe.entities.data.ps.logbook.SampleActivity;
import fr.ird.observe.entities.data.ps.logbook.Well;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

/**
 * Created on 14/04/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
class SampleActivityConsolidateContext implements ConsolidateContext<SampleActivity> {
    private final JavaBeanMonitor monitor;
    private final Decorator decorator;
    private final Set<Well> tripWell;
    private final Set<String> acceptedSpeciesIds;
    private final java.util.Set<String> warnings = new LinkedHashSet<>();
    private final Set<String> sampledWellIds;
    private Sample sample;
    private SampleActivity datum;

    SampleActivityConsolidateContext(JavaBeanMonitor monitor, Decorator decorator, Trip trip, Set<String> acceptedSpeciesIds) {
        this.monitor = monitor;
        this.decorator = decorator;
        this.tripWell = trip.getWell();
        this.sampledWellIds = trip.getSample().stream().map(Sample::getWell).collect(Collectors.toSet());
        this.acceptedSpeciesIds = acceptedSpeciesIds;
    }

    public Set<String> getSampledWellIds() {
        return sampledWellIds;
    }

    public Set<String> getAcceptedSpeciesIds() {
        return acceptedSpeciesIds;
    }

    public void watch(Sample sample, SampleActivity datum) {
        this.sample = Objects.requireNonNull(sample);
        this.datum = Objects.requireNonNull(datum);
        monitor.setBean(datum);
    }

    public void flush(SampleConsolidateResultBuilder resultBuilder) {
        if (monitor.wasModified()) {
            datum.registerDecorator(decorator);
            monitor.toModifications(modifications -> new ToolkitIdModifications(datum, modifications, warnings.isEmpty() ? null : warnings)).ifPresent( m-> {
                resultBuilder.flushSampleActivityModification(m);
                m.reset(datum);
            });
        } else if (!warnings.isEmpty()) {
            datum.registerDecorator(decorator);
            resultBuilder.flushSampleActivityModification(new ToolkitIdModifications(datum, java.util.Set.of(), java.util.Set.copyOf(warnings)));
        }
        monitor.setBean(null);
        this.datum = null;
        this.sample = null;
        warnings.clear();
    }

    public double sumOnTripWell(ToDoubleFunction<Well> function) {
        return tripWell.stream().mapToDouble(function).sum();
    }

    public String getSampleWell() {
        return sample.getWell();
    }

    @Override
    public JavaBeanMonitor monitor() {
        return monitor;
    }

    @Override
    public Class<SampleActivity> dataType() {
        return SampleActivity.class;
    }
}
