package fr.ird.observe.consolidation.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.entities.data.ps.logbook.Sample;
import fr.ird.observe.entities.data.ps.logbook.Well;
import fr.ird.observe.entities.data.ps.logbook.WellActivity;
import fr.ird.observe.entities.data.ps.logbook.WellActivitySpecies;
import fr.ird.observe.entities.referential.ps.common.WeightCategory;
import io.ultreia.java4all.lang.Numbers;

import java.util.List;

/**
 * Created at 12/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
public enum SampleConsolidateActions implements AtomicConsolidateAction<SampleConsolidateContext, Sample> {

    /**
     * Compute {@code Sample#weights}.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2669">issue 2669</a>
     */
    ComputeWeights() {
        @Override
        public List<String> fieldNames() {
            return List.of(Sample.PROPERTY_BIGS_WEIGHT, Sample.PROPERTY_SMALLS_WEIGHT, Sample.PROPERTY_TOTAL_WEIGHT, Sample.PROPERTY_WEIGHTS_COMPUTED);
        }

        @Override
        public boolean test(SampleConsolidateContext context, Sample datum) {
            return datum.isWeightsComputed() || (datum.getBigsWeight() == null && datum.getSmallsWeight() == null && datum.getTotalWeight() == null);
        }

        @Override
        public void accept(SampleConsolidateContext context, Sample datum) {
            datum.setSmallsWeight(null);
            datum.setBigsWeight(null);
            datum.setTotalWeight(null);
            datum.setWeightsComputed(false);
            String wellId = datum.getWell();
            Well well = context.getWell(wellId);
            if (well == null) {
                return;
            }
            double bigsWeight = 0d;
            double smallsWeight = 0d;
            double totalWeight = 0d;
            int bigsHit = 0;
            int smallsHit = 0;
            int totalHit = 0;
            for (WellActivity wellActivity : well.getWellActivity()) {
                for (WellActivitySpecies wellActivitySpecies : wellActivity.getWellActivitySpecies()) {
                    if (context.acceptSpecies(wellActivitySpecies)) {
                        Float weight = wellActivitySpecies.getWeight();
                        if (totalHit > 0) {
                            // once this flag is on, always add to totalWeight
                            totalWeight += weight;
                            totalHit++;
                            continue;
                        }
                        WeightCategory weightCategory = wellActivitySpecies.getWeightCategory();
                        if (ProtectedIdsPs.PS_LOGBOOK_WEIGHT_CATEGORY_SMALLS_WEIGHT_ID.equals(weightCategory.getId())) {
                            smallsWeight += weight;
                            smallsHit++;
                        } else if (ProtectedIdsPs.PS_LOGBOOK_WEIGHT_CATEGORY_BIGS_WEIGHT_ID.equals(weightCategory.getId())) {
                            bigsWeight += weight;
                            bigsHit++;
                        } else {
                            totalWeight += weight;
                            totalHit++;
                        }
                    }
                }
            }
            if (totalHit > 0) {
                datum.setTotalWeight(Numbers.roundFourDigits((float) (bigsWeight + smallsWeight + totalWeight)));
                datum.setWeightsComputed(true);
                return;
            }
            if (smallsHit > 0 || bigsHit > 0) {
                datum.setSmallsWeight(Numbers.roundFourDigits((float) (smallsWeight)));
                datum.setBigsWeight(Numbers.roundFourDigits((float) (bigsWeight)));
                datum.setWeightsComputed(true);
            }
        }
    };

    @Override
    public String toString() {
        return "ps.logbook." + SampleConsolidateActions.class.getSimpleName() + "." + name();
    }
}
