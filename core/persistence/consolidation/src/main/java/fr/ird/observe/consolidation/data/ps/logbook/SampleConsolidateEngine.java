package fr.ird.observe.consolidation.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.data.ps.logbook.Sample;
import fr.ird.observe.entities.data.ps.logbook.SampleActivity;
import fr.ird.observe.spi.consolidation.ToolkitIdModificationsToSql;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 14/04/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public class SampleConsolidateEngine {

    private static final Logger log = LogManager.getLogger(SampleConsolidateEngine.class);
    private final JavaBeanMonitor sampleMonitor;
    private final JavaBeanMonitor sampleActivityMonitor;
    private final Decorator sampleDecorator;
    private final Decorator sampleActivityDecorator;
    private final Set<String> speciesListForLogbookSampleWeights;
    private final Set<String> speciesListForLogbookSampleActivityWeightedWeight;

    public SampleConsolidateEngine(DecoratorService decoratorService, Set<String> speciesListForLogbookSampleWeights, Set<String> speciesListForLogbookSampleActivityWeightedWeight) {
        this.speciesListForLogbookSampleWeights = speciesListForLogbookSampleWeights;
        this.speciesListForLogbookSampleActivityWeightedWeight = speciesListForLogbookSampleActivityWeightedWeight;
        this.sampleMonitor = new JavaBeanMonitor(Sample.PROPERTY_BIGS_WEIGHT, Sample.PROPERTY_SMALLS_WEIGHT, Sample.PROPERTY_TOTAL_WEIGHT, Sample.PROPERTY_WEIGHTS_COMPUTED);
        this.sampleActivityMonitor = new JavaBeanMonitor(SampleActivity.PROPERTY_WEIGHTED_WEIGHT, SampleActivity.PROPERTY_WEIGHTED_WEIGHT_COMPUTED);
        this.sampleDecorator = decoratorService.getDecoratorByType(Sample.class);
        this.sampleActivityDecorator = decoratorService.getDecoratorByType(SampleActivity.class);
    }

    public void toSql(java.util.Set<SampleConsolidateResult> samplesResults, ToolkitIdModificationsToSql toolkitIdModificationsToSql) {
        for (SampleConsolidateResult sampleConsolidateResult : samplesResults) {
            toolkitIdModificationsToSql.toSql(Sample.SPI, sampleConsolidateResult.getModifications());
            toolkitIdModificationsToSql.toSql(SampleActivity.SPI, sampleConsolidateResult.getSampleActivityModifications());
        }
    }

    public Set<SampleConsolidateResult> consolidateTrip(Trip trip) {
        Set<SampleConsolidateResult> result = new LinkedHashSet<>();
        if (trip.isWellEmpty() || trip.isSampleEmpty()) {
            return result;
        }
        SampleConsolidateContext sampleConsolidateContext = new SampleConsolidateContext(sampleMonitor,
                                                                                         sampleDecorator,
                                                                                         trip,
                                                                                         speciesListForLogbookSampleWeights);
        SampleActivityConsolidateContext sampleActivityConsolidateContext = new SampleActivityConsolidateContext(sampleActivityMonitor,
                                                                                                                 sampleActivityDecorator,
                                                                                                                 trip,
                                                                                                                 speciesListForLogbookSampleActivityWeightedWeight);
        int sampleIndex = 0;
        int sampleMax = trip.getSampleSize();
        for (fr.ird.observe.entities.data.ps.logbook.Sample sample : trip.getSample()) {
            sample.registerDecorator(sampleDecorator);
            String logPrefix = String.format("Logbook - Sample %s [%s/%s] ", sample, ++sampleIndex, sampleMax);
            log.debug("{} Start consolidate sample: {}", logPrefix, sample.getTopiaId());
            Optional<SampleConsolidateResult> sampleResult = consolidateSample(
                    sample,
                    sampleConsolidateContext,
                    sampleActivityConsolidateContext);
            sampleResult.ifPresent(result::add);
        }
        return result;
    }

    Optional<SampleConsolidateResult> consolidateSample(Sample sample,
                                                        SampleConsolidateContext sampleConsolidateContext,
                                                        SampleActivityConsolidateContext sampleActivityConsolidateContext) {
        SampleConsolidateResultBuilder resultBuilder = new SampleConsolidateResultBuilder(sample.getId(), sample.toString());

        try {
            sampleConsolidateContext.watch(sample);
            SampleConsolidateActions.ComputeWeights.execute(sampleConsolidateContext, sample);
        } finally {
            sampleConsolidateContext.flush(resultBuilder);
        }
        if (sample.isSampleActivityNotEmpty()) {
            for (SampleActivity sampleActivity : sample.getSampleActivity()) {
                try {
                    sampleActivityConsolidateContext.watch(sample, sampleActivity);
                    SampleActivityConsolidateActions.ComputeWeightedWeight.execute(sampleActivityConsolidateContext, sampleActivity);
                } finally {
                    sampleActivityConsolidateContext.flush(resultBuilder);
                }
            }
        }
        return resultBuilder.build();
    }
}
