package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.data.ps.localmarket.GetOptionalRtpMeanWeightImpl;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.referential.common.LengthLengthParameterNotFoundException;
import fr.ird.observe.dto.referential.common.LengthWeightParameterNotFoundException;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.data.ps.observation.Activity;
import fr.ird.observe.entities.data.ps.observation.Route;
import fr.ird.observe.entities.referential.common.LengthLengthParameter;
import fr.ird.observe.entities.referential.common.LengthLengthParameterCache;
import fr.ird.observe.entities.referential.common.LengthWeightParameter;
import fr.ird.observe.entities.referential.common.LengthWeightParameterCache;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.Sex;
import fr.ird.observe.entities.referential.common.SizeMeasureType;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.i18n.I18n;

import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created by tchemit on 02/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ActivityConsolidateContext {

    private final Locale locale;
    private final LengthWeightParameterCache lengthWeightParameterCache;
    private final LengthLengthParameterCache lengthLengthParameterCache;
    private final Trip trip;
    private final Route route;
    private final Activity activity;
    private final boolean failIfLengthWeightParameterNotFound;
    private final boolean failIfLengthLengthParameterNotFound;
    private final DecoratorService decoratorService;
    private final String activityPrefix;
    private final ActivityConsolidateResultBuilder resultBuilder;

    public ActivityConsolidateContext(ServiceContext context,
                                      LengthWeightParameterCache lengthWeightParameterCache,
                                      LengthLengthParameterCache lengthLengthParameterCache,
                                      Trip trip,
                                      Route route,
                                      Activity activity,
                                      String activityPrefix,
                                      boolean failIfLengthWeightParameterNotFound,
                                      boolean failIfLengthLengthParameterNotFound) {
        this.lengthWeightParameterCache = lengthWeightParameterCache;
        this.lengthLengthParameterCache = lengthLengthParameterCache;
        this.trip = trip;
        this.route = route;
        this.activity = activity;
        this.activityPrefix = activityPrefix;
        this.failIfLengthWeightParameterNotFound = failIfLengthWeightParameterNotFound;
        this.failIfLengthLengthParameterNotFound = failIfLengthLengthParameterNotFound;
        this.decoratorService = context.getDecoratorService();
        this.locale = context.getApplicationLocale();
        this.resultBuilder = new ActivityConsolidateResultBuilder(activity.getTopiaId(), activity.toString());
    }

    public Ocean getOcean() {
        return trip.getOcean();
    }

    public Date getRouteDate() {
        return route.getDate();
    }

    public Activity getActivity() {
        return activity;
    }

    public fr.ird.observe.entities.data.ps.observation.Set getSet() {
        return activity.getSet();
    }

    public String getActivityPrefix() {
        return activityPrefix;
    }

    public Optional<LengthWeightParameter> findLengthWeightParameter(Species species, Sex sex, SizeMeasureType sizeMeasureType, Consumer<String> warningFallback) {
        Ocean ocean = getOcean();
        Date routeDate = getRouteDate();
        try {
            return lengthWeightParameterCache.find(ocean, routeDate, species, sex, sizeMeasureType);
        } catch (LengthWeightParameterNotFoundException e) {
            if (failIfLengthWeightParameterNotFound) {
                throw e;
            }
            GetOptionalRtpMeanWeightImpl.addWarning(decoratorService, locale, I18n.n("observe.service.actions.consolidate.lengthWeightParameterNotFound"), species, sex, ocean, routeDate, warningFallback);
            return Optional.empty();
        }
    }

    public Optional<LengthLengthParameter> findLengthLengthParameter(Species species, Sex sex, SizeMeasureType inputSizeMeasureType, SizeMeasureType outputSizeMeasureType, Consumer<String> warningFallback) {
        Ocean ocean = getOcean();
        Date routeDate = getRouteDate();
        try {
            return lengthLengthParameterCache.find(ocean, routeDate, species, sex, inputSizeMeasureType, outputSizeMeasureType);
        } catch (LengthLengthParameterNotFoundException e) {
            if (failIfLengthLengthParameterNotFound) {
                throw e;
            }
            GetOptionalRtpMeanWeightImpl.addWarning(decoratorService, locale, I18n.n("observe.service.actions.consolidate.LengthLengthParameterNotFound"), species, sex, ocean, routeDate, warningFallback);
            return Optional.empty();
        }
    }

    public void flushSet(ToolkitIdModifications modifications) {
        resultBuilder.flushSetModification(modifications);
    }

    public void flushCatch(ToolkitIdModifications modifications) {
        resultBuilder.flushCatchModification(modifications);
    }

    public void flushSampleMeasure(ToolkitIdModifications modifications) {
        resultBuilder.flushSampleMeasureModification(modifications);
    }

    public void flushFloatingObject(ToolkitIdModifications modifications) {
        resultBuilder.flushFloatingObjectModification(modifications);
    }

    public Optional<ActivityConsolidateResult> buildResult() {
        return resultBuilder.build();
    }

}
