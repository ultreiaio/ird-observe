package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeManager;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.ActivityAware;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectPartDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.data.ps.observation.Activity;
import fr.ird.observe.entities.data.ps.observation.Catch;
import fr.ird.observe.entities.data.ps.observation.FloatingObject;
import fr.ird.observe.entities.data.ps.observation.FloatingObjectPart;
import fr.ird.observe.entities.data.ps.observation.Route;
import fr.ird.observe.entities.data.ps.observation.SampleMeasure;
import fr.ird.observe.entities.data.ps.observation.Set;
import fr.ird.observe.entities.referential.common.LengthLengthParameterCache;
import fr.ird.observe.entities.referential.common.LengthWeightParameterCache;
import fr.ird.observe.spi.consolidation.ToolkitIdModificationsToSql;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.util.Dates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashSet;
import java.util.Optional;

/**
 * To consolidate observation activities.
 * <p>
 * Created on 22/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class ActivityConsolidateEngine {
    private static final Logger log = LogManager.getLogger(ActivityConsolidateEngine.class);
    private final ServiceContext context;
    private final LengthWeightParameterCache lengthWeightParameterCache;
    private final LengthLengthParameterCache lengthLengthParameterCache;
    private final FloatingObjectConsolidateEngine floatingObjectConsolidateEngine;
    private final SetConsolidateEngine setConsolidateEngine;
    private final Decorator activityDecorator;

    public ActivityConsolidateEngine(ServiceContext context, SimplifiedObjectTypeManager simplifiedObjectTypeManager, LengthWeightParameterCache lengthWeightParameterCache, LengthLengthParameterCache lengthLengthParameterCache) {
        this.context = context;
        this.lengthWeightParameterCache = lengthWeightParameterCache;
        this.lengthLengthParameterCache = lengthLengthParameterCache;
        this.floatingObjectConsolidateEngine = new FloatingObjectConsolidateEngine(simplifiedObjectTypeManager, context.getDecoratorService());
        this.setConsolidateEngine = new SetConsolidateEngine(context);
        this.activityDecorator = context.getDecoratorService().getDecoratorByType(Activity.class, ActivityAware.CLASSIFIER_WITH_ROUTE);
    }

    public void toSql(java.util.Set<ActivityConsolidateResult> activityObservationResults, ToolkitIdModificationsToSql toolkitIdModificationsToSql) {
        for (ActivityConsolidateResult activityConsolidateResult : activityObservationResults) {
            toolkitIdModificationsToSql.toSql(FloatingObject.SPI, activityConsolidateResult.getFloatingObjectModifications());
            toolkitIdModificationsToSql.toSql(Set.SPI, activityConsolidateResult.getSetModifications());
            toolkitIdModificationsToSql.toSql(Catch.SPI, activityConsolidateResult.getCatchModifications());
            toolkitIdModificationsToSql.toSql(SampleMeasure.SPI, activityConsolidateResult.getSampleMeasureModifications());
        }
    }

    public Optional<ActivityConsolidateResult> consolidateActivity(Trip trip,
                                                                   Route route,
                                                                   Activity activity,
                                                                   String activityPrefix,
                                                                   boolean failIfLengthWeightParameterNotFound,
                                                                   boolean failIfLengthLengthParameterNotFound) {
        if (activity.getSetSize() == 0 && activity.isFloatingObjectEmpty()) {
            log.debug(String.format("%s Skip activity (No set nor floating object found): %s", activityPrefix, activity.getTopiaId()));
            return Optional.empty();
        }
        activity.setTime(Dates.getDateAndTime(route.getDate(), activity.getTime(), true, true));
        activity.registerDecorator(activityDecorator);

        ActivityConsolidateContext activityContext = new ActivityConsolidateContext(context,
                                                                                    lengthWeightParameterCache,
                                                                                    lengthLengthParameterCache,
                                                                                    trip,
                                                                                    route,
                                                                                    activity,
                                                                                    activityPrefix,
                                                                                    failIfLengthWeightParameterNotFound,
                                                                                    failIfLengthLengthParameterNotFound);
        try {
            log.debug(String.format("%s Start consolidate activity: %s", activityPrefix, activity.getTopiaId()));
            if (activity.isFloatingObjectNotEmpty()) {
                for (FloatingObject floatingObject : activity.getFloatingObject()) {
                    consolidateFloatingObject(activityContext, floatingObject);
                }
            }
            setConsolidateEngine.consolidate(activityContext, activity);
            Optional<ActivityConsolidateResult> result = activityContext.buildResult();
            result.ifPresent(r -> {
                if (r.withModifications()) {
                    log.info(String.format("%s Found some modifications on activity: %s - %s", activityPrefix, activity.getTopiaId(), r.getActivityLabel()));
                }
                if (r.withWarnings()) {
                    log.info(String.format("%s Found some warnings on activity: %s - %s", activityPrefix, activity.getTopiaId(), r.getActivityLabel()));
                }
                if (r.getCatchModifications() != null) {
                    // Replace in modifications any entity by his id (only necessary for catches modifications)
                    r.getCatchModifications().forEach(ToolkitIdModifications::replaceToolkitIdReferencesById);
                }
            });
            return result;
        } catch (Exception e) {
            log.error(String.format("%s Could not consolidate trip: %s, route: %s - activity: %s", activityPrefix, trip.getTopiaId(), route.getTopiaId(), activity.getTopiaId()), e);
            throw e;
        }
    }

    private void consolidateFloatingObject(ActivityConsolidateContext activityContext, FloatingObject floatingObject) {

        if (floatingObject.getObjectOperation() == null) {
            log.warn("**********************");
            log.warn(String.format("Floating object %s has no object operation....", floatingObject.getTopiaId()));
            log.warn("**********************");
            return;
        }

        ReferentialLocale referentialLocale = context.getReferentialLocale();

        FloatingObjectDto floatingObjectDto = FloatingObject.toDto(referentialLocale, floatingObject);

        java.util.Set<FloatingObjectPartDto> floatingObjectPartSet = new LinkedHashSet<>(FloatingObjectPart.SPI.toDataDtoList(referentialLocale, floatingObject.getFloatingObjectPart()));
        FloatingObjectConsolidateRequest request = new FloatingObjectConsolidateRequest(floatingObjectDto, floatingObjectPartSet);
        Optional<ToolkitIdModifications> result = floatingObjectConsolidateEngine.consolidate(request);

        result.ifPresent(r -> {
            // flush to main result
            activityContext.flushFloatingObject(r);
            // flush back modifications detected to entity
            r.flushToBean(floatingObject);
        });

        if (request.isNeedClean()) {
            log.warn("**********************");
            log.warn(String.format("Floating object %s has some bad parts, this will be fixed here....", floatingObject.getTopiaId()));
            log.warn("**********************");
            // Need to clean dcp parts
            boolean notWhenArriving = !floatingObject.getObjectOperation().isWhenArriving();
            boolean notWhenLeaving = !floatingObject.getObjectOperation().isWhenLeaving();
            for (FloatingObjectPart floatingObjectPart : floatingObject.getFloatingObjectPart()) {
                if (floatingObjectPart.getWhenArriving() != null && notWhenArriving) {
                    log.info(String.format("Set whenArriving to null for part: %s", floatingObjectPart.getTopiaId()));
                    floatingObjectPart.setWhenArriving(null);
                }
                if (floatingObjectPart.getWhenLeaving() != null && notWhenLeaving) {
                    log.info(String.format("Set whenLeaving to null for part: %s", floatingObjectPart.getTopiaId()));
                    floatingObjectPart.setWhenLeaving(null);
                }
            }
        }
    }
}
