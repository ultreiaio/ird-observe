package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.dto.data.ps.CatchComputedValueSource;
import fr.ird.observe.entities.data.ps.observation.Catch;
import fr.ird.observe.entities.data.ps.observation.SampleMeasure;
import fr.ird.observe.entities.referential.common.LengthWeightParameter;

import java.util.List;

/**
 * All consolidation actions possible for a catch.
 * <p>
 * Created on 23/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public enum CatchConsolidateActions implements AtomicConsolidateAction<CatchConsolidateContext, Catch> {

    /**
     * Compute {@link Catch#getMeanLength()} using {@link Catch#getMeanWeight()} and the given RTP.
     */
    ComputeMeanLengthFromMeanWeight() {
        @Override
        public List<String> fieldNames() {
            return List.of(Catch.PROPERTY_MEAN_LENGTH, Catch.PROPERTY_MEAN_LENGTH_COMPUTED_SOURCE);
        }

        @Override
        public boolean test(CatchConsolidateContext context, Catch aCatch) {
            return context.withLengthWeightParameter()
                    && aCatch.withoutMeanLength()
                    && aCatch.withMeanWeight();
        }

        @Override
        public void accept(CatchConsolidateContext context, Catch aCatch) {
            LengthWeightParameter lengthWeightParameter = context.getLengthWeightParameter();
            float meanWeight = aCatch.getMeanWeight();
            Float meanLength = lengthWeightParameter.computeFromFormulaTwo(meanWeight);
            if (meanLength != null) {
                aCatch.setMeanLength(meanLength);
                aCatch.setMeanLengthComputedSource(CatchComputedValueSource.fromData);
            }
        }
    },
    /**
     * Compute {@link Catch#getMeanWeight()} using {@link Catch#getMeanLength()} and the given RTP.
     */
    ComputeMeanWeightFromMeanLength() {
        @Override
        public List<String> fieldNames() {
            return List.of(Catch.PROPERTY_MEAN_WEIGHT, Catch.PROPERTY_MEAN_WEIGHT_COMPUTED_SOURCE);
        }

        @Override
        public boolean test(CatchConsolidateContext context, Catch aCatch) {
            return context.withLengthWeightParameter()
                    && aCatch.withoutMeanWeight()
                    && aCatch.withMeanLength();
        }

        @Override
        public void accept(CatchConsolidateContext context, Catch aCatch) {
            LengthWeightParameter lengthWeightParameter = context.getLengthWeightParameter();
            float meanLength = aCatch.getMeanLength();
            Float meanWeight = lengthWeightParameter.computeFromFormulaOne(meanLength);
            if (meanWeight != null) {
                aCatch.setMeanWeight(meanWeight);
                aCatch.setMeanWeightComputedSource(CatchComputedValueSource.fromData);
            }
        }
    },
    /**
     * Compute {@link Catch#getCatchWeight()} using {@link Catch#getTotalCount()} and {@link Catch#getMeanWeight()}.
     * <p>
     * calcul le weight poids à partir de nb estime et du poids moyen
     */
    ComputeCatchWeightFromTotalCountAndMeanWeight() {
        @Override
        public List<String> fieldNames() {
            return List.of(Catch.PROPERTY_CATCH_WEIGHT, Catch.PROPERTY_CATCH_WEIGHT_COMPUTED_SOURCE);
        }

        @Override
        public boolean test(CatchConsolidateContext context, Catch aCatch) {
            return aCatch.withoutCatchWeight()
                    && aCatch.withMeanWeight()
                    && aCatch.withTotalCount();
        }

        @Override
        public void accept(CatchConsolidateContext context, Catch aCatch) {
            float meanWeight = aCatch.getMeanWeight();
            int totalCount = aCatch.getTotalCount();
            float catchWeight = meanWeight * (float) totalCount / 1000.f;
            aCatch.setCatchWeight(catchWeight);
            aCatch.setCatchWeightComputedSource(CatchComputedValueSource.fromData);
        }
    },
    /**
     * Compute {@link Catch#getTotalCount()} using {@link Catch#getCatchWeight()} and {@link Catch#getMeanWeight()}.
     * <p>
     * calcul le nb estime à partir du poids estime et du poids moyen
     */
    ComputeTotalCountFromCatchWeightAndMeanWeight() {
        @Override
        public List<String> fieldNames() {
            return List.of(Catch.PROPERTY_TOTAL_COUNT, Catch.PROPERTY_TOTAL_COUNT_COMPUTED_SOURCE);
        }

        @Override
        public boolean test(CatchConsolidateContext context, Catch aCatch) {
            return aCatch.withoutTotalCount()
                    && aCatch.withCatchWeight()
                    && aCatch.withMeanWeight();
        }

        @Override
        public void accept(CatchConsolidateContext context, Catch aCatch) {
            float meanWeight = aCatch.getMeanWeight();
            float catchWeight = aCatch.getCatchWeight();
            int totalCount = Math.round(1000f * catchWeight / meanWeight);
            aCatch.setTotalCount(totalCount);
            aCatch.setTotalCountComputedSource(CatchComputedValueSource.fromData);
        }
    },
    /**
     * Compute {@link Catch#getMeanWeight()} using {@link Catch#getTotalCount()} and {@link Catch#getCatchWeight()}.
     * <p>
     * calcul le poids moyen à partir de nb estime et du poids estime uniquement si le nombre estimé ne vient
     * pas des échantillons.
     * <p>
     * See <a href="http://forge.codelutin.com/issues/4670">issue 4670</a>
     */
    ComputeMeanWeightFromTotalCountAndCatchWeight() {
        @Override
        public List<String> fieldNames() {
            return List.of(Catch.PROPERTY_MEAN_WEIGHT, Catch.PROPERTY_MEAN_WEIGHT_COMPUTED_SOURCE);
        }

        @Override
        public boolean test(CatchConsolidateContext context, Catch aCatch) {
            return aCatch.withoutMeanWeight()
                    && aCatch.withCatchWeight()
                    && aCatch.withTotalCount()
                    && aCatch.getTotalCount() != 0
                    && !CatchComputedValueSource.fromSample.equals(aCatch.getTotalCountComputedSource());
        }

        @Override
        public void accept(CatchConsolidateContext context, Catch aCatch) {
            float catchWeight = aCatch.getCatchWeight();
            int totalCount = aCatch.getTotalCount();
            float meanWeight = catchWeight * (float) 1000 / (float) totalCount;
            aCatch.setMeanWeight(meanWeight);
            aCatch.setMeanWeightComputedSource(CatchComputedValueSource.fromData);
        }
    },
    /**
     * Copy from RTP the meanLength to {@link Catch#getMeanLength()}}.
     */
    CopyMeanLengthFromParameter() {
        @Override
        public List<String> fieldNames() {
            return List.of(Catch.PROPERTY_MEAN_LENGTH, Catch.PROPERTY_MEAN_LENGTH_COMPUTED_SOURCE);
        }

        @Override
        public boolean test(CatchConsolidateContext context, Catch aCatch) {
            return aCatch.withoutMeanLength()
                    && context.withLengthWeightParameter()
                    && context.getLengthWeightParameter().getMeanLength() != null;
        }

        @Override
        public void accept(CatchConsolidateContext context, Catch aCatch) {
            float meanLength = context.getLengthWeightParameter().getMeanLength();
            aCatch.setMeanLength(meanLength);
            aCatch.setMeanLengthComputedSource(CatchComputedValueSource.fromReferentiel);
        }
    },
    /**
     * Copy from RTP the meanWeight to {@link Catch#getMeanWeight()}.
     */
    CopyMeanWeightFromParameter() {
        @Override
        public List<String> fieldNames() {
            return List.of(Catch.PROPERTY_MEAN_WEIGHT, Catch.PROPERTY_MEAN_WEIGHT_COMPUTED_SOURCE);
        }

        @Override
        public boolean test(CatchConsolidateContext context, Catch aCatch) {
            return aCatch.withoutMeanWeight()
                    && context.withLengthWeightParameter()
                    && context.getLengthWeightParameter().getMeanWeight() != null;
        }

        @Override
        public void accept(CatchConsolidateContext context, Catch aCatch) {
            float meanWeight = context.getLengthWeightParameter().getMeanWeight();
            aCatch.setMeanWeight(meanWeight);
            aCatch.setMeanWeightComputedSource(CatchComputedValueSource.fromReferentiel);
        }
    },
    /**
     * Compute {@link Catch#getMeanLength()} from the given sample measures.
     */
    ComputeMeanLengthFromSamples() {
        @Override
        public List<String> fieldNames() {
            return List.of(Catch.PROPERTY_MEAN_LENGTH, Catch.PROPERTY_MEAN_LENGTH_COMPUTED_SOURCE);
        }

        @Override
        public boolean test(CatchConsolidateContext context, Catch aCatch) {
            return context.withSampleMeasures() && aCatch.withoutMeanLength();
        }

        @Override
        public void accept(CatchConsolidateContext context, Catch aCatch) {

            // on essaye de calculer la taille moyenne à partir des échantillons
            // on calcul la taille moyenne à partir des échantillons
            float totalLength = 0f;
            int totalCount = 0;
            for (SampleMeasure sampleMeasure : context.getSampleMeasures()) {
                Integer count = sampleMeasure.getCount();
                Float length = sampleMeasure.getLength();
                if (count != null && length != null) {
                    totalCount += count;
                    totalLength += length * count;
                }
            }
            if (totalCount != 0) {
                float meanLength = totalLength / totalCount;
                aCatch.setMeanLength(meanLength);
                aCatch.setMeanLengthComputedSource(CatchComputedValueSource.fromSample);
            }
        }
    },
    /**
     * Compute {@link Catch#getTotalCount()} from the given sample measures.
     */
    ComputeTotalCountFromSamples() {
        @Override
        public List<String> fieldNames() {
            return List.of(Catch.PROPERTY_TOTAL_COUNT, Catch.PROPERTY_TOTAL_COUNT_COMPUTED_SOURCE);
        }

        @Override
        public boolean test(CatchConsolidateContext context, Catch aCatch) {
            return context.withSampleMeasures() && aCatch.withoutTotalCount();
        }

        @Override
        public void accept(CatchConsolidateContext context, Catch aCatch) {
            int totalCount = 0;
            // on calcul la nombre d'individus à partir des échantillons
            for (SampleMeasure sampleMeasure : context.getSampleMeasures()) {
                Integer count = sampleMeasure.getCount();
                if (count != null) {
                    totalCount += count;
                }
            }
            if (totalCount != 0) {
                aCatch.setTotalCount(totalCount);
                aCatch.setTotalCountComputedSource(CatchComputedValueSource.fromSample);
            }
        }
    },
    /**
     * Remove measure method when no catchWeight or catchWeight is computed.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2628">issue 2628</a>
     */
    CleanCatchWeightMeasureMethod() {
        @Override
        public List<String> fieldNames() {
            return List.of(Catch.PROPERTY_WEIGHT_MEASURE_METHOD);
        }

        @Override
        public boolean test(CatchConsolidateContext context, Catch aCatch) {
            return aCatch.getWeightMeasureMethod() != null && (aCatch.withoutCatchWeight() || aCatch.isCatchWeightComputed());
        }

        @Override
        public void accept(CatchConsolidateContext context, Catch aCatch) {
            aCatch.setWeightMeasureMethod(null);
        }
    };

    @Override
    public String toString() {
        return "ps.observation." + CatchConsolidateActions.class.getSimpleName() + "." + name();
    }
}
