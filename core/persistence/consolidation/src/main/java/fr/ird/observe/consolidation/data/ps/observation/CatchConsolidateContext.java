package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.ConsolidateContext;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.entities.data.ps.observation.Catch;
import fr.ird.observe.entities.data.ps.observation.SampleMeasure;
import fr.ird.observe.entities.data.ps.observation.Set;
import fr.ird.observe.entities.referential.common.LengthWeightParameter;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.ps.common.SpeciesFate;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created on 22/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class CatchConsolidateContext implements ConsolidateContext<Catch> {
    private static final Logger log = LogManager.getLogger(CatchConsolidateContext.class);
    private final ActivityConsolidateContext activityConsolidateContext;
    private final JavaBeanMonitor monitor;
    private final Decorator decorator;
    private final java.util.Set<String> warnings = new LinkedHashSet<>();
    private Catch aCatch;
    private LengthWeightParameter lengthWeightParameter;
    private boolean lengthWeightParameterLoaded;
    private Collection<SampleMeasure> sampleMeasures;
    private boolean sampleMeasuresLoaded;

    public CatchConsolidateContext(ActivityConsolidateContext activityConsolidateContext, JavaBeanMonitor monitor, Decorator decorator) {
        this.activityConsolidateContext = activityConsolidateContext;
        this.monitor = monitor;
        this.decorator = decorator;
    }

    @Override
    public Class<Catch> dataType() {
        return Catch.class;
    }

    @Override
    public JavaBeanMonitor monitor() {
        return monitor;
    }

    public void watch(Catch aCatch) {
        this.aCatch = Objects.requireNonNull(aCatch);
        monitor.setBean(aCatch);
        if (aCatch.isCatchWeightComputed()) {
            aCatch.setCatchWeight(null);
            aCatch.setCatchWeightComputedSource(null);
        }
        if (aCatch.isTotalCountComputed()) {
            aCatch.setTotalCount(null);
            aCatch.setTotalCountComputedSource(null);
        }
        if (aCatch.isMeanWeightComputed()) {
            aCatch.setMeanWeight(null);
            aCatch.setMeanWeightComputedSource(null);
        }
        if (aCatch.isMeanLengthComputed()) {
            aCatch.setMeanLength(null);
            aCatch.setMeanLengthComputedSource(null);
        }
    }

    public boolean isCatchFullyFilled() {
        return aCatch.withCatchWeight() && aCatch.withTotalCount() && aCatch.withMeanWeight() && aCatch.withMeanLength();
    }

    public LengthWeightParameter getLengthWeightParameter() {
        if (!lengthWeightParameterLoaded) {
            lengthWeightParameter = loadLengthWeightParameter();
        }
        return lengthWeightParameter;
    }

    public boolean withLengthWeightParameter() {
        return getLengthWeightParameter() != null;
    }

    public Collection<SampleMeasure> getSampleMeasures() {
        if (!sampleMeasuresLoaded) {
            sampleMeasures = loadSampleMeasures();
        }
        return sampleMeasures;
    }

    public boolean withSampleMeasures() {
        return getSampleMeasures() != null && !getSampleMeasures().isEmpty();
    }

    public void flush() {
        Catch entity = (Catch) monitor.getBean();
        if (monitor.wasModified()) {
            entity.registerDecorator(decorator);
            monitor.toModifications(modifications -> new ToolkitIdModifications(entity, modifications, warnings.isEmpty() ? null : warnings)).ifPresent(activityConsolidateContext::flushCatch);
        } else if (!warnings.isEmpty()) {
            entity.registerDecorator(decorator);
            activityConsolidateContext.flushCatch(new ToolkitIdModifications(entity, java.util.Set.of(), java.util.Set.copyOf(warnings)));
        }
        monitor.setBean(null);
        aCatch = null;
        warnings.clear();
        lengthWeightParameterLoaded = false;
        lengthWeightParameter = null;
        sampleMeasuresLoaded = false;
        sampleMeasures = null;
    }

    private LengthWeightParameter loadLengthWeightParameter() {
        lengthWeightParameterLoaded = true;
        Species species = aCatch.getSpecies();
        Optional<LengthWeightParameter> optionalLengthWeightParameter = activityConsolidateContext.findLengthWeightParameter(species, null/* pas de sexe spécifié*/, species.getSizeMeasureType(), this::addWarning);
        return optionalLengthWeightParameter.orElse(null);
    }

    /**
     * Obtain sample measures from the set owner matching species/speciesFate from the catch.
     * <p>
     * If not found, will search for sample measures matching species from the catch and with no speciesFate.
     *
     * @return sample measures
     */
    private Collection<SampleMeasure> loadSampleMeasures() {
        sampleMeasuresLoaded = true;
        Set set = activityConsolidateContext.getSet();
        Species species = aCatch.getSpecies();
        SpeciesFate speciesFate = aCatch.getSpeciesFate();
        Collection<SampleMeasure> sampleMeasures = null;
        if (set.isSampleNotEmpty()) {
            // search on couple species/speciesFate
            sampleMeasures = set.getSample().iterator().next().getSampleMeasure()
                    .stream()
                    .filter(l -> species.equals(l.getSpecies()) && Objects.equals(speciesFate, l.getSpeciesFate()))
                    .collect(Collectors.toList());

            if (sampleMeasures.isEmpty() && speciesFate != null) {
                // no match ou couple species/speciesFate
                // try a fallback search on couple species/null
                sampleMeasures = set.getSample().iterator().next().getSampleMeasure()
                        .stream()
                        .filter(l -> species.equals(l.getSpecies()) && l.getSpeciesFate() == null)
                        .collect(Collectors.toList());
            }
        }
        return sampleMeasures;
    }

    private void addWarning(String warning) {
        log.warn(String.format("%s [%s], %s", SampleMeasure.class.getSimpleName(), aCatch.getTopiaId(), warning));
        warnings.add(warning);
    }
}
