package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.entities.data.ps.observation.Catch;
import fr.ird.observe.entities.data.ps.observation.Set;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;

/**
 * To consolidate catches.
 * <p>
 * Created on 22/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class CatchConsolidateEngine {

    private final JavaBeanMonitor monitor;
    private final Decorator decorator;

    public CatchConsolidateEngine(DecoratorService decoratorService) {
        this.monitor = new JavaBeanMonitor(
                Catch.PROPERTY_MEAN_LENGTH,
                Catch.PROPERTY_MEAN_LENGTH_COMPUTED_SOURCE,
                Catch.PROPERTY_MEAN_WEIGHT,
                Catch.PROPERTY_MEAN_WEIGHT_COMPUTED_SOURCE,
                Catch.PROPERTY_CATCH_WEIGHT,
                Catch.PROPERTY_CATCH_WEIGHT_COMPUTED_SOURCE,
                Catch.PROPERTY_WEIGHT_MEASURE_METHOD,
                Catch.PROPERTY_TOTAL_COUNT,
                Catch.PROPERTY_TOTAL_COUNT_COMPUTED_SOURCE);
        this.decorator = decoratorService.getDecoratorByType(Catch.class);
    }

    public void consolidate(ActivityConsolidateContext activityContext, Set set) {
        if (set.isCatchesNotEmpty()) {
            CatchConsolidateContext context = new CatchConsolidateContext(activityContext, monitor, decorator);
            for (Catch aCatch : set.getCatches()) {
                context.watch(aCatch);
                try {
                    consolidateCatch(context, aCatch);
                    CatchConsolidateActions.CleanCatchWeightMeasureMethod.execute(context, aCatch);
                } finally {
                    context.flush();
                }
            }
        }
    }

    private void consolidateCatch(CatchConsolidateContext context, Catch aCatch) {
        if (context.isCatchFullyFilled()) {
            return;
        }

        // -- Case n°1 (compute states from RTP and catch internal states)
        updateCatchByLengthWeightParameter(context, aCatch);
        if (context.isCatchFullyFilled()) {
            return;
        }

        if (aCatch.withCatchWeight() || aCatch.withTotalCount()) {
            // -- Case n°2 (No mean length, nor mean weight, but at least catchWeight or totalCount is filled)

            CatchConsolidateActions.ComputeMeanLengthFromSamples.execute(context, aCatch);
            CatchConsolidateActions.CopyMeanLengthFromParameter.execute(context, aCatch);
            if (context.isCatchFullyFilled()) {
                return;
            }

            if (aCatch.withMeanLength()) {
                // Try again Case n°1
                updateCatchByLengthWeightParameter(context, aCatch);
            }
            if (context.isCatchFullyFilled()) {
                return;
            }
        }

        // -- Case n°3 (No total count and some sample measures to compute it)
        if (CatchConsolidateActions.ComputeTotalCountFromSamples.test(context, aCatch)) {
            CatchConsolidateActions.ComputeTotalCountFromSamples.accept(context, aCatch);
            if (context.isCatchFullyFilled()) {
                return;
            }
            if (aCatch.withMeanWeight() || aCatch.withMeanLength()) {
                // Try again Case n°1
                updateCatchByLengthWeightParameter(context, aCatch);
            }
            if (context.isCatchFullyFilled()) {
                return;
            }
        }

        // -- Case n°4 (No mean weight, nor mean length)
        CatchConsolidateActions.CopyMeanWeightFromParameter.execute(context, aCatch);
        CatchConsolidateActions.CopyMeanLengthFromParameter.execute(context, aCatch);

        if (context.isCatchFullyFilled()) {
            // all data are filled, nothing more to do
            return;
        }

        if (aCatch.withMeanWeight() || aCatch.withMeanLength()) {
            // Try again Case n°1
            updateCatchByLengthWeightParameter(context, aCatch);
        }
    }

    /**
     * @param context catch consolidation context
     * @param aCatch  data to use
     */
    private void updateCatchByLengthWeightParameter(CatchConsolidateContext context, Catch aCatch) {
        CatchConsolidateActions.ComputeMeanLengthFromMeanWeight.execute(context, aCatch);
        CatchConsolidateActions.ComputeMeanWeightFromMeanLength.execute(context, aCatch);
        CatchConsolidateActions.ComputeCatchWeightFromTotalCountAndMeanWeight.execute(context, aCatch);
        CatchConsolidateActions.ComputeTotalCountFromCatchWeightAndMeanWeight.execute(context, aCatch);
        CatchConsolidateActions.ComputeMeanWeightFromTotalCountAndCatchWeight.execute(context, aCatch);
        // on ressaye d'appliquer la relation taille - poids au cas où une des
        // trois valeurs précédentes a été calculée, on pourrait peut-être
        // ainsi en déduire via le paramétrage la taille moyenne
        CatchConsolidateActions.ComputeMeanLengthFromMeanWeight.execute(context, aCatch);
    }

}
