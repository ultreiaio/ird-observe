package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.entities.data.ps.observation.SampleMeasure;
import fr.ird.observe.entities.referential.common.LengthLengthParameter;
import fr.ird.observe.entities.referential.common.LengthWeightParameter;
import fr.ird.observe.entities.referential.common.SizeMeasureType;

import java.util.List;
import java.util.Objects;

/**
 * Created on 23/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public enum SampleMeasureConsolidateActions implements AtomicConsolidateAction<SampleMeasureConsolidateContext, SampleMeasure> {

    /**
     * Compute {@link SampleMeasure#getLength()} from {@link SampleMeasure#getWeight()} using the RTP.
     */
    ComputeLengthFromWeight() {
        @Override
        public List<String> fieldNames() {
            return List.of(SampleMeasure.PROPERTY_LENGTH, SampleMeasure.PROPERTY_IS_LENGTH_COMPUTED);
        }

        @Override
        public boolean test(SampleMeasureConsolidateContext context, SampleMeasure sampleMeasure) {
            return sampleMeasure.getLength() == null
                    && sampleMeasure.getWeight() != null
                    && context.withLengthWeightParameter();
        }

        @Override
        public void accept(SampleMeasureConsolidateContext context, SampleMeasure sampleMeasure) {
            LengthWeightParameter lengthWeightParameter = context.getLengthWeightParameter();
            Float weight = sampleMeasure.getWeight();
            Float length = lengthWeightParameter.computeFromFormulaTwo(weight);
            if (length != null) {
                sampleMeasure.setLength(length);
                sampleMeasure.setIsLengthComputed(true);
            }
        }
    },
    /**
     * Compute {@link SampleMeasure#getWeight()} from {@link SampleMeasure#getLength()} using the RTP with a length
     * conversion if required (if {@link SampleMeasure#getSizeMeasureType()} is not equals to
     * {@link LengthWeightParameter#getSizeMeasureType()}).
     */
    ComputeWeightFromLength() {
        @Override
        public List<String> fieldNames() {
            return List.of(SampleMeasure.PROPERTY_WEIGHT, SampleMeasure.PROPERTY_IS_WEIGHT_COMPUTED);
        }

        @Override
        public boolean test(SampleMeasureConsolidateContext context, SampleMeasure sampleMeasure) {
            boolean canExecute = sampleMeasure.getWeight() == null
                    && sampleMeasure.getLength() != null
                    && context.withLengthWeightParameter();
            if (canExecute) {
                SizeMeasureType inputSizeMeasureType = sampleMeasure.getSizeMeasureType();
                LengthWeightParameter lengthWeightParameter = context.getLengthWeightParameter();
                SizeMeasureType outputSizeMeasureType = lengthWeightParameter.getSizeMeasureType();
                if (!Objects.equals(inputSizeMeasureType, outputSizeMeasureType)) {
                    canExecute = context.withLengthLengthParameter();
                }
            }
            return canExecute;
        }

        @Override
        public void accept(SampleMeasureConsolidateContext context, SampleMeasure sampleMeasure) {
            Float length = sampleMeasure.getLength();
            SizeMeasureType inputSizeMeasureType = sampleMeasure.getSizeMeasureType();
            LengthWeightParameter lengthWeightParameter = context.getLengthWeightParameter();
            SizeMeasureType outputSizeMeasureType = lengthWeightParameter.getSizeMeasureType();
            if (!Objects.equals(inputSizeMeasureType, outputSizeMeasureType)) {
                // execute length conversion before applying the RTP
                LengthLengthParameter lengthLengthParameter = context.getLengthLengthParameter();
                length = lengthLengthParameter.computeFromFormulaOne(length);
                if (length == null) {
                    return;
                }
            }
            Float weight = lengthWeightParameter.computeFromFormulaOne(length);
            if (weight != null) {
                sampleMeasure.setWeight(weight);
                sampleMeasure.setIsWeightComputed(true);
            }
        }
    };

    @Override
    public String toString() {
        return "ps.observation." + SampleMeasureConsolidateActions.class.getSimpleName() + "." + name();
    }
}

