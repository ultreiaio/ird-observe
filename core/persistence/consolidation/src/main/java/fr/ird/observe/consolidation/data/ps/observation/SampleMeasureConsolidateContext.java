package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.ConsolidateContext;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.entities.data.ps.observation.SampleMeasure;
import fr.ird.observe.entities.referential.common.LengthLengthParameter;
import fr.ird.observe.entities.referential.common.LengthWeightParameter;
import fr.ird.observe.entities.referential.common.SizeMeasureType;
import fr.ird.observe.entities.referential.common.Species;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 23/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class SampleMeasureConsolidateContext implements ConsolidateContext<SampleMeasure> {
    private static final Logger log = LogManager.getLogger(SampleMeasureConsolidateContext.class);
    private final ActivityConsolidateContext activityConsolidateContext;
    private final JavaBeanMonitor monitor;
    private final Decorator decorator;
    private final java.util.Set<String> warnings = new LinkedHashSet<>();
    private SampleMeasure sampleMeasure;

    private LengthWeightParameter lengthWeightParameter;
    private boolean lengthWeightParameterLoaded;
    private LengthLengthParameter lengthLengthParameter;
    private boolean lengthLengthParameterLoaded;

    public SampleMeasureConsolidateContext(ActivityConsolidateContext activityConsolidateContext, JavaBeanMonitor monitor, Decorator decorator) {
        this.activityConsolidateContext = activityConsolidateContext;
        this.monitor = monitor;
        this.decorator = decorator;
    }

    public void watch(SampleMeasure sampleMeasure) {
        this.sampleMeasure = sampleMeasure;
        monitor.setBean(sampleMeasure);
        if (sampleMeasure.isIsLengthComputed()) {
            sampleMeasure.setLength(null);
            sampleMeasure.setIsLengthComputed(false);
        }
        if (sampleMeasure.isIsWeightComputed()) {
            sampleMeasure.setWeight(null);
            sampleMeasure.setIsWeightComputed(false);
        }
    }

    public void flush() {
        SampleMeasure entity = (SampleMeasure) monitor.getBean();
        if (monitor.wasModified()) {
            entity.registerDecorator(decorator);
            monitor.toModifications(modifications -> new ToolkitIdModifications(entity, modifications, warnings.isEmpty() ? null : Set.copyOf(warnings))).ifPresent(activityConsolidateContext::flushSampleMeasure);
        } else if (!warnings.isEmpty()) {
            entity.registerDecorator(decorator);
            activityConsolidateContext.flushSampleMeasure(new ToolkitIdModifications(entity, Set.of(), Set.copyOf(warnings)));
        }
        sampleMeasure = null;
        lengthLengthParameterLoaded = false;
        lengthWeightParameterLoaded = false;
        lengthWeightParameter = null;
        lengthLengthParameter = null;
        monitor.setBean(null);
        warnings.clear();
    }

    public LengthWeightParameter getLengthWeightParameter() {
        if (!lengthWeightParameterLoaded) {
            lengthWeightParameter = loadLengthWeightParameter();
        }
        return lengthWeightParameter;
    }

    public LengthLengthParameter getLengthLengthParameter() {
        if (!lengthLengthParameterLoaded) {
            lengthLengthParameter = loadLengthLengthParameter();
        }
        return lengthLengthParameter;
    }

    public boolean withLengthWeightParameter() {
        return getLengthWeightParameter() != null;
    }

    public boolean withLengthLengthParameter() {
        return getLengthLengthParameter() != null;
    }

    @Override
    public Class<SampleMeasure> dataType() {
        return SampleMeasure.class;
    }

    @Override
    public JavaBeanMonitor monitor() {
        return monitor;
    }

    private LengthLengthParameter loadLengthLengthParameter() {
        lengthLengthParameterLoaded = true;
        SizeMeasureType sizeMeasureType = sampleMeasure.getSizeMeasureType();
        SizeMeasureType outputSizeMeasureType = lengthWeightParameter.getSizeMeasureType();
        return activityConsolidateContext.findLengthLengthParameter(sampleMeasure.getSpecies(), null, sizeMeasureType, outputSizeMeasureType, this::addWarning).orElse(null);
    }

    private LengthWeightParameter loadLengthWeightParameter() {
        lengthWeightParameterLoaded = true;
        SizeMeasureType sizeMeasureType = sampleMeasure.getSizeMeasureType();
        Species species = sampleMeasure.getSpecies();
        Optional<LengthWeightParameter> optionalLengthWeightParameter = activityConsolidateContext.findLengthWeightParameter(species, null/* pas de sexe spécifié*/, sizeMeasureType, this::addWarning);
        return optionalLengthWeightParameter.orElse(null);
    }

    private void addWarning(String warning) {
        log.warn(String.format("%s [%s], %s", SampleMeasure.class.getSimpleName(), sampleMeasure.getTopiaId(), warning));
        warnings.add(warning);
    }
}
