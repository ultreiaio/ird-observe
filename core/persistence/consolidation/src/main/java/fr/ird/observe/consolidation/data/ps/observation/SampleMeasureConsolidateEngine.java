package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.entities.data.ps.observation.Sample;
import fr.ird.observe.entities.data.ps.observation.SampleMeasure;
import fr.ird.observe.entities.data.ps.observation.Set;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;

/**
 * To consolidate sample.
 * <p>
 * Created on 22/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class SampleMeasureConsolidateEngine {
    private final JavaBeanMonitor monitor;
    private final Decorator decorator;

    public SampleMeasureConsolidateEngine(DecoratorService decoratorService) {
        this.decorator = decoratorService.getDecoratorByType(SampleMeasure.class);
        this.monitor = new JavaBeanMonitor(
                SampleMeasure.PROPERTY_LENGTH,
                SampleMeasure.PROPERTY_IS_LENGTH_COMPUTED,
                SampleMeasure.PROPERTY_WEIGHT,
                SampleMeasure.PROPERTY_IS_WEIGHT_COMPUTED);
    }

    public void consolidate(ActivityConsolidateContext activityContext, Set set) {
        if (set.isSampleNotEmpty()) {
            SampleMeasureConsolidateContext context = new SampleMeasureConsolidateContext(activityContext, monitor, decorator);

            for (Sample sample : set.getSample()) {
                if (sample.isSampleMeasureNotEmpty()) {
                    for (SampleMeasure sampleMeasure : sample.getSampleMeasure()) {
                        context.watch(sampleMeasure);
                        try {
                            consolidateSampleMeasure(context, sampleMeasure);
                        } finally {
                            context.flush();
                        }
                    }
                }
            }
        }
    }

    private void consolidateSampleMeasure(SampleMeasureConsolidateContext context, SampleMeasure sampleMeasure) {
        SampleMeasureConsolidateActions.ComputeLengthFromWeight.execute(context, sampleMeasure);
        SampleMeasureConsolidateActions.ComputeWeightFromLength.execute(context, sampleMeasure);
    }

}
