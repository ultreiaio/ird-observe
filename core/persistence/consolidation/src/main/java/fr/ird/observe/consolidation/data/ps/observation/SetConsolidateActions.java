package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.AtomicConsolidateAction;
import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.entities.data.ps.observation.Activity;
import fr.ird.observe.entities.data.ps.observation.Set;
import fr.ird.observe.entities.referential.ps.common.ObservedSystem;
import fr.ird.observe.entities.referential.ps.common.SchoolType;

import java.util.List;
import java.util.Objects;

/**
 * Created on 12/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.28
 */
public enum SetConsolidateActions implements AtomicConsolidateAction<SetConsolidateContext, Set> {

    ComputeSchoolType() {
        @Override
        public boolean test(SetConsolidateContext context, Set set) {
            // can always try to compute it
            return true;
        }

        @Override
        public void accept(SetConsolidateContext context, Set set) {
            SchoolType oldSchoolType = set.getSchoolType();
            String oldSchoolTypeId = oldSchoolType == null ? null : oldSchoolType.getTopiaId();
            String newSchoolTypeId = computeSchoolTypeId(context.activity());
            if (!Objects.equals(oldSchoolTypeId, newSchoolTypeId)) {
                // school type has changed
                SchoolType newSchoolType = context.schoolType(newSchoolTypeId);
                // apply to entity
                set.setSchoolType(newSchoolType);
            }
        }

        @Override
        public List<String> fieldNames() {
            return List.of(Set.PROPERTY_SCHOOL_TYPE);
        }

        /**
         * Compute {@link Set#getSchoolType()}.
         * <p>
         * If activity contains at least one observed system with {@code Object school type} then return it,
         * otherwise use {@code Free school type}.
         *
         * @param activity activity to test
         * @return computed school type id
         */
        private String computeSchoolTypeId(Activity activity) {
            for (ObservedSystem s : activity.getObservedSystem()) {
                if (ProtectedIdsPs.PS_COMMON_SCHOOL_TYPE_OBJECT_ID.equals(s.getSchoolType().getTopiaId())) {
                    // set on object school type
                    return ProtectedIdsPs.PS_COMMON_SCHOOL_TYPE_OBJECT_ID;
                }
            }
            // if no observed system on object school type, use free school type.
            return ProtectedIdsPs.PS_COMMON_SCHOOL_TYPE_FREE_ID;
        }
    };

    @Override
    public String toString() {
        return "ps.observation." + SetConsolidateActions.class.getSimpleName() + "." + name();
    }
}
