package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.ConsolidateContext;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.entities.data.ps.observation.Activity;
import fr.ird.observe.entities.data.ps.observation.Set;
import fr.ird.observe.entities.referential.ps.common.SchoolType;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;

import java.util.Map;
import java.util.Objects;

/**
 * Created on 12/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.28
 */
public class SetConsolidateContext implements ConsolidateContext<Set> {

    private final ActivityConsolidateContext activityConsolidateContext;
    private final JavaBeanMonitor monitor;
    private final Decorator decorator;
    private final Map<String, SchoolType> schoolTypeById;
    private Set data;

    public SetConsolidateContext(ActivityConsolidateContext activityConsolidateContext, JavaBeanMonitor monitor, Decorator decorator, Map<String, SchoolType> schoolTypeById) {
        this.activityConsolidateContext = activityConsolidateContext;
        this.monitor = monitor;
        this.decorator = decorator;
        this.schoolTypeById = schoolTypeById;
    }

    public SchoolType schoolType(String id) {
        return schoolTypeById.get(id);
    }

    public Activity activity() {
        return activityConsolidateContext.getActivity();
    }

    public void watch(Set data) {
        this.data = Objects.requireNonNull(data);
        monitor.setBean(data);
    }

    public void flush() {
        if (monitor.wasModified()) {
            data.registerDecorator(decorator);
            monitor.toModifications(modifications -> new ToolkitIdModifications(data, modifications, null)).ifPresent(activityConsolidateContext::flushSet);
        }
        data = null;
        monitor.setBean(null);
    }

    @Override
    public Class<Set> dataType() {
        return Set.class;
    }

    @Override
    public JavaBeanMonitor monitor() {
        return monitor;
    }

}
