package fr.ird.observe.consolidation.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Consolidation
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.entities.data.ps.observation.Activity;
import fr.ird.observe.entities.data.ps.observation.Set;
import fr.ird.observe.entities.referential.ps.common.SchoolType;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.bean.monitor.JavaBeanMonitor;
import io.ultreia.java4all.decoration.Decorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

/**
 * To consolidate set.
 * <p>
 * Created on 22/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class SetConsolidateEngine {
    private static final Logger log = LogManager.getLogger(SetConsolidateEngine.class);

    private final SampleMeasureConsolidateEngine sampleMeasureConsolidateEngine;
    private final CatchConsolidateEngine catchConsolidateEngine;
    private final Map<String, SchoolType> schoolTypeById;
    private final JavaBeanMonitor monitor;
    private final Decorator decorator;

    public SetConsolidateEngine(ServiceContext context) {
        this.schoolTypeById = Maps.uniqueIndex(SchoolType.loadEntities(context), SchoolType::getTopiaId);
        this.sampleMeasureConsolidateEngine = new SampleMeasureConsolidateEngine(context.getDecoratorService());
        this.catchConsolidateEngine = new CatchConsolidateEngine(context.getDecoratorService());
        this.decorator = context.getDecoratorService().getDecoratorByType(Set.class);
        this.monitor = new JavaBeanMonitor(Set.PROPERTY_SCHOOL_TYPE);
    }

    public void consolidate(ActivityConsolidateContext activityContext, Activity activity) {
        Set set = activity.getSet();
        if (set == null) {
            log.debug(String.format("%s No set found for activity: %s", activityContext.getActivityPrefix(), activity.getTopiaId()));
            return;
        }
        consolidate(activityContext, set);
        sampleMeasureConsolidateEngine.consolidate(activityContext, set);
        catchConsolidateEngine.consolidate(activityContext, set);
    }

    private void consolidate(ActivityConsolidateContext activityContext, Set set) {
        SetConsolidateContext context = new SetConsolidateContext(activityContext, monitor, decorator, schoolTypeById);
        context.watch(set);
        try {
            SetConsolidateActions.ComputeSchoolType.execute(context, set);
        } finally {
            context.flush();
        }
    }
}
