package fr.ird.observe.entities.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.decoration.ObserveEntityDecoratorRenderer;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.i18n.I18n;
import java.util.Locale;
import java.util.Optional;
import javax.annotation.Generated;

@SuppressWarnings("rawtypes")
@AutoService(DecoratorDefinition.class)
@Generated(value = "io.ultreia.java4all.decoration.spi.DecoratorDefinitionJavaFileBuilder")
public final class TripTripLocalmarketDecoratorDefinition extends DecoratorDefinition<Trip, ObserveEntityDecoratorRenderer<Trip>> {

    public TripTripLocalmarketDecoratorDefinition() {
        super(Trip.class, "TripLocalmarket", "##", " - ", new ObserveEntityDecoratorRenderer<>(Trip.class), "_observe.data.ps.common.TripLocalmarket.type", "localmarketBatchStat", "localmarketSurveyStat", "localmarketSampleStat");
    }

    @Override
    public final String decorateContext(Locale locale, ObserveEntityDecoratorRenderer<Trip> renderer, Trip source, int index) {
        switch (index) {
            case 0:
              return I18n.l(locale, "observe.data.ps.common.TripLocalmarket.type");
            case 1:
              return " ( " + renderer.stat(locale, source.getLocalmarketBatchStat());
            case 2:
              return renderer.stat(locale, source.getLocalmarketSurveyStat());
            case 3:
              return renderer.stat(locale, source.getLocalmarketSampleStat()) + " )";
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    @Override
    public final String decorate(Locale locale, Object source, String rendererSeparator, int index) {
        return decoratorForContextCountEquals4(locale, source, rendererSeparator, index);
    }

}
