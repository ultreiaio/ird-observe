package fr.ird.observe.entities.decoration;

/*-
 * #%L
 * ObServe Core :: Persistence :: Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import io.ultreia.java4all.decoration.DecoratorProviderInitializer;

import java.util.Set;

/**
 * Created on 02/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@AutoService(DecoratorProviderInitializer.class)
public class ObserveEntityDecoratorProviderInitializer extends EntityDecoratorProviderInitializer {
    @Override
    public Set<String> codeProperties() {
        return ObserveI18nDecoratorHelper.CODE_PROPERTIES;
    }

    @Override
    protected ObservePersistenceBusinessProject businessProject() {
        return ObservePersistenceBusinessProject.get();
    }
}
