package fr.ird.observe.entities.referential.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.decoration.ObserveEntityDecoratorRenderer;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.i18n.I18n;
import java.util.Locale;
import java.util.Optional;
import javax.annotation.Generated;

@SuppressWarnings("rawtypes")
@AutoService(DecoratorDefinition.class)
@Generated(value = "io.ultreia.java4all.decoration.spi.DecoratorDefinitionJavaFileBuilder")
public final class LengthLengthParameterDecoratorDefinition extends DecoratorDefinition<LengthLengthParameter, ObserveEntityDecoratorRenderer<LengthLengthParameter>> {

    public LengthLengthParameterDecoratorDefinition() {
        super(LengthLengthParameter.class, null, "##", " - ", new ObserveEntityDecoratorRenderer<>(LengthLengthParameter.class), "speciesFaoCode", "speciesLabel", "ocean", "sex", "validityRangeLabel", "inputSizeMeasureType", "outputSizeMeasureType");
    }

    @Override
    public final String decorateContext(Locale locale, ObserveEntityDecoratorRenderer<LengthLengthParameter> renderer, LengthLengthParameter source, int index) {
        switch (index) {
            case 0:
              return renderer.onNullValue("speciesFaoCode", locale, source.getSpeciesFaoCode());
            case 1:
              return renderer.speciesSimpleLabel(locale, source.getSpeciesLabel());
            case 2:
              return I18n.l(locale, "observe.Common.ocean") + " " + renderer.labelOrUnknown(locale, source.getOcean());
            case 3:
              return I18n.l(locale, "observe.Common.sex") + " " + renderer.label(locale, source.getSex());
            case 4:
              return renderer.validityRangeLabel(locale, source);
            case 5:
              return renderer.labelOrUnknown(locale, source.getInputSizeMeasureType());
            case 6:
              return renderer.labelOrUnknown(locale, source.getOutputSizeMeasureType());
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    @Override
    public final String decorate(Locale locale, Object source, String rendererSeparator, int index) {
        return decoratorForContextCountEquals7(locale, source, rendererSeparator, index);
    }

}
