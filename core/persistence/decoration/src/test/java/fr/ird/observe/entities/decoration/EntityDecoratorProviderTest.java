package fr.ird.observe.entities.decoration;

/*-
 * #%L
 * ObServe Core :: Persistence :: Decoration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObservePersistenceFixtures;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.referential.common.Sex;
import fr.ird.observe.entities.referential.common.SexImpl;
import fr.ird.observe.spi.GroupBySpiContext;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created on 18/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class EntityDecoratorProviderTest {

    private static final Logger log = LogManager.getLogger(EntityDecoratorProviderTest.class);
    private DecoratorProvider provider;
    private ObserveBusinessProject businessProject;
    private DecoratorService decoratorService;

    @Before
    public void setUp() {
        provider = DecoratorProvider.get();
    }

    @After
    public void tearDown() {
        provider.close();
    }

    @Test
    public void init() {
        ObservePersistenceFixtures.assertFixture("DECORATOR_DEFINITIONS_COUNT", ObservePersistenceFixtures.DECORATOR_DEFINITIONS_COUNT, provider.definitions().size());
    }

    @Test
    public void decorator() {
        assertEquals(0, provider.decorators().size());
        Decorator actual = provider.decorator(Locale.FRANCE, Sex.class);
        assertNotNull(actual);
        assertEquals(1, provider.decorators().size());
        Decorator actual2 = provider.decorator(Locale.FRANCE, Sex.class);
        assertEquals(actual, actual2);
        assertEquals(1, provider.decorators().size());
        actual2 = provider.decorator(Locale.UK, Sex.class);
        assertNotEquals(actual, actual2);
        assertEquals(2, provider.decorators().size());
    }

    @Test(expected = IllegalStateException.class)
    public void badDecorator() {
        provider.decorator(Locale.FRANCE, Sex.class, "Taiste");
    }

    @Test(expected = IllegalStateException.class)
    public void badDecorator2() {
        provider.decorator(Locale.FRANCE, ReferentialDto.class);
    }

    @Test
    public void testSexDto() {
        DecoratorProvider provider = DecoratorProvider.get();
        Decorator decorator = provider.decorator(Locale.FRANCE, Sex.class);
        assertNotNull(decorator);
        Sex dto = new SexImpl();
        String actual = decorator.decorate(dto);
        System.out.println(actual);
        dto.setCode("code");
        dto.setLabel2("Label FR");
        dto.setLabel1("Label UK");
        actual = decorator.decorate(dto);
        System.out.println("FR:" + actual);
        actual = provider.decorator(Locale.UK, Sex.class).decorate(dto);
        System.out.println("UK:" + actual);
        actual = provider.decorator(new Locale("es", "ES"), Sex.class).decorate(dto);
        System.out.println("ES:" + actual);
    }

    @Test
    public void testGroupBy() {
        businessProject = ObserveBusinessProject.get();
        decoratorService = new DecoratorService(ReferentialLocale.FR);
        businessProject.getRootOpenableDataTypes().forEach(this::testGroupBy);

    }

    private <D extends RootOpenableDto> void testGroupBy(Class<D> dtoType) {
        List<DataGroupByDtoDefinition<D, ?>> definitions = businessProject.getDataGroupByDtoDefinitions(dtoType);
        Assert.assertNotNull("could not find groupBy dto definitions for:" + dtoType.getName(), definitions);
        for (DataGroupByDtoDefinition<D, ?> definition : definitions) {
            log.info(definition.getDefinitionLabel());
            GroupBySpiContext<?, ?, ?, ?, ?, ?, ?> spi = ObservePersistenceBusinessProject.get().getGroupBySpiContextMapping().get(definition.getName());
            Assert.assertNotNull("could not find groupBy entity definitions for:" + dtoType.getName(), spi);

            Decorator decorator = decoratorService.getDecoratorByType(spi.definition().getContainerType());
            Assert.assertNotNull("could not find decorator for entity groupBy definitions :" + dtoType.getName(), decorator);
        }
    }

}
