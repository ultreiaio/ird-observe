package fr.ird.observe.entities;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.request.CreateDatabaseRequest;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 16/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.7
 */
public class DataSourceCreateHelper {

    private static final Logger log = LogManager.getLogger(DataSourceCreateHelper.class);

    public static void createEmpty(TopiaApplicationContext<?> applicationContext) throws BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        log.debug(String.format("Create topia application context: %s", applicationContext));
        // get generated schema from class-path, instead of letting hibernate generating it
        // this script contains data and standalone tables, but not migration schema
        CreateDatabaseRequest request = CreateDatabaseRequest
                .builder(applicationContext.getConfiguration().isPostgresConfiguration(), Version.valueOf(applicationContext.getModelVersion()))
                .addGeneratedSchema()
                .addVersionTable() // add migration service schema
                .build();
        SqlScript dump = applicationContext.getSqlService().consume(request);
        applicationContext.executeSqlStatements(dump);
        // fill the lastUpdateDate table
        //FIXME Make sure this is ok, but not sure
        insertLastUpdateDate(applicationContext);
    }

    public static void createFromDump(TopiaApplicationContext<?> applicationContext, ObserveDataSourceConfiguration dataSourceConfiguration, SqlScript importDatabase) throws BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        log.debug(String.format("Create topia application context: %s", applicationContext));
        log.info("Create new database from a script.");
        if (dataSourceConfiguration.isLocal()) {
            applicationContext.executeSqlStatements(importDatabase);
            applicationContext.getMigrationService().runSchemaMigration();
        } else {
            // base postgres
            // do import in temporary h2 database
            Path temporaryDirectory = dataSourceConfiguration.getTemporaryDirectory();

            Version modelVersion = dataSourceConfiguration.getModelVersion();
            ObserveDataSourceConfigurationTopiaH2 temporaryConfiguration = ObserveDataSourceConfigurationTopiaH2.createTemporaryConfiguration(temporaryDirectory.toFile(), modelVersion);
            try (ObserveTopiaApplicationContext temporaryTopiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(temporaryConfiguration)) {
                temporaryTopiaApplicationContext.executeSqlStatements(importDatabase);
                temporaryTopiaApplicationContext.getMigrationService().runSchemaMigration();
                CreateDatabaseRequest request = CreateDatabaseRequest
                        .builder(true, modelVersion)
                        .addGeneratedSchema()
                        .addVersionTable()
                        .addStandaloneTables()
                        .addAllData()
                        .build();
                SqlScript dump = applicationContext.getSqlService().consume(request);
                applicationContext.executeSqlStatements(dump);
            }
        }
    }

    public static void createAndImport(TopiaApplicationContext<?> applicationContext, SqlScript dump, SqlScript optionalDump) {
        log.debug(String.format("Create topia application context: %s", applicationContext));
        applicationContext.executeSqlStatements(dump);
        if (optionalDump != null) {
            applicationContext.executeSqlStatements(optionalDump);
        }
    }

    private static final String INSERT_LAST_UPDATE_PATTERN = "INSERT INTO common.lastUpdateDate (topiaId, topiaVersion, topiaCreateDate, type, lastUpdateDate) VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.%03d', 0, CURRENT_TIMESTAMP, '%s', CURRENT_TIMESTAMP);";

    public static void insertLastUpdateDate(TopiaApplicationContext<?> applicationContext) {
        try {
            try (TopiaPersistenceContext topiaPersistenceContext = applicationContext.newPersistenceContext()) {
                Set<String> request = new LinkedHashSet<>();
                int index = 0;
                for (Class<? extends Entity> entityType : applicationContext.getModel().getDescriptorsByType().keySet()) {
                    if (LastUpdateDate.class.equals(entityType)) {
                        // this technical table does not support lastUpdateDate
                        continue;
                    }
                    request.add(String.format(INSERT_LAST_UPDATE_PATTERN, index++, entityType.getName()));
                }
                topiaPersistenceContext.executeSqlScript(SqlScript.of(String.join("\n", request)));
                topiaPersistenceContext.commit();
            }
        } catch (TopiaException eee) {
            throw eee;
        } catch (Exception eee) {
            throw new TopiaException(String.format("Could insert lastUpdateDate for reason: %s", eee.getMessage()), eee);
        }
    }
}
