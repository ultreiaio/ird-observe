package fr.ird.observe.entities;

/*
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.TripMapPointType;
import fr.ird.observe.entities.referential.common.Harbour;
import io.ultreia.java4all.util.Dates;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created on 8/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class Entities {


    public static Float getFloat(Object[] resultSet, int columnIndex) {
        Number number = (Number) resultSet[columnIndex];
        return number == null ? null : number.floatValue();
        //FIXME Check this is ok ???
//        return resultSet.wasNull() ? null : result;
    }
    public static long getPrimitiveLong(Object[] resultSet, int columnIndex) {
        Number number = (Number) resultSet[columnIndex];
        return number.longValue();
        //FIXME Check this is ok ???
//        return resultSet.wasNull() ? null : result;
    }

    public static Integer getInteger(Object[] resultSet, int columnIndex) {
        Number number = (Number) resultSet[columnIndex];
        return number == null ? null : number.intValue();
        //FIXME Check this is ok ???
//        return resultSet.wasNull() ? null : result;
    }

    public static Optional<TripMapPoint> createHarbourPoint(Harbour harbour, Date date, TripMapPointType tripMapPointType) {
        TripMapPoint result = null;
        if (harbour != null) {
            result = TripMapPoint.of(tripMapPointType, date, harbour.getLatitude(), harbour.getLongitude());
        }
        return Optional.ofNullable(result);
    }

    public static TripMapPoint createPoint(TripMapPointType type, Date dateTime, Object[] resultSet, int columnIndex) {
        Float latitude = getFloat(resultSet, columnIndex);
        Float longitude = getFloat(resultSet, columnIndex + 1);
        return TripMapPoint.of(type, dateTime, latitude, longitude);
    }

    public static TripMapPoint createPoint(TripMapPointType type, Object[] resultSet, int dateColumnIndex) {
        Date date = (Date) resultSet[dateColumnIndex];
        return createPoint(type, date, resultSet, dateColumnIndex + 1);
    }

    public static String updateLastUpdateDate(String id, String now) {
        return String.format("UPDATE common.LastUpdateDate SET" +
                                     " topiaVersion = topiaVersion + 1," +
                                     " lastUpdateDate = '%s'::timestamp WHERE type = '%s';", now, id);
    }

    public static TripMapPoint addPointWithDateAndOptionalTime(TripMapPointType type, Object[] resultSet, int columnIndex, List<TripMapPoint> points)  {
        Date date = (Date) resultSet[columnIndex];
        Date optionalTime = (Date) resultSet[columnIndex + 1];
        Date dateTime = optionalTime == null ? date : Dates.getDateAndTime(date, optionalTime, true, false);
        TripMapPoint point = createPoint(type, dateTime, resultSet, columnIndex + 2);
        points.add(point);
        return point;
    }

    public static TripMapPoint addPointWithTimestamp(TripMapPointType type, Object[] resultSet, int columnIndex, List<TripMapPoint> points)  {
        TripMapPoint point = createPoint(type, resultSet, columnIndex);
        points.add(point);
        return point;
    }
}
