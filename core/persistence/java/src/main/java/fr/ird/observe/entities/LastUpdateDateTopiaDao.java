package fr.ird.observe.entities;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import java.util.Map;
import java.util.Optional;

public class LastUpdateDateTopiaDao extends AbstractLastUpdateDateTopiaDao<LastUpdateDate> {

    public Optional<LastUpdateDate> getLastUpdateDate(String type) {
        return forTypeEquals(type)
                .setOrderByArguments(LastUpdateDate.PROPERTY_LAST_UPDATE_DATE + " DESC")
                .tryFindFirst();
    }

    public Optional<Date> getLastUpdateDate() {
        return forAll().stream().map(LastUpdateDate::getLastUpdateDate).max(Date::compareTo);
    }

    public void setLastUpdateDate(String type, Date newLastUpdateDate) {
        Optional<LastUpdateDate> optionalLastUpdateDate = getLastUpdateDate(type);
        if (optionalLastUpdateDate.isEmpty()) {
            create(type, newLastUpdateDate);
        } else {
            LastUpdateDate lastUpdateDate = optionalLastUpdateDate.get();
            lastUpdateDate.setLastUpdateDate(newLastUpdateDate);
        }
    }

    protected LastUpdateDate create(String type, Date date) {
        return create(Map.of(LastUpdateDate.PROPERTY_TYPE, type,
                             LastUpdateDate.PROPERTY_TOPIA_CREATE_DATE, date,
                             LastUpdateDate.PROPERTY_LAST_UPDATE_DATE, date
        ));
    }
}
