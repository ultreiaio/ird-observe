package fr.ird.observe.entities;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.referential.common.Vessel;
import fr.ird.observe.spi.migration.ObserveTopiaMigrationServiceAskUserToMigrate;
import io.ultreia.java4all.util.Version;
import org.nuiton.topia.persistence.security.SecurityScriptConfiguration;

/**
 * Created on 12/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveSecurityScriptConfiguration extends SecurityScriptConfiguration {
    @Override
    public String getDataTableSchemaName() {
        return Trip.SPI.getEntitySqlDescriptor().getTable().getSchemaName();
    }

    @Override
    public String getDataTableTableName() {
        return Trip.SPI.getEntitySqlDescriptor().getTable().getTableName();
    }

    @Override
    public String getReferentialTableSchemaName() {
        return Vessel.SPI.getEntitySqlDescriptor().getTable().getSchemaName();
    }

    @Override
    public String getReferentialTableTableName() {
        return Vessel.SPI.getEntitySqlDescriptor().getTable().getTableName();
    }

    @Override
    public Version getMinimumMigrationVersion() {
        return ObserveTopiaMigrationServiceAskUserToMigrate.getMinimumVersion();
    }
}
