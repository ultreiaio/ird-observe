package fr.ird.observe.entities;

/*
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.ReferentialIds;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.referential.OneSideSqlResultBuilder;
import fr.ird.observe.spi.referential.ReferentialIdsBuilder;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.sql.SqlScript;
import org.nuiton.topia.persistence.TagValues;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaIdFactoryForBulkSupport;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContextConstructorParameter;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobModel;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanModel;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanModel;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanModel;
import org.nuiton.topia.service.sql.request.DeleteRequestCallback;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequestCallback;
import org.nuiton.topia.service.sql.request.ReplicateRequestCallback;
import org.nuiton.topia.service.sql.script.TopiaEntitySqlScriptModel;
import org.nuiton.topia.service.sql.usage.TopiaEntitySqlUsageModel;

import java.util.Date;
import java.util.Set;
import java.util.function.Consumer;

public class ObserveTopiaApplicationContext extends AbstractObserveTopiaApplicationContext {

    // package visibility to avoid anyone to create it except the factory
    ObserveTopiaApplicationContext(TopiaConfiguration topiaConfiguration) {
        super(topiaConfiguration);
    }

    public ObserveTopiaPersistenceContext newPersistenceContext(Consumer<ObserveTopiaPersistenceContext> onPreClose) {
        ObserveTopiaPersistenceContext persistenceContext = newPersistenceContext();
        if (onPreClose != null) {
            persistenceContext.setOnPreClose(onPreClose);
        }
        return persistenceContext;
    }

    @Override
    public ObserveTopiaPersistenceContext newPersistenceContext() {
        ObserveTopiaPersistenceContext persistenceContext = new ObserveTopiaPersistenceContext(
                new AbstractTopiaPersistenceContextConstructorParameter(
                        getConfiguration(),
                        getHibernateProvider(),
                        getTopiaIdFactory(),
                        getDaoMapping())) {
            @Override
            public void close() {
                // remove it before closing to avoid re-entrant code
                unregisterPersistenceContext(this);
                super.close();
            }
        };
        registerPersistenceContext(persistenceContext);
        return persistenceContext;
    }

    @Override
    public final OneSideSqlResultBuilder newOneSideSqlResultBuilder(ServiceContext context, Date defaultLastUpdateDate) {
        return ObserveTopiaEntitySqlModelResource.get().newOneSideSqlResultBuilder(context, defaultLastUpdateDate);
    }

    @Override
    public ReferentialIds getReferentialIds(Class<? extends TopiaEntity> dataType, String... ids) {
        return ReferentialIdsBuilder.builder(ObservePersistenceBusinessProject.get(), this, dataType, ids).build(this);
    }

    @Override
    public TopiaIdFactoryForBulkSupport newIdFactoryForBulk(long timestamp) {
        return new ObserveIdFactoryForBulk(timestamp);
    }

    @Override
    public final TagValues getPersistenceTagValues() {
        return ObserveTopiaEntitySqlModelResource.get().getPersistenceTagValues();
    }

    @Override
    public final Set<ReplicatePartialRequestCallback> getReplicatePartialRequestCallbacks() {
        return ObserveTopiaEntitySqlModelResource.get().getReplicatePartialRequestCallbacks();
    }

    @Override
    public Set<ReplicateRequestCallback> getReplicateRequestCallbacks() {
        return ObserveTopiaEntitySqlModelResource.get().getReplicateRequestCallbacks();
    }

    @Override
    public Set<DeleteRequestCallback> getDeleteRequestCallbacks() {
        return ObserveTopiaEntitySqlModelResource.get().getDeleteRequestCallbacks();
    }

    @Override
    public final TagValues getDtoTagValues() {
        return ObserveTopiaEntitySqlModelResource.get().getDtoTagValues();
    }

    @Override
    public final TopiaEntitySqlModel getModel() {
        return ObserveTopiaEntitySqlModelResource.get().getModel();
    }

    @Override
    public TopiaEntitySqlCopyPlanModel getCopyPlanModel() {
        return ObserveTopiaEntitySqlModelResource.get().getCopyPlanModel();
    }

    @Override
    public TopiaEntitySqlReplicatePlanModel getReplicatePlanModel() {
        return ObserveTopiaEntitySqlModelResource.get().getReplicatePlanModel();
    }

    @Override
    public TopiaEntitySqlDeletePlanModel getDeletePlanModel() {
        return ObserveTopiaEntitySqlModelResource.get().getDeletePlanModel();
    }

    @Override
    public TopiaEntitySqlBlobModel getBlobModel() {
        return ObserveTopiaEntitySqlModelResource.get().getBlobModel();
    }

    @Override
    public TopiaEntitySqlUsageModel getUsageModel() {
        return ObserveTopiaEntitySqlModelResource.get().getUsageModel();
    }

    @Override
    public Set<Class<? extends ReferentialDto>> referentialForReplicationDto() {
        return ObserveTopiaEntitySqlModelResource.get().referentialForReplicationDto();
    }

    @Override
    public TopiaEntitySqlScriptModel getScriptModel() {
        return ObserveTopiaEntitySqlModelResource.get().getScriptModel();
    }

    public void createEmpty() throws BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        DataSourceCreateHelper.createEmpty(this);
    }

    public void createFromDump(ObserveDataSourceConfiguration dataSourceConfiguration, SqlScript dump) throws BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        DataSourceCreateHelper.createFromDump(this, dataSourceConfiguration, dump);
    }

    public void createAndImport(SqlScript dump, SqlScript optionalDump) throws BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        DataSourceCreateHelper.createAndImport(this, dump, optionalDump);
    }
}
