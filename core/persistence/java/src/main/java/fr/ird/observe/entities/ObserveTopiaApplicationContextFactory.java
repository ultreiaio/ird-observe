package fr.ird.observe.entities;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import io.ultreia.java4all.util.ServiceLoaders;
import org.nuiton.topia.persistence.TopiaApplicationContextFactory;
import org.nuiton.topia.persistence.TopiaConfiguration;

/**
 * Usine de contexte applicatif ToPIA.
 * <p>
 * Created on 23/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(TopiaApplicationContextFactory.class)
public final class ObserveTopiaApplicationContextFactory extends TopiaApplicationContextFactory<ObserveTopiaPersistenceContext, ObserveTopiaApplicationContext> {

    private static final ObserveTopiaApplicationContextFactory INSTANCE = (ObserveTopiaApplicationContextFactory) ServiceLoaders.loadUniqueService(TopiaApplicationContextFactory.class);

    public static ObserveTopiaApplicationContext createContext(ObserveDataSourceConfiguration configuration) {
        TopiaConfiguration topiaConfiguration = ObserveTopiaConfigurationFactory.create(configuration);
        return INSTANCE.create(topiaConfiguration);
    }

    public static ObserveTopiaApplicationContext getContext(String authenticationToken) {
        return INSTANCE.get(authenticationToken);
    }

    public static void closeContext(String authenticationToken) {
        INSTANCE.close(authenticationToken);
    }

    public static void closeFactory() {
        INSTANCE.close();
    }

    @Override
    public ObserveTopiaApplicationContext create(TopiaConfiguration topiaConfiguration) {
        return new ObserveTopiaApplicationContext(topiaConfiguration) {
            @Override
            protected void init() {
                addToCache(this);
                super.init();
            }

            @Override
            public void close() {
                // remove before closing from cache to be sure nobody else will try to reuse it
                removeFromCache(this);
                super.close();
            }
        };
    }
}
