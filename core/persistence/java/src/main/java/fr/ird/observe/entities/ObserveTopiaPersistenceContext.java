package fr.ird.observe.entities;

/*
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.report.ReportRequestExecutor;
import fr.ird.observe.report.definition.ReportDefinition;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.spi.navigation.parent.ObserveToParentIdProvider;
import fr.ird.observe.spi.navigation.parent.ObserveToParentIdProviderImpl;
import fr.ird.observe.spi.report.DefaultReportRequestExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContextConstructorParameter;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

public class ObserveTopiaPersistenceContext extends AbstractObserveTopiaPersistenceContext {

    private static final Logger log = LogManager.getLogger(ObserveTopiaPersistenceContext.class);

    // package visibility to avoid anyone to create it except the application context
    ObserveTopiaPersistenceContext(AbstractTopiaPersistenceContextConstructorParameter parameter) {
        super(parameter);
    }

    @Override
    public Date getLastUpdateDate(String type) {
        Date lastUpdateDate = getLastUpdateDateDao().getLastUpdateDate(type).map(LastUpdateDate::getLastUpdateDate).orElse(null);
        log.debug(String.format("getLastUpdateDate: %s for entity type: %s", lastUpdateDate, type));
        return lastUpdateDate;
    }

    @Override
    public void setLastUpdateDate(String type, Date date) {
        getLastUpdateDateDao().setLastUpdateDate(type, date);
    }

    @Override
    public ReportRequestExecutor newReportRequestExecutor(ReportDefinition reportDefinition, ReferentialLocale referentialLocale) {
        Class<? extends DataEntity> mainDataType;
        if (fr.ird.observe.spi.module.ps.BusinessModule.get().getName().equalsIgnoreCase(reportDefinition.getModelType())) {
            mainDataType = fr.ird.observe.entities.data.ps.common.Trip.class;
        } else {
            mainDataType = fr.ird.observe.entities.data.ll.common.Trip.class;
        }
        return new DefaultReportRequestExecutor(referentialLocale, this, mainDataType);
    }

    @Override
    public ObserveToParentIdProvider getToParentIdProvider() {
        return (ObserveToParentIdProvider) super.getToParentIdProvider();
    }

    @Override
    protected ObserveToParentIdProvider newToParentIdProvider() {
        return new ObserveToParentIdProviderImpl() {
            @Override
            protected ObserveTopiaPersistenceContext getPersistenceContext() {
                return ObserveTopiaPersistenceContext.this;
            }
        };
    }

    public void updateReferentialLastUpdateDates(String lastUpdateDateKey, Date now) {
        LastUpdateDateTopiaDao dao = getLastUpdateDateDao();
        for (Class<? extends ReferentialDto> dtoType : ObserveBusinessProject.get().getReferentialTypes()) {
            ReferentialDtoEntityContext<? extends ReferentialDto, ReferentialDtoReference, ReferentialEntity, TopiaDao<ReferentialEntity>> spi = PersistenceBusinessProject.fromReferentialDto(dtoType);
            updateLastUpdateDate(dao, spi);
        }
        dao.setLastUpdateDate(lastUpdateDateKey, now);
    }

    private <E extends Entity> void updateLastUpdateDate(LastUpdateDateTopiaDao dao, DtoEntityContext<?, ?, E, ?> spi) {
        Timestamp maxLastUpdateDate = spi.getLastUpdateDate(this);
        if (maxLastUpdateDate == null) {
            // no data in this table, do nothing
            return;
        }
        String type = spi.toEntityType().getName();
        Optional<LastUpdateDate> optionalLastUpdateDate = dao.getLastUpdateDate(type);
        LastUpdateDate lastUpdateDate;
        if (optionalLastUpdateDate.isEmpty()) {
            log.info(String.format("Create LastUpdateDate for %s with value: %s", type, maxLastUpdateDate));
            lastUpdateDate = dao.create(type, maxLastUpdateDate);
        } else {
            lastUpdateDate = optionalLastUpdateDate.get();
        }

        if (lastUpdateDate.getLastUpdateDate().before(maxLastUpdateDate)) {
            log.info(String.format("Update LastUpdateDate for %s with value: %s", type, maxLastUpdateDate));
            lastUpdateDate.setLastUpdateDate(maxLastUpdateDate);
        }
    }
}
