package fr.ird.observe.entities.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referential.common.Vessel;
import fr.ird.observe.spi.context.RootOpenableDtoEntityContext;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created on 13/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public interface TripEntityAware extends RootOpenableEntity, WithEndDateEntityAware {
    Logger log = LogManager.getLogger(TripEntityAware.class);

    static <T extends TripEntityAware, SPI extends RootOpenableDtoEntityContext<?, ?, T, ?, ?>> Consumer<AddEntityToUpdateStep> onSaveUpdateTripIfEndDateChanged(SPI spi, T trip, WithEndDateEntityAware entity, Consumer<AddEntityToUpdateStep> extraConsumer) {
        Objects.requireNonNull(spi);
        Objects.requireNonNull(trip);
        boolean updateTripEndDate = trip.updateEndDate(entity);
        return updateTripEndDate ? spi.andThen(extraConsumer, s -> s.update(spi, trip)) : extraConsumer;
    }

    Vessel getVessel();

    Date getStartDate();

    Date getEndDate();

    void setEndDate(Date endDate);

    default String getVesselCode() {
        return getVessel() == null ? null : getVessel().getCode();
    }

    default boolean updateEndDate(WithEndDateEntityAware entity) {
        Date theoreticalEndDate = entity.getTheoreticalEndDate();
        if (theoreticalEndDate == null) {
            log.info("Skip, no theoreticalEndDate.");
            return false;
        }
        Date realEndDate = getEndDate();
        if (realEndDate == null || theoreticalEndDate.after(realEndDate)) {
            log.info("Do update, replace {} by {}", realEndDate, theoreticalEndDate);
            setEndDate(new java.sql.Date(theoreticalEndDate.getTime()));
            return true;
        }
        log.info("Skip, end date {} vs theoreticalEndDate {}.", realEndDate, theoreticalEndDate);
        return false;
    }
}
