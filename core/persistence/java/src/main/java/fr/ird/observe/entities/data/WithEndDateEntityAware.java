package fr.ird.observe.entities.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Dates;

import java.util.Date;

/**
 * Represents an entity with an end date that we can use to compute end date of the trip.
 * <p>
 * Created at 18/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public interface WithEndDateEntityAware extends DataEntity {

    Date getTheoreticalEndDate();

    class TheoreticalEndDateBuilder {

        private Date date;

        public TheoreticalEndDateBuilder(Date date) {
            addDate(date);
        }

        public TheoreticalEndDateBuilder(Date... dates) {
            for (Date date : dates) {
                addDate(date);
            }
        }

        protected TheoreticalEndDateBuilder() {
        }

        public static Date of(Date... dates) {
            return new TheoreticalEndDateBuilder(dates).build();
        }

        public static Date of(WithEndDateEntityAware date) {
            return date == null ? null : date.getTheoreticalEndDate();
        }

        public TheoreticalEndDateBuilder addDate(WithEndDateEntityAware date) {
            return date == null ? this : addDate(date.getTheoreticalEndDate());
        }

        public TheoreticalEndDateBuilder addDate(Date date) {
            if (date == null) {
                return this;
            }
            date = Dates.getEndOfDay(date);
            if (this.date == null || date.after(this.date)) {

                this.date = date;
            }
            return this;
        }

        public Date build() {
            return date;
        }
    }
}
