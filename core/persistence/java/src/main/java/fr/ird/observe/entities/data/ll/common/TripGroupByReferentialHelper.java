package fr.ird.observe.entities.data.ll.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.dto.data.ll.common.TripReference;
import fr.ird.observe.dto.data.ll.common.TripStatBuilder;
import fr.ird.observe.dto.referential.common.CountryReference;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.dto.referential.ll.common.ObservationMethodReference;
import fr.ird.observe.dto.referential.ll.common.ProgramReference;
import fr.ird.observe.dto.referential.ll.common.TripTypeReference;
import fr.ird.observe.entities.referential.common.Country;
import fr.ird.observe.entities.referential.common.DataQuality;
import fr.ird.observe.entities.referential.common.Harbour;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.Person;
import fr.ird.observe.entities.referential.common.ShipOwner;
import fr.ird.observe.entities.referential.common.Vessel;
import fr.ird.observe.entities.referential.common.VesselSizeCategory;
import fr.ird.observe.entities.referential.common.VesselType;
import fr.ird.observe.entities.referential.ll.common.ObservationMethod;
import fr.ird.observe.entities.referential.ll.common.Program;
import fr.ird.observe.entities.referential.ll.common.TripType;
import fr.ird.observe.spi.context.DataGroupByReferentialHelper;
import fr.ird.observe.spi.context.ReferentialCache;
import fr.ird.observe.spi.context.RelationCountCache;
import fr.ird.observe.spi.service.ServiceContext;
import org.apache.commons.lang3.mutable.MutableInt;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

/**
 * Created on 04/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripGroupByReferentialHelper extends DataGroupByReferentialHelper<TripDto, TripReference, Trip> {

    public final RelationCountCache<Trip> observationActivityCount;
    public final RelationCountCache<Trip> logbookActivityCount;
    public final RelationCountCache<Trip> logbookSampleCount;
    public final RelationCountCache<Trip> logbookActivitySampleCount;
    public final RelationCountCache<Trip> logbookActivitySetCount;
    public final RelationCountCache<Trip> observationsActivitySetCount;
    public final RelationCountCache<Trip> landingCount;
    public final RelationCountCache<Trip> gearUseFeaturesCount;
    private final ReferentialCache<ProgramReference, Program> program;
    private final ReferentialCache<TripTypeReference, TripType> tripType;
    private final ReferentialCache<ObservationMethodReference, ObservationMethod> observationMethod;
    private final ReferentialCache<VesselReference, Vessel> vessel;
    private final ReferentialCache<CountryReference, Country> country;
    private final StatsBuilder statsBuilder;

    public TripGroupByReferentialHelper(ServiceContext context) {
        super(Trip.SPI,
              context,
              List.of(fr.ird.observe.entities.data.ll.observation.Activity.SPI,
                      fr.ird.observe.entities.data.ll.logbook.Activity.SPI,
                      fr.ird.observe.entities.data.ll.logbook.Sample.SPI,
                      fr.ird.observe.entities.data.ll.landing.Landing.SPI,
                      fr.ird.observe.entities.data.ll.common.GearUseFeatures.SPI),
              List.of(Ocean.SPI,
                      Program.SPI,
                      ObservationMethod.SPI,
                      Country.SPI,
                      Harbour.SPI,
                      Person.SPI,
                      Vessel.SPI,
                      VesselSizeCategory.SPI,
                      VesselType.SPI,
                      ShipOwner.SPI,
                      TripType.SPI,
                      DataQuality.SPI));
        this.program = referentialGet(Program.class);
        this.vessel = referentialGet(Vessel.class);
        this.tripType = referentialGet(TripType.class);
        this.country = referentialGet(Country.class);
        this.observationMethod = referentialGet(ObservationMethod.class);

        this.observationActivityCount = relationGet(fr.ird.observe.entities.data.ll.observation.Activity.SPI);
        this.logbookActivityCount = relationGet(fr.ird.observe.entities.data.ll.logbook.Activity.SPI);
        this.logbookSampleCount = relationGet(fr.ird.observe.entities.data.ll.logbook.Sample.SPI);
        this.landingCount = relationGet(fr.ird.observe.entities.data.ll.landing.Landing.SPI);
        this.gearUseFeaturesCount = relationGet(fr.ird.observe.entities.data.ll.common.GearUseFeatures.SPI);
        this.logbookActivitySampleCount = relationGet(fr.ird.observe.entities.data.ll.logbook.Activity.SPI, fr.ird.observe.entities.data.ll.logbook.Sample.SPI);
        this.logbookActivitySetCount = relationGet(fr.ird.observe.entities.data.ll.logbook.Activity.SPI, fr.ird.observe.entities.data.ll.logbook.Set.SPI);
        this.observationsActivitySetCount = relationGet(fr.ird.observe.entities.data.ll.observation.Activity.SPI, fr.ird.observe.entities.data.ll.observation.Set.SPI);
        this.statsBuilder = new StatsBuilder();
    }

    @Override
    protected void addExtraCaches(List<RelationCountCache<Trip>> relations) {
        relations.add(spi.newRelationCountCache(fr.ird.observe.entities.data.ll.logbook.Activity.SPI, fr.ird.observe.entities.data.ll.logbook.Sample.SPI));
        relations.add(spi.newRelationCountCache(fr.ird.observe.entities.data.ll.logbook.Activity.SPI, fr.ird.observe.entities.data.ll.logbook.Set.SPI));
        relations.add(spi.newRelationCountCache(fr.ird.observe.entities.data.ll.observation.Activity.SPI, fr.ird.observe.entities.data.ll.observation.Set.SPI));
    }

    @Override
    public TripReference newReference(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        TripDto dto = newDto(resultSet, rowIndex);
        dto.setTripType(tripType.reference(resultSet, rowIndex));
        dto.setObservationsAvailability(getBoolean(resultSet, rowIndex));
        dto.setLogbookAvailability(getBoolean(resultSet, rowIndex));
        dto.setObservationMethod(observationMethod.reference(resultSet, rowIndex));
        statsBuilder.buildStatistics(dto);
        return TripReference.of(referentialLocale, dto);
    }

    @Override
    public TripDto newDto(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        TripDto dto = super.newDto(resultSet, rowIndex);
        dto.setObservationsProgram(program.reference(resultSet, rowIndex));
        dto.setLogbookProgram(program.reference(resultSet, rowIndex));
        dto.setStartDate(getDate(resultSet, rowIndex));
        dto.setEndDate(getDate(resultSet, rowIndex));
        dto.setVessel(vessel.reference(resultSet, rowIndex));
        return dto;
    }

    @Override
    public Trip newChildren(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        Trip children = super.newChildren(resultSet, rowIndex);
        children.setObservationsProgram(program.entity(resultSet, rowIndex));
        children.setLogbookProgram(program.entity(resultSet, rowIndex));
        children.setStartDate(getDate(resultSet, rowIndex));
        children.setEndDate(getDate(resultSet, rowIndex));
        children.setVessel(vessel.entity(resultSet, rowIndex));
        return children;
    }

    @Override
    public void decorateGroupValueChildren(List<Trip> groupByValues) {
        vessel.decorate(groupByValues, Trip::getVessel, (x, r) -> r.getLabel());
        country.decorate(groupByValues, t -> t.getVessel().getFlagCountry(), (x, r) -> r.getLabel());
        country.decorate(groupByValues, t -> t.getVessel().getFleetCountry(), (x, r) -> r.getLabel());
        super.decorateGroupValueChildren(groupByValues);
        groupByValues.sort(Comparator.comparing(Trip::getStartDate).thenComparing(t -> t.getVessel() == null ? null : t.getVessel().toString()));
    }

    class StatsBuilder implements TripStatBuilder {

        private String id;

        public void buildStatistics(TripDto statistics) {
            this.id = statistics.getId();
            TripStatBuilder.super.buildStatistics(statistics);
        }

        @Override
        public long buildLandingStatValue() {
            return landingCount.count(id);
        }

        @Override
        public long buildGearUseFeaturesStatValue() {
            return gearUseFeaturesCount.count(id);
        }

        @Override
        public long buildActivityObsStatValue() {
            return observationActivityCount.count(id);
        }

        @Override
        public long buildObservationsActivitySetStatValue() {
            return observationsActivitySetCount.count(id);
        }

        @Override
        public long buildActivityLogbookStatValue() {
            return logbookActivityCount.count(id);
        }

        @Override
        public long buildLogbookActivitySetStatValue() {
            return logbookActivitySetCount.count(id);
        }

        @Override
        public long buildLogbookActivitySampleStatValue() {
            return logbookActivitySampleCount.count(id);
        }

        @Override
        public long buildSampleStatValue() {
            return logbookSampleCount.count(id);
        }

    }
}
