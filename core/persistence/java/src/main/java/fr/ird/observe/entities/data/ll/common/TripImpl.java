package fr.ird.observe.entities.data.ll.common;

/*
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;

/**
 * Created on 8/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class TripImpl extends TripAbstract {
    private static final long serialVersionUID = 1L;

    @Override
    public long buildObservationsActivitySetStatValue() {
        return isActivityObsEmpty() ? 0 : getActivityObs().stream().mapToLong(fr.ird.observe.dto.data.ll.observation.ActivityStatBuilder::buildSetStatValue).sum();
    }

    @Override
    public long buildLogbookActivitySetStatValue() {
        return isActivityLogbookEmpty() ? 0 : getActivityLogbook().stream().mapToLong(fr.ird.observe.dto.data.ll.logbook.ActivityStatBuilder::buildSetStatValue).sum();
    }

    @Override
    public long buildLogbookActivitySampleStatValue() {
        return isActivityLogbookEmpty() ? 0 : getActivityLogbook().stream().mapToLong(fr.ird.observe.dto.data.ll.logbook.ActivityStatBuilder::buildSampleStatValue).sum();
    }

    @Override
    public Date getTheoreticalEndDate() {
        TheoreticalEndDateBuilder builder = new TheoreticalEndDateBuilder(getStartDate());
        if (isActivityLogbookNotEmpty()) {
            getActivityLogbook().forEach(builder::addDate);
        }
        if (isActivityObsNotEmpty()) {
            getActivityObs().forEach(builder::addDate);
        }
        return builder.build();
//        Date lastActivityDate = isActivityObsEmpty() ? null : getActivityObs().stream().map(fr.ird.observe.entities.data.ll.observation.Activity::getTimeStamp).max(Date::compareTo).orElse(null);
//        return Dates.getEndOfDay(lastActivityDate == null ? getStartDate() : lastActivityDate);
    }
}
