package fr.ird.observe.entities.data.ll.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.dto.data.ll.common.TripReference;
import fr.ird.observe.dto.data.ll.pairing.ActivityPairingDto;
import fr.ird.observe.dto.data.ll.pairing.TripActivitiesPairingDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ll.common.VesselActivityReference;
import fr.ird.observe.entities.data.ll.logbook.Activity;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.common.SpeciesList;
import fr.ird.observe.entities.referential.ll.common.VesselActivity;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.Dates;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripSpi extends GeneratedTripSpi {

    public String updateTripVersion(TripTopiaDao dao, String id, Timestamp now) {
        return dao.updateTripVersionToSql(id, now);
    }

    public String copyLogbookMetadata(TripTopiaDao dao, Function<String, String> commentFormat, Trip trip, String newId) {
        return dao.copyLogbookMetadataToSql(commentFormat, trip, newId);
    }

    public String copyObservationsMetadata(TripTopiaDao dao, Function<String, String> commentFormat, Trip trip, String newId) {
        return dao.copyObservationsMetadataToSql(commentFormat, trip, newId);
    }

    public List<String> getLogbookCatchSpeciesIds(ServiceContext context, String tripId) {
        TripTopiaDao dao = getDao(context);
        return dao.getLogbookCatchSpeciesIds(tripId);
    }

    @Override
    public Form<TripDto> preCreate(ServiceContext context, Trip preCreated) {
        if (preCreated.getStartDate() == null && preCreated.getEndDate() == null) {
            Date date = Dates.getDay(context.now());
            preCreated.setStartDate(date);
            preCreated.setEndDate(date);
        }
        if (preCreated.getObservationsProgram() != null) {
            //FIXME Should we add some default values?
            preCreated.setObservationsAvailability(true);
        }
        if (preCreated.getLogbookProgram() != null) {
            //FIXME Should we add some default values?
            preCreated.setLogbookAvailability(true);
        }
        return super.preCreate(context, preCreated);
    }

    @Override
    public void loadDtoForValidation(ServiceContext context, Trip entity, TripDto dto) {
        if (dto.getEndDate() == null) {
            Date date = Dates.getEndOfDay(context.now());
            dto.setEndDate(date);
        }
        entity.buildStatistics(dto);
        int observationsFishingOperationCount = 0;
        int logbookFishingOperationCount = 0;
        int associatedActivityCount = 0;
        int associatedFishingOperationCount = 0;
        if (entity.isActivityObsNotEmpty()) {
            for (fr.ird.observe.entities.data.ll.observation.Activity a : entity.getActivityObs()) {
                if (a.getVesselActivity().isAllowSet()) {
                    observationsFishingOperationCount++;
                }
            }
        }
        if (entity.isActivityLogbookNotEmpty()) {
            for (fr.ird.observe.entities.data.ll.logbook.Activity a : entity.getActivityLogbook()) {
                boolean withRelated = a.getRelatedObservedActivity() != null;
                if (withRelated) {
                    associatedActivityCount++;
                }
                if (a.getVesselActivity().isAllowSet()) {
                    logbookFishingOperationCount++;
                    if (withRelated) {
                        associatedFishingOperationCount++;
                    }
                }
            }
        }
        dto.setObservationsFishingOperationCount(observationsFishingOperationCount);
        dto.setLogbookFishingOperationCount(logbookFishingOperationCount);
        dto.setAssociatedActivityCount(associatedActivityCount);
        dto.setAssociatedFishingOperationCount(associatedFishingOperationCount);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Trip entity, TripDto dto) {
        fromDto(context.getReferentialLocale(), entity, dto);
        if (entity.isPersisted()) {
            entity.updateEndDate(entity);
        }
        return null;
    }

    @Override
    public TripGroupByReferentialHelper createReferentialHelper(ServiceContext context) {
        return new TripGroupByReferentialHelper(context);
    }

    public TripMapDto getTripMap(ServiceContext context, TripMapConfigDto config) {
        String tripId = config.getTripId();
        TripTopiaDao dao = getDao(context);
        Trip trip = dao.forTopiaIdEquals(tripId).findUnique();
        LinkedHashSet<TripMapPoint> points = dao.buildMap(config, trip);
        return TripMapDto.of(config.getTripId(), points);
    }

    public int getMatchingTripsVesselWithinDateRange(ServiceContext context, String id, String vesselId, Date startDate, Date endDate) {
        if (vesselId == null || startDate == null || endDate == null) {
            return 0;
        }
        return (int) getDao(context).
                <Trip>stream("From fr.ird.observe.entities.data.ll.common.TripImpl Where vessel.id = :vesselId " +
                                     "And (( :startDate <= endDate And :endDate >= endDate ) " +
                                     " Or ( :endDate <= startDate And :endDate >= startDate )) " +
                                     "Order By startDate, endDate", Map.of(
                "vesselId", vesselId,
                "startDate", startDate,
                "endDate", endDate))
                .filter(t -> !Objects.equals(id, t.getTopiaId()))
                .count();
    }

    public ReferentialDtoReferenceSet<SpeciesReference> getSpeciesByListAndTrip(ServiceContext context, String tripId, String speciesListId) {
        Ocean ocean = null;
        if (tripId != null) {
            Trip trip = loadEntity(context, tripId);
            ocean = trip.getOcean();
        }
        Stream<Species> speciesList = SpeciesList.loadEntity(context, speciesListId).getSpecies().stream();
        if (ocean != null) {
            Ocean finalOcean = ocean;
            speciesList = speciesList.filter(s -> s.containsOcean(finalOcean));
        }
        return Species.toReferenceSet(context.getReferentialLocale(), speciesList, null, null);
    }

    public List<ToolkitIdLabel> getSampleActivityParentCandidate(ServiceContext context, String tripId, String activityId) {
        Trip trip = loadEntity(context, tripId);
        Stream<Activity> stream = trip.getActivityLogbook().stream().filter(e -> e.getSample() == null);
        if (activityId != null) {
            stream = stream.filter(e -> !activityId.equals(e.getTopiaId()));
        }
        return Activity.toLabelList(context.getReferentialLocale().getLocale(), stream);
    }

    public TripActivitiesPairingDto getTripPairingDto(ServiceContext context, String tripId) {
        Map<String, VesselActivityReference> vesselActivities = VesselActivity.toReferenceSet(context, null).toId();
        TripTopiaDao dao = getDao(context);
        List<fr.ird.observe.dto.data.ll.observation.ActivityReference> observationActivities = dao.getPairingObservationActivities(vesselActivities, tripId);
        if (observationActivities.isEmpty()) {
            return null;
        }
        List<fr.ird.observe.dto.data.ll.logbook.ActivityReference> logbookActivities = dao.getPairingLogbookActivities(vesselActivities, tripId, observationActivities);
        if (logbookActivities.isEmpty()) {
            return null;
        }
        TripReference tripReference = toReference(context, tripId, null);
        return new TripActivitiesPairingDto(tripReference, logbookActivities, observationActivities);
    }

    public ActivityPairingDto getActivityPairingDto(ServiceContext context, String tripId) {
        Map<String, VesselActivityReference> vesselActivities = VesselActivity.toReferenceSet(context, null).toId();
        List<fr.ird.observe.dto.data.ll.observation.ActivityReference> observationActivities = getDao(context).getPairingObservationActivities(vesselActivities, tripId);
        if (observationActivities.isEmpty()) {
            return null;
        }
        return new ActivityPairingDto(observationActivities);
    }
}
