package fr.ird.observe.entities.data.ll.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.TripMapPointType;
import fr.ird.observe.dto.referential.ll.common.VesselActivityReference;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.referential.common.DataQuality;
import fr.ird.observe.entities.referential.common.Person;
import fr.ird.observe.entities.referential.ll.common.ObservationMethod;
import fr.ird.observe.entities.referential.ll.common.Program;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.topia.persistence.support.QuerySupport.toComment;
import static org.nuiton.topia.persistence.support.QuerySupport.toId;

/**
 * Created at 14/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class TripTopiaDao extends AbstractTripTopiaDao<Trip> {
    private static final Logger log = LogManager.getLogger(TripTopiaDao.class);


    public List<TripMapPoint> getObservationActivityPoint(String tripId) {
        return findMultipleResultBySqlQuery(queryObservationActivityPoint(tripId), (resultSet, points) -> {
            if (resultSet[4] == null) {
                String vesselActivityId = (String) resultSet[3];
                TripMapPointType pointType;
                if (ProtectedIdsLl.LL_COMMON_ACTIVITY_HARBOUR_ID.equals(vesselActivityId)) {
                    pointType = TripMapPointType.llActivityObsInHarbour;
                } else {
                    pointType = TripMapPointType.llActivityObs;
                }
                TripMapPoint point = Entities.addPointWithTimestamp(pointType, resultSet, 0, points);
                log.debug(String.format("[LL-Observation]  Added simple activity point: %s", point));
                return;
            }

            TripMapPoint activityPoint = Entities.addPointWithTimestamp(TripMapPointType.llActivityObs, resultSet, 0, points);
            log.debug(String.format("[LL-Logbook] Added activity point: %s", activityPoint));

            TripMapPoint settingStart = Entities.addPointWithTimestamp(TripMapPointType.llActivityObsWithSettingStart, resultSet, 5, points);
            log.debug(String.format("[LL-Observation]  Added set settingStart point: %s", settingStart));
            if (settingStart.isValid()) {
                points.remove(activityPoint);
            }
            TripMapPoint settingEnd = Entities.addPointWithTimestamp(TripMapPointType.llActivityObsWithSettingEnd, resultSet, 8, points);
            log.debug(String.format("[LL-Observation]  Added set settingEnd point: %s", settingEnd));

            TripMapPoint haulingStart = Entities.addPointWithTimestamp(TripMapPointType.llActivityObsWithHaulingStart, resultSet, 11, points);
            log.debug(String.format("[LL-Observation]  Added set haulingStart point: %s", haulingStart));

            TripMapPoint haulingEnd = Entities.addPointWithTimestamp(TripMapPointType.llActivityObsWithHaulingEnd, resultSet, 14, points);
            log.debug(String.format("[LL-Observation]  Added set haulingEnd point: %s", haulingEnd));
        });

    }

    public List<TripMapPoint> getLogbookActivityPoint(String tripId) {
        return findMultipleResultBySqlQuery(queryLogbookActivityPoint(tripId), (resultSet, points) -> {
            if (resultSet[4] == null) {
                String vesselActivityId = (String) resultSet[3];
                TripMapPointType pointType;
                if (ProtectedIdsLl.LL_COMMON_ACTIVITY_HARBOUR_ID.equals(vesselActivityId)) {
                    pointType = TripMapPointType.llActivityLogbookInHarbour;
                } else {
                    pointType = TripMapPointType.llActivityLogbook;
                }
                TripMapPoint point = Entities.addPointWithTimestamp(pointType, resultSet, 0, points);
                log.debug(String.format("[LL-Logbook] Added simple activity point: %s", point));
                return;
            }

            TripMapPoint activityPoint = Entities.addPointWithTimestamp(TripMapPointType.llActivityLogbook, resultSet, 0, points);
            log.debug(String.format("[LL-Logbook] Added activity point: %s", activityPoint));

            TripMapPoint settingStart = Entities.addPointWithTimestamp(TripMapPointType.llActivityLogbookWithSettingStart, resultSet, 5, points);
            log.debug(String.format("[LL-Logbook] Added set settingStart point: %s", settingStart));
            if (settingStart.isValid()) {
                points.remove(activityPoint);
            }
            TripMapPoint settingEnd = Entities.addPointWithTimestamp(TripMapPointType.llActivityLogbookWithSettingEnd, resultSet, 8, points);
            log.debug(String.format("[LL-Logbook] Added set settingEnd point: %s", settingEnd));

            TripMapPoint haulingStart = Entities.addPointWithTimestamp(TripMapPointType.llActivityLogbookWithHaulingStart, resultSet, 11, points);
            log.debug(String.format("[LL-Logbook] Added set haulingStart point: %s", haulingStart));

            TripMapPoint haulingEnd = Entities.addPointWithTimestamp(TripMapPointType.llActivityLogbookWithHaulingEnd, resultSet, 14, points);
            log.debug(String.format("[LL-Logbook] Added set haulingEnd point: %s", haulingEnd));
        });
    }

    public LinkedHashSet<TripMapPoint> buildMap(TripMapConfigDto tripMapConfig, Trip trip) {
        String tripId = tripMapConfig.getTripId();
        boolean addObservation = tripMapConfig.isAddObservations();
        boolean addLogbook = tripMapConfig.isAddLogbook();

        LinkedHashSet<TripMapPoint> tripMapPoints = new LinkedHashSet<>();

        log.info(String.format("[LL] Loading points for %s", tripMapConfig));

        // add departure harbour
        Entities.createHarbourPoint(trip.getDepartureHarbour(), trip.getStartDate(), TripMapPointType.llTripDepartureHarbour).ifPresent(e -> {
            tripMapPoints.add(e);
            log.debug(String.format("[LL] Added departure harbour point: %s", e));
        });

        if (addObservation) {
            // add Observations activities
            tripMapPoints.addAll(getObservationActivityPoint(tripId));
        }
        if (addLogbook) {
            // add Logbook activities
            tripMapPoints.addAll(getLogbookActivityPoint(tripId));
        }

        // add landing harbour
        Entities.createHarbourPoint(trip.getLandingHarbour(), trip.getEndDate(), TripMapPointType.llTripLandingHarbour).ifPresent(e -> {
            tripMapPoints.add(e);
            log.debug(String.format("[LL] Added landing harbour point: %s", e));
        });
        log.info(String.format("[LL] Loaded %d point(s) for trip: %s...", tripMapPoints.size(), tripId));
        return tripMapPoints;
    }

    public String updateTripVersionToSql(String tripId, Date now) {
        return fillSql(queryUpdateVersion(new Timestamp(now.getTime()), tripId));
    }

    public String copyLogbookMetadataToSql(Function<String, String> commentFormat, Trip trip, String newId) {
        Program logbookProgram = trip.getLogbookProgram();
        Person logbookDataEntryOperator = trip.getLogbookDataEntryOperator();
        DataQuality logbookDataQuality = trip.getLogbookDataQuality();
        String logbookComment = trip.getLogbookComment();
        return fillSql(queryCopyLogbookMetadata(toId(logbookProgram),
                                                toId(logbookDataEntryOperator),
                                                toId(logbookDataQuality),
                                                toComment(commentFormat, logbookComment),
                                                newId));
    }

    public String copyObservationsMetadataToSql(Function<String, String> commentFormat, Trip trip, String newId) {
        Program observationsProgram = trip.getObservationsProgram();
        Person observationsDataEntryOperator = trip.getObservationsDataEntryOperator();
        DataQuality observationsDataQuality = trip.getObservationsDataQuality();
        String observationsComment = trip.getObservationsComment();
        ObservationMethod observationMethod = trip.getObservationMethod();
        Person observer = trip.getObserver();
        return fillSql(queryCopyObservationsMetadata(toId(observationsProgram),
                                                     toId(observationsDataEntryOperator),
                                                     toId(observationsDataQuality),
                                                     toId(observationMethod),
                                                     toId(observer),
                                                     toComment(commentFormat, observationsComment),
                                                     newId));
    }

    public List<String> getLogbookCatchSpeciesIds(String tripId) {
        return findMultipleResultBySqlQuery(queryGetLogbookCatchSpeciesIds(tripId));
    }

    public List<fr.ird.observe.dto.data.ll.logbook.ActivityReference> getPairingLogbookActivities(Map<String, VesselActivityReference> vesselActivities, String tripId, List<fr.ird.observe.dto.data.ll.observation.ActivityReference> observationActivities) {
        Map<String, fr.ird.observe.dto.data.ll.observation.ActivityReference> observationActivitiesById = observationActivities.stream().collect(Collectors.toMap(fr.ird.observe.dto.data.ll.observation.ActivityReference::getId, Function.identity()));

        return findMultipleResultBySqlQuery(queryGetPairingLogbookActivities(tripId), row -> {
            fr.ird.observe.dto.data.ll.logbook.ActivityReference value = new fr.ird.observe.dto.data.ll.logbook.ActivityReference();
            int index = -1;
            value.setId((String) row[++index]);
            value.setLastUpdateDate((java.util.Date) row[++index]);
            value.setTopiaVersion(Entities.getPrimitiveLong(row, ++index));
            value.setTopiaCreateDate((java.util.Date) row[++index]);
            value.setStartTimeStamp((java.util.Date) row[++index]);
            value.setEndTimeStamp((java.util.Date) row[++index]);
            value.setLatitude(Entities.getFloat(row, ++index));
            value.setLongitude(Entities.getFloat(row, ++index));
            String vesselActivityId = (String) row[++index];
            value.setVesselActivity(vesselActivityId == null ? null : vesselActivities.get(vesselActivityId));
            String relatedObservedActivityId = (String) row[++index];
            fr.ird.observe.dto.data.ll.observation.ActivityReference relatedObservedActivity = null;
            if (relatedObservedActivityId != null) {
                relatedObservedActivity = observationActivitiesById.get(relatedObservedActivityId);
            }
            value.setRelatedObservedActivity(relatedObservedActivity);
            return value;
        });
    }

    public List<fr.ird.observe.dto.data.ll.observation.ActivityReference> getPairingObservationActivities(Map<String, VesselActivityReference> vesselActivities, String tripId) {
        return findMultipleResultBySqlQuery(queryGetPairingObservationActivities(tripId), row -> {
            fr.ird.observe.dto.data.ll.observation.ActivityReference value = new fr.ird.observe.dto.data.ll.observation.ActivityReference();
            int index = -1;
            value.setId((String) row[++index]);
            value.setLastUpdateDate((java.util.Date) row[++index]);
            value.setTopiaVersion(Entities.getPrimitiveLong(row, ++index));
            value.setTopiaCreateDate((java.util.Date) row[++index]);
            value.setTimeStamp((java.util.Date) row[++index]);
            value.setLatitude(Entities.getFloat(row, ++index));
            value.setLongitude(Entities.getFloat(row, ++index));
            String vesselActivityId = (String) row[++index];
            value.setVesselActivity(vesselActivityId == null ? null : vesselActivities.get(vesselActivityId));
            return value;
        });
    }
}
