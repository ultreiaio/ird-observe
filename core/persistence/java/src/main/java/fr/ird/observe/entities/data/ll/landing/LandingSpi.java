package fr.ird.observe.entities.data.ll.landing;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.landing.LandingDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.entities.data.ll.common.Trip;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.function.Consumer;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class LandingSpi extends GeneratedLandingSpi {

    @Override
    public Form<LandingDto> preCreate(ServiceContext context, Trip parent, Landing preCreated) {
        preCreated.setStartDate(parent.getEndDate());
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Trip parent, Landing entity, LandingDto dto, boolean needCopy) {
        Consumer<AddEntityToUpdateStep> result = super.onSave(context, parent, entity, dto, needCopy);
        LandingPart.SPI.initId(context, entity.getLandingPart());
        return result;
    }
}
