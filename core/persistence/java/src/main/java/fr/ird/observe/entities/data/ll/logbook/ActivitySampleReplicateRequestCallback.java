package fr.ird.observe.entities.data.ll.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.datasource.request.ReplicateRequest;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.entities.data.ll.common.Trip;
import fr.ird.observe.entities.data.ll.common.TripTopiaDao;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.request.ReplicateRequestCallback;

import java.sql.Timestamp;
import java.util.Map;

/**
 * Replicate call back for Activity Sample to a Trip.
 * <p>
 * Created on 15/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@AutoService(ReplicateRequestCallback.class)
public class ActivitySampleReplicateRequestCallback implements ReplicateRequestCallback {

    @Override
    public boolean accept(ReplicateRequest request) {
        return !request.getOldParentId().contains(".Trip#")
                && request.getNewParentId().contains(".Trip#")
                && request.getDataType().equals(Sample.class.getName());
    }

    @Override
    public void consume(ReplicateRequest request, Map<String, String> replaceIds, SqlRequestSetConsumerContext context) {
        SqlScriptWriter writer = context.getWriter();
        AbstractTopiaPersistenceContext persistenceContext = context.getSourcePersistenceContext();
        TripTopiaDao dao = Trip.getDao(persistenceContext);
        boolean newTripModified = false;
        String oldId = request.getOldParentId();
        String oldTripId = Activity.SPI.getParentId((ToolkitTopiaPersistenceContextSupport) persistenceContext, oldId).getId();
        Trip oldTrip = dao.forTopiaIdEquals(oldTripId).findUnique();
        String newId = request.getNewParentId();
        Trip newTrip = dao.forTopiaIdEquals(newId).findUnique();
        if (!newTrip.isLogbookAvailability()) {
            newTripModified = true;
            writer.writeSql(Trip.SPI.copyLogbookMetadata(dao, request.commentFormat(), oldTrip, newId));
        }
        Timestamp now = context.nowTimestamp();
        writer.writeSql(Trip.SPI.updateTripVersion(dao, oldId, now));
        if (newTripModified) {
            writer.writeSql(Trip.SPI.updateTripVersion(dao, newId, now));
        }
    }
}
