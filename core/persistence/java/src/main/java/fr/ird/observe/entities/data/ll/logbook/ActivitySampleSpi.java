package fr.ird.observe.entities.data.ll.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.logbook.ActivitySampleDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.function.Consumer;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ActivitySampleSpi extends GeneratedActivitySampleSpi {

    @Override
    public Form<ActivitySampleDto> preCreate(ServiceContext context, Activity parent, Sample preCreated) {
        preCreated.setTimeStamp(parent.getStartTimeStamp());
        preCreated.setLatitude(parent.getLatitude());
        preCreated.setLongitude(parent.getLongitude());
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Activity parent, Sample entity, ActivitySampleDto dto) {
        entity.setActivity(parent);
        Consumer<AddEntityToUpdateStep> result = andThen(super.onSave(context, parent, entity, dto), s -> s.updateLastUpdateDateField(Activity.SPI, parent));
        SamplePart.SPI.initId(context, entity.getSamplePart());
        return result;
    }
}
