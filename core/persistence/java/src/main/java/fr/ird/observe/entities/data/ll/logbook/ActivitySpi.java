package fr.ird.observe.entities.data.ll.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.ll.logbook.ActivityDto;
import fr.ird.observe.dto.data.ll.logbook.ActivityReference;
import fr.ird.observe.dto.data.ll.logbook.ActivitySampleDto;
import fr.ird.observe.dto.data.ll.logbook.ActivitySampleReference;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.TripEntityAware;
import fr.ird.observe.entities.data.ll.common.Trip;
import fr.ird.observe.entities.referential.common.DataQuality;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.Dates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ActivitySpi extends GeneratedActivitySpi {
    private static final Logger log = LogManager.getLogger(ActivitySpi.class);

    @Override
    public void toDto(ReferentialLocale referentialLocale, Activity entity, ActivityDto dto) {
        super.toDto(referentialLocale, entity, dto);
        Set set = entity.getSet();
        Sample sample = entity.getSample();
        if (sample == null) {
            dto.setActivitySample(null);
        } else {
            ActivitySampleDto activitySampleDto = Sample.ACTIVITY_SAMPLE_SPI.toDto(referentialLocale, sample);
            dto.setActivitySample(ActivitySampleReference.of(referentialLocale, activitySampleDto));
        }
        if (set != null && set.isCatchesNotEmpty()) {
            dto.setCatchSpeciesIds(set.getCatches().stream().map(Catch::getSpecies).distinct().map(Species::getTopiaId).collect(Collectors.toSet()));
        }
    }

    @Override
    public ActivityReference toReference(ReferentialLocale referentialLocale, Activity entity, String classifier) {
        ActivityReference reference = super.toReference(referentialLocale, entity, classifier);
        if (entity != null && DecoratorService.WITH_STATS_CLASSIFIER.equals(classifier) && reference.getSet() != null) {
            entity.getSet().buildStatistics(reference.getSet());
        }
        return reference;
    }

    @Override
    public Form<ActivityDto> preCreate(ServiceContext context, Trip parent, Activity preCreated) {
        Activity lastActivity = Iterables.getLast(parent.getActivityLogbook(), null);
        Date timestamp;
        Calendar calendar = Calendar.getInstance();
        if (lastActivity == null) {
            // première activité, on utilise la date de début de marée (voir http://forge.codelutin.com/issues/6777)
            calendar.setTime(parent.getStartDate());
        } else {
            // on reprend la date +1 de la dernière activité
            calendar.setTime(lastActivity.getStartTimeStamp());
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }
        timestamp = Dates.getDay(calendar.getTime());
        preCreated.setStartTimeStamp(timestamp);
        DataQuality defaultDataQuality = DataQuality.loadEntity(context, ProtectedIdsLl.LL_LOGBOOK_ACTIVITY_DEFAULT_DATA_QUALITY_ID);
        preCreated.setDataQuality(defaultDataQuality);
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Trip parent, Activity entity, ActivityDto dto, boolean needCopy) {
        if (!dto.isSetEnabled() && entity.getSet() != null) {
            log.info("Remove set: {}", entity.getSet().getTopiaId());
            entity.setSet(null);
        }
        return TripEntityAware.onSaveUpdateTripIfEndDateChanged(Trip.SPI, parent, entity, super.onSave(context, parent, entity, dto, needCopy));
    }

    //FIXME Add a callback in MoveRequest
//    @Override
//    public void moveCallback(Trip oldParent, Trip newParent, List<Activity> moved) {
//        TripSpi.copyTripLogbookMetaData(oldParent, newParent);

//    }

    public void applyPairing(ObserveTopiaPersistenceContext context, Timestamp now, Map<String, String> activityMapping) {
        ActivityTopiaDao dao = getDao(context);
        for (Map.Entry<String, String> entry : activityMapping.entrySet()) {
            dao.applyActivityPairing(now, entry.getValue(), entry.getKey());
        }
        updateLastUpdateDateTable(context, now);
    }
}
