package fr.ird.observe.entities.data.ll.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.service.ServiceContext;

public class SetGlobalCompositionSpi extends GeneratedSetGlobalCompositionSpi {
    @Override
    protected SaveResultDto doSave(ServiceContext context, Set entity) {
        prepareSave(context, entity);
        return super.doSave(context, entity);
    }

    public void prepareSave(ServiceContext context, Set entity) {
        BaitsComposition.SPI.initId(context, entity.getBaitsComposition());
        BranchlinesComposition.SPI.initId(context, entity.getBranchlinesComposition());
        HooksComposition.SPI.initId(context, entity.getHooksComposition());
        FloatlinesComposition.SPI.initId(context, entity.getFloatlinesComposition());
    }
}
