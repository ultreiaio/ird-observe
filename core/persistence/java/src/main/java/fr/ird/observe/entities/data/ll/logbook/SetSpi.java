package fr.ird.observe.entities.data.ll.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ProtectedIdsLl;
import fr.ird.observe.dto.data.ll.logbook.ActivityReference;
import fr.ird.observe.dto.data.ll.logbook.SetDto;
import fr.ird.observe.dto.data.ll.logbook.SetGlobalCompositionDto;
import fr.ird.observe.dto.data.ll.logbook.SetStubDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.TripEntityAware;
import fr.ird.observe.entities.data.ll.common.Trip;
import fr.ird.observe.entities.referential.ll.common.SettingShape;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SetSpi extends GeneratedSetSpi {
    @Override
    public Form<SetDto> preCreate(ServiceContext context, Activity parent, Set preCreated) {
        // on utilise la date - heure de l'activité pour initialiser les horodatages de l'opération de pêche
        Date timeStamp = parent.getStartTimeStamp();
        preCreated.setSettingStartTimeStamp(timeStamp);
        preCreated.setSettingEndTimeStamp(DateUtils.addHours(timeStamp, 1));
        preCreated.setHaulingStartTimeStamp(DateUtils.addHours(timeStamp, 2));
        preCreated.setHaulingEndTimeStamp(DateUtils.addHours(timeStamp, 3));
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2312
        SettingShape settingShape = SettingShape.loadEntity(context, ProtectedIdsLl.LL_LOGBOOK_ACTIVITY_DEFAULT_SETTING_SHAPE_ID);
        preCreated.setSettingShape(settingShape);

        // on reporte la position de l'activité pour la position de début de filage
        Float latitude = parent.getLatitude();
        Float longitude = parent.getLongitude();
        if (latitude != null && longitude != null) {
            preCreated.setSettingStartLatitude(latitude);
            preCreated.setSettingStartLongitude(longitude);
        }
        // by default 1 section count
        preCreated.setTotalSectionsCount(1);
        // by default (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2685)
        preCreated.setLightsticksUsed(false);
        // by default (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2685)
        preCreated.setMonitored(false);
        //FIXME super.preCreate does already this?
//        Form<SetDto> form = super.preCreate(context, parent, preCreated);
//        form.getObject().setOtherSets(getBrothers(context, parent));
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Activity parent, Set entity, SetDto dto) {
        Trip trip = Activity.SPI.getParent(context, parent.getId());
        return TripEntityAware.onSaveUpdateTripIfEndDateChanged(Trip.SPI, trip, entity, super.onSave(context, parent, entity, dto));
    }

    @Override
    public void loadDtoForValidation(ServiceContext context, Activity parent, Set entity, SetDto dto) {
        dto.setOtherSets(getBrothers(context, parent));
    }

    public void loadDtoForValidation(ServiceContext context, Trip trip, Activity parent, SetDto dto) {
        dto.setOtherSets(getBrothers(context.getReferentialLocale(), parent, trip));
    }

    public java.util.Set<SetStubDto> getBrothers(ServiceContext context, Activity parent) {
        Trip trip = Activity.SPI.getParent(context, parent.getTopiaId());
        return getBrothers(context.getReferentialLocale(), parent, trip);
    }

    private java.util.Set<SetStubDto> getBrothers(ReferentialLocale referentialLocale, Activity parent, Trip trip) {
        java.util.Set<SetStubDto> result = new HashSet<>();
        trip.getActivityLogbook().stream().filter(a -> a.getVesselActivity().isAllowSet() && !Objects.equals(a, parent) && a.getSet() != null).forEach(
                oneParent -> {
                    Set otherSet = oneParent.getSet();

                    SetStubDto otherSetDto = Set.SET_STUB_SPI.toDto(referentialLocale, otherSet);
                    ActivityReference reference = Activity.toReference(referentialLocale, oneParent);
                    otherSetDto.setActivity(reference);
                    result.add(otherSetDto);
                }
        );
        return result;
    }

    public SaveResultDto saveAndCopyProperties(ServiceContext context, String activityId, String setToCopyId, SetDto dto) throws ConcurrentModificationException {

        Set entityToCopy = loadEntity(context, setToCopyId);
        Set entity = loadOrCreateEntityFromDto(context, dto);
        Activity parent = Activity.SPI.loadEntity(context, activityId);
        checkLastUpdateDate(context, entity, dto);

        SetGlobalCompositionDto globalCompositionToCopy = Set.SET_GLOBAL_COMPOSITION_SPI.loadForm(context, setToCopyId).getObject();

        ReferentialLocale referentialLocale = context.getReferentialLocale();
        globalCompositionToCopy.removeIds();
        Set.SET_GLOBAL_COMPOSITION_SPI.fromDto(referentialLocale, entity, globalCompositionToCopy);
        fromDto(referentialLocale, entity, dto);

        // characteristics tab

        entity.setSettingVesselSpeed(entityToCopy.getSettingVesselSpeed());
        entity.setTimeBetweenHooks(entityToCopy.getTimeBetweenHooks());
        entity.setTotalLineLength(entityToCopy.getTotalLineLength());
        entity.setBasketLineLength(entityToCopy.getBasketLineLength());
        entity.setLengthBetweenBranchlines(entityToCopy.getLengthBetweenBranchlines());
        entity.setShooterUsed(entityToCopy.getShooterUsed());
        entity.setShooterSpeed(entityToCopy.getShooterSpeed());
        entity.setMaxDepthTargeted(entityToCopy.getMaxDepthTargeted());
        entity.setTotalSectionsCount(entityToCopy.getTotalSectionsCount());
        entity.setBasketsPerSectionCount(entityToCopy.getBasketsPerSectionCount());
        entity.setTotalBasketsCount(entityToCopy.getTotalBasketsCount());
        entity.setBranchlinesPerBasketCount(entityToCopy.getBranchlinesPerBasketCount());
        entity.setTotalHooksCount(entityToCopy.getTotalHooksCount());
        entity.setLineType(entityToCopy.getLineType());
        entity.setWeightedSwivel(entityToCopy.getWeightedSwivel());
        entity.setSwivelWeight(entityToCopy.getSwivelWeight());
        entity.setWeightedSnap(entityToCopy.getWeightedSnap());
        entity.setSnapWeight(entityToCopy.getSnapWeight());
        entity.setMonitored(entityToCopy.getMonitored());

        // lightsticks tab

        entity.setLightsticksUsed(entityToCopy.getLightsticksUsed());
        entity.setLightsticksPerBasketCount(entityToCopy.getLightsticksPerBasketCount());
        entity.setTotalLightsticksCount(entityToCopy.getTotalLightsticksCount());
        entity.setLightsticksType(entityToCopy.getLightsticksType());
        entity.setLightsticksColor(entityToCopy.getLightsticksColor());

        Set.SET_GLOBAL_COMPOSITION_SPI.prepareSave(context, entity);

        if (dto.isNotPersisted()) {
            setNotPersistedEntity(parent, entity);
        }
        return saveEntity(context, entity, null);
    }
}
