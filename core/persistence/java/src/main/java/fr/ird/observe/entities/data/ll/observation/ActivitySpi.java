package fr.ird.observe.entities.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ll.observation.ActivityDto;
import fr.ird.observe.dto.data.ll.observation.ActivityReference;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.TripEntityAware;
import fr.ird.observe.entities.data.ll.common.Trip;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.function.Consumer;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ActivitySpi extends GeneratedActivitySpi {
    private static final Logger log = LogManager.getLogger(ActivitySpi.class);

    @Override
    public ActivityReference toReference(ReferentialLocale referentialLocale, Activity entity, String classifier) {
        ActivityReference reference = super.toReference(referentialLocale, entity, classifier);
        if (entity != null && DecoratorService.WITH_STATS_CLASSIFIER.equals(classifier) && reference.getSet() != null) {
            entity.getSet().buildStatistics(reference.getSet());
        }
        return reference;
    }

    @Override
    public Form<ActivityDto> preCreate(ServiceContext context, Trip parent, Activity preCreated) {
        Activity lastActivity = Iterables.getLast(parent.getActivityObs(), null);
        Date timestamp;
        if (lastActivity == null) {
            // première activité, on utilise la date de début de marée (voir http://forge.codelutin.com/issues/6777)
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(parent.getStartDate());
            timestamp = calendar.getTime();
        } else {
            // on reprend la date et l'heure de la dernière activité
            timestamp = lastActivity.getTimeStamp();
        }
        preCreated.setTimeStamp(timestamp);
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Trip parent, Activity entity, ActivityDto dto, boolean needCopy) {
        if (!dto.isSetEnabled() && entity.getSet() != null) {
            log.info("Remove set: {}", entity.getSet().getTopiaId());
            entity.setSet(null);
        }
        return TripEntityAware.onSaveUpdateTripIfEndDateChanged(Trip.SPI, parent, entity, super.onSave(context, parent, entity, dto, needCopy));
    }

//FIXME Add a callback in MoveRequest
//    @Override
//    public void moveCallback(Trip oldParent, Trip newParent, List<Activity> moved) {
//        TripSpi.copyTripObservationMetaData(Objects.requireNonNull(oldParent), newParent);
//    }
}
