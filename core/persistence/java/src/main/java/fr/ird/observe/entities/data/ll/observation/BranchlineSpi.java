package fr.ird.observe.entities.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ToolkitParentIdDtoBean;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.service.ServiceContext;

public class BranchlineSpi extends GeneratedBranchlineSpi {

    public SaveResultDto save(ServiceContext context, BranchlineDto dto) throws ConcurrentModificationException {
        Set parent = getParent(context, dto);
        Branchline entity = Branchline.loadOrCreateEntityFromDto(context, dto);
        checkLastUpdateDate(context, entity, dto);
        Branchline.fromDto(context.getReferentialLocale(), entity, dto);
        return newSaveHelper(context)
//                .update(this, entity, Entity::setLastUpdateDate, true)
                .update(this, entity)
                .updateLastUpdateDateField(Set.SPI, parent)
                .build(entity);
    }

    protected Set getParent(ServiceContext context, BranchlineDto dto) {
        ToolkitParentIdDtoBean basketId = getParentId(context, dto.getTopiaId());
        ToolkitParentIdDtoBean sectionId = Basket.SPI.getParentId(context, basketId.getTopiaId());
        ToolkitParentIdDtoBean setId = Section.SPI.getParentId(context, sectionId.getTopiaId());
        return Set.loadEntity(context, setId.getTopiaId());
    }
}
