package fr.ird.observe.entities.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.observation.SetCatchDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;

/**
 * Created on 04/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SetCatchSpi extends GeneratedSetCatchSpi {

    @Override
    public void toDto(ReferentialLocale referentialLocale, Set entity, SetCatchDto dto) {
        super.toDto(referentialLocale, entity, dto);
        // Do not copy set.comment (there is no editor in form for this)
        dto.setComment(null);
        SetSpi.loadDtoReferenceParents(entity, dto, dto.getCatches(), referentialLocale);
    }

    @Override
    public void saveCallback(AddEntityToUpdateStep saveHelper, Set entity) {
        super.saveCallback(saveHelper, entity);
        entity.getCatches().forEach(c -> {
            saveHelper.updateCollection(SizeMeasure.SPI, c.getSizeMeasure(), false);
            saveHelper.updateCollection(WeightMeasure.SPI, c.getWeightMeasure(), false);
        });
        saveHelper.updateLastUpdateDateTable(SizeMeasure.SPI);
        saveHelper.updateLastUpdateDateTable(WeightMeasure.SPI);
    }
}
