package fr.ird.observe.entities.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.observation.BasketDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.data.ll.observation.SectionDto;
import fr.ird.observe.dto.data.ll.observation.SetDetailCompositionDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.service.ServiceContext;

/**
 * Created on 11/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SetDetailCompositionSpi extends GeneratedSetDetailCompositionSpi {

    @Override
    public void toDto(ReferentialLocale referentialLocale, Set entity, SetDetailCompositionDto dto) {
        super.toDto(referentialLocale, entity, dto);
        // Do not copy set.comment (there is no editor in form for this)
        dto.setComment(null);
    }

    @Override
    public Form<SetDetailCompositionDto> entityToForm(ServiceContext context, Set entity) {
        Form<SetDetailCompositionDto> form = super.entityToForm(context, entity);
        SetDetailCompositionDto dto = form.getObject();
        SetTopiaDao dao = Set.getDao(context);
        //FIXME Use a real sql query to get this in one time
        java.util.Set<String> sectionUsed = dao.getSectionUsed(entity.getTopiaId());
        java.util.Set<String> basketUsed = dao.getBasketUsed(entity.getTopiaId());
        java.util.Set<String> branchlineUsed = dao.getBranchlineUsed(entity.getTopiaId());
        for (SectionDto section : dto.getSection()) {
            section.setParentId(dto.getId());
            String sectionId = section.getTopiaId();
            boolean sectionIsUsed = sectionUsed.contains(sectionId);
            for (BasketDto basket : section.getBasket()) {
                basket.setParentId(sectionId);
                String basketId = basket.getTopiaId();
                boolean basketIsUsed = basketUsed.contains(basketId);
                for (BranchlineDto branchline : basket.getBranchline()) {
                    String branchlineId = branchline.getId();
                    branchline.setParentId(basketId);
                    boolean branchlineIsUsed = branchlineUsed.contains(branchlineId);
                    branchline.setNotUsed(!branchlineIsUsed);
                    if (branchlineIsUsed) {
                        basketIsUsed = true;
                    }
                }
                if (basketIsUsed) {
                    sectionIsUsed = true;
                }
                basket.setNotUsed(!basketIsUsed);
            }
            section.setNotUsed(!sectionIsUsed);
        }
        return form;
    }

    @Override
    protected SaveResultDto doSave(ServiceContext context, Set entity) {
        Section.SPI.initId(context, entity.getSection());
        entity.getSection().forEach(c -> {
            Basket.SPI.initId(context, c.getBasket());
            c.getBasket().forEach(d -> Branchline.SPI.initId(context, d.getBranchline()));
        });
        return super.doSave(context, entity);
    }
}
