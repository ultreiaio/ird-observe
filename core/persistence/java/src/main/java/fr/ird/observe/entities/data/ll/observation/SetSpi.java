package fr.ird.observe.entities.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ll.observation.ActivityReference;
import fr.ird.observe.dto.data.ll.observation.BasketReference;
import fr.ird.observe.dto.data.ll.observation.BranchlineReference;
import fr.ird.observe.dto.data.ll.observation.LonglinePositionAware;
import fr.ird.observe.dto.data.ll.observation.LonglinePositionContainerAware;
import fr.ird.observe.dto.data.ll.observation.SectionReference;
import fr.ird.observe.dto.data.ll.observation.SetDto;
import fr.ird.observe.dto.data.ll.observation.SetStubDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.TripEntityAware;
import fr.ird.observe.entities.data.ll.common.Trip;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SetSpi extends GeneratedSetSpi {

    public static void loadDtoReferenceParents(Set entity, LonglinePositionContainerAware dto, Collection<? extends LonglinePositionAware> data, ReferentialLocale referentialLocale) {
        List<SectionReference> sections = new LinkedList<>();
        List<BasketReference> baskets = new LinkedList<>();
        Map<String, BasketReference> basketsParents = new LinkedHashMap<>();
        Map<String, BranchlineReference> branchlinesParents = new LinkedHashMap<>();
        List<BranchlineReference> branchlines = new LinkedList<>();
        entity.getSection().forEach(section -> {
            section.setParentId(entity.getTopiaId());
            String sectionId = section.getTopiaId();
            SectionReference sectionReference = Section.toReference(referentialLocale, section);
            sections.add(sectionReference);
            section.getBasket().forEach(basket -> {
                String basketId = basket.getTopiaId();
                basket.setParentId(sectionId);
                BasketReference basketReference = Basket.toReference(referentialLocale, basket);
                baskets.add(basketReference);
                basketsParents.put(basketId, basketReference);
                basket.getBranchline().forEach(branchline -> {
                    branchline.setParentId(basketId);
                    BranchlineReference branchlineReference = Branchline.toReference(referentialLocale, branchline);
                    branchlines.add(branchlineReference);
                    branchlinesParents.put(branchline.getTopiaId(), branchlineReference);
                });
            });
        });
        dto.setSections(sections);
        dto.setBaskets(baskets);
        dto.setBranchlines(branchlines);
        for (LonglinePositionAware datum : data) {
            BranchlineReference branchline = datum.getBranchline();
            if (branchline != null) {
                BranchlineReference branchlineWithParentId = branchlinesParents.get(branchline.getId());
                datum.setBranchline(branchlineWithParentId);
            } else {
                BasketReference basket = datum.getBasket();
                if (basket != null) {
                    BasketReference basketWithParentId = basketsParents.get(basket.getId());
                    datum.setBasket(basketWithParentId);
                }
            }
        }

    }

    @Override
    public Form<SetDto> preCreate(ServiceContext context, Activity parent, Set preCreated) {
        // on utilise la date - heure de l'activité pour initialiser les horodatages de l'opération de pêche
        Date timeStamp = parent.getTimeStamp();
        preCreated.setSettingStartTimeStamp(timeStamp);
        preCreated.setSettingEndTimeStamp(DateUtils.addHours(timeStamp, 1));
        preCreated.setHaulingStartTimeStamp(DateUtils.addHours(timeStamp, 2));
        preCreated.setHaulingEndTimeStamp(DateUtils.addHours(timeStamp, 3));

        // on reporte la position de l'activité pour la position de début de filage
        preCreated.setSettingStartLatitude(parent.getLatitude());
        preCreated.setSettingStartLongitude(parent.getLongitude());
        //FIXME super.preCreate does already this?
//        Form<SetDto> form = super.preCreate(context, parent, preCreated);
//        form.getObject().setOtherSets(getBrothers(context, parent));
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Activity parent, Set entity, SetDto dto) {
        Trip trip = Activity.SPI.getParent(context, parent.getId());
        return TripEntityAware.onSaveUpdateTripIfEndDateChanged(Trip.SPI, trip, entity, super.onSave(context, parent, entity, dto));
    }

    @Override
    public void loadDtoForValidation(ServiceContext context, Activity parent, Set entity, SetDto dto) {
        dto.setOtherSets(getBrothers(context, parent));
    }

    public void loadDtoForValidation(ServiceContext context, Trip trip, Activity parent, SetDto dto) {
        dto.setOtherSets(getBrothers(context, trip, parent));
    }

    public java.util.Set<SetStubDto> getBrothers(ServiceContext context, Activity parent) {
        Trip trip = Activity.SPI.getParent(context, parent.getTopiaId());
        return getBrothers(context, trip, parent);
    }

    public java.util.Set<SetStubDto> getBrothers(ServiceContext context, Trip trip, Activity parent) {
        java.util.Set<SetStubDto> result = new HashSet<>();
        ReferentialLocale referentialLocale = context.getReferentialLocale();
        trip.getActivityObs().stream().filter(a -> a.getVesselActivity().isAllowSet()).filter(a -> !Objects.equals(a, parent) && a.getSet() != null).forEach(
                oneParent -> {
                    Set otherSet = oneParent.getSet();
                    SetStubDto otherSetDto = Set.SET_STUB_SPI.toDto(referentialLocale, otherSet);
                    ActivityReference reference = Activity.toReference(referentialLocale, oneParent);
                    otherSetDto.setActivity(reference);
                    result.add(otherSetDto);
                }
        );
        return result;
    }
}
