package fr.ird.observe.entities.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created on 12/18/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class SetTopiaDao extends AbstractSetTopiaDao<Set> {

    public java.util.Set<String> getSectionUsed(String setId) {
        java.util.Set<String> builder = new LinkedHashSet<>();
        builder.addAll(getCatchSectionUsed(setId));
        builder.addAll(getTdrSectionUsed(setId));
        return builder;
    }

    public java.util.Set<String> getBasketUsed(String setId) {
        java.util.Set<String> builder = new LinkedHashSet<>();
        builder.addAll(getCatchBasketUsed(setId));
        builder.addAll(getTdrBasketUsed(setId));
        return builder;
    }

    public java.util.Set<String> getBranchlineUsed(String setId) {
        java.util.Set<String> builder = new LinkedHashSet<>();
        builder.addAll(getCatchBranchlineUsed(setId));
        builder.addAll(getTdrBranchlineUsed(setId));
        return builder;
    }

    //FIXME Use this in next consolidate ll action
    public int sanitizeLonglineElements() {
        return execute(querySanitizeLonglineElements());
    }

    public List<String> getCatchSectionUsed(String setId) {
        return findMultipleResultBySqlQuery(queryCatchSectionUsed(setId));
    }

    public List<String> getCatchBasketUsed(String setId) {
        return findMultipleResultBySqlQuery(queryCatchBasketUsed(setId));
    }

    public List<String> getCatchBranchlineUsed(String setId) {
        return findMultipleResultBySqlQuery(queryCatchBranchlineUsed(setId));
    }

    public List<String> getTdrSectionUsed(String setId) {
        return findMultipleResultBySqlQuery(queryTdrSectionUsed(setId));
    }

    public List<String> getTdrBasketUsed(String setId) {
        return findMultipleResultBySqlQuery(queryTdrBasketUsed(setId));
    }

    public List<String> getTdrBranchlineUsed(String setId) {
        return findMultipleResultBySqlQuery(queryTdrBranchlineUsed(setId));
    }
}
