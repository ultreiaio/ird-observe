package fr.ird.observe.entities.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.localmarket.TripBatchDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.referential.common.Harbour;
import fr.ird.observe.entities.referential.ps.localmarket.Buyer;
import fr.ird.observe.entities.referential.ps.localmarket.Packaging;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TripBatchSpi extends GeneratedTripBatchSpi {

    @Override
    public void loadDtoForValidation(ServiceContext context, Trip entity, TripBatchDto dto) {
        LinkedHashSet<String> availablePackagingIds = getAvailablePackagingIds(context, entity);
        dto.setAvailablePackagingIds(availablePackagingIds);
        LinkedHashSet<String> availableBuyerIds = getAvailableBuyerIds(context, entity);
        dto.setAvailableBuyerIds(availableBuyerIds);
    }

    @Override
    public void toDto(ReferentialLocale referentialLocale, Trip entity, TripBatchDto dto) {
        super.toDto(referentialLocale, entity, dto);
        dto.setComment(entity.getLogbookComment());
    }

    @Override
    public void fromDto(ReferentialLocale referentialLocale, Trip entity, TripBatchDto dto) {
        super.fromDto(referentialLocale, entity, dto);
        entity.setLogbookComment(dto.getComment());
    }

    protected LinkedHashSet<String> getAvailablePackagingIds(ServiceContext context, Trip trip) {
        Harbour landingHarbour = trip.getLandingHarbour();
        Date date = trip.getEndDate();
        try (Stream<Packaging> stream = Packaging.getDao(context).streamAll()) {
            return stream
                    .filter(p -> p.acceptHarbour(landingHarbour) && p.acceptDate(date))
                    .map(Packaging::getTopiaId)
                    .collect(Collectors.toCollection(LinkedHashSet::new));
        }
    }

    protected LinkedHashSet<String> getAvailableBuyerIds(ServiceContext context, Trip trip) {
        Harbour landingHarbour = trip.getLandingHarbour();
        try (Stream<Buyer> stream = Buyer.getDao(context).streamAll()) {
            return stream
                    .filter(p -> p.acceptHarbour(landingHarbour))
                    .map(Buyer::getTopiaId)
                    .collect(Collectors.toCollection(LinkedHashSet::new));
        }
    }

}
