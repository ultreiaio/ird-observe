package fr.ird.observe.entities.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.result.AddEntityToUpdateStep;

public class TripGearUseFeaturesSpi extends GeneratedTripGearUseFeaturesSpi {

    @Override
    public void saveCallback(AddEntityToUpdateStep saveHelper, Trip entity) {
        super.saveCallback(saveHelper, entity);
        entity.getGearUseFeatures().forEach(c -> saveHelper.updateCollection(GearUseFeaturesMeasurement.SPI, c.getGearUseFeaturesMeasurement(), false));
        saveHelper.updateLastUpdateDateTable(GearUseFeaturesMeasurement.SPI);
    }
}
