package fr.ird.observe.entities.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.common.TripReference;
import fr.ird.observe.dto.data.ps.common.TripStatBuilder;
import fr.ird.observe.dto.referential.common.CountryReference;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.dto.referential.ps.common.AcquisitionStatusReference;
import fr.ird.observe.dto.referential.ps.common.ProgramReference;
import fr.ird.observe.entities.referential.common.Country;
import fr.ird.observe.entities.referential.common.DataQuality;
import fr.ird.observe.entities.referential.common.Harbour;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.Person;
import fr.ird.observe.entities.referential.common.ShipOwner;
import fr.ird.observe.entities.referential.common.Vessel;
import fr.ird.observe.entities.referential.common.VesselSizeCategory;
import fr.ird.observe.entities.referential.common.VesselType;
import fr.ird.observe.entities.referential.ps.common.AcquisitionStatus;
import fr.ird.observe.entities.referential.ps.common.Program;
import fr.ird.observe.entities.referential.ps.logbook.WellContentStatus;
import fr.ird.observe.spi.context.DataGroupByReferentialHelper;
import fr.ird.observe.spi.context.ReferentialCache;
import fr.ird.observe.spi.context.RelationCountCache;
import fr.ird.observe.spi.service.ServiceContext;
import org.apache.commons.lang3.mutable.MutableInt;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

/**
 * Created on 04/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripGroupByReferentialHelper extends DataGroupByReferentialHelper<TripDto, TripReference, Trip> {

    private final ReferentialCache<ProgramReference, Program> program;
    private final ReferentialCache<VesselReference, Vessel> vessel;
    private final ReferentialCache<AcquisitionStatusReference, AcquisitionStatus> acquisitionStatus;
    private final ReferentialCache<CountryReference, Country> country;
    private final RelationCountCache<Trip> observationRouteCount;
    private final RelationCountCache<Trip> logbookRouteCount;
    private final RelationCountCache<Trip> logbookSampleCount;
    private final RelationCountCache<Trip> logbookWellCount;
    private final RelationCountCache<Trip> landingLandingCount;
    private final RelationCountCache<Trip> localmarketBatchCount;
    private final RelationCountCache<Trip> localmarketSurveyCount;
    private final RelationCountCache<Trip> localmarketSampleCount;
    private final RelationCountCache<Trip> gearUseFeaturesCount;
    private final StatsBuilder statsBuilder;

    public TripGroupByReferentialHelper(ServiceContext context) {
        super(Trip.SPI,
              context,
              List.of(
                      fr.ird.observe.entities.data.ps.observation.Route.SPI,
                      fr.ird.observe.entities.data.ps.logbook.Route.SPI,
                      fr.ird.observe.entities.data.ps.logbook.Sample.SPI,
                      fr.ird.observe.entities.data.ps.logbook.Well.SPI,
                      fr.ird.observe.entities.data.ps.landing.Landing.SPI,
                      fr.ird.observe.entities.data.ps.localmarket.Batch.SPI,
                      fr.ird.observe.entities.data.ps.localmarket.Survey.SPI,
                      fr.ird.observe.entities.data.ps.localmarket.Sample.SPI,
                      fr.ird.observe.entities.data.ps.common.GearUseFeatures.SPI),
              List.of(Ocean.SPI,
                      Program.SPI,
                      Country.SPI,
                      Harbour.SPI,
                      Person.SPI,
                      Vessel.SPI,
                      VesselSizeCategory.SPI,
                      VesselType.SPI,
                      ShipOwner.SPI,
                      AcquisitionStatus.SPI,
                      DataQuality.SPI,
                      WellContentStatus.SPI));
        this.program = referentialGet(Program.class);
        this.vessel = referentialGet(Vessel.class);
        this.acquisitionStatus = referentialGet(AcquisitionStatus.class);
        this.country = referentialGet(Country.class);

        this.observationRouteCount = relationGet(fr.ird.observe.entities.data.ps.observation.Route.SPI);
        this.logbookRouteCount = relationGet(fr.ird.observe.entities.data.ps.logbook.Route.SPI);
        this.logbookSampleCount = relationGet(fr.ird.observe.entities.data.ps.logbook.Sample.SPI);
        this.logbookWellCount = relationGet(fr.ird.observe.entities.data.ps.logbook.Well.SPI);
        this.landingLandingCount = relationGet(fr.ird.observe.entities.data.ps.landing.Landing.SPI);
        this.localmarketBatchCount = relationGet(fr.ird.observe.entities.data.ps.localmarket.Batch.SPI);
        this.localmarketSurveyCount = relationGet(fr.ird.observe.entities.data.ps.localmarket.Survey.SPI);
        this.localmarketSampleCount = relationGet(fr.ird.observe.entities.data.ps.localmarket.Sample.SPI);
        this.gearUseFeaturesCount = relationGet(fr.ird.observe.entities.data.ps.common.GearUseFeatures.SPI);
        statsBuilder = new StatsBuilder();
    }

    @Override
    public Trip newChildren(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        Trip children = super.newChildren(resultSet, rowIndex);
        children.setObservationsProgram(program.entity(resultSet, rowIndex));
        children.setLogbookProgram(program.entity(resultSet, rowIndex));
        children.setStartDate(getDate(resultSet, rowIndex));
        children.setEndDate(getDate(resultSet, rowIndex));
        children.setVessel(vessel.entity(resultSet, rowIndex));
        return children;
    }

    @Override
    public TripDto newDto(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        TripDto dto = super.newDto(resultSet, rowIndex);
        dto.setObservationsProgram(program.reference(resultSet, rowIndex));
        dto.setLogbookProgram(program.reference(resultSet, rowIndex));
        dto.setStartDate(getDate(resultSet, rowIndex));
        dto.setEndDate(getDate(resultSet, rowIndex));
        dto.setVessel(vessel.reference(resultSet, rowIndex));
        return dto;
    }

    @Override
    public TripReference newReference(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        TripDto dto = newDto(resultSet, rowIndex);
        dto.setObservationsAcquisitionStatus(acquisitionStatus.reference(resultSet, rowIndex));
        dto.setLogbookAcquisitionStatus(acquisitionStatus.reference(resultSet, rowIndex));
        dto.setTargetWellsSamplingAcquisitionStatus(acquisitionStatus.reference(resultSet, rowIndex));
        dto.setLandingAcquisitionStatus(acquisitionStatus.reference(resultSet, rowIndex));
        dto.setLocalMarketAcquisitionStatus(acquisitionStatus.reference(resultSet, rowIndex));
        dto.setLocalMarketWellsSamplingAcquisitionStatus(acquisitionStatus.reference(resultSet, rowIndex));
        dto.setLocalMarketSurveySamplingAcquisitionStatus(acquisitionStatus.reference(resultSet, rowIndex));
        dto.setAdvancedSamplingAcquisitionStatus(acquisitionStatus.reference(resultSet, rowIndex));

        statsBuilder.buildStatistics(dto);

        long observationsCount = dto.getRouteObsStatValue();
        dto.setObservationsFilled(observationsCount > 0);

        long logbookCount = dto.getRouteLogbookStatValue();
        dto.setLogbookFilled(logbookCount > 0);

        long wellCount = dto.getWellStatValue();
        dto.setTargetWellsSamplingFilled(wellCount > 0);

        long landingCount = dto.getLandingStatValue();
        dto.setLandingFilled(landingCount > 0);

        long marketBatchSize = dto.getLocalmarketBatchStatValue();

        long marketSampleSize = dto.getLocalmarketSampleStatValue();
        dto.setLocalmarketWellsSamplingFilled(marketSampleSize > 0);

        long marketSurveySize = dto.getLocalmarketSurveyStatValue();
        dto.setLocalmarketSurveySamplingFilled(marketSurveySize > 0);
        dto.setLocalmarketFilled(dto.isLocalmarketWellsSamplingFilled() || dto.isLocalmarketSurveySamplingFilled() || marketBatchSize > 0);
        dto.setAdvancedSamplingFilled(false);

        return TripReference.of(referentialLocale, dto);
    }

    @Override
    public void decorateGroupValueChildren(List<Trip> groupByValues) {
        vessel.decorate(groupByValues, Trip::getVessel, (x, r) -> r.getLabel());
        country.decorate(groupByValues, t -> t.getVessel().getFlagCountry(), (x, r) -> r.getLabel());
        country.decorate(groupByValues, t -> t.getVessel().getFleetCountry(), (x, r) -> r.getLabel());
        super.decorateGroupValueChildren(groupByValues);
        groupByValues.sort(Comparator.comparing(Trip::getStartDate).thenComparing(t -> t.getVessel() == null ? null : t.getVessel().toString()));
    }

    class StatsBuilder implements TripStatBuilder {

        private String id;

        public void buildStatistics(TripDto statistics) {
            this.id = statistics.getId();
            TripStatBuilder.super.buildStatistics(statistics);
        }

        @Override
        public long buildGearUseFeaturesStatValue() {
            return gearUseFeaturesCount.count(id);
        }

        @Override
        public long buildRouteObsStatValue() {
            return observationRouteCount.count(id);
        }

        @Override
        public long buildRouteLogbookStatValue() {
            return logbookRouteCount.count(id);
        }

        @Override
        public long buildLandingStatValue() {
            return landingLandingCount.count(id);
        }

        @Override
        public long buildSampleStatValue() {
            return logbookSampleCount.count(id);
        }

        @Override
        public long buildLocalmarketBatchStatValue() {
            return localmarketBatchCount.count(id);
        }

        @Override
        public long buildLocalmarketSurveyStatValue() {
            return localmarketSurveyCount.count(id);
        }

        @Override
        public long buildLocalmarketSampleStatValue() {
            return localmarketSampleCount.count(id);
        }

        @Override
        public long buildWellStatValue() {
            return logbookWellCount.count(id);
        }
    }
}
