/*
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.data.ps.common;

import java.util.Date;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class TripImpl extends TripAbstract {

    private static final long serialVersionUID = 1L;

    public static boolean shouldCopyObservationsMetaData(Trip trip) {
        return trip.getObservationsAcquisitionStatus() == null || !trip.getObservationsAcquisitionStatus().isFieldEnabler();
    }

    public static boolean shouldRemoveObservationsMetaData(Trip trip) {
        return !trip.isObservationsFilled();
    }

    public static boolean shouldCopyLandingMetaData(Trip trip) {
        return trip.getLandingAcquisitionStatus() == null || !trip.getLandingAcquisitionStatus().isFieldEnabler();
    }

    public static boolean shouldRemoveLandingMetaData(Trip trip) {
        return !trip.isLandingFilled();
    }

    public static boolean shouldCopyLogbookMetaData(Trip trip) {
        return trip.getLogbookAcquisitionStatus() == null || !trip.getLogbookAcquisitionStatus().isFieldEnabler();
    }

    public static boolean shouldRemoveLogbookMetaData(Trip trip) {
        return !trip.isLogbookFilled();
    }

    public static boolean shouldCopyLocalmarketMetaData(Trip trip) {
        return trip.getLocalMarketAcquisitionStatus() == null || !trip.getLocalMarketAcquisitionStatus().isFieldEnabler();
    }

    public static boolean shouldRemoveLocalmarketMetaData(Trip trip) {
        return !trip.isLocalmarketFilled();
    }

    @Override
    public Date getTheoreticalEndDate() {
        TheoreticalEndDateBuilder builder = new TheoreticalEndDateBuilder(getStartDate());
        if (isRouteObsNotEmpty()) {
            getRouteObs().forEach(builder::addDate);
        }
        if (isRouteLogbookNotEmpty()) {
            getRouteLogbook().forEach(builder::addDate);
        }
        return builder.build();
//        Date lastActivityDate = isRouteObsEmpty() ? null : getRouteObs().stream().map(Route::getDate).max(Date::compareTo).orElse(null);
//        return Dates.getEndOfDay(lastActivityDate == null ? getStartDate() : lastActivityDate);
    }

    @Override
    public boolean isObservationsFilled() {
        return isRouteObsNotEmpty();
    }

    @Override
    public boolean isLogbookFilled() {
        return isRouteLogbookNotEmpty();
    }

    @Override
    public boolean isTargetWellsSamplingFilled() {
        return isWellNotEmpty();
    }

    @Override
    public boolean isLandingFilled() {
        return isLandingNotEmpty();
    }

    @Override
    public boolean isLocalmarketFilled() {
        return isLocalmarketBatchNotEmpty() || isLocalmarketSampleNotEmpty() || isLocalmarketSurveyNotEmpty();
    }

    @Override
    public boolean isLocalmarketWellsSamplingFilled() {
        return isLocalmarketSampleNotEmpty();
    }

    @Override
    public boolean isLocalmarketSurveySamplingFilled() {
        return isLocalmarketSurveyNotEmpty();
    }

    @Override
    public boolean isAdvancedSamplingFilled() {
        return false;
    }
}
