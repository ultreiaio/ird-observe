package fr.ird.observe.entities.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.datasource.request.ReplicatePartialRequest;
import fr.ird.observe.entities.data.DataEntity;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequestCallback;

import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Function;

/**
 * Created on 07/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@AutoService(ReplicatePartialRequestCallback.class)
public class TripReplicatePartialRequestCallback implements ReplicatePartialRequestCallback {

    @Override
    public boolean accept(ReplicatePartialRequest request) {
        return Trip.class.getName().equals(request.getDataType());
    }

    private boolean isGearUseFeatures(ReplicatePartialRequest request) {
        return request.getLayoutTypes().contains(fr.ird.observe.entities.data.ps.common.GearUseFeatures.class.getName());
    }

    private boolean isLanding(ReplicatePartialRequest request) {
        return request.getLayoutTypes().contains(fr.ird.observe.entities.data.ps.landing.Landing.class.getName());
    }

    private boolean isObservations(ReplicatePartialRequest request) {
        return request.getLayoutTypes().contains(fr.ird.observe.entities.data.ps.observation.Route.class.getName());
    }

    private boolean isLogbook(ReplicatePartialRequest request) {
        return request.getLayoutTypes().contains(fr.ird.observe.entities.data.ps.logbook.Route.class.getName());
    }

    private boolean isLocalmarket(ReplicatePartialRequest request) {
        return request.getLayoutTypes().contains(fr.ird.observe.entities.data.ps.localmarket.Batch.class.getName());
    }

    @Override
    public void consume(ReplicatePartialRequest request, SqlRequestSetConsumerContext context) {
        SqlScriptWriter writer = context.getWriter();
        TripTopiaDao dao = Trip.getDao(context.getSourcePersistenceContext());
        String oldId = request.getOldId();
        Trip oldTrip = dao.forTopiaIdEquals(oldId).findUnique();
        String newId = request.getNewId();
        Trip newTrip = dao.forTopiaIdEquals(newId).findUnique();
        Set<Class<? extends DataEntity>> lastUpdateDates = new LinkedHashSet<>();
        lastUpdateDates.add(Trip.class);
        if (isGearUseFeatures(request)) {
            lastUpdateDates.add(GearUseFeatures.class);
        }
        Function<String, String> commentFormat = request.commentFormat();
        if (isObservations(request)) {
            Trip.SPI.updateObservationsMetadata(dao, commentFormat, writer, lastUpdateDates, oldTrip, newTrip);
        }
        if (isLogbook(request)) {
            Trip.SPI.updateLogbookMetadata(dao, commentFormat, writer, lastUpdateDates, oldTrip, newTrip);
        }
        if (isLanding(request)) {
            Trip.SPI.updateLandingMetadata(dao, commentFormat, writer, lastUpdateDates, oldTrip, newTrip);
        }
        if (isLocalmarket(request)) {
            Trip.SPI.updateLocalmarketMetadata(dao, commentFormat, writer, lastUpdateDates, oldTrip, newTrip);
        }
        Timestamp now = context.nowTimestamp();
        TripSpi.updateTrip(dao, writer, lastUpdateDates, oldId, newId, now);
    }

}
