package fr.ird.observe.entities.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.ps.ActivitiesAcquisitionMode;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.common.TripReference;
import fr.ird.observe.dto.data.ps.pairing.RoutePairingDto;
import fr.ird.observe.dto.data.ps.pairing.TripActivitiesPairingDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.data.ps.landing.Landing;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.entities.data.ps.logbook.Route;
import fr.ird.observe.entities.data.ps.logbook.Sample;
import fr.ird.observe.entities.data.ps.logbook.SampleActivity;
import fr.ird.observe.entities.data.ps.logbook.Well;
import fr.ird.observe.entities.data.ps.logbook.WellActivity;
import fr.ird.observe.entities.referential.common.DataQuality;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.common.SpeciesList;
import fr.ird.observe.entities.referential.ps.common.AcquisitionStatus;
import fr.ird.observe.entities.referential.ps.common.VesselActivity;
import fr.ird.observe.entities.referential.ps.logbook.WellContentStatus;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.Dates;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripSpi extends GeneratedTripSpi {

    public static void updateTrip(TripTopiaDao dao, SqlScriptWriter writer, Set<Class<? extends DataEntity>> lastUpdateDates, String oldId, String newId, Timestamp now) {
        writer.writeSql(dao.updateTripVersionToSql(oldId, now));
        writer.writeSql(dao.updateTripVersionToSql(newId, now));
        for (Class<? extends DataEntity> lastUpdateDate : lastUpdateDates) {
            writer.writeSql(Entities.updateLastUpdateDate(lastUpdateDate.getName(), now.toString()));
        }
    }

    @Override
    public void toDto(ReferentialLocale referentialLocale, Trip entity, TripDto dto) {
        super.toDto(referentialLocale, entity, dto);
        dto.setObservationsFilled(entity.isObservationsFilled());
        dto.setLogbookFilled(entity.isLogbookFilled());
        dto.setTargetWellsSamplingFilled(entity.isTargetWellsSamplingFilled());
        dto.setLandingFilled(entity.isLandingFilled());
        dto.setLocalmarketFilled(entity.isLocalmarketFilled());
        dto.setLocalmarketWellsSamplingFilled(entity.isLocalmarketWellsSamplingFilled());
        dto.setLocalmarketSurveySamplingFilled(entity.isLocalmarketSurveySamplingFilled());
        dto.setAdvancedSamplingFilled(entity.isAdvancedSamplingFilled());
    }

    @Override
    public Form<TripDto> preCreate(ServiceContext context, Trip preCreated) {
        if (preCreated.getStartDate() == null && preCreated.getEndDate() == null) {
            Date date = Dates.getDay(context.now());
            preCreated.setStartDate(date);
            preCreated.setEndDate(date);
        }
        AcquisitionStatus acquisitionStatus = AcquisitionStatus.loadEntity(context, ProtectedIdsPs.PS_COMMON_DEFAULT_ACQUISITION_STATUS_ID);
        AcquisitionStatus acquisitionStatusDone = AcquisitionStatus.loadEntity(context, ProtectedIdsPs.PS_COMMON_DONE_ACQUISITION_STATUS_ID);
        if (preCreated.getObservationsProgram() == null) {
            preCreated.setObservationsAcquisitionStatus(acquisitionStatus);
        } else {
            //FIXME Should we add some default values?
            preCreated.setObservationsAcquisitionStatus(acquisitionStatusDone);

        }
        if (preCreated.getLogbookProgram() == null) {
            preCreated.setLogbookAcquisitionStatus(acquisitionStatus);
        } else {
            preCreated.setLogbookAcquisitionStatus(acquisitionStatusDone);
            WellContentStatus wellContentStatus = WellContentStatus.loadEntity(context, ProtectedIdsPs.PS_LOGBOOK_DEFAULT_WELL_CONTENT_STATUS_ID);
            preCreated.setDepartureWellContentStatus(wellContentStatus);
            preCreated.setLandingWellContentStatus(wellContentStatus);
            DataQuality dataQuality = DataQuality.loadEntity(context, ProtectedIdsPs.PS_LOGBOOK_TRIP_DEFAULT_DATA_QUALITY_ID);
            preCreated.setLogbookDataQuality(dataQuality);
        }
        preCreated.setTargetWellsSamplingAcquisitionStatus(acquisitionStatus);
        preCreated.setLandingAcquisitionStatus(acquisitionStatus);
        preCreated.setLocalMarketAcquisitionStatus(acquisitionStatus);
        preCreated.setLocalMarketWellsSamplingAcquisitionStatus(acquisitionStatus);
        preCreated.setLocalMarketSurveySamplingAcquisitionStatus(acquisitionStatus);
        preCreated.setAdvancedSamplingAcquisitionStatus(acquisitionStatus);
        preCreated.setActivitiesAcquisitionMode(ActivitiesAcquisitionMode.BY_TIME);
        return super.preCreate(context, preCreated);
    }

    @Override
    public void loadDtoForValidation(ServiceContext context, Trip entity, TripDto dto) {
        entity.buildStatistics(dto);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Trip entity, TripDto dto) {
        ActivitiesAcquisitionMode previousActivitiesAcquisitionMode = entity.getActivitiesAcquisitionMode();
        fromDto(context, entity, dto);
        ActivitiesAcquisitionMode newActivitiesAcquisitionMode = entity.getActivitiesAcquisitionMode();
        Consumer<AddEntityToUpdateStep> extraConsumer = null;
        if (entity.isPersisted()) {
            entity.updateEndDate(entity);
            if (!Objects.equals(previousActivitiesAcquisitionMode, newActivitiesAcquisitionMode) && newActivitiesAcquisitionMode == ActivitiesAcquisitionMode.BY_TIME) {
                // We need to reorder all logbook activities number according to the order deduced by their time
                // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2729#note_1538955823
                extraConsumer = reorderLogbookActivitiesByTime(entity);
            }
        }
        return extraConsumer;
    }

    @Override
    public TripGroupByReferentialHelper createReferentialHelper(ServiceContext context) {
        return new TripGroupByReferentialHelper(context);
    }

    @Override
    public Set<Class<? extends DataEntity>> layoutTypeToDataTypes(Class<? extends DataDto> dtoType) {
        if (fr.ird.observe.dto.data.ps.common.TripLogbookDto.class.equals(dtoType)) {
            return Set.of(fr.ird.observe.entities.data.ps.logbook.Route.class, fr.ird.observe.entities.data.ps.logbook.Sample.class, fr.ird.observe.entities.data.ps.logbook.Well.class);
        }
        if (fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class.equals(dtoType)) {
            return Set.of(fr.ird.observe.entities.data.ps.localmarket.Batch.class, fr.ird.observe.entities.data.ps.localmarket.Sample.class, fr.ird.observe.entities.data.ps.localmarket.Survey.class);
        }
        if (fr.ird.observe.dto.data.ps.common.TripGearUseFeaturesDto.class.equals(dtoType)) {
            return Set.of(fr.ird.observe.entities.data.ps.common.GearUseFeatures.class);
        }
        if (fr.ird.observe.dto.data.ps.landing.TripLandingDto.class.equals(dtoType)) {
            return Set.of(fr.ird.observe.entities.data.ps.landing.Landing.class);
        }
        if (fr.ird.observe.dto.data.ps.observation.RouteDto.class.isAssignableFrom(dtoType)) {
            return Set.of(fr.ird.observe.entities.data.ps.observation.Route.class);
        }
        return super.layoutTypeToDataTypes(dtoType);
    }

    public ReferentialDtoReferenceSet<SpeciesReference> getSpeciesByListAndTrip(ServiceContext context, String tripId, String speciesListId) {
        Ocean ocean = null;
        if (tripId != null) {
            Trip trip = loadEntity(context, tripId);
            ocean = trip.getOcean();
        }
        Stream<Species> speciesList = SpeciesList.loadEntity(context, speciesListId).getSpecies().stream();
        if (ocean != null) {
            Ocean finalOcean = ocean;
            speciesList = speciesList.filter(s -> s.containsOcean(finalOcean));
        }
        return Species.toReferenceSet(context.getReferentialLocale(), speciesList, null, null);
    }

    public TripMapDto getTripMap(ServiceContext context, TripMapConfigDto config) {
        String tripId = config.getTripId();
        TripTopiaDao dao = getDao(context);
        Trip trip = dao.forTopiaIdEquals(tripId).findUnique();
        LinkedHashSet<TripMapPoint> points = dao.buildMap(config, trip);
        return TripMapDto.of(config.getTripId(), points);
    }

    public int getMatchingTripsVesselWithinDateRange(ServiceContext context, String id, String vesselId, Date startDate, Date endDate) {
        if (vesselId == null || startDate == null || endDate == null) {
            return 0;
        }
        return (int) getDao(context).
                <Trip>stream("From fr.ird.observe.entities.data.ps.common.TripImpl Where vessel.id = :vesselId " +
                                     "And (( :startDate <= endDate And :endDate >= endDate ) " +
                                     " Or ( :endDate <= startDate And :endDate >= startDate )) " +
                                     "Order By startDate, endDate", Map.of(
                "vesselId", vesselId,
                "startDate", startDate,
                "endDate", endDate))
                .filter(t -> !Objects.equals(id, t.getTopiaId()))
                .count();
    }

    /**
     * Get all logbook activities with a {@code Set} of the given trip.
     *
     * @param context context
     * @param tripId  id of the trip
     * @return data set of logbook set activities in the given trip
     */
    public List<fr.ird.observe.dto.data.ps.logbook.ActivityStubDto> getLogbookSetActivities(ServiceContext context, String tripId) {
        Map<String, fr.ird.observe.dto.referential.ps.common.VesselActivityReference> vesselActivities = VesselActivity.toReferenceSet(context, null).toMap(VesselActivityReference::getId);
        return Trip.getDao(context).getLogbookSetActivities(tripId, vesselActivities);
    }

    /**
     * Get all logbook activities candidates for a well plan (the activity can be with a set or with vessel activity 31).
     *
     * @param context context
     * @param tripId  id of the trip
     * @return data set of logbook set activities in the given trip
     */
    public List<fr.ird.observe.dto.data.ps.logbook.ActivityStubDto> getLogbookWellPlanActivities(ServiceContext context, String tripId) {
        Map<String, fr.ird.observe.dto.referential.ps.common.VesselActivityReference> vesselActivities = VesselActivity.toReferenceSet(context, null).toMap(VesselActivityReference::getId);
        return Trip.getDao(context).getLogbookWellPlanActivities(tripId, vesselActivities);
    }

    public boolean isActivityEndOfSearchFound(ServiceContext context, String routeId) {
        fr.ird.observe.entities.data.ps.observation.Route route = fr.ird.observe.entities.data.ps.observation.Route.loadEntity(context, routeId);
        return route.getActivity().stream().anyMatch(fr.ird.observe.entities.data.ps.observation.Activity::isActivityEndOfSearching);
    }

    public boolean isActivitiesAcquisitionModeByTimeEnabled(ServiceContext context, String tripId) {
        return Trip.getDao(context).isActivitiesAcquisitionModeByTimeEnabled(tripId);
    }

    public TripActivitiesPairingDto getTripPairingDto(ServiceContext context, String tripId) {
        TripDto trip = loadEntityToDto(context, tripId);
        if (trip.isRouteLogbookEmpty() || trip.isRouteObsEmpty()) {
            return null;
        }
        ReferentialLocale referentialLocale = context.getReferentialLocale();
        Map<String, VesselActivityReference> vesselActivities = VesselActivity.toReferenceSet(context, null).toId();
        Map<fr.ird.observe.dto.data.ps.observation.RouteReference, List<fr.ird.observe.dto.data.ps.observation.ActivityReference>> observationRoutes = fr.ird.observe.entities.data.ps.observation.Route.SPI.getActivitiesByRouteId(context, vesselActivities, trip);
        Map<fr.ird.observe.dto.data.ps.logbook.RouteReference, List<fr.ird.observe.dto.data.ps.logbook.ActivityReference>> logbookRoutes = fr.ird.observe.entities.data.ps.logbook.Route.SPI.getActivitiesByRouteId(context, vesselActivities, trip, observationRoutes);
        TripReference tripReference = trip.toReference(referentialLocale);
        return new TripActivitiesPairingDto(tripReference, logbookRoutes, observationRoutes);
    }

    public RoutePairingDto getRoutePairingDto(ServiceContext context, String tripId, String logbookRouteId) {
        TripDto trip = loadEntityToDto(context, tripId);
        if (trip.isRouteObsEmpty()) {
            return null;
        }
        Map<String, VesselActivityReference> vesselActivities = VesselActivity.toReferenceSet(context, null).toId();
        Map<fr.ird.observe.dto.data.ps.observation.RouteReference, List<fr.ird.observe.dto.data.ps.observation.ActivityReference>> observationRoutes = fr.ird.observe.entities.data.ps.observation.Route.SPI.getActivitiesByRouteId(context, vesselActivities, trip);
        fr.ird.observe.dto.data.ps.logbook.RouteReference logbookRoute = fr.ird.observe.entities.data.ps.logbook.Route.toReference(context, logbookRouteId, null);
        return new RoutePairingDto(logbookRoute, observationRoutes);
    }

    public void updateObservationsMetadata(TripTopiaDao dao, Function<String, String> commentFormat, SqlScriptWriter writer, Set<Class<? extends DataEntity>> lastUpdateDates, Trip oldTrip, Trip newTrip) {
        if (TripImpl.shouldCopyObservationsMetaData(newTrip)) {
            writer.writeSql(dao.copyObservationsMetadataToSql(commentFormat, oldTrip, newTrip.getId()));
        }
        if (TripImpl.shouldRemoveObservationsMetaData(oldTrip)) {
            writer.writeSql(dao.removeObservationsMetadataToSql(oldTrip.getId()));
        }
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.Route.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.Activity.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.FloatingObject.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.FloatingObjectPart.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.TransmittingBuoy.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.Set.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.Catch.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.NonTargetCatchRelease.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.Sample.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.SampleMeasure.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.SchoolEstimate.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.ObjectSchoolEstimate.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.observation.ObjectObservedSpecies.class);
    }

    private Consumer<AddEntityToUpdateStep> reorderLogbookActivitiesByTime(Trip entity) {
        Map<Route, Set<Activity>> modifiedActivitiesByRoute = new LinkedHashMap<>();
        for (Route route : entity.getRouteLogbook()) {
            Set<Activity> modifiedActivities = Route.SPI.reorderLogbookActivitiesByTime(route);
            if (!modifiedActivities.isEmpty()) {
                modifiedActivitiesByRoute.put(route, modifiedActivities);
            }
        }
        if (modifiedActivitiesByRoute.isEmpty()) {
            // no activity was modified
            return null;
        }
        // Some activities were modified, need then to update some lastUpdateDates
        return s -> {
            // update route lastUpdateDate table entry
            s.updateLastUpdateDateTable(Route.SPI);
            // update activity lastUpdateDate table entry
            s.updateLastUpdateDateTable(Activity.SPI);
            for (Map.Entry<Route, Set<Activity>> entry : modifiedActivitiesByRoute.entrySet()) {
                // update route lastUpdateDate field
                s.updateLastUpdateDateField(Route.SPI, entry.getKey());
                // update activities lastUpdateDate field
                entry.getValue().forEach(a -> s.updateLastUpdateDateField(Activity.SPI, a));
            }
        };
    }

    public void updateLandingMetadata(TripTopiaDao dao, Function<String, String> commentFormat, SqlScriptWriter writer, Set<Class<? extends DataEntity>> lastUpdateDates, Trip oldTrip, Trip newTrip) {
        String newTripId = newTrip.getTopiaId();
        if (TripImpl.shouldCopyLandingMetaData(newTrip)) {
            writer.writeSql(dao.copyLandingMetadataToSql(oldTrip, newTripId));
        }
        dao.updateLogbookCommonMetadataToSql(commentFormat, oldTrip, newTrip, writer::writeSql);
        if (TripImpl.shouldRemoveLandingMetaData(oldTrip)) {
            writer.writeSql(dao.removeLandingMetadataToSql(oldTrip.getTopiaId()));
        }
        lastUpdateDates.add(Landing.class);
    }

    public void updateLogbookMetadata(TripTopiaDao dao, Function<String, String> commentFormat, SqlScriptWriter writer, Set<Class<? extends DataEntity>> lastUpdateDates, Trip oldTrip, Trip newTrip) {
        if (TripImpl.shouldCopyLogbookMetaData(newTrip)) {
            writer.writeSql(dao.copyLogbookMetadataToSql(commentFormat, oldTrip, newTrip.getId()));
        }
        // This avoid in others update to do it again
        newTrip.setLogbookComment(oldTrip.getLogbookComment());
        // This avoid in others update to do it again
        newTrip.setLogbookDataEntryOperator(oldTrip.getLogbookDataEntryOperator());
        // This avoid in others update to do it again
        newTrip.setLogbookDataQuality(oldTrip.getLogbookDataQuality());

        if (TripImpl.shouldRemoveLogbookMetaData(oldTrip)) {
            writer.writeSql(dao.removeLogbookMetadataToSql(oldTrip.getId()));
        }
        if (lastUpdateDates != null) {
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.Route.class);
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.Activity.class);
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.FloatingObject.class);
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.FloatingObjectPart.class);
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.TransmittingBuoy.class);
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.Catch.class);
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.Well.class);
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.Sample.class);
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.SampleActivity.class);
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.SampleSpecies.class);
            lastUpdateDates.add(fr.ird.observe.entities.data.ps.logbook.SampleSpeciesMeasure.class);
        }
    }

    public void updateLocalmarketMetadata(TripTopiaDao dao, Function<String, String> commentFormat, SqlScriptWriter writer, Set<Class<? extends DataEntity>> lastUpdateDates, Trip oldTrip, Trip newTrip) {
        String newTripId = newTrip.getId();
        if (TripImpl.shouldCopyLocalmarketMetaData(newTrip)) {
            writer.writeSql(dao.copyLocalmarketMetadataToSql(oldTrip, newTripId));
        }
        if (TripImpl.shouldRemoveLocalmarketMetaData(oldTrip)) {
            writer.writeSql(dao.removeLocalmarketMetadataToSql(oldTrip.getId()));
        }
        dao.updateLogbookCommonMetadataToSql(commentFormat, oldTrip, newTrip, writer::writeSql);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.localmarket.Sample.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.localmarket.SampleSpecies.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.localmarket.SampleSpeciesMeasure.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.localmarket.Batch.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.localmarket.Survey.class);
        lastUpdateDates.add(fr.ird.observe.entities.data.ps.localmarket.SurveyPart.class);
    }

    public Set<String> getLogbookWellIdsFromWellPlan(ServiceContext context, String tripId) {
        return Trip.getDao(context).getLogbookWellIdsFromWellPlan(tripId);
    }

    public Set<String> getLogbookWellIdsFromSample(ServiceContext context, String tripId) {
        return Trip.getDao(context).getLogbookWellIdsFromSample(tripId);
    }

    public Set<String> createLogbookSampleActivityFromWellPlan(ServiceContext context, String tripId, String wellId) {
        Trip trip = loadEntity(context, tripId);
        Set<String> result = new LinkedHashSet<>();
        for (Well well : trip.getWell()) {
            if (!well.getWell().equals(wellId)) {
                continue;
            }
            for (WellActivity wellActivity : well.getWellActivity()) {
                Activity activity = wellActivity.getActivity();
                result.add(activity.getId());
            }
        }
        return result;
    }

    public Set<String> createLogbookWellActivityFromSample(ServiceContext context, String tripId, String wellId) {
        Set<String> result = new LinkedHashSet<>();
        Trip trip = loadEntity(context, tripId);
        for (Sample sample : trip.getSample()) {
            if (!sample.getWell().equals(wellId)) {
                continue;
            }
            for (SampleActivity sampleActivity : sample.getSampleActivity()) {
                result.add(sampleActivity.getActivity().getId());
            }
        }

        return result;
    }
}
