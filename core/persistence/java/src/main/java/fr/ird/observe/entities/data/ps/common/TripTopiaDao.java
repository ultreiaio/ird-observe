package fr.ird.observe.entities.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.TripMapPointType;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.referential.common.DataQuality;
import fr.ird.observe.entities.referential.common.Person;
import fr.ird.observe.entities.referential.ps.common.AcquisitionStatus;
import fr.ird.observe.entities.referential.ps.common.Program;
import fr.ird.observe.entities.referential.ps.logbook.WellContentStatus;
import io.ultreia.java4all.util.Dates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import static fr.ird.observe.entities.Entities.addPointWithDateAndOptionalTime;
import static org.nuiton.topia.persistence.support.QuerySupport.toComment;
import static org.nuiton.topia.persistence.support.QuerySupport.toId;

/**
 * Created at 14/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class TripTopiaDao extends AbstractTripTopiaDao<Trip> {
    private static final Logger log = LogManager.getLogger(TripTopiaDao.class);

    public List<ActivityStubDto> getLogbookSetActivities(String tripId, Map<String, VesselActivityReference> vesselActivities) {
        return findMultipleResultBySqlQuery(queryLogbookFishingActivities(tripId), s -> toActivityStubDto(s, vesselActivities));
    }

    public List<ActivityStubDto> getLogbookWellPlanActivities(String tripId, Map<String, VesselActivityReference> vesselActivities) {
        return findMultipleResultBySqlQuery(queryLogbookWellPlanActivities(tripId), s -> toActivityStubDto(s, vesselActivities));
    }

    public Set<String> getLogbookWellIdsFromWellPlan(String tripId) {
        return Set.copyOf(findMultipleResultBySqlQuery(queryLogbookWellIdsFromWellPlan(tripId)));
    }

    public Set<String> getLogbookActivityIdsFromWellPlan(String tripId, String wellId) {
        return new LinkedHashSet<>(findMultipleResultBySqlQuery(queryLogbookActivityIdsFromWellPlan(tripId, wellId)));
    }

    public Set<String> getLogbookWellIdsFromSample(String tripId) {
        return Set.copyOf(findMultipleResultBySqlQuery(queryLogbookWellIdsFromSample(tripId)));
    }

    public Set<String> getLogbookActivityIdsFromSample(String tripId, String wellId) {
        return new LinkedHashSet<>(findMultipleResultBySqlQuery(queryLogbookActivityIdsFromSample(tripId, wellId)));
    }

    public boolean isActivitiesAcquisitionModeByTimeEnabled(String tripId) {
        return this.<Number>findSingleResultBySqlQuery(queryIsActivitiesAcquisitionModeByTimeEnabled(tripId)).intValue() == 0;
    }

    public List<TripMapPoint> getObservationActivityPoint(String tripId) {
        return findMultipleResultBySqlQuery(queryObservationActivityPoint(tripId), (resultSet, tripMapPoints) -> {
            String vesselActivityId = (String) resultSet[4];
            String schoolType = (String) resultSet[5];
            TripMapPointType type = TripMapPointType.psActivityObs;
            if (ProtectedIdsPs.PS_COMMON_ACTIVITY_HARBOUR_ID.equals(vesselActivityId)) {
                type = TripMapPointType.psActivityObsInHarbour;
            } else if (schoolType != null) {
                switch (schoolType) {
                    case ProtectedIdsPs.PS_COMMON_SCHOOL_TYPE_FREE_ID:
                        type = TripMapPointType.psActivityObsWithFreeSchoolType;
                        break;
                    case ProtectedIdsPs.PS_COMMON_SCHOOL_TYPE_OBJECT_ID:
                        type = TripMapPointType.psActivityObsWithObjectSchoolType;
                        break;
                }
            }
            TripMapPoint point = addPointWithDateAndOptionalTime(type, resultSet, 0, tripMapPoints);
            log.debug(String.format("[PS-Observation] Added activity point: %s", point));
        });
    }

    public List<TripMapPoint> getLogbookActivityPoint(String tripId) {
        return findMultipleResultBySqlQuery(queryLogbookActivityPoint(tripId), (resultSet, tripMapPoints) -> {
            String vesselActivityId = (String) resultSet[4];
            String schoolType = (String) resultSet[5];
            TripMapPointType type = TripMapPointType.psActivityLogbook;
            boolean withSample = ((Number) resultSet[6]).intValue() > 0;
            if (withSample) {
                type = TripMapPointType.psActivityLogbookWithSampling;
            }
            if (ProtectedIdsPs.PS_COMMON_ACTIVITY_HARBOUR_ID.equals(vesselActivityId)) {
                type = TripMapPointType.psActivityLogbookInHarbour;
            } else if (schoolType != null) {
                switch (schoolType) {
                    case ProtectedIdsPs.PS_COMMON_SCHOOL_TYPE_FREE_ID:
                        type = TripMapPointType.psActivityLogbookWithFreeSchoolType;
                        if (withSample) {
                            type = TripMapPointType.psActivityLogbookWithFreeSchoolTypeWithSampling;
                        }
                        break;
                    case ProtectedIdsPs.PS_COMMON_SCHOOL_TYPE_OBJECT_ID:
                        type = TripMapPointType.psActivityLogbookWithObjectSchoolType;
                        if (withSample) {
                            type = TripMapPointType.psActivityLogbookWithObjectSchoolTypeWithSampling;
                        }
                        break;
                }
            }
            TripMapPoint point = addPointWithDateAndOptionalTime(type, resultSet, 0, tripMapPoints);
            log.debug(String.format("[PS-Logbook] Added activity point: %s", point));
        });
    }

    public List<TripMapPoint> getLogbookTransmittingBuoyPoint(String tripId) {
        return findMultipleResultBySqlQuery(queryLogbookTransmittingBuoyPoint(tripId), (resultSet, tripMapPoints) -> {
            TripMapPoint point = addPointWithDateAndOptionalTime(TripMapPointType.psTransmittingBuoyLostLogbook, resultSet, 0, tripMapPoints);
            log.debug(String.format("[PS-Logbook] Added transmitting buoy point: %s", point));
        });
    }

    public LinkedHashSet<TripMapPoint> buildMap(TripMapConfigDto tripMapConfig, Trip trip) {
        String tripId = tripMapConfig.getTripId();
        boolean addObservation = tripMapConfig.isAddObservations();
        boolean addLogbook = tripMapConfig.isAddLogbook();

        log.info(String.format("[PS] Loading points for %s", tripMapConfig));

        LinkedHashSet<TripMapPoint> tripMapPoints = new LinkedHashSet<>();

        // add departure harbour
        Entities.createHarbourPoint(trip.getDepartureHarbour(), trip.getStartDate(), TripMapPointType.psTripDepartureHarbour).ifPresent(e -> {
            tripMapPoints.add(e);
            log.debug(String.format("[PS] Added departure harbour point: %s", e));
        });

        if (addObservation) {
            // add Observation activities
            tripMapPoints.addAll(getObservationActivityPoint(tripId));
        }
        if (addLogbook) {
            // add Logbook activities
            tripMapPoints.addAll(getLogbookActivityPoint(tripId));
            // add Logbook transmitting buoy
            tripMapPoints.addAll(getLogbookTransmittingBuoyPoint(tripId));
        }
        // add landing harbour
        Entities.createHarbourPoint(trip.getLandingHarbour(), trip.getEndDate(), TripMapPointType.psTripLandingHarbour).ifPresent(e -> {
            tripMapPoints.add(e);
            log.debug(String.format("[PS] Added landing harbour point: %s", e));
        });
        log.info(String.format("[PS] Loaded %d point(s) for trip: %s...", tripMapPoints.size(), tripId));
        return tripMapPoints;
    }

    public String copyObservationsMetadataToSql(Function<String, String> commentFormat, Trip trip, String newId) {
        Program observationsProgram = trip.getObservationsProgram();
        AcquisitionStatus observationsAcquisitionStatus = trip.getObservationsAcquisitionStatus();
        Person observationsDataEntryOperator = trip.getObservationsDataEntryOperator();
        DataQuality observationsDataQuality = trip.getObservationsDataQuality();
        Person observer = trip.getObserver();
        String observationsComment = trip.getObservationsComment();
        String formsUrl = trip.getFormsUrl();
        String reportsUrl = trip.getReportsUrl();
        return fillSql(queryCopyObservationsMetadata(toId(observationsProgram),
                                                     toId(observationsAcquisitionStatus),
                                                     toId(observationsDataEntryOperator),
                                                     toId(observationsDataQuality),
                                                     toId(observer),
                                                     toComment(commentFormat, observationsComment),
                                                     formsUrl,
                                                     reportsUrl,
                                                     newId
        ));
    }


    public String removeObservationsMetadataToSql(String id) {
        return fillSql(queryRemoveObservationsMetadata(id));
    }


    public String copyLandingMetadataToSql(Trip trip, String newId) {
        AcquisitionStatus landingAcquisitionStatus = trip.getLandingAcquisitionStatus();
        Float landingTotalWeight = trip.getLandingTotalWeight();
        return fillSql(queryCopyLandingMetadata(toId(landingAcquisitionStatus), landingTotalWeight, newId));
    }

    public String removeLandingMetadataToSql(String id) {
        return fillSql(queryRemoveLandingMetadata(id));
    }

    public String copyLogbookMetadataToSql(Function<String, String> commentFormat, Trip trip, String newId) {
        Program logbookProgram = trip.getLogbookProgram();
        AcquisitionStatus logbookAcquisitionStatus = trip.getLogbookAcquisitionStatus();
        String logbookComment = trip.getLogbookComment();
        Person logbookDataEntryOperator = trip.getLogbookDataEntryOperator();
        DataQuality logbookDataQuality = trip.getLogbookDataQuality();
        AcquisitionStatus targetWellsSamplingAcquisitionStatus = trip.getTargetWellsSamplingAcquisitionStatus();
        AcquisitionStatus advancedSamplingAcquisitionStatus = trip.getAdvancedSamplingAcquisitionStatus();

        WellContentStatus departureWellContentStatus = trip.getDepartureWellContentStatus();
        WellContentStatus landingWellContentStatus = trip.getLandingWellContentStatus();

        Integer timeAtSea = trip.getTimeAtSea();
        Integer fishingTime = trip.getFishingTime();
        Integer loch = trip.getLoch();
        return fillSql(queryCopyLogbookMetadata(toId(logbookProgram),
                                                toId(logbookAcquisitionStatus),
                                                toId(logbookDataEntryOperator),
                                                toId(logbookDataQuality),
                                                toId(targetWellsSamplingAcquisitionStatus),
                                                toId(advancedSamplingAcquisitionStatus),
                                                toId(departureWellContentStatus),
                                                toId(landingWellContentStatus),
                                                toComment(commentFormat, logbookComment),
                                                timeAtSea,
                                                fishingTime,
                                                loch,
                                                newId
        ));
    }

    public String copyLogbookDataEntryOperatorToSql(Trip trip, String newId) {
        Person logbookDataEntryOperator = trip.getLogbookDataEntryOperator();
        return fillSql(queryCopyLogbookDataEntryOperator(toId(logbookDataEntryOperator), newId));
    }

    public String copyLogbookDataQualityToSql(Trip trip, String newId) {
        DataQuality logbookDataQuality = trip.getLogbookDataQuality();
        return fillSql(queryCopyLogbookDataQuality(toId(logbookDataQuality), newId));
    }

    public String copyLogbookCommentToSql(Function<String, String> commentFormat, Trip trip, String newId) {
        String logbookComment = trip.getLogbookComment();
        return fillSql(queryCopyLogbookComment(toComment(commentFormat, logbookComment),
                                               newId));
    }

    public String removeLogbookMetadataToSql(String id) {
        return fillSql(queryRemoveLogbookMetadata(id));
    }

    public void updateLogbookCommonMetadataToSql(Function<String, String> commentFormat, Trip oldTrip, Trip newTrip, Consumer<String> sqlConsumer) {
        String newTripId = newTrip.getTopiaId();
        if (newTrip.getLogbookDataEntryOperator() == null) {
            sqlConsumer.accept(copyLogbookDataEntryOperatorToSql(oldTrip, newTripId));
        }
        if (newTrip.getLogbookDataQuality() == null) {
            sqlConsumer.accept(copyLogbookDataQualityToSql(oldTrip, newTripId));
        }
        if (oldTrip.getLogbookComment() != null && newTrip.getLogbookComment() == null) {
            sqlConsumer.accept(copyLogbookCommentToSql(commentFormat, oldTrip, newTripId));
        }
    }

    public String copyLocalmarketMetadataToSql(Trip trip, String newId) {
        AcquisitionStatus localMarketAcquisitionStatus = trip.getLocalMarketAcquisitionStatus();
        AcquisitionStatus localMarketSurveySamplingAcquisitionStatus = trip.getLocalMarketSurveySamplingAcquisitionStatus();
        AcquisitionStatus localMarketWellsSamplingAcquisitionStatus = trip.getLocalMarketWellsSamplingAcquisitionStatus();
        Float localMarketTotalWeight = trip.getLocalMarketTotalWeight();
        return fillSql(queryCopyLocalmarketMetadata(toId(localMarketAcquisitionStatus),
                                                    toId(localMarketSurveySamplingAcquisitionStatus),
                                                    toId(localMarketWellsSamplingAcquisitionStatus),
                                                    localMarketTotalWeight,
                                                    newId
        ));
    }

    public String removeLocalmarketMetadataToSql(String id) {
        return fillSql(queryRemoveLocalmarketMetadata(id));
    }

    private ActivityStubDto toActivityStubDto(Object[] set, Map<String, VesselActivityReference> vesselActivities) {
        ActivityStubDto activity = new ActivityStubDto();
        activity.setId((String) set[0]);
        Date date = (Date) set[1];
        Time time = (Time) set[2];
        if (time == null) {
            activity.setTime(Dates.getDay(date));
        } else {
            activity.setTime(Dates.getDateAndTime(date, time, false, false));
        }
        activity.setNumber((Integer) set[3]);
        String vesselActivityId = (String) set[4];
        VesselActivityReference vesselActivity = vesselActivities.get(vesselActivityId);
        activity.setVesselActivity(vesselActivity);
        return activity;
    }

    public String updateTripVersionToSql(String tripId, Timestamp now) {
        return fillSql(queryUpdateVersion(now, tripId));
    }
}
