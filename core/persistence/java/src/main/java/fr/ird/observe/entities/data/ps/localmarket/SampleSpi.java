package fr.ird.observe.entities.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.localmarket.SampleDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.referential.ps.common.SampleType;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.Set;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleSpi extends GeneratedSampleSpi {

    @Override
    public Form<SampleDto> preCreate(ServiceContext context, Trip parent, Sample preCreated) {
        preCreated.setSampleType(SampleType.loadEntity(context, ProtectedIdsPs.PS_LOCAL_MARKET_SAMPLE_DEFAULT_SAMPLE_TYPE_ID));
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public void fromDto(ReferentialLocale referentialLocale, Sample entity, SampleDto dto) {
        Set<String> well = entity.getWell();
        super.fromDto(referentialLocale, entity, dto);
        if (well != null) {
            well.clear();
            well.addAll(dto.getWell());
            entity.setWell(well);
        }
    }
//
//    @Override
//    public void moveCallback(Trip oldParent, Trip newParent, List<Sample> moved) {
//        //FIXME Copy missing meta-data to new parent?
//    }
}
