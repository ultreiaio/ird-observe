package fr.ird.observe.entities.data.ps.localmarket;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.localmarket.BatchReference;
import fr.ird.observe.dto.data.ps.localmarket.SurveyDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SurveySpi extends GeneratedSurveySpi {

    @Override
    public Form<SurveyDto> preCreate(ServiceContext context, Trip parent, Survey preCreated) {
        preCreated.setNumber(parent.getLocalmarketSurveySize() + 1);
//        Form<SurveyDto> form = super.preCreate(context, parent, preCreated);
//        form.getObject().setNumber(parent.getLocalmarketSurveySize() + 1);
        //FIXME super.preCreate does already this?
//        fillFormObject(context.getReferentialLocale(), parent, preCreated, form.getObject());
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public void loadDtoForValidation(ServiceContext context, Trip parent, Survey entity, SurveyDto dto) {
        fillFormObject(context.getReferentialLocale(), parent, entity, dto);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Trip parent, Survey entity, SurveyDto dto, boolean needCopy) {

        // Get all batch ids associated to this survey
        Set<String> newBatchIds = dto.getBatches().stream().map(BatchReference::getId).collect(Collectors.toSet());
        Collection<Batch> localmarketBatches = parent.getLocalmarketBatch();
        if (dto.isPersisted()) {
            // Remove from batches survey if equals to this one
            String surveyId = dto.getId();
            localmarketBatches.stream()
                              .filter(b -> b.getSurvey() != null && surveyId.equals(b.getSurvey().getTopiaId()))
                              .forEach(b -> b.setSurvey(null));
        }
        if (needCopy) {
            fromDto(context, entity, dto);
        }
        // Set new association on batches associated to this survey
        localmarketBatches.stream().filter(b -> newBatchIds.contains(b.getTopiaId())).forEach(b -> b.setSurvey(entity));
        super.onSave(context, parent, entity, dto, false);
        return s -> s.updateCollection(SurveyPart.SPI, entity.getSurveyPart());
    }

//    @Override
//    public void moveCallback(Trip oldParent, Trip newParent, List<Survey> moved) {
//        //FIXME Copy missing meta-data to new parent?
//    }

//
//    @Override
//    public void saveCallback(AddEntityToUpdateStep saveHelper, Survey entity) {
//        saveHelper.updateCollection(SurveyPart.SPI, entity.getSurveyPart());
//    }

    public void fillFormObject(ReferentialLocale referentialLocale, Trip trip, Survey survey, SurveyDto dto) {
        Collection<Batch> availableBatches = new LinkedList<>();
        Collection<Batch> selectedBatches = new LinkedList<>();
        if (trip.isLocalmarketBatchNotEmpty()) {
            for (Batch batch : trip.getLocalmarketBatch()) {
                Survey batchSurvey = batch.getSurvey();
                if (Objects.equals(batchSurvey, survey)) {
                    // association this with survey
                    selectedBatches.add(batch);
                } else if (batchSurvey == null) {
                    // available batch (associated with no survey)
                    availableBatches.add(batch);
                }
            }
        }
        dto.setAvailableBatches(Batch.toReferenceSet(referentialLocale, availableBatches, null, null).toList());
        dto.setBatches(Batch.toReferenceSet(referentialLocale, selectedBatches, null, null).toList());
    }
}
