package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.datasource.request.DeleteRequest;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.request.DeleteRequestCallback;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static fr.ird.observe.entities.data.ps.logbook.ActivityTopiaDao.ActivityNumberRecord;

/**
 * Call back after deleting an activity.
 * <p>
 * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2784">issue 2784</a>
 * Created at 14/09/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
@AutoService(DeleteRequestCallback.class)
public class ActivityDeleteRequestCallback implements DeleteRequestCallback {
    private static final Logger log = LogManager.getLogger(ActivityDeleteRequestCallback.class);

    @Override
    public boolean accept(DeleteRequest request) {
        return request.getDataType().equals(Activity.class.getName());
    }

    @Override
    public void consume(DeleteRequest request, SqlRequestSetConsumerContext context) {
        recomputeActivityNumber(context, request.getParentId(), request.getDataIds());
    }

    private void recomputeActivityNumber(SqlRequestSetConsumerContext context, String routeId, Set<String> deletedIds) {
        // get all activities of the route (without the deleted one)
        // the position of each record is now exactly the related activity number
        ActivityTopiaDao dao = Activity.getDao(context.getSourcePersistenceContext());
        List<ActivityNumberRecord> records = dao.activityTimeAndOrderRecordByNumber(routeId)
                                                .stream().filter(r -> !deletedIds.contains(r.getId())).collect(Collectors.toList());
        if (records.isEmpty()) {
            return;
        }
        Timestamp now = context.nowTimestamp();
        SqlScriptWriter writer = context.getWriter();
        int number = 0;
        for (ActivityNumberRecord entry : records) {
            number++;
            int activityNumber = entry.getNumber();
            if (activityNumber != number) {
                String activityId = entry.getId();
                String updateSql = dao.generateUpdateNumberAndVersionSql(activityId, number, now);
                log.info("Change activity ({}) number (from {} → {}) callback sql: ({})", activityId, activityNumber, number, updateSql);
                writer.writeSql(updateSql);
            }
        }
    }
}
