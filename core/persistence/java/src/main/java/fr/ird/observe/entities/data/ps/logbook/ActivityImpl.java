package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsPs;
import io.ultreia.java4all.util.Dates;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 23/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ActivityImpl extends ActivityAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public long buildSetStatValue() {
        return getVesselActivity().isAllowSet() ? 1 : 0;
    }

    @Override
    public Date getDate() {
        return getTime() == null ? null : Dates.getDay(getTime());
    }

    /**
     * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2663">issue 2663</a>
     *
     * @return {@code true} if coordinate are required, {@code false} otherwise.
     */
    @Override
    public boolean isCoordinateRequiredForFloatingObjects() {
        if (getFloatingObject() == null) {
            return true;
        }
        List<TransmittingBuoy> transmittingBuoys = getFloatingObject().stream().filter(f -> f.getTransmittingBuoy() != null).flatMap(f -> f.getTransmittingBuoy().stream()).collect(Collectors.toList());
        if (transmittingBuoys.isEmpty()) {
            return true;
        }
        return !transmittingBuoys.stream().allMatch(t -> t.getTransmittingBuoyOperation() != null && ProtectedIdsPs.PS_COMMON_TRANSMITTING_BUOY_WITH_COORDINATES.contains(t.getTransmittingBuoyOperation().getTopiaId()));
    }

}
