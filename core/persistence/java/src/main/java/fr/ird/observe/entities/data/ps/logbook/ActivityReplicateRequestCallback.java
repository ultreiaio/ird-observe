package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.datasource.request.ReplicateRequest;
import fr.ird.observe.dto.ToolkitParentIdDtoBean;
import fr.ird.observe.dto.data.ps.ActivitiesAcquisitionMode;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.entities.data.ps.common.Trip;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.request.ReplicateRequestCallback;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static fr.ird.observe.entities.data.ps.logbook.ActivityTopiaDao.ActivityNumberRecord;

/**
 * Created at 14/09/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
@AutoService(ReplicateRequestCallback.class)
public class ActivityReplicateRequestCallback implements ReplicateRequestCallback {
    private static final Logger log = LogManager.getLogger(ActivityReplicateRequestCallback.class);

    @Override
    public boolean accept(ReplicateRequest request) {
        return request.getDataType().equals(Activity.class.getName());
    }

    @Override
    public void consume(ReplicateRequest request, Map<String, String> replaceIds, SqlRequestSetConsumerContext context) {

        ToolkitParentIdDtoBean tripBean = Route.SPI.getParentId((ToolkitTopiaPersistenceContextSupport) context.getSourcePersistenceContext(), request.getOldParentId());
        Trip trip = Trip.SPI.loadEntity(Locale.FRANCE, context.getSourcePersistenceContext(), tripBean.getId());
        ActivitiesAcquisitionMode activitiesAcquisitionMode = trip.getActivitiesAcquisitionMode();

        switch (activitiesAcquisitionMode) {
            case BY_NUMBER:
                onSaveWithByNumberActivitiesAcquisitionMode(request, replaceIds, context);
                break;
            case BY_TIME:
                onSaveWithByTimeActivitiesAcquisitionMode(request, replaceIds, context);
                break;
        }
    }

    private void onSaveWithByNumberActivitiesAcquisitionMode(ReplicateRequest request, Map<String, String> replaceIds, SqlRequestSetConsumerContext context) {
        Set<String> dataIds = request.getDataIds();
        // find out old activities record for only dataIds
        ActivityTopiaDao dao = Activity.getDao(context.getSourcePersistenceContext());
        List<ActivityNumberRecord> oldActivityTimeAndOrderRecords = dao.activityTimeAndOrderRecordByNumber(request.getOldParentId()).stream().filter(e -> dataIds.contains(e.getId())).collect(Collectors.toList());
        // get the size of activities in the new parent route, will start at this size plus one for the first activity replicated
        int newNumber = dao.activityTimeAndOrderRecordByNumber(request.getNewParentId()).size();
        Timestamp now = context.nowTimestamp();
        SqlScriptWriter writer = context.getWriter();
        for (ActivityNumberRecord entry : oldActivityTimeAndOrderRecords) {
            // the new number to use for replicated activity
            newNumber++;
            int activityNumber = entry.getNumber();
            if (activityNumber == newNumber) {
                // nothing to change, same numbers
                continue;
            }
            // need to update the new activity number
            String oldTopiaId = entry.getId();
            String newTopiaId = Objects.requireNonNull(replaceIds.get(oldTopiaId));
            String updateSql = dao.generateUpdateNumberAndVersionSql(newTopiaId, newNumber, now);
            log.info("Change activity ({}) number (from {} → {}) callback sql: ({})", newTopiaId, activityNumber, newNumber, updateSql);
            writer.writeSql(updateSql);
        }
    }

    private void onSaveWithByTimeActivitiesAcquisitionMode(ReplicateRequest request, Map<String, String> replaceIds, SqlRequestSetConsumerContext context) {
        Set<String> dataIds = request.getDataIds();
        ActivityTopiaDao dao = Activity.getDao(context.getSourcePersistenceContext());
        // find out old activities record for only dataIds order by time/topiaCreateDate
        List<ActivityNumberRecord> oldActivityTimeAndOrderRecords = dao.activityTimeAndOrderRecordByTime(request.getOldParentId()).stream().filter(e -> dataIds.contains(e.getId())).collect(Collectors.toList());

        // find out new route activities record
        List<ActivityNumberRecord> newActivityTimeAndOrderRecords = dao.activityTimeAndOrderRecordByTime(request.getNewParentId());
        // add replicated activities
        newActivityTimeAndOrderRecords.addAll(oldActivityTimeAndOrderRecords);

        // sort the list by time (with fallback to topiaCreateDate fi some time are equals)
        // The position of each record is now exactly the related activity number
        newActivityTimeAndOrderRecords.sort(Comparator.comparing(ActivityNumberRecord::getTime).thenComparing(ActivityNumberRecord::getTopiaCreateDate));

        Timestamp now = context.nowTimestamp();
        SqlScriptWriter writer = context.getWriter();
        int newNumber = 0;
        for (ActivityNumberRecord record : newActivityTimeAndOrderRecords) {
            // the new number for current activity
            newNumber++;
            int activityNumber = record.getNumber();
            if (activityNumber == newNumber) {
                // nothing to change, same numbers
                continue;
            }
            String activityId = record.getId();
            // Need to get replace id if was a replicated activity, otherwise keep the same id (means we are updating a not replicated activity on target route)
            String targetId = dataIds.contains(activityId) ? Objects.requireNonNull(replaceIds.get(activityId)) : activityId;
            String updateSql = dao.generateUpdateNumberAndVersionSql(targetId, newNumber, now);
            log.info("Change activity ({}) number (from {} → {}) callback sql: ({})", targetId, activityNumber, newNumber, updateSql);
            writer.writeSql(updateSql);
        }

    }
}
