package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.ActivitiesAcquisitionMode;
import fr.ird.observe.dto.data.ps.logbook.ActivityDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityReference;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.referential.common.FpaZone;
import fr.ird.observe.entities.referential.ps.logbook.InformationSource;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.Dates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ActivitySpi extends GeneratedActivitySpi {

    private static final Logger log = LogManager.getLogger(ActivitySpi.class);

    @Override
    public ActivityReference toReference(ReferentialLocale referentialLocale, Activity entity, String classifier) {
        ActivityReference reference = super.toReference(referentialLocale, entity, classifier);
        if (entity != null) {
            if (entity.isFloatingObjectNotEmpty()) {
                reference.setFloatingObject(FloatingObject.toReferenceSet(referentialLocale, entity.getFloatingObject(), new Date(), classifier).toList());
            }
            Date time = reference.getTime();
            if (time != null) {
                time = new java.sql.Time(time.getTime());
                if (time.toString().equals("00:00:00")) {
                    reference.setTime(null);
                }
            }
        }
        return reference;
    }

    @Override
    public Form<ActivityDto> preCreate(ServiceContext context, Route parent, Activity preCreated) {
        Activity lastActivity = Iterables.getLast(parent.getActivity(), null);
        Date time;
        FpaZone currentFpaZone = null;
        int number;
        if (lastActivity == null) {
            // first activity, use current time
            time = context.now();
            number = 1;
        } else {
            // use last activity time
            time = lastActivity.getTime();
            // use last activity fpa zone
            currentFpaZone = lastActivity.getNextFpaZone();
            if (currentFpaZone == null) {
                currentFpaZone = lastActivity.getCurrentFpaZone();
            }
            number = parent.getActivitySize() + 1;
        }
        if (time != null) {
            preCreated.setTime(Dates.getTime(time, false, false));
        }
        preCreated.setCurrentFpaZone(currentFpaZone);
        preCreated.setNumber(number);
        preCreated.setSetCount(1);
        preCreated.setInformationSource(InformationSource.loadEntity(context, ProtectedIdsPs.PS_LOGBOOK_ACTIVITY_DEFAULT_INFORMATION_SOURCE_ID));
        //FIXME super.preCreate does already this?
//        Form<ActivityDto> form = super.preCreate(context, parent, preCreated);
//        addRouteDate(parent, form.getObject());
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public void loadDtoForValidation(ServiceContext context, Route parent, Activity entity, ActivityDto dto) {
        addRouteDate(parent, dto);
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2663
        dto.setCoordinateRequiredForFloatingObjects(entity.isCoordinateRequiredForFloatingObjects());
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Route parent, Activity entity, ActivityDto dto, boolean needCopy) {
        if (!dto.isCatchesEnabled() && entity.isCatchesNotEmpty()) {
            log.info(String.format("Activity does not support any more catches, will then remove %d catch(es).", entity.getCatchesSize()));
            for (Catch aCatch : entity.getCatches()) {
                log.info(String.format("Remove catch: %s", aCatch.getTopiaId()));
            }
            entity.clearCatches();
        }
        if (!dto.isFloatingObjectEnabled() && entity.isFloatingObjectNotEmpty()) {
            log.info(String.format("Activity does not support any more floating objects, will then remove %d floating object(s).", entity.getFloatingObjectSize()));
            for (FloatingObject floatingObject : entity.getFloatingObject()) {
                log.info(String.format("Remove floatingObject: %s", floatingObject.getTopiaId()));
            }
            entity.clearFloatingObject();
        }
        Trip trip = Route.SPI.getParent(context, parent.getId());
        ActivitiesAcquisitionMode activitiesAcquisitionMode = trip.getActivitiesAcquisitionMode();
        boolean notPersisted = dto.isNotPersisted();
        int previousNumber = notPersisted ? parent.getActivitySize() + 1 : entity.getNumber();
        super.onSave(context, parent, entity, dto, needCopy);
        Set<Activity> activitiesSet = Set.copyOf(parent.getActivity());
        boolean needToSaveOtherActivities = false;
        switch (activitiesAcquisitionMode) {
            case BY_NUMBER:
                needToSaveOtherActivities = onSaveWithByNumberActivitiesAcquisitionMode(entity, activitiesSet, previousNumber);
                break;
            case BY_TIME:
                needToSaveOtherActivities = onSaveWithByTimeActivitiesAcquisitionMode(entity, activitiesSet);
                break;
        }
        if (needToSaveOtherActivities) {
            // add to save callback all other activities
            return s -> s.updateCollection(Activity.SPI, parent.getActivity(), false);
        }
        return null;
    }

    @Override
    protected BiConsumer<Activity, ActivityReference> getChildrenExtraConsumer(Route parent) {
        return (entity, reference) -> {
            reference.setDate(parent.getDate());
        };
    }

    void addRouteDate(Route parent, ActivityDto dto) {
        dto.setDate(parent.getDate());
    }

    /**
     * To reo-order the {@link Activity#getNumber()} of the given set of activities when using the indexed acquisition mode.
     * <br/>
     * <h2>Preamble notes:</h2>
     * <ul>
     *     <li>The {@code previousNumber} is the value of field {@code number} before any modification.</li>
     *     <li>For a not persisted activity, the default value of field {@code number} is size of previous activities
     *     set plus one, so the {@code previousNumber} is that value. In that way we are able to treat not persisted
     *     activity like a persisted one in the algorithm.</i>
     *     <li>The {@code newNumber} is the value of field {@code number} modified by user.</li>
     *     <li>The parameter {@code activitiesSet} represents the set of all activities of the route containing any modification on the editing activity.</li>
     *     <li>The {@code orderedActivitiesList} is this set sorted by his {@link Activity#getNumber()}.</li>
     *     <li>In all examples, we consider this {@code activitiesSet} (the number is between parenthesis) before any modification:
     *     <ul>
     *         <li>A(1)</li>
     *         <li>B(2)</li>
     *         <li>C(3)</li>
     *         <li>D(4)</li>
     *         <li>E(5)</li>
     *     </ul>
     *     </li>
     * </ul>
     * <h2>Algorithm</h2>
     * <p>The algorithm distinguish three cases:</p>
     * <h3>First case: {@code previousNumber == newNumber}</h3>
     * <p>Nothing to do, since the number has not changed.</p>
     * <p>For a not persisted activity, the behaviour is correct since the previous number was set to his default value, </p>
     * <h3>Second case: {@code previousNumber < newNumber}</h3>
     * <p>The behaviour is to permute the modified activity with the existing one having number equals to {@code newNumber}.</p>
     * <h4>Example with a persisted activity:</h4>
     * <p>We edit activity {@code B(2)} and modify his number to <b>4</b>, before saving the {@code orderedActivitiesList} is:</p>
     * <ul>
     *     <li>A(1)</li>
     *     <li>C(3)</li>
     *     <li><b>B(4)</b> → <i>this is the correct number value to keep</i></li>
     *     <li>D(4) → <i>this one will become <b>D(2)</b></i></li>
     *     <li>E(5)</li>
     * </ul>
     * <p>After saving the result will be:</p>
     * <ul>
     *     <li>A(1)</li>
     *     <li><b>D(2)</b></li>
     *     <li>C(3)</li>
     *     <li><b>B(4)</b></li>
     *     <li>E(5)</li>
     * </ul>
     * <h4>Example with a not persisted activity:</h4>
     * <p>This case can not happen, since by default his number is the highest number possible.</p>
     * <h3>Third case: {@code previousNumber > newNumber}</h3>
     * <p>The behaviour is to interpose the modified activity just before the existing activity with number {@code newNumber}, and then to increment the number
     * of all activities from this existing activity to activity with number equals to {@code previousNumber - 1} (this is a {@code shift + 1} operation).</p>
     * <h4>Example with a persisted activity:</h4>
     * <p>We edit activity {@code D(4)} and modify his number to <b>2</b>, before saving the {@code orderedActivitiesList} is:</p>
     * <ul>
     *     <li>A(1)</li>
     *     <li>B(2) → <i>this one will become <b>B(3)</b></i></li>
     *     <li><b>D(2)</b> → <i>this is the correct number value to keep</i></li>
     *     <li>C(3) → <i>this one will become <b>C(4)</b></i></li>
     *     <li>E(5)</li>
     * </ul>
     * <p>After saving the result will be:</p>
     * <ul>
     *     <li>A(1)</li>
     *     <li><b>D(2)</b></li>
     *     <li><b>C(3)</b></li>
     *     <li><b>B(4)</b></li>
     *     <li>E(5)</li>
     * </ul>
     * <h4>Example with a not persisted activity:</h4>
     * <p>We create a new activity {@code F(6)} (his default number is <b>6</b>, since we had previously 5 activities on this route), and modify his number to <b>2</b>.</p>
     * <p>Before saving, the {@code orderedActivitiesList} is:</p>
     * <ul>
     *     <li>A(1)</li>
     *     <li>B(2) → <i>this one will become <b>B(3)</b></i></li>
     *     <li><b>F(2)</b> → <i>this is the correct number value to keep</i></li>
     *     <li>C(3) → <i>this one will become <b>C(4)</b></i></li>
     *     <li>D(4) → <i>this one will become <b>D(5)</b></i></li>
     *     <li>E(5) → <i>this one will become <b>E(6)</b></i></li>
     * </ul>
     * <p>After saving, activity {@code F(2)} was added and the result is:</p>
     * <ul>
     *     <li>A(1)</li>
     *     <li><b>F(2)</b></li>
     *     <li><b>B(3)</b></li>
     *     <li><b>C(4)</b></li>
     *     <li><b>D(5)</b></li>
     *     <li><b>E(6)</b></li>
     * </ul>
     *
     * @param entity         the current activity to save
     * @param activitiesSet  the set of activities of the route including modification of the editing activity
     * @param previousNumber the previous number of the current activity
     *                       (if activity was not persisted this is the default value says size of previous activities set plus one)
     * @return {@code true} if at least one activity number was changed, {@code false} otherwise
     */
    private boolean onSaveWithByNumberActivitiesAcquisitionMode(Activity entity, Set<Activity> activitiesSet, int previousNumber) {
        int newNumber = entity.getNumber();
        if (newNumber == previousNumber) {
            // no number changed
            return false;
        }
        // Sort the previous set of activities by his number to be sure the order is correct and does not depend of outside world of this method
        List<Activity> orderedActivityList = activitiesSet.stream()
                                                          .sorted(Comparator.comparing(Activity::getNumber))
                                                          .collect(Collectors.toList());
        boolean result = false;
        if (previousNumber < newNumber) {
            // need to change the other activity with number by previousNumber
            for (Activity activity : orderedActivityList) {
                if (Objects.equals(activity.getId(), entity.getId())) {
                    // this is the current activity to save, his number is already correct
                    continue;
                }
                int activityNumber = activity.getNumber();
                if (activityNumber == newNumber) {
                    activity.setNumber(previousNumber);
                    result = true;
                }
            }
            return result;
        }
        // in this case, previousNumber > number, we need to increment all number from number + 1 to previous number

        for (Activity activity : orderedActivityList) {
            if (Objects.equals(activity.getId(), entity.getId())) {
                // this is the current activity to save, his number is already correct
                continue;
            }
            int activityNumber = activity.getNumber();
            if (activityNumber > previousNumber) {
                // the activity number has no need to change
                continue;
            }
            if (activityNumber >= newNumber) {
                // the activity number need to be incremented
                activity.setNumber(activityNumber + 1);
                result = true;
            }
        }
        return result;
    }

    /**
     * To reo-order the {@link Activity#getNumber()} of the given set of activities when using the timestamping acquisition mode.
     * <p>
     * The algorithm is quite simple:
     * <ul>
     *     <li>Sort the given set of activities by the {@link Activity#getNumber()} (then by {@link Activity#getTopiaCreateDate()}
     *     if some activities has the same time)</li>
     *     <li>Walk on this list to set {@link Activity#getNumber()} using the order on the sorted list</li>
     * </ul>
     *
     * @param entity        the current activity to save
     * @param activitiesSet the previous set of activities (may or not include (if current activity is not
     *                      persisted)), ordered by the {@link Activity#getNumber()}.
     * @return {@code true} if at least one activity number was changed, {@code false} otherwise
     */
    private boolean onSaveWithByTimeActivitiesAcquisitionMode(Activity entity, Set<Activity> activitiesSet) {
        // order it by Activity.time, then by Activity.topiaCreateDate (if some activities has the same time)
        List<Activity> orderedActivityList = activitiesSet.stream()
                                                          .sorted(Comparator.comparing(Activity::getTime).thenComparing(Activity::getTopiaCreateDate))
                                                          .collect(Collectors.toList());
        boolean result = false;
        int number = 0;
        for (Activity activity : orderedActivityList) {
            number++;
            int activityNumber = activity.getNumber();
            if (activityNumber != number) {
                activity.setNumber(number);
                result = true;
            }
        }
        return result;
    }

    public void applyPairing(ObserveTopiaPersistenceContext context, Timestamp now, Map<String, String> activityMapping) {
        //FIXME Loosing timeZone ?
        ActivityTopiaDao dao = getDao(context);
        for (Map.Entry<String, String> entry : activityMapping.entrySet()) {
            dao.applyActivityPairing(now, entry.getValue(), entry.getKey());
        }
        updateLastUpdateDateTable(context, now);
    }
}
