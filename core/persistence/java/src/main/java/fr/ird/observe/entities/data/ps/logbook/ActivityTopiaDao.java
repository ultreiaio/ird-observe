package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

/**
 * Created at 14/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ActivityTopiaDao extends AbstractActivityTopiaDao<Activity> {

    public List<ActivityNumberRecord> activityTimeAndOrderRecordByNumber(String routeId) {
        return findMultipleResultBySqlQuery(queryActivitiesOrderByNumber(routeId), ActivityNumberRecord::new);
    }

    public List<ActivityNumberRecord> activityTimeAndOrderRecordByTime(String routeId) {
        return findMultipleResultBySqlQuery(queryActivitiesOrderByTime(routeId), ActivityNumberRecord::new);
    }

    public String generateUpdateNumberAndVersionSql(String activityId, int number, Timestamp now) {
        return fillSql(queryUpdateNumberAndVersion(number, now, activityId));
    }

    public int applyActivityPairing(Timestamp timestamp, String relatedObservedActivityId, String activityId) {
        return execute(queryApplyActivityPairing(timestamp, relatedObservedActivityId, activityId));
    }

    public static class ActivityNumberRecord {
        private final String id;
        private final int number;
        private final Time time;
        private final Timestamp topiaCreateDate;

        public ActivityNumberRecord(Object[] row) {
            this.id = (String) row[0];
            this.number = (int) row[1];
            this.time = (Time) row[2];
            this.topiaCreateDate = (Timestamp) row[3];
        }

        public String getId() {
            return id;
        }

        public int getNumber() {
            return number;
        }

        public Time getTime() {
            return time;
        }

        public Timestamp getTopiaCreateDate() {
            return topiaCreateDate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ActivityNumberRecord)) return false;
            ActivityNumberRecord that = (ActivityNumberRecord) o;
            return Objects.equals(id, that.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }
}
