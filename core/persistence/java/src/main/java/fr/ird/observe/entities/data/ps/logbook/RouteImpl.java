package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;

public class RouteImpl extends RouteAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public long buildFloatingObjectStatValue() {
        return isActivityEmpty() ? 0 : getActivity().stream().mapToLong(Activity::buildFloatingObjectStatValue).sum();
    }

    @Override
    public long buildSetStatValue() {
        return isActivityEmpty() ? 0 : getActivity().stream().mapToLong(Activity::buildSetStatValue).sum();
    }

    @Override
    public Date getTheoreticalEndDate() {
        return TheoreticalEndDateBuilder.of(getDate());
    }
}
