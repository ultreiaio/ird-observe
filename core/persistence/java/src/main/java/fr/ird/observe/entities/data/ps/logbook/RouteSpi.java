package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityReference;
import fr.ird.observe.dto.data.ps.logbook.RouteDto;
import fr.ird.observe.dto.data.ps.logbook.RouteReference;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.entities.data.TripEntityAware;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.Dates;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class RouteSpi extends GeneratedRouteSpi {

    @Override
    public Form<RouteDto> preCreate(ServiceContext context, Trip parent, Route preCreated) {
        Date date = parent.getRouteLogbook().stream()
                // get higher route date
                .map(Route::getDate).max(Date::compareTo)
                // if found use next day
                .map(d -> DateUtils.addDays(d, 1))
                // if not found use trip start date (first route)
                .orElse(parent.getStartDate());
        preCreated.setDate(Dates.getDay(date));
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Trip parent, Route entity, RouteDto dto, boolean needCopy) {
        return TripEntityAware.onSaveUpdateTripIfEndDateChanged(Trip.SPI, parent, entity, super.onSave(context, parent, entity, dto, needCopy));
    }

    @Override
    public Set<ToolkitIdDtoBean> getRealDependencies(ServiceContext context, Set<String> ids) {
        return Activity.SPI.byParentId(context, ids).map(id -> ToolkitIdDtoBean.of(ActivityDto.class, id)).collect(Collectors.toSet());
    }

    public Set<Activity> reorderLogbookActivitiesByTime(Route entity) {
        Set<Activity> modifiedActivities = new LinkedHashSet<>();
        List<Activity> activityListOrdered = entity.getActivity().stream()
                                                   .sorted(Comparator.comparing(Activity::getTime).thenComparing(Activity::getTopiaCreateDate))
                                                   .collect(Collectors.toList());
        int number = 0;
        for (Activity activity : activityListOrdered) {
            number++;
            int activityNumber = activity.getNumber();
            if (activityNumber != number) {
                // activity number has changed
                activity.setNumber(number);
                // keep it as modified (we will later in trip save process update their lastUpdateDate)
                modifiedActivities.add(activity);
            }
        }
        return modifiedActivities;
    }

    public Map<RouteReference, List<ActivityReference>> getActivitiesByRouteId(ServiceContext context, Map<String, VesselActivityReference> vesselActivities, TripDto trip, Map<fr.ird.observe.dto.data.ps.observation.RouteReference, List<fr.ird.observe.dto.data.ps.observation.ActivityReference>> observationRoutes) {
        return getDao(context).getActivitiesByRoute(vesselActivities, trip, observationRoutes);
    }
}
