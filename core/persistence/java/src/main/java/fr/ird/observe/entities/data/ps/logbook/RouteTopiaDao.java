package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityReference;
import fr.ird.observe.dto.data.ps.logbook.RouteReference;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.entities.Entities;
import org.nuiton.topia.persistence.support.QuerySupport;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created at 18/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class RouteTopiaDao extends AbstractRouteTopiaDao<Route> {

    public Map<RouteReference, List<ActivityReference>> getActivitiesByRoute(Map<String, VesselActivityReference> vesselActivities,
                                                                             TripDto trip,
                                                                             Map<fr.ird.observe.dto.data.ps.observation.RouteReference, List<fr.ird.observe.dto.data.ps.observation.ActivityReference>> observationRoutes) {
        List<fr.ird.observe.dto.data.ps.observation.ActivityReference> observationActivities = new LinkedList<>();
        observationRoutes.values().forEach(observationActivities::addAll);

        Map<String, fr.ird.observe.dto.data.ps.observation.ActivityReference> observationActivitiesById = observationActivities.stream().collect(Collectors.toMap(fr.ird.observe.dto.data.ps.observation.ActivityReference::getId, Function.identity()));
        return new QuerySupport.MapBuilder<>(new TreeMap<>(Comparator.comparing(RouteReference::getDate)), trip.getRouteLogbook(), (currentKey, row) -> {
            ActivityReference value = new ActivityReference();
            value.setDate(currentKey.getDate());
            int index = 0;
            value.setId((String) row[++index]);
            value.setLastUpdateDate((java.util.Date) row[++index]);
            value.setTopiaVersion(Entities.getPrimitiveLong(row, ++index));
            value.setTopiaCreateDate((java.util.Date) row[++index]);
            value.setNumber(Entities.getInteger(row, ++index));
            value.setTime((java.util.Date) row[++index]);
            value.setLatitude(Entities.getFloat(row, ++index));
            value.setLongitude(Entities.getFloat(row, ++index));
            String vesselActivityId = (String) row[++index];
            value.setVesselActivity(vesselActivityId == null ? null : vesselActivities.get(vesselActivityId));
            String relatedObservedActivityId = (String) row[++index];
            fr.ird.observe.dto.data.ps.observation.ActivityReference relatedObservedActivity = null;
            if (relatedObservedActivityId != null) {
                relatedObservedActivity = observationActivitiesById.get(relatedObservedActivityId);
            }
            value.setRelatedObservedActivity(relatedObservedActivity);
            return value;
        }).build(this.<Object[]>queryGetActivitiesByRouteId(trip.getId()).stream());
    }
}
