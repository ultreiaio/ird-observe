package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.ps.logbook.SampleDto;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.spi.service.ServiceContext;
import fr.ird.observe.spi.validation.callback.EntityInterceptorCallback;

import java.util.Deque;

/**
 * Created at 22/05/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.5
 */
@SuppressWarnings("rawtypes")
@AutoService(EntityInterceptorCallback.class)
public class SampleInterceptorCallback implements EntityInterceptorCallback<SampleDto, Sample, SampleSpi> {

    @Override
    public boolean acceptType(Class<? extends BusinessDto> dtoType, Class<? extends Entity> entityType) {
        return Sample.class.equals(entityType);
    }

    @Override
    public void loadDtoForValidation(ServiceContext context, Deque<Entity> path, SampleSpi spi, Sample sample, SampleDto dto) {
        Trip trip = EntityInterceptorCallback.getParent(path, sample, Trip.class);
        spi.loadDtoForValidation(context, trip, dto);
    }
}
