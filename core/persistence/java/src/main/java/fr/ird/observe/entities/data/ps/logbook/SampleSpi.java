package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.data.ps.logbook.SampleDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.referential.ps.common.SampleType;
import fr.ird.observe.entities.referential.ps.logbook.SampleQuality;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SampleSpi extends GeneratedSampleSpi {
    @Override
    public void loadDtoForValidation(ServiceContext context, Trip parent, Sample entity, SampleDto dto) {
        dto.setActivity(Trip.SPI.getLogbookSetActivities(context, parent.getId()));
    }

    @Override
    public void toDto(ReferentialLocale referentialLocale, Sample entity, SampleDto dto) {
        super.toDto(referentialLocale, entity, dto);
        if (entity.isSampleSpeciesNotEmpty()) {
            // compute sampleSpeciesSubNumber
            int sampleSpeciesSubNumber = getSampleSpeciesSubNumber(entity);
            dto.setSampleSpeciesSubNumber(sampleSpeciesSubNumber);
        }
    }

    @Override
    public Form<SampleDto> preCreate(ServiceContext context, Trip parent, Sample preCreated) {
        preCreated.setSampleType(SampleType.loadEntity(context, ProtectedIdsPs.PS_LOGBOOK_SAMPLE_DEFAULT_SAMPLE_TYPE_ID));
        preCreated.setSampleQuality(SampleQuality.loadEntity(context, ProtectedIdsPs.PS_LOGBOOK_SAMPLE_DEFAULT_SAMPLE_QUALITY_ID));
        preCreated.setSuperSample(false);
        //FIXME super.preCreate does already this?
//        Form<SampleDto> form = super.preCreate(context, parent, preCreated);
//        form.getObject().setActivity(Trip.SPI.getLogbookSetActivities(context, parent.getTopiaId()));
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Trip parent, Sample entity, SampleDto dto, boolean needCopy) {
        boolean superSampleChanged = !Objects.equals(entity.isSuperSample(), dto.isSuperSample());
        if (superSampleChanged && entity.isSampleSpeciesNotEmpty()) {
            // superSample changed (adapt)

            if (dto.isSuperSample()) {
                // set all subSampleNumber to 1
                entity.getSampleSpecies().forEach(sampleSpecies -> sampleSpecies.setSubSampleNumber(1));
            } else {
                int sampleSpeciesSubNumber = getSampleSpeciesSubNumber(entity);
                if (sampleSpeciesSubNumber != 1) {
                    throw new IllegalStateException("Can't become not superSample and have more than one sample species sub number.");
                }
                // set all subSampleNumber to 0
                entity.getSampleSpecies().forEach(sampleSpecies -> sampleSpecies.setSubSampleNumber(0));
            }
        }
        Consumer<AddEntityToUpdateStep> result = super.onSave(context, parent, entity, dto, needCopy);
        SampleActivity.SPI.initId(context, entity.getSampleActivity());
        return result;
    }

    public void loadDtoForValidation(ServiceContext context, Trip parent, SampleDto dto) {
        List<ActivityStubDto> logbookSetActivities = new LinkedList<>();
        for (Route route : parent.getRouteLogbook()) {
            List<ActivityStubDto> routeActivities = Activity.ACTIVITY_STUB_SPI.toDataDtoList(context.getReferentialLocale(), route.getActivity());
            logbookSetActivities.addAll(routeActivities);
        }
        dto.setActivity(logbookSetActivities);
    }

    private int getSampleSpeciesSubNumber(Sample entity) {
        return (int) entity.getSampleSpecies().stream().mapToInt(SampleSpecies::getSubSampleNumber).distinct().count();
    }

//FIXME Add a callback in MoveRequest
//    @Override
//    public void moveCallback(Trip oldParent, Trip newParent, List<Sample> moved) {
//        //FIXME Copy missing meta-data to new parent?
//    }
//
//    @Override
//    public void moveCallback(Trip oldParent, Trip newParent, Sample moved) {
//        // changing trip → no more activities
//        moved.clearSampleActivity();
//    }
}
