package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorProvider;

import java.util.Collection;

public class WellActivityImpl extends WellActivityAbstract {
    private static final long serialVersionUID = 1L;

    @Override
    public double getTotalWeight(Activity activity, Collection<String> speciesIds) {
        return getActivity().equals(activity) ? getTotalWeight(speciesIds) : 0;
    }

    @Override
    public double getTotalWeight(Collection<String> speciesIds) {
        return getWellActivitySpecies().stream().filter(was -> speciesIds.contains(was.getSpecies().getId())).mapToDouble(WellActivitySpecies::getWeight).sum();
    }

    @Override
    public void registerDecorator(Decorator decorator) {
        super.registerDecorator(decorator);
        Activity activity = getActivity();
        if (activity != null) {
            DecoratorProvider.registerDecorator(Activity.class, decorator.getLocale(), activity);
        }
    }
} //WellActivityImpl
