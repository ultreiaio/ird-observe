package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;

/**
 * Created on 14/04/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
public class WellImpl extends WellAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public double getTotalWeight(Activity activity, Collection<String> speciesIds) {
        return getWellActivity().stream().mapToDouble(wa -> wa.getTotalWeight(activity, speciesIds)).sum();
    }

    @Override
    public double getTotalWeight(Activity activity, Collection<String> speciesIds, Collection<String> wellIds) {
        return wellIds.contains(getWell()) ? getTotalWeight(activity, speciesIds) : 0;
    }
}
