package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.logbook.WellDto;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.Set;
import java.util.function.Consumer;

public class WellSpi extends GeneratedWellSpi {

//    @Override
//    public void saveCallback(AddEntityToUpdateStep saveHelper, Well entity) {
//        Set<WellActivity> wellActivity = entity.getWellActivity();
//        saveHelper.updateCollection(WellActivity.SPI, wellActivity);
//        wellActivity.forEach(c -> saveHelper.updateCollection(WellActivitySpecies.SPI, c.getWellActivitySpecies(), false));
//        saveHelper.updateLastUpdateDateTable(WellActivitySpecies.SPI);
//    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Trip parent, Well entity, WellDto dto, boolean needCopy) {
        Consumer<AddEntityToUpdateStep> result = andThen(super.onSave(context, parent, entity, dto, needCopy), s -> {
            Set<WellActivity> wellActivity = entity.getWellActivity();
            s.updateCollection(WellActivity.SPI, wellActivity);
            wellActivity.forEach(c -> s.updateCollection(WellActivitySpecies.SPI, c.getWellActivitySpecies(), false));
            s.updateLastUpdateDateTable(WellActivitySpecies.SPI);
        });
        Set<WellActivity> wellActivity = entity.getWellActivity();
        if (wellActivity != null) {
            WellActivity.SPI.initId(context, wellActivity);
            wellActivity.forEach(activity -> WellActivitySpecies.SPI.initId(context, activity.getWellActivitySpecies()));
        }
        return result;
    }
}
