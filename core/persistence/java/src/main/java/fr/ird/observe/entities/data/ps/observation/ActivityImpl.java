/*
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.data.ps.observation;

import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import io.ultreia.java4all.util.Dates;

import java.util.Date;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ActivityImpl extends ActivityAbstract {

    private static final long serialVersionUID = 2L;

    @Override
    public boolean isActivityEndOfSearching() {
        return VesselActivityReference.isEndOfSearchingOperation(vesselActivity);
    }

    @Override
    public Date getDate() {
        return Dates.getDay(getTime());
    }

    @Override
    public Date getTheoreticalEndDate() {
        return TheoreticalEndDateBuilder.of(getSet());
    }
}


