package fr.ird.observe.entities.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.ps.observation.ActivityDto;
import fr.ird.observe.dto.data.ps.observation.ActivityReference;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.entities.data.TripEntityAware;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.referential.common.FpaZone;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.Dates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ActivitySpi extends GeneratedActivitySpi {
    private static final Logger log = LogManager.getLogger(ActivitySpi.class);

    @Override
    public void toDto(ReferentialLocale referentialLocale, Activity entity, ActivityDto dto) {
        super.toDto(referentialLocale, entity, dto);
        java.util.Set<SpeciesReference> nonTargetSpecies = new LinkedHashSet<>();
        Set set = entity.getSet();
        if (set != null && set.isCatchesNotEmpty()) {
            Stream<Species> speciesStream = set.getCatches().stream().map(Catch::getSpecies);
            nonTargetSpecies.addAll(Species.toReferenceSet(referentialLocale, speciesStream, null, null).toSet());
        }
        dto.setNonTargetCatchSpecies(nonTargetSpecies);
    }

    @Override
    public ActivityReference toReference(ReferentialLocale referentialLocale, Activity entity, String classifier) {
        ActivityReference reference = super.toReference(referentialLocale, entity, classifier);
        if (entity != null && entity.isFloatingObjectNotEmpty()) {
            reference.setFloatingObject(FloatingObject.SPI.toReferenceSet(referentialLocale, entity.getFloatingObject(), new Date(), classifier).toList());
        }
        if (entity != null && DecoratorService.WITH_STATS_CLASSIFIER.equals(classifier) && reference.getSet() != null) {
            entity.getSet().buildStatistics(reference.getSet());
        }
        return reference;
    }

    @Override
    public Form<ActivityDto> preCreate(ServiceContext context, Route parent, Activity preCreated) {
        Activity lastActivity = Iterables.getLast(parent.getActivity(), null);
        Date time;
        FpaZone currentFpaZone = null;
        if (lastActivity == null) {
            // first activity, use current time
            time = preCreated.getTopiaCreateDate();
        } else {
            // use last activity time
            time = lastActivity.getTime();
            // use last activity fpa zone
            currentFpaZone = lastActivity.getNextFpaZone();
            if (currentFpaZone == null) {
                currentFpaZone = lastActivity.getCurrentFpaZone();
            }
        }
        preCreated.setTime(Dates.getTime(time, false, false));
        preCreated.setCurrentFpaZone(currentFpaZone);
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Route parent, Activity entity, ActivityDto dto, boolean needCopy) {
        Trip trip = Route.SPI.getParent(context, parent.getId());
        if (!dto.isSetEnabled() && entity.getSet() != null) {
            log.info("Remove set: {}", entity.getSet().getTopiaId());
            entity.setSet(null);
        }
        if (!dto.isFloatingObjectEnabled() && entity.isFloatingObjectNotEmpty()) {
            for (FloatingObject floatingObject : entity.getFloatingObject()) {
                log.info("Remove floatingObject: {}", floatingObject.getTopiaId());
            }
            entity.clearFloatingObject();
        }
        return TripEntityAware.onSaveUpdateTripIfEndDateChanged(Trip.SPI, trip, entity, super.onSave(context, parent, entity, dto, needCopy));
    }

    @Override
    protected BiConsumer<Activity, ActivityReference> getChildrenExtraConsumer(Route parent) {
        return (entity, reference) -> {
            reference.setDate(parent.getDate());
//            if (reference.getSet() != null) {
//                entity.getSet().buildStatistics(reference.getSet());
//            }
        };
    }

}
