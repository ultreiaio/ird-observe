/*
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.data.ps.observation;

import fr.ird.observe.entities.referential.common.LengthWeightParameter;

/**
 * L'implantation par defaut d'une discarded faune.
 * <p>
 * Une discarded faune implante le contrat {@link LengthWeightParameter} sur les champs
 * <ul>
 * <li>{@link #meanWeight}</li>
 * <li>{@link #meanLength}</li>
 * </ul>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class CatchImpl extends CatchAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public boolean withCatchWeight() {
        return getCatchWeight() != null;
    }

    @Override
    public boolean withoutCatchWeight() {
        return getCatchWeight() == null;
    }

    @Override
    public boolean withMeanWeight() {
        return getMeanWeight() != null;
    }

    @Override
    public boolean withoutMeanWeight() {
        return getMeanWeight() == null;
    }

    @Override
    public boolean withTotalCount() {
        return getTotalCount() != null;
    }

    @Override
    public boolean withoutTotalCount() {
        return getTotalCount() == null;
    }

    @Override
    public boolean withMeanLength() {
        return getMeanLength() != null;
    }

    @Override
    public boolean withoutMeanLength() {
        return getMeanLength() == null;
    }

    @Override
    public boolean isCatchWeightComputed() {
        return catchWeightComputedSource != null;
    }

    @Override
    public boolean isMeanWeightComputed() {
        return meanWeightComputedSource != null;
    }

    @Override
    public boolean isTotalCountComputed() {
        return totalCountComputedSource != null;
    }

    @Override
    public boolean isMeanLengthComputed() {
        return meanLengthComputedSource != null;
    }
}
