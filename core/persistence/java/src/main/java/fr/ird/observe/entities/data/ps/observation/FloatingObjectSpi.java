package fr.ird.observe.entities.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectBuoyPreset;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.data.ps.observation.FloatingObjectDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.entities.referential.common.Country;
import fr.ird.observe.entities.referential.common.Vessel;
import fr.ird.observe.entities.referential.ps.common.ObjectMaterial;
import fr.ird.observe.entities.referential.ps.common.ObjectOperation;
import fr.ird.observe.entities.referential.ps.common.ObservedSystem;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOperation;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOwnership;
import fr.ird.observe.entities.referential.ps.common.TransmittingBuoyType;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Created on 11/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class FloatingObjectSpi extends GeneratedFloatingObjectSpi {
    private static final Logger log = LogManager.getLogger(FloatingObjectSpi.class);

    public Form<FloatingObjectDto> preCreate(ServiceContext context, FloatingObjectPreset floatingObjectPreset) {
        Date now = context.now();
        FloatingObject entity = newEntity(now);
        if (floatingObjectPreset != null) {
            entity.setSupportVesselName(floatingObjectPreset.getSupplyName());
            entity.setObjectOperation(ObjectOperation.loadNullableEntity(context, floatingObjectPreset.getObjectOperationId()));
            Optional.ofNullable(floatingObjectPreset.getBuoy1()).map(b -> preCreate(context, b, now)).ifPresent(entity::addTransmittingBuoy);
            Optional.ofNullable(floatingObjectPreset.getBuoy2()).map(b -> preCreate(context, b, now)).ifPresent(entity::addTransmittingBuoy);

            Set<String> materialIds = new LinkedHashSet<>();
            Map<String, String> whenArrivingMaterials = floatingObjectPreset.getWhenArrivingMaterials();
            boolean whenArriving = whenArrivingMaterials != null;
            if (whenArriving) {
                materialIds.addAll(whenArrivingMaterials.keySet());
            } else {
                whenArrivingMaterials = Collections.emptyMap();
            }
            Map<String, String> whenLeavingMaterials = floatingObjectPreset.getWhenLeavingMaterials();
            boolean whenLeaving = whenLeavingMaterials != null;
            if (whenLeaving) {
                materialIds.addAll(whenLeavingMaterials.keySet());
            } else {
                whenLeavingMaterials = Collections.emptyMap();
            }
            for (String materialId : materialIds) {
                FloatingObjectPart newFloatingObjectPart = FloatingObjectPart.newEntity(now);
                newFloatingObjectPart.setObjectMaterial(ObjectMaterial.loadEntity(context, materialId));
                newFloatingObjectPart.setWhenArriving(whenArrivingMaterials.get(materialId));
                newFloatingObjectPart.setWhenLeaving(whenLeavingMaterials.get(materialId));
                entity.addFloatingObjectPart(newFloatingObjectPart);
            }
        }
        return entityToForm(context.getReferentialLocale(), entity);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Activity parent, FloatingObject entity, FloatingObjectDto dto) {
        log.info(String.format("Will persist %d part(s).", entity.getFloatingObjectPartSize()));
        boolean addFobObservedSystem = !ProtectedIdsPs.PS_OBSERVATION_FOB_OBSERVED_SYSTEM_EXCLUDE_OPERATIONS.contains(dto.getObjectOperation().getId())
                && !parent.getObservedSystemTopiaIds().contains(ProtectedIdsPs.PS_OBSERVATION_FOB_OBSERVED_SYSTEM);
        if (addFobObservedSystem) {
            log.info("Add FOB Observed System to activity: " + parent.getTopiaId());
            ObservedSystem observedSystem = ObservedSystem.loadEntity(context, ProtectedIdsPs.PS_OBSERVATION_FOB_OBSERVED_SYSTEM);
            parent.addObservedSystem(observedSystem);
        }
        Consumer<AddEntityToUpdateStep> result = super.onSave(context, parent, entity, dto);
        FloatingObjectPart.SPI.initId(context, entity.getFloatingObjectPart());
        TransmittingBuoy.SPI.initId(context, entity.getTransmittingBuoy());
        return result;
    }


    private TransmittingBuoy preCreate(ServiceContext context, FloatingObjectBuoyPreset floatingObjectBuoyPreset, Date now) {
        TransmittingBuoy transmittingBuoy = TransmittingBuoy.newEntity(now);
        transmittingBuoy.setCode(floatingObjectBuoyPreset.getCode());
        transmittingBuoy.setComment(floatingObjectBuoyPreset.getComment());
        transmittingBuoy.setCountry(Country.loadNullableEntity(context, floatingObjectBuoyPreset.getCountryId()));
        transmittingBuoy.setTransmittingBuoyOperation(TransmittingBuoyOperation.loadNullableEntity(context, floatingObjectBuoyPreset.getTransmittingBuoyOperationId()));
        transmittingBuoy.setTransmittingBuoyOwnership(TransmittingBuoyOwnership.loadNullableEntity(context, floatingObjectBuoyPreset.getTransmittingBuoyOwnershipId()));
        transmittingBuoy.setTransmittingBuoyType(TransmittingBuoyType.loadNullableEntity(context, floatingObjectBuoyPreset.getTransmittingBuoyTypeId()));
        transmittingBuoy.setVessel(Vessel.loadNullableEntity(context, floatingObjectBuoyPreset.getVesselId()));
        return transmittingBuoy;
    }

}
