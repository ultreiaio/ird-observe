package fr.ird.observe.entities.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.observation.ActivityDto;
import fr.ird.observe.dto.data.ps.observation.ActivityReference;
import fr.ird.observe.dto.data.ps.observation.ActivityStubDto;
import fr.ird.observe.dto.data.ps.observation.RouteDto;
import fr.ird.observe.dto.data.ps.observation.RouteReference;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.entities.data.TripEntityAware;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.Dates;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class RouteSpi extends GeneratedRouteSpi {

    @Override
    public Form<RouteDto> preCreate(ServiceContext context, Trip parent, Route preCreated) {
        Date date = parent.getRouteObs().stream()
                          // get route with higher date
                          .max(Comparator.comparing(Route::getDate))
                          // if found use next day of route date and set startLogValue
                          .map(r -> {
                              preCreated.setStartLogValue(r.getEndLogValue());
                              return DateUtils.addDays(r.getDate(), 1);

                          })
                          // if not found use now date (first route)
                          .orElse(context.now());
        preCreated.setDate(Dates.getDay(date));
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Trip parent, Route entity, RouteDto dto, boolean needCopy) {
        boolean dateHasChanged = false;
        if (dto.isPersisted()) {
            Date oldRouteDate = entity.getDate();
            Date oldDate = Dates.getDay(oldRouteDate);
            // si le jour a change, il faut mettre à jour les dates des activités et des sets
            dateHasChanged = !oldDate.equals(dto.getDate());
        }

        if (dateHasChanged) {
            getDao(context).updateActivitiesDate(entity.getTopiaId(), new java.sql.Date(dto.getDate().getTime()));
        }
        return TripEntityAware.onSaveUpdateTripIfEndDateChanged(Trip.SPI, parent, entity, super.onSave(context, parent, entity, dto, needCopy));
    }

    @Override
    public void loadDtoForValidation(ServiceContext context, Trip parent, Route entity, RouteDto dto) {
        bindEndOfSearchingProperties(entity.getActivity(), dto.getActivity());
    }

    @Override
    public Set<ToolkitIdDtoBean> getRealDependencies(ServiceContext context, Set<String> ids) {
        return Activity.SPI.byParentId(context, ids).map(id -> ToolkitIdDtoBean.of(ActivityDto.class, id)).collect(Collectors.toSet());
    }

    public Map<RouteReference, List<ActivityReference>> getActivitiesByRouteId(ServiceContext context, Map<String, VesselActivityReference> vesselActivities, TripDto trip) {
        return getDao(context).getActivitiesByRoute(vesselActivities, trip);
    }

    private void bindEndOfSearchingProperties(Set<Activity> activities, LinkedHashSet<ActivityStubDto> activitySeineList) {
        for (ActivityStubDto activitySeineDto : activitySeineList) {
            Activity activity = activities.stream().filter(e -> Objects.equals(e.getTopiaId(), activitySeineDto.getId())).findFirst().orElse(null);
            boolean isEndOfSearching = activity != null && activity.isActivityEndOfSearching();
            activitySeineDto.setActivityEndOfSearching(isEndOfSearching);
        }
    }
}
