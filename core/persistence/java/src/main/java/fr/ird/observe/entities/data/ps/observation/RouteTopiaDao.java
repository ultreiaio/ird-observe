package fr.ird.observe.entities.data.ps.observation;

/*
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.observation.ActivityReference;
import fr.ird.observe.dto.data.ps.observation.RouteReference;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.entities.Entities;
import org.nuiton.topia.persistence.support.QuerySupport;

import java.sql.Date;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class RouteTopiaDao extends AbstractRouteTopiaDao<Route> {

    /**
     * To update date part of
     * <ul>
     *     <li>{@link Set#getHaulingEndTimeStamp()}}</li>
     *     <li>{@link Set#getHaulingEndTimeStamp()}</li>
     *     <li>{@link Set#getEndTimeStamp()}}</li>
     * </ul>
     *  with the day date of the route.
     *
     * @param routeId id of the route to use
     * @param date    day date to use
     * @return number of row updated
     * @since 4.0
     */
    public int updateActivitiesDate(String routeId, Date date) {
        int result = updateSetEndTimeStamp(routeId, date);
        result += updateSetHaulingStartTimeStamp(routeId, date);
        result += updateSetHaulingEndTimeStamp(routeId, date);
        return result;
    }

    public int updateSetEndTimeStamp(String routeId, Date date) {
        return execute(queryUpdateSetEndTimeStamp(date, routeId));
    }

    public int updateSetHaulingStartTimeStamp(String routeId, Date date) {
        return execute(queryUpdateSetHaulingStartTimeStamp(date, routeId));
    }

    public int updateSetHaulingEndTimeStamp(String routeId, Date date) {
        return execute(queryUpdateSetHaulingEndTimeStamp(date, routeId));
    }

    public Map<RouteReference, List<ActivityReference>> getActivitiesByRoute(Map<String, VesselActivityReference> vesselActivities, TripDto trip) {
        return new QuerySupport.MapBuilder<>(new TreeMap<>(Comparator.comparing(RouteReference::getDate)), trip.getRouteObs(), (currentKey, row) -> {
            ActivityReference value = new ActivityReference();
            value.setDate(currentKey.getDate());
            int index = 0;
            value.setId((String) row[++index]);
            value.setLastUpdateDate((java.util.Date) row[++index]);
            value.setTopiaVersion(Entities.getPrimitiveLong(row, ++index));
            value.setTopiaCreateDate((java.util.Date) row[++index]);
            value.setTime((java.util.Date) row[++index]);
            value.setLatitude(Entities.getFloat(row, ++index));
            value.setLongitude(Entities.getFloat(row, ++index));
            String vesselActivityId = (String) row[++index];
            value.setVesselActivity(vesselActivityId == null ? null : vesselActivities.get(vesselActivityId));
            return value;
        }).build(this.<Object[]>queryGetActivitiesByRouteId(trip.getId()).stream());
    }
}
