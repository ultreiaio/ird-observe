package fr.ird.observe.entities.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.data.ps.observation.SampleDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.entities.referential.ps.common.SpeciesFate;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

public class SampleSpi extends GeneratedSampleSpi {

    //FIXME We need to fix this code, this is the only case where we create a first entity when not found
    @Override
    public Form<SampleDto> loadForm(ServiceContext context, String id) {
        Set parent = Set.loadEntity(context, id);
        Sample entity = loadSampleFormEntity(context, parent);
        Form<SampleDto> form = Sample.entityToForm(context, entity);
        loadDtoForValidation(context, parent, entity, form.getObject());
        return form;
    }

    @Override
    public void loadDtoForValidation(ServiceContext context, Set parent, Sample entity, SampleDto dto) {
        dto.setId(parent.getTopiaId());
        dto.setSpeciesFateBySpeciesMap(getCatchesSpeciesFateBySpeciesId(parent));
    }

    @Override
    public SaveResultDto save(ServiceContext context, SampleDto dto) throws ConcurrentModificationException {
        // this is a trick we always use id as parent id...
        Set parent = Set.loadEntity(context, dto.getId());
        Sample entity = loadSampleFormEntity(context, parent);
        checkLastUpdateDate(context, entity, dto);
        // use the real sample id
        // use the real id otherwise copy will alter entity id and commit will fail
        dto.setId(entity.getTopiaId());
        Sample.fromDto(context, entity, dto);
        if (!entity.isPersisted()) {
            Sample.SPI.getDao(context).initId(entity);
            parent.addSample(entity);
        }
        SampleMeasure.SPI.initId(context, entity.getSampleMeasure());
        context.getTopiaPersistenceContext().flush();
        AddEntityToUpdateStep saveHelper = newSaveHelper(context)
                .update(this, entity);
        saveCallback(saveHelper, entity);
        return saveHelper
                .updateLastUpdateDateField(Set.SPI, parent)
                .build(entity);
    }


    protected Sample loadSampleFormEntity(ServiceContext context, Set parent) {
        if (parent.isSampleEmpty()) {
            return Sample.newEntity(context.now());
        }
        return parent.getSample().iterator().next();
    }

    protected Map<String, java.util.Set<String>> getCatchesSpeciesFateBySpeciesId(Set parent) {
        Map<String, java.util.Set<String>> result = new TreeMap<>();
        parent.getCatches().stream().filter(c -> c.getSpeciesFate() != null).forEach(aCatch -> {
            String speciesId = aCatch.getSpecies().getId();
            SpeciesFate speciesFate = aCatch.getSpeciesFate();
            result.computeIfAbsent(speciesId, e -> new TreeSet<>()).add(speciesFate.getId());
        });
        return result;
    }
}
