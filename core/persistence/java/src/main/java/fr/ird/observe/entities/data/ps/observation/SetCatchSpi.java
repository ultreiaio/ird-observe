package fr.ird.observe.entities.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.ps.observation.SetCatchDto;
import fr.ird.observe.dto.referential.ReferentialLocale;

/**
 * Created on 04/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SetCatchSpi extends GeneratedSetCatchSpi {

    @Override
    public void toDto(ReferentialLocale referentialLocale, Set entity, SetCatchDto dto) {
        super.toDto(referentialLocale, entity, dto);
        // Do not copy set.comment (there is no editor in form for this)
        dto.setComment(null);
        // is there some sample?
        if (entity.isSampleNotEmpty()) {
            Sample targetSample = entity.getSample().iterator().next();
            // get usable species ids
            java.util.Set<String> speciesId = ToolkitId.ids(targetSample.getSampleMeasure().stream().map(SampleMeasure::getSpecies));
            // mark catch if there is associated samples to it
            dto.getCatches().forEach(childDto -> {
                boolean hasSample = speciesId.contains(childDto.getSpecies().getId());
                childDto.setHasSample(hasSample);
            });
        }
        // is there some non target catch release?
        if (entity.isNonTargetCatchReleaseNotEmpty()) {
            // get usable species ids
            java.util.Set<String> speciesId = ToolkitId.ids(entity.getNonTargetCatchRelease().stream().map(NonTargetCatchRelease::getSpecies));
            // mark catch if there is associated releases to it
            dto.getCatches().forEach(childDto -> {
                boolean hasRelease = speciesId.contains(childDto.getSpecies().getId());
                childDto.setHasRelease(hasRelease);
            });
        }
    }

    @Override
    public void fromDto(ReferentialLocale referentialLocale, Set entity, SetCatchDto dto) {
        super.fromDto(referentialLocale, entity, dto);
        if (entity.isSampleNotEmpty()) {
            // get usable species ids
            java.util.Set<String> speciesIds = ToolkitId.ids(entity.getCatches().stream().map(Catch::getSpecies));
            // delete any sample which not using the given species ids
            entity.getSample().iterator().next().getSampleMeasure()
                    .removeIf(targetLength -> !speciesIds.contains(targetLength.getSpecies().getTopiaId()));
        }
        if (entity.isNonTargetCatchReleaseNotEmpty()) {
            // get usable species ids
            java.util.Set<String> speciesIds = ToolkitId.ids(entity.getCatches().stream().map(Catch::getSpecies));
            // delete any sample which not using the given species ids
            entity.getNonTargetCatchRelease()
                    .removeIf(catchRelease -> !speciesIds.contains(catchRelease.getSpecies().getTopiaId()));
        }
    }
}
