package fr.ird.observe.entities.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsPs;
import fr.ird.observe.dto.data.ps.observation.SetDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.TripEntityAware;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.referential.ps.common.SchoolType;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.Dates;

import java.util.Date;
import java.util.function.Consumer;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SetSpi extends GeneratedSetSpi {

    @Override
    public void toDto(ReferentialLocale referentialLocale, Set entity, SetDto dto) {
        dto.setSampleMeasureEnabled(entity.canUseSample());
        dto.setNonTargetCatchReleaseEnabled(entity.canUseNonTargetCatchRelease());
//        dto.setSampleMeasureSize(entity.getSampleMeasureSize());
        super.toDto(referentialLocale, entity, dto);
    }

    @Override
    public Form<SetDto> preCreate(ServiceContext context, Activity parent, Set preCreated) {
        Route route = Activity.SPI.getParent(context, parent.getTopiaId());
        Date routeDate = route.getDate();
        // on utilise l'heure de l'activité comme début de calée
        Date date = Dates.getTime(parent.getTime(), false, false);
        preCreated.setStartTime(date);
        // pour les dates de fin on utilise la date de la route
        preCreated.setEndTimeStamp(Dates.getDateAndTime(routeDate, date, false, false));
        preCreated.setHaulingEndTimeStamp(Dates.getDateAndTime(routeDate, date, false, false));
        // by default, school type is undefined, and only computed by consolidation action
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2573
        SchoolType schoolType = SchoolType.loadEntity(context, ProtectedIdsPs.PS_COMMON_SCHOOL_TYPE_UNDEFINED_ID);
        preCreated.setSchoolType(schoolType);
        //FIXME super.preCreate does already this?
//        Form<SetDto> form = super.preCreate(context, parent, preCreated);
//        form.getObject().setStartSetDate(routeDate);
        return super.preCreate(context, parent, preCreated);
    }

    @Override
    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, Activity parent, Set entity, SetDto dto) {
        Route route = Activity.SPI.getParent(context, parent.getId());
        Trip trip = Route.SPI.getParent(context, route.getId());
        entity.setActivity(parent);
        return TripEntityAware.onSaveUpdateTripIfEndDateChanged(Trip.SPI, trip, entity, super.onSave(context, parent, entity, dto));
    }

    @Override
    public void loadDtoForValidation(ServiceContext context, Activity parent, Set entity, SetDto dto) {
        Route route = Activity.SPI.getParent(context, parent.getTopiaId());
        Date routeDate = route.getDate();
        dto.setStartSetDate(routeDate);
    }

    public void loadDtoForValidation(Route route, SetDto dto) {
        Date routeDate = route.getDate();
        dto.setStartSetDate(routeDate);
    }
}
