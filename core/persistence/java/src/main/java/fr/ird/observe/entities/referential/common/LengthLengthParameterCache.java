package fr.ird.observe.entities.referential.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.referential.common.DuplicateLengthLengthParameterException;
import fr.ird.observe.dto.referential.common.LengthLengthParameterNotFoundException;
import fr.ird.observe.entities.ObserveTopiaDaoSupplier;
import io.ultreia.java4all.util.Dates;

import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;

/**
 * A cache to get {@link LengthLengthParameter}.
 * <p>
 * This is used by the consolidation API.
 * <p>
 * Created on 13/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.28
 */
public class LengthLengthParameterCache {

    /**
     * Application locale (for error messages).
     */
    private final Locale applicationLocale;
    /**
     * To decorate entities in exception if necessary.
     */
    private final DecoratorService decoratorService;
    /**
     * Dao supplier to query database.
     */
    public final ObserveTopiaDaoSupplier daoSupplier;
    /**
     * Matching cache.
     */
    private final Map<String, LengthLengthParameter> cache;
    /**
     * Cache for not found queries.
     */
    private final Map<String, LengthLengthParameterNotFoundException> notFoundCache;
    /**
     * Cache for duplicated queries.
     */
    private final Map<String, DuplicateLengthLengthParameterException> duplicateCache;

    public LengthLengthParameterCache(Locale applicationLocale, DecoratorService decoratorService, ObserveTopiaDaoSupplier daoSupplier) {
        this.applicationLocale = applicationLocale;
        this.decoratorService = Objects.requireNonNull(decoratorService);
        this.daoSupplier = Objects.requireNonNull(daoSupplier);
        this.cache = new TreeMap<>();
        this.notFoundCache = new TreeMap<>();
        this.duplicateCache = new TreeMap<>();
    }

    public Optional<LengthLengthParameter> find(Ocean ocean, Date date, Species species, Sex sex, SizeMeasureType inputSizeMeasureType, SizeMeasureType outputSizeMeasureType) {
        String cacheKey = buildCacheKey(ocean, date, species, sex, inputSizeMeasureType, outputSizeMeasureType);

        LengthLengthParameter result = cache.get(cacheKey);
        if (result != null) {
            return Optional.of(result);
        }
        LengthLengthParameterNotFoundException lengthLengthParameterNotFoundException = notFoundCache.get(cacheKey);
        if (lengthLengthParameterNotFoundException != null) {
            throw lengthLengthParameterNotFoundException;
        }
        DuplicateLengthLengthParameterException duplicateLengthLengthParameterException = duplicateCache.get(cacheKey);
        if (duplicateLengthLengthParameterException != null) {
            throw duplicateLengthLengthParameterException;
        }

        try {
            result = LengthLengthParameters.findLengthLengthParameter(applicationLocale, decoratorService, daoSupplier, species, ocean, sex, date, inputSizeMeasureType, outputSizeMeasureType).orElse(null);
            cache.put(cacheKey, result);
            return Optional.ofNullable(result);
        } catch (LengthLengthParameterNotFoundException e) {
            notFoundCache.put(cacheKey, e);
            throw e;
        } catch (DuplicateLengthLengthParameterException e) {
            duplicateCache.put(cacheKey, e);
            throw e;
        }
    }

    public void clear() {
        cache.clear();
        notFoundCache.clear();
        duplicateCache.clear();
    }

    private String buildCacheKey(Ocean ocean, Date date, Species species, Sex sex, SizeMeasureType inputSizeMeasureType, SizeMeasureType outputSizeMeasureType) {
        return String.format("%s#%s#%s#%s#%s#%s",
                             ocean.getId(),
                             Dates.getDay(date),
                             species.getId(),
                             sex == null ? null : sex.getId(),
                             inputSizeMeasureType.getId(),
                             outputSizeMeasureType.getId());
    }
}
