package fr.ird.observe.entities.referential.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.WithStartEndDate;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LengthLengthParameterTopiaDao extends AbstractLengthLengthParameterTopiaDao<LengthLengthParameter> {

    public static final Comparator<LengthLengthParameter> FORMULA_SUPPORT_START_DATE_COMPARATOR = Comparator.comparing(LengthLengthParameter::getStartDate, WithStartEndDate.START_DATE_COMPARATOR);
    public static final Comparator<LengthLengthParameter> FORMULA_SUPPORT_END_DATE_COMPARATOR = Comparator.comparing(LengthLengthParameter::getEndDate, WithStartEndDate.END_DATE_COMPARATOR);
    public static final Comparator<LengthLengthParameter> FORMULA_SUPPORT_COMPARATOR = FORMULA_SUPPORT_START_DATE_COMPARATOR.thenComparing(FORMULA_SUPPORT_END_DATE_COMPARATOR);

    public static <E extends LengthLengthParameter> List<E> sortAndFilter(Stream<E> list, Date date) {
        return list.sorted(FORMULA_SUPPORT_COMPARATOR)
                .filter(f -> WithStartEndDate.START_DATE_COMPARATOR.compare(f.getStartDate(), date) <= 0 && WithStartEndDate.END_DATE_COMPARATOR.compare(date, f.getEndDate()) <= 0)
                .collect(Collectors.toList());
    }

    public List<LengthLengthParameter> findAll(Species species, Sex sex, Ocean ocean, Date date, SizeMeasureType inputSizeMeasureType, SizeMeasureType outputSizeMeasureType) {
        Objects.requireNonNull(species);
        if (sex == null) {
            // use undetermined sex
            sex = getUndeterminedSex();
        }
        Objects.requireNonNull(ocean);
        Objects.requireNonNull(date);
        Objects.requireNonNull(inputSizeMeasureType);
        Objects.requireNonNull(outputSizeMeasureType);

        Stream<LengthLengthParameter> stream = forSpeciesEquals(species)
                .addEquals(LengthLengthParameter.PROPERTY_SEX, sex)
                .addEquals(LengthLengthParameter.PROPERTY_OCEAN, ocean)
                .addEquals(LengthLengthParameter.PROPERTY_INPUT_SIZE_MEASURE_TYPE, inputSizeMeasureType)
                .addEquals(LengthLengthParameter.PROPERTY_OUTPUT_SIZE_MEASURE_TYPE, outputSizeMeasureType)
                .stream();
        List<LengthLengthParameter> result = sortAndFilter(stream, date);
        if (result.isEmpty() && !Objects.equals(sex.getCode(), "0")) {
            // try this undetermined sex
            result = findAll(species, getUndeterminedSex(), ocean, date, inputSizeMeasureType, outputSizeMeasureType);
        }
        return result;
    }

    protected Sex getUndeterminedSex() {
        return topiaDaoSupplier.getDao(Sex.class, SexTopiaDao.class).forCodeEquals("0").findUnique();
    }
}
