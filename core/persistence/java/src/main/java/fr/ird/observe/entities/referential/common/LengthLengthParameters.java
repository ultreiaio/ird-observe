package fr.ird.observe.entities.referential.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.referential.common.DuplicateLengthLengthParameterException;
import fr.ird.observe.dto.referential.common.LengthLengthParameterNotFoundException;
import fr.ird.observe.entities.ObserveTopiaDaoSupplier;
import io.ultreia.java4all.i18n.I18n;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created on 05/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class LengthLengthParameters {

    /**
     * Recherche d'un {@link LengthLengthParameter} à partir des paramètres donnés.
     * <p>
     * La recherche peut ne peut être aussi exacte que les paramètres donnés :
     * <p>
     * Dans le cas d'une espèce faune, si non trouvé alors on recherche sur son speciesGroup d'espèce.
     * <p>
     * Si non trouvé pour l'océan donné (et que celui-ci est non null), alors on recherche avec un ocean vide.
     * <p>
     * Si non trouvé sur le sexe (et que le sexe n'est pas indéterminé) , alors on recherche avec le sexe indéterminé (sexe=0).
     *
     * @param applicationLocale
     * @param decoratorService      le service de décoration
     * @param daoSupplier           la transaction en cours d'utilisation
     * @param species               l'espèce sur lequel on recherche le paramétrage
     * @param ocean                 l'ocean recherché (peut être null)
     * @param sex                   le sexe recherché (on essayera sans sexe (sexe.code=0) si non trouvé)
     * @param date                  le jour recherché
     * @param inputSizeMeasureType  le type de mensuration recherché
     * @param outputSizeMeasureType le type de mesure de sortie recherché
     * @return le paramétrage adéquate
     * @since 1.5
     */
    public static Optional<LengthLengthParameter> findLengthLengthParameter(Locale applicationLocale, DecoratorService decoratorService,
                                                                            ObserveTopiaDaoSupplier daoSupplier,
                                                                            Species species,
                                                                            Ocean ocean,
                                                                            Sex sex,
                                                                            Date date,
                                                                            SizeMeasureType inputSizeMeasureType,
                                                                            SizeMeasureType outputSizeMeasureType) {
        Sex unknownSex = getUnknownSex(daoSupplier);

        if (sex == null) {

            // on utilise le sexe indéterminé
            sex = unknownSex;
        }

        List<LengthLengthParameter> list = daoSupplier.getCommonLengthLengthParameterDao().findAll(species, sex, ocean, date, inputSizeMeasureType, outputSizeMeasureType);

        if ((list == null || list.isEmpty()) && !unknownSex.equals(sex)) {

            // on essaye avec le sexe indéterminé
            sex = unknownSex;
            list = daoSupplier.getCommonLengthLengthParameterDao().findAll(species, sex, ocean, date, inputSizeMeasureType, outputSizeMeasureType);
        }

        if (list == null || list.isEmpty()) {
            decoratorService.installDecorator(Species.class, species);
            decoratorService.installDecorator(Ocean.class, ocean);
            decoratorService.installDecorator(Sex.class, sex);
            throwLengthLengthParameterNotFoundException(applicationLocale, species, ocean, sex, date);
        }

//        if (inputSizeMeasureType != null) {
//
//            // on filtre par le type de mensuration entrant
//            List<LengthLengthParameter> list2 = filterByInputSizeMeasureType(list, inputSizeMeasureType);
//
//            if (!list2.isEmpty()) {
//
//                // ok pas trouvé avec le bon type de mensuration, on fera la conversion plus tard
//                // on tente cependante d'utiliser la RTP trouvé
//                list = list2;
//            }
//        }
//        if (outputSizeMeasureType != null) {
//
//            // on filtre par le type de mensuration sortant
//            List<LengthLengthParameter> list2 = filterByOutputSizeMeasureType(list, outputSizeMeasureType);
//
//            if (!list2.isEmpty()) {
//
//                // ok pas trouvé avec le bon type de mensuration, on fera la conversion plus tard
//                // on tente cependante d'utiliser la RTP trouvé
//                list = list2;
//            }
//        }

        // au final il ne devrait en rester qu'un

        if (list.size() > 1) {
            decoratorService.installDecorator(Species.class, species);
            decoratorService.installDecorator(Ocean.class, ocean);
            decoratorService.installDecorator(Sex.class, sex);
            decoratorService.installDecorator(LengthLengthParameter.class, list.stream());
            throwDuplicateLengthLengthParameterException(applicationLocale, species, ocean, sex, date, list);
        }

        return Optional.of(list.get(0));

    }


    private static void throwDuplicateLengthLengthParameterException(Locale locale, Species species, Ocean ocean, Sex sex, Date date, List<LengthLengthParameter> foundLengthLengthParameters) {
        throw new DuplicateLengthLengthParameterException(I18n.l(locale, "observe.error.DuplicateLengthLengthParameterException",
                species.toString(),
                ocean.toString(),
                sex == null ? null : sex.toString(),
                date,
                foundLengthLengthParameters.stream().map(Object::toString).collect(Collectors.joining("\n * ")))
        );
    }

    private static void throwLengthLengthParameterNotFoundException(Locale locale, Species species, Ocean ocean, Sex sex, Date date) {
        throw new LengthLengthParameterNotFoundException(I18n.l(locale, "observe.error.LengthLengthParameterNotFoundException",
                species.toString(),
                ocean.toString(),
                sex == null ? null : sex.toString(),
                date));
    }

    private static Sex getUnknownSex(ObserveTopiaDaoSupplier daoSupplier) {
        return daoSupplier.getCommonSexDao().forCodeEquals("0").findUnique();
    }
}
