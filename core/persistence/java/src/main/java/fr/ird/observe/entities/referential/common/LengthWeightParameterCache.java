package fr.ird.observe.entities.referential.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.referential.common.DuplicateLengthWeightParameterException;
import fr.ird.observe.dto.referential.common.LengthWeightParameterNotFoundException;
import fr.ird.observe.entities.ObserveTopiaDaoSupplier;
import io.ultreia.java4all.util.Dates;

import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;

/**
 * A cache to get {@link LengthWeightParameter}.
 * <p>
 * This is used by the consolidation API.
 * <p>
 * Created on 13/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.28
 */
public class LengthWeightParameterCache {

    /**
     * Application locale (for error messages).
     */
    private final Locale applicationLocale;
    /**
     * To decorate entities in exception if necessary.
     */
    private final DecoratorService decoratorService;
    /**
     * Dao supplier to query database.
     */
    public final ObserveTopiaDaoSupplier daoSupplier;
    /**
     * Matching cache.
     */
    private final Map<String, LengthWeightParameter> cache;
    /**
     * Cache for not found queries.
     */
    private final Map<String, LengthWeightParameterNotFoundException> notFoundCache;
    /**
     * Cache for duplicated queries.
     */
    private final Map<String, DuplicateLengthWeightParameterException> duplicateCache;

    public LengthWeightParameterCache(Locale applicationLocale, DecoratorService decoratorService, ObserveTopiaDaoSupplier daoSupplier) {
        this.applicationLocale = applicationLocale;
        this.decoratorService = Objects.requireNonNull(decoratorService);
        this.daoSupplier = Objects.requireNonNull(daoSupplier);
        this.cache = new TreeMap<>();
        this.notFoundCache = new TreeMap<>();
        this.duplicateCache = new TreeMap<>();
    }

    public Optional<LengthWeightParameter> find(Ocean ocean, Date date, Species species, Sex sex, SizeMeasureType sizeMeasureType) {
        String cacheKey = buildCacheKey(ocean, date, species, sex, sizeMeasureType);

        LengthWeightParameter result = cache.get(cacheKey);
        if (result != null) {
            return Optional.of(result);
        }
        LengthWeightParameterNotFoundException lengthWeightParameterNotFoundException = notFoundCache.get(cacheKey);
        if (lengthWeightParameterNotFoundException != null) {
            throw lengthWeightParameterNotFoundException;
        }
        DuplicateLengthWeightParameterException duplicateLengthWeightParameterException = duplicateCache.get(cacheKey);
        if (duplicateLengthWeightParameterException != null) {
            throw duplicateLengthWeightParameterException;
        }

        try {
            result = LengthWeightParameters.findLengthWeightParameter(applicationLocale, decoratorService, daoSupplier, species, sex, ocean, date, sizeMeasureType);
            cache.put(cacheKey, result);
            return Optional.ofNullable(result);
        } catch (LengthWeightParameterNotFoundException e) {
            notFoundCache.put(cacheKey, e);
            throw e;
        } catch (DuplicateLengthWeightParameterException e) {
            duplicateCache.put(cacheKey, e);
            throw e;
        }
    }

    public void clear() {
        cache.clear();
        notFoundCache.clear();
        duplicateCache.clear();
    }

    private String buildCacheKey(Ocean ocean, Date date, Species species, Sex sex, SizeMeasureType sizeMeasureType) {
        return String.format("%s#%s#%s#%s#%s",
                             ocean.getId(),
                             Dates.getDay(date),
                             species.getId(),
                             sex == null ? null : sex.getId(),
                             sizeMeasureType == null ? null : sizeMeasureType.getId());
    }
}
