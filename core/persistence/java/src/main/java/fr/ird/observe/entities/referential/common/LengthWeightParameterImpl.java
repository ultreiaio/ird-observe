/*
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.referential.common;

import fr.ird.observe.dto.referential.FormulaHelper;
import io.ultreia.java4all.lang.Numbers;

import java.util.Map;
import java.util.Objects;

public class LengthWeightParameterImpl extends LengthWeightParameterAbstract {

    private Map<String, Double> coefficientValues;
    private Boolean formulaOneValid;
    private Boolean formulaTwoValid;

    @Override
    public String getSpeciesFaoCode() {
        return getSpecies() == null ? null : getSpecies().getFaoCode();
    }

    @Override
    public Species getSpeciesLabel() {
        return (Species) super.getSpeciesLabel();
    }

    @Override
    public void setLengthWeightFormula(String value) {
        super.setLengthWeightFormula(value);
        revalidateFormulaOne();
    }

    @Override
    public void setWeightLengthFormula(String value) {
        super.setWeightLengthFormula(value);
        revalidateFormulaTwo();
    }

    @Override
    public String getFormulaOneVariableName() {
        return FormulaHelper.VARIABLE_LENGTH;
    }

    @Override
    public String getFormulaTwoVariableName() {
        return FormulaHelper.VARIABLE_WEIGHT;
    }

    @Override
    public String getFormulaOne() {
        return lengthWeightFormula;
    }

    @Override
    public String getFormulaTwo() {
        return weightLengthFormula;
    }

    @Override
    public Float computeFromFormulaOne(float length) {
        return FormulaHelper.computeValue(this, lengthWeightFormula, null, FormulaHelper.VARIABLE_LENGTH, length, Numbers::roundThreeDigits);
    }

    @Override
    public Float computeFromFormulaTwo(float weight) {
        return FormulaHelper.computeValue(this, weightLengthFormula, FormulaHelper.COEFFICIENT_B, FormulaHelper.VARIABLE_WEIGHT, weight, Numbers::roundOneDigit);
    }

    @Override
    public String getCode() {
        // pas utilise
        return null;
    }

    @Override
    public void setCode(String code) {
        // pas utilise
    }

    @Override
    public void setCoefficients(String value) {
        super.setCoefficients(value);
        coefficientValues = null;
        revalidateFormulaOne();
        revalidateFormulaTwo();
    }

    @Override
    public Map<String, Double> getCoefficientValues() {
        if (coefficientValues == null) {
            coefficientValues = FormulaHelper.getCoefficientValues(this);
        }
        return coefficientValues;
    }

    @Override
    public boolean isFormulaOneValid() {
        if (formulaOneValid == null) {
            revalidateFormulaOne();
        }
        return Objects.equals(true, formulaOneValid);
    }

    @Override
    public boolean isFormulaTwoValid() {
        if (formulaTwoValid == null) {
            revalidateFormulaTwo();
        }
        return Objects.equals(true, formulaTwoValid);
    }

    @Override
    public void setFormulaOneValid(boolean formulaOneValid) {
        this.formulaOneValid = formulaOneValid;
        fireOnPostWrite(PROPERTY_FORMULA_ONE_VALID, null, formulaOneValid);
    }

    @Override
    public void setFormulaTwoValid(boolean formulaTwoValid) {
        this.formulaTwoValid = formulaTwoValid;
        fireOnPostWrite(PROPERTY_FORMULA_TWO_VALID, null, formulaTwoValid);
    }

}
