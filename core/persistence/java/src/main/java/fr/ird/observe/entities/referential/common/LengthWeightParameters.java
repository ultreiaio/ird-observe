package fr.ird.observe.entities.referential.common;

/*
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.referential.common.DuplicateLengthWeightParameterException;
import fr.ird.observe.dto.referential.common.LengthWeightParameterNotFoundException;
import fr.ird.observe.entities.ObserveTopiaDaoSupplier;
import io.ultreia.java4all.i18n.I18n;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 28/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LengthWeightParameters {

    /**
     * Recherche d'un {@link LengthWeightParameter} à partir des paramètres donnés.
     * <p>
     * La recherche peut ne peut être aussi exacte que les paramètres donnés :
     * <p>
     * Dans le cas d'une espèce faune, si non trouvé alors on recherche sur son speciesGroup d'espèce.
     * <p>
     * Si non trouvé pour l'océan donné (et que celui-ci est non null), alors on recherche avec un ocean vide.
     * <p>
     * Si non trouvé sur le sexe (et que le sexe n'est pas indéterminé) , alors on recherche avec le sexe indéterminé (sexe=0).
     *
     * @param applicationLocale
     * @param decoratorService  le service de décoration
     * @param daoSupplier       la transaction en cours d'utilisation
     * @param species           l'espèce sur lequel on recherche le paramétrage
     * @param sex               le sexe recherché (on essayera sans sexe (sexe.code=0) si non trouvé)
     * @param ocean             l'ocean recherché (peut être null)
     * @param date              le jour recherché
     * @param sizeMeasureType   le type de mensuration recherché
     * @return le paramétrage adéquate
     * @since 1.5
     */
    public static LengthWeightParameter findLengthWeightParameter(Locale applicationLocale, DecoratorService decoratorService,
                                                                  ObserveTopiaDaoSupplier daoSupplier,
                                                                  Species species, Sex sex, Ocean ocean, Date date, SizeMeasureType sizeMeasureType) {

//        Sex unknownSex = getUnknownSex(daoSupplier);
//
//        if (sex == null) {
//
//            // on utilise le sexe indéterminé
//            sex = unknownSex;
//        }

        List<LengthWeightParameter> list = daoSupplier.getCommonLengthWeightParameterDao().findAll(species, sex, ocean, date, sizeMeasureType);

//        if (CollectionUtils.isEmpty(list) && !unknownSex.equals(sex)) {
//
//            // on essaye avec le sexe indéterminé
////            sex = unknownSex;
//            list = daoSupplier.getCommonLengthWeightParameterDao().findAll(species, unknownSex, ocean, date, sizeMeasureType);
//        }

        if (list == null || list.isEmpty()) {
            decoratorService.installDecorator(Species.class, species);
            decoratorService.installDecorator(Ocean.class, ocean);
            if (sex != null) {
                decoratorService.installDecorator(Sex.class, sex);
            }
            throwLengthWeightParameterNotFoundException(applicationLocale, species, ocean, sex, date);
        }

//        if (sizeMeasureType != null) {
//
//            // on filtre par le type de mensuration entrant
//            List<LengthWeightParameter> list2 = filterBySizeMeasureType(list, sizeMeasureType);
//
//            if (!list2.isEmpty()) {
//
//                // ok pas trouvé avec le bon type de mensuration, on fera la conversion plus tard
//                // on tente cependante d'utiliser la RTP trouvé
//                list = list2;
//            }
//        }

        // au final il ne devrait en rester qu'un

        if (list.size() > 1) {
            decoratorService.installDecorator(Species.class, species);
            decoratorService.installDecorator(Ocean.class, ocean);
            if (sex != null) {
                decoratorService.installDecorator(Sex.class, sex);
            }
            decoratorService.installDecorator(LengthWeightParameter.class, list.stream());
            throwDuplicateLengthWeightParameterException(applicationLocale, species, ocean, sex, date, list);
        }

        return Objects.requireNonNull(list.get(0));

    }

    private static void throwDuplicateLengthWeightParameterException(Locale locale, Species species, Ocean ocean, Sex sex, Date date, List<LengthWeightParameter> foundLengthWeightParameters) {
        throw new DuplicateLengthWeightParameterException(I18n.l(locale, "observe.error.DuplicateLengthWeightParameterException",
                species.toString(),
                ocean.toString(),
                sex == null ? null : sex.toString(),
                date,
                foundLengthWeightParameters.stream().map(Object::toString).collect(Collectors.joining("\n * ")))
        );
    }


    private static void throwLengthWeightParameterNotFoundException(Locale locale, Species species, Ocean ocean, Sex sex, Date date) {
        throw new LengthWeightParameterNotFoundException(I18n.l(locale, "observe.error.LengthWeightParameterNotFoundException",
                species.toString(),
                ocean.toString(),
                sex == null ? null : sex.toString(),
                date));
    }

}
