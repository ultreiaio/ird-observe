package fr.ird.observe.entities.referential.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.common.OceanDto;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class OceanSpi extends GeneratedOceanSpi {

    @Override
    public Form<OceanDto> loadForm(ServiceContext context, String id) {
        Form<OceanDto> form = super.loadForm(context, id);
        // add species that use this ocean, to be able to edit species list from Ocean form
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2366
        try (Stream<Species> speciesStream = Species.loadEntities(context, s -> s.getOceanTopiaIds().contains(id))) {
            List<SpeciesReference> speciesReferences = Species.toReferenceSet(context.getReferentialLocale(), speciesStream, null, null, null).toList();
            form.getObject().setSpecies(speciesReferences);
        }
        return form;
    }

    @Override
    public SaveResultDto saveEntity(ServiceContext context, Ocean entity, OceanDto dto) {
        Set<Species> speciesToUpdate = new LinkedHashSet<>();
        Set<String> newSpeciesWithThisOceanIds = dto.getSpecies().stream().map(SpeciesReference::getId).collect(Collectors.toSet());
        if (dto.isPersisted()) {
            // In that case, we could have some species to update by removing some ocean associations
            String thisOceanId = dto.getId();
            try (Stream<Species> speciesStream = Species.loadEntities(context, s -> s.getOceanTopiaIds().contains(thisOceanId))) {
                speciesStream.forEach(oldSpeciesWithThisOcean -> {
                    String oldSpeciesWithThisOceanId = oldSpeciesWithThisOcean.getTopiaId();
                    if (!newSpeciesWithThisOceanIds.contains(oldSpeciesWithThisOceanId)) {
                        // this species was removed from the ocean
                        oldSpeciesWithThisOcean.removeOcean(entity);
                        speciesToUpdate.add(oldSpeciesWithThisOcean);
                    } else {
                        // this species still have this ocean, no need to keep it
                        newSpeciesWithThisOceanIds.remove(oldSpeciesWithThisOceanId);
                    }
                });
            }
        }
        if (!newSpeciesWithThisOceanIds.isEmpty()) {
            // need to add this ocean to those species
            try (Stream<Species> speciesStream = Species.loadEntities(context, s -> newSpeciesWithThisOceanIds.contains(s.getTopiaId()))) {
                speciesStream.forEach(s -> {
                    s.addOcean(entity);
                    speciesToUpdate.add(s);
                });
            }
        }
        AddEntityToUpdateStep update = newSaveHelper(context).update(this, entity);
        for (Species species : speciesToUpdate) {
            update.update(Species.SPI, species);
        }
        return update.build(entity);
    }
} //OceanSpi
