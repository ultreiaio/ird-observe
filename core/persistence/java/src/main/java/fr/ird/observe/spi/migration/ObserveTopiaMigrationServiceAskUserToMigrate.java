package fr.ird.observe.spi.migration;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.util.Version;
import org.nuiton.topia.service.migration.TopiaMigrationServiceAskUserToMigrate;

import java.util.List;

/**
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(TopiaMigrationServiceAskUserToMigrate.class)
public class ObserveTopiaMigrationServiceAskUserToMigrate implements TopiaMigrationServiceAskUserToMigrate {

    private static final Version MINIMUM_VERSION = Version.create("3.0").build();

    public static Version getMinimumVersion() {
        return MINIMUM_VERSION;
    }

    @Override
    public boolean canIMigrate(Version dbVersion, List<Version> versions) {
        // on autorise les migrations dès quelles sont demandée pour une version égale ou superieur à la V3.0.
        //FIXME On devrait peut-être à ce niveau demander réellement à l'utilisateur, car dans le cas de création
        //FIXME d'une base via un script, la migration est effectuée sans demande, mais d'un autre côté si on charge une
        //FIXME backup, alors la source n'est pas altérée donc une migration automatique est possible...
        return dbVersion.afterOrEquals(getMinimumVersion());
//        boolean headless = GraphicsEnvironment.isHeadless();
//
//        if (canIMigrate) {
//            if (headless) {
//                // on a server, this mean we can do it
//                canIMigrate = true;
//            } else {
//                // ask user to confirm this
//            }
//        } else {
//            if (!headless) {
//                // says to use this is bad...
//            }
//        }
//        return canIMigrate;
    }
}
