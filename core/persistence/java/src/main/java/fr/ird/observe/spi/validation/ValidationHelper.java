package fr.ird.observe.spi.validation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.SpeciesDto;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.spi.service.ServiceContext;
import fr.ird.observe.validation.ValidationContextSupport;
import fr.ird.observe.validation.api.request.DataValidationRequest;
import fr.ird.observe.validation.api.request.ReferentialValidationRequest;
import fr.ird.observe.validation.api.result.ValidationResult;
import fr.ird.observe.validation.api.result.ValidationResultBuilder;
import io.ultreia.java4all.i18n.spi.type.TypeTranslators;
import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.validation.bean.NuitonValidatorsMap;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created on 22/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class ValidationHelper {
    private static final Logger log = LogManager.getLogger(ValidationHelper.class);
    private static final TimeLog TIME_LOG = new TimeLog(ValidationHelper.class, 100, 500);

    public static NuitonValidatorsMap toValidatorsMap(DataValidationRequest request, ValidationContextSupport validationContext) {
        return NuitonValidatorsMap.create(NuitonValidatorProviders.newProvider(NuitonValidatorProviders.getDefaultFactoryName(), validationContext), ObserveBusinessProject.get().getMapping().getMainDataClasses(), request.getValidationContext(), request.getScopes()).build();
    }

    //FIXME Change this design when no more using XWorks, we won't need to use a specific thread for this operation
    //FIXME We need a separate thread since XWorks ActionContext is ThreadLocal
    public static <E extends DataEntity> ValidationResult validateApiData(ServiceContext context, ValidationRequestConfiguration configuration, DataValidationRequest request, E entity) {
        Future<ValidationResult> future = context.submit(() -> {
            // Always scan to add for none persistence data
            SetFakeIdsEntityVisitor idVisitor = new SetFakeIdsEntityVisitor();
            entity.accept(idVisitor);
            try (ValidationContextSupport validationContext = context.createServiceValidationContext(configuration, request)) {
                ReferentialLocale referentialLocale = context.getReferentialLocale();
                try (ValidationResultBuilder resultBuilder = ValidationResultBuilder.create()) {
                    // We need to inject species dto (required by some validators)
                    Set<SpeciesDto> species = Species.loadEntitiesToDto(context);
                    ValidationMessageDetector detector = ValidationMessageDetector.forData(context, toValidatorsMap(request, validationContext), validationContext, validationContext.getSelectModel(), referentialLocale, resultBuilder);
                    long t0 = TimeLog.getTime();
                    try {
                        validateData(detector, validationContext, entity, species);
                    } finally {
                        idVisitor.removeIds();
                        TIME_LOG.log(t0, "validateData", entity.getId());
                    }
                    return resultBuilder.build();
                }
            }
        });
        return get(future);
    }

    public static ValidationResult validateData(ServiceContext context, ValidationRequestConfiguration configuration, DataValidationRequest request) {
        Future<ValidationResult> future = context.submit(() -> {
            try (ValidationContextSupport validationContext = context.createServiceValidationContext(configuration, request)) {
                ReferentialLocale referentialLocale = context.getReferentialLocale();
                try (ValidationResultBuilder resultBuilder = ValidationResultBuilder.create()) {
                    // We need to inject species dto (required by some validators)
                    Set<SpeciesDto> species = Species.loadEntitiesToDto(context);
                    ValidationMessageDetector detector = ValidationMessageDetector.forData(context, toValidatorsMap(request, validationContext), validationContext, validationContext.getSelectModel(), referentialLocale, resultBuilder);
                    for (String dataId : request.getDataIds()) {
                        DataDtoEntityContext<?, ?, ?, ?> spi;
                        if (TripAware.isSeineId(dataId)) {
                            spi = fr.ird.observe.entities.data.ps.common.Trip.SPI;
                        } else {
                            spi = fr.ird.observe.entities.data.ll.common.Trip.SPI;
                        }
                        validateData(context, spi, dataId, detector, validationContext, species);
                    }
                    return resultBuilder.build();
                }
            }
        });
        return get(future);
    }

    public static <D extends ReferentialDto, E extends ReferentialEntity> ValidationResult validateApiReferential(ServiceContext context, ValidationRequestConfiguration configuration, ReferentialValidationRequest request, Class<D> dtoType, E entity) {
        Future<ValidationResult> future = context.submit(() -> {
            try (ServiceValidationContext validationContext = (ServiceValidationContext) context.createServiceValidationContext(configuration, request)) {
                ReferentialLocale referentialLocale = context.getReferentialLocale();
                try (ValidationResultBuilder resultBuilder = ValidationResultBuilder.create()) {
                    ValidationMessageDetector detector = ValidationMessageDetector.forReferential(context, toValidatorsMap(request, validationContext), validationContext, referentialLocale, resultBuilder);
                    boolean notPersisted = entity.isNotPersisted();
                    if (notPersisted) {
                        entity.setTopiaId("__create");
                    }
                    try {
                        validateReferential(dtoType, detector, validationContext, Collections.singletonList(entity));
                    } finally {
                        if (notPersisted) {
                            entity.setTopiaId(null);
                        }
                    }
                    return resultBuilder.build();
                }
            }
        });
        return get(future);
    }

    private static NuitonValidatorsMap toValidatorsMap(ReferentialValidationRequest request, ServiceValidationContext validationContext) {
        return NuitonValidatorsMap.create(NuitonValidatorProviders.newProvider(NuitonValidatorProviders.getDefaultFactoryName(), validationContext), ObserveBusinessProject.get().getReferentialTypes(), request.getValidationContext(), request.getScopes()).build();
    }

    public static ValidationResult validateReferential(ServiceContext context, ValidationRequestConfiguration configuration, ReferentialValidationRequest request) {
        Future<ValidationResult> future = context.submit(() -> {
            try (ServiceValidationContext validationContext = (ServiceValidationContext) context.createServiceValidationContext(configuration, request)) {
                ReferentialLocale referentialLocale = context.getReferentialLocale();
                try (ValidationResultBuilder resultBuilder = ValidationResultBuilder.create()) {
                    ValidationMessageDetector detector = ValidationMessageDetector.forReferential(context, toValidatorsMap(request, validationContext), validationContext, referentialLocale, resultBuilder);
                    for (Class<? extends ReferentialDto> type : request.getReferentialTypes()) {
                        ReferentialDtoEntityContext<?, ?, ?, ?> spi = context.fromReferentialDto(type);
                        validateReferential(context, spi, detector, validationContext);
                    }
                    return resultBuilder.build();
                }
            }
        });
        return get(future);
    }

    public static void validateData(ServiceContext context,
                                    DataDtoEntityContext<?, ?, ?, ?> spi,
                                    String dataId,
                                    ValidationMessageDetector detector,
                                    ValidationContextSupport validationContext,
                                    Set<SpeciesDto> species) {
        DataEntity data = spi.loadEntity(context, dataId);
        validateData(detector, validationContext, data, species);
    }

    public static void validateReferential(ServiceContext context, ReferentialDtoEntityContext<?, ?, ?, ?> spi, ValidationMessageDetector detector, ValidationContextSupport validationContext) {
        List<? extends ReferentialEntity> entities = spi.loadEntities(context);
        validateReferential(spi.toDtoType(), detector, validationContext, entities);
    }

    public static <D extends ReferentialDto, R extends ReferentialDtoReference, E extends ReferentialEntity> void validateReferential(Class<D> dtoType, ValidationMessageDetector detector, ValidationContextSupport validationContext, List<E> entities) {
        long t0 = TimeLog.getTime();
        ReferentialDtoEntityContext<D, R, E, ?> spi = ObservePersistenceBusinessProject.fromReferentialDto(dtoType);
        log.info(String.format("Validate %d referential(s) of type: %s", entities.size(), TypeTranslators.getSimplifiedName(dtoType)));
        List<R> dtoList = spi.toReferentialReferenceList(validationContext.getReferentialLocale(), entities);
        validationContext.setEditingReferentielList(dtoList);
        TIME_LOG.log(t0, "loadReferential", dtoType.getName());
        t0 = TimeLog.getTime();
        try {
            detector.detectValidationMessages(entities);
        } finally {
            validationContext.setEditingReferentielList(null);
            TIME_LOG.log(t0, "validateReferential", dtoType.getName());
        }
    }

    public static void validateData(ValidationMessageDetector detector, ValidationContextSupport validationContext, DataEntity entity, Set<SpeciesDto> species) {
        long t0 = TimeLog.getTime();
        species.forEach(validationContext::add);
        detector.detectValidationMessages(entity);
        TIME_LOG.log(t0, "validateData", entity.getId());
    }

    private static <X> X get(Future<X> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new ValidationException(e.getCause());
        }
    }
}
