<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  ObServe Core :: Persistence :: Java
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<hibernate-mapping xmlns="http://www.hibernate.org/xsd/hibernate-mapping"
                   xsi:schemaLocation="http://www.hibernate.org/xsd/hibernate-mapping classpath://org/hibernate/hibernate-mapping-4.0.xsd"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   default-access="field" auto-import="true" package="fr.ird.observe.entities.data.ps.observation">
    <class name="fr.ird.observe.entities.data.ps.observation.SampleImpl" table="sample" abstract="false" schema="ps_observation" proxy="fr.ird.observe.entities.data.ps.observation.Sample" entity-name="fr.ird.observe.entities.data.ps.observation.SampleImpl">
        <id name="topiaId" type="string" length="255"/>
        <version name="topiaVersion" type="long"/>
           <property name="topiaCreateDate" access="field" type="timestamp" not-null="true">
               <column name="topiaCreateDate" sql-type="timestamp"/>
           </property>
        <property name="lastUpdateDate" access="field" type="timestamp" not-null="true">
            <column default="CURRENT_TIMESTAMP" name="lastUpdateDate" sql-type="timestamp"/>
        </property>
        <property name="homeId" access="field" type="java.lang.String" column="homeId"/>
        <property name="comment" access="field" type="java.lang.String" length="8192">
            <column length="8192" name="comment"/>
        </property>
        <list name="sampleMeasure" lazy="true" cascade="all,delete-orphan" >
            <key column="sample" foreign-key="fk_ps_observation_sample_samplemeasure" not-null="true" />
            <list-index column="sample_idx"/>
            <one-to-many class="fr.ird.observe.entities.data.ps.observation.SampleMeasureImpl"/>
        </list>
    </class>
    <sql-query name="fr.ird.observe.entities.data.ps.observation.Sample::GetLastUpdateDate" read-only="true"
               comment="Get last update date for the given entity type"><![CDATA[
    SELECT max(lastUpdateDate) FROM ps_observation.sample;
]]></sql-query>
    <query name="fr.ird.observe.entities.data.ps.observation.Sample::id::all" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.data.ps.observation.SampleImpl as e
]]></query>
    <query name="fr.ird.observe.entities.data.ps.observation.Sample::id::equals" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.data.ps.observation.SampleImpl as e
    where e.id = ?1
]]></query>
    <query name="fr.ird.observe.entities.data.ps.observation.Sample::id::in" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.data.ps.observation.SampleImpl as e
    where e.id in ( ?1 )
]]></query>
    <query name="fr.ird.observe.entities.data.ps.observation.Sample::id::before" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.data.ps.observation.SampleImpl as e
    where e.lastUpdateDate <= ?1
]]></query>
    <query name="fr.ird.observe.entities.data.ps.observation.Sample::id::after" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.data.ps.observation.SampleImpl as e
    where e.lastUpdateDate > ?1
]]></query>
    <sql-query name="fr.ird.observe.entities.data.ps.observation.Sample::getParentId::fr.ird.observe.entities.data.ps.observation.Set-sample" read-only="true"><![CDATA[
    select p.topiaId, p.lastUpdateDate from ps_observation.set p inner join ps_observation.sample e on e.set = p.topiaId where e.topiaId = ?
]]></sql-query>
    <sql-query name="fr.ird.observe.entities.data.ps.observation.Sample::byParentId::fr.ird.observe.entities.data.ps.observation.Set-sample" read-only="true"><![CDATA[
    select distinct(e.topiaId) from ps_observation.sample e where e.set in ( ?1 )
]]></sql-query>
    <database-object>
        <create>CREATE INDEX idx_ps_observation_sample_lastupdatedate ON ps_observation.sample(lastUpdateDate)</create>
        <drop>DROP INDEX idx_ps_observation_sample_lastupdatedate</drop>
    </database-object>
    <database-object>
        <create>CREATE INDEX idx_ps_observation_samplemeasure_sample ON ps_observation.sampleMeasure(sample)</create>
        <drop>DROP INDEX idx_ps_observation_samplemeasure_sample</drop>
    </database-object>
</hibernate-mapping>
