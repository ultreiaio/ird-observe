<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  ObServe Core :: Persistence :: Java
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<hibernate-mapping xmlns="http://www.hibernate.org/xsd/hibernate-mapping"
                   xsi:schemaLocation="http://www.hibernate.org/xsd/hibernate-mapping classpath://org/hibernate/hibernate-mapping-4.0.xsd"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   default-access="field" auto-import="true" package="fr.ird.observe.entities.referential.common">
    <class name="fr.ird.observe.entities.referential.common.LengthLengthParameterImpl" table="lengthLengthParameter" abstract="false" schema="common" proxy="fr.ird.observe.entities.referential.common.LengthLengthParameter" entity-name="fr.ird.observe.entities.referential.common.LengthLengthParameterImpl">
        <id name="topiaId" type="string" length="255"/>
        <version name="topiaVersion" type="long"/>
           <property name="topiaCreateDate" access="field" type="timestamp" not-null="true">
               <column name="topiaCreateDate" sql-type="timestamp"/>
           </property>
        <property name="lastUpdateDate" access="field" type="timestamp" not-null="true">
            <column default="CURRENT_TIMESTAMP" name="lastUpdateDate" sql-type="timestamp"/>
        </property>
        <property name="code" access="field" type="java.lang.String" column="code"/>
        <property name="uri" access="field" type="java.lang.String" column="uri"/>
        <property name="homeId" access="field" type="java.lang.String" column="homeId"/>
        <property name="needComment" access="field" type="boolean" not-null="true">
            <column default="false" name="needComment"/>
        </property>
        <property name="status" access="field" column="status" not-null="true">
            <type name="org.hibernate.type.EnumType">
                <param name="enumClass">fr.ird.observe.dto.referential.ReferenceStatus</param>
            </type>
        </property>
        <many-to-one name="ocean" class="fr.ird.observe.entities.referential.common.OceanImpl" column="ocean" foreign-key="fk_common_lengthlengthparameter_ocean" lazy="false" not-null="true" />
        <many-to-one name="species" class="fr.ird.observe.entities.referential.common.SpeciesImpl" column="species" foreign-key="fk_common_lengthlengthparameter_species" lazy="false" not-null="true" />
        <many-to-one name="sex" class="fr.ird.observe.entities.referential.common.SexImpl" column="sex" foreign-key="fk_common_lengthlengthparameter_sex" lazy="false" not-null="true" />
        <many-to-one name="inputSizeMeasureType" class="fr.ird.observe.entities.referential.common.SizeMeasureTypeImpl" column="inputSizeMeasureType" foreign-key="fk_common_lengthlengthparameter_inputsizemeasuretype" lazy="false" not-null="true" />
        <many-to-one name="outputSizeMeasureType" class="fr.ird.observe.entities.referential.common.SizeMeasureTypeImpl" column="outputSizeMeasureType" foreign-key="fk_common_lengthlengthparameter_outputsizemeasuretype" lazy="false" not-null="true" />
        <property name="startDate" access="field" type="date" column="startDate"/>
        <property name="endDate" access="field" type="date" column="endDate"/>
        <property name="coefficients" access="field" type="java.lang.String" column="coefficients" not-null="true"/>
        <property name="source" access="field" type="java.lang.String" length="8192">
            <column length="8192" name="source"/>
        </property>
        <property name="inputOutputFormula" access="field" type="java.lang.String" column="inputOutputFormula" not-null="true"/>
        <property name="outputInputFormula" access="field" type="java.lang.String" column="outputInputFormula" not-null="true"/>
    </class>
    <sql-query name="fr.ird.observe.entities.referential.common.LengthLengthParameter::GetLastUpdateDate" read-only="true"
               comment="Get last update date for the given entity type"><![CDATA[
    SELECT max(lastUpdateDate) FROM common.lengthLengthParameter;
]]></sql-query>
    <query name="fr.ird.observe.entities.referential.common.LengthLengthParameter::id::all" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.referential.common.LengthLengthParameterImpl as e
]]></query>
    <query name="fr.ird.observe.entities.referential.common.LengthLengthParameter::id::equals" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.referential.common.LengthLengthParameterImpl as e
    where e.id = ?1
]]></query>
    <query name="fr.ird.observe.entities.referential.common.LengthLengthParameter::id::in" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.referential.common.LengthLengthParameterImpl as e
    where e.id in ( ?1 )
]]></query>
    <query name="fr.ird.observe.entities.referential.common.LengthLengthParameter::id::before" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.referential.common.LengthLengthParameterImpl as e
    where e.lastUpdateDate <= ?1
]]></query>
    <query name="fr.ird.observe.entities.referential.common.LengthLengthParameter::id::after" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.referential.common.LengthLengthParameterImpl as e
    where e.lastUpdateDate > ?1
]]></query>
    <database-object>
        <create>CREATE INDEX idx_common_lengthlengthparameter_inputsizemeasuretype ON common.lengthLengthParameter(inputSizeMeasureType)</create>
        <drop>DROP INDEX idx_common_lengthlengthparameter_inputsizemeasuretype</drop>
    </database-object>
    <database-object>
        <create>CREATE INDEX idx_common_lengthlengthparameter_lastupdatedate ON common.lengthLengthParameter(lastUpdateDate)</create>
        <drop>DROP INDEX idx_common_lengthlengthparameter_lastupdatedate</drop>
    </database-object>
    <database-object>
        <create>CREATE INDEX idx_common_lengthlengthparameter_ocean ON common.lengthLengthParameter(ocean)</create>
        <drop>DROP INDEX idx_common_lengthlengthparameter_ocean</drop>
    </database-object>
    <database-object>
        <create>CREATE INDEX idx_common_lengthlengthparameter_outputsizemeasuretype ON common.lengthLengthParameter(outputSizeMeasureType)</create>
        <drop>DROP INDEX idx_common_lengthlengthparameter_outputsizemeasuretype</drop>
    </database-object>
    <database-object>
        <create>CREATE INDEX idx_common_lengthlengthparameter_sex ON common.lengthLengthParameter(sex)</create>
        <drop>DROP INDEX idx_common_lengthlengthparameter_sex</drop>
    </database-object>
    <database-object>
        <create>CREATE INDEX idx_common_lengthlengthparameter_species ON common.lengthLengthParameter(species)</create>
        <drop>DROP INDEX idx_common_lengthlengthparameter_species</drop>
    </database-object>
</hibernate-mapping>
