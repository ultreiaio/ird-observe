<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  ObServe Core :: Persistence :: Java
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<hibernate-mapping xmlns="http://www.hibernate.org/xsd/hibernate-mapping"
                   xsi:schemaLocation="http://www.hibernate.org/xsd/hibernate-mapping classpath://org/hibernate/hibernate-mapping-4.0.xsd"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   default-access="field" auto-import="true" package="fr.ird.observe.entities.referential.common">
    <class name="fr.ird.observe.entities.referential.common.OceanImpl" table="ocean" abstract="false" schema="common" proxy="fr.ird.observe.entities.referential.common.Ocean" entity-name="fr.ird.observe.entities.referential.common.OceanImpl">
        <id name="topiaId" type="string" length="255"/>
        <version name="topiaVersion" type="long"/>
           <property name="topiaCreateDate" access="field" type="timestamp" not-null="true">
               <column name="topiaCreateDate" sql-type="timestamp"/>
           </property>
        <property name="lastUpdateDate" access="field" type="timestamp" not-null="true">
            <column default="CURRENT_TIMESTAMP" name="lastUpdateDate" sql-type="timestamp"/>
        </property>
        <property name="code" access="field" type="java.lang.String" column="code" not-null="true"/>
        <property name="uri" access="field" type="java.lang.String" column="uri"/>
        <property name="homeId" access="field" type="java.lang.String" column="homeId"/>
        <property name="needComment" access="field" type="boolean" not-null="true">
            <column default="false" name="needComment"/>
        </property>
        <property name="status" access="field" column="status" not-null="true">
            <type name="org.hibernate.type.EnumType">
                <param name="enumClass">fr.ird.observe.dto.referential.ReferenceStatus</param>
            </type>
        </property>
        <property name="label1" access="field" type="java.lang.String" column="label1" not-null="true"/>
        <property name="label2" access="field" type="java.lang.String" column="label2" not-null="true"/>
        <property name="label3" access="field" type="java.lang.String" column="label3" not-null="true"/>
        <property name="label4" access="field" type="java.lang.String" column="label4"/>
        <property name="label5" access="field" type="java.lang.String" column="label5"/>
        <property name="label6" access="field" type="java.lang.String" column="label6"/>
        <property name="label7" access="field" type="java.lang.String" column="label7"/>
        <property name="label8" access="field" type="java.lang.String" column="label8"/>
        <property name="northEastAllowed" access="field" type="boolean" not-null="true">
            <column default="false" name="northEastAllowed"/>
        </property>
        <property name="southEastAllowed" access="field" type="boolean" not-null="true">
            <column default="false" name="southEastAllowed"/>
        </property>
        <property name="southWestAllowed" access="field" type="boolean" not-null="true">
            <column default="false" name="southWestAllowed"/>
        </property>
        <property name="northWestAllowed" access="field" type="boolean" not-null="true">
            <column default="false" name="northWestAllowed"/>
        </property>
    </class>
    <sql-query name="fr.ird.observe.entities.referential.common.Ocean::GetLastUpdateDate" read-only="true"
               comment="Get last update date for the given entity type"><![CDATA[
    SELECT max(lastUpdateDate) FROM common.ocean;
]]></sql-query>
    <query name="fr.ird.observe.entities.referential.common.Ocean::id::all" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.referential.common.OceanImpl as e
]]></query>
    <query name="fr.ird.observe.entities.referential.common.Ocean::id::equals" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.referential.common.OceanImpl as e
    where e.id = ?1
]]></query>
    <query name="fr.ird.observe.entities.referential.common.Ocean::id::in" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.referential.common.OceanImpl as e
    where e.id in ( ?1 )
]]></query>
    <query name="fr.ird.observe.entities.referential.common.Ocean::id::before" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.referential.common.OceanImpl as e
    where e.lastUpdateDate <= ?1
]]></query>
    <query name="fr.ird.observe.entities.referential.common.Ocean::id::after" read-only="true"><![CDATA[
    select new fr.ird.observe.dto.ToolkitIdBean(id, lastUpdateDate)
    from fr.ird.observe.entities.referential.common.OceanImpl as e
    where e.lastUpdateDate > ?1
]]></query>
    <database-object>
        <create>CREATE INDEX idx_common_ocean_lastupdatedate ON common.ocean(lastUpdateDate)</create>
        <drop>DROP INDEX idx_common_ocean_lastupdatedate</drop>
    </database-object>
</hibernate-mapping>
