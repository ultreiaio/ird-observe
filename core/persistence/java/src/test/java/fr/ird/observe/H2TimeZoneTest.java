package fr.ird.observe;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by tchemit on 13/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class H2TimeZoneTest {

    public static final TimeZone TIME_ZONE = TimeZone.getTimeZone("Europe/Paris");
    static String JDBC_URL;
    static Path path;
    static java.util.Date expectedDate;

    @BeforeClass
    public static void beforeClass() throws IOException, SQLException {

        Path dir = Paths.get(System.getProperty("java.io.tmpdir"));
        Files.createDirectories(dir);

        path = dir.resolve("H2TimeZoneTest-" + System.nanoTime());
        Files.createDirectories(path);
        JDBC_URL = String.format("jdbc:h2:file:%s/test", path.toFile().getAbsolutePath());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2016);
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        expectedDate = calendar.getTime();


        try (Connection connection = DriverManager.getConnection(JDBC_URL)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("CREATE TABLE test (simpleDate DATE)")) {
                preparedStatement.execute();
            }

            try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO test VALUES( ? );")) {

                preparedStatement.setDate(1, new java.sql.Date(expectedDate.getTime()));
                preparedStatement.execute();
            }
            connection.commit();
        }
    }

    @Test
    public void testWithDefaultTimeZone() {

        TimeZone.setDefault(TIME_ZONE);
        try (Connection connection = DriverManager.getConnection(JDBC_URL)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM test LIMIT 1;")) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    Assert.assertTrue(resultSet.next());
                    Date actualDate = resultSet.getDate(1);
                    Assert.assertEquals(expectedDate, actualDate);
                }
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testWithUtcTimeZone() {

        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        try (Connection connection = DriverManager.getConnection(JDBC_URL)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM test LIMIT 1;")) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    Assert.assertTrue(resultSet.next());
                    Date actualDate = resultSet.getDate(1);
                    Assert.assertEquals(expectedDate, actualDate);
                }
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
