package fr.ird.observe.entities.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DeletePartialRequest;
import fr.ird.observe.datasource.request.ReplicatePartialRequest;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.entities.ObserveTopiaEntitySqlModelResource;
import fr.ird.observe.services.service.data.MoveLayoutRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 05/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TripSpiTest {

    public static final List<String> GEAR_USE_FEATURES = List.of("ps_common.gearUseFeatures",
                                                                 "ps_common.gearUseFeaturesMeasurement");
    public static final List<String> OBSERVATION = List.of("ps_observation.route",
                                                           "ps_observation.activity",
                                                           "ps_observation.set",
                                                           "ps_observation.catch",
                                                           "ps_observation.floatingObject",
                                                           "ps_observation.floatingObjectPart",
                                                           "ps_observation.nonTargetCatchRelease",
                                                           "ps_observation.objectObservedSpecies",
                                                           "ps_observation.objectSchoolEstimate",
                                                           "ps_observation.schoolEstimate",
                                                           "ps_observation.transmittingBuoy",
                                                           "ps_observation.sample",
                                                           "ps_observation.sampleMeasure",
                                                           "ps_observation.activity_observedSystem");
    public static final List<String> LOGBOOK = List.of("ps_logbook.sample",
                                                       "ps_logbook.route",
                                                       "ps_logbook.activity",
                                                       "ps_logbook.sampleActivity",
                                                       "ps_logbook.catch",
                                                       "ps_logbook.sampleSpecies",
                                                       "ps_logbook.sampleSpeciesMeasure",
                                                       "ps_logbook.well",
                                                       "ps_logbook.wellActivity",
                                                       "ps_logbook.wellActivitySpecies",
                                                       "ps_logbook.floatingObject",
                                                       "ps_logbook.floatingObjectPart",
                                                       "ps_logbook.transmittingBuoy",
                                                       "ps_logbook.sample_person",
                                                       "ps_logbook.activity_observedSystem");
    public static final List<String> LANDING = List.of("ps_landing.landing");
    public static final List<String> GEAR_USE_FEATURES_AND_LANDING = List.of("ps_landing.landing",
                                                                             "ps_common.gearUseFeatures",
                                                                             "ps_common.gearUseFeaturesMeasurement");
    public static final List<String> GEAR_USE_FEATURES_AND_LOGBOOK = List.of("ps_common.gearUseFeatures",
                                                                             "ps_common.gearUseFeaturesMeasurement",
                                                                             "ps_logbook.sample",
                                                                             "ps_logbook.route",
                                                                             "ps_logbook.activity",
                                                                             "ps_logbook.sampleActivity",
                                                                             "ps_logbook.catch",
                                                                             "ps_logbook.sampleSpecies",
                                                                             "ps_logbook.sampleSpeciesMeasure",
                                                                             "ps_logbook.well",
                                                                             "ps_logbook.wellActivity",
                                                                             "ps_logbook.wellActivitySpecies",
                                                                             "ps_logbook.floatingObject",
                                                                             "ps_logbook.floatingObjectPart",
                                                                             "ps_logbook.transmittingBuoy",
                                                                             "ps_logbook.sample_person",
                                                                             "ps_logbook.activity_observedSystem");
    public static final List<String> LOCALMARKET = List.of("ps_localmarket.survey",
                                                           "ps_localmarket.batch",
                                                           "ps_localmarket.surveyPart",
                                                           "ps_localmarket.sample",
                                                           "ps_localmarket.sampleSpecies",
                                                           "ps_localmarket.sampleSpeciesMeasure",
                                                           "ps_localmarket.sample_well");
    public static final List<String> OBSERVATION_AND_LOCALMARKET = List.of("ps_localmarket.survey",
                                                                           "ps_localmarket.batch",
                                                                           "ps_localmarket.surveyPart",
                                                                           "ps_observation.route",
                                                                           "ps_observation.activity",
                                                                           "ps_localmarket.sample",
                                                                           "ps_localmarket.sampleSpecies",
                                                                           "ps_localmarket.sampleSpeciesMeasure",
                                                                           "ps_observation.set",
                                                                           "ps_observation.catch",
                                                                           "ps_observation.floatingObject",
                                                                           "ps_observation.floatingObjectPart",
                                                                           "ps_observation.nonTargetCatchRelease",
                                                                           "ps_observation.objectObservedSpecies",
                                                                           "ps_observation.objectSchoolEstimate",
                                                                           "ps_observation.schoolEstimate",
                                                                           "ps_observation.transmittingBuoy",
                                                                           "ps_observation.sample",
                                                                           "ps_observation.sampleMeasure",
                                                                           "ps_observation.activity_observedSystem",
                                                                           "ps_localmarket.sample_well");
    public static final List<String> OBSERVATION_AND_LOGBOOK = List.of("ps_logbook.sample",
                                                                       "ps_logbook.route",
                                                                       "ps_observation.route",
                                                                       "ps_observation.activity",
                                                                       "ps_logbook.activity",
                                                                       "ps_logbook.sampleActivity",
                                                                       "ps_logbook.catch",
                                                                       "ps_logbook.sampleSpecies",
                                                                       "ps_logbook.sampleSpeciesMeasure",
                                                                       "ps_logbook.well",
                                                                       "ps_logbook.wellActivity",
                                                                       "ps_logbook.wellActivitySpecies",
                                                                       "ps_logbook.floatingObject",
                                                                       "ps_logbook.floatingObjectPart",
                                                                       "ps_logbook.transmittingBuoy",
                                                                       "ps_observation.set",
                                                                       "ps_observation.catch",
                                                                       "ps_observation.floatingObject",
                                                                       "ps_observation.floatingObjectPart",
                                                                       "ps_observation.nonTargetCatchRelease",
                                                                       "ps_observation.objectObservedSpecies",
                                                                       "ps_observation.objectSchoolEstimate",
                                                                       "ps_observation.schoolEstimate",
                                                                       "ps_observation.transmittingBuoy",
                                                                       "ps_observation.sample",
                                                                       "ps_observation.sampleMeasure",
                                                                       "ps_logbook.sample_person",
                                                                       "ps_observation.activity_observedSystem",
                                                                       "ps_logbook.activity_observedSystem");
    public static final List<String> OBSERVATION_AND_LOGBOOK_AND_LOCALMARKET = List.of("ps_localmarket.survey",
                                                                                       "ps_localmarket.batch",
                                                                                       "ps_localmarket.surveyPart",
                                                                                       "ps_logbook.sample",
                                                                                       "ps_logbook.route",
                                                                                       "ps_observation.route",
                                                                                       "ps_observation.activity",
                                                                                       "ps_logbook.activity",
                                                                                       "ps_logbook.sampleActivity",
                                                                                       "ps_localmarket.sample",
                                                                                       "ps_localmarket.sampleSpecies",
                                                                                       "ps_localmarket.sampleSpeciesMeasure",
                                                                                       "ps_logbook.catch",
                                                                                       "ps_logbook.sampleSpecies",
                                                                                       "ps_logbook.sampleSpeciesMeasure",
                                                                                       "ps_logbook.well",
                                                                                       "ps_logbook.wellActivity",
                                                                                       "ps_logbook.wellActivitySpecies",
                                                                                       "ps_logbook.floatingObject",
                                                                                       "ps_logbook.floatingObjectPart",
                                                                                       "ps_logbook.transmittingBuoy",
                                                                                       "ps_observation.set",
                                                                                       "ps_observation.catch",
                                                                                       "ps_observation.floatingObject",
                                                                                       "ps_observation.floatingObjectPart",
                                                                                       "ps_observation.nonTargetCatchRelease",
                                                                                       "ps_observation.objectObservedSpecies",
                                                                                       "ps_observation.objectSchoolEstimate",
                                                                                       "ps_observation.schoolEstimate",
                                                                                       "ps_observation.transmittingBuoy",
                                                                                       "ps_observation.sample",
                                                                                       "ps_observation.sampleMeasure",
                                                                                       "ps_logbook.sample_person",
                                                                                       "ps_observation.activity_observedSystem",
                                                                                       "ps_logbook.activity_observedSystem",
                                                                                       "ps_localmarket.sample_well");

    public static final List<String> LOGBOOK_AND_LOCALMARKET = List.of("ps_localmarket.survey",
                                                                       "ps_localmarket.batch",
                                                                       "ps_localmarket.surveyPart",
                                                                       "ps_logbook.sample",
                                                                       "ps_logbook.route",
                                                                       "ps_logbook.activity",
                                                                       "ps_logbook.sampleActivity",
                                                                       "ps_localmarket.sample",
                                                                       "ps_localmarket.sampleSpecies",
                                                                       "ps_localmarket.sampleSpeciesMeasure",
                                                                       "ps_logbook.catch",
                                                                       "ps_logbook.sampleSpecies",
                                                                       "ps_logbook.sampleSpeciesMeasure",
                                                                       "ps_logbook.well",
                                                                       "ps_logbook.wellActivity",
                                                                       "ps_logbook.wellActivitySpecies",
                                                                       "ps_logbook.floatingObject",
                                                                       "ps_logbook.floatingObjectPart",
                                                                       "ps_logbook.transmittingBuoy",
                                                                       "ps_logbook.sample_person",
                                                                       "ps_logbook.activity_observedSystem",
                                                                       "ps_localmarket.sample_well");
    private ObserveTopiaEntitySqlModelResource resource;

    @Before
    public void setUp() throws Exception {
        resource = ObserveTopiaEntitySqlModelResource.get();
    }

    @Test
    public void layoutTypesToDatabaseTables() {

        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripGearUseFeaturesDto.class), GEAR_USE_FEATURES);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.observation.RouteDto.class), OBSERVATION);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLogbookDto.class), LOGBOOK);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class), LOCALMARKET);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.landing.TripLandingDto.class), LANDING);

        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripGearUseFeaturesDto.class, fr.ird.observe.dto.data.ps.common.TripLogbookDto.class), GEAR_USE_FEATURES_AND_LOGBOOK);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLogbookDto.class, fr.ird.observe.dto.data.ps.common.TripGearUseFeaturesDto.class), GEAR_USE_FEATURES_AND_LOGBOOK);

        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.observation.RouteDto.class, fr.ird.observe.dto.data.ps.common.TripLogbookDto.class), OBSERVATION_AND_LOGBOOK);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLogbookDto.class, fr.ird.observe.dto.data.ps.observation.RouteDto.class), OBSERVATION_AND_LOGBOOK);

        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class, fr.ird.observe.dto.data.ps.observation.RouteDto.class), OBSERVATION_AND_LOCALMARKET);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.observation.RouteDto.class, fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class), OBSERVATION_AND_LOCALMARKET);

        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class, fr.ird.observe.dto.data.ps.common.TripLogbookDto.class), LOGBOOK_AND_LOCALMARKET);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLogbookDto.class, fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class), LOGBOOK_AND_LOCALMARKET);

        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class, fr.ird.observe.dto.data.ps.common.TripLogbookDto.class, fr.ird.observe.dto.data.ps.observation.RouteDto.class), OBSERVATION_AND_LOGBOOK_AND_LOCALMARKET);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class, fr.ird.observe.dto.data.ps.observation.RouteDto.class, fr.ird.observe.dto.data.ps.common.TripLogbookDto.class), OBSERVATION_AND_LOGBOOK_AND_LOCALMARKET);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLogbookDto.class, fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class, fr.ird.observe.dto.data.ps.observation.RouteDto.class), OBSERVATION_AND_LOGBOOK_AND_LOCALMARKET);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripLogbookDto.class, fr.ird.observe.dto.data.ps.observation.RouteDto.class, fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class), OBSERVATION_AND_LOGBOOK_AND_LOCALMARKET);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.observation.RouteDto.class, fr.ird.observe.dto.data.ps.common.TripLogbookDto.class, fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class), OBSERVATION_AND_LOGBOOK_AND_LOCALMARKET);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.observation.RouteDto.class, fr.ird.observe.dto.data.ps.common.TripLocalmarketDto.class, fr.ird.observe.dto.data.ps.common.TripLogbookDto.class), OBSERVATION_AND_LOGBOOK_AND_LOCALMARKET);

        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.common.TripGearUseFeaturesDto.class, fr.ird.observe.dto.data.ps.landing.TripLandingDto.class), GEAR_USE_FEATURES_AND_LANDING);
        assertLayoutTypesToDatabaseTables(List.of(fr.ird.observe.dto.data.ps.landing.TripLandingDto.class, fr.ird.observe.dto.data.ps.common.TripGearUseFeaturesDto.class), GEAR_USE_FEATURES_AND_LANDING);
    }

    private void assertLayoutTypesToDatabaseTables(List<Class<? extends DataDto>> dtoTypes, List<String> expected) {
        MoveLayoutRequest moveLayoutRequest = new MoveLayoutRequest(new LinkedHashSet<>(dtoTypes), "old", "new", false);
        ReplicatePartialRequest replicatePartialRequest = Trip.SPI.toSqlRequest(false, moveLayoutRequest);
        Set<String> replicateShell = resource.createShell(replicatePartialRequest);
        Assert.assertEquals(expected, List.copyOf(replicateShell));
        DeletePartialRequest deleteRequest = replicatePartialRequest.toDeleteRequest();
        Set<String> deleteShell = resource.createShell(deleteRequest);
        Assert.assertEquals(expected, List.copyOf(deleteShell));
    }
}
