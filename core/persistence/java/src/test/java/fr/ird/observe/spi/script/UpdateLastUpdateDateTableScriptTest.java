package fr.ird.observe.spi.script;

/*-
 * #%L
 * ObServe Core :: Persistence :: Java
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ObserveTopiaEntitySqlModelResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;
import org.nuiton.topia.service.sql.script.TopiaEntitySqlScriptModel;
import org.nuiton.topia.service.sql.script.UpdateLastUpdateDateTableScript;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

public class UpdateLastUpdateDateTableScriptTest {

    private TopiaEntitySqlScriptModel scriptModel;
    private TopiaEntitySqlModel model;

    @Before
    public void setUp() {
        model = ObserveTopiaEntitySqlModelResource.get().getModel();
        scriptModel = ObserveTopiaEntitySqlModelResource.get().getScriptModel();
    }

    @Test
    public void stream() {
        for (Class<? extends Entity> type : model.getDescriptorsByType().keySet()) {
            UpdateLastUpdateDateTableScript script = scriptModel.getScript(type.getName()).getLastUpdateDateTableScript();
            Stream<String> stream = script.stream();
            Assert.assertNotNull(stream);
            Assert.assertTrue(stream.iterator().hasNext());
        }
    }

    @Test
    public void generate() {
        Date timeStamp = new Date();
        for (Class<? extends Entity> type : model.getDescriptorsByType().keySet()) {
            UpdateLastUpdateDateTableScript script = scriptModel.getScript(type.getName()).getLastUpdateDateTableScript();
            List<String> requests = script.generate(timeStamp);
            Assert.assertFalse(requests.isEmpty());
        }
    }

}
