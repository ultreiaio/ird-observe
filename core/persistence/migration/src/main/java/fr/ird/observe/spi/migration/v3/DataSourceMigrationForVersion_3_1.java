package fr.ird.observe.spi.migration.v3;

/*
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.LegacyMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.apache.commons.lang3.tuple.Pair;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Migration class for version {@code 3.1}.
 * <p>
 * Created on 10/10/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_3_1 extends LegacyMigrationVersionResource {

    static class GetAllSpeciesOceanDoublonQuery extends SqlQuery<Pair<String, String>> {

        private final String tableName;

        private GetAllSpeciesOceanDoublonQuery(String tableName) {
            this.tableName = tableName;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            String sql = "SELECT e.espece%1$s, e.ocean, count(e.*) " +
                    "FROM  espece%1$s_ocean e \n" +
                    "GROUP BY e.espece%1$s, e.ocean\n" +
                    "HAVING count(e.*) > 1\n" +
                    "ORDER BY e.espece%1$s, e.ocean;";
            return connection.prepareStatement(String.format(sql, tableName));
        }

        @Override
        public Pair<String, String> prepareResult(ResultSet set) throws SQLException {
            return Pair.of(set.getString(1), set.getString(2));
        }
    }

    public DataSourceMigrationForVersion_3_1() {
        super(Version.valueOf("3.1"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        // Doublons et absence de clé primaire dans especefaune_ocean et especethon_ocean
        // (see http://forge.codelutin.com/issues/3398)
        updateReferentielSpecies(executor, "thon");
        updateReferentielSpecies(executor, "faune");
    }

    private void updateReferentielSpecies(MigrationVersionResourceExecutor executor, String speciesType) {

        GetAllSpeciesOceanDoublonQuery request = new GetAllSpeciesOceanDoublonQuery(speciesType);

        List<Pair<String, String>> entities = executor.findMultipleResult(request);

        String deleteQuery = "DELETE FROM espece%1$s_ocean WHERE espece%1$s='%2$s' AND ocean='%3$s';";
        String insertQuery = "INSERT INTO espece%1$s_ocean VALUES('%2$s','%3$s');";
        String addConstraintQuery = "ALTER TABLE espece%1$s_ocean ADD CONSTRAINT espece%1$s_ocean_unique_key UNIQUE ( espece%1$s, ocean );";

        for (Pair<String, String> tuple : entities) {
            String speciesId = tuple.getLeft();
            String oceanId = tuple.getRight();

            executor.writeSql(String.format(deleteQuery, speciesType, speciesId, oceanId));
            executor.writeSql(String.format(insertQuery, speciesType, speciesId, oceanId));
        }

        executor.writeSql(String.format(addConstraintQuery, speciesType));
    }

}
