package fr.ird.observe.spi.migration.v3;

/*
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.LegacyMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on 3/23/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.16
 */
@SuppressWarnings("SqlDialectInspection")
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_3_16 extends LegacyMigrationVersionResource {

    /**
     * Logger.
     */
    private static final Logger log = LogManager.getLogger(DataSourceMigrationForVersion_3_16.class);

    private static class SeineData {

        String tripId;

        Integer seineCircumference;

        Integer seineDepth;

        Integer seineBallastWeight;
    }

    public DataSourceMigrationForVersion_3_16() {
        super(Version.valueOf("3.16"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        executor.addScript("01", "add-gear-referential");
        executor.addScript("02", "add-gear-data");
        migrateTripSeineData(executor);
        executor.addScript("03", "remove-tripSeine-seine");

    }

    private void migrateTripSeineData(MigrationVersionResourceExecutor executor) {

        String gearUseFeaturesIdPrefix = "fr.ird.observe.entities.seine.GearUseFeatures#1427183650941#";

        int gearUseFeaturesCount = -1;
        Set<SeineData> seineData = getSeineData(executor);
        for (SeineData senne : seineData) {

            String gearUseFeaturesId = gearUseFeaturesIdPrefix + (++gearUseFeaturesCount);
            if (log.isInfoEnabled()) {
                log.info(String.format("Transform senne from trip: %s to: %s", senne.tripId, gearUseFeaturesId));
            }
            // Create GearUseFeatures
            executor.writeSql(String.format("INSERT INTO OBSERVE_SEINE.GEARUSEFEATURES(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, TRIP, GEAR, NUMBER) VALUES ('%s', 0, TIMESTAMP '2015-03-24 00:00:00.00', '%s', 'fr.ird.observe.entities.referential.common.Gear#1239832686125#0.20', 1);", gearUseFeaturesId, senne.tripId));

            // Create GearUseFeaturesMeasurements
            String gearUseFeatureMeasurementIdPrefix = "fr.ird.observe.entities.seine.GearUseFeaturesMeasurement#1427183650941#" + gearUseFeaturesCount;
            if (senne.seineCircumference != null) {
                String gearUseFeatureMeasurementId = gearUseFeatureMeasurementIdPrefix + 0;
                executor.writeSql(String.format("INSERT INTO OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, GEARCARACTERISTIC, GEARUSEFEATURES, MEASUREMENTVALUE) VALUES ('%s', 0, TIMESTAMP '2015-03-24 00:00:00.00', 'fr.ird.observe.entities.referential.common.GearCharacteristic#1239832686124#0.7', '%s', '%s' );", gearUseFeatureMeasurementId, gearUseFeaturesId, senne.seineCircumference));
            }
            if (senne.seineDepth != null) {
                String gearUseFeatureMeasurementId = gearUseFeatureMeasurementIdPrefix + 1;
                executor.writeSql(String.format("INSERT INTO OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, GEARCARACTERISTIC, GEARUSEFEATURES, MEASUREMENTVALUE) VALUES ('%s', 0, TIMESTAMP '2015-03-24 00:00:00.00', 'fr.ird.observe.entities.referential.common.GearCharacteristic#1239832686124#0.10', '%s', '%s' );", gearUseFeatureMeasurementId, gearUseFeaturesId, senne.seineBallastWeight));
            }
            if (senne.seineBallastWeight != null) {
                String gearUseFeatureMeasurementId = gearUseFeatureMeasurementIdPrefix + 2;
                executor.writeSql(String.format("INSERT INTO OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, GEARCARACTERISTIC, GEARUSEFEATURES, MEASUREMENTVALUE) VALUES ('%s', 0, TIMESTAMP '2015-03-24 00:00:00.00', 'fr.ird.observe.entities.referential.common.GearCharacteristic#1239832686124#0.9', '%s', '%s' );", gearUseFeatureMeasurementId, gearUseFeaturesId, senne.seineDepth));
            }
        }
    }

    private Set<SeineData> getSeineData(MigrationVersionResourceExecutor tx) {

        final Set<SeineData> result = new HashSet<>();

        tx.doSqlWork(connection -> {
            String sql = "SELECT topiaId, seineCircumference, seineDepth, seineBallastWeight FROM OBSERVE_SEINE.TRIP WHERE seineDepth IS NOT NULL OR seineDepth IS NOT NULL OR seineBallastWeight IS NOT NULL;";
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    SeineData seineData = new SeineData();
                    seineData.tripId = set.getString(1);
                    seineData.seineCircumference = set.getInt(2);
                    seineData.seineDepth = set.getInt(3);
                    seineData.seineBallastWeight = set.getInt(4);
                    result.add(seineData);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain trip senne data", e);
            }
        });

        return result;
    }

}
