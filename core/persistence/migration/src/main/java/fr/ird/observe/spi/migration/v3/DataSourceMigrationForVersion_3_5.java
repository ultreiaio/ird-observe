package fr.ird.observe.spi.migration.v3;

/*
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.LegacyMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


import java.util.Set;

/**
 * Created on 6/19/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_3_5 extends LegacyMigrationVersionResource {

    private static final String[] TABLES = new String[]{
            "ParametrageTaillePoidsFaune",
            "ParametrageTaillePoidsThon",
            "ActiviteEnvironnante",
            "EstimationBancObjet",
            "EspeceFauneObservee",
            "GroupeEspeceFaune",
            "CauseNonCoupSenne",
            "EchantillonFaune",
            "OperationBalise",
            "EchantillonThon",
            "CategorieBateau",
            "SystemeObserve",
            "OperationObjet",
            "EstimationBanc",
            "CategoriePoids",
            "ActiviteBateau",
            "ObjetFlottant",
            "ModeDetection",
            "VentBeaufort",
            "StatutEspece",
            "DevenirObjet",
            "DevenirFaune",
            "CauseCoupNul",
            "CaptureFaune",
            "TailleFaune",
            "RaisonRejet",
            "Observateur",
            "EspeceFaune",
            "EspeceThon",
            "CaptureThon",
            "TypeBateau",
            "TypeBalise",
            "TailleThon",
            "TypeObjet",
            "Programme",
            "Organisme",
            "BaliseLue",
            "Activite",
            "Bateau",
            "Route",
            "Ocean",
            "Maree",
            "Calee",
            "Pays",
            "ESPECEFAUNE_OCEAN",
            "ESPECETHON_OCEAN",
            "ACTIVITE_SYSTEMEOBSERVE"
    };

    public DataSourceMigrationForVersion_3_5() {
        super(Version.valueOf("3.5"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        // Get all especeThon (to migrate them into a speciesList later)
        Set<String> speciesThonIds = executor.getTopiaIds("especethon");

        // Get all especeFaune (to migrate them into a speciesList later)
        Set<String> speciesFauneIds = executor.getTopiaIds("especefaune");

        // translate model (see http://forge.codelutin.com/issues/4115)
        // migrate wind (see http://forge.codelutin.com/issues/5304)
        // migrate persons (see http://forge.codelutin.com/issues/5303)
        // add captain and dataInputor on Trip (see http://forge.codelutin.com/issues/5305)
        // add gearType on Program (see http://forge.codelutin.com/issues/5604)
        translateModel(executor);

        // add SpeciesList (see http://forge.codelutin.com/issues/)
        addSpeciesList(executor, speciesThonIds, speciesFauneIds);

        // add longline schema
        executor.addScript("02", "add-longline-schema");

        // update common references
        executor.addScript("03", "update-common-references");

        // add longline references
        executor.addScript("04", "add-longline-references");

    }

    private void translateModel(MigrationVersionResourceExecutor executor) {

        for (String oldTableName : TABLES) {

            executor.removeFK(oldTableName);
            executor.removeUK(oldTableName);

        }
        executor.addScript("01", "migration");
    }

    private void addSpeciesList(MigrationVersionResourceExecutor executor, Set<String> speciesThonIds, Set<String> speciesFauneIds) {

        String insertListQuery = "INSERT INTO OBSERVE_COMMON.SPECIESLIST (topiaId, topiaCreateDate, topiaversion, code, label1, label2, label3) VALUES('%s', '%s', 0, '%s', '%s', '%s', '%s');";
        String insertSpeciesQuery = "INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST (SPECIESLIST, SPECIES) VALUES('%s','%s');";

        // add targetSpeciesSeine list
        {
            String speciesListId = "fr.ird.observe.entities.referential.common.SpeciesList#1239832675370#0.1";
            executor.writeSql(String.format(insertListQuery, speciesListId, "2014-06-23", "0", "PS - Target species", "PS - Thonidés (espèces ciblées)", "PS - Target species"));
            for (String oldId : speciesThonIds) {
                String newId = oldId.replace("EspeceThon", "Species");
                executor.writeSql(String.format(insertSpeciesQuery, speciesListId, newId));
            }
        }

        // add nonTargetSpeciesSeine list
        {
            String speciesListId = "fr.ird.observe.entities.referential.common.SpeciesList#1239832675370#0.2";
            executor.writeSql(String.format(insertListQuery, speciesListId, "2014-06-23", "1", "PS - Non target species", "PS - Faune associée (espèces non ciblée)", "PS - Non target species"));
            for (String oldId : speciesFauneIds) {
                String newId = oldId.replace("EspeceFaune", "Species");
                executor.writeSql(String.format(insertSpeciesQuery, speciesListId, newId));
            }
        }

        // add catchSpeciesLongline list
        executor.writeSql(String.format(insertListQuery, "fr.ird.observe.entities.referential.common.SpeciesList#1239832675370#0.3", "2014-06-23", "2", "LL - Catch species", "LL - Espèces capturées", "LL - Catch species"));

        // add encounterSpeciesLongline list
        executor.writeSql(String.format(insertListQuery, "fr.ird.observe.entities.referential.common.SpeciesList#1239832675370#0.4", "2014-06-23", "3", "LL - Species for encounters", "LL - Espèces des rencontres", "LL - Species for encounters"));

        // add baitSpeciesLongline list
        executor.writeSql(String.format(insertListQuery, "fr.ird.observe.entities.referential.common.SpeciesList#1239832675370#0.5", "2014-06-23", "4", "LL - Bait species", "LL - Appâts", "LL - Bait species"));

        // add depredatorsSpeciesLongline list
        executor.writeSql(String.format(insertListQuery, "fr.ird.observe.entities.referential.common.SpeciesList#1239832675370#0.6", "2014-06-23", "5", "LL - Depredators", "LL - Déprédateurs", "LL - Depredators"));

    }

}
