package fr.ird.observe.spi.migration.v3;

/*
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.LegacyMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


/**
 * Created on 8/25/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_3_7 extends LegacyMigrationVersionResource {

    public DataSourceMigrationForVersion_3_7() {
        super(Version.valueOf("3.7"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        executor.addScript("01", "rename-seine-model");
        executor.addScript("02", "remove-trip-organism");
        executor.addScript("03", "add-referential-needComment");
        executor.addScript("04", "normalize-triplongline");
        executor.addScript("05", "normalize-activitylongline");
        executor.addScript("06", "normalize-setlongline");
        executor.addScript("07", "normalize-catchlongline");
        executor.addScript("08", "normalize-tdr");
        executor.addScript("09", "add-species-list");
        executor.addScript("10", "increase-comment-field");

    }

}
