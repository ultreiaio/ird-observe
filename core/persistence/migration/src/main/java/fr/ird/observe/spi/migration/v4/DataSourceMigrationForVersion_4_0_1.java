package fr.ird.observe.spi.migration.v4;

/*
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.entities.ObserveIdFactory;
import fr.ird.observe.spi.migration.LegacyMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 6/8/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0.1
 */
@SuppressWarnings("SqlDialectInspection")
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_4_0_1 extends LegacyMigrationVersionResource {

    public DataSourceMigrationForVersion_4_0_1() {
        super(Version.valueOf("4.0.1"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        // See https://forge.codelutin.com/issues/7226
        executor.executeForH2(e -> e.addScript("01", "remove-gender-column"));

        // See https://forge.codelutin.com/issues/7229
        removeForeignKeys(executor);
        executor.addScript("02", "add-missing-fk");

        // See https://forge.codelutin.com/issues/7350
        migrateGearUseFeaturesSeineMeasurementIds(executor);

        // See https://forge.codelutin.com/issues/7328
        recomputeLonglineHaulingIds(executor);

    }

    private void recomputeLonglineHaulingIds(MigrationVersionResourceExecutor executor) {

        class SetData {

            private String id;

            private int totalSectionsCount;

            private int basketsPerSectionCount;

            private int branchlinesPerBasketCount;

        }

        // L'ensemble des opérations de pêche où l'on doit regénerer les générer les haulingIds
        final Set<SetData> setIds = new LinkedHashSet<>();
        executor.doSqlWork(connection -> {
            String sql = "SELECT topiaId, totalSectionsCount, basketsPerSectionCount, branchlinesPerBasketCount FROM OBSERVE_LONGLINE.SET WHERE HAULINGBREAKS = 0 AND haulingDirectionSameAsSetting IS FALSE;";
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    SetData setData = new SetData();
                    setData.id = set.getString(1);
                    setData.totalSectionsCount = set.getInt(2);
                    setData.basketsPerSectionCount = set.getInt(3);
                    setData.branchlinesPerBasketCount = set.getInt(4);
                    setIds.add(setData);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain SET data", e);
            }
        });

        for (SetData setData : setIds) {

            executor.writeSql(String.format("UPDATE OBSERVE_LONGLINE.SECTION SET haulingIdentifier = (%s - settingIdentifier + 1 ) WHERE set = '%s'", setData.totalSectionsCount, setData.id));
            executor.writeSql(String.format("UPDATE OBSERVE_LONGLINE.BASKET SET haulingIdentifier = (%s - settingIdentifier + 1 ) WHERE section IN  ( SELECT topiaid FROM OBSERVE_LONGLINE.SECTION WHERE set ='%s' )", setData.basketsPerSectionCount, setData.id));
            executor.writeSql(String.format("UPDATE OBSERVE_LONGLINE.BRANCHLINE SET haulingIdentifier = (%s - settingIdentifier + 1 ) WHERE basket IN ( SELECT distinct b.topiaid FROM OBSERVE_LONGLINE.BASKET b, OBSERVE_LONGLINE.SECTION s WHERE b.section = s.topiaid AND s.set  = '%s' )", setData.branchlinesPerBasketCount, setData.id));

        }
    }

    private void migrateGearUseFeaturesSeineMeasurementIds(MigrationVersionResourceExecutor executor) {

        final Multimap<String, String> gearUseFeaturesSeineAndMeasurementIds = LinkedHashMultimap.create();
        executor.doSqlWork(connection -> {
            String sql = "SELECT GEARUSEFEATURES, topiaId FROM OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT;";
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    String gearUseFeaturesSeineId = set.getString(1);
                    String gearUseFeaturesMeasurementSeineId = set.getString(2);
                    gearUseFeaturesSeineAndMeasurementIds.put(gearUseFeaturesSeineId, gearUseFeaturesMeasurementSeineId);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain GEARUSEFEATURESMEASUREMENT ids", e);
            }
        });

        ObserveIdFactory topiaIdFactory = new ObserveIdFactory();
        for (String gearUseFeaturesSeineId : gearUseFeaturesSeineAndMeasurementIds.keySet()) {


            String newGearUseFeaturesSeineId = topiaIdFactory.newTopiaIdFromIdPrefix("fr.ird.observe.entities.seine.GearUseFeaturesSeine");

            executor.writeSql(String.format("INSERT INTO OBSERVE_SEINE.GEARUSEFEATURES(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, GEAR, NUMBER) VALUES ('%s', 0, TIMESTAMP '2015-03-24 00:00:00.00', 'fr.ird.observe.entities.referential.common.Gear#1239832686125#0.20', 1)", newGearUseFeaturesSeineId));
            executor.writeSql(String.format("UPDATE OBSERVE_SEINE.GEARUSEFEATURES SET TRIP = (SELECT TRIP FROM OBSERVE_SEINE.GEARUSEFEATURES WHERE topiaId='%s') WHERE topiaId='%s';", gearUseFeaturesSeineId, newGearUseFeaturesSeineId));

            Collection<String> gearUseFeaturesMeasurementSeineIds = gearUseFeaturesSeineAndMeasurementIds.get(gearUseFeaturesSeineId);

            for (String gearUseFeaturesMeasurementSeineId : gearUseFeaturesMeasurementSeineIds) {

                String newGearUseFeaturesMeasurementSeineId = topiaIdFactory.newTopiaIdFromIdPrefix("fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine");
                executor.writeSql(String.format("UPDATE OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT SET GEARUSEFEATURES = '%s', topiaid = '%s' WHERE topiaid='%s'", newGearUseFeaturesSeineId, newGearUseFeaturesMeasurementSeineId, gearUseFeaturesMeasurementSeineId));

            }

            executor.writeSql(String.format("DELETE FROM OBSERVE_SEINE.GEARUSEFEATURES WHERE topiaId='%s'", gearUseFeaturesSeineId));

        }

    }

    private void removeForeignKeys(MigrationVersionResourceExecutor executor) {

        executor.removeFKIfExists("observe_seine", "FLOATINGOBJECT", "OBJECTTYPE");
        executor.removeFKIfExists("observe_seine", "FLOATINGOBJECT", "OBJECTFATE");
        executor.removeFKIfExists("observe_seine", "FLOATINGOBJECT", "OBJECTOPERATION");
        executor.removeFKIfExists("observe_seine", "ACTIVITY_OBSERVEDSYSTEM", "OBSERVEDSYSTEM");
        executor.removeFKIfExists("observe_seine", "TRIP", "OBSERVER");

    }

}
