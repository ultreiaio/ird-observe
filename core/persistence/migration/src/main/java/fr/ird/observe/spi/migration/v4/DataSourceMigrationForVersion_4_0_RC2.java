package fr.ird.observe.spi.migration.v4;

/*
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.LegacyMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

/**
 * Created on 4/10/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0-RC2
 */
@SuppressWarnings("SqlDialectInspection")
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_4_0_RC2 extends LegacyMigrationVersionResource {

    public DataSourceMigrationForVersion_4_0_RC2() {
        super(Version.valueOf("4.0-RC2"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        // recherche du nom de la constrainte
        Set<String> result = executor.findMultipleResultAstSet(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("select distinct (geartype||'') from observe_common.program");
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        });

//        executor.doSqlWork(connection -> {
//
//            String sql = "select distinct (geartype||'') from observe_common.program";
//            {
//                try (PreparedStatement ps = connection.prepareStatement(sql)) {
//                    ResultSet set = ps.executeQuery();
//                    while (set.next()) {
//                        String gearType = set.getString(1);
//                        result.add(gearType);
//                    }
//
//                } catch (Exception e) {
//                    throw new SQLException("Could not obtain program gear types", e);
//                }
//            }
//
//        });

        if (result.contains("seine")) {

            executor.addScript("01", "clean-program-gear-type");
        }

    }

}
