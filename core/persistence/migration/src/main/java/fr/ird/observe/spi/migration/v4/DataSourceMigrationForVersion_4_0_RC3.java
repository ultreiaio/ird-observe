package fr.ird.observe.spi.migration.v4;

/*
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.LegacyMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on 4/16/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0-RC3
 */
@SuppressWarnings("SqlDialectInspection")
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_4_0_RC3 extends LegacyMigrationVersionResource {

    private static final Logger log = LogManager.getLogger(DataSourceMigrationForVersion_4_0_RC3.class);

    public DataSourceMigrationForVersion_4_0_RC3() {
        super(Version.valueOf("4.0-RC3"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        // See https://forge.codelutin.com/issues/6964
        executor.executeForPG(this::addMissingForeignKeys);

        // See https://forge.codelutin.com/issues/6983
        executor.addScript("01", "update-senne-gear-usedInTrip");

        // See https://forge.codelutin.com/issues/6991
        executor.addScript("02", "rename-unknown-longliner");

    }

    private void addMissingForeignKeys(MigrationVersionResourceExecutor executor) {

        removeForeignKeyIndex(executor, "OBSERVE_COMMON", "gear_gearcaracteristic", "gear");
        removeForeignKeyIndex(executor, "OBSERVE_COMMON", "ocean_species", "species");
        removeForeignKeyIndex(executor, "OBSERVE_COMMON", "species_specieslist", "speciesList");

        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "activity", "trip");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "baitsComposition", "set");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "basket", "section");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "branchline", "basket");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "branchlinesComposition", "set");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "catch", "basket");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "catch", "branchline");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "catch_predator", "catch");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "catch", "section");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "catch", "set");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "encounter", "activity");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "floatlinesComposition", "set");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "hooksComposition", "set");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "mitigationtype_set", "set");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "section", "set");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "sensorUsed", "activity");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "sizeMeasure", "catch");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "species_tdr", "tdr");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "tdr", "basket");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "tdr", "branchline");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "tdrRecord", "basket");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "tdrRecord", "tdr");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "tdr", "section");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "tdr", "set");
        removeForeignKeyIndex(executor, "OBSERVE_LONGLINE", "weightMeasure", "catch");

        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "activity_observedsystem", "activity");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "activity", "route");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "floatingObject", "activity");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "gearUseFeaturesMeasurement", "gearUseFeatures");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "gearUseFeatures", "trip");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "nonTargetCatch", "set");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "nonTargetLength", "nonTargetSample");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "nonTargetSample", "set");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "objectObservedSpecies", "floatingObject");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "objectSchoolEstimate", "floatingObject");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "route", "trip");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "schoolEstimate", "set");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "targetCatch", "set");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "targetLength", "targetSample");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "targetSample", "set");
        removeForeignKeyIndex(executor, "OBSERVE_SEINE", "transmittingBuoy", "floatingObject");

        executor.addScript("03", "add-foreign-key-indexes");

    }

    private void removeForeignKeyIndex(MigrationVersionResourceExecutor executor, String schemaName, String tableName, String columnName) {

        executor.doSqlWork(connection -> {

            // get table oid
            int oid = getTableOid(connection, schemaName, tableName);

            // get attribute num
            int attNum = getAttributeNum(connection, oid, columnName);

            Set<Integer> indexIds = getIndexId(connection, oid, attNum);

            for (Integer indexId : indexIds) {

                String indexName = getIndexName(connection, indexId);
                executor.writeSql("DROP INDEX " + schemaName + "." + indexName + ";");

            }

        });

    }

    private Integer getTableOid(Connection connection, String schemaName, String tableName) throws SQLException {

        Integer oid = null;

        String sqlOid = "SELECT '" + schemaName + "." + tableName + "'::regclass::oid;";
        try (PreparedStatement ps = connection.prepareStatement(sqlOid)) {
            ResultSet set = ps.executeQuery();
            if (set.next()) {
                oid = set.getInt(1);
                if (log.isDebugEnabled())
                    log.debug("found table oid " + schemaName + "." + tableName + ": " + oid);
            }
        } catch (Exception e) {
            throw new SQLException("Could not obtain oid for table" + tableName, e);
        }

        return oid;

    }

    private Integer getAttributeNum(Connection connection, int oid, String columnName) throws SQLException {

        Integer attNum = null;

        String attNumSql = "SELECT attnum FROM pg_attribute WHERE attrelid = ? AND attname = ?";
        PreparedStatement ps = connection.prepareStatement(attNumSql);
        ps.setInt(1, oid);
        ps.setString(2, columnName.toLowerCase());
        try {
            ResultSet set = ps.executeQuery();
            if (set.next()) {
                attNum = set.getInt(1);
                if (log.isDebugEnabled())
                    log.debug("found attribute " + columnName + " attNum : " + attNum);
            }
        } catch (Exception e) {
            throw new SQLException("Could not obtain attNum for column" + columnName, e);
        } finally {
            ps.close();
        }

        return attNum;

    }

    private Set<Integer> getIndexId(Connection connection, int oid, int attNum) throws SQLException {

        Set<Integer> indexIds = new HashSet<>();

        String sql = "SELECT indexrelid FROM pg_index " +
                "WHERE indrelid = ? " +
                "AND indkey = '" + attNum + "' " +
                "AND indisunique = FALSE " +
                "AND indisprimary = FALSE;";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1, oid);

        try {
            ResultSet set = ps.executeQuery();

            while (set.next()) {

                indexIds.add(set.getInt(1));

            }
        } catch (Exception e) {
            throw new SQLException("Could not obtain index Id  unique for table oid " + oid + " and column attNum " + attNum, e);
        } finally {
            ps.close();
        }
        return indexIds;

    }

    private String getIndexName(Connection connection, int indexId) throws SQLException {

        String indexName = null;

        String sqlIndexName = "SELECT relname FROM pg_class WHERE oid = ?;";
        PreparedStatement ps = connection.prepareStatement(sqlIndexName);
        ps.setInt(1, indexId);
        try {
            ResultSet set = ps.executeQuery();
            if (set.next()) {
                indexName = set.getString(1);
            }
        } catch (Exception e) {
            throw new SQLException("Could not obtain index name  for indexId " + indexId, e);
        } finally {
            ps.close();
        }

        return indexName;

    }


}
