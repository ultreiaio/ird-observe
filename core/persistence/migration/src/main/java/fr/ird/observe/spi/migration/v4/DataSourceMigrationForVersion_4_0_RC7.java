package fr.ird.observe.spi.migration.v4;

/*
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.LegacyMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


/**
 * Created on 5/27/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0-RC7
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_4_0_RC7 extends LegacyMigrationVersionResource {

    public DataSourceMigrationForVersion_4_0_RC7() {
        super(Version.valueOf("4.0-RC7"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {


        // See https://forge.codelutin.com/issues/6983
        // Should have been donne in RC3, but was missed!
        executor.addScript("01", "update-senne-gear-usedInTrip");

        // See https://forge.codelutin.com/issues/6991
        // Should have been donne in RC3, but was missed!
        executor.addScript("02", "rename-unknown-longliner");

    }

}

