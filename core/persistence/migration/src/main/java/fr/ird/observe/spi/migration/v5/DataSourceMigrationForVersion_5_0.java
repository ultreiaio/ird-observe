package fr.ird.observe.spi.migration.v5;

/*
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 25/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_5_0 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_5_0() {
        super(Version.valueOf("5.0"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        // Suppression du champ open sur les entités anciennement du type Openable
        // See https://forge.codelutin.com/issues/7469
        executor.addScript("01", "remove_open_field");

        // Ajout des champs lastUpdate
        // See https://forge.codelutin.com/issues/7470
        executor.addScript("02", "add_lastUpdateDate_field");

        // Ajout de la table lastUpdateDate
        // See https://forge.codelutin.com/issues/7470
        executor.addScript("03", "add_lastUpdateDate_table");


        // Ajout de la table lastUpdateDate
        // See https://forge.codelutin.com/issues/7470
        executor.addScript("04", "remove_unit_field");
        executor.addScript("05", "remove_gender_field");
        executor.addScript("06", "remove_duplicate_rows");


        executor.addScript("07", "drop_not_null_constraint_gearUseFeatures");
        executor.addScript("08", "drop_not_null_constraint_hauling_identifier");

        Set<String> foreignKeys = executor.getForeignKeyConstraintNames("nontargetlength");
        Set<String> collect = foreignKeys.stream().map(String::toLowerCase).collect(Collectors.toSet());
        if (!collect.contains("fk_nontargetlength_sex")) {
            executor.addScript("09", "add_non_target_length_sex_fk");
        }
    }

}
