package fr.ird.observe.spi.migration.v6;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.apache.commons.lang3.tuple.Pair;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 27/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
@SuppressWarnings("SqlDialectInspection")
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_6_0 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_6_0() {
        super(Version.valueOf("6.0"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        executor.executeForPG(this::fixPostgisTriggers);

        executor.addScript("01", "evol_8374_delete_sonarUsed");
        executor.addScript("02", "evol_7886_catch_add_set_idx");
        migrateIdx(executor, "observe_longline", "catch", "set");

        executor.addScript("04", "evol_6832_branchlineComposition_add_tracelineLength");
        executor.addScript("05", "evol_6999_nonTargetCatch_add_well");
        executor.addScript("06", "evol_8375_setSeine_add_booleans");
        executor.addScript("07", "evol_8389_rename_source_columns");

        executor.addScript("08", "evol_8390_measure_type");
        evol8390(executor);
        executor.addScript("08_1", "evol_8390_measure_type");

        executor.addScript("09", "evol_7877_targetLength_add_sex");
        executor.addScript("10", "evol_8391_species_add_sizeMeasureType");
        evol8391(executor);

        executor.addScript("11", "evol_8571_length_add_tagNumber");
        executor.addScript("12", "evol_8475_vessel_add_fields");
        executor.addScript("13", "evol_7901_add_armateur_table");
        executor.addScript("14", "evol_8196_lengthWeightParameter_add_source");
        executor.addScript("15", "evol_8578_nonTargetLength_add_speciesFate");
        executor.addScript("16", "evol_8377_speciesFate_add_discard");
        executor.addScript("17", "evol_8376_vesselActivitySeine_add_allowFad");
        executor.addScript("18", "evol_8404_lengthLengthParameter");
        executor.addScript("19", "ano_8554_brancheline_add_section");
        executor.addScript("20", "evol_8391_species_drop_lengthmeasuretype");

    }

    private void fixPostgisTriggers(MigrationVersionResourceExecutor executor) {
        Boolean withTriggers = executor.findSingleResult(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("select exists(select * from pg_proc where proname = 'sync_activity_the_geom');");
            }

            @Override
            public Boolean prepareResult(ResultSet resultSet) throws SQLException {
                return resultSet.getBoolean(1);
            }
        });

        if (withTriggers) {
            executor.writeSql("CREATE OR REPLACE function sync_activity_the_geom () returns trigger as '\n" +
                                      "BEGIN\n" +
                                      "  IF (TG_OP = ''DELETE'') THEN\n" +
                                      "   RETURN OLD;\n" +
                                      "  END IF;\n" +
                                      "  IF (NEW.latitude IS NULL OR NEW.longitude IS NULL) THEN\n" +
                                      "   -- on ne calcule pas le point postgis si au moins une des -- coordonnees n est pas renseignee\n" +
                                      "   RAISE NOTICE ''No latitude or longitude, can not compute postgis field for id % '', NEW.topiaId;\n" +
                                      "   NEW.the_geom := NULL;\n" +
                                      "   return NEW;\n" +
                                      "  END IF;\n" +
                                      "  IF (TG_OP = ''UPDATE'' AND NEW.latitude = OLD.latitude AND NEW.longitude = OLD.longitude)\n" +
                                      "  THEN\n" +
                                      "    -- on ne calcule pas le point postgis si les coordonnées n''ont pas changées\n" +
                                      "    return NEW;\n" +
                                      "  END IF;\n" +
                                      "  RAISE NOTICE ''Will compute the_geom for activite % - latitude % and longitude %'', NEW.topiaId, NEW.latitude, NEW.longitude;\n" +
                                      "  -- affectation du point\n" +
                                      "  NEW.the_geom := ST_SetSRID(ST_MakePoint(NEW.longitude,NEW.latitude), 4326);\n" +
                                      "  RAISE NOTICE ''Computed for activity % latitude % and longitude %, the_geom %'', NEW.topiaId, NEW.latitude, NEW.longitude, NEW.the_geom;\n" +
                                      "\n" +
                                      "  RETURN NEW;\n" +
                                      "END\n" +
                                      "'\n" +
                                      "LANGUAGE 'plpgsql';\n");
            executor.writeSql("CREATE OR REPLACE function sync_harbour_the_geom () returns trigger as '\n" +
                                      "BEGIN\n" +
                                      "  IF (TG_OP = ''DELETE'') THEN\n" +
                                      "   RETURN OLD;\n" +
                                      "  END IF;\n" +
                                      "  IF (NEW.latitude IS NULL OR NEW.longitude IS NULL) THEN\n" +
                                      "   -- on ne calcule pas le point postgis si au moins une des -- coordonnees n est pas renseignee\n" +
                                      "   RAISE NOTICE ''No latitude or longitude, can not compute postgis field for id % '', NEW.topiaId;\n" +
                                      "   NEW.the_geom := NULL;\n" +
                                      "   return NEW;\n" +
                                      "  END IF;\n" +
                                      "  IF (TG_OP = ''UPDATE'' AND NEW.latitude = OLD.latitude AND NEW.longitude = OLD.longitude)\n" +
                                      "  THEN\n" +
                                      "    -- on ne calcule pas le point postgis si les coordonnées n''ont pas changées\n" +
                                      "    return NEW;\n" +
                                      "  END IF;\n" +
                                      "  RAISE NOTICE ''Will compute the_geom for harbour % - latitude % and longitude %'', NEW.topiaId, NEW.latitude, NEW.longitude;\n" +
                                      "  -- affectation du point\n" +
                                      "  NEW.the_geom := ST_SetSRID(ST_MakePoint(NEW.longitude,NEW.latitude), 4326);\n" +
                                      "  RAISE NOTICE ''Computed for harbour % latitude % and longitude %, the_geom %'', NEW.topiaId, NEW.latitude, NEW.longitude, NEW.the_geom;\n" +
                                      "  RETURN NEW;\n" +
                                      "END\n" +
                                      "'\n" +
                                      "LANGUAGE 'plpgsql';");
            executor.addScript("00", "fix_trigger");

        }
    }

    private void evol8391(MigrationVersionResourceExecutor executor) {

        ImmutableMap<String, String> sqlMappingByCode =
                ImmutableMap.<String, String>builder()
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499465700#0.0902433863375336", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499465700#0.0902433863375336', 2, '2015-06-05', '2016-10-18 11:51:54.099140','FL', 1, null, 'Fork Length', 'Fork Length', 'Fork Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499465999#0.707568018231541", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499465999#0.707568018231541', 3, '2015-06-05', '2016-10-18 11:51:54.099140', 'CFL', 1, null, 'Curved Fork Length', 'Curved Fork Length', 'Curved Fork Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466255#0.444246932631359", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466255#0.444246932631359', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'PAL', 1, null, 'Pectoral-Anal Length', 'Pectoral-Anal Length', 'Pectoral-Anal Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466532#0.844473292818293", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466532#0.844473292818293', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'TL', 1, null, 'Total Length', 'Total Length', 'Total Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466774#0.529249255312607", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466774#0.529249255312607', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'PD1', 1, null, 'Predorsal Length', 'Predorsal Length', 'Predorsal Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467059#0.563144549960271", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467059#0.563144549960271', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'SL', 1, null, 'Standard Length', 'Standard Length', 'Standard Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467326#0.537910396233201", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467326#0.537910396233201', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'LJFL', 1, null, 'Lower Jaw-Fork Length', 'Lower Jaw-Fork Length', 'Lower Jaw-Fork Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467638#0.409153908025473", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467638#0.409153908025473', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'CLJFL', 1, null, 'Curved Lower Jaw-Fork Length', 'Curved Lower Jaw-Fork Length', 'Curved Lower Jaw-Fork Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467945#0.249704661779106", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467945#0.249704661779106', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'IDL', 1, null, 'Interdorsal Length', 'Interdorsal Length', 'Interdorsal Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499468283#0.152317869942635", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499468283#0.152317869942635', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'EFL', 1, null, 'Eye-Fork Length', 'Eye-Fork Length', 'Eye-Fork Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499468646#0.710986209334806", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499468646#0.710986209334806', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'DW', 1, null, 'Disk Width', 'Disk Width', 'Disk Width', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499469115#0.42996318358928", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499469115#0.42996318358928', 2, '2015-06-05', '2016-10-18 11:51:54.099140',  'DL', 1, null, 'Disk Length', 'Disk Length', 'Disk Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499469572#0.969587777974084", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499469572#0.969587777974084', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'TW', 1, null, 'Total Weight', 'Total Weight', 'Total Weight', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499470528#0.813851526239887", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499470528#0.813851526239887', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'CTL', 1, null, 'Curved Total Length', 'Curved Total Length', 'Curved Total Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499470887#0.950930784922093", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499470887#0.950930784922093', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'DML', 1, null, 'Dorsal Mantle Length', 'Dorsal Mantle Length', 'Dorsal Mantle Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499471278#0.425988764036447", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499471278#0.425988764036447', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'CDML', 1, null, 'Curved Dorsal Mantle Length', 'Curved Dorsal Mantle Length', 'Curved Dorsal Mantle Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1433499472220#0.885759855154902", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499472220#0.885759855154902', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'PFL', 1, null, 'Pectoral-Fork Length', 'Pectoral-Fork Length', 'Pectoral-Fork Length', null, null, null, null, null, false);")
                        .put("fr.ird.observe.entities.referentiel.SizeMeasureType#1479120383659#0.03964411579456617", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1479120383659#0.03964411579456617', 2, '2016-11-14', '2016-12-06 15:54:27.727005', 'SCL', 1, null, 'Straight Carapace Length', 'Straight Carapace Length', 'Straight Carapace Length', null, null, null, null, null, false);")
                        .build();

        ImmutableMap<String, String> knownSizeMeasureTypeMapping = ImmutableMap.<String, String>builder()
                .put("LT ?", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466532#0.844473292818293")
                .put("LF", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499465700#0.0902433863375336")
                .put("L1", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466774#0.529249255312607")
                .put("LC", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499469115#0.42996318358928")
                .put("LT", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466532#0.844473292818293")
                .put("", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466532#0.844473292818293")
                .put(" ", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466532#0.844473292818293")
                .put("FL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499465700#0.0902433863375336")
                .put("CFL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499465999#0.707568018231541")
                .put("PAL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466255#0.444246932631359")
                .put("TL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466532#0.844473292818293")
                .put("PD1", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466774#0.529249255312607")
                .put("PDL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466774#0.529249255312607")
                .put("SL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467059#0.563144549960271")
                .put("LJFL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467326#0.537910396233201")
                .put("CLJFL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467638#0.409153908025473")
                .put("IDL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467945#0.249704661779106")
                .put("EFL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499468283#0.152317869942635")
                .put("DW", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499468646#0.710986209334806")
                .put("DL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499469115#0.42996318358928")
                .put("TW", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499469572#0.969587777974084")
                .put("CTL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499470528#0.813851526239887")
                .put("DML", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499470887#0.950930784922093")
                .put("CDML", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499471278#0.425988764036447")
                .put("PFL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1433499472220#0.885759855154902")
                .put("SCL", "fr.ird.observe.entities.referentiel.SizeMeasureType#1479120383659#0.03964411579456617")
                .build();

        ImmutableMap.Builder<String, String> existingSizeMeasureMappingBuilder = ImmutableMap.builder();

        executor.findMultipleResult(new SqlQuery<Pair<String, String>>() {

            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                // Attention à ce moment là ce référentiel est encore dans le schema longline
                return connection.prepareStatement("SELECT code, topiaid FROM observe_longline.sizemeasuretype WHERE code IS NOT NULL");
            }

            @Override
            public Pair<String, String> prepareResult(ResultSet set) throws SQLException {
                String code = set.getString(1);
                String topiaId = set.getString(2);
                return Pair.of(code, topiaId.replace(".longline", ""));
            }
        }).forEach(p -> existingSizeMeasureMappingBuilder.put(p.getKey(), p.getValue()));

        ImmutableMap<String, String> existingSizeMeasureMapping = existingSizeMeasureMappingBuilder.build();

        List<String> lengthMeasureTypes = executor.findMultipleResult(new SqlQuery<>() {

            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT DISTINCT(lengthmeasuretype) FROM observe_common.species WHERE lengthmeasuretype IS NOT NULL");
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        });

        Set<String> unknownCodes = new HashSet<>(lengthMeasureTypes);
        unknownCodes.removeAll(knownSizeMeasureTypeMapping.keySet());
        unknownCodes.removeAll(existingSizeMeasureMapping.keySet());

        if (!unknownCodes.isEmpty()) {

            // migration impossible, il manque encore des référentiels
            // ce cas ne devrait jamais arrivé
            throw new IllegalStateException("Les types de mesure de taille pour les codes suivants :\n\t" + Joiner.on("\n\t").join(unknownCodes) + " sont requis pour la migration mais n'ont pas été trouvés en base.");

        }

        Set<String> missingSizeMeasureTypeIds = knownSizeMeasureTypeMapping.values().stream().filter(id -> !existingSizeMeasureMapping.containsValue(id)).collect(Collectors.toSet());

        for (String missingSizeMeasureTypeId : missingSizeMeasureTypeIds) {
            String sql = sqlMappingByCode.get(missingSizeMeasureTypeId);
            if (sql == null) {
                throw new IllegalStateException("Can't fin size measure type with id: " + missingSizeMeasureTypeId);
            }
            executor.writeSql(sql);
        }

        ImmutableMap.Builder<String, String> allSizeMeasureTypeMappingBuilder = ImmutableMap.<String, String>builder().putAll(existingSizeMeasureMapping);
        knownSizeMeasureTypeMapping.entrySet().stream().filter(e -> !existingSizeMeasureMapping.containsKey(e.getKey())).forEach(e -> allSizeMeasureTypeMappingBuilder.put(e.getKey(), e.getValue()));
        ImmutableMap<String, String> allSizeMeasureTypeMapping = allSizeMeasureTypeMappingBuilder.build();

        for (String lengthMeasureType : lengthMeasureTypes) {

            lengthMeasureType = lengthMeasureType.trim();

            String sizeMeasureTypeId = allSizeMeasureTypeMapping.get(lengthMeasureType);
            executor.writeSql(String.format("UPDATE observe_common.species SET sizeMeasureType = '%s' WHERE lengthMeasureType = '%s';", sizeMeasureTypeId, lengthMeasureType));

        }

    }

    private void evol8390(MigrationVersionResourceExecutor executor) {
        {
            List<String> oldIds = executor.findMultipleResult(new SqlQuery<>() {

                @Override
                public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                    return connection.prepareStatement("SELECT topiaId FROM observe_longline.sizemeasuretype");
                }

                @Override
                public String prepareResult(ResultSet set) throws SQLException {
                    return set.getString(1);
                }
            });

            for (String oldId : oldIds) {
                String newId = oldId.replace(".longline", "");
                executor.writeSql(String.format("UPDATE observe_common.sizeMeasureType SET topiaId = '%s' WHERE topiaId = '%s';", newId, oldId));
                executor.writeSql(String.format("UPDATE observe_longline.sizeMeasure SET sizeMeasureType2 = '%s' WHERE sizeMeasureType = '%s';", newId, oldId));
                executor.writeSql(String.format("UPDATE observe_seine.targetLength SET sizeMeasureType2 = '%s' WHERE sizeMeasureType = '%s';", newId, oldId));
            }
        }
        {
            List<String> oldIds = executor.findMultipleResult(new SqlQuery<>() {

                @Override
                public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                    return connection.prepareStatement("SELECT topiaId FROM observe_longline.weightmeasuretype");
                }

                @Override
                public String prepareResult(ResultSet set) throws SQLException {
                    return set.getString(1);
                }
            });

            for (String oldId : oldIds) {
                String newId = oldId.replace(".longline", "");
                executor.writeSql(String.format("UPDATE observe_common.weightMeasureType SET topiaId = '%s' WHERE topiaId = '%s';", newId, oldId));
                executor.writeSql(String.format("UPDATE observe_longline.weightMeasure SET weightMeasureType2 = '%s' WHERE weightMeasureType = '%s';", newId, oldId));
            }
        }
    }

}

