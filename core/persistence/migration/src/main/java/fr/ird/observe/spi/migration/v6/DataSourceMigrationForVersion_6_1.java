package fr.ird.observe.spi.migration.v6;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 27/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_6_1 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_6_1() {
        super(Version.valueOf("6.1"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        executor.addScript("01", "add_object_operation");
        executor.addScript("02", "add_object_material");
        executor.addScript("03", "add_floating_object_part");
        executor.addScript("04", "remove_dcp_fields");

        Map<String, String> speciesGroupIdMapping = new TreeMap<>();
        speciesGroupIdMapping.put("fr.ird.observe.entities.referentiel.SpeciesGroup#1445863056144#0.9820877553253712", "INSERT INTO OBSERVE_COMMON.SPECIESGROUP (TOPIAID, TOPIAVERSION, TOPIACREATEDATE, LASTUPDATEDATE, CODE, URI, NEEDCOMMENT, STATUS, LABEL1, LABEL2, LABEL3, LABEL4, LABEL5, LABEL6, LABEL7, LABEL8) VALUES ('fr.ird.observe.entities.referentiel.SpeciesGroup#1445863056144#0.9820877553253712', 1, '2015-10-26 16:37:36.144000000', '2016-12-06 15:54:27.727005000', '11', null, false, 1, 'Rays', 'Raies', 'Rayas', null, null, null, null, null);");
        speciesGroupIdMapping.put("fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.24333033683679461", "INSERT INTO OBSERVE_COMMON.SPECIESGROUP (TOPIAID, TOPIAVERSION, TOPIACREATEDATE, LASTUPDATEDATE, CODE, URI, NEEDCOMMENT, STATUS, LABEL1, LABEL2, LABEL3, LABEL4, LABEL5, LABEL6, LABEL7, LABEL8) VALUES ('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.24333033683679461', 14, '2009-04-15 00:00:00.003000000', '2016-12-06 15:54:27.727005000', '4', null, false, 1, 'Turtles', 'Tortues', 'Tortugas', null, null, null, null, null);");
        speciesGroupIdMapping.put("fr.ird.observe.entities.referentiel.SpeciesGroup#1446014286433#0.6480183366605247", "INSERT INTO OBSERVE_COMMON.SPECIESGROUP (TOPIAID, TOPIAVERSION, TOPIACREATEDATE, LASTUPDATEDATE, CODE, URI, NEEDCOMMENT, STATUS, LABEL1, LABEL2, LABEL3, LABEL4, LABEL5, LABEL6, LABEL7, LABEL8) VALUES ('fr.ird.observe.entities.referentiel.SpeciesGroup#1446014286433#0.6480183366605247', 1, '2015-10-28 10:38:06.432000000', '2016-12-06 15:54:27.727005000', '12', null, false, 1, 'Whale shark', 'Requin-baleine', 'Tiburón ballena', null, null, null, null, null);");
        speciesGroupIdMapping.put("fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075", "INSERT INTO OBSERVE_COMMON.SPECIESGROUP (TOPIAID, TOPIAVERSION, TOPIACREATEDATE, LASTUPDATEDATE, CODE, URI, NEEDCOMMENT, STATUS, LABEL1, LABEL2, LABEL3, LABEL4, LABEL5, LABEL6, LABEL7, LABEL8) VALUES ('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075', 17, '2009-04-15 00:00:00.001000000', '2017-02-28 15:04:55.370000000', '2', null, false, 1, 'Sharks', 'Requins', 'Tiburones', null, null, null, null, null);");

        Set<String> existingSpeciesGroupIds = executor.getTopiaIds("OBSERVE_COMMON.SPECIESGROUP");
        for (Map.Entry<String, String> entry : speciesGroupIdMapping.entrySet()) {
            if (!existingSpeciesGroupIds.contains(entry.getKey())) {
                executor.writeSql(entry.getValue());
            }
        }
        executor.addScript("05", "add_species_group_release_mode");
        executor.addScript("06", "add_non_target_catch_release");
        executor.addScript("07", "drop_table_sizemeasuretype");
        executor.addScript("08", "add_floating_object_fields");
        executor.addScript("09", "fill_object_material");
        executor.addScript("10", "update_object_operation");

    }

}

