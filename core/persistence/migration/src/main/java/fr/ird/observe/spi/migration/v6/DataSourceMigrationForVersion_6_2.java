package fr.ird.observe.spi.migration.v6;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.SetMultimap;
import fr.ird.observe.entities.ObserveIdFactory;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 27/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
@SuppressWarnings("SqlDialectInspection")
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_6_2 extends ByMajorMigrationVersionResource {

    private static final String INSERT_FLOATING_OBJECT_PART = "INSERT INTO observe_seine.FloatingObjectPart(topiaId, topiaVersion, topiaCreateDate, lastUpdateDate, objectMaterial, floatingObject, whenArriving, whenLeaving) VALUES( '%s', 0, CURRENT_DATE, CURRENT_TIMESTAMP, '%s', '%s', %s, %s );";
    private static final String SELECT_DCP_AND_TYPE_IDS_FOR_OPERATION = "SELECT topiaid, objectType FROM observe_seine.FloatingObject WHERE objectOperation = '%s'";
    private static final String SELECT_USABLE_OBJECT_MATERIALS = "SELECT topiaid, legacyCode FROM observe_seine.ObjectMaterial WHERE legacyCode IS NOT NULL AND objectMaterialType = 'fr.ird.observe.entities.referentiel.seine.ObjectMaterialType#0#0'";
    private static final String SELECT_OBJECT_TYPE_IDS_AND_CODES = "SELECT topiaid, code FROM observe_seine.ObjectType";
    private static final String SELECT_OBJECT_OPERATION_WHEN_LEAVING_IDS = "SELECT topiaid FROM observe_seine.ObjectOperation WHERE whenLeaving = TRUE";
    private static final String SELECT_OBJECT_OPERATION_WHEN_ARRIVING_IDS = "SELECT topiaid FROM observe_seine.ObjectOperation WHERE whenArriving = TRUE";

    public DataSourceMigrationForVersion_6_2() {
        super(Version.valueOf("6.2"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        migrateFloatingObjectOperation(executor);

        executor.addScript("01", "drop_floating_object_type");
        executor.addScript("02", "drop_object_type");
    }

    private void migrateFloatingObjectOperation(MigrationVersionResourceExecutor executor) {

        ImmutableMap.Builder<String, String> objectTypeCodeToIdMappingBuilder = ImmutableMap.builder();

        executor.findMultipleResult(new SqlQuery<Void>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement(SELECT_OBJECT_TYPE_IDS_AND_CODES);
            }

            @Override
            public Void prepareResult(ResultSet set) throws SQLException {
                String legacyCode = set.getString(2);
                String topiaId = set.getString(1);
                objectTypeCodeToIdMappingBuilder.put(legacyCode, topiaId);
                return null;
            }
        });

        // ObjectOperation mapping code -> topia Id
        ImmutableMap<String, String> objectTypeCodeToIdMapping = objectTypeCodeToIdMappingBuilder.build();

        // For a operation Id, get all material id to use
        SetMultimap<String, String> materialIdsForTypeId = HashMultimap.create();

        executor.findMultipleResult(new SqlQuery<Void>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement(SELECT_USABLE_OBJECT_MATERIALS);
            }

            @Override
            public Void prepareResult(ResultSet set) throws SQLException {
                String materialId = set.getString(1);
                String legacyCode = set.getString(2);
                for (String code : legacyCode.split(",")) {
                    String operationCode = Strings.removeEnd(code.trim(), "*");
                    String operationId = objectTypeCodeToIdMapping.get(operationCode);
                    materialIdsForTypeId.put(operationId, materialId);
                }
                return null;
            }
        });


        // All operation ids using whenArriving
        Set<String> whenArrivingOperationIds = new LinkedHashSet<>(executor.findMultipleResult(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement(SELECT_OBJECT_OPERATION_WHEN_ARRIVING_IDS);
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        }));

        // All operation ids using whenLeaving
        Set<String> whenLeavingOperationIds = new LinkedHashSet<>(executor.findMultipleResult(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement(SELECT_OBJECT_OPERATION_WHEN_LEAVING_IDS);
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        }));

        ObserveIdFactory idFactory = new ObserveIdFactory();

        Set<String> operationIds = new LinkedHashSet<>(whenArrivingOperationIds);
        operationIds.addAll(whenLeavingOperationIds);
        for (String operationId : operationIds) {

            boolean whenArriving = whenArrivingOperationIds.contains(operationId);
            boolean whenLeaving = whenLeavingOperationIds.contains(operationId);

            List<Pair<String, String>> dcpIds = executor.findMultipleResult(new SqlQuery<>() {
                @Override
                public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                    return connection.prepareStatement(String.format(SELECT_DCP_AND_TYPE_IDS_FOR_OPERATION, operationId));
                }

                @Override
                public Pair<String, String> prepareResult(ResultSet set) throws SQLException {
                    return Pair.of(set.getString(1), set.getString(2));
                }
            });

            for (Pair<String, String> dcpIdAndTypeId : dcpIds) {
                String dcpId = dcpIdAndTypeId.getLeft();
                String typeId = dcpIdAndTypeId.getRight();
                int materialIndex = 0;
                Set<String> materialIds = materialIdsForTypeId.get(typeId);
                for (String materialId : materialIds) {
                    String topiaId = idFactory.newTopiaId("fr.ird.observe.entities.seine.FloatingObjectPart", System.currentTimeMillis() + "", idFactory.getRandomPart(idFactory.getRandomPart(dcpId)) + materialIndex++);
                    executor.writeSql(String.format(INSERT_FLOATING_OBJECT_PART, topiaId, materialId, dcpId, whenArriving, whenLeaving));
                }
            }
        }
    }

}

