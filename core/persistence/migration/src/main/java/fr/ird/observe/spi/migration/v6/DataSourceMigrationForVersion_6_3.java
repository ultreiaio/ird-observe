package fr.ird.observe.spi.migration.v6;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


/**
 * Created on 27/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_6_3 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_6_3() {
        super(Version.valueOf("6.3"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        executor.addScript("01", "fill_non_target_sample_size_measure_type");
        executor.addScript("02", "add_data_quality");
        executor.addScript("03", "add_order_on_seine_catches");
        executor.addScript("04", "add_order_on_seine_samples");

        migrateIdx(executor, "observe_seine", "nonTargetCatch", "set");
        migrateIdx(executor, "observe_seine", "targetCatch", "set");
        migrateIdx(executor, "observe_seine", "targetLength", "targetSample");
        migrateIdx(executor, "observe_seine", "nonTargetLength", "nonTargetSample");
    }

}

