package fr.ird.observe.spi.migration.v6;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created on 27/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
@SuppressWarnings("SqlDialectInspection")
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_6_5 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_6_5() {
        super(Version.valueOf("6.5"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {

        executor.addScript("02", "add_non_target_catch_release_conformity");
        executor.addScript("03", "add_non_target_catch_releasing_time");
        executor.addScript("04", "update_non_target_catch_release_status");
        executor.addScript("05", "remove_species_group_release_mode_value");

        Boolean withIds = executor.findSingleResult(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT COUNT (*) FROM observe_common.SpeciesGroup WHERE topiaid='fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.9204972827240977';");
            }

            @Override
            public Boolean prepareResult(ResultSet resultSet) throws SQLException {
                return resultSet.getInt(1) > 0;
            }
        });
        if (withIds) {
            executor.addScript("06", "fix_species_group_release_mode_for_cetaceans");
        }

        List<String> systemObservedIds = executor.findMultipleResult(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement(
                        "SELECT DISTINCT(a.code) FROM observe_seine.observedsystem a");
            }

            @Override
            public String prepareResult(ResultSet resultSet) throws SQLException {
                return resultSet.getString(1);
            }
        });

        if (!systemObservedIds.contains("20")) {
            executor.writeSql("INSERT INTO observe_seine.observedsystem (topiaid, topiaversion, topiacreatedate, lastupdatedate, needComment, status, code, label1, label2, label3) values ('fr.ird.observe.entities.referentiel.seine.ObservedSystem#0#1.2', 0, CURRENT_DATE, CURRENT_TIMESTAMP, false,  1, '20', ' FOB', 'FOB', 'FOB');");
        }
        if (!systemObservedIds.contains("21")) {
            executor.writeSql("INSERT INTO observe_seine.observedsystem (topiaid, topiaversion, topiacreatedate, lastupdatedate, needComment, status, code, label1, label2, label3) values ('fr.ird.observe.entities.referentiel.seine.ObservedSystem#0#1.0', 0, CURRENT_DATE, CURRENT_TIMESTAMP, false,  1, '21', 'Whale shark seen before set', 'Requin-baleine vu avant la calée', 'Tiburón ballena visto antes del lance');");
        }
        if (!systemObservedIds.contains("22")) {
            executor.writeSql("INSERT INTO observe_seine.observedsystem (topiaid, topiaversion, topiacreatedate, lastupdatedate, needComment, status, code, label1, label2, label3) values ('fr.ird.observe.entities.referentiel.seine.ObservedSystem#0#1.1', 0, CURRENT_DATE, CURRENT_TIMESTAMP, false,  1, '22', ' Whale shark seen later during set', 'Requin-baleine vu plus tard durant la calée', 'Tiburón ballena visto más tarde durante el lance');");
        }

        executor.addScript("07", "update_observed_system");

        List<String> activitiesWithWhaleSharkInBycatch = executor.findMultipleResult(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement(
                        "SELECT DISTINCT(a.topiaid) FROM observe_seine.NonTargetCatch t " +
                                " INNER JOIN observe_seine.set s ON (s.topiaid=t.set)" +
                                " INNER JOIN observe_seine.ACTIVITY a ON (a.set=s.topiaid)" +
                                " WHERE t.species='fr.ird.observe.entities.referentiel.Species#1239832684290#0.04680507324710936';");
            }

            @Override
            public String prepareResult(ResultSet resultSet) throws SQLException {
                return resultSet.getString(1);
            }
        });

        for (String activityId : activitiesWithWhaleSharkInBycatch) {
            Boolean withoutWhaleSharkObservedSystem = executor.findSingleResult(new SqlQuery<>() {
                @Override
                public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                    return connection.prepareStatement(
                            "SELECT COUNT(DISTINCT(bs.activity)) FROM  observe_seine.activity_observedsystem bs" +
                                    " WHERE bs.observedsystem !='fr.ird.observe.entities.referentiel.seine.ObservedSystem#1239832686428#0.9217864901728908'" +
                                    " AND bs.activity = '" + activityId + "'");
                }

                @Override
                public Boolean prepareResult(ResultSet resultSet) throws SQLException {
                    return resultSet.getInt(1) == 0;
                }
            });
            if (withoutWhaleSharkObservedSystem) {
                executor.writeSql("INSERT INTO observe_seine.activity_observedsystem(activity, observedSystem) VALUES('" + activityId + "', 'fr.ird.observe.entities.referentiel.seine.ObservedSystem#0#1.1');");
                executor.writeSql("UPDATE observe_seine.set SET schoolType = 1 WHERE topiaId = (SELECT a.set FROM observe_seine.activity a WHERE a.topiaId = '" + activityId + "');");
            }
        }
    }

}

