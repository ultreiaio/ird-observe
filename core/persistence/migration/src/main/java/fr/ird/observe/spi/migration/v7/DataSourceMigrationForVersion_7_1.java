package fr.ird.observe.spi.migration.v7;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


/**
 * Created on 16/07/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_7_1 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_7_1() {
        super(Version.valueOf("7.1"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {
        executor.addScript("01", "add_not_null_on_length_weight_parameters");
        executor.addScript("02", "add_not_null_on_length_length_parameters");
        executor.addScript("03", "add_not_null_on_technical_fields");
    }

}

