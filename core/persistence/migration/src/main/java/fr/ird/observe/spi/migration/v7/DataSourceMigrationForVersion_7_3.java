package fr.ird.observe.spi.migration.v7;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.3
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_7_3 extends ByMajorMigrationVersionResource {

    private static final Logger log = LogManager.getLogger(DataSourceMigrationForVersion_7_3.class);

    public DataSourceMigrationForVersion_7_3() {
        super(Version.valueOf("7.3"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {
        executor.addScript("01", "ps_observation_drop_set_fields");
        executor.addScript("02", "fix_object_material_empty_standard_code");
        Boolean withIds = executor.findSingleResult(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT COUNT (*) FROM observe_seine.floatingObjectPart");
            }

            @Override
            public Boolean prepareResult(ResultSet resultSet) throws SQLException {
                return resultSet.getInt(1) > 0;
            }
        });
        if (withIds) {
            log.info("Fix ps observation floating object parts");
            fixObjectParts(executor);
        }
    }

    private void fixObjectParts(MigrationVersionResourceExecutor executor) {
        Set<String> whenNotArrivingIds = executor.findMultipleResultAstSet(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT o.topiaId FROM observe_seine.objectOperation o WHERE o.whenArriving IS FALSE AND o.topiaID IN (SELECT DISTINCT(f.objectOperation) FROM observe_seine.floatingObject f )");
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        });
        for (String whenNotArrivingId : whenNotArrivingIds) {
            executor.writeSql(String.format("UPDATE observe_seine.floatingObjectPart p SET whenArriving = NULL WHERE p.floatingObject IN (SELECT topiaId FROM observe_seine.floatingObject WHERE objectOperation = '%s')", whenNotArrivingId));
        }
        Set<String> whenNotLeavingIds = executor.findMultipleResultAstSet(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT o.topiaId FROM observe_seine.objectOperation o WHERE o.whenLeaving IS FALSE AND o.topiaID IN (SELECT DISTINCT(f.objectOperation) FROM observe_seine.floatingObject f )");
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        });
        for (String whenNotLeavingId : whenNotLeavingIds) {
            executor.writeSql(String.format("UPDATE observe_seine.floatingObjectPart p SET whenLeaving = NULL WHERE p.floatingObject IN (SELECT topiaId FROM observe_seine.floatingObject WHERE objectOperation = '%s')", whenNotLeavingId));
        }

        executor.writeSql("DELETE FROM observe_seine.floatingObjectPart WHERE whenArriving = NULL AND whenLeaving = NULL");
    }

}

