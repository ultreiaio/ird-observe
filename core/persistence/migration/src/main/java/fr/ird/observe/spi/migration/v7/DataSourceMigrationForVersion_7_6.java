package fr.ird.observe.spi.migration.v7;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.apache.commons.lang3.tuple.Pair;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.5.1
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_7_6 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_7_6() {
        super(Version.valueOf("7.6"));
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {
        Set<Pair<String, String>> tripIdAndComment = executor.findMultipleResultAstSet(new SqlQuery<>() {
            @SuppressWarnings("SqlDialectInspection")
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT topiaId, comment FROM observe_seine.trip WHERE comment LIKE '#%#%'");
            }

            @Override
            public Pair<String, String> prepareResult(ResultSet set) throws SQLException {
                return Pair.of(set.getString(1), set.getString(2));
            }
        });

        executor.writeSql("ALTER TABLE observe_seine.trip ADD COLUMN homeId VARCHAR(255)");

        for (Pair<String, String> pair : tripIdAndComment) {
            String tripId = pair.getKey();
            String comment = pair.getValue().trim();

            int endIndex = comment.indexOf('#', 1);
            if (endIndex == 1) {
                // Must be a ## starting comment
                continue;
            }
            String homeId = comment.substring(1, endIndex);
            String newComment = endIndex + 1 == comment.length() ? "NULL" : (String.format("'%s'", comment.substring(endIndex + 1).trim().replaceAll("'", "''")));
            executor.writeSql(String.format("UPDATE observe_seine.trip t SET comment = %s, homeId = '%s', topiaVersion = topiaVersion + 1, lastUpdateDate = CURRENT_TIMESTAMP WHERE t.topiaId = '%s';", newComment, homeId, tripId));
        }
    }

}

