package fr.ird.observe.spi.migration.v8;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.apache.commons.lang3.tuple.Pair;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by tchemit on 18/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_8_0 extends ByMajorMigrationVersionResource {

    private static final String SANITIZE_BASKET_SQL_PATTERN = "UPDATE %1$s SET basket = NULL WHERE branchline IS NOT NULL;";
    private static final String SANITIZE_SECTION_SQL_PATTERN = "UPDATE %1$s SET section = NULL WHERE basket IS NOT NULL OR branchline IS NOT NULL;";

    public static java.util.Set<String> generateSanitizeLonglineElementsStatements() {
        return new LinkedHashSet<>(Arrays.asList(
                String.format(SANITIZE_BASKET_SQL_PATTERN, "ll_observation.tdr"),
                String.format(SANITIZE_SECTION_SQL_PATTERN, "ll_observation.tdr"),
                String.format(SANITIZE_BASKET_SQL_PATTERN, "ll_observation.catch"),
                String.format(SANITIZE_SECTION_SQL_PATTERN, "ll_observation.catch")
        ));
    }

    public DataSourceMigrationForVersion_8_0() {
        super(Version.valueOf("8.0"), true);
        createResourceScriptVariables(this, "2020-10-01", "2020-10-01 00:00:00.000000");
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {
        Boolean withIds = executor.findSingleResult(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT COUNT (*) FROM observe_common.person");
            }

            @Override
            public Boolean prepareResult(ResultSet resultSet) throws SQLException {
                return resultSet.getInt(1) > 0;
            }
        });
        if (withIds) {
            Map<String, String> scriptVariables = getScriptVariables();
            // Add program variables (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2115)
            Set<Pair<String, String>> programIdByCode = executor.findMultipleResultAstSet(SqlQuery.wrap(
                    "SELECT code, topiaId FROM observe_common.Program WHERE code IN ('9')", resultSet -> Pair.of(resultSet.getString(1), resultSet.getString(2))));
            for (Pair<String, String> pair : programIdByCode) {
                scriptVariables.put("Program_" + pair.getKey(), pair.getValue().replace("observe.entities.referentiel", "referential.common"));
            }
            addCommonVesselType(executor);
            addLlCommonVesselActivity(executor);
        }
        executor.addScript("observe", "create-schema");

        processSchema(executor, withIds, "30", "common");
        if (withIds) {
            addCommonCountry(executor);
            addCommonPerson(executor);
            fillSpeciesLists(executor);
        }
        processSchema(executor, withIds, "40", "ps_common");
        processSchema(executor, withIds, "50", "ps_observation");
        processSchema(executor, withIds, "60", "ll_common");
        processSchema(executor, withIds, "70", "ll_observation");
        processSchema(executor, withIds, "80", "ll_logbook");
        processSchema(executor, withIds, "90", "ll_landing");

        executor.addScript("observe", "finalize-schema");

        executor.dropSchema("observe_common");
        executor.dropSchema("observe_seine");
        executor.dropSchema("observe_longline");

        executor.addScript("99", "ll_common_drop_weightCategory");
        executor.addScript("99", "update_referential_ps_observation_WeightCategory");
        executor.addScript("99", "update_referential_ps_common_ObjectMaterial");
        executor.addScript("99", "update_referential_common_Program");
        executor.addScript("99", "update_referential_common_Person");
        executor.addScript("99", "update_referential_common_Vessel");
        executor.addScript("99", "update_referential_wind");

        if (withIds) {

            executor.addScript("100", "update_referential_common_WeightMeasureMethod_add_code");
            executor.addScript("100", "update_referential_common_LengthMeasureMethod_add_code");
            executor.addScript("100", "update_referential_common_SpeciesGroupReleaseMode_add_code");

            // Add missing not-null on referential - See https://gitlab.com/ultreiaio/ird-observe/-/issues/2255
            executor.addScript("100", "update_referential_common_Program_add_organism");
            executor.addScript("100", "update_referential_common_Vessel_add_code");
            executor.addScript("100", "update_referential_common_VesselType_add_code");

            executor.addScript("100", "update_referential_ll_landing_DataSource_add_code");
            executor.addScript("100", "update_referential_ll_common_BaitHaulingStatus_add_code");
            executor.addScript("100", "update_referential_ll_common_CatchFate_add_code");
            executor.addScript("100", "update_referential_ll_common_EncounterType_add_code");
            executor.addScript("100", "update_referential_ll_common_HealthStatus_add_code");
            executor.addScript("100", "update_referential_ll_common_HookPosition_add_code");
            executor.addScript("100", "update_referential_ll_common_HookSize_add_code");
            executor.addScript("100", "update_referential_ll_common_HookType_add_code");
            executor.addScript("100", "update_referential_ll_common_ItemHorizontalPosition_add_code");
            executor.addScript("100", "update_referential_ll_common_ItemVerticalPosition_add_code");
            executor.addScript("100", "update_referential_ll_common_LightSticksColor_add_code");
            executor.addScript("100", "update_referential_ll_common_MaturityStatus_add_code");
            executor.addScript("100", "update_referential_ll_common_MitigationType_add_code");
            executor.addScript("100", "update_referential_ll_common_SensorBrand_add_code");
            executor.addScript("100", "update_referential_ll_common_SensorDataFormat_add_code");
            executor.addScript("100", "update_referential_ll_common_StomachFullness_add_code");
            executor.addScript("100", "update_referential_ll_common_VesselActivity_add_code");

        }
    }

    @Override
    public void generateFinalizeSqlScript(MigrationVersionResourceExecutor executor) {
        generateSanitizeLonglineElementsStatements().forEach(executor::writeSql);
    }

    private void processSchema(MigrationVersionResourceExecutor executor, boolean withIds, String order, String schemaName) {
        if (withIds) {
            executor.addScript(order, schemaName + "_fill");
        }
        executor.addScript(order, schemaName + "_finalize");
    }

    private void addCommonVesselType(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT code FROM observe_common.VesselType", resultSet -> resultSet.getString(1)));
        Stream.of("14", "15", "16", "17").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_common_VesselType_" + code);
            }
        });
    }

    private void addLlCommonVesselActivity(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT code FROM observe_common.VesselType", resultSet -> resultSet.getString(1)));
        Stream.of("CRUISE", "DRIFT", "OUTZEE", "PORT", "REPAIR", "TRANSSHIP", "UNK").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_ll_common_VesselActivity_" + code);
            }
        });
    }

    private void addCommonCountry(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT code FROM observe_common.Country", resultSet -> resultSet.getString(1)));
        Stream.of("92", "93").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("30", "add_referential_common_Country_" + code);
            }
        });
    }

    private void addCommonPerson(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT lastName || '-' || firstName FROM observe_common.Person", resultSet -> resultSet.getString(1).toLowerCase()));
        Stream.of(
                "Augustin-Emilie",
                "Bamboche-Nattyfa",
                "Boniface-Breta",
                "Claude-Samantha",
                "Dogley-Ruby",
                "Julienne-Cynthia",
                "Labrosse-Valentina",
                "Lagrenade-Vanessa",
                "Melanie-Cindy",
                "Melanie-Julienne",
                "Perikanan-Pengawas",
                "Pillay-Sandra",
                "Pothin-Nikita",
                "Quatre-Ron",
                "Ramkalawan-Relix",
                "Rose-Hendricka",
                "Savy-Marie Stella",
                "Savy-Vincent",
                "Sinon-James",
                "Sinon-Marie-Claire",
                "Sopha-Davis",
                "Stephen-Andy",
                "Theresine-Bernard",
                "Valentin-Bahiri",
                "Vidot-Agnes").sorted().forEach(code -> {
            if (!existingCodes.contains(code.toLowerCase())) {
                executor.addScript("30", "add_referential_common_Person_" + code.replaceAll("\\s+", "_"));
            }
        });
    }

    private void fillSpeciesLists(MigrationVersionResourceExecutor executor) {
        List<String> speciesIds = executor.findMultipleResult(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT species FROM observe_common.species_specieslist WHERE speciesList = 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3'");
            }

            @Override
            public String prepareResult(ResultSet resultSet) throws SQLException {
                return resultSet.getString(1).replace("observe.entities.referentiel.", "referential.common.");
            }
        });

        for (String speciesId : speciesIds) {
            executor.writeSql(String.format("INSERT INTO common.speciesList_species(speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#1239832675370#0.7', '%s');", speciesId));
            executor.writeSql(String.format("INSERT INTO common.speciesList_species(speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#1239832675370#0.8', '%s');", speciesId));
        }
    }
}

