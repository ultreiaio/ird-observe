package fr.ird.observe.spi.migration.v8;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import org.apache.commons.lang3.tuple.Pair;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created on 01/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_8_1 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_8_1() {
        super(Version.valueOf("8.1"), true);
        ByMajorMigrationVersionResource.createResourceScriptVariables(this, "2021-09-17", "2021-09-17 00:00:00.000000");
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) throws IOException {
        Boolean withIds = executor.findSingleResult(new SqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT COUNT (*) FROM common.person");
            }

            @Override
            public Boolean prepareResult(ResultSet resultSet) throws SQLException {
                return resultSet.getInt(1) > 0;
            }
        });
        if (withIds) {
            if (isForTck()) {
                // Add missing referential to be able to generate public API documentation
                executor.addScript("00-tck", "add_common_LengthLengthParameter");
                executor.addScript("00-tck", "add_ll_common_WeightDeterminationMethod");
            }

            Map<String, String> scriptVariables = getScriptVariables();

            // Add missing countries (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2255)
            addCountry(executor, scriptVariables);

            // Add country variables (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2115)
            Set<Pair<String, String>> countryIdByCode = executor.findMultipleResultAstSet(SqlQuery.wrap(
                    "SELECT iso3code, topiaId FROM common.Country WHERE iso3code IN ('FRA', 'SYC', 'SEN', 'CPV', 'CIV', 'ESP', 'CUW', 'GAB', 'BEN', 'CMR', 'EGY', 'NGA', 'MOZ', 'SOM', 'MDV', 'LKA', 'ARE', 'IDN', 'KIR', 'XXX')", resultSet -> Pair.of(resultSet.getString(1), resultSet.getString(2))));
            for (Pair<String, String> pair : countryIdByCode) {
                scriptVariables.put("Country_" + pair.getKey(), pair.getValue());
            }
            // Add program variables (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2115)
            Set<Pair<String, String>> programIdByCode = executor.findMultipleResultAstSet(SqlQuery.wrap(
                    "SELECT code, topiaId FROM common.Program WHERE code IN ('9')", resultSet -> Pair.of(resultSet.getString(1), resultSet.getString(2))));
            for (Pair<String, String> pair : programIdByCode) {
                scriptVariables.put("Program_" + pair.getKey(), pair.getValue());
            }
            // Add schoolType variables (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2115)
            Set<Pair<String, String>> schoolTypedByCode = executor.findMultipleResultAstSet(SqlQuery.wrap(
                    "SELECT code, topiaId FROM ps_common.SchoolType WHERE code IN " +
                            "('0', '1', '2')", resultSet -> Pair.of(resultSet.getString(1), resultSet.getString(2))));
            for (Pair<String, String> pair : schoolTypedByCode) {
                scriptVariables.put("SchoolType_" + pair.getKey(), pair.getValue());
            }
        }

        if (withIds) {

            addPerson(executor);
            addProgram(executor);
            addHarbour(executor);
            addSpeciesList(executor);
            addSizeMeasureType(executor);
            addVesselActivity(executor);
            addObjectOperation(executor);
            addTransmittingBuoyOperation(executor);
            addTransmittingBuoyType(executor);
            addFpaZone(executor);
            addObservedSystem(executor);

            // Add new ps_common.ObjectMaterial See https://gitlab.com/ultreiaio/ird-observe/-/issues/1974
            addObjectMaterial(executor);

            // Add missing species for AVDTH import See https://gitlab.com/ultreiaio/ird-observe/-/issues/2068
            addSpecies(executor);

            // Add extra referential See https://gitlab.com/ultreiaio/ird-observe/-/issues/2111
            addLlCommonOnBoardProcessing(executor);
            addLlLandingCompany(executor);

            // Update Vessel.regEx See https://gitlab.com/ultreiaio/ird-observe/-/issues/2388
            executor.addScript("05", "update_referential_common_Vessel_wellRegex");
            executor.addScript("06", "update_referential_common_FpaZone");
        }

        executor.addScript("99", "fix_date_type");
        executor.addScript("99", "fix_not_null_constraints");
    }

    private void addCountry(MigrationVersionResourceExecutor executor, Map<String, String> scriptVariables) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT iso3code FROM common.Country", resultSet -> resultSet.getString(1)));
        Map<String, String> missing = Map.of(
                "ARE", "fr.ird.referential.common.Country#1464000000000#0.00082",
                "IDN", "fr.ird.referential.common.Country#1464000000000#0.00083",
                "KIR", "fr.ird.referential.common.Country#1464000000000#0.00084",
                "LKA", "fr.ird.referential.common.Country#1464000000000#0.00078"
        );
        Stream.of("ARE", "IDN", "KIR", "LKA").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_common_Country_" + code);
                scriptVariables.put("Country_" + code, missing.get(code));
            }
        });
        executor.addScript("01", "add_referential_common_Country_finalize");
    }

    private void addPerson(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT lastName || '-' || firstName FROM common.Person", resultSet -> resultSet.getString(1).toLowerCase()));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of(
                "Akpatou-Yao Lydiane",
                "Amatcha-Hélène",
                "Assare-Eynoux Cédric",
                "Attin-Assamoi Ulrich",
                "Attin-Kablan Sonia",
                "Bakayoko-Ibrahima",
                "Ban Yakesseu'-Aristide Nicaise",
                "Beugré-Degré Armel Landry",
                "Boniface-Patrick",
                "Diatta-Isidor",
                "Elizabeth-Mervin",
                "Gnadou-Zegbehi Magloire ",
                "Gueye-Oulimata",
                "Hoareau-Laurent",
                "Komenan-Kouame Lucien",
                "Kouadio-Dakon",
                "Kouakou-Kodjo Christophe",
                "Laporte-Travis",
                "Lesperance-Mike",
                "Mathiot-Evans",
                "N'Da-Kouao Donatien",
                "Poris-Darel",
                "Rose-Rodney",
                "Senghor-Emile",
                "Stephen-Alvis",
                "Thiaw-Arame",
                "Wane-Ibra", "Yala-Dominique").sorted().forEach(code -> {
            if (!existingCodes.contains(code.toLowerCase())) {
                newCodes.add(code);
                executor.addScript("01", "add_referential_common_Person_" + code.replaceAll("\\s+", "_"));
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("01", "add_referential_common_Person_finalize");
        }
    }

    private void addHarbour(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM common.Harbour", resultSet -> resultSet.getString(1)));
        Stream.of("94", "99", "100", "101", "102", "103", "104", "105", "107", "110", "111", "112", "113", "114", "116", "117", "118", "119", "120", "121", "122").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_common_Harbour_" + code);
            }
        });
        executor.addScript("01", "add_referential_common_Harbour_finalize");
    }

    private void addSpeciesList(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM common.SpeciesList", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("8", "9").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_common_SpeciesList_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("01", "add_referential_common_SpeciesList_finalize");
        }
    }

    private void addSizeMeasureType(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM common.SizeMeasureType", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Set.of("99").forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_common_SizeMeasureType_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("01", "add_referential_common_SizeMeasureType_finalize");
        }
    }

    private void addVesselActivity(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_common.VesselActivity", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("22", "23", "24", "25", "26", "27", "29", "30", "31", "32", "37", "38", "39").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_ps_common_VesselActivity_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("01", "add_referential_ps_common_VesselActivity_finalize");
        }
    }

    private void addObjectOperation(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_common.ObjectOperation", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Set.of("11").forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_ps_common_ObjectOperation_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("01", "add_referential_ps_common_ObjectOperation_finalize");
        }
    }

    private void addTransmittingBuoyOperation(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_common.TransmittingBuoyOperation", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("4", "5").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_ps_common_TransmittingBuoyOperation_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("01", "add_referential_ps_common_TransmittingBuoyOperation_finalize");
        }
    }

    private void addTransmittingBuoyType(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_common.TransmittingBuoyType", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("100", "999").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_ps_common_TransmittingBuoyType_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("01", "add_referential_ps_common_TransmittingBuoyType_finalize");
        }
    }

    private void addFpaZone(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM common.FpaZone", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("42", "43", "999").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_common_FpaZone_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("01", "add_referential_common_FpaZone_finalize");
        }
    }

    private void addSpecies(MigrationVersionResourceExecutor executor) {
        Set<String> existingFaoCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT faoCode FROM common.Species", resultSet -> resultSet.getString(1)));
        Set<String> newFaoCodes = new LinkedHashSet<>();
        Stream.of("ANE", "BOG", "COM", "JAX", "PAX", "PIL", "BIP", "BOP", "SSM", "KGM", "CER", "ACC").sorted().forEach(faoCode -> {
            if (!existingFaoCodes.contains(faoCode)) {
                executor.addScript("02", "add_referential_common_Species_" + faoCode);
                newFaoCodes.add(faoCode);
            }
        });
        if (!newFaoCodes.isEmpty()) {
            executor.addScript("02", "add_referential_common_Species_finalize");
        }
    }

    private void addObservedSystem(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_common.ObservedSystem", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("23", "24", "25", "26", "27", "50", "53", "62", "31", "28", "32", "33", "34", "35", "36", "37", "38", "40", "41", "42", "43", "70", "71", "72", "73", "80", "90", "91", "92", "93", "94", "95", "96", "97", "98", "101", "102", "103", "104").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_ps_common_ObservedSystem_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("01", "add_referential_ps_common_ObservedSystem_finalize");
        }
    }

    private void addProgram(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM common.Program", resultSet -> resultSet.getString(1)));
        Stream.of("99").forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("04", "add_referential_common_Program_" + code);
            }
        });

    }

    private void addObjectMaterial(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_common.ObjectMaterial", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("4-10", "4-11").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01", "add_referential_ps_common_ObjectMaterial_" + code);
                newCodes.add(code);
            }
        });
        Stream.of("1-1-1-2-6", "1-1-1-3-6", "1-1-2-4-6", "1-1-2-4-7", "4-12", "4-12-1", "4-12-2", "4-12-3").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("01_2260", "add_referential_ps_common_ObjectMaterial_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("01", "add_referential_ps_common_ObjectMaterial_finalize");
        }
    }

    private void addLlCommonOnBoardProcessing(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ll_common.OnBoardProcessing", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("TAD").forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("03", "add_referential_ll_common_OnBoardProcessing_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("03", "add_referential_ll_common_OnBoardProcessing_finalize");
        }
    }

    private void addLlLandingCompany(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ll_landing.Company", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("SPAR", "FRESH CUP", "KEMP", "TFO", "OCEANBASKE", "PBO", "FSI", "BREEZE", "COCODEMER", "SAZ").sorted().forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("03", "add_referential_ll_landing_Company_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("03", "add_referential_ll_landing_Company_finalize");
        }
    }
}
