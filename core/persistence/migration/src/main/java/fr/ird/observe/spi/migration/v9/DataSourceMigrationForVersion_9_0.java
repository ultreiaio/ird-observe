package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created on 19/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_9_0 extends ByMajorMigrationVersionResource {
    private static final Logger log = LogManager.getLogger(DataSourceMigrationForVersion_9_0.class);
    public static final String INFORMATION_SOURCE_O = "fr.ird.referential.ps.observation.InformationSource#1464000000000#01";
    public static final String INFORMATION_SOURCE_P = "fr.ird.referential.ps.observation.InformationSource#1464000000000#02";
    public static final String INFORMATION_SOURCE_U = "fr.ird.referential.ps.observation.InformationSource#1464000000000#03";
    public static final String SPECIES_FATE_5 = "fr.ird.referential.ps.common.SpeciesFate#1239832683619#0.6250731662108877";
    public static final String SPECIES_FATE_6 = "fr.ird.referential.ps.common.SpeciesFate#1239832683619#0.5722739932065866";
    public static final String DEFAULT_WEIGHT_MEASURE_METHOD = "fr.ird.referential.common.WeightMeasureMethod#666#03";
    public static final Set<String> SMALL_SPECIES_TO_SPECIES_FATE_6 =
            Set.of("fr.ird.referential.common.Species#1239832685476#0.5618871286604711", //ALB
                   "fr.ird.referential.common.Species#1239832685475#0.13349466123905152", //BET
                   "fr.ird.referential.common.Species#1239832685474#0.975344121171992", //SKJ
                   "fr.ird.referential.common.Species#1239832685474#0.8943253454598569"); //YFT
    public static final String SPECIES_FATE_15 = "fr.ird.referential.ps.common.SpeciesFate#1464000000000#15";

    public DataSourceMigrationForVersion_9_0() {
        super(Version.valueOf("9.0"), true);
        ByMajorMigrationVersionResource.createResourceScriptVariables(this, "2021-09-17", "2021-09-17 00:00:00.000000");
    }

    private boolean usePostgis;

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {
        // This will detect if posgis is here and remove old trigger which are well design
        executor.executeForPG(this::detectPostgisTriggers);

        boolean withIds = executor.findSingleResult(SqlQuery.wrap("SELECT COUNT (*) FROM common.person", r -> r.getInt(1) > 0));
        if (withIds) {
            Map<String, String> scriptVariables = getScriptVariables();
            // Add harbour variables (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2115)
            Set<Pair<String, String>> harbourIdByCode = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT code, topiaId FROM common.Harbour WHERE code IN ('10', '113', '122', '2', '3', '31', '32', '42', '43', '45', '48')", resultSet -> Pair.of(resultSet.getString(1), resultSet.getString(2))));
            for (Pair<String, String> pair : harbourIdByCode) {
                scriptVariables.put("Harbour_" + pair.getKey(), pair.getValue());
            }
            // Add species variables (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2115)
            Set<Pair<String, String>> speciesIdByFaoCode = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT faoCode, topiaId FROM common.Species WHERE faoCode IN ('BET','LTA','FRI','ALB','TUN','LOT','SKJ','YFT')", resultSet -> Pair.of(resultSet.getString(1), resultSet.getString(2))));
            for (Pair<String, String> pair : speciesIdByFaoCode) {
                scriptVariables.put("Species_" + pair.getKey(), pair.getValue());
            }
            // Add program variables (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2115)
            Set<Pair<String, String>> programIdByCode = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT code, topiaId FROM common.Program WHERE code IN ('9', '99')", resultSet -> Pair.of(resultSet.getString(1), resultSet.getString(2))));
            for (Pair<String, String> pair : programIdByCode) {
                String key = pair.getKey();
                String value = pair.getValue();
                switch (key) {
                    case "9": {
                        scriptVariables.put("Program_PS_9", value.replace(".common.", ".ps.common."));
                    }
                    break;
                    case "99": {
                        String replace = value.replace(".common.", ".ll.common.");
                        scriptVariables.put("Program_LL_9", replace);
                    }
                    break;
                }
            }
            if (!scriptVariables.containsKey("Program_LL_9")) {
                scriptVariables.put("Program_LL_9", "fr.ird.referential.ll.common.Program#" + ByMajorMigrationVersionResource.REFERENTIAL_PREFIX + "9");
            }
            // Add country variables (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2115)
            Set<Pair<String, String>> countryIdByCode = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT code, topiaId FROM common.Country WHERE code IN ('2','3', '23', '56')", resultSet -> Pair.of(resultSet.getString(1), resultSet.getString(2))));
            for (Pair<String, String> pair : countryIdByCode) {
                scriptVariables.put("Country_" + pair.getKey(), pair.getValue());
            }

            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2123
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2129
            removeDuplicatedAssociations(executor, true, "common", "gear", "gearCharacteristic");
            removeDuplicatedAssociations(executor, true, "common", "species", "ocean");
            removeDuplicatedAssociations(executor, true, "common", "speciesList", "species");
            removeDuplicatedAssociations(executor, true, "common", "speciesGroup", "speciesGroupReleaseMode");
            removeDuplicatedAssociations(executor, true, "ll_common", "trip", "species");
            removeDuplicatedAssociations(executor, true, "ll_logbook", "catch_predator", "catch", "species");
            removeDuplicatedAssociations(executor, true, "ll_logbook", "set", "mitigationType");
            removeDuplicatedAssociations(executor, true, "ll_observation", "catch_predator", "catch", "species");
            removeDuplicatedAssociations(executor, true, "ll_observation", "set", "mitigationType");
            removeDuplicatedAssociations(executor, true, "ll_observation", "tdr", "species");
            removeDuplicatedAssociations(executor, true, "ps_observation", "activity", "observedSystem");
        }
        addNewTable(executor, withIds, "01_0", "table-ll_common_program");
        addNewTable(executor, withIds, "01_1", "table-ps_common_program");
        executor.addScript("01_2", "adapt_program");
        addNewTable(executor, withIds, "02_1", "table-ps_common_acquisitionstatus");
        addNewTable(executor, withIds, "02_2", "table-ps_common_sampletype");
        addNewTable(executor, withIds, "02_3", "table-ps_common_weightcategory");
        addNewTable(executor, withIds, "02_4", "table-ps_observation_informationsource");
        addNewTable(executor, withIds, "02_5", "table-ps_common_reasonfornofishing");
        addNewTable(executor, withIds, "02_6", "table-ps_common_reasonfornullset");

        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2515
        executor.addScript("02_7_1", "adapt_table_ps_common_speciesFate-add-weightRangeAllowed");

        if (withIds) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2515
            Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT code FROM ps_common.SpeciesFate WHERE code ='15'", resultSet -> resultSet.getString(1).toLowerCase()));
            if (existingCodes.isEmpty()) {
                executor.addScript("02_7_2", "add_ps_common_speciesFate-15");
            }
            executor.addScript("02_7_3", "update_ps_common_speciesFate-6");
        }
        addNewTable(executor, withIds, "03", "table-ps_observation_catch", "table-ps_observation_sample", "table-ps_observation_samplemeasure");

        if (withIds) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2515
            // migrate NonTargetCatch
            executor.addScript("03_1", "migrate-table-ps_observation_nonTargetCatch");

            // migrate TargetCatch
            TargetCatchRecord.processTargetCatches(executor);

            // To fix well
            executor.addScript("03_2", "migrate-table-ps_observation_catch_well");

            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2436
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2539
            SampleRecord.processSample(executor);
        }

        executor.addScript("04_0", "adapt_table-ps_observation_activity");
        executor.addScript("04_1", "adapt_table-ps_observation_set");

        executor.addScript("04_3", "adapt_table-ps_common_trip_route");

        addNewSchema(executor, withIds, "05", "schema-ps_logbook");
        addNewSchema(executor, withIds, "06", "schema-ps_landing");
        addNewSchema(executor, withIds, "07", "schema-ps_localmarket");

        executor.addScript("10_1", "adapt_table-ps_common_trip");
        executor.addScript("10_2", "adapt_table-ps_observation_set");
        executor.addScript("10_3", "adapt_table-ps_observation_TransmittingBuoy");

        if (withIds) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2299
            executor.addScript("20_0", "update-ps_common_referential-labels");
        }

        executor.addScript("21_0", "adapt_table_common_ocean");

        if (withIds) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2326
            Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT code FROM ll_landing.DataSource WHERE code ='R Dogley'", resultSet -> resultSet.getString(1).toLowerCase()));
            if (existingCodes.isEmpty()) {
                executor.addScript("21_1", "add_ll_landing_datasource");
            }
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2421
            existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT faoCode FROM common.Species WHERE faoCode = 'RAV*'", resultSet -> resultSet.getString(1)));
            if (existingCodes.isEmpty()) {
                executor.addScript("22", "add_species_rav");
            }
        }
        executor.addScript("23", "adapt_table-ll_common_trip");
        executor.addScript("24", "adapt_table-ll_landing_landing");
        executor.addScript("98_0", "create_migrate_oneToOne");

        migrateOneToOneComposition(executor, "ll_observation", "activity", "set", null);
        migrateOneToOneComposition(executor, "ll_logbook", "activity", "set", null);
        migrateOneToOneComposition(executor, "ll_logbook", "activity", "sample", "_target.trip IS NULL");
        migrateOneToOneComposition(executor, "ps_observation", "activity", "set", null);

        if (withIds) {
            Map<String, String> scriptVariables = getScriptVariables();
            // Add schoolType variables (See https://gitlab.com/ultreiaio/ird-observe/-/issues/2115)
            Set<Pair<String, String>> schoolTypedByCode = executor.findMultipleResultAstSet(SqlQuery.wrap(
                    "SELECT code, topiaId FROM ps_common.SchoolType WHERE code IN " +
                            "('0', '1', '2')", resultSet -> Pair.of(resultSet.getString(1), resultSet.getString(2))));
            for (Pair<String, String> pair : schoolTypedByCode) {
                scriptVariables.put("SchoolType_" + pair.getKey(), pair.getValue());
            }
            addObservedSystem(executor);
        }
        executor.addScript("97", "change_nautical_length_types");
        executor.addScript("98_0", "finalize_migrate_oneToOne");

        executor.addScript("99_0", "add_missing_not_null");
        executor.addScript("99_0", "add_missing_referential_code_not_null");
        executor.addScript("99_0", "add_missing_referential_status_not_null");
        executor.addScript("99_0", "add_missing_referential_label1_not_null");
        executor.addScript("99_0", "add_missing_referential_label2_not_null");
        executor.addScript("99_0", "add_missing_referential_label3_not_null");
        executor.addScript("99_1", "add_missing_primary_key");
        executor.addScript("99_1", "add_missing_unique_key");
        executor.addScript("99_2", "drop_tables");
        cleanVariables("Country_", "Harbour_", "Program_", "Species_");

        if (withIds && isForTck()) {
            // Add some referential with no usage inside others referential to make possible delete test
            // See fr.ird.observe.persistence.test.request.DeleteReferentialRequestTest
            executor.addScript("100-tck", "add_referential_for_delete_test");
        }
        if (usePostgis) {
            executor.executeForPG(this::fixPostgisTriggers);
        }
    }

    private void addObservedSystem(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_common.ObservedSystem", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("74", "81", "110").forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("96", "add_referential_ps_common_ObservedSystem_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("96", "add_referential_ps_common_ObservedSystem_finalize");
        }
    }

    @Override
    public void generateFinalizeSqlScript(MigrationVersionResourceExecutor executor) {
        long stringFixedCount = new FixStringHelper(executor).execute();
        long commentFixedCount = new FixCommentHelper(executor).execute();
        if (stringFixedCount > 0) {
            log.warn(String.format("Fix %s string rows(s).", stringFixedCount));
        }
        if (commentFixedCount > 0) {
            log.warn(String.format("Fix %s comment rows(s).", commentFixedCount));
        }
        migrateIdx(executor, "ps_observation", "catch", "set");
        migrateIdx(executor, "ps_observation", "SampleMeasure", "sample");
    }

    private void detectPostgisTriggers(MigrationVersionResourceExecutor executor) {
        usePostgis = executor.findSingleResult(SqlQuery.wrap("SELECT EXISTS(SELECT * FROM pg_proc WHERE proname = 'sync_activity_the_geom')", r -> r.getBoolean(1)));
        if (usePostgis) {
            log.info("PostGis triggers detected.");
        }
    }

    private void fixPostgisTriggers(MigrationVersionResourceExecutor executor) {
        addTrigger(executor, "sync_the_geom_default", "latitude", "longitude", "the_geom", List.of(
                "common.harbour",
                "ps_observation.activity",
                "ps_observation.transmittingbuoy",
                "ps_logbook.activity",
                "ps_logbook.transmittingbuoy",
                "ll_observation.activity",
                "ll_logbook.activity",
                "ll_landing.landing"
        ));
        addTrigger(executor, "sync_the_geom_original", "latitudeOriginal", "longitudeOriginal", "the_geom_original", List.of(
                "ps_logbook.activity"
        ));
        addTrigger(executor, "sync_the_geom_settingStart", "settingStartLatitude", "settingStartLongitude", "the_geom_settingstart", List.of(
                "ll_observation.set",
                "ll_logbook.set"
        ));
        addTrigger(executor, "sync_the_geom_settingEnd", "settingEndLatitude", "settingEndLongitude", "the_geom_settingend", List.of(
                "ll_observation.set",
                "ll_logbook.set"
        ));
        addTrigger(executor, "sync_the_geom_haulingStart", "haulingStartLatitude", "haulingStartLongitude", "the_geom_haulingstart", List.of(
                "ll_observation.set",
                "ll_logbook.set"
        ));
        addTrigger(executor, "sync_the_geom_haulingEnd", "haulingEndLatitude", "haulingEndLongitude", "the_geom_haulingend", List.of(
                "ll_observation.set",
                "ll_logbook.set"
        ));
        executor.writeSql("DROP FUNCTION IF EXISTS sync_activity_the_geom CASCADE;");
        executor.writeSql("DROP FUNCTION IF EXISTS sync_harbour_the_geom CASCADE;");
    }
}

