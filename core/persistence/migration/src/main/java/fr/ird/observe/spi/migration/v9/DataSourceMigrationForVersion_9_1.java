package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.TopiaIdFactoryForBulkSupport;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created on 14/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
@SuppressWarnings("SpellCheckingInspection")
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_9_1 extends ByMajorMigrationVersionResource {

    private static final String INSERT_WELL = "INSERT INTO ps_logbook.Well(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, well, wellSamplingConformity, wellSamplingStatus, trip) VALUES(" +
            "'%s'," + //topiaId
            "'%s'::timestamp," + //topiaCreateDate
            "%s," + //topiaVersion
            "'%s'::timestamp," + //topiaLastUpdateDate
            "'%s'," + //well
            "'%s'," + //wellSamplingConformity
            "'%s'," + //wellSamplingStatus
            "'%s'" + //trip
            ");";

    private static final String INSERT_WELL_ACTIVITY = "INSERT INTO ps_logbook.WellActivity(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, activity, well) VALUES(" +
            "'%s'," + //topiaId
            "'%s'::timestamp," + //topiaCreateDate
            "%s," + //topiaVersion
            "'%s'::timestamp," + //topiaLastUpdateDate
            "'%s'," + //activity
            "'%s'" + //well
            ");";
    private static final String INSERT_WELL_ACTIVITY_SPECIES = "INSERT INTO ps_logbook.WellActivitySpecies(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, species, weightCategory, weight, wellActivity, wellActivity_idx) VALUES (" +
            "'%s'," + //topiaId
            "'%s'::timestamp," + //topiaCreateDate
            "%s," + //topiaVersion
            "'%s'::timestamp," + //topiaLastUpdateDate
            "'%s'," + //species
            "'%s'," + //weightCategory
            "%s," + //weight
            "'%s'," + //wellActivity
            "%d" + //wellActivityIdx
            ");";
    private final TopiaIdFactory idFactory;

    public DataSourceMigrationForVersion_9_1() {
        super(Version.valueOf("9.1"), true);
        ByMajorMigrationVersionResource.createResourceScriptVariables(this, "2023-02-14", "2023-02-14 00:00:00.000000");
        idFactory = new TopiaIdFactoryForBulkSupport(LocalDateTime.of(2023, 2, 14, 0, 0).toInstant(ZoneOffset.UTC).toEpochMilli());
    }

    static class WellPlanStructure {
        final Date createDate;
        final Date lastUpdateDate;
        final String well;
        final String speciesId;
        final String activityId;
        final String weightCategoryId;
        final Float weight;
        final String wellSamplingConformityId;
        final String wellSamplingStatusId;
        final String tripId;
        final int tripIdx;

        WellPlanStructure(Date createDate,
                          Date lastUpdateDate,
                          String well,
                          String speciesId,
                          String activityId,
                          String weightCategoryId,
                          Float weight,
                          String wellSamplingConformityId,
                          String wellSamplingStatusId,
                          String tripId,
                          int tripIdx) {
            this.createDate = createDate;
            this.lastUpdateDate = lastUpdateDate;
            this.well = well;
            this.speciesId = speciesId;
            this.activityId = activityId;
            this.weightCategoryId = weightCategoryId;
            this.weight = weight;
            this.wellSamplingConformityId = wellSamplingConformityId;
            this.wellSamplingStatusId = wellSamplingStatusId;
            this.tripId = tripId;
            this.tripIdx = tripIdx;
        }

        WellStructure toWell() {
            return new WellStructure(createDate, lastUpdateDate, well, wellSamplingConformityId, wellSamplingStatusId, tripId);
        }

        WellActivityStructure toWellActivity() {
            return new WellActivityStructure(activityId);
        }

        WellActivitySpeciesStructure toWellActivitySpecies(int wellActivityIdx) {
            return new WellActivitySpeciesStructure(speciesId, weightCategoryId, weight, wellActivityIdx);
        }
    }

    static class WellStructure {
        final Date createDate;
        final Date lastUpdateDate;
        final String well;
        final String wellSamplingConformityId;
        final String wellSamplingStatusId;
        final String tripId;
        final List<WellActivityStructure> wellActivity = new LinkedList<>();

        WellStructure(Date createDate,
                      Date lastUpdateDate,
                      String well,
                      String wellSamplingConformityId,
                      String wellSamplingStatusId,
                      String tripId) {
            this.createDate = createDate;
            this.lastUpdateDate = lastUpdateDate;
            this.well = well;
            this.wellSamplingConformityId = wellSamplingConformityId;
            this.wellSamplingStatusId = wellSamplingStatusId;
            this.tripId = tripId;
        }

        public WellActivityStructure addActivity(WellPlanStructure wellPlanStructure) {
            WellActivityStructure result = wellPlanStructure.toWellActivity();
            wellActivity.add(result);
            return result;
        }
    }

    static class WellActivityStructure {
        final String activityId;
        final List<WellActivitySpeciesStructure> wellActivitySpecies = new LinkedList<>();

        WellActivityStructure(String activityId) {
            this.activityId = activityId;
        }

        public void addSpecies(WellPlanStructure wellPlanStructure) {
            WellActivitySpeciesStructure result = wellPlanStructure.toWellActivitySpecies(wellActivitySpecies.size());
            wellActivitySpecies.add(result);
        }
    }


    static class WellActivitySpeciesStructure {
        final String speciesId;
        final String weightCategoryId;
        final Float weight;
        final int wellActivityIdx;

        WellActivitySpeciesStructure(String speciesId, String weightCategoryId, Float weight, int wellActivityIdx) {
            this.speciesId = speciesId;
            this.weightCategoryId = weightCategoryId;
            this.weight = weight;
            this.wellActivityIdx = wellActivityIdx;
        }
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {
        boolean withIds = executor.findSingleResult(SqlQuery.wrap("SELECT COUNT (*) FROM common.person", r -> r.getInt(1) > 0));
        addNewTable(executor, withIds, "01", "table-ps_logbook_well");
        addNewTable(executor, withIds, "01", "table-ps_logbook_wellactivity");
        addNewTable(executor, withIds, "01", "table-ps_logbook_wellactivityspecies");

        List<WellPlanStructure> existingWellPlan = executor.findMultipleResult(SqlQuery.wrap(
                "SELECT topiaCreateDate, lastUpdateDate, well, species, activity, weightCategory, weight, wellSamplingConformity, wellSamplingStatus, trip, trip_idx FROM ps_logbook.WellPlan ORDER BY trip, trip_idx", resultSet -> new WellPlanStructure(
                        resultSet.getTimestamp(1),
                        resultSet.getTimestamp(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getFloat(7),
                        resultSet.getString(8),
                        resultSet.getString(9),
                        resultSet.getString(10),
                        resultSet.getInt(11))));

        // get all records by trip
        Map<String, List<WellPlanStructure>> byTrip = splitByTrip(existingWellPlan);

        for (Map.Entry<String, List<WellPlanStructure>> byTripEntry : byTrip.entrySet()) {

            // -- trip level

            // get all records by well
            Map<String, List<WellPlanStructure>> byWell = splitByWell(byTripEntry.getValue());

            // add all well for this trip
            for (Map.Entry<String, List<WellPlanStructure>> byWellEntry : byWell.entrySet()) {

                // -- well level

                List<WellPlanStructure> forWell = byWellEntry.getValue();

                // create a new well
                WellStructure wellStructure = forWell.get(0).toWell();

                // get all records by activity
                Map<String, List<WellPlanStructure>> byActivity = splitByActivity(forWell);

                // add all wellActivity for this well
                for (Map.Entry<String, List<WellPlanStructure>> byActivityEntry : byActivity.entrySet()) {

                    // -- wellActivity level

                    List<WellPlanStructure> forActivity = byActivityEntry.getValue();

                    // create a new wellActivity
                    WellActivityStructure wellActivity = wellStructure.addActivity(forWell.get(0));

                    // add all wellActivitySpecies for this wellActivity
                    for (WellPlanStructure wellPlanStructure : forActivity) {

                        // -- wellActivitySpecies level

                        // create a new wellActivitySpecies
                        wellActivity.addSpecies(wellPlanStructure);
                    }
                }

                // the well is well formed, can flush it to sql
                flushWell(executor, wellStructure);
            }
        }
        executor.addScript("02", "drop-table-wellPlan");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2546
        executor.addScript("03", "fix_sql_types_issue_2546");
        executor.addScript("04", "drop_ll_common_weightDeterminationMethod");

        if (withIds) {
            addVesselActivities(executor);
            addTransmittingBuoyOperation(executor);
            addDestination(executor);
        }
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2435
        addNewTable(executor, withIds, "06", "table-common_sizemeasuremethod");
        executor.addScript("06", "adapt-data-table-common_sizemeasuremethod");

        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2619
        addNewTable(executor, withIds, "07", "table-ps_localmarket_buyer");
        executor.addScript("07", "adapt-table-ps_localmarket_batch");
        //FIXME Set this back to tck (or remove simply it when we will have some data)
        if (withIds /**&& isForTck()**/) {
            // Add some referential with no usage inside others referential to make possible delete test
            // See fr.ird.observe.persistence.test.request.DeleteReferentialRequestTest
            executor.addScript("07-tck", "fill-table-ps_localmarket_buyer");
        }

        executor.addScript("08", "fix_ps_observation_catch_weightMeasureMethod");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2647
        executor.addScript("09", "adapt_table-common_vessel-add-fields");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2646
        executor.addScript("10", "adapt_table-common_vessel-rename-field");
        if (withIds) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2800
            addCountries(executor);
        }
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2659
        executor.addScript("11", "adapt_table-common_fpazone-add-fields");
        if (withIds) {
            executor.addScript("11", "fill_table-common_fpazone-add-fields");
        }
        executor.addScript("11", "finalize_table-common_fpazone-add-fields");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2661
        executor.addScript("12", "fix_ps_localmarket_packaging");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2672
        executor.addScript("13", "fill-table-common_Vessel-cfrId");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2053
        executor.addScript("14", "adapt-table-ps_logbook_sampleActivity");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2676
        executor.addScript("15", "fix_ll_primitive_boolean");

    }

    @Override
    public void generateFinalizeSqlScript(MigrationVersionResourceExecutor executor) throws IOException {
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2620
        cleanLastUpdateDate(executor);
    }

    private void addVesselActivities(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_common.VesselActivity", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("36", "101", "102").forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("05", "add_referential_ps_common_VesselActivity_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("05", "add_referential_ps_common_VesselActivity_finalize");
        }
    }

    private void addTransmittingBuoyOperation(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_common.TransmittingBuoyOperation", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("99").forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("05", "add_referential_ps_common_TransmittingBuoyOperation_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("05", "add_referential_ps_common_TransmittingBuoyOperation_finalize");
        }
    }

    private void addDestination(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_landing.Destination", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        Stream.of("28", "29").forEach(code -> {
            if (!existingCodes.contains(code)) {
                executor.addScript("05", "add_referential_ps_landing_Destination_" + code);
                newCodes.add(code);
            }
        });
        if (!newCodes.isEmpty()) {
            executor.addScript("05", "add_referential_ps_landing_Destination_finalize");
        }
    }

    private void addCountries(MigrationVersionResourceExecutor executor) {
        Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT topiaId FROM common.Country", resultSet -> resultSet.getString(1)));
        Set<String> newCodes = new LinkedHashSet<>();
        if (!existingCodes.contains("fr.ird.referential.common.Country#1239832675593#0.24921769452411147")) {
            executor.addScript("10_2", "add_referential_common_Country_22");
            newCodes.add("22");
        }
        if (!existingCodes.contains("fr.ird.referential.common.Country#1464000000000#0.00072")) {
            executor.addScript("10_2", "add_referential_common_Country_72");
            newCodes.add("72");
        }
        if (!newCodes.isEmpty()) {
            executor.addScript("10_2", "add_referential_common_Country_finalize");
        }
    }

    private Map<String, List<WellPlanStructure>> splitByTrip(List<WellPlanStructure> existingWellPlan) {
        String tripId = null;
        List<WellPlanStructure> forTrip = null;
        Map<String, List<WellPlanStructure>> result = new LinkedHashMap<>();
        for (WellPlanStructure wellPlanStructure : existingWellPlan) {
            String currentTripId = wellPlanStructure.tripId;
            if (!Objects.equals(tripId, currentTripId)) {
                // new trip to use
                tripId = currentTripId;
                result.put(tripId, forTrip = new LinkedList<>());
            }
            forTrip.add(wellPlanStructure);
        }
        return result;
    }

    private Map<String, List<WellPlanStructure>> splitByWell(List<WellPlanStructure> existingWellPlan) {
        Map<String, List<WellPlanStructure>> result = new LinkedHashMap<>();
        String wellId = null;
        List<WellPlanStructure> forWell = null;
        for (WellPlanStructure wellPlanStructure : existingWellPlan) {
            String currentWellId = wellPlanStructure.well;
            if (!Objects.equals(wellId, currentWellId)) {
                // new well to use
                wellId = currentWellId;
                result.put(wellId, forWell = new LinkedList<>());
            }
            forWell.add(wellPlanStructure);
        }
        return result;
    }

    private Map<String, List<WellPlanStructure>> splitByActivity(List<WellPlanStructure> existingWellPlan) {
        Map<String, List<WellPlanStructure>> result = new LinkedHashMap<>();
        String activityId = null;
        List<WellPlanStructure> forActivity = null;
        for (WellPlanStructure wellPlanStructure : existingWellPlan) {
            String currentActivityId = wellPlanStructure.activityId;
            if (!Objects.equals(activityId, currentActivityId)) {
                // new wellActivity to use
                activityId = currentActivityId;
                result.put(activityId, forActivity = new LinkedList<>());
            }
            forActivity.add(wellPlanStructure);
        }
        return result;
    }

    private void flushWell(MigrationVersionResourceExecutor executor, WellStructure well) {
        String wellId = idFactory.newTopiaIdFromIdPrefix("fr.ird.data.ps.logbook.Well");
        String createDate = new Timestamp(well.createDate.getTime()).toString();
        String lastUpdateDate = new Timestamp(well.lastUpdateDate.getTime()).toString();
        executor.writeSql(String.format(INSERT_WELL,
                                        wellId,
                                        createDate,
                                        0,
                                        lastUpdateDate,
                                        well.well,
                                        well.wellSamplingConformityId,
                                        well.wellSamplingStatusId,
                                        well.tripId));
        for (WellActivityStructure wellActivity : well.wellActivity) {
            String wellActivityId = idFactory.newTopiaIdFromIdPrefix("fr.ird.data.ps.logbook.WellActivity");
            executor.writeSql(String.format(INSERT_WELL_ACTIVITY,
                                            wellActivityId,
                                            createDate,
                                            0,
                                            lastUpdateDate,
                                            wellActivity.activityId,
                                            wellId));
            for (WellActivitySpeciesStructure wellActivitySpecies : wellActivity.wellActivitySpecies) {
                String wellActivitySpeciesId = idFactory.newTopiaIdFromIdPrefix("fr.ird.data.ps.logbook.WellActivitySpecies");
                executor.writeSql(String.format(INSERT_WELL_ACTIVITY_SPECIES,
                                                wellActivitySpeciesId,
                                                createDate,
                                                0,
                                                lastUpdateDate,
                                                wellActivitySpecies.speciesId,
                                                wellActivitySpecies.weightCategoryId,
                                                wellActivitySpecies.weight,
                                                wellActivityId,
                                                wellActivitySpecies.wellActivityIdx));
            }
        }
    }

    private void cleanLastUpdateDate(MigrationVersionResourceExecutor executor) {
        List<LastUpdateDateRecord> records = executor.findMultipleResult(SqlQuery.wrap("SELECT type, topiaCreateDate, lastUpdateDate, topiaVersion  FROM common.LastUpdateDate", LastUpdateDateRecord::new));
        Iterator<LastUpdateDateRecord> iterator = records.iterator();
        while (iterator.hasNext()) {
            LastUpdateDateRecord record = iterator.next();
            String type = record.getType();
            switch (type) {
                case "fr.ird.observe.entities.data.ll.WeightMeasure ":
                    record.setType("fr.ird.observe.entities.data.ll.observation.WeightMeasure");
                    break;
                case "fr.ird.observe.entities.longline.GearUsefeaturesLongline":
                    record.setType("fr.ird.observe.entities.data.ll.common.GearUseFeatures");
                    break;
                case "fr.ird.observe.entities.data.ll.observation.TdrRecord":
                    iterator.remove();
                    break;
            }
        }
        records.sort(Comparator.comparing(LastUpdateDateRecord::getType));
        int index = 0;
        executor.writeSql("DELETE from common.lastUpdateDate;");
        for (LastUpdateDateRecord record : records) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2650
            String id = String.format("fr.ird.LastUpdateDate#0#%s", Strings.leftPad("" + (index++), 3, "0"));
            executor.writeSql(String.format("INSERT INTO common.LastUpdateDate(topiaId, topiaVersion, topiaCreateDate, lastUpdateDate, type) values ('%s', %s, '%s'::timestamp, '%s'::timestamp, '%s');",
                                            id,
                                            record.getVersion(),
                                            record.getCreateDate(),
                                            record.getLastUpdateDate(),
                                            record.getType()));
        }
    }
}

