package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.TopiaMigrationServiceException;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.io.IOException;
import java.util.List;

/**
 * Created on 16/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_9_2 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_9_2() {
        super(Version.valueOf("9.2"), true);
        createResourceScriptVariables(this, "2023-09-01", "2023-09-01 00:00:00.000000");
    }


    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {
        boolean withIds = executor.findSingleResult(SqlQuery.wrap("SELECT COUNT (*) FROM common.person", r -> r.getInt(1) > 0));

        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2714
        issue2714(executor);
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2729
        executor.addScript("02", "issue-2729");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2449
        executor.addScript("03", "issue-2449");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2740
        executor.addScript("04", "issue-2740");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2708
        addNewTable(executor, withIds, "05_01_issue-2708", "table-ps_observation_nontargetcatchreleasehandlingplace");
        executor.addScript("05_02_issue-2708", "adapt-data-table");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2498
        executor.addScript("06", "issue-2498");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2762
        executor.addScript("07", "issue-2762");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2497
        executor.addScript("08", "issue-2497");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2725
        executor.addScript("09", "issue-2725");
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2763
        if (withIds) {
            executor.addScript("10", "issue-2763");
            if (isForTck()) {
                executor.addScript("10", "issue-2763-tck");
            }
        }
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2764
        if (withIds) {
            executor.addScript("11_01", "issue-2764_add-type-list");
        }
        addNewTable(executor, withIds, "11_02_issue-2764", "table-common_gearcharacteristiclistitem");
        if (withIds) {
            executor.addScript("11_03", "issue-2764_add-list-001");
            executor.addScript("11_03", "issue-2764_add-list-002");
            executor.addScript("11_03", "issue-2764_add-list-003");
            executor.addScript("11_03", "issue-2764_add-list-004");
            executor.addScript("11_03", "issue-2764_add-list-005");
            executor.addScript("11_03", "issue-2764_add-list-006");
            executor.addScript("11_03", "issue-2764_add-list-007");
            executor.addScript("11_03", "issue-2764_add-list-008");
            executor.addScript("11_03", "issue-2764_add-list-009");
            executor.addScript("11_03", "issue-2764_add-list-010");
            executor.addScript("11_03", "issue-2764_add-list-011");
            executor.addScript("11_03", "issue-2764_add-list-012");
            executor.addScript("11_03", "issue-2764_add-list-013");
            executor.addScript("11_03", "issue-2764_add-list-014");
            executor.addScript("11_03", "issue-2764_add-list-015");
            executor.addScript("11_03", "issue-2764_add-list-016");
            executor.addScript("11_03", "issue-2764_add-list-017");
            executor.addScript("11_03", "issue-2764_add-list-018");
            executor.addScript("11_03", "issue-2764_add-list-019");
            executor.addScript("11_03", "issue-2764_add-list-020");
            executor.addScript("11_03", "issue-2764_add-list-021");
            executor.addScript("11_03", "issue-2764_add-list-022");
            executor.addScript("11_03", "issue-2764_add-list-023");
            executor.addScript("11_03", "issue-2764_add-list-024");
            executor.addScript("11_03", "issue-2764_add-list-025");
        }
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2765
        addNewTable(executor, withIds, "12_01_issue-2765", "table-common_gear_associations");
        if (withIds) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2787
            executor.addScript("13", "issue-2787");
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2788
            executor.addScript("14", "issue-2788");
        }

    }

    @Override
    public void generateFinalizeSqlScript(MigrationVersionResourceExecutor executor) throws IOException {
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2780
        ReorderActivityNumber.run(executor);
    }

    private void issue2714(MigrationVersionResourceExecutor executor) {
        List<String> badCatchCountNotNull = executor.findMultipleResult(SqlQuery.wrap(
                "SELECT topiaId FROM ll_observation.Catch WHERE count IS NULL and acquisitionMode != 0 ORDER BY set, topiaId", resultSet ->
                        resultSet.getString(1)));
        if (!badCatchCountNotNull.isEmpty()) {
            throw new TopiaMigrationServiceException("There is some ll_observation.Catch with count NULL and acquisitionMode != 0:\n" + String.join("\n", badCatchCountNotNull));
        }
        executor.addScript("01", "issue-2714");

    }
}


