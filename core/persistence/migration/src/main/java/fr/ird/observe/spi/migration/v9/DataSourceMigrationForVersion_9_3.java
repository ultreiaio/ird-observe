package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

/**
 * Created at 15/11/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_9_3 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_9_3() {
        super(Version.valueOf("9.3"), false);
        createResourceScriptVariables(this, "2024-05-01", "2024-05-01 00:00:00.000000");
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {
        boolean withIds = executor.findSingleResult(SqlQuery.wrap("SELECT COUNT (*) FROM common.person", r -> r.getInt(1) > 0));
        if (withIds) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2044
            executor.addScript("01", "issue-2879");
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2857
            executor.addScript("02", "issue-2857");
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2869
            executor.addScript("03", "issue-2869");
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2877
            executor.addScript("04", "issue-2877");
        }
    }

}



