package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created at 11/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
@AutoService(MigrationVersionResource.class)
public class DataSourceMigrationForVersion_9_4 extends ByMajorMigrationVersionResource {

    public DataSourceMigrationForVersion_9_4() {
        super(Version.valueOf("9.4"), false);
        createResourceScriptVariables(this, "2024-09-11", "2024-09-11 00:00:00.000000");
    }

    @Override
    public void generateSqlScript(MigrationVersionResourceExecutor executor) {
        boolean withIds = executor.findSingleResult(SqlQuery.wrap("SELECT COUNT (*) FROM common.person", r -> r.getInt(1) > 0));
        if (withIds) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2914
            issue2914(executor);
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2931
            executor.addScript("01", "issue-2931");
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2909
            executor.addScript("02", "issue-2909");
        }
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2818
        issue2818(executor, withIds);
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2669
        issue2669(executor);
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2932
        issue2932(executor,withIds);
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2959
        issue2959(executor,withIds);
        if (withIds) {
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2958
            executor.addScript("07", "issue-2958");
        }
    }

    private void issue2914(MigrationVersionResourceExecutor executor) {
        executor.findMultipleResult(SqlQuery.wrap("SELECT topiaId, floatline1Length, floatline2Length FROM ll_observation.Basket WHERE floatline1Length IS NOT NULL OR floatline2Length IS NOT NULL", BasketRecord::new))
                .stream()
                .filter(BasketRecord::isModified)
                .map(BasketRecord::toSql)
                .forEach(executor::writeSql);
    }

    private void issue2818(MigrationVersionResourceExecutor executor, boolean withIds) {
        addNewTable(executor, withIds, "03_01_issue-2818", "table-common_enginemake");
        addNewTable(executor, withIds, "03_02_issue-2818", "table-common_hullmaterial");
        executor.addScript("03_03", "issue-2818_update-table-common_vessel");
    }

    private void issue2669(MigrationVersionResourceExecutor executor) {
        executor.addScript("04_01", "issue-2669");
        executor.addScript("04_02", "issue-2669");
    }

    private void issue2932(MigrationVersionResourceExecutor executor, boolean withIds) {
        if (withIds) {
            if (!executor.findSingleResult(SqlQuery.wrap("SELECT COUNT (*) FROM common.weightMeasureType WHERE topiaId = 'fr.ird.referential.common.WeightMeasureType#1726219730662#0.7553113855315937'", r -> r.getInt(1) > 0))) {
                executor.addScript("05_01", "issue-2932_add_weight_measure_type");
            }
            if (!executor.findSingleResult(SqlQuery.wrap("SELECT COUNT (*) FROM common.sizeMeasureMethod WHERE topiaId = 'fr.ird.referential.common.SizeMeasureMethod#1726219851150#0.2400733645586871'", r -> r.getInt(1) > 0))) {
                executor.addScript("05_02", "issue-2932_add_size_measure_method");
            }
            if (!executor.findSingleResult(SqlQuery.wrap("SELECT COUNT (*) FROM common.weightMeasureMethod WHERE topiaId = 'fr.ird.referential.common.WeightMeasureMethod#1726219878250#0.4217724493500721'", r -> r.getInt(1) > 0))) {
                executor.addScript("05_03", "issue-2932_add_weight_measure_method");
            }
        }
        executor.addScript("05_04", "issue-2932_update-table-ll-observation-catch");
        executor.addScript("05_05", "issue-2932_update-table-ll-observation-size-measure");
        executor.addScript("05_06", "issue-2932_update-table-ll-observation-weight-measure");
    }


    private void issue2959(MigrationVersionResourceExecutor executor, boolean withIds) {
        if (withIds) {
            Set<String> existingCodes = executor.findMultipleResultAstSet(SqlQuery.wrap("SELECT CODE FROM ps_landing.Fate", resultSet -> resultSet.getString(1)));
            Set<String> newCodes = new LinkedHashSet<>();
            Stream.of("3", "4").forEach(code -> {
                if (!existingCodes.contains(code)) {
                    executor.addScript("06_issue-2959_add_referential_ps_landing_Fate",  code);
                    newCodes.add(code);
                }
            });
            if (!newCodes.isEmpty()) {
                executor.addScript("06_issue-2959_add_referential_ps_landing_Fate", "finalize");
            }
        }
    }

    private static class BasketRecord {
        private final String id;
        private final Float floatline1Length;
        private final Float floatline2Length;
        private final boolean modified;

        public BasketRecord(ResultSet r) throws SQLException {
            this.id = r.getString(1);
            Double floatline1Length = toDouble(r.getObject(2));
            Double floatline2Length = toDouble(r.getObject(3));
            this.floatline1Length = floatline1Length == null ? null : Numbers.roundNDigits(floatline1Length.floatValue(), 2);
            this.floatline2Length = floatline2Length == null ? null : Numbers.roundNDigits(floatline2Length.floatValue(), 2);
            this.modified = (this.floatline1Length != null && this.floatline1Length.toString().equals(floatline1Length.toString())) ||
                    (this.floatline2Length != null && this.floatline2Length.toString().equals(floatline2Length.toString()));
        }

        private static Double toDouble(Object r) {
            return r == null ? null : new BigDecimal(r.toString()).setScale(10, RoundingMode.HALF_UP).doubleValue();
        }

        public boolean isModified() {
            return modified;
        }

        public String toSql() {
            return String.format("UPDATE ll_observation.Basket SET floatline1Length = %s, floatline2Length = %s WHERE topiaId ='%s';", floatline1Length, floatline2Length, id);
        }
    }
}




