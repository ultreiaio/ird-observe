package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

/**
 * To auto-trim string comment fields.
 * <p>
 * <strong>Note:</strong> If fixed value is empty or blank then set {@code null} value.
 * <p>
 * Created on 11/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.17
 */
public class FixCommentHelper {

    private static final Logger log = LogManager.getLogger(FixCommentHelper.class);

    private final MigrationVersionResourceExecutor executor;
    private final AtomicLong count = new AtomicLong();
    private final Function<String, String> commentFormat;

    public FixCommentHelper(MigrationVersionResourceExecutor executor) {
        this.executor = Objects.requireNonNull(executor);
        commentFormat = executor.commentFormat();
    }

    public long execute() {

        fixField("common.Vessel", "comment");

        fixField("ll_common.GearUseFeatures", "comment");
        fixField("ll_common.Program", "comment");
        fixField("ll_common.Trip", "generalComment");
        fixField("ll_common.Trip", "logbookComment");
        fixField("ll_common.Trip", "observationsComment");

        fixField("ll_landing.Landing", "comment");

        fixField("ll_logbook.Activity", "comment");
        fixField("ll_logbook.Catch", "comment");
        fixField("ll_logbook.Set", "comment");
        fixField("ll_logbook.Sample", "comment");

        fixField("ll_observation.Activity", "comment");
        fixField("ll_observation.Branchline", "comment");
        fixField("ll_observation.Catch", "comment");
        fixField("ll_observation.Set", "comment");

        fixField("ps_common.GearUseFeatures", "comment");
        fixField("ps_common.Program", "comment");
        fixField("ps_common.Trip", "generalComment");
        fixField("ps_common.Trip", "logbookComment");
        fixField("ps_common.Trip", "observationsComment");

        fixField("ps_localmarket.Batch", "origin");
        fixField("ps_localmarket.Sample", "comment");
        fixField("ps_localmarket.SampleSpecies", "comment");
        fixField("ps_localmarket.Survey", "comment");

        fixField("ps_logbook.Activity", "comment");
        fixField("ps_logbook.Catch", "comment");
        fixField("ps_logbook.FloatingObject", "comment");
        fixField("ps_logbook.Route", "comment");
        fixField("ps_logbook.Sample", "comment");
        fixField("ps_logbook.SampleSpecies", "comment");
        fixField("ps_logbook.TransmittingBuoy", "comment");

        fixField("ps_observation.Activity", "comment");
        fixField("ps_observation.Catch", "comment");
        fixField("ps_observation.FloatingObject", "comment");
        fixField("ps_observation.NonTargetCatchRelease", "comment");
        fixField("ps_observation.Route", "comment");
        fixField("ps_observation.Set", "comment");
        fixField("ps_observation.TransmittingBuoy", "comment");
        return count.get();
    }

    public void fixField(String gav, String field) {

        executor.doSqlWork(connection -> {
            try (PreparedStatement statement = connection.prepareStatement(String.format("SELECT topiaId, %2$s FROM %1$s WHERE %2$s IS NOT NULL", gav, field))) {
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        String fieldValue = resultSet.getString(2);
                        String fixFieldValue = fieldValue.trim();
                        if (!fieldValue.equals(fixFieldValue) || fixFieldValue.equals("\\")) {
                            if (fixFieldValue.isEmpty() || fixFieldValue.equals("\\")) {
                                fixFieldValue = null;
                            }
                            String id = resultSet.getString(1);
                            String finalFieldValue = commentFormat.apply(fixFieldValue);
                            log.info(String.format("Fix comment field %s.%s[%s] from\n[%s]\nto\n[%s]", gav, field, id, fieldValue, fixFieldValue));
                            executor.writeSql(String.format("UPDATE %s SET %s = %s WHERE topiaId = '%s';", gav, field, finalFieldValue, id));
                            count.incrementAndGet();
                        }
                    }
                }
            }
        });

    }
}
