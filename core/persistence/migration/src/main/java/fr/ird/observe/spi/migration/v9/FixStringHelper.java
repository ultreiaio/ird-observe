package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.SqlHelper;
import fr.ird.observe.dto.StringCleaner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;

/**
 * To auto-trim string fields and remove any {@code '} characters inside it.
 * <p>
 * <strong>Note:</strong> If fixed value is empty or blank then set {@code null} value.
 * <p>
 * Created on 11/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.17
 */
public class FixStringHelper {

    private static final Logger log = LogManager.getLogger(FixStringHelper.class);

    private final MigrationVersionResourceExecutor executor;
    private final AtomicLong count = new AtomicLong();

    public FixStringHelper(MigrationVersionResourceExecutor executor) {
        this.executor = Objects.requireNonNull(executor);
    }

    static class FixStringEntityModel {

        private final String gav;
        private final Map<String, StringCleaner> fieldsActions;
        private final Set<String> fieldsDeleteIfNull;

        static FixStringEntityModel on(String gav, StringCleaner cleaner, String firstField, String... fields) {
            return new FixStringEntityModel(gav).add(cleaner, firstField, fields);
        }

        static FixStringEntityModel onI18n(String gav, StringCleaner cleaner, String firstField, String... fields) {
            return on(gav, StringCleaner.TRIM, "label1", "label2", "label3").add(cleaner, firstField, fields);
        }

        private FixStringEntityModel(String gav) {
            this.gav = gav;
            this.fieldsActions = new TreeMap<>();
            this.fieldsDeleteIfNull = new TreeSet<>();
        }

        FixStringEntityModel add(StringCleaner cleaner, String firstField, String... fields) {
            fieldsActions.put(firstField, cleaner);
            for (String field : fields) {
                fieldsActions.put(field, cleaner);
            }
            return this;
        }

        FixStringEntityModel deleteIfNull(String firstField, String... fields) {
            fieldsDeleteIfNull.add(firstField);
            Collections.addAll(fieldsDeleteIfNull, fields);
            return this;
        }

    }

    public long execute() {
        executor.doSqlWork(connection -> {
            execute(connection, FixStringEntityModel.onI18n("common.Country", StringCleaner.ALL, "code", "iso2Code", "iso3Code"));
            execute(connection, FixStringEntityModel.onI18n("common.DataQuality", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.FpaZone", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.Gear", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.GearCharacteristic", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.GearCharacteristicType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.Harbour", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.on("common.LengthLengthParameter", StringCleaner.ALL, "coefficients", "inputOutputFormula", "outputInputFormula"));
            execute(connection, FixStringEntityModel.onI18n("common.LengthMeasureMethod", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.on("common.LengthWeightParameter", StringCleaner.ALL, "coefficients", "lengthWeightFormula", "weightLengthFormula"));
            execute(connection, FixStringEntityModel.onI18n("common.Ocean", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.Organism", StringCleaner.ALL, "code"));
            // not removing single quotes
            execute(connection, FixStringEntityModel.on("common.Person", StringCleaner.TRIM, "firstName", "lastName"));
            execute(connection, FixStringEntityModel.onI18n("common.Sex", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.on("common.ShipOwner", StringCleaner.ALL, "code", "label"));
            execute(connection, FixStringEntityModel.onI18n("common.SizeMeasureType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.Species", StringCleaner.ALL, "scientificLabel"));
            execute(connection, FixStringEntityModel.onI18n("common.SpeciesGroup", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.SpeciesGroupReleaseMode", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.SpeciesList", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.Vessel", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.VesselType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.on("common.VesselSizeCategory", StringCleaner.ALL, "code", "capacityLabel", "gaugeLabel"));
            execute(connection, FixStringEntityModel.onI18n("common.WeightMeasureMethod", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.WeightMeasureType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("common.Wind", StringCleaner.ALL, "code"));

            execute(connection, FixStringEntityModel.onI18n("ll_common.BaitSettingStatus", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.BaitType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.CatchFate", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.HealthStatus", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.HookSize", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.HookType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.LightsticksColor", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.LightsticksType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.LineType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.MitigationType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.ObservationMethod", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.OnBoardProcessing", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.Program", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.SettingShape", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.TripType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.VesselActivity", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_common.WeightDeterminationMethod", StringCleaner.ALL, "code"));

            execute(connection, FixStringEntityModel.onI18n("ll_landing.Company", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_landing.Conservation", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_landing.DataSource", StringCleaner.ALL, "code"));

            execute(connection, FixStringEntityModel.onI18n("ll_observation.BaitHaulingStatus", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_observation.EncounterType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_observation.HookPosition", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_observation.ItemHorizontalPosition", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_observation.ItemVerticalPosition", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_observation.MaturityStatus", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.on("ll_observation.SensorBrand", StringCleaner.ALL, "brandName", "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_observation.SensorDataFormat", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_observation.SensorType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ll_observation.StomachFullness", StringCleaner.ALL, "code"));

            execute(connection, FixStringEntityModel.onI18n("ps_common.AcquisitionStatus", StringCleaner.ALL, "code"));
            // not set to null if empty (special value FOB has an empty code)
            execute(connection, FixStringEntityModel.onI18n("ps_common.ObjectMaterial", StringCleaner.REMOVE_SINGLE_QUOTE_AND_TRIM, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.ObjectMaterialType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.ObjectOperation", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.ObservedSystem", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.Program", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.ReasonForNoFishing", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.ReasonForNullSet", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.SampleType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.SchoolType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.SpeciesFate", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.TransmittingBuoyOperation", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.TransmittingBuoyOwnership", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.TransmittingBuoyType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.VesselActivity", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_common.WeightCategory", StringCleaner.ALL, "code"));

            execute(connection, FixStringEntityModel.onI18n("ps_landing.Destination", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_landing.Fate", StringCleaner.ALL, "code"));

            execute(connection, FixStringEntityModel.onI18n("ps_localmarket.BatchComposition", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_localmarket.BatchWeightType", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_localmarket.Packaging", StringCleaner.ALL, "code"));

            execute(connection, FixStringEntityModel.onI18n("ps_logbook.InformationSource", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_logbook.SampleQuality", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_logbook.SetSuccessStatus", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_logbook.WellContentStatus", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_logbook.WellSamplingConformity", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_logbook.WellSamplingStatus", StringCleaner.ALL, "code"));

            execute(connection, FixStringEntityModel.onI18n("ps_observation.DetectionMode", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_observation.InformationSource", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_observation.NonTargetCatchReleaseConformity", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_observation.NonTargetCatchReleaseStatus", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_observation.NonTargetCatchReleasingTime", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_observation.ReasonForDiscard", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_observation.SpeciesStatus", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.onI18n("ps_observation.SurroundingActivity", StringCleaner.ALL, "code"));

            execute(connection, FixStringEntityModel.on("ll_common.GearUseFeaturesMeasurement", StringCleaner.ALL, "measurementValue").deleteIfNull("measurementValue"));
            execute(connection, FixStringEntityModel.on("ll_common.Trip", StringCleaner.ALL, "ersId", "homeId"));

            execute(connection, FixStringEntityModel.on("ll_logbook.Catch", StringCleaner.ALL, "photoReferences", "tagNumber"));
            execute(connection, FixStringEntityModel.on("ll_logbook.SamplePart", StringCleaner.ALL, "tagNumber"));
            execute(connection, FixStringEntityModel.on("ll_logbook.Set", StringCleaner.ALL, "homeId"));

            execute(connection, FixStringEntityModel.on("ll_observation.Catch", StringCleaner.ALL, "photoReferences", "tagNumber"));
            execute(connection, FixStringEntityModel.on("ll_observation.SensorUsed", StringCleaner.ALL, "sensorSerialNo"));
            execute(connection, FixStringEntityModel.on("ll_observation.Set", StringCleaner.ALL, "homeId"));
            execute(connection, FixStringEntityModel.on("ll_observation.Tdr", StringCleaner.ALL, "serialNo"));

            execute(connection, FixStringEntityModel.on("ps_common.GearUseFeaturesMeasurement", StringCleaner.ALL, "measurementValue").deleteIfNull("measurementValue"));
            execute(connection, FixStringEntityModel.on("ps_common.Trip", StringCleaner.ALL, "ersId", "formsUrl", "homeId", "reportsUrl"));

            execute(connection, FixStringEntityModel.on("ps_localmarket.Sample", StringCleaner.ALL, "number"));

            execute(connection, FixStringEntityModel.on("ps_logbook.Catch", StringCleaner.ALL, "well"));
            execute(connection, FixStringEntityModel.on("ps_logbook.FloatingObject", StringCleaner.ALL, "supportVesselName"));
            execute(connection, FixStringEntityModel.on("ps_logbook.Sample", StringCleaner.ALL, "well"));
            execute(connection, FixStringEntityModel.on("ps_logbook.TransmittingBuoy", StringCleaner.ALL, "code"));
            execute(connection, FixStringEntityModel.on("ps_logbook.WellPlan", StringCleaner.ALL, "well"));

            execute(connection, FixStringEntityModel.on("ps_observation.Activity", StringCleaner.ALL, "ersId"));
            execute(connection, FixStringEntityModel.on("ps_observation.FloatingObject", StringCleaner.ALL, "supportVesselName"));
            execute(connection, FixStringEntityModel.on("ps_observation.SampleMeasure", StringCleaner.ALL, "picturesReferences", "tagNumber"));
            execute(connection, FixStringEntityModel.on("ps_observation.Set", StringCleaner.ALL, "supportVesselName"));
            execute(connection, FixStringEntityModel.on("ps_observation.TransmittingBuoy", StringCleaner.ALL, "code"));
        });
        return count.get();
    }

    void execute(Connection connection, FixStringEntityModel model) throws SQLException {
        for (Map.Entry<String, StringCleaner> entry : model.fieldsActions.entrySet()) {
            execute(connection, model.gav, entry.getKey(), entry.getValue(), model.fieldsDeleteIfNull.contains(entry.getKey()));
        }
    }

    void execute(Connection connection, String gav, String field, StringCleaner cleaner, boolean deleteIfNull) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(String.format("SELECT topiaId, %2$s FROM %1$s WHERE %2$s IS NOT NULL", gav, field))) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    String fieldValue = resultSet.getString(2);
                    String fixFieldValue = cleaner.apply(fieldValue);
                    if (!fieldValue.equals(fixFieldValue)) {
                        String id = resultSet.getString(1);
                        if (deleteIfNull && fixFieldValue == null) {
                            log.warn(String.format("Delete row since field is empty %s.%s[%s] (was '%s')", gav, field, id, fieldValue));
                            executor.writeSql(String.format("DELETE FROM %1$s WHERE topiaId = '%2$s';", gav, id));
                        } else {
                            log.info(String.format("Fix string field %s.%s[%s] from\n[%s]\n to\n[%s]", gav, field, id, fieldValue, fixFieldValue));
                            String finalFieldValue = SqlHelper.escapeString(fixFieldValue);
                            executor.writeSql(String.format("UPDATE %1$s SET %2$s = %3$s WHERE topiaId = '%4$s';", gav, field, finalFieldValue, id));
                        }
                        count.incrementAndGet();
                    }
                }
            }
        }
    }
}
