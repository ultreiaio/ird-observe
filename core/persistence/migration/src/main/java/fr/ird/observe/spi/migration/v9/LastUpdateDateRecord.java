package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created on 05/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.0
 */
class LastUpdateDateRecord {

    private String type;
    private final long version;
    private final Date createDate;
    private final Date lastUpdateDate;

    LastUpdateDateRecord(ResultSet resultSet) throws SQLException {
        type = resultSet.getString(1);
        createDate = resultSet.getTimestamp(2);
        lastUpdateDate = resultSet.getTimestamp(3);
        version = resultSet.getLong(4);
    }

    public String getType() {
        return type;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public long getVersion() {
        return version;
    }

    public void setType(String type) {
        this.type = type;
    }
}
