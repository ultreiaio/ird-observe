package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.util.List;

/**
 * To re-order ps_logbook.Activity#number while migration to v9.2.0.
 * <p>
 * See <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2780">issue 2780</a>.
 * <p>
 * Created at 12/09/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
class ReorderActivityNumber {

    static void run(MigrationVersionResourceExecutor executor) {
        // Re-order activities where trip.activitiesAcquisitionMode is BY_NUMBER
        run(executor, "BY_NUMBER", "number, topiaCreateDate");
        // Re-order activities where trip.activitiesAcquisitionMode is BY_TIME
        run(executor, "BY_TIME", "time, topiaCreateDate");
    }

    private static void run(MigrationVersionResourceExecutor executor, String activitiesAcquisitionMode, String activitiesOrder) {
        // Get all routes for trip using the activitiesAcquisitionMode
        List<String> routeIds = executor.findMultipleResult(SqlQuery.wrap(String.format("SELECT r.topiaId FROM ps_logbook.Route r WHERE r.trip IN (SELECT t.topiaId FROM ps_common.Trip t WHERE t.activitiesAcquisitionMode = '%s')", activitiesAcquisitionMode),
                                                                          rs -> rs.getString(1)));
        for (String routeId : routeIds) {
            // Get all activities using the activitiesOrder order clause
            List<ActivityRecord> activityRecords = executor.findMultipleResult(SqlQuery.wrap(String.format("SELECT a.topiaId, a.number FROM ps_logbook.Activity a WHERE a.route = '%s' ORDER BY %s", routeId, activitiesOrder),
                                                                                             rs -> new ActivityRecord(rs.getString(1), rs.getInt(2))));
            int number = 0;
            for (ActivityRecord activityRecord : activityRecords) {
                number++;
                if (activityRecord.getNumber() != number) {
                    // need to update the number on this activity
                    String sql = String.format("UPDATE ps_logbook.Activity SET number = %s WHERE topiaId ='%s';", number, activityRecord.getId());
                    executor.writeSql(sql);
                }
            }
        }
    }

    private static class ActivityRecord {
        private final String id;
        private final int number;

        private ActivityRecord(String id, int number) {
            this.id = id;
            this.number = number;
        }

        public String getId() {
            return id;
        }

        public int getNumber() {
            return number;
        }
    }
}
