package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.StringCleaner;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.function.BiFunction;

import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toTimeStamp;

/**
 * Created on 20/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.18
 */
class SampleMeasureRecord {

    static void addNonTargetSampleMeasure(Connection connection, MigrationVersionResourceExecutor executor, Map<String, String> sampleMapping) throws SQLException {
        addSampleMeasure(connection, executor, sampleMapping, "SELECT " +
                                 "REPLACE(topiaId, '.NonTargetLength', '.SampleMeasure'), " +
                                 "topiaVersion + 1, " +
                                 "topiaCreateDate, " +
                                 "homeId, " +
                                 "length, " +
                                 "isLengthComputed, " +
                                 "picturesReferences, " +
                                 "weight, " +
                                 "isWeightComputed, " +
                                 "count, " +
                                 "acquisitionMode, " +
                                 "species, " +
                                 "REPLACE(nonTargetSample, '.NonTargetSample', '.Sample'), " +
                                 "sex, " +
                                 "lastUpdateDate, " +
                                 "sizeMeasureType, " +
                                 "weightMeasureType, " +
                                 "tagNumber, " +
                                 "speciesFate, " +
                                 "nonTargetSample_idx, " +
                                 "lengthMeasureMethod, " +
                                 "weightMeasureMethod " +
                                 "FROM ps_observation.NonTargetLength",
                         SampleMeasureRecord::toNonTargetSpeciesFate);
    }

    static void addDiscardedTargetSampleMeasure(Connection connection, MigrationVersionResourceExecutor executor, Map<String, String> sampleMapping) throws SQLException {
        addSampleMeasure(connection, executor, sampleMapping, "SELECT " +
                                 "REPLACE(tl.topiaId, '.TargetLength', '.SampleMeasure'), " +
                                 "tl.topiaVersion + 1, " +
                                 "tl.topiaCreateDate, " +
                                 "tl.homeId, " +
                                 "tl.length, " +
                                 "tl.isLengthComputed, " +
                                 "NULL, " +
                                 "tl.weight, " +
                                 "tl.isWeightComputed, " +
                                 "tl.count, " +
                                 "tl.acquisitionMode, " +
                                 "tl.species, " +
                                 "REPLACE(tl.targetSample, '.TargetSample', '.Sample'), " +
                                 "'fr.ird.referential.common.Sex#1239832686121#0.0', " +
                                 "tl.lastUpdateDate, " +
                                 "tl.sizeMeasureType, " +
                                 "tl.weightMeasureType, " +
                                 "tl.tagNumber, " +
                                 "'fr.ird.referential.ps.common.SpeciesFate#1239832683619#0.6250731662108877', " +
                                 "-tl.targetSample_idx, " +
                                 "tl.lengthMeasureMethod, " +
                                 "tl.weightMeasureMethod " +
                                 "FROM ps_observation.TargetSample ts " +
                                 "INNER JOIN ps_observation.TargetLength tl ON (tl.targetSample=ts.topiaId) " +
                                 "WHERE ts.discarded",
                         SampleMeasureRecord::toDiscardedTargetSpeciesFate);
    }

    static void addNotDiscardedTargetSampleMeasure(Connection connection, MigrationVersionResourceExecutor executor, Map<String, String> sampleMapping) throws SQLException {
        addSampleMeasure(connection, executor, sampleMapping, "SELECT " +
                                 "REPLACE(tl.topiaId, '.TargetLength', '.SampleMeasure'), " +
                                 "tl.topiaVersion + 1, " +
                                 "tl.topiaCreateDate, " +
                                 "tl.homeId, " +
                                 "tl.length, " +
                                 "tl.isLengthComputed, " +
                                 "NULL, " +
                                 "tl.weight, " +
                                 "tl.isWeightComputed, " +
                                 "tl.count, " +
                                 "tl.acquisitionMode, " +
                                 "tl.species, " +
                                 "REPLACE(tl.targetSample, '.TargetSample', '.Sample'), " +
                                 "'fr.ird.referential.common.Sex#1239832686121#0.0', " +
                                 "tl.lastUpdateDate, " +
                                 "tl.sizeMeasureType, " +
                                 "tl.weightMeasureType, " +
                                 "tl.tagNumber, " +
                                 "'fr.ird.referential.ps.common.SpeciesFate#1239832683619#0.5722739932065866', " +
                                 "-tl.targetSample_idx, " +
                                 "tl.lengthMeasureMethod, " +
                                 "tl.weightMeasureMethod " +
                                 "FROM ps_observation.TargetSample ts " +
                                 "INNER JOIN ps_observation.TargetLength tl ON (tl.targetSample=ts.topiaId) " +
                                 "WHERE NOT ts.discarded",
                         SampleMeasureRecord::toNotDiscardedTargetSpeciesFate);
    }

    static void addSampleMeasure(Connection connection,
                                 MigrationVersionResourceExecutor executor,
                                 Map<String, String> sampleMapping,
                                 String query,
                                 BiFunction<SampleMeasureRecord, String, String> speciesFateSupplier) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    SampleMeasureRecord sampleMeasure = new SampleMeasureRecord(resultSet, sampleMapping, speciesFateSupplier);
                    executor.writeSql(sampleMeasure.toSql());
                }
            }
        }
    }

    String topiaId;
    long topiaVersion;
    Timestamp topiaCreateDate;
    Timestamp lastUpdateDate;
    String homeId;
    Float length;
    Boolean isLengthComputed;
    String picturesReferences;
    Float weight;
    Boolean isWeightComputed;
    int count;
    int acquisitionMode;
    String species;
    String sampleId;
    String sex;
    String sizeMeasureType;
    String weightMeasureType;
    String tagNumber;
    String speciesFate;
    int sample_idx;
    String lengthMeasureMethod;
    String weightMeasureMethod;

    public SampleMeasureRecord(ResultSet resultSet,
                               Map<String, String> sampleMapping,
                               BiFunction<SampleMeasureRecord, String, String> speciesFateSupplier) throws SQLException {
        this.topiaId = resultSet.getString(1);
        this.topiaVersion = resultSet.getLong(2);
        this.topiaCreateDate = resultSet.getTimestamp(3);
        this.homeId = resultSet.getString(4);
        this.length = resultSet.getFloat(5);
        this.isLengthComputed = resultSet.getBoolean(6);
        this.picturesReferences = resultSet.getString(7);
        this.weight = resultSet.getFloat(8);
        this.isWeightComputed = resultSet.getBoolean(9);
        this.count = resultSet.getInt(10);
        this.acquisitionMode = resultSet.getInt(11);
        this.species = resultSet.getString(12);
        String originalSampleId = resultSet.getString(13);
        String replaceSampleId = sampleMapping.get(originalSampleId);
        this.sampleId = replaceSampleId == null ? originalSampleId : replaceSampleId;
        this.sex = resultSet.getString(14);
        this.lastUpdateDate = resultSet.getTimestamp(15);
        this.sizeMeasureType = resultSet.getString(16);
        this.weightMeasureType = resultSet.getString(17);
        this.tagNumber = resultSet.getString(18);
        String originalSpeciesFate = resultSet.getString(19);
        this.speciesFate = speciesFateSupplier.apply(this, originalSpeciesFate);
        this.sample_idx = resultSet.getInt(20);
        this.lengthMeasureMethod = resultSet.getString(21);
        this.weightMeasureMethod = resultSet.getString(22);
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2503
        if (acquisitionMode == 0 && weight != null && !isWeightComputed) {
            isWeightComputed = true;
        }
    }

    String toSql() {
        return String.format("INSERT INTO ps_observation.SampleMeasure(" +
                                     " topiaId," +
                                     " topiaVersion," +
                                     " topiaCreateDate," +
                                     " homeId," +
                                     " length," +
                                     " isLengthComputed," +
                                     " picturesReferences," +
                                     " weight," +
                                     " isWeightComputed," +
                                     " count," +
                                     " acquisitionMode," +
                                     " species," +
                                     " sample," +
                                     " sex," +
                                     " lastUpdateDate," +
                                     " sizeMeasureType," +
                                     " weightMeasureType," +
                                     " tagNumber," +
                                     " speciesFate," +
                                     " sample_idx," +
                                     " lengthMeasureMethod," +
                                     " weightMeasureMethod)" +
                                     " VALUES(" +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s, " +
                                     "%s " +
                                     ");",
                             escapeString(topiaId),
                             topiaVersion,
                             toTimeStamp(topiaCreateDate),
                             escapeString(StringCleaner.ALL.apply(homeId)),
                             length,
                             isLengthComputed,
                             escapeString(StringCleaner.ALL.apply(picturesReferences)),
                             weight,
                             isWeightComputed,
                             count,
                             acquisitionMode,
                             escapeString(species),
                             escapeString(sampleId),
                             escapeString(sex),
                             toTimeStamp(lastUpdateDate),
                             escapeString(sizeMeasureType),
                             escapeString(weightMeasureType),
                             escapeString(tagNumber),
                             escapeString(speciesFate),
                             sample_idx,
                             escapeString(lengthMeasureMethod),
                             escapeString(weightMeasureMethod));
    }

    private String toNonTargetSpeciesFate(String speciesFate) {
        if (speciesFate == null) {
            // keep null value from NonTargetCatch
            return null;
        }
        if (DataSourceMigrationForVersion_9_0.SPECIES_FATE_6.equals(speciesFate)) {
            // like for non target catches always change from speciesFate 6 to 15
            speciesFate = DataSourceMigrationForVersion_9_0.SPECIES_FATE_15;
        }
        return speciesFate;
    }

    private String toDiscardedTargetSpeciesFate(String speciesFate) {
        // Discarded target species use always speciesFate 5 (and we have it from the sql query)
        return speciesFate;
    }

    private String toNotDiscardedTargetSpeciesFate(String speciesFate) {
        // by default we use in sql query speciesFate = 6
        if (!DataSourceMigrationForVersion_9_0.SMALL_SPECIES_TO_SPECIES_FATE_6.contains(species)) {
            // for small species, they are always with speciesFate = 15
            speciesFate = DataSourceMigrationForVersion_9_0.SPECIES_FATE_15;
        }
        return speciesFate;
    }
}
