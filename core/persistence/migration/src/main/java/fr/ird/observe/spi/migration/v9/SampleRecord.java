package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.SqlHelper;
import fr.ird.observe.dto.StringCleaner;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toTimeStamp;

/**
 * Created on 20/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.18
 */
class SampleRecord {
    final String topiaId;
    final long topiaVersion;
    final Timestamp topiaCreateDate;
    final Timestamp lastUpdateDate;
    String homeId;
    String comment;
    String set;

    static void processSample(MigrationVersionResourceExecutor executor) {
        executor.doSqlWork(connection -> {
            Function<String, String> commentFormat = executor.commentFormat();
            Map<String, String> sampleMapping = new TreeMap<>();
            Map<String, SampleRecord> setToSampleMap = new TreeMap<>();
            SampleRecord.addNonTargetSample(connection, setToSampleMap, sampleMapping);
            SampleRecord.addTargetSample(connection, setToSampleMap, sampleMapping);
            for (SampleRecord sample : setToSampleMap.values()) {
                executor.writeSql(sample.toSql(commentFormat));
            }
            SampleMeasureRecord.addNonTargetSampleMeasure(connection, executor, sampleMapping);
            SampleMeasureRecord.addDiscardedTargetSampleMeasure(connection, executor, sampleMapping);
            SampleMeasureRecord.addNotDiscardedTargetSampleMeasure(connection, executor, sampleMapping);
        });
    }

    static void addNonTargetSample(Connection connection, Map<String, SampleRecord> setToSampleMap, Map<String, String> sampleMapping) throws SQLException {

        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT " +
                        "REPLACE(topiaId, '.NonTargetSample', '.Sample'), " +
                        "topiaVersion, " +
                        "topiaCreateDate, " +
                        "homeId, " +
                        "comment, " +
                        "set, " +
                        "lastUpdateDate " +
                        "FROM ps_observation.nontargetsample " +
                        "ORDER BY set, topiaId")) {
            addSample(statement, setToSampleMap, sampleMapping);
        }
    }


    static void addTargetSample(Connection connection, Map<String, SampleRecord> setToSampleMap, Map<String, String> sampleMapping) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT " +
                        "REPLACE(topiaId, '.TargetSample', '.Sample'), " +
                        "topiaVersion, " +
                        "topiaCreateDate, " +
                        "homeId, " +
                        "comment, " +
                        "set, " +
                        "lastUpdateDate " +
                        "FROM ps_observation.targetsample " +
                        "ORDER BY set, topiaId")) {
            addSample(statement, setToSampleMap, sampleMapping);
        }
    }

    static void addSample(PreparedStatement statement, Map<String, SampleRecord> setToSampleMap, Map<String, String> sampleMapping) throws SQLException {
        try (ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                SampleRecord sample = new SampleRecord(resultSet);
                SampleRecord existingSample = setToSampleMap.get(sample.set);
                if (existingSample == null) {
                    // new sample
                    setToSampleMap.put(sample.set, sample);
                } else {
                    // add to sample mapping
                    sampleMapping.put(sample.topiaId, existingSample.topiaId);
                    // update homeId
                    existingSample.addHomeId(sample.homeId);
                    // update comment
                    existingSample.addComment(sample.comment);
                }
            }
        }
    }

    SampleRecord(ResultSet resultSet) throws SQLException {
        this.topiaId = resultSet.getString(1);
        this.topiaVersion = resultSet.getLong(2);
        this.topiaCreateDate = resultSet.getTimestamp(3);
        this.homeId = resultSet.getString(4);
        this.comment = resultSet.getString(5);
        this.set = resultSet.getString(6);
        this.lastUpdateDate = resultSet.getTimestamp(7);
    }

    public String toSql(Function<String, String> commentFormat) {
        return String.format("INSERT INTO ps_observation.Sample(topiaId, topiaVersion, topiaCreateDate, homeId, comment, set, lastUpdateDate) VALUES(%s, %s, %s, %s, %s, %s, %s);",
                             SqlHelper.escapeString(topiaId),
                             topiaVersion,
                             toTimeStamp(topiaCreateDate),
                             escapeString(StringCleaner.ALL.apply(homeId)),
                             commentFormat.apply(comment),
                             escapeString(set),
                             toTimeStamp(lastUpdateDate));
    }

    public void addHomeId(String homeId) {
        if (homeId == null) {
            return;
        }
        if (this.homeId == null) {
            this.homeId = homeId;
            return;
        }
        this.homeId += " - " + homeId;
    }

    public void addComment(String comment) {
        if (comment == null) {
            return;
        }
        if (this.comment == null) {
            this.comment = comment;
            return;
        }
        this.comment += " \n " + comment;
    }

}
