package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.StringCleaner;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;

import static fr.ird.observe.spi.SqlConversions.escapeString;
import static fr.ird.observe.spi.SqlConversions.toTimeStamp;

/**
 * Created on 06/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.17
 */
public class TargetCatchRecord {

    static final String DISCARDED_SQL = "SELECT" +
            /* 01 */    " REPLACE(tc.topiaId, '.TargetCatch', '.Catch')," +
            /* 02 */    " tc.topiaVersion + 1," +
            /* 03 */    " tc.topiaCreateDate," +
            /* 04 */    " tc.homeId," +
            /* 05 */    " tc.catchWeight," +
            /* 06 */    " tc.weightCategory," +
            /* 07 */    " tc.comment," +
            /* 08 */    " tc.set," +
            /* 09 */    " tc.lastUpdateDate," +
            /* 10 */    " tc.well," +
            /* 11 */    " -tc.set_idx," +
            /* 12 */    " tc.weightMeasureMethod," +
            /* 13 */    " tc.reasonForDiscard" +
            " FROM ps_observation.TargetCatch tc" +
            " WHERE tc.discarded AND NOT ( tc.weightCategory IS NULL AND tc.catchWeight IS NULL AND tc.well IS NULL AND tc.broughtOnDeck IS NULL AND tc.reasonForDiscard IS NULL )";

    static final String NOT_DISCARDED_SQL = "SELECT" +
            /* 01 */    " REPLACE(tc.topiaId, '.TargetCatch', '.Catch')," +
            /* 02 */    " tc.topiaVersion + 1," +
            /* 03 */    " tc.topiaCreateDate," +
            /* 04 */    " tc.homeId," +
            /* 05 */    " tc.catchWeight," +
            /* 06 */    " tc.weightCategory," +
            /* 07 */    " tc.comment," +
            /* 08 */    " tc.set," +
            /* 09 */    " tc.lastUpdateDate," +
            /* 10 */    " tc.well," +
            /* 11 */    " -tc.set_idx," +
            /* 12 */    " tc.weightMeasureMethod," +
            /* 13 */    " s.targetCatchCompositionEstimatedByObserver " +
            " FROM ps_observation.TargetCatch tc INNER JOIN ps_observation.set s on s.topiaId = tc.set" +
            " WHERE NOT tc.discarded AND NOT ( tc.weightCategory IS NULL AND tc.catchWeight IS NULL AND tc.well IS NULL AND tc.broughtOnDeck IS NULL AND tc.reasonForDiscard IS NULL )";

    public static void processTargetCatches(MigrationVersionResourceExecutor executor) {
        // Load weight category records indexed by their id
        Map<String, WeightCategoryRecord> weightCategoryRecordById = WeightCategoryRecord.loadWeightCategoriesMap(executor);
        // To track target catches with no weight category
        Set<String> catchesWithNullWeightCategory = new TreeSet<>();
        executor.doSqlWork(connection -> {
            // process discarded values
            processTargetCatches(connection, executor, weightCategoryRecordById, catchesWithNullWeightCategory, true);
            // process not discarded values
            processTargetCatches(connection, executor, weightCategoryRecordById, catchesWithNullWeightCategory, false);
        });
        // can't perform migration, we found some target catches with no weight category
        if (!catchesWithNullWeightCategory.isEmpty()) {
            StringBuilder error = new StringBuilder();
            error.append(String.format("\nFound %d target catches with null category:", catchesWithNullWeightCategory.size()));
            for (String id : catchesWithNullWeightCategory) {
                error.append("\n\t").append(id);
            }
            throw new IllegalStateException(error.toString());
        }
    }

    static void processTargetCatches(Connection connection,
                                     MigrationVersionResourceExecutor executor,
                                     Map<String, WeightCategoryRecord> weightCategoryRecordById,
                                     Set<String> catchesWithNullWeightCategory,
                                     boolean discarded) throws SQLException {
        Function<String, String> commentFormat = executor.commentFormat();
        // to skip writing sql to executor as soon as a record was found with no weight category
        // since this case will stop migration
        boolean skipWriteSql = !catchesWithNullWeightCategory.isEmpty();
        try (PreparedStatement statement = connection.prepareStatement(discarded ? DISCARDED_SQL : NOT_DISCARDED_SQL)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    TargetCatchRecord record = new TargetCatchRecord(resultSet, weightCategoryRecordById, discarded);
                    if (record.isWeightCategoryNull()) {
                        // can't process a such record,
                        // just keep id of target catch for migration failure exception message
                        catchesWithNullWeightCategory.add(record.getId());
                        // no more need to add sql to executor
                        skipWriteSql = true;
                        continue;
                    }
                    if (skipWriteSql) {
                        continue;
                    }
                    executor.writeSql(record.toSql(commentFormat));
                }
            }
        }
    }

    private final String id;
    private final long topiaVersion;
    private final Timestamp topiaCreateDate;
    private final String homeId;
    private final Float catchWeight;
    private final WeightCategoryRecord weightCategory;
    private final String comment;
    private final String reasonForDiscardId;
    private final String setId;
    private final Timestamp lastUpdateDate;
    private final String well;
    private final long setIdx;
    private final String weightMeasureMethodId;
    private final String speciesFateId;
    private final String informationSourceId;

    public TargetCatchRecord(ResultSet resultSet, Map<String, WeightCategoryRecord> weightCategoryRecordById, boolean discarded) throws SQLException {
        this.id = resultSet.getString(1);
        this.topiaVersion = resultSet.getLong(2);
        this.topiaCreateDate = resultSet.getTimestamp(3);
        this.homeId = resultSet.getString(4);
        this.catchWeight = resultSet.getFloat(5);
        String weightCategoryId = resultSet.getString(6);
        this.weightCategory = weightCategoryId == null ? null : weightCategoryRecordById.get(weightCategoryId);
        this.comment = resultSet.getString(7);
        this.setId = resultSet.getString(8);
        this.lastUpdateDate = resultSet.getTimestamp(9);
        this.well = resultSet.getString(10);
        this.setIdx = resultSet.getLong(11);
        String weightMeasureMethodId = resultSet.getString(12);
        this.weightMeasureMethodId = weightMeasureMethodId == null ? DataSourceMigrationForVersion_9_0.DEFAULT_WEIGHT_MEASURE_METHOD : weightMeasureMethodId;
        if (discarded) {
            this.reasonForDiscardId = resultSet.getString(13);
            this.speciesFateId = DataSourceMigrationForVersion_9_0.SPECIES_FATE_5;
            this.informationSourceId = DataSourceMigrationForVersion_9_0.INFORMATION_SOURCE_O;
        } else {
            this.reasonForDiscardId = null;
            this.speciesFateId = weightCategory != null && "10".equals(weightCategory.getCode()) ? DataSourceMigrationForVersion_9_0.SPECIES_FATE_15 : DataSourceMigrationForVersion_9_0.SPECIES_FATE_6;
            boolean targetCatchCompositionEstimatedByObserver = resultSet.getBoolean(13);
            this.informationSourceId = targetCatchCompositionEstimatedByObserver ? DataSourceMigrationForVersion_9_0.INFORMATION_SOURCE_P : DataSourceMigrationForVersion_9_0.INFORMATION_SOURCE_U;
        }
    }

    public String toSql(Function<String, String> commentFormat) {
        return String.format("INSERT INTO ps_observation.Catch(" +
                                     /*1*/                  "topiaId, " +
                                     /*2*/                  "topiaVersion, " +
                                     /*3*/                  "topiaCreateDate, " +
                                     /*4*/                  "homeId, " +
                                     /*5*/                  "catchWeight, " +
                                     /*6*/                  "minWeight, " +
                                     /*7*/                  "maxWeight, " +
                                     /*8*/                  "meanWeight, " +
                                     /*9*/                  "comment, " +
                                     /*10*/                 "reasonForDiscard, " +
                                     /*11*/                 "species, " +
                                     /*12*/                 "speciesFate, " +
                                     /*13*/                 "set, " +
                                     /*14*/                 "lastUpdateDate, " +
                                     /*15*/                 "well, " +
                                     /*16*/                 "set_idx, " +
                                     /*17*/                 "weightMeasureMethod, " +
                                     /*18*/                 "informationSource) VALUES ( " +
                                     "%1$s, " +
                                     "%2$s, " +
                                     "%3$s, " +
                                     "%4$s, " +
                                     "%5$s, " +
                                     "%6$s, " +
                                     "%7$s, " +
                                     "%8$s, " +
                                     "%9$s, " +
                                     "%10$s, " +
                                     "%11$s, " +
                                     "%12$s, " +
                                     "%13$s, " +
                                     "%14$s, " +
                                     "%15$s, " +
                                     "%16$s, " +
                                     "%17$s, " +
                                     "%18$s);",
                /*1*/          escapeString(id),
                /*2*/          topiaVersion,
                /*3*/          toTimeStamp(topiaCreateDate),
                /*4*/          escapeString(StringCleaner.ALL.apply(homeId)),
                /*5*/          catchWeight,
                /*6*/          weightCategory.getMinWeight(),
                /*7*/          weightCategory.getMaxWeight(),
                /*8*/          null,
                /*9*/          getComment(weightCategory, commentFormat),
                /*10*/         escapeString(reasonForDiscardId),
                /*11*/         escapeString(weightCategory.getSpeciesId()),
                /*12*/         escapeString(speciesFateId),
                /*13*/         escapeString(setId),
                /*14*/         toTimeStamp(lastUpdateDate),
                /*15*/         escapeString(StringCleaner.ALL.apply(well)),
                /*16*/         setIdx,
                /*17*/         escapeString(weightMeasureMethodId),
                /*18*/         escapeString(informationSourceId)
        );
    }

    public String getComment(WeightCategoryRecord weightCategoryRecord, Function<String, String> stringFormat) {
        if (comment == null) {
            return stringFormat.apply(weightCategoryRecord.toComment());
        }
        return stringFormat.apply(comment + "\n" + weightCategoryRecord.toComment());
    }

    public String getId() {
        return id;
    }

    public boolean isWeightCategoryNull() {
        return weightCategory == null;
    }
}
