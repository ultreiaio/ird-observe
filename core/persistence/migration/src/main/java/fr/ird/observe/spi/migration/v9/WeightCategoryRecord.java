package fr.ird.observe.spi.migration.v9;

/*-
 * #%L
 * ObServe Core :: Persistence :: Migration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created on 06/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.17
 */
public class WeightCategoryRecord {

    private final String id;
    private final String code;
    private final String speciesId;
    private final String label1;
    private final String speciesFaoCode;
    private final String speciesLabel1;
    private Float minWeight;
    private Float maxWeight;

    public static Map<String, WeightCategoryRecord> loadWeightCategoriesMap(MigrationVersionResourceExecutor executor) {
        Set<WeightCategoryRecord> weightCategories = executor.findMultipleResultAstSet(SqlQuery.wrap(
                "SELECT wc.topiaId, wc.code, wc.species, wc.label1, s.faoCode, s.label1" +
                        " FROM ps_observation.WeightCategory wc" +
                        " INNER JOIN common.species s on wc.species = s.topiaId", resultSet -> new WeightCategoryRecord(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6)
                )));
        // all weight categories found in database
        // while building safe map, this map will be reduced to weight categories that are not in the safe list
        Map<String, WeightCategoryRecord> toFixMap = new TreeMap<>(Maps.uniqueIndex(weightCategories, WeightCategoryRecord::getId));
        // safe weight categories we can use in migration (we need to get all of them, otherwise let's throw an error)
        Map<String, WeightCategoryRecord> safeMap = new TreeMap<>();
        // missing weight categories (from the safe list)
        Set<String> missingIds = new TreeSet<>();

        // load weight categories extra fields (minWeight-maxWeight)
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685804#0.034641865339321565", 0f, 3f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685810#0.9054833697032716", 3f, 10f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685818#0.5146901232402814", 11f, 30f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685824#0.30572839692068965", 3f, 30f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685830#0.3505424853460628", 31f, 50f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685837#0.9977145363359975", 11f, 50f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1265900731495#0.02540873055336068", 50f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685843#0.5227248075951917", 10f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685849#0.25030425638120324", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336721513613#0.1603994688043372", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1389775941412#0.15834335814453693", 30f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1389776343228#0.7378693597245063", 30f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1424937125831#0.9928551215821171", 0f, 10f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685747#0.8903018444376307", 0f, 3f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685754#0.10338259022397256", 3f, 10f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685760#0.5190060347627616", 11f, 30f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685767#0.17985459818605176", 3f, 30f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685773#0.36018870624305044", 31f, 50f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685779#0.6459697632929102", 11f, 50f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685785#0.017557737970281928", 50f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685791#0.96882088900269", 10f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685798#0.8205778575049493", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336721546508#0.9537814356455492", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1389776259896#0.07638719997162424", 30f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1424937065301#0.2847458239357389", 0f, 10f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1447659295872#0.5394619880622312", 0f, 3.5f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1447659325662#0.768248108810874", 3.5f, 10f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336722270972#0.37652846873357493", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1285247232470#0.6003570416302645", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336722322572#0.3938987624410807", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685856#0.37681463866680454", 0f, 1.8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685862#0.3577261083569382", 1.8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685869#0.1577912646543379", 1.8f, 4f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685876#0.571764466269097", 1.8f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685882#0.6060344598115799", 4f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685888#0.04548287749163349", 4f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685894#0.7596812507048669", 6f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685901#0.9401665182005969", 8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685907#0.703864325385401", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336740964786#0.9038903638109549", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1602771681957#0.9874591590552072", 0f, 1.8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1602771701326#0.4010839902789137", 1.8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1602771715715#0.7853847204136084", 1.8f, 4f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1602771729301#0.4884568851061948", 1.8f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1602771742931#0.7051564476067321", 4f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1602771756464#0.45050535009150927", 4f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1602771767261#0.6841254148640502", 6f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1602771778122#0.16162153365230103", 8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1602770129319#0.19516930145898193", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1602770254324#0.846496629553337", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685913#0.6908151762213386", 0f, 1.8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685919#0.32661937164161003", 1.8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685926#0.26263537384431435", 1.8f, 4f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685932#0.05603149764956994", 1.8f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685938#0.1289482411270122", 4f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685944#0.6386905101076812", 4f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685950#0.5552574879416816", 6f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685956#0.8040582347597208", 8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685962#0.5585432399656218", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336741018010#0.03331113748771242", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303922010990#0.3754603442395006", 0f, 1.8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303922245707#0.40050172552023944", 1.8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303922280084#0.6622140480197926", 1.8f, 4f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303922599555#0.7704492147942078", 1.8f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303922687314#0.6979902619400133", 4f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303922752777#0.47792415661458676", 4f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303922804724#0.4624943876905273", 6f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303922867136#0.4287768516967707", 8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1285247232470#0.2988865794350679", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336741045619#0.26215173370303935", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303925079863#0.4512897233754356", 0f, 1.8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303925079863#0.5772066302124889", 1.8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303925079863#0.5076973842921164", 1.8f, 4f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303925079864#0.15355738476896108", 1.8f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303925590606#0.2425119417078675", 4f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303925590606#0.8917988680730413", 4f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303925716959#0.9994063870345968", 6f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303925716959#0.07903343358670578", 8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1285247232470#0.6346372733268454", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336741085150#0.8502977815118109", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1285247232470#0.6536295925217674", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336741106738#0.8017167831584734", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685968#0.8743676757879854", 0f, 1.8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685975#0.9269185705341096", 1.8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685981#0.20973946035734992", 1.8f, 4f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685987#0.8124900788304172", 1.8f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685993#0.7110149379733451", 4f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685999#0.35760983661297885", 4f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832686006#0.27918502949209356", 6f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832686012#0.3678254910247588", 8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685610#0.04119062512809801", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336741128034#0.08367392559943787", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1265900836849#0.18015633272140852", 0f, 1.8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1265900836849#0.9036635253340434", 1.8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685673#0.3163596616269717", 1.8f, 4f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685679#0.019733673419385367", 1.8f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685685#0.7315998797178913", 4f, 6f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685692#0.5940916998227946", 4f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685722#0.9714869893887224", 6f, 8f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685729#0.0015801959709714763", 8f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685741#0.9084571608340902", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336741168338#0.14273986795446758", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1447659383213#0.4400294468298005", 0f, 3.5f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1447659418223#0.8121977439460182", 3.5f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1555434122029#0.33046872986365916", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1555434233562#0.6221497586001838", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1563970176195#0.042949635286452015", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1563970198102#0.5406480296062075", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685499#0.45137386216494235", 0f, 3f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1239832685505#0.6893474228813191", 3f, 10f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1265900972942#0.14809205088390143", 11f, 30f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1265900972963#0.6915765595336695", 3f, 30f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1265901072470#0.48282507736865", 31f, 50f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1265901072470#0.049555168356280266", 11f, 50f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1265901072470#0.3502049636413197", 50f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1265901225547#0.7474093737982056", 10f, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1265901225548#0.3886087214402537", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336741209227#0.14815475082140694", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1424936892080#0.3071247026973247", 0f, 10f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1447659237402#0.5659786750175115", 0f, 3.5f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1447658978491#0.015566209814640963", 3.5f, 10f);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336741235125#0.9596181329735903", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1303924814809#0.6964774216650054", null, null);
        addWeightCategoryMinMaxWeight(toFixMap, safeMap, missingIds, "fr.ird.referential.ps.observation.WeightCategory#1336741257450#0.021228233536910834", null, null);
        if (!missingIds.isEmpty()) {
            StringBuilder error = new StringBuilder();
            error.append(String.format("\nDetects %d official weight categories not found in current database:\n\t", missingIds.size()));
            error.append(String.join("\n\t", missingIds));
            log.warn(error);
        }
        if (toFixMap.isEmpty()) {
            // everything is ok, we can use the safe map
            return safeMap;
        }
        StringBuilder error = new StringBuilder(String.format("\nDetects %d weight categories in current database, that should be replaced by official one:", toFixMap.size()));
        for (WeightCategoryRecord value : toFixMap.values()) {
            error.append("\n").append(value.toString());
        }
        throw new IllegalStateException(error.toString());
    }

    private static final Logger log = LogManager.getLogger(WeightCategoryRecord.class);

    private static void addWeightCategoryMinMaxWeight(Map<String, WeightCategoryRecord> existingMap,
                                                      Map<String, WeightCategoryRecord> safeMap,
                                                      Set<String> missingIds,
                                                      String id,
                                                      Float minWeight,
                                                      Float maxWeight) {
        WeightCategoryRecord weightCategoryRecord = existingMap.get(id);
        if (weightCategoryRecord == null) {
            missingIds.add(id);
            return;
        }
        // remove it from existing map (since this weight category is ok)
        existingMap.remove(id);
        // add it to safe map
        safeMap.put(id, weightCategoryRecord);
        // add min weight
        weightCategoryRecord.setMinWeight(minWeight);
        // add max weight
        weightCategoryRecord.setMaxWeight(maxWeight);
    }

    WeightCategoryRecord(String id,
                         String code,
                         String speciesId,
                         String label1,
                         String speciesFaoCode,
                         String speciesLabel1) {
        this.id = id;
        this.code = code;
        this.speciesId = speciesId;
        this.label1 = label1;
        this.speciesFaoCode = speciesFaoCode;
        this.speciesLabel1 = speciesLabel1;
    }

    public void setMinWeight(Float minWeight) {
        this.minWeight = minWeight;
    }

    public void setMaxWeight(Float maxWeight) {
        this.maxWeight = maxWeight;
    }

    public String getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getSpeciesId() {
        return speciesId;
    }

    public String getLabel1() {
        return label1;
    }

    public Float getMinWeight() {
        return minWeight;
    }

    public Float getMaxWeight() {
        return maxWeight;
    }

    public String toComment() {
        return String.format(COMMENT_PATTERN, getCode(), getLabel1());
    }

    @Override
    public String toString() {
        return String.format("{id='%s', code='%s', label1='%s', speciesId='%s', speciesFaoCode='%s', speciesLabel1='%s'}", id, code, label1, speciesId, speciesFaoCode, speciesLabel1);
    }

    public static final String COMMENT_PATTERN = "<weightcategory>\n" +
            "   <code>%1$s</code>\n" +
            "   <label1>%2$s</label1>\n" +
            "</weightcategory>\n";
}
