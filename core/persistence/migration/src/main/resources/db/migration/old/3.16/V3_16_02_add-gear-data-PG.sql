---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
--
-- GearUseFeatures Definition
--
CREATE TABLE OBSERVE_SEINE.GEARUSEFEATURES(topiaid character varying(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, trip character varying(255), gear character varying(255) NOT NULL, number INTEGER NOT NULL, comment VARCHAR(1023), usedInTrip BOOLEAN);
ALTER TABLE OBSERVE_SEINE.GEARUSEFEATURES ADD CONSTRAINT PK_GEARUSEFEATURES PRIMARY KEY(TOPIAID);
ALTER TABLE OBSERVE_SEINE.GEARUSEFEATURES ADD CONSTRAINT FK_GEARUSEFEATURES_TRIP FOREIGN KEY(trip) REFERENCES OBSERVE_SEINE.TRIP(topiaid);
ALTER TABLE OBSERVE_SEINE.GEARUSEFEATURES ADD CONSTRAINT FK_GEARUSEFEATURES_GEAR FOREIGN KEY(gear) REFERENCES OBSERVE_COMMON.GEAR(topiaid);
CREATE INDEX FK_GEARUSEFEATURES_TRIP_IDX ON OBSERVE_SEINE.GEARUSEFEATURES(trip);
CREATE INDEX FK_GEARUSEFEATURES_GEAR_IDX ON OBSERVE_SEINE.GEARUSEFEATURES(gear);
--
-- GearUseFeaturesMeasurement Definition
--
CREATE TABLE OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT(topiaid character varying(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, gearCaracteristic character varying(255) NOT NULL, gearUseFeatures character varying(255), measurementValue character varying(255) NOT NULL);
ALTER TABLE OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT ADD CONSTRAINT PK_GEARUSEFEATURESMEASUREMENT PRIMARY KEY(TOPIAID);
ALTER TABLE OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT ADD CONSTRAINT FK_GEARUSEFEATURESMEASUREMENT_GEARCARACTERISTIC FOREIGN KEY(gearCaracteristic) REFERENCES OBSERVE_COMMON.GEARCARACTERISTIC(topiaid);
ALTER TABLE OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT ADD CONSTRAINT FK_GEARUSEFEATURESMEASUREMENT_GEARUSEFEATURES FOREIGN KEY(gearUseFeatures) REFERENCES OBSERVE_SEINE.GEARUSEFEATURES(topiaid);
CREATE INDEX FK_GEARUSEFEATURESMEASUREMENT_GEARCARACTERISTIC_IDX ON OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT(gearCaracteristic);
CREATE INDEX FK_GEARUSEFEATURESMEASUREMENT_GEARUSEFEATURES_IDX ON OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT(GearUseFeatures);
