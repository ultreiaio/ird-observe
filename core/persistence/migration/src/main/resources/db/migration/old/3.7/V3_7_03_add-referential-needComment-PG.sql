---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

ALTER TABLE observe_common.VesselSizeCategory ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.Country ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.VesselType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.Vessel ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.Sex ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.FpaZone ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.SpeciesList ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.Person ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.Ocean ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.Organism ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.LengthWeightParameter ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_common.Program ADD COLUMN NEEDCOMMENT boolean DEFAULT false;

ALTER TABLE observe_seine.SurroundingActivity ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_seine.WeightCategory ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_seine.TransmittingBuoyOperation ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_seine.ObjectOperation ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_seine.SpeciesStatus ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_seine.Wind ADD COLUMN NEEDCOMMENT boolean DEFAULT false;

ALTER TABLE observe_longline.BaitHaulingStatus ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.BaitSettingStatus ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.BaitType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.CatchFate ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.EncounterType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.Healthness ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.HookPosition ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.HookSize ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.HookType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.ItemVerticalPosition ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.ItemHorizontalPosition ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.LightsticksColor ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.LightsticksType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.LineType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.MaturityStatus ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.MitigationType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.SensorBrand ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.SensorDataFormat ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.SensorPosition ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.SensorType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.SettingShape ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.SizeMeasureType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.StomacFullness ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.TripType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.VesselActivity ADD COLUMN NEEDCOMMENT boolean DEFAULT false;
ALTER TABLE observe_longline.WeightMeasureType ADD COLUMN NEEDCOMMENT boolean DEFAULT false;

UPDATE observe_common.VesselSizeCategory SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.Country SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.VesselType SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.Vessel SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.Sex SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.FpaZone SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.SpeciesList SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.Person SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.Ocean SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.Organism SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.LengthWeightParameter SET topiaVersion = topiaVersion + 1;
UPDATE observe_common.Program SET topiaVersion = topiaVersion + 1;

UPDATE observe_seine.SurroundingActivity SET topiaVersion = topiaVersion + 1;
UPDATE observe_seine.WeightCategory SET topiaVersion = topiaVersion + 1;
UPDATE observe_seine.TransmittingBuoyOperation SET topiaVersion = topiaVersion + 1;
UPDATE observe_seine.ObjectOperation SET topiaVersion = topiaVersion + 1;
UPDATE observe_seine.SpeciesStatus SET topiaVersion = topiaVersion + 1;
UPDATE observe_seine.Wind SET topiaVersion = topiaVersion + 1;

UPDATE observe_longline.BaitHaulingStatus SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.BaitSettingStatus SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.BaitType SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.CatchFate SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.EncounterType SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.Healthness SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.HookPosition SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.HookSize SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.HookType SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.ItemVerticalPosition SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.ItemHorizontalPosition SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.LightsticksColor SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.LightsticksType SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.LineType SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.MaturityStatus SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.MitigationType SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.SensorBrand SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.SensorDataFormat SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.SensorPosition SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.SensorType SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.SettingShape SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.SizeMeasureType SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.StomacFullness SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.TripType SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.VesselActivity SET topiaVersion = topiaVersion + 1;
UPDATE observe_longline.WeightMeasureType SET topiaVersion = topiaVersion + 1;
