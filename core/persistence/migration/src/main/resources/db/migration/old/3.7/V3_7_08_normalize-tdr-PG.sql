---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE OBSERVE_LONGLINE.Tdr RENAME deploymentStart TO deployementStart;
ALTER TABLE OBSERVE_LONGLINE.Tdr DROP COLUMN  homeId;
ALTER TABLE OBSERVE_LONGLINE.Tdr ADD COLUMN homeId character varying(255);

ALTER TABLE OBSERVE_LONGLINE.TDR ADD COLUMN BRANCHLINE character varying(255);
ALTER TABLE OBSERVE_LONGLINE.TDR ADD CONSTRAINT FK_TDR_BRANCHLINE FOREIGN KEY(BRANCHLINE) REFERENCES OBSERVE_LONGLINE.BRANCHLINE(topiaid);
ALTER TABLE OBSERVE_LONGLINE.BRANCHLINE DROP COLUMN TDR;
