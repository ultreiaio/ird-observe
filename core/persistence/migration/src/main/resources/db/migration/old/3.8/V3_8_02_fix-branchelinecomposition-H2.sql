---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE OBSERVE_LONGLINE.BRANCHLINESCOMPOSITION DROP CONSTRAINT FK_BRANCHLINESCOMPOSITION_BRANCHLINETYPE;
ALTER TABLE OBSERVE_LONGLINE.BRANCHLINESCOMPOSITION ALTER COLUMN BRANCHLINETYPE RENAME TO TRACELINETYPE;
ALTER TABLE OBSERVE_LONGLINE.BRANCHLINESCOMPOSITION ADD CONSTRAINT FK_BRANCHLINESCOMPOSITION_TRACELINETYPE FOREIGN KEY(TRACELINETYPE) REFERENCES OBSERVE_LONGLINE.LINETYPE(TOPIAID) CHECK;
--CREATE INDEX FK_BRANCHLINESCOMPOSITION_TRACELINETYPE_INDEX_D ON OBSERVE_LONGLINE.BRANCHLINESCOMPOSITION (TRACELINETYPE);
