---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
UPDATE OBSERVE_SEINE.gearusefeaturesmeasurement SET topiaid = replace(topiaid, 'GearUseFeaturesMeasurement#', 'GearUseFeaturesMeasurementSeine#');
--ALTER TABLE OBSERVE_SEINE.gearusefeaturesmeasurement DROP CONSTRAINT fk_gearusefeaturesmeasurement_gearusefeatures;
UPDATE OBSERVE_SEINE.gearusefeaturesmeasurement SET gearUseFeatures = replace(gearUseFeatures, 'GearUseFeatures#', 'GearUseFeaturesSeine#');
UPDATE OBSERVE_SEINE.gearusefeatures SET topiaid = replace(topiaid, 'GearUseFeatures#', 'GearUseFeaturesSeine#');
ALTER TABLE OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT ADD CONSTRAINT fk_gearusefeaturesmeasurement_gearusefeatures FOREIGN KEY(gearUseFeatures) REFERENCES OBSERVE_SEINE.GEARUSEFEATURES(topiaid);
