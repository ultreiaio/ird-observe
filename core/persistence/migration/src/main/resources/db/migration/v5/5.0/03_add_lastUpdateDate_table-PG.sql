---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
DROP TABLE IF EXISTS OBSERVE_COMMON.LASTUPDATEDATE;
CREATE TABLE OBSERVE_COMMON.LASTUPDATEDATE (TOPIAID VARCHAR(255) NOT NULL, TOPIAVERSION BIGINT NOT NULL, TOPIACREATEDATE TIMESTAMP NOT NULL, TYPE VARCHAR(255) NOT NULL UNIQUE, LASTUPDATEDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP);
ALTER TABLE OBSERVE_COMMON.LASTUPDATEDATE ADD CONSTRAINT PK_LASTUPDATEDATE PRIMARY KEY (TOPIAID);
DROP INDEX IF EXISTS  INDEX_OBSERVE_COMMON_LASTUPDATEDATE_LASTUPDATEDATE;
CREATE INDEX INDEX_OBSERVE_COMMON_LASTUPDATEDATE_LASTUPDATEDATE ON OBSERVE_COMMON.LASTUPDATEDATE (TYPE, LASTUPDATEDATE);

-- Referentiel commum
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.01', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.Country', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.02', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.FpaZone', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.03', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.Gear', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.04', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.GearCaracteristic', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.05', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.GearCaracteristicType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.06', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.Harbour', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.07', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.LengthWeightParameter', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.08', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.Ocean', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.09', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.Organism', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.10', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.Person', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.11', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.Program', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.12', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.Sex', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.13', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.Species', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.14', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.SpeciesGroup', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.15', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.SpeciesList', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.16', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.Vessel', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.17', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.VesselSizeCategory', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.18', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.VesselType', CURRENT_TIMESTAMP);
-- Référentiel Senne
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.19', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.DetectionMode', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.20', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.ObjectFate', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.21', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.ObjectOperation', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.22', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.ObjectType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.23', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.ObservedSystem', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.24', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.ReasonForDiscard', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.25', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.ReasonForNoFishing', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.26', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.ReasonForNullSet', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.27', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.SpeciesFate', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.28', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.SpeciesStatus', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.29', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.SurroundingActivity', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.30', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.TransmittingBuoyOperation', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.31', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.TransmittingBuoyType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.32', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.VesselActivitySeine', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.33', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.WeightCategory', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.34', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.seine.Wind', CURRENT_TIMESTAMP);
-- Référentiel Palangre
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.35', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.BaitHaulingStatus', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.36', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.BaitSettingStatus', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.37', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.BaitType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.38', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.CatchFateLongline', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.39', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.EncounterType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.40', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.Healthness', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.41', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.HookPosition', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.42', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.HookSize', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.43', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.HookType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.44', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.ItemHorizontalPosition', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.45', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.ItemVerticalPosition', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.46', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.LightsticksColor', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.47', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.LightsticksType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.48', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.LineType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.49', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.MaturityStatus', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.50', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.MitigationType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.51', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.SensorBrand', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.52', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.SensorDataFormat', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.53', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.SensorType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.54', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.SettingShape', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.55', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.SizeMeasureType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.56', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.StomacFullness', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.57', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.ObservationTripType', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.58', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.VesselActivityLongline', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.59', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.referentiel.longline.WeightMeasureType', CURRENT_TIMESTAMP);
-- Data Senne
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.60', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.ActivitySeine', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.61', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.FloatingObject', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.62', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.GearUsefeaturesMeasurementSeine', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.63', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.GearUsefeaturesSeine', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.64', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.NonTargetCatch', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.65', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.NonTargetLength', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.66', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.NonTargetSample', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.67', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.ObjectObservedSpecies', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.68', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.ObjectSchoolEstimate', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.69', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.Route', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.70', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.SchoolEstimate', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.71', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.SetSeine', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.72', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.TargetCatch', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.73', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.TargetLength', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.74', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.TargetSample', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.75', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.TransmittingBuoy', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.76', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.seine.TripSeine', CURRENT_TIMESTAMP);
-- Data Palangre
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.77', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.ActivityLonglineObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.78', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.BaitsCompositionObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.79', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.BasketObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.80', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.BranchlineObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.81', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.BranchlinesCompositionObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.82', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.CatchLonglineObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.83', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.EncounterObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.84', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.FloatlinesCompositionObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.85', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.GearUsefeaturesLongline', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.86', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.GearUsefeaturesMeasurementLongline', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.87', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.HooksCompositionObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.88', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.SectionObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.89', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.SensorUsedObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.90', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.SetLonglineObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.91', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.SizeMeasureObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.92', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.TdrObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.93', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.TdrRecordObsObs', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.94', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.TripLongline', CURRENT_TIMESTAMP);
INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.95', 0, CURRENT_TIMESTAMP, 'fr.ird.observe.entities.longline.WeightMeasureObsObs ', CURRENT_TIMESTAMP);
