---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

CREATE INDEX idx_activity_seine_gist ON observe_seine.activity USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_seine_activity_the_geom ON observe_seine.activity;
CREATE TRIGGER tr_sync_seine_activity_the_geom BEFORE insert or update ON observe_seine.activity FOR EACH ROW EXECUTE PROCEDURE sync_activity_the_geom();

CREATE INDEX idx_activity_longline_gist ON observe_longline.activity USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_longline_activity_the_geom ON observe_longline.activity;
CREATE TRIGGER tr_sync_longline_activity_the_geom BEFORE insert or update ON observe_longline.activity FOR EACH ROW EXECUTE PROCEDURE sync_activity_the_geom();


CREATE INDEX idx_harbour_gist ON observe_common.harbour USING GIST (the_geom);
DROP TRIGGER IF EXISTS tr_sync_harbour_the_geom ON observe_common.harbour;
CREATE TRIGGER tr_sync_harbour_the_geom BEFORE insert or update ON observe_common.harbour FOR EACH ROW EXECUTE PROCEDURE sync_harbour_the_geom();

