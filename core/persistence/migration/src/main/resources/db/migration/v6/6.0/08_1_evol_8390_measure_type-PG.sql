---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE observe_seine.targetLength DROP COLUMN sizeMeasureType CASCADE;
ALTER TABLE observe_seine.targetLength RENAME COLUMN sizeMeasureType2 TO sizeMeasureType;
ALTER TABLE observe_seine.targetlength ADD CONSTRAINT fk_targetLength_sizeMeasureType FOREIGN KEY(sizeMeasureType) REFERENCES observe_common.sizeMeasureType(topiaId);

ALTER TABLE observe_longline.sizeMeasure DROP COLUMN sizeMeasureType CASCADE;
ALTER TABLE observe_longline.sizeMeasure RENAME COLUMN sizeMeasureType2 TO sizeMeasureType;
ALTER TABLE observe_longline.sizeMeasure ADD CONSTRAINT fk_sizeMeasure_sizeMeasureType FOREIGN KEY(sizeMeasureType) REFERENCES observe_common.sizeMeasureType(topiaId);

ALTER TABLE observe_longline.weightMeasure DROP COLUMN weightMeasureType CASCADE;
ALTER TABLE observe_longline.weightMeasure RENAME COLUMN weightMeasureType2 TO weightMeasureType;
ALTER TABLE observe_longline.weightMeasure ADD CONSTRAINT fk_weightMeasure_weightMeasureType FOREIGN KEY(weightMeasureType) REFERENCES observe_common.weightMeasureType(topiaId);
