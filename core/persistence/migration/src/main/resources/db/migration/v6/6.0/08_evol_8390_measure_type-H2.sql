---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE TABLE observe_common.SIZEMEASURETYPE( topiaid VARCHAR(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code VARCHAR(255), status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri VARCHAR(255), label1 VARCHAR(255), label2 VARCHAR(255), label3 VARCHAR(255), label4 VARCHAR(255), label5 VARCHAR(255), label6 VARCHAR(255), label7 VARCHAR(255), label8 VARCHAR(255)) AS SELECT topiaId, topiaversion, topiacreatedate, lastupdatedate, code, status, needComment, uri, label1, label2, label3, label4, label5, label6, label7, label8 FROM OBSERVE_LONGLINE.SIZEMEASURETYPE;
ALTER TABLE observe_common.SIZEMEASURETYPE ADD CONSTRAINT PK_SIZEMEASURETYPE PRIMARY KEY(TOPIAID);

CREATE TABLE observe_common.WEIGHTMEASURETYPE( topiaid VARCHAR(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code VARCHAR(255), status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri VARCHAR(255), label1 VARCHAR(255), label2 VARCHAR(255), label3 VARCHAR(255), label4 VARCHAR(255), label5 VARCHAR(255), label6 VARCHAR(255), label7 VARCHAR(255), label8 VARCHAR(255)) AS SELECT topiaId, topiaversion, topiacreatedate, lastupdatedate, code, status, needComment, uri, label1, label2, label3, label4, label5, label6, label7, label8 FROM OBSERVE_LONGLINE.WEIGHTMEASURETYPE;
ALTER TABLE observe_common.WEIGHTMEASURETYPE ADD CONSTRAINT PK_WEIGHTMEASURETYPE PRIMARY KEY(TOPIAID);

UPDATE observe_common.LASTUPDATEDATE SET TYPE = 'fr.ird.observe.entities.referentiel.SizeMeasureType' WHERE TYPE = 'fr.ird.observe.entities.referentiel.longline.SizeMeasureType';
UPDATE observe_common.LASTUPDATEDATE SET TYPE = 'fr.ird.observe.entities.referentiel.WeightMeasureType' WHERE TYPE = 'fr.ird.observe.entities.referentiel.longline.WeightMeasureType';

ALTER TABLE observe_seine.targetlength ADD COLUMN sizeMeasureType VARCHAR(255);
UPDATE observe_seine.targetlength SET sizeMeasureType = NULL WHERE measureType = 0;
UPDATE observe_seine.targetlength SET sizeMeasureType = (select topiaid from observe_longline.sizemeasuretype where topiaid='fr.ird.observe.entities.referentiel.longline.SizeMeasureType#1433499466774#0.529249255312607')  WHERE measureType = 1;
UPDATE observe_seine.targetlength SET sizeMeasureType =(select topiaid from observe_longline.sizemeasuretype where topiaid ='fr.ird.observe.entities.referentiel.longline.SizeMeasureType#1433499465700#0.0902433863375336')  WHERE measureType = 2;
ALTER TABLE observe_seine.targetlength DROP COLUMN measureType;
ALTER TABLE observe_seine.targetlength ADD COLUMN sizeMeasureType2 VARCHAR(255);

ALTER TABLE observe_seine.nontargetlength ADD COLUMN sizeMeasureType VARCHAR(255);
ALTER TABLE observe_seine.nontargetlength ADD CONSTRAINT FK_NON_TARGET_LENGTH_SIZE_MEASURE_TYPE FOREIGN KEY(sizeMeasureType) REFERENCES OBSERVE_COMMON.sizemeasuretype(topiaid);

ALTER TABLE observe_longline.sizeMeasure ADD COLUMN sizeMeasureType2 VARCHAR(255);
ALTER TABLE observe_longline.weightMeasure ADD COLUMN weightMeasureType2 VARCHAR(255);

