---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE TABLE observe_common.shipOwner( topiaid VARCHAR(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code VARCHAR(255), status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri VARCHAR(255), label VARCHAR(255), country VARCHAR(255), startDate DATE, endDate DATE);
ALTER TABLE observe_common.shipOwner ADD CONSTRAINT PK_SHIP_OWNER PRIMARY KEY(topiaid);
ALTER TABLE observe_common.shipOwner ADD CONSTRAINT FK_SHIP_OWNER_COUNTRY FOREIGN KEY(country) REFERENCES observe_common.country(topiaid);
CREATE INDEX observe_common.INDEX_OBSERVE_COMMON_SHIP_OWNER_COUNTRY ON observe_common.shipOwner(country);

INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#0', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '1','SAUPIQUET', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#1', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '2','SAPMER', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#2', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '3','CFTO', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#3', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '4','IOSMS', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#4', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '5','TFC', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#5', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '6','ACF', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#6', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '7','CMB', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#7', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '8','PECHE ET FROID', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#8', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '9','IAT', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#9', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '10','LE BOUTER', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#10', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '11','ACF/CMB', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#11', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '12','COBRECAF', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#12', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '13','CHARLOT', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#13', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '14','HARTSWATER', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#14', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '15','PEVASA', 'fr.ird.observe.entities.referentiel.Country#1239832675584#0.0783072255559325', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#15', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '16','ECHEBASTAR', 'fr.ird.observe.entities.referentiel.Country#1239832675584#0.0783072255559325', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#16', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '17','ISABELLA FISHING', 'fr.ird.observe.entities.referentiel.Country#1239832675584#0.0783072255559325', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#17', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '18','ATUNSA', 'fr.ird.observe.entities.referentiel.Country#1239832675584#0.0783072255559325', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#18', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '19','ALBACORA', 'fr.ird.observe.entities.referentiel.Country#1239832675584#0.0783072255559325', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#19', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '20','DONWONG', 'fr.ird.observe.entities.referentiel.Country#1239832675585#0.7535948739996284', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#20', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '21','ENEZ DU', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#21', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '22','MINATCHY', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#22', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '23','PECHE AVENIR', 'fr.ird.observe.entities.referentiel.Country#1239832675583#0.9493110781716075', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#23', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '24','INPESCA', 'fr.ird.observe.entities.referentiel.Country#1239832675584#0.0783072255559325', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#24', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1 , '25','CALVO', 'fr.ird.observe.entities.referentiel.Country#1239832675584#0.0783072255559325', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, country, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#25', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '26','NICRA', 'fr.ird.observe.entities.referentiel.Country#1239832675584#0.0783072255559325', false);
INSERT INTO observe_common.shipOwner (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label, needComment) values ('fr.ird.observe.entities.referentiel.ShipOwner#0#26', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, '999','UND', true);

INSERT INTO observe_common.LASTUPDATEDATE (topiaId, topiaversion, topiacreatedate, TYPE , LASTUPDATEDATE) values ('fr.ird.observe.entities.LastUpdateDate#666#1', 0,CURRENT_DATE, 'fr.ird.observe.entities.referentiel.ShipOwner', CURRENT_TIMESTAMP);

ALTER TABLE observe_common.vessel ADD COLUMN shipOwner VARCHAR(255);
ALTER TABLE observe_common.vessel ADD CONSTRAINT FK_VESSEL_SHIP_OWNER FOREIGN KEY(shipOwner) REFERENCES observe_common.shipOwner(topiaid);
