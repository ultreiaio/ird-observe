---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE observe_common.lengthweightparameter ADD COLUMN sizeMeasureType VARCHAR(255);
ALTER TABLE observe_common.lengthweightparameter ADD CONSTRAINT FK_LENGTH_WEIGHT_PARAMETER_SIZE_MEASURE_TYPE FOREIGN KEY(sizeMeasureType) REFERENCES OBSERVE_COMMON.sizemeasuretype(topiaid);
UPDATE observe_common.lengthweightparameter SET sizeMeasureType = ( SELECT sizeMeasureType FROM observe_common.species s WHERE s.topiaId = species);

CREATE TABLE observe_common.lengthlengthparameter ( topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, inputSizeMeasureType VARCHAR(255) NOT NULL, outputSizeMeasureType VARCHAR(255) NOT NULL, topiacreatedate TIMESTAMP, uri VARCHAR(255), status INTEGER DEFAULT 1, coefficients VARCHAR(255), inputOutputFormula VARCHAR(255), outputInputFormula VARCHAR(255), code VARCHAR(255) DEFAULT 0, needcomment BOOLEAN DEFAULT false, lastupdatedate TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, source VARCHAR(1024));
ALTER TABLE observe_common.lengthlengthparameter ADD CONSTRAINT fk_lengthlengthparameter_inputSizeMeasureType FOREIGN KEY (inputSizeMeasureType) REFERENCES observe_common.sizeMeasureType (topiaid);
ALTER TABLE observe_common.lengthlengthparameter ADD CONSTRAINT fk_lengthlengthparameter_outputSizeMeasureType FOREIGN KEY (outputSizeMeasureType) REFERENCES observe_common.sizeMeasureType (topiaid);

CREATE INDEX index_observe_common_lengthlengthparameter_lastupdatedate ON observe_common.lengthlengthparameter (lastupdatedate);

INSERT INTO observe_common.LASTUPDATEDATE (topiaId, topiaversion, topiacreatedate, TYPE , LASTUPDATEDATE) values ('fr.ird.observe.entities.LastUpdateDate#666#2', 0,CURRENT_DATE, 'fr.ird.observe.entities.referentiel.LengthLengthParameter', CURRENT_TIMESTAMP);
