---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE observe_longline.basket ADD COLUMN set character varying(255);
UPDATE observe_longline.basket main SET set = (SELECT section.set FROM observe_longline.section section INNER JOIN observe_longline.basket basket ON basket.section = section.topiaid  WHERE basket.topiaid = main.topiaid);
ALTER TABLE observe_longline.basket ADD CONSTRAINT FK_BASKET_SET FOREIGN KEY(set) REFERENCES observe_longline.set(topiaid);
CREATE INDEX idx_observe_longline_basket_set ON OBSERVE_LONGLINE.basket(set);
