---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE observe_seine.floatingObject ADD COLUMN objectOperation2 CHARACTER VARYING (256);

UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#1'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686248#0.8669327599318251';

UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#5'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.7838704130950722' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683674#0.32469201752917276';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#4'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.7838704130950722' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.7559688295127481';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#6'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.7838704130950722' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.88526017739943';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#7'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.7838704130950722' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.190123844350496';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#8'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.7838704130950722' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1396860761530#0.8869464242156488';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#4'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.7838704130950722' AND objectFate not in ('fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683674#0.32469201752917276','fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.7559688295127481','fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.88526017739943','fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.190123844350496', 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1396860761530#0.8869464242156488');

UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#5'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8268884472438458' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683674#0.32469201752917276';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#4'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8268884472438458' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.7559688295127481';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#6'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8268884472438458' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.88526017739943';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#7'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8268884472438458' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.190123844350496';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#8'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8268884472438458' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1396860761530#0.8869464242156488';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#2'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8268884472438458' AND objectFate not in ('fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683674#0.32469201752917276','fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.7559688295127481','fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.88526017739943','fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.190123844350496', 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1396860761530#0.8869464242156488');

UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#5'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8431519556575698' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683674#0.32469201752917276';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#4'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8431519556575698' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.7559688295127481';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#6'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8431519556575698' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.88526017739943';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#7'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8431519556575698' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.190123844350496';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#8'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8431519556575698' AND objectFate = 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1396860761530#0.8869464242156488';
UPDATE observe_seine.floatingObject set objectOperation2 = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#2'  WHERE objectOperation = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation#1239832686249#0.8431519556575698' AND objectFate not in ('fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683674#0.32469201752917276','fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.7559688295127481','fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.88526017739943','fr.ird.observe.entities.referentiel.seine.ObjectFate#1239832683675#0.190123844350496', 'fr.ird.observe.entities.referentiel.seine.ObjectFate#1396860761530#0.8869464242156488');

alter table observe_seine.floatingObject drop COLUMN objectOperation CASCADE;
alter table observe_seine.floatingObject drop COLUMN objectFate CASCADE;
drop table observe_seine.objectFate CASCADE;

delete from observe_seine.objectOperation;

alter table observe_seine.objectOperation ADD COLUMN whenArriving BOOLEAN DEFAULT TRUE;
alter table observe_seine.objectOperation ADD COLUMN whenLeaving BOOLEAN DEFAULT TRUE;

INSERT INTO observe_seine.objectOperation (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label1, label2, label3, needComment, whenArriving, whenLeaving) values ('fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#1' , 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '1' ,'Deployment', 'Mise à l''eau', 'Mise à l''eau TODO', false, false, true);
INSERT INTO observe_seine.objectOperation (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label1, label2, label3, needComment, whenArriving, whenLeaving) values ('fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#2' , 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '2' ,'Visit', 'Visite', 'Visite TODO', false, true, false);
INSERT INTO observe_seine.objectOperation (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label1, label2, label3, needComment, whenArriving, whenLeaving) values ('fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#3' , 0, CURRENT_DATE, CURRENT_TIMESTAMP,  0, '3' ,'Fishing', 'Pêche', 'Pêche TODO', true, false, false);
INSERT INTO observe_seine.objectOperation (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label1, label2, label3, needComment, whenArriving, whenLeaving) values ('fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#4' , 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '4' ,'Removal', 'Retrait', 'Retrait TODO', false, true, false);
INSERT INTO observe_seine.objectOperation (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label1, label2, label3, needComment, whenArriving, whenLeaving) values ('fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#5' , 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '5' ,'Abandonned (without tag, not destroyed, not sunk)', 'Abandonné (sans balise, non détruit, non coulé)', 'Abandonné (sans balise, non détruit, non coulé) TODO', false, true, true);
INSERT INTO observe_seine.objectOperation (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label1, label2, label3, needComment, whenArriving, whenLeaving) values ('fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#6' , 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '6' ,'Destroyed', 'Détruit', 'Détruit TODO', false, true, false);
INSERT INTO observe_seine.objectOperation (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label1, label2, label3, needComment, whenArriving, whenLeaving) values ('fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#7' , 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '7' ,'Sunk', 'Coulé', 'Coulé TODO', false, true, false);
INSERT INTO observe_seine.objectOperation (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label1, label2, label3, needComment, whenArriving, whenLeaving) values ('fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#8' , 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '8' ,'Modified or reinforced', 'Modification ou renforcement', 'Modification ou renforcement TODO', false, true, true);
INSERT INTO observe_seine.objectOperation (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label1, label2, label3, needComment, whenArriving, whenLeaving) values ('fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#9' , 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '9' ,'Replacement', 'Remplacement', 'Remplacement TODO', false, true, true);
INSERT INTO observe_seine.objectOperation (topiaid, topiaversion, topiacreatedate, lastupdatedate, status, code, label1, label2, label3, needComment, whenArriving, whenLeaving) values ('fr.ird.observe.entities.referentiel.seine.ObjectOperation#0#10', 0, CURRENT_DATE, CURRENT_TIMESTAMP,  1, '10','Other (to be precised)', 'Autre (préciser dans les notes)', 'Autre (préciser dans les notes) TODO', true, true, true);
UPDATE OBSERVE_COMMON.LASTUPDATEDATE SET  LASTUPDATEDATE = CURRENT_TIMESTAMP WHERE type = 'fr.ird.observe.entities.referentiel.seine.ObjectOperation';

ALTER TABLE observe_seine.floatingObject RENAME objectOperation2 TO objectOperation;

ALTER TABLE observe_seine.floatingObject ADD CONSTRAINT fk_floatingobject_objectOperation FOREIGN KEY (objectOperation) REFERENCES observe_seine.objectOperation(topiaid);

