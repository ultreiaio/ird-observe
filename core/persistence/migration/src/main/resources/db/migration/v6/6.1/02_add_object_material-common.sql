---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

CREATE TABLE observe_seine.objectMaterialType( topiaid VARCHAR(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code VARCHAR(255) NOT NULL, status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri VARCHAR(255), label1 VARCHAR(255), label2 VARCHAR(255), label3 VARCHAR(255),label4 VARCHAR(255),label5 VARCHAR(255),label6 VARCHAR(255),label7 VARCHAR(255),label8 VARCHAR(255));
ALTER TABLE observe_seine.objectMaterialType ADD CONSTRAINT PK_OBJECT_MATERIAL_TYPE PRIMARY KEY(topiaid);
INSERT INTO observe_common.LASTUPDATEDATE (topiaId, topiaversion, topiacreatedate, TYPE , LASTUPDATEDATE) values ('fr.ird.observe.entities.LastUpdateDate#666#900', 0,CURRENT_DATE, 'fr.ird.observe.entities.referentiel.seine.ObjectMaterialType', CURRENT_TIMESTAMP);

INSERT INTO observe_seine.objectMaterialType(topiaid, topiaversion, topiacreatedate, lastupdatedate, needComment, status, code, label1, label2) values ('fr.ird.observe.entities.referentiel.seine.ObjectMaterialType#0#0', 0, CURRENT_DATE, CURRENT_TIMESTAMP, false,  1, '1', 'Boolean', 'Boolean');
INSERT INTO observe_seine.objectMaterialType(topiaid, topiaversion, topiacreatedate, lastupdatedate, needComment, status, code, label1, label2) values ('fr.ird.observe.entities.referentiel.seine.ObjectMaterialType#0#1', 0, CURRENT_DATE, CURRENT_TIMESTAMP, false,  1, '2', 'Decimal one digit', 'Nombre décimal (précision une déciamle)');
INSERT INTO observe_seine.objectMaterialType(topiaid, topiaversion, topiacreatedate, lastupdatedate, needComment, status, code, label1, label2) values ('fr.ird.observe.entities.referentiel.seine.ObjectMaterialType#0#2', 0, CURRENT_DATE, CURRENT_TIMESTAMP, false,  1, '3', 'Integer', 'Entier');
INSERT INTO observe_seine.objectMaterialType(topiaid, topiaversion, topiacreatedate, lastupdatedate, needComment, status, code, label1, label2) values ('fr.ird.observe.entities.referentiel.seine.ObjectMaterialType#0#3', 0, CURRENT_DATE, CURRENT_TIMESTAMP, false,  1, '4', 'Text', 'Texte');
UPDATE observe_seine.objectMaterialType set label3 = label2 || ' TODO';

CREATE TABLE observe_seine.objectMaterial( topiaid VARCHAR(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code VARCHAR(255) NOT NULL, legacyCode VARCHAR(1024), standardCode VARCHAR(255), parent VARCHAR(255), status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri VARCHAR(255), objectMaterialType VARCHAR(255), biodegradable BOOLEAN, nonEntangling BOOLEAN, label1 VARCHAR(255), label2 VARCHAR(255), label3 VARCHAR(255),label4 VARCHAR(255),label5 VARCHAR(255),label6 VARCHAR(255),label7 VARCHAR(255),label8 VARCHAR(255), selectable boolean ,childrenMultiSelectable boolean,childSelectionMandatory boolean, validation VARCHAR(255));
ALTER TABLE observe_seine.objectMaterial ADD CONSTRAINT PK_OBJECT_MATERIAL PRIMARY KEY(topiaid);
ALTER TABLE observe_seine.objectMaterial ADD CONSTRAINT FK_OBJECT_MATERIAL_PARENT FOREIGN KEY (parent) REFERENCES observe_seine.objectMaterial(topiaid);
ALTER TABLE observe_seine.objectMaterial ADD CONSTRAINT FK_OBJECT_MATERIAL_OBJECT_MATERIAL_TYPE FOREIGN KEY (objectMaterialType) REFERENCES observe_seine.objectMaterialType(topiaid);
INSERT INTO observe_common.LASTUPDATEDATE (topiaId, topiaversion, topiacreatedate, TYPE , LASTUPDATEDATE) values ('fr.ird.observe.entities.LastUpdateDate#666#901', 0,CURRENT_DATE, 'fr.ird.observe.entities.referentiel.seine.ObjectMaterial', CURRENT_TIMESTAMP);
