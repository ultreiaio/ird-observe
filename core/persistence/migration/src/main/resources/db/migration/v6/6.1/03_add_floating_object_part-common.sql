---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE TABLE observe_seine.FloatingObjectPart( topiaid VARCHAR(255) NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, objectMaterial VARCHAR(255) NOT NULL, floatingObject VARCHAR(255) NOT NULL, whenArriving VARCHAR(255), whenLeaving VARCHAR(255));
ALTER TABLE observe_seine.FloatingObjectPart ADD CONSTRAINT PK_FLOATING_OBJECT_PART PRIMARY KEY(topiaid);
ALTER TABLE observe_seine.FloatingObjectPart ADD CONSTRAINT FK_FLOATING_OBJECT_PART_OBJECT_MATERIAL FOREIGN KEY (objectMaterial) REFERENCES observe_seine.ObjectMaterial(topiaid);
ALTER TABLE observe_seine.FloatingObjectPart ADD CONSTRAINT FK_FLOATING_OBJECT_PART_FLOATING_OBJECT FOREIGN KEY (floatingObject) REFERENCES observe_seine.FloatingObject(topiaid);
