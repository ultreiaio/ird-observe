---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE TABLE observe_common.SpeciesGroupReleaseMode(topiaid VARCHAR(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code VARCHAR(255), status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri VARCHAR(255), label1 VARCHAR(255), label2 VARCHAR(255), label3 VARCHAR(255),label4 VARCHAR(255),label5 VARCHAR(255),label6 VARCHAR(255),label7 VARCHAR(255),label8 VARCHAR(255));
ALTER TABLE observe_common.SpeciesGroupReleaseMode ADD CONSTRAINT PK_SPECIES_GROUP_RELEASE_MODE PRIMARY KEY(topiaid);
INSERT INTO observe_common.LASTUPDATEDATE(topiaId, topiaversion, topiacreatedate, TYPE , LASTUPDATEDATE) values ('fr.ird.observe.entities.LastUpdateDate#666#902', 0,CURRENT_DATE, 'fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode', CURRENT_TIMESTAMP);

INSERT INTO observe_common.SpeciesGroupReleaseMode(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2) values ('fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#1' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, null,'par salabarde');
INSERT INTO observe_common.SpeciesGroupReleaseMode(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2) values ('fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#2' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, null,'par civière, bâche, sarria, filet de cargo');
INSERT INTO observe_common.SpeciesGroupReleaseMode(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2) values ('fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#3' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, null,'avec équipement spécifique');
INSERT INTO observe_common.SpeciesGroupReleaseMode(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2) values ('fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#4' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, null,'à la main depuis le pont');
INSERT INTO observe_common.SpeciesGroupReleaseMode(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2) values ('fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#5' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, null,'après démaillage');
INSERT INTO observe_common.SpeciesGroupReleaseMode(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2) values ('fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#6' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 0, true,  null,'non conforme');
INSERT INTO observe_common.SpeciesGroupReleaseMode(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2) values ('fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#7' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, null,'Submersion des flotteurs');
INSERT INTO observe_common.SpeciesGroupReleaseMode(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2) values ('fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#8' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, null,'Entaille dans le filet');
INSERT INTO observe_common.SpeciesGroupReleaseMode(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2) values ('fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#9' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, null,'une fois retiré des restes de plastique');
INSERT INTO observe_common.SpeciesGroupReleaseMode(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2) values ('fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#10', 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, null,'jour à bord');

UPDATE observe_common.SpeciesGroupReleaseMode set label1 = label2 || ' TODO', label3 = label2 || ' TODO';

CREATE TABLE observe_common.SpeciesGroup_SpeciesGroupReleaseMode ( SpeciesGroup varchar(255) NOT NULL, SpeciesGroupReleaseMode varchar(255) NOT NULL, CONSTRAINT FK_SpeciesGroup_SpeciesGroupReleaseMode_SpeciesGroup FOREIGN KEY (SpeciesGroup) REFERENCES observe_common.SpeciesGroup (TOPIAID), CONSTRAINT FK_SpeciesGroup_SpeciesGroupReleaseMode_SpeciesGroupReleaseMode FOREIGN KEY (SpeciesGroupReleaseMode) REFERENCES observe_common.SpeciesGroupReleaseMode (TOPIAID) );

-- requin
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES ('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#1');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#2');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#3');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#4');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#5');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#6');

-- requin baleine
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES ('fr.ird.observe.entities.referentiel.SpeciesGroup#1446014286433#0.6480183366605247','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#1');;
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1446014286433#0.6480183366605247','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#6');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1446014286433#0.6480183366605247','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#7');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1446014286433#0.6480183366605247','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#8');

-- raie
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES ('fr.ird.observe.entities.referentiel.SpeciesGroup#1445863056144#0.9820877553253712','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#1');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1445863056144#0.9820877553253712','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#2');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1445863056144#0.9820877553253712','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#3');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1445863056144#0.9820877553253712','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#4');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1445863056144#0.9820877553253712','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#6');

-- tortue
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES ('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.24333033683679461','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#4');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.24333033683679461','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#5');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.24333033683679461','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#6');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.24333033683679461','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#9');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup,SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.24333033683679461','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#10');

