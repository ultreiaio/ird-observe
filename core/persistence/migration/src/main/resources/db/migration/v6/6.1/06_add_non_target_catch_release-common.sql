---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- drop table observe_seine.NonTargetCatchRelease CASCADE ;

CREATE TABLE observe_seine.NonTargetCatchRelease(topiaid VARCHAR(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, status INTEGER, length REAL , count INTEGER, acquisitionMode int, detectionTime TIMESTAMP , releaseTime TIMESTAMP, comment VARCHAR(1024), species VARCHAR(255), sex VARCHAR(255), speciesGroupReleaseMode VARCHAR(255), SET VARCHAR(255),SET_IDX integer, CONSTRAINT FK_NON_TARGET_CATCH_RELEASED_SPECIES FOREIGN KEY (SPECIES) REFERENCES observe_common.SPECIES(TOPIAID), CONSTRAINT FK_NON_TARGET_CATCH_RELEASED_SEX FOREIGN KEY (SEX) REFERENCES observe_common.SEX(TOPIAID), CONSTRAINT FK_NON_TARGET_CATCH_RELEASED_SPECIES_GROUP_RELEASE_MODE FOREIGN KEY (speciesGroupReleaseMode) REFERENCES observe_common.speciesGroupReleaseMode(TOPIAID), CONSTRAINT FK_NON_TargetCatchRelease_SET FOREIGN KEY (SET) REFERENCES observe_seine.set(TOPIAID));
ALTER TABLE observe_seine.NonTargetCatchRelease ADD CONSTRAINT PK_NON_TARGET_CATCH_RELEASE PRIMARY KEY(topiaid);

INSERT INTO observe_common.LASTUPDATEDATE(topiaId, topiaversion, topiacreatedate, TYPE , LASTUPDATEDATE) values ('fr.ird.observe.entities.LastUpdateDate#666#904', 0,CURRENT_DATE, 'fr.ird.observe.entities.seine.NonTargetCatchRelease', CURRENT_TIMESTAMP);
