---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE TABLE observe_common.DataQuality(topiaid VARCHAR(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code VARCHAR(255), status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri VARCHAR(255), label1 VARCHAR(255), label2 VARCHAR(255), label3 VARCHAR(255),label4 VARCHAR(255),label5 VARCHAR(255),label6 VARCHAR(255),label7 VARCHAR(255),label8 VARCHAR(255));
ALTER TABLE observe_common.DataQuality ADD CONSTRAINT PK_DataQuality PRIMARY KEY(topiaid);
INSERT INTO observe_common.LASTUPDATEDATE(topiaId, topiaversion, topiacreatedate, TYPE , LASTUPDATEDATE) values ('fr.ird.observe.entities.LastUpdateDate#666#1000', 0,CURRENT_DATE, 'fr.ird.observe.entities.referentiel.DataQuality', CURRENT_TIMESTAMP);

INSERT INTO observe_common.DataQuality(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label1, label2) values ('fr.ird.observe.entities.referentiel.DataQuality#0#1' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'A','Very good', 'Très bonne');
INSERT INTO observe_common.DataQuality(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label1, label2) values ('fr.ird.observe.entities.referentiel.DataQuality#0#2' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'B','Medium', 'Moyenne');
INSERT INTO observe_common.DataQuality(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label1, label2) values ('fr.ird.observe.entities.referentiel.DataQuality#0#3' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'C','Doubtful', 'Douteuse');
INSERT INTO observe_common.DataQuality(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label1, label2) values ('fr.ird.observe.entities.referentiel.DataQuality#0#4' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'D','Bad', 'Mauvaise');
INSERT INTO observe_common.DataQuality(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label1, label2) values ('fr.ird.observe.entities.referentiel.DataQuality#0#5' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'E','unknown', 'Inconnue');

UPDATE observe_common.DataQuality set label3 = label2 || ' TODO';

ALTER TABLE observe_seine.trip ADD COLUMN dataQuality VARCHAR(255);
ALTER TABLE observe_seine.trip ADD CONSTRAINT fk_trip_data_quality FOREIGN KEY (dataQuality) REFERENCES observe_common.DataQuality(topiaid);
UPDATE observe_seine.trip set dataQuality = 'fr.ird.observe.entities.referentiel.DataQuality#0#1';

ALTER TABLE observe_seine.activity ADD COLUMN dataQuality VARCHAR(255);
ALTER TABLE observe_seine.activity ADD CONSTRAINT fk_activity_data_quality FOREIGN KEY (dataQuality) REFERENCES observe_common.DataQuality(topiaid);
UPDATE observe_seine.activity set dataQuality = 'fr.ird.observe.entities.referentiel.DataQuality#0#1';

ALTER TABLE observe_longline.trip ADD COLUMN dataQuality VARCHAR(255);
ALTER TABLE observe_longline.trip ADD CONSTRAINT fk_trip_data_quality FOREIGN KEY (dataQuality) REFERENCES observe_common.DataQuality(topiaid);
UPDATE observe_longline.trip set dataQuality = 'fr.ird.observe.entities.referentiel.DataQuality#0#1';

ALTER TABLE observe_longline.activity ADD COLUMN dataQuality VARCHAR(255);
ALTER TABLE observe_longline.activity ADD CONSTRAINT fk_activity_data_quality FOREIGN KEY (dataQuality) REFERENCES observe_common.DataQuality(topiaid);
UPDATE observe_longline.activity set dataQuality = 'fr.ird.observe.entities.referentiel.DataQuality#0#1';

