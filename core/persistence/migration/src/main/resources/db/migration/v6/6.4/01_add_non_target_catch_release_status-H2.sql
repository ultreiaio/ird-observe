---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE TABLE observe_seine.NonTargetCatchReleaseStatus(topiaid VARCHAR(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code VARCHAR(255), status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri VARCHAR(255), label1 VARCHAR(255), label2 VARCHAR(255), label3 VARCHAR(255),label4 VARCHAR(255),label5 VARCHAR(255),label6 VARCHAR(255),label7 VARCHAR(255),label8 VARCHAR(255));
ALTER TABLE observe_seine.NonTargetCatchReleaseStatus ADD CONSTRAINT PK_NonTargetCatchReleaseStatus PRIMARY KEY(topiaid);
INSERT INTO observe_common.LASTUPDATEDATE(topiaId, topiaversion, topiacreatedate, TYPE , LASTUPDATEDATE) values ('fr.ird.observe.entities.LastUpdateDate#666#1001', 0,CURRENT_DATE, 'fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus', CURRENT_TIMESTAMP);

INSERT INTO observe_seine.NonTargetCatchReleaseStatus(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2, label1) VALUES ('fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#1' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'P','Parfait', 'Perfect');
INSERT INTO observe_seine.NonTargetCatchReleaseStatus(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2, label1) VALUES ('fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#2' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'M','Modéré', 'Moderate');
INSERT INTO observe_seine.NonTargetCatchReleaseStatus(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2, label1) VALUES ('fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#3' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'S','Sévère', 'Servere');
INSERT INTO observe_seine.NonTargetCatchReleaseStatus(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2, label1) VALUES ('fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#4' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'U','Inconnu', 'Unknown');

UPDATE observe_seine.NonTargetCatchReleaseStatus SET label3 = label2 || ' TODO';

ALTER TABLE observe_seine.NonTargetCatchRelease add column status2 VARCHAR(255);
ALTER TABLE observe_seine.NonTargetCatchRelease ADD CONSTRAINT fk_NonTargetCatchRelease_status FOREIGN KEY (status2) REFERENCES observe_seine.NonTargetCatchReleaseStatus(topiaid);
UPDATE observe_seine.NonTargetCatchRelease SET status2 = 'fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#1' WHERE status = 0;
UPDATE observe_seine.NonTargetCatchRelease SET status2 = 'fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#2' WHERE status = 1;
UPDATE observe_seine.NonTargetCatchRelease SET status2 = 'fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#3' WHERE status = 2;
UPDATE observe_seine.NonTargetCatchRelease SET status2 = 'fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#4' WHERE status = 3;

ALTER TABLE observe_seine.NonTargetCatchRelease DROP COLUMN status;
ALTER TABLE observe_seine.NonTargetCatchRelease alter COLUMN status2 RENAME TO status;
ALTER TABLE observe_seine.NonTargetCatchRelease ALTER COLUMN status SET NOT NULL;

