---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE TABLE observe_seine.NonTargetCatchReleaseConformity(topiaid VARCHAR(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code VARCHAR(255), status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri VARCHAR(255), label1 VARCHAR(255), label2 VARCHAR(255), label3 VARCHAR(255),label4 VARCHAR(255),label5 VARCHAR(255),label6 VARCHAR(255),label7 VARCHAR(255),label8 VARCHAR(255));
ALTER TABLE observe_seine.NonTargetCatchReleaseConformity ADD CONSTRAINT PK_NonTargetCatchReleaseConformity PRIMARY KEY(topiaid);
INSERT INTO observe_common.LASTUPDATEDATE(topiaId, topiaversion, topiacreatedate, TYPE , LASTUPDATEDATE) values ('fr.ird.observe.entities.LastUpdateDate#666#1002', 0,CURRENT_DATE, 'fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseConformity', CURRENT_TIMESTAMP);

INSERT INTO observe_seine.NonTargetCatchReleaseConformity(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2, label1, label3) VALUES ('fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseConformity#0#1' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'C','Conforme','Conform','Conforme');
INSERT INTO observe_seine.NonTargetCatchReleaseConformity(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2, label1, label3) VALUES ('fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseConformity#0#2' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'NC-RI','Non conforme RI - Mortalité résiduelle inévitable','Not conform RI - Residual unavoidable mortality','No conforme RI - Mortalidad residual inevitable');
INSERT INTO observe_seine.NonTargetCatchReleaseConformity(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2, label1, label3) VALUES ('fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseConformity#0#3' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'NC-M','Non conforme M - Manque de matériel' , 'Not conform M - Lack of material' , 'No conforme M - Falta de material');
INSERT INTO observe_seine.NonTargetCatchReleaseConformity(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2, label1, label3) VALUES ('fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseConformity#0#4' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'NC-NC','Non conforme NC - Non respecté' , 'Not conform NC - Not complying' , 'No conforme NC - No se cumple');
INSERT INTO observe_seine.NonTargetCatchReleaseConformity(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label2, label1, label3) VALUES ('fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseConformity#0#5' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'U','Inconnue' , 'Unknown' , 'Desconocido');

ALTER TABLE observe_seine.NonTargetCatchRelease add column conformity VARCHAR(255);
ALTER TABLE observe_seine.NonTargetCatchRelease ADD CONSTRAINT fk_NonTargetCatchRelease_conformity FOREIGN KEY (conformity) REFERENCES observe_seine.NonTargetCatchReleaseConformity(topiaid);

