---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

INSERT INTO observe_seine.NonTargetCatchReleaseStatus(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label1, label2, label3) VALUES ('fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#5' , 0, CURRENT_DATE, CURRENT_TIMESTAMP, 1, false, 'I','Unacceptable', 'Inacceptable', 'Inaceptable');

UPDATE observe_seine.NonTargetCatchReleaseStatus set label1='Excellent', label2='Excellent', label3='Excelente', code='E', topiaversion = topiaversion + 1  where topiaid='fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#1';
UPDATE observe_seine.NonTargetCatchReleaseStatus set label1='Good', label2='Bon', label3='Bueno', code='B' , topiaversion = topiaversion + 1 where topiaid='fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#2';
UPDATE observe_seine.NonTargetCatchReleaseStatus set label1='Poor', label2='Mauvais', label3='Malo', code='M', topiaversion = topiaversion + 1 where TOPIAID='fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#3';
UPDATE observe_seine.NonTargetCatchReleaseStatus set label1='Unknown', label2='Inconnu', label3='Desconocido' , topiaversion = topiaversion + 1 where TOPIAID='fr.ird.observe.entities.referentiel.seine.NonTargetCatchReleaseStatus#0#4';
