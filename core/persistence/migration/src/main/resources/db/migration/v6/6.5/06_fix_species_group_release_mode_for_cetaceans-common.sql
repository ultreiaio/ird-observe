---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup, SpeciesGroupReleaseMode) VALUES ('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.9204972827240977','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#1');;
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup, SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.9204972827240977','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#7');
INSERT INTO observe_common.SpeciesGroup_SpeciesGroupReleaseMode(SpeciesGroup, SpeciesGroupReleaseMode) VALUES('fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683690#0.9204972827240977','fr.ird.observe.entities.referentiel.SpeciesGroupReleaseMode#0#8');

