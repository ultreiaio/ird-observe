---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

UPDATE observe_seine.observedsystem SET schoolType = 1 WHERE topiaId = 'fr.ird.observe.entities.referentiel.seine.ObservedSystem#0#1.0';
UPDATE observe_seine.observedsystem SET schoolType = 1 WHERE topiaId = 'fr.ird.observe.entities.referentiel.seine.ObservedSystem#0#1.1';
UPDATE observe_seine.observedsystem SET schoolType = 1 WHERE topiaId = 'fr.ird.observe.entities.referentiel.seine.ObservedSystem#0#1.2';
UPDATE observe_seine.observedsystem SET status = 0 WHERE code = '5';
UPDATE observe_seine.observedsystem SET status = 0 WHERE code = '6';
UPDATE observe_seine.observedsystem SET status = 0 WHERE code = '7';
UPDATE observe_seine.observedsystem SET status = 0 WHERE code = '8';
UPDATE observe_seine.observedsystem SET status = 0 WHERE code = '12';
DELETE FROM observe_seine.objectmaterial WHERE topiaid = 'fr.ird.observe.entities.referentiel.seine.ObjectMaterial#0#0.58';
