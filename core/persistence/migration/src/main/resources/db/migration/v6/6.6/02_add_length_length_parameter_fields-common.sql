---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

ALTER TABLE observe_common.lengthlengthparameter ADD COLUMN species VARCHAR(255);
ALTER TABLE observe_common.lengthlengthparameter ADD CONSTRAINT fk_lengthlengthparameter_species FOREIGN KEY (species) REFERENCES observe_common.species(topiaid);
CREATE INDEX idx_observe_common_lengthlengthparameter_species ON observe_common.lengthlengthparameter(species);

ALTER TABLE observe_common.lengthlengthparameter ADD COLUMN ocean VARCHAR(255);
ALTER TABLE observe_common.lengthlengthparameter ADD CONSTRAINT fk_lengthlengthparameter_ocean FOREIGN KEY (ocean) REFERENCES observe_common.ocean(topiaid);
CREATE INDEX idx_observe_common_lengthlengthparameter_ocean ON observe_common.lengthlengthparameter(ocean);

ALTER TABLE observe_common.lengthlengthparameter ADD COLUMN sex VARCHAR(255);
ALTER TABLE observe_common.lengthlengthparameter ADD CONSTRAINT fk_lengthlengthparameter_sex FOREIGN KEY (sex) REFERENCES observe_common.sex(topiaid);
CREATE INDEX idx_observe_common_lengthlengthparameter_sex ON observe_common.lengthlengthparameter(sex);

ALTER TABLE observe_common.lengthlengthparameter ADD COLUMN startDate DATE;
ALTER TABLE observe_common.lengthlengthparameter ADD COLUMN endDate DATE;
