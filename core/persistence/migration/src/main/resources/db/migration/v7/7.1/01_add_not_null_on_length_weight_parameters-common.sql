---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
DELETE FROM observe_common.LengthWeightParameter WHERE topiaId = 'fr.ird.observe.entities.referentiel.LengthWeightParameter#201008191245#0.299';
DELETE FROM observe_common.LengthWeightParameter WHERE topiaId = 'fr.ird.observe.entities.referentiel.LengthWeightParameter#201008191245#0.158';
DELETE FROM observe_common.LengthWeightParameter WHERE topiaId = 'fr.ird.observe.entities.referentiel.LengthWeightParameter#201008191245#0.157';
DELETE FROM observe_common.LengthWeightParameter WHERE topiaId = 'fr.ird.observe.entities.referentiel.LengthWeightParameter#201008191245#0.298';
UPDATE observe_common.LengthWeightParameter SET sizeMeasureType = 'fr.ird.observe.entities.referentiel.SizeMeasureType#1433499465700#0.0902433863375336' WHERE sizeMeasureType IS NULL;
UPDATE observe_common.LengthWeightParameter SET coefficients = 'a=0.5:b=0.0' WHERE coefficients IS NULL;

ALTER TABLE observe_common.LengthWeightParameter ALTER COLUMN sizeMeasureType SET NOT NULL;
ALTER TABLE observe_common.LengthWeightParameter ALTER COLUMN lengthWeightFormula SET NOT NULL;
ALTER TABLE observe_common.LengthWeightParameter ALTER COLUMN weightLengthFormula SET NOT NULL;
ALTER TABLE observe_common.LengthWeightParameter ALTER COLUMN coefficients SET NOT NULL;
ALTER TABLE observe_common.LengthWeightParameter ALTER COLUMN ocean SET NOT NULL;
ALTER TABLE observe_common.LengthWeightParameter ALTER COLUMN sex SET NOT NULL;
ALTER TABLE observe_common.LengthWeightParameter ALTER COLUMN species SET NOT NULL;
