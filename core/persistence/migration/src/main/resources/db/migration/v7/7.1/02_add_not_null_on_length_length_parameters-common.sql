---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

ALTER TABLE observe_common.LengthLengthParameter ALTER COLUMN inputOutputFormula SET NOT NULL;
ALTER TABLE observe_common.LengthLengthParameter ALTER COLUMN outputInputFormula SET NOT NULL;
ALTER TABLE observe_common.LengthLengthParameter ALTER COLUMN inputSizeMeasureType SET NOT NULL;
ALTER TABLE observe_common.LengthLengthParameter ALTER COLUMN outputSizeMeasureType SET NOT NULL;
ALTER TABLE observe_common.LengthLengthParameter ALTER COLUMN coefficients SET NOT NULL;
ALTER TABLE observe_common.LengthLengthParameter ALTER COLUMN species SET NOT NULL;
ALTER TABLE observe_common.LengthLengthParameter ALTER COLUMN ocean SET NOT NULL;
ALTER TABLE observe_common.LengthLengthParameter ALTER COLUMN sex SET NOT NULL;
