---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE observe_longline.Activity ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.BaitsComposition ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.Basket ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.Branchline ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.BranchlinesComposition ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.Catch ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.Encounter ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.FloatlinesComposition ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.GearUseFeatures ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.GearUseFeaturesMeasurement ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.HooksComposition ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.Section ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.SensorUsed ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.Set ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.SizeMeasure ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.Tdr ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.TdrRecord ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.Trip ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.WeightMeasure ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.Country ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.DataQuality ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.FpaZone ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.Gear ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.GearCaracteristic ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.GearCaracteristicType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.Harbour ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.LastUpdateDate ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.LengthLengthParameter ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.LengthWeightParameter ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.Ocean ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.Organism ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.Person ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.Program ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.Sex ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.ShipOwner ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.SizeMeasureType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.Species ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.SpeciesGroup ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.SpeciesGroupReleaseMode ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.SpeciesList ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.Vessel ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.VesselSizeCategory ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.VesselType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_common.WeightMeasureType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.BaitHaulingStatus ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.BaitSettingStatus ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.BaitType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.CatchFate ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.EncounterType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.Healthness ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.HookPosition ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.HookSize ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.HookType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.ItemHorizontalPosition ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.ItemVerticalPosition ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.LightsticksColor ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.LightsticksType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.LineType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.MaturityStatus ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.MitigationType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.SensorBrand ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.SensorDataFormat ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.SensorType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.SettingShape ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.StomacFullness ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.TripType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_longline.VesselActivity ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.DetectionMode ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.NonTargetCatchReleaseConformity ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.NonTargetCatchReleaseStatus ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.NonTargetCatchReleasingTime ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.ObjectMaterial ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.ObjectMaterialType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.ObjectOperation ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.ObservedSystem ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.ReasonForDiscard ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.ReasonForNoFishing ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.ReasonForNullSet ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.SpeciesFate ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.SpeciesStatus ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.SurroundingActivity ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.TransmittingBuoyOperation ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.TransmittingBuoyType ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.VesselActivity ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.WeightCategory ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.Wind ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.Activity ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.FloatingObject ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.FloatingObjectPart ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.GearUseFeaturesMeasurement ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.GearUseFeatures ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.NonTargetCatch ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.NonTargetCatchRelease ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.NonTargetLength ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.NonTargetSample ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.ObjectObservedSpecies ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.ObjectSchoolEstimate ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.Route ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.SchoolEstimate ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.Set ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.TargetCatch ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.TargetLength ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.TargetSample ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.TransmittingBuoy ALTER COLUMN topiaCreateDate SET NOT NULL;
ALTER TABLE observe_seine.Trip ALTER COLUMN topiaCreateDate SET NOT NULL;
