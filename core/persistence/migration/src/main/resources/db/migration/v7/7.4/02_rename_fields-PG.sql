---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE observe_longline.Tdr RENAME deployementStart TO deploymentStart;
ALTER TABLE observe_longline.Tdr RENAME deployementEnd TO deploymentEnd;
ALTER TABLE observe_longline.Tdr RENAME meanDeployementDepth TO meanDeploymentDepth;
ALTER TABLE observe_longline.Tdr RENAME medianDeployementDepth TO medianDeploymentDepth;
ALTER TABLE observe_common.Program RENAME detailledactivitiesobservation TO detailedActivitiesObservation;
