---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE TABLE observe_longline.HealthStatus(topiaId VARCHAR(255) NOT NULL, topiaVersion BIGINT NOT NULL, topiaCreateDate TIMESTAMP NOT NULL, code VARCHAR(255), status INTEGER DEFAULT 1, uri VARCHAR(255), label1 VARCHAR(255), label2 VARCHAR(255), label3 VARCHAR(255), label4 VARCHAR(255), label5 VARCHAR(255), label6 VARCHAR(255), label7 VARCHAR(255), label8 VARCHAR(255), needcomment BOOLEAN DEFAULT FALSE, lastupdatedate TIMESTAMP DEFAULT now() NOT NULL, CONSTRAINT pk_observe_longline_HealthStatus primary key(topiaId));
INSERT INTO observe_longline.HealthStatus(topiaId, topiaVersion, topiaCreateDate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment, lastupdatedate) SELECT REPLACE(topiaId, 'Healthness', 'HealthStatus'), topiaVersion, topiaCreateDate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment, lastupdatedate FROM observe_longline.healthness;

CREATE TABLE observe_longline.StomachFullness(topiaId VARCHAR(255) NOT NULL, topiaVersion BIGINT NOT NULL, topiaCreateDate TIMESTAMP NOT NULL, code VARCHAR(255), status INTEGER DEFAULT 1, uri VARCHAR(255), label1 VARCHAR(255), label2 VARCHAR(255), label3 VARCHAR(255), label4 VARCHAR(255), label5 VARCHAR(255), label6 VARCHAR(255), label7 VARCHAR(255), label8 VARCHAR(255), needcomment BOOLEAN DEFAULT FALSE, lastupdatedate TIMESTAMP DEFAULT now() NOT NULL, CONSTRAINT pk_observe_longline_StomachFullness primary key(topiaId));
INSERT INTO observe_longline.StomachFullness(topiaId, topiaVersion, topiaCreateDate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment, lastupdatedate) SELECT REPLACE(topiaId, 'StomacFullness', 'StomachFullness'), topiaVersion, topiaCreateDate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment, lastupdatedate FROM observe_longline.StomacFullness;

ALTER TABLE observe_longline.Catch ADD COLUMN discardHealthStatus VARCHAR(255);
ALTER TABLE observe_longline.Catch ADD COLUMN catchHealthStatus VARCHAR(255);
ALTER TABLE observe_longline.Catch ADD COLUMN stomachFullness VARCHAR(255);
UPDATE observe_longline.Catch SET discardHealthStatus = REPLACE(discardHealthness, 'Healthness','HealthStatus'), catchHealthStatus = REPLACE(catchHealthness, 'Healthness','HealthStatus'), stomachFullness = REPLACE(stomacFullness, 'StomacFullness','StomachFullness');
ALTER TABLE observe_longline.Catch DROP COLUMN discardHealthness CASCADE;
ALTER TABLE observe_longline.Catch DROP COLUMN catchHealthness CASCADE;
ALTER TABLE observe_longline.Catch DROP COLUMN stomacFullness CASCADE;
ALTER TABLE observe_longline.Catch ADD CONSTRAINT fk_longline_catch_discardHealthStatus foreign key(discardHealthStatus) references observe_longline.HealthStatus;
ALTER TABLE observe_longline.Catch ADD CONSTRAINT fk_longline_catch_catchHealthStatus foreign key(catchHealthStatus) references observe_longline.HealthStatus;
ALTER TABLE observe_longline.Catch ADD CONSTRAINT fk_longline_catch_stomachFullness foreign key(stomachFullness) references observe_longline.StomachFullness;
CREATE INDEX idx_observe_longline_catch_discardHealthStatus ON observe_longline.catch(discardHealthStatus);
CREATE INDEX idx_observe_longline_catch_catchHealthStatus ON observe_longline.catch(catchHealthStatus);
CREATE INDEX idx_observe_longline_catch_stomachFullness ON observe_longline.catch(stomachFullness);

DROP TABLE observe_longline.Healthness CASCADE;
DROP TABLE observe_longline.StomacFullness CASCADE;

UPDATE observe_common.LASTUPDATEDATE SET TYPE = REPLACE(TYPE, 'referentiel.longline.Healthness', 'referentiel.longline.HealthStatus');
UPDATE observe_common.LASTUPDATEDATE SET TYPE = REPLACE(TYPE, 'referentiel.longline.StomacFullness', 'referentiel.longline.StomachFullness');
