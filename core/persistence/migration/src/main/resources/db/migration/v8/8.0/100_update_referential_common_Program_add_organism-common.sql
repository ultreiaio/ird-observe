---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

-- Add not-null on organism - See #2255
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838'  WHERE topiaid = 'fr.ird.referential.common.Program#1239832686139#0.1' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838'  WHERE topiaid = 'fr.ird.referential.common.Program#1433499209304#0.771258150460199' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.06632537553306939' WHERE topiaid = 'fr.ird.referential.common.Program#1239832686139#0.2' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838'  WHERE topiaid = 'fr.ird.referential.common.Program#1239832686139#0.3' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1433499207551#0.533253957750276'   WHERE topiaid = 'fr.ird.referential.common.Program#1476709649993#0.9593100746141565' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838'  WHERE topiaid = 'fr.ird.referential.common.Program#1539251571663#0.10915739659024792' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1327072060581#0.7854385453860163'  WHERE topiaid = 'fr.ird.referential.common.Program#1540296800663#0.28879602188546094' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838'  WHERE topiaid = 'fr.ird.referential.common.Program#1239832686139#0.8' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1433499207967#0.904187304666266'   WHERE topiaid = 'fr.ird.referential.common.Program#1433499208879#0.233676712261513' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.5652983591594903'  WHERE topiaid = 'fr.ird.referential.common.Program#1574078003724#0.7327731619625379' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.15010546073051645' WHERE topiaid = 'fr.ird.referential.common.Program#1433499208418#0.603629674296826' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838'  WHERE topiaid = 'fr.ird.referential.common.Program#1239832686139#0.5' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838'  WHERE topiaid = 'fr.ird.referential.common.Program#1239832686139#0.7' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1433499207551#0.533253957750276'   WHERE topiaid = 'fr.ird.referential.common.Program#1239832686139#0.6' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838'  WHERE topiaid = 'fr.ird.referential.common.Program#1239832686139#0.4' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1441272043546#0.21579847592007906' WHERE topiaid = 'fr.ird.referential.common.Program#1441272173250#0.22224648148214798' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1441272043546#0.21579847592007906' WHERE topiaid = 'fr.ird.referential.common.Program#1508406051783#0.3774859341347857' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838'  WHERE topiaid = 'fr.ird.referential.common.Program#1464000000000#99' AND organism IS NULL;

UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838' WHERE topiaid = 'fr.ird.referential.common.Program#1239832686262#0.3362184368606246' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.06632537553306939' WHERE topiaid = 'fr.ird.referential.common.Program#1239832686262#0.714540816186228' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838' WHERE topiaid = 'fr.ird.referential.common.Program#1264610568269#0.26135820135328836' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838' WHERE topiaid = 'fr.ird.referential.common.Program#1239832686262#0.8610783468649943' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.5652983591594903' WHERE topiaid = 'fr.ird.referential.common.Program#1264610568269#0.20791810830326396' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1327072060581#0.7854385453860163' WHERE topiaid = 'fr.ird.referential.common.Program#1302877007400#0.8159545375917711' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838' WHERE topiaid = 'fr.ird.referential.common.Program#1239832686262#0.31033946454061234' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.14910700446959457' WHERE topiaid = 'fr.ird.referential.common.Program#1264610568269#0.3041024869394401' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.15010546073051645' WHERE topiaid = 'fr.ird.referential.common.Program#1264610568269#0.04557703863701967' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838' WHERE topiaid = 'fr.ird.referential.common.Program#1308048349668#0.7314513252652438' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838' WHERE topiaid = 'fr.ird.referential.common.Program#1363095174385#0.011966550987014823' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.14910700446959457' WHERE topiaid = 'fr.ird.referential.common.Program#1326997807154#0.6381090187977135' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.14910700446959457' WHERE topiaid = 'fr.ird.referential.common.Program#1326997947903#0.15492621989367272' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838' WHERE topiaid = 'fr.ird.referential.common.Program#1373642516190#0.998459307142491' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1539251468084#0.4304283149693777' WHERE topiaid = 'fr.ird.referential.common.Program#1539949992432#0.4308822110391043' AND organism IS NULL;
UPDATE common.program SET organism = 'fr.ird.referential.common.Organism#1267835067241#0.6705236825871838' WHERE topiaid = 'fr.ird.referential.common.Program#1239832686262#0.42751447061198444' AND organism IS NULL;
