---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- See #2085
UPDATE common.SpeciesGroupReleaseMode SET code =  '1', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId='fr.ird.referential.common.SpeciesGroupReleaseMode#0#1';
UPDATE common.SpeciesGroupReleaseMode SET code =  '2', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId='fr.ird.referential.common.SpeciesGroupReleaseMode#0#2';
UPDATE common.SpeciesGroupReleaseMode SET code =  '3', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId='fr.ird.referential.common.SpeciesGroupReleaseMode#0#3';
UPDATE common.SpeciesGroupReleaseMode SET code =  '4', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId='fr.ird.referential.common.SpeciesGroupReleaseMode#0#4';
UPDATE common.SpeciesGroupReleaseMode SET code =  '5', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId='fr.ird.referential.common.SpeciesGroupReleaseMode#0#5';
UPDATE common.SpeciesGroupReleaseMode SET code =  '7', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId='fr.ird.referential.common.SpeciesGroupReleaseMode#0#7';
UPDATE common.SpeciesGroupReleaseMode SET code =  '8', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId='fr.ird.referential.common.SpeciesGroupReleaseMode#0#8';
UPDATE common.SpeciesGroupReleaseMode SET code =  '9', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId='fr.ird.referential.common.SpeciesGroupReleaseMode#0#9';
UPDATE common.SpeciesGroupReleaseMode SET code = '10', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId='fr.ird.referential.common.SpeciesGroupReleaseMode#0#10';

UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.SpeciesGroupReleaseMode';
