---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- See #2255
UPDATE ll_observation.baithaulingstatus SET code = 'ABS' WHERE topiaid = 'fr.ird.referential.ll.observation.BaitHaulingStatus#1239832686122#0.1' AND code IS NULL;
UPDATE ll_observation.baithaulingstatus SET code = 'INT' WHERE topiaid = 'fr.ird.referential.ll.observation.BaitHaulingStatus#1239832686122#0.2' AND code IS NULL;
UPDATE ll_observation.baithaulingstatus SET code = 'DEP' WHERE topiaid = 'fr.ird.referential.ll.observation.BaitHaulingStatus#1239832686122#0.3' AND code IS NULL;
UPDATE ll_observation.baithaulingstatus SET code = 'BIT' WHERE topiaid = 'fr.ird.referential.ll.observation.BaitHaulingStatus#1239832686122#0.4' AND code IS NULL;
UPDATE ll_observation.baithaulingstatus SET code = 'UNK' WHERE topiaid = 'fr.ird.referential.ll.observation.BaitHaulingStatus#1433499461063#0.595451570814475' AND code IS NULL;
