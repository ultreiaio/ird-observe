---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- See #2255
UPDATE ll_observation.encountertype SET code = '01' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.1' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '02' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.10' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '03' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.11' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '04' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.12' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '05' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.2' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '06' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.3' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '07' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.4' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '08' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.5' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '09' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.6' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '10' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.7' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '11' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.8' AND code IS NULL;
UPDATE ll_observation.encountertype SET code = '12' WHERE topiaid = 'fr.ird.referential.ll.observation.EncounterType#1239832686126#0.9' AND code IS NULL;
