---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- See #2255
UPDATE ll_common.healthstatus SET code = 'A' WHERE topiaid = 'fr.ird.referential.ll.common.HealthStatus#1239832686128#0.1' AND code IS NULL;
UPDATE ll_common.healthstatus SET code = 'AUSC' WHERE topiaid = 'fr.ird.referential.ll.common.HealthStatus#1239832686128#0.2' AND code IS NULL;
UPDATE ll_common.healthstatus SET code = 'D' WHERE topiaid = 'fr.ird.referential.ll.common.HealthStatus#1239832686128#0.3' AND code IS NULL;
UPDATE ll_common.healthstatus SET code = 'UNK' WHERE topiaid = 'fr.ird.referential.ll.common.HealthStatus#1239832686128#0.4' AND code IS NULL;
UPDATE ll_common.healthstatus SET code = 'AW' WHERE topiaid = 'fr.ird.referential.ll.common.HealthStatus#1239832686128#0.5' AND code IS NULL;
UPDATE ll_common.healthstatus SET code = 'DFCO' WHERE topiaid = 'fr.ird.referential.ll.common.HealthStatus#1239832686128#0.6' AND code IS NULL;
