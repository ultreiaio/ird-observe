---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- See #2255
UPDATE ll_observation.hookposition SET code = 'UNK' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.1' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'BIL' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.3' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'ENT' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.4' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'THR' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1575642035789#0.9332211910258391' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'ANF' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.7' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'CHK' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.2' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'DOF' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.9' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'EYE' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.12' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'GIL' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.13' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'JAW' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.5' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'PCF' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.10' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'CDF' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.8' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'BOD' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.11' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'SWA' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1239832686150#0.6' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'PAL' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1596017949535#0.7928414829994028' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'STO' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1596017992241#0.7018221233529162' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'LIP' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1596018021861#0.448473301798728' AND code IS NULL;
UPDATE ll_observation.hookposition SET code = 'EXT' WHERE topiaid = 'fr.ird.referential.ll.observation.HookPosition#1597050676143#0.6862417851282098' AND code IS NULL;
