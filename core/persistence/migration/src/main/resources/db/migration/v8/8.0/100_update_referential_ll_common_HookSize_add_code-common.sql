---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- See #2255
UPDATE ll_common.hooksize SET code = '9/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.2' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '10/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.3' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '11/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.4' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '12/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.5' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '13/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.6' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '14/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.7' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '15/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.8' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '16/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.9' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '17/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.10' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '18/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.11' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '3.2 sun' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.12' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '3.4 sun' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.13' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '3.6 sun' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.14' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '3.8 sun' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.15' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '4.0 sun' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.16' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '4.2 sun' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.17' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '1' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.18' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '2' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.19' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '3' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.20' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '4' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.21' AND code IS NULL;
UPDATE ll_common.hooksize SET code = 'Unknown' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1433499436918#0.552373287733644' AND code IS NULL;
UPDATE ll_common.hooksize SET code = '8/0' WHERE topiaid = 'fr.ird.referential.ll.common.HookSize#1239832686151#0.1' AND code IS NULL;
