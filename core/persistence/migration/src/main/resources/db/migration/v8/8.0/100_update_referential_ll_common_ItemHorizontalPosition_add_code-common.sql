---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- See #2255
UPDATE ll_observation.itemhorizontalposition SET code = '1' WHERE topiaid = 'fr.ird.referential.ll.observation.ItemHorizontalPosition#1239832686153#0.1' AND code IS NULL;
UPDATE ll_observation.itemhorizontalposition SET code = '2' WHERE topiaid = 'fr.ird.referential.ll.observation.ItemHorizontalPosition#1239832686153#0.2' AND code IS NULL;
UPDATE ll_observation.itemhorizontalposition SET code = '3' WHERE topiaid = 'fr.ird.referential.ll.observation.ItemHorizontalPosition#1239832686153#0.3' AND code IS NULL;
