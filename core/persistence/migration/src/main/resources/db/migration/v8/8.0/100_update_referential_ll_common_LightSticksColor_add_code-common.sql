---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- See #2255
UPDATE ll_common.lightstickscolor SET code = 'R' WHERE topiaid = 'fr.ird.referential.ll.common.LightsticksColor#1239832686155#0.1' AND code IS NULL;
UPDATE ll_common.lightstickscolor SET code = 'G' WHERE topiaid = 'fr.ird.referential.ll.common.LightsticksColor#1239832686155#0.2' AND code IS NULL;
UPDATE ll_common.lightstickscolor SET code = 'B' WHERE topiaid = 'fr.ird.referential.ll.common.LightsticksColor#1239832686155#0.3' AND code IS NULL;
UPDATE ll_common.lightstickscolor SET code = 'Y' WHERE topiaid = 'fr.ird.referential.ll.common.LightsticksColor#1239832686155#0.4' AND code IS NULL;
UPDATE ll_common.lightstickscolor SET code = 'P' WHERE topiaid = 'fr.ird.referential.ll.common.LightsticksColor#1239832686155#0.5' AND code IS NULL;
UPDATE ll_common.lightstickscolor SET code = 'O' WHERE topiaid = 'fr.ird.referential.ll.common.LightsticksColor#1239832686155#0.6' AND code IS NULL;
UPDATE ll_common.lightstickscolor SET code = 'UNK' WHERE topiaid = 'fr.ird.referential.ll.common.LightsticksColor#1433499427451#0.722765480168164' AND code IS NULL;
UPDATE ll_common.lightstickscolor SET code = 'VAR' WHERE topiaid = 'fr.ird.referential.ll.common.LightsticksColor#1480346842433#0.4413676426379999' AND code IS NULL;
