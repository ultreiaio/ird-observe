---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- See #2255
UPDATE ll_common.vesselactivity SET code = 'FO' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.1' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'OTH' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.2' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'SAMP' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.3' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'INT' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.4' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'FAIL' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.5' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'CRUISE' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#666#01' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'DRIFT' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#666#02' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'OUTZEE' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#666#04' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'PORT' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#666#03' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'REPAIR' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#666#05' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'TRANSSHIP' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#666#06' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'UNK' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#666#07' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'WAITING' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.01' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'SEINE_BACKDOWN' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.02' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'HOVE' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.03' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'FO_NEGATIVE' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.04' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'FO_POSITIVE' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.05' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'FO_UNKNOWN' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.06' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'FOB_REMOVAL' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.07' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'FOB_DEPLOYMENT' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.08' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'INACTIVE' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.09' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'LANDING' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.10' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'FO_HAULING' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.11' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'ONEDAYTRIP' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.12' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'OUT' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.13' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'SEARCH' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.14' AND code IS NULL;
UPDATE ll_common.vesselactivity SET code = 'TRANSIT' WHERE topiaid = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.15' AND code IS NULL;
