---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.Country' , 'referential.common.Country'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.DataQuality' , 'referential.common.DataQuality');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.FpaZone' , 'referential.common.FpaZone');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.GearCaracteristicType' , 'referential.common.GearCharacteristicType');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.GearCaracteristic' , 'referential.common.GearCharacteristic');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.Gear' , 'referential.common.Gear');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.lastUpdateDate' , 'referential.common.lastUpdateDate');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.Ocean' , 'referential.common.Ocean');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.Sex' , 'referential.common.Sex');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.SizeMeasureType' , 'referential.common.SizeMeasureType');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.SpeciesGroupReleaseMode' , 'referential.common.SpeciesGroupReleaseMode');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.SpeciesGroup' , 'referential.common.SpeciesGroup');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.SpeciesList' , 'referential.common.SpeciesList'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.Species' , 'referential.common.Species'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.VesselSizeCategory' , 'referential.common.VesselSizeCategory');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.VesselType' , 'referential.common.VesselType'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.WeightMeasureMethod' , 'referential.common.WeightMeasureMethod');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.WeightMeasureType' , 'referential.common.WeightMeasureType');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.Wind' , 'referential.common.Wind'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.LengthLengthParameter' , 'referential.common.LengthLengthParameter');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.LengthWeightParameter' , 'referential.common.LengthWeightParameter'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.Harbour' , 'referential.common.Harbour');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.Organism' , 'referential.common.Organism');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.Person' , 'referential.common.Person'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.Program' , 'referential.common.Program'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.ShipOwner' , 'referential.common.ShipOwner');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.Vessel' , 'referential.common.Vessel');

INSERT INTO common.lastUpdateDate(topiaId, topiaVersion, topiaCreateDate, TYPE , lastUpdateDate) values ('fr.ird.common.lastUpdateDate#666#1007', 0,${CURRENT_DATE}, 'fr.ird.observe.entities.referential.common.LengthMeasureMethod', ${CURRENT_TIMESTAMP});
INSERT INTO common.lastUpdateDate(topiaId, topiaVersion, topiaCreateDate, TYPE , lastUpdateDate) values ('fr.ird.common.lastUpdateDate#666#1008', 0,${CURRENT_DATE}, 'fr.ird.observe.entities.referential.common.WeightMeasureMethod', ${CURRENT_TIMESTAMP});
