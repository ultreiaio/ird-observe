---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or(at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program. If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.SpeciesFate', 'referential.ps.common.SpeciesFate');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.TransmittingBuoyOperation', 'referential.ps.common.TransmittingBuoyOperation');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.TransmittingBuoyType', 'referential.ps.common.TransmittingBuoyType');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.VesselActivitySeine', 'referential.ps.common.VesselActivity');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.ObjectMaterialType', 'referential.ps.common.ObjectMaterialType');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.ObjectMaterial', 'referential.ps.common.ObjectMaterial'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.ObjectOperation', 'referential.ps.common.ObjectOperation');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.ObservedSystem', 'referential.ps.common.ObservedSystem');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.TripSeine', 'data.ps.common.Trip');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.GearUsefeaturesMeasurementSeine', 'data.ps.common.GearUseFeaturesMeasurement');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.GearUsefeaturesSeine', 'data.ps.common.GearUseFeatures');

INSERT INTO common.lastUpdateDate(topiaId, topiaVersion, topiaCreateDate, TYPE , lastUpdateDate) values ('fr.ird.common.lastUpdateDate#666#2000', 0, ${CURRENT_DATE}, 'fr.ird.observe.entities.referential.ps.common.SchoolType', ${CURRENT_TIMESTAMP});
INSERT INTO common.lastUpdateDate(topiaId, topiaVersion, topiaCreateDate, TYPE , lastUpdateDate) values ('fr.ird.common.lastUpdateDate#666#2001', 0, ${CURRENT_DATE}, 'fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOwnership', ${CURRENT_TIMESTAMP});
