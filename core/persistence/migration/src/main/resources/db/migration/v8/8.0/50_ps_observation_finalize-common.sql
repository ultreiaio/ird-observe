---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.DetectionMode', 'referential.ps.observation.DetectionMode');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.NonTargetCatchReleaseConformity', 'referential.ps.observation.NonTargetCatchReleaseConformity');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.NonTargetCatchReleaseStatus', 'referential.ps.observation.NonTargetCatchReleaseStatus');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.NonTargetCatchReleasingTime', 'referential.ps.observation.NonTargetCatchReleasingTime');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.ReasonForDiscard', 'referential.ps.observation.ReasonForDiscard');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.ReasonForNoFishing', 'referential.ps.observation.ReasonForNoFishing');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.ReasonForNullSet', 'referential.ps.observation.ReasonForNullSet');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.SpeciesStatus', 'referential.ps.observation.SpeciesStatus');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.SurroundingActivity', 'referential.ps.observation.SurroundingActivity');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.seine.WeightCategory', 'referential.ps.observation.WeightCategory');

UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.SetSeine', 'data.ps.observation.Set');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.NonTargetCatchRelease', 'data.ps.observation.NonTargetCatchRelease');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.NonTargetSample', 'data.ps.observation.NonTargetSample');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.SchoolEstimate', 'data.ps.observation.SchoolEstimate');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.NonTargetCatch', 'data.ps.observation.NonTargetCatch');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.NonTargetLength', 'data.ps.observation.NonTargetLength');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.TargetSample', 'data.ps.observation.TargetSample');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.TargetLength', 'data.ps.observation.TargetLength');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.Route', 'data.ps.observation.Route');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.ActivitySeine', 'data.ps.observation.Activity');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.FloatingObject', 'data.ps.observation.FloatingObject');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.FloatingObjectPart', 'data.ps.observation.FloatingObjectPart');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.ObjectObservedSpecies', 'data.ps.observation.ObjectObservedSpecies');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.ObjectSchoolEstimate', 'data.ps.observation.ObjectSchoolEstimate');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.TransmittingBuoy', 'data.ps.observation.TransmittingBuoy');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'seine.TargetCatch', 'data.ps.observation.TargetCatch');
