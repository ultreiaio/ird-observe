---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.BaitSettingStatus', 'referential.ll.common.BaitSettingStatus');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.BaitType', 'referential.ll.common.BaitType');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.CatchFateLongline', 'referential.ll.common.CatchFate');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.HealthStatus', 'referential.ll.common.HealthStatus');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.HookSize', 'referential.ll.common.HookSize');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.HookType', 'referential.ll.common.HookType');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.LightsticksColor', 'referential.ll.common.LightsticksColor');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.LightsticksType', 'referential.ll.common.LightsticksType');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.LineType', 'referential.ll.common.LineType');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.MitigationType', 'referential.ll.common.MitigationType');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.TripType', 'referential.ll.common.ObservationMethod'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.SettingShape', 'referential.ll.common.SettingShape');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.VesselActivityLongline', 'referential.ll.common.VesselActivity'), lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'referentiel.longline.WeightCategoryLongline', 'referential.ll.common.WeightCategory');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'longline.GearUsefeaturesMeasurementLongline', 'data.ll.common.GearUseFeaturesMeasurement');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'longline.GearUsefeaturesMeasurementLongline', 'data.ll.common.GearUseFeatures');
UPDATE common.lastUpdateDate SET TYPE = REPLACE(TYPE, 'longline.TripLongline', 'data.ll.common.Trip');

INSERT INTO common.lastUpdateDate(topiaId, topiaVersion, topiaCreateDate, TYPE , lastUpdateDate) values ('fr.ird.common.lastUpdateDate#666#1004', 0,${CURRENT_DATE}, 'fr.ird.observe.entities.referential.ll.common.WeightDeterminationMethod', ${CURRENT_TIMESTAMP});
INSERT INTO common.lastUpdateDate(topiaId, topiaVersion, topiaCreateDate, TYPE , lastUpdateDate) values ('fr.ird.common.lastUpdateDate#666#1005', 0,${CURRENT_DATE}, 'fr.ird.observe.entities.referential.ll.common.WeightCategory', ${CURRENT_TIMESTAMP});
INSERT INTO common.lastUpdateDate(topiaId, topiaVersion, topiaCreateDate, TYPE , lastUpdateDate) values ('fr.ird.common.lastUpdateDate#666#1010', 0,${CURRENT_DATE}, 'fr.ird.observe.entities.referential.ll.common.OnBoardProcessing', ${CURRENT_TIMESTAMP});
INSERT INTO common.lastUpdateDate(topiaId, topiaVersion, topiaCreateDate, TYPE , lastUpdateDate) values ('fr.ird.common.lastUpdateDate#666#1013', 0,${CURRENT_DATE}, 'fr.ird.observe.entities.referential.ll.common.TripType', ${CURRENT_TIMESTAMP});
