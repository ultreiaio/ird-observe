---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'referentiel.longline.BaitHaulingStatus', 'referential.ll.observation.BaitHaulingStatus');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'referentiel.longline.EncounterType', 'referential.ll.observation.EncounterType');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'referentiel.longline.HookPosition', 'referential.ll.observation.HookPosition');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'referentiel.longline.ItemHorizontalPosition', 'referential.ll.observation.ItemHorizontalPosition');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'referentiel.longline.ItemVerticalPosition', 'referential.ll.observation.ItemVerticalPosition');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'referentiel.longline.MaturityStatus', 'referential.ll.observation.MaturityStatus');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'referentiel.longline.SensorBrand', 'referential.ll.observation.SensorBrand');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'referentiel.longline.SensorDataFormat', 'referential.ll.observation.SensorDataFormat');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'referentiel.longline.SensorType', 'referential.ll.observation.SensorType');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'referentiel.longline.StomachFullness', 'referential.ll.observation.StomachFullness');

UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.ActivityLongline', 'data.ll.observation.Activity');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.BaitsComposition', 'data.ll.observation.BaitsComposition');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.Basket', 'data.ll.observation.Basket');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.Branchline', 'data.ll.observation.Branchline');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.BranchlinesComposition', 'data.ll.observation.BranchlinesComposition');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.CatchLongline', 'data.ll.observation.Catch');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.Encounter', 'data.ll.observation.Encounter');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.FloatlinesComposition', 'data.ll.observation.FloatlinesComposition');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.HooksComposition', 'data.ll.observation.HooksComposition');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.Section', 'data.ll.observation.Section');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.SensorUsed', 'data.ll.observation.SensorUsed');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.SetLongline', 'data.ll.observation.Set');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.SizeMeasure', 'data.ll.observation.SizeMeasure');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.Tdr', 'data.ll.observation.Tdr');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.TdrRecord', 'data.ll.observation.TdrRecord');
UPDATE common.lastUpdateDate SET TYPE = replace(TYPE, 'longline.WeightMeasure', 'data.ll.WeightMeasure');
