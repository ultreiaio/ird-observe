---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- Add Program.observation, Program.logbook - See #2052

ALTER TABLE common.Program ADD COLUMN observation boolean default false NOT NULL;
ALTER TABLE common.Program ADD COLUMN logbook boolean default false NOT NULL;
UPDATE common.Program SET logbook = true WHERE topiaId = '${Program_9}';
UPDATE common.Program SET observation = true;
UPDATE common.Program SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.Program';

-- Supprimer ces flags sur Program - See #2051
ALTER TABLE common.Program DROP COLUMN nonTargetObservation;
ALTER TABLE common.Program DROP COLUMN targetDiscardsObservation;
ALTER TABLE common.Program DROP COLUMN samplesObservation;
ALTER TABLE common.Program DROP COLUMN objectsObservation;
ALTER TABLE common.Program DROP COLUMN detailedActivitiesObservation;
ALTER TABLE common.Program DROP COLUMN mammalsObservation;
ALTER TABLE common.Program DROP COLUMN birdsObservation;
ALTER TABLE common.Program DROP COLUMN baitObservation;
