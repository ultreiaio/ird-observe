---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE common.Vessel ALTER COLUMN power RENAME TO powerCv;
ALTER TABLE common.Vessel ADD COLUMN powerKW INTEGER;

-- Add Vessel.wellRegex See https://gitlab.com/ultreiaio/ird-observe/-/issues/1890

ALTER TABLE common.Vessel ADD COLUMN wellRegex VARCHAR(512);
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.Vessel';

-- Ajout de 2 colonnes dates sur Vessel See https://gitlab.com/ultreiaio/ird-observe/-/issues/2040
ALTER TABLE common.Vessel ADD COLUMN startDate DATE;
ALTER TABLE common.Vessel ADD COLUMN endDate DATE;
