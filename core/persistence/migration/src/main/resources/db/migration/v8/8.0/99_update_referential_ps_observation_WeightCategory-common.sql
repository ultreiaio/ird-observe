---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

-- Add ps_logbook.WeightCategory.meanWeight - See #1871 (we start to set it to legacy form Catches migration)
ALTER TABLE ps_observation.weightCategory ADD COLUMN meanWeight numeric;

UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-1-10' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1424936892080#0.3071247026973247';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-1-11' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1265901225547#0.7474093737982056';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-1-1' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685499#0.45137386216494235';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-1-2' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685505#0.6893474228813191';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-1-3' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1265900972942#0.14809205088390143';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-1-5' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1265901072470#0.48282507736865';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-1-7' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1265901072470#0.3502049636413197';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-2-1' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1265900836849#0.18015633272140852';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-2-2' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1265900836849#0.9036635253340434';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-2-3' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685673#0.3163596616269717';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-2-4' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685679#0.019733673419385367';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-2-5' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685685#0.7315998797178913';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-2-6' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685692#0.5940916998227946';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-2-7' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685722#0.9714869893887224';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-2-8' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685729#0.0015801959709714763';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-3-10' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1424937065301#0.2847458239357389';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-3-11' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685791#0.96882088900269';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-3-13' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1389776259896#0.07638719997162424';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-3-1' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685747#0.8903018444376307';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-3-2' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685754#0.10338259022397256';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-3-3' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685760#0.5190060347627616';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-3-5' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685773#0.36018870624305044';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-3-7' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685785#0.017557737970281928';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-4-10' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1424937125831#0.9928551215821171';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-4-11' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685843#0.5227248075951917';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-4-13' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1389775941412#0.15834335814453693';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-4-1' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685804#0.034641865339321565';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-4-2' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685810#0.9054833697032716';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-4-3' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685818#0.5146901232402814';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-4-5' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685830#0.3505424853460628';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-4-7' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1265900731495#0.02540873055336068';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-5-1' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685968#0.8743676757879854';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-5-2' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685975#0.9269185705341096';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-5-3' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685981#0.20973946035734992';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-5-4' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685987#0.8124900788304172';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-5-5' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685993#0.7110149379733451';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-5-7' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832686006#0.27918502949209356';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-5-8' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832686012#0.3678254910247588';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-6-1' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685913#0.6908151762213386';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-6-2' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685919#0.32661937164161003';
UPDATE ps_observation.weightcategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code ='C-6-3' WHERE topiaId = 'fr.ird.referential.ps.observation.WeightCategory#1239832685926#0.26263537384431435';


UPDATE ps_observation.WeightCategory SET meanWeight = 7  WHERE code = 'C-1-2';
UPDATE ps_observation.WeightCategory SET meanWeight = 20 WHERE code = 'C-1-3';
UPDATE ps_observation.WeightCategory SET meanWeight = 13 WHERE code = 'C-1-4';
UPDATE ps_observation.WeightCategory SET meanWeight = 40 WHERE code = 'C-1-5';
UPDATE ps_observation.WeightCategory SET meanWeight = 30 WHERE code = 'C-1-6';
UPDATE ps_observation.WeightCategory SET meanWeight = 50 WHERE code = 'C-1-8';
UPDATE ps_observation.WeightCategory SET meanWeight = 2  WHERE code = 'C-2-3';
UPDATE ps_observation.WeightCategory SET meanWeight = 3  WHERE code = 'C-2-4';
UPDATE ps_observation.WeightCategory SET meanWeight = 5  WHERE code = 'C-2-5';
UPDATE ps_observation.WeightCategory SET meanWeight = 6  WHERE code = 'C-2-6';
UPDATE ps_observation.WeightCategory SET meanWeight = 7  WHERE code = 'C-2-7';
UPDATE ps_observation.WeightCategory SET meanWeight = 7  WHERE code = 'C-3-2';
UPDATE ps_observation.WeightCategory SET meanWeight = 20 WHERE code = 'C-3-3';
UPDATE ps_observation.WeightCategory SET meanWeight = 13 WHERE code = 'C-3-4';
UPDATE ps_observation.WeightCategory SET meanWeight = 40 WHERE code = 'C-3-5';
UPDATE ps_observation.WeightCategory SET meanWeight = 30 WHERE code = 'C-3-6';
UPDATE ps_observation.WeightCategory SET meanWeight = 50 WHERE code = 'C-3-8';
UPDATE ps_observation.WeightCategory SET meanWeight = 7  WHERE code = 'C-4-2';
UPDATE ps_observation.WeightCategory SET meanWeight = 20 WHERE code = 'C-4-3';
UPDATE ps_observation.WeightCategory SET meanWeight = 13 WHERE code = 'C-4-4';
UPDATE ps_observation.WeightCategory SET meanWeight = 40 WHERE code = 'C-4-5';
UPDATE ps_observation.WeightCategory SET meanWeight = 30 WHERE code = 'C-4-6';
UPDATE ps_observation.WeightCategory SET meanWeight = 50 WHERE code = 'C-4-8';
UPDATE ps_observation.WeightCategory SET meanWeight = 7  WHERE code = 'C-11-2';
UPDATE ps_observation.WeightCategory SET meanWeight = 20 WHERE code = 'C-11-3';
UPDATE ps_observation.WeightCategory SET meanWeight = 13 WHERE code = 'C-11-4';
UPDATE ps_observation.WeightCategory SET meanWeight = 40 WHERE code = 'C-11-5';
UPDATE ps_observation.WeightCategory SET meanWeight = 30 WHERE code = 'C-11-6';
UPDATE ps_observation.WeightCategory SET meanWeight = 50 WHERE code = 'C-11-8';
