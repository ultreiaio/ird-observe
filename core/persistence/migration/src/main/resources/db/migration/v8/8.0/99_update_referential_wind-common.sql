---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

ALTER TABLE common.Wind ADD COLUMN  minSpeed INTEGER;
ALTER TABLE common.Wind ADD COLUMN  maxSpeed INTEGER;
ALTER TABLE common.Wind ADD COLUMN  minSwellHeight NUMERIC;
ALTER TABLE common.Wind ADD COLUMN  maxSwellHeight NUMERIC;
ALTER TABLE common.Wind DROP COLUMN speedRange;
ALTER TABLE common.Wind DROP COLUMN waveHeight;

UPDATE common.Wind SET minSpeed = NULL, maxSpeed = 1    , minSwellHeight = 0    , maxSwellHeight = 0     WHERE topiaId = 'fr.ird.referential.common.Wind#1239832686604#0.6861700847985526';
UPDATE common.Wind SET minSpeed = 1,    maxSpeed = 3    , minSwellHeight = 0    , maxSwellHeight =  0.1  WHERE topiaId = 'fr.ird.referential.common.Wind#1239832686605#0.013889226172736802';
UPDATE common.Wind SET minSpeed = 4,    maxSpeed = 6    , minSwellHeight = 0.1  , maxSwellHeight =  0.5  WHERE topiaId = 'fr.ird.referential.common.Wind#1239832686605#0.14941733815370462';
UPDATE common.Wind SET minSpeed = 7,    maxSpeed = 10   , minSwellHeight = 0.5  , maxSwellHeight =  1.25 WHERE topiaId = 'fr.ird.referential.common.Wind#1239832686605#0.561188597983181';
UPDATE common.Wind SET minSpeed = 11,   maxSpeed = 16   , minSwellHeight = 1.25 , maxSwellHeight =  2.5  WHERE topiaId = 'fr.ird.referential.common.Wind#1239832686605#0.756896053766457';
UPDATE common.Wind SET minSpeed = 17,   maxSpeed = 21   , minSwellHeight = 2.5  , maxSwellHeight =  4    WHERE topiaId = 'fr.ird.referential.common.Wind#1239832686606#0.4900119760201238';
UPDATE common.Wind SET minSpeed = 22,   maxSpeed = 27   , minSwellHeight = 4    , maxSwellHeight =  6    WHERE topiaId = 'fr.ird.referential.common.Wind#1239832686607#0.7102940452522897';
UPDATE common.Wind SET minSpeed = 41,   maxSpeed = 47   , minSwellHeight = 14   , maxSwellHeight =  NULL WHERE topiaId = 'fr.ird.referential.common.Wind#1239832686608#0.5265273246398433';
UPDATE common.Wind SET minSpeed = 34,   maxSpeed = 40   , minSwellHeight = 9    , maxSwellHeight =  14   WHERE topiaId = 'fr.ird.referential.common.Wind#1239832686608#0.7828762960828749';
UPDATE common.Wind SET minSpeed = 28,   maxSpeed = 33   , minSwellHeight = 6    , maxSwellHeight =  9    WHERE topiaId = 'fr.ird.referential.common.Wind#1239832686608#0.8409158782611442';
UPDATE common.Wind SET minSpeed = 48,   maxSpeed = 53   , minSwellHeight = NULL , maxSwellHeight =  NULL WHERE topiaId = 'fr.ird.referential.common.Wind#1303918173912#0.23915390900201217';
UPDATE common.Wind SET minSpeed = 54,   maxSpeed = 63   , minSwellHeight = NULL , maxSwellHeight = NULL  WHERE topiaId = 'fr.ird.referential.common.Wind#1303918224279#0.47516001917713135';
UPDATE common.Wind SET minSpeed = 63,   maxSpeed = NULL , minSwellHeight = NULL , maxSwellHeight = NULL  WHERE topiaId = 'fr.ird.referential.common.Wind#1303918247368#0.6152325455305158';
