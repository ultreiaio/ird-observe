---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- update harbour coordinates
UPDATE common.Harbour SET latitude = -4.47, longitude = 11.5  WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.0';
UPDATE common.Harbour SET latitude = 50.43, longitude = 1.35  WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.4';
UPDATE common.Harbour SET latitude = 43.23, longitude = -1.4  WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.5';
UPDATE common.Harbour SET latitude = -0.43, longitude = 8.47  WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.6';
UPDATE common.Harbour SET latitude = 14.36, longitude = -61.03 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.7';
UPDATE common.Harbour SET latitude = 5.38 , longitude = 0.01  WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.9';
UPDATE common.Harbour SET latitude = 28.08, longitude = -15.25 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.10';
UPDATE common.Harbour SET latitude = -33.32, longitude = 18.16 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.11';
UPDATE common.Harbour SET latitude = 28.29, longitude = -16.13 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.15';
UPDATE common.Harbour SET latitude = 8.3  , longitude = -13.13 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.16';
UPDATE common.Harbour SET latitude = -34.53, longitude = -56.13 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.17';
UPDATE common.Harbour SET latitude = 0.24 , longitude = 9.26  WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.26';
UPDATE common.Harbour SET latitude = 0.21 , longitude = 6.44  WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.27';
UPDATE common.Harbour SET latitude = 38.24, longitude = -28.14 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.28';
UPDATE common.Harbour SET latitude = 42.36, longitude = -8.47 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.33';
UPDATE common.Harbour SET latitude = 39.35, longitude = -0.19 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.36';
UPDATE common.Harbour SET latitude = -4.38, longitude = 55.29 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.41';
UPDATE common.Harbour SET latitude = -20.167, longitude = 57.517 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.42';
UPDATE common.Harbour SET latitude = -20.56, longitude = 55.18 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.43';
UPDATE common.Harbour SET latitude = -12.17, longitude = 49.17 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.44';
UPDATE common.Harbour SET latitude = 11.36, longitude = 43.07 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.45';
UPDATE common.Harbour SET latitude = -4.03, longitude = 39.38 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.47';
UPDATE common.Harbour SET latitude = -35.45, longitude = 174.2 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.48';
UPDATE common.Harbour SET latitude = 1.16 , longitude = 103.45 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.50';
UPDATE common.Harbour SET latitude = 13.37, longitude = 100.33 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.51';
UPDATE common.Harbour SET latitude = 38.6 , longitude = -0.09 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.52';
UPDATE common.Harbour SET latitude = 27.09, longitude = 56.14 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.53';
UPDATE common.Harbour SET latitude = 25.19, longitude = 60.36 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.54';
UPDATE common.Harbour SET latitude = 7.51 , longitude = 98.24 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.55';
UPDATE common.Harbour SET latitude = -12.47, longitude = 45.15 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.56';
UPDATE common.Harbour SET latitude = -12.47, longitude = 45.14 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.57';
UPDATE common.Harbour SET latitude = 10.15, longitude = -64.35 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.59';
UPDATE common.Harbour SET latitude = -29.53, longitude = 31.02 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.60';
UPDATE common.Harbour SET latitude = 12.32, longitude = -81.43 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.65';
UPDATE common.Harbour SET latitude = 25.17, longitude = 55.18 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.66';
UPDATE common.Harbour SET latitude = -15.43, longitude = 46.19 WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.67';
UPDATE common.Harbour SET latitude = 43.19, longitude = 5.22  WHERE topiaId = 'fr.ird.referential.common.Harbour#11#0.69';
UPDATE common.Harbour SET latitude = 6.08 , longitude = 80.07 WHERE topiaId = 'fr.ird.referential.common.Harbour#1433499238370#0.230351090431213';
UPDATE common.Harbour SET latitude = 4.08 , longitude = 96.08 WHERE topiaId = 'fr.ird.referential.common.Harbour#1479488637407#0.5063164133989542';
UPDATE common.Harbour SET latitude = 3.32 , longitude = 96.56 WHERE topiaId = 'fr.ird.referential.common.Harbour#1479725082165#0.20103941000445025';
UPDATE common.Harbour SET latitude = 4.56 , longitude = -52.2 WHERE topiaId = 'fr.ird.referential.common.Harbour#1473151016115#0.62975807462962';

UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.Harbour';
