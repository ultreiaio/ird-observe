---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- add a new species list PS - Catch
INSERT INTO common.SpeciesList (topiaid, topiaversion, topiacreatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment, lastupdatedate, homeid) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.9', 0, ${CURRENT_DATE}, '8', 1, null, 'PS - Catch species', 'PS - Espèces capturées', 'PS - Catch species', null, null, null, null, null, false, ${CURRENT_TIMESTAMP} , null);
INSERT INTO common.speciesList_species (species, speciesList) (SELECT species , 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.9' FROM common.speciesList_species WHERE speciesList= 'fr.ird.referential.common.SpeciesList#1239832675370#0.1');
INSERT INTO common.speciesList_species (species, speciesList) (SELECT species , 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.9' FROM common.speciesList_species WHERE speciesList= 'fr.ird.referential.common.SpeciesList#1239832675370#0.2');
