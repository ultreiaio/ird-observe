---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- add a new species list PS - Landing
INSERT INTO common.SpeciesList (topiaid, topiaversion, topiacreatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment, lastupdatedate, homeid) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 0, ${CURRENT_DATE}, '9', 1, null, 'PS - Landing species', 'PS - Liste d''espèces des débarquements', 'PS - Landing species', null, null, null, null, null, false, ${CURRENT_TIMESTAMP} , null);
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1239832685474#0.8943253454598569');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1239832685474#0.975344121171992');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1239832685475#0.13349466123905152');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1239832685476#0.5618871286604711');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1239832685477#0.8024257002747615');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1239832685477#0.3846921632590058');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1441287921299#0.016754076421811148');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1239832685478#0.7676744877900202');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1239832685476#0.36339915670317835');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1239832685477#0.2673009297087321');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}0.10', 'fr.ird.referential.common.Species#1239832685477#0.5989181185528589');
