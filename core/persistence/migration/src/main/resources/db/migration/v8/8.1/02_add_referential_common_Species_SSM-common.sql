---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
INSERT INTO common.species (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8, speciesgroup, faocode, scientificlabel, wormsid, minlength, maxlength, minweight, maxweight, sizemeasuretype, weightmeasuretype) VALUES ('fr.ird.referential.common.Species#1635497518451#0.33547599611682555', 1, '2021-10-29 10:50:09.310000', '2020-10-01 00:00:00.000000', null, null, null, false, 1, 'Atlantic Spanish mackerel', 'Thazard atlantique', 'Carite atlántico', null, null, null, null, null, 'fr.ird.referential.common.SpeciesGroup#1239832683689#0.12092280503502995', 'SSM', 'Scomberomorus maculatus', null, 10, 91, 0.1, 6, 'fr.ird.referential.common.SizeMeasureType#1433499465700#0.0902433863375336', 'fr.ird.referential.common.WeightMeasureType#1239832686139#0.2');
INSERT INTO common.species_ocean (species, ocean) VALUES ('fr.ird.referential.common.Species#1635497518451#0.33547599611682555', 'fr.ird.referential.common.Ocean#1239832686151#0.17595105505051245');
