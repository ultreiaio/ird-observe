---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- Update Vessel.wellRegex See https://gitlab.com/ultreiaio/ird-observe/-/issues/2388
UPDATE common.Vessel SET wellRegex = null;
UPDATE common.Vessel SET wellRegex = '^(1A|([1-9]{1}[B;T]{1}|(1{1}[0-9]{1}[B;T]{1}))){1}$', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE vesselType = 'fr.ird.referential.common.VesselType#1239832675735#0.307197212385357';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.Vessel';
