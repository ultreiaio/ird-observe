---*0)-.=*-/*0- ,thjg,k-kioç-(pp) p  PPM! !PP P!PPPPPPPPPPPPPPPPPPP!P!
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =   '1' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#0.1';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =   '2' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#0.3';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =   '3' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#0.5';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =   '4' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#0.7';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =   '5' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#0.9';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =   '6' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#1.1';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =   '7' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#1.3';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =   '8' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#1.5';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =   '9' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#1.7';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '10' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#1.9';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '11' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#2.1';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '12' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#2.3';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '13' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#2.5';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '14' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#2.7';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '15' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#2.9';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '16' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#3.1';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '17' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#3.3';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '18' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#3.5';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '19' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#0.2';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '20' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#0.4';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '21' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#0.6';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '22' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#0.8';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '23' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#1.0';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '24' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#1.2';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '25' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#1.4';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '26' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#1.6';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '27' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#1.8';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '28' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#2.0';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '29' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#2.2';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '30' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#2.4';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '31' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#2.6';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '32' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#2.8';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '33' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#3.0';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '34' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#3.2';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '35' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#3.4';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '36' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#3.6';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '37' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#3.7';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '38' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#3.8';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '39' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#3.9';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '40' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#4.0';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '41' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1239832686122#4.1';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '42' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1464000000000#42';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '43' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1464000000000#43';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId =  '44' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1472549603992#0.4034175948140135';
UPDATE common.FpaZone SET  topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, homeId = '999' WHERE topiaId = 'fr.ird.referential.common.FpaZone#1464000000000#999';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.FpaZone';
