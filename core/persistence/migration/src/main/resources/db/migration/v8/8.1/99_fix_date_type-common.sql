---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

-- Fix some wrong timestamp field should be date - See ultreiaio/ird-observe#1944

ALTER TABLE common.FpaZone ALTER COLUMN endDate TYPE DATE;
ALTER TABLE common.FpaZone ALTER COLUMN startDate TYPE DATE;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN endDate TYPE DATE;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN startDate TYPE DATE;
ALTER TABLE common.LengthWeightParameter ALTER COLUMN endDate TYPE DATE;
ALTER TABLE common.LengthWeightParameter ALTER COLUMN startDate TYPE DATE;
ALTER TABLE common.Program ALTER COLUMN endDate TYPE DATE;
ALTER TABLE common.Program ALTER COLUMN startDate TYPE DATE;
ALTER TABLE common.ShipOwner ALTER COLUMN endDate TYPE DATE;
ALTER TABLE common.ShipOwner ALTER COLUMN startDate TYPE DATE;
ALTER TABLE common.Vessel ALTER COLUMN changeDate TYPE DATE;
ALTER TABLE ll_common.Trip ALTER COLUMN startDate TYPE DATE;
ALTER TABLE ll_common.Trip ALTER COLUMN endDate TYPE DATE;
ALTER TABLE ll_landing.Landing ALTER COLUMN startDate TYPE DATE;
ALTER TABLE ll_landing.Landing ALTER COLUMN endDate TYPE DATE;
