---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- Fix some not null constraints - See #1928

UPDATE common.Species SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, sizeMeasureType = 'fr.ird.referential.common.SizeMeasureType#${REFERENTIAL_PREFIX}99' WHERE sizeMeasureType IS NULL;
UPDATE common.Species SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, label2 = label1 WHERE label2 IS NULL;

-- See https://gitlab.com/ultreiaio/ird-observe/-/issues/2258
-- FIXME Remove this ASAP
-- UPDATE common.Species SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, speciesGroup = 'fr.ird.referential.common.SpeciesGroup#1239832683690#0.15109899841418006' WHERE speciesGroup IS NULL;
-- UPDATE common.GearCharacteristic SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code = '101' WHERE topiaId ='fr.ird.referential.common.GearCharacteristic#1486638173805#0.0030605343198446544';
-- UPDATE common.GearCharacteristic SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code = '102' WHERE topiaId ='fr.ird.referential.common.GearCharacteristic#1486638291965#0.2821012172001982';
-- UPDATE common.SizeMeasureType SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, code = label1 WHERE code IS NULL;
-- UPDATE ps_common.Trip SET ocean ='fr.ird.referential.common.Ocean#1239832686151#0.17595105505051245' WHERE topiaId='fr.ird.data.ps.common.Trip#637683497261934005#0.1' AND ocean IS NULL;

UPDATE common.person SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, country = '${Country_XXX}' WHERE country IS NULL;
UPDATE common.harbour SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, country = '${Country_XXX}' WHERE country IS NULL;
UPDATE common.vesselsizecategory SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, capacitylabel='n/a', gaugelabel='n/a' WHERE code IN ('9', '99');
UPDATE ll_observation.Catch SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, catchFate = 'fr.ird.referential.ll.common.CatchFate#1433499462055#0.71151070506312' WHERE catchFate IS NULL;
UPDATE ps_common.Trip SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, departureHarbour = 'fr.ird.referential.common.Harbour#11#0.71' WHERE departureHarbour IS NULL;
UPDATE ps_observation.activity SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, vesselActivity = 'fr.ird.referential.ps.common.VesselActivity#1239832675372#0.43920247699937853' WHERE vesselActivity IS NULL;
UPDATE ps_observation.ObjectObservedSpecies SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, count = 1 WHERE species IS NOT NULL AND count IS NULL;
DELETE FROM ps_observation.targetCatch WHERE weightCategory IS NULL AND catchweight IS NULL AND well IS NULL AND broughtondeck IS NULL AND reasonfordiscard IS NULL;
DELETE FROM ps_observation.ObjectObservedSpecies WHERE species IS NULL AND count IS NULL AND speciesStatus IS NULL;
DELETE FROM ps_observation.ObjectSchoolEstimate WHERE species IS NULL AND totalWeight IS NULL;
DELETE FROM ll_observation.BranchlinesComposition WHERE topType IS NULL AND tracelineType IS NULL AND length IS NULL;
DELETE FROM ll_observation.SizeMeasure WHERE size IS NULL;
DELETE FROM ps_observation.nonTargetCatch WHERE species IS NULL AND catchweight IS NULL AND meanWeight IS NULL AND totalCount IS NULL AND reasonfordiscard IS NULL AND well IS NULL;
DELETE FROM ps_common.GearUseFeatures WHERE gear IS NULL AND number IS NULL AND usedintrip IS NULL;
DELETE FROM ps_common.GearUseFeaturesMeasurement WHERE measurementvalue IS NULL;
DELETE FROM ll_common.GearUseFeatures WHERE gear IS NULL AND number IS NULL AND usedintrip IS NULL;
DELETE FROM ll_common.GearUseFeaturesMeasurement WHERE measurementvalue IS NULL;

UPDATE ps_observation.NonTargetLength SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, species = 'fr.ird.referential.common.Species#1433499266610#0.696541526820511' WHERE species IS NULL;
UPDATE ps_observation.NonTargetLength SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, count = 1 WHERE count IS NULL AND species IS NOT NULL;
UPDATE ps_observation.NonTargetLength t SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, sizeMeasureType = (SELECT s.sizeMeasureType FROM common.species s WHERE t.species = s.topiaId)  WHERE t.sizeMeasureType IS NULL;
UPDATE ps_observation.TargetLength t SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, sizeMeasureType = (SELECT s.sizeMeasureType FROM common.species s WHERE t.species = s.topiaId)  WHERE t.sizeMeasureType IS NULL;

UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.Person';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.Harbour';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.VesselSizeCategory';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.data.ll.observation.Catch';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.data.ps.common.Trip';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.data.ps.observation.Activity';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.data.ps.observation.ObjectObservedSpecies';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.data.ps.observation.ObjectSchoolEstimate';
