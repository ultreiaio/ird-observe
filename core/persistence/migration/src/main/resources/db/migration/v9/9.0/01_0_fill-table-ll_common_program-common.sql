---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
INSERT INTO ll_common.program(topiaId, topiaVersion, topiaCreateDate, code, homeId, uri, status, label1, label2, label3, label4, label5, label6, label7, label8, startdate, enddate, comment, organism, needComment, lastUpdateDate, observation, logbook) SELECT REPLACE(topiaId, '.common.', '.ll.common.'), topiaVersion, topiaCreateDate, code, homeId, uri, status, label1, label2, label3, label4, label5, label6, label7, label8,  startdate, enddate, comment, organism, needComment, lastUpdateDate, observation, logbook FROM common.program  WHERE gearType = 1;
UPDATE ll_common.Program SET code = '9' WHERE code ='99';
INSERT INTO common.LastUpdateDate(topiaId, topiaversion, topiacreatedate, type , lastUpdateDate) values ('${LAST_UPDATE_PREFIX}2036', 0, ${CURRENT_DATE}, 'fr.ird.observe.entities.referential.ll.common.Program', ${CURRENT_TIMESTAMP});
DELETE FROM common.LastUpdateDate WHERE type = 'fr.ird.observe.entities.referential.common.Program';
