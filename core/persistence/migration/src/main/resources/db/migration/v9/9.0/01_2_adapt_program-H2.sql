---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

ALTER TABLE ll_common.Trip ADD COLUMN observationsProgram VARCHAR(255);
ALTER TABLE ll_common.Trip ADD COLUMN logbookProgram VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN observationsProgram VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN logbookProgram VARCHAR(255);

UPDATE ll_common.Trip SET observationsProgram = REPLACE(program, '.common.', '.ll.common.'), topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP};
UPDATE ps_common.Trip SET observationsProgram = REPLACE(program, '.common.', '.ps.common.'), topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP};

ALTER TABLE ll_common.Trip ADD CONSTRAINT fk_ll_common_Trip_observationsProgram FOREIGN KEY(observationsProgram) REFERENCES ll_common.Program;
ALTER TABLE ll_common.Trip ADD CONSTRAINT fk_ll_common_Trip_logbookProgram  FOREIGN KEY(logbookProgram) REFERENCES ll_common.Program;

ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_observationsProgram FOREIGN KEY(observationsProgram) REFERENCES ps_common.Program;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_logbookProgram  FOREIGN KEY(logbookProgram) REFERENCES ps_common.Program;

CREATE INDEX idx_ll_common_Trip_observationsProgram ON ll_common.Trip(observationsProgram);
CREATE INDEX idx_ll_common_Trip_logbookProgram ON ll_common.Trip(logbookProgram);

CREATE INDEX idx_ps_common_Trip_observationsProgram ON ps_common.Trip(observationsProgram);
CREATE INDEX idx_ps_common_Trip_logbookProgram ON ps_common.Trip(logbookProgram);

UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type LIKE '%ll.common.Trip';
UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type LIKE '%ps.common.Trip';
