---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

INSERT INTO ps_common.SampleType (topiaid, topiaversion, topiacreatedate, lastUpdateDate, needComment, status, code, label1, label2, label3, startDate, endDate, localMarket, logbook) VALUES ('fr.ird.referential.ps.common.SampleType#${REFERENTIAL_PREFIX}01', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 1, '1', 'Débarquement #TODO', 'Débarquement', 'Débarquement #TODO', NULL, NULL, true, true);
INSERT INTO ps_common.SampleType (topiaid, topiaversion, topiacreatedate, lastUpdateDate, needComment, status, code, label1, label2, label3, startDate, endDate, localMarket, logbook) VALUES ('fr.ird.referential.ps.common.SampleType#${REFERENTIAL_PREFIX}02', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 1, '2', 'Observateur #TODO', 'Observateur', 'Observateur #TODO', NULL, NULL, true, true);
INSERT INTO ps_common.SampleType (topiaid, topiaversion, topiacreatedate, lastUpdateDate, needComment, status, code, label1, label2, label3, startDate, endDate, localMarket, logbook) VALUES ('fr.ird.referential.ps.common.SampleType#${REFERENTIAL_PREFIX}03', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 1, '3', 'Poisson trié #TODO', 'Poisson trié', 'Poisson trié #TODO', NULL, NULL, true, true);
INSERT INTO ps_common.SampleType (topiaid, topiaversion, topiacreatedate, lastUpdateDate, needComment, status, code, label1, label2, label3, startDate, endDate, localMarket, logbook) VALUES ('fr.ird.referential.ps.common.SampleType#${REFERENTIAL_PREFIX}04', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 1, '4', 'Types de bancs mélangés #TODO', 'Types de bancs mélangés', 'Types de bancs mélangés #TODO', NULL, NULL, true, false);
INSERT INTO ps_common.SampleType (topiaid, topiaversion, topiacreatedate, lastUpdateDate, needComment, status, code, label1, label2, label3, startDate, endDate, localMarket, logbook) VALUES ('fr.ird.referential.ps.common.SampleType#${REFERENTIAL_PREFIX}09', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 1, '9', 'Unknown', 'Indéterminé', 'Desconocido', NULL, NULL, true, true);
INSERT INTO ps_common.SampleType (topiaid, topiaversion, topiacreatedate, lastUpdateDate, needComment, status, code, label1, label2, label3, startDate, endDate, localMarket, logbook) VALUES ('fr.ird.referential.ps.common.SampleType#${REFERENTIAL_PREFIX}11', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 1, '11', 'Débarquement en frais (canneurs) #TODO', 'Débarquement en frais (canneurs)', 'Débarquement en frais (canneurs) #TODO', NULL, NULL, true, false);

INSERT INTO common.LastUpdateDate(topiaId, topiaversion, topiacreatedate, lastUpdateDate, type) values ('${LAST_UPDATE_PREFIX}2005', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'fr.ird.observe.entities.referential.ps.common.SampleType');
