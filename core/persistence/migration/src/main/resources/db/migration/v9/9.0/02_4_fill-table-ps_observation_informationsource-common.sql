---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

INSERT INTO ps_observation.InformationSource(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label1, label2, label3) values ('fr.ird.referential.ps.observation.InformationSource#${REFERENTIAL_PREFIX}01' , 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 1, false, 'O',      'Observateur #TODO'              , 'Observateur'              , 'Observateur #TODO');
INSERT INTO ps_observation.InformationSource(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label1, label2, label3) values ('fr.ird.referential.ps.observation.InformationSource#${REFERENTIAL_PREFIX}02' , 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 1, false, 'P',      'Equipage avec corrections de l''observateur #TODO'    , 'Equipage avec corrections de l''observateur'    , 'Equipage avec corrections de l''observateur #TODO');
INSERT INTO ps_observation.InformationSource(topiaid, topiaversion, topiacreatedate, lastupdatedate, status, needComment, code, label1, label2, label3) values ('fr.ird.referential.ps.observation.InformationSource#${REFERENTIAL_PREFIX}03' , 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 1, false, 'U',      'Equipage #TODO', 'Equipage', 'Equipage #TODO');

INSERT INTO common.LastUpdateDate(topiaId, topiaversion, topiacreatedate, type , lastUpdateDate) values ('${LAST_UPDATE_PREFIX}2002', 0, ${CURRENT_DATE}, 'fr.ird.observe.entities.referential.ps.observation.InformationSource', ${CURRENT_TIMESTAMP});
