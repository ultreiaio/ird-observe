---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_common.SpeciesFate ADD COLUMN weightRangeAllowed boolean DEFAULT FALSE NOT NULL;

UPDATE ps_common.SpeciesFate SET weightRangeAllowed = TRUE, topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.SpeciesFate#1239832683619#0.5722739932065866';
UPDATE ps_common.SpeciesFate SET weightRangeAllowed = TRUE, topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.SpeciesFate#1239832683619#0.6250731662108877';

UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.data.ps.common.SpeciesFate';
