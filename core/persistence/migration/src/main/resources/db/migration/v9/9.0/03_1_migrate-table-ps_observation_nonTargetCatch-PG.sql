---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
INSERT INTO ps_observation.Catch(topiaId, topiaVersion, topiaCreateDate, homeId, catchWeight, catchWeightComputedSource, meanWeight, meanWeightComputedSource, meanLength, meanLengthComputedSource, totalCount, totalCountComputedSource, comment, reasonForDiscard, species, speciesFate, set, lastUpdateDate, well, set_idx, lengthMeasureMethod, weightMeasureMethod, informationSource) SELECT REPLACE(topiaId, '.NonTargetCatch', '.Catch'), topiaVersion + 1, topiaCreateDate, homeId, catchWeight, catchWeightComputedSource, meanWeight, meanWeightComputedSource, meanLength, meanLengthComputedSource, totalCount, totalCountComputedSource, substr(trim(comment), 0, 8192), reasonForDiscard, species, speciesFate, set, lastUpdateDate, well, set_idx, lengthMeasureMethod, weightMeasureMethod, 'fr.ird.referential.ps.observation.InformationSource#${REFERENTIAL_PREFIX}01' FROM ps_observation.NonTargetCatch WHERE speciesFate IS NOT NULL AND speciesFate != 'fr.ird.referential.ps.common.SpeciesFate#1239832683619#0.5722739932065866';
INSERT INTO ps_observation.Catch(topiaId, topiaVersion, topiaCreateDate, homeId, catchWeight, catchWeightComputedSource, meanWeight, meanWeightComputedSource, meanLength, meanLengthComputedSource, totalCount, totalCountComputedSource, comment, reasonForDiscard, species, speciesFate, set, lastUpdateDate, well, set_idx, lengthMeasureMethod, weightMeasureMethod, informationSource) SELECT REPLACE(topiaId, '.NonTargetCatch', '.Catch'), topiaVersion + 1, topiaCreateDate, homeId, catchWeight, catchWeightComputedSource, meanWeight, meanWeightComputedSource, meanLength, meanLengthComputedSource, totalCount, totalCountComputedSource, substr(trim(comment), 0, 8192), reasonForDiscard, species, 'fr.ird.referential.ps.common.SpeciesFate#${REFERENTIAL_PREFIX}15', set, lastUpdateDate, well, set_idx, lengthMeasureMethod, weightMeasureMethod, 'fr.ird.referential.ps.observation.InformationSource#${REFERENTIAL_PREFIX}01' FROM ps_observation.NonTargetCatch WHERE speciesFate = 'fr.ird.referential.ps.common.SpeciesFate#1239832683619#0.5722739932065866';
INSERT INTO ps_observation.Catch(topiaId, topiaVersion, topiaCreateDate, homeId, catchWeight, catchWeightComputedSource, meanWeight, meanWeightComputedSource, meanLength, meanLengthComputedSource, totalCount, totalCountComputedSource, comment, reasonForDiscard, species, speciesFate, set, lastUpdateDate, well, set_idx, lengthMeasureMethod, weightMeasureMethod, informationSource) SELECT REPLACE(topiaId, '.NonTargetCatch', '.Catch'), topiaVersion + 1, topiaCreateDate, homeId, catchWeight, catchWeightComputedSource, meanWeight, meanWeightComputedSource, meanLength, meanLengthComputedSource, totalCount, totalCountComputedSource, 'Devenir passé de null à ''Autres'' par la migration 9.1', reasonForDiscard, species, 'fr.ird.referential.ps.common.SpeciesFate#1239832683621#0.9099804284263154', set, lastUpdateDate, well, set_idx, lengthMeasureMethod, weightMeasureMethod, 'fr.ird.referential.ps.observation.InformationSource#${REFERENTIAL_PREFIX}01' FROM ps_observation.NonTargetCatch WHERE speciesFate IS NULL AND COMMENT IS NULL;
INSERT INTO ps_observation.Catch(topiaId, topiaVersion, topiaCreateDate, homeId, catchWeight, catchWeightComputedSource, meanWeight, meanWeightComputedSource, meanLength, meanLengthComputedSource, totalCount, totalCountComputedSource, comment, reasonForDiscard, species, speciesFate, set, lastUpdateDate, well, set_idx, lengthMeasureMethod, weightMeasureMethod, informationSource) SELECT REPLACE(topiaId, '.NonTargetCatch', '.Catch'), topiaVersion + 1, topiaCreateDate, homeId, catchWeight, catchWeightComputedSource, meanWeight, meanWeightComputedSource, meanLength, meanLengthComputedSource, totalCount, totalCountComputedSource, CONCAT(substr(trim(comment), 0, 8100), E'\nDevenir passé de null à ''Autres'' par la migration 9.1'), reasonForDiscard, species, 'fr.ird.referential.ps.common.SpeciesFate#1239832683621#0.9099804284263154', set, lastUpdateDate, well, set_idx, lengthMeasureMethod, weightMeasureMethod, 'fr.ird.referential.ps.observation.InformationSource#${REFERENTIAL_PREFIX}01' FROM ps_observation.NonTargetCatch WHERE speciesFate IS NULL AND COMMENT IS NOT NULL;
UPDATE ps_observation.Catch SET weightMeasureMethod = 'fr.ird.referential.common.WeightMeasureMethod#666#03' WHERE weightMeasureMethod IS NULL;
