---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
alter table ps_observation.catch add constraint fk_ps_observation_catch_informationsource foreign key (informationSource) references ps_observation.informationSource;
alter table ps_observation.catch add constraint fk_ps_observation_catch_lengthmeasuremethod foreign key (lengthMeasureMethod) references common.lengthMeasureMethod;
alter table ps_observation.catch add constraint fk_ps_observation_catch_reasonfordiscard foreign key (reasonForDiscard) references ps_observation.reasonForDiscard;
alter table ps_observation.catch add constraint fk_ps_observation_catch_species foreign key (species) references common.species;
alter table ps_observation.catch add constraint fk_ps_observation_catch_speciesfate foreign key (speciesFate) references ps_common.speciesFate;
alter table ps_observation.catch add constraint fk_ps_observation_catch_weightmeasuremethod foreign key (weightMeasureMethod) references common.weightMeasureMethod;
alter table ps_observation.catch add constraint fk_ps_observation_set_catches foreign key (set) references ps_observation.set;
CREATE INDEX idx_ps_observation_catch_informationsource ON ps_observation.catch(informationSource);
CREATE INDEX idx_ps_observation_catch_lastupdatedate ON ps_observation.catch(lastUpdateDate);
CREATE INDEX idx_ps_observation_catch_lengthmeasuremethod ON ps_observation.catch(lengthMeasureMethod);
CREATE INDEX idx_ps_observation_catch_reasonfordiscard ON ps_observation.catch(reasonForDiscard);
CREATE INDEX idx_ps_observation_catch_set ON ps_observation.catch(set);
CREATE INDEX idx_ps_observation_catch_species ON ps_observation.catch(species);
CREATE INDEX idx_ps_observation_catch_speciesfate ON ps_observation.catch(speciesFate);
CREATE INDEX idx_ps_observation_catch_weightmeasuremethod ON ps_observation.catch(weightMeasureMethod);
