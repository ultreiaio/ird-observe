---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
alter table ps_observation.sampleMeasure add constraint fk_ps_observation_sample_samplemeasure foreign key (sample) references ps_observation.sample;
alter table ps_observation.sampleMeasure add constraint fk_ps_observation_samplemeasure_lengthmeasuremethod foreign key (lengthMeasureMethod) references common.lengthMeasureMethod;
alter table ps_observation.sampleMeasure add constraint fk_ps_observation_samplemeasure_sex foreign key (sex) references common.sex;
alter table ps_observation.sampleMeasure add constraint fk_ps_observation_samplemeasure_sizemeasuretype foreign key (sizeMeasureType) references common.sizeMeasureType;
alter table ps_observation.sampleMeasure add constraint fk_ps_observation_samplemeasure_species foreign key (species) references common.species;
alter table ps_observation.sampleMeasure add constraint fk_ps_observation_samplemeasure_speciesfate foreign key (speciesFate) references ps_common.speciesFate;
alter table ps_observation.sampleMeasure add constraint fk_ps_observation_samplemeasure_weightmeasuremethod foreign key (weightMeasureMethod) references common.weightMeasureMethod;
alter table ps_observation.sampleMeasure add constraint fk_ps_observation_samplemeasure_weightmeasuretype foreign key (weightMeasureType) references common.weightMeasureType;
CREATE INDEX idx_ps_observation_samplemeasure_lastupdatedate ON ps_observation.sampleMeasure(lastUpdateDate);
CREATE INDEX idx_ps_observation_samplemeasure_lengthmeasuremethod ON ps_observation.sampleMeasure(lengthMeasureMethod);
CREATE INDEX idx_ps_observation_samplemeasure_sample ON ps_observation.sampleMeasure(sample);
CREATE INDEX idx_ps_observation_samplemeasure_sex ON ps_observation.sampleMeasure(sex);
CREATE INDEX idx_ps_observation_samplemeasure_sizemeasuretype ON ps_observation.sampleMeasure(sizeMeasureType);
CREATE INDEX idx_ps_observation_samplemeasure_species ON ps_observation.sampleMeasure(species);
CREATE INDEX idx_ps_observation_samplemeasure_speciesfate ON ps_observation.sampleMeasure(speciesFate);
CREATE INDEX idx_ps_observation_samplemeasure_weightmeasuremethod ON ps_observation.sampleMeasure(weightMeasureMethod);
CREATE INDEX idx_ps_observation_samplemeasure_weightmeasuretype ON ps_observation.sampleMeasure(weightMeasureType);
