---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_observation.Activity ADD COLUMN reasonForNoFishing2 VARCHAR(255);
UPDATE ps_observation.Activity SET reasonForNoFishing2 = REPLACE(reasonForNoFishing, 'ps.observation', 'ps.common') WHERE reasonForNoFishing IS NOT NULL;
ALTER TABLE ps_observation.Activity DROP COLUMN reasonForNoFishing;
ALTER TABLE ps_observation.Activity ALTER COLUMN reasonForNoFishing2 RENAME TO reasonForNoFishing;
ALTER TABLE ps_observation.Activity ADD CONSTRAINT fk_ps_observation_Activity_reasonForNoFishing FOREIGN KEY(reasonForNoFishing) REFERENCES ps_common.ReasonForNoFishing;
CREATE INDEX idx_ps_observation_Activity_reasonForNoFishing ON ps_observation.Activity(reasonForNoFishing);
