---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_activity_dataquality foreign key (dataQuality) references common.dataQuality;
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_activity_fpazone foreign key (fpaZone) references common.fpaZone;
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_activity_informationsource foreign key (informationSource) references ps_logbook.informationSource;
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_activity_reasonfornofishing foreign key (reasonForNoFishing) references ps_common.reasonForNoFishing;
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_activity_reasonfornullset foreign key (reasonForNullSet) references ps_common.reasonForNullSet;
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_activity_relatedobservedactivity foreign key (relatedObservedActivity) references ps_observation.activity;
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_activity_schooltype foreign key (schoolType) references ps_common.schoolType;
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_activity_setsuccessstatus foreign key (setSuccessStatus) references ps_logbook.setSuccessStatus;
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_activity_vesselactivity foreign key (vesselActivity) references ps_common.vesselActivity;
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_activity_wind foreign key (wind) references common.wind;
alter table if exists ps_logbook.activity add constraint fk_ps_logbook_route_activity foreign key (route) references ps_logbook.route;
alter table if exists ps_logbook.activity_observedSystem add constraint fk_ps_logbook_activity_observedsystem_activity foreign key (activity) references ps_logbook.activity;
alter table if exists ps_logbook.activity_observedSystem add constraint fk_ps_logbook_activity_observedsystem_observedsystem foreign key (observedSystem) references ps_common.observedSystem;
alter table if exists ps_logbook.catch add constraint fk_ps_logbook_activity_catches foreign key (activity) references ps_logbook.activity;
alter table if exists ps_logbook.catch add constraint fk_ps_logbook_catch_species foreign key (species) references common.species;
alter table if exists ps_logbook.catch add constraint fk_ps_logbook_catch_speciesfate foreign key (speciesFate) references ps_common.speciesFate;
alter table if exists ps_logbook.catch add constraint fk_ps_logbook_catch_weightcategory foreign key (weightCategory) references ps_common.weightCategory;
alter table if exists ps_logbook.catch add constraint fk_ps_logbook_catch_weightmeasuremethod foreign key (weightMeasureMethod) references common.weightMeasureMethod;
alter table if exists ps_logbook.floatingObject add constraint fk_ps_logbook_activity_floatingobject foreign key (activity) references ps_logbook.activity;
alter table if exists ps_logbook.floatingObject add constraint fk_ps_logbook_floatingobject_objectoperation foreign key (objectOperation) references ps_common.objectOperation;
alter table if exists ps_logbook.floatingObjectPart add constraint fk_ps_logbook_floatingobject_floatingobjectpart foreign key (floatingObject) references ps_logbook.floatingObject;
alter table if exists ps_logbook.floatingObjectPart add constraint fk_ps_logbook_floatingobjectpart_objectmaterial foreign key (objectMaterial) references ps_common.objectMaterial;
alter table if exists ps_logbook.route add constraint fk_ps_common_trip_routelogbook foreign key (trip) references ps_common.trip;
alter table if exists ps_logbook.sample add constraint fk_ps_common_trip_sample foreign key (trip) references ps_common.trip;
alter table if exists ps_logbook.sample add constraint fk_ps_logbook_sample_samplequality foreign key (sampleQuality) references ps_logbook.sampleQuality;
alter table if exists ps_logbook.sample add constraint fk_ps_logbook_sample_sampletype foreign key (sampleType) references ps_common.sampleType;
alter table if exists ps_logbook.sample_person add constraint fk_ps_logbook_sample_person_person foreign key (person) references common.person;
alter table if exists ps_logbook.sample_person add constraint fk_ps_logbook_sample_person_sample foreign key (sample) references ps_logbook.sample;
alter table if exists ps_logbook.sampleActivity add constraint fk_ps_logbook_sample_sampleactivity foreign key (sample) references ps_logbook.sample;
alter table if exists ps_logbook.sampleActivity add constraint fk_ps_logbook_sampleactivity_activity foreign key (activity) references ps_logbook.activity;
alter table if exists ps_logbook.sampleSpecies add constraint fk_ps_logbook_sample_samplespecies foreign key (sample) references ps_logbook.sample;
alter table if exists ps_logbook.sampleSpecies add constraint fk_ps_logbook_samplespecies_sizemeasuretype foreign key (sizeMeasureType) references common.sizeMeasureType;
alter table if exists ps_logbook.sampleSpecies add constraint fk_ps_logbook_samplespecies_species foreign key (species) references common.species;
alter table if exists ps_logbook.sampleSpeciesMeasure add constraint fk_ps_logbook_samplespecies_samplespeciesmeasure foreign key (sampleSpecies) references ps_logbook.sampleSpecies;
alter table if exists ps_logbook.transmittingBuoy add constraint fk_ps_logbook_floatingobject_transmittingbuoy foreign key (floatingObject) references ps_logbook.floatingObject;
alter table if exists ps_logbook.transmittingBuoy add constraint fk_ps_logbook_transmittingbuoy_country foreign key (country) references common.country;
alter table if exists ps_logbook.transmittingBuoy add constraint fk_ps_logbook_transmittingbuoy_transmittingbuoyoperation foreign key (transmittingBuoyOperation) references ps_common.transmittingBuoyOperation;
alter table if exists ps_logbook.transmittingBuoy add constraint fk_ps_logbook_transmittingbuoy_transmittingbuoyownership foreign key (transmittingBuoyOwnership) references ps_common.transmittingBuoyOwnership;
alter table if exists ps_logbook.transmittingBuoy add constraint fk_ps_logbook_transmittingbuoy_transmittingbuoytype foreign key (transmittingBuoyType) references ps_common.transmittingBuoyType;
alter table if exists ps_logbook.transmittingBuoy add constraint fk_ps_logbook_transmittingbuoy_vessel foreign key (vessel) references common.vessel;
alter table if exists ps_logbook.wellPlan add constraint fk_ps_common_trip_wellplan foreign key (trip) references ps_common.trip;
alter table if exists ps_logbook.wellPlan add constraint fk_ps_logbook_wellplan_activity foreign key (activity) references ps_logbook.activity;
alter table if exists ps_logbook.wellPlan add constraint fk_ps_logbook_wellplan_species foreign key (species) references common.species;
alter table if exists ps_logbook.wellPlan add constraint fk_ps_logbook_wellplan_weightcategory foreign key (weightCategory) references ps_common.weightCategory;
alter table if exists ps_logbook.wellPlan add constraint fk_ps_logbook_wellplan_wellsamplingconformity foreign key (wellSamplingConformity) references ps_logbook.wellSamplingConformity;
alter table if exists ps_logbook.wellPlan add constraint fk_ps_logbook_wellplan_wellsamplingstatus foreign key (wellSamplingStatus) references ps_logbook.wellSamplingStatus;
CREATE INDEX idx_ps_logbook_activity_dataquality ON ps_logbook.activity(dataQuality);
CREATE INDEX idx_ps_logbook_activity_fpazone ON ps_logbook.activity(fpaZone);
CREATE INDEX idx_ps_logbook_activity_informationsource ON ps_logbook.activity(informationSource);
CREATE INDEX idx_ps_logbook_activity_lastupdatedate ON ps_logbook.activity(lastUpdateDate);
CREATE INDEX idx_ps_logbook_activity_observedsystem_activity ON ps_logbook.activity_observedSystem(activity);
CREATE INDEX idx_ps_logbook_activity_observedsystem_observedsystem ON ps_logbook.activity_observedSystem(observedSystem);
CREATE INDEX idx_ps_logbook_activity_reasonfornofishing ON ps_logbook.activity(reasonForNoFishing);
CREATE INDEX idx_ps_logbook_activity_reasonfornullset ON ps_logbook.activity(reasonForNullSet);
CREATE INDEX idx_ps_logbook_activity_relatedobservedactivity ON ps_logbook.activity(relatedObservedActivity);
CREATE INDEX idx_ps_logbook_activity_route ON ps_logbook.activity(route);
CREATE INDEX idx_ps_logbook_activity_schooltype ON ps_logbook.activity(schoolType);
CREATE INDEX idx_ps_logbook_activity_setsuccessstatus ON ps_logbook.activity(setSuccessStatus);
CREATE INDEX idx_ps_logbook_activity_vesselactivity ON ps_logbook.activity(vesselActivity);
CREATE INDEX idx_ps_logbook_activity_wind ON ps_logbook.activity(wind);
CREATE INDEX idx_ps_logbook_catch_activity ON ps_logbook.catch(activity);
CREATE INDEX idx_ps_logbook_catch_lastupdatedate ON ps_logbook.catch(lastUpdateDate);
CREATE INDEX idx_ps_logbook_catch_species ON ps_logbook.catch(species);
CREATE INDEX idx_ps_logbook_catch_speciesfate ON ps_logbook.catch(speciesFate);
CREATE INDEX idx_ps_logbook_catch_weightcategory ON ps_logbook.catch(weightCategory);
CREATE INDEX idx_ps_logbook_catch_weightmeasuremethod ON ps_logbook.catch(weightMeasureMethod);
CREATE INDEX idx_ps_logbook_floatingobject_activity ON ps_logbook.floatingObject(activity);
CREATE INDEX idx_ps_logbook_floatingobject_lastupdatedate ON ps_logbook.floatingObject(lastUpdateDate);
CREATE INDEX idx_ps_logbook_floatingobject_objectoperation ON ps_logbook.floatingObject(objectOperation);
CREATE INDEX idx_ps_logbook_floatingobjectpart_floatingobject ON ps_logbook.floatingObjectPart(floatingObject);
CREATE INDEX idx_ps_logbook_floatingobjectpart_lastupdatedate ON ps_logbook.floatingObjectPart(lastUpdateDate);
CREATE INDEX idx_ps_logbook_floatingobjectpart_objectmaterial ON ps_logbook.floatingObjectPart(objectMaterial);
CREATE INDEX idx_ps_logbook_informationsource_lastupdatedate ON ps_logbook.informationSource(lastUpdateDate);
CREATE INDEX idx_ps_logbook_route_lastupdatedate ON ps_logbook.route(lastUpdateDate);
CREATE INDEX idx_ps_logbook_route_trip ON ps_logbook.route(trip);
CREATE INDEX idx_ps_logbook_sample_lastupdatedate ON ps_logbook.sample(lastUpdateDate);
CREATE INDEX idx_ps_logbook_sample_person_person ON ps_logbook.sample_person(person);
CREATE INDEX idx_ps_logbook_sample_person_sample ON ps_logbook.sample_person(sample);
CREATE INDEX idx_ps_logbook_sample_samplequality ON ps_logbook.sample(sampleQuality);
CREATE INDEX idx_ps_logbook_sample_sampletype ON ps_logbook.sample(sampleType);
CREATE INDEX idx_ps_logbook_sample_trip ON ps_logbook.sample(trip);
CREATE INDEX idx_ps_logbook_sampleactivity_activity ON ps_logbook.sampleActivity(activity);
CREATE INDEX idx_ps_logbook_sampleactivity_lastupdatedate ON ps_logbook.sampleActivity(lastUpdateDate);
CREATE INDEX idx_ps_logbook_sampleactivity_sample ON ps_logbook.sampleActivity(sample);
CREATE INDEX idx_ps_logbook_samplequality_lastupdatedate ON ps_logbook.sampleQuality(lastUpdateDate);
CREATE INDEX idx_ps_logbook_samplespecies_lastupdatedate ON ps_logbook.sampleSpecies(lastUpdateDate);
CREATE INDEX idx_ps_logbook_samplespecies_sample ON ps_logbook.sampleSpecies(sample);
CREATE INDEX idx_ps_logbook_samplespecies_sizemeasuretype ON ps_logbook.sampleSpecies(sizeMeasureType);
CREATE INDEX idx_ps_logbook_samplespecies_species ON ps_logbook.sampleSpecies(species);
CREATE INDEX idx_ps_logbook_samplespeciesmeasure_lastupdatedate ON ps_logbook.sampleSpeciesMeasure(lastUpdateDate);
CREATE INDEX idx_ps_logbook_samplespeciesmeasure_samplespecies ON ps_logbook.sampleSpeciesMeasure(sampleSpecies);
CREATE INDEX idx_ps_logbook_setsuccessstatus_lastupdatedate ON ps_logbook.setSuccessStatus(lastUpdateDate);
CREATE INDEX idx_ps_logbook_transmittingbuoy_country ON ps_logbook.transmittingBuoy(country);
CREATE INDEX idx_ps_logbook_transmittingbuoy_floatingobject ON ps_logbook.transmittingBuoy(floatingObject);
CREATE INDEX idx_ps_logbook_transmittingbuoy_lastupdatedate ON ps_logbook.transmittingBuoy(lastUpdateDate);
CREATE INDEX idx_ps_logbook_transmittingbuoy_transmittingbuoyoperation ON ps_logbook.transmittingBuoy(transmittingBuoyOperation);
CREATE INDEX idx_ps_logbook_transmittingbuoy_transmittingbuoyownership ON ps_logbook.transmittingBuoy(transmittingBuoyOwnership);
CREATE INDEX idx_ps_logbook_transmittingbuoy_transmittingbuoytype ON ps_logbook.transmittingBuoy(transmittingBuoyType);
CREATE INDEX idx_ps_logbook_transmittingbuoy_vessel ON ps_logbook.transmittingBuoy(vessel);
CREATE INDEX idx_ps_logbook_wellcontentstatus_lastupdatedate ON ps_logbook.wellContentStatus(lastUpdateDate);
CREATE INDEX idx_ps_logbook_wellplan_activity ON ps_logbook.wellPlan(activity);
CREATE INDEX idx_ps_logbook_wellplan_lastupdatedate ON ps_logbook.wellPlan(lastUpdateDate);
CREATE INDEX idx_ps_logbook_wellplan_species ON ps_logbook.wellPlan(species);
CREATE INDEX idx_ps_logbook_wellplan_trip ON ps_logbook.wellPlan(trip);
CREATE INDEX idx_ps_logbook_wellplan_weightcategory ON ps_logbook.wellPlan(weightCategory);
CREATE INDEX idx_ps_logbook_wellplan_wellsamplingconformity ON ps_logbook.wellPlan(wellSamplingConformity);
CREATE INDEX idx_ps_logbook_wellplan_wellsamplingstatus ON ps_logbook.wellPlan(wellSamplingStatus);
CREATE INDEX idx_ps_logbook_wellsamplingconformity_lastupdatedate ON ps_logbook.wellSamplingConformity(lastUpdateDate);
CREATE INDEX idx_ps_logbook_wellsamplingstatus_lastupdatedate ON ps_logbook.wellSamplingStatus(lastUpdateDate);
