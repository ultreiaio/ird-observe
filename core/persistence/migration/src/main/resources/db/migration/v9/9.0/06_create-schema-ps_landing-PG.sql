---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE SCHEMA IF NOT EXISTS ps_landing;
create table ps_landing.destination (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, code varchar(255) not null, uri varchar(255), homeId varchar(255), needComment boolean default false not null, status integer not null, label1 varchar(255) not null, label2 varchar(255) not null, label3 varchar(255) not null, label4 varchar(255), label5 varchar(255), label6 varchar(255), label7 varchar(255), label8 varchar(255), primary key (topiaId));
create table ps_landing.fate (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, code varchar(255) not null, uri varchar(255), homeId varchar(255), needComment boolean default false not null, status integer not null, label1 varchar(255) not null, label2 varchar(255) not null, label3 varchar(255) not null, label4 varchar(255), label5 varchar(255), label6 varchar(255), label7 varchar(255), label8 varchar(255), discard boolean not null, primary key (topiaId));
create table ps_landing.landing (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, homeId varchar(255), date date, species varchar(255) not null, weightCategory varchar(255), destination varchar(255), fate varchar(255) not null, fateVessel varchar(255), weight numeric not null, trip varchar(255) not null, trip_idx integer, primary key (topiaId));
