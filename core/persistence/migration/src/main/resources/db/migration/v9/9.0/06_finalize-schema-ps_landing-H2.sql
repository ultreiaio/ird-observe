---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
alter table ps_landing.landing add constraint fk_ps_common_trip_landing foreign key (trip) references ps_common.trip;
alter table ps_landing.landing add constraint fk_ps_landing_landing_destination foreign key (destination) references ps_landing.destination;
alter table ps_landing.landing add constraint fk_ps_landing_landing_fate foreign key (fate) references ps_landing.fate;
alter table ps_landing.landing add constraint fk_ps_landing_landing_fatevessel foreign key (fateVessel) references common.vessel;
alter table ps_landing.landing add constraint fk_ps_landing_landing_species foreign key (species) references common.species;
alter table ps_landing.landing add constraint fk_ps_landing_landing_weightcategory foreign key (weightCategory) references ps_common.weightCategory;
CREATE INDEX idx_ps_landing_destination_lastupdatedate ON ps_landing.destination(lastUpdateDate);
CREATE INDEX idx_ps_landing_fate_lastupdatedate ON ps_landing.fate(lastUpdateDate);
CREATE INDEX idx_ps_landing_landing_destination ON ps_landing.landing(destination);
CREATE INDEX idx_ps_landing_landing_fate ON ps_landing.landing(fate);
CREATE INDEX idx_ps_landing_landing_fatevessel ON ps_landing.landing(fateVessel);
CREATE INDEX idx_ps_landing_landing_lastupdatedate ON ps_landing.landing(lastUpdateDate);
CREATE INDEX idx_ps_landing_landing_species ON ps_landing.landing(species);
CREATE INDEX idx_ps_landing_landing_trip ON ps_landing.landing(trip);
CREATE INDEX idx_ps_landing_landing_weightcategory ON ps_landing.landing(weightCategory);
