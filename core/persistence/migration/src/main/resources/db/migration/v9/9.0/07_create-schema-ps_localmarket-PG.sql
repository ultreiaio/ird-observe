---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE SCHEMA IF NOT EXISTS ps_localmarket;
create table ps_localmarket.batch (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, homeId varchar(255), date date, species varchar(255) not null, packaging varchar(255) not null, count integer, weight numeric, weightComputedSource integer, survey varchar(255), origin varchar(8192), comment varchar(8192), trip varchar(255) not null, primary key (topiaId));
create table ps_localmarket.batchComposition (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, code varchar(255) not null, uri varchar(255), homeId varchar(255), needComment boolean default false not null, status integer not null, label1 varchar(255) not null, label2 varchar(255) not null, label3 varchar(255) not null, label4 varchar(255), label5 varchar(255), label6 varchar(255), label7 varchar(255), label8 varchar(255), primary key (topiaId));
create table ps_localmarket.batchWeightType (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, code varchar(255) not null, uri varchar(255), homeId varchar(255), needComment boolean default false not null, status integer not null, label1 varchar(255) not null, label2 varchar(255) not null, label3 varchar(255) not null, label4 varchar(255), label5 varchar(255), label6 varchar(255), label7 varchar(255), label8 varchar(255), primary key (topiaId));
create table ps_localmarket.packaging (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, code varchar(255) not null, uri varchar(255), homeId varchar(255), needComment boolean default false not null, status integer not null, label1 varchar(255) not null, label2 varchar(255) not null, label3 varchar(255) not null, label4 varchar(255), label5 varchar(255), label6 varchar(255), label7 varchar(255), label8 varchar(255), batchComposition varchar(255) not null, batchWeightType varchar(255) not null, startDate date, endDate date, meanWeight numeric, harbour varchar(255) not null, primary key (topiaId));
create table ps_localmarket.sample (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, homeId varchar(255), number varchar(255) not null, date date, comment varchar(8192), sampleType varchar(255) not null, trip varchar(255) not null, primary key (topiaId));
create table ps_localmarket.sample_well (sample varchar(255) not null, well varchar(255) not null, primary key (sample, well));
create table ps_localmarket.sampleSpecies (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, homeId varchar(255), comment varchar(8192), measuredCount integer not null, species varchar(255) not null, sizeMeasureType varchar(255) not null, sample varchar(255) not null, primary key (topiaId));
create table ps_localmarket.sampleSpeciesMeasure (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, homeId varchar(255), count integer not null, sizeClass numeric not null, sampleSpecies varchar(255) not null, primary key (topiaId));
create table ps_localmarket.survey (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, homeId varchar(255), comment varchar(8192), date date not null, number integer not null, trip varchar(255) not null, primary key (topiaId));
create table ps_localmarket.surveyPart (topiaId varchar(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp not null, lastUpdateDate timestamp default CURRENT_TIMESTAMP not null, homeId varchar(255), species varchar(255) not null, proportion integer not null, survey varchar(255) not null, primary key (topiaId));
