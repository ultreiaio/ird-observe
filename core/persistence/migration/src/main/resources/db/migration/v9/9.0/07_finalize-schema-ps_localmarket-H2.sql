---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
alter table ps_localmarket.batch add constraint fk_ps_common_trip_localmarketbatch foreign key (trip) references ps_common.trip;
alter table ps_localmarket.batch add constraint fk_ps_localmarket_batch_packaging foreign key (packaging) references ps_localmarket.packaging;
alter table ps_localmarket.batch add constraint fk_ps_localmarket_batch_species foreign key (species) references common.species;
alter table ps_localmarket.batch add constraint fk_ps_localmarket_batch_survey foreign key (survey) references ps_localmarket.survey;
alter table ps_localmarket.packaging add constraint fk_ps_localmarket_packaging_batchcomposition foreign key (batchComposition) references ps_localmarket.batchComposition;
alter table ps_localmarket.packaging add constraint fk_ps_localmarket_packaging_batchweighttype foreign key (batchWeightType) references ps_localmarket.batchWeightType;
alter table ps_localmarket.packaging add constraint fk_ps_localmarket_packaging_harbour foreign key (harbour) references common.harbour;
alter table ps_localmarket.sample add constraint fk_ps_common_trip_localmarketsample foreign key (trip) references ps_common.trip;
alter table ps_localmarket.sample add constraint fk_ps_localmarket_sample_sampletype foreign key (sampleType) references ps_common.sampleType;
alter table ps_localmarket.sample_well add constraint fk_ps_localmarket_sample_well_sample foreign key (sample) references ps_localmarket.sample;
alter table ps_localmarket.sampleSpecies add constraint fk_ps_localmarket_sample_samplespecies foreign key (sample) references ps_localmarket.sample;
alter table ps_localmarket.sampleSpecies add constraint fk_ps_localmarket_samplespecies_sizemeasuretype foreign key (sizeMeasureType) references common.sizeMeasureType;
alter table ps_localmarket.sampleSpecies add constraint fk_ps_localmarket_samplespecies_species foreign key (species) references common.species;
alter table ps_localmarket.sampleSpeciesMeasure add constraint fk_ps_localmarket_samplespecies_samplespeciesmeasure foreign key (sampleSpecies) references ps_localmarket.sampleSpecies;
alter table ps_localmarket.survey add constraint fk_ps_common_trip_localmarketsurvey foreign key (trip) references ps_common.trip;
alter table ps_localmarket.surveyPart add constraint fk_ps_localmarket_survey_surveypart foreign key (survey) references ps_localmarket.survey;
alter table ps_localmarket.surveyPart add constraint fk_ps_localmarket_surveypart_species foreign key (species) references common.species;
CREATE INDEX idx_ps_localmarket_batch_lastupdatedate ON ps_localmarket.batch(lastUpdateDate);
CREATE INDEX idx_ps_localmarket_batch_packaging ON ps_localmarket.batch(packaging);
CREATE INDEX idx_ps_localmarket_batch_species ON ps_localmarket.batch(species);
CREATE INDEX idx_ps_localmarket_batch_survey ON ps_localmarket.batch(survey);
CREATE INDEX idx_ps_localmarket_batch_trip ON ps_localmarket.batch(trip);
CREATE INDEX idx_ps_localmarket_batchcomposition_lastupdatedate ON ps_localmarket.batchComposition(lastUpdateDate);
CREATE INDEX idx_ps_localmarket_batchweighttype_lastupdatedate ON ps_localmarket.batchWeightType(lastUpdateDate);
CREATE INDEX idx_ps_localmarket_packaging_batchcomposition ON ps_localmarket.packaging(batchComposition);
CREATE INDEX idx_ps_localmarket_packaging_batchweighttype ON ps_localmarket.packaging(batchWeightType);
CREATE INDEX idx_ps_localmarket_packaging_harbour ON ps_localmarket.packaging(harbour);
CREATE INDEX idx_ps_localmarket_packaging_lastupdatedate ON ps_localmarket.packaging(lastUpdateDate);
CREATE INDEX idx_ps_localmarket_sample_lastupdatedate ON ps_localmarket.sample(lastUpdateDate);
CREATE INDEX idx_ps_localmarket_sample_sampletype ON ps_localmarket.sample(sampleType);
CREATE INDEX idx_ps_localmarket_sample_trip ON ps_localmarket.sample(trip);
CREATE INDEX idx_ps_localmarket_sample_well_sample ON ps_localmarket.sample_well(sample);
CREATE INDEX idx_ps_localmarket_samplespecies_lastupdatedate ON ps_localmarket.sampleSpecies(lastUpdateDate);
CREATE INDEX idx_ps_localmarket_samplespecies_sample ON ps_localmarket.sampleSpecies(sample);
CREATE INDEX idx_ps_localmarket_samplespecies_sizemeasuretype ON ps_localmarket.sampleSpecies(sizeMeasureType);
CREATE INDEX idx_ps_localmarket_samplespecies_species ON ps_localmarket.sampleSpecies(species);
CREATE INDEX idx_ps_localmarket_samplespeciesmeasure_lastupdatedate ON ps_localmarket.sampleSpeciesMeasure(lastUpdateDate);
CREATE INDEX idx_ps_localmarket_samplespeciesmeasure_samplespecies ON ps_localmarket.sampleSpeciesMeasure(sampleSpecies);
CREATE INDEX idx_ps_localmarket_survey_lastupdatedate ON ps_localmarket.survey(lastUpdateDate);
CREATE INDEX idx_ps_localmarket_survey_trip ON ps_localmarket.survey(trip);
CREATE INDEX idx_ps_localmarket_surveypart_lastupdatedate ON ps_localmarket.surveyPart(lastUpdateDate);
CREATE INDEX idx_ps_localmarket_surveypart_species ON ps_localmarket.surveyPart(species);
CREATE INDEX idx_ps_localmarket_surveypart_survey ON ps_localmarket.surveyPart(survey);
