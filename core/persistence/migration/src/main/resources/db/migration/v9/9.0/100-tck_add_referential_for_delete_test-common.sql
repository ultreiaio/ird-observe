---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- Ocean
INSERT INTO common.ocean(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8, northEastAllowed, southEastAllowed, southWestAllowed, northWestAllowed) VALUES ('fr.ird.referential.common.Ocean#1651431107744#0.7000579392148572', '2009-04-15 00:00:00.002', 16, '2021-09-17 00:00:00.0', '4', NULL, NULL, false, 1, 'Med', 'Méditerranée', 'Méditerrano', NULL, NULL, NULL, NULL, NULL, true, false, false, true);
INSERT INTO common.ocean(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8, northEastAllowed, southEastAllowed, southWestAllowed, northWestAllowed) VALUES ('fr.ird.referential.common.Ocean#1651650345031#0.44320492543276846', '2009-04-15 00:00:00.002', 16, '2021-09-17 00:00:00.0', '99', NULL, NULL, false, 1, 'Multiple', 'Multiples', 'múltiple', NULL, NULL, NULL, NULL, NULL, true, true, true, true);
-- Organism
INSERT INTO common.organism(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8, description, country) VALUES ('fr.ird.referential.common.Organism#0#0', '2015-09-03 11:20:43.546', 2, '2018-10-19 13:30:23.388', '92', NULL, NULL, false, 1, 'Mayotte Marine National Parc 2', 'Parc Naturel Marin de Mayotte 2', 'Parc Naturel Marin de Mayotte 2', NULL, NULL, NULL, NULL, NULL, NULL, 'fr.ird.referential.common.Country#1239832675583#0.9493110781716075');
-- Sex
INSERT INTO common.sex(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.Sex#0#0', '2014-07-26 00:00:00.0', 2, '2018-07-23 10:08:18.207', '32', NULL, NULL, false, 0, 'Juvenile 2', 'Juvénile 2', 'Juvenile 2', NULL, NULL, NULL, NULL, NULL);
-- Species
INSERT INTO common.species(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8, faoCode, scientificLabel, wormsId, minLength, maxLength, minWeight, maxWeight, speciesGroup, sizeMeasureType, weightMeasureType) VALUES ('fr.ird.referential.common.Species#0#0', '2019-03-06 11:23:55.155', 1, '2020-10-01 00:00:00.0', NULL, NULL, 'DYL2', false, 1, 'Flying gunnard 2', 'Grondin volant 2', 'Alon volador 2', NULL, NULL, NULL, NULL, NULL, 'DYL2', 'Dactylopterus volitans 2', NULL, 5, 50, 0, 2, 'fr.ird.referential.common.SpeciesGroup#1239832683689#0.12092280503502995', 'fr.ird.referential.common.SizeMeasureType#1433499466532#0.844473292818293', 'fr.ird.referential.common.WeightMeasureType#1239832686139#0.2');
-- SpeciesGroup
INSERT INTO common.speciesGroup(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.SpeciesGroup#0#0', '2009-04-15 00:00:00.001', 17, '2017-02-28 15:04:55.37', '22', NULL, NULL, false, 1, 'Sharks 2', 'Requins 2', 'Tiburones 2', NULL, NULL, NULL, NULL, NULL);
-- SpeciesGroupReleaseMode
INSERT INTO common.speciesGroupReleaseMode(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.SpeciesGroupReleaseMode#0#0', '2018-12-07 00:00:00.0', 2, '2021-09-17 00:00:00.0', '102', NULL, NULL, false, 1, 'Daylight onboard 2', 'jour à bord 2', 'jour à bord TODO 2', NULL, NULL, NULL, NULL, NULL);
-- ObjectMaterialType
INSERT INTO ps_common.objectMaterialType(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.ps.common.ObjectMaterialType#00#0', '2018-12-07 00:00:00.0', 0, '2018-12-07 12:36:58.706996', '42', NULL, NULL, false, 1, 'Text 2', 'Texte 2', 'Texte TODO 2', NULL, NULL, NULL, NULL, NULL);
-- SchoolType
INSERT INTO ps_common.schoolType(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.ps.common.SchoolType#00#0', '2020-10-01 00:00:00.0', 0, '2020-10-01 00:00:00.0', '22', NULL, NULL, false, 1, 'Free school type 2', 'Banc libre 2', 'Banco libre 2', NULL, NULL, NULL, NULL, NULL);
-- BatchComposition
INSERT INTO ps_localmarket.batchComposition(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.ps.localmarket.BatchComposition#0#0', '2021-09-17 00:00:00.0', 0, '2021-09-17 00:00:00.0', '22', NULL, NULL, false, 1, 'Mono specific (no survey) 2', 'Mono spécifique (pas de sondage) 2', 'Mono specific (no survey) #TODO 2', NULL, NULL, NULL, NULL, NULL);
-- BatchWeightType
INSERT INTO ps_localmarket.batchWeightType(topiaId, topiaCreateDate, topiaVersion, lastUpdateDate, code, uri, homeId, needComment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.ps.localmarket.BatchWeightType#0#0', '2021-09-17 00:00:00.0', 0, '2021-09-17 00:00:00.0', '12', NULL, NULL, false, 1, 'Weighing 2', 'Pesée 2', 'Weighing #TODO 2', NULL, NULL, NULL, NULL, NULL);
-- Country
INSERT INTO common.country(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8, iso2code, iso3code) VALUES ('fr.ird.referential.common.Country#0#0', 5, '2011-06-15 00:00:00.043', '2016-11-26 08:35:40.72', '666', NULL, false, 1, 'Tck data', 'Tck data Fr', 'Tck data Es', NULL, NULL, NULL, NULL, NULL, '__', '___');
