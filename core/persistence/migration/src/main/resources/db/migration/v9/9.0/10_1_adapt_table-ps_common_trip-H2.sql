---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_common.Trip ALTER COLUMN comment RENAME TO observationsComment;
ALTER TABLE ps_common.Trip ALTER COLUMN dataEntryOperator RENAME TO observationsDataEntryOperator;
ALTER TABLE ps_common.Trip ALTER COLUMN dataQuality RENAME TO observationsDataQuality;

ALTER TABLE ps_common.Trip ADD COLUMN logbookComment VARCHAR(8192);
ALTER TABLE ps_common.Trip ADD COLUMN generalComment VARCHAR(8192);
ALTER TABLE ps_common.Trip ADD COLUMN timeAtSea INTEGER;
ALTER TABLE ps_common.Trip ADD COLUMN fishingTime INTEGER;
ALTER TABLE ps_common.Trip ADD COLUMN loch INTEGER;
ALTER TABLE ps_common.Trip ADD COLUMN landingTotalWeight DECIMAL;
ALTER TABLE ps_common.Trip ADD COLUMN localMarketTotalWeight DECIMAL;
ALTER TABLE ps_common.Trip ADD COLUMN logbookDataEntryOperator VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN logbookDataQuality VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN departureWellContentStatus VARCHAR (255);
ALTER TABLE ps_common.Trip ADD COLUMN landingWellContentStatus VARCHAR (255);
ALTER TABLE ps_common.Trip ADD COLUMN observationsAcquisitionStatus VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN logbookAcquisitionStatus VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN targetWellsSamplingAcquisitionStatus VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN landingAcquisitionStatus VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN localMarketAcquisitionStatus VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN localMarketWellsSamplingAcquisitionStatus VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN localMarketSurveySamplingAcquisitionStatus VARCHAR(255);
ALTER TABLE ps_common.Trip ADD COLUMN advancedSamplingAcquisitionStatus VARCHAR(255);
ALTER TABLE ps_common.Trip DROP COLUMN checkLevel;

UPDATE ps_common.Trip SET observationsAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#${REFERENTIAL_PREFIX}001', logbookAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#${REFERENTIAL_PREFIX}999', landingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#${REFERENTIAL_PREFIX}999', targetWellsSamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#${REFERENTIAL_PREFIX}999', localMarketAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#${REFERENTIAL_PREFIX}999', localMarketWellsSamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#${REFERENTIAL_PREFIX}999', localMarketSurveySamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#${REFERENTIAL_PREFIX}999', advancedSamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#${REFERENTIAL_PREFIX}999';

ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_departureWellContentStatus FOREIGN KEY(departureWellContentStatus) REFERENCES ps_logbook.WellContentStatus;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_landingWellContentStatus FOREIGN KEY(landingWellContentStatus) REFERENCES ps_logbook.WellContentStatus;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_observationsDataEntryOperator FOREIGN KEY(observationsDataEntryOperator) REFERENCES common.Person;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_logbookDataEntryOperator FOREIGN KEY(logbookDataQuality) REFERENCES common.DataQuality;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_observationsDataQuality FOREIGN KEY(observationsDataEntryOperator) REFERENCES common.Person;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_logbookDataQuality FOREIGN KEY(logbookDataQuality) REFERENCES common.DataQuality;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_observationsAcquisitionStatus  FOREIGN KEY(observationsAcquisitionStatus) REFERENCES ps_common.AcquisitionStatus;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_logbookAcquisitionStatus  FOREIGN KEY(logbookAcquisitionStatus) REFERENCES ps_common.AcquisitionStatus;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_targetWellsSamplingAcquisitionStatus  FOREIGN KEY(targetWellsSamplingAcquisitionStatus) REFERENCES ps_common.AcquisitionStatus;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_landingAcquisitionStatus  FOREIGN KEY(landingAcquisitionStatus) REFERENCES ps_common.AcquisitionStatus;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_localMarketAcquisitionStatus  FOREIGN KEY(localMarketAcquisitionStatus) REFERENCES ps_common.AcquisitionStatus;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_localMarketWellsSamplingAcquisitionStatus  FOREIGN KEY(localMarketWellsSamplingAcquisitionStatus) REFERENCES ps_common.AcquisitionStatus;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_localMarketSurveySamplingAcquisitionStatus  FOREIGN KEY(localMarketSurveySamplingAcquisitionStatus) REFERENCES ps_common.AcquisitionStatus;
ALTER TABLE ps_common.Trip ADD CONSTRAINT fk_ps_common_Trip_advancedSamplingAcquisitionStatus  FOREIGN KEY(advancedSamplingAcquisitionStatus) REFERENCES ps_common.AcquisitionStatus;

CREATE INDEX idx_ps_common_Trip_departureWellContentStatus ON ps_common.Trip(departureWellContentStatus);
CREATE INDEX idx_ps_common_Trip_landingWellContentStatus ON ps_common.Trip(landingWellContentStatus);
CREATE INDEX idx_ps_common_Trip_observationsDataEntryOperator ON ps_common.Trip(observationsDataEntryOperator);
CREATE INDEX idx_ps_common_Trip_logbookDataEntryOperator ON ps_common.Trip(logbookDataEntryOperator);
CREATE INDEX idx_ps_common_Trip_observationsDataQuality ON ps_common.Trip(observationsDataQuality);
CREATE INDEX idx_ps_common_Trip_logbookDataQuality ON ps_common.Trip(logbookDataQuality);
CREATE INDEX idx_ps_common_Trip_observationsAcquisitionStatus ON ps_common.Trip(observationsAcquisitionStatus);
CREATE INDEX idx_ps_common_Trip_landingAcquisitionStatus ON ps_common.Trip(landingAcquisitionStatus);
CREATE INDEX idx_ps_common_Trip_logbookAcquisitionStatus ON ps_common.Trip(logbookAcquisitionStatus);
CREATE INDEX idx_ps_common_Trip_targetWellsSamplingAcquisitionStatus ON ps_common.Trip(targetWellsSamplingAcquisitionStatus);
CREATE INDEX idx_ps_common_Trip_localMarketAcquisitionStatus ON ps_common.Trip(localMarketAcquisitionStatus);
CREATE INDEX idx_ps_common_Trip_localMarketWellsSamplingAcquisitionStatus ON ps_common.Trip(localMarketWellsSamplingAcquisitionStatus);
CREATE INDEX idx_ps_common_Trip_localMarketSurveySamplingAcquisitionStatus ON ps_common.Trip(localMarketSurveySamplingAcquisitionStatus);
CREATE INDEX idx_ps_common_Trip_advancedSamplingAcquisitionStatus ON ps_common.Trip(advancedSamplingAcquisitionStatus);
