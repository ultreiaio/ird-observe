---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
UPDATE ps_common.TransmittingBuoyOwnership SET label1 = 'Unknown vessel', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyOwnership#0#0';
UPDATE ps_common.TransmittingBuoyOwnership SET label1 = 'This vessel or its company', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyOwnership#0#1';
UPDATE ps_common.TransmittingBuoyOwnership SET label1 = 'Other vessel from another company', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyOwnership#0#2';
UPDATE ps_common.TransmittingBuoyOwnership SET label1 = 'Other vessel from the same company', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyOwnership#0#4';
UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOwnership';

UPDATE ps_common.TransmittingBuoyOperation SET label1 = 'Visit', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyOperation#1239832686237#0.4947461794167761';
UPDATE ps_common.TransmittingBuoyOperation SET label1 = 'Grab', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyOperation#1239832686238#0.38090479793636556';
UPDATE ps_common.TransmittingBuoyOperation SET label1 = 'Deployment', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyOperation#1239832686238#0.4755624782839416';
UPDATE ps_common.TransmittingBuoyOperation SET label1 = 'End of signal', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyOperation#1464000000000#4';
UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.ps.common.TransmittingBuoyOperation';

UPDATE ps_common.ObjectOperation SET label1 = 'Loss', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.ps.common.ObjectOperation#1464000000000#11';
UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.ps.common.ObjectOperation';

UPDATE ps_common.TransmittingBuoyType SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'GPS finder' WHERE topiaid = 'fr.ird.referential.ps.common.TransmittingBuoyType#1464000000000#100';
UPDATE ps_common.TransmittingBuoyType SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'None' WHERE topiaid = 'fr.ird.referential.ps.common.TransmittingBuoyType#1464000000000#999';
UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.ps.common.TransmittingBuoyType';

UPDATE ps_logbook.SampleQuality SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Super - T3', label2 = 'Super - T3'            WHERE topiaid = 'fr.ird.referential.ps.logbook.SampleQuality#1464000000000#01';
UPDATE ps_logbook.SampleQuality SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Biology - Size' WHERE topiaid = 'fr.ird.referential.ps.logbook.SampleQuality#1464000000000#02';
UPDATE ps_logbook.SampleQuality SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Biology'         WHERE topiaid = 'fr.ird.referential.ps.logbook.SampleQuality#1464000000000#03';
UPDATE ps_logbook.SampleQuality SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Bad'           WHERE topiaid = 'fr.ird.referential.ps.logbook.SampleQuality#1464000000000#04';
UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.ps.logbook.SampleQuality';

UPDATE ps_observation.InformationSource SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Observer'                                   WHERE topiaid = 'fr.ird.referential.ps.observation.InformationSource#1464000000000#01';
UPDATE ps_observation.InformationSource SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Crew with update by observer' WHERE topiaid = 'fr.ird.referential.ps.observation.InformationSource#1464000000000#02';
UPDATE ps_observation.InformationSource SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Crew'                                      WHERE topiaid = 'fr.ird.referential.ps.observation.InformationSource#1464000000000#03';
UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.ps.observation.InformationSource';

UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Damage'                                 WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#22';
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Bait fishing'                                  WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#23';
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Waiting'                                WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#24';
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Offshore transshipment (details unknown)' WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#25';
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Transshipment from a purse seiner'       WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#26';
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Transshipment to a purse seiner'         WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#27';
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Capsizing of the pursed seine net'                  WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#28';
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Transshipment to a bait boat'         WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#29';
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Transshipment from a bait boat'       WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#30';
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Fish discard'                       WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#31';
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Activity/set created from the well plan'    WHERE topiaid = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#32';
UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.ps.common.VesselActivity';

UPDATE ps_common.SampleType SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'At landing'                     WHERE topiaid = 'fr.ird.referential.ps.common.SampleType#1464000000000#01';
UPDATE ps_common.SampleType SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'By observer'                      WHERE topiaid = 'fr.ird.referential.ps.common.SampleType#1464000000000#02';
UPDATE ps_common.SampleType SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Fish sorted'                     WHERE topiaid = 'fr.ird.referential.ps.common.SampleType#1464000000000#03';
UPDATE ps_common.SampleType SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Mixed school types'          WHERE topiaid = 'fr.ird.referential.ps.common.SampleType#1464000000000#04';
UPDATE ps_common.SampleType SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'At landing - fresh fish from bait boat' WHERE topiaid = 'fr.ird.referential.ps.common.SampleType#1464000000000#11';
UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.ps.common.SampleType';

UPDATE common.SpeciesGroupReleaseMode SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'With brail'                              WHERE topiaid = 'fr.ird.referential.common.SpeciesGroupReleaseMode#0#1';
UPDATE common.SpeciesGroupReleaseMode SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'By stretcher, tarp, sarria, cargo net' WHERE topiaid = 'fr.ird.referential.common.SpeciesGroupReleaseMode#0#2';
UPDATE common.SpeciesGroupReleaseMode SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'With specific device'                 WHERE topiaid = 'fr.ird.referential.common.SpeciesGroupReleaseMode#0#3';
UPDATE common.SpeciesGroupReleaseMode SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'By hand from the deck'                   WHERE topiaid = 'fr.ird.referential.common.SpeciesGroupReleaseMode#0#4';
UPDATE common.SpeciesGroupReleaseMode SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'After disentanglement'                           WHERE topiaid = 'fr.ird.referential.common.SpeciesGroupReleaseMode#0#5';
UPDATE common.SpeciesGroupReleaseMode SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Submergence of floaters'                   WHERE topiaid = 'fr.ird.referential.common.SpeciesGroupReleaseMode#0#7';
UPDATE common.SpeciesGroupReleaseMode SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Cut in the net'                     WHERE topiaid = 'fr.ird.referential.common.SpeciesGroupReleaseMode#0#8';
UPDATE common.SpeciesGroupReleaseMode SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Once remaining plastic pieces were removed'    WHERE topiaid = 'fr.ird.referential.common.SpeciesGroupReleaseMode#0#9';
UPDATE common.SpeciesGroupReleaseMode SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, label1 = 'Daylight onboard'                                WHERE topiaid = 'fr.ird.referential.common.SpeciesGroupReleaseMode#0#10';
UPDATE common.LASTUPDATEDATE SET  lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.common.SpeciesGroupReleaseMode';
