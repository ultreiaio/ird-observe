---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE common.Ocean ADD COLUMN northEastAllowed boolean default false not null;
ALTER TABLE common.Ocean ADD COLUMN southEastAllowed boolean default false not null;
ALTER TABLE common.Ocean ADD COLUMN southWestAllowed boolean default false not null;
ALTER TABLE common.Ocean ADD COLUMN northWestAllowed boolean default false not null;

-- For Ocean 1 ATL
UPDATE common.Ocean set lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, northEastAllowed = TRUE, southEastAllowed = TRUE, southWestAllowed = TRUE,  northWestAllowed = TRUE WHERE topiaId = 'fr.ird.referential.common.Ocean#1239832686151#0.17595105505051245';
-- For Ocean 2 Indian
UPDATE common.Ocean set lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, northEastAllowed = TRUE, southEastAllowed = TRUE WHERE topiaId = 'fr.ird.referential.common.Ocean#1239832686152#0.8325731048817705';
-- For Ocean 3 Pacific
UPDATE common.Ocean set lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, northEastAllowed = TRUE, southEastAllowed = TRUE, southWestAllowed = TRUE,  northWestAllowed = TRUE WHERE topiaId = 'fr.ird.referential.common.Ocean#1239832686152#0.7039171539191688';
-- For Ocean 4
UPDATE common.Ocean set lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, northEastAllowed = TRUE, northWestAllowed = TRUE WHERE topiaId = 'fr.ird.referential.common.Ocean#1651431107744#0.7000579392148572';
-- For Ocean 99
UPDATE common.Ocean set lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, northEastAllowed = TRUE, southEastAllowed = TRUE, southWestAllowed = TRUE,  northWestAllowed = TRUE WHERE topiaId = 'fr.ird.referential.common.Ocean#1651650345031#0.44320492543276846';

UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.Ocean';
