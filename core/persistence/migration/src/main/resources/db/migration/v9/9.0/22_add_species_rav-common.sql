---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- See https://gitlab.com/ultreiaio/ird-observe/-/issues/2421
INSERT INTO common.species (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8, speciesgroup, faocode, scientificlabel, wormsid, minlength, maxlength, minweight, maxweight, sizemeasuretype, weightmeasuretype) VALUES ('fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 1, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '43', null, null, false, 0, 'Ravil', 'Ravil', 'Ravil', null, null, null, null, null, 'fr.ird.referential.common.SpeciesGroup#1433499401278#0.203439718578011', 'RAV*', 'Ravil', null, null, null, null, null, null, null);
INSERT INTO common.species_ocean (species, ocean) VALUES ('fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 'fr.ird.referential.common.Ocean#1239832686151#0.17595105505051245');
INSERT INTO common.species_ocean (species, ocean) VALUES ('fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 'fr.ird.referential.common.Ocean#1239832686152#0.8325731048817705');
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.common.Species';
INSERT INTO ps_common.WeightCategory(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, species, label1, label2, label3, minWeight, meanWeight, maxWeight, allowLanding ,allowLogbook ,allowWellPlan) VALUES ( 'fr.ird.referential.ps.common.WeightCategory#${REFERENTIAL_PREFIX}1077', 0, 'C-RAV*-1', 1, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 'fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 'RAV moins de 1.8 kg', 'RAV moins de 1.8 kg', 'RAV moins de 1.8 kg', NULL, NULL, NULL, false, true, false);
INSERT INTO ps_common.WeightCategory(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, species, label1, label2, label3, minWeight, meanWeight, maxWeight, allowLanding ,allowLogbook ,allowWellPlan) VALUES ( 'fr.ird.referential.ps.common.WeightCategory#${REFERENTIAL_PREFIX}1078', 0, 'C-RAV*-2', 1, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 'fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 'RAV plus de 1.8 kg', 'RAV plus de 1.8 kg', 'RAV plus de 1.8 kg', NULL, NULL, NULL, false, true, false);
INSERT INTO ps_common.WeightCategory(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, species, label1, label2, label3, minWeight, meanWeight, maxWeight, allowLanding ,allowLogbook ,allowWellPlan) VALUES ( 'fr.ird.referential.ps.common.WeightCategory#${REFERENTIAL_PREFIX}1079', 0, 'C-RAV*-3', 1, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 'fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 'RAV 1.8 à 4 kg', 'RAV 1.8 à 4 kg', 'RAV 1.8 à 4 kg', NULL, NULL, NULL, false, true, false);
INSERT INTO ps_common.WeightCategory(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, species, label1, label2, label3, minWeight, meanWeight, maxWeight, allowLanding ,allowLogbook ,allowWellPlan) VALUES ( 'fr.ird.referential.ps.common.WeightCategory#${REFERENTIAL_PREFIX}1080', 0, 'C-RAV*-4', 1, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 'fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 'RAV 1.8 à 6 kg', 'RAV 1.8 à 6 kg', 'RAV 1.8 à 6 kg', NULL, NULL, NULL, false, true, false);
INSERT INTO ps_common.WeightCategory(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, species, label1, label2, label3, minWeight, meanWeight, maxWeight, allowLanding ,allowLogbook ,allowWellPlan) VALUES ( 'fr.ird.referential.ps.common.WeightCategory#${REFERENTIAL_PREFIX}1081', 0, 'C-RAV*-5', 1, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 'fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 'RAV 4 à 6 kg', 'RAV 4 à 6 kg', 'RAV 4 à 6 kg', NULL, NULL, NULL, false, true, false);
INSERT INTO ps_common.WeightCategory(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, species, label1, label2, label3, minWeight, meanWeight, maxWeight, allowLanding ,allowLogbook ,allowWellPlan) VALUES ( 'fr.ird.referential.ps.common.WeightCategory#${REFERENTIAL_PREFIX}1082', 0, 'C-RAV*-6', 1, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 'fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 'RAV 4 à 8 kg', 'RAV 4 à 8 kg', 'RAV 4 à 8 kg', NULL, NULL, NULL, false, true, false);
INSERT INTO ps_common.WeightCategory(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, species, label1, label2, label3, minWeight, meanWeight, maxWeight, allowLanding ,allowLogbook ,allowWellPlan) VALUES ( 'fr.ird.referential.ps.common.WeightCategory#${REFERENTIAL_PREFIX}1083', 0, 'C-RAV*-7', 1, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 'fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 'RAV 6 à 8 kg', 'RAV 6 à 8 kg', 'RAV 6 à 8 kg', NULL, NULL, NULL, false, true, false);
INSERT INTO ps_common.WeightCategory(topiaId, topiaVersion, code, status, topiaCreateDate, lastUpdateDate, needComment, species, label1, label2, label3, minWeight, meanWeight, maxWeight, allowLanding ,allowLogbook ,allowWellPlan) VALUES ( 'fr.ird.referential.ps.common.WeightCategory#${REFERENTIAL_PREFIX}1084', 0, 'C-RAV*-8', 1, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, false, 'fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43', 'RAV plus de 8 kg', 'RAV plus de 8 kg', 'RAV plus de 8 kg', NULL, NULL, NULL, false, true, false);
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.ps.common.WeightCategory';
-- See https://gitlab.com/ultreiaio/ird-observe/-/issues/2425
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#1464000000000#0.9', 'fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43');
INSERT INTO common.speciesList_species (speciesList, species) VALUES ('fr.ird.referential.common.SpeciesList#1464000000000#0.10', 'fr.ird.referential.common.Species#${REFERENTIAL_PREFIX}43');
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.common.SpeciesList';
