---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_observation.Set ADD COLUMN activity VARCHAR(255);
ALTER TABLE ps_observation.Set ADD CONSTRAINT fk_ps_observation_Set_activity FOREIGN KEY(activity) REFERENCES ps_observation.Activity;
CREATE INDEX idx_ps_observation_set_activity ON ps_observation.Set(activity);

ALTER TABLE ll_observation.Set ADD COLUMN activity VARCHAR(255);
ALTER TABLE ll_observation.Set ADD CONSTRAINT fk_ll_observation_Set_activity FOREIGN KEY(activity) REFERENCES ll_observation.Activity;
CREATE INDEX idx_ll_observation_set_activity ON ll_observation.Set(activity);

ALTER TABLE ll_logbook.Set ADD COLUMN activity VARCHAR(255);
ALTER TABLE ll_logbook.Set ADD CONSTRAINT fk_ll_logbook_Set_activity FOREIGN KEY(activity) REFERENCES ll_logbook.Activity;
CREATE INDEX idx_ll_logbook_set_activity ON ll_logbook.Set(activity);

ALTER TABLE ll_logbook.Sample ADD COLUMN activity VARCHAR(255);
ALTER TABLE ll_logbook.Sample ADD CONSTRAINT fk_ll_logbook_Sample_activity FOREIGN KEY(activity) REFERENCES ll_logbook.Activity;
CREATE INDEX idx_ll_logbook_sample_activity ON ll_logbook.Sample(activity);
