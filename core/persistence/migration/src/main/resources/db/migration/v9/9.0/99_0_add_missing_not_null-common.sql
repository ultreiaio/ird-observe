---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE common.Country ALTER COLUMN iso2Code SET NOT NULL;
ALTER TABLE common.Country ALTER COLUMN iso3Code SET NOT NULL;
ALTER TABLE common.Harbour ALTER COLUMN country SET NOT NULL;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN coefficients SET NOT NULL;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN inputOutputFormula SET NOT NULL;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN inputSizeMeasureType SET NOT NULL;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN ocean SET NOT NULL;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN outputInputFormula SET NOT NULL;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN outputSizeMeasureType SET NOT NULL;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN sex SET NOT NULL;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN species SET NOT NULL;
ALTER TABLE common.LengthWeightParameter ALTER COLUMN coefficients SET NOT NULL;
ALTER TABLE common.LengthWeightParameter ALTER COLUMN lengthWeightFormula SET NOT NULL;
ALTER TABLE common.LengthWeightParameter ALTER COLUMN ocean SET NOT NULL;
ALTER TABLE common.LengthWeightParameter ALTER COLUMN sex SET NOT NULL;
ALTER TABLE common.LengthWeightParameter ALTER COLUMN sizeMeasureType SET NOT NULL;
ALTER TABLE common.LengthWeightParameter ALTER COLUMN species SET NOT NULL;
ALTER TABLE common.LengthWeightParameter ALTER COLUMN weightLengthFormula SET NOT NULL;
ALTER TABLE common.Organism ALTER COLUMN country SET NOT NULL;
ALTER TABLE common.Person ALTER COLUMN captain SET NOT NULL;
ALTER TABLE common.Person ALTER COLUMN country SET NOT NULL;
ALTER TABLE common.Person ALTER COLUMN dataEntryOperator SET NOT NULL;
ALTER TABLE common.Person ALTER COLUMN dataSource SET NOT NULL;
ALTER TABLE common.Person ALTER COLUMN observer SET NOT NULL;
ALTER TABLE common.Person ALTER COLUMN psSampler SET NOT NULL;
ALTER TABLE common.Person ALTER COLUMN country SET NOT NULL;
ALTER TABLE common.Person ALTER COLUMN firstName SET NOT NULL;
ALTER TABLE common.Person ALTER COLUMN lastName SET NOT NULL;
ALTER TABLE common.Program ALTER COLUMN logbook SET NOT NULL;
ALTER TABLE common.Program ALTER COLUMN observation SET NOT NULL;
ALTER TABLE common.Program ALTER COLUMN organism SET NOT NULL;
ALTER TABLE common.Program ALTER COLUMN gearType SET NOT NULL;
ALTER TABLE common.Program ALTER COLUMN organism SET NOT NULL;
ALTER TABLE common.ShipOwner ALTER COLUMN label SET NOT NULL;
ALTER TABLE common.Species ALTER COLUMN scientificLabel SET NOT NULL;
ALTER TABLE common.Species ALTER COLUMN speciesGroup SET NOT NULL;
ALTER TABLE common.Vessel ALTER COLUMN flagCountry SET NOT NULL;
ALTER TABLE common.Vessel ALTER COLUMN fleetCountry SET NOT NULL;
ALTER TABLE common.Vessel ALTER COLUMN vesselSizeCategory SET NOT NULL;
ALTER TABLE common.Vessel ALTER COLUMN vesselType SET NOT NULL;
ALTER TABLE common.VesselSizeCategory ALTER COLUMN capacityLabel SET NOT NULL;
ALTER TABLE common.VesselSizeCategory ALTER COLUMN gaugeLabel SET NOT NULL;

ALTER TABLE ll_common.GearUseFeatures ALTER COLUMN gear SET NOT NULL;
ALTER TABLE ll_common.GearUseFeatures ALTER COLUMN number SET NOT NULL;
ALTER TABLE ll_common.GearUseFeaturesMeasurement ALTER COLUMN gearCharacteristic SET NOT NULL;
ALTER TABLE ll_common.GearUseFeaturesMeasurement ALTER COLUMN measurementValue SET NOT NULL;
ALTER TABLE ll_common.Trip ALTER COLUMN endDate SET NOT NULL;
ALTER TABLE ll_common.Trip ALTER COLUMN logbookAvailability SET NOT NULL;
ALTER TABLE ll_common.Trip ALTER COLUMN observationsAvailability SET NOT NULL;
ALTER TABLE ll_common.Trip ALTER COLUMN ocean SET NOT NULL;
ALTER TABLE ll_common.Trip ALTER COLUMN startDate SET NOT NULL;
ALTER TABLE ll_common.Trip ALTER COLUMN tripType SET NOT NULL;
ALTER TABLE ll_common.Trip ALTER COLUMN vessel SET NOT NULL;
ALTER TABLE ll_landing.Landing ALTER COLUMN harbour SET NOT NULL;
ALTER TABLE ll_landing.LandingPart ALTER COLUMN species SET NOT NULL;
ALTER TABLE ll_logbook.Activity ALTER COLUMN startTimeStamp SET NOT NULL;
ALTER TABLE ll_logbook.Activity ALTER COLUMN vesselActivity SET NOT NULL;
ALTER TABLE ll_logbook.BaitsComposition ALTER COLUMN baitType SET NOT NULL;
ALTER TABLE ll_logbook.BranchlinesComposition ALTER COLUMN length SET NOT NULL;
ALTER TABLE ll_logbook.BranchlinesComposition ALTER COLUMN topType SET NOT NULL;
ALTER TABLE ll_logbook.BranchlinesComposition ALTER COLUMN tracelineType SET NOT NULL;
ALTER TABLE ll_logbook.Catch ALTER COLUMN catchFate SET NOT NULL;
ALTER TABLE ll_logbook.Catch ALTER COLUMN species SET NOT NULL;
ALTER TABLE ll_logbook.FloatlinesComposition ALTER COLUMN length SET NOT NULL;
ALTER TABLE ll_logbook.FloatlinesComposition ALTER COLUMN lineType SET NOT NULL;
ALTER TABLE ll_logbook.HooksComposition ALTER COLUMN hookType SET NOT NULL;
-- See https://gitlab.com/ultreiaio/ird-observe/-/issues/2373
ALTER TABLE ll_logbook.BaitsComposition ALTER COLUMN proportion DROP NOT NULL;
ALTER TABLE ll_logbook.BranchlinesComposition ALTER COLUMN proportion DROP NOT NULL;
ALTER TABLE ll_logbook.FloatlinesComposition ALTER COLUMN proportion DROP NOT NULL;
ALTER TABLE ll_logbook.HooksComposition ALTER COLUMN proportion DROP NOT NULL;

ALTER TABLE ll_logbook.Sample ALTER COLUMN timeStamp SET NOT NULL;
ALTER TABLE ll_logbook.SamplePart ALTER COLUMN count SET NOT NULL;
ALTER TABLE ll_logbook.SamplePart ALTER COLUMN species SET NOT NULL;
ALTER TABLE ll_logbook.Set ALTER COLUMN settingStartTimeStamp SET NOT NULL;
ALTER TABLE ll_observation.Activity ALTER COLUMN latitude SET NOT NULL;
ALTER TABLE ll_observation.Activity ALTER COLUMN longitude SET NOT NULL;
ALTER TABLE ll_observation.Activity ALTER COLUMN timeStamp SET NOT NULL;
ALTER TABLE ll_observation.Activity ALTER COLUMN vesselActivity SET NOT NULL;
ALTER TABLE ll_observation.BaitsComposition ALTER COLUMN baitType SET NOT NULL;
ALTER TABLE ll_observation.Basket ALTER COLUMN settingIdentifier SET NOT NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN settingIdentifier SET NOT NULL;
ALTER TABLE ll_observation.BranchlinesComposition ALTER COLUMN length SET NOT NULL;
ALTER TABLE ll_observation.BranchlinesComposition ALTER COLUMN topType SET NOT NULL;
ALTER TABLE ll_observation.BranchlinesComposition ALTER COLUMN tracelineType SET NOT NULL;
ALTER TABLE ll_observation.Catch ALTER COLUMN catchFate SET NOT NULL;
ALTER TABLE ll_observation.Catch ALTER COLUMN species SET NOT NULL;
ALTER TABLE ll_observation.Encounter ALTER COLUMN encounterType SET NOT NULL;
ALTER TABLE ll_observation.FloatlinesComposition ALTER COLUMN length SET NOT NULL;
ALTER TABLE ll_observation.FloatlinesComposition ALTER COLUMN lineType SET NOT NULL;
ALTER TABLE ll_observation.HooksComposition ALTER COLUMN hookType SET NOT NULL;
ALTER TABLE ll_observation.Section ALTER COLUMN settingIdentifier SET NOT NULL;
ALTER TABLE ll_observation.SensorBrand ALTER COLUMN brandName SET NOT NULL;
ALTER TABLE ll_observation.SensorUsed ALTER COLUMN sensorType SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN basketsPerSectionCount SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN branchlinesPerBasketCount SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN haulingDirectionSameAsSetting SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN haulingEndLatitude SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN haulingEndLongitude SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN haulingEndTimeStamp SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN haulingStartLatitude SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN haulingStartLongitude SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN haulingStartTimeStamp SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN settingEndLatitude SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN settingEndLongitude SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN settingEndTimeStamp SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN settingStartLatitude SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN settingStartLongitude SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN settingStartTimeStamp SET NOT NULL;
ALTER TABLE ll_observation.SizeMeasure ALTER COLUMN size SET NOT NULL;
ALTER TABLE ll_observation.SizeMeasure ALTER COLUMN sizeMeasureType SET NOT NULL;
ALTER TABLE ll_observation.WeightMeasure ALTER COLUMN weight SET NOT NULL;
ALTER TABLE ll_observation.WeightMeasure ALTER COLUMN weightMeasureType SET NOT NULL;
ALTER TABLE ll_observation.SizeMeasure DROP COLUMN lengthMeasureMethod;
ALTER TABLE ll_observation.WeightMeasure DROP COLUMN weightMeasureMethod;
ALTER TABLE ps_common.AcquisitionStatus ALTER COLUMN fieldEnabler SET NOT NULL;
ALTER TABLE ps_common.AcquisitionStatus ALTER COLUMN logbook SET NOT NULL;
ALTER TABLE ps_common.AcquisitionStatus ALTER COLUMN observation SET NOT NULL;
ALTER TABLE ps_common.ObjectOperation ALTER COLUMN whenArriving SET NOT NULL;
ALTER TABLE ps_common.ObjectOperation ALTER COLUMN whenLeaving SET NOT NULL;
ALTER TABLE ps_common.ObservedSystem ALTER COLUMN allowLogbook SET NOT NULL;
ALTER TABLE ps_common.ObservedSystem ALTER COLUMN allowObservation SET NOT NULL;
ALTER TABLE ps_common.SampleType ALTER COLUMN localMarket SET NOT NULL;
ALTER TABLE ps_common.SampleType ALTER COLUMN logbook SET NOT NULL;
ALTER TABLE ps_common.WeightCategory ALTER COLUMN allowLanding SET NOT NULL;
ALTER TABLE ps_common.WeightCategory ALTER COLUMN allowLogbook SET NOT NULL;
ALTER TABLE ps_common.WeightCategory ALTER COLUMN allowWellPlan SET NOT NULL;
ALTER TABLE ps_common.GearUseFeatures ALTER COLUMN gear SET NOT NULL;
ALTER TABLE ps_common.GearUseFeatures ALTER COLUMN number SET NOT NULL;
ALTER TABLE ps_common.GearUseFeaturesMeasurement ALTER COLUMN gearCharacteristic SET NOT NULL;
ALTER TABLE ps_common.GearUseFeaturesMeasurement ALTER COLUMN measurementValue SET NOT NULL;
ALTER TABLE ps_common.ObjectMaterial ALTER COLUMN childrenMultiSelectable SET NOT NULL;
ALTER TABLE ps_common.ObjectMaterial ALTER COLUMN childSelectionMandatory SET NOT NULL;
ALTER TABLE ps_common.ObjectOperation ALTER COLUMN whenArriving SET NOT NULL;
ALTER TABLE ps_common.ObjectOperation ALTER COLUMN whenLeaving SET NOT NULL;
ALTER TABLE ps_common.ObservedSystem ALTER COLUMN allowLogbook SET NOT NULL;
ALTER TABLE ps_common.ObservedSystem ALTER COLUMN allowObservation SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN departureHarbour SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN endDate SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN ocean SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN startDate SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN vessel SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN observationsAcquisitionStatus SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN logbookAcquisitionStatus SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN targetWellsSamplingAcquisitionStatus SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN localMarketWellsSamplingAcquisitionStatus SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN localMarketSurveySamplingAcquisitionStatus SET NOT NULL;
ALTER TABLE ps_common.Trip ALTER COLUMN advancedSamplingAcquisitionStatus SET NOT NULL;
ALTER TABLE ps_landing.Fate ALTER COLUMN discard SET NOT NULL;
ALTER TABLE ps_landing.Landing ALTER COLUMN fate SET NOT NULL;
ALTER TABLE ps_landing.Landing ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_landing.Landing ALTER COLUMN weight SET NOT NULL;
ALTER TABLE ps_localmarket.Batch ALTER COLUMN packaging SET NOT NULL;
ALTER TABLE ps_localmarket.Batch ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_localmarket.Packaging ALTER COLUMN batchComposition SET NOT NULL;
ALTER TABLE ps_localmarket.Packaging ALTER COLUMN batchWeightType SET NOT NULL;
ALTER TABLE ps_localmarket.Packaging ALTER COLUMN harbour SET NOT NULL;
ALTER TABLE ps_localmarket.Sample ALTER COLUMN number SET NOT NULL;
ALTER TABLE ps_localmarket.Sample_well ALTER COLUMN sample SET NOT NULL;
ALTER TABLE ps_localmarket.Sample_well ALTER COLUMN well SET NOT NULL;
ALTER TABLE ps_localmarket.SampleSpecies ALTER COLUMN sizeMeasureType SET NOT NULL;
ALTER TABLE ps_localmarket.SampleSpecies ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_localmarket.SampleSpeciesMeasure ALTER COLUMN count SET NOT NULL;
ALTER TABLE ps_localmarket.SampleSpeciesMeasure ALTER COLUMN sizeClass SET NOT NULL;
ALTER TABLE ps_localmarket.Survey ALTER COLUMN date SET NOT NULL;
ALTER TABLE ps_localmarket.Survey ALTER COLUMN number SET NOT NULL;
ALTER TABLE ps_localmarket.SurveyPart ALTER COLUMN proportion SET NOT NULL;
ALTER TABLE ps_localmarket.SurveyPart ALTER COLUMN species SET NOT NULL;
-- See #2078
-- ALTER TABLE ps_logbook.Activity ALTER COLUMN latitude SET NOT NULL;
-- ALTER TABLE ps_logbook.Activity ALTER COLUMN longitude SET NOT NULL;
ALTER TABLE ps_logbook.Activity ALTER COLUMN number SET NOT NULL;
ALTER TABLE ps_logbook.Activity ALTER COLUMN originalDataModified SET NOT NULL;
ALTER TABLE ps_logbook.Activity ALTER COLUMN positionCorrected SET NOT NULL;
ALTER TABLE ps_logbook.Activity ALTER COLUMN vesselActivity SET NOT NULL;
ALTER TABLE ps_logbook.Activity ALTER COLUMN vmsDivergent SET NOT NULL;
ALTER TABLE ps_logbook.Catch ALTER COLUMN speciesFate SET NOT NULL;
ALTER TABLE ps_logbook.Catch ALTER COLUMN species SET NOT NULL;
-- See #2385
-- ALTER TABLE ps_logbook.Catch ALTER COLUMN weightMeasureMethod SET NOT NULL;
-- ALTER TABLE ps_logbook.Catch ALTER COLUMN weight SET NOT NULL;
ALTER TABLE ps_logbook.Catch ALTER COLUMN weightMeasureMethod DROP NOT NULL;
ALTER TABLE ps_logbook.Catch ALTER COLUMN weight DROP NOT NULL;
ALTER TABLE ps_logbook.FloatingObjectPart ALTER COLUMN objectMaterial SET NOT NULL;
ALTER TABLE ps_logbook.Route ALTER COLUMN date SET NOT NULL;
ALTER TABLE ps_logbook.Sample ALTER COLUMN sampleQuality SET NOT NULL;
ALTER TABLE ps_logbook.Sample ALTER COLUMN sampleType SET NOT NULL;
ALTER TABLE ps_logbook.Sample ALTER COLUMN superSample SET NOT NULL;
ALTER TABLE ps_logbook.Sample ALTER COLUMN well SET NOT NULL;
ALTER TABLE ps_logbook.SampleActivity ALTER COLUMN sample SET NOT NULL;
ALTER TABLE ps_logbook.SampleActivity ALTER COLUMN activity SET NOT NULL;
ALTER TABLE ps_logbook.SampleSpecies ALTER COLUMN measuredCount SET NOT NULL;
ALTER TABLE ps_logbook.SampleSpecies ALTER COLUMN subSampleNumber SET NOT NULL;
ALTER TABLE ps_logbook.SampleSpecies ALTER COLUMN totalCount SET NOT NULL;
ALTER TABLE ps_logbook.SampleSpecies ALTER COLUMN sizeMeasureType SET NOT NULL;
ALTER TABLE ps_logbook.SampleSpecies ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_logbook.SampleSpeciesMeasure ALTER COLUMN count SET NOT NULL;
ALTER TABLE ps_logbook.SampleSpeciesMeasure ALTER COLUMN sizeClass SET NOT NULL;
ALTER TABLE ps_logbook.TransmittingBuoy ALTER COLUMN transmittingBuoyOperation SET NOT NULL;
ALTER TABLE ps_logbook.TransmittingBuoy ALTER COLUMN transmittingBuoyType SET NOT NULL;
ALTER TABLE ps_logbook.WellPlan ALTER COLUMN activity SET NOT NULL;
ALTER TABLE ps_logbook.WellPlan ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_logbook.WellPlan ALTER COLUMN weightCategory SET NOT NULL;
ALTER TABLE ps_logbook.WellPlan ALTER COLUMN weight SET NOT NULL;
ALTER TABLE ps_logbook.WellPlan ALTER COLUMN wellSamplingConformity SET NOT NULL;
ALTER TABLE ps_logbook.WellPlan ALTER COLUMN wellSamplingStatus SET NOT NULL;
ALTER TABLE ps_logbook.WellPlan ALTER COLUMN well SET NOT NULL;
ALTER TABLE ps_observation.Catch ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_observation.Catch ALTER COLUMN informationSource SET NOT NULL;

-- ALTER TABLE ps_observation.Activity ALTER COLUMN latitude SET NOT NULL;
-- ALTER TABLE ps_observation.Activity ALTER COLUMN longitude SET NOT NULL;
ALTER TABLE ps_observation.Activity ALTER COLUMN time SET NOT NULL;
ALTER TABLE ps_observation.Activity ALTER COLUMN vesselActivity SET NOT NULL;
ALTER TABLE ps_observation.FloatingObjectPart ALTER COLUMN objectMaterial SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchRelease ALTER COLUMN count SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchRelease ALTER COLUMN sex SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchRelease ALTER COLUMN speciesGroupReleaseMode SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchRelease ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchRelease ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_observation.ObjectObservedSpecies ALTER COLUMN count SET NOT NULL;
ALTER TABLE ps_observation.ObjectObservedSpecies ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_observation.ObjectObservedSpecies ALTER COLUMN speciesStatus SET NOT NULL;
ALTER TABLE ps_observation.ObjectSchoolEstimate ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_observation.ObjectSchoolEstimate ALTER COLUMN totalWeight SET NOT NULL;
ALTER TABLE ps_observation.Route ALTER COLUMN date SET NOT NULL;
ALTER TABLE ps_observation.SchoolEstimate ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_observation.SampleMeasure ALTER COLUMN count SET NOT NULL;
ALTER TABLE ps_observation.SampleMeasure ALTER COLUMN sizeMeasureType SET NOT NULL;
ALTER TABLE ps_observation.SampleMeasure ALTER COLUMN species SET NOT NULL;
ALTER TABLE ps_observation.TransmittingBuoy ALTER COLUMN transmittingBuoyOperation SET NOT NULL;
ALTER TABLE ps_observation.TransmittingBuoy ALTER COLUMN transmittingBuoyType SET NOT NULL;
