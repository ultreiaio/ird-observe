---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE common.Country ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.DataQuality ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.FpaZone ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.GearCharacteristic ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.GearCharacteristicType ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.Gear ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.Harbour ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.LengthMeasureMethod ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.Ocean ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.Organism ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.Sex ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.ShipOwner ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.SizeMeasureType ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.SpeciesGroup ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.SpeciesGroupReleaseMode ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.SpeciesList ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.Vessel ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.VesselSizeCategory ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.VesselType ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.WeightMeasureMethod ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.WeightMeasureType ALTER COLUMN code SET NOT NULL;
ALTER TABLE common.Wind ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.BaitSettingStatus ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.BaitType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.CatchFate ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.HealthStatus ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.HookSize ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.HookType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.LightsticksColor ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.LightsticksType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.LineType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.MitigationType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.ObservationMethod ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.OnBoardProcessing ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.SettingShape ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.TripType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.VesselActivity ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_common.WeightDeterminationMethod ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_landing.Company ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_landing.Conservation ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_landing.DataSource ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_observation.BaitHaulingStatus ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_observation.EncounterType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_observation.HookPosition ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_observation.ItemHorizontalPosition ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_observation.ItemVerticalPosition ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_observation.MaturityStatus ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_observation.SensorBrand ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_observation.SensorDataFormat ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_observation.SensorType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ll_observation.StomachFullness ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.AcquisitionStatus ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.ObjectMaterial ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.ObjectMaterialType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.ObjectOperation ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.ObservedSystem ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.ReasonForNoFishing ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.ReasonForNullSet ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.SampleType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.SchoolType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.SpeciesFate ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.TransmittingBuoyOperation ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.TransmittingBuoyOwnership ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.TransmittingBuoyType ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.VesselActivity ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_common.WeightCategory ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_observation.DetectionMode ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_observation.InformationSource ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchReleaseConformity ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchReleaseStatus ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchReleasingTime ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_observation.ReasonForDiscard ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_observation.SpeciesStatus ALTER COLUMN code SET NOT NULL;
ALTER TABLE ps_observation.SurroundingActivity ALTER COLUMN code SET NOT NULL;
