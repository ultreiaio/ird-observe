---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

ALTER TABLE common.Country ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.DataQuality ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.FpaZone ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.GearCharacteristic ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.GearCharacteristicType ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.Gear ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.Harbour ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.LengthMeasureMethod ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.Ocean ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.Organism ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.Sex ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.SizeMeasureType ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.SpeciesGroup ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.SpeciesGroupReleaseMode ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.Species ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.SpeciesList ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.Vessel ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.VesselType ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.WeightMeasureMethod ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.WeightMeasureType ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE common.Wind ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.BaitSettingStatus  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.BaitType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.CatchFate  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.HealthStatus  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.HookSize  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.HookType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.LightsticksColor  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.LightsticksType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.LineType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.MitigationType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.ObservationMethod  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.OnBoardProcessing  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.Program  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.SettingShape  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.TripType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.VesselActivity  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_common.WeightDeterminationMethod  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_landing.Company  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_landing.Conservation  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_landing.DataSource  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_observation.BaitHaulingStatus  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_observation.EncounterType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_observation.HookPosition  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_observation.ItemHorizontalPosition  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_observation.ItemVerticalPosition  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_observation.MaturityStatus  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_observation.SensorDataFormat  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_observation.SensorType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ll_observation.StomachFullness  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.AcquisitionStatus  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.ObjectMaterial  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.ObjectMaterialType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.ObjectOperation  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.ObservedSystem  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.Program  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.ReasonForNoFishing  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.ReasonForNullSet  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.SampleType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.SchoolType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.SpeciesFate  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.TransmittingBuoyOperation  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.TransmittingBuoyOwnership  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.TransmittingBuoyType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.VesselActivity  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_common.WeightCategory  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_landing.Destination  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_landing.Fate  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_localmarket.BatchComposition  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_localmarket.BatchWeightType  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_localmarket.Packaging  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_logbook.InformationSource  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_logbook.SampleQuality  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_logbook.SetSuccessStatus  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_logbook.WellContentStatus  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_logbook.WellSamplingConformity  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_logbook.WellSamplingStatus  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_observation.DetectionMode  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_observation.InformationSource  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchReleaseConformity  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchReleaseStatus  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchReleasingTime  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_observation.ReasonForDiscard  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_observation.SpeciesStatus  ALTER COLUMN label3 SET NOT NULL;
ALTER TABLE ps_observation.SurroundingActivity  ALTER COLUMN label3 SET NOT NULL;

