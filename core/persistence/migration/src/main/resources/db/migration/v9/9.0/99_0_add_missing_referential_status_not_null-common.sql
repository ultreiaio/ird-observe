---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE common.Country ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.DataQuality ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.FpaZone ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.GearCharacteristic ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.GearCharacteristicType ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.Gear ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.Harbour ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.LengthMeasureMethod ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.LengthLengthParameter ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.LengthWeightParameter ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.Ocean ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.Organism ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.Person ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.Sex ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.ShipOwner ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.SizeMeasureType ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.Species ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.SpeciesGroup ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.SpeciesGroupReleaseMode ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.SpeciesList ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.Vessel ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.VesselSizeCategory ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.VesselType ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.WeightMeasureMethod ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.WeightMeasureType ALTER COLUMN status SET NOT NULL;
ALTER TABLE common.Wind ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.BaitSettingStatus ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.BaitType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.CatchFate ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.HealthStatus ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.HookSize ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.HookType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.LightsticksColor ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.LightsticksType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.LineType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.MitigationType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.ObservationMethod ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.OnBoardProcessing ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.SettingShape ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.TripType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.VesselActivity ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_common.WeightDeterminationMethod ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_landing.Company ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_landing.Conservation ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_landing.DataSource ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_observation.BaitHaulingStatus ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_observation.EncounterType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_observation.HookPosition ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_observation.ItemHorizontalPosition ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_observation.ItemVerticalPosition ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_observation.MaturityStatus ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_observation.SensorBrand ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_observation.SensorDataFormat ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_observation.SensorType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ll_observation.StomachFullness ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.AcquisitionStatus ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.ObjectMaterial ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.ObjectMaterialType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.ObjectOperation ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.ObservedSystem ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.ReasonForNoFishing ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.ReasonForNullSet ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.SampleType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.SchoolType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.SpeciesFate ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.TransmittingBuoyOperation ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.TransmittingBuoyOwnership ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.TransmittingBuoyType ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.VesselActivity ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_common.WeightCategory ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_observation.DetectionMode ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_observation.InformationSource ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchReleaseConformity ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchReleaseStatus ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchReleasingTime ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_observation.ReasonForDiscard ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_observation.SpeciesStatus ALTER COLUMN status SET NOT NULL;
ALTER TABLE ps_observation.SurroundingActivity ALTER COLUMN status SET NOT NULL;
