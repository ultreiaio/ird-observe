---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE common.gear_gearCharacteristic ADD PRIMARY KEY (gear, gearCharacteristic);
ALTER TABLE common.species_ocean ADD PRIMARY KEY (species, ocean);
ALTER TABLE common.speciesGroup_speciesGroupReleasemode ADD PRIMARY KEY (speciesGroup, speciesGroupReleaseMode);
ALTER TABLE common.speciesList_species ADD PRIMARY KEY (speciesList, species);
ALTER TABLE ll_common.trip_species ADD PRIMARY KEY(trip, species);
ALTER TABLE ll_logbook.catch_predator ADD PRIMARY KEY(catch, species);
ALTER TABLE ll_logbook.set_mitigationType ADD PRIMARY KEY(set, mitigationType);
ALTER TABLE ll_observation.catch_predator ADD PRIMARY KEY(catch, species);
ALTER TABLE ll_observation.set_mitigationType ADD PRIMARY KEY(set, mitigationType);
ALTER TABLE ll_observation.tdr_species ADD PRIMARY KEY(tdr, species);
ALTER TABLE ps_observation.activity_observedsystem ADD PRIMARY KEY(activity, observedSystem);
