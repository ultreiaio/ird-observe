---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_observation.Set DROP COLUMN targetcatchcompositionestimatedbyobserver;
ALTER TABLE ps_observation.Set DROP COLUMN targetdiscardcatchcompositionestimatedbyobserver;

ALTER TABLE ll_common.Trip DROP COLUMN program;
ALTER TABLE ps_common.Trip DROP COLUMN program;

DROP TABLE ps_observation.NonTargetLength;
DROP TABLE ps_observation.NonTargetSample;
DROP TABLE ps_observation.NonTargetCatch;
DROP TABLE ps_observation.TargetCatch;
DROP TABLE ps_observation.TargetSample;
DROP TABLE ps_observation.TargetLength;
DROP TABLE ps_observation.ReasonForNullSet;
DROP TABLE ps_observation.ReasonForNoFishing;
DROP TABLE ps_observation.WeightCategory;
DROP TABLE common.Program;

ALTER TABLE ps_observation.Activity DROP COLUMN set;
ALTER TABLE ll_observation.Activity DROP COLUMN set;
ALTER TABLE ll_logbook.Activity DROP COLUMN set;
ALTER TABLE ll_logbook.Activity DROP COLUMN sample;
