---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_observation.Set DROP COLUMN targetcatchcompositionestimatedbyobserver CASCADE;
ALTER TABLE ps_observation.Set DROP COLUMN targetdiscardcatchcompositionestimatedbyobserver CASCADE;

ALTER TABLE ll_common.Trip DROP COLUMN program CASCADE;
ALTER TABLE ps_common.Trip DROP COLUMN program CASCADE;

DROP TABLE ps_observation.NonTargetLength CASCADE;
DROP TABLE ps_observation.NonTargetSample CASCADE;
DROP TABLE ps_observation.NonTargetCatch CASCADE;
DROP TABLE ps_observation.TargetLength CASCADE;
DROP TABLE ps_observation.TargetSample CASCADE;
DROP TABLE ps_observation.TargetCatch CASCADE;
DROP TABLE ps_observation.ReasonForNullSet CASCADE;
DROP TABLE ps_observation.ReasonForNoFishing CASCADE;
DROP TABLE ps_observation.WeightCategory CASCADE;
DROP TABLE common.Program CASCADE;

ALTER TABLE ps_observation.Activity DROP COLUMN set CASCADE;
ALTER TABLE ll_observation.Activity DROP COLUMN set CASCADE;
ALTER TABLE ll_logbook.Activity DROP COLUMN set CASCADE;
ALTER TABLE ll_logbook.Activity DROP COLUMN sample CASCADE;
