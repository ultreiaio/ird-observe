---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
alter table ps_logbook.well add constraint fk_ps_common_trip_well foreign key (trip) references ps_common.trip;
alter table ps_logbook.well add constraint fk_ps_logbook_well_wellsamplingconformity foreign key (wellSamplingConformity) references ps_logbook.wellSamplingConformity;
alter table ps_logbook.well add constraint fk_ps_logbook_well_wellsamplingstatus foreign key (wellSamplingStatus) references ps_logbook.wellSamplingStatus;
CREATE INDEX idx_ps_logbook_well_lastupdatedate ON ps_logbook.well(lastUpdateDate);
CREATE INDEX idx_ps_logbook_well_trip ON ps_logbook.well(trip);
CREATE INDEX idx_ps_logbook_well_wellsamplingconformity ON ps_logbook.well(wellSamplingConformity);
CREATE INDEX idx_ps_logbook_well_wellsamplingstatus ON ps_logbook.well(wellSamplingStatus);
