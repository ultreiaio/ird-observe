---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
alter table if exists ps_logbook.wellActivity add constraint fk_ps_logbook_well_wellactivity foreign key (well) references ps_logbook.well;
alter table if exists ps_logbook.wellActivity add constraint fk_ps_logbook_wellactivity_activity foreign key (activity) references ps_logbook.activity;
CREATE INDEX idx_ps_logbook_wellactivity_activity ON ps_logbook.wellActivity(activity);
CREATE INDEX idx_ps_logbook_wellactivity_lastupdatedate ON ps_logbook.wellActivity(lastUpdateDate);
CREATE INDEX idx_ps_logbook_wellactivity_well ON ps_logbook.wellActivity(well);
