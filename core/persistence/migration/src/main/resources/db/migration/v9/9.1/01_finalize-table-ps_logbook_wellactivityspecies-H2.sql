---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
alter table ps_logbook.wellActivitySpecies add constraint fk_ps_logbook_wellactivity_wellactivityspecies foreign key (wellActivity) references ps_logbook.wellActivity;
alter table ps_logbook.wellActivitySpecies add constraint fk_ps_logbook_wellactivityspecies_species foreign key (species) references common.species;
alter table ps_logbook.wellActivitySpecies add constraint fk_ps_logbook_wellactivityspecies_weightcategory foreign key (weightCategory) references ps_common.weightCategory;
CREATE INDEX idx_ps_logbook_wellactivityspecies_lastupdatedate ON ps_logbook.wellActivitySpecies(lastUpdateDate);
CREATE INDEX idx_ps_logbook_wellactivityspecies_species ON ps_logbook.wellActivitySpecies(species);
CREATE INDEX idx_ps_logbook_wellactivityspecies_weightcategory ON ps_logbook.wellActivitySpecies(weightCategory);
CREATE INDEX idx_ps_logbook_wellactivityspecies_wellactivity ON ps_logbook.wellActivitySpecies(wellActivity);
