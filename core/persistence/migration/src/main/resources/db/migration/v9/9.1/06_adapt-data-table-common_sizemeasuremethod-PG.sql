---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

-- Replace ps_observation.NonTargetCatchRelease.lengthMeasureMethod
ALTER TABLE ps_observation.NonTargetCatchRelease ADD COLUMN sizeMeasureMethod VARCHAR(255);
CREATE INDEX idx_ps_observation_NonTargetCatchRelease_SizeMeasureMethod ON ps_observation.NonTargetCatchRelease (sizeMeasureMethod);
ALTER TABLE ps_observation.NonTargetCatchRelease ADD CONSTRAINT fk_ps_observation_NonTargetCatchRelease_SizeMeasureMethod foreign key (sizeMeasureMethod) references common.SizeMeasureMethod;
UPDATE ps_observation.NonTargetCatchRelease SET sizeMeasureMethod = REPLACE(lengthMeasureMethod, '.common.LengthMeasureMethod', '.common.SizeMeasureMethod') WHERE lengthMeasureMethod IS NOT NULL;
ALTER TABLE ps_observation.NonTargetCatchRelease DROP COLUMN lengthMeasureMethod CASCADE;

-- Replace ps_observation.Catch.lengthMeasureMethod
ALTER TABLE ps_observation.Catch ADD COLUMN sizeMeasureMethod VARCHAR(255);
CREATE INDEX idx_ps_observation_Catch_SizeMeasureMethod ON ps_observation.Catch (sizeMeasureMethod);
ALTER TABLE ps_observation.Catch ADD CONSTRAINT fk_ps_observation_Catch_SizeMeasureMethod foreign key (sizeMeasureMethod) references common.SizeMeasureMethod;
UPDATE ps_observation.Catch SET sizeMeasureMethod = REPLACE(lengthMeasureMethod, '.common.LengthMeasureMethod', '.common.SizeMeasureMethod') WHERE lengthMeasureMethod IS NOT NULL;
ALTER TABLE ps_observation.Catch DROP COLUMN lengthMeasureMethod CASCADE;

-- Replace ps_observation.SampleMeasure.lengthMeasureMethod
ALTER TABLE ps_observation.SampleMeasure ADD COLUMN sizeMeasureMethod VARCHAR(255);
CREATE INDEX idx_ps_observation_SampleMeasure_SizeMeasureMethod ON ps_observation.SampleMeasure (sizeMeasureMethod);
ALTER TABLE ps_observation.SampleMeasure ADD CONSTRAINT fk_ps_observation_SampleMeasure_SizeMeasureMethod foreign key (sizeMeasureMethod) references common.SizeMeasureMethod;
UPDATE ps_observation.SampleMeasure SET sizeMeasureMethod = REPLACE(lengthMeasureMethod, '.common.LengthMeasureMethod', '.common.SizeMeasureMethod') WHERE lengthMeasureMethod IS NOT NULL;
ALTER TABLE ps_observation.SampleMeasure DROP COLUMN lengthMeasureMethod CASCADE;

-- Replace ll_logbook.SamplePart.lengthMeasureMethod
ALTER TABLE ll_logbook.SamplePart ADD COLUMN sizeMeasureMethod VARCHAR(255);
CREATE INDEX idx_ll_logbook_SamplePart_SizeMeasureMethod ON ll_logbook.SamplePart (sizeMeasureMethod);
ALTER TABLE ll_logbook.SamplePart ADD CONSTRAINT fk_ll_logbook_SamplePart_SizeMeasureMethod foreign key (sizeMeasureMethod) references common.SizeMeasureMethod;
UPDATE ll_logbook.SamplePart SET sizeMeasureMethod = REPLACE(lengthMeasureMethod, '.common.LengthMeasureMethod', '.common.SizeMeasureMethod') WHERE lengthMeasureMethod IS NOT NULL;
ALTER TABLE ll_logbook.SamplePart DROP COLUMN lengthMeasureMethod CASCADE;

DROP TABLE common.LengthMeasureMethod CASCADE;
