---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
INSERT INTO common.SizeMeasureMethod(topiaId, topiaVersion, topiaCreateDate, code, homeId, uri, status, label1, label2, label3, label4, label5, label6, label7, label8, needComment, lastUpdateDate) SELECT REPLACE(topiaId, '.common.LengthMeasureMethod', '.common.SizeMeasureMethod'), topiaVersion, topiaCreateDate, code, homeId, uri, status, label1, label2, label3, label4, label5, label6, label7, label8, needComment, lastUpdateDate FROM common.lengthMeasureMethod;

UPDATE common.LastUpdateDate SET type ='fr.ird.observe.entities.referential.common.SizeMeasureMethod', lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.common.LengthMeasureMethod';

