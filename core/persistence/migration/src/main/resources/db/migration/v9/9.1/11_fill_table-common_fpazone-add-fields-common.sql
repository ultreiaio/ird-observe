---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00048' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#0.1';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00056' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#0.3';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00057' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#0.5';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675593#0.5564506922817382' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#0.7';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675591#0.43427027828177345' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#0.9';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675584#0.7208429105932204' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#1.1';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'GA', country = 'fr.ird.referential.common.Country#1464000000000#0.00047' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#1.3';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00058' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#1.5';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675590#0.8642150406061876' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#1.7';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675595#0.9573433532024453' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#1.9';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00059' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#2.1';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00055' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#2.3';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675594#0.141909095388714' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#2.5';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675590#0.9860530760211061' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#2.7';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00054' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#2.9';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00060' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#3.1';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675591#0.43427027828177345' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#3.3';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00067' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#3.5';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00066' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#0.2';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1433499422828#0.162370163481683' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#0.4';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1433499422444#0.191530283074826' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#0.6';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1433499421089#0.790609733667225' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#0.8';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00065' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#1.0';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675593#0.24921769452411147' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#1.2';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'YT', country = 'fr.ird.referential.common.Country#1239832675597#0.708958027082267' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#1.4';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1433499422061#0.580995921976864' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#1.6';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'RE', country = 'fr.ird.referential.common.Country#1239832675583#0.9493110781716075' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#1.8';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675593#0.3601938043845213' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#2.0';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00062' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#2.2';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'TZ', country = 'fr.ird.referential.common.Country#1433499420350#0.873300955630839' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#2.4';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00064' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#2.6';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'BA', country = 'fr.ird.referential.common.Country#1433499423185#0.295651502907276' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#2.8';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'EU', country = 'fr.ird.referential.common.Country#1433499423185#0.295651502907276' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#3.0';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'GL', country = 'fr.ird.referential.common.Country#1433499423185#0.295651502907276' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#3.2';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'JN', country = 'fr.ird.referential.common.Country#1433499423185#0.295651502907276' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#3.4';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'TR', country = 'fr.ird.referential.common.Country#1433499423185#0.295651502907276' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#3.6';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'ST', country = 'fr.ird.referential.common.Country#1464000000000#0.00049' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#3.7';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675584#0.10128636927717882' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#3.8';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675595#0.4516205269922994' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#3.9';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00063' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#4.0';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = 'XIN',country = 'fr.ird.referential.common.Country#1464000000000#0.00050' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1239832686122#4.1';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1464000000000#0.00072' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1464000000000#43';
UPDATE common.fpazone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, subdivision = null, country = 'fr.ird.referential.common.Country#1239832675584#0.0783072255559325' WHERE topiaid = 'fr.ird.referential.common.FpaZone#1472549603992#0.4034175948140135';
UPDATE common.FpaZone SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, country = 'fr.ird.referential.common.Country#1308140312730#0.23814082967457195' WHERE country IS NULL;
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.FpaZone';
