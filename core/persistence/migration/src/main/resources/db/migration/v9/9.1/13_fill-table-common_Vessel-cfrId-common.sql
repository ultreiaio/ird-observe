---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'ESP000026408' WHERE code = '1163';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'ESP000027578' WHERE code = '912';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'ESP000100247' WHERE code = '1268';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000115945' WHERE code = '195';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000184776' WHERE code = '239';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000194597' WHERE code = '26';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000196571' WHERE code = '228';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000242670' WHERE code = '20';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000242670' WHERE code = '181';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000242670' WHERE code = '286';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000243864' WHERE code = '225';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000243910' WHERE code = '218';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000243991' WHERE code = '231';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000244206' WHERE code = '240';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000260661' WHERE code = '68';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000260661' WHERE code = '768';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000291635' WHERE code = '257';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000291635' WHERE code = '512';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000291635' WHERE code = '544';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000291635' WHERE code = '836';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000291645' WHERE code = '259';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000291655' WHERE code = '258';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000294483' WHERE code = '28';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000294612' WHERE code = '83';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000294626' WHERE code = '79';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000294632' WHERE code = '120';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000294666' WHERE code = '285';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000318251' WHERE code = '441';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000318268' WHERE code = '23';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000318268' WHERE code = '417';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000318268' WHERE code = '477';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000318362' WHERE code = '193';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000436219' WHERE code = '944';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000436219' WHERE code = '975';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000436721' WHERE code = '967';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000461464' WHERE code = '276';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000461464' WHERE code = '676';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000461464' WHERE code = '705';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000461497' WHERE code = '274';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000461543' WHERE code = '275';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000462062' WHERE code = '298';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000462066' WHERE code = '296';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000486391' WHERE code = '474';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000544869' WHERE code = '306';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000544901' WHERE code = '305';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000544901' WHERE code = '769';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000545067' WHERE code = '325';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000545309' WHERE code = '326';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000545309' WHERE code = '677';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000545309' WHERE code = '704';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000545342' WHERE code = '327';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000545342' WHERE code = '771';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000545388' WHERE code = '334';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000545540' WHERE code = '345';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000584559' WHERE code = '324';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000584571' WHERE code = '328';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000622908' WHERE code = '401';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000683123' WHERE code = '395';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000683153' WHERE code = '403';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000683528' WHERE code = '430';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000683675' WHERE code = '433';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000686877' WHERE code = '453';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000686878' WHERE code = '462';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000692091' WHERE code = '1001';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000692094' WHERE code = '936';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000692313' WHERE code = '463';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000692572' WHERE code = '955';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000692659' WHERE code = '933';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000692694' WHERE code = '969';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000692719' WHERE code = '1013';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000692855' WHERE code = '970';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000716572' WHERE code = '1011';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000719999' WHERE code = '77';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000724048' WHERE code = '427';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000724152' WHERE code = '536';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000739506' WHERE code = '932';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000752550' WHERE code = '483';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000752558' WHERE code = '490';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000752560' WHERE code = '491';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000752564' WHERE code = '482';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000752577' WHERE code = '492';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000790948' WHERE code = '502';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000791294' WHERE code = '513';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000791294' WHERE code = '527';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000791294' WHERE code = '551';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000819516' WHERE code = '652';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000819516' WHERE code = '879';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000854428' WHERE code = '235';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000854429' WHERE code = '600';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000854430' WHERE code = '572';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000854440' WHERE code = '579';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899731' WHERE code = '951';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899734' WHERE code = '938';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899735' WHERE code = '965';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899739' WHERE code = '972';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899751' WHERE code = '923';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899751' WHERE code = '973';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899755' WHERE code = '939';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899756' WHERE code = '956';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899770' WHERE code = '971';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899771' WHERE code = '934';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899774' WHERE code = '974';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899774' WHERE code = '1010';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899781' WHERE code = '930';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899783' WHERE code = '1003';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899783' WHERE code = '1004';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899788' WHERE code = '954';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899791' WHERE code = '960';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899794' WHERE code = '950';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899810' WHERE code = '942';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899812' WHERE code = '947';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899817' WHERE code = '964';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899817' WHERE code = '1005';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899818' WHERE code = '953';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000899950' WHERE code = '760';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000900687' WHERE code = '924';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909601' WHERE code = '1002';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909606' WHERE code = '921';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909609' WHERE code = '940';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909611' WHERE code = '946';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909633' WHERE code = '937';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909645' WHERE code = '922';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909673' WHERE code = '945';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909674' WHERE code = '948';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909675' WHERE code = '935';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909676' WHERE code = '926';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909677' WHERE code = '929';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909678' WHERE code = '931';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000909690' WHERE code = '957';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000910231' WHERE code = '1014';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000910231' WHERE code = '1015';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911194' WHERE code = '587';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911286' WHERE code = '581';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911286' WHERE code = '592';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911286' WHERE code = '679';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911287' WHERE code = '707';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911289' WHERE code = '648';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911289' WHERE code = '701';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911289' WHERE code = '706';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911313' WHERE code = '691';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911313' WHERE code = '702';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911313' WHERE code = '703';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911320' WHERE code = '583';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911320' WHERE code = '593';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000911320' WHERE code = '693';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000912514' WHERE code = '958';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000912534' WHERE code = '959';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000912541' WHERE code = '952';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000914223' WHERE code = '776';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000918276' WHERE code = '925';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000918277' WHERE code = '966';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000922241' WHERE code = '1195';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000922241' WHERE code = '1202';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000925310' WHERE code = '917';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000925754' WHERE code = '767';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000925754' WHERE code = '887';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000925755' WHERE code = '770';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000925755' WHERE code = '888';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000925772' WHERE code = '1176';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000928376' WHERE code = '790';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000928376' WHERE code = '889';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000929143' WHERE code = '1177';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000929204' WHERE code = '798';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000929204' WHERE code = '890';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000929727' WHERE code = '835';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000929727' WHERE code = '891';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000929981' WHERE code = '1175';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000930479' WHERE code = '1178';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000930604' WHERE code = '861';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000930605' WHERE code = '873';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000930605' WHERE code = '877';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000930605' WHERE code = '1226';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000932206' WHERE code = '1181';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000932207' WHERE code = '1192';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000932399' WHERE code = '1012';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000933961' WHERE code = '1266';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000935546' WHERE code = '1214';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA000935546' WHERE code = '1223';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'FRA008998080' WHERE code = '943';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'ITA000023376' WHERE code = '595';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'ITA000023376' WHERE code = '796';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'ITA000023376' WHERE code = '963';
UPDATE common.Vessel SET lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1, cfrId = 'ITA000023376' WHERE code = '1262';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.common.Vessel';
