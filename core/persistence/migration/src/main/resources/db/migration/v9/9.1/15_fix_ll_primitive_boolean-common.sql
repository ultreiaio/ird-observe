---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

UPDATE ll_observation.Catch SET depredated = FALSE WHERE depredated IS NULL;
ALTER TABLE ll_observation.Catch ALTER COLUMN depredated SET NOT NULL ;
ALTER TABLE ll_observation.Catch ALTER COLUMN depredated  SET DEFAULT FALSE;

UPDATE ll_observation.Set SET weightedSnap = FALSE WHERE weightedSnap IS NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN weightedSnap SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN weightedSnap SET DEFAULT FALSE;

UPDATE ll_observation.Set SET weightedSwivel = FALSE WHERE weightedSwivel IS NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN weightedSwivel SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN weightedSwivel SET DEFAULT FALSE;

UPDATE ll_observation.Set SET shooterUsed = FALSE WHERE shooterUsed IS NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN shooterUsed SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN shooterUsed SET DEFAULT FALSE;

UPDATE ll_observation.Set SET monitored = FALSE WHERE monitored IS NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN monitored SET NOT NULL;
ALTER TABLE ll_observation.Set ALTER COLUMN monitored SET DEFAULT FALSE;

UPDATE ll_observation.Branchline SET weightedSnap = FALSE WHERE weightedSnap IS NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN weightedSnap SET NOT NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN weightedSnap SET DEFAULT FALSE;

UPDATE ll_observation.Branchline SET weightedSwivel = FALSE WHERE weightedSwivel IS NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN weightedSwivel SET NOT NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN weightedSwivel SET DEFAULT FALSE;

UPDATE ll_observation.Branchline SET depthRecorder = FALSE WHERE depthRecorder IS NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN depthRecorder SET NOT NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN depthRecorder SET DEFAULT FALSE;

UPDATE ll_observation.Branchline SET timer = FALSE WHERE timer IS NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN timer SET NOT NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN timer SET DEFAULT FALSE;

UPDATE ll_observation.Branchline SET hookLost = FALSE WHERE hookLost IS NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN hookLost SET NOT NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN hookLost SET DEFAULT FALSE;

UPDATE ll_observation.Branchline SET traceCutOff = FALSE WHERE traceCutOff IS NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN traceCutOff SET NOT NULL;
ALTER TABLE ll_observation.Branchline ALTER COLUMN traceCutOff SET DEFAULT FALSE;
