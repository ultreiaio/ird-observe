---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_common.VesselActivity ADD COLUMN allowFpaZoneChange BOOLEAN DEFAULT FALSE NOT NULL;
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, allowFpaZoneChange = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1379684416896#0.38648073770690594';
ALTER TABLE ps_common.VesselActivity ADD COLUMN allowSet BOOLEAN DEFAULT FALSE NOT NULL;
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, allowSet = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.ps.common.VesselActivity';
ALTER TABLE ll_common.VesselActivity ADD COLUMN allowSet BOOLEAN DEFAULT FALSE NOT NULL;
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, allowSet = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.1';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.ll.common.VesselActivity';
