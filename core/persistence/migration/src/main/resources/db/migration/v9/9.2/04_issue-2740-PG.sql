---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_logbook.Activity ADD COLUMN previousFpaZone VARCHAR(256);
ALTER TABLE ps_logbook.Activity ADD CONSTRAINT fk_ps_logbook_activity_previousfpazone FOREIGN KEY (previousFpaZone) REFERENCES common.fpaZone;
CREATE INDEX idx_common_ps_logbook_activity_previousfpazone ON ps_logbook.Activity(previousFpaZone);
ALTER TABLE ps_logbook.Activity ADD COLUMN nextFpaZone VARCHAR(256);
ALTER TABLE ps_logbook.Activity ADD CONSTRAINT fk_ps_logbook_activity_nextfpazone FOREIGN KEY (nextFpaZone) REFERENCES common.fpaZone;
CREATE INDEX idx_common_ps_logbook_activity_nextfpazone ON ps_logbook.Activity(nextFpaZone);
ALTER TABLE ps_logbook.Activity RENAME fpaZone TO currentFpaZone;
ALTER TABLE ps_logbook.Activity DROP CONSTRAINT fk_ps_logbook_activity_fpazone;
ALTER TABLE ps_logbook.Activity ADD CONSTRAINT fk_ps_logbook_activity_currentfpazone FOREIGN KEY (currentFpaZone) REFERENCES common.fpaZone;
CREATE INDEX idx_common_ps_logbook_activity_currentfpazone ON ps_logbook.Activity(currentFpaZone);
