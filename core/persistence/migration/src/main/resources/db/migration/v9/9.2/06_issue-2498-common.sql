---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_common.VesselActivity ADD COLUMN observation BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE ps_common.VesselActivity ADD COLUMN logbook BOOLEAN DEFAULT FALSE NOT NULL;
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.ps.common.VesselActivity';

-- obs + logbook
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675349#0.363119635949572' ; -- code 0
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675368#0.976590514005213' ; -- code 1
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675368#0.23635552874754517' ; -- code 2
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675368#0.9186535488462055' ; -- code 3
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675368#0.4510981840764775' ; -- code 4
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.08910085510139154' ; -- code 5
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' ; -- code 6
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.027011527426829218' ; -- code 7
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.10731168577264139' ; -- code 8
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675370#0.6475754136223717' ; -- code 9
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675370#0.45119681303299985' ; -- code 10
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675370#0.495613158646268' ; -- code 11
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675370#0.7823480383025052' ; -- code 12
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675370#0.9125190289998782' ; -- code 13
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675371#0.15012142294280495' ; -- code 14
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675371#0.23195165331640677' ; -- code 15
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675372#0.21399033380125898' ; -- code 16
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675372#0.6114689927107798' ; -- code 17
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#201007311100#0.6789545761820200' ; -- code 18
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#201007311100#0.67895457618202001' ; -- code 19
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#201007311100#0.67895457618202002' ; -- code 20
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1379684416896#0.38648073770690594' ; -- code 21
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#36' ; -- code 36
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#37' ; -- code 37
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#38' ; -- code 38
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#39' ; -- code 39
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1689075956320#0.37474533493899786'; -- code 50
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1689076060255#0.7545533498559192'; -- code 51
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1239832675372#0.43920247699937853' ; -- code 99

-- Only logbook
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#23' ; -- code 23
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#24' ; -- code 24
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#25' ; -- code 25
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#26' ; -- code 26
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#27' ; -- code 27
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#29' ; -- code 29
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#30' ; -- code 30
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#31' ; -- code 31
UPDATE ps_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.VesselActivity#1464000000000#32' ; -- code 32

ALTER TABLE ll_common.VesselActivity ADD COLUMN observation BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE ll_common.VesselActivity ADD COLUMN logbook BOOLEAN DEFAULT FALSE NOT NULL;
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.ll.common.VesselActivity';

-- obs + logbook
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.1'; -- code FO
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.2'; -- code OTH
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.3'; -- code SAMP
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.4'; -- code INT
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1239832686138#0.5'; -- code FAIL
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1636586674023#0.12680942941535378'; -- code ANCHOR

-- only logbook
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#666#01'; -- code CRUISE
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#666#02'; -- code DRIFT
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#666#04'; -- code OUTZEE
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#666#03'; -- code PORT
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#666#05'; -- code REPAIR
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#666#06'; -- code TRANSSHIP
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#666#07'; -- code UNK
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.01'; -- code WAITING
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.09'; -- code INACTIVE
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.10'; -- code LANDING
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.11'; -- code FO_HAULING
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.12'; -- code ONEDAYTRIP
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.13'; -- code OUT
UPDATE ll_common.VesselActivity SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, logbook = TRUE WHERE topiaId = 'fr.ird.referential.ll.common.VesselActivity#1464000000000#0.14'; -- code SEARCH
