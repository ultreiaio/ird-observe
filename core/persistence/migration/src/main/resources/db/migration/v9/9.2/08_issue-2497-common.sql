---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_common.SpeciesFate ADD COLUMN observation BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE ps_common.SpeciesFate ADD COLUMN logbook BOOLEAN DEFAULT FALSE NOT NULL;
UPDATE ps_common.SpeciesFate SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE;
UPDATE ps_common.SpeciesFate SET logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.SpeciesFate#1239832683619#0.5722739932065866';
UPDATE ps_common.SpeciesFate SET logbook = TRUE WHERE topiaId = 'fr.ird.referential.ps.common.SpeciesFate#1501492537510#0.9210847837998154';
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.ps.common.SpeciesFate';
ALTER TABLE ll_common.CatchFate ADD COLUMN observation BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE ll_common.CatchFate ADD COLUMN logbook BOOLEAN DEFAULT FALSE NOT NULL;
UPDATE ll_common.CatchFate SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, observation = TRUE, logbook = TRUE;
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.ll.common.CatchFate';
