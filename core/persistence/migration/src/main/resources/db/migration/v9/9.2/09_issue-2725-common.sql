---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ps_common.TransmittingBuoyType ADD COLUMN regex VARCHAR(255);
UPDATE ps_common.TransmittingBuoyType SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP}, regex = '[0-9]{1,10}';
ALTER TABLE ps_common.TransmittingBuoyType ALTER COLUMN regex SET NOT NULL;
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type ='fr.ird.observe.entities.referential.ps.common.TransmittingBuoyType';

-- Marine Instrument (ou Nautical)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{6}'   WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1646923320527#0.33523179253089086'; -- Marine Instrument (or Nautical) model M3iGO
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555598009579#0.25827174812998643'; -- Marine Instrument (or Nautical) model MDP
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555598589397#0.03790222487071604'; -- Marine Instrument (ou Nautical) modèle M2D
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555598060517#0.12868413749130259'; -- Marine Instrument (ou Nautical) modèle M2D
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555598614711#0.6131017221127856'; -- Marine Instrument (ou Nautical) modèle M3i
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555598638230#0.5391991932966195'; -- Marine Instrument (ou Nautical) modèle M3i+
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555598678780#0.06080786326877474'; -- Marine Instrument (ou Nautical) modèle M4i
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555598751847#0.7574798232447126'; -- Marine Instrument (ou Nautical) modèle M4i+
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555598035871#0.8886016505218918'; -- Marine Instrument (ou Nautical) modèle MDS
-- Satlink
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599350670#0.10789924191805456'; -- Satlink model D+
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599380646#0.45107835940060714'; -- Satlink model DS+
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599417765#0.817066197232802'; -- Satlink model DL+
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599441632#0.42691787771952094'; -- Satlink model DSL+
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599463852#0.5255265580245724'; -- Satlink model ISL+
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599481654#0.09243156434558908'; -- Satlink model ISD+
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{5,6}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599504631#0.7052345637173283'; -- Satlink model SLX+
-- Zunibal
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599549734#0.4666518568553424'; -- Zunibal model T07 (Tunabal-7)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599566974#0.8900769670656881'; -- Zunibal model Te7 (Tunabal-e7)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599589456#0.1959160960189371'; -- Zunibal model T7+ (Tunabal-e7+)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599610075#0.12839013415640665'; -- Zunibal model T8E (Tuna8 Explorer)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599629226#0.5044111804216899'; -- Zunibal model T8X (Tuna8 Xtreme)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599647693#0.8926963646430409'; -- Zunibal model F07 (Tunabal-7 F series)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599665156#0.4303037081433846'; -- Zunibal model Fe7 (Tunabal-e7 F series)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599688737#0.5201429859021608'; -- Zunibal model F7+ (Tunabal-e7+ F series)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599706786#0.5215710241823598'; -- Zunibal model F8E (Tuna8 Explorer (F series)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599732594#0.21820134592960183'; -- Zunibal model Z07 (Zuni sin sonda)
UPDATE ps_common.TransmittingBuoyType SET regex = '[0-9]{9}' WHERE topiaId = 'fr.ird.referential.ps.common.TransmittingBuoyType#1555599751014#0.9982419937613096'; -- Zunibal model Ze7 (Zuni sin sonda)
