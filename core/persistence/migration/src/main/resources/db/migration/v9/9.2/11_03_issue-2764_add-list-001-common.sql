---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 001 Correntometro (8)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L001', null, null, false, 1, 'Brand and model (Currentometer)', 'Marque et modèle (Courantomètre)', 'Marca y modelo (Correntometro)', 'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}001001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '001_001', null, null, false, 1, 'FURUNO - CI-30',  'FURUNO - CI-30',  'FURUNO - CI-30', null,'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}001002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '001_002', null, null, false, 1, 'FURUNO - CI-35',  'FURUNO - CI-35',  'FURUNO - CI-35', null,'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}001003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '001_003', null, null, false, 1, 'FURUNO - CI-60',  'FURUNO - CI-60',  'FURUNO - CI-60', null,'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}001004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '001_004', null, null, false, 1, 'FURUNO - CI-60G', 'FURUNO - CI-60G', 'FURUNO - CI-60G',null,'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}001005', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '001_005', null, null, false, 1, 'FURUNO - CI-68',  'FURUNO - CI-68',  'FURUNO - CI-68', null,'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}001006', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '001_006', null, null, false, 1, 'FURUNO - CI-68B', 'FURUNO - CI-68B', 'FURUNO - CI-68B',null,'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}001007', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '001_007', null, null, false, 1, 'FURUNO - CI-88',  'FURUNO - CI-88',  'FURUNO - CI-88', null,'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}001008', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '001_008', null, null, false, 1, 'JRC - JLN-652',  'JRC - JLN-652',  'JRC - JLN-652', null,'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001');
-- Attach to Gear 8
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#1239832686125#0.7', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001');
UPDATE common.Gear SET label1 = 'Currentometer', label3 = 'Correntometro', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.common.Gear#1239832686125#0.7';
