---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 003 Anemometro (6)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L003', null, null, false, 1, 'Brand and model (Anemometer (wind gauge))',         'Marque et modèle (Anémomètre)',         'Marca y modelo (Anemometro)',               'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}003001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '003_001', null, null, false, 1, 'B&G - unknown'  ,  'B&G - inconnu'  ,  'B&G - desconocido'  , NULL, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}003');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}003002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '003_002', null, null, false, 1, 'FURUNO - FI-50'  , 'FURUNO - FI-50'  , 'FURUNO - FI-50'  , 'Analogico y Digital', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}003');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}003003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '003_003', null, null, false, 1, 'FURUNO - FI-70'  , 'FURUNO - FI-70'  , 'FURUNO - FI-70'  , 'Instrument/Data Organizer', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}003');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}003004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '003_004', null, null, false, 1, 'SIMRAD - WI80'   , 'SIMRAD - WI80'   , 'SIMRAD - WI80'   , 'Analogico y Digital', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}003');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}003005', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '003_005', null, null, false, 1, 'SIMRAD - IS20'   , 'SIMRAD - IS20'   , 'SIMRAD - IS20'   , 'Analogico', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}003');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}003006', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '003_006', null, null, false, 1, 'FURUNO - digital', 'FURUNO - digital', 'FURUNO - digital', 'Digital', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}003');
-- Add Gear 28
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}28', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '28', null, null, false, 1, 'Anemometer (wind gauge)', 'Anémomètre', 'Anemometro');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}28', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}003');
