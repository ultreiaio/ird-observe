---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 004 Carta electronica (ECDIS) (3)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L004', null, null, false, 1, 'Brand and model (Electronic Chart (ECDIS))',         'Marque et modèle (Logiciel cartographique (ECDIS))',         'Marca y modelo (Carta electronica (ECDIS))', 'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}004001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '004_001', null, null, false, 1, 'MaxSea - PRO FISHING', 'MaxSea - PRO FISHING', 'MaxSea - PRO FISHING', null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}004');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}004002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '004_002', null, null, false, 1, 'MaxSea - PRO TUNA',    'MaxSea - PRO TUNA',    'MaxSea - PRO TUNA',    null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}004');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}004003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '004_003', null, null, false, 1, 'MaxSea - TIME ZERO',   'MaxSea - TIME ZERO',   'MaxSea - TIME ZERO',   null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}004');
-- Attach to Gear 14
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#1239832686125#0.13', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}004');
UPDATE common.Gear SET label1 = 'Electronic Chart (ECDIS)', label2 = 'Logiciel cartographique (ECDIS)', label3 = 'Carta electronica (ECDIS)', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.common.Gear#1239832686125#0.13';
