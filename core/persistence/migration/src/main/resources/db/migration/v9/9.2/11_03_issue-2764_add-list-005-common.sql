---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 005 Plotter (4)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}005', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L005', null, null, false, 1, 'Brand and model (GPS plotter)',         'Marque et modèle (Traceur GPS)',         'Marca y modelo (GPS plotter)',                   'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}005001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '005_001', null, null, false, 1, 'FURUNO - GD-180',     'FURUNO - GD-180',     'FURUNO - GD-180',     'Color CRT Video Plotter', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}005');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}005002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '005_002', null, null, false, 1, 'FURUNO - GD-180 MK2', 'FURUNO - GD-180 MK2', 'FURUNO - GD-180 MK2', 'Color CRT Video Plotter', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}005');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}005003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '005_003', null, null, false, 1, 'FURUNO - GD-188',     'FURUNO - GD-188',     'FURUNO - GD-188',     'Color CRT Video Plotter', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}005');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}005004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '005_004', null, null, false, 1, 'FURUNO - FAX-214',    'FURUNO - FAX-214',    'FURUNO - FAX-214',    'Video Plotter', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}005');
-- Add Gear 29
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}29', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '29', null, null, false, 1, 'GPS plotter', 'Traceur GPS', 'GPS plotter');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}29', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}005');
