---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 006 Procesador radares banda S (1)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}006', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L006', null, null, false, 1, 'Brand and model (S-Band Radar Processor (birds radars agregator))',         'Marque et modèle (Processeur radar en bande S (agrégateur de radars oiseaux))',         'Marca y modelo (Procesador radares banda S (agregador de radares pajaros))', 'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}006001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '006_001', null, null, false, 1, 'DIADES MARINE - FishSight', 'DIADES MARINE - FishSight', 'DIADES MARINE - FishSight', null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}006');
-- Add Gear 33
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}33', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '33', null, null, false, 1, 'S-Band Radar Processor (birds radars agregator)', 'Processeur radar en bande S (agrégateur de radars oiseaux)', 'Procesador radares banda S (agregador de radares pajaros)');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}33', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}006');
