---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 007 Radar desconocido (9)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L007', null, null, false, 1, 'Brand and model (Unknown radar type)',         'Marque et modèle (Radar de type inconnu)',         'Marca y modelo (Radar desconocido)',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}007001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '007_001', null, null, false, 1, 'FURUNO - FAR-1513',   'FURUNO - FAR-1513',   'FURUNO - FAR-1513',   null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}007002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '007_002', null, null, false, 1, 'FURUNO - FAR-1523',   'FURUNO - FAR-1523',   'FURUNO - FAR-1523',   null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}007003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '007_003', null, null, false, 1, 'FURUNO - FAR-2140',   'FURUNO - FAR-2140',   'FURUNO - FAR-2140',   null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}007004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '007_004', null, null, false, 1, 'FURUNO - FAR-2862',   'FURUNO - FAR-2862',   'FURUNO - FAR-2862',   null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}007005', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '007_005', null, null, false, 1, 'FURUNO - FAR-2866SW', 'FURUNO - FAR-2866SW', 'FURUNO - FAR-2866SW', null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}007006', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '007_006', null, null, false, 1, 'FURUNO - FAR-2XX8',   'FURUNO - FAR-2XX8',   'FURUNO - FAR-2XX8',   null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}007007', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '007_007', null, null, false, 1, 'FURUNO - FR-2800',    'FURUNO - FR-2800',    'FURUNO - FR-2800',    null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}007008', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '007_008', null, null, false, 1, 'FURUNO - FR-825D/DA', 'FURUNO - FR-825D/DA', 'FURUNO - FR-825D/DA', null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}007009', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '007_009', null, null, false, 1, 'JRC - JMA-149 A',     'JRC - JMA-149 A',     'JRC - JMA-149 A',     null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007');
-- Add Gear 34
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}34', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '34', null, null, false, 1, 'Unknown radar type', 'Radar de type inconnu', 'Radar desconocido');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}34', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007');
