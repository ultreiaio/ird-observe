---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 010 Sensor profundidad caida red (4)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}010', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L010', null, null, false, 1, 'Brand and model (net depth sounder)',         'Marque et modèle (Sonde de profondeur de senne)',         'Marca y modelo (Sensor profundidad caida red)',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}01001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '010_001', null, null, false, 1, 'SIMRAD - PI32',        'SIMRAD - PI32',        'SIMRAD - PI32',        null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}010');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}01002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '010_002', null, null, false, 1, 'SIMRAD - PI44',        'SIMRAD - PI44',        'SIMRAD - PI44',        null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}010');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}01003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '010_003', null, null, false, 1, 'SIMRAD - PI50',        'SIMRAD - PI50',        'SIMRAD - PI50',        null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}010');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}01004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '010_004', null, null, false, 1, 'ZUNIBAL - ZUN NET 35', 'ZUNIBAL - ZUN NET 35', 'ZUNIBAL - ZUN NET 35', null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}010');
-- Attach to Gear 23
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#1239832686125#0.23', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}010');
UPDATE common.Gear SET topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.common.Gear#1239832686125#0.23';
