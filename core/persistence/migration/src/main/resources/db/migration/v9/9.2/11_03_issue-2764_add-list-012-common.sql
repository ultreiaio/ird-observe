---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 012 Software Predicciun Pesca (4)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}012', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L012', null, null, false, 1, 'Brand and model (Oceanographic data software)',         'Marque et modèle (Logiciel de données océanographiques)',         'Marca y modelo (Software Prediccion Pesca)',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}012001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '012_001', null, null, false, 1, 'CATSAT',                  'CATSAT',                  'CATSAT',                  null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}012');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}012002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '012_002', null, null, false, 1, 'NAUTICAL',                'NAUTICAL',                'NAUTICAL',                null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}012');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}012003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '012_003', null, null, false, 1, 'ZUNIBAL - Fortunacharts', 'ZUNIBAL - Fortunacharts', 'ZUNIBAL - Fortunacharts', null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}012');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}012004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '012_004', null, null, false, 1, 'INSIGHT EXPLORER',        'INSIGHT EXPLORER',        'INSIGHT EXPLORER',        null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}012');
-- Add gear 31
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}31', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '31', null, null, false, 1, 'Oceanographic data software', 'Logiciel de données océanographiques', 'Software Prediccion Pesca');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}31', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}012');
