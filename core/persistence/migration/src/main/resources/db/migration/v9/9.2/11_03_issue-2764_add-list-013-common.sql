---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 013 Sonda desconocida (7)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}013', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L013', null, null, false, 1, 'Brand and model (Unkonwn sounder type)',         'Marque et modèle (Sondeur de type inconnu)',         'Marca y modelo (Sonda desconocida)',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}013001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '013_001', null, null, false, 1, 'CRAME - 828',              'CRAME - 828',              'CRAME - 828',              null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}013');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}013002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '013_002', null, null, false, 1, 'FURUNO - FCV-220/221T',    'FURUNO - FCV-220/221T',    'FURUNO - FCV-220/221T',    null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}013');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}013003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '013_003', null, null, false, 1, 'HONDEX - HE-301 A/N',      'HONDEX - HE-301 A/N',      'HONDEX - HE-301 A/N',      null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}013');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}013004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '013_004', null, null, false, 1, 'HONDEX - HE-710 MARK II',  'HONDEX - HE-710 MARK II',  'HONDEX - HE-710 MARK II',  null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}013');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}013005', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '013_005', null, null, false, 1, 'HONDEX - HE-710 MARK III', 'HONDEX - HE-710 MARK III', 'HONDEX - HE-710 MARK III', null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}013');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}013006', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '013_006', null, null, false, 1, 'SKIPPER - 802',            'SKIPPER - 802',            'SKIPPER - 802',            null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}013');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}013007', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '013_007', null, null, false, 1, 'SIMRAD - ST15',            'SIMRAD - ST15',            'SIMRAD - ST15',            null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}013');
-- Add Gear 37
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}37', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '37', null, null, false, 1, 'Unkonwn sounder type', 'Sondeur de type inconnu', 'Sonda desconocida');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}37', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}013');
