---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 014 Sonda vertical (por la navegaccion) (9)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L014', null, null, false, 1, 'Brand and model (Vertical sounder (for navigation))',         'Marque et modèle (Sondeur vertical (pour la navigation))',         'Marca y modelo (Sonda vertical (por la navegaccion))',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');

INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}014001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '014_001', null, null, false, 1, 'FURUNO - FE-700',           'FURUNO - FE-700',           'FURUNO - FE-700',           'Navegacion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}014002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '014_002', null, null, false, 1, 'FURUNO - FE-800',           'FURUNO - FE-800',           'FURUNO - FE-800',           'Navegacion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}014003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '014_003', null, null, false, 1, 'FURUNO - FE-801',           'FURUNO - FE-801',           'FURUNO - FE-801',           'Navegacion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}014004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '014_004', null, null, false, 1, 'FURUNO - FE-881',           'FURUNO - FE-881',           'FURUNO - FE-881',           'Navegacion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}014005', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '014_005', null, null, false, 1, 'HONEYWELL-ELAC - LAZ-2500', 'HONEYWELL-ELAC - LAZ-2500', 'HONEYWELL-ELAC - LAZ-2500', 'Navegacion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}014006', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '014_006', null, null, false, 1, 'JRC - JFE-380',             'JRC - JFE-380',             'JRC - JFE-380',             'Navegacion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}014007', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '014_007', null, null, false, 1, 'JRC - JFE-570S/SD',         'JRC - JFE-570S/SD',         'JRC - JFE-570S/SD',         'Navegacion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}014008', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '014_008', null, null, false, 1, 'KONGSBERG - EN-250',        'KONGSBERG - EN-250',        'KONGSBERG - EN-250',        'Navegacion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}014009', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '014_009', null, null, false, 1, 'SKIPPER - GDS-101',         'SKIPPER - GDS-101',         'SKIPPER - GDS-101',         'Navegacion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014');
-- Attach to Gear 2
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#1239832686125#0.1', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014');
UPDATE common.Gear SET label1 = 'Vertical sounder (for navigation)', label2 = 'Sondeur vertical (pour la navigation)',  label3 = 'Sonda vertical (por la navegaccion)', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.common.Gear#1239832686125#0.1';
