---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 015 Sonar de escaneo multihaz (1)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}015', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L015', null, null, false, 1, 'Brand and model (Multi-Beam Scanning Sonar)',         'Marque et modèle (Sonar à balayage multifaisceau)',         'Marca y modelo (Sonar de escaneo multihaz)',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}015001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '015_001', null, null, false, 1, 'FURUNO - CSH-20',     'FURUNO - CSH-20',    'FURUNO - CSH-20',    'Multi-Beam Scanning Sonar', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}015');
-- Add Gear 38
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}38', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '38', null, null, false, 1, 'Multi-Beam Scanning Sonar', 'Sonar à balayage multifaisceau', 'Sonar de escaneo multihaz');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}38', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}015');
