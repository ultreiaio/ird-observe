---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 018 Termometro (SST) (6)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}018', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L018', null, null, false, 1, 'Brand and model (Thermometer (SST))',         'Marque et modèle (Thermomètre (SST))',         'Marca y modelo (Termometro (SST))',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}018001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '018_001', null, null, false, 1, 'Desconocido - B17HP', 'Desconocido - B17HP', 'Desconocido - B17HP', null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}018');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}018002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '018_002', null, null, false, 1, 'FURUNO - T-42',       'FURUNO - T-42',       'FURUNO - T-42',       null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}018');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}018003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '018_003', null, null, false, 1, 'FURUNO - TI-20',      'FURUNO - TI-20',      'FURUNO - TI-20',      null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}018');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}018004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '018_004', null, null, false, 1, 'NISSIN - MT-0013A',   'NISSIN - MT-0013A',   'NISSIN - MT-0013A',   null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}018');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}018005', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '018_005', null, null, false, 1, 'FURUNO - T-2000',     'FURUNO - T-2000',     'FURUNO - T-2000',     null, 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}018');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}018006', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '018_006', null, null, false, 1, 'JRC - NWZ-4610',      'JRC - NWZ-4610',      'JRC - NWZ-4610',      'Multi Info Display', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}018');
-- Attach to Gear 7
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#1239832686125#0.6', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}018');
UPDATE common.Gear SET label1 = 'Thermometer (SST)', label2 = 'Thermomètre (SST)', label3 = 'Termometro (SST)', topiaVersion = topiaVersion + 1, lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE topiaId = 'fr.ird.referential.common.Gear#1239832686125#0.6';
