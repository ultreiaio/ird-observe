---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 019 Sonar matricial (1)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}019', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L019', null, null, false, 1, 'Brand and model (Matricial sonar)',         'Marque et modèle (Sonar matriciel)',         'Marca y modelo (Sonar matricial)',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}019001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '019_001', null, null, false, 1, 'SIMRAD - SN90', 'SIMRAD - SN90', 'SIMRAD - SN90', 'Sonar de cerco', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}019');
INSERT INTO common.LastUpdateDate(topiaId, topiaVersion, topiaCreateDate, lastUpdateDate, type) values ('${LAST_UPDATE_PREFIX}159', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'fr.ird.observe.entities.referential.common.GearCharacteristicListItem');
-- Add Gear 36
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}36', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '36', null, null, false, 1, 'Matricial sonar', 'Sonar matriciel', 'Sonar matricial');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}36', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}019');
