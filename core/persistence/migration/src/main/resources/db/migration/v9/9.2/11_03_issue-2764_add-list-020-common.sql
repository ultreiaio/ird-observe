---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 020 Sonar de baja frecuencia - largo alcance (1)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}020', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L020', null, null, false, 1, 'Brand and model (Low frequency - long range sonar)',         'Marque et modèle (Sonar basse fréquence - longue portée)',         'Marca y modelo (Sonar de baja frecuencia - largo alcance)',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}020001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '020_001', null, null, false, 1, 'FURUNO - CSH-24F',    'FURUNO - CSH-24F',   'FURUNO - CSH-24F',   'Baja frecuencia', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}020');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}020002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '020_002', null, null, false, 1, 'SIMRAD - SX90',       'SIMRAD - SX90',      'SIMRAD - SX90',      'Sonar largo alcance y alta resolucion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}020');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}020003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '020_003', null, null, false, 1, 'SIMRAD - SX93',       'SIMRAD - SX93',      'SIMRAD - SX93',      'Sonar largo alcance y alta resolucion', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}020');
-- Add Gear 42
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}42', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '42', null, null, false, 1, 'Low frequency - long range sonar', 'Sonar basse fréquence - longue portée', 'Sonar de baja frecuencia - largo alcance');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}42', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}020');
