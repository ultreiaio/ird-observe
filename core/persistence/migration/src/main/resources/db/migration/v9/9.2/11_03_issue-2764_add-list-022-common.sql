---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 022 Sonda de escaneo en color 360° (8)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L022', null, null, false, 1, 'Brand and model (Full-circle color scanning sonar)',         'Marque et modèle (Sonar à balayage couleur 360°)',         'Marca y modelo (Sonda de escaneo en color 360°)',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}022001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '022001', null, null, false, 1, 'FURUNO - CSH-83',     'FURUNO - CSH-83',    'FURUNO - CSH-83',    'Full-Circle Color Scanning Sonar', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}022002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '022002', null, null, false, 1, 'FURUNO - FSV-24',     'FURUNO - FSV-24',    'FURUNO - FSV-24',    'Full-Circle Color Scanning Sonar', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}022003', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '022003', null, null, false, 1, 'FURUNO - FSV-25',     'FURUNO - FSV-25',    'FURUNO - FSV-25',    'Full-Circle Color Scanning Sonar', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}022004', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '022004', null, null, false, 1, 'FURUNO - FSV-30',     'FURUNO - FSV-30',    'FURUNO - FSV-30',    'Full-Circle Color Scanning Sonar', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}022005', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '022005', null, null, false, 1, 'FURUNO - FSV-30S',    'FURUNO - FSV-30S',   'FURUNO - FSV-30S',   'Full-Circle Color Scanning Sonar', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}022006', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '022006', null, null, false, 1, 'FURUNO - FSV-35',     'FURUNO - FSV-35',    'FURUNO - FSV-35',    'Full-Circle Color Scanning Sonar', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}022007', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '022007', null, null, false, 1, 'FURUNO - FSV-84',     'FURUNO - FSV-84',    'FURUNO - FSV-84',    'Full-Circle Color Scanning Sonar', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}022008', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '022008', null, null, false, 1, 'FURUNO - FSV-85',     'FURUNO - FSV-85',    'FURUNO - FSV-85',    'Full-Circle Color Scanning Sonar', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022');
-- Add Gear 39
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}39', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '39', null, null, false, 1, 'Full-circle color scanning sonar', 'Sonar à balayage couleur 360°', 'Sonda de escaneo en color 360°');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}39', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022');
