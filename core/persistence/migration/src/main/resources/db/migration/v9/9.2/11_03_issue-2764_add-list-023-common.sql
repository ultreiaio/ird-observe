---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- LIST 023 Sonar "Fish finder" (2)
INSERT INTO common.GearCharacteristic (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, gearCharacteristicType) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}023', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'L023', null, null, false, 1, 'Brand and model (Fish finder sonar)',         'Marque et modèle (Sonar "Fish finder")',         'Marca y modelo (Sonar "Fish finder")',         'fr.ird.referential.common.GearCharacteristicType#1464000000000#0.8');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}023001', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '023_001', null, null, false, 1, 'SIMRAD - SH80',       'SIMRAD - SH80',      'SIMRAD - SH80',      'Fish Finder', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}023');
INSERT INTO common.GearCharacteristicListItem (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, comment, gearCharacteristic) VALUES ('fr.ird.referential.common.GearCharacteristicListItem#${REFERENTIAL_PREFIX}023002', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '023_002', null, null, false, 1, 'SIMRAD - SU90',       'SIMRAD - SU90',      'SIMRAD - SU90',      'Fish Finder', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}023');
-- Add Gear 41
INSERT INTO common.Gear (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3) VALUES ('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}41', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '41', null, null, false, 1, 'Fish finder sonar', 'Sonar "Fish finder"', 'Sonar "Fish finder"');
INSERT INTO common.Gear_GearCharacteristic(gear, gearCharacteristic) VALUES('fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}41', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}023');
