---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) SELECT gear, gearCharacteristic FROM common.gear_gearCharacteristic;
INSERT INTO common.gear_defaultGearCharacteristic(gear, gearCharacteristic) SELECT gear, gearCharacteristic FROM common.gear_gearCharacteristic;
-- Kep as default only characteristic list
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.7' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}001';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.16' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}002';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}28' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}003';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.13' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}004';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}29' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}005';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}33' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}006';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}34' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}007';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.4' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}008';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.3' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}009';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.23' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}010';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}30' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}011';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}31' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}012';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}37' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}013';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.1' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}014';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}38' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}015';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}35' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}016';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.10' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}017';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.6' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}018';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}36' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}019';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}42' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}020';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}40' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}021';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}39' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}022';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}41' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}023';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#${REFERENTIAL_PREFIX}43' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}024';
DELETE FROM common.gear_defaultGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.2' AND gearCharacteristic != 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}025';
