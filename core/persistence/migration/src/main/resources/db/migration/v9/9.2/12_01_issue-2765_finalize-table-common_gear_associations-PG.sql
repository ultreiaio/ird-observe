---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE common.gear_defaultGearCharacteristic ADD CONSTRAINT fk_common_gear_defaultGearCharacteristic_gear FOREIGN KEY (gear) REFERENCES common.gear;
ALTER TABLE common.gear_defaultGearCharacteristic ADD CONSTRAINT fk_common_gear_defaultGearCharacteristic_defaultGearCharacteristic FOREIGN KEY (gearCharacteristic) REFERENCES common.gearCharacteristic;
CREATE INDEX idx_common_gear_defaultgearcharacteristic_gear ON common.gear_defaultGearCharacteristic(gear);
CREATE INDEX idx_common_gear_defaultgearcharacteristic_defaultgearcharacteristic ON common.gear_defaultGearCharacteristic(gearCharacteristic);

ALTER TABLE common.gear_allowedGearCharacteristic ADD CONSTRAINT fk_common_gear_allowedGearCharacteristic_gear FOREIGN KEY (gear) REFERENCES common.gear;
ALTER TABLE common.gear_allowedGearCharacteristic ADD CONSTRAINT fk_common_gear_allowedGearCharacteristic_allowedGearCharacteristic FOREIGN KEY (gearCharacteristic) REFERENCES common.gearCharacteristic;
CREATE INDEX idx_common_gear_allowedgearcharacteristic_gear ON common.gear_allowedGearCharacteristic(gear);
CREATE INDEX idx_common_gear_allowedgearcharacteristic_allowedgearcharacteristic ON common.gear_allowedGearCharacteristic(gearCharacteristic);

DROP TABLE common.gear_gearCharacteristic CASCADE;
