---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- add caracteristic 27
INSERT INTO common.GearCharacteristic (TOPIAID, TOPIAVERSION, TOPIACREATEDATE, LASTUPDATEDATE, CODE, URI, HOMEID, NEEDCOMMENT, STATUS, LABEL1, LABEL2, LABEL3, LABEL4, LABEL5, LABEL6, LABEL7, LABEL8, UNIT, GEARCHARACTERISTICTYPE) VALUES ('fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '27', null, null, false, 1, 'Electric power', 'Puissance électrique', 'Energia electrica', null, null, null, null, null, 'kW', 'fr.ird.referential.common.GearCharacteristicType#1239832686123#0.3');

-- add characteristic (4, 5, 15 and 27) to gear 2
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.1' AND gearCharacteristic = 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.1' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.1' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.1' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027';
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.1', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ( 'fr.ird.referential.common.Gear#1239832686125#0.1', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ( 'fr.ird.referential.common.Gear#1239832686125#0.1', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ( 'fr.ird.referential.common.Gear#1239832686125#0.1', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 3
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.2' AND gearCharacteristic = 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.2' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.2' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.2' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027';
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.2', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.2', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.2', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.2', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 4
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.3' AND gearCharacteristic = 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.3' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.3' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.3' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027';
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.3', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.3', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.3', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.3', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 5
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.4' AND gearCharacteristic = 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.4' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.4' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.4' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027';
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.4', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.4', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.4', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.4', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 6
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.5' AND gearCharacteristic = 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.5' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.5' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1';
DELETE FROM common.gear_allowedGearCharacteristic WHERE gear = 'fr.ird.referential.common.Gear#1239832686125#0.5' AND gearCharacteristic =  'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027';
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.5', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.5', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.5', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1239832686125#0.5', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 34
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#34', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#34', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#34', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#34', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 35
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#35', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#35', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#35', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#35', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 36
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#36', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#36', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#36', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#36', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 37
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#37', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#37', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#37', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#37', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 38
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#38', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#38', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#38', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#38', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 39
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#39', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#39', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#39', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#39', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 40
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#40', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#40', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#40', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#40', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 41
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#41', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#41', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#41', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#41', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 42
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#42', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#42', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#42', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#42', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');

-- add characteristic (4, 5, 15 and 27) to gear 43
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#43', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.4');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#43', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.5');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#43', 'fr.ird.referential.common.GearCharacteristic#1239832686124#0.1');
INSERT INTO common.gear_allowedGearCharacteristic(gear, gearCharacteristic) VALUES ('fr.ird.referential.common.Gear#1464000000000#43', 'fr.ird.referential.common.GearCharacteristic#${REFERENTIAL_PREFIX}027');
