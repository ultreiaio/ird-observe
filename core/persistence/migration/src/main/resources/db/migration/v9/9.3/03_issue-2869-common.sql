---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE common.VesselType ADD COLUMN seine boolean DEFAULT FALSE NOT NULL;
ALTER TABLE common.VesselType ADD COLUMN longline boolean DEFAULT FALSE NOT NULL;
UPDATE common.VesselType set lastUpdateDate = ${CURRENT_TIMESTAMP}, topiaVersion = topiaVersion + 1;

UPDATE common.VesselType SET seine = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1239832675734#0.24685054061673772';
UPDATE common.VesselType SET seine = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1308149674400#0.8030832839591066';
UPDATE common.VesselType SET seine = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1239832675734#0.4191950326431938';
UPDATE common.VesselType SET seine = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1239832675735#0.044156847891821505';
UPDATE common.VesselType SET seine = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1239832675735#0.7380146830307519';
UPDATE common.VesselType SET seine = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1239832675735#0.9086075071905084';
UPDATE common.VesselType SET seine = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1239832675735#0.307197212385357';
UPDATE common.VesselType SET longline = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1239832675736#0.8708229847859869';
UPDATE common.VesselType SET longline = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1239832686137#0.1';
UPDATE common.VesselType SET longline = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1464000000000#0.14';
UPDATE common.VesselType SET longline = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1464000000000#0.15';
UPDATE common.VesselType SET longline = TRUE WHERE topiaId = 'fr.ird.referential.common.VesselType#1239832675735#0.044156847891821505';
