---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}01', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '1', null, null, FALSE, 1,  'DEUTZ', 'DEUTZ', 'DEUTZ', null, null, null, null, null);
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}02', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '2', null, null, FALSE, 1,  'BAUDOUIN', 'BAUDOUIN', 'BAUDOUIN', null, null, null, null, null);
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}03', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '3', null, null, FALSE, 1,  'CATERPILLAR', 'CATERPILLAR', 'CATERPILLAR', null, null, null, null, null);
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}04', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '4', null, null, FALSE, 1,  'CUMMINS', 'CUMMINS', 'CUMMINS', null, null, null, null, null);
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}05', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '5', null, null, FALSE, 1,  'PERKINS', 'PERKINS', 'PERKINS', null, null, null, null, null);
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}06', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '6', null, null, FALSE, 1,  'SCANIA', 'SCANIA', 'SCANIA', null, null, null, null, null);
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}07', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '7', null, null, FALSE, 1,  'VOLVO PENTA', 'VOLVO PENTA', 'VOLVO PENTA', null, null, null, null, null);
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}08', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '8', null, null, FALSE, 1,  'YANMAR', 'YANMAR', 'YANMAR', null, null, null, null, null);
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}09', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '9', null, null, FALSE, 1,  'WÄRTSILÄ', 'WÄRTSILÄ', 'WÄRTSILÄ', null, null, null, null, null);
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}10', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '10', null, null, FALSE, 1, 'MAN B&W', 'MAN B&W', 'MAN B&W', null, null, null, null, null);
INSERT INTO common.EngineMake(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8) VALUES ('fr.ird.referential.common.EngineMake#${REFERENTIAL_PREFIX}99', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '99', null, null, FALSE, 1, 'Unknown', 'Inconnu', 'Desconocido', null, null, null, null, null);
INSERT INTO common.LastUpdateDate(topiaId, topiaVersion, topiaCreateDate, lastUpdateDate, type) values ('${LAST_UPDATE_PREFIX}160', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'fr.ird.observe.entities.referential.common.EngineMake');
