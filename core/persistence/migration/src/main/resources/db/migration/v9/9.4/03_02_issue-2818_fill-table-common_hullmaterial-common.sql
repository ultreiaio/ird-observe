---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
INSERT INTO common.HullMaterial(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8, source, comment) VALUES ('fr.ird.referential.common.HullMaterial#${REFERENTIAL_PREFIX}01', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '1', null, 'AL', FALSE, 1,  'Aluminium'                    , 'Aluminium'                                    , 'Aluminium TODO'                    , null, null, null, null, null, 'IOTC', null);
INSERT INTO common.HullMaterial(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8, source, comment) VALUES ('fr.ird.referential.common.HullMaterial#${REFERENTIAL_PREFIX}02', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '2', null, 'FR', FALSE, 1,  'Fibreglass-reinforced plastic', 'Matière plastique renforcée de fibre de verre', 'Fibreglass-reinforced plastic TODO', null, null, null, null, null, 'IOTC', null);
INSERT INTO common.HullMaterial(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8, source, comment) VALUES ('fr.ird.referential.common.HullMaterial#${REFERENTIAL_PREFIX}03', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '3', null, 'OT', FALSE, 1,  'Other'                        , 'Autre'                                        , 'Other TODO'                        , null, null, null, null, null, 'IOTC', null);
INSERT INTO common.HullMaterial(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8, source, comment) VALUES ('fr.ird.referential.common.HullMaterial#${REFERENTIAL_PREFIX}04', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '4', null, 'ST', FALSE, 1,  'Steel'                        , 'Acier'                                        , 'Steel TODO'                        , null, null, null, null, null, 'IOTC', null);
INSERT INTO common.HullMaterial(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8, source, comment) VALUES ('fr.ird.referential.common.HullMaterial#${REFERENTIAL_PREFIX}05', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '5', null, 'WO', FALSE, 1,  'Wood'                         , 'Bois'                                         , 'Wood TODO'                         , null, null, null, null, null, 'IOTC', null);
INSERT INTO common.HullMaterial(topiaid, topiaversion, topiacreatedate, lastupdatedate, code, uri, homeid, needcomment, status, label1, label2, label3, label4, label5, label6, label7, label8, source, comment) VALUES ('fr.ird.referential.common.HullMaterial#${REFERENTIAL_PREFIX}99', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, '99', null,'UN', FALSE, 1,  'Unknown'                      , 'Inconnu'                                      , 'Desconocido'                       , null, null, null, null, null, 'IOTC', null);
INSERT INTO common.LastUpdateDate(topiaId, topiaVersion, topiaCreateDate, lastUpdateDate, type) values ('${LAST_UPDATE_PREFIX}161', 0, ${CURRENT_DATE}, ${CURRENT_TIMESTAMP}, 'fr.ird.observe.entities.referential.common.HullMaterial');
