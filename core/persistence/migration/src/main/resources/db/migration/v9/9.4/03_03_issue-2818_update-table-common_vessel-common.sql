---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE common.Vessel ADD COLUMN engineMake VARCHAR(255);
ALTER TABLE common.Vessel ADD COLUMN hullMaterial VARCHAR(255);
ALTER TABLE common.Vessel ADD COLUMN grossTonnage NUMERIC;
ALTER TABLE common.Vessel DROP COLUMN comId;
ALTER TABLE common.vessel ADD CONSTRAINT fk_common_vessel_enginemake FOREIGN KEY (engineMake) REFERENCES common.engineMake;
ALTER TABLE common.vessel ADD CONSTRAINT fk_common_vessel_hullmaterial FOREIGN KEY (hullMaterial) REFERENCES common.hullMaterial;
CREATE INDEX idx_common_enginemake_lastupdatedate ON common.engineMake(lastUpdateDate);
CREATE INDEX idx_common_hullmaterial_lastupdatedate ON common.hullMaterial(lastUpdateDate);
CREATE INDEX idx_common_vessel_enginemake ON common.vessel(engineMake);
CREATE INDEX idx_common_vessel_hullmaterial ON common.vessel(hullMaterial);
