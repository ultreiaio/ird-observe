---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
INSERT INTO common.SpeciesList (topiaid, topiaversion, topiacreatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment, lastupdatedate, homeid) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}101', 0, ${CURRENT_DATE}, '101', 1, null, ' Consolidation - PS Logbook - Categories'' weights - fleet France', 'Consolidation - PS Logbook - Poids des catégories - flotte France', 'Consolidación - PS Logbook - Pesos de categorías - flota de Francia', null, null, null, null, null, false, ${CURRENT_TIMESTAMP} , null);
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685475#0.13349466123905152', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}101'); -- BET
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685474#0.8943253454598569', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}101'); -- YFT
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685474#0.975344121171992', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}101'); -- SKJ
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685476#0.5618871286604711', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}101'); -- ALB
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.common.SpeciesList';
