---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE ll_observation.Catch ADD COLUMN weightMeasureType VARCHAR(255);
ALTER TABLE ll_observation.Catch ADD CONSTRAINT fk_ll_observation_catch_weightmeasuretype FOREIGN KEY (weightMeasureType) REFERENCES common.weightMeasureType;
CREATE INDEX idx_ll_observation_catch_weightmeasuretype ON ll_observation.Catch(weightMeasureType);
UPDATE ll_observation.Catch SET weightMeasureType = 'fr.ird.referential.common.WeightMeasureType#1726219730662#0.7553113855315937' WHERE totalWeight IS NOT NULL;
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.data.ll.observation.Catch';
