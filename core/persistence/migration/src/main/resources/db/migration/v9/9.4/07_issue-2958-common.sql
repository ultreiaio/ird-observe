---
-- #%L
-- ObServe Core :: Persistence :: Migration
-- %%
-- Copyright (C) 2008 - 2025 IRD, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
INSERT INTO common.SpeciesList (topiaid, topiaversion, topiacreatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment, lastupdatedate, homeid) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102', 0, ${CURRENT_DATE}, '102', 1, null, 'Consolidation - PS Logbook - Weighted weights - fleet Spain', 'Consolidation - PS Logbook - Poids pondérés - flotte Espagne', 'Consolidación - PS Logbook - Pesos ponderados - flota de España', null, null, null, null, null, false, ${CURRENT_TIMESTAMP} , null);
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685474#0.8943253454598569', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685474#0.975344121171992', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685475#0.13349466123905152', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685476#0.5618871286604711', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685477#0.8024257002747615', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685477#0.3846921632590058', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1441287921299#0.016754076421811148', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1433499265113#0.891799515346065', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685477#0.2673009297087321', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685478#0.7676744877900202', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685477#0.5989181185528589', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685476#0.36339915670317835', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}102');
INSERT INTO common.SpeciesList (topiaid, topiaversion, topiacreatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment, lastupdatedate, homeid) VALUES ('fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}103', 0, ${CURRENT_DATE}, '103', 1, null, ' Consolidation - PS Logbook - Categories'' weights - fleet Spain', 'Consolidation - PS Logbook - Poids des catégories - flotte Espagne', 'Consolidación - PS Logbook - Pesos de categorías - flota de España', null, null, null, null, null, false, ${CURRENT_TIMESTAMP} , null);
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685475#0.13349466123905152', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}103'); -- BET
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685474#0.8943253454598569', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}103'); -- YFT
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685474#0.975344121171992', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}103'); -- SKJ
INSERT INTO common.speciesList_species (species, speciesList) VALUES ('fr.ird.referential.common.Species#1239832685476#0.5618871286604711', 'fr.ird.referential.common.SpeciesList#${REFERENTIAL_PREFIX}103'); -- ALB
UPDATE common.LastUpdateDate SET lastUpdateDate = ${CURRENT_TIMESTAMP} WHERE type = 'fr.ird.observe.entities.referential.common.SpeciesList';
