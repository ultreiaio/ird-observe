package fr.ird.observe.persistence.report.operation.ll;

/*-
 * #%L
 * ObServe Core :: Persistence :: Report
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportOperationConsumer;
import fr.ird.observe.report.ReportRequestExecutor;
import io.ultreia.java4all.util.matrix.DataMatrix;
import io.ultreia.java4all.util.matrix.DataMatrixDimension;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created on 14/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@AutoService(ReportOperationConsumer.class)
public class UnionAndSortLlLogbookSamples implements ReportOperationConsumer {

    /**
     * Indexes of sample business key
     */
    private static final Set<Integer> KEY_INDEXES = Set.of(0, 1, 4, 12, 26, 29);
    /**
     * Use only one array to get keys for a row, instead of creating a new one at each row... (Thanks to Benjamin Poussin DimensionHelper :))
     */
    private final String[] keys = new String[KEY_INDEXES.size()];

    private static class SampleKey {
        final int index;
        final String logbookProgramLabel;
        final String tripTypeCode;
        final String vesselLabel;
        final String endDate;
        final String speciesCode;
        final String length;

        private SampleKey(int index, String[] keys) {
            this.index = index;
            this.logbookProgramLabel = keys[0];
            this.tripTypeCode = keys[1];
            this.vesselLabel = keys[2];
            this.endDate = keys[3];
            this.length = keys[4];
            this.speciesCode = keys[5];
        }

        public String getLogbookProgramLabel() {
            return logbookProgramLabel;
        }

        public String getTripTypeCode() {
            return tripTypeCode;
        }

        public String getVesselLabel() {
            return vesselLabel;
        }

        public String getEndDate() {
            return endDate;
        }

        public String getSpeciesCode() {
            return speciesCode;
        }

        public String getLength() {
            return length;
        }

    }

    @Override
    public DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming) {
        DataMatrixDimension dimension = incoming.getDimension();
        int finalWidth = dimension.getWidth() / 2;
        int incomingHeight = dimension.getHeight();
        DataMatrix tmpMatrix = createTmpMatrix(0, 0, finalWidth, incomingHeight * 2);

        int y = -1;
        List<SampleKey> keys = new LinkedList<>();
        for (int j = 0; j < incomingHeight; j++) {
            boolean copyLeft = incoming.getValue(0, j) != null;
            boolean copyRight = incoming.getValue(finalWidth + 1, j) != null;
            if (copyLeft) {
                keys.add(copyRow(incoming, tmpMatrix, ++y, j, finalWidth, 0));
            }
            if (copyRight) {
                keys.add(copyRow(incoming, tmpMatrix, ++y, j, finalWidth, finalWidth + 1));
            }
        }
        keys.sort(Comparator.comparing(SampleKey::getLogbookProgramLabel)
                          .thenComparing(SampleKey::getTripTypeCode)
                          .thenComparing(SampleKey::getVesselLabel)
                          .thenComparing(SampleKey::getEndDate)
                          .thenComparing(SampleKey::getSpeciesCode)
                          .thenComparing(SampleKey::getLength));
        DataMatrix finalMatrix = createTmpMatrix(0, 0, finalWidth, y + 1);
        y = -1;
        for (SampleKey key : keys) {
            finalMatrix.simpleCopyRow(tmpMatrix, finalWidth, ++y, key.index);
        }
        return finalMatrix;
    }

    private SampleKey copyRow(DataMatrix incoming, DataMatrix tmpMatrix, int targetJ, int incomingJ, int width, int incomingX) {
        tmpMatrix.copyRow(incoming, width, targetJ, incomingX, incomingJ, KEY_INDEXES, keys);
        return new SampleKey(targetJ, keys);
    }

}
