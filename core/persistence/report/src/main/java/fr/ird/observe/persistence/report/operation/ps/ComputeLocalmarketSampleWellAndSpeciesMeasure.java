package fr.ird.observe.persistence.report.operation.ps;

/*-
 * #%L
 * ObServe Core :: Persistence :: Report
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.data.ps.localmarket.Sample;
import fr.ird.observe.entities.data.ps.localmarket.SampleSpecies;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportOperationConsumer;
import fr.ird.observe.report.ReportRepeatVariable;
import fr.ird.observe.report.ReportRequestExecutor;
import fr.ird.observe.spi.report.DefaultReportRequestExecutor;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.util.matrix.DataMatrix;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 21/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.44
 */
@AutoService(ReportOperationConsumer.class)
public class ComputeLocalmarketSampleWellAndSpeciesMeasure implements ReportOperationConsumer {
    public static String getWellStr(Sample sample) {
        if (sample.isWellEmpty()) {
            return "";
        }
        return String.join(", ", sample.getWell());
    }

    public static String getSampleSpeciesMeasureStr(Sample sample) {
        if (sample.isSampleSpeciesEmpty()) {
            return "";
        }
        return sample.getSampleSpecies().stream().map(p -> String.format("{ %s - %s - [ %s ] }", p.getSpecies().getFaoCode(), p.getSizeMeasureType().getLabel2(), getSampleSpeciesMeasureStr(p))).collect(Collectors.joining(", "));
    }

    public static String getSampleSpeciesMeasureStr(SampleSpecies sampleSpecies) {
        if (sampleSpecies.isSampleSpeciesMeasureEmpty()) {
            return "";
        }
        return sampleSpecies.getSampleSpeciesMeasure().stream().map(p -> String.format("%s - %d", Numbers.roundOneDigit(p.getSizeClass()), p.getCount())).collect(Collectors.joining(", "));
    }

    @Override
    public DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming) {
        ReportRepeatVariable<String> variable = report.getRepeatVariable("sampleId");
        Set<String> ids = variable.getValues();
        int row = 0;
        for (Sample sample : ((DefaultReportRequestExecutor) requestExecutor).getDaoSupplier().getDao(Sample.class).forTopiaIdIn(ids).findAll()) {
            String wellStr = getWellStr(sample);
            incoming.setValue(5, row, wellStr);
            String sampleSpeciesMeasureStr = getSampleSpeciesMeasureStr(sample);
            incoming.setValue(6, row++, sampleSpeciesMeasureStr);
        }
        return incoming;
    }
}
