package fr.ird.observe.persistence.report.operation.ps;

/*-
 * #%L
 * ObServe Core :: Persistence :: Report
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.ObserveI18nLabelsBuilder;
import fr.ird.observe.entities.data.ps.common.GearUseFeatures;
import fr.ird.observe.entities.data.ps.common.GearUseFeaturesMeasurement;
import fr.ird.observe.entities.referential.common.GearCharacteristic;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportOperationConsumer;
import fr.ird.observe.report.ReportRepeatVariable;
import fr.ird.observe.report.ReportRequestExecutor;
import fr.ird.observe.spi.report.DefaultReportRequestExecutor;
import io.ultreia.java4all.util.matrix.DataMatrix;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 21/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.44
 */
@AutoService(ReportOperationConsumer.class)
public class ComputeMeasurementsSeine implements ReportOperationConsumer {

    @Override
    public DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming) {
        DataMatrix tmpMatrix = createTmpMatrix(incoming.getWidth(), 0, 1, incoming.getHeight());
        ReportRepeatVariable<String> variable = report.getRepeatVariable("gearUseFeaturesId");
        Set<String> ids = variable.getValues();
        int row = 0;
        for (GearUseFeatures gearUseFeatures : ((DefaultReportRequestExecutor) requestExecutor).getDaoSupplier().getDao(GearUseFeatures.class).forTopiaIdIn(ids).findAll()) {
            String measurementsAsString = getMeasurementsAsString(gearUseFeatures);
            tmpMatrix.setValue(0, row, measurementsAsString);
        }
        return DataMatrix.merge(incoming, tmpMatrix);
    }


    public static String getMeasurementsAsString(GearUseFeatures bean) {
        return bean.getGearUseFeaturesMeasurement().stream().map(ComputeMeasurementsSeine::getMeasurementAsString).collect(Collectors.joining(", "));
    }

    public static String getMeasurementAsString(GearUseFeaturesMeasurement g) {
        GearCharacteristic gearCharacteristic = g.getGearCharacteristic();
        Pair<Boolean, String> pair = ObserveI18nLabelsBuilder.getMeasurementValue(g.getMeasurementValue());
        boolean isBoolean = pair.getKey();
        String measurementValue = pair.getValue();
        String codeLabel = ObserveI18nLabelsBuilder.getCodeLabel(gearCharacteristic.getCode());
        String unitLabel = ObserveI18nLabelsBuilder.getUnitLabel(isBoolean, gearCharacteristic.getUnit());
        return String.format("( %s - %s%s = %s )", codeLabel, gearCharacteristic.getLabel2(), unitLabel, measurementValue);
    }
}
