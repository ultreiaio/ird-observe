###
# #%L
# ObServe Core :: Persistence :: Report
# %%
# Copyright (C) 2008 - 2023 IRD, Ultreia.io
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
modelType=PS
name=Livre de bord - Utilisation des FOB, tableau détaillé
name.en=Logbook - FOB usage - detailled version
name.es=
description=Tableau récapitulatif des FOB rencontrés, par types CECOFAD et types d’actions (déploiements, simples visites, modifications, diverses modalités de retraits), avec et sans pêche
description.en=Summary of FOB encountered, by CECOFAD types, types of object operation (deployment, visit, update, kinds of retrievals), presence or absence of associated fishing set
description.es=
columns=Type de FOB,Visités Pêchés,Visités non Pêchés,Déploiement après Pêches,Déploiement seul,Modifiés Pêchés,Modifiés non Pêchés,Retirés Pêchés,Retirés non Pêchés,Abandonnés Pêchés,Abandonnés non Pêchés,Coulés Pêchés,Coulés non Pêchés,Remplacés Pêchés,Remplacés non Pêchés,Autre Pêchés,Autre non Pêchés
columns.en=CECOFAD type,Visit and fishing,Visit only,Deployment after fishing,Deployment only,Modification after fishing,Modification only,Retrieval after fishing,Retrieval only,Abandonment after fishing,Abandonment only,Sinking after fishing,Sinking only,Replacment after fishing,Replacment only,Other activity after fishing,Other activity only
columns.es=
repeatVariable.1.name=typeObjetId
repeatVariable.1.type=java.lang.String
repeatVariable.1.request=Select distinct (om.standardCode) \
From ObjectMaterialImpl om \
Where om.standardCode is not null \
And om.standardCode != '' \
And (om.standardCode In (Select Distinct(dcp.computedWhenArrivingSimplifiedObjectType) \
From fr.ird.observe.entities.data.ps.common.TripImpl m \
Join m.routeLogbook r \
Join r.activity a \
Join a.floatingObject dcp With dcp.computedWhenArrivingSimplifiedObjectType Is Not Null \
Where m.id In :tripId ) Or om.standardCode In (Select Distinct(dcp.computedWhenLeavingSimplifiedObjectType) \
From fr.ird.observe.entities.data.ps.common.TripImpl m \
Join m.routeLogbook r \
Join r.activity a \
Join a.floatingObject dcp With dcp.computedWhenLeavingSimplifiedObjectType Is Not Null \
Where m.id In :tripId )) \
Order by om.standardCode
request.1.location=0,0
request.1.layout=row
request.1.request=Select concat(str(om.standardCode) , ' - ', om.@i18nColumnName@) \
From ObjectMaterialImpl om \
Where \
om.standardCode = :typeObjetId \
And om.status != 0
request.1.repeat.name=typeObjetId
request.1.repeat.layout=column
request.2.location=1,0
request.2.layout=row
request.2.request=Select \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#2' And a.vesselActivity.id = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#2' And a.vesselActivity.id != 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenLeavingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#1' And a.vesselActivity.id = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenLeavingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#1' And a.vesselActivity.id != 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#8' And a.vesselActivity.id = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#8' And a.vesselActivity.id != 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#4' And a.vesselActivity.id = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#4' And a.vesselActivity.id != 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#5' And a.vesselActivity.id = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#5' And a.vesselActivity.id != 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#7' And a.vesselActivity.id = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#7' And a.vesselActivity.id != 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#9' And a.vesselActivity.id = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#9' And a.vesselActivity.id != 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And (dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#10' Or dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#3') And a.vesselActivity.id = 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End), \
Sum(Case When dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId And (dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#10' Or dcp.objectOperation.id = 'fr.ird.referential.ps.common.ObjectOperation#0#3') And a.vesselActivity.id != 'fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586' Then 1 Else 0 End) \
From fr.ird.observe.entities.data.ps.common.TripImpl m \
Join m.routeLogbook r \
Join r.activity a \
Join a.floatingObject dcp With dcp.computedWhenLeavingSimplifiedObjectType = :typeObjetId Or dcp.computedWhenArrivingSimplifiedObjectType = :typeObjetId \
Where \
m.id In :tripId
request.2.repeat.name=typeObjetId
request.2.repeat.layout=column
request.2.comment=visite + peche / visite - peche / Deploiement + peche / Deploiement - peche / Modifie + peche / Modifie - peche / Retire + peche / Retire - peche / Abandonne + peche / Abandonne - peche / Coule + peche / Coule - peche / Remplace + peche / Remplace - peche / Autre ou ancien peche + peche / Autre ou ancien peche - peche / Nombre de tortues
