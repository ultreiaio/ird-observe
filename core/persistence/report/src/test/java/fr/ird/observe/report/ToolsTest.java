package fr.ird.observe.report;

/*-
 * #%L
 * ObServe Core :: Persistence :: Report
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.definition.DefaultByClassPathReportDefinitionsBuilder;
import fr.ird.observe.report.definition.DefaultReportDefinitionsBuilder;
import fr.ird.observe.report.definition.ReportDefinition;
import fr.ird.observe.report.definition.ReportDefinitionsBuilder;
import fr.ird.observe.report.tools.PersistenceComputeReportListFileRunner;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

/**
 * Created at 06/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */

public class ToolsTest {

    public static final int REPORTS_COUNT_COUNT = 39;

    private static Path reportsPath(Path basedir) {
        return basedir.resolve("src").resolve("main").resolve("resources").resolve("META-INF").resolve("report");
    }

    private static Path reportListPath(Path basedir) {
        return basedir.resolve("target").resolve(ReportDefinitionsBuilder.REPORT_LIST_LOCATION.substring(1) + System.nanoTime());
    }

    @Test
    public void computeReportListFile() throws IOException {
        Path basedir = new File("").getAbsoluteFile().toPath();
        PersistenceComputeReportListFileRunner runner = new PersistenceComputeReportListFileRunner();
        runner.setReportListPath(reportListPath(basedir));
        runner.setImportPath(reportsPath(basedir));
        Assert.assertTrue(Files.exists(runner.getImportPath()));
        Assert.assertFalse(Files.exists(runner.getReportListPath()));
        runner.run();
        Assert.assertTrue(Files.exists(runner.getReportListPath()));
        List<String> lines = Files.readAllLines(runner.getReportListPath());
        Assert.assertEquals(REPORTS_COUNT_COUNT, lines.size());
    }

    @Test
    public void loadBDefaultByPath() throws Exception {
        Path basedir = new File("").getAbsoluteFile().toPath();
        Path path = reportsPath(basedir);
        List<ReportDefinition> reports = DefaultReportDefinitionsBuilder.build(Objects.requireNonNull(path));
        Assert.assertNotNull(reports);
        Assert.assertEquals(REPORTS_COUNT_COUNT, reports.size());
    }

    @Test
    public void loadDefaultByClassPath() throws Exception {
        URL reportFileUrl = getClass().getResource(ReportDefinitionsBuilder.REPORT_LIST_LOCATION);
        List<ReportDefinition> reports = DefaultByClassPathReportDefinitionsBuilder.build(Objects.requireNonNull(reportFileUrl));
        Assert.assertNotNull(reports);
        Assert.assertEquals(REPORTS_COUNT_COUNT, reports.size());
    }
}
