package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.toolkit.templates.entity.query.SqlQueryDefinitions;
import org.junit.Assert;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 28/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObservePersistenceFixtures extends ObserveFixtures {

    public static Map<String, Long> getTablesCount(ObserveTopiaApplicationContext applicationContext, List<String> schemaAndTableNames) {
        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {
            return getTablesCount(persistenceContext, schemaAndTableNames);
        }
    }

    public static Map<String, Long> getTablesCount(ObserveTopiaPersistenceContext persistenceContext, List<String> schemaAndTableNames) {
        Map<String, Long> countBuilder = new LinkedHashMap<>();
        for (String table : schemaAndTableNames) {
            Long actualCount = persistenceContext.countTable(table);
            countBuilder.put(table, actualCount);
        }
        return countBuilder;
    }

    public static void assertDataExist(ObserveTopiaApplicationContext applicationContext, DtoEntityContext<?, ?, ?, ?> spi, String id) {
        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {
            boolean exists = spi.getDao(persistenceContext).forTopiaIdEquals(id).exists();
            Assert.assertTrue(String.format("data %s should exists.", id), exists);
        }
    }

    public static void assertDataExist(ObserveTopiaPersistenceContext persistenceContext, DtoEntityContext<?, ?, ?, ?> spi, String id) {
        boolean exists = spi.getDao(persistenceContext).forTopiaIdEquals(id).exists();
        Assert.assertTrue(String.format("data %s should exists.", id), exists);
    }

    public static void assertDataNotExist(ObserveTopiaApplicationContext applicationContext, DtoEntityContext<?, ?, ?, ?> spi, String id) {
        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {
            boolean exists = spi.getDao(persistenceContext).forTopiaIdEquals(id).exists();
            Assert.assertFalse(String.format("data %s should not exists.", id), exists);
        }
    }

    public static void assertDataNotExist(ObserveTopiaPersistenceContext persistenceContext, DtoEntityContext<?, ?, ?, ?> spi, String id) {
        boolean exists = spi.getDao(persistenceContext).forTopiaIdEquals(id).exists();
        Assert.assertFalse(String.format("data %s should not exists.", id), exists);
    }

    public static void assertSqlQueries(Class<? extends Entity> type, int exceptedCount) {
        SqlQueryDefinitions queryDefinitions = SqlQueryDefinitions.load(type.getName()).orElseThrow();
//        queryDefinitions.getQueries().forEach(System.out::println);
        Assert.assertEquals(exceptedCount, queryDefinitions.getQueries().size());
    }
}

