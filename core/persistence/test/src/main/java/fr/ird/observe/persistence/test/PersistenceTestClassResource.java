package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.test.DataSourcesForTestManager;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestClassResourceSupport;
import fr.ird.observe.test.spi.DatabaseClassifier;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.Description;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 21/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public class PersistenceTestClassResource extends TestClassResourceSupport {

    private static final Logger log = LogManager.getLogger(PersistenceTestClassResource.class);

    private boolean ignored;

    public PersistenceTestClassResource(boolean ignore) {
        this();
        setIgnored(ignore);
    }

    public boolean isIgnored() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    public PersistenceTestClassResource() {
        this(DatabaseClassifier.DEFAULT);
    }

    public PersistenceTestClassResource(DatabaseClassifier classifier) {
        super(classifier);
    }

    public DataSourcesForTestManager getDataSourcesForTestManager() {
        return dataSourcesForTestManager;
    }

    public ObserveDataSourceConfigurationTopiaH2 createDataSourceConfiguration(Version dbVersion, String dbName, File targetPath, String login, char[] password) throws IOException {
        ObserveDataSourceConfigurationTopiaH2 sharedDatabaseConfiguration = dataSourcesForTestManager.createSharedDataSourceConfigurationH2(dbVersion, dbName, login, password);
        File sharedDatabaseFile = sharedDatabaseConfiguration.getDatabaseFile();
        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration;
        boolean sharedDatabaseExist = sharedDatabaseFile.exists();
        Path temporaryDirectoryRoot = getTemporaryDirectoryRoot();
        if (!sharedDatabaseExist) {
            log.info(String.format("Create shared database: %s/%s to %s", dbVersion, dbName, sharedDatabaseFile));
            SqlScript script = dataSourcesForTestManager.getCache(dbVersion, dbName);
            sharedDatabaseConfiguration.setTemporaryDirectory(temporaryDirectoryRoot);
            try (ObserveTopiaApplicationContext applicationContext = ObserveTopiaApplicationContextFactory.createContext(sharedDatabaseConfiguration)) {
                applicationContext.executeSqlStatements(script);
            }
        }
        if (targetPath == null) {
            dataSourceConfiguration = sharedDatabaseConfiguration;
        } else {
            // Use a copy
            dataSourceConfiguration = dataSourcesForTestManager.createDataSourceConfigurationH2(targetPath, dbVersion, dbName, login, password);
            File databaseFileTarget = dataSourceConfiguration.getDatabaseFile();
            log.info(String.format("Copy database: %s/%s to %s", dbVersion, dbName, databaseFileTarget));
            Files.createDirectories(databaseFileTarget.toPath().getParent());
            Files.copy(sharedDatabaseFile.toPath(), databaseFileTarget.toPath());
        }
        dataSourceConfiguration.setModelVersion(ObserveTestConfiguration.getModelVersion());
        dataSourceConfiguration.setTemporaryDirectory(temporaryDirectoryRoot);
        return dataSourceConfiguration;
    }

    @Override
    protected void after(Description description) throws IOException {
        super.after(description);
        ObserveTopiaApplicationContextFactory.closeFactory();
    }
}
