package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import org.junit.runner.Description;

import java.io.IOException;
import java.util.Objects;

/**
 * Created on 20/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class PersistenceTestMethodResourceRead extends PersistenceTestClassResource {

    public PersistenceTestMethodResourceRead(boolean ignored) {
        super(ignored);
    }

    private ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration;
    private ObserveTopiaApplicationContext applicationContext;

    public ObserveTopiaPersistenceContext newPersistenceContext() {
        return getTopiaApplicationContext().newPersistenceContext();
    }

    public ObserveTopiaApplicationContext getTopiaApplicationContext() {
        return applicationContext == null ? applicationContext = ObserveTopiaApplicationContextFactory.createContext(dataSourceConfiguration) : applicationContext;
    }

    @Override
    protected void before(Description description) throws Throwable {
        if (isIgnored()) {
            return;
        }
        super.before(description);

        Objects.requireNonNull(getDbName(), "Pas de nom de base spécifié");
        Objects.requireNonNull(getDbVersion(), "Pas de version de base spécifié");
        Objects.requireNonNull(getLogin(), "Pas de login spécifié");
        Objects.requireNonNull(getPassword(), "Pas de password spécifié");
        initTemporaryDirectoryRoot("all");

        if (dataSourceConfiguration == null) {
            dataSourceConfiguration = createDataSourceConfiguration(getDbVersion(), getDbName(), null, getLogin(), getPassword());
        }
    }

    @Override
    protected void after(Description description) throws IOException {
        if (isIgnored()) {
            return;
        }
        super.after(description);
        try {
            if (applicationContext != null && applicationContext.isOpen()) {
                applicationContext.close();
            }
        } finally {
            dataSourceConfiguration = null;
            applicationContext = null;
        }
    }
}
