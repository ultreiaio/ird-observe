package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ObserveTopiaEntitySqlModelResource;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestSupportWithConfig;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.ClassRule;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptor;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created on 20/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@DatabaseVersionConfiguration(ObserveTestConfiguration.MODEL_VERSION)
@DatabaseLoginConfiguration
@DatabasePasswordConfiguration(ObserveTestConfiguration.H2_PASSWORD)
@DatabaseNameConfiguration(DatabaseName.data)
public abstract class PersistenceTestSupportRead extends TestSupportWithConfig {
    public static final String CLASSIFIER_REFERENTIAL = "referential";
    public static final String CLASSIFIER_DATA_PS = "data-ps";
    public static final String CLASSIFIER_DATA_LL = "data-ll";
    @ClassRule
    public static final IgnoreTestClassRule IGNORE = new IgnoreTestClassRule("Persistence tests", "persistence.test.skip");

    @ClassRule
    public static final PersistenceTestMethodResourceRead localTestMethodResource = new PersistenceTestMethodResourceRead(IGNORE.isIgnore());
    protected final Logger log = LogManager.getLogger(getClass());


    public static Stream<Class<? extends Entity>> streamSortByFqn(Stream<Class<? extends Entity>> entities) {
        return entities.sorted(Comparator.comparing(Class::getName));
    }


    private static Stream<Class<? extends Entity>> referentialStream() {
        return streamSortByFqn(ObserveTopiaEntitySqlModelResource.get().getModel().getDescriptorsByType().keySet().stream().filter(PersistenceTestSupportWrite::isReferential));
    }

    private static Stream<Class<? extends Entity>> dataPsStream() {
        return streamSortByFqn(ObserveTopiaEntitySqlModelResource.get().getModel().getDescriptorsByType().keySet().stream().filter(PersistenceTestSupportWrite::isDataPs));
    }

    private static Stream<Class<? extends Entity>> dataLlStream() {
        return streamSortByFqn(ObserveTopiaEntitySqlModelResource.get().getModel().getDescriptorsByType().keySet().stream().filter(PersistenceTestSupportWrite::isDataLl));
    }

    protected Stream<Class<? extends Entity>> stream(String classifier) {
        switch (classifier) {
            case CLASSIFIER_REFERENTIAL:
                return referentialStream();
            case CLASSIFIER_DATA_PS:
                return dataPsStream();
            case CLASSIFIER_DATA_LL:
                return dataLlStream();
            default:
                throw new IllegalStateException("Can't manage classifier: " + classifier);
        }
    }

    protected List<String> getReplicationOrder(String classifier) {
        TopiaEntitySqlModel model = ObserveTopiaEntitySqlModelResource.get().getModel();
        switch (classifier) {
            case CLASSIFIER_REFERENTIAL:
                return model.getReplicationOrderForStandalone();
            case CLASSIFIER_DATA_PS:
                TopiaEntitySqlDescriptor entryPointPs = model.getDescriptor(fr.ird.observe.entities.data.ps.common.Trip.class);
                return model.getReplicationOrderByType(entryPointPs.getTable().getEntityName());
            case CLASSIFIER_DATA_LL:
                TopiaEntitySqlDescriptor entryPointLl = model.getDescriptor(fr.ird.observe.entities.data.ll.common.Trip.class);
                return model.getReplicationOrderByType(entryPointLl.getTable().getEntityName());
            default:
                throw new IllegalStateException("Can't manage classifier: " + classifier);
        }
    }
}

