package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.LastUpdateDate;
import fr.ird.observe.entities.ObserveTopiaEntitySqlModelResource;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestSupportWithConfig;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptors;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlan;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanModel;

import java.util.List;

/**
 * Created on 22/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
@DatabaseVersionConfiguration(ObserveTestConfiguration.MODEL_VERSION)
@DatabaseLoginConfiguration
@DatabasePasswordConfiguration(ObserveTestConfiguration.H2_PASSWORD)
@DatabaseNameConfiguration(DatabaseName.data)
public abstract class PersistenceTestSupportWrite extends TestSupportWithConfig {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE = new IgnoreTestClassRule("Persistence tests", "persistence.test.skip");

    @ClassRule
    public static final PersistenceTestClassResource TOPIA_TEST_CLASS_RESOURCE = new PersistenceTestClassResource(IGNORE.isIgnore());
    public static final String CLASSIFIER_REFERENTIAL = "referential";
    public static final String CLASSIFIER_DATA_PS = "data-ps";
    public static final String CLASSIFIER_DATA_LL = "data-ll";
    @Rule
    public final PersistenceTestMethodResourceWrite localTestMethodResource = new PersistenceTestMethodResourceWrite(TOPIA_TEST_CLASS_RESOURCE);
    protected final Logger log = LogManager.getLogger(getClass());

    public static boolean isReferential(Class<? extends Entity> entityType) {
        return ReferentialEntity.class.isAssignableFrom(entityType) && !LastUpdateDate.class.equals(entityType);
    }

    public static boolean isData(Class<? extends Entity> entityType) {
        return DataEntity.class.isAssignableFrom(entityType);
    }

    public static boolean isDataPs(Class<? extends Entity> entityType) {
        return isData(entityType) && entityType.getName().contains(".ps.");
    }

    public static boolean isDataLl(Class<? extends Entity> entityType) {
        return isData(entityType) && entityType.getName().contains(".ll.");
    }

    public static void assertOrder(String exceptedOrderFixtures, List<String> actualOrder) {
        assertOrder(ObservePersistenceFixtures.loadFixturesList(exceptedOrderFixtures), actualOrder);
    }

    public static void assertOrder(List<String> exceptedOrder, List<String> actualOrder) {
        if (!ObservePersistenceFixtures.WITH_ASSERT) {
            for (String actualName : actualOrder) {
                System.out.println(actualName);
            }
        }
        int index = 0;
        for (String exceptedName : exceptedOrder) {
            String actualName = actualOrder.get(index++);
            if (ObservePersistenceFixtures.WITH_ASSERT) {
                Assert.assertEquals(String.format("Wrong value at index %d", index - 1), exceptedName, actualName);
            }
        }
    }

    public static TopiaEntitySqlDescriptors descriptorForReplication(String classifier) {
        TopiaEntitySqlModel model = ObserveTopiaEntitySqlModelResource.get().getModel();
        switch (classifier) {
            case CLASSIFIER_REFERENTIAL:
                return model.getReplicationOrderWithStandaloneDescriptors();
            case CLASSIFIER_DATA_PS:
                return model.getReplicationOrderDescriptors(fr.ird.observe.entities.data.ps.common.Trip.class.getName());
            case CLASSIFIER_DATA_LL:
                return model.getReplicationOrderDescriptors(fr.ird.observe.entities.data.ll.common.Trip.class.getName());
            default:
                throw new IllegalStateException("Can't manage classifier: " + classifier);
        }
    }

    public static TopiaEntitySqlCopyPlan copyPlan(String classifier) {
        TopiaEntitySqlCopyPlanModel model = ObserveTopiaEntitySqlModelResource.get().getCopyPlanModel();
        switch (classifier) {
            case CLASSIFIER_REFERENTIAL:
                return model.getStandalonePlan();
            case CLASSIFIER_DATA_PS:
                return model.getEntryPointPlan(fr.ird.observe.entities.data.ps.common.Trip.class.getName());
            case CLASSIFIER_DATA_LL:
                return model.getEntryPointPlan(fr.ird.observe.entities.data.ll.common.Trip.class.getName());
            default:
                throw new IllegalStateException("Can't manage classifier: " + classifier);
        }
    }

    @After
    public void tearDown() {
        localTestMethodResource.close();
    }
}
