package fr.ird.observe.entities.data.ll.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.ll.common.VesselActivityReference;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ll.common.VesselActivity;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.test.ObserveFixtures;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * Created at 15/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class TripTopiaDaoTest extends PersistenceTestSupportRead {

    private String tripId;

    @Before
    public void setUp() {
        tripId = ObservePersistenceFixtures.getVariable("data.ll.common.Trip.id");
    }

    @Test
    public void loadSqlQueries() {
        ObservePersistenceFixtures.assertSqlQueries(Trip.class, 8);
    }

    @Test
    public void getObservationActivityPoint() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<TripMapPoint> actual = Trip.SPI.getDao(persistenceContext).getObservationActivityPoint(tripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(29, actual.size());
        }
    }

    @Test
    public void getLogbookActivityPoint() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<TripMapPoint> actual = Trip.SPI.getDao(persistenceContext).getLogbookActivityPoint(tripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(51, actual.size());
        }
    }

    @Test
    public void buildMap() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            Trip trip = dao.forTopiaIdEquals(tripId).findAnyOrNull();
            TripMapConfigDto tripMapConfig = new TripMapConfigDto();
            tripMapConfig.setAddLogbook(true);
            tripMapConfig.setAddLogbookTripSegment(true);
            tripMapConfig.setAddObservations(true);
            tripMapConfig.setAddObservationsTripSegment(true);
            tripMapConfig.setTripId(tripId);
            LinkedHashSet<TripMapPoint> actual = dao.buildMap(tripMapConfig, trip);
            Assert.assertNotNull(actual);
            Assert.assertEquals(82, actual.size());
        }
    }

    @Test
    public void updateTripVersion() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.updateTripVersionToSql(tripId, new Timestamp(ObserveFixtures.DATE.getTime()));
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ll_common.Trip SET topiaVersion = topiaVersion + 1, lastUpdateDate = '2016-08-21 17:15:36'::timestamp WHERE topiaId = 'fr.ird.data.ll.common.Trip#1573218053521#0.35390398604784334';", actual);
        }
    }

    @Test
    public void getPairingLogbookActivities() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<VesselActivity> vesselActivities = VesselActivity.loadEntities(persistenceContext);
            Map<String, VesselActivityReference> vesselActivitiesById = VesselActivity.toReferenceSet(ReferentialLocale.FR, vesselActivities, null, null).toId();
            List<fr.ird.observe.dto.data.ll.observation.ActivityReference> observationActivities = Trip.SPI.getDao(persistenceContext).getPairingObservationActivities(vesselActivitiesById, tripId);
            List<fr.ird.observe.dto.data.ll.logbook.ActivityReference> actual = Trip.SPI.getDao(persistenceContext).getPairingLogbookActivities(vesselActivitiesById, tripId, observationActivities);
            Assert.assertNotNull(actual);
            Assert.assertEquals(21, actual.size());
        }
    }

    @Test
    public void getPairingObservationActivities() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<VesselActivity> vesselActivities = VesselActivity.loadEntities(persistenceContext);
            Map<String, VesselActivityReference> vesselActivitiesById = VesselActivity.toReferenceSet(ReferentialLocale.FR, vesselActivities, null, null).toId();
            List<fr.ird.observe.dto.data.ll.observation.ActivityReference> actual = Trip.SPI.getDao(persistenceContext).getPairingObservationActivities(vesselActivitiesById, tripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(8, actual.size());
        }
    }
}
