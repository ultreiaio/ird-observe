package fr.ird.observe.entities.data.ll.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.test.ObserveFixtures;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;

/**
 * Created at 26/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ActivityTopiaDaoTest extends PersistenceTestSupportRead {

    @Test
    public void loadSqlQueries() {
        ObservePersistenceFixtures.assertSqlQueries(Activity.class, 1);
    }

    @Test
    public void applyActivityPairing() {
        String activityId = ObservePersistenceFixtures.getVariable("data.ll.logbook.Activity.id");
        String relatedObservedActivityId = ObservePersistenceFixtures.getVariable("data.ll.observation.Activity.id");

        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            try {
                int actual = Activity.getDao(persistenceContext).applyActivityPairing(new Timestamp(ObserveFixtures.DATE.getTime()), relatedObservedActivityId, activityId);
                Assert.assertEquals(1, actual);
            } finally {
                persistenceContext.rollback();
            }
        }
    }
}
