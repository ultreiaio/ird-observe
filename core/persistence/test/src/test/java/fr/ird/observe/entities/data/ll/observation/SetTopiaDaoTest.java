package fr.ird.observe.entities.data.ll.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * FIXME Add cases in tck to have some results here.
 * <p>
 * Created at 15/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class SetTopiaDaoTest extends PersistenceTestSupportRead {

    private String setId;

    @Before
    public void setUp() {
        setId = ObservePersistenceFixtures.getVariable("data.ll.observation.Set.id");
    }

    @Test
    public void loadSqlQueries() {
        ObservePersistenceFixtures.assertSqlQueries(Set.class, 7);
    }

    @Test
    public void getSectionUsed() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            java.util.Set<String> actual = Set.SPI.getDao(persistenceContext).getSectionUsed(setId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(0, actual.size());
        }
    }

    @Test
    public void getBasketUsed() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            java.util.Set<String> actual = Set.SPI.getDao(persistenceContext).getSectionUsed(setId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(0, actual.size());
        }
    }

    @Test
    public void getBranchlineUsed() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            java.util.Set<String> actual = Set.SPI.getDao(persistenceContext).getBranchlineUsed(setId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(0, actual.size());
        }
    }

    @Test
    public void getCatchSectionUsed() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<String> actual = Set.SPI.getDao(persistenceContext).getCatchSectionUsed(setId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(0, actual.size());
        }
    }

    @Test
    public void getCatchBasketUsed() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<String> actual = Set.SPI.getDao(persistenceContext).getCatchBasketUsed(setId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(0, actual.size());
        }
    }

    @Test
    public void getCatchBranchlineUsed() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<String> actual = Set.SPI.getDao(persistenceContext).getCatchBranchlineUsed(setId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(0, actual.size());
        }
    }

    @Test
    public void getTdrSectionUsed() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<String> actual = Set.SPI.getDao(persistenceContext).getTdrSectionUsed(setId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(0, actual.size());
        }
    }

    @Test
    public void getTdrBasketUsed() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<String> actual = Set.SPI.getDao(persistenceContext).getTdrBasketUsed(setId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(0, actual.size());
        }
    }

    @Test
    public void getTdrBranchlineUsed() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<String> actual = Set.SPI.getDao(persistenceContext).getTdrBranchlineUsed(setId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(0, actual.size());
        }
    }

    @Test
    public void sanitizeLonglineElements() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            try {
                int actual = Set.SPI.getDao(persistenceContext).sanitizeLonglineElements();
                Assert.assertEquals(37, actual);
            } finally {
                persistenceContext.rollback();
            }
        }
    }
}
