package fr.ird.observe.entities.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.SqlHelper;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapPoint;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ps.common.VesselActivity;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.test.ObserveFixtures;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created at 15/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class TripTopiaDaoTest extends PersistenceTestSupportRead {

    private String tripId;
    private String targetTripId;

    @Before
    public void setUp() {
        tripId = ObservePersistenceFixtures.getVariable("data.ps.common.Trip.id");
        targetTripId = ObservePersistenceFixtures.getVariable("data.ps.common.Trip.id.move");
    }

    @Test
    public void loadSqlQueries() {
        ObservePersistenceFixtures.assertSqlQueries(Trip.class, 23);
    }

    @Test
    public void getLogbookSetActivities() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Map<String, VesselActivityReference> vesselActivities = VesselActivity.loadEntities(persistenceContext).stream().map(e -> VesselActivity.toReference(ReferentialLocale.FR, e)).collect(Collectors.toMap(VesselActivityReference::getTopiaId, Function.identity()));
            List<ActivityStubDto> actual = Trip.SPI.getDao(persistenceContext).getLogbookSetActivities(tripId, vesselActivities);
            Assert.assertNotNull(actual);
            Assert.assertEquals(3, actual.size());
        }
    }

    @Test
    public void getLogbookWellPlanActivities() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Map<String, VesselActivityReference> vesselActivities = VesselActivity.loadEntities(persistenceContext).stream().map(e -> VesselActivity.toReference(ReferentialLocale.FR, e)).collect(Collectors.toMap(VesselActivityReference::getTopiaId, Function.identity()));
            List<ActivityStubDto> actual = Trip.SPI.getDao(persistenceContext).getLogbookWellPlanActivities(tripId, vesselActivities);
            Assert.assertNotNull(actual);
            Assert.assertEquals(3, actual.size());
        }
    }

    @Test
    public void isActivitiesAcquisitionModeByTimeEnabled() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            boolean actual = Trip.SPI.getDao(persistenceContext).isActivitiesAcquisitionModeByTimeEnabled(tripId);
            Assert.assertTrue(actual);
        }
    }

    @Test
    public void getObservationActivityPoint() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<TripMapPoint> actual = Trip.SPI.getDao(persistenceContext).getObservationActivityPoint(tripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(140, actual.size());
        }
    }

    @Test
    public void getLogbookActivityPoint() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<TripMapPoint> actual = Trip.SPI.getDao(persistenceContext).getLogbookActivityPoint(tripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(3, actual.size());
        }
    }

    @Test
    public void getLogbookTransmittingBuoyPoint() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<TripMapPoint> actual = Trip.SPI.getDao(persistenceContext).getLogbookTransmittingBuoyPoint(tripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(0, actual.size());
        }
    }

    @Test
    public void getLogbookWellIdsFromSample() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Set<String> actual = Trip.SPI.getDao(persistenceContext).getLogbookWellIdsFromSample(tripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(1, actual.size());
        }
    }

    @Test
    public void getLogbookWellIdsFromWellPlan() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Set<String> actual = Trip.SPI.getDao(persistenceContext).getLogbookWellIdsFromWellPlan(tripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(1, actual.size());
        }
    }

    @Test
    public void getLogbookActivityIdsFromWellPlan() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Set<String> actual = Trip.SPI.getDao(persistenceContext).getLogbookActivityIdsFromWellPlan(tripId, "3T");
            Assert.assertNotNull(actual);
            Assert.assertEquals(1, actual.size());
        }
    }

    @Test
    public void getLogbookActivityIdsFromSample() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Set<String> actual = Trip.SPI.getDao(persistenceContext).getLogbookActivityIdsFromSample(tripId, "3T");
            Assert.assertNotNull(actual);
            Assert.assertEquals(1, actual.size());
        }
    }

    @Test
    public void buildMap() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            Trip trip = dao.forTopiaIdEquals(tripId).findAnyOrNull();
            TripMapConfigDto tripMapConfig = new TripMapConfigDto();
            tripMapConfig.setAddLogbook(true);
            tripMapConfig.setAddLogbookTripSegment(true);
            tripMapConfig.setAddObservations(true);
            tripMapConfig.setAddObservationsTripSegment(true);
            tripMapConfig.setTripId(tripId);
            LinkedHashSet<TripMapPoint> actual = dao.buildMap(tripMapConfig, trip);
            Assert.assertNotNull(actual);
            Assert.assertEquals(145, actual.size());
        }
    }

    @Test
    public void copyObservationsMetadataToSql() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Trip trip = Trip.SPI.loadEntity(Locale.FRANCE, persistenceContext, tripId);
            trip.setObservationsComment("Comment\n");
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.copyObservationsMetadataToSql(SqlHelper.escapeComment(false), trip, targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET\n" +
                                        "    observationsProgram = 'fr.ird.referential.ps.common.Program#1363095174385#0.011966550987014823',\n" +
                                        "    observationsAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#001',\n" +
                                        "    observationsDataEntryOperator = 'fr.ird.referential.common.Person#1371455748566#0.18706650956418513',\n" +
                                        "    observationsDataQuality = NULL,\n" +
                                        "    observer = 'fr.ird.referential.common.Person#1371455748566#0.18706650956418513',\n" +
                                        "    observationsComment = STRINGDECODE('Comment'),\n" +
                                        "    formsUrl = NULL,\n" +
                                        "    reportsUrl = NULL\n" +
                                        "    WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void removeObservationsMetadata() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.removeObservationsMetadataToSql(targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET\n" +
                                        "    observationsProgram = NULL,\n" +
                                        "    observationsAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#099',\n" +
                                        "    observationsDataEntryOperator = NULL,\n" +
                                        "    observationsDataQuality = NULL,\n" +
                                        "    observer = NULL,\n" +
                                        "    observationsComment = NULL,\n" +
                                        "    formsUrl = NULL,\n" +
                                        "    reportsUrl = NULL\n" +
                                        "    WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void copyLandingMetadata() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Trip trip = Trip.SPI.loadEntity(Locale.FRANCE, persistenceContext, tripId);
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.copyLandingMetadataToSql(trip, targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET\n" +
                                        "    landingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#999',\n" +
                                        "    landingTotalWeight = NULL\n" +
                                        "    WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void removeLandingMetadata() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.removeLandingMetadataToSql(targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET\n" +
                                        "    landingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#099',\n" +
                                        "    landingTotalWeight = NULL\n" +
                                        "    WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void copyLogbookMetadata() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Trip trip = Trip.SPI.loadEntity(Locale.FRANCE, persistenceContext, tripId);
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.copyLogbookMetadataToSql(SqlHelper.escapeComment(false), trip, targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET\n" +
                                        "    logbookProgram = 'fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444',\n" +
                                        "    logbookAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#999',\n" +
                                        "    logbookDataEntryOperator = 'fr.ird.referential.common.Person#1446117474872#0.3673578034116256',\n" +
                                        "    logbookDataQuality = 'fr.ird.referential.common.DataQuality#0#5',\n" +
                                        "    targetWellsSamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#999',\n" +
                                        "    advancedSamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#999',\n" +
                                        "    departureWellContentStatus = 'fr.ird.referential.ps.logbook.WellContentStatus#1464000000000#03',\n" +
                                        "    landingWellContentStatus = 'fr.ird.referential.ps.logbook.WellContentStatus#1464000000000#03',\n" +
                                        "    logbookComment = NULL,\n" +
                                        "    timeAtSea = NULL,\n" +
                                        "    fishingTime = NULL,\n" +
                                        "    loch = NULL\n" +
                                        "    WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void copyLogbookDataEntryOperator() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Trip trip = Trip.SPI.loadEntity(Locale.FRANCE, persistenceContext, tripId);
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.copyLogbookDataEntryOperatorToSql(trip, targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET logbookDataEntryOperator = 'fr.ird.referential.common.Person#1446117474872#0.3673578034116256' WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void copyLogbookDataQuality() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Trip trip = Trip.SPI.loadEntity(Locale.FRANCE, persistenceContext, tripId);
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.copyLogbookDataQualityToSql(trip, targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET logbookDataQuality = 'fr.ird.referential.common.DataQuality#0#5' WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void copyLogbookComment() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Trip trip = Trip.SPI.loadEntity(Locale.FRANCE, persistenceContext, tripId);
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.copyLogbookCommentToSql(SqlHelper.escapeComment(false), trip, targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET logbookComment = NULL WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void removeLogbookMetadata() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.removeLogbookMetadataToSql(targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET\n" +
                                        "    logbookProgram = NULL,\n" +
                                        "    logbookAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#099',\n" +
                                        "    logbookDataEntryOperator = NULL,\n" +
                                        "    logbookDataQuality = NULL,\n" +
                                        "    targetWellsSamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#099',\n" +
                                        "    advancedSamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#099',\n" +
                                        "    departureWellContentStatus = NULL,\n" +
                                        "    landingWellContentStatus = NULL,\n" +
                                        "    logbookComment = NULL,\n" +
                                        "    timeAtSea = NULL,\n" +
                                        "    fishingTime =NULL,\n" +
                                        "    loch = NULL\n" +
                                        "    WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void copyLocalmarketMetadata() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            Trip trip = Trip.SPI.loadEntity(Locale.FRANCE, persistenceContext, tripId);
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.copyLocalmarketMetadataToSql(trip, targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET\n" +
                                        "    localMarketAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#999',\n" +
                                        "    localMarketSurveySamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#999',\n" +
                                        "    localMarketWellsSamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#999',\n" +
                                        "    localMarketTotalWeight = NULL\n" +
                                        "    WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void removeLocalmarketMetadata() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.removeLocalmarketMetadataToSql(targetTripId);
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET\n" +
                                        "    localMarketAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#099',\n" +
                                        "    localMarketSurveySamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#099',\n" +
                                        "    localMarketWellsSamplingAcquisitionStatus = 'fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#099',\n" +
                                        "    localMarketTotalWeight = NULL\n" +
                                        "    WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }

    @Test
    public void updateTripVersion() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            TripTopiaDao dao = Trip.SPI.getDao(persistenceContext);
            String actual = dao.updateTripVersionToSql(targetTripId, new Timestamp(ObserveFixtures.DATE.getTime()));
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_common.Trip SET topiaVersion = topiaVersion + 1, lastUpdateDate = '2016-08-21 17:15:36'::timestamp WHERE topiaId = 'fr.ird.data.ps.common.Trip#1573473170279#0.39901519164877486';", actual);
        }
    }
}
