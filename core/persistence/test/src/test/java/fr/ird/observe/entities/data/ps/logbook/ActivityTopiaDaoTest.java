package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.test.ObserveFixtures;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created at 15/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ActivityTopiaDaoTest extends PersistenceTestSupportRead {

    private String routeId;

    @Before
    public void setUp() {
        routeId = ObservePersistenceFixtures.getVariable("data.ps.logbook.Route.id");
    }

    @Test
    public void loadSqlQueries() {
        ObservePersistenceFixtures.assertSqlQueries(Activity.class, 4);
    }

    @Test
    public void activityTimeAndOrderRecordByNumber() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<ActivityTopiaDao.ActivityNumberRecord> actual = Activity.getDao(persistenceContext).activityTimeAndOrderRecordByNumber(routeId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(1, actual.size());
        }
    }

    @Test
    public void activityTimeAndOrderRecordByTime() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            List<ActivityTopiaDao.ActivityNumberRecord> actual = Activity.getDao(persistenceContext).activityTimeAndOrderRecordByTime(routeId);
            Assert.assertNotNull(actual);
            Assert.assertEquals(1, actual.size());
        }
    }

    @Test
    public void generateUpdateNumberAndVersionSql() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            String actual = Activity.getDao(persistenceContext).generateUpdateNumberAndVersionSql(routeId, 2, new Timestamp(ObserveFixtures.DATE.getTime()));
            Assert.assertNotNull(actual);
            Assert.assertEquals("UPDATE ps_logbook.Activity SET number = 2, topiaVersion = topiaVersion + 1, lastUpdateDate = '2016-08-21 17:15:36'::timestamp WHERE topiaId = 'fr.ird.data.ps.logbook.Route#1616767950667#0.2733836567509864';", actual);
        }
    }

    @Test
    public void applyActivityPairing() {
        String activityId = ObservePersistenceFixtures.getVariable("data.ps.logbook.Activity.id");
        String relatedObservedActivityId = ObservePersistenceFixtures.getVariable("data.ps.observation.Activity.id");

        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {

            try {
                int actual = Activity.getDao(persistenceContext).applyActivityPairing(new Timestamp(ObserveFixtures.DATE.getTime()), relatedObservedActivityId, activityId);
                Assert.assertEquals(1, actual);
            } finally {
                persistenceContext.rollback();
            }
        }
    }
}
