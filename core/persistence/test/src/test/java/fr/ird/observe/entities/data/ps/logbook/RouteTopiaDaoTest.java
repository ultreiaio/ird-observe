package fr.ird.observe.entities.data.ps.logbook;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityReference;
import fr.ird.observe.dto.data.ps.logbook.RouteReference;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.referential.ps.common.VesselActivity;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created at 18/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class RouteTopiaDaoTest extends PersistenceTestSupportRead {

    @Test
    public void getPsLogbookRouteQueries() {
        ObservePersistenceFixtures.assertSqlQueries(Route.class, 1);
    }

    @Test
    public void GetActivitiesByRouteId() {
        String tripId = ObservePersistenceFixtures.getVariable("data.ps.common.Trip.id");
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {

            Trip trip = Trip.SPI.loadEntity(Locale.FRANCE, persistenceContext, tripId);
            TripDto tripDto = new TripDto();
            Trip.SPI.toDto(ReferentialLocale.FR, trip, tripDto);
            List<VesselActivity> vesselActivities = VesselActivity.loadEntities(persistenceContext);
            Map<String, VesselActivityReference> vesselActivitiesById = VesselActivity.toReferenceSet(ReferentialLocale.FR, vesselActivities, null, null).toId();
            Map<fr.ird.observe.dto.data.ps.observation.RouteReference, List<fr.ird.observe.dto.data.ps.observation.ActivityReference>> observationRoutes = fr.ird.observe.entities.data.ps.observation.Route.SPI.getDao(persistenceContext).getActivitiesByRoute(vesselActivitiesById, tripDto);
            Map<RouteReference, List<ActivityReference>> actual = Route.SPI.getDao(persistenceContext).getActivitiesByRoute(vesselActivitiesById, tripDto, observationRoutes);
            Assert.assertNotNull(actual);
            Assert.assertEquals(2, actual.size());
        }
    }
}
