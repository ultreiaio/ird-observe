package fr.ird.observe.entities.data.ps.observation;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.observation.ActivityReference;
import fr.ird.observe.dto.data.ps.observation.RouteReference;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.referential.ps.common.VesselActivity;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.persistence.test.PersistenceTestSupportWrite;
import fr.ird.observe.test.ObserveFixtures;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created at 15/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class RouteTopiaDaoTest extends PersistenceTestSupportRead {

    public static final int EXPECTED_END_TIME_STAMP = 3;
    public static final int EXPECTED_HAULING_START_TIME_STAMP = 0;
    public static final int EXPECTED_HAULING_END_TIME_STAMP = 3;
    private String routeId;

    @Before
    public void setUp() {
        routeId = ObservePersistenceFixtures.getVariable("data.ps.observation.Route.id");
    }

    @Test
    public void getPsObservationRouteQueries() {
        ObservePersistenceFixtures.assertSqlQueries(Route.class, 4);
    }

    @Test
    public void updateActivitiesDate() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            int actual = Route.SPI.getDao(persistenceContext).updateActivitiesDate(routeId, new Date(ObserveFixtures.DATE.getTime()));
            Assert.assertEquals(EXPECTED_HAULING_START_TIME_STAMP + EXPECTED_HAULING_END_TIME_STAMP + EXPECTED_END_TIME_STAMP, actual);
        }
    }

    @Test
    public void updateSetEndTimeStamp() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            int actual = Route.SPI.getDao(persistenceContext).updateSetEndTimeStamp(routeId, new Date(DateUtils.addDays(ObserveFixtures.DATE, 1).getTime()));
            Assert.assertEquals(EXPECTED_END_TIME_STAMP, actual);
        }
    }

    @Test
    public void updateSetHaulingStartTimeStamp() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            int actual = Route.SPI.getDao(persistenceContext).updateSetHaulingStartTimeStamp(routeId, new Date(DateUtils.addDays(ObserveFixtures.DATE, 2).getTime()));
            Assert.assertEquals(EXPECTED_HAULING_START_TIME_STAMP, actual);
        }
    }

    @Test
    public void updateSetHaulingEndTimeStamp() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            int actual = Route.SPI.getDao(persistenceContext).updateSetHaulingEndTimeStamp(routeId, new Date(DateUtils.addDays(ObserveFixtures.DATE, EXPECTED_END_TIME_STAMP).getTime()));
            Assert.assertEquals(EXPECTED_HAULING_END_TIME_STAMP, actual);
        }
    }

    @Test
    public void GetActivitiesByRouteId() {
        String tripId = ObservePersistenceFixtures.getVariable("data.ps.common.Trip.id");
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {

            Trip trip = Trip.SPI.loadEntity(Locale.FRANCE, persistenceContext, tripId);
            TripDto tripDto = new TripDto();
            Trip.SPI.toDto(ReferentialLocale.FR, trip, tripDto);
            List<VesselActivity> vesselActivities = VesselActivity.loadEntities(persistenceContext);
            Map<String, VesselActivityReference> vesselActivitiesById = VesselActivity.toReferenceSet(ReferentialLocale.FR, vesselActivities, null, null).toId();
            Map<RouteReference, List<ActivityReference>> actual = Route.SPI.getDao(persistenceContext).getActivitiesByRoute(vesselActivitiesById, tripDto);
            Assert.assertNotNull(actual);
            Assert.assertEquals(10, actual.size());
        }
    }
}
