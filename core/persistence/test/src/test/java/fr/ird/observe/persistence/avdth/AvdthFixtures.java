package fr.ird.observe.persistence.avdth;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.ObserveTestConfiguration;
import org.junit.Assert;

import java.nio.file.Path;
import java.util.Map;

/**
 * Created on 24/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class AvdthFixtures extends ObserveFixtures {

    public static final Map<String, String> AVDTH = loadFixturesMap("avdth");
    public static final String AVDTH_CACHE_PATH = "observetest.avdth.cache.path";
    /**
     * To force prepare even if prepare step is not valid.
     */
    public static final boolean FORCE_PREPARE = Boolean.parseBoolean(System.getProperty("test_forcePrepare", "false"));
    /**
     * To force import  even if prepare step is not valid.
     */
    public static final boolean FORCE_IMPORT = Boolean.parseBoolean(System.getProperty("test_forceImport", "false"));
    /**
     * To launch import for all years.
     */
    public static final boolean IMPORT_ALL_YEARS = Boolean.parseBoolean(System.getProperty("test_allYears", "false"));
    /**
     * Root path of avdth databases.
     */
    private static Path avdthCachePath;

    public static Path getAvdthCachePath() {
        if (avdthCachePath == null) {
            avdthCachePath = Path.of(ObserveTestConfiguration.getTestPropertyAsString(AVDTH_CACHE_PATH));
        }
        return avdthCachePath;
    }

    public static void assertAvdthFixture(String fixtureName, Object actual) {
        String expected = AVDTH.get(fixtureName);
        assertFixture(fixtureName, expected, actual);
    }

    public static void assertFixture(String fixtureName, Object expected, Object actual) {
        Assert.assertEquals("Could not find fixtures " + fixtureName, expected, actual);
    }

}
