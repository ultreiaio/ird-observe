package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsCommon;
import fr.ird.observe.services.service.data.ps.AvdthDataImportResult;
import fr.ird.observe.entities.data.ps.logbook.SampleActivity;
import org.junit.runners.Parameterized;

import java.nio.file.Path;
import java.util.List;

/**
 * Created on 24/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class AvdthAtlanticDataBuilderAddSampleActivitiesTest extends AvdthDataBuilderByYearTestSupport {

    public static final Path ROOT_PATH = getPath("OA");


    public static final String TEST_ADD_SAMPLE_ACTIVITY = "OA_1977_V35.mdb";

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Iterable<?> data() {
        List<String> databases = allDatabases(ROOT_PATH);
        databases.removeIf(n -> !TEST_ADD_SAMPLE_ACTIVITY.equals(n));
        return databases;
    }

    @Override
    protected void checkNotValidResult(AvdthDataImportResult result) {
        if (!result.getExportResult().containsKey(SampleActivity.class.getName())) {
            super.checkNotValidResult(result);
        }
    }

    @Override
    protected Path getRootPath() {
        return ROOT_PATH;
    }

    @Override
    protected String getOceanId() {
        return ProtectedIdsCommon.COMMON_OCEAN_ATLANTIC;
    }
}
