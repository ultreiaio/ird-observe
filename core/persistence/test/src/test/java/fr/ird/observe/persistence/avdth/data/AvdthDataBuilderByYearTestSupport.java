package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.persistence.avdth.AvdthFixtures;

import java.nio.file.Path;
import java.util.List;

/**
 * Created on 28/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class AvdthDataBuilderByYearTestSupport extends AvdthDataBuilderTestSupport {

    public static List<String> getDatabases(Path path) {
        List<String> databases = allDatabases(path);
        return AvdthFixtures.IMPORT_ALL_YEARS || databases.size() < 3 ? databases : databases.subList(0, 3);
    }

    @Override
    protected boolean doInlineImport() {
        return true;
    }

    @Override
    protected boolean forcePrepare() {
        return AvdthFixtures.FORCE_PREPARE;
    }

    @Override
    protected boolean forceImport() {
        return AvdthFixtures.FORCE_IMPORT;
    }
}
