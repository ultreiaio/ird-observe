package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.persistence.avdth.AvdthFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportWrite;
import fr.ird.observe.services.service.data.ps.AvdthDataImportConfiguration;
import fr.ird.observe.services.service.data.ps.AvdthDataImportResult;
import fr.ird.observe.services.service.data.ps.MissingReferentialException;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import io.ultreia.java4all.util.SortedProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 24/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@RunWith(Parameterized.class)
public abstract class AvdthDataBuilderTestSupport extends PersistenceTestSupportWrite {
    private static final Logger log = LogManager.getLogger(AvdthDataBuilderTestSupport.class);
    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Avdth", "persistence.avdth.test.skip");

    public static final SortedProperties COUNT = new SortedProperties();

    @Parameterized.Parameter
    public String dbName;

    @AfterClass
    public static void afterTest() throws Exception {
        if (IGNORE.isIgnore()) {
            return;
        }
        COUNT.store(System.out, "");
    }

    @BeforeClass
    public static void beforeTest() {
        if (IGNORE.isIgnore()) {
            return;
        }
        if (!AvdthFixtures.WITH_ASSERT) {
            for (Map.Entry<String, String> entry : AvdthFixtures.AVDTH.entrySet()) {
                COUNT.setProperty(entry.getKey(), entry.getValue());
            }
        }
    }

    public static List<String> allDatabases(Path path) {
        Assume.assumeTrue("Skip, avdth root path " + path + " not found", Files.exists(path));
        try (Stream<Path> pathStream = Files.find(path, 1, (p, b) -> p.toFile().getName().endsWith(".mdb"))) {
            List<String> collect = pathStream.map(p -> p.toFile().getName()).sorted().collect(Collectors.toList());
            Collections.reverse(collect);
            return collect;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Path getPath(String type) {
        return AvdthFixtures.getAvdthCachePath().resolve(type);
    }

    protected abstract Path getRootPath();

    protected abstract String getOceanId();

    protected abstract boolean forcePrepare();

    protected abstract boolean forceImport();

    protected abstract boolean doInlineImport();

    @CopyDatabaseConfiguration
    @Test
    @DatabaseNameConfiguration(DatabaseName.referential)
    public void build() throws SQLException, IOException, MissingReferentialException {
        log.info(String.format("Start for database: %s", dbName));
        Path avdthFile = getRootPath().resolve(dbName);
        Assume.assumeTrue("Skip, avdth file " + avdthFile + " not found", Files.exists(avdthFile));
        Path scriptPath = TOPIA_TEST_CLASS_RESOURCE.getTestDirectory().toPath().resolve("export-" + dbName.replace(".mdb", ".sql"));
        Path scriptTemporaryDirectory = scriptPath.getParent().resolve(scriptPath.toFile().getName().replace(".sql", "-tmp"));

        boolean doInlineImport = doInlineImport();
        AvdthDataImportConfiguration configuration = new AvdthDataImportConfiguration(
                AvdthFixtures.DATE,
                20200601L,
                "fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444",
                getOceanId(),
                ReferentialLocale.FR,
                scriptTemporaryDirectory, forcePrepare(), doInlineImport, avdthFile, scriptPath, new ProgressionModel());

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        AvdthDataImportResult result;
        try {
            result = ImportEngine.run(configuration, localTestMethodResource.getTopiaApplicationContext());
            String resultAsGson = gson.toJson(result);
            log.debug(String.format("Import result:\n%s", resultAsGson));
            // Used for debugging...
            Map<String, Integer> exportResult = result.getExportResult();
            if (!exportResult.isEmpty()) {
                log.info(String.format("Export result:\n%s", exportResult.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("\n"))));
            }
        } finally {
            ObserveUtil.cleanMemory();
        }
        if (!result.isValid()) {
            checkNotValidResult(result);
            return;
        }
        if (!doInlineImport) {
            log.info(String.format("Import to database from script: %s", result.getSqlResultPath()));
            try {
                localTestMethodResource.getTopiaApplicationContext().executeSqlStatements(result.getSqlResultContent());
            } finally {
                ObserveUtil.cleanMemory();
            }
        }
        int actual = result.getExportResult().get(Trip.class.getName());
        COUNT.setProperty(dbName, actual + "");
        int expected = Integer.parseInt(AvdthFixtures.AVDTH.getOrDefault(dbName, actual + ""));
        Assert.assertEquals(expected, actual);
        long countAfter = count();
        log.info(String.format("After - Found %d trip(s) in database.", countAfter));
        Assert.assertEquals(expected, countAfter);
    }

    protected void checkNotValidResult(AvdthDataImportResult result) {
        Map<String, Integer> notReadResult = result.getNotReadResult();
        String message;
        if (notReadResult.isEmpty()) {
            message = result.getMessages() + "";
        } else {
            message = notReadResult + "";
            return;
        }
        Assume.assumeTrue(message, forceImport());
    }

    protected long count() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            return Trip.SPI.getDao(persistenceContext).count();
        }
    }
}
