package fr.ird.observe.persistence.avdth.data;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProtectedIdsCommon;
import org.junit.runners.Parameterized;

import java.nio.file.Path;

/**
 * Created on 19/01/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.24
 */
public class AvdthIndianDataBuilderByYearLatestTest extends AvdthDataBuilderByYearTestSupport {

    public static final Path ROOT_PATH = getPath("OI").resolve("latest");

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Iterable<?> data() {
        return getDatabases(ROOT_PATH);
    }

    @Override
    protected Path getRootPath() {
        return ROOT_PATH;
    }

    @Override
    protected String getOceanId() {
        return ProtectedIdsCommon.COMMON_OCEAN_INDIAN;
    }
}
