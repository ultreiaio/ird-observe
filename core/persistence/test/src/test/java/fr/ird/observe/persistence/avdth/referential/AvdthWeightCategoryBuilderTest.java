package fr.ird.observe.persistence.avdth.referential;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.services.service.data.ps.AvdthReferentialImportConfiguration;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.persistence.avdth.AvdthFixtures;
import fr.ird.observe.persistence.avdth.Query;
import fr.ird.observe.persistence.avdth.data.ImportEngine;
import fr.ird.observe.persistence.test.PersistenceTestMethodResourceRead;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestSupportWithConfig;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created on 21/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@DatabaseVersionConfiguration(ObserveTestConfiguration.MODEL_VERSION)
@DatabaseLoginConfiguration
@DatabasePasswordConfiguration(ObserveTestConfiguration.H2_PASSWORD)
@DatabaseNameConfiguration(DatabaseName.referential)
public class AvdthWeightCategoryBuilderTest extends TestSupportWithConfig {
    @ClassRule
    public static final IgnoreTestClassRule IGNORE = new IgnoreTestClassRule("Persistence tests", "persistence.test.skip");

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Avdth", "persistence.avdth.test.skip");

    @ClassRule
    public static final PersistenceTestMethodResourceRead localTestMethodResource = new PersistenceTestMethodResourceRead(IGNORE.isIgnore() || IGNORE2.isIgnore());

    @Test
    public void build() throws SQLException, IOException {
        Path avdthFile = AvdthFixtures.getAvdthCachePath()
                .resolve("OA/OA_2020_V35.mdb");
        Assume.assumeTrue("Skip, avdth file " + avdthFile + " not found", Files.exists(avdthFile));
        Path scriptPath = localTestMethodResource.getTestDirectory().toPath().resolve("export-" + avdthFile.toFile().getName().replace(".mdb", ".sql"));
        Path scriptTemporaryDirectory = scriptPath.getParent().resolve(scriptPath.toFile().getName().replace(".sql", "-tmp"));

        Path exportFile = scriptTemporaryDirectory.resolve("weight-category-export.sql");
        AvdthReferentialImportConfiguration configuration = new AvdthReferentialImportConfiguration(
                AvdthFixtures.DATE,
                ReferentialLocale.FR,
                avdthFile,
                exportFile,
                new ProgressionModel());
        AvdthWeightCategoryBuilder builder = new AvdthWeightCategoryBuilder(configuration);
        try (Connection conn = Query.create(avdthFile.toFile(), ImportEngine.timeLog)) {
            AvdthReferentialImportResult actual = builder.build(conn, localTestMethodResource.getTopiaApplicationContext());
            Assert.assertNotNull(actual);
            AvdthFixtures.assertAvdthFixture("WeightCategoryExtractor.referentialCount", actual.getReferentialCount() + "");
            AvdthFixtures.assertAvdthFixture("WeightCategoryExtractor.speciesCount", actual.getSpecies().size() + "");
            AvdthFixtures.assertAvdthFixture("WeightCategoryExtractor.categoriesCount", actual.getWeightCategory().size() + "");
        }
    }
}
