package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.LayoutAware;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.DtoEntityContextSupport;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.test.IgnoreTestClassRule;
import io.ultreia.java4all.decoration.Decorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by tchemit on 31/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DecoratorServiceTest extends PersistenceTestSupportRead {

    private static final Logger log = LogManager.getLogger(DecoratorServiceTest.class);
    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    private DecoratorService decoratorService;
    private ObserveBusinessProject businessProject;

    @Before
    public void setUp() {
        businessProject = ObserveBusinessProject.get();
        decoratorService = new DecoratorService(ReferentialLocale.FR);
    }

    @Test
    public void getReferentialReferenceDecorator() {
        businessProject.getReferentialTypes().forEach(this::testReferential);
    }

    @Test
    public void getDataReferenceDecoratorOnPs() {
        businessProject.getPsBusinessModule().getDataTypes().forEach(this::testData);
    }

    @Test
    public void getDataReferenceDecoratorOnLl() {
        businessProject.getLlBusinessModule().getDataTypes().forEach(this::testData);
    }

    private <D extends ReferentialDto, R extends ReferentialDtoReference, E extends ReferentialEntity> void testReferential(Class<D> dtoType) {
        ReferentialDtoEntityContext<D, R, E, ?> typeContext = ObservePersistenceBusinessProject.fromReferentialDto(dtoType);
        test(typeContext);
    }

    private <D extends DataDto, R extends DataDtoReference, E extends DataEntity> void testData(Class<D> dtoType) {
        DataDtoEntityContext<D, R, E, ?> typeContext = ObservePersistenceBusinessProject.fromDataDto(dtoType);
        try {
            typeContext.toReferenceType();
            test(typeContext);
        } catch (NoSuchElementException e) {
            // no reference here
        }
    }

    private <D extends BusinessDto, R extends DtoReference, E extends Entity> void test(DtoEntityContextSupport<D, R, E, ?> spi) {
        Class<D> dtoType = spi.toDtoType();
        Class<R> referenceType = spi.toReferenceType();
        Decorator referenceDecorator = decoratorService.getDecoratorByType(Objects.requireNonNull(referenceType));
        Decorator decorator = decoratorService.getDecoratorByType(dtoType);
        Decorator entityDecorator = decoratorService.getDecoratorByType(spi.toEntityType());
        Assert.assertNotNull("could not find reference decorator for " + dtoType.getName(), decorator);

        log.debug(String.format("Testing on %s", dtoType.getName()));
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.getTopiaApplicationContext().newPersistenceContext()) {

            D newDto = spi.newDto();
            decoratorService.installDecorator(newDto);
            decorate(decorator, newDto);
            R reference1 = spi.toReference(ReferentialLocale.FR, newDto);
            decoratorService.installDecorator(reference1);
            decorate(referenceDecorator, reference1);
            try (Stream<E> stream = spi.getDao(persistenceContext).streamAll().limit(ObservePersistenceFixtures.ENTITIES_LIMIT_SIZE)) {
                stream.forEach(entity -> {
                    D dto = spi.toDto(ReferentialLocale.FR, entity);
                    entity.registerDecorator(entityDecorator);
                    String entityToString = entityDecorator.decorate(entity);
                    Assert.assertNotNull(entityToString);
                    decoratorService.installDecorator(dto);
                    String dtoToString = decorate(decorator, dto);
                    if (!(dto instanceof LayoutAware)) {
                        Assert.assertEquals("For dto type: " + dtoType.getName(), entityToString, dtoToString);
                    }

                    R reference = spi.toReference(ReferentialLocale.FR, dto);
                    decoratorService.installDecorator(reference);
                    String referenceToString = decorate(referenceDecorator, reference);
                    Assert.assertEquals(referenceToString, dtoToString);
                });
            }
        }
    }

    private String decorate(Decorator d, ToolkitId o) {
        String fromDecorator = d.decorate(o);
        String fromToString = o.toString();
        log.trace(String.format("fromDecorator: %s, fromToString: %s", fromDecorator, fromToString));
        Assert.assertNotNull(fromDecorator);
        Assert.assertFalse(fromDecorator.isEmpty());
        Assert.assertEquals(fromDecorator, fromToString);
        return fromDecorator;
    }

}
