package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.LengthWeightParameterNotFoundException;
import fr.ird.observe.entities.ObserveTopiaDaoSupplier;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.common.LengthWeightParameter;
import fr.ird.observe.entities.referential.common.LengthWeightParameterTopiaDao;
import fr.ird.observe.entities.referential.common.LengthWeightParameters;
import fr.ird.observe.entities.referential.common.Ocean;
import fr.ird.observe.entities.referential.common.OceanTopiaDao;
import fr.ird.observe.entities.referential.common.Sex;
import fr.ird.observe.entities.referential.common.SexTopiaDao;
import fr.ird.observe.entities.referential.common.SizeMeasureType;
import fr.ird.observe.entities.referential.common.SizeMeasureTypeTopiaDao;
import fr.ird.observe.entities.referential.common.Species;
import fr.ird.observe.entities.referential.common.SpeciesTopiaDao;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import io.ultreia.java4all.util.Dates;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.Date;
import java.util.Locale;
import java.util.Optional;

/**
 * Test de la classe {@link LengthWeightParameters}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.8
 */
public class LengthWeightParametersTest extends PersistenceTestSupportWrite {
    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    private ObserveTopiaPersistenceContext persistenceContext;
    private DecoratorService decoratorService;

    protected static void createLengthWeightParameter(LengthWeightParameterTopiaDao lengthWeightParameterDao,
                                                      Species species,
                                                      Ocean ocean,
                                                      Sex sex,
                                                      SizeMeasureType sizeMeasureType,
                                                      Date startDate,
                                                      Date endDate) {

        LengthWeightParameter entity = lengthWeightParameterDao.newInstance();

        entity.setSpecies(species);
        entity.setSex(sex);
        entity.setOcean(ocean);
        entity.setSizeMeasureType(sizeMeasureType);
        entity.setStartDate(startDate);
        entity.setEndDate(endDate);
        entity.setStatus(ReferenceStatus.enabled);
        entity.setCoefficients("a=3.8E-5:b=2.78");
        entity.setLengthWeightFormula("a * Math.pow(L, b)");
        entity.setWeightLengthFormula("Math.pow(P/a, 1/b)");
        entity.setLastUpdateDate(new Date());

        lengthWeightParameterDao.create(entity);

    }

    @Before
    public void setUp() {

        persistenceContext = localTestMethodResource.newPersistenceContext();

        decoratorService = new DecoratorService(ReferentialLocale.FR);
        SpeciesTopiaDao speciesDAO = persistenceContext.getCommonSpeciesDao();
        OceanTopiaDao oceanDAO = persistenceContext.getCommonOceanDao();
        SexTopiaDao sexDao = persistenceContext.getCommonSexDao();
        SizeMeasureTypeTopiaDao sizeMeasureTypeDao = persistenceContext.getCommonSizeMeasureTypeDao();

        Optional<Species> optionalSpecies = speciesDAO.forFaoCodeEquals("ALB").tryFindUnique();
        Assert.assertTrue("Could not find species with faoCode: ALB", optionalSpecies.isPresent());

        Optional<Sex> optionalUndeterminedSex = sexDao.forCodeEquals("0").tryFindUnique();
        Assert.assertTrue("Could not find sex with code 0 (Undetermined)", optionalUndeterminedSex.isPresent());

        Optional<Sex> optionalMaleSex = sexDao.forCodeEquals("1").tryFindUnique();
        Assert.assertTrue("Could not find sex with code 1 (male)", optionalMaleSex.isPresent());

        Optional<Sex> optionalFemaleSex = sexDao.forCodeEquals("2").tryFindUnique();
        Assert.assertTrue("Could not find sex with code 2 (female)", optionalFemaleSex.isPresent());

        Optional<SizeMeasureType> optionalSizeMeasureType = sizeMeasureTypeDao.forCodeEquals("FL").tryFindUnique();
        Assert.assertTrue("Could not find sizeMeasureType with code FL", optionalSizeMeasureType.isPresent());
        SizeMeasureType sizeMeasureType = optionalSizeMeasureType.get();

        Date firstStartDate = Dates.createDate(1, 1, 2010);
        Date firstEndDate = Dates.createDate(31, 12, 2010);
        Date secondStartDate = Dates.createDate(1, 1, 2011);

        Optional<Ocean> optionalAtlanticOcean = oceanDAO.forCodeEquals("1").tryFindUnique();
        Assert.assertTrue("Could not find ocean with code 1 (Atlantic)", optionalAtlanticOcean.isPresent());

        Optional<Ocean> optionalIndianOcean = oceanDAO.forCodeEquals("2").tryFindUnique();
        Assert.assertTrue("Could not find ocean with code 2 (Indian)", optionalIndianOcean.isPresent());

        LengthWeightParameterTopiaDao lengthWeightParameterDao = persistenceContext.getCommonLengthWeightParameterDao();

        Species species = optionalSpecies.orElse(null);
        Sex maleSex = optionalMaleSex.orElse(null);
        Ocean atlanticOcean = optionalAtlanticOcean.orElse(null);
        Ocean indianOcean = optionalIndianOcean.orElse(null);

        // Add parameter Male / Atlantique (2010)
        createLengthWeightParameter(lengthWeightParameterDao, species, atlanticOcean, maleSex, sizeMeasureType, firstStartDate, firstEndDate);
        // Add parameter Male / Atlantique (after 2010)
        createLengthWeightParameter(lengthWeightParameterDao, species, atlanticOcean, maleSex, sizeMeasureType, secondStartDate, null);
        // Add parameter Male / Indien (2010)
        createLengthWeightParameter(lengthWeightParameterDao, species, indianOcean, maleSex, sizeMeasureType, firstStartDate, firstEndDate);
        // Add parameter Male / Indien (after 2010)
        createLengthWeightParameter(lengthWeightParameterDao, species, indianOcean, maleSex, sizeMeasureType, secondStartDate, null);

    }

    @After
    public final void tearDown() {
        try {
            persistenceContext.close();
        } finally {
            super.tearDown();
        }
    }

    /**
     * Pour tester que l'on récupère la bonne relation.
     * See http://forge.codelutin.com/issues/7628
     */
    @Test
    @CopyDatabaseConfiguration
    public void testGetCorrectLengthWeightParameter() {

        SpeciesTopiaDao speciesDAO = persistenceContext.getCommonSpeciesDao();
        OceanTopiaDao oceanDAO = persistenceContext.getCommonOceanDao();
        SexTopiaDao sexDao = persistenceContext.getCommonSexDao();
        SizeMeasureTypeTopiaDao sizeMeasureTypeDao = persistenceContext.getCommonSizeMeasureTypeDao();

        Optional<Species> optionalSpecies = speciesDAO.forFaoCodeEquals("ALB").tryFindUnique();
        Assert.assertTrue("Could not find species with faoCode: ALB", optionalSpecies.isPresent());

        Optional<Sex> optionalUndeterminedSex = sexDao.forCodeEquals("0").tryFindUnique();
        Assert.assertTrue("Could not find sex with code 0 (Undetermined)", optionalUndeterminedSex.isPresent());

        Optional<Sex> optionalMaleSex = sexDao.forCodeEquals("1").tryFindUnique();
        Assert.assertTrue("Could not find sex with code 1 (male)", optionalMaleSex.isPresent());

        Optional<Sex> optionalFemaleSex = sexDao.forCodeEquals("2").tryFindUnique();
        Assert.assertTrue("Could not find sex with code 2 (female)", optionalFemaleSex.isPresent());

        Date date1970 = Dates.createDate(1, 1, 1970);
        Date date2009 = Dates.createDate(1, 1, 2009);
        Date date2010 = Dates.createDate(1, 1, 2010);
        Date date2011 = Dates.createDate(1, 1, 2011);

        Optional<Ocean> optionalAtlanticOcean = oceanDAO.forCodeEquals("1").tryFindUnique();
        Assert.assertTrue("Could not find ocean with code 1 (Atlantic)", optionalAtlanticOcean.isPresent());

        Optional<Ocean> optionalIndianOcean = oceanDAO.forCodeEquals("2").tryFindUnique();
        Assert.assertTrue("Could not find ocean with code 2 (Indian)", optionalIndianOcean.isPresent());

        Optional<Ocean> optionalPacificOcean = oceanDAO.forCodeEquals("3").tryFindUnique();
        Assert.assertTrue("Could not find ocean with code 3 (Pacific)", optionalPacificOcean.isPresent());

        Species species = optionalSpecies.orElse(null);
        Sex undeterminedSex = optionalUndeterminedSex.orElse(null);
        Sex maleSex = optionalMaleSex.orElse(null);
        Sex femaleSex = optionalFemaleSex.orElse(null);
        Ocean atlanticOcean = optionalAtlanticOcean.orElse(null);
        Ocean indianOcean = optionalIndianOcean.orElse(null);
        Ocean pacificOcean = optionalPacificOcean.orElse(null);


        Optional<SizeMeasureType> optionalSizeMeasureType = sizeMeasureTypeDao.forCodeEquals("FL").tryFindUnique();
        Assert.assertTrue("Could not find sizeMeasureType with code FL", optionalSizeMeasureType.isPresent());
        SizeMeasureType sizeMeasureType = optionalSizeMeasureType.get();

        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, null, undeterminedSex, date2009, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, undeterminedSex, undeterminedSex, date2009, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, maleSex, undeterminedSex, date2009, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, femaleSex, undeterminedSex, date2009, date1970, sizeMeasureType);

        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, null, undeterminedSex, date2010, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, undeterminedSex, undeterminedSex, date2010, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, maleSex, maleSex, date2010, date2010, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, femaleSex, undeterminedSex, date2010, date1970, sizeMeasureType);

        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, null, undeterminedSex, date2011, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, undeterminedSex, undeterminedSex, date2011, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, maleSex, maleSex, date2011, date2011, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, femaleSex, undeterminedSex, date2011, date1970, sizeMeasureType);

        // this test is now working!!! since we manage now perfectly null-null date range
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, undeterminedSex, undeterminedSex, date2009, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, undeterminedSex, undeterminedSex, date2009, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, maleSex, undeterminedSex, date2009, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, femaleSex, undeterminedSex, date2009, date1970, sizeMeasureType);

        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, undeterminedSex, undeterminedSex, date2010, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, maleSex, maleSex, date2010, date2010, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, femaleSex, undeterminedSex, date2010, date1970, sizeMeasureType);

        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, undeterminedSex, undeterminedSex, date2011, date1970, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, maleSex, maleSex, date2011, date2011, sizeMeasureType);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, femaleSex, undeterminedSex, date2011, date1970, sizeMeasureType);

        assertNotFoundLengthWeightParameter(persistenceContext, species, pacificOcean, undeterminedSex, date2010, sizeMeasureType);
        assertNotFoundLengthWeightParameter(persistenceContext, species, pacificOcean, maleSex, date2010, sizeMeasureType);
        assertNotFoundLengthWeightParameter(persistenceContext, species, pacificOcean, femaleSex, date2010, sizeMeasureType);

    }

    protected void assertFoundLengthWeightParameter(ObserveTopiaDaoSupplier supplier, Species species, Ocean ocean, Sex sex, Sex expectedSex, Date date, Date expectedStartDate, SizeMeasureType expectedSizeMeasureType) {

        if (log.isInfoEnabled()) {
            log.info("Try to find length weight parameter for species " + species.getFaoCode() + " - ocean " + ocean.getLabel1() + " - sex " + (sex == null ? "null" : sex.getLabel1()) + " at " + date);
        }
        LengthWeightParameter lengthWeightParameter = LengthWeightParameters.findLengthWeightParameter(Locale.FRANCE, decoratorService, supplier, species, sex, ocean, date, expectedSizeMeasureType);

        Assert.assertNotNull("length weight parameter not found for species " + species.getFaoCode() + " - ocean " + ocean.getLabel1() + " - sex " + (sex == null ? "null" : sex.getLabel1()) + " at " + date, lengthWeightParameter);
        Assert.assertEquals("Expected sex is " + expectedSex.getLabel1() + " but the one found was " + lengthWeightParameter.getSex().getLabel1(), expectedSex, lengthWeightParameter.getSex());
        if (lengthWeightParameter.getStartDate() != null) {
            Date startDate = Dates.getDay(lengthWeightParameter.getStartDate());
            Assert.assertEquals("Expected startDate is " + expectedStartDate + " but the one found was " + startDate, expectedStartDate, startDate);
        }
    }

    protected void assertNotFoundLengthWeightParameter(ObserveTopiaDaoSupplier supplier, Species species, Ocean ocean, Sex sex, Date date, SizeMeasureType expectedSizeMeasureType) {

        if (log.isInfoEnabled()) {
            log.info("Try to find length weight parameter for species " + species.getFaoCode() + " - ocean " + ocean.getLabel1() + " - sex " + (sex == null ? "null" : sex.getLabel1()) + " at " + date);
        }
        try {
            LengthWeightParameters.findLengthWeightParameter(Locale.FRANCE, decoratorService, supplier, species, sex, ocean, date, expectedSizeMeasureType);
            Assert.fail();
        } catch (LengthWeightParameterNotFoundException ignored) {
        }
    }

}
