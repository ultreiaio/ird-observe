package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.mapping.DtoToReferenceDtoMapping;
import fr.ird.observe.spi.mapping.ObserveDtoToReferenceDtoMapping;
import fr.ird.observe.spi.mapping.ObserveEntityToDtoClassMapping;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.ObserveFixtures;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by tchemit on 03/09/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObservePersistenceBusinessProjectTest extends PersistenceTestSupportRead {

    private static final Logger log = LogManager.getLogger(ObservePersistenceBusinessProjectTest.class);
    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    @Test
    public void testGetJavaBeanDefinition() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        for (Class<? extends TopiaEntity> value : ObserveEntityToDtoClassMapping.get().getImplementationClasses()) {
            if (Modifier.isAbstract(value.getModifiers())) {
                continue;
            }
            JavaBean topiaEntity = (JavaBean) value.getDeclaredConstructor().newInstance();
            JavaBeanDefinition javaBeanDefinition = topiaEntity.javaBeanDefinition();
            Assert.assertNotNull(javaBeanDefinition);
        }
    }

    @Test
    public void fromReferentialDto() {
        for (Class<? extends ReferentialDto> dtoType : ObserveBusinessProject.get().getReferentialTypes()) {
            fromReferentialDto(dtoType);
        }
    }

    @Test
    public void fromReferentialReference() {
        DtoToReferenceDtoMapping classMapping = ObserveDtoToReferenceDtoMapping.get();
        for (Class<? extends ReferentialDto> dtoType : ObserveBusinessProject.get().getReferentialTypes()) {
            fromReferentialReference(classMapping, dtoType);
        }
    }

    @Test
    public void fromDataDto() {
        MutableInt referenceCount = new MutableInt();
        for (Class<? extends DataDto> dtoType : ObserveBusinessProject.get().getDataTypes()) {
            fromDataDto(dtoType, referenceCount);
        }
        Assert.assertEquals(ObservePersistenceFixtures.REFERENCE_DATA_COUNT, referenceCount.intValue());
    }

    @Test
    public void fromDataReference() {

        MutableInt referenceCount = new MutableInt();
        DtoToReferenceDtoMapping classMapping = ObserveDtoToReferenceDtoMapping.get();
        for (Class<? extends DataDto> dtoType : ObserveBusinessProject.get().getDataTypes()) {
            fromDataReference(classMapping, dtoType, referenceCount);
        }
        Assert.assertEquals(ObservePersistenceFixtures.REFERENCE_DATA_COUNT, referenceCount.intValue());
    }

    private <D extends ReferentialDto, R extends ReferentialDtoReference> void fromReferentialDto(Class<D> dtoType) {

        ReferentialDtoEntityContext<D, R, ?, ?> typeContext = ObservePersistenceBusinessProject.fromReferentialDto(dtoType);
        Assert.assertNotNull(typeContext);

        Class<R> referenceType = typeContext.toReferenceType();
        Assert.assertNotNull(referenceType);

//        DtoReferenceDefinition referenceDefinition = typeContext.toReferenceDefinition();
//        Assert.assertNotNull(referenceDefinition);

    }

    private <D extends ReferentialDto, R extends ReferentialDtoReference> void fromReferentialReference(DtoToReferenceDtoMapping classMapping, Class<D> dtoType) {

        Class<R> referenceType = classMapping.get(dtoType);

        Assert.assertNotNull(referenceType);

        ReferentialDtoEntityContext<D, R, ?, ?> typeContext = ObservePersistenceBusinessProject.fromReferentialDto(dtoType);
        Assert.assertNotNull(typeContext);

        Class<D> dtoType2 = typeContext.toDtoType();
        Assert.assertNotNull(dtoType2);

//        DtoReferenceDefinition referenceDefinition = typeContext.toReferenceDefinition();
//        Assert.assertNotNull(referenceDefinition);

    }

    private <D extends DataDto, R extends DataDtoReference> void fromDataDto(Class<D> dtoType, MutableInt referenceCount) {

        DataDtoEntityContext<D, R, ?, ?> typeContext = ObservePersistenceBusinessProject.fromDataDto(dtoType);
        Assert.assertNotNull(typeContext);

        try {
            Class<R> referenceType = typeContext.toReferenceType();
            if (referenceType != null) {
                referenceCount.increment();
                Assert.assertNotNull(referenceType);
            }
        } catch (NoSuchElementException e) {
            // no reference here
        }
    }

    private <D extends DataDto, R extends DataDtoReference> void fromDataReference(DtoToReferenceDtoMapping classMapping, Class<D> dtoType, MutableInt referenceCount) {

        Class<R> referenceType = classMapping.get(dtoType);

        if (referenceType == null) {
            return;
        }

        referenceCount.increment();

        DataDtoEntityContext<D, R, ?, ?> typeContext = ObservePersistenceBusinessProject.fromDataDto(dtoType);
        Assert.assertNotNull(typeContext);

        Class<D> dtoType2 = typeContext.toDtoType();
        Assert.assertNotNull(dtoType2);

//        DataDtoReferenceBinder referenceBinder = typeContext.toReferenceBinder();
//        Assert.assertNotNull(referenceBinder);

//        DtoReferenceDefinition referenceDefinition = typeContext.toReferenceDefinition();
//        Assert.assertNotNull(referenceDefinition);

    }

    @Test
    public void testTransformEntityToReferentialDto() {
        for (Class<? extends ReferentialDto> dtoType : ObserveBusinessProject.get().getReferentialTypes()) {
            transformReferentialEntityToDto(dtoType);
        }
    }

    @Test
    public void testTransformEntityToDataDto() {
        for (Class<? extends DataDto> dtoType : ObserveBusinessProject.get().getDataTypes()) {
            transformDataEntityToDto(dtoType);
        }
    }

    private <D extends DataDto, E extends DataEntity> void transformDataEntityToDto(Class<D> dtoType) {

        DataDtoEntityContext<D, ?, E, ?> typeContext = ObservePersistenceBusinessProject.fromDataDto(dtoType);
        Class<E> entityType = typeContext.toEntityType();

        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.getTopiaApplicationContext().newPersistenceContext()) {

            TopiaDao<E> dao = persistenceContext.getDao(entityType);
            List<E> entities = dao.findAll();

            if (entities.isEmpty()) {
                log.debug("No entity found for " + dtoType.getName());
                return;
            }
            log.debug(ObservePersistenceFixtures.logTest("transform from entity to dto ", entities.size(), dtoType.getName()));
            for (E entity : Iterables.limit(entities, ObservePersistenceFixtures.ENTITIES_LIMIT_SIZE)) {

                log.debug("transform " + entity.getTopiaId() + " to " + dtoType.getName());
                D dto = typeContext.toDto(ReferentialLocale.FR, entity);

                Assert.assertNotNull(dto);
                Assert.assertEquals(entity.getTopiaId(), dto.getId());
//            Assert.assertEquals(entity.getTopiaCreateDate(), dto.getCreateDate());
//            Assert.assertEquals(entity.getTopiaVersion(), dto.getTestPropertyAsVersion());

                E entity2 = dao.forTopiaIdEquals(entity.getTopiaId()).findUnique();
                typeContext.toDto(ReferentialLocale.FR, entity2, dto);

                Assert.assertNotNull(entity2);
                Assert.assertEquals(entity.getTopiaId(), entity.getTopiaId());
                Assert.assertEquals(entity.getTopiaCreateDate(), entity.getTopiaCreateDate());
                Assert.assertEquals(entity.getTopiaVersion(), entity.getTopiaVersion());
            }
        }
    }

    private <D extends ReferentialDto, E extends ReferentialEntity> void transformReferentialEntityToDto(Class<D> dtoType) {

        ReferentialDtoEntityContext<D, ?, E, ?> typeContext = ObservePersistenceBusinessProject.fromReferentialDto(dtoType);
        Class<E> entityType = typeContext.toEntityType();

        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.getTopiaApplicationContext().newPersistenceContext()) {

            TopiaDao<E> dao = persistenceContext.getDao(entityType);
            List<E> entities = dao.findAll();

            if (entities.isEmpty()) {
                log.debug("No entity found for " + dtoType.getName());
                return;
            }
            log.debug(ObserveFixtures.logTest("transform from entity to dto ", entities.size(), dtoType.getName()));

            for (E entity : Iterables.limit(entities, ObservePersistenceFixtures.ENTITIES_LIMIT_SIZE)) {

                log.debug("transform " + entity.getTopiaId() + " to " + dtoType.getName());
                D dto = typeContext.toDto(ReferentialLocale.FR, entity);

                Assert.assertNotNull(dto);
                Assert.assertEquals(entity.getTopiaId(), dto.getId());
                Assert.assertEquals(entity.getTopiaCreateDate(), dto.getTopiaCreateDate());
                Assert.assertEquals(entity.getTopiaVersion(), dto.getTopiaVersion());
                Assert.assertEquals(entity.getLastUpdateDate(), dto.getLastUpdateDate());
                Assert.assertEquals(entity.isNeedComment(), dto.isNeedComment());
                Assert.assertEquals(entity.isEnabled(), dto.isEnabled());
                Assert.assertEquals(entity.getUri(), dto.getUri());
                Assert.assertEquals(entity.getCode(), dto.getCode());

                E entity2 = typeContext.toEntity(ReferentialLocale.FR, dto);

                Assert.assertNotNull(entity2);
                Assert.assertEquals(entity.getTopiaId(), entity.getTopiaId());
                Assert.assertEquals(entity.getTopiaCreateDate(), entity.getTopiaCreateDate());
                Assert.assertEquals(entity.getTopiaVersion(), entity.getTopiaVersion());
                Assert.assertEquals(entity.getLastUpdateDate(), entity.getLastUpdateDate());
                Assert.assertEquals(entity.isNeedComment(), entity.isNeedComment());
                Assert.assertEquals(entity.isEnabled(), entity.isEnabled());
                Assert.assertEquals(entity.getUri(), entity.getUri());
                Assert.assertEquals(entity.getCode(), entity.getCode());
            }
        }
    }
}
