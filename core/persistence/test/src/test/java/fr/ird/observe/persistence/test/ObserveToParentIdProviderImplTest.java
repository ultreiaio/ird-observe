package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.ToolkitParentIdDtoBean;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.spi.navigation.parent.ObserveToParentIdProvider;
import fr.ird.observe.spi.relation.DtoEntityRelation;
import fr.ird.observe.spi.relation.WithDtoEntityRelation;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.ObserveFixtures;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * Created on 29/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveToParentIdProviderImplTest extends PersistenceTestSupportRead {
    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    @Test
    public void testGetParent() {

        Map<String, String> fixtures = ObservePersistenceFixtures.loadFixturesMap("persistence/parent");

        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            ObserveToParentIdProvider parentIdProvider = persistenceContext.getToParentIdProvider();
            new TestGetParent(persistenceContext) {

                @Override
                protected void assertType(WithDtoEntityRelation<?, ?, ?, ?> spi, String id) {
                    log.debug(String.format("Test id: %s", id));
                    DtoEntityRelation<?, ?, ?, ?> relation = spi.relation();
                    ToolkitId query = parentIdProvider.getParent(relation.toId(id, null));
                    log.debug(String.format("Result: %s → %s", id, query));
                    Assert.assertNotNull(query);

                    String getParentId = relation.requestName("getParentId");
                    String getParentIdKey = getParentId.replaceAll("::", "_");
                    String expectedGetParentId = fixtures.get(getParentIdKey);
                    String parentId = query.getId();
                    String byParentId = relation.requestName("byParentId");
                    String byParentIdKey = byParentId.replaceAll("::", "_") + "_level";
                    String expectedBytParentId = fixtures.get(byParentIdKey);


                    List<ToolkitParentIdDtoBean> path = parentIdProvider.getPathToRoot(relation.toId(id, null));
                    Assert.assertNotNull(path);

                    long count = path.size();

                    if (ObserveFixtures.WITH_ASSERT) {
                        Assert.assertEquals("Bad value for " + getParentIdKey, expectedGetParentId, parentId);
                        Assert.assertEquals("Bad value for " + byParentIdKey, expectedBytParentId, "" + count);
                    } else {
                        System.out.println(getParentIdKey + "=" + parentId);
                        System.out.println(byParentIdKey + "=" + count);
                    }
                }
            }.run();
        }
    }
}
