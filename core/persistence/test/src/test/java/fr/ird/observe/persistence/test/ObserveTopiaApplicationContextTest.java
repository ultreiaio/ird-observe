package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.LastUpdateDate;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.entities.ObserveTopiaEntitySqlModelResource;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.service.sql.TopiaSqlService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;

/**
 * Created by tchemit on 25/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveTopiaApplicationContextTest extends PersistenceTestSupportRead {
    private static final Logger log = LogManager.getLogger(ObserveTopiaApplicationContextTest.class);
    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    //FIXME Make this nicer and just use the generated script
//    @DatabaseNameConfiguration(DatabaseName.empty_h2)
    @Test
    public void generateSchema() throws IOException {
        ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext();
        TopiaConfiguration configuration = applicationContext.getConfiguration();
        Path temporaryDirectory = configuration.getTemporaryDirectory();
        String modelVersion = applicationContext.getModelVersion();
        {
            Path targetPath = temporaryDirectory.resolve(modelVersion).resolve("observe-empty-h2.sql");
            log.info(String.format("generate empty schema to %s", targetPath));
            SqlScript script = TopiaSqlService.generateH2Schema(targetPath, applicationContext.getSqlService());
            applyEmptyDb(targetPath, configuration, modelVersion, script);
        }
        {
            Path targetPath = temporaryDirectory.resolve(modelVersion).resolve("observe-empty-pg.sql");
            log.info(String.format("generate empty schema to %s", targetPath));
            SqlScript script = TopiaSqlService.generatePgSchema(targetPath, applicationContext.getSqlService());
            applyEmptyDb(targetPath, configuration, modelVersion, script);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    public void testUpdateRequests() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {

            for (Class<? extends Entity> type : ObserveTopiaEntitySqlModelResource.get().getModel().getDescriptorsByType().keySet()) {
                if (LastUpdateDate.class.equals(type)) {
                    return;
                }
                DtoEntityContext<?, ?, ?, ?> spi = ObservePersistenceBusinessProject.fromEntity(type);
                assertUpdateRequests(spi, persistenceContext, null);
            }

            assertUpdateRequests(fr.ird.observe.entities.data.ps.common.Trip.SPI, persistenceContext, 1);
            assertUpdateRequests(fr.ird.observe.entities.data.ps.observation.Route.SPI, persistenceContext, 2);
            assertUpdateRequests(fr.ird.observe.entities.data.ps.observation.Activity.SPI, persistenceContext, 3);
            assertUpdateRequests(fr.ird.observe.entities.data.ps.observation.Set.SPI, persistenceContext, 4);
            assertUpdateRequests(fr.ird.observe.entities.data.ps.observation.FloatingObject.SPI, persistenceContext, 4);
            assertUpdateRequests(fr.ird.observe.entities.data.ll.common.Trip.SPI, persistenceContext, 1);
            assertUpdateRequests(fr.ird.observe.entities.data.ll.observation.Activity.SPI, persistenceContext, 2);
            assertUpdateRequests(fr.ird.observe.entities.data.ll.observation.Set.SPI, persistenceContext, 3);
            assertUpdateRequests(fr.ird.observe.entities.data.ll.observation.Tdr.SPI, persistenceContext, 4);
            assertUpdateRequests(fr.ird.observe.entities.data.ll.logbook.Activity.SPI, persistenceContext, 2);
            assertUpdateRequests(fr.ird.observe.entities.data.ll.logbook.Set.SPI, persistenceContext, 3);
            assertUpdateRequests(fr.ird.observe.entities.data.ll.logbook.Catch.SPI, persistenceContext, 4);
            assertUpdateRequests(fr.ird.observe.entities.data.ll.logbook.Sample.SPI, persistenceContext, 2);
            assertUpdateRequests(fr.ird.observe.entities.referential.ps.common.Program.SPI, persistenceContext, 1);
        }
    }

    private void applyEmptyDb(Path targetPath, TopiaConfiguration configuration, String modelVersion, SqlScript script) {
        File dbFile = targetPath.getParent().resolve(targetPath.toFile().getName() + "-db").toFile();
        log.info(String.format("will apply empty schema from  %s to %s", targetPath, dbFile));
        ObserveDataSourceConfigurationTopiaH2 h2Configuration = localTestMethodResource.getDataSourcesForTestManager().createDataSourceConfigurationH2(
                dbFile,
                Version.valueOf(modelVersion),
                "taiste",
                configuration.getJdbcConnectionUser(),
                configuration.getJdbcConnectionPassword().toCharArray()
        );
        h2Configuration.setTemporaryDirectory(configuration.getTemporaryDirectory());
        try (ObserveTopiaApplicationContext targetTopiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(h2Configuration)) {
            targetTopiaApplicationContext.executeSqlStatements(script);
        }
    }

    private void assertUpdateRequests(DtoEntityContext<?, ?, ?, ?> spi, ObserveTopiaPersistenceContext persistenceContext, Integer expectedSize) {
        Date timeStamp = new Date();
        String id = "id#0#0";
        List<String> fieldRequests = spi.updateLastUpdateDateField(persistenceContext, id, timeStamp);
        Assert.assertNotNull(fieldRequests);
        Assert.assertFalse(fieldRequests.isEmpty());
        persistenceContext.commit();
        List<String> tableRequests = spi.updateLastUpdateDateTable(persistenceContext, timeStamp);
        Assert.assertNotNull(tableRequests);
        Assert.assertFalse(tableRequests.isEmpty());
        if (expectedSize != null) {
            Assert.assertEquals((int) expectedSize, fieldRequests.size());
            Assert.assertEquals((int) expectedSize, tableRequests.size());
        }
        persistenceContext.commit();
//        if (expectedSize!=tableRequests.size()) {
//            System.out.println(spi.toEntityType().getName()+" → "+tableRequests.size());
//            return;
//        }
    }

}
