package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.ToolkitIdMap;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.spi.mapping.ObserveEntityToDtoClassMapping;
import fr.ird.observe.spi.relation.WithDtoEntityRelation;
import fr.ird.observe.test.IgnoreTestClassRule;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created on 28/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveTopiaPersistenceContextTest extends PersistenceTestSupportRead {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    @Test
    public void testMapProjection() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            for (Class<? extends TopiaEntity> value : ObserveEntityToDtoClassMapping.get().getContractClasses()) {
                if (Modifier.isAbstract(value.getModifiers())) {
                    continue;
                }
                @SuppressWarnings("unchecked") Class<? extends Entity> contract = (Class<? extends Entity>) value;
                List<String> allIds = persistenceContext.getDao(contract).findAllIds();
                if (allIds.isEmpty()) {
                    continue;
                }
                if (allIds.size() < 100) {
                    Stream<ToolkitIdMap> queryAll = persistenceContext.executeQueryMapAll(contract);
                    Assert.assertNotNull(queryAll);
                    Assert.assertEquals(value + "", allIds.size(), queryAll.count());
                }

                String id = allIds.get(0);
                ToolkitIdMap queryOne = persistenceContext.executeQueryMapOne(contract, id);
                Assert.assertNotNull(queryOne);
                Assert.assertEquals(id, queryOne.getTopiaId());

                Stream<ToolkitIdMap> querySome = persistenceContext.executeQueryMapSome(contract, Collections.singleton(id));
                Assert.assertNotNull(querySome);
                Assert.assertEquals(1, querySome.count());

            }
        }
    }

    @Test
    public void testMapProjectionFromDao() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            for (Class<? extends TopiaEntity> contract : ObserveEntityToDtoClassMapping.get().getContractClasses()) {
                if (Modifier.isAbstract(contract.getModifiers())) {
                    continue;
                }
                TopiaDao<? extends TopiaEntity> dao = persistenceContext.getDao(contract);
                List<String> allIds = dao.findAllIds();
                if (allIds.isEmpty()) {
                    continue;
                }
                if (allIds.size() < 100) {
                    Stream<ToolkitIdMap> queryAll = dao.executeQueryMapAll();
                    Assert.assertNotNull(queryAll);
                    Assert.assertEquals(contract + "", allIds.size(), queryAll.count());
                }

                String id = allIds.get(0);
                ToolkitIdMap queryOne = dao.executeQueryMapOne(id);
                Assert.assertNotNull(queryOne);
                Assert.assertEquals(id, queryOne.getTopiaId());

                Stream<ToolkitIdMap> querySome = dao.executeQueryMapSome(Collections.singleton(id));
                Assert.assertNotNull(querySome);
                Assert.assertEquals(1, querySome.count());

            }
        }
    }

    @Test
    public void testGetParentId() {

        Map<String, String> fixtures = ObservePersistenceFixtures.loadFixturesMap("persistence/parent");
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            new TestGetParent(persistenceContext) {

                @Override
                protected void assertType(WithDtoEntityRelation<?, ?, ?, ?> spi, String id) {
                    log.debug(String.format("Test id: %s", id));
                    ToolkitId query = spi.getParentId(persistenceContext, id);
                    log.debug(String.format("Result: %s → %s", id, query));
                    Assert.assertNotNull(query);
                    String getParentId = spi.relation().requestName("getParentId");
                    String getParentIdKey = getParentId.replaceAll("::", "_");
                    String expectedGetParentId = fixtures.get(getParentIdKey);
                    String parentId = query.getId();
                    String byParentId = spi.relation().requestName("byParentId");
                    String byParentIdKey = byParentId.replaceAll("::", "_");
                    String expectedBytParentId = fixtures.get(byParentIdKey);
                    long count = persistenceContext.executeQueryByParentId(spi.relation(), Set.of(parentId)).count();

                    if (ObservePersistenceFixtures.WITH_ASSERT) {
                        Assert.assertEquals("Bad value for " + getParentIdKey, expectedGetParentId, parentId);
                        Assert.assertEquals("Bad value for " + byParentIdKey, expectedBytParentId, "" + count);
                    } else {
                        System.out.println(getParentIdKey + "=" + parentId);
                        System.out.println(byParentIdKey + "=" + count);
                    }
                    log.debug(String.format("ByParent for: %s : %d", spi, count));

                }
            }.run();
        }

    }

    @Test
    public void testIdProjection() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            for (Class<? extends TopiaEntity> value : ObserveEntityToDtoClassMapping.get().getContractClasses()) {
                if (Modifier.isAbstract(value.getModifiers())) {
                    continue;
                }
                @SuppressWarnings("unchecked") Class<? extends Entity> contract = (Class<? extends Entity>) value;
                List<String> allIds = persistenceContext.getDao(contract).findAllIds();
                if (allIds.isEmpty()) {
                    continue;
                }
                if (allIds.size() < 100) {
                    Stream<ToolkitId> queryAll = persistenceContext.executeQueryIdAll(contract);
                    Assert.assertNotNull(queryAll);
                    Assert.assertEquals(allIds.size(), queryAll.count());
                }
                String id = allIds.get(0);
                ToolkitId query = persistenceContext.executeQueryIdEquals(contract, id);
                Assert.assertNotNull(query);
                Assert.assertEquals(id, query.getTopiaId());
                Assert.assertEquals(id, query.getId());
                Assert.assertNotNull(query.getLastUpdateDate());

                Stream<ToolkitId> querySome = persistenceContext.executeQueryIdIn(contract, Collections.singleton(id));
                Assert.assertNotNull(querySome);
                Assert.assertEquals(1, querySome.count());

                Stream<ToolkitId> queryBefore = persistenceContext.executeQueryIdBefore(contract, ObservePersistenceFixtures.DATE);
                Assert.assertNotNull(queryBefore);

                Stream<ToolkitId> queryAfter = persistenceContext.executeQueryIdAfter(contract, ObservePersistenceFixtures.DATE);
                Assert.assertNotNull(queryAfter);
            }
        }
    }


    @Test
    public void testIdProjectionFromDao() {
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            for (Class<? extends TopiaEntity> contract : ObserveEntityToDtoClassMapping.get().getContractClasses()) {
                if (Modifier.isAbstract(contract.getModifiers())) {
                    continue;
                }
                TopiaDao<? extends TopiaEntity> dao = persistenceContext.getDao(contract);
                List<String> allIds = dao.findAllIds();
                if (allIds.isEmpty()) {
                    continue;
                }
                if (allIds.size() < 100) {
                    Stream<ToolkitId> queryAll = dao.executeQueryIdAll();
                    Assert.assertNotNull(queryAll);
                    Assert.assertEquals(allIds.size(), queryAll.count());
                }
                String id = allIds.get(0);
                ToolkitId query = dao.executeQueryIdEquals(id);
                Assert.assertNotNull(query);
                Assert.assertEquals(id, query.getTopiaId());
                Assert.assertEquals(id, query.getId());
                Assert.assertNotNull(query.getLastUpdateDate());

                Stream<ToolkitId> querySome = dao.executeQueryIdIn(Collections.singleton(id));
                Assert.assertNotNull(querySome);
                Assert.assertEquals(1, querySome.count());

                Stream<ToolkitId> queryBefore = dao.executeQueryIdBefore(ObservePersistenceFixtures.DATE);
                Assert.assertNotNull(queryBefore);

                Stream<ToolkitId> queryAfter = dao.executeQueryIdAfter(ObservePersistenceFixtures.DATE);
                Assert.assertNotNull(queryAfter);
            }
        }
    }
}
