package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelector;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * To test the model:
 *
 * <ul>
 *     <li>table selector count</li>
 *     <li>table row count</li>
 *     <li>association selector count</li>
 *     <li>association row count</li>
 *     <li>replication order</li>
 *     <li>replication</li>
 *     <li>deletion</li>
 * </ul>
 * Created on 21/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
@RunWith(Parameterized.class)
@DatabaseNameConfiguration(DatabaseName.data)
public class PersistenceModelReadTest extends PersistenceTestSupportRead {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    @Parameterized.Parameter
    public String classifier;
    @Parameterized.Parameter(1)
    public String tripId;

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[]{CLASSIFIER_REFERENTIAL, null},
                             new Object[]{CLASSIFIER_DATA_PS, ObservePersistenceFixtures.getPsCommonTripId()},
                             new Object[]{CLASSIFIER_DATA_LL, ObservePersistenceFixtures.getLlCommonTripId()});
    }

    @Test
    public void countTableSelectors() {
        Map<String, Long> expectedCount = ObservePersistenceFixtures.loadFixturesMapCount("persistence/selector_count/table/" + classifier);
        stream(classifier).forEach(entityType -> assertCountTableSelector(entityType, expectedCount));
    }

    @Test
    public void countAssociationSelectors() {
        Map<String, Long> expectedCount = ObservePersistenceFixtures.loadFixturesMapCount("persistence/selector_count/association/" + classifier);
        stream(classifier).forEach(entityType -> assertCountAssociationSelector(entityType, expectedCount));
    }

    @Test
    public void countTableRows() {
        Map<String, Long> expectedCount = ObservePersistenceFixtures.loadFixturesMapCount("persistence/table_count/" + classifier);
        ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext();
        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {
            stream(classifier).forEach(entityType -> assertCountTableRows(entityType, persistenceContext, expectedCount));
        }
    }

    @Test
    public void countAssociationRows() {
        Map<String, Long> expectedCount = ObservePersistenceFixtures.loadFixturesMapCount("persistence/table_count/" + classifier);
        ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext();
        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {
            stream(classifier).forEach(entityType -> assertAssociationTableRows(entityType, persistenceContext, expectedCount));
        }
    }

    @Test
    public void replicationOrder() {
        List<String> replicatorOrder = getReplicationOrder(classifier);
        PersistenceTestSupportWrite.assertOrder("persistence/replication_order/" + classifier, replicatorOrder);
    }

    private <E extends Entity> void assertCountTableRows(Class<E> entityType, ObserveTopiaPersistenceContext persistenceContext, Map<String, Long> expected) {
        Date now = new Date();
        DtoEntityContext<?, ?, E, ?> spi = ObservePersistenceBusinessProject.fromEntity(entityType);
        TopiaEntitySqlTable table = spi.getEntitySqlDescriptor().getTable();
        String tableName = table.getSchemaAndTableName();

        List<String> requestList = table.generate(table.getTableName() + ".*", tripId == null ? null : TopiaEntitySqlSelectArgument.of(tripId));
        Assert.assertNotNull(requestList);
        long actualTableCount = 0L;
        for (String request : requestList) {
            log.debug(String.format("[%s] → %s", entityType.getName(), request));
            List<E> result = persistenceContext.getSqlSupport().findMultipleResult(new SqlQuery<>() {
                @Override
                public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                    return connection.prepareStatement(request);
                }

                @Override
                public E prepareResult(ResultSet set) throws SQLException {
                    E e = spi.newEntity(now);
                    e.setTopiaId(set.getString(1));
                    return e;
                }
            });
            Assert.assertNotNull(result);
            actualTableCount += result.size();
        }

        Long expectedTableCount = expected.get(tableName);
        if (ObservePersistenceFixtures.WITH_ASSERT) {
            Assert.assertEquals(String.format("[%s] Bad table rows count.", tableName), expectedTableCount, (Long) actualTableCount);
        } else {
            System.out.printf("%s=%d%n", tableName, actualTableCount);
        }
    }

    private <E extends Entity> void assertAssociationTableRows(Class<E> entityType, ObserveTopiaPersistenceContext persistenceContext, Map<String, Long> expected) {
        Date now = new Date();
        DtoEntityContext<?, ?, E, ?> spi = ObservePersistenceBusinessProject.fromEntity(entityType);

        List<TopiaEntitySqlAssociationTable> associations = spi.getEntitySqlDescriptor().getAssociations();

        for (TopiaEntitySqlAssociationTable association : associations) {
            String tableName = association.getSchemaAndTableName();
            List<String> requestList = association.generate(association.getTableName() + ".*", tripId == null ? null : TopiaEntitySqlSelectArgument.of(tripId));
            Assert.assertNotNull(requestList);
            for (String request : requestList) {
                log.debug(String.format("[%s] → %s", tableName, request));
                List<E> result = persistenceContext.getSqlSupport().findMultipleResult(new SqlQuery<>() {
                    @Override
                    public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                        return connection.prepareStatement(request);
                    }

                    @Override
                    public E prepareResult(ResultSet set) throws SQLException {
                        E e = spi.newEntity(now);
                        e.setTopiaId(set.getString(1));
                        return e;
                    }
                });
                Assert.assertNotNull(result);
                long actualCount = result.size();
                Long expectedCount = expected.getOrDefault(tableName, 0L);
                if (ObservePersistenceFixtures.WITH_ASSERT) {
                    Assert.assertEquals(String.format("[%s] Bad association table rows count.", tableName), expectedCount, (Long) actualCount);
                } else if (actualCount > 0) {
                    System.out.printf("%s=%d%n", tableName, actualCount);
                }
            }

        }
    }

    private <E extends Entity> void assertCountTableSelector(Class<E> entityType, Map<String, Long> expected) {
        DtoEntityContext<?, ?, E, ?> spi = ObservePersistenceBusinessProject.fromEntity(entityType);
        TopiaEntitySqlTable table = spi.getEntitySqlDescriptor().getTable();
        String tableName = entityType.getName();
        Long expectedCount = expected.get(tableName);
        List<TopiaEntitySqlSelector> selectors = table.getSelectors();
        Long actualSize = (long) selectors.size();
        if (ObservePersistenceFixtures.WITH_ASSERT) {
            Assert.assertEquals(String.format("[%s] Bad table selector count.", tableName), expectedCount, actualSize);
        } else {
            System.out.printf("%s=%d%n", tableName, actualSize);
        }
    }

    private <E extends Entity> void assertCountAssociationSelector(Class<E> entityType, Map<String, Long> expected) {
        DtoEntityContext<?, ?, E, ?> spi = ObservePersistenceBusinessProject.fromEntity(entityType);
        String tableName = entityType.getName();
        Long expectedCount = expected.get(tableName);

        boolean shouldHaveAssociation = expectedCount != null && expectedCount > 0;
        List<TopiaEntitySqlAssociationTable> associations = spi.getEntitySqlDescriptor().getAssociations();
        if (ObservePersistenceFixtures.WITH_ASSERT && associations.isEmpty()) {
            Assert.assertFalse(String.format("[%s] Not registered association.", entityType.getName()), shouldHaveAssociation);
            return;
        }
        List<TopiaEntitySqlSelector> selectors = associations.stream().flatMap(e -> e.getSelectors().stream()).collect(Collectors.toList());
        Long actualSelectorsCount = (long) selectors.size();
        if (ObservePersistenceFixtures.WITH_ASSERT) {
            Assert.assertEquals(String.format("[%s] Bad association selector count.", entityType.getName()), expectedCount, actualSelectorsCount);
        } else {
            System.out.printf("%s=%d%n", entityType.getName(), actualSelectorsCount);
        }
    }
}
