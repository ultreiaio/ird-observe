package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeManager;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeNode;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeSpecializedRules;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeManagerFactory;
import fr.ird.observe.test.IgnoreTestClassRule;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;

import java.net.URL;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 02/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SimplifiedObjectTypeManagerTest extends PersistenceTestSupportRead {
    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    /**
     * From <a href="https://gitlab.com/ultreiaio/ird-observe/issues/1412">...</a> (specific cases)
     */
    @Test
    public void getSpecializedStandardCode() {

        URL definition = getClass().getResource("/observe-specialized-fad-rules.properties");
        SimplifiedObjectTypeManager simplifiedObjectTypeManager;
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            simplifiedObjectTypeManager = SimplifiedObjectTypeManagerFactory.create(persistenceContext, SimplifiedObjectTypeSpecializedRules.of(definition));
        }

        @SuppressWarnings("unused") SimplifiedObjectTypeNode fob = simplifiedObjectTypeManager.getNodeForStandardCode("FOB");
        SimplifiedObjectTypeNode fad = simplifiedObjectTypeManager.getNodeForStandardCode("FAD");
        SimplifiedObjectTypeNode dfad = simplifiedObjectTypeManager.getNodeForStandardCode("DFAD");
        SimplifiedObjectTypeNode afad = simplifiedObjectTypeManager.getNodeForStandardCode("AFAD");
        SimplifiedObjectTypeNode log = simplifiedObjectTypeManager.getNodeForStandardCode("LOG");
        SimplifiedObjectTypeNode alog = simplifiedObjectTypeManager.getNodeForStandardCode("ALOG");

        //{DFAD, AFAD} (cas théorique qui n'a pas de sens), il devient FAD, ce qui est correct
        assertStandardCode(simplifiedObjectTypeManager, fad, dfad, afad);

        //{FAD, LOG}, il devient FOB. Or il devrait devenir FAD
//        assertStandardCode(fob, fad, log);
        assertStandardCode(simplifiedObjectTypeManager, fad, fad, log);
        assertStandardCode(simplifiedObjectTypeManager, fad, fad, alog);

        //{DFAD, LOG}, il devient FOB. Or il devrait devenir DFAD
//        assertStandardCode(fob, dfad, log);
        assertStandardCode(simplifiedObjectTypeManager, dfad, dfad, log);
        assertStandardCode(simplifiedObjectTypeManager, dfad, dfad, alog);

        //{AFAD, LOG}, il devient FOB. Or il devrait devenir AFAD
//        assertStandardCode(fob, afad, log);
        assertStandardCode(simplifiedObjectTypeManager, afad, afad, log);
        assertStandardCode(simplifiedObjectTypeManager, afad, afad, alog);

        //{DFAD, AFAD, LOG}, il devient FOB. Or il devrait devenir FAD
//        assertStandardCode(fob, dfad, afad, log);
        assertStandardCode(simplifiedObjectTypeManager, fad, dfad, afad, log);
        assertStandardCode(simplifiedObjectTypeManager, fad, dfad, afad, alog);

    }

    @Test
    public void getStandardCodeWithSpecialized() {

        URL definition = getClass().getResource("/observe-specialized-fad-rules.properties");
        SimplifiedObjectTypeManager simplifiedObjectTypeManager;
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            simplifiedObjectTypeManager = SimplifiedObjectTypeManagerFactory.create(persistenceContext, SimplifiedObjectTypeSpecializedRules.of(definition));
        }

        SimplifiedObjectTypeNode fob = simplifiedObjectTypeManager.getNodeForStandardCode("FOB");
        SimplifiedObjectTypeNode fad = simplifiedObjectTypeManager.getNodeForStandardCode("FAD");
        SimplifiedObjectTypeNode dfad = simplifiedObjectTypeManager.getNodeForStandardCode("DFAD");
        SimplifiedObjectTypeNode log = simplifiedObjectTypeManager.getNodeForStandardCode("LOG");
        SimplifiedObjectTypeNode alog = simplifiedObjectTypeManager.getNodeForStandardCode("ALOG");
        SimplifiedObjectTypeNode nlog = simplifiedObjectTypeManager.getNodeForStandardCode("NLOG");
        SimplifiedObjectTypeNode vnlog = simplifiedObjectTypeManager.getNodeForStandardCode("VNLOG");
        SimplifiedObjectTypeNode falog = simplifiedObjectTypeManager.getNodeForStandardCode("FALOG");
        SimplifiedObjectTypeNode halog = simplifiedObjectTypeManager.getNodeForStandardCode("HALOG");

        /* From https://gitlab.com/ultreiaio/ird-observe/issues/1412 (specific cases)
{DFAD, AFAD} (cas théorique qui n'a pas de sens), il devient FAD, ce qui est correct
{FAD, LOG}, il devient FOB. Or il devrait devenir FAD
{DFAD, LOG}, il devient FOB. Or il devrait devenir DFAD
{AFAD, LOG}, il devient FOB. Or il devrait devenir AFAD
{DFAD, AFAD, LOG}, il devient FOB. Or il devrait devenir FAD
         */
        assertStandardCode(simplifiedObjectTypeManager, fob, fob);
        assertStandardCode(simplifiedObjectTypeManager, fad, fob, fad);
        assertStandardCode(simplifiedObjectTypeManager, dfad, fad, dfad, dfad);
        assertStandardCode(simplifiedObjectTypeManager, falog, log, alog, falog);
        // due to specialized rules
//        assertStandardCode(simplifiedObjectTypeManager, fob, dfad, log, nlog);
        assertStandardCode(simplifiedObjectTypeManager, dfad, dfad, log, nlog);
        assertStandardCode(simplifiedObjectTypeManager, falog, alog, falog, falog);
        assertStandardCode(simplifiedObjectTypeManager, alog, alog, falog, halog);
        assertStandardCode(simplifiedObjectTypeManager, log, falog, vnlog);

    }

    @Test
    public void getStandardCode() {

        URL definition = getClass().getResource("/fixtures/persistence/observe-empty-specialized-fad-rules.properties");
        SimplifiedObjectTypeManager simplifiedObjectTypeManager;
        try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
            simplifiedObjectTypeManager = SimplifiedObjectTypeManagerFactory.create(persistenceContext, SimplifiedObjectTypeSpecializedRules.of(definition));
        }

        SimplifiedObjectTypeNode fob = simplifiedObjectTypeManager.getNodeForStandardCode("FOB");
        SimplifiedObjectTypeNode fad = simplifiedObjectTypeManager.getNodeForStandardCode("FAD");
        SimplifiedObjectTypeNode dfad = simplifiedObjectTypeManager.getNodeForStandardCode("DFAD");
        SimplifiedObjectTypeNode log = simplifiedObjectTypeManager.getNodeForStandardCode("LOG");
        SimplifiedObjectTypeNode alog = simplifiedObjectTypeManager.getNodeForStandardCode("ALOG");
        SimplifiedObjectTypeNode nlog = simplifiedObjectTypeManager.getNodeForStandardCode("NLOG");
        SimplifiedObjectTypeNode vnlog = simplifiedObjectTypeManager.getNodeForStandardCode("VNLOG");
        SimplifiedObjectTypeNode falog = simplifiedObjectTypeManager.getNodeForStandardCode("FALOG");
        SimplifiedObjectTypeNode halog = simplifiedObjectTypeManager.getNodeForStandardCode("HALOG");

        /* From https://gitlab.com/ultreiaio/ird-observe/issues/1412 (specific cases)
{DFAD, AFAD} (cas théorique qui n'a pas de sens), il devient FAD, ce qui est correct
{FAD, LOG}, il devient FOB. Or il devrait devenir FAD
{DFAD, LOG}, il devient FOB. Or il devrait devenir DFAD
{AFAD, LOG}, il devient FOB. Or il devrait devenir AFAD
{DFAD, AFAD, LOG}, il devient FOB. Or il devrait devenir FAD
         */
        assertStandardCode(simplifiedObjectTypeManager, fob, fob);
        assertStandardCode(simplifiedObjectTypeManager, fad, fob, fad);
        assertStandardCode(simplifiedObjectTypeManager, dfad, fad, dfad, dfad);
        assertStandardCode(simplifiedObjectTypeManager, falog, log, alog, falog);
        assertStandardCode(simplifiedObjectTypeManager, fob, dfad, log, nlog);
        assertStandardCode(simplifiedObjectTypeManager, falog, alog, falog, falog);
        assertStandardCode(simplifiedObjectTypeManager, alog, alog, falog, halog);
        assertStandardCode(simplifiedObjectTypeManager, log, falog, vnlog);

    }

    private void assertStandardCode(SimplifiedObjectTypeManager simplifiedObjectTypeManager, SimplifiedObjectTypeNode expectedStandardCode, SimplifiedObjectTypeNode... objectMaterialIds) {
        String actualStandardCode = simplifiedObjectTypeManager.getStandardCode(Arrays.stream(objectMaterialIds).map(SimplifiedObjectTypeNode::getId).collect(Collectors.toSet()));
        Assert.assertEquals(expectedStandardCode.getStandardCode(), actualStandardCode);
    }

}
