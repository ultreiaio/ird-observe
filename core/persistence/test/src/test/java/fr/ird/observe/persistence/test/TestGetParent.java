package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.mapping.ObserveEntityToDtoClassMapping;
import fr.ird.observe.spi.relation.WithDtoEntityRelation;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Modifier;
import java.util.List;

/**
 * Created on 29/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class TestGetParent implements Runnable {

    protected final ObserveTopiaPersistenceContext persistenceContext;

    public TestGetParent(ObserveTopiaPersistenceContext persistenceContext) {
        this.persistenceContext = persistenceContext;
    }

    @Override
    public void run() {

        for (Class<? extends TopiaEntity> value : ObserveEntityToDtoClassMapping.get().getContractClasses()) {
            if (Modifier.isAbstract(value.getModifiers())) {
                continue;
            }
            @SuppressWarnings("unchecked") Class<? extends Entity> contract = (Class<? extends Entity>) value;
            DtoEntityContext<?, ?, ?, ?> spi = PersistenceBusinessProject.fromEntity(contract);
            if (spi == null) {
                continue;
            }
            if (!(spi instanceof WithDtoEntityRelation)) {
                continue;
            }
            TopiaDao<? extends Entity> dao = persistenceContext.getDao(contract);
            List<String> allIds = dao.findAllIds();
            if (allIds.isEmpty()) {
                continue;
            }
            String id = allIds.get(0);
            if (id == null) {
                continue;
            }
            assertType((WithDtoEntityRelation<?, ?, ?, ?>) spi, id);
        }
    }

    protected abstract void assertType(WithDtoEntityRelation<?, ?, ?, ?> spi, String id);
}
