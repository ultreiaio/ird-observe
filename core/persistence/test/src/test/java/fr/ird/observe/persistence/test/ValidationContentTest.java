package fr.ird.observe.persistence.test;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.LastUpdateDate;
import fr.ird.observe.entities.ObserveTopiaEntitySqlModelResource;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.test.IgnoreTestClassRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;

/**
 * Created on 22/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ValidationContentTest {
    @ClassRule
    public static final IgnoreTestClassRule IGNORE = new IgnoreTestClassRule("Persistence Model", "persistence.test.skip");
    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    private TopiaEntitySqlModel model;

    @Before
    public void setUp() {
        model = ObserveTopiaEntitySqlModelResource.get().getModel();
    }

    @Test
    public void getDefinitionContent() {
        for (Class<? extends Entity> type : model.getDescriptorsByType().keySet()) {
            if (LastUpdateDate.class.equals(type)) {
                return;
            }
            DtoEntityContext<?, ?, ?, ?> spi = PersistenceBusinessProject.fromEntity(type);
            Assert.assertNotNull(spi);
            String validationCreateContent = spi.getValidationCreateContent();
            Assert.assertNotNull(validationCreateContent);
            String validationUpdateContent = spi.getValidationUpdateContent();
            Assert.assertNotNull(validationUpdateContent);
        }

    }
}
