package fr.ird.observe.persistence.test.request;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.request.CopyRequest;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.TestSupportWithConfig;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import io.ultreia.java4all.util.sql.SqlScript;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.nuiton.topia.persistence.TopiaIdFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * Created on 08/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@RunWith(Parameterized.class)
@DatabaseNameConfiguration(DatabaseName.data)
public class CopyRequestTest extends PersistenceTestSupportRead {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Request", "persistence.request.test.skip");

    private static ObserveTopiaApplicationContext targetTopiaApplicationContext;
    @Parameterized.Parameter
    public String classifier;
    @Parameterized.Parameter(1)
    public String tripId;

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[]{CLASSIFIER_DATA_PS, ObservePersistenceFixtures.getPsCommonTripId()},
                             new Object[]{CLASSIFIER_DATA_LL, ObservePersistenceFixtures.getLlCommonTripId()});
    }

    @BeforeClass
    public static void beforeClass() throws IOException {
        if (IGNORE.isIgnore()) {
            return;
        }
        try {
            TestSupportWithConfig.beforeClass();
        } finally {
            File targetDatabaseDirectory = new File(localTestMethodResource.getTestDirectory(), "import-copyEntryPoint");
            ObserveDataSourceConfigurationTopiaH2 targetTopiaConfiguration = localTestMethodResource.createDataSourceConfiguration(localTestMethodResource.getDbVersion(),
                                                                                                                                   DatabaseName.referential.name(),
                                                                                                                                   targetDatabaseDirectory,
                                                                                                                                   localTestMethodResource.getLogin(),
                                                                                                                                   localTestMethodResource.getPassword());
            targetTopiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(targetTopiaConfiguration);
        }
    }

    @AfterClass
    public static void afterClass() {
        if (IGNORE.isIgnore()) {
            return;
        }
        try {
            TestSupportWithConfig.afterClass();
        } finally {
            if (targetTopiaApplicationContext != null) {
                targetTopiaApplicationContext.close();
            }
        }
    }

    @Test
    public void copy() throws Exception {
        Map<String, Long> expectedCount = ObservePersistenceFixtures.loadFixturesMapCount("persistence/table_count/" + classifier);

        ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext();
        TopiaIdFactory topiaIdFactory = applicationContext.getTopiaIdFactory();
        Class<?> entityType = topiaIdFactory.getClassName(tripId);
        CopyRequest request = new CopyRequest(false, entityType.getName(), tripId);
        SqlScript script = applicationContext.getSqlService().consume(request);
        importScriptAndAssertCount(script, expectedCount);
    }

    void importScriptAndAssertCount(SqlScript script, Map<String, Long> expectedResults) {
        targetTopiaApplicationContext.executeSqlStatements(script);
        StringBuilder asserts = new StringBuilder();
        try (ObserveTopiaPersistenceContext persistenceContext = targetTopiaApplicationContext.newPersistenceContext()) {
            for (Map.Entry<String, Long> entry : expectedResults.entrySet()) {
                String table = entry.getKey();
                Long actualCount = persistenceContext.countTable(table);
                if (ObservePersistenceFixtures.WITH_ASSERT) {
                    Assert.assertEquals("bad size for table: " + table, expectedResults.get(table), actualCount);
                } else {
                    asserts.append(table).append("=").append(actualCount).append("\n");
                }
            }
        }
        if (!ObservePersistenceFixtures.WITH_ASSERT) {
            System.out.println(asserts);
        }
    }

}
