package fr.ird.observe.persistence.test.request;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DeleteRequest;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitParentIdDtoBean;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.persistence.test.PersistenceTestSupportWrite;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.context.OpenableDtoEntityContext;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.test.IgnoreTestClassRule;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptors;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static fr.ird.observe.persistence.test.ObservePersistenceFixtures.getTablesCount;

/**
 * Created on 26/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@RunWith(Parameterized.class)
public class DeleteDataRequestTest extends PersistenceTestSupportRead {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Request", "persistence.request.test.skip");


    private static final Logger log = LogManager.getLogger(DeleteDataRequestTest.class);

    @Parameterized.Parameter
    public Class<? extends DataEntity> entityType;

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> data() {
        return ObservePersistenceBusinessProject.get().getEntityToDtoClassMapping().keySet().stream()
                                                .filter(Class::isInterface)
                                                .filter(DataEntity.class::isAssignableFrom)
                                                .sorted(Comparator.comparing(Object::toString))
                                                .map(t -> new Object[]{t})
                                                .collect(Collectors.toList());
    }

    @Test
    public void test() throws SQLException {

        String variableName = ObservePersistenceFixtures.getEntityVariableName(entityType);
        String id = ObservePersistenceFixtures.getVariable(variableName);
        log.info(String.format("test delete for: %s", id));
        DtoEntityContext<BusinessDto, DtoReference, ? extends DataEntity, ?> spi = ObservePersistenceBusinessProject.fromEntity(entityType);

        String parentId = null;
        if (spi instanceof OpenableDtoEntityContext<?, ?, ?, ?, ?>) {
            OpenableDtoEntityContext<?, ?, ?, ?, ?> openableSpi = (OpenableDtoEntityContext<?, ?, ?, ?, ?>) spi;
            ToolkitParentIdDtoBean parentBean;
            try (ObserveTopiaPersistenceContext persistenceContext = localTestMethodResource.newPersistenceContext()) {
                parentBean = openableSpi.getParentId(persistenceContext, id);
            }
            Assert.assertNotNull("Could not find parent id from: " + id, parentBean);
            parentId = parentBean.getId();
        }
        DeleteRequest request = new DeleteRequest(false, entityType.getName(), parentId, id);

        ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext();

        SqlScript script = applicationContext.getSqlService().consume(request);
        boolean entryPoint = RootOpenableEntity.class.isAssignableFrom(entityType);
        List<String> schemaAndTableNames = null;
        Map<String, Long> expectedCount = null;
        if (entryPoint) {
            BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByDtoType(spi.toDtoType());
            String classifier = "data-" + businessModule.getName();
            expectedCount = ObservePersistenceFixtures.loadFixturesMapCount("persistence/table_count/" + classifier);
            TopiaEntitySqlDescriptors descriptors = PersistenceTestSupportWrite.descriptorForReplication(classifier).reverse();
            schemaAndTableNames = descriptors.getSchemaAndTableNames();
        }

        ObservePersistenceFixtures.assertDataExist(applicationContext, spi, id);
        Map<String, Long> beforeTablesCount = null;
        if (entryPoint) {
            beforeTablesCount = getTablesCount(applicationContext, schemaAndTableNames);
        }
        if (!ObservePersistenceFixtures.WITH_ASSERT) {
            String content = script.content();
            System.out.println(content);
        }
        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {

            persistenceContext.executeSqlScript(script);
            persistenceContext.flush();
            try {
                ObservePersistenceFixtures.assertDataNotExist(persistenceContext, spi, id);
                if (entryPoint) {
                    Map<String, Long> afterTablesCount = getTablesCount(persistenceContext, schemaAndTableNames);
                    assertDeleteEntryPoint(beforeTablesCount, afterTablesCount, expectedCount);
                }
            } finally {
                persistenceContext.rollback();
            }
        }
    }

    private void assertDeleteEntryPoint(Map<String, Long> beforeTablesCount, Map<String, Long> afterTablesCount, Map<String, Long> expectedResults) {
        StringBuilder asserts = new StringBuilder();
        for (Map.Entry<String, Long> entry : expectedResults.entrySet()) {
            String table = entry.getKey();
            Long beforeC = Objects.requireNonNull(beforeTablesCount.get(table), "Can't find key: " + table);
            Long afterC = Objects.requireNonNull(afterTablesCount.get(table), "Can't find key: " + table);
            Long actualCount = beforeC - afterC;
            if (ObservePersistenceFixtures.WITH_ASSERT) {
                Assert.assertEquals("bad size for table: " + table, expectedResults.get(table), actualCount);
            } else {
                asserts.append(table).append("=").append(actualCount).append("\n");
            }
        }
        if (!ObservePersistenceFixtures.WITH_ASSERT) {
            System.out.println(asserts);
        }
    }
}
