package fr.ird.observe.persistence.test.request;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DeletePartialRequest;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.services.service.data.DeleteLayoutRequest;
import fr.ird.observe.test.IgnoreTestClassRule;
import io.ultreia.java4all.util.sql.SqlScript;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Map;
import java.util.Set;

/**
 * Created on 06/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@RunWith(Parameterized.class)
public class DeletePartialRequestTest extends PersistenceTestSupportRead {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Request", "persistence.request.test.skip");


    @Parameterized.Parameter
    public String classifier;
    @Parameterized.Parameter(1)
    public String flavor;
    @Parameterized.Parameter(2)
    public String tripId;
    @Parameterized.Parameter(3)
    public String newId;
    @Parameterized.Parameter(4)
    public Set<Class<? extends DataDto>> scopes;

    @Parameterized.Parameters(name = "{0}-{1}")
    public static Iterable<Object[]> data() {
        return ReplicatePartialRequestTest.PARTIAL_PS_FIXTURES;
    }

    static void assertDelete(ObserveTopiaPersistenceContext persistenceContext, SqlScript deleteScript, Set<String> shell, Map<String, Long> beforeResults, Map<String, Long> afterDelete) {
        if (!ObservePersistenceFixtures.WITH_ASSERT) {
            System.out.println(deleteScript.content());
        }
        for (String table : shell) {
            long actualCount = persistenceContext.countTable(table);
            long beforeCount = beforeResults.get(table);
            long afterDeleteCount = afterDelete.get(table);
            Assert.assertEquals(String.format("bad size for table: %s (excepted %d == %d)", table, actualCount, beforeCount), actualCount, afterDeleteCount);
        }
    }

    @Test
    public void test() {
        ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext();

        DeletePartialRequest deleteRequest = Trip.SPI.toSqlRequest(false, new DeleteLayoutRequest(scopes, newId));
        Set<String> shell = applicationContext.createShell(deleteRequest);


        SqlScript deleteScript = applicationContext.getSqlService().consume(deleteRequest);

        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {

            try {
                Map<String, Long> beforeResults = ReplicatePartialRequestTest.getBeforeResults(persistenceContext, shell);

                persistenceContext.executeSqlScript(deleteScript);
                persistenceContext.flush();

                Map<String, Long> afterDelete = ReplicatePartialRequestTest.getBeforeResults(persistenceContext, shell);

                assertDelete(persistenceContext, deleteScript, shell, beforeResults, afterDelete);
            } finally {
                persistenceContext.rollback();
            }

        }
    }
}
