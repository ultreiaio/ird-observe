package fr.ird.observe.persistence.test.request;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.services.service.referential.ReferentialIds;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import io.ultreia.java4all.lang.Objects2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 12/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@RunWith(Parameterized.class)
@DatabaseNameConfiguration(DatabaseName.data)
public class GetReferentialIdsTest extends PersistenceTestSupportRead {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Request", "persistence.request.test.skip");


    @Parameterized.Parameter
    public String classifier;
    @Parameterized.Parameter(1)
    public Class<? extends TopiaEntity> entityType;
    @Parameterized.Parameter(2)
    public Set<String> tripId;
    private Map<String, Long> expectedResults;

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[]{CLASSIFIER_DATA_PS, fr.ird.observe.entities.data.ps.common.Trip.class, Set.of(ObservePersistenceFixtures.getPsCommonTripId())},
                             new Object[]{CLASSIFIER_DATA_LL, fr.ird.observe.entities.data.ll.common.Trip.class, Set.of(ObservePersistenceFixtures.getLlCommonTripId())},
                             new Object[]{CLASSIFIER_DATA_PS + "-all", fr.ird.observe.entities.data.ps.common.Trip.class, Set.of()},
                             new Object[]{CLASSIFIER_DATA_LL + "-all", fr.ird.observe.entities.data.ll.common.Trip.class, Set.of()}
        );
    }

    @Before
    public void setUp() {
        expectedResults = ObservePersistenceFixtures.loadFixturesMapCount("persistence/referential_ids_count/" + classifier);
    }

    @Test
    public void test() throws Exception {

        ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext();
        ReferentialIds actual = applicationContext.getReferentialIds(entityType, tripId.toArray(new String[0]));

        if (!ObservePersistenceFixtures.WITH_ASSERT) {
            List<String> actualList = new LinkedList<>();
            for (Map.Entry<Class<? extends ReferentialDto>, Set<String>> entry : actual.getIds().entrySet()) {
                Class<? extends ReferentialDto> dtoType = entry.getKey();
                actualList.add(String.format("%s=%d", dtoType.getName(), entry.getValue().size()));
            }
            actualList.sort(String::compareTo);
            System.out.println(String.join("\n", actualList));
            return;
        }
        for (Map.Entry<String, Long> entry : expectedResults.entrySet()) {
            String table = entry.getKey();
            Class<? extends BusinessDto> dtoType = Objects2.forName(table);
            Set<String> actualIds = actual.getIds().getOrDefault(dtoType, Set.of());
            int actualCount = actualIds.size();
            long expectedCount = expectedResults.get(table);
            Assert.assertEquals("bad size for table: " + table, expectedCount, actualCount);
        }
    }

}

