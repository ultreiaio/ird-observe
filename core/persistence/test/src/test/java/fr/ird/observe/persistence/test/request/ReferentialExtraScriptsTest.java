package fr.ird.observe.persistence.test.request;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DeleteRequest;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created on 17/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
@DatabaseNameConfiguration(DatabaseName.referential)
public class ReferentialExtraScriptsTest extends PersistenceTestSupportRead {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Request", "persistence.request.test.skip");

    private static final Logger log = LogManager.getLogger(ReferentialExtraScriptsTest.class);

    public static List<Class<? extends Entity>> data() {
        return ObservePersistenceBusinessProject.get().getEntityToDtoClassMapping().keySet().stream()
                                                .filter(Class::isInterface)
                                                .filter(ReferentialEntity.class::isAssignableFrom)
                                                .sorted(Comparator.comparing(Object::toString))
                                                .collect(Collectors.toList());
    }

    @Test
//    @CopyDatabaseConfiguration
    public void test() {
        for (Class<? extends Entity> entityType : data()) {
            test(entityType);
        }
    }

    public void test(Class<? extends Entity> entityType) {
        String variableName = ObservePersistenceFixtures.getEntityVariableName(entityType).replace("referential.common", "referential.common.common");
        String id = ObservePersistenceFixtures.getVariable(variableName + ".delete");
        if (id == null) {
            id = ObservePersistenceFixtures.getVariable(variableName);
        }
        log.info(String.format("test delete for: %s", id));
        @SuppressWarnings("unchecked") ReferentialDtoEntityContext<?, ?, ReferentialEntity, ?> spi = (ReferentialDtoEntityContext<?, ?, ReferentialEntity, ?>) ObservePersistenceBusinessProject.fromEntity(entityType);

        SqlScript copyScript;
        SqlScript updateScript;
        try (ObserveTopiaPersistenceContext topiaPersistenceContext = localTestMethodResource.newPersistenceContext()) {
            ReferentialEntity entity = spi.loadEntity(Locale.FRENCH, topiaPersistenceContext, id);
            List<String> sqlList = spi.getExtraScripts().generateCopyScript(topiaPersistenceContext, entity, Map.of());
            copyScript = SqlScript.of(String.join("\n", sqlList));

            sqlList = spi.getExtraScripts().generateUpdateScript(topiaPersistenceContext, entity, null, null, null);
            updateScript = SqlScript.of(String.join("\n", sqlList));
        }
        if (!ObservePersistenceFixtures.WITH_ASSERT) {
            System.out.println(copyScript.content());
            System.out.println(updateScript.content());
        }

        DeleteRequest request = new DeleteRequest(false, entityType.getName(), null, id);

        ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext();

        SqlScript deleteScript = applicationContext.getSqlService().consume(request);

        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {

            try {
                ObservePersistenceFixtures.assertDataExist(persistenceContext, spi, id);

                persistenceContext.executeSqlScript(deleteScript);
                persistenceContext.flush();
                ObservePersistenceFixtures.assertDataNotExist(persistenceContext, spi, id);

                persistenceContext.executeSqlScript(copyScript);
                persistenceContext.flush();
                ObservePersistenceFixtures.assertDataExist(persistenceContext, spi, id);
                persistenceContext.executeSqlScript(updateScript);
                persistenceContext.flush();
                ObservePersistenceFixtures.assertDataExist(persistenceContext, spi, id);
            } finally {
                persistenceContext.rollback();
            }

        }
//        ObservePersistenceFixtures.assertDataExist(applicationContext, spi, id);
//
//        applicationContext.executeSqlStatements(deleteScript);
//        ObservePersistenceFixtures.assertDataNotExist(applicationContext, spi, id);
//
//        applicationContext.executeSqlStatements(copyScript);
//        ObservePersistenceFixtures.assertDataExist(applicationContext, spi, id);
//        applicationContext.executeSqlStatements(updateScript);
//        ObservePersistenceFixtures.assertDataExist(applicationContext, spi, id);
    }
}
