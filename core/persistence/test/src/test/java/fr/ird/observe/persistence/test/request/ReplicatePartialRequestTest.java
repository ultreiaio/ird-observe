package fr.ird.observe.persistence.test.request;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.ReplicatePartialRequest;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.common.TripGearUseFeaturesDto;
import fr.ird.observe.dto.data.ps.common.TripLocalmarketDto;
import fr.ird.observe.dto.data.ps.common.TripLogbookDto;
import fr.ird.observe.dto.data.ps.landing.TripLandingDto;
import fr.ird.observe.dto.data.ps.observation.RouteDto;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.services.service.data.MoveLayoutRequest;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import io.ultreia.java4all.util.sql.SqlScript;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 06/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@RunWith(Parameterized.class)
public class ReplicatePartialRequestTest extends PersistenceTestSupportRead {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Request", "persistence.request.test.skip");


    private static final String PS_COMMON_TRIP_ID = ObservePersistenceFixtures.getPsCommonTripId();
    private static final String PS_COMMON_TRIP_MOVE = ObserveFixtures.getId("PS_COMMON_TRIP_MOVE");
    public static final Iterable<Object[]> PARTIAL_PS_FIXTURES = Arrays.asList(
            new Object[]{CLASSIFIER_DATA_PS, "GearUseFeatures", PS_COMMON_TRIP_ID, PS_COMMON_TRIP_MOVE, Set.of(TripGearUseFeaturesDto.class)},
            new Object[]{CLASSIFIER_DATA_PS, "Observations", PS_COMMON_TRIP_ID, PS_COMMON_TRIP_MOVE, Set.of(RouteDto.class)},
            new Object[]{CLASSIFIER_DATA_PS, "Logbook", PS_COMMON_TRIP_ID, PS_COMMON_TRIP_MOVE, Set.of(TripLogbookDto.class)},
            new Object[]{CLASSIFIER_DATA_PS, "Localmarket", PS_COMMON_TRIP_ID, PS_COMMON_TRIP_MOVE, Set.of(TripLocalmarketDto.class)},
            new Object[]{CLASSIFIER_DATA_PS, "Landing", PS_COMMON_TRIP_ID, PS_COMMON_TRIP_MOVE, Set.of(TripLandingDto.class)},
            new Object[]{CLASSIFIER_DATA_PS, "Observations-Logbook", PS_COMMON_TRIP_ID, PS_COMMON_TRIP_MOVE, Set.of(TripLogbookDto.class, RouteDto.class)},
            new Object[]{CLASSIFIER_DATA_PS, "Observations-Logbook-Localmarket", PS_COMMON_TRIP_ID, PS_COMMON_TRIP_MOVE, Set.of(TripLocalmarketDto.class, TripLogbookDto.class, RouteDto.class)},
            new Object[]{CLASSIFIER_DATA_PS, "Landing-Logbook-Localmarket", PS_COMMON_TRIP_ID, PS_COMMON_TRIP_MOVE, Set.of(TripLocalmarketDto.class, TripLogbookDto.class, TripLandingDto.class)}
    );

    @Parameterized.Parameter
    public String classifier;
    @Parameterized.Parameter(1)
    public String flavor;
    @Parameterized.Parameter(2)
    public String tripId;
    @Parameterized.Parameter(3)
    public String newId;
    @Parameterized.Parameter(4)
    public Set<Class<? extends DataDto>> scopes;

    @Parameterized.Parameters(name = "{0}-{1}")
    public static Iterable<Object[]> data() {
        return PARTIAL_PS_FIXTURES;
    }

    static Map<String, Long> getBeforeResults(ObserveTopiaPersistenceContext persistenceContext, Set<String> shell) {
        Map<String, Long> beforeResults = new TreeMap<>();
        for (String table : shell) {
            Long actualCount = persistenceContext.countTable(table);
            beforeResults.put(table, actualCount);
        }
        return beforeResults;
    }

    static void assertReplicate(ObserveTopiaApplicationContext applicationContext, ObserveTopiaPersistenceContext persistenceContext, ReplicatePartialRequest replicateRequest, Set<String> shell, Map<String, Long> expected, Map<String, Long> before, int factor) {
        SqlScript replicateScript = applicationContext.getSqlService().consume(replicateRequest);
        persistenceContext.executeSqlScript(replicateScript);
        persistenceContext.flush();
        if (!ObservePersistenceFixtures.WITH_ASSERT) {
            Map<String, Long> actualResults = new TreeMap<>();
            for (String table : shell) {
                long actualCount = factor * persistenceContext.countTable(table) - before.get(table);
                actualResults.put(table, actualCount);
            }
            System.out.println(replicateScript.content());
            StringBuilder asserts = new StringBuilder();
            actualResults.forEach((k, v) -> asserts.append(k).append("=").append(v).append("\n"));
            System.out.println(asserts);
            return;
        }
        for (String table : shell) {
            long actualCount = persistenceContext.countTable(table) - before.get(table);
            long expectedCount = factor * expected.get(table);
            Assert.assertEquals("bad size for table: " + table, expectedCount, actualCount);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    public void test() {
        Map<String, Long> expected = ObservePersistenceFixtures.loadFixturesMapCount("persistence/replicate_partial_count/" + classifier + "-" + flavor);
        ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext();

        ReplicatePartialRequest replicateRequest = Trip.SPI.toSqlRequest(false, new MoveLayoutRequest(scopes, tripId, newId, false));
        Set<String> shell = applicationContext.createShell(replicateRequest);

        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {

            Map<String, Long> beforeResults = getBeforeResults(persistenceContext, shell);
            try {
                assertReplicate(applicationContext, persistenceContext, replicateRequest, shell, expected, beforeResults, 1);
                if (!ObservePersistenceFixtures.WITH_ASSERT) {
                    return;
                }
                assertReplicate(applicationContext, persistenceContext, replicateRequest, shell, expected, beforeResults, 2);
                assertReplicate(applicationContext, persistenceContext, replicateRequest, shell, expected, beforeResults, 3);
                assertReplicate(applicationContext, persistenceContext, replicateRequest, shell, expected, beforeResults, 4);
            } finally {
                persistenceContext.rollback();
            }
        }
    }
}
