package fr.ird.observe.persistence.test.request;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DeleteRequest;
import fr.ird.observe.datasource.request.ReplicateRequest;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.entities.data.ps.logbook.SampleActivity;
import fr.ird.observe.entities.data.ps.logbook.WellActivity;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.relation.WithDtoEntityRelation;
import fr.ird.observe.test.IgnoreTestClassRule;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptConsumer;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.nuiton.topia.persistence.TopiaDao;

import java.util.Comparator;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created on 08/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@RunWith(Parameterized.class)
public class ReplicateRequestTest extends PersistenceTestSupportRead {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Request", "persistence.request.test.skip");

    /**
     * Those types won't be replicated since we need some stuff from outside his shell.
     */
    public static final Set<Class<?>> NO_REPLICATE_TYPES = Set.of(SampleActivity.class, WellActivity.class);
    @Parameterized.Parameter
    public Class<? extends DataEntity> entityType;

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> data() {
        return ObservePersistenceBusinessProject.get().getEntityToDtoClassMapping().keySet().stream()
                                                .filter(Class::isInterface)
                                                .filter(Predicate.not(RootOpenableEntity.class::isAssignableFrom).and(DataEntity.class::isAssignableFrom).and(Predicate.not(NO_REPLICATE_TYPES::contains)))
                                                .sorted(Comparator.comparing(Object::toString)).map(t -> new Object[]{t}).collect(Collectors.toList());
    }


    @Test
    public void replicate() {
        String variableName = ObservePersistenceFixtures.getEntityVariableName(entityType);
        String id = ObservePersistenceFixtures.getVariable(variableName);
        log.info(String.format("test replicate for: %s", id));
        DtoEntityContext<BusinessDto, DtoReference, ? extends DataEntity, ?> spi = ObservePersistenceBusinessProject.fromEntity(entityType);

        ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext();
        ObservePersistenceFixtures.assertDataExist(applicationContext, spi, id);
        String parentId;
        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {
            parentId = ((WithDtoEntityRelation<?, ?, ?, ?>) spi).getParentId(persistenceContext, id).getId();
        }
        log.info(String.format("use parent id: %s", parentId));

        ReplicateRequest replicateRequest = new ReplicateRequest(false, parentId, parentId, entityType.getName(), id);
        DeleteRequest deleteRequest = new DeleteRequest(false, entityType.getName(), parentId, id);
        SqlScript deleteScript = applicationContext.getSqlService().consume(deleteRequest);
        SqlScript replicateScript = applicationContext.getSqlService().consume(replicateRequest);
        String replicateScriptContent = replicateScript.content();
        String deleteScriptContent = deleteScript.content();
        if (!ObservePersistenceFixtures.WITH_ASSERT) {
            System.out.println(deleteScriptContent);
            System.out.println(replicateScriptContent);
        }

        try (ObserveTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {
            try {
                ObservePersistenceFixtures.assertDataExist(persistenceContext, spi, id);
                TopiaDao<? extends DataEntity> dao = spi.getDao(persistenceContext);
                long beforeCount = dao.count();
                persistenceContext.executeSqlScript(deleteScript);
                persistenceContext.flush();
                ObservePersistenceFixtures.assertDataNotExist(persistenceContext, spi, id);
                persistenceContext.executeSqlScript(SqlScriptConsumer.of(replicateScript));
                persistenceContext.flush();
                long afterCount = dao.count();

                Assert.assertEquals(String.format("On type %s, should get count: %d, but get %d", entityType.getName(), beforeCount, afterCount), beforeCount, afterCount);
                ObservePersistenceFixtures.assertDataNotExist(persistenceContext, spi, id);
            } finally {
                persistenceContext.rollback();
            }

        }
    }

}
