package fr.ird.observe.persistence.test.request;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.persistence.test.ObservePersistenceFixtures;
import fr.ird.observe.persistence.test.PersistenceTestSupportRead;
import fr.ird.observe.persistence.test.PersistenceTestSupportWrite;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import io.ultreia.java4all.util.sql.SqlScript;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.topia.service.sql.internal.SqlRequestSet;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlan;

import java.io.File;
import java.util.Map;

/**
 * To test the model:
 *
 * <ul>
 *     <li>table selector count</li>
 *     <li>table row count</li>
 *     <li>association selector count</li>
 *     <li>association row count</li>
 *     <li>replication order</li>
 *     <li>replication</li>
 *     <li>deletion</li>
 * </ul>
 * Created on 21/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
@DatabaseNameConfiguration(DatabaseName.data)
public class StandaloneCopyPlanTest extends PersistenceTestSupportRead {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Request", "persistence.request.test.skip");


    @Test
    public void copy() throws Exception {
        Map<String, Long> expectedCount = ObservePersistenceFixtures.loadFixturesMapCount("persistence/table_count/" + CLASSIFIER_REFERENTIAL);

        File scriptFile = generateScriptFile();
        SqlScript script;
        try (ObserveTopiaApplicationContext applicationContext = localTestMethodResource.getTopiaApplicationContext()) {
            TopiaEntitySqlCopyPlan copyPlan = PersistenceTestSupportWrite.copyPlan(CLASSIFIER_REFERENTIAL);
            log.info(String.format("Generated script: %s", scriptFile));
            SqlRequestSet request = SqlRequestSet.builder(scriptFile.toPath())
                    .forH2()
                    .addCopyTableRequest(copyPlan, null)
                    .setBlobModel(applicationContext.getBlobModel())
                    .build();
            script = applicationContext.getSqlService().consume(request);
        }

        importScriptAndAssertCount(scriptFile, script, expectedCount);
    }

    protected File generateScriptFile() {
        return new File(localTestMethodResource.getTestDirectory(), String.format("script%s.sql", "-" + PersistenceTestSupportRead.CLASSIFIER_REFERENTIAL));
    }

    void importScriptAndAssertCount(File scriptFile, SqlScript script, Map<String, Long> expectedResults) throws Exception {

        File targetDatabaseDirectory = new File(localTestMethodResource.getTestDirectory(), "import-" + scriptFile.getName());
        ObserveDataSourceConfigurationTopiaH2 targetTopiaConfiguration = localTestMethodResource.createDataSourceConfiguration(localTestMethodResource.getDbVersion(), DatabaseName.empty.name(), targetDatabaseDirectory, localTestMethodResource.getLogin(), localTestMethodResource.getPassword());
        try (ObserveTopiaApplicationContext targetTopiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(targetTopiaConfiguration)) {

            targetTopiaApplicationContext.executeSqlStatements(script);

            StringBuilder asserts = new StringBuilder();
            try (ObserveTopiaPersistenceContext persistenceContext = targetTopiaApplicationContext.newPersistenceContext()) {
                for (Map.Entry<String, Long> entry : expectedResults.entrySet()) {
                    String table = entry.getKey();
                    Long actualCount = persistenceContext.countTable(table);
                    if (ObservePersistenceFixtures.WITH_ASSERT) {
                        Assert.assertEquals("bad size for table: " + table, expectedResults.get(table), actualCount);
                    } else {
                        asserts.append(table).append("=").append(actualCount).append("\n");
                    }
                }
            }
            if (!ObservePersistenceFixtures.WITH_ASSERT) {
                System.out.println(asserts);
            }
        }
    }

}
