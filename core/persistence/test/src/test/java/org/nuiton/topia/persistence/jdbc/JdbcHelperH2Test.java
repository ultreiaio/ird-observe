package org.nuiton.topia.persistence.jdbc;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.persistence.test.PersistenceTestSupportWrite;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.topia.service.migration.version.MigrationServiceSqlHelper;

import java.sql.SQLException;

/**
 * Created on 25/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
@DatabaseNameConfiguration(DatabaseName.empty)
public class JdbcHelperH2Test extends PersistenceTestSupportWrite {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    private JdbcHelperH2 jdbcHelper;

    @Before
    public void setUp() {
        jdbcHelper = localTestMethodResource.getTopiaApplicationContext().newJdbcHelperH2();
    }

    @Test
    public void isTableExist() throws SQLException {
        Assert.assertTrue(jdbcHelper.isTableExist(MigrationServiceSqlHelper.CURRENT.schemaName(), MigrationServiceSqlHelper.CURRENT.tableName()));
        Assert.assertTrue(jdbcHelper.isTableExist(MigrationServiceSqlHelper.CURRENT.schemaName().toUpperCase(), MigrationServiceSqlHelper.CURRENT.tableName()));
        Assert.assertTrue(jdbcHelper.isTableExist(MigrationServiceSqlHelper.CURRENT.schemaName().toUpperCase(), MigrationServiceSqlHelper.CURRENT.tableName().toUpperCase()));

        Assert.assertFalse(jdbcHelper.isTableExist(MigrationServiceSqlHelper.CURRENT.schemaName(), MigrationServiceSqlHelper.CURRENT.tableName()+"-Fake"));
    }

    @Test
    public void isSchemaExist() throws SQLException {
        Assert.assertTrue(jdbcHelper.isSchemaExist("Public"));
        Assert.assertFalse(jdbcHelper.isSchemaExist("PUBLIC-fake"));
    }

}
