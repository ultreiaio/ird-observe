package org.nuiton.topia.persistence.jdbc;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.ObserveTestConfiguration;
import io.ultreia.java4all.util.sql.conf.JdbcConfiguration;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Created on 25/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class JdbcHelperPostgresTest {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    private JdbcHelperPostgres jdbcHelper;

    @Before
    public void setUp() {
        ObserveTestConfiguration.injectCredentials();
        JdbcConfiguration jdbcConfiguration = ObserveTestConfiguration.pgConfiguration();
        jdbcHelper = new JdbcHelperPostgres(jdbcConfiguration);

        try {
            jdbcHelper.runSelect("SELECT 1;", a -> true);
        } catch (SQLException e) {
            Assume.assumeFalse(String.format("Skip %s tests... (postgres database does not exist)", JdbcHelperPostgresTest.class.getName()), true);
        }
    }

    @Test
    public void isTableExist() throws SQLException {
        boolean actual = jdbcHelper.isTableExist("Common", "database_version");
        Assert.assertTrue(actual);

        actual = jdbcHelper.isTableExist("PUBLIC", "database_version-fake");
        Assert.assertFalse(actual);
    }
    @Test
    public void isSchemaExist() throws SQLException {
        boolean actual = jdbcHelper.isSchemaExist("Public");
        Assert.assertTrue(actual);

        actual = jdbcHelper.isSchemaExist("PUBLIC-fake");
        Assert.assertFalse(actual);
    }
}
