package org.nuiton.topia.persistence.jdbc;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.persistence.test.PersistenceTestSupportWrite;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.topia.service.migration.version.MigrationServiceSqlHelper;

import java.util.List;
import java.util.Set;

/**
 * Created on 05/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
@DatabaseNameConfiguration(DatabaseName.empty)
public class JdbcSecurityHelperH2Test extends PersistenceTestSupportWrite {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    private JdbcSecurityHelper securityHelper;

    @Before
    public void setUp() {
        securityHelper = localTestMethodResource.getTopiaApplicationContext().newJdbcHelperH2().newSecurityHelper();
    }

    @Test
    public void getTables() {
        List<Pair<String, String>> actual;

        actual = securityHelper.getTables(Set.of("PUbLIC-fake"), Set.of());
        Assert.assertNotNull(actual);
        Assert.assertEquals(0, actual.size());

        actual = securityHelper.getTables(Set.of(MigrationServiceSqlHelper.CURRENT.schemaName()), Set.of());
        Assert.assertNotNull(actual);
        Assert.assertTrue(actual.contains(Pair.of(MigrationServiceSqlHelper.CURRENT.schemaName().toUpperCase(), MigrationServiceSqlHelper.CURRENT.tableName().toUpperCase())));
    }

    @Test
    public void getRoles() {
        Set<String> actual = securityHelper.getRoles();
        Assert.assertNotNull(actual);
        Assert.assertEquals(1, actual.size());
        Assert.assertEquals(Set.of("SA"), actual);
    }

    @Test
    public void isOwner() {
        boolean actual = securityHelper.isOwner();
        Assert.assertTrue(actual);
    }

    @Test
    public void isSuperUser() {
        boolean actual = securityHelper.isSuperUser();
        Assert.assertTrue(actual);
    }

    @Test
    public void getTablePrivileges() {
        Set<String> actual = securityHelper.getTablePrivileges(MigrationServiceSqlHelper.CURRENT.schemaName(), MigrationServiceSqlHelper.CURRENT.tableName());
        Assert.assertNotNull(actual);
        Assert.assertEquals(0, actual.size());
    }
}
