package org.nuiton.topia.persistence.security;

/*-
 * #%L
 * ObServe Core :: Persistence :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.model.DataSourceUserDto;
import fr.ird.observe.datasource.security.model.DataSourceUserRole;
import fr.ird.observe.persistence.test.PersistenceTestSupportWrite;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.IgnoreTestClassRule;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptConsumer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.topia.persistence.jdbc.JdbcSecurityHelper;
import org.nuiton.topia.service.migration.version.MigrationServiceSqlHelper;

import java.sql.SQLException;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 08/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class SecurityScriptHelperTest extends PersistenceTestSupportWrite {

    @ClassRule
    public static final IgnoreTestClassRule IGNORE2 = new IgnoreTestClassRule("Persistence Model", "persistence.model.test.skip");

    private JdbcHelper helper;
    private JdbcSecurityHelper securityHelper;

    @Before
    public void setUp() {
        helper = localTestMethodResource.getTopiaApplicationContext().newJdbcHelper();
        securityHelper = helper.newSecurityHelper();
    }

    @Test
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(DatabaseName.empty)
    public void applySecurity() throws SQLException {
        Set<DataSourceUserDto> users = securityHelper.getUsers();
        Assert.assertNotNull(users);
        Assert.assertEquals(1, users.size());

        SqlScript sqlScript = SqlScript.of("" +
                                                   "CREATE USER IF NOT EXISTS referential PASSWORD 'referential';\n" +
                                                   "CREATE USER IF NOT EXISTS user PASSWORD 'user';\n" +
                                                   "CREATE USER IF NOT EXISTS dataEntryOperator PASSWORD 'dataEntryOperator';\n" +
                                                   "CREATE USER IF NOT EXISTS technical PASSWORD 'technical';\n");
        helper.consume(SqlScriptConsumer.builder(sqlScript).build());
        users = securityHelper.getUsers();
        Assert.assertNotNull(users);
        Assert.assertEquals(5, users.size());
        Map<String, DataSourceUserDto> userMap = users.stream().collect(Collectors.toMap(DataSourceUserDto::getName, Function.identity()));

        assertUser(userMap, "sa", DataSourceUserRole.ADMINISTRATOR, DataSourceUserRole.ADMINISTRATOR);
        assertUser(userMap, "user", DataSourceUserRole.UNUSED, DataSourceUserRole.USER);
        assertUser(userMap, "dataEntryOperator", DataSourceUserRole.UNUSED, DataSourceUserRole.DATA_ENTRY_OPERATOR);
        assertUser(userMap, "referential", DataSourceUserRole.UNUSED, DataSourceUserRole.REFERENTIAL);
        assertUser(userMap, "technical", DataSourceUserRole.UNUSED, DataSourceUserRole.TECHNICAL);

        SecurityScriptHelper securityScriptHelper = localTestMethodResource.getTopiaApplicationContext().newSecurityScriptHelper();
        securityScriptHelper.applySecurity(users);

        Set<String> actual = securityHelper.getTablePrivileges(MigrationServiceSqlHelper.CURRENT.schemaName().toUpperCase(), MigrationServiceSqlHelper.CURRENT.tableName().toUpperCase());
        Assert.assertNotNull(actual);
        Assert.assertEquals(4, actual.size());
        Assert.assertEquals(Set.of("DELETE", "INSERT", "UPDATE", "SELECT"), actual);
    }

    private void assertUser(Map<String, DataSourceUserDto> userMap, String name, DataSourceUserRole expectedRole, DataSourceUserRole newRole) {
        name = name.toUpperCase();
        DataSourceUserDto userUser = userMap.get(name);
        Assert.assertNotNull(userUser);
        Assert.assertEquals(name, userUser.getName());
        Assert.assertEquals(expectedRole, userUser.getRole());
        userUser.setRole(newRole);
    }
}
