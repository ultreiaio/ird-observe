package fr.ird.observe.services.client;

/*-
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.ObserveServiceInitializer;
import io.ultreia.java4all.util.sql.SqlScript;

public class AnonymousServiceClient extends GeneratedAnonymousServiceClient {

    @Override
    public ObserveDataSourceConnection createEmpty(ObserveDataSourceConfiguration config) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        ObserveServiceInitializer initializer = start(config);
        ObserveDataSourceConnection dataSourceConnection = super.createEmpty(config);
        return end(initializer, dataSourceConnection);
    }

    @Override
    public ObserveDataSourceConnection createFromDump(ObserveDataSourceConfiguration config, SqlScript dump) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        ObserveServiceInitializer initializer = start(config);
        ObserveDataSourceConnection dataSourceConnection = super.createFromDump(config, dump);
        return end(initializer, dataSourceConnection);
    }

    @Override
    public ObserveDataSourceConnection createFromImport(ObserveDataSourceConfiguration config, SqlScript dump, SqlScript optionalDump) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        ObserveServiceInitializer initializer = start(config);
        ObserveDataSourceConnection dataSourceConnection = super.createFromImport(config, dump, optionalDump);
        return end(initializer, dataSourceConnection);
    }

    @Override
    public ObserveDataSourceConnection open(ObserveDataSourceConfiguration config) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        ObserveServiceInitializer initializer = start(config);
        ObserveDataSourceConnection dataSourceConnection = super.open(config);
        return end(initializer, dataSourceConnection);
    }

    private ObserveServiceInitializer start(ObserveDataSourceConfiguration config) {
        ObserveServiceInitializer initializer = getServiceContext().getInitializer();
        initializer.setConfiguration(config);
        return initializer;
    }

    private ObserveDataSourceConnection end(ObserveServiceInitializer initializer, ObserveDataSourceConnection dataSourceConnection) {
        initializer.setConnection(dataSourceConnection);
        initializer.setConfiguration(null);
        return dataSourceConnection;
    }

}
