package fr.ird.observe.services.client;

/*-
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRestConstants;
import fr.ird.observe.services.ObserveServiceInitializerConfig;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HRequestBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Created by tchemit on 17/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveRequestBuilder extends HRequestBuilder {
    private static final Logger log = LogManager.getLogger(ObserveRequestBuilder.class);

    ObserveRequestBuilder(ObserveServiceClientContext serviceContext, String baseUrl) {
        super(serviceContext.getServiceUrl(baseUrl));
        ObserveServiceInitializerConfig initializerConfig = serviceContext.getInitializer().getInitializerConfig();
        if (serviceContext.getInitializer().withConnection()) {
            addAuthenticationTokenSupplier(serviceContext.getAuthTokenSupplier());
        }
        if (initializerConfig.withApplicationLocale()) {
            addHeader(ObserveDataSourceConfigurationRestConstants.REQUEST_APPLICATION_LOCALE, initializerConfig.getApplicationLocale().toString());
        }
        if (initializerConfig.withReferentialLocale()) {
            addHeader(ObserveDataSourceConfigurationRestConstants.REQUEST_REFERENTIAL_LOCALE, initializerConfig.getReferentialLocale().getLocale().toString());
        }
    }

    @Override
    protected void setAuthenticationTokenInRequest() {
        String authenticationToken = authenticationTokenSupplier.get();
        Objects.requireNonNull(authenticationToken, "Can't add null authenticationToken");
        addHeader(ObserveDataSourceConfigurationRestConstants.REQUEST_AUTHENTICATION_TOKEN, authenticationToken);
    }

    @Override
    protected HRequest build() {
        HRequest request = super.build();
        log.debug("Created request: {}", request);
        return request;
    }
}
