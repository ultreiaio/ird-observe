package fr.ird.observe.services.client;

/*-
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.ObserveServiceInitializer;
import io.ultreia.java4all.http.HResponseBuilder;

import java.util.function.Supplier;

/**
 * Created by tchemit on 16/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveServiceClientContext {

    private final ObserveServiceInitializer initializer;
    private final HResponseBuilder responseBuilder;
    private final ObserveRequestBuilderFactory requestBuilderFactory;
    private final String locateService;

    ObserveServiceClientContext(ObserveServiceInitializer initializer, HResponseBuilder responseBuilder, ObserveRequestBuilderFactory requestBuilderFactory, String locateService) {
        this.initializer = initializer;
        this.responseBuilder = responseBuilder;
        this.requestBuilderFactory = requestBuilderFactory;
        if (!locateService.endsWith("/")) {
            locateService += "/";
        }
        this.locateService = locateService;
    }

    ObserveRequestBuilderFactory getRequestBuilderFactory() {
        return requestBuilderFactory;
    }

    HResponseBuilder getResponseBuilder() {
        return responseBuilder;
    }

    public ObserveServiceInitializer getInitializer() {
        return initializer;
    }

    String getServiceUrl(String suffix) {
        return getServiceUrl() + suffix;
    }

    private String getServiceUrl() {
        String serviceUrl;
        if (initializer.withConnection()) {
            ObserveDataSourceConnection dataSourceConnection = initializer.getConnection();
            serviceUrl = dataSourceConnection.getUrl() + locateService;
        } else if (initializer.withConfiguration()) {
            ObserveDataSourceConfigurationRest dataSourceConfiguration = (ObserveDataSourceConfigurationRest) initializer.getConfiguration();
            serviceUrl = dataSourceConfiguration.getUrl() + locateService;
        } else {
            throw new IllegalStateException("No data source configuration, nor connection defined");
        }
        return serviceUrl;
    }

    Supplier<String> getAuthTokenSupplier() {
        return () -> initializer.optionalConnection().orElseThrow(IllegalStateException::new).getAuthenticationToken();
    }
}
