package fr.ird.observe.services.client;

/*-
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.services.service.ObserveService;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HResponse;
import io.ultreia.java4all.http.HRestClientService;

/**
 * Created by tchemit on 15/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveServiceClientSupport implements HRestClientService, ObserveService {

    private ObserveServiceClientContext serviceContext;

    @Override
    public Gson gson() {
        return serviceContext.getResponseBuilder().getGson().get();
    }

    @Override
    public ObserveRequestBuilderFactory getRequestBuilderFactory() {
        return serviceContext.getRequestBuilderFactory();
    }

    @Override
    public ObserveRequestBuilder create(String baseUrl) {
        return getRequestBuilderFactory().create(serviceContext, baseUrl);
    }

    @Override
    public HResponse executeRequest(HRequest request, int expectedStatusCode) {
        return serviceContext.getResponseBuilder().executeRequest(request, expectedStatusCode);
    }

    @Override
    public HResponse executeRequest(HRequest request) {
        return serviceContext.getResponseBuilder().executeRequest(request);
    }

    ObserveServiceClientContext getServiceContext() {
        return serviceContext;
    }

    void setServiceContext(ObserveServiceClientContext serviceContext) {
        this.serviceContext = serviceContext;
    }

}
