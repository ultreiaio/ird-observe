package fr.ird.observe.services.client;

/*
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.ObserveServiceFactory;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import io.ultreia.java4all.http.HResponseBuilder;
import io.ultreia.java4all.lang.Strings;
import org.apache.hc.client5.http.config.ConnectionConfig;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.BasicCookieStore;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.impl.io.BasicHttpClientConnectionManager;
import org.apache.hc.core5.http.io.SocketConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(ObserveServiceFactory.class)
public class ObserveServiceFactoryClient implements ObserveServiceFactory {

    private static final Logger log = LogManager.getLogger(ObserveServiceFactoryClient.class);
    private final static ClassMappingClient CLASS_MAPPING = ClassMappingClient.get();
    private final static int LOCATE_PREFIX_LENGTH = ObserveServiceClientSupport.class.getPackage().getName().length();
    private final ObserveRequestBuilderFactory requestBuilderFactory = new ObserveRequestBuilderFactory();

    private HResponseBuilder responseBuilder;

    private synchronized HResponseBuilder getResponseBuilder(ObserveServiceInitializer serviceInitializer) {
        if (responseBuilder == null) {
            int timeout = serviceInitializer.getInitializerConfig().getHttpTimeout();
            ConnectionConfig connConfig = ConnectionConfig.custom()
                                                          .setTimeToLive(200, TimeUnit.MILLISECONDS)
                                                          .setSocketTimeout(timeout, TimeUnit.MILLISECONDS)
                                                          .build();
            BasicHttpClientConnectionManager cm = new BasicHttpClientConnectionManager();
            cm.setConnectionConfig(connConfig);
            CloseableHttpClient httpClient = HttpClientBuilder.create()
                                                              .setConnectionManager(cm)
                                                              .setDefaultCookieStore(new BasicCookieStore())
                                                              .setDefaultRequestConfig(RequestConfig.custom().setResponseTimeout(timeout, TimeUnit.MILLISECONDS).build())
                                                              //FIXME-migration
//                .setMaxConnTotal(1000)
//                                             .setMaxConnPerRoute(1000)
//                .setConnectionTimeToLive(45, TimeUnit.SECONDS)
                                                              .build();
            responseBuilder = HResponseBuilder.create(new DtoGsonSupplier(), httpClient);
        }
        return responseBuilder;
    }

    @Override
    public <S extends ObserveService> boolean accept(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {
        Objects.requireNonNull(dataSourceConfiguration, "dataSourceConfiguration can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        return dataSourceConfiguration.isServer();
    }

    @Override
    public <S extends ObserveService> boolean accept(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {
        Objects.requireNonNull(dataSourceConnection, "dataSourceConfiguration can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        return dataSourceConnection.isServer();
    }

    @Override
    public <S extends ObserveService> S newService(ObserveServiceInitializer serviceInitializer, Class<S> serviceType) {
        Objects.requireNonNull(serviceInitializer, "serviceInitializerContext can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        Objects.requireNonNull(serviceInitializer.getInitializerConfig().getApplicationLocale(), "applicationLocale can't be null.");
        Objects.requireNonNull(serviceInitializer.getInitializerConfig().getReferentialLocale(), "referentialLocale can't be null.");
        Objects.requireNonNull(serviceInitializer.getInitializerConfig().getTemporaryDirectoryRoot(), "temporaryDirectoryRoot can't be null.");
        if (serviceInitializer.withConnection()) {
            ObserveDataSourceConnection dataSourceConnection = serviceInitializer.getConnection();
            if (!dataSourceConnection.isServer()) {
                throw new IllegalStateException("dataSourceConnection must be of type http, but url was " + dataSourceConnection.getUrl());
            }
        } else {
            ObserveDataSourceConfiguration dataSourceConfiguration = serviceInitializer.getConfiguration();
            if (!(dataSourceConfiguration.isServer())) {
                throw new IllegalStateException("dataSourceConfiguration must be of type  http but url was " + dataSourceConfiguration.getUrl());
            }
        }
        S service = CLASS_MAPPING.create(serviceType);
        Class<?> implementationType = service.getClass();
        String serviceLocation = implementationType.getCanonicalName().substring(LOCATE_PREFIX_LENGTH).replaceAll("\\.", "/");
        serviceLocation = Strings.removeEnd(serviceLocation, "Client");
        ObserveServiceClientContext serviceRestApiContext = new ObserveServiceClientContext(
                serviceInitializer,
                getResponseBuilder(serviceInitializer),
                requestBuilderFactory,
                serviceLocation);
        ((ObserveServiceClientSupport) service).setServiceContext(serviceRestApiContext);
        return service;
    }

    @Override
    public void close() {
        if (responseBuilder != null) {
            try {
                responseBuilder.close();
            } catch (Exception e) {
                log.error("Can't close response builder", e);
            }
        }
    }

}
