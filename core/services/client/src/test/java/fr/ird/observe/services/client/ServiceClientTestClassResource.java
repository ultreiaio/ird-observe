package fr.ird.observe.services.client;

/*
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.ObserveServiceInitializerConfig;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.test.TestClassResourceSupport;
import fr.ird.observe.test.spi.DatabaseClassifier;
import io.ultreia.java4all.util.Version;
import org.junit.runner.Description;

import java.io.IOException;
import java.util.Locale;

/**
 * Created on 03/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ServiceClientTestClassResource extends TestClassResourceSupport {

    private ObserveServiceFactoryClient serviceFactory;
    private ObserveServiceInitializerConfig serviceInitializerConfig;

    public ServiceClientTestClassResource() {
        super(DatabaseClassifier.DEFAULT);
    }

    public ObserveServiceFactoryClient getServiceFactory() {
        if (serviceFactory == null) {
            this.serviceFactory = new ObserveServiceFactoryClient();
        }
        return serviceFactory;
    }

    public <S extends ObserveService> S newService(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {
        ObserveServiceInitializer serviceInitializer = new ObserveServiceInitializer(getServiceInitializerConfig(), dataSourceConfiguration, null);
        serviceInitializer.setConfiguration(dataSourceConfiguration);
        return getServiceFactory().newService(serviceInitializer, serviceType);
    }

    public <S extends ObserveService> S newService(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {
        ObserveServiceInitializer serviceInitializer = new ObserveServiceInitializer(getServiceInitializerConfig(), null, dataSourceConnection);
        serviceInitializer.setConnection(dataSourceConnection);
        return getServiceFactory().newService(serviceInitializer, serviceType);
    }

    @Override
    protected void after(Description description) throws IOException {
        super.after(description);
        if (serviceFactory != null) {
            serviceFactory.close();
            serviceFactory = null;
        }
    }

    ObserveDataSourceConfigurationRest createDataSourceConfigurationRest(Class<?> testClass,
                                                                         String databaseName,
                                                                         Version dbVersion,
                                                                         String serverUrl,
                                                                         String login,
                                                                         char... password) {
        ObserveDataSourceConfigurationRest configurationRest = new ObserveDataSourceConfigurationRest();
        configurationRest.setLabel(testClass.getSimpleName() + "#" + serverUrl);
        configurationRest.setUrl(serverUrl);
        configurationRest.setLogin(login);
        configurationRest.setPassword(password);
        configurationRest.setDatabaseName(databaseName);
        configurationRest.setModelVersion(dbVersion);
        return configurationRest;

    }

    public ObserveServiceInitializerConfig getServiceInitializerConfig() {
        if (serviceInitializerConfig == null) {
            serviceInitializerConfig = new ObserveServiceInitializerConfig(
                    Locale.FRANCE,
                    ReferentialLocale.FR,
                    temporaryDirectoryRoot.toFile(),
                    30 * 1000, getDbVersion(), Version.create("9.0.0").build()/*FIXME Use from config */);
        }
        return serviceInitializerConfig;
    }
}
