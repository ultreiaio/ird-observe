package fr.ird.observe.services.client;

/*-
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.AnonymousService;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.test.ObserveTestConfiguration;
import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assume;
import org.junit.runner.Description;

import java.io.IOException;
import java.util.Objects;

/**
 * Created on 18/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ServiceClientTestMethodResourceRead extends ServiceClientTestClassResource {

    private static final Logger log = LogManager.getLogger(ServiceClientTestMethodResourceWrite.class);

    private ObserveDataSourceConfigurationRest dataSourceConfiguration;
    private ObserveServicesProvider servicesProvider;
    private ObserveDataSourceConnection dataSourceConnection;
    private DataSourceService dataSourceService;

    public ObserveDataSourceConfigurationRest getDataSourceConfiguration() {
        return dataSourceConfiguration;
    }

    public ObserveServicesProvider getServicesProvider() {
        return servicesProvider;
    }

    @Override
    protected void before(Description description) throws Throwable {

        super.before(description);

        Objects.requireNonNull(getUrl(), "Pas d'url spécifié");
        Objects.requireNonNull(getLogin(), "Pas de login spécifié");
        Objects.requireNonNull(getPassword(), "Pas de password spécifié");
        initTemporaryDirectoryRoot("all");
        Class<?> testClass = description.getTestClass();
        String methodName = description.getMethodName();

        Version modelVersion = ObserveTestConfiguration.getModelVersion();

        dataSourceConfiguration = createDataSourceConfigurationRest(testClass, getServerDbName(), modelVersion, getUrl(), getLogin(), getPassword());

        checkServerIsAvailable(testClass, methodName);

        dataSourceService = newService(dataSourceConfiguration, DataSourceService.class);
        dataSourceConnection = newService(dataSourceConfiguration, AnonymousService.class).open(dataSourceConfiguration);
        servicesProvider = new ObserveServicesProvider() {
            @Override
            public <S extends ObserveService> S getService(Class<S> serviceType) {
                try {
                    return newService(getDataSourceConfiguration(), serviceType);
                } catch (Exception e) {
                    throw new IllegalStateException("Can't get service: " + serviceType, e);
                }
            }
        };

    }

    @Override
    protected void after(Description description) throws IOException {
        super.after(description);
        if (dataSourceConnection != null) {
            dataSourceService.close();
        }
    }

    public <S extends ObserveService> S newService(Class<S> serviceType) {
        return newService(getDataSourceConfiguration(), serviceType);
    }

    private void checkServerIsAvailable(Class<?> testClass, String methodName) {

        boolean serverExist = true;

        AnonymousService service = newService(dataSourceConfiguration, AnonymousService.class);

        try {
            Version serverVersion = service.getModelVersion();
            Version modelVersion = ObserveTestConfiguration.getModelVersion();
            if (!serverVersion.equals(modelVersion)) {
                serverExist = false;
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("error on check server ", e);
            }
            serverExist = false;
        }

        if (!serverExist) {
            if (log.isWarnEnabled()) {
                log.warn("Skip test [" + testClass.getName() + "#" + methodName + "], server " + dataSourceConfiguration.getUrl() + " is not available.");
            }
        }
        Assume.assumeTrue("Server " + dataSourceConfiguration.getUrl() + " not found", serverExist);
    }
}

