package fr.ird.observe.services.client;

/*
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.service.AnonymousService;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestMethodResourceSupportWrite;
import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assume;
import org.junit.runner.Description;

import java.util.Objects;

/**
 * Created on 03/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ServiceClientTestMethodResourceWrite extends TestMethodResourceSupportWrite<ServiceClientTestClassResource> {

    /**
     * Logger.
     */
    private static final Logger log = LogManager.getLogger(ServiceClientTestMethodResourceWrite.class);

    private ObserveDataSourceConfigurationRest dataSourceConfiguration;

    public ServiceClientTestMethodResourceWrite(ServiceClientTestClassResource serviceClientTestClassResource) {
        super(serviceClientTestClassResource);
    }

    public ObserveDataSourceConfigurationRest getDataSourceConfiguration() {
        return dataSourceConfiguration;
    }

    @Override
    protected void before(Description description) throws Throwable {

        super.before(description);

        Objects.requireNonNull(getUrl(), "Pas d'url spécifié");
        Objects.requireNonNull(getLogin(), "Pas de login spécifié");
        Objects.requireNonNull(getPassword(), "Pas de password spécifié");

        Class<?> testClass = description.getTestClass();
        String methodName = description.getMethodName();

        Version modelVersion = ObserveTestConfiguration.getModelVersion();

        dataSourceConfiguration = testClassResource.createDataSourceConfigurationRest(testClass, getServerDbName(), modelVersion, getUrl(), getLogin(), getPassword());

        checkServerIsAvailable(testClass, methodName);

    }

    private void checkServerIsAvailable(Class<?> testClass, String methodName) {

        boolean serverExist = true;

        AnonymousService service = testClassResource.newService(dataSourceConfiguration, AnonymousService.class);

        try {
            Version serverVersion = service.getModelVersion();
            Version modelVersion = ObserveTestConfiguration.getModelVersion();
            if (!serverVersion.equals(modelVersion)) {
                serverExist = false;
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("error on check server ", e);
            }
            serverExist = false;
        }

        if (!serverExist) {
            if (log.isWarnEnabled()) {
                log.warn("Skip test [" + testClass.getName() + "#" + methodName + "], server " + dataSourceConfiguration.getUrl() + " is not available.");
            }
        }
        Assume.assumeTrue("Server " + dataSourceConfiguration.getUrl() + " not found", serverExist);
    }
}
