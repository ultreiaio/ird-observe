package fr.ird.observe.services.client.service;

/*
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.service.AnonymousService;
import fr.ird.observe.services.service.DataSourceService;
import io.ultreia.java4all.http.HResponseNotAvailableException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created on 03/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AnonymousServiceClientReadTest extends GeneratedAnonymousServiceClientReadTest {

    //FIXME
    @Ignore
    @Test
    public void testOpenNotExistingDatabase() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, CloneNotSupportedException, BabModelVersionException {

        ObserveDataSourceConfigurationRest dataSourceConfiguration = serviceClientTestMethodResource.getDataSourceConfiguration().clone();
        dataSourceConfiguration.setLogin(dataSourceConfiguration.getLogin() + System.nanoTime());
        serviceClientTestMethodResource.newService(dataSourceConfiguration, AnonymousService.class).open(dataSourceConfiguration);

    }

    @Test
    public void testOpen() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

        ObserveDataSourceConfigurationRest dataSourceConfiguration = serviceClientTestMethodResource.getDataSourceConfiguration();

        AnonymousService service1 = serviceClientTestMethodResource.newService(dataSourceConfiguration, AnonymousService.class);
        ObserveDataSourceConnection dataSourceConnection = service1.open(dataSourceConfiguration);
        Assert.assertNotNull(dataSourceConnection);
        Assert.assertNotNull(dataSourceConnection.getAuthenticationToken());

        AnonymousService service2 = serviceClientTestMethodResource.newService(dataSourceConfiguration, AnonymousService.class);
        ObserveDataSourceConnection dataSourceConnection2 = service2.open(dataSourceConfiguration);
        Assert.assertNotNull(dataSourceConnection2);
        Assert.assertNotNull(dataSourceConnection2.getAuthenticationToken());

        serviceClientTestMethodResource.newService(dataSourceConfiguration, DataSourceService.class).close();
    }

    //FIXME
    @Ignore
    @Test
    public void testCreateEmptyDataSource() throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {

        //FIXME Should get an not implemented exception for this service
        ObserveDataSourceConfigurationRest dataSourceConfiguration = serviceClientTestMethodResource.getDataSourceConfiguration();
        AnonymousService service = serviceClientTestMethodResource.newService(dataSourceConfiguration, AnonymousService.class);
        service.createEmpty(dataSourceConfiguration);

    }

    @Test(expected = HResponseNotAvailableException.class)
    public void testPingWithBadServerUrl() throws CloneNotSupportedException {
        ObserveDataSourceConfigurationRest dataSourceConfiguration = serviceClientTestMethodResource.getDataSourceConfiguration().clone();
        dataSourceConfiguration.setUrl("http://fake_" + System.nanoTime());
        AnonymousService service = serviceClientTestMethodResource.newService(dataSourceConfiguration, AnonymousService.class);
        service.getModelVersion();
    }

    @Test
    public void testPingWithBadUserName() throws CloneNotSupportedException {
        ObserveDataSourceConfigurationRest dataSourceConfiguration = serviceClientTestMethodResource.getDataSourceConfiguration().clone();
        dataSourceConfiguration.setLogin("fake_" + System.nanoTime());
        AnonymousService service = serviceClientTestMethodResource.newService(dataSourceConfiguration, AnonymousService.class);
        service.getModelVersion();
    }
}
