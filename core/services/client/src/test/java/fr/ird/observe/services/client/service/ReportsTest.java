package fr.ird.observe.services.client.service;

/*-
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.ReportService;
import fr.ird.observe.services.service.ReportServiceFixtures;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.util.Set;

/**
 * Created at 11/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ReportsTest extends ServiceClientTestSupportRead<ReportService> {

    protected static final ReportServiceFixtures fixtures = new ReportServiceFixtures();

    @Parameterized.Parameter
    public String reportId;

    public ReportsTest() {
        super(ReportService.class);
    }

    @Parameterized.Parameters(name = "{0}")
    public static Set<String> data() {
        return ReportServiceFixtures.getReports().keySet();
    }

    @Test
    public void tesReport() {
        ReportService service = getService();
        fixtures.executeReport(service, reportId);
    }
}
