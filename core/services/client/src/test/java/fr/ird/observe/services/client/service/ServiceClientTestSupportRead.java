package fr.ird.observe.services.client.service;

/*-
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.client.ServiceClientTestMethodResourceRead;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseServerNameConfiguration;
import fr.ird.observe.test.spi.DatabaseUrlConfiguration;
import io.ultreia.java4all.http.HRestApiService;
import io.ultreia.java4all.http.spi.Internal;
import org.junit.Before;
import org.junit.ClassRule;

import java.util.Objects;

/**
 * Created on 18/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@DatabaseLoginConfiguration(ObserveTestConfiguration.WEB_LOGIN)
@DatabasePasswordConfiguration(ObserveTestConfiguration.WEB_PASSWORD)
@DatabaseUrlConfiguration
@DatabaseServerNameConfiguration
@Internal // mark internal to not get it in any http plugin
public abstract class ServiceClientTestSupportRead<S extends ObserveService> implements HRestApiService<S> {
    @ClassRule
    public static final ServiceClientTestMethodResourceRead serviceClientTestMethodResource = new ServiceClientTestMethodResourceRead();
    private final Class<S> serviceType;
    protected S service;

    protected ServiceClientTestSupportRead(Class<S> serviceType) {
        this.serviceType = Objects.requireNonNull(serviceType);
    }

    @Before
    public void setUp() {
        service = serviceClientTestMethodResource.newService(serviceType);
    }

    @Override
    public S getService() {
        return service;
    }

    public ObserveServicesProvider getServicesProvider() {
        return serviceClientTestMethodResource.getServicesProvider();
    }


}
