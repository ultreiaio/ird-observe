package fr.ird.observe.services.client.service;

/*
 * #%L
 * ObServe Core :: Services :: Client
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.client.ServiceClientTestClassResource;
import fr.ird.observe.services.client.ServiceClientTestMethodResourceWrite;
import fr.ird.observe.services.service.AnonymousService;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseServerNameConfiguration;
import fr.ird.observe.test.spi.DatabaseUrlConfiguration;
import io.ultreia.java4all.http.HRestApiService;
import io.ultreia.java4all.http.spi.Internal;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;

import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
@DatabaseLoginConfiguration(ObserveTestConfiguration.WEB_LOGIN)
@DatabasePasswordConfiguration(ObserveTestConfiguration.WEB_PASSWORD)
@DatabaseUrlConfiguration
@DatabaseServerNameConfiguration
@Internal
public abstract class ServiceClientTestSupportWrite<S extends ObserveService> implements HRestApiService<S> {

    @ClassRule
    public static final ServiceClientTestClassResource REST_TEST_CLASS_RESOURCE = new ServiceClientTestClassResource();
    @Rule
    public final ServiceClientTestMethodResourceWrite serviceClientTestMethodResource = new ServiceClientTestMethodResourceWrite(REST_TEST_CLASS_RESOURCE);
    private final Class<S> serviceType;
    protected S service;
    private ObserveServicesProvider servicesProvider;
    private ObserveDataSourceConnection dataSourceConnection;

    private DataSourceService dataSourceService;

    protected ServiceClientTestSupportWrite(Class<S> serviceType) {
        this.serviceType = Objects.requireNonNull(serviceType);
    }

    @Override
    public S getService() {
        return service;
    }

    @Before
    public void setUp() throws Exception {

        ObserveDataSourceConfigurationRest dataSourceConfiguration = serviceClientTestMethodResource.getDataSourceConfiguration();

        dataSourceService = REST_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class);
        dataSourceConnection = REST_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, AnonymousService.class).open(dataSourceConfiguration);

        servicesProvider = new ObserveServicesProvider() {
            @Override
            public <S extends ObserveService> S getService(Class<S> serviceType) {
                try {
                    return REST_TEST_CLASS_RESOURCE.newService(serviceClientTestMethodResource.getDataSourceConfiguration(), serviceType);
                } catch (Exception e) {
                    throw new IllegalStateException("Can't get service: " + serviceType, e);
                }
            }
        };
        service = REST_TEST_CLASS_RESOURCE.newService(serviceClientTestMethodResource.getDataSourceConfiguration(), serviceType);
    }

    @After
    public void tearDown() {
        if (dataSourceConnection != null) {
            dataSourceService.close();
        }
    }

    public <S extends ObserveService> S newService(Class<S> serviceType) {
        return REST_TEST_CLASS_RESOURCE.newService(dataSourceConnection, serviceType);
    }

    protected <T extends BusinessDto> void assertEditLabels(Form<T> form, int expectedLabels, Class<?>... expectedTypes) {

        //FIXME Rest test
//        Assert.assertNotNull(formDto.getLabels());
//
//        Set<Class<?>> types = ReferenceSetDtos.getTypes(formDto.getLabels());
//
//        Assert.assertEquals(expectedTypes.length, types.size());
//
//        for (Class<?> expectedType : expectedTypes) {
//            Assert.assertTrue(types.contains(expectedType));
//        }
//        Assert.assertEquals(expectedLabels, formDto.sizeLabels());
//
//        for (ReferenceSetDto referenceSetDto : formDto.getLabels()) {
//
//            Assert.assertTrue(referenceSetDto.size() > 0);
//
//        }
    }

    protected <T extends BusinessDto> void assertReadLabels(Form<T> form, int expectedLabels, Class<?>... expectedTypes) {

        //FIXME Rest test
//        Assert.assertNotNull(formDto.getLabels());
//
//        Set<Class<?>> types = ReferenceSetDtos.getTypes(formDto.getLabels());
//
//        Assert.assertEquals(expectedTypes.length, types.size());
//
//        for (Class<?> expectedType : expectedTypes) {
//            Assert.assertTrue(types.contains(expectedType));
//        }
//        Assert.assertEquals(expectedLabels, formDto.sizeLabels());
//
//        for (ReferenceSetDto referenceSetDto : formDto.getLabels()) {
//
//            Assert.assertTrue(referenceSetDto.isReferenceEmpty());
//
//        }
    }

    public ObserveServicesProvider getServicesProvider() {
        return servicesProvider;
    }
}
