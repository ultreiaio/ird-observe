# Abstract

This module exposes the **Services I18n** of the *ObServe* project, which is all the **I18n** stuff used by *core* and 
*services* parts of the project.