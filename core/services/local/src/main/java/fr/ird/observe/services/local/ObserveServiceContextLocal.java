package fr.ird.observe.services.local;

/*
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.datasource.security.UnauthorizedException;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.services.ObserveServiceFactory;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.ObserveServicesProviderImpl;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.Objects;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public final class ObserveServiceContextLocal extends ObserveServicesProviderImpl {

    private static final Logger log = LogManager.getLogger(ObserveServiceContextLocal.class);
    private String methodName;
    private ObserveTopiaPersistenceContext persistenceContext;
    private ObserveTopiaApplicationContext topiaApplicationContext;
    private DecoratorService decoratorService;

    /**
     * If any error was found, will stop any commit then closing transaction.
     */
    private Throwable error;

    ObserveServiceContextLocal(ObserveServiceInitializer serviceInitializer, ObserveServiceFactory serviceFactory) {
        super(serviceFactory, () -> serviceInitializer);
    }

    public ObserveTopiaApplicationContext getTopiaApplicationContext() {
        return topiaApplicationContext;
    }

    public void setTopiaApplicationContext(ObserveTopiaApplicationContext topiaApplicationContext) {
        this.topiaApplicationContext = topiaApplicationContext;
    }

    public DecoratorService getDecoratorService() {
        if (decoratorService == null) {
            decoratorService = new DecoratorService(serviceInitializer().getInitializerConfig().getReferentialLocale());
        }
        return decoratorService;
    }

    public Date now() {
        return serviceInitializer().now();
    }

    public void initPersistence(String methodName, Permission methodeCredentials) {
        checkCredentials(methodName, methodeCredentials);
        if (getTopiaApplicationContext() == null) {
            if (persistenceContext != null) {
                throw new IllegalStateException("Can't have a persistence context while not having a persistence application context!");
            }
            ObserveServiceInitializer initializer = serviceInitializer();
            ObserveTopiaApplicationContext topiaApplicationContext;
            if (initializer.withConnection()) {
                String authenticationToken = initializer.getConnection().getAuthenticationToken();
                topiaApplicationContext = ObserveTopiaApplicationContextFactory.getContext(authenticationToken);
            } else {
                ObserveDataSourceConfiguration dataSourceConfiguration = initializer.getConfiguration();
                topiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(dataSourceConfiguration);
            }
            setTopiaApplicationContext(topiaApplicationContext);
        }
    }

    public boolean initReadTransaction(String methodName, Permission methodeCredentials) {
        initPersistence(methodName, methodeCredentials);
        if (persistenceContext == null) {
            // start new read transaction
            this.methodName = Objects.requireNonNull(methodName);
            log.debug(String.format("Init read persistenceContext for %s", methodName));
            this.persistenceContext = getTopiaApplicationContext().newPersistenceContext(null);
            // will close this transaction
            return true;
        }
        // will use existing transaction
        //FIXME Should check that credentials are ok with existing transaction
        // do not close the transaction
        return false;
    }

    public boolean initWriteTransaction(String methodName, Permission methodeCredentials) {
        initPersistence(methodName, methodeCredentials);
        if (persistenceContext == null) {
            // start new write transaction
            this.methodName = Objects.requireNonNull(methodName);
            log.debug(String.format("Init write persistenceContext for %s", methodName));
            this.persistenceContext = getTopiaApplicationContext().newPersistenceContext(this::commit);
            // will close this transaction
            return true;
        }
        // will use existing transaction
        //FIXME Should check that credentials are ok with existing transaction
        // do not close the transaction
        return false;
    }

    public void closeTransaction() {
        if (persistenceContext == null) {
            throw new IllegalStateException("No persistence context found, can not close it!");
        }
        try {
            persistenceContext.close();
        } finally {
            this.persistenceContext = null;
            this.methodName = null;
        }
    }

    public ObserveTopiaPersistenceContext getTopiaPersistenceContext() {
        return persistenceContext;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error, String methodName) {
        this.error = Objects.requireNonNull(error);
        if (log.isInfoEnabled()) {
            log.error(String.format("Could not invoke for %s", methodName), error);
        }
    }

    public void checkCredentials(String methodName, Permission methodeCredentials) {
        if (methodeCredentials != null && serviceInitializer().withConnection()) {
            ObserveDataSourceConnection dataSourceConnection = serviceInitializer().getConnection();
            if (dataSourceConnection.canNotExecute(methodeCredentials)) {
                throw new UnauthorizedException(serviceInitializer().getInitializerConfig().getApplicationLocale(), getClass().getCanonicalName(), methodName);
            }
        }
    }

    public boolean isH2Database() {
        if (serviceInitializer().withConnection()) {
            return topiaApplicationContext.getConfiguration().isH2Configuration();
        }
        return serviceInitializer().getConfiguration().isH2Configuration();
    }

    public DtoGsonSupplier newGsonSupplier(boolean prettyPrint, boolean serializeNulls) {
        return new DtoGsonSupplier(prettyPrint, serializeNulls);
    }

    private void commit(ObserveTopiaPersistenceContext persistenceContext) {
        if (error == null) {
            log.debug(String.format("Commit persistenceContext for %s", methodName));
            persistenceContext.commit();
        } else {
            log.warn(String.format("Skip commit persistenceContext for %s, due to error on call...", methodName));
        }
    }
}
