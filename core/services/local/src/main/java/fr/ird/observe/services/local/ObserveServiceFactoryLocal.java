package fr.ird.observe.services.local;

/*
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.services.ObserveServiceFactory;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.ObserveService;

import java.util.Objects;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(ObserveServiceFactory.class)
public class ObserveServiceFactoryLocal implements ObserveServiceFactory {

    private static final ClassMappingLocal CLASS_MAPPING = ClassMappingLocal.get();

    @Override
    public <S extends ObserveService> boolean accept(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {
        Objects.requireNonNull(dataSourceConfiguration, "dataSourceConfiguration can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        return !dataSourceConfiguration.isServer();
    }

    @Override
    public <S extends ObserveService> boolean accept(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {
        Objects.requireNonNull(dataSourceConnection, "dataSourceConfiguration can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        return !dataSourceConnection.isServer();
    }

    @Override
    public <S extends ObserveService> S newService(ObserveServiceInitializer serviceInitializer, Class<S> serviceType) {
        Objects.requireNonNull(serviceInitializer, "serviceInitializerContext can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        boolean anonymous = CLASS_MAPPING.isAnonymous(serviceType);
        if (serviceInitializer.withConnection()) {
            ObserveDataSourceConnection dataSourceConnection = serviceInitializer.getConnection();
            Objects.requireNonNull(dataSourceConnection, "dataSourceConnection can't be null.");
        } else {
            ObserveDataSourceConfiguration dataSourceConfiguration = serviceInitializer.getConfiguration();
            Objects.requireNonNull(dataSourceConfiguration, "dataSourceConfiguration can't be null.");
            if (!anonymous) {
                throw new IllegalStateException(String.format("Service %s is not anonymous, can not instantiate without a connexion, please call method AnonymousService.open before.", serviceType.getName()));
            }
        }
        S service = CLASS_MAPPING.create(serviceType);
        Objects.requireNonNull(service, "serviceTypeImpl not found for : " + serviceType.getName());
        ObserveServiceContextLocal serviceContext = createServiceContext(serviceInitializer);
        ((ObserveServiceLocal) service).setServiceContext(serviceContext);
        return service;
    }

    @Override
    public void close() {
        ObserveTopiaApplicationContextFactory.closeFactory();
    }

    protected ObserveServiceContextLocal createServiceContext(ObserveServiceInitializer observeServiceInitializer) {
        return new ObserveServiceContextLocal(observeServiceInitializer, this);
    }

}
