package fr.ird.observe.services.local.service;

/*
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.DatabaseVersionIsTooHighException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.datasource.security.model.DataSourceUserDto;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.entities.ObserveSecurityScriptConfiguration;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.entities.ObserveTopiaConfigurationFactory;
import fr.ird.observe.services.service.AnonymousService;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;

import java.nio.file.Path;
import java.util.Set;

/**
 * FIXME:Services move all this to toolkit
 * Created on 21/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AnonymousServiceLocalSupport extends ObserveServiceLocal implements AnonymousService {

    private static final Logger log = LogManager.getLogger(AnonymousServiceLocalSupport.class);
    private static final ObserveSecurityScriptConfiguration SECURITY_HELPER_MODEL = new ObserveSecurityScriptConfiguration();

    @Override
    public Version getModelVersion() {
        return serviceContext().serviceInitializer().getInitializerConfig().getPersistenceModelVersion();
    }

    @Override
    public Version getServerVersion() {
        Version buildVersion = serviceContext().serviceInitializer().getInitializerConfig().getApplicationBuildVersion();
        return buildVersion.isSnapshot() ? Version.removeSnapshot(buildVersion) : buildVersion;
    }

    @Override
    public ObserveDataSourceInformation checkCanConnectOrBeEmpty(ObserveDataSourceConfiguration config) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        return checkCanConnect0(config, true);
    }

    @Override
    public ObserveDataSourceInformation checkCanConnect(ObserveDataSourceConfiguration config) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        return checkCanConnect0(config, false);
    }

    @Override
    public ObserveDataSourceConnection createEmpty(ObserveDataSourceConfiguration config) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        setTemporaryDirectory(config);
        ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(config);
        log.info(String.format("Create topia application context: %s", topiaApplicationContext.getAuthenticationToken()));
        try {
            topiaApplicationContext.createEmpty();
        } catch (Exception e) {
            close(topiaApplicationContext);
            throw e;
        }
        ObserveDataSourceInformation dataSourceInformation = getDataSourceInformation(topiaApplicationContext, config.isLocal());
        return config.toConnection(topiaApplicationContext.getAuthenticationToken(), dataSourceInformation);
    }

    @Override
    public ObserveDataSourceConnection createFromDump(ObserveDataSourceConfiguration config, SqlScript dump) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        setTemporaryDirectory(config);
        ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(config);
        log.info(String.format("Create topia application context: %s", topiaApplicationContext.getAuthenticationToken()));
        try {
            topiaApplicationContext.createFromDump(config, dump);
        } catch (Exception e) {
            close(topiaApplicationContext);
            throw e;
        }
        ObserveDataSourceInformation dataSourceInformation = getDataSourceInformation(topiaApplicationContext, config.isLocal());
        return config.toConnection(topiaApplicationContext.getAuthenticationToken(), dataSourceInformation);
    }

    @Override
    public ObserveDataSourceConnection createFromImport(ObserveDataSourceConfiguration config, SqlScript dump, SqlScript optionalDump) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        setTemporaryDirectory(config);
        ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(config);
        log.info(String.format("Create topia application context: %s", topiaApplicationContext.getAuthenticationToken()));
        try {
            topiaApplicationContext.createAndImport(dump, optionalDump);
        } catch (Exception e) {
            close(topiaApplicationContext);
            throw e;
        }
        ObserveDataSourceInformation dataSourceInformation = getDataSourceInformation(topiaApplicationContext, config.isLocal());
        return config.toConnection(topiaApplicationContext.getAuthenticationToken(), dataSourceInformation);
    }

    @Override
    public ObserveDataSourceConnection open(ObserveDataSourceConfiguration config) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        setTemporaryDirectory(config);
        ObserveDataSourceInformation dataSourceInformation = checkCanConnect(config);
        Version dbVersion = dataSourceInformation.getVersion();
        Version requestVersion = config.getModelVersion();
        if (!config.isAutoMigrate() && !dbVersion.equals(requestVersion)) {
            BabModelVersionException.createAndThrow(getApplicationLocale(), requestVersion, dbVersion);
        }
        ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(config);
        log.info(String.format("Create topia application context: %s", topiaApplicationContext.getAuthenticationToken()));
        return config.toConnection(topiaApplicationContext.getAuthenticationToken(), dataSourceInformation);
    }

    @Override
    public Set<DataSourceUserDto> getUsers(ObserveDataSourceConfiguration config) {
        setTemporaryDirectory(config);
        try (ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(config)) {
            log.info(String.format("Create temporary topia application context: %s", topiaApplicationContext.getAuthenticationToken()));
            return topiaApplicationContext.newSecurityScriptHelper().getUsers();
        }
    }

    @Override
    public void applySecurity(ObserveDataSourceConfiguration config, Set<DataSourceUserDto> users) {
        setTemporaryDirectory(config);
        try (ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(config)) {
            log.info(String.format("Create temporary topia application context: %s", topiaApplicationContext.getAuthenticationToken()));
            topiaApplicationContext.newSecurityScriptHelper().applySecurity(users);
        }
    }

    @Override
    public void migrateData(ObserveDataSourceConfiguration config) {
        setTemporaryDirectory(config);
        try (ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(config)) {
            log.info(String.format("Create temporary topia application context: %s", topiaApplicationContext.getAuthenticationToken()));
            topiaApplicationContext.migrate();
        }
    }

    @Override
    public String generateHomeId() {
        return ObserveUtil.newUUID(now());
    }

    @Override
    public Set<String> getAvailableDatabaseNames() {
        // not used on local service
        return Set.of();
    }

    private void setTemporaryDirectory(ObserveDataSourceConfiguration dataSourceConfiguration) {
        Path temporaryDirectory = dataSourceConfiguration.getTemporaryDirectory();
        if (temporaryDirectory == null) {
            temporaryDirectory = serviceContext().serviceInitializer().getInitializerConfig().getTemporaryDirectoryRoot().toPath();
            dataSourceConfiguration.setTemporaryDirectory(temporaryDirectory);
        }
        log.debug(String.format("Will use temporary directory: %s", temporaryDirectory));
    }

    private ObserveDataSourceInformation checkCanConnect0(ObserveDataSourceConfiguration config, boolean canBeEmpty) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        setTemporaryDirectory(config);
        boolean localDatabase = config.isLocal();
        if (localDatabase) {
            ObserveDataSourceConfigurationTopiaH2 h2DataSourceConfiguration = (ObserveDataSourceConfigurationTopiaH2) config;
            String message = h2DataSourceConfiguration.checkConfiguration(getApplicationLocale());
            if (message != null) {
                log.warn(message);
            }
        }
        TopiaConfiguration topiaConfiguration = ObserveTopiaConfigurationFactory.create(config);
        ObserveDataSourceInformation dataSourceInformation = getDataSourceInformation(topiaConfiguration, localDatabase, canBeEmpty && !localDatabase);
        if (dataSourceInformation != null) {
            Version dbVersion = dataSourceInformation.getVersion();
            Version requestVersion = config.getModelVersion();
            if (dbVersion.after(requestVersion)) {
                // We cant migrate this database
                throw new DatabaseVersionIsTooHighException(getApplicationLocale(), requestVersion, dbVersion);
            }
        }
        return dataSourceInformation;
    }

    private ObserveDataSourceInformation getDataSourceInformation(TopiaConfiguration topiaConfiguration, boolean localDatabase, boolean canBeEmpty) throws DatabaseConnexionNotAuthorizedException {
        try {
            return JdbcHelper.create(topiaConfiguration).newSecurityHelper().getDataSourceInformation(SECURITY_HELPER_MODEL, localDatabase, canBeEmpty);
        } catch (Exception e) {
            throw new DatabaseConnexionNotAuthorizedException(getApplicationLocale(), e);
        }
    }

    private ObserveDataSourceInformation getDataSourceInformation(TopiaApplicationContext<?> topiaApplicationContext, boolean localDatabase) throws DatabaseConnexionNotAuthorizedException {
        try {
            return topiaApplicationContext.newJdbcHelper().newSecurityHelper().getDataSourceInformation(SECURITY_HELPER_MODEL, localDatabase, false);
        } catch (Exception e) {
            throw new DatabaseConnexionNotAuthorizedException(getApplicationLocale(), e);
        }
    }

    private void close(ObserveTopiaApplicationContext topiaApplicationContext) {
        if (topiaApplicationContext != null) {
            log.info("Closing topia application context: " + topiaApplicationContext.getAuthenticationToken());
            topiaApplicationContext.close();
        }
    }
}
