package fr.ird.observe.services.local.service;

/*
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.CopyRequest;
import fr.ird.observe.datasource.request.CreateDatabaseRequest;
import fr.ird.observe.datasource.request.DeleteRequest;
import fr.ird.observe.datasource.request.ReplicateRequest;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.DtoEntityContext;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * FIXME:Services move all this to toolkit
 * Created on 21/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DataSourceServiceLocalSupport extends ObserveServiceLocal implements DataSourceService {

    private static final Logger log = LogManager.getLogger(DataSourceServiceLocalSupport.class);

    @Override
    public void close() {
        serviceContext().serviceInitializer().optionalConnection().ifPresent(connection -> {
            String authenticationToken = connection.getAuthenticationToken();
            log.info("Closing topia application context: " + authenticationToken);
            ObserveTopiaApplicationContextFactory.closeContext(authenticationToken);
        });
    }

    @Override
    public <D extends BusinessDto> boolean isIdValid(Class<D> type, String id) {
        DtoEntityContext<D, ?, ?, ?> spi = ObservePersistenceBusinessProject.fromDto(type);
        return id != null && getTopiaPersistenceContext().isTopiaId(spi.toEntityType(), id);
    }

    @Override
    public Date getLastUpdateDate(String type) {
        return getTopiaPersistenceContext().getLastUpdateDate(type);
    }

    @Override
    public Set<String> retainExistingIds(Set<String> ids) {
        Set<String> result = new LinkedHashSet<>();
        ObserveTopiaPersistenceContext topiaPersistenceContext = getTopiaPersistenceContext();
        for (String id : ids) {
            boolean exists = topiaPersistenceContext.exists(id);
            if (exists) {
                result.add(id);
            }
        }
        return result;
    }

    @Override
    public SqlScript produceAddSqlScript(CopyRequest request) {
        return getTopiaApplicationContext().getSqlService().consume(request);
    }

    @Override
    public SqlScript produceMoveSqlScript(ReplicateRequest request) {
        return getTopiaApplicationContext().getSqlService().consume(request);
    }

    @Override
    public SqlScript produceCreateSqlScript(CreateDatabaseRequest request) {
        if (request.isAddStandaloneTables()) {
            checkCredentials("produceAddSqlScript", Permission.READ_REFERENTIAL);
        }
        if (request.isAddData()) {
            checkCredentials("produceAddSqlScript", Permission.READ_DATA);
        }
        return getTopiaApplicationContext().getSqlService().consume(request);
    }

    @Override
    public SqlScript produceDeleteSqlScript(DeleteRequest request) {
        return getTopiaApplicationContext().getSqlService().consume(request);
    }

    @Override
    public void executeSqlScript(SqlScript sqlScript) {
        getTopiaPersistenceContext().executeSqlScript(Objects.requireNonNull(sqlScript));
    }
}
