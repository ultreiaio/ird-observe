package fr.ird.observe.services.local.service;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.navigation.tree.NavigationResult;
import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModel;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelPathRequest;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.navigation.tree.navigation.RootNavigationTreeNode;
import fr.ird.observe.services.service.NavigationService;
import fr.ird.observe.spi.navigation.tree.navigation.ObserveNavigationTreeNodeBuildChildrenInterceptor;

import java.util.Date;
import java.util.Optional;

/**
 * Created on 07/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class NavigationServiceLocalSupport extends ObserveServiceLocal implements NavigationService {

    @Override
    public DataGroupByDtoSet<?, ?> getGroupByDtoSet(ToolkitTreeFlatModelRootRequest request) {
        return newTreeBuilder().buildDataGroupByDtoSet(request, now());
    }

    @Override
    public NavigationResult getNavigation(ToolkitTreeFlatModelRootRequest request, Date timestamp) {
        if (timestamp == null) {
            timestamp = now();
        } else {
            Optional<Date> optionalLastUpdateDate = getTopiaPersistenceContext().getLastUpdateDateDao().getLastUpdateDate();
            if (optionalLastUpdateDate.isPresent()) {
                Date lastUpdateDate = optionalLastUpdateDate.get();
                if (lastUpdateDate.after(timestamp)) {
                    // need to rebuild
                    // use now the last update date
                    timestamp = lastUpdateDate;
                } else {
                    // nothing has changed, return null
                    return null;
                }
            }
        }
        return newTreeBuilder().buildNavigationResult(request, timestamp);
    }

    @Override
    public ToolkitTreeFlatModel loadNavigationRoot(ToolkitTreeFlatModelRootRequest request) {
        return newTreeBuilder().buildNavigationFlatModel(request);
    }

    @Override
    public ToolkitTreeFlatModel loadNavigationPath(ToolkitTreeFlatModelPathRequest request) {
        ObserveDataSourceConnection permission = serviceContext().serviceInitializer().getConnection();
        return newTreeBuilder().loadNavigationPath(request, new RootNavigationTreeNode(), recursive -> new ObserveNavigationTreeNodeBuildChildrenInterceptor(recursive, permission.canWriteData()) {
            @Override
            protected String decorate(DataEntity data) {
                //FIXME Decorator Should use the ToolkitIdLabel decorator see ToolkitIdLabel.of
                @SuppressWarnings("unchecked") Class<DataEntity> contractType = (Class<DataEntity>) data.contractType();
                getDecoratorService().installDecorator(contractType, data);
                return data.toString();
            }
        });
    }

    @Override
    public ToolkitTreeFlatModel loadSelectionRoot(ToolkitTreeFlatModelRootRequest request) {
        return newTreeBuilder().buildSelectionFlatModel(request);
    }

}
