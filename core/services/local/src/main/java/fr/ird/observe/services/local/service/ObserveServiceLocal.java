package fr.ird.observe.services.local.service;

/*
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.datasource.DataSourceValidationMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ObserveIdFactory;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.data.DataGroupByEntity;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServiceMainFactory;
import fr.ird.observe.services.local.ObserveServiceContextLocal;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.services.service.api.InvalidDataException;
import fr.ird.observe.spi.GroupBySpiContext;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.ContainerDtoEntityContext;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.DataGroupByReferentialHelper;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.context.EditableDtoEntityContext;
import fr.ird.observe.spi.context.OpenableDtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.context.RootOpenableDtoEntityContext;
import fr.ird.observe.spi.context.SimpleDtoEntityContext;
import fr.ird.observe.spi.io.EntityDeserializer;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import fr.ird.observe.spi.mapping.ObserveReferenceDtoToDtoClassMapping;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.spi.navigation.tree.DtoToToolkitTreePathMapping;
import fr.ird.observe.spi.navigation.tree.TreeBuilderSupport;
import fr.ird.observe.spi.navigation.tree.navigation.ObserveDtoToNavigationTreePathMapping;
import fr.ird.observe.spi.service.ServiceContext;
import fr.ird.observe.spi.validation.ServiceValidationContext;
import fr.ird.observe.validation.ValidationContextSupport;
import fr.ird.observe.validation.api.request.DataValidationRequest;
import fr.ird.observe.validation.api.request.ReferentialValidationRequest;
import fr.ird.observe.validation.api.request.ValidationRequestConfigurationSupport;
import fr.ird.observe.validation.api.request.ValidationRequestSupport;
import fr.ird.observe.validation.api.result.ValidationResult;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.nuiton.topia.persistence.TopiaDao;

import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.function.Supplier;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ObserveServiceLocal implements ObserveService, ServiceContext {

    protected static final ObserveIdFactory STANDALONE_ID_FACTORY = new ObserveIdFactory();
    private ObserveServiceContextLocal serviceContext;
    private ReferentialLocale referentialLocale;
    private Locale applicationLocale;

    public final void setServiceContext(ObserveServiceContextLocal serviceContext) {
        this.serviceContext = Objects.requireNonNull(serviceContext, "serviceContext can't be null.");
        this.referentialLocale = serviceContext.serviceInitializer().getInitializerConfig().getReferentialLocale();
        this.applicationLocale = serviceContext.serviceInitializer().getInitializerConfig().getApplicationLocale();
    }

    @Override
    public final DataSourceValidationMode getValidationMode() {
        return serviceContext.serviceInitializer().getInitializerConfig().getValidationMode();
    }

    @Override
    public final ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    @Override
    public final Path getTemporaryDirectoryRoot() {
        return serviceContext.serviceInitializer().getInitializerConfig().getTemporaryDirectoryRoot().toPath();
    }

    @Override
    public final DecoratorService getDecoratorService() {
        return serviceContext.getDecoratorService();
    }

    @Override
    public final Locale getApplicationLocale() {
        return applicationLocale;
    }

    @Override
    public final <T> Future<T> submit(Callable<T> task) {
        return ObserveServiceMainFactory.getExecutorService().submit(task);
    }

    @Override
    public final ObserveTopiaPersistenceContext getTopiaPersistenceContext() {
        return serviceContext.getTopiaPersistenceContext();
    }

    @Override
    public final ObserveTopiaApplicationContext getTopiaApplicationContext() {
        return serviceContext.getTopiaApplicationContext();
    }

    @Override
    public final Date now() {
        return serviceContext.now();
    }

    @Override
    public final Timestamp timestampNow() {
        return new Timestamp(serviceContext.now().getTime());
    }

    @Override
    public boolean isPostgresDatabase() {
        return !serviceContext.isH2Database();
    }

    @Override
    public final DtoGsonSupplier newGsonSupplier(boolean prettyPrint, boolean serializeNulls) {
        return serviceContext.newGsonSupplier(prettyPrint, serializeNulls);
    }

    @Override
    public ObserveDataSourceInformation getDataSourceInformation() {
        return serviceContext.serviceInitializer().optionalConnection().orElseThrow().getDataSourceInformation();
    }

    @Override
    public final EntityDeserializer newEntityDeserializer(Supplier<Gson> gsonSupplier, Date now) {
        return new EntityDeserializer(getTopiaPersistenceContext(), gsonSupplier, getTopiaApplicationContext().newIdFactoryForBulk(now.getTime()), now);
    }

    @Override
    public final TreeBuilderSupport newTreeBuilder() {
        return new TreeBuilderSupport(this,
                                      ObserveBusinessProject.get(),
                                      serviceContext.serviceInitializer().getConnection(),
                                      this::fromGroupByName);
    }

    @Override
    public final Supplier<DtoToToolkitTreePathMapping> getNavigationTreePathMapping() {
        return ObserveDtoToNavigationTreePathMapping::get;
    }

    @Override
    public final void checkCredentials(String methodName, Permission methodeCredentials) {
        serviceContext.checkCredentials(methodName, methodeCredentials);
    }

    @Override
    public final <D extends DataDto,
            R extends DtoReference,
            E extends Entity,
            T extends TopiaDao<E>> DtoEntityContext<D, R, E, T> fromEntity(E dtoType) {
        return ObservePersistenceBusinessProject.fromEntity(dtoType);
    }

    @Override
    public final <D extends DataDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> fromDataDto(Class<D> dtoType) {
        return ObservePersistenceBusinessProject.fromDataDto(dtoType);
    }

    @Override
    public final <D extends DataDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> fromDataReference(Class<R> referenceType) {
        Class<D> dtoType = ObserveReferenceDtoToDtoClassMapping.get().get(referenceType);
        return ObservePersistenceBusinessProject.fromDataDto(dtoType);
    }

    @Override
    public final <D extends DataDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> fromDataDto(ToolkitIdDtoBean dtoType) {
        @SuppressWarnings("unchecked") Class<D> type = (Class<D>) dtoType.getType();
        return ObservePersistenceBusinessProject.fromDataDto(type);
    }

    @Override
    public final <PE extends Entity,
            D extends OpenableDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> OpenableDtoEntityContext<PE, D, R, E, T> fromOpenableDto(Class<D> dtoType) {
        return ObservePersistenceBusinessProject.fromOpenableDto(dtoType);
    }

    @Override
    public final <D extends RootOpenableDto,
            R extends DataDtoReference,
            E extends RootOpenableEntity,
            T extends TopiaDao<E>,
            H extends DataGroupByReferentialHelper<D, R, E>> RootOpenableDtoEntityContext<D, R, E, T, H> fromRootOpenableDto(Class<D> dtoType) {
        return ObservePersistenceBusinessProject.fromRootOpenableDto(dtoType);
    }

    @Override
    public final <PE extends Entity,
            D extends EditableDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> EditableDtoEntityContext<PE, D, R, E, T> fromEditableDto(Class<D> dtoType) {
        return ObservePersistenceBusinessProject.fromEditableDto(dtoType);
    }

    @Override
    public final <C extends ContainerChildDto,
            D extends ContainerDto<C>,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> ContainerDtoEntityContext<D, R, E, T> fromContainerDto(Class<D> dtoType) {
        return ObservePersistenceBusinessProject.fromContainerDto(dtoType);
    }

    @Override
    public final <D extends SimpleDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> SimpleDtoEntityContext<D, R, E, T> fromSimpleDto(Class<D> dtoType) {
        return ObservePersistenceBusinessProject.fromSimpleDto(dtoType);
    }

    @Override
    public final <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialDto(ToolkitIdDtoBean dtoType) {
        @SuppressWarnings("unchecked") Class<D> type = (Class<D>) dtoType.getType();
        return ObservePersistenceBusinessProject.fromReferentialDto(type);
    }

    @Override
    public final <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialEntity(Class<E> dtoType) {
        return ObservePersistenceBusinessProject.fromReferentialEntity(dtoType);
    }

    @Override
    public final <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialDto(Class<D> dtoType) {
        return ObservePersistenceBusinessProject.fromReferentialDto(dtoType);
    }

    @Override
    public final <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialReference(Class<R> referenceType) {
        Class<D> dtoType = ObserveReferenceDtoToDtoClassMapping.get().get(referenceType);
        return ObservePersistenceBusinessProject.fromReferentialDto(dtoType);
    }

    @Override
    public final <D extends RootOpenableDto,
            R extends DataDtoReference,
            E extends RootOpenableEntity,
            T extends TopiaDao<E>,
            H extends DataGroupByReferentialHelper<D, R, E>,
            F extends DataGroupByEntity<E>,
            FF extends DataGroupByDto<D>> GroupBySpiContext<D, R, E, T, H, F, FF> fromGroupByName(String parentProperty) {
        return ObservePersistenceBusinessProject.fromGroupByName(parentProperty);
    }

    @Override
    public final ServiceValidationContext createServiceValidationContext(ValidationRequestConfigurationSupport configuration, ValidationRequestSupport request) {
        return new ServiceValidationContext((ValidationRequestConfiguration) configuration, getDecoratorService(), new Project(), serviceContext);
    }

    public final ObserveServiceContextLocal serviceContext() {
        return serviceContext;
    }

    protected final void initPersistence(String methodName) {
        //FIXME For close method we got in a test a failure on this (the persistence was already close), need a fix
        serviceContext.initPersistence(methodName, Permission.NONE);
    }

    protected final boolean initReadTransaction(String methodName, Permission methodeCredentials) {
        return serviceContext.initReadTransaction(methodName, methodeCredentials);
    }

    protected final boolean initWriteTransaction(String methodName, Permission methodeCredentials) {
        return serviceContext.initWriteTransaction(methodName, methodeCredentials);
    }

    protected final void recordError(Exception e, String methodName) {
        serviceContext.setError(e, methodName);
    }

    protected final void closeTransaction(boolean doClose) {
        if (doClose) {
            serviceContext.closeTransaction();
        }
    }

    protected ReferentialValidationRequest newReferentialValidationRequest(Set<Class<? extends ReferentialDto>> referentialTypes) {
        ReferentialValidationRequest request = new ReferentialValidationRequest();
        request.setScopes(new LinkedHashSet<>(List.of(NuitonValidatorScope.ERROR, NuitonValidatorScope.FATAL)));
        request.setValidationContext(ValidationContextSupport.UPDATE_VALIDATION_CONTEXT);
        request.setReferentialTypes(referentialTypes);
        return request;
    }

    protected boolean withoutValidation() {
        return DataSourceValidationMode.NONE == getValidationMode();
    }

    protected DataValidationRequest newDataValidationRequest(String id) {
        DataValidationRequest request = new DataValidationRequest();
        request.setScopes(new LinkedHashSet<>(List.of(NuitonValidatorScope.ERROR, NuitonValidatorScope.FATAL)));
        request.setValidationContext(ValidationContextSupport.UPDATE_VALIDATION_CONTEXT);
        request.setDataIds(Set.of(id == null ? "__create" : id));
        return request;
    }

    protected final void checkValidationResult(ValidationResult result) throws InvalidDataException {
        boolean withError = result.withError();
        if (withError) {
            String message = String.format("Invalid content:\n%s", newGsonSupplier(true, false).disableHtmlEscaping().get().toJson(result));
            throw new InvalidDataException(message, result);
        }
        //FIXME We should may be return the result for warning messages?
    }

    protected final ValidationRequestConfiguration newValidationRequestConfiguration(boolean forData) {
        DataSourceValidationMode validationMode = getValidationMode();
        ValidationRequestConfiguration configuration = new ValidationRequestConfiguration();
        if (DataSourceValidationMode.STRONG == validationMode) {
            configuration.setValidationUseDisabledReferential(true);
            configuration.setValidationSpeedEnable(true);
            configuration.setValidationSpeedMaxValue(30f);
            configuration.setValidationLengthWeightEnable(true);
        }
        if (forData) {
            //FIXME Coming from public API, need to add default configuration on server config ?
            //configuration.setSeineBycatchObservedSystemConfig(new SeineBycatchObservedSystemConfig());
        }
        return configuration;
    }
}


