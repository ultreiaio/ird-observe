package fr.ird.observe.services.local.service;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.services.service.ReferenceService;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;

import java.util.Objects;

/**
 * Created on 10/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
class ReferenceServiceLocalSupport extends ObserveServiceLocal implements ReferenceService {

    @Override
    public <R extends DataDtoReference> R createData(Class<R> type) {
        DataDtoEntityContext<?, R, ?, ?> spi = fromDataReference(type);
        return spi.toReference(this, null, null);
    }

    @Override
    public <R extends DataDtoReference> R loadData(Class<R> type, String id, String classifier) {
        DataDtoEntityContext<?, R, ?, ?> spi = fromDataReference(type);
        return spi.toReference(this, Objects.requireNonNull(id), classifier);
    }

    @Override
    public <R extends ReferentialDtoReference> R createReferential(Class<R> type) {
        ReferentialDtoEntityContext<?, R, ?, ?> spi = fromReferentialReference(type);
        return spi.toReference(this, null);
    }

    @Override
    public <R extends ReferentialDtoReference> R loadReferential(Class<R> type, String id) {
        ReferentialDtoEntityContext<?, R, ?, ?> spi = fromReferentialReference(type);
        return spi.toReference(this, Objects.requireNonNull(id));
    }
}
