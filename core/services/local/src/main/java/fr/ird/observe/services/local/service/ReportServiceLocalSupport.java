package fr.ird.observe.services.local.service;

/*
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportColumnRenderersParameters;
import fr.ird.observe.report.ReportRequestExecutor;
import fr.ird.observe.report.definition.ReportDefinition;
import fr.ird.observe.services.service.ReportService;
import io.ultreia.java4all.util.matrix.DataMatrix;

import java.util.Set;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReportServiceLocalSupport extends ObserveServiceLocal implements ReportService {
    @Override
    public ReportColumnRenderersParameters initColumnRendererParameters(ReportDefinition reportDefinition) {
        ReportRequestExecutor executor = getTopiaPersistenceContext().newReportRequestExecutor(reportDefinition, getReferentialLocale());
        return executor.initColumnRendererParameters(reportDefinition);
    }

    @Override
    public Report populateVariables(Report report, Set<String> tripIds) {
        ReportRequestExecutor executor = getTopiaPersistenceContext().newReportRequestExecutor(report.definition(), getReferentialLocale());
        return executor.populateVariables(report, tripIds);
    }

    @Override
    public DataMatrix executeReport(Report report, Set<String> tripId) {
        ReportRequestExecutor executor = getTopiaPersistenceContext().newReportRequestExecutor(report.definition(), getReferentialLocale());
        return executor.executeReport(report, tripId);
    }
}
