package fr.ird.observe.services.local.service;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.UsageService;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.Set;

/**
 * Created on 11/02/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class UsageServiceLocalSupport extends ObserveServiceLocal implements UsageService {

    public static UsageCount countReferential(ServiceContext context, ToolkitIdDtoBean request) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = context.fromReferentialDto(request);
        return spi.count(context, request);
    }

    public static UsageCount countOptionalData(ServiceContext context, ToolkitIdDtoBean request) {
        DataDtoEntityContext<?, ?, ?, ?> spi = context.fromDataDto(request);
        return spi.countOptional(context, request);
    }

    public static UsageCount countMandatoryData(ServiceContext context, ToolkitIdDtoBean request) {
        DataDtoEntityContext<?, ?, ?, ?> spi = context.fromDataDto(request);
        return spi.countMandatory(context, request);
    }

    public static <D extends BusinessDto> Set<ToolkitIdLabel> findOptionalData(ServiceContext context, ToolkitIdDtoBean request, Class<D> targetType) {
        DataDtoEntityContext<?, ?, ?, ?> spi = context.fromDataDto(request);
        DtoEntityContext<?, ?, ?, ?> spi2 = ObservePersistenceBusinessProject.fromDto(targetType);
        return spi.findOptionalUsages(context, request, spi2);
    }

    public static <D extends BusinessDto> Set<ToolkitIdLabel> findMandatoryData(ServiceContext context, ToolkitIdDtoBean request, Class<D> targetType) {
        DataDtoEntityContext<?, ?, ?, ?> spi = context.fromDataDto(request);
        DtoEntityContext<?, ?, ?, ?> spi2 = ObservePersistenceBusinessProject.fromDto(targetType);
        return spi.findMandatoryUsages(context, request, spi2);
    }

    @Override
    public UsageCount countReferential(ToolkitIdDtoBean request) {
        return countReferential(this, request);
    }

    @Override
    public UsageCount countReferentialInData(ToolkitIdDtoBean request) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(request);
        // reject any result in referential packages
        return spi.count(this, request, c -> !c.getPackageName().contains(".referential"));
    }

    @Override
    public <D extends BusinessDto> Set<ToolkitIdLabel> findReferential(ToolkitIdDtoBean request, Class<D> targetType) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(request);
        DtoEntityContext<?, ?, ?, ?> spi2 = ObservePersistenceBusinessProject.fromDto(targetType);
        return spi.findUsages(this, request, spi2);
    }

    @Override
    public UsageCount countOptionalData(ToolkitIdDtoBean request) {
        return countOptionalData(this, request);
    }

    @Override
    public UsageCount countMandatoryData(ToolkitIdDtoBean request) {
        return countMandatoryData(this, request);
    }

    @Override
    public <D extends BusinessDto> Set<ToolkitIdLabel> findOptionalData(ToolkitIdDtoBean request, Class<D> targetType) {
        return findOptionalData(this, request, targetType);
    }

    @Override
    public <D extends BusinessDto> Set<ToolkitIdLabel> findMandatoryData(ToolkitIdDtoBean request, Class<D> targetType) {
        return findMandatoryData(this, request, targetType);
    }
}
