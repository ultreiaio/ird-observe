package fr.ird.observe.services.local.service.api;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.navigation.tree.io.request.ToolkitRequestConfig;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.api.DataEntityService;
import fr.ird.observe.services.service.api.InvalidDataException;
import fr.ird.observe.services.service.api.OrderEnum;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.validation.ValidationHelper;
import fr.ird.observe.validation.api.request.DataValidationRequest;
import fr.ird.observe.validation.api.result.ValidationResult;
import org.nuiton.topia.persistence.filter.ToolkitRequestFilter;

import java.util.List;
import java.util.Map;

/**
 * Created on 18/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class DataEntityServiceLocalSupport extends ObserveServiceLocal implements DataEntityService {

    @Override
    public ToolkitTreeNodeStates getOne(Class<? extends DataDto> dtoType, ToolkitRequestConfig config, String id) {
        DataDtoEntityContext<?, ?, ?, ?> spi = fromDataDto(dtoType);
        return spi.toStates(this, config, ToolkitRequestFilter.onId(id));
    }

    @Override
    public ToolkitTreeNodeStates getSome(Class<? extends DataDto> dtoType, ToolkitRequestConfig config, Map<String, String> filters, Map<String, OrderEnum> orders) {
        //FIXME Add requiresNotNull on all parameters on concrete service
        DataDtoEntityContext<?, ?, ?, ?> spi = fromDataDto(dtoType);
        return spi.toStates(this, config, ToolkitRequestFilter.requiresNotAll(filters, orders));
    }

    @Override
    public List<String> generateId(Class<? extends DataDto> dtoType, int number) {
        DataDtoEntityContext<?, ?, ?, ?> spi = fromDataDto(dtoType);
        return STANDALONE_ID_FACTORY.generateId(spi.toEntityType(), number);
    }

    @Override
    public ToolkitId create(Class<? extends DataDto> dtoType, String content) throws InvalidDataException {
        DataDtoEntityContext<?, ?, ?, ?> spi = fromDataDto(dtoType);
        return spi.create(this, content, this::doValidate);
    }

    @Override
    public ToolkitId update(Class<? extends DataDto> dtoType, String id, String content) throws InvalidDataException {
        id = ToolkitId.decodeId(id);
        DataDtoEntityContext<?, ?, ?, ?> spi = fromDataDto(dtoType);
        return spi.update(this, id, content, this::doValidate);
    }

    @Override
    public void delete(Class<? extends DataDto> dtoType, String id) {
        id = ToolkitId.decodeId(id);
        DataDtoEntityContext<?, ?, ?, ?> spi = fromDataDto(dtoType);
        spi.delete(this, id);
    }

    protected <E extends DataEntity> void doValidate(E entity) throws InvalidDataException {
        if (withoutValidation()) {
            return;
        }
        //FIXME Must have this somewhere on server or request
        ValidationRequestConfiguration configuration = newValidationRequestConfiguration(true);
        DataValidationRequest request = newDataValidationRequest(entity.getTopiaId());
        ValidationResult result = ValidationHelper.validateApiData(this, configuration, request, entity);
        checkValidationResult(result);
    }
}
