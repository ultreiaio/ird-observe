package fr.ird.observe.services.local.service.api;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.navigation.tree.io.request.ToolkitRequestConfig;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.api.InvalidDataException;
import fr.ird.observe.services.service.api.OrderEnum;
import fr.ird.observe.services.service.api.ReferentialEntityService;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProjectVisitor;
import fr.ird.observe.spi.module.BusinessReferentialPackage;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.spi.validation.ValidationHelper;
import fr.ird.observe.validation.api.request.ReferentialValidationRequest;
import fr.ird.observe.validation.api.result.ValidationResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.filter.ToolkitRequestFilter;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 18/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ReferentialEntityServiceLocalSupport extends ObserveServiceLocal implements ReferentialEntityService {
    public static final String STATE_CONTENT_NAME = ToolkitTreeNodeBean.STATE_CONTENT.name();
    public static final String STATE_REFERENCES_NAME = ToolkitTreeNodeBean.STATE_REFERENCES.name();
    private static final Logger log = LogManager.getLogger(ReferentialEntityServiceLocalSupport.class);

    @Override
    public ToolkitTreeNodeStates getByModule(String moduleName, ToolkitRequestConfig config) {
        Set<Class<? extends ReferentialDto>> dtoTypes = new LinkedHashSet<>();
        ObserveBusinessProject.get().accept(new BusinessProjectVisitor() {
            @Override
            public void enterSubModuleReferentialType(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage, Class<? extends ReferentialDto> dtoType) {
                if (moduleName == null || moduleName.equals(module.getName())) {
                    dtoTypes.add(dtoType);
                }
            }
        });
        log.info(String.format("Perform getByModule on %d referential type(s)", dtoTypes.size()));
        return getByType(config, dtoTypes);
    }

    @Override
    public ToolkitTreeNodeStates getByPackage(String packageName, ToolkitRequestConfig config) {
        Set<Class<? extends ReferentialDto>> dtoTypes = new LinkedHashSet<>();
        ObserveBusinessProject.get().accept(new BusinessProjectVisitor() {
            @Override
            public void enterSubModuleReferentialType(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage, Class<? extends ReferentialDto> dtoType) {
                if (packageName == null || packageName.equals(module.getName() + "_" + subModule.getName())) {
                    dtoTypes.add(dtoType);
                }
            }
        });
        log.info(String.format("Perform getByPackage on %d referential type(s)", dtoTypes.size()));
        return getByType(config, dtoTypes);
    }

    @Override
    public ToolkitTreeNodeStates getAll(Class<? extends ReferentialDto> dtoType, ToolkitRequestConfig config, Map<String, OrderEnum> orders) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(dtoType);
        return spi.toStates(this, config, ToolkitRequestFilter.onOrders(orders));
    }

    @Override
    public ToolkitTreeNodeStates getOne(Class<? extends ReferentialDto> dtoType, ToolkitRequestConfig config, String id) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(dtoType);
        return spi.toStates(this, config, ToolkitRequestFilter.onId(id));
    }

    @Override
    public ToolkitTreeNodeStates getSome(Class<? extends ReferentialDto> dtoType, ToolkitRequestConfig config, Map<String, String> filters, Map<String, OrderEnum> orders) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(dtoType);
        return spi.toStates(this, config, ToolkitRequestFilter.requiresNotAll(filters, orders));
    }

    @Override
    public List<String> generateId(Class<? extends ReferentialDto> dtoType, int number) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(dtoType);
        return STANDALONE_ID_FACTORY.generateId(spi.toEntityType(), number);
    }

    @Override
    public void delete(Class<? extends ReferentialDto> dtoType, String id) {
        id = ToolkitId.decodeId(id);
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(dtoType);
        spi.delete(this, id);
    }

    @Override
    public ToolkitId create(Class<? extends ReferentialDto> dtoType, String content) throws InvalidDataException {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(dtoType);
        return spi.create(this, content, e -> doValidate(spi.toDtoType(), e));
    }

    @Override
    public ToolkitId update(Class<? extends ReferentialDto> dtoType, String id, String content) throws InvalidDataException {
        id = ToolkitId.decodeId(id);
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(dtoType);
        return spi.update(this, id, content, e -> doValidate(spi.toDtoType(), e));
    }

    private <D extends ReferentialDto, E extends ReferentialEntity> void doValidate(Class<D> dtoType, E entity) throws InvalidDataException {
        if (withoutValidation()) {
            return;
        }
        //FIXME Must have this somewhere on server or request
        ValidationRequestConfiguration configuration = newValidationRequestConfiguration(false);
        ReferentialValidationRequest request = newReferentialValidationRequest(Set.of(dtoType));
        //FIXME What about the not persisted case ? See Data
        ValidationResult result = ValidationHelper.validateApiReferential(this, configuration, request, dtoType, entity);
        checkValidationResult(result);
    }

    protected ToolkitTreeNodeStates getByType(ToolkitRequestConfig config, Set<Class<? extends ReferentialDto>> dtoTypes) {
        ToolkitTreeNodeStates result = new ToolkitTreeNodeStates();
        StringBuilder contentBuilder = new StringBuilder();
        StringBuilder referencesBuilder = new StringBuilder();
        boolean prettyPrint = config.isPrettyPrint();
        for (Class<? extends ReferentialDto> dtoType : dtoTypes) {
            DtoEntityContext<? extends ReferentialDto, DtoReference, Entity, ?> spi = PersistenceBusinessProject.fromDto(dtoType);
            Class<? extends Entity> entityType = spi.toEntityType();
            String entityTypeName = entityType.getName();
            ToolkitTreeNodeStates states = spi.toStates(this, config, new ToolkitRequestFilter());
            String contentState = states.getState(STATE_CONTENT_NAME);
            addProperty(prettyPrint, entityTypeName, contentBuilder, contentState);
            String referencesState = states.getState(STATE_REFERENCES_NAME);
            addProperty(prettyPrint, entityTypeName, referencesBuilder, referencesState);
        }
        result.addState(STATE_CONTENT_NAME, "{\n" + contentBuilder.append("\n}"));
        if (referencesBuilder.length() > 0) {
            result.addState(STATE_REFERENCES_NAME, "{\n" + referencesBuilder.append("\n}"));
        }
        return result;
    }

    protected void addProperty(boolean prettyPrint, String entityType, StringBuilder builder, String contentState) {
        if (contentState != null) {
            if (builder.length() > 0) {
                builder.append("\n,");
            }
            if (prettyPrint) {
                builder.append(String.format("\n    \"%s\":\n%s", entityType, contentState));
            } else {
                builder.append(String.format("\"%s\": %s", entityType, contentState));
            }
        }
    }
}
