package fr.ird.observe.services.local.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.data.ContainerService;
import fr.ird.observe.spi.context.ContainerDtoEntityContext;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ContainerServiceLocalSupport extends ObserveServiceLocal implements ContainerService {

    @Override
    public <C extends ContainerChildDto, D extends ContainerDto<C>> Form<D> loadForm(Class<D> dtoType, String parentId) {
        ContainerDtoEntityContext<D, ?, ?, ?> spi = fromContainerDto(dtoType);
        return spi.loadForm(this, parentId);
    }

    @Override
    public <C extends ContainerChildDto, D extends ContainerDto<C>> SaveResultDto save(D dto) throws ConcurrentModificationException {
        @SuppressWarnings("unchecked") Class<D> dtoType = (Class<D>) dto.getClass();
        ContainerDtoEntityContext<D, ?, ?, ?> spi = fromContainerDto(dtoType);
        return spi.save(this, dto);
    }
}
