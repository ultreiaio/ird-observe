package fr.ird.observe.services.local.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.data.DataManagementService;
import fr.ird.observe.services.service.data.DeleteDataRequest;
import fr.ird.observe.services.service.data.DeleteDataResult;
import fr.ird.observe.services.service.data.ExportDataRequest;
import fr.ird.observe.services.service.data.ExportDataResult;
import fr.ird.observe.services.service.data.ImportDataRequest;
import fr.ird.observe.services.service.data.ImportDataResult;

/**
 * Created on 20/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class DataManagementServiceLocalSupport extends ObserveServiceLocal implements DataManagementService {

    @Override
    public ExportDataResult exportData(ExportDataRequest request) {
        return getTopiaPersistenceContext().exportData(this, request);
    }

    @Override
    public DeleteDataResult deleteData(DeleteDataRequest request) {
        return getTopiaPersistenceContext().deleteData(this, request);
    }

    @Override
    public ImportDataResult importData(ImportDataRequest request) {
        return getTopiaPersistenceContext().importData(this, request);
    }
}
