package fr.ird.observe.services.local.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.data.EditableService;
import fr.ird.observe.spi.context.EditableDtoEntityContext;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class EditableServiceLocalSupport extends ObserveServiceLocal implements EditableService {
    @Override
    public <D extends EditableDto> Form<D> loadForm(Class<D> dtoType, String id) {
        EditableDtoEntityContext<?, D, ?, ?, ?> spi = fromEditableDto(dtoType);
        return spi.loadForm(this, id);
    }

    @Override
    public <D extends EditableDto> D loadDto(Class<D> dtoType, String id) {
        EditableDtoEntityContext<?, D, ?, ?, ?> spi = fromEditableDto(dtoType);
        return spi.loadEntityToDto(this, id);
    }

    @Override
    public <D extends EditableDto> boolean exists(Class<D> dtoType, String id) {
        EditableDtoEntityContext<?, ?, ?, ?, ?> spi = fromEditableDto(dtoType);
        return spi.existsEntity(this, id);
    }

    @Override
    public <D extends EditableDto> Form<D> preCreate(Class<D> dtoType, String parentId) {
        EditableDtoEntityContext<?, D, ?, ?, ?> spi = fromEditableDto(dtoType);
        return spi.preCreate(this, parentId);
    }

    @Override
    public <D extends EditableDto> SaveResultDto save(String parentId, D dto) throws ConcurrentModificationException {
        @SuppressWarnings("unchecked") Class<D> dtoType = (Class<D>) dto.getClass();
        EditableDtoEntityContext<?, D, ?, ?, ?> spi = fromEditableDto(dtoType);
        return spi.save(this, parentId, dto);
    }

    @Override
    public <D extends EditableDto> boolean delete(Class<D> dtoType, String id) {
        EditableDtoEntityContext<?, ?, ?, ?, ?> spi = fromEditableDto(dtoType);
        spi.delete(this, id);
        return false;
    }
}
