package fr.ird.observe.services.local.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.local.service.UsageServiceLocalSupport;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.data.MoveRequest;
import fr.ird.observe.services.service.data.OpenableService;
import fr.ird.observe.spi.context.OpenableDtoEntityContext;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created on 24/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class OpenableServiceLocalSupport extends ObserveServiceLocal implements OpenableService {

    @Override
    public <D extends OpenableDto> List<ToolkitIdLabel> getBrothers(Class<D> dtoType, String id) {
        OpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.getBrothersLabel(this, id);
    }

    @Override
    public <D extends OpenableDto> List<ToolkitIdLabel> getBrothersFromParent(Class<D> dtoType, String parentId) {
        OpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.getBrothersFromParentLabel(this, parentId, null);
    }

    @Override
    public <D extends OpenableDto, R extends DataDtoReference> DataDtoReferenceSet<R> getChildren(Class<D> dtoType, String parentId) {
        OpenableDtoEntityContext<?, ?, R, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.getChildren(this, parentId);
    }

    @Override
    public <D extends OpenableDto, R extends DataDtoReference> UpdatedDataDtoReferenceSet<R> getChildrenUpdate(Class<D> dtoType, String parentId, Date lastUpdate) {
        OpenableDtoEntityContext<?, ?, R, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.getChildrenUpdate(this, parentId, lastUpdate);
    }

    @Override
    public <D extends OpenableDto> Form<D> loadForm(Class<D> dtoType, String id) {
        OpenableDtoEntityContext<?, D, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.loadForm(this, id);
    }

    @Override
    public <D extends OpenableDto> D loadDto(Class<D> dtoType, String id) {
        OpenableDtoEntityContext<?, D, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.loadEntityToDto(this, id);
    }

    @Override
    public <D extends OpenableDto> Form<D> preCreate(Class<D> dtoType, String parentId) {
        OpenableDtoEntityContext<?, D, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.preCreate(this, parentId);
    }

    @Override
    public <D extends OpenableDto> boolean exists(Class<D> dtoType, String id) {
        OpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.existsEntity(this, id);
    }

    @Override
    public <D extends OpenableDto> SaveResultDto save(String parentId, D dto) throws ConcurrentModificationException {
        @SuppressWarnings("unchecked") Class<D> dtoType = (Class<D>) dto.getClass();
        OpenableDtoEntityContext<?, D, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.save(this, parentId, dto);
    }

    @Override
    public <D extends OpenableDto> UsageCount getOptionalDependenciesCount(Class<D> dtoType, Set<String> ids) {
        OpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.getDependenciesCount(this, d -> UsageServiceLocalSupport.countOptionalData(this, d), ids);
    }

    @Override
    public <D extends OpenableDto> UsageCount getMandatoryDependenciesCount(Class<D> dtoType, Set<String> ids) {
        OpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.getDependenciesCount(this, d -> UsageServiceLocalSupport.countMandatoryData(this, d), ids);
    }

    @Override
    public <D extends OpenableDto, T extends BusinessDto> Set<ToolkitIdLabel> getOptionalDependencies(Class<D> dtoType, Set<String> ids, Class<T> targetType) {
        OpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.getDependencies(this, id -> UsageServiceLocalSupport.findOptionalData(this, id, targetType), ids);
    }

    @Override
    public <D extends OpenableDto, T extends BusinessDto> Set<ToolkitIdLabel> getMandatoryDependencies(Class<D> dtoType, Set<String> ids, Class<T> targetType) {
        OpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.getDependencies(this, id -> UsageServiceLocalSupport.findMandatoryData(this, id, targetType), ids);
    }

    @Override
    public <D extends OpenableDto> boolean delete(Class<D> dtoType, String id) {
        OpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromOpenableDto(dtoType);
        spi.delete(this, id);
        return false;
    }

    @Override
    public <D extends OpenableDto> Set<String> move(Class<D> dtoType, MoveRequest request) throws ConcurrentModificationException {
        OpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromOpenableDto(dtoType);
        return spi.move(this, request);
    }
}
