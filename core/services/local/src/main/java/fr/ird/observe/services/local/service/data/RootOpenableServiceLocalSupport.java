package fr.ird.observe.services.local.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.data.DeleteLayoutRequest;
import fr.ird.observe.services.service.data.MissingReferentialRequest;
import fr.ird.observe.services.service.data.MissingReferentialResult;
import fr.ird.observe.services.service.data.MoveLayoutRequest;
import fr.ird.observe.services.service.data.RootOpenableService;
import fr.ird.observe.spi.GroupBySpiContext;
import fr.ird.observe.spi.context.RootOpenableDtoEntityContext;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created on 29/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class RootOpenableServiceLocalSupport extends ObserveServiceLocal implements RootOpenableService {
    @Override
    public List<ToolkitIdLabel> getBrothers(DataGroupByParameter groupBy, String id) {
        GroupBySpiContext<?, ?, ?, ?, ?, ?, ?> spi = fromGroupByName(groupBy.getGroupByName());
        return spi.getBrothersLabel(this, groupBy.getValue(), groupBy.getGroupByFlavor(), id);
    }

    @Override
    public String getGroupByValue(String groupByName, String groupByFlavor, String id) {
        GroupBySpiContext<?, ?, ?, ?, ?, ?, ?> spi = fromGroupByName(groupByName);
        return spi.getGroupByValue(this, groupByFlavor, id);
    }

    @Override
    public <R extends DataDtoReference> DataDtoReferenceSet<R> getChildren(DataGroupByParameter groupBy) {
        GroupBySpiContext<?, R, ?, ?, ?, ?, ?> spi = fromGroupByName(groupBy.getGroupByName());
        return spi.getChildren(this, groupBy.getValue(), groupBy.getGroupByFlavor());
    }

    @Override
    public <R extends DataDtoReference> UpdatedDataDtoReferenceSet<R> getChildrenUpdate(DataGroupByParameter groupBy, Date lastUpdate) {
        GroupBySpiContext<?, R, ?, ?, ?, ?, ?> spi = fromGroupByName(groupBy.getGroupByName());
        return spi.getChildrenUpdate(this, groupBy.getValue(), groupBy.getGroupByFlavor(), lastUpdate);
    }

    @Override
    public <D extends RootOpenableDto, F extends DataGroupByDto<D>> F getGroupByDtoValue(Class<F> dtoType, DataGroupByParameter groupBy) {
        GroupBySpiContext<D, ?, ?, ?, ?, ?, F> spi = fromGroupByName(groupBy.getGroupByName());
        return spi.getGroupByDtoValue(this, groupBy.getValue(), groupBy.getGroupByFlavor());
    }

    @Override
    public <D extends RootOpenableDto> Form<D> loadForm(Class<D> dtoType, String id) {
        RootOpenableDtoEntityContext<D, ?, ?, ?, ?> spi = fromRootOpenableDto(dtoType);
        return spi.loadForm(this, id);
    }

    @Override
    public <D extends RootOpenableDto> D loadDto(Class<D> dtoType, String id) {
        RootOpenableDtoEntityContext<D, ?, ?, ?, ?> spi = fromRootOpenableDto(dtoType);
        return spi.loadEntityToDto(this, id);
    }

    @Override
    public <D extends RootOpenableDto> Form<D> preCreate(Class<D> dtoType, DataGroupByParameter groupBy) {
        GroupBySpiContext<D, ?, RootOpenableEntity, ?, ?, ?, ?> spi = fromGroupByName(groupBy.getGroupByName());
        return spi.preCreate(this, groupBy.getGroupByFlavor(), groupBy.getValue());
    }

    @Override
    public <D extends RootOpenableDto> boolean exists(Class<D> dtoType, String id) {
        RootOpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromRootOpenableDto(dtoType);
        return spi.existsEntity(this, id);
    }

    @Override
    public <D extends RootOpenableDto> SaveResultDto save(D dto) throws ConcurrentModificationException {
        @SuppressWarnings("unchecked") Class<D> dtoType = (Class<D>) dto.getClass();
        RootOpenableDtoEntityContext<D, ?, ?, ?, ?> spi = fromRootOpenableDto(dtoType);
        return spi.save(this, dto);
    }

    @Override
    public <D extends RootOpenableDto> void delete(Class<D> dtoType, String id) {
        RootOpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromRootOpenableDto(dtoType);
        spi.delete(this, id);
    }

    @Override
    public <D extends RootOpenableDto> void deleteLayout(Class<D> dtoType, DeleteLayoutRequest request) {
        RootOpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromRootOpenableDto(dtoType);
        spi.deleteLayout(this, request);
    }

    @Override
    public <D extends RootOpenableDto> void moveLayout(Class<D> dtoType, MoveLayoutRequest request) {
        RootOpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromRootOpenableDto(dtoType);
        spi.moveLayout(this, request);
    }

    @Override
    public <D extends RootOpenableDto> MissingReferentialResult computeMissingReferential(Class<D> dtoType, MissingReferentialRequest request) {
        RootOpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromRootOpenableDto(dtoType);
        return getTopiaApplicationContext().computeMissingReferential(this, spi.toEntityType(), request);
    }

    @Override
    public <D extends RootOpenableDto> void updateQualitativeProperty(Class<D> dtoType, DataGroupByParameter groupBy, Set<String> ids) {
        RootOpenableDtoEntityContext<?, ?, ?, ?, ?> spi = fromRootOpenableDto(dtoType);
        spi.updateQualitativeProperty(this, groupBy, ids);
    }
}

