package fr.ird.observe.services.local.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.data.SimpleService;
import fr.ird.observe.spi.context.SimpleDtoEntityContext;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SimpleServiceLocalSupport extends ObserveServiceLocal implements SimpleService {

    @Override
    public <D extends SimpleDto> Form<D> loadForm(Class<D> dtoType, String id) {
        SimpleDtoEntityContext<D, ?, ?, ?> spi = fromSimpleDto(dtoType);
        return spi.loadForm(this, id);
    }

    @Override
    public <D extends SimpleDto> SaveResultDto save(D dto) throws ConcurrentModificationException {
        @SuppressWarnings("unchecked") Class<D> type = (Class<D>) dto.getClass();
        SimpleDtoEntityContext<D, ?, ?, ?> spi = fromSimpleDto(type);
        return spi.save(this, dto);
    }
}
