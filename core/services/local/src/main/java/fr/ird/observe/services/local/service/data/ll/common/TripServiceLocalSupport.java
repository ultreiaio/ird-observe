package fr.ird.observe.services.local.service.data.ll.common;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.data.ll.logbook.SetDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.entities.data.ll.common.Trip;
import fr.ird.observe.entities.data.ll.logbook.Sample;
import fr.ird.observe.entities.data.ll.observation.Branchline;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.data.MoveRequest;
import fr.ird.observe.services.service.data.ll.common.TripService;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class TripServiceLocalSupport extends ObserveServiceLocal implements TripService {

    @Override
    public int getMatchingTripsVesselWithinDateRange(String tripId, String vesselId, Date startDate, Date endDate) {
        return Trip.SPI.getMatchingTripsVesselWithinDateRange(this, tripId, vesselId, startDate, endDate);
    }

    @Override
    public java.util.Set<String> getAllTripIds() {
        return new LinkedHashSet<>(Trip.SPI.getDao(getTopiaPersistenceContext()).findAllIds());
    }

    @Override
    public TripMapDto getTripMap(TripMapConfigDto config) {
        return Trip.SPI.getTripMap(this, config);
    }

    public ReferentialDtoReferenceSet<SpeciesReference> getSpeciesByListAndTrip(String tripId, String speciesListId) {
        return Trip.SPI.getSpeciesByListAndTrip(this, tripId, speciesListId);
    }

    @Override
    public Form<BranchlineDto> loadBranchlineForm(String id) {
        return Branchline.SPI.loadForm(this, id);
    }

    @Override
    public SaveResultDto saveBranchline(BranchlineDto dto) throws ConcurrentModificationException {
        return Branchline.SPI.save(this, dto);
    }

    @Override
    public List<ToolkitIdLabel> getSampleActivityParentCandidate(String tripId, String activityId) {
        return Trip.SPI.getSampleActivityParentCandidate(this, tripId, activityId);
    }

    @Override
    public java.util.Set<String> moveActivitySample(MoveRequest request) throws ConcurrentModificationException {
        return Sample.SPI.move(this, request);
    }

    @Override
    public List<String> getLogbookCatchSpeciesIds(String tripId) {
        return Trip.SPI.getLogbookCatchSpeciesIds(this, tripId);
    }

    @Override
    public SaveResultDto saveAndCopyProperties(String activityId, String setToCopyId, SetDto dto) throws ConcurrentModificationException {
        return fr.ird.observe.entities.data.ll.logbook.Set.SPI.saveAndCopyProperties(this, activityId, setToCopyId, dto);
    }
}
