package fr.ird.observe.services.local.service.data.ps;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.pairing.ApplyPairingRequest;
import fr.ird.observe.dto.data.ps.pairing.RoutePairingDto;
import fr.ird.observe.dto.data.ps.pairing.TripActivitiesPairingDto;
import fr.ird.observe.dto.data.ps.pairing.TripActivitiesPairingEngine;
import fr.ird.observe.dto.data.ps.pairing.TripActivitiesPairingResult;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.entities.data.ps.logbook.Activity;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.data.ps.ActivityPairingService;

/**
 * Created on 20/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ActivityPairingServiceLocalSupport extends ObserveServiceLocal implements ActivityPairingService {


    @Override
    public TripActivitiesPairingDto getTripActivitiesPairingDto(String tripId) {
        return Trip.SPI.getTripPairingDto(this, tripId);
    }

    @Override
    public RoutePairingDto getRoutePairingDto(String tripId, String logbookRouteId) {
        return Trip.SPI.getRoutePairingDto(this, tripId, logbookRouteId);
    }

    @Override
    public TripActivitiesPairingResult computePairing(String tripId) {
        TripActivitiesPairingDto data = getTripActivitiesPairingDto(tripId);
        if (data == null) {
            return null;
        }
        TripActivitiesPairingEngine pairingContext = new TripActivitiesPairingEngine(data);
        return pairingContext.computeResult();
    }

    @Override
    public void applyPairing(ApplyPairingRequest activityMapping) {
        Activity.SPI.applyPairing(getTopiaPersistenceContext(), timestampNow(), activityMapping.getMapping());
    }
}
