package fr.ird.observe.services.local.service.data.ps;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.data.ps.AvdthDataImportConfiguration;
import fr.ird.observe.services.service.data.ps.AvdthDataImportResult;
import fr.ird.observe.services.service.data.ps.MissingReferentialException;
import fr.ird.observe.persistence.avdth.data.ImportEngine;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.data.ps.AvdthService;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created on 21/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class AvdthServiceLocalSupport extends ObserveServiceLocal implements AvdthService {

    @Override
    public AvdthDataImportResult importData(AvdthDataImportConfiguration configuration) throws MissingReferentialException {
        try {
            return ImportEngine.run(configuration, getTopiaApplicationContext());
        } catch (IOException | SQLException e) {
            throw new IllegalStateException("Something dark happened with database: " + configuration, e);
        }
    }
}
