package fr.ird.observe.services.local.service.data.ps;

/*
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.consolidation.data.ps.common.TripConsolidateEngine;
import fr.ird.observe.consolidation.data.ps.common.TripConsolidateRequest;
import fr.ird.observe.consolidation.data.ps.common.TripConsolidateResult;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeManager;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeManagerFactory;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeSpecializedRules;
import fr.ird.observe.consolidation.data.ps.localmarket.BatchConsolidateEngine;
import fr.ird.observe.consolidation.data.ps.localmarket.BatchConsolidateRequest;
import fr.ird.observe.consolidation.data.ps.localmarket.GetOptionalRtpMeanWeightImpl;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.entities.referential.common.LengthWeightParameterCache;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.data.ps.ConsolidateDataService;

import java.util.Optional;

/**
 * Created on 28/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ConsolidateDataServiceLocalSupport extends ObserveServiceLocal implements ConsolidateDataService {

    @Override
    public TripConsolidateResult consolidateTrip(SimplifiedObjectTypeSpecializedRules rules, TripConsolidateRequest request) {
        SimplifiedObjectTypeManager simplifiedObjectTypeManager = newSimplifiedObjectTypeManager0(rules);
        TripConsolidateEngine engine = new TripConsolidateEngine(this, simplifiedObjectTypeManager, request);
        return engine.consolidate(request).orElse(null);
    }

    @Override
    public ToolkitIdModifications consolidateLocalmarketBatch(BatchConsolidateRequest request) {
        LengthWeightParameterCache lengthWeightParameterCache = new LengthWeightParameterCache(getApplicationLocale(), getDecoratorService(), getTopiaPersistenceContext());
        BatchConsolidateEngine engine = new BatchConsolidateEngine(new GetOptionalRtpMeanWeightImpl(this, lengthWeightParameterCache), getDecoratorService());
        Optional<ToolkitIdModifications> result = engine.consolidate(request);
        return result.orElse(null);
    }

    @Override
    public SimplifiedObjectTypeManager newSimplifiedObjectTypeManager(SimplifiedObjectTypeSpecializedRules rules) {
        return newSimplifiedObjectTypeManager0(rules);
    }

    private SimplifiedObjectTypeManager newSimplifiedObjectTypeManager0(SimplifiedObjectTypeSpecializedRules rules) {
        return SimplifiedObjectTypeManagerFactory.create(getTopiaPersistenceContext(), rules);
    }

}
