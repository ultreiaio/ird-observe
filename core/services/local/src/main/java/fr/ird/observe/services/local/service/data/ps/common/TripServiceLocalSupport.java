package fr.ird.observe.services.local.service.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.data.ps.common.TripLocalmarketDto;
import fr.ird.observe.dto.data.ps.common.TripLogbookDto;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.data.ps.common.TripService;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class TripServiceLocalSupport extends ObserveServiceLocal implements TripService {

    @Override
    public int getMatchingTripsVesselWithinDateRange(String tripId, String vesselId, Date startDate, Date endDate) {
        return Trip.SPI.getMatchingTripsVesselWithinDateRange(this, tripId, vesselId, startDate, endDate);
    }

    @Override
    public ReferentialDtoReferenceSet<SpeciesReference> getSpeciesByListAndTrip(String tripId, String speciesListId) {
        return Trip.SPI.getSpeciesByListAndTrip(this, tripId, speciesListId);
    }

    @Override
    public java.util.Set<String> getAllTripIds() {
        return new LinkedHashSet<>(Trip.SPI.getDao(getTopiaPersistenceContext()).findAllIds());
    }

    @Override
    public TripMapDto getTripMap(TripMapConfigDto config) {
        return Trip.SPI.getTripMap(this, config);
    }

    @Override
    public Form<TripLogbookDto> loadLogbookForm(String tripId) {
        return Trip.TRIP_LOGBOOK_SPI.loadForm(this, tripId);
    }

    @Override
    public Form<TripLocalmarketDto> loadLocalmarketForm(String tripId) {
        return Trip.TRIP_LOCALMARKET_SPI.loadForm(this, tripId);
    }

    @Override
    public List<ActivityStubDto> getLogbookWellPlanActivities(String tripId) {
        return Trip.SPI.getLogbookWellPlanActivities(this, tripId);
    }

    @Override
    public Set<String> getLogbookWellIdsFromWellPlan(String tripId) {
        return Trip.SPI.getLogbookWellIdsFromWellPlan(this, tripId);
    }

    @Override
    public Set<String> createLogbookSampleActivityFromWellPlan(String tripId, String wellId) {
        return Trip.SPI.createLogbookSampleActivityFromWellPlan(this, tripId, wellId);
    }

    @Override
    public Set<String> createLogbookWellActivityFromSample(String tripId, String wellId) {
        return Trip.SPI.createLogbookWellActivityFromSample(this, tripId, wellId);
    }

    @Override
    public Set<String> getLogbookWellIdsFromSample(String tripId) {
        return Trip.SPI.getLogbookWellIdsFromSample(this, tripId);
    }

    @Override
    public boolean isActivityEndOfSearchFound(String routeId) {
        return Trip.SPI.isActivityEndOfSearchFound(this, routeId);
    }

    @Override
    public boolean isActivitiesAcquisitionModeByTimeEnabled(String tripId) {
        return Trip.SPI.isActivitiesAcquisitionModeByTimeEnabled(this, tripId);
    }

    @Override
    public Form<fr.ird.observe.dto.data.ps.observation.FloatingObjectDto> preCreateObservationFloatingObject(String activityId, FloatingObjectPreset floatingObjectPreset) {
        return fr.ird.observe.entities.data.ps.observation.FloatingObject.SPI.preCreate(this, floatingObjectPreset);
    }

    @Override
    public Form<fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto> preCreateLogbookFloatingObject(String activityId, FloatingObjectPreset floatingObjectPreset) {
        return fr.ird.observe.entities.data.ps.logbook.FloatingObject.SPI.preCreate(this, floatingObjectPreset);
    }
}
