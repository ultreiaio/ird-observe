package fr.ird.observe.services.local.service.referential;

/*
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferenceSetsRequest;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.referential.ReferentialIds;
import fr.ird.observe.services.service.referential.ReferentialService;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.util.sql.SqlScript;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
class ReferentialServiceLocalSupport extends ObserveServiceLocal implements ReferentialService {

    @Override
    public <R extends ReferentialDtoReference> ReferentialDtoReferenceSet<R> getReferenceSet(Class<R> type, Date lastUpdateDate) {
        ReferentialDtoEntityContext<?, R, ?, ?> spi = fromReferentialReference(type);
        return spi.getReferenceSet0(this, lastUpdateDate);
    }

    @Override
    public <D extends BusinessDto> Set<ReferentialDtoReferenceSet<?>> getReferentialReferenceSets(ReferenceSetsRequest<D> request) {
        return getTopiaPersistenceContext().getReferentialReferenceSets(this, request, ObserveBusinessProject.get());
    }

    @Override
    public <D extends ReferentialDto> D loadDto(Class<D> type, String id) {
        ReferentialDtoEntityContext<D, ?, ?, ?> spi = fromReferentialDto(Objects.requireNonNull(type));
        return spi.loadEntityToDto(this, id);
    }

    @Override
    public <D extends ReferentialDto> Set<D> loadDtoList(Class<D> type) {
        ReferentialDtoEntityContext<D, ?, ?, ?> spi = fromReferentialDto(Objects.requireNonNull(type));
        return spi.loadDtoList(this);
    }

    @Override
    public <D extends ReferentialDto> Form<D> loadForm(Class<D> type, String id) {
        ReferentialDtoEntityContext<D, ?, ?, ?> spi = fromReferentialDto(type);
        return spi.loadForm(this, id);
    }

    @Override
    public <D extends ReferentialDto> Form<D> preCreate(Class<D> type) {
        ReferentialDtoEntityContext<D, ?, ?, ?> spi = fromReferentialDto(type);
        return spi.preCreate(getReferentialLocale());
    }

    @Override
    public <D extends ReferentialDto> SaveResultDto save(D bean) throws ConcurrentModificationException {
        @SuppressWarnings("unchecked") Class<D> type = (Class<D>) bean.getClass();
        ReferentialDtoEntityContext<D, ?, ?, ?> spi = fromReferentialDto(type);
        return spi.save(this, bean);
    }

    @Override
    public <D extends ReferentialDto> void delete(Class<D> type, String id) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(type);
        spi.delete(this, id);
    }

    @Override
    public <D extends ReferentialDto> void replaceReference(Class<D> type, String idToReplace, String replaceId) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(type);
        spi.replaceReference(this, idToReplace, replaceId, true, true);
    }

    @Override
    public <D extends ReferentialDto> void replaceReferenceInData(Class<D> type, String idToReplace, String replaceId) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(type);
        spi.replaceReference(this, idToReplace, replaceId, false, true);
    }

    @Override
    public <D extends ReferentialDto> void changeId(Class<D> type, String id, String newId) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(type);
        spi.changeId(this, id, newId);
    }

    @Override
    public <D extends ReferentialDto> Set<String> loadIds(Class<D> type) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(type);
        return new LinkedHashSet<>(spi.getDao(this).findAllIds());
    }

    @Override
    public <D extends ReferentialDto> boolean exists(Class<D> type, String id) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(type);
        return spi.existsEntity(this, id);
    }

    @Override
    public ReferentialIds getReferentialIds() {
        return getTopiaPersistenceContext().getReferentialIds(ObserveBusinessProject.get());
    }

    @Override
    public void insertMissingReferential(SqlScript sqlContent) {
        getTopiaPersistenceContext().executeSqlScript(sqlContent);
    }

}
