package fr.ird.observe.services.local.service.referential;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.ObserveTopiaEntitySqlModelResource;
import fr.ird.observe.services.local.service.ObserveServiceLocal;
import fr.ird.observe.services.local.service.UsageServiceLocalSupport;
import fr.ird.observe.services.service.referential.SynchronizeService;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlRequest;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlResult;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptConsumer;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created on 15/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SynchronizeServiceLocalSupport extends ObserveServiceLocal implements SynchronizeService {

    @Override
    public Set<String> filterIdsUsed(Class<? extends ReferentialDto> dtoType, Set<String> ids) {
        Set<String> result = new LinkedHashSet<>();
        for (String id : ids) {
            ToolkitIdDtoBean request = ToolkitIdDtoBean.of(dtoType, id);
            Map<Class<? extends BusinessDto>, Long> usageCount = UsageServiceLocalSupport.countReferential(this, request);
            long count = usageCount.values().stream().reduce(Long::sum).orElse(0L);
            if (count > 0) {
                result.add(id);
            }
        }
        return result;
    }

    @Override
    public Set<ToolkitIdLabel> getReferentialToDelete(Class<? extends ReferentialDto> dtoType, Set<String> ids) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(dtoType);
        return spi.getReferentialToDelete(this, ids);
    }

    @Override
    public <D extends ReferentialDto> Set<ToolkitIdLabel> getEnabledReferentialLabelSet(Class<D> type) {
        ReferentialDtoEntityContext<?, ?, ?, ?> spi = fromReferentialDto(type);
        return spi.getEnabledReferentialLabelSet(this);
    }

    @Override
    public OneSideSqlResult produceSqlResult(OneSideSqlRequest request) {
        return ObserveTopiaEntitySqlModelResource.get()
                .newOneSideSqlResultBuilder(this, now())
                .build(request);
    }

    @Override
    public void applySql(SqlScript script, String lastUpdateDateKey) {
        if (script != null) {
            getTopiaPersistenceContext().executeSqlScript(SqlScriptConsumer.of(script));
        }
        getTopiaPersistenceContext().updateReferentialLastUpdateDates(lastUpdateDateKey, now());
    }
}
