package fr.ird.observe.services.local;

/*
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceValidationMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.ObserveServiceInitializerConfig;
import fr.ird.observe.services.service.AnonymousService;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestClassResourceSupport;
import fr.ird.observe.test.spi.DatabaseClassifier;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.Description;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.Locale;

/**
 * Created on 18/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LocalTestClassResource extends TestClassResourceSupport {

    private static final Logger log = LogManager.getLogger(LocalTestClassResource.class);

    private final ObserveServiceFactoryLocal serviceFactory;

    ObserveServiceInitializerConfig serviceInitializerConfig;
    private boolean generateNow;

    public LocalTestClassResource() {
        this(DatabaseClassifier.DEFAULT);
    }

    public LocalTestClassResource(DatabaseClassifier classifier) {
        super(classifier);
        this.serviceFactory = new ObserveServiceFactoryLocal() {

            @Override
            protected ObserveServiceContextLocal createServiceContext(ObserveServiceInitializer observeServiceInitializer) {
                return new ObserveServiceContextLocal(observeServiceInitializer, this);
            }
        };
    }

    public ObserveServiceInitializerConfig getServiceInitializerConfig() {
        if (serviceInitializerConfig == null) {
            serviceInitializerConfig = new ObserveServiceInitializerConfig(
                    Locale.FRANCE,
                    ReferentialLocale.FR,
                    temporaryDirectoryRoot.toFile(),
                    30 * 1000, getDbVersion(), Version.create("9.0.0").build() /*FIXME Use from config */);
            serviceInitializerConfig.setValidationMode(DataSourceValidationMode.NONE);
        }
        return serviceInitializerConfig;
    }

    public ObserveDataSourceConfigurationTopiaH2 createDataSourceConfiguration(Version dbVersion, String dbName, File targetPath, String login, char[] password) throws DataSourceCreateWithNoReferentialImportException, IOException, IncompatibleDataSourceCreateConfigurationException, DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        ObserveDataSourceConfigurationTopiaH2 sharedDatabaseConfiguration = getDataSourcesForTestManager().createSharedDataSourceConfigurationH2(dbVersion, dbName, login, password);
        File sharedDatabaseFile = sharedDatabaseConfiguration.getDatabaseFile();
        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration;
        boolean sharedDatabaseExist = sharedDatabaseFile.exists();
        if (!sharedDatabaseExist) {
            log.info(String.format("Create shared database: %s/%s to %s", dbVersion.toString(), dbName, sharedDatabaseFile));
            sharedDatabaseConfiguration.setTemporaryDirectory(getTemporaryDirectoryRoot());
            AnonymousService service = newService(sharedDatabaseConfiguration, AnonymousService.class);
            ObserveDataSourceConnection connection = null;
            try {
                SqlScript script = getDataSourcesForTestManager().getCache(dbVersion, dbName);
                connection = service.createFromDump(sharedDatabaseConfiguration, script);
            } finally {
                if (connection != null) {
                    newService(connection, DataSourceService.class).close();
                }
            }
        }
        if (targetPath == null) {
            dataSourceConfiguration = sharedDatabaseConfiguration;
        } else {
            // Use a copy
            dataSourceConfiguration = getDataSourcesForTestManager().createDataSourceConfigurationH2(targetPath, dbVersion, dbName, login, password);
            File databaseFileTarget = dataSourceConfiguration.getDatabaseFile();
            log.info(String.format("Copy database: %s/%s to %s", dbVersion.toString(), dbName, databaseFileTarget));
            Files.createDirectories(databaseFileTarget.toPath().getParent());
            Files.copy(sharedDatabaseFile.toPath(), databaseFileTarget.toPath());
        }
        dataSourceConfiguration.setModelVersion(ObserveTestConfiguration.getModelVersion());
        dataSourceConfiguration.setTemporaryDirectory(getTemporaryDirectoryRoot());
        return dataSourceConfiguration;
    }

    public <S extends ObserveService> S newService(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {
        dataSourceConfiguration.setTemporaryDirectory(getTemporaryDirectoryRoot());
        ObserveServiceInitializer serviceInitializer = new ObserveServiceInitializer(getServiceInitializerConfig(), dataSourceConfiguration, null) {
            @Override
            public Date now() {
                return generateNow ? new Date() : ObserveFixtures.DATE;
            }
        };
        serviceInitializer.setConfiguration(dataSourceConfiguration);
        return serviceFactory.newService(serviceInitializer, serviceType);
    }

    public boolean isGenerateNow() {
        return generateNow;
    }

    public void setGenerateNow(boolean generateNow) {
        this.generateNow = generateNow;
    }

    public <S extends ObserveService> S newService(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {
        ObserveServiceInitializer serviceInitializer = new ObserveServiceInitializer(getServiceInitializerConfig(), null, dataSourceConnection) {
            @Override
            public Date now() {
                return generateNow ? new Date() : ObserveFixtures.DATE;
            }
        };
        return serviceFactory.newService(serviceInitializer, serviceType);
    }

    void closeServiceFactory() {
        serviceFactory.close();
    }

    protected void after(Description description) throws IOException {
        super.after(description);
        closeServiceFactory();
    }

}
