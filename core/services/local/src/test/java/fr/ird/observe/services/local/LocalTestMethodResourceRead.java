package fr.ird.observe.services.local;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.AnonymousService;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.test.spi.DatabaseClassifier;
import io.ultreia.java4all.util.Version;
import org.junit.runner.Description;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * Created on 18/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class LocalTestMethodResourceRead extends LocalTestClassResource {

    protected ObserveServicesProvider servicesProvider;
    private ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration;
    private ObserveDataSourceConnection dataSourceConnection;

    public LocalTestMethodResourceRead(DatabaseClassifier classifier) {
        super(classifier);
    }

    public LocalTestMethodResourceRead() {
        this(DatabaseClassifier.DEFAULT);
    }

    public <S extends ObserveService> S newService(Class<S> serviceType) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        ObserveDataSourceConnection dataSourceConnection = getDataSourceConnection();
        if (dataSourceConnection == null) {
            ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = getDataSourceConfiguration();
            dataSourceConnection = newService(dataSourceConfiguration, AnonymousService.class).open(dataSourceConfiguration);
            ObserveDataSourceInformation dataSourceInformation = new ObserveDataSourceInformation(
                    Permission.ALL,
                    true,
                    true,
                    Version.VZERO,
                    Version.VZERO,
                    List.of());
            // get all credentials on test data source
            dataSourceConnection = dataSourceConfiguration.toConnection(dataSourceConnection.getAuthenticationToken(), dataSourceInformation);
            setDataSourceConnection(dataSourceConnection);
        }
        return newService(dataSourceConnection, serviceType);
    }

    public ObserveDataSourceConfigurationTopiaH2 getDataSourceConfiguration() {
        return dataSourceConfiguration;
    }

    public ObserveDataSourceConnection getDataSourceConnection() {
        return dataSourceConnection;
    }

    public void setDataSourceConnection(ObserveDataSourceConnection dataSourceConnection) {
        this.dataSourceConnection = dataSourceConnection;
    }

    public ObserveTopiaApplicationContext getTopiaApplicationContext() {
        return ObserveTopiaApplicationContextFactory.createContext(dataSourceConfiguration);
    }

    @Override
    protected void before(Description description) throws Throwable {
        super.before(description);
        Objects.requireNonNull(getDbName(), "Pas de nom de base spécifié");
        Objects.requireNonNull(getDbVersion(), "Pas de version de base spécifié");
        Objects.requireNonNull(getLogin(), "Pas de login spécifié");
        Objects.requireNonNull(getPassword(), "Pas de password spécifié");

        initTemporaryDirectoryRoot("all");

        dataSourceConfiguration = createDataSourceConfiguration(getDbVersion(), getDbName(), null, getLogin(), getPassword());
        servicesProvider = new ObserveServicesProvider() {
            @Override
            public <S extends ObserveService> S getService(Class<S> serviceType) {
                try {
                    return newService(serviceType);
                } catch (Exception e) {
                    throw new IllegalStateException("Can't get service: " + serviceType, e);
                }
            }
        };

    }

    public ObserveServicesProvider getServicesProvider() {
        return servicesProvider;
    }

    @Override
    protected void after(Description description) throws IOException {
        super.after(description);
        if (dataSourceConnection != null) {
            dataSourceConnection = null;
            try {
                getTopiaApplicationContext().close();
            } finally {
                dataSourceConfiguration = null;
            }
        }
    }

}

