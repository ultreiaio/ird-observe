package fr.ird.observe.services.local;

/*
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.entities.ObserveTopiaPersistenceContext;
import fr.ird.observe.services.service.AnonymousService;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.services.service.referential.ObserveReferentialCache;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestMethodResourceSupportWrite;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseClassifier;
import io.ultreia.java4all.util.Version;
import org.junit.runner.Description;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

import java.io.File;
import java.util.List;
import java.util.Objects;


/**
 * Created on 18/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("unused")
public class LocalTestMethodResourceWrite extends TestMethodResourceSupportWrite<LocalTestClassResource> {

    private ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration;
    private ObserveDataSourceConnection dataSourceConnection;
    private ObserveReferentialCache referentialCache;

    public LocalTestMethodResourceWrite(LocalTestClassResource localTestClassResource) {
        super(localTestClassResource);
    }

    public <S extends ObserveService> S newService(Class<S> serviceType) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        ObserveDataSourceConnection dataSourceConnection = getDataSourceConnection();
        if (dataSourceConnection == null) {
            ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = getDataSourceConfiguration();
            dataSourceConnection = testClassResource.newService(dataSourceConfiguration, AnonymousService.class).open(dataSourceConfiguration);
            ObserveDataSourceInformation dataSourceInformation = new ObserveDataSourceInformation(
                    Permission.ALL,
                    true,
                    true,
                    Version.VZERO,
                    Version.VZERO,
                    List.of());
            // get all credentials on test data source
            dataSourceConnection = dataSourceConfiguration.toConnection(dataSourceConnection.getAuthenticationToken(), dataSourceInformation);
            setDataSourceConnection(dataSourceConnection);
        }
        return testClassResource.newService(dataSourceConnection, serviceType);
    }


    public ObserveDataSourceConfigurationTopiaH2 getDataSourceConfiguration() {
        return dataSourceConfiguration;
    }

    public ObserveDataSourceConnection getDataSourceConnection() {
        return dataSourceConnection;
    }

    public void setDataSourceConnection(ObserveDataSourceConnection dataSourceConnection) {
        this.dataSourceConnection = dataSourceConnection;
    }

    public ObserveReferentialCache getReferentialCache() {
        if (referentialCache == null) {
            referentialCache = new ObserveReferentialCache(ObserveBusinessProject.get(), new DecoratorService(ReferentialLocale.FR));
        }
        return referentialCache;
    }

    public ObserveTopiaPersistenceContext newPersistenceContext() {
        return getTopiaApplicationContext().newPersistenceContext();
    }

    public ObserveTopiaApplicationContext getTopiaApplicationContext() {
        return ObserveTopiaApplicationContextFactory.createContext(dataSourceConfiguration);
    }

    public <E extends TopiaEntity> E findById(Class<E> entityType, String id) {
        try (TopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            TopiaDao<E> dao = persistenceContext.getDao(entityType);
            return dao.forTopiaIdEquals(id).findUnique();
        }
    }

    public <E extends TopiaEntity> boolean exists(Class<E> entityType, String id) {
        try (TopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            TopiaDao<E> dao = persistenceContext.getDao(entityType);
            return dao.forTopiaIdEquals(id).exists();
        }
    }

    @Override
    protected void before(Description description) throws Throwable {
        super.before(description);
        Objects.requireNonNull(getDbName(), "Pas de nom de base spécifié");
        Objects.requireNonNull(getDbVersion(), "Pas de version de base spécifié");
        Objects.requireNonNull(getLogin(), "Pas de login spécifié");
        Objects.requireNonNull(getPassword(), "Pas de password spécifié");
        DatabaseClassifier classifier = testClassResource.getClassifier();
        CopyDatabaseConfiguration copyDatabaseConfiguration = ObserveTestConfiguration.getCopyDatabaseConfigurationAnnotation(testClassMethod, classifier);
        boolean useSharedDatabase = copyDatabaseConfiguration == null;
        File databasePath = useSharedDatabase ? null : getTestDirectory().toPath().resolve("localDb-" + classifier).toFile();
        dataSourceConfiguration = testClassResource.createDataSourceConfiguration(getDbVersion(), getDbName(), databasePath, getLogin(), getPassword());
    }

    @Override
    protected void after(Description description) {
        super.after(description);
        if (referentialCache != null) {
            referentialCache.close();
        }
        testClassResource.closeServiceFactory();
    }

}
