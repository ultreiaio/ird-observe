package fr.ird.observe.services.local.service;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.Report;
import fr.ird.observe.services.service.ReportServiceFixtures;
import org.junit.Test;

import java.util.Map;

public class ReportServiceLocalReadTest extends GeneratedReportServiceLocalReadTest {

    @Test
    public void loadDefault() {
        Map<String, Report> reports = ReportServiceFixtures.getReports();
        fixtures.assertReports(reports);
    }

}
