package fr.ird.observe.services.local.service;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.local.LocalTestClassResource;
import fr.ird.observe.services.local.LocalTestMethodResourceWrite;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.services.service.referential.ReferentialService;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestSupportWithConfig;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import io.ultreia.java4all.http.HRestApiService;
import io.ultreia.java4all.http.spi.Internal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;

/**
 * Created on 26/12/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("unused")
@DatabaseVersionConfiguration(ObserveTestConfiguration.MODEL_VERSION)
@DatabaseLoginConfiguration
@DatabasePasswordConfiguration(ObserveTestConfiguration.H2_PASSWORD)
@DatabaseNameConfiguration(DatabaseName.data)
@Internal // mark internal to not get it in any http plugin
public abstract class ServiceLocalTestSupportWrite<S extends ObserveService> extends TestSupportWithConfig implements HRestApiService<S> {

    @ClassRule
    public static final LocalTestClassResource TOPIA_TEST_CLASS_RESOURCE = new LocalTestClassResource();
    @Rule
    public final LocalTestMethodResourceWrite localTestMethodResource = new LocalTestMethodResourceWrite(TOPIA_TEST_CLASS_RESOURCE);
    private final Class<S> serviceType;
    protected S service;
    private ObserveServicesProvider servicesProvider;

    public ServiceLocalTestSupportWrite(Class<S> serviceType) {
        this.serviceType = serviceType;
    }

    @Before
    public void setUp() throws Exception {
        servicesProvider = new ObserveServicesProvider() {
            @Override
            public <SS extends ObserveService> SS getService(Class<SS> serviceType) {
                try {
                    return localTestMethodResource.newService(serviceType);
                } catch (Exception e) {
                    throw new IllegalStateException("Can't get service: " + serviceType, e);
                }
            }
        };
        service = localTestMethodResource.newService(serviceType);
    }

    @Override
    public S getService() {
        return service;
    }

    public ObserveServicesProvider getServicesProvider() {
        return servicesProvider;
    }

    protected void loadReferenceSets(ReferentialService referentialService, Form<?> form) {
        localTestMethodResource.getReferentialCache().loadReferenceSets(referentialService, form.getType());
    }

    protected <D extends ReferentialDto, R extends ReferentialDtoReference> R getReference(Class<D> type, int index) throws DatabaseNotFoundException, BabModelVersionException, DatabaseConnexionNotAuthorizedException {
        ReferentialDtoEntityContext<D, R, ?, ?> dtoEntityContext = ObservePersistenceBusinessProject.fromReferentialDto(type);
        Class<R> referenceType = dtoEntityContext.toReferenceType();
        ReferentialService referentialService = localTestMethodResource.newService(ReferentialService.class);
        ReferentialDtoReferenceSet<R> referentialReferenceSet = localTestMethodResource.getReferentialCache().getReferentialReferenceSet(referentialService, referenceType);
        return referentialReferenceSet.getReferenceByPosition(index);
    }

    protected void assertEntityEqualsReferenceDto(Entity entity, ReferentialDtoReference referenceDto) {
        if (entity == null) {
            Assert.assertNull(referenceDto);
        } else {
            Assert.assertEquals(entity.getTopiaId(), referenceDto.getId());
        }
    }

    protected void assertReferenceDtoEqualsEntity(ReferentialDtoReference referenceDto, Entity entity) {
        if (referenceDto == null) {
            Assert.assertNull(entity);
        } else {
            Assert.assertEquals(referenceDto.getId(), entity.getTopiaId());
        }
    }
}
