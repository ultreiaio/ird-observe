package fr.ird.observe.services.local.service.api;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceValidationMode;
import fr.ird.observe.services.service.api.DataEntityService;
import org.junit.Test;

public class DataEntityServiceLocalReadTest extends GeneratedDataEntityServiceLocalReadTest {

    @Override
    public void setUp() throws Exception {
        localTestMethodResource.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.STRONG);
        super.setUp();
    }

    @Test
    public void getPsTrip() {
        DataEntityService service = getService();
        fixtures.getPsTrip(service);
    }

    @Test
    public void getLlTrip() {
        DataEntityService service = getService();
        fixtures.getLlTrip(service);
    }

}
