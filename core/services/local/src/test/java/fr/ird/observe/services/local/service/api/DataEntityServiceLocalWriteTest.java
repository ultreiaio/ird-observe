package fr.ird.observe.services.local.service.api;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceValidationMode;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.entities.data.ps.common.Trip;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.services.service.api.InvalidDataException;
import fr.ird.observe.services.service.data.ps.common.TripService;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import io.ultreia.java4all.validation.impl.java.NuitonValidatorProviderFactoryImpl;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DataEntityServiceLocalWriteTest extends GeneratedDataEntityServiceLocalWriteTest {
    @BeforeClass
    public static void beforeClass() {
        NuitonValidatorProviders.setDefaultFactoryName(NuitonValidatorProviderFactoryImpl.PROVIDER_NAME);
    }

    @Override
    public void setUp() throws Exception {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.STRONG);
        super.setUp();
    }

    @Override
    @CopyDatabaseConfiguration
    public void create() {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        super.create();
    }

    @Override
    @CopyDatabaseConfiguration
    public void update() {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        super.update();
    }

    @Override
    public void delete() {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        super.delete();
    }

    @Test
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(DatabaseName.referential)
    public void createPsTrip() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        {
            List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                            fr.ird.observe.dto.data.ps.common.TripDto.class, "gearUseFeatures", 1);
            Assert.assertNotNull(data);
        }
        {
            List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                            fr.ird.observe.dto.data.ps.common.TripDto.class, "routeObs", 2);
            Assert.assertNotNull(data);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(DatabaseName.referential)
    public void createPsTripGearUseFeatures() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        {
            List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                            fr.ird.observe.dto.data.ps.common.TripDto.class, "gearUseFeatures", 1);
            Assert.assertNotNull(data);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(DatabaseName.referential)
    public void createPsTripRouteObs() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        {
            List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                            fr.ird.observe.dto.data.ps.common.TripDto.class, "routeObs", 2);
            Assert.assertNotNull(data);
        }
    }


    @Test
    @CopyDatabaseConfiguration
    public void createPsTrip2() throws InvalidDataException {
        DataSourceValidationMode validationMode = TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().getValidationMode();
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        try {
            {
                List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                                fr.ird.observe.dto.data.ps.common.TripDto.class, "routeLogbook", 1);
                Assert.assertNotNull(data);
            }
            {
                Map<String, Object> data = assertCreateTrip0(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                                             fr.ird.observe.dto.data.ps.common.TripDto.class, "all");
                Assert.assertNotNull(data);
            }
        } finally {
            TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(validationMode);
        }
    }


    @Test
    @CopyDatabaseConfiguration
    public void createPsTripRouteLogbook() throws InvalidDataException {
        DataSourceValidationMode validationMode = TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().getValidationMode();
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        try {
            {
                List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                                fr.ird.observe.dto.data.ps.common.TripDto.class, "routeLogbook", 1);
                Assert.assertNotNull(data);
            }
        } finally {
            TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(validationMode);
        }
    }


    @Test
    @CopyDatabaseConfiguration
    public void createPsTripAll() throws InvalidDataException {
        DataSourceValidationMode validationMode = TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().getValidationMode();
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        try {
            {
                Map<String, Object> data = assertCreateTrip0(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                                             fr.ird.observe.dto.data.ps.common.TripDto.class, "all");
                Assert.assertNotNull(data);
            }
        } finally {
            TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(validationMode);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(DatabaseName.referential)
    public void createPsTrip_2196() throws InvalidDataException {
        String classifier = "issue2196";
        Map<?, ?> tripMap = assertCreateTrip0(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                              fr.ird.observe.dto.data.ps.common.TripDto.class, classifier);

        Assert.assertNotNull(tripMap);
        String id = (String) tripMap.get(ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID);
        Assert.assertNotNull(id);

        Map<String, Object> resultObject = assertUpdateTrip0(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                                             fr.ird.observe.dto.data.ps.common.TripDto.class, id, classifier);
        Assert.assertNotNull(resultObject);
    }

    @Test
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(DatabaseName.referential)
    public void createPsTrip_2285() throws InvalidDataException {
        String classifier = "issue2285";
        Map<?, ?> tripMap = assertCreateTrip0(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                              fr.ird.observe.dto.data.ps.common.TripDto.class, classifier);

        Assert.assertNotNull(tripMap);
        String id = (String) tripMap.get(ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID);
        Assert.assertNotNull(id);

        Map<String, Object> resultObject = assertUpdateTrip0(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                                             fr.ird.observe.dto.data.ps.common.TripDto.class, id, classifier);
        Assert.assertNotNull(resultObject);
    }

    @Test(expected = InvalidDataException.class)
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(DatabaseName.referential)
    public void createPsTrip_2196NotValid() throws InvalidDataException {
        String classifier = "issue2196-notValid";
        assertCreateTrip0(fr.ird.observe.services.service.data.ps.common.TripService.class,
                          fr.ird.observe.dto.data.ps.common.TripDto.class, classifier);

    }

    @Test
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(DatabaseName.referential)
    public void updatePsTrip() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        Map<?, ?> tripMap;
        {
            tripMap = assertCreateTrip0(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                        fr.ird.observe.dto.data.ps.common.TripDto.class, "gearUseFeatures");
            Assert.assertNotNull(tripMap);
            Assert.assertNotNull(tripMap.get(Trip.PROPERTY_GEAR_USE_FEATURES));
            Assert.assertEquals(1, ((Collection<?>) tripMap.get(Trip.PROPERTY_GEAR_USE_FEATURES)).size());
            Assert.assertNotNull(tripMap.get(Trip.PROPERTY_ROUTE_OBS));
            Assert.assertEquals(0, ((Collection<?>) tripMap.get(Trip.PROPERTY_ROUTE_OBS)).size());
            Assert.assertNotNull(tripMap.get(Trip.PROPERTY_ROUTE_LOGBOOK));
            Assert.assertEquals(0, ((Collection<?>) tripMap.get(Trip.PROPERTY_ROUTE_LOGBOOK)).size());
        }
        Objects.requireNonNull(tripMap);
        String tripId = (String) tripMap.get(ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID);
        int exceptedRouteObsCount = 2;
        int exceptedRouteLogbookCount = 1;
        {
            Map<String, Object> data = assertUpdateTrip(TripService.class,
                                                        TripDto.class, Objects.requireNonNull(tripId), Trip.PROPERTY_ROUTE_OBS, exceptedRouteObsCount);
            Assert.assertNotNull(data);
            Assert.assertNotNull(data.get(Trip.PROPERTY_GEAR_USE_FEATURES));
            Assert.assertEquals(0, ((Collection<?>) data.get(Trip.PROPERTY_GEAR_USE_FEATURES)).size());
            Assert.assertNotNull(data.get(Trip.PROPERTY_ROUTE_OBS));
            Assert.assertEquals(exceptedRouteObsCount, ((Collection<?>) data.get(Trip.PROPERTY_ROUTE_OBS)).size());
            Assert.assertNotNull(data.get(Trip.PROPERTY_ROUTE_LOGBOOK));
            Assert.assertEquals(0, ((Collection<?>) data.get(Trip.PROPERTY_ROUTE_LOGBOOK)).size());
        }
        {
            Map<String, Object> data = assertUpdateTrip(TripService.class,
                                                        TripDto.class, Objects.requireNonNull(tripId), Trip.PROPERTY_ROUTE_OBS, exceptedRouteObsCount);
            Assert.assertNotNull(data);
            Assert.assertNotNull(data.get(Trip.PROPERTY_GEAR_USE_FEATURES));
            Assert.assertEquals(0, ((Collection<?>) data.get(Trip.PROPERTY_GEAR_USE_FEATURES)).size());
            Assert.assertNotNull(data.get(Trip.PROPERTY_ROUTE_OBS));
            Assert.assertEquals(exceptedRouteObsCount, ((Collection<?>) data.get(Trip.PROPERTY_ROUTE_OBS)).size());
            Assert.assertNotNull(data.get(Trip.PROPERTY_ROUTE_LOGBOOK));
            Assert.assertEquals(0, ((Collection<?>) data.get(Trip.PROPERTY_ROUTE_LOGBOOK)).size());
        }
        {
            Map<String, Object> data = assertUpdateTrip(TripService.class,
                                                        TripDto.class, Objects.requireNonNull(tripId), Trip.PROPERTY_ROUTE_LOGBOOK, exceptedRouteLogbookCount);
            Assert.assertNotNull(data);
            Assert.assertNotNull(data.get(Trip.PROPERTY_GEAR_USE_FEATURES));
            Assert.assertEquals(0, ((Collection<?>) data.get(Trip.PROPERTY_GEAR_USE_FEATURES)).size());
            Assert.assertNotNull(data.get(Trip.PROPERTY_ROUTE_OBS));
            Assert.assertEquals(0, ((Collection<?>) data.get(Trip.PROPERTY_ROUTE_OBS)).size());
            Assert.assertNotNull(data.get(Trip.PROPERTY_ROUTE_LOGBOOK));
            Assert.assertEquals(exceptedRouteLogbookCount, ((Collection<?>) data.get(Trip.PROPERTY_ROUTE_LOGBOOK)).size());
        }
        //FIXME Add more test for update https://gitlab.com/ultreiaio/ird-observe/-/issues/2171
    }

    @Test
    @CopyDatabaseConfiguration
    public void createLlTrip() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        {
            List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ll.common.TripService.class,
                                            fr.ird.observe.dto.data.ll.common.TripDto.class, "gearUseFeatures", 1);
            Assert.assertNotNull(data);
        }
        {
            List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ll.common.TripService.class,
                                            fr.ird.observe.dto.data.ll.common.TripDto.class, "activityObs", 2);
            Assert.assertNotNull(data);
        }
        {
            List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ll.common.TripService.class,
                                            fr.ird.observe.dto.data.ll.common.TripDto.class, "activityLogbook", 2);
            Assert.assertNotNull(data);
        }
        {
            Map<String, Object> data = assertCreateTrip0(fr.ird.observe.services.service.data.ll.common.TripService.class,
                                                         fr.ird.observe.dto.data.ll.common.TripDto.class, "all");
            Assert.assertNotNull(data);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    public void createLlTrip_2886() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.STRONG);
        {
            Map<String, Object> data = assertCreateTrip0(fr.ird.observe.services.service.data.ll.common.TripService.class,
                                                         fr.ird.observe.dto.data.ll.common.TripDto.class, "issue2886");
            Assert.assertNotNull(data);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    public void createLlTripGearUseFeatures() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        {
            List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ll.common.TripService.class,
                                            fr.ird.observe.dto.data.ll.common.TripDto.class, "gearUseFeatures", 1);
            Assert.assertNotNull(data);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    public void createLlTripActivityObs() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        {
            List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ll.common.TripService.class,
                                            fr.ird.observe.dto.data.ll.common.TripDto.class, "activityObs", 2);
            Assert.assertNotNull(data);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    public void createLlTripActivityLogbook() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        {
            List<?> data = assertCreateTrip(fr.ird.observe.services.service.data.ll.common.TripService.class,
                                            fr.ird.observe.dto.data.ll.common.TripDto.class, "activityLogbook", 2);
            Assert.assertNotNull(data);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    public void createLlTripAll() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        {
            Map<String, Object> data = assertCreateTrip0(fr.ird.observe.services.service.data.ll.common.TripService.class,
                                                         fr.ird.observe.dto.data.ll.common.TripDto.class, "all");
            Assert.assertNotNull(data);
        }
    }

    @Test
    @CopyDatabaseConfiguration
    public void createPsTripLinks() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        Map<String, Object> data = assertCreateTrip0(fr.ird.observe.services.service.data.ps.common.TripService.class,
                                                     fr.ird.observe.dto.data.ps.common.TripDto.class, "links");
        Assert.assertNotNull(data);
    }

    @Test
    @CopyDatabaseConfiguration
    public void createLlTripLinks() throws InvalidDataException {
        TOPIA_TEST_CLASS_RESOURCE.getServiceInitializerConfig().setValidationMode(DataSourceValidationMode.NONE);
        Map<String, Object> data = assertCreateTrip0(fr.ird.observe.services.service.data.ll.common.TripService.class,
                                                     fr.ird.observe.dto.data.ll.common.TripDto.class, "links");
        Assert.assertNotNull(data);
    }

    protected List<?> assertCreateTrip(Class<?> serviceType, Class<? extends DataDto> dtoType, String classifier, int expectedCount) throws InvalidDataException {
        Map<String, Object> resultObject = assertCreateTrip0(serviceType, dtoType, classifier);
        Assert.assertNotNull(resultObject);

        List<?> data = (List<?>) resultObject.get(classifier);
        Assert.assertNotNull(data);
        Assert.assertEquals(expectedCount, data.size());
        return data;
    }

    protected Map<String, Object> assertCreateTrip0(Class<?> serviceType, Class<? extends DataDto> dtoType, String classifier) throws InvalidDataException {
        String json = fixtures.loadContent(serviceType, classifier);
        ToolkitTreeNodeStates result = fixtures.testCreate(service, dtoType, json);
        String content = result.getState("content");
        Assert.assertNotNull(content);
        @SuppressWarnings("unchecked") Map<String, Object> resultObject = (Map<String, Object>) fixtures.fromJsonList(content).get(0);
        Assert.assertNotNull(resultObject);
        return resultObject;

    }

    protected Map<String, Object> assertUpdateTrip(Class<?> serviceType, Class<? extends DataDto> dtoType, String id, String classifier, int expectedCount) throws InvalidDataException {
        Map<String, Object> resultObject = assertUpdateTrip0(serviceType, dtoType, id, classifier);
        Assert.assertNotNull(resultObject);

        List<?> data = (List<?>) resultObject.get(classifier);
        Assert.assertNotNull(data);
        Assert.assertEquals(expectedCount, data.size());
        return resultObject;
    }

    protected Map<String, Object> assertUpdateTrip0(Class<?> serviceType, Class<? extends DataDto> dtoType, String id, String classifier) throws InvalidDataException {
        String json = fixtures.loadContent(serviceType, classifier);
        ToolkitTreeNodeStates result = fixtures.testUpdate(service, dtoType, id, json);
        String content = result.getState("content");
        Assert.assertNotNull(content);
        @SuppressWarnings("unchecked") Map<String, Object> resultObject = (Map<String, Object>) fixtures.fromJsonList(content).get(0);
        Assert.assertNotNull(resultObject);
        return resultObject;

    }
}
