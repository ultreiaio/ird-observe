package fr.ird.observe.services.local.service.referential;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.services.local.LocalTestMethodResourceRead;
import fr.ird.observe.services.local.service.ServiceLocalTestSupportWrite;
import fr.ird.observe.services.service.DataSourceServiceFixtures;
import fr.ird.observe.services.service.referential.ReferentialService;
import fr.ird.observe.services.service.referential.differential.DifferentialList;
import fr.ird.observe.services.service.referential.differential.DifferentialType;
import fr.ird.observe.services.service.referential.differential.ObserveDifferentialMetaModel;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.spi.DatabaseClassifier;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import io.ultreia.java4all.util.LeftOrRightContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.4.0
 */
@DatabaseVersionConfiguration(ObserveTestConfiguration.MODEL_VERSION)
@DatabaseLoginConfiguration
@DatabasePasswordConfiguration(ObserveTestConfiguration.H2_PASSWORD)
@DatabaseNameConfiguration(value = DatabaseName.data, classifier = DatabaseClassifier.CENTRAL)
@DatabaseVersionConfiguration(value = ObserveTestConfiguration.MODEL_VERSION, classifier = DatabaseClassifier.CENTRAL)
public class DifferentialModelTest extends ServiceLocalTestSupportWrite<ReferentialService> {

    private static final Logger log = LogManager.getLogger(DifferentialModelTest.class);
    @Rule
    public final LocalTestMethodResourceRead localTestMethodResourceCentral = new LocalTestMethodResourceRead(DatabaseClassifier.CENTRAL);
    private ReferentialService rightReferentialService;

    public DifferentialModelTest() {
        super(ReferentialService.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        rightReferentialService = localTestMethodResourceCentral.newService(ReferentialService.class);
    }

    @DatabaseNameConfiguration(DatabaseName.empty)
    @Test
    public void testSynchronizeFromEmptyDatabase() {

        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.addPropertyChangeListener(ProgressionModel.PROPERTY_MESSAGE, evt -> log.debug(evt.getNewValue()));
        LeftOrRightContext<DifferentialList> result = ReferentialService.createDifferentialModelBuilder(ReferentialLocale.FR.getLocale(), ObserveDifferentialMetaModel::get, LeftOrRightContext.of(service, rightReferentialService)).build(progressionModel);

        Assert.assertNotNull(result);
        DifferentialList leftSideDifferentialList = result.left();
        Assert.assertNotNull(leftSideDifferentialList);
        Assert.assertNotNull(leftSideDifferentialList.getStates());
        Assert.assertEquals(0, leftSideDifferentialList.getStates().size());
        DifferentialList rightSideDifferentialList = result.right();
        Assert.assertNotNull(rightSideDifferentialList);
        Assert.assertNotNull(rightSideDifferentialList.getStates());
        Assert.assertEquals(DataSourceServiceFixtures.REFERENTIAL_COUNT, rightSideDifferentialList.getStates().size());
        Assert.assertTrue(rightSideDifferentialList.getStates().stream().allMatch(s -> DifferentialType.ADDED.equals(s.getDifferentialType())));
    }

    @DatabaseNameConfiguration(DatabaseName.data)
    @Test
    public void testSynchronizeWithNoChange() {

        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.addPropertyChangeListener(ProgressionModel.PROPERTY_MESSAGE, evt -> log.debug(evt.getNewValue()));
        LeftOrRightContext<DifferentialList> result = ReferentialService.createDifferentialModelBuilder(ReferentialLocale.FR.getLocale(), ObserveDifferentialMetaModel::get, LeftOrRightContext.of(service, rightReferentialService)).build(progressionModel);

        Assert.assertNotNull(result);
        DifferentialList leftSideDifferentialList = result.left();
        Assert.assertNotNull(leftSideDifferentialList);
        Assert.assertNotNull(leftSideDifferentialList.getStates());
        Assert.assertEquals(0, leftSideDifferentialList.getStates().size());
        DifferentialList rightSideDifferentialList = result.right();
        Assert.assertNotNull(rightSideDifferentialList);
        Assert.assertNotNull(rightSideDifferentialList.getStates());
        Assert.assertEquals(0, rightSideDifferentialList.getStates().size());
    }
}
