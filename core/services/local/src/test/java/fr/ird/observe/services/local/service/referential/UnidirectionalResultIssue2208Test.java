package fr.ird.observe.services.local.service.referential;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.OceanReference;
import fr.ird.observe.dto.referential.common.SpeciesDto;
import fr.ird.observe.services.local.LocalTestClassResource;
import fr.ird.observe.services.local.LocalTestMethodResourceWrite;
import fr.ird.observe.services.local.service.ServiceLocalTestSupportWrite;
import fr.ird.observe.services.service.referential.ReferentialService;
import fr.ird.observe.services.service.referential.ReferentialServiceFixtures;
import fr.ird.observe.services.service.referential.SynchronizeEngine;
import fr.ird.observe.services.service.referential.SynchronizeService;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeContext;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeEngine;
import fr.ird.observe.services.service.referential.differential.DifferentialModelBuilder;
import fr.ird.observe.services.service.referential.differential.ObserveDifferentialMetaModel;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalResult;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.ToolkitFixtures;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseClassifier;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import io.ultreia.java4all.util.LeftOrRightContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.Set;

/**
 * Created on 04/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@DatabaseVersionConfiguration(ObserveTestConfiguration.MODEL_VERSION)
@DatabaseLoginConfiguration
@DatabasePasswordConfiguration(ObserveTestConfiguration.H2_PASSWORD)
@DatabaseNameConfiguration(DatabaseName.referential)
@DatabaseNameConfiguration(value = DatabaseName.referential, classifier = DatabaseClassifier.CENTRAL)
@DatabaseVersionConfiguration(value = ObserveTestConfiguration.MODEL_VERSION, classifier = DatabaseClassifier.CENTRAL)
public class UnidirectionalResultIssue2208Test extends ServiceLocalTestSupportWrite<SynchronizeService> {

    @ClassRule
    public static final LocalTestClassResource TOPIA_TEST_CLASS_RESOURCE_CENTRAL = new LocalTestClassResource(DatabaseClassifier.CENTRAL);
    @Rule
    public final LocalTestMethodResourceWrite localTestMethodResourceCentral = new LocalTestMethodResourceWrite(TOPIA_TEST_CLASS_RESOURCE_CENTRAL);

    private DifferentialModelBuilder diffsEngine;
    private SynchronizeEngine synchronizeEngine;
    private ReferentialServiceFixtures fixtures;
    private ReferentialService centralReferentialService;
    private ReferentialService localReferentialService;

    public UnidirectionalResultIssue2208Test() {
        super(SynchronizeService.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        localReferentialService = localTestMethodResource.newService(ReferentialService.class);
        centralReferentialService = localTestMethodResourceCentral.newService(ReferentialService.class);
        diffsEngine = ReferentialService.createDifferentialModelBuilder(ReferentialLocale.FR.getLocale(), ObserveDifferentialMetaModel::get, LeftOrRightContext.of(localReferentialService, centralReferentialService));
        SynchronizeService centralService = localTestMethodResourceCentral.newService(SynchronizeService.class);
        synchronizeEngine = new SynchronizeEngine(localTestMethodResource.getTestDirectory().toPath(), LeftOrRightContext.of(service, centralService));
        fixtures = new ReferentialServiceFixtures();
    }

    @CopyDatabaseConfiguration
    @CopyDatabaseConfiguration(classifier = DatabaseClassifier.CENTRAL)
    @Test
    public void testSynchronizeIssue2208() throws ConcurrentModificationException {

        // add ocean to a species in central database
        // synchronize
        // check in local database that the ocean is there for the given species

        String speciesVariableName = fixtures.getVariableName(SpeciesDto.class).replace(".common.", ".common.common.");
        String speciesId = ToolkitFixtures.getVariable(speciesVariableName);

        SpeciesDto localSpecies = localReferentialService.loadDto(SpeciesDto.class, speciesId);
        Assert.assertEquals(1, localSpecies.getOceanSize());

        SpeciesDto centralSpecies = centralReferentialService.loadDto(SpeciesDto.class, speciesId);
        Assert.assertEquals(1, centralSpecies.getOceanSize());

        OceanReference ocean = new OceanReference();
        //FIXME Make it fixture
        ocean.setId("fr.ird.referential.common.Ocean#1239832686152#0.7039171539191688");
        centralSpecies.getOcean().add(ocean);
        TOPIA_TEST_CLASS_RESOURCE_CENTRAL.setGenerateNow(true);
        try {
            centralReferentialService.save(centralSpecies);
        } finally {
            TOPIA_TEST_CLASS_RESOURCE_CENTRAL.setGenerateNow(false);
        }
        centralSpecies = centralReferentialService.loadDto(SpeciesDto.class, speciesId);
        Assert.assertEquals(2, centralSpecies.getOceanSize());

        // do synchro

        ProgressionModel progressionModel = new ProgressionModel();
        UnidirectionalSynchronizeEngine referentialSynchronizeEngine = new UnidirectionalSynchronizeEngine(synchronizeEngine);
        UnidirectionalSynchronizeContext unidirectionalSynchronizeContext = referentialSynchronizeEngine.prepareContext(diffsEngine, progressionModel);
        UnidirectionalResult result = referentialSynchronizeEngine.prepareResult(unidirectionalSynchronizeContext, null);
        unidirectionalSynchronizeContext.finish(service);

        Assert.assertNotNull(result);

        Set<Class<? extends ReferentialDto>> referentialNames = result.getReferentialNames();
        Assert.assertNotNull(referentialNames);
        Assert.assertEquals(1, referentialNames.size());
        Assert.assertTrue(referentialNames.contains(SpeciesDto.class));

        localSpecies = localReferentialService.loadDto(SpeciesDto.class, speciesId);
        Assert.assertEquals(2, localSpecies.getOceanSize());
        Assert.assertEquals(centralSpecies.getTopiaVersion(), localSpecies.getTopiaVersion());
        Assert.assertEquals(centralSpecies.getLastUpdateDate(), localSpecies.getLastUpdateDate());
        Assert.assertEquals(centralSpecies, localSpecies);

    }

}
