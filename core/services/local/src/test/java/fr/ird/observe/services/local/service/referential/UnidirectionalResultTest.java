package fr.ird.observe.services.local.service.referential;

/*-
 * #%L
 * ObServe Core :: Services :: Local
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.LastUpdateDate;
import fr.ird.observe.entities.referential.common.LengthLengthParameter;
import fr.ird.observe.services.local.LocalTestMethodResourceRead;
import fr.ird.observe.services.local.service.ServiceLocalTestSupportWrite;
import fr.ird.observe.services.service.referential.ReferentialService;
import fr.ird.observe.services.service.referential.SynchronizeEngine;
import fr.ird.observe.services.service.referential.SynchronizeService;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeContext;
import fr.ird.observe.services.service.referential.UnidirectionalSynchronizeEngine;
import fr.ird.observe.services.service.referential.differential.DifferentialModelBuilder;
import fr.ird.observe.services.service.referential.differential.ObserveDifferentialMetaModel;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalResult;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseClassifier;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.util.LeftOrRightContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptor;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptors;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 04/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@DatabaseVersionConfiguration(ObserveTestConfiguration.MODEL_VERSION)
@DatabaseLoginConfiguration
@DatabasePasswordConfiguration(ObserveTestConfiguration.H2_PASSWORD)
@DatabaseNameConfiguration(DatabaseName.data)
@DatabaseNameConfiguration(value = DatabaseName.data, classifier = DatabaseClassifier.CENTRAL)
@DatabaseVersionConfiguration(value = ObserveTestConfiguration.MODEL_VERSION, classifier = DatabaseClassifier.CENTRAL)
public class UnidirectionalResultTest extends ServiceLocalTestSupportWrite<SynchronizeService> {
    @ClassRule
    public static final LocalTestMethodResourceRead localTestMethodResourceCentral = new LocalTestMethodResourceRead(DatabaseClassifier.CENTRAL);
    private static final Logger log = LogManager.getLogger(UnidirectionalResultTest.class);
    private DifferentialModelBuilder diffsEngine;
    private SynchronizeEngine synchronizeEngine;

    public UnidirectionalResultTest() {
        super(SynchronizeService.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        ReferentialService service = localTestMethodResource.newService(ReferentialService.class);
        diffsEngine = ReferentialService.createDifferentialModelBuilder(ReferentialLocale.FR.getLocale(), ObserveDifferentialMetaModel::get, LeftOrRightContext.of(service, localTestMethodResourceCentral.newService(ReferentialService.class)));
        synchronizeEngine = new SynchronizeEngine(localTestMethodResource.getTestDirectory().toPath(), LeftOrRightContext.of(this.service, localTestMethodResourceCentral.newService(SynchronizeService.class)));

    }

    @DatabaseNameConfiguration(DatabaseName.empty)
    @CopyDatabaseConfiguration
    @Test
    @Ignore // We inject some species with data... Can't continue to migrate data from empty!!!
    public void testSynchronizeFromEmptyDatabase() {

        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.addPropertyChangeListener(ProgressionModel.PROPERTY_MESSAGE, evt -> log.debug(evt.getNewValue()));
        UnidirectionalSynchronizeEngine referentialSynchronizeEngine = new UnidirectionalSynchronizeEngine(synchronizeEngine);
        UnidirectionalSynchronizeContext unidirectionalSynchronizeContext = referentialSynchronizeEngine.prepareContext(diffsEngine, progressionModel);
        UnidirectionalResult result = referentialSynchronizeEngine.prepareResult(unidirectionalSynchronizeContext, null);
        unidirectionalSynchronizeContext.finish(service);

        Assert.assertNotNull(result);
        Set<Class<? extends ReferentialDto>> referentialNames = result.getReferentialNames();

        Assert.assertNotNull(referentialNames);
        Assert.assertFalse(referentialNames.isEmpty());
        Set<String> skipTypes = new LinkedHashSet<>(Arrays.asList(LastUpdateDate.class.getName(),
                                                                  LengthLengthParameter.class.getName()));
        TopiaEntitySqlDescriptors sqlDescriptors = localTestMethodResourceCentral.getTopiaApplicationContext().getSqlService().getModel().getReplicationOrderWithStandaloneDescriptors();
        for (TopiaEntitySqlDescriptor sqlDescriptor : sqlDescriptors) {
            String entityName = sqlDescriptor.getTable().getEntityName();
            if (skipTypes.contains(entityName)) {
                continue;
            }
            Class<? extends ReferentialDto> dtoType = ObservePersistenceBusinessProject.fromReferentialEntity(Objects2.forName(entityName)).toDtoType();
            Assert.assertTrue("can't find type " + dtoType, referentialNames.contains(dtoType));
            Assert.assertNull(result.getReferentialUpdated(dtoType));
            Assert.assertNull(result.getReferentialRemoved(dtoType));
            Assert.assertNull(result.getReferentialReplaced(dtoType));
            Assert.assertNull(result.getReferentialReverted(dtoType));
            Assert.assertNotNull(result.getReferentialAdded(dtoType));
            Assert.assertFalse(result.getReferentialAdded(dtoType).isEmpty());
        }
    }

    @DatabaseNameConfiguration(DatabaseName.data)
    @CopyDatabaseConfiguration
    @Test
    public void testSynchronizeWithNoChange() {
        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.addPropertyChangeListener(ProgressionModel.PROPERTY_MESSAGE, evt -> log.debug(evt.getNewValue()));

        UnidirectionalSynchronizeEngine referentialSynchronizeEngine = new UnidirectionalSynchronizeEngine(synchronizeEngine);
        UnidirectionalSynchronizeContext unidirectionalSynchronizeContext = referentialSynchronizeEngine.prepareContext(diffsEngine, progressionModel);
        UnidirectionalResult result = referentialSynchronizeEngine.prepareResult(unidirectionalSynchronizeContext, null);
        unidirectionalSynchronizeContext.finish(service);

        Assert.assertNotNull(result);
        Set<Class<? extends ReferentialDto>> referentialNames = result.getReferentialNames();
        Assert.assertNotNull(referentialNames);
        Assert.assertTrue(referentialNames.isEmpty());
        Set<String> skipTypes = Collections.singleton(LastUpdateDate.class.getName());
        TopiaEntitySqlDescriptors sqlDescriptors = localTestMethodResourceCentral.getTopiaApplicationContext().getSqlService().getModel().getReplicationOrderWithStandaloneDescriptors();
        for (TopiaEntitySqlDescriptor sqlDescriptor : sqlDescriptors) {
            String entityName = sqlDescriptor.getTable().getEntityName();
            if (skipTypes.contains(entityName)) {
                continue;
            }
            Class<? extends ReferentialDto> dtoType = ObservePersistenceBusinessProject.fromReferentialEntity(Objects2.forName(entityName)).toDtoType();
            Assert.assertNull(result.getReferentialUpdated(dtoType));
            Assert.assertNull(result.getReferentialRemoved(dtoType));
            Assert.assertNull(result.getReferentialReplaced(dtoType));
            Assert.assertNull(result.getReferentialReverted(dtoType));
            Assert.assertNull(result.getReferentialAdded(dtoType));
        }
    }

//    // Il faut reecrire ce test en clonant une base, y fiasant des modif puis test
//    // et plus en utilisant deux bases différentes (lourd à maintenir).
//    @DatabaseNameConfiguration(DatabaseName.tck)
//    @CopyDatabaseConfiguration
//    @DatabaseNameConfiguration(value = DatabaseName.dataForTestUnidirectionalReferentialSynchro, classifier = DatabaseClassifier.CENTRAL)
//    @DatabaseVersionConfiguration(value = ObserveTestConfiguration.MODEL_VERSION, classifier = DatabaseClassifier.CENTRAL)
//    @Test
//    @Ignore
//    public void testSynchronizeWithAllChanges() {
//
//        ProgressionModel progressionModel = new ProgressionModel();
//        progressionModel.addPropertyChangeListener(ProgressionModel.PROPERTY_MESSAGE, evt -> log.info(evt.getNewValue()));
//        UnidirectionalSynchronizeEngine referentialSynchronizeEngine = new UnidirectionalSynchronizeEngine(diffsEngine);
//        UnidirectionalSynchronizeContext unidirectionalReferentialSynchronizeContext = referentialSynchronizeEngine.prepareContext(progressionModel, localService);
//
//        UnidirectionalCallbackResults results = new UnidirectionalCallbackResults();
//        results.addCallbackResult(PersonDto.class, "fr.ird.referential.common.Person#1355399844272#0.32586441962131485", "fr.ird.referential.common.Person#1429515754659#0.322074382333085");
//
//        UnidirectionalResult result = referentialSynchronizeEngine.prepareResult(localService, unidirectionalReferentialSynchronizeContext, results);
//        referentialSynchronizeEngine.finish(localService, unidirectionalReferentialSynchronizeContext);
//
//        Assert.assertNotNull(result);
//        Set<Class<? extends ReferentialDto>> referentialNames = result.getReferentialNames();
//        Assert.assertNotNull(referentialNames);
//        Assert.assertFalse(referentialNames.isEmpty());
//        {
//            Assert.assertTrue(referentialNames.contains(VesselActivityDto.class));
//            Assert.assertTrue(result.getReferentialAdded(VesselActivityDto.class).isEmpty());
//            Collection<String> referentialUpdated = result.getReferentialUpdated(VesselActivityDto.class);
//            Assert.assertFalse(referentialUpdated.isEmpty());
//            Assert.assertEquals(1, referentialUpdated.size());
//            Assert.assertTrue(referentialUpdated.contains("fr.ird.referential.ps.common.VesselActivity#1239832675369#0.12552908048322586"));
//
//            Assert.assertTrue(result.getReferentialRemoved(VesselActivityDto.class).isEmpty());
//            Assert.assertTrue(result.getReferentialReplaced(VesselActivityDto.class).isEmpty());
//        }
//
//        {
//            Assert.assertTrue(referentialNames.contains(SpeciesDto.class));
//
//            Assert.assertTrue(result.getReferentialAdded(SpeciesDto.class).isEmpty());
//            Collection<String> referentialUpdated = result.getReferentialUpdated(SpeciesDto.class);
//            Assert.assertFalse(referentialUpdated.isEmpty());
//            Assert.assertEquals(1, referentialUpdated.size());
//            Assert.assertTrue(referentialUpdated.contains("fr.ird.referential.common.Species#1239832685474#0.8943253454598569"));
//
//            Assert.assertTrue(result.getReferentialRemoved(SpeciesDto.class).isEmpty());
//            Assert.assertTrue(result.getReferentialReplaced(SpeciesDto.class).isEmpty());
//        }
//        {
//            Assert.assertTrue(referentialNames.contains(PersonDto.class));
//            Assert.assertTrue(result.getReferentialAdded(PersonDto.class).isEmpty());
//            Assert.assertTrue(result.getReferentialUpdated(PersonDto.class).isEmpty());
//            Collection<String> referentialRemoved = result.getReferentialRemoved(PersonDto.class);
//            Assert.assertFalse(referentialRemoved.isEmpty());
//            Assert.assertEquals(1, referentialRemoved.size());
//            Assert.assertTrue(referentialRemoved.contains("fr.ird.referential.common.Person#1355399844272#0.32586441962131485"));
//            Collection<Pair<String, String>> referentialReplaced = result.getReferentialReplaced(PersonDto.class);
//            Assert.assertFalse(referentialReplaced.isEmpty());
//            Assert.assertEquals(1, referentialReplaced.size());
//            Assert.assertTrue(referentialReplaced.contains(Pair.of("fr.ird.referential.common.Person#1355399844272#0.32586441962131485", "fr.ird.referential.common.Person#1429515754659#0.322074382333085")));
//        }
//        {
//            Assert.assertTrue(referentialNames.contains(VesselDto.class));
//            Assert.assertTrue(result.getReferentialAdded(VesselDto.class).isEmpty());
//            Collection<String> referentialUpdated = result.getReferentialUpdated(VesselDto.class);
//            Assert.assertFalse(referentialUpdated.isEmpty());
//            Assert.assertEquals(1, referentialUpdated.size());
//            Assert.assertTrue(referentialUpdated.contains("fr.ird.referential.common.Vessel#1306847717532#0.7435948873477364"));
//            Assert.assertTrue(result.getReferentialRemoved(VesselDto.class).isEmpty());
//            Assert.assertTrue(result.getReferentialReplaced(VesselDto.class).isEmpty());
//        }
//
//    }
}
