package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.util.Version;
import org.junit.Assert;

public class AnonymousServiceFixtures extends GeneratedAnonymousServiceFixtures {

    @Override
    public void generateHomeId(ObserveServicesProvider servicesProvider, AnonymousService service) {
        String actual = service.generateHomeId();
        Assert.assertNotNull(actual);
    }

    @Override
    public void getModelVersion(ObserveServicesProvider servicesProvider, AnonymousService service) {
        Version actual = service.getModelVersion();
        Assert.assertNotNull(actual);
    }

    @Override
    public void getServerVersion(ObserveServicesProvider servicesProvider, AnonymousService service) {
        Version actual = service.getServerVersion();
        Assert.assertNotNull(actual);
    }

    @Override
    public void applySecurity(fr.ird.observe.services.ObserveServicesProvider servicesProvider, AnonymousService service) {
        // FIXME:Test Remove super method invocation and implements fixture
        // actual = service.applySecurity(getProperty("applySecurity.id"));
        // Assert.assertNotNull(actual);
        super.applySecurity(servicesProvider, service);
    }

    @Override
    public void checkCanConnect(fr.ird.observe.services.ObserveServicesProvider servicesProvider, AnonymousService service) {
        // FIXME:Test Remove super method invocation and implements fixture
        // actual = service.checkCanConnect(getProperty("checkCanConnect.id"));
        // Assert.assertNotNull(actual);
        super.checkCanConnect(servicesProvider, service);
    }

    @Override
    public void checkCanConnectOrBeEmpty(fr.ird.observe.services.ObserveServicesProvider servicesProvider, AnonymousService service) {
        // FIXME:Test Remove super method invocation and implements fixture
        // actual = service.checkCanConnectOrBeEmpty(getProperty("checkCanConnectOrBeEmpty.id"));
        // Assert.assertNotNull(actual);
        super.checkCanConnectOrBeEmpty(servicesProvider, service);
    }

    @Override
    public void createEmpty(fr.ird.observe.services.ObserveServicesProvider servicesProvider, AnonymousService service) {
        // FIXME:Test Remove super method invocation and implements fixture
        // actual = service.create(getProperty("create.id"));
        // Assert.assertNotNull(actual);
        super.createEmpty(servicesProvider, service);
    }

    @Override
    public void createFromDump(ObserveServicesProvider servicesProvider, AnonymousService service) {
        //FIXME
        super.createFromDump(servicesProvider, service);
    }

    @Override
    public void createFromImport(ObserveServicesProvider servicesProvider, AnonymousService service) {
        //FIXME
        super.createFromImport(servicesProvider, service);
    }

    @Override
    public void getUsers(fr.ird.observe.services.ObserveServicesProvider servicesProvider, AnonymousService service) {
        // FIXME:Test Remove super method invocation and implements fixture
        // actual = service.getUsers(getProperty("getUsers.id"));
        // Assert.assertNotNull(actual);
        super.getUsers(servicesProvider, service);
    }

    @Override
    public void migrateData(fr.ird.observe.services.ObserveServicesProvider servicesProvider, AnonymousService service) {
        // FIXME:Test Remove super method invocation and implements fixture
        // actual = service.migrateData(getProperty("migrateData.id"));
        // Assert.assertNotNull(actual);
        super.migrateData(servicesProvider, service);
    }

    @Override
    public void open(fr.ird.observe.services.ObserveServicesProvider servicesProvider, AnonymousService service) {
        // FIXME:Test Remove super method invocation and implements fixture
        // actual = service.open(getProperty("open.id"));
        // Assert.assertNotNull(actual);
        super.open(servicesProvider, service);
    }

}
