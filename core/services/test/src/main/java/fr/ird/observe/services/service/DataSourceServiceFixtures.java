package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.request.CopyRequest;
import fr.ird.observe.datasource.request.CreateDatabaseRequest;
import fr.ird.observe.datasource.request.DeleteRequest;
import fr.ird.observe.datasource.request.ReplicateRequest;
import fr.ird.observe.dto.referential.ps.common.ProgramDto;
import fr.ird.observe.entities.ObserveTopiaApplicationContext;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.test.ObserveFixtures;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.junit.Assert;

import java.nio.file.Path;
import java.util.Collections;
import java.util.Set;

public class DataSourceServiceFixtures extends GeneratedDataSourceServiceFixtures {

    private Path testDirectory;
    private ObserveDataSourceConnection dataSourceConnection;

    @Override
    public void isIdValid(ObserveServicesProvider servicesProvider, DataSourceService service) {
        boolean actual = service.isIdValid(ProgramDto.class, getProperty("isIdValid.id"));
        Assert.assertTrue(actual);
        actual = service.isIdValid(ProgramDto.class, "Fake__" + getProperty("isIdValid.id"));
        Assert.assertFalse(actual);
    }

    @Override
    public void close(ObserveServicesProvider servicesProvider, DataSourceService service) {
        // FIXME:Test Remove super method invocation and implements fixture
        // actual = service.close(getProperty("close.id"));
        // Assert.assertNotNull(actual);
        super.close(servicesProvider, service);
    }

    @Override
    public void getLastUpdateDate(ObserveServicesProvider servicesProvider, DataSourceService service) {
        Assert.assertNull(service.getLastUpdateDate(getProperty("getLastUpdateDate.type")));
        Assert.assertNotNull(service.getLastUpdateDate("fr.ird.observe.entities.referential.common.Ocean"));
    }

    @Override
    public void retainExistingIds(ObserveServicesProvider servicesProvider, DataSourceService service) {
        Set<String> actual = service.retainExistingIds(Collections.singleton(getProperty("retainExistingIds.id")));
        Assert.assertNotNull(actual);
        Assert.assertFalse(actual.isEmpty());
        actual = service.retainExistingIds(Collections.singleton(getProperty("retainExistingIds.id") + "Fake"));
        Assert.assertNotNull(actual);
        Assert.assertTrue(actual.isEmpty());
    }

    @Override
    public void produceCreateSqlScript(ObserveServicesProvider servicesProvider, DataSourceService service) {
        Version dbVersion = servicesProvider.getAnonymousService().getModelVersion();
        produceCreateSqlScript(service, CreateDatabaseRequest.builder(false, dbVersion));
        produceCreateSqlScript(service, CreateDatabaseRequest.builder(true, dbVersion));
    }

    @Override
    public void produceAddSqlScript(ObserveServicesProvider servicesProvider, DataSourceService service) {
        produceAddSqlScript(service, false);
        produceAddSqlScript(service, true);
    }

    @Override
    public void produceMoveSqlScript(ObserveServicesProvider servicesProvider, DataSourceService service) {
        produceMoveSqlScript(service, false);
        produceMoveSqlScript(service, true);
    }

    @Override
    public void produceDeleteSqlScript(ObserveServicesProvider servicesProvider, DataSourceService service) {
        String id = getProperty("produceDeleteSqlScript.id");
        SqlScript actual = service.produceDeleteSqlScript(new DeleteRequest(true, "fr.ird.observe.entities.data.ps.common.Trip", null, id));
        Assert.assertNotNull(actual);
    }

    protected void produceCreateSqlScript(DataSourceService service, CreateDatabaseRequest.Builder request) {
        try {
            testRequest(service, request);
        } catch (IllegalStateException e) {
            // this is normal request is empty and this is no more accepted
        }
        testRequest(service, request = request.addGeneratedSchema());
        testRequest(service, request = request.addVersionTable());
        testRequest(service, request = request.addStandaloneTables());
        testRequest(service, request.dataIdsToAdd("fr.ird.observe.entities.data.ps.common.Trip", Collections.singleton(getPsCommonTripId())));
        testRequest(service, request.dataIdsToAdd("fr.ird.observe.entities.data.ll.common.Trip", Collections.singleton(getLlCommonTripId())));
        testRequest(service, request.addAllData());
    }

    protected void produceAddSqlScript(DataSourceService service, boolean pg) {
        testRequest(service, new CopyRequest(pg, "fr.ird.observe.entities.data.ps.common.Trip"));
        testRequest(service, new CopyRequest(pg, "fr.ird.observe.entities.data.ll.common.Trip"));
        testRequest(service, new CopyRequest(pg, "fr.ird.observe.entities.data.ps.common.Trip", getPsCommonTripId()));
        testRequest(service, new CopyRequest(pg, "fr.ird.observe.entities.data.ll.common.Trip", getLlCommonTripId()));
    }

    protected void produceMoveSqlScript(DataSourceService service, boolean pg) {
        String psObsRouteVariable = getVariableName(fr.ird.observe.dto.data.ps.observation.RouteDto.class);
        String psObsRouteId = getVariable(psObsRouteVariable);
        String psObsRouteIdTarget = getVariable(psObsRouteVariable + ".move");
        String psObsActivityVariable = getVariableName(fr.ird.observe.dto.data.ps.observation.ActivityDto.class);
        String psObsActivityId = getVariable(psObsActivityVariable);
        String psObsActivityIdTarget = getVariable(psObsActivityVariable + ".move");
        String psObsSetId = getVariable(getVariableName(fr.ird.observe.dto.data.ps.observation.SetDto.class));

        String psLogbookRouteVariable = getVariableName(fr.ird.observe.dto.data.ps.logbook.RouteDto.class);
        String psLogbookRouteId = getVariable(psLogbookRouteVariable);
        String psLogbookRouteIdTarget = getVariable(psLogbookRouteVariable + ".move");
        String psLogbookActivityId = getVariable(getVariableName(fr.ird.observe.dto.data.ps.logbook.ActivityDto.class));

        String llObsActivityVariable = getVariableName(fr.ird.observe.dto.data.ll.observation.ActivityDto.class);
        String llObsActivityId = getVariable(llObsActivityVariable);
        String llObsActivityIdTarget = getVariable(llObsActivityVariable + ".move");

        String llObSetId = getVariable(getVariableName(fr.ird.observe.dto.data.ll.observation.SetDto.class));

        String llLogbookActivityVariable = getVariableName(fr.ird.observe.dto.data.ll.logbook.ActivityDto.class);
        String llLogbookActivityId = getVariable(llLogbookActivityVariable);
        String llLogbookActivityIdTarget = getVariable(llLogbookActivityVariable + ".move");

        String llLogbookSetId = getVariable(getVariableName(fr.ird.observe.dto.data.ll.logbook.SetDto.class));
        String psCommTripVariable = getVariableName(fr.ird.observe.dto.data.ps.common.TripDto.class);

        String psCommonTripId = getVariable(psCommTripVariable);
        String psCommonTripIdTarget = getVariable(psCommTripVariable + ".move");

        String llCommTripVariable = getVariableName(fr.ird.observe.dto.data.ll.common.TripDto.class);
        String llCommonTripId = getVariable(llCommTripVariable);
        String llCommonTripIdTarget = getVariable(llCommTripVariable + ".move");

        testRequest(service, new ReplicateRequest(pg, psCommonTripId, psCommonTripIdTarget, "fr.ird.observe.entities.data.ps.observation.Route", psObsRouteId));

        testRequest(service, new ReplicateRequest(pg, psObsRouteId, psObsRouteIdTarget, "fr.ird.observe.entities.data.ps.observation.Activity", psObsActivityId));
        testRequest(service, new ReplicateRequest(pg, psObsActivityId, psObsActivityIdTarget, "fr.ird.observe.entities.data.ps.observation.Set", psObsSetId));

        testRequest(service, new ReplicateRequest(pg, psCommonTripId, psCommonTripIdTarget, "fr.ird.observe.entities.data.ps.logbook.Route", psLogbookRouteId));
        testRequest(service, new ReplicateRequest(pg, psLogbookRouteId, psLogbookRouteIdTarget, "fr.ird.observe.entities.data.ps.logbook.Activity", psLogbookActivityId));

        testRequest(service, new ReplicateRequest(pg, llCommonTripId, llCommonTripIdTarget, "fr.ird.observe.entities.data.ll.observation.Activity", llObsActivityId));
        testRequest(service, new ReplicateRequest(pg, llObsActivityId, llObsActivityIdTarget, "fr.ird.observe.entities.data.ll.observation.Set", llObSetId));

        testRequest(service, new ReplicateRequest(pg, llCommonTripId, llCommonTripIdTarget, "fr.ird.observe.entities.data.ll.logbook.Activity", llLogbookActivityId));
        testRequest(service, new ReplicateRequest(pg, llLogbookActivityId, llLogbookActivityIdTarget, "fr.ird.observe.entities.data.ll.logbook.Set", llLogbookSetId));
    }

    //FIXME Add tests for replicatePartial, delete and deletePartial
    protected void testRequest(DataSourceService service, CopyRequest request) {
        SqlScript actual = service.produceAddSqlScript(request);
        Assert.assertNotNull(actual);
    }

    protected void testRequest(DataSourceService service, CreateDatabaseRequest.Builder requestBuilder) {
        CreateDatabaseRequest request = requestBuilder.build();
        SqlScript actual = service.produceCreateSqlScript(request);
        Assert.assertNotNull(actual);
        if (testDirectory != null) {
            ObserveDataSourceConfigurationTopiaH2 targetConfiguration = ObserveDataSourceConfigurationTopiaH2.createTemporaryConfiguration(testDirectory.toFile(), request.getDbVersion());
            try (ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.createContext(targetConfiguration)) {
                topiaApplicationContext.executeSqlStatements(actual);
            }
        }
    }

    protected void testRequest(DataSourceService service, ReplicateRequest request) {
        SqlScript actual = service.produceMoveSqlScript(request);
        Assert.assertNotNull(actual);
        String content = actual.content();
        Assert.assertFalse(String.format("Content is empty for: %s", request), content.isEmpty());
        if (!ObserveFixtures.WITH_ASSERT) {
            System.out.printf("Move script for: %s\n\n:%s%n", request, content);
        }
    }

    public void setTemporaryDirectory(Path testDirectory) {
        this.testDirectory = testDirectory;
    }

    public ObserveDataSourceConnection getDataSourceConnection() {
        return dataSourceConnection;
    }

    public void setDataSourceConnection(ObserveDataSourceConnection dataSourceConnection) {
        this.dataSourceConnection = dataSourceConnection;
    }
}
