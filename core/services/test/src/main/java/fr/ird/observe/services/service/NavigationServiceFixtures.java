package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.gson.Gson;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.navigation.tree.NavigationResult;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModel;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelPathRequest;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.navigation.tree.navigation.RootNavigationTreeNode;
import fr.ird.observe.navigation.tree.selection.RootSelectionTreeNode;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import fr.ird.observe.spi.navigation.tree.ToolkitTreeNodeBuilder;
import io.ultreia.java4all.lang.Strings;
import org.junit.Assert;

import java.util.function.Function;
import java.util.function.Supplier;

public class NavigationServiceFixtures extends GeneratedNavigationServiceFixtures {

    public static <Root extends ToolkitTreeNode, Request extends ToolkitTreeFlatModelRootRequest, Result extends ToolkitTreeFlatModel> void test(ObserveServiceFixtures<?> fixtures, Function<Request, Result> service, Supplier<Request> requestSupplier, Supplier<Root> rootNode) {

        testModules(fixtures, service, requestSupplier.get(), rootNode, "none", null);
        testModules(fixtures, service, requestSupplier.get(), rootNode, "common", "common");
        testModules(fixtures, service, requestSupplier.get(), rootNode, "ps", "ps");
        testModules(fixtures, service, requestSupplier.get(), rootNode, "ll", "ll");
    }

    public static <Root extends ToolkitTreeNode, Request extends ToolkitTreeFlatModelRootRequest, Result extends ToolkitTreeFlatModel> void testModules(ObserveServiceFixtures<?> fixtures, Function<Request, Result> service, Request request, Supplier<Root> rootNode, String classifier, String moduleNames) {
        request.setModuleName(moduleNames);
        if (moduleNames == null || "common".equals(moduleNames)) {
            request.setLoadData(false);
        } else {
            if (request.isLoadData()) {
                request.setGroupByName("data" + Strings.capitalize(moduleNames) + "CommonTripGroupByObservationsProgram");
            }
        }
        String fixturePrefix = "count." + classifier + ".";
        assertRequest(fixtures, service, request, rootNode.get(), fixturePrefix + "default");
        request.setLoadEmptyGroupBy(true);
        assertRequest(fixtures, service, request, rootNode.get(), fixturePrefix + "loadEmptyRootNodes");
        request.setLoadDisabledGroupBy(true);
        assertRequest(fixtures, service, request, rootNode.get(), fixturePrefix + "loadDisabledRootNodes");
        request.setLoadReferential(true);
        assertRequest(fixtures, service, request, rootNode.get(), fixturePrefix + "full");
    }

    public static <Root extends ToolkitTreeNode, Request extends ToolkitTreeFlatModelRootRequest, Result extends ToolkitTreeFlatModel> void assertRequest(ObserveServiceFixtures<?> fixtures, Function<Request, Result> service, Request request, Root rootNode, String expectedCount) {
        Result actual = service.apply(request);
        Assert.assertNotNull(actual);
        if (!WITH_ASSERT || expectedCount.endsWith(".all.full")) {
            Gson gson = new DtoGsonSupplier(true).get();
            System.out.println(gson.toJson(actual));
        }
        ToolkitTreeNodeBuilder builder = new ToolkitTreeNodeBuilder();
        builder.load(rootNode, actual.getMapping());
        Assert.assertEquals("Bad value for " + expectedCount, fixtures.getIntegerProperty(expectedCount), rootNode.getChildCount());
    }

    public static <Root extends ToolkitTreeNode, Request extends ToolkitTreeFlatModelRootRequest, Result extends ToolkitTreeFlatModel> void testRequest(ObserveServiceFixtures<?> fixtures, Function<Request, Result> service, Request request, Root rootNode, String expectedCount) {
        Result actual = service.apply(request);
        Assert.assertNotNull(actual);
        String statesCountProperty = expectedCount + ".states.count";
        Assert.assertEquals("Bad value for " + statesCountProperty, fixtures.getIntegerProperty(statesCountProperty), actual.getMapping().size());
        if (!WITH_ASSERT || expectedCount.endsWith(".all.full")) {
            Gson gson = new DtoGsonSupplier(true).get();
            System.out.println(gson.toJson(actual));
        }
        ToolkitTreeNodeBuilder builder = new ToolkitTreeNodeBuilder();
        builder.load(rootNode, actual.getMapping());
        String nodesCountProperty = expectedCount + ".nodes.count";
        Assert.assertEquals("Bad value for " + nodesCountProperty, fixtures.getIntegerProperty(nodesCountProperty), rootNode.getChildCount());
    }

    @Override
    public void loadNavigationPath(ObserveServicesProvider servicesProvider, NavigationService service) {

        loadNavigationPath(service, "dataPsCommonTripGroupByObservationsProgram~fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444", "loadPath.count0", "loadPath.count0.recursive");
        loadNavigationPath(service, "dataPsCommonTripGroupByObservationsProgram~fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444/fr.ird.data.ps.common.Trip#1343641056380#0.32424601977176837", "loadPath.count1", "loadPath.count1.recursive");
        loadNavigationPath(service, "dataPsCommonTripGroupByObservationsProgram~fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444/fr.ird.data.ps.common.Trip#1343641056380#0.32424601977176837/routeObs", "loadPath.count2", "loadPath.count2.recursive");
        loadNavigationPath(service, "dataPsCommonTripGroupByObservationsProgram~fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444/fr.ird.data.ps.common.Trip#1343641056380#0.32424601977176837/routeObs/fr.ird.data.ps.observation.Route#1343641087190#0.7801355724943301", "loadPath.count3", "loadPath.count3.recursive");
        loadNavigationPath(service, "dataPsCommonTripGroupByObservationsProgram~fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444/fr.ird.data.ps.common.Trip#1343641056380#0.32424601977176837/routeLogbook", "loadPath.count4", "loadPath.count4.recursive");
    }

//    @Override
//    public void loadContent(ObserveServicesProvider servicesProvider, NavigationService service) {
//
//        loadContent(service, "dataPsCommonTripGroupByObservationsProgram~fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444", "loadContent.count0");
//        loadContent(service, "dataPsCommonTripGroupByObservationsProgram~fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444/fr.ird.data.ps.common.Trip#1343641056380#0.32424601977176837", "loadContent.count1");
//        loadContent(service, "dataPsCommonTripGroupByObservationsProgram~fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444/fr.ird.data.ps.common.Trip#1343641056380#0.32424601977176837/routeObs", "loadContent.count2");
//        loadContent(service, "dataPsCommonTripGroupByObservationsProgram~fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444/fr.ird.data.ps.common.Trip#1343641056380#0.32424601977176837/routeObs/fr.ird.data.ps.observation.Route#1343641087190#0.7801355724943301", "loadContent.count3");
//        loadContent(service, "dataPsCommonTripGroupByObservationsProgram~fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444/fr.ird.data.ps.common.Trip#1343641056380#0.32424601977176837/routeLogbook", "loadContent.count4");
//
//        loadContent(service, "referentialPsCommon/program", "loadContent.count0");
//        loadContent(service, "referentialPsCommon/program:fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444", "loadContent.count0");
//    }

    @Override
    public void loadNavigationRoot(ObserveServicesProvider servicesProvider, NavigationService service) {
        test(this, service::loadNavigationRoot, ToolkitTreeFlatModelRootRequest::new, RootNavigationTreeNode::new);
    }

    @Override
    public void loadSelectionRoot(ObserveServicesProvider servicesProvider, NavigationService service) {
        NavigationServiceFixtures.test(this, service::loadSelectionRoot, ToolkitTreeFlatModelRootRequest::new, RootSelectionTreeNode::new);

        ToolkitTreeFlatModelRootRequest request = new ToolkitTreeFlatModelRootRequest();
        request.setLoadData(false);
        NavigationServiceFixtures.testRequest(this, service::loadSelectionRoot, request, new RootSelectionTreeNode(), "selection.request.empty");
        request.setLoadReferential(true);
        NavigationServiceFixtures.testRequest(this, service::loadSelectionRoot, request, new RootSelectionTreeNode(), "selection.request.referential");
        request.setLoadReferential(false);
        request.setLoadData(true);
        request.setGroupByName("dataPsCommonTripGroupByObservationsProgram");
//        request.setLoadDisabledGroupBy(true);
//        request.setLoadEmptyGroupBy(true);
        NavigationServiceFixtures.testRequest(this, service::loadSelectionRoot, request, new RootSelectionTreeNode(), "selection.request.dataPsCommonTripGroupByObservationsProgram");
    }

    @Override
    public void getNavigation(ObserveServicesProvider servicesProvider, NavigationService service) {
        ToolkitTreeFlatModelRootRequest request = new ToolkitTreeFlatModelRootRequest();
        request.setModuleName("ps");
        request.setGroupByName("dataPsCommonTripGroupByObservationsProgram");
        NavigationResult actual = service.getNavigation(request, null);
        Assert.assertNotNull(actual);
    }

    @Override
    public void getGroupByDtoSet(ObserveServicesProvider servicesProvider, NavigationService service) {
        ToolkitTreeFlatModelRootRequest request = new ToolkitTreeFlatModelRootRequest();
        request.setModuleName("ps");
        request.setGroupByName("dataPsCommonTripGroupByObservationsProgram");
        DataGroupByDtoSet<?, ?> actual = service.getGroupByDtoSet(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(3, actual.size());
    }

    protected void loadNavigationPath(NavigationService service, String id, String fixtureId, String fixtureRecursiveId) {
        ToolkitTreeFlatModel actual;
        actual = service.loadNavigationPath(new ToolkitTreeFlatModelPathRequest(id, DATE, false));
        Assert.assertNotNull(actual);
        Assert.assertEquals("Bad value for " + fixtureId, getIntegerProperty(fixtureId), actual.getMapping().size());
        actual = service.loadNavigationPath(new ToolkitTreeFlatModelPathRequest(id, DATE, true));
        Assert.assertNotNull(actual);
        Assert.assertEquals("Bad value for " + fixtureRecursiveId, getIntegerProperty(fixtureRecursiveId), actual.getMapping().size());
    }

//    protected void loadContent(NavigationService service, String id, String fixtureId) {
//        ToolkitTreeNodeStates actual = service.loadContent(new ToolkitTreeFlatModelContentRequest(id, DATE, true, true, true, true, false));
//        Assert.assertNotNull(actual);
//        Assert.assertEquals("Bad value for " + fixtureId, getIntegerProperty(fixtureId), actual.getStates().size());
//        String content = actual.getState(ToolkitTreeNodeBean.STATE_CONTENT.name());
//        Assert.assertNotNull(content);
////        System.out.println(content);
//    }
}
