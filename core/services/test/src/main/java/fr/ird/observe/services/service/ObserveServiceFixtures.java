package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import fr.ird.observe.test.ObserveFixtures;
import io.ultreia.java4all.util.RecursiveProperties;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 02/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.5
 */
public class ObserveServiceFixtures<S extends ObserveService> extends ObserveFixtures {

    private static final Logger log = LogManager.getLogger(ObserveServiceFixtures.class);
    protected final Map<String, String> properties;
    private final Class<S> serviceType;
    private Gson gson;

    protected ObserveServiceFixtures(Class<S> serviceType) {
        this.serviceType = Objects.requireNonNull(serviceType);
        RecursiveProperties properties = new RecursiveProperties();
        properties.putAll(IDS);

        String serviceResourcePath = getServiceResourcePath();
        Map<String, String> properties0 = loadFixturesMap(serviceResourcePath);
        properties.putAll(properties0);

        Map<String, String> propertiesBuilder = new LinkedHashMap<>();
        properties.stringPropertyNames().forEach(k -> propertiesBuilder.put(k, properties.getProperty(k)));
        this.properties = propertiesBuilder;
        log.info(String.format("Load %d fixtures for %s", properties.size(), serviceResourcePath));
    }

    public <O> O loadJson(Type type, String suffix) {
        String resourcePath = getResourcePath(suffix);
        URL url = Objects.requireNonNull(getClass().getClassLoader().getResource("fixtures/" + resourcePath), "Fixture not found: " + resourcePath);
        try (InputStreamReader inputStream = new InputStreamReader(url.openStream(), StandardCharsets.UTF_8)) {
            log.debug(String.format("Fixture [%s] - Loading...", url));
            return getGson().fromJson(inputStream, type);

        } catch (IOException e) {
            throw new IllegalArgumentException("Can't load fixtures " + resourcePath, e);
        }
    }

    private String getResourcePath(String suffix) {
        return getServiceResourcePath() + "-" + suffix;
    }

    public <O> void storeJson(O source, String suffix) {
        String resourcePath = getResourcePath(suffix);
        URL url = Objects.requireNonNull(getClass().getClassLoader().getResource("fixtures/" + resourcePath), "Fixture not found: " + resourcePath);
        File f = new File(url.getFile().replace("target/classes/", "src/main/resources/"));
        try {
            log.info(String.format("Fixture [%s] - Writing...", url));

            String content = getGson().toJson(source);
            Files.write(f.toPath(), content.getBytes(StandardCharsets.UTF_8));

        } catch (Exception e) {
            throw new IllegalArgumentException("Can't write fixtures " + resourcePath, e);
        }
    }

    public Gson getGson() {
        if (gson == null) {
            gson = new DtoGsonSupplier(true).disableHtmlEscaping().get();
        }
        return gson;
    }

    public String getServiceResourcePath() {
        return getServiceResourcePath(serviceType);
    }

    public String getServiceResourcePath(Class<?> serviceType) {
        return serviceType.getName().replaceAll("\\.", "/");
    }

    public final Class<S> getServiceType() {
        return serviceType;
    }

    public final Map<String, String> getProperties() {
        return properties;
    }

    public final String getProperty(String propertyName) {
        return getProperties().get(propertyName);
    }

    public final String[] getArrayProperty(String propertyName) {
        String property = getProperty(propertyName);
        if (property == null) {
            return new String[0];
        }
        property = property.trim();
        if (property.isEmpty()) {
            return new String[0];
        }
        String[] data;
        if (property.endsWith("^")) {
            data = (property + "_^").split("\\s*\\^\\s*");
            data[data.length - 1] = "";
        } else {
            data = property.split("\\s*\\^\\s*");
        }
        return data;
    }

    public final int getIntegerProperty(String propertyName) {
        String property = Objects.requireNonNull(getProperty(propertyName), String.format("%s Can not find fixture: %s", getClass().getName(), propertyName));
        return Integer.parseInt(property);
    }

    public String loadContent(Class<?> service, String classifier) {
        String resourceName = "/fixtures/" + getServiceResourcePath(service) + "-" + classifier + ".json";
        try {
            return IOUtils.resourceToString(resourceName, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't load resource %s", resourceName), e);
        }
    }
}
