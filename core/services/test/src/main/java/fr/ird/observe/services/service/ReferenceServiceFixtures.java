package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.data.ps.common.TripReference;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.HarbourDto;
import fr.ird.observe.dto.referential.common.HarbourReference;
import fr.ird.observe.dto.referential.common.OceanDto;
import fr.ird.observe.dto.referential.common.OceanReference;
import fr.ird.observe.dto.referential.common.VesselDto;
import fr.ird.observe.dto.referential.common.VesselReference;
import fr.ird.observe.dto.referential.ll.common.TripTypeDto;
import fr.ird.observe.dto.referential.ll.common.TripTypeReference;
import fr.ird.observe.dto.referential.ps.common.SampleTypeReference;
import fr.ird.observe.dto.referential.ps.common.VesselActivityDto;
import fr.ird.observe.dto.referential.ps.common.VesselActivityReference;
import fr.ird.observe.services.ObserveServicesProvider;
import org.junit.Assert;

public class ReferenceServiceFixtures extends GeneratedReferenceServiceFixtures {

    public static OceanReference oceanReference() {
        OceanDto dto = new OceanDto();
        dto.setId(getVariable("referential.common.common.Ocean.id"));
        return OceanReference.of(ReferentialLocale.FR, dto);
    }

    public static HarbourReference harbourReference() {
        HarbourDto dto = new HarbourDto();
        dto.setId(getVariable("referential.common.common.Harbour.id"));
        return HarbourReference.of(ReferentialLocale.FR, dto);
    }

    public static VesselReference vesselReference() {
        VesselDto dto = new VesselDto();
        dto.setId(getVariable("referential.common.common.Vessel.id"));
        return VesselReference.of(ReferentialLocale.FR, dto);
    }

    public static fr.ird.observe.dto.referential.ps.common.ProgramReference psProgramReference() {
        fr.ird.observe.dto.referential.ps.common.ProgramDto dto = new fr.ird.observe.dto.referential.ps.common.ProgramDto();
        dto.setId(getVariable("referential.ps.common.Program.id"));
        return fr.ird.observe.dto.referential.ps.common.ProgramReference.of(ReferentialLocale.FR, dto);
    }

    public static fr.ird.observe.dto.referential.ll.common.ProgramReference llProgramReference() {
        fr.ird.observe.dto.referential.ll.common.ProgramDto dto = new fr.ird.observe.dto.referential.ll.common.ProgramDto();
        dto.setId(getVariable("referential.ll.common.Program.id"));
        return fr.ird.observe.dto.referential.ll.common.ProgramReference.of(ReferentialLocale.FR, dto);
    }

    public static TripTypeReference tripTypeReference() {
        TripTypeDto dto = new TripTypeDto();
        dto.setId(getVariable("referential.ll.common.TripType.id"));
        return TripTypeReference.of(ReferentialLocale.FR, dto);
    }

    public static fr.ird.observe.dto.referential.ll.common.VesselActivityReference llVesselActivityReference() {
        fr.ird.observe.dto.referential.ll.common.VesselActivityDto dto = new fr.ird.observe.dto.referential.ll.common.VesselActivityDto();
        dto.setId(getVariable("referential.ll.common.VesselActivity.id"));
        return fr.ird.observe.dto.referential.ll.common.VesselActivityReference.of(ReferentialLocale.FR, dto);
    }

    public static VesselActivityReference psVesselActivityReference() {
        VesselActivityDto dto = new VesselActivityDto();
        dto.setId(getVariable("referential.ps.common.VesselActivity.id"));
        return VesselActivityReference.of(ReferentialLocale.FR, dto);
    }

    public static SampleTypeReference newSampleType() {
        fr.ird.observe.dto.referential.ps.common.SampleTypeDto dto = new fr.ird.observe.dto.referential.ps.common.SampleTypeDto();
        dto.setId(getVariable("referential.ps.common.SampleType.id"));
        return fr.ird.observe.dto.referential.ps.common.SampleTypeReference.of(ReferentialLocale.FR, dto);
    }

    @Override
    public void loadData(ObserveServicesProvider servicesProvider, ReferenceService service) {
        TripReference actual = service.loadData(TripReference.class, getProperty("loadData.id"), null);
        Assert.assertNotNull(actual);
    }

    @Override
    public void createData(ObserveServicesProvider servicesProvider, ReferenceService service) {
        TripReference actual = service.createData(TripReference.class);
        Assert.assertNotNull(actual);
    }

    @Override
    public void createReferential(ObserveServicesProvider servicesProvider, ReferenceService service) {
        fr.ird.observe.dto.referential.ps.common.ProgramReference actual = service.createReferential(fr.ird.observe.dto.referential.ps.common.ProgramReference.class);
        Assert.assertNotNull(actual);
    }

    @Override
    public void loadReferential(ObserveServicesProvider servicesProvider, ReferenceService service) {
        fr.ird.observe.dto.referential.ps.common.ProgramReference actual = service.loadReferential(fr.ird.observe.dto.referential.ps.common.ProgramReference.class, getProperty("loadReferential.id"));
        Assert.assertNotNull(actual);
    }
}
