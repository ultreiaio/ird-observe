package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportColumnRenderersParameters;
import fr.ird.observe.report.ReportVariable;
import fr.ird.observe.report.definition.ReportRequestDefinition;
import fr.ird.observe.report.definition.RequestLayout;
import fr.ird.observe.test.ToolkitFixtures;
import io.ultreia.java4all.util.matrix.DataMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.beans.Introspector;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import static fr.ird.observe.test.ObserveFixtures.WITH_ASSERT;

/**
 * Created on 16/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public abstract class ReportFixture {

    private static final Logger log = LogManager.getLogger(ReportFixture.class);

    private final ReportServiceFixtures serviceFixtures;

    protected ReportFixture() {
        serviceFixtures = new ReportServiceFixtures() {
            @Override
            public String getServiceResourcePath() {
                return super.getServiceResourcePath() + "-" + getReportId();
            }
        };
    }

    protected ReportServiceFixtures getServiceFixtures() {
        return serviceFixtures;
    }

    protected String getReportId() {
        return Introspector.decapitalize(getClass().getSimpleName()).replace("ReportFixture", "");
    }

    public Report populateVariables(ReportService service, Report report) {
        report = service.populateVariables(report, getTripIds());
        setVariables(report);
        Assert.assertNotNull(report);
        return report;
    }

    public final DataMatrix execute(ReportService service, Report report) {
        log.warn("Starting report {}", report.getName());
        return service.executeReport(report, getTripIds());
    }

    protected Set<String> getTripIds() {
        String ids = getServiceFixtures().getProperty("tripIds");
        if (ids == null) {
            String reportId = getReportId();
            String prefix = reportId.substring(0, 2).toUpperCase();
            ids = getServiceFixtures().getProperty(prefix + "_COMMON_TRIP");
        }
        return new LinkedHashSet<>(Arrays.asList(ids.split("\\s*,\\s*")));
    }

    protected void setVariables(Report report) {
        // by default, nothing to add
    }

    @SuppressWarnings({"rawtypes", "unchecked", "SameParameterValue"})
    protected void setVariableValue(Report report, String variableName, String id) {
        for (ReportVariable variable : report.getVariables()) {
            if (variableName.equals(variable.getName())) {
                Object value = variable.getValues().stream()
                        .filter(DtoReference.newIdPredicate(id))
                        .findFirst()
                        .orElse(null);
                variable.setSelectedValue(value);
            }
        }
    }

    public void assertSyntax(Report actual) {
        Assert.assertNotNull(actual);
        assertReportName(actual);
        assertReportDimension(actual);
        assertReportNbRequests(actual);
    }

    public void initColumnRendererParameters(ReportService service, Report definition) {
        ReportColumnRenderersParameters actual = service.initColumnRendererParameters(definition.definition());
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getColumnRendererParameters());
        Assert.assertEquals(definition.definition().getColumnRenderers().length, actual.getColumnRendererParameters().size());
    }

    public final void assertResult(DataMatrix actual) {
        log.info(String.format("Start assertResult for %s", getReportId()));
        if (WITH_ASSERT) {
            int columns = getServiceFixtures().getIntegerProperty("result.columns");
            int rows = getServiceFixtures().getIntegerProperty("result.rows");
            assertResultDimension(actual, columns, rows);
            assertResult0(actual);
        } else {
            ToolkitFixtures.addAssert("result.columns", actual.getWidth());
            int rows = actual.getHeight();
            ToolkitFixtures.addAssert("result.rows", rows);
            for (int i = 0; i < rows; i++) {
                ToolkitFixtures.writeArrayAssert(String.format("result.%d", i), actual.getData()[i]);
            }
            System.out.println("New asserts:\n" + String.join("\n", ToolkitFixtures.getAsserts()));
            ToolkitFixtures.getAsserts().clear();
        }
    }

    protected final void assertResult0(DataMatrix actual) {
        int rows = actual.getHeight();
        for (int i = 0; i < rows; i++) {
            String[] data = getServiceFixtures().getArrayProperty("result." + i);
            assertResultRow(actual, i, data);
        }
    }

    protected void assertReportName(Report report) {
        if (WITH_ASSERT) {
            String name = getServiceFixtures().getProperty("syntax.name");
            String description = getServiceFixtures().getProperty("syntax.description");
            Assert.assertEquals(name, report.getName());
            Assert.assertEquals(description, report.getDescription().replaceAll("\\n", "-"));
        } else {
            ToolkitFixtures.addAssert("syntax.name", report.getName());
            ToolkitFixtures.addAssert("syntax.description", report.getDescription().replaceAll("\\n", "-"));
        }
    }

    protected void assertReportDimension(Report report) {
        if (WITH_ASSERT) {
            int columns = getServiceFixtures().getIntegerProperty("syntax.columns");
            int rows = getServiceFixtures().getIntegerProperty("syntax.rows");
            String[] columnsHeader = getServiceFixtures().getArrayProperty("syntax.columnsHeader");
            String[] rowsHeader = getServiceFixtures().getArrayProperty("syntax.rowsHeader");

            Assert.assertEquals(rows, report.getRows());
            Assert.assertEquals(columns, report.getColumns());
            if (report.getColumnHeaders() == null) {
                Assert.assertEquals(0, columnsHeader.length);
            } else {
                Assert.assertArrayEquals(columnsHeader, report.getColumnHeaders());
            }
            if (report.getRowHeaders() == null) {
                Assert.assertEquals(0, rowsHeader.length);
            } else {
                Assert.assertArrayEquals(rowsHeader, report.getRowHeaders());
            }
        } else {
            ToolkitFixtures.addAssert("syntax.rows", report.getRows());
            ToolkitFixtures.addAssert("syntax.columns", report.getColumns());
            if (report.getColumnHeaders() == null) {
                ToolkitFixtures.writeArrayAssert("syntax.columnsHeader");
            } else {
                ToolkitFixtures.writeArrayAssert("syntax.columnsHeader", (Object[]) report.getColumnHeaders());
            }
            if (report.getRowHeaders() == null) {
                ToolkitFixtures.writeArrayAssert("syntax.rowsHeader");
            } else {
                ToolkitFixtures.writeArrayAssert("syntax.rowsHeader", (Object[]) report.getRowHeaders());
            }
        }
    }

    protected void assertReportNbRequests(Report report) {
        ReportRequestDefinition[] requests = report.getRequests();
        int actual = requests.length;
        if (WITH_ASSERT) {
            int expected = getServiceFixtures().getIntegerProperty("syntax.nbRequests");
            Assert.assertNotNull(requests);
            Assert.assertEquals(expected, actual);
        } else {
            ToolkitFixtures.addAssert("syntax.nbRequests", actual);
        }
    }

    protected Iterator<ReportRequestDefinition> getRequestIterator(Report report) {
        return Arrays.asList(report.getRequests()).iterator();
    }

    protected void assertReportRequestDimension(ReportRequestDefinition request,
                                                RequestLayout layout,
                                                int x,
                                                int y) {
        Assert.assertEquals(layout, request.getLayout());
//        Assert.assertEquals(new Point(width, height), request.getLocation());
        Assert.assertEquals(x, request.getX());
        Assert.assertEquals(y, request.getY());
    }

    protected void assertReportRequestDimension(Iterator<ReportRequestDefinition> iterator, RequestLayout layout, int x, int y) {
        if (WITH_ASSERT) {
            Assert.assertTrue(iterator.hasNext());
            ReportRequestDefinition request = iterator.next();
            Assert.assertEquals(layout, request.getLayout());
//        Assert.assertEquals(new Point(width, height), request.getLocation());
            Assert.assertEquals(x, request.getX());
            Assert.assertEquals(y, request.getY());
        }
    }

    protected void assertResultDimension(DataMatrix result, int width, int height) {
        Assert.assertEquals(width, result.getWidth());
        Assert.assertEquals(height, result.getHeight());
        Assert.assertEquals(0, result.getX());
        Assert.assertEquals(0, result.getY());
    }

    protected void assertResultRow(DataMatrix result, int rowId, String... row) {
        Object[] actualRow = result.getData()[rowId];
        int index = 0;
        for (Object o : actualRow) {
            if (o == null) {
                Assert.assertEquals(row[index++], "null");
            } else {
                String[] excepted = row[index++].split("\\s*\\n\\s*");
                for (int i = 0; i < excepted.length; i++) {
                    excepted[i] = excepted[i].trim();
                }
                String[] actual = o.toString().split("\\s*\\n\\s*");
                for (int i = 0; i < actual.length; i++) {
                    actual[i] = actual[i].trim();
                }

                Assert.assertArrayEquals(excepted, actual);
            }
        }
        //Assert.assertArrayEquals(row, actualRow);
    }
}
