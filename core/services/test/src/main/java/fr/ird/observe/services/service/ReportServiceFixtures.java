package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.report.ColumnRendererConsumers;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportColumnRenderersParameters;
import fr.ird.observe.report.definition.DefaultByClassPathReportDefinitionsBuilder;
import fr.ird.observe.report.definition.ReportDefinition;
import fr.ird.observe.report.definition.ReportDefinitionsBuilder;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.util.matrix.DataMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Assume;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TreeMap;

@SuppressWarnings("ConstantConditions")
public class ReportServiceFixtures extends GeneratedReportServiceFixtures {
    private static final Logger log = LogManager.getLogger(ReportServiceFixtures.class);

    /**
     * Dictionary of reports indexed by their name.
     */
    protected static Map<String, Report> reports;
    protected static Map<String, ReportFixture> reportsFixtures;

    public static Set<String> getReportIds() {
        return getReports().keySet();
    }

    public static Map<String, ReportFixture> getReportsFixtures() {
        if (reportsFixtures == null) {
            reportsFixtures = new TreeMap<>();
            for (ReportFixture reportFixture : ServiceLoader.load(ReportFixture.class, ReportServiceFixtures.class.getClassLoader())) {
                reportsFixtures.put(reportFixture.getReportId(), reportFixture);
            }
            log.info(String.format("Found %d report fixtures.", reportsFixtures.size()));
        }
        return reportsFixtures;
    }

    public static Map<String, Report> getReports() {
        if (reports == null) {
            reports = new TreeMap<>();
            URL reportLocation = Objects.requireNonNull(ReportServiceFixtures.class.getResource(ReportDefinitionsBuilder.REPORT_LIST_LOCATION));
            try {
                log.info("Loading reports from " + reportLocation);
                List<ReportDefinition> reportList = DefaultByClassPathReportDefinitionsBuilder.build(reportLocation);
                reportList.forEach(r -> reports.put(r.getId(), r.toReport()));
            } catch (IOException e) {
                throw new IllegalStateException("Can't load reports", e);
            }
            Assert.assertNotNull(reports);
            Assert.assertFalse(reports.isEmpty());
        }
        return reports;
    }

    @Override
    public void executeReport(ObserveServicesProvider servicesProvider, ReportService service) {
        for (String id : ReportServiceFixtures.getReportIds()) {
            executeReport(service, id);
        }
    }

    @Override
    public void initColumnRendererParameters(ObserveServicesProvider servicesProvider, ReportService service) {
        for (String id : ReportServiceFixtures.getReportIds()) {
            Report definition = getReport(id);
            initColumnRendererParameters(definition, service);
        }
    }

    @Override
    public void populateVariables(ObserveServicesProvider servicesProvider, ReportService service) {
        for (String id : ReportServiceFixtures.getReportIds()) {
            Report definition = getReport(id);
            populateVariables(definition, service);
        }
    }

    public void executeReport(ReportService service, String id) {
        Report definition = getReport(id);
        executeReport(definition, service);
    }

    protected void initColumnRendererParameters(Report definition, ReportService service) {
        ReportFixture reportFixture = getReportFixture(definition);
        if (reportFixture == null) {
            Assume.assumeFalse(String.format("Missing report fixture for %s", definition.getId()), true);
            return;
        }
        reportFixture.initColumnRendererParameters(service, definition);
    }

    protected void populateVariables(Report definition, ReportService service) {
        ReportFixture reportFixture = getReportFixture(definition);
        if (reportFixture == null) {
            Assume.assumeFalse(String.format("Missing report fixture for %s", definition.getId()), true);
            return;
        }
        reportFixture.assertSyntax(definition);
        Report actual = reportFixture.populateVariables(service, definition);
        Assert.assertNotNull(actual);
    }

    protected void executeReport(Report definition, ReportService service) {
        ReportFixture reportFixture = getReportFixture(definition);
        if (reportFixture == null) {
            Assume.assumeFalse(String.format("Missing report fixture for %s", definition.getId()), true);
            return;
        }
        reportFixture.assertSyntax(definition);
        if (definition.needInitColumnRendererParameters()) {
            ReportColumnRenderersParameters renderersParameters = service.initColumnRendererParameters(definition.definition());
            Assert.assertNotNull(renderersParameters);
            Set<String> columnRendererFunctions = ColumnRendererConsumers.htmlFunctions(renderersParameters);
            Assert.assertNotNull(columnRendererFunctions);
            List<String> columnRendererInitCode = ColumnRendererConsumers.htmlInitCode(renderersParameters);
            Assert.assertNotNull(columnRendererInitCode);
        }
        Report report = reportFixture.populateVariables(service, definition);
        if (report == null) {
            return;
        }
        DataMatrix actual = reportFixture.execute(service, report);
        if (actual == null) {
            return;
        }
        Assert.assertNotNull(actual);
        reportFixture.assertResult(actual);
    }

    protected Report getReport(String reportId) {
        Map<String, Report> reports = getReports();
        Report result = reports.get(reportId);
        Assert.assertNotNull("Could not find report with id " + reportId, ReportServiceFixtures.reports);
        return result;
    }

    protected ReportFixture getReportFixture(Report report) {
//        ReportFixture reportFixture = getReportsFixtures().get(report.getId());
//        if (reportFixture != null) {
//            reportFixture.setServiceFixtures(this);
//        }
//        return reportFixture;
        return getReportsFixtures().get(report.getId());
    }

    public void assertReports(Map<String, Report> reports) {
        Assert.assertNotNull(reports);
        int expectedCount = getIntegerProperty("report.count");
        int actualCount = reports.size();
        assertFixture("report.count", expectedCount, actualCount);

    }
}
