package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.logbook.ActivityDto;
import fr.ird.observe.dto.data.ps.logbook.SampleActivityDto;
import fr.ird.observe.dto.data.ps.logbook.WellActivityDto;
import fr.ird.observe.dto.referential.ps.common.ProgramDto;
import fr.ird.observe.services.ObserveServicesProvider;
import org.junit.Assert;

import java.util.Map;
import java.util.Set;

public class UsageServiceFixtures extends GeneratedUsageServiceFixtures {

    @Override
    public void countReferential(ObserveServicesProvider servicesProvider, UsageService service) {
        ToolkitIdDtoBean request = ToolkitIdDtoBean.of(ProgramDto.class, getProperty("countReferential.id"));
        Map<Class<? extends BusinessDto>, Long> actual = service.countReferential(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("countReferential.count"), actual.size());
    }

    @Override
    public void countReferentialInData(ObserveServicesProvider servicesProvider, UsageService service) {
        ToolkitIdDtoBean request = ToolkitIdDtoBean.of(ProgramDto.class, getProperty("countReferential.id"));
        Map<Class<? extends BusinessDto>, Long> actual = service.countReferential(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("countReferential.count.in.data"), actual.size());
    }

    @Override
    public void findReferential(ObserveServicesProvider servicesProvider, UsageService service) {
        ToolkitIdDtoBean request = ToolkitIdDtoBean.of(ProgramDto.class, getProperty("findReferential.id"));
        Set<ToolkitIdLabel> actual = service.findReferential(request, TripDto.class);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("findReferential.trip.count"), actual.size());
    }

    //FIXME Rewrite this test with a correct data (with optional dependencies)
//    @Override
//    public void countOptionalData(ObserveServicesProvider servicesProvider, UsageService service) {
//        ToolkitIdDtoBean request = ToolkitIdDtoBean.of(ActivityDto.class, getProperty("countOptionalData.id"));
//        Map<Class<? extends BusinessDto>, Long> actual = service.countData(request);
//        Assert.assertNotNull(actual);
//        Assert.assertEquals(getIntegerProperty("countOptionalData.count"), actual.size());
//        countOptionalData(SampleActivityDto.class, actual);
//        countOptionalData(WellActivityDto.class, actual);
//    }

    @Override
    public void countMandatoryData(ObserveServicesProvider servicesProvider, UsageService service) {
        ToolkitIdDtoBean request = ToolkitIdDtoBean.of(ActivityDto.class, getProperty("countMandatoryData.id"));
        Map<Class<? extends BusinessDto>, Long> actual = service.countMandatoryData(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("countMandatoryData.count"), actual.size());
        countMandatoryData(SampleActivityDto.class, actual);
        countMandatoryData(WellActivityDto.class, actual);
    }

    @Override
    public void findMandatoryData(ObserveServicesProvider servicesProvider, UsageService service) {
        ToolkitIdDtoBean request = ToolkitIdDtoBean.of(ActivityDto.class, getProperty("findMandatoryData.id"));
        findMandatoryData(service, SampleActivityDto.class, request);
        findMandatoryData(service, WellActivityDto.class, request);
    }

    private void findMandatoryData(UsageService service, Class<? extends DataDto> dtoType, ToolkitIdDtoBean request) {
        Set<ToolkitIdLabel> actual = service.findMandatoryData(request, dtoType);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty(String.format("findMandatoryData.%s.count", dtoType.getSimpleName().replace("Dto", ""))), actual.size());
    }

    private void countMandatoryData(Class<? extends DataDto> dtoType, Map<Class<? extends BusinessDto>, Long> actual) {
        Assert.assertEquals(getIntegerProperty("countMandatoryData.count"), actual.size());
        Assert.assertNotNull(actual.get(dtoType));
        Assert.assertEquals(getIntegerProperty(String.format("countMandatoryData.%s.count", dtoType.getSimpleName().replace("Dto", ""))), (long) actual.get(dtoType));
    }

    private void countOptionalData(Class<? extends DataDto> dtoType, Map<Class<? extends BusinessDto>, Long> actual) {
        Assert.assertEquals(getIntegerProperty("countOptionalData.count"), actual.size());
        Assert.assertNotNull(actual.get(dtoType));
        Assert.assertEquals(getIntegerProperty(String.format("countOptionalData.%s.count", dtoType.getSimpleName().replace("Dto", ""))), (long) actual.get(dtoType));
    }
}
