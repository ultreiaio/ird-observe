package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.collect.ArrayListMultimap;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.validation.DtoValidationContext;
import fr.ird.observe.dto.validation.SeineBycatchObservedSystemConfig;
import fr.ird.observe.dto.validation.ValidationRequestConfiguration;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import fr.ird.observe.validation.api.request.DataValidationRequest;
import fr.ird.observe.validation.api.request.ReferentialValidationRequest;
import fr.ird.observe.validation.api.result.ValidationResult;
import fr.ird.observe.validation.api.result.ValidationResultDto;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.impl.java.NuitonValidatorProviderFactoryImpl;
import org.junit.Assert;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ValidateServiceFixtures extends GeneratedValidateServiceFixtures {

    public static final String DATA_RESULT_JSON = "dataResult-%s.json";
    public static final String REFERENTIAL_RESULT_JSON = "referentialResult-%s.json";
    private String defaultFactoryName = NuitonValidatorProviderFactoryImpl.PROVIDER_NAME;

    public void useDefaultMode() {
        this.defaultFactoryName = NuitonValidatorProviderFactoryImpl.PROVIDER_NAME;
    }

    @Override
    public void validateData(ObserveServicesProvider servicesProvider, ValidateService service) {
        NuitonValidatorProviders.setDefaultFactoryName(defaultFactoryName);
        validateData(service, "ps");
        validateData(service, "ll");
    }

    @Override
    public void validateReferential(ObserveServicesProvider servicesProvider, ValidateService service) {
        NuitonValidatorProviders.setDefaultFactoryName(defaultFactoryName);
        validateReferential(service, "common");
        validateReferential(service, "ps");
        validateReferential(service, "ll");

    }

    private void validateData(ValidateService service, String classifier) {
        String[] id = getArrayProperty("validateData.id." + classifier);
        ValidationRequestConfiguration configuration = new ValidationRequestConfiguration();
        configuration.setValidationSpeedMaxValue(100f);
        configuration.setValidationSpeedEnable(true);
        configuration.setValidationLengthWeightEnable(true);
        configuration.setValidationUseDisabledReferential(true);
        SeineBycatchObservedSystemConfig seineBycatchObservedSystemConfig = new SeineBycatchObservedSystemConfig();
        ArrayListMultimap<String, String> data = ArrayListMultimap.create();
        data.put("fr.ird.referential.common.Species#1239832684537#0.2397229787936519", "fr.ird.referential.ps.common.ObservedSystem#0#1.0");
        seineBycatchObservedSystemConfig.setData(data);
        configuration.setSeineBycatchObservedSystemConfig(seineBycatchObservedSystemConfig);

        DataValidationRequest request = new DataValidationRequest();
        request.setDataIds(new LinkedHashSet<>(Arrays.asList(id)));
        request.setScopes(new LinkedHashSet<>(Arrays.asList(NuitonValidatorScope.values())));
        request.setValidationContext(DtoValidationContext.UPDATE_VALIDATION_CONTEXT);

        ValidationResult actual = service.validateData(configuration, request);
        Assert.assertNotNull(actual);

        doAssert(DATA_RESULT_JSON, classifier, actual);
    }

    private void validateReferential(ValidateService service, String classifier) {
        ValidationRequestConfiguration configuration = new ValidationRequestConfiguration();

        ReferentialValidationRequest request = new ReferentialValidationRequest();
        BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByName(classifier);
        Set<Class<? extends ReferentialDto>> referentialTypes = businessModule.getReferentialTypes();
        referentialTypes = referentialTypes.stream().sorted(Comparator.comparing(Class::getName)).collect(Collectors.toCollection(LinkedHashSet::new));
        request.setReferentialTypes(referentialTypes);
        request.setScopes(new LinkedHashSet<>(Arrays.asList(NuitonValidatorScope.values())));
        request.setValidationContext(DtoValidationContext.UPDATE_VALIDATION_CONTEXT);
        ValidationResult actual = service.validateReferential(configuration, request);
        Assert.assertNotNull(actual);

        doAssert(REFERENTIAL_RESULT_JSON, classifier, actual);
    }

    private void doAssert(String resultLocationPattern, String classifier, ValidationResult actual) {
        String resultLocation = String.format(resultLocationPattern, classifier + "-" + defaultFactoryName);
        if (WITH_ASSERT) {
            ValidationResult expected = loadJson(ValidationResult.class, resultLocation);
            assertResult(actual, expected);
        } else {
            storeJson(actual, resultLocation);
        }
    }

    private void assertResult(ValidationResult actual, ValidationResult expected) {
        Assert.assertEquals(expected.getNodes().size(), actual.getNodes().size());

        Iterator<ValidationResultDto> expectedIterator = expected.getNodes().iterator();
        for (ValidationResultDto actualNode : actual.getNodes()) {
            ValidationResultDto expectedNode = expectedIterator.next();
            assertResult(actualNode, expectedNode);
        }
    }

    private void assertResult(ValidationResultDto actual, ValidationResultDto expected) {
        Assert.assertEquals(expected.getDatum().getId(), actual.getDatum().getId());
        Assert.assertEquals(expected.getMessages(), actual.getMessages());
        Assert.assertEquals(expected.getChildCount(), actual.getChildCount());
        if (actual.getChildren() != null) {
            Iterator<ValidationResultDto> actualChildren = actual.getChildren().iterator();
            Iterator<ValidationResultDto> expectedChildren = expected.getChildren().iterator();
            while (actualChildren.hasNext()) {
                ValidationResultDto actualChildNode = actualChildren.next();
                ValidationResultDto expectedChildNode = expectedChildren.next();
                assertResult(actualChildNode, expectedChildNode);
            }
        }
    }

}
