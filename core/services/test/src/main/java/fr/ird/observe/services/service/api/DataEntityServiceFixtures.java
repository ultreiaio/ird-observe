package fr.ird.observe.services.service.api;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.TripAware;
import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.navigation.tree.io.request.ToolkitRequestConfig;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import fr.ird.observe.spi.json.JsonHelper;
import fr.ird.observe.spi.module.BusinessDataPackage;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProjectVisitor;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.lang.Objects2;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Assume;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

public class DataEntityServiceFixtures extends GeneratedDataEntityServiceFixtures {

    private static final Logger log = LogManager.getLogger(DataEntityServiceFixtures.class);

    final Gson gson = new DtoGsonSupplier(true).get();


    public static List<?> assertStatesAndGet(ToolkitTreeNodeStates actual, Predicate<Integer> predicate) {
        Assert.assertNotNull(actual);
        String content = actual.getState("content");
        List<?> map = new GsonBuilder().create().fromJson(content, (Type) Object.class);
        int actualCount = map.size();
        Assert.assertTrue("Found actual count: " + actualCount, predicate.test(actualCount));
        return map;
    }

    public static int assertStates(ToolkitTreeNodeStates actual, Predicate<Integer> predicate) {
        Assert.assertNotNull(actual);
        String content = actual.getState("content");
        Object map = new GsonBuilder().create().fromJson(content, Object.class);
        int actualCount;
        if (map instanceof Collection) {
            actualCount = ((Collection<?>) map).size();
        } else {
            actualCount = 1;
        }
        Assert.assertTrue(String.format("Found actual count: %d for: %s", actualCount, map), predicate.test(actualCount));
        return actualCount;
    }

    public static void assertStates(ToolkitTreeNodeStates actual, int exceptedResult) {
        assertStates(actual, i -> exceptedResult == i);
    }

    public List<?> fromJsonList(String content) {
        return gson.fromJson(content, (Type) Object.class);
    }

    @Override
    public void getSome(ObserveServicesProvider servicesProvider, DataEntityService service) {
        for (Map.Entry<String, String> entry : IDS.entrySet()) {
            String key = entry.getKey();
            String dtoName = constantToType(key);
            if (dtoName == null) {
                continue;
            }
            String id = entry.getValue();
            log.info(String.format("Look for id: %s", id));
            Class<? extends DataDto> dtoType = Objects2.forName(dtoName);
            ToolkitTreeNodeStates actual = service.getSome(dtoType, new ToolkitRequestConfig(true, true, false, true, false), Map.of("topiaVersion", "min::0"));
            assertStates(actual, l -> l > 0);
        }
    }

    @Override
    public void getOne(ObserveServicesProvider servicesProvider, DataEntityService service) {
        for (Map.Entry<String, String> entry : IDS.entrySet()) {
            String key = entry.getKey();
            String dtoName = constantToType(key);
            if (dtoName == null) {
                continue;
            }
            String id = entry.getValue();
            log.info(String.format("Look for id: %s", id));
            Class<? extends DataDto> dtoType = Objects2.forName(dtoName);
            ToolkitTreeNodeStates actual = service.getOne(dtoType, new ToolkitRequestConfig(true, true, false, true, false), id);
            assertStates(actual, 1);
        }
    }

    public void getPsTrip(DataEntityService service) {
        ToolkitRequestConfig config = new ToolkitRequestConfig(true, false, true, false, false);
        {
            // all (not authorized)
            try {
                service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                Map.of(),
                                Map.of()
                );
                //noinspection ConstantConditions
                Assume.assumeFalse("Should not be able to perform a all get on data", true);
            } catch (Exception ignored) {
                // normal behavior
            }
        }
        {
            String id = IDS.get("PS_COMMON_TRIP");
            // on id fr.ird.data.ps.common.Trip#1553970328158#0.44863535394269627
            ToolkitTreeNodeStates actual = service.getOne(fr.ird.observe.dto.data.ps.common.TripDto.class, config, id);
            assertStates(actual, 1);
        }

        {
            // captain null
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("captain_id", "null::"));
            assertStates(actual, 2);
        }
        {
            // captain not null
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("captain_id", "not_null::"));
            assertStates(actual, 2);
        }
        {
            // start Date 2019-03-30
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("startDate", "2019-03-30"));
            assertStates(actual, 1);
        }
        {
            // start Date >= 2019-03-30
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("startDate", "min::2019-03-30"),
                                                           Map.of("startDate", OrderEnum.ASC)
            );
            assertStates(actual, 2);
        }
        {
            // start Date <= 2019-03-30
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("startDate", "max::2019-03-30")
            );
            assertStates(actual, 3);
        }
        {
            // start Date = 2019-03-30
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("startDate", "min::2019-03-30~max::2019-03-30"));
            assertStates(actual, 1);
        }
        {
            // end Date 2019-04-08
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("endDate", "2019-04-08"));
            assertStates(actual, 1);
        }
        {
            // end Date >= 2019-04-08
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("endDate", "mIn::2019-04-08"));
            assertStates(actual, 2);
        }
        {
            // end Date <= 2019-04-08
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("endDate", "mAX::2019-04-08"));
            assertStates(actual, 3);
        }
        {
            // ocean fr.ird.referential.common.Ocean#1239832686151#0.17595105505051245
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("ocean_id", "fr.ird.referential.common.Ocean#1239832686151#0.17595105505051245"));
            assertStates(actual, 2);
        }
        {
            // ocean_label = %Atl%
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("ocean_label", "%Atl%"));
            assertStates(actual, 2);
        }
        {
            // vessel_label = CAP%
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("vessel_label", "CAP%"));
            assertStates(actual, 1);

            service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                            Map.of("vessel_label", "cap%"));
            assertStates(actual, 1);
            service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                            Map.of("vessel_label", "CAP BoJADOR")
            );
            assertStates(actual, 1);
        }
        {
            // vessel
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("vessel_id", "fr.ird.referential.common.Vessel#1239832679425#0.9136908731720471"));
            assertStates(actual, 1);
        }
        {
            // observationsProgram fr.ird.referential.ps.common.Program#1363095174385#0.011966550987014823
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("observationsProgram_id", "fr.ird.referential.ps.common.Program#1363095174385#0.011966550987014823"));
            assertStates(actual, 1);
        }
        {
            // observationsProgram_code = 0
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("observationsProgram_code", "0"));
            assertStates(actual, 1);
        }
        {
            // observationsProgram_label = CAP%
            ToolkitTreeNodeStates actual = service.getSome(fr.ird.observe.dto.data.ps.common.TripDto.class, config,
                                                           Map.of("observationsProgram_label", "%ICCAT%"));
            assertStates(actual, 1);
        }
    }

    public void getLlTrip(DataEntityService service) {
        ToolkitRequestConfig config = new ToolkitRequestConfig(true, false, true, false, false);
        {
            // all (not authorized)
            try {
                service.getSome(TripDto.class, config, Map.of());
                //noinspection ConstantConditions
                Assume.assumeFalse("Should not be able to perform a all get on data", true);
            } catch (Exception ignored) {
                // normal behavior
            }
        }
        {
            String id = IDS.get("LL_COMMON_TRIP");
            ToolkitTreeNodeStates actual = service.getOne(TripDto.class, config, id);
            assertStates(actual, 1);
        }
        {
            ToolkitTreeNodeStates actual = service.getSome(TripDto.class, config,
                                                           Map.of("startDate", "2018-08-24"));
            assertStates(actual, 1);
        }
        {
            ToolkitTreeNodeStates actual = service.getSome(TripDto.class, config,
                                                           Map.of("endDate", "2019-11-04"));
            assertStates(actual, 1);
        }
        {
            ToolkitTreeNodeStates actual = service.getSome(TripDto.class, config,
                                                           Map.of("ocean_id", "fr.ird.referential.common.Ocean#1239832686152#0.8325731048817705"));
            assertStates(actual, 2);
        }
        {
            ToolkitTreeNodeStates actual = service.getSome(TripDto.class, config,
                                                           Map.of("vessel_id", "fr.ird.referential.common.Vessel#1433499225457#0.75786798959598"));
            assertStates(actual, 1);
        }
        {
            ToolkitTreeNodeStates actual = service.getSome(TripDto.class, config,
                                                           Map.of("observationsProgram_id", "fr.ird.referential.ll.common.Program#1239832686139#0.1"));
            assertStates(actual, 1);
        }
    }

    @Override
    public void delete(ObserveServicesProvider servicesProvider, DataEntityService service) {
        Gson gson = new DtoGsonSupplier(true).get();
        ObserveBusinessProject.get().accept(new BusinessProjectVisitor() {
            @Override
            public void enterSubModuleDataType(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage dataPackage, Class<? extends DataDto> dtoType) {
                if (RootOpenableDto.class.isAssignableFrom(dtoType)) {
                    try {
                        String content = loadFixture(module, subModule, dtoType, "create.json");
                        content = JsonHelper.removeProperties(gson, content, ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID, ToolkitId.PROPERTY_TOOLKIT_CREATE_DATE, ToolkitId.PROPERTY_TOOLKIT_LAST_UPDATE_DATE);
                        @SuppressWarnings("unchecked") Class<? extends RootOpenableDto> dtoType1 = (Class<? extends RootOpenableDto>) dtoType;
                        testDelete(service, dtoType1, content);
                    } catch (IOException e) {
                        log.warn(String.format("No fixture for %s", dtoType));
                    } catch (InvalidDataException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
    }

    private void testDelete(DataEntityService service, Class<? extends RootOpenableDto> dtoType, String content) throws InvalidDataException {
        log.info(String.format("for type: %s", dtoType.getName()));
        ToolkitTreeNodeStates states = testCreate(service, dtoType, content);
        String content2 = states.getState("content");
        @SuppressWarnings("unchecked") Map<String, Object> resultObject = (Map<String, Object>) fromJsonList(content2).get(0);
        Assert.assertNotNull(resultObject);
        String id = (String) resultObject.get(ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID);
        service.delete(dtoType, Objects.requireNonNull(id));
        ToolkitTreeNodeStates getResult = service.getOne(dtoType, new ToolkitRequestConfig(), id);
        Assert.assertNotNull(getResult);
        Object contentState = getResult.getState("content");
        Assert.assertNotNull(contentState);
        Assert.assertEquals("[]", contentState);
    }

    @Override
    public void create(ObserveServicesProvider servicesProvider, DataEntityService service) {
        ObserveBusinessProject businessProject = ObserveBusinessProject.get();
        businessProject.accept(new BusinessProjectVisitor() {
            @Override
            public void enterSubModuleDataType(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage DataPackage, Class<? extends DataDto> dtoType) {
                Class<?> mainDtoType = businessProject.getMapping().getMainDtoType(dtoType);
                if (mainDtoType != dtoType && !EditableDto.class.isAssignableFrom(dtoType)) {
                    return;
                }
                //FIXME Remove this: just need to have a parent id to attach data
                //FIXME Need to add a new method in DataService createFromParent(parentId,...)
                if (!(TripAware.class.isAssignableFrom(dtoType))) {
                    return;
                }
                try {
                    String content = loadFixture(module, subModule, dtoType, "create.json");
                    testCreate(service, dtoType, content);
                } catch (IOException e) {
                    log.warn(String.format("No fixture for %s", dtoType));
                } catch (InvalidDataException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public void update(ObserveServicesProvider servicesProvider, DataEntityService service) {
        ObserveBusinessProject businessProject = ObserveBusinessProject.get();
        businessProject.accept(new BusinessProjectVisitor() {
            @Override
            public void enterSubModuleDataType(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage DataPackage, Class<? extends DataDto> dtoType) {
                Class<?> mainDtoType = businessProject.getMapping().getMainDtoType(dtoType);
                if (mainDtoType != dtoType && !EditableDto.class.isAssignableFrom(dtoType)) {
                    return;
                }
                if (RootOpenableDto.class.isAssignableFrom(dtoType)) {
                    // This will regenerate some ids, can't do it here
                    return;
                }
                try {
                    String content = loadFixture(module, subModule, dtoType, "content.json");
                    testUpdate(service, dtoType, null, content);
                } catch (IOException e) {
                    log.warn(String.format("No fixture for %s", dtoType));
                } catch (InvalidDataException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    public ToolkitTreeNodeStates testCreate(DataEntityService service, Class<? extends DataDto> dtoType, String content) throws InvalidDataException {
        log.debug(String.format("for type: %s", dtoType.getName()));
        content = JsonHelper.removeProperties(gson, content, ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID, ToolkitId.PROPERTY_TOOLKIT_CREATE_DATE, ToolkitId.PROPERTY_TOOLKIT_LAST_UPDATE_DATE);

        ToolkitId actual = service.create(dtoType, content);
        Assert.assertNotNull(actual);
        Assert.assertEquals(DATE, actual.getLastUpdateDate());
        String topiaId = actual.getTopiaId();
        ToolkitRequestConfig config = new ToolkitRequestConfig();
        config.setRecursive(true);
        config.setPrettyPrint(true);
        ToolkitTreeNodeStates getResult = service.getOne(dtoType, config, topiaId);
        Assert.assertNotNull(getResult);
        return getResult;
    }

    public ToolkitTreeNodeStates testUpdate(DataEntityService service, Class<? extends DataDto> dtoType, String id, String content) throws InvalidDataException {
        log.debug(String.format("for type: %s", dtoType.getName()));
        Pair<String, List<Object>> result = JsonHelper.removeAndCollectProperties(gson, content, ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID, ToolkitId.PROPERTY_TOOLKIT_CREATE_DATE, ToolkitId.PROPERTY_TOOLKIT_LAST_UPDATE_DATE);
        content = result.getKey();
        if (id == null) {

            id = (String) result.getValue().get(0);
        }
        ToolkitId actual = service.update(dtoType, Objects.requireNonNull(id), content);
        Assert.assertNotNull(actual);
        Assert.assertEquals(id, actual.getTopiaId());
        Assert.assertEquals(DATE, actual.getLastUpdateDate());
        ToolkitTreeNodeStates getResult = service.getOne(dtoType, new ToolkitRequestConfig(), id);
        Assert.assertNotNull(getResult);
        return getResult;
    }

}


