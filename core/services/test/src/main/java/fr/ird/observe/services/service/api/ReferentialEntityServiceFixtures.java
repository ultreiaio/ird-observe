package fr.ird.observe.services.service.api;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ps.common.ProgramDto;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.navigation.tree.io.request.ToolkitRequestConfig;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import fr.ird.observe.spi.json.JsonHelper;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProjectVisitor;
import fr.ird.observe.spi.module.BusinessReferentialPackage;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ReferentialEntityServiceFixtures extends GeneratedReferentialEntityServiceFixtures {

    private static final Logger log = LogManager.getLogger(ReferentialEntityServiceFixtures.class);

    public void testProgramSort(ReferentialEntityService service) {
        ToolkitRequestConfig config = new ToolkitRequestConfig(false, false, true, false, false);
        // by id
        assertOrder(service, config, "id", 17, true);
        // by code
        assertOrder(service, config, "code", 17, false);
        // by label2
        assertOrder(service, config, "label2", 17, true);
        // organism_id
        assertOrder(service, config, "organism_id", 17, false);
        // organism_code
        assertOrder(service, config, "organism_code", 17, false);
        // organism_label
        assertOrder(service, config, "organism_label", 17, false);
        // startDate
        assertOrder(service, config, "startDate", 17, false);
        // topiaVersion
        assertOrder(service, config, "topiaVersion", 17, false);
        // topiaCreateDate
        assertOrder(service, config, "topiaCreateDate", 17, true);
        // lastUpdateDate
        assertOrder(service, config, "lastUpdateDate", 17, false);
    }

    public void testProgram(ReferentialEntityService service) {
        ToolkitRequestConfig config = new ToolkitRequestConfig(false, false, true, false, false);
        ToolkitTreeNodeStates actual;
        // all
        actual = service.getAll(ProgramDto.class, config);
        DataEntityServiceFixtures.assertStates(actual, 17);
        // on exact id
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("id", "fr.ird.referential.ps.common.Program#1239832686262#0.42751447061198444"));
        DataEntityServiceFixtures.assertStates(actual, 1);
        // bad id
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("id", "fr.ird.referential.common.Program#1239832686262#0.42751447061198444Fake"));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // equals code
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "9"));
        DataEntityServiceFixtures.assertStates(actual, 1);
        // not equals code (includes null values)
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "not_equals::9"));
        DataEntityServiceFixtures.assertStates(actual, 16);
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "not_equals::9~not_null::"));
        DataEntityServiceFixtures.assertStates(actual, 14);
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "not_equals::9~not_null::~1%"));
        DataEntityServiceFixtures.assertStates(actual, 2);

        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "not_equals::9" + System.nanoTime()));
        DataEntityServiceFixtures.assertStates(actual, 17);
        // null code
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "null::"));
        DataEntityServiceFixtures.assertStates(actual, 2);
        // not_null code
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "not_null::"));
        DataEntityServiceFixtures.assertStates(actual, 15);
        // empty code
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", ""));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // bad code
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "abc"));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // partial code
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "1%"));
        DataEntityServiceFixtures.assertStates(actual, 2);
        // partial code
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "%1"));
        DataEntityServiceFixtures.assertStates(actual, 1);
        // partial code
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "%1%"));
        DataEntityServiceFixtures.assertStates(actual, 2);
        // partial code
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("code", "%0%"));
        DataEntityServiceFixtures.assertStates(actual, 6);

        // exact lastUpdateDate
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("lastUpdateDate", "2016-12-06 15:54:27.727005"));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // lastUpdateDate_min
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("lastUpdateDate", "min::2016-12-06 15:54:27.727005"));
        DataEntityServiceFixtures.assertStates(actual, 17);
        // lastUpdateDate_max
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("lastUpdateDate", "max::2016-12-06 15:54:27.727005"));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // no startDate
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("startDate", ""));
        DataEntityServiceFixtures.assertStates(actual, 9);
        // exact startDate
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("startDate", "2019-01-01"));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // startDate_min
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("startDate", "min::2019-01-01"));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // startDate_max
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("startDate", "max::2019-01-01"));
        DataEntityServiceFixtures.assertStates(actual, 8);
        // organism_id equals
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_id", "fr.ird.referential.common.Organism#1267835067241#0.6705217825871838"));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // organism_code is null
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_code", ""));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // organism_code equals
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_code", "1"));
        DataEntityServiceFixtures.assertStates(actual, 9);
        // organism_code equals
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_code", "ABC"));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // organism_code like
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_code", "%1"));
        DataEntityServiceFixtures.assertStates(actual, 9);
        // organism_code like
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_code", "1%"));
        DataEntityServiceFixtures.assertStates(actual, 10);
        // organism_code equals
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_code", "%1%"));
        DataEntityServiceFixtures.assertStates(actual, 10);
        // organism_label equals
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_label", ""));
        DataEntityServiceFixtures.assertStates(actual, 0);
        // organism_label equals
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_label", "IRD"));
        DataEntityServiceFixtures.assertStates(actual, 9);
        // organism_label like
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_label", "%I"));
        DataEntityServiceFixtures.assertStates(actual, 1);
        // organism_label like
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_label", "I%"));
        DataEntityServiceFixtures.assertStates(actual, 12);
        // organism_label like
        actual = service.getSome(ProgramDto.class, config,
                                 Map.of("organism_label", "%I%"));
        DataEntityServiceFixtures.assertStates(actual, 14);

    }

    @Override
    public void getOne(ObserveServicesProvider servicesProvider, ReferentialEntityService service) {
        for (Class<? extends ReferentialDto> referentialType : ObserveBusinessProject.get().getReferentialTypes()) {
            ToolkitRequestConfig config = new ToolkitRequestConfig(false, true, false, true, false);
            testGet(service, config, referentialType);
        }
    }

    @Override
    public void getSome(ObserveServicesProvider servicesProvider, ReferentialEntityService service) {
        for (Class<? extends ReferentialDto> referentialType : ObserveBusinessProject.get().getReferentialTypes()) {
            ToolkitRequestConfig config = new ToolkitRequestConfig(false, true, false, true, false);
            testGetSome(service, config, referentialType);
        }
    }

    @Override
    public void getAll(ObserveServicesProvider servicesProvider, ReferentialEntityService service) {
        for (Class<? extends ReferentialDto> referentialType : ObserveBusinessProject.get().getReferentialTypes()) {
            ToolkitRequestConfig config = new ToolkitRequestConfig(false, true, false, true, false);
            testGetAll(service, config, referentialType);
        }
    }

    protected void assertOrder(ReferentialEntityService service, ToolkitRequestConfig config, String property, int exceptedCount, boolean unique) {
        // asc
        ToolkitTreeNodeStates actualAsc = service.getAll(ProgramDto.class, config, Map.of(property, OrderEnum.ASC));
        List<?> resultAsc = DataEntityServiceFixtures.assertStatesAndGet(actualAsc, i -> i == exceptedCount);
        // desc
        ToolkitTreeNodeStates actualDesc = service.getAll(ProgramDto.class, config, Map.of(property, OrderEnum.DESC));
        List<?> resultDesc = DataEntityServiceFixtures.assertStatesAndGet(actualDesc, i -> i == exceptedCount);

        Assert.assertEquals(resultAsc.size(), resultDesc.size());
        if (unique) {
            Collections.reverse(resultDesc);
            Assert.assertEquals(resultAsc.get(0), resultDesc.get(0));
        }
    }

    protected <D extends ReferentialDto> void testGetAll(ReferentialEntityService service, ToolkitRequestConfig config, Class<D> referentialType) {
        ToolkitTreeNodeStates all = service.getAll(referentialType, config);
        Assert.assertNotNull(all);
        int allCount = DataEntityServiceFixtures.assertStates(all, l -> true);
        ToolkitTreeNodeStates actual = service.getAll(referentialType, config);
        DataEntityServiceFixtures.assertStates(actual, l -> l == allCount);
    }

    protected <D extends ReferentialDto> void testGetSome(ReferentialEntityService service, ToolkitRequestConfig config, Class<D> referentialType) {
        ToolkitTreeNodeStates some = service.getSome(referentialType, config, Map.of("code", "2"));
        Assert.assertNotNull(some);
        //TODO Add more nice filter tests
    }

    protected <D extends ReferentialDto> void testGet(ReferentialEntityService service, ToolkitRequestConfig config, Class<D> referentialType) {
        ToolkitTreeNodeStates all = service.getAll(referentialType, config);
        Assert.assertNotNull(all);
        int allCount = DataEntityServiceFixtures.assertStates(all, l -> true);
        boolean empty = allCount == 0;
        ToolkitTreeNodeStates actual = service.getSome(referentialType, config,
                                                       Map.of("status", ""));
        DataEntityServiceFixtures.assertStates(actual, l -> l == 0);
        actual = service.getSome(referentialType, config,
                                 Map.of("status", "enabled"));
        int enabledCount = DataEntityServiceFixtures.assertStates(actual, empty ? l -> l == 0 : l -> l > 0);

        actual = service.getSome(referentialType, config,
                                 Map.of("status", "disabled"));
        int expectedDisabledCount = allCount - enabledCount;
        int disabledCount = DataEntityServiceFixtures.assertStates(actual, l -> l == expectedDisabledCount);
        Assert.assertEquals(allCount, enabledCount + disabledCount);
        if (empty) {
            return;
        }
        actual = service.getSome(referentialType, config,
                                 Map.of("uri", "null::"));
        DataEntityServiceFixtures.assertStates(actual, l -> l > 0);
        String allContent = (String) all.getStates().get("content");
        Assert.assertNotNull(allContent);
        int i = allContent.indexOf("\"topiaId\"");
        int j = allContent.indexOf("\"fr.", i) + 1;
        int k = allContent.indexOf("\"", j);
        String id = allContent.substring(j, k);
        actual = service.getOne(referentialType, config, id);
        Assert.assertNotNull(actual);
        String content = (String) actual.getStates().get("content");
        Assert.assertNotNull(content);
        Assert.assertTrue(content.contains(id));
    }

    @Override
    public void delete(ObserveServicesProvider servicesProvider, ReferentialEntityService service) {
        Gson gson = new DtoGsonSupplier(true).get();
        ObserveBusinessProject.get().accept(new BusinessProjectVisitor() {
            @Override
            public void enterSubModuleReferentialType(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage, Class<? extends ReferentialDto> dtoType) {
                try {
                    String content = loadFixture(module, subModule, dtoType, "create.json");
                    content = JsonHelper.removeProperties(gson, content, ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID, ToolkitId.PROPERTY_TOOLKIT_CREATE_DATE, ToolkitId.PROPERTY_TOOLKIT_LAST_UPDATE_DATE);
                    testDelete(service, dtoType, content);
                } catch (IOException e) {
                    log.warn(String.format("No fixture for %s", dtoType));
                } catch (InvalidDataException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public void create(ObserveServicesProvider servicesProvider, ReferentialEntityService service) {
        Gson gson = new DtoGsonSupplier(true).get();
        ObserveBusinessProject.get().accept(new BusinessProjectVisitor() {
            @Override
            public void enterSubModuleReferentialType(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage, Class<? extends ReferentialDto> dtoType) {
                try {
                    String content = loadFixture(module, subModule, dtoType, "create.json");
                    content = JsonHelper.removeProperties(gson, content, ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID, ToolkitId.PROPERTY_TOOLKIT_CREATE_DATE, ToolkitId.PROPERTY_TOOLKIT_LAST_UPDATE_DATE);
                    testCreate(service, dtoType, content);
                } catch (IOException e) {
                    log.warn(String.format("No fixture for %s", dtoType));
                } catch (InvalidDataException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public void update(ObserveServicesProvider servicesProvider, ReferentialEntityService service) {
        Gson gson = new DtoGsonSupplier(true).get();
        ObserveBusinessProject.get().accept(new BusinessProjectVisitor() {
            @Override
            public void enterSubModuleReferentialType(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage, Class<? extends ReferentialDto> dtoType) {
                try {
                    String content = loadFixture(module, subModule, dtoType, "content.json");
                    Pair<String, List<Object>> result = JsonHelper.removeAndCollectProperties(gson, content, ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID, ToolkitId.PROPERTY_TOOLKIT_CREATE_DATE, ToolkitId.PROPERTY_TOOLKIT_LAST_UPDATE_DATE);
                    content = result.getKey();
                    String id = (String) result.getValue().get(0);
                    testUpdate(service, dtoType, id, content);
                } catch (IOException e) {
                    log.warn(String.format("No fixture for %s", dtoType));
                } catch (InvalidDataException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public void getByModule(ObserveServicesProvider servicesProvider, ReferentialEntityService service) {
        Gson gson = new DtoGsonSupplier(true).get();
        ObserveBusinessProject.get().accept(new BusinessProjectVisitor() {
            @Override
            public void enterModule(BusinessModule module) {
                testGetByModule(service, gson, module);
            }
        });
    }

    @Override
    public void getByPackage(ObserveServicesProvider servicesProvider, ReferentialEntityService service) {
        Gson gson = new DtoGsonSupplier(true).get();
        ObserveBusinessProject.get().accept(new BusinessProjectVisitor() {
            @Override
            public void enterSubModuleReferentialPackage(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage) {
                testGetByPackage(service, gson, module, subModule);
            }
        });
    }

    private void testGetByModule(ReferentialEntityService service, Gson gson, BusinessModule module) {
        ToolkitTreeNodeStates actual = service.getByModule(module.getName(), new ToolkitRequestConfig());
        Map<String, List<Map<String, Object>>> map = testGetResult(gson, actual);
        Assert.assertEquals(module.getReferentialTypes().size(), map.size());
    }

    private void testGetByPackage(ReferentialEntityService service, Gson gson, BusinessModule module, BusinessSubModule subModule) {
        ToolkitTreeNodeStates actual = service.getByPackage(module.getName() + "_" + subModule.getName(), new ToolkitRequestConfig());
        Map<String, List<Map<String, Object>>> map = testGetResult(gson, actual);
        Assert.assertEquals(subModule.getReferentialTypes().size(), map.size());
    }

    private String testCreate(ReferentialEntityService service, Class<? extends ReferentialDto> dtoType, String content) throws InvalidDataException {
        log.debug(String.format("for type: %s", dtoType.getName()));
        ToolkitId actual = service.create(dtoType, content);
        Assert.assertNotNull(actual);
        Assert.assertEquals(DATE, actual.getLastUpdateDate());
        String topiaId = actual.getId();
        ToolkitTreeNodeStates getResult = service.getOne(dtoType, new ToolkitRequestConfig(), topiaId);
        Assert.assertNotNull(getResult);
        return topiaId;
    }

    private void testUpdate(ReferentialEntityService service, Class<? extends ReferentialDto> dtoType, String id, String content) throws InvalidDataException {
        log.debug(String.format("for type: %s", dtoType.getName()));
        ToolkitId actual = service.update(dtoType, Objects.requireNonNull(id), content);
        Assert.assertNotNull(actual);
        Assert.assertEquals(id, actual.getId());
        Assert.assertEquals(DATE, actual.getLastUpdateDate());
        ToolkitTreeNodeStates getResult = service.getOne(dtoType, new ToolkitRequestConfig(), id);
        Assert.assertNotNull(getResult);
    }

    private void testDelete(ReferentialEntityService service, Class<? extends ReferentialDto> dtoType, String content) throws InvalidDataException {
        log.debug(String.format("for type: %s", dtoType.getName()));
        String id = testCreate(service, dtoType, content);

        service.delete(dtoType, Objects.requireNonNull(id));
        ToolkitTreeNodeStates getResult = service.getOne(dtoType, new ToolkitRequestConfig(), id);
        Assert.assertNotNull(getResult);
        Object contentState = getResult.getState("content");
        Assert.assertNotNull(contentState);
        Assert.assertEquals("[]", contentState);
    }

    private Map<String, List<Map<String, Object>>> testGetResult(Gson gson, ToolkitTreeNodeStates actual) {
        Assert.assertNotNull(actual);
        String content = actual.getState("content");
        @SuppressWarnings("UnstableApiUsage") Map<String, List<Map<String, Object>>> map = gson.fromJson(content, new TypeToken<Map<String, List<Map<String, Object>>>>() {
        }.getType());
        Assert.assertNotNull(map);
        return map;
    }
}
