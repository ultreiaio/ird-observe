package fr.ird.observe.services.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.data.ps.observation.SetDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.service.ObserveServiceFixtures;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.junit.Assert;

import java.util.function.Consumer;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ContainerServiceFixtures extends GeneratedContainerServiceFixtures {

    @SuppressWarnings("unused")
    public static
    class SaveResult<C extends ContainerChildDto, D extends ContainerDto<C>> {
        private final Form<D> updateForm;
        private final SaveResultDto updateSave;

        public SaveResult(Form<D> updateForm, SaveResultDto updateSave) {
            this.updateForm = updateForm;
            this.updateSave = updateSave;
        }

        public Form<D> getUpdateForm() {
            return updateForm;
        }

        public SaveResultDto getUpdateSave() {
            return updateSave;
        }
    }

    public static <C extends ContainerChildDto, D extends ContainerDto<C>> SaveResult<C, D> save(ObserveServiceFixtures<?> fixtures, ContainerService service, Class<D> dtoType, Consumer<D> beanUpdateFunction) throws ConcurrentModificationException {
        String id = getParentId(fixtures, dtoType);
        Form<D> updateForm = service.loadForm(dtoType, id);
        D updateDto = updateForm.getObject();
        beanUpdateFunction.accept(updateDto);
        SaveResultDto updateSave = service.save(updateDto);
        return new SaveResult<>(updateForm, updateSave);
    }

    public static <C extends ContainerChildDto, D extends ContainerDto<C>> Form<D> loadForm(ObserveServiceFixtures<?> fixtures, ContainerService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String countPropertyName = variableName.replace(".id", ".count");
        String id = getParentId(fixtures, dtoType);
        Form<D> form = service.loadForm(dtoType, id);
        Assert.assertNotNull(form);
        D dto = form.getObject();
        Assert.assertEquals(id, dto.getId());
        int actualCount = dto.getChildrenSize();
        OpenableServiceFixtures.assertCount(fixtures, actualCount, countPropertyName);

        return form;
    }

    private static <C extends ContainerChildDto, D extends ContainerDto<C>> String getParentId(ObserveServiceFixtures<?> fixtures, Class<D> dtoType) {
        Class<? extends BusinessDto> mainDtoType = ObserveBusinessProject.get().getMapping().getMainDtoType(dtoType);
        if (mainDtoType.equals(dtoType)) {
            // special case for ps_observation.SampleDto
            mainDtoType = SetDto.class;
        }
        String variableName = fixtures.getVariableName(mainDtoType);
        return getVariable(variableName);
    }

    @Override
    public void loadForm(fr.ird.observe.services.ObserveServicesProvider servicesProvider, ContainerService service) {
        for (Class<? extends ContainerDto> dtoType : ObserveBusinessProject.get().getContainerDataTypes()) {
            loadForm(this, service, dtoType);
        }
    }

    @Override
    public void save(fr.ird.observe.services.ObserveServicesProvider servicesProvider, ContainerService service) {
        for (Class<? extends ContainerDto> dtoType : ObserveBusinessProject.get().getContainerDataTypes()) {
            try {
                save(this, service, dtoType, d -> {
                    //FIXME:Test
                });
            } catch (ConcurrentModificationException e) {
                throw new RuntimeException(e);
            }
        }
    }
//FIXME Make those tests alive again
//    public void psObservationSampleLoadForm(ObserveServicesProvider servicesProvider, ContainerService service) {
//        Form<SampleDto> form = loadForm(this, service, SampleDto.class);
//        SampleDto dto = form.getObject();
//        Assert.assertTrue(dto.isNotAvailableSpeciesIdsEmpty());
//        Assert.assertTrue(dto.isNotAvailableSpeciesFateIdsEmpty());
//    }
//    public void psObservationSetNonTargetCatchReleaseLoadForm(ObserveServicesProvider servicesProvider, ContainerService service) {
//        Form<SetNonTargetCatchReleaseDto> form = loadForm(this, service, SetNonTargetCatchReleaseDto.class);
//        SetNonTargetCatchReleaseDto dto = form.getObject();
//        Assert.assertTrue(dto.isNotNonTargetCatchReleaseEmpty());
//        Assert.assertTrue(dto.isNotAvailableSpeciesIdsEmpty());
//    }
//    public void llObservationSetTdrLoadForm(ObserveServicesProvider servicesProvider, ContainerService service) {
//        Form<SetTdrDto> form = loadForm(this, service, SetTdrDto.class);
//        SetCatchServiceFixtures.assertDto(form);
//    }
//    public void llObservationSetCatchLoadForm(ObserveServicesProvider servicesProvider, ContainerService service) {
//        Form<SetCatchDto> form = loadForm(this, service, SetCatchDto.class);
//        SetCatchServiceFixtures.assertDto(form);
//    }
//    public void llObservationSetDetailCompositionLoadForm(ObserveServicesProvider servicesProvider, ContainerService service) {
//        Form<SetDetailCompositionDto> form = service.loadForm(getProperty("loadForm.id"));
//
//        Assert.assertNotNull(form);
//
//        SetDetailCompositionDto compositionDto = form.getObject();
//        Assert.assertNotNull(compositionDto);
//
//        Assert.assertEquals(15, compositionDto.getSectionSize());
//
//        SectionDto sectionDto = compositionDto.getSection(0);
//        Assert.assertEquals(Integer.valueOf(1), sectionDto.getSettingIdentifier());
//        Assert.assertEquals(Integer.valueOf(15), sectionDto.getHaulingIdentifier());
//        Assert.assertEquals(13, sectionDto.getBasketSize());
//        Assert.assertNotNull(sectionDto.getParentId());
//        Assert.assertTrue(sectionDto.isNotUsed());
//        Assert.assertEquals(compositionDto.getId(), sectionDto.getParentId());
//        Assert.assertNotNull(SectionReference.of(ReferentialLocale.FR, sectionDto));
//        Assert.assertNotNull(SectionReference.of(ReferentialLocale.FR, sectionDto).getParentId());
//
//        BasketDto basketDto = sectionDto.getBasket(0);
//        Assert.assertEquals(Integer.valueOf(1), basketDto.getSettingIdentifier());
//        Assert.assertEquals(Integer.valueOf(13), basketDto.getHaulingIdentifier());
//        Assert.assertEquals(7, basketDto.getBranchlineSize());
//        Assert.assertNotNull(basketDto.getParentId());
//        Assert.assertTrue(basketDto.isNotUsed());
//
//        Assert.assertEquals(sectionDto.getId(), basketDto.getParentId());
//        Assert.assertNotNull(BasketReference.of(ReferentialLocale.FR, basketDto));
//        Assert.assertNotNull(BasketReference.of(ReferentialLocale.FR, basketDto).getParentId());
//
//        BranchlineDto branchlineDto = basketDto.getBranchline(0);
//        Assert.assertEquals(Integer.valueOf(1), branchlineDto.getSettingIdentifier());
//        Assert.assertEquals(Integer.valueOf(7), branchlineDto.getHaulingIdentifier());
//        Assert.assertNotNull(branchlineDto.getParentId());
//        Assert.assertTrue(branchlineDto.isNotUsed());
//        Assert.assertEquals(basketDto.getId(), branchlineDto.getParentId());
//        Assert.assertNotNull(BranchlineReference.of(ReferentialLocale.FR, branchlineDto));
//        Assert.assertNotNull(BranchlineReference.of(ReferentialLocale.FR, branchlineDto).getParentId());
//
//        sectionDto = compositionDto.getSection(6);
//        basketDto = sectionDto.getBasket(8);
//        Assert.assertTrue(basketDto.isNotUsed());
//    }

}
