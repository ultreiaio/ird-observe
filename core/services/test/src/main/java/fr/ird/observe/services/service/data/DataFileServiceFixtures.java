package fr.ird.observe.services.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.DataNotFoundException;
import fr.ird.observe.dto.data.DataFileDto;
import fr.ird.observe.services.ObserveServicesProvider;
import org.junit.Assert;

public class DataFileServiceFixtures extends GeneratedDataFileServiceFixtures {

    @Override
    public void getDataFile(ObserveServicesProvider servicesProvider, DataFileService service) {

        try {
            service.getDataFile(getProperty("getDataFile.id") + "Fake" + System.nanoTime());
            Assert.fail("Should not find a data file for a fake id");
        } catch (DataNotFoundException e) {
            // fine :)
        }
        //FIXME:Tck Add a tdr without a dataFile
//        try {
//            service.getDataFile(getProperty("getDataFile.id.noDataFile"));
//            Assert.fail("Should not find a data file for this id/");
//        } catch (DataFileNotFoundException e) {
//            // fine :)
//        }
        DataFileDto actual = service.getDataFile(getProperty("getDataFile.id"));
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getContent());
        Assert.assertEquals(getIntegerProperty("getDataFile.length"), actual.getContent().length);

        actual = service.getDataFile(getProperty("getDataFile.id.sensorUsed"));
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getContent());
        Assert.assertEquals(getIntegerProperty("getDataFile.sensorUsed.length"), actual.getContent().length);

    }

}
