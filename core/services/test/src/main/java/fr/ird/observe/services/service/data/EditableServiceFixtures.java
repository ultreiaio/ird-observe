package fr.ird.observe.services.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.ll.logbook.ActivitySampleDto;
import fr.ird.observe.dto.data.ll.logbook.SetDto;
import fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.ObserveServiceFixtures;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.junit.Assert;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class EditableServiceFixtures extends GeneratedEditableServiceFixtures {

    @SuppressWarnings("unused")
    public static
    class SaveResult<D extends EditableDto> {
        private final Form<D> createForm;
        private final SaveResultDto createSave;
        private final Form<D> updateForm;
        private final SaveResultDto updateSave;

        public SaveResult(Form<D> createForm, SaveResultDto createSave, Form<D> updateForm, SaveResultDto updateSave) {
            this.createForm = createForm;
            this.createSave = createSave;
            this.updateForm = updateForm;
            this.updateSave = updateSave;
        }

        public Form<D> getCreateForm() {
            return createForm;
        }

        public SaveResultDto getCreateSave() {
            return createSave;
        }

        public Form<D> getUpdateForm() {
            return updateForm;
        }

        public SaveResultDto getUpdateSave() {
            return updateSave;
        }
    }

    public static <D extends EditableDto> Form<D> loadForm(ObserveServiceFixtures<?> fixtures, EditableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        Form<D> form = service.loadForm(dtoType, id);
        Assert.assertNotNull(form);
        D dto = form.getObject();
        Assert.assertEquals(id, dto.getId());
        return form;
    }

    public static <D extends EditableDto> D loadDto(ObserveServiceFixtures<?> fixtures, EditableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        D dto = service.loadDto(dtoType, id);
        Assert.assertNotNull(dto);
        Assert.assertEquals(id, dto.getId());
        return dto;
    }

    public static <D extends EditableDto> void exists(EditableServiceFixtures fixtures, EditableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        Assert.assertTrue(service.exists(dtoType, id));
        Assert.assertFalse(service.exists(dtoType, id + "Fake"));
    }

    public static <D extends EditableDto> void delete(EditableServiceFixtures fixtures, EditableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        Assert.assertTrue(service.exists(dtoType, id));
        service.delete(dtoType, id);
        Assert.assertFalse(service.exists(dtoType, id));
    }

    public static <D extends EditableDto> Form<D> preCreate(EditableServiceFixtures fixtures, EditableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType, ".parentId");
        String parentId = fixtures.getProperty(variableName);
        if (!WITH_ASSERT) {
            if (parentId == null) {
                System.out.println(variableName + "=");
                return null;
            }
        }
        Form<D> form = service.preCreate(dtoType, parentId);
        Assert.assertNotNull(form);
        D dto = form.getObject();
        Assert.assertNull(dto.getId());
        return form;
    }

    public static <D extends EditableDto> SaveResult<D> save(ObserveServicesProvider servicesProvider, EditableServiceFixtures fixtures, Class<D> dtoType, Consumer<D> beanCreateFunction, Consumer<D> beanUpdateFunction) throws ConcurrentModificationException {
        EditableService editableService = servicesProvider.getEditableService();
        Form<D> createForm = preCreate(fixtures, editableService, dtoType);
        if (!WITH_ASSERT) {
            if (createForm == null) {
                return null;
            }
        }
        D createDto = Objects.requireNonNull(createForm).getObject();
        beanCreateFunction.accept(createDto);
        String variableName = fixtures.getVariableName(dtoType, ".parentId");
        String parentId = fixtures.getProperty(variableName);
        SaveResultDto createSave = editableService.save(parentId, createDto);

        Form<D> updateForm = editableService.loadForm(dtoType, createSave.getId());
        D updateDto = updateForm.getObject();
        beanUpdateFunction.accept(updateDto);
        SaveResultDto updateSave = editableService.save(parentId, updateDto);
        return new SaveResult<>(createForm, createSave, updateForm, updateSave);
    }

    @Override
    public void delete(fr.ird.observe.services.ObserveServicesProvider servicesProvider, EditableService service) {
        for (Class<? extends EditableDto> dtoType : ObserveBusinessProject.get().getEditableDataTypes()) {
            delete(this, service, dtoType);
        }
    }

    @Override
    public void exists(fr.ird.observe.services.ObserveServicesProvider servicesProvider, EditableService service) {
        for (Class<? extends EditableDto> dtoType : ObserveBusinessProject.get().getEditableDataTypes()) {
            exists(this, service, dtoType);
        }
    }

    @Override
    public void loadDto(fr.ird.observe.services.ObserveServicesProvider servicesProvider, EditableService service) {
        for (Class<? extends EditableDto> dtoType : ObserveBusinessProject.get().getEditableDataTypes()) {
            loadDto(this, service, dtoType);
        }
    }

    @Override
    public void loadForm(fr.ird.observe.services.ObserveServicesProvider servicesProvider, EditableService service) {
        for (Class<? extends EditableDto> dtoType : ObserveBusinessProject.get().getEditableDataTypes()) {
            loadForm(this, service, dtoType);
        }
    }

    @Override
    public void preCreate(fr.ird.observe.services.ObserveServicesProvider servicesProvider, EditableService service) {
        for (Class<? extends EditableDto> dtoType : ObserveBusinessProject.get().getEditableDataTypes()) {
            preCreate(this, service, dtoType);
        }
    }

    @Override
    public void save(fr.ird.observe.services.ObserveServicesProvider servicesProvider, EditableService service) {
        try {
            saveLlLogbookSet(servicesProvider);
            saveLlLogbookActivitySample(servicesProvider);
            saveLlObservationSet(servicesProvider);
            savePsLogbookFloatingObject(servicesProvider);
            savePsObservationFloatingObject(servicesProvider);
            savePsObservationSet(servicesProvider);
        } catch (ConcurrentModificationException e) {
            throw new RuntimeException(e);
        }
    }

    public void saveLlLogbookActivitySample(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                ActivitySampleDto.class,
                d -> {
                    //FIXME:Test
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void saveLlLogbookSet(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                SetDto.class,
                d -> {
                    //FIXME:Test
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void saveLlObservationSet(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                fr.ird.observe.dto.data.ll.observation.SetDto.class,
                d -> {
                    d.setSettingEndLatitude(-1f);
                    d.setSettingEndLongitude(-1f);
                    d.setHaulingStartLatitude(-1f);
                    d.setHaulingStartLongitude(-1f);
                    d.setHaulingEndLatitude(-1f);
                    d.setHaulingEndLongitude(-1f);
                    d.setSettingVesselSpeed(2.0f);
                    d.setBasketsPerSectionCount(2);
                    d.setBranchlinesPerBasketCount(2);
                    d.setHaulingDirectionSameAsSetting(true);
                    d.setTotalSectionsCount(4);
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void savePsLogbookFloatingObject(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        FloatingObjectDto dto = EditableServiceFixtures.loadDto(this, servicesProvider.getEditableService(), FloatingObjectDto.class);
        save(servicesProvider,
             this,
             FloatingObjectDto.class,
             dto::copy,
             d -> {
                 //FIXME:Test
             });
    }

    public void savePsObservationFloatingObject(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        fr.ird.observe.dto.data.ps.observation.FloatingObjectDto dto = EditableServiceFixtures.loadDto(this, servicesProvider.getEditableService(), fr.ird.observe.dto.data.ps.observation.FloatingObjectDto.class);
        save(servicesProvider,
             this,
             fr.ird.observe.dto.data.ps.observation.FloatingObjectDto.class,
             dto::copy,
             d -> {
                 //FIXME:Test
             });
    }

    public void savePsObservationSet(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(servicesProvider,
             this,
             fr.ird.observe.dto.data.ps.observation.SetDto.class,
             d -> {
                 //FIXME:Test
             },
             d -> {
                 //FIXME:Test
             });
    }
}
