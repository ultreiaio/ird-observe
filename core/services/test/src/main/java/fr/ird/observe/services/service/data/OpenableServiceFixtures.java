package fr.ird.observe.services.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.collect.ArrayListMultimap;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.ll.landing.LandingDto;
import fr.ird.observe.dto.data.ps.localmarket.SurveyDto;
import fr.ird.observe.dto.data.ps.logbook.SampleDto;
import fr.ird.observe.dto.data.ps.observation.ActivityDto;
import fr.ird.observe.dto.data.ps.observation.RouteDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;
import fr.ird.observe.navigation.id.IdModel;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.ObserveServiceFixtures;
import fr.ird.observe.services.service.ReferenceServiceFixtures;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.util.Dates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;


/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings({"UnusedReturnValue", "unused"})
public class OpenableServiceFixtures extends GeneratedOpenableServiceFixtures {

    private static final Logger log = LogManager.getLogger(OpenableServiceFixtures.class);

    @SuppressWarnings("unused")
    public static
    class SaveResult<D extends OpenableDto> {
        private final Form<D> createForm;
        private final SaveResultDto createSave;
        private final Form<D> updateForm;
        private final SaveResultDto updateSave;

        public SaveResult(Form<D> createForm, SaveResultDto createSave, Form<D> updateForm, SaveResultDto updateSave) {
            this.createForm = createForm;
            this.createSave = createSave;
            this.updateForm = updateForm;
            this.updateSave = updateSave;
        }

        public Form<D> getCreateForm() {
            return createForm;
        }

        public SaveResultDto getCreateSave() {
            return createSave;
        }

        public Form<D> getUpdateForm() {
            return updateForm;
        }

        public SaveResultDto getUpdateSave() {
            return updateSave;
        }
    }

    public static <D extends OpenableDto> List<ToolkitIdLabel> getBrothers(OpenableServiceFixtures fixtures, OpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        List<ToolkitIdLabel> brothers = service.getBrothers(dtoType, id);
        Assert.assertNotNull(brothers);
        int actualCount = brothers.size();
        String countPropertyName = variableName.replace(".id", ".getBrothers.count");
        assertCount(fixtures, actualCount, countPropertyName);
        return brothers;
    }

    public static <D extends OpenableDto> List<ToolkitIdLabel> getBrothersFromParent(OpenableServiceFixtures fixtures, OpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType, ".parentId");
        String parentId = fixtures.getProperty(variableName);
        String countPropertyName = variableName.replace(".parentId", ".getBrothersFromParent.count");
        List<ToolkitIdLabel> children = service.getBrothersFromParent(dtoType, parentId);
        Assert.assertNotNull(children);
        int actualCount = children.size();
        assertCount(fixtures, actualCount, countPropertyName);
        return children;
    }

    public static <D extends OpenableDto, R extends DataDtoReference> DataDtoReferenceSet<R> getChildren(OpenableServiceFixtures fixtures, OpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType, ".parentId");
        String parentId = fixtures.getProperty(variableName);
        String countPropertyName = variableName.replace(".parentId", ".getChildren.count");
        DataDtoReferenceSet<R> children = service.getChildren(dtoType, parentId);
        Assert.assertNotNull(children);
        int actualCount = children.size();
        assertCount(fixtures, actualCount, countPropertyName);
        return children;
    }

    public static <D extends OpenableDto, R extends DataDtoReference> UpdatedDataDtoReferenceSet<R> getChildrenUpdate(OpenableServiceFixtures fixtures, OpenableService service, Date date, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType, ".parentId");
        String parentId = fixtures.getProperty(variableName);
        String countPropertyName = variableName.replace(".parentId", ".getChildrenUpdate.count");
        UpdatedDataDtoReferenceSet<R> children = service.getChildrenUpdate(dtoType, parentId, date);
        Assert.assertNotNull(children);
        Assert.assertNotNull(children.getUpdatedReferences());
        int actualCount = children.getUpdatedReferences().size();
        assertCount(fixtures, actualCount, countPropertyName);
        return children;
    }

    public static <D extends OpenableDto> Form<D> loadForm(OpenableServiceFixtures fixtures, OpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        Form<D> form = service.loadForm(dtoType, id);
        Assert.assertNotNull(form);
        D dto = form.getObject();
        Assert.assertEquals(id, dto.getId());
        return form;
    }

    public static <D extends OpenableDto> D loadDto(ObserveServiceFixtures<?> fixtures, OpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        D dto = service.loadDto(dtoType, id);
        Assert.assertNotNull(dto);
        Assert.assertEquals(id, dto.getId());
        return dto;
    }

    public static <D extends OpenableDto> void exists(OpenableServiceFixtures fixtures, OpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        Assert.assertTrue(service.exists(dtoType, id));
        Assert.assertFalse(service.exists(dtoType, id + "Fake"));
    }

    public static <D extends OpenableDto> void delete(OpenableServiceFixtures fixtures, OpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        Assert.assertTrue(service.exists(dtoType, id));
        log.debug("Delete for: " + id);
        service.delete(dtoType, id);
        Assert.assertFalse(service.exists(dtoType, id));
    }

    public static <D extends OpenableDto> void getOptionalDependenciesCount(OpenableServiceFixtures fixtures, OpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        UsageCount dependencies = service.getOptionalDependenciesCount(dtoType, Set.of(id));
        String countPropertyName = variableName.replace(".id", ".dependencies.count");
        assertCount(fixtures, dependencies == null ? 0 : dependencies.size(), countPropertyName);
    }

    public static <D extends OpenableDto> void getMandatoryDependenciesCount(OpenableServiceFixtures fixtures, OpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        UsageCount dependencies = service.getMandatoryDependenciesCount(dtoType, Set.of(id));
        String countPropertyName = variableName.replace(".id", ".mandatoryDependencies.count");
        assertCount(fixtures, dependencies == null ? 0 : dependencies.size(), countPropertyName);
    }

    public static <D extends OpenableDto> Form<D> preCreate(ObserveServiceFixtures<?> fixtures, OpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType, ".parentId");
        String parentId = fixtures.getProperty(variableName);
        Form<D> form = service.preCreate(dtoType, parentId);
        Assert.assertNotNull(form);
        D dto = form.getObject();
        Assert.assertNull(dto.getId());
        return form;
    }

    public static void assertCount(ObserveServiceFixtures<?> fixtures, int actualCount, String countPropertyName) {
        if (WITH_ASSERT) {
            int expectedCount = fixtures.getIntegerProperty(countPropertyName);
            Assert.assertEquals(countPropertyName + "=" + actualCount, expectedCount, actualCount);
        } else {
            System.out.println(countPropertyName + "=" + actualCount);
        }
    }

    public static <D extends OpenableDto> SaveResult<D> save(ObserveServicesProvider servicesProvider, ObserveServiceFixtures<?> fixtures, Class<D> dtoType, Consumer<D> beanCreateFunction, Consumer<D> beanUpdateFunction) throws ConcurrentModificationException {
        OpenableService openableService = servicesProvider.getOpenableService();
        Form<D> createForm = preCreate(fixtures, openableService, dtoType);
        D createDto = createForm.getObject();
        beanCreateFunction.accept(createDto);
        String variableName = fixtures.getVariableName(dtoType, ".parentId");
        String parentId = fixtures.getProperty(variableName);
        SaveResultDto createSave = openableService.save(parentId, createDto);

        Form<D> updateForm = openableService.loadForm(dtoType, createSave.getId());
        D updateDto = updateForm.getObject();
        beanUpdateFunction.accept(updateDto);
        SaveResultDto updateSave = openableService.save(parentId, updateDto);

        // on fait croire que notre version est plus ancienne
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(updateDto.getLastUpdateDate());
        calendar.add(Calendar.HOUR, -1);
        updateDto.setLastUpdateDate(calendar.getTime());

        try {
            openableService.save(parentId, updateDto);
            Assert.fail(String.format("Should got a concurrent modification on %s", updateDto));
        } catch (ConcurrentModificationException e) {
            Assert.assertTrue(true);
        }
        return new SaveResult<>(createForm, createSave, updateForm, updateSave);
    }

    public static <D extends OpenableDto> void move(ObserveServicesProvider servicesProvider, ObserveServiceFixtures<?> fixtures, Class<D> dtoType) throws ConcurrentModificationException {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        String newParentIdProperty = fixtures.getVariableName(dtoType, ".move.newParentId");
        ToolkitIdDtoBean oldParentId = getIdReference(fixtures, fixtures.getVariableName(dtoType, ".parentId"));
        ToolkitIdDtoBean newParentId = getIdReference(fixtures, newParentIdProperty);

        assertMoveLimitCases(servicesProvider, dtoType, id, oldParentId, newParentId);
        Set<String> newIds = assertMove(servicesProvider, dtoType, id, oldParentId, newParentId);

        // move back to original
        newIds = assertMove(servicesProvider, dtoType, newIds.iterator().next(), newParentId, oldParentId);

        if (fr.ird.observe.dto.data.ll.logbook.SampleDto.class.isAssignableFrom(dtoType)) {
            id = newIds.iterator().next();

            newParentIdProperty = fixtures.getVariableName(dtoType, ".move.newParentId2");
            newParentId = getIdReference(fixtures, newParentIdProperty);

            OpenableServiceFixtures.assertMoveLimitCases(servicesProvider, dtoType, id, oldParentId, newParentId);
            OpenableServiceFixtures.assertMoveSimple(servicesProvider, dtoType, id, oldParentId, newParentId);
        }
    }

    public static <D extends OpenableDto> void assertMoveLimitCases(
            ObserveServicesProvider servicesProvider, Class<D> dtoType,
            String id,
            ToolkitIdDtoBean oldParentId,
            ToolkitIdDtoBean newParentId) {
        OpenableService openableService = servicesProvider.getOpenableService();

        {// try to move them back using bad timestamp (will fail)
            ToolkitIdDtoBean oldParentId2 = oldParentId.of(new Date(0));
            try {
                openableService.move(dtoType, new MoveRequest(oldParentId2, newParentId, Collections.singleton(id), false));
                Assert.fail(String.format("Should got a concurrent modification on move %s", id));
            } catch (ConcurrentModificationException e) {
                Assert.assertTrue(true);
            }
        }
        {// try to move them back using bad timestamp (will fail)
            ToolkitIdDtoBean newParentId2 = newParentId.of(new Date(0));
            try {
                openableService.move(dtoType, new MoveRequest(oldParentId, newParentId2, Collections.singleton(id), true));
                Assert.fail(String.format("Should got a concurrent modification on move %s", id));
            } catch (ConcurrentModificationException e) {
                Assert.assertTrue(true);
            }
        }
        {// try to move them back using bad timestamp (will fail)
            ToolkitIdDtoBean oldParentId2 = oldParentId.of(new Date(0));
            ToolkitIdDtoBean newParentId2 = newParentId.of(new Date(0));
            try {
                openableService.move(dtoType, new MoveRequest(oldParentId2, newParentId2, Collections.singleton(id), true));
                Assert.fail(String.format("Should got a concurrent modification on move %s", id));
            } catch (ConcurrentModificationException e) {
                Assert.assertTrue(true);
            }
        }
    }

    public static <D extends OpenableDto> Set<String> assertMove(
            ObserveServicesProvider servicesProvider,
            Class<D> dtoType,
            String id,
            ToolkitIdDtoBean oldParentId,
            ToolkitIdDtoBean newParentId) throws ConcurrentModificationException {

        OpenableService openableService = servicesProvider.getOpenableService();
        DataDtoReferenceSet<?> childrenBefore = openableService.getChildren(dtoType, newParentId.getId());

        Set<String> newIds = openableService.move(dtoType, new MoveRequest(oldParentId, newParentId, Collections.singleton(id), true));
        DataDtoReferenceSet<?> childrenAfter = openableService.getChildren(dtoType, newParentId.getId());

        boolean exists = openableService.exists(dtoType, id);
        Assert.assertFalse(String.format("After move, should not be able to find back %s", id), exists);
        if (childrenBefore.size() > 0) {
            Assert.assertEquals(String.format("After move, should have one more children after move of %s", id), childrenBefore.size() + 1, childrenAfter.size());
        }
        for (String newId : newIds) {
            exists = openableService.exists(dtoType, newId);
            Assert.assertTrue(String.format("After move, should be able to find back %s", newId), exists);
        }
        return newIds;
    }

    public static <D extends OpenableDto> Set<String> assertMoveSimple(
            ObserveServicesProvider servicesProvider,
            Class<D> dtoType,
            String id,
            ToolkitIdDtoBean oldParentId,
            ToolkitIdDtoBean newParentId) throws ConcurrentModificationException {
        OpenableService openableService = servicesProvider.getOpenableService();
        Set<String> newIds = openableService.move(dtoType, new MoveRequest(oldParentId, newParentId, Collections.singleton(id), false));

        boolean exists = openableService.exists(dtoType, id);
        Assert.assertFalse(String.format("After move, should not be able to find back %s", id), exists);
        for (String newId : newIds) {
            exists = openableService.exists(dtoType, newId);
            Assert.assertTrue(String.format("After move, should be able to find back %s", newId), exists);
        }

        return newIds;
    }

    public static ToolkitIdDtoBean getIdReference(ObserveServiceFixtures<?> fixtures, String property) {
        String id = Objects.requireNonNull(fixtures.getProperty(property), "Required property: " + property);
        int index = id.indexOf(":");
        Class<? extends BusinessDto> type;
        if (index == -1) {
            return ObserveBusinessProject.get().toShortDto(id);
        } else {
            type = Objects2.forName(id.substring(0, index));
            id = id.substring(index + 1);
            return ToolkitIdDtoBean.of(type, id);
        }
    }

    public static <D extends OpenableDto> void getOptionalDependencies(OpenableServiceFixtures fixtures, OpenableService service, Class<D> dtoType, Collection<Class<? extends DataDto>> dependencyTypes) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        for (Class<? extends DataDto> dependencyType : dependencyTypes) {
            Set<ToolkitIdLabel> dependencies = service.getOptionalDependencies(dtoType, Set.of(id), dependencyType);
            String countPropertyName = "dependencies." + variableName
                    .replace(".id", "." +
                            dependencyType.getName()
                                    .replace("Dto", "")
                                    .replace("fr.ird.observe.dto.data", "")
                                    .replace(".ll.", "")
                                    .replace(".ps.", ""));
            assertCount(fixtures, dependencies.size(), countPropertyName);
        }
    }

    @Override
    public void getBrothers(ObserveServicesProvider servicesProvider, OpenableService service) {
        for (Class<? extends OpenableDto> dtoType : ObserveBusinessProject.get().getOpenableDataTypes()) {
            getBrothers(this, service, dtoType);
        }
    }

    @Override
    public void getBrothersFromParent(ObserveServicesProvider servicesProvider, OpenableService service) {
        for (Class<? extends OpenableDto> dtoType : ObserveBusinessProject.get().getOpenableDataTypes()) {
            getBrothersFromParent(this, service, dtoType);
        }
    }

    @Override
    public void getChildren(ObserveServicesProvider servicesProvider, OpenableService service) {
        for (Class<? extends OpenableDto> dtoType : ObserveBusinessProject.get().getOpenableDataTypes()) {
            getChildren(this, service, dtoType);
        }
    }

    @Override
    public void getChildrenUpdate(ObserveServicesProvider servicesProvider, OpenableService service) {
        for (Class<? extends OpenableDto> dtoType : ObserveBusinessProject.get().getOpenableDataTypes()) {
            getChildrenUpdate(this, service, DATE, dtoType);
        }
    }

    @Override
    public void loadForm(ObserveServicesProvider servicesProvider, OpenableService service) {
        for (Class<? extends OpenableDto> dtoType : ObserveBusinessProject.get().getOpenableDataTypes()) {
            loadForm(this, service, dtoType);
        }
    }

    @Override
    public void loadDto(ObserveServicesProvider servicesProvider, OpenableService service) {
        for (Class<? extends OpenableDto> dtoType : ObserveBusinessProject.get().getOpenableDataTypes()) {
            loadDto(this, service, dtoType);
        }
    }

    @Override
    public void preCreate(ObserveServicesProvider servicesProvider, OpenableService service) {
        for (Class<? extends OpenableDto> dtoType : ObserveBusinessProject.get().getOpenableDataTypes()) {
            preCreate(this, service, dtoType);
        }
    }

    @Override
    public void exists(ObserveServicesProvider servicesProvider, OpenableService service) {
        for (Class<? extends OpenableDto> dtoType : ObserveBusinessProject.get().getOpenableDataTypes()) {
            exists(this, service, dtoType);
        }
    }

    @Override
    public void delete(ObserveServicesProvider servicesProvider, OpenableService service) {
        Set<Class<? extends OpenableDto>> types = ObserveBusinessProject.get().getOpenableDataTypes();
        Project editModel = new Project();
        ArrayListMultimap<Integer, Class<? extends OpenableDto>> orderedTypes = ArrayListMultimap.create();
        for (IdModel model : editModel.getModels()) {
            for (IdNode<?> node : model.getNodes()) {
                if (types.contains(node.getType())) {
                    @SuppressWarnings("unchecked") Class<? extends OpenableDto> dtoType = (Class<? extends OpenableDto>) node.getType();
                    orderedTypes.put(node.getLevel(), dtoType);
                }
            }
        }
        List<Integer> order = new LinkedList<>(orderedTypes.keySet());
        order.sort(Integer::compareTo);
        Collections.reverse(order);
        for (int thisOrder : order) {
            for (Class<? extends OpenableDto> dtoType : orderedTypes.get(thisOrder)) {
                delete(this, service, dtoType);
            }
        }
    }

    @Override
    public void getOptionalDependenciesCount(ObserveServicesProvider servicesProvider, OpenableService service) {
        Set<Class<? extends OpenableDto>> types = ObserveBusinessProject.get().getOpenableDataTypes();
        Project editModel = new Project();
        ArrayListMultimap<Integer, Class<? extends OpenableDto>> orderedTypes = ArrayListMultimap.create();
        for (IdModel model : editModel.getModels()) {
            for (IdNode<?> node : model.getNodes()) {
                if (types.contains(node.getType())) {
                    @SuppressWarnings("unchecked") Class<? extends OpenableDto> dtoType = (Class<? extends OpenableDto>) node.getType();
                    orderedTypes.put(node.getLevel(), dtoType);
                }
            }
        }
        List<Integer> order = new LinkedList<>(orderedTypes.keySet());
        order.sort(Integer::compareTo);
        Collections.reverse(order);
        for (int thisOrder : order) {
            for (Class<? extends OpenableDto> dtoType : orderedTypes.get(thisOrder)) {
                getOptionalDependenciesCount(this, service, dtoType);
            }
        }
    }

    @Override
    public void getMandatoryDependenciesCount(ObserveServicesProvider servicesProvider, OpenableService service) {
        Set<Class<? extends OpenableDto>> types = ObserveBusinessProject.get().getOpenableDataTypes();
        Project editModel = new Project();
        ArrayListMultimap<Integer, Class<? extends OpenableDto>> orderedTypes = ArrayListMultimap.create();
        for (IdModel model : editModel.getModels()) {
            for (IdNode<?> node : model.getNodes()) {
                if (types.contains(node.getType())) {
                    @SuppressWarnings("unchecked") Class<? extends OpenableDto> dtoType = (Class<? extends OpenableDto>) node.getType();
                    orderedTypes.put(node.getLevel(), dtoType);
                }
            }
        }
        List<Integer> order = new LinkedList<>(orderedTypes.keySet());
        order.sort(Integer::compareTo);
        Collections.reverse(order);
        for (int thisOrder : order) {
            for (Class<? extends OpenableDto> dtoType : orderedTypes.get(thisOrder)) {
                getMandatoryDependenciesCount(this, service, dtoType);
            }
        }
    }

    //FIXME Do we really need this method ? always returns 0
    @Override
    public void getOptionalDependencies(ObserveServicesProvider servicesProvider, OpenableService service) {
        Set<Class<? extends OpenableDto>> types = ObserveBusinessProject.get().getOpenableDataTypes();
        Project editModel = new Project();
        ArrayListMultimap<Class<? extends OpenableDto>, Class<? extends DataDto>> orderedTypes = ArrayListMultimap.create();
        for (IdModel model : editModel.getModels()) {
            for (IdNode<?> node : model.getEditableNodes()) {
                if (types.contains(node.getType())) {
                    @SuppressWarnings("unchecked") Class<? extends OpenableDto> dtoType = (Class<? extends OpenableDto>) node.getType();
                    for (IdNode<?> child : node.getEditableChildren()) {
                        @SuppressWarnings("unchecked") Class<? extends DataDto> dependencyType = (Class<? extends DataDto>) child.getType();
                        orderedTypes.put(dtoType, dependencyType);
                    }
                    Class<?> parentType = node.getParent().getType();
                    if (OpenableDto.class.isAssignableFrom(parentType)) {
                        @SuppressWarnings("unchecked") Class<? extends OpenableDto> parentType1 = (Class<? extends OpenableDto>) parentType;
                        orderedTypes.put(parentType1, dtoType);
                    }

                }
            }
        }
        for (Map.Entry<Class<? extends OpenableDto>, Collection<Class<? extends DataDto>>> entry : orderedTypes.asMap().entrySet()) {
            getOptionalDependencies(this, service, entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void move(ObserveServicesProvider servicesProvider, OpenableService service) {
        Set<Class<? extends OpenableDto>> types = ObserveBusinessProject.get().getOpenableDataTypes();
        Project editModel = new Project();
        ArrayListMultimap<Integer, Class<? extends OpenableDto>> orderedTypes = ArrayListMultimap.create();
        for (IdModel model : editModel.getModels()) {
            for (IdNode<?> node : model.getNodes()) {
                if (types.contains(node.getType())) {
                    @SuppressWarnings("unchecked") Class<? extends OpenableDto> dtoType = (Class<? extends OpenableDto>) node.getType();
                    orderedTypes.put(node.getLevel(), dtoType);
                }
            }
        }
        List<Integer> order = new LinkedList<>(orderedTypes.keySet());
        order.sort(Integer::compareTo);
        Collections.reverse(order);
        for (int thisOrder : order) {
            for (Class<? extends OpenableDto> dtoType : orderedTypes.get(thisOrder)) {
                try {
                    move(servicesProvider, this, dtoType);
                } catch (ConcurrentModificationException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @Override
    public void save(ObserveServicesProvider servicesProvider, OpenableService service) {
        try {
            saveLlObservationActivity(servicesProvider);
            saveLlLanding(servicesProvider);
            saveLlLogbookActivity(servicesProvider);
            saveLlLogbookSample(servicesProvider);
            savePsLogbookRoute(servicesProvider);
            savePsLogbookActivity(servicesProvider);
            savePsLogbookSample(servicesProvider);
            savePsObservationRoute(servicesProvider);
            savePsObservationActivity(servicesProvider);
            savePsLocalMarketSample(servicesProvider);
            savePsLocalmarketSurvey(servicesProvider);
        } catch (ConcurrentModificationException e) {
            throw new RuntimeException(e);
        }
    }


    public void savePsObservationRoute(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                RouteDto.class,
                d -> {
                    //FIXME:Test
                },
                d -> {
                    //FIXME:Test
                });
    }


    public void savePsObservationActivity(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                ActivityDto.class,
                d -> {
                    d.setVesselActivity(ReferenceServiceFixtures.psVesselActivityReference());
                    d.setLatitude(-1f);
                    d.setLongitude(-1f);
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void savePsLogbookSample(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                SampleDto.class,
                d -> {
                    d.setWell("3A");
                    d.setNumber(1);
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void savePsLogbookRoute(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                fr.ird.observe.dto.data.ps.logbook.RouteDto.class,
                d -> {
                    //FIXME:Test
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void savePsLogbookActivity(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                fr.ird.observe.dto.data.ps.logbook.ActivityDto.class,
                d -> {
                    d.setVesselActivity(ReferenceServiceFixtures.psVesselActivityReference());
                    d.setLatitude(-1f);
                    d.setLongitude(-1f);
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void savePsLocalMarketSample(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                fr.ird.observe.dto.data.ps.localmarket.SampleDto.class,
                d -> {
                    d.setNumber("Yo");
                    //FIXME:Test
                },
                d -> {
                    //FIXME:Test
                    d.setNumber("Ya");
                });
    }

    public void savePsLocalmarketSurvey(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                SurveyDto.class,
                d -> {
                    d.setDate(DATE);
                    //FIXME:Test
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void saveLlObservationActivity(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                fr.ird.observe.dto.data.ll.observation.ActivityDto.class,
                d -> {
                    d.setVesselActivity(ReferenceServiceFixtures.llVesselActivityReference());
                    d.setLatitude(-1f);
                    d.setLongitude(-1f);
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void saveLlLogbookActivity(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                fr.ird.observe.dto.data.ll.logbook.ActivityDto.class,
                d -> {
                    d.setVesselActivity(ReferenceServiceFixtures.llVesselActivityReference());
                    d.setLatitude(-1f);
                    d.setLongitude(-1f);
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void saveLlLanding(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                LandingDto.class,
                d -> d.setHarbour(ReferenceServiceFixtures.harbourReference()),
                d -> {
                    //FIXME:Test
                });
    }

    public void saveLlLogbookSample(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                fr.ird.observe.dto.data.ll.logbook.SampleDto.class,
                d -> {
                    //FIXME:Test
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void psObservationRouteLoadForm(ObserveServicesProvider servicesProvider, OpenableService service) {
        Form<RouteDto> form = loadForm(this, service, RouteDto.class);
        RouteDto dto = form.getObject();
        Assert.assertEquals(0, Dates.createDate(1, 4, 2019).compareTo(dto.getDate()));
        Assert.assertEquals(Float.valueOf(198.0F), dto.getStartLogValue());
        Assert.assertEquals(Float.valueOf(270.0F), dto.getEndLogValue());
        Assert.assertNotNull(dto.getComment());
        Assert.assertEquals(14, dto.getActivitySize());
    }

    public void psObservationRoutePreCreate(ObserveServicesProvider servicesProvider, OpenableService service) {
        Form<RouteDto> form = preCreate(this, service, RouteDto.class);
        RouteDto dto = form.getObject();
        Assert.assertEquals(0, Dates.createDate(9, 4, 2019).compareTo(dto.getDate()));
        Assert.assertEquals(Float.valueOf(1323.0F), dto.getStartLogValue());
        Assert.assertEquals(0, dto.getActivitySize());
        Assert.assertNull(dto.getEndLogValue());
        Assert.assertNull(dto.getComment());
    }

}
