package fr.ird.observe.services.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.collect.ArrayListMultimap;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;
import fr.ird.observe.navigation.id.IdModel;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.id.Project;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.ObserveServiceFixtures;
import fr.ird.observe.services.service.ReferenceServiceFixtures;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class RootOpenableServiceFixtures extends GeneratedRootOpenableServiceFixtures {

    private static final Logger log = LogManager.getLogger(RootOpenableServiceFixtures.class);

    @SuppressWarnings("unused")
    public static
    class SaveResult<D extends RootOpenableDto> {
        private final Form<D> createForm;
        private final SaveResultDto createSave;
        private final Form<D> updateForm;
        private final SaveResultDto updateSave;

        public SaveResult(Form<D> createForm, SaveResultDto createSave, Form<D> updateForm, SaveResultDto updateSave) {
            this.createForm = createForm;
            this.createSave = createSave;
            this.updateForm = updateForm;
            this.updateSave = updateSave;
        }

        public Form<D> getCreateForm() {
            return createForm;
        }

        public SaveResultDto getCreateSave() {
            return createSave;
        }

        public Form<D> getUpdateForm() {
            return updateForm;
        }

        public SaveResultDto getUpdateSave() {
            return updateSave;
        }
    }

    public static <D extends RootOpenableDto> void getGroupByValue(RootOpenableServiceFixtures fixtures, RootOpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByDtoType(dtoType);
        String parentProperty = "data" + Strings.capitalize(businessModule.getName()) + "CommonTripGroupByObservationsProgram";
        String groupByValue = service.getGroupByValue(parentProperty, null, id);
        Assert.assertNotNull(groupByValue);
        String countPropertyName = variableName.replace(".id", ".getGroupByValue");
        assertGroupByValue(fixtures, groupByValue, countPropertyName);
    }

    public static <D extends RootOpenableDto> List<ToolkitIdLabel> getBrothers(RootOpenableServiceFixtures fixtures, RootOpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByDtoType(dtoType);
        String parentProperty = "data" + Strings.capitalize(businessModule.getName()) + "CommonTripGroupByObservationsProgram";
        String parentId = fixtures.getProperty(variableName.replace(".id", ".parentId"));
        List<ToolkitIdLabel> brothers = service.getBrothers(new DataGroupByParameter(parentProperty, null, parentId), id);
        Assert.assertNotNull(brothers);
        int actualCount = brothers.size();
        String countPropertyName = variableName.replace(".id", ".getBrothers.count");
        assertCount(fixtures, actualCount, countPropertyName);
        return brothers;
    }

    public static <D extends RootOpenableDto, R extends DataDtoReference> DataDtoReferenceSet<R> getChildren(RootOpenableServiceFixtures fixtures, RootOpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType, ".parentId");
        String parentId = fixtures.getProperty(variableName);
        String countPropertyName = variableName.replace(".parentId", ".getChildren.count");
        BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByDtoType(dtoType);
        String parentProperty = "data" + Strings.capitalize(businessModule.getName()) + "CommonTripGroupByObservationsProgram";
        DataDtoReferenceSet<R> children = service.getChildren(new DataGroupByParameter(parentProperty, null, parentId));
        Assert.assertNotNull(children);
        int actualCount = children.size();
        assertCount(fixtures, actualCount, countPropertyName);
        return children;
    }

    public static <D extends RootOpenableDto, R extends DataDtoReference> UpdatedDataDtoReferenceSet<R> getChildrenUpdate(RootOpenableServiceFixtures fixtures, RootOpenableService service, Date date, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType, ".parentId");
        String parentId = fixtures.getProperty(variableName);
        String countPropertyName = variableName.replace(".parentId", ".getChildrenUpdate.count");
        BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByDtoType(dtoType);
        String parentProperty = "data" + Strings.capitalize(businessModule.getName()) + "CommonTripGroupByObservationsProgram";
        UpdatedDataDtoReferenceSet<R> children = service.getChildrenUpdate(new DataGroupByParameter(parentProperty, null, parentId), date);
        Assert.assertNotNull(children);
        Assert.assertNotNull(children.getUpdatedReferences());
        int actualCount = children.getUpdatedReferences().size();
        assertCount(fixtures, actualCount, countPropertyName);
        return children;
    }

    public static <D extends RootOpenableDto, R extends DataDtoReference> void getGroupByDtoValue(RootOpenableServiceFixtures fixtures, RootOpenableService service, Class<D> dtoType) {
//        String variableName = fixtures.getVariableName(dtoType);
//        String id = getVariable(variableName);

        BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByDtoType(dtoType);
        List<DataGroupByDtoDefinition<?, ?>> dataGroupByDtoDefinitions = businessModule.getDataGroupByDtoDefinitions();
        Assert.assertNotNull(dataGroupByDtoDefinitions);
        Assert.assertTrue(dataGroupByDtoDefinitions.size() > 0);
        DataGroupByDtoDefinition<?, ?> dataGroupByDtoDefinition = dataGroupByDtoDefinitions.get(0);
        DataGroupByParameter groupBy = dataGroupByDtoDefinition.toParameter(null, null);

        DataGroupByDto<?> groupByDtoValue = service.getGroupByDtoValue(dataGroupByDtoDefinition.getContainerType(), groupBy);
        Assert.assertNotNull(groupByDtoValue);
        Assert.assertEquals(0, groupByDtoValue.getCount());
    }

    public static <D extends RootOpenableDto> Form<D> loadForm(RootOpenableServiceFixtures fixtures, RootOpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        Form<D> form = service.loadForm(dtoType, id);
        Assert.assertNotNull(form);
        D dto = form.getObject();
        Assert.assertEquals(id, dto.getId());
        return form;
    }

    public static <D extends RootOpenableDto> D loadDto(RootOpenableServiceFixtures fixtures, RootOpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        D dto = service.loadDto(dtoType, id);
        Assert.assertNotNull(dto);
        Assert.assertEquals(id, dto.getId());
        return dto;
    }

    public static <D extends RootOpenableDto> void exists(RootOpenableServiceFixtures fixtures, RootOpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        Assert.assertTrue(service.exists(dtoType, id));
        Assert.assertFalse(service.exists(dtoType, id + "Fake"));
    }

    public static <D extends RootOpenableDto> void delete(RootOpenableServiceFixtures fixtures, RootOpenableService service, Class<D> dtoType) {
        String variableName = fixtures.getVariableName(dtoType);
        String id = getVariable(variableName);
        Assert.assertTrue(service.exists(dtoType, id));
        log.info("Delete for: " + id);
        service.delete(dtoType, id);
        Assert.assertFalse(service.exists(dtoType, id));
    }

    public static <D extends RootOpenableDto> Form<D> preCreate(RootOpenableServiceFixtures fixtures, RootOpenableService service, Class<D> dtoType) {
        BusinessModule businessModule = ObserveBusinessProject.get().getBusinessModuleByDtoType(dtoType);
        String variableName = fixtures.getVariableName(dtoType, ".parentId");
        String parentProperty = "data" + Strings.capitalize(businessModule.getName()) + "CommonTripGroupByObservationsProgram";
        DataGroupByDtoDefinition<D, ?> definition = ObserveBusinessProject.get().getDataGroupByDtoDefinition(parentProperty);
        Assert.assertNotNull(definition);
        String parentId = fixtures.getProperty(variableName.replace(".id", ".parentId"));
        DataGroupByParameter groupByParameter = new DataGroupByParameter(parentProperty, null, parentId);
        Form<D> form = service.preCreate(dtoType, groupByParameter);
        Assert.assertNotNull(form);
        D dto = form.getObject();
        Assert.assertNull(dto.getId());
        ToolkitId groupByValue = (ToolkitId) definition.toGroupByObjectValue(dto);
        Assert.assertNotNull(groupByValue);
        Assert.assertEquals(parentId, groupByValue.getId());
        return form;
    }

    public static void assertCount(ObserveServiceFixtures<?> fixtures, int actual, String countPropertyName) {
        if (WITH_ASSERT) {
            int expected = fixtures.getIntegerProperty(countPropertyName);
            Assert.assertEquals(countPropertyName + "=" + actual, expected, actual);
        } else {
            System.out.println(countPropertyName + "=" + actual);
        }
    }

    public static void assertGroupByValue(ObserveServiceFixtures<?> fixtures, String actual, String countPropertyName) {
        if (WITH_ASSERT) {
            String expected = fixtures.getProperty(countPropertyName);
            Assert.assertEquals(countPropertyName + "=" + actual, expected, actual);
        } else {
            System.out.println(countPropertyName + "=" + actual);
        }
    }

    public static <D extends RootOpenableDto> RootOpenableServiceFixtures.SaveResult<D> save(ObserveServicesProvider servicesProvider,
                                                                                             RootOpenableServiceFixtures fixtures,
                                                                                             Class<D> dtoType,
                                                                                             Consumer<D> beanCreateFunction,
                                                                                             Consumer<D> beanUpdateFunction) throws ConcurrentModificationException {
        RootOpenableService service = servicesProvider.getRootOpenableService();
        Form<D> createForm = preCreate(fixtures, service, dtoType);
        D createDto = createForm.getObject();
        beanCreateFunction.accept(createDto);
        SaveResultDto createSave = service.save(createDto);

        Form<D> updateForm = service.loadForm(dtoType, createSave.getId());
        D updateDto = updateForm.getObject();
        beanUpdateFunction.accept(updateDto);
        SaveResultDto updateSave = service.save(updateDto);

        // on fait croire que notre version est plus ancienne
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(updateDto.getLastUpdateDate());
        calendar.add(Calendar.HOUR, -1);
        updateDto.setLastUpdateDate(calendar.getTime());

        try {
            service.save(updateDto);
            Assert.fail(String.format("Should got a concurrent modification on %s", updateDto));
        } catch (ConcurrentModificationException e) {
            Assert.assertTrue(true);
        }
        return new RootOpenableServiceFixtures.SaveResult<>(createForm, createSave, updateForm, updateSave);
    }


    @Override
    public void computeMissingReferential(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        String id = getProperty("computeMissingReferential.id");
        MissingReferentialResult actual = service.computeMissingReferential(TripDto.class, MissingReferentialRequest.of(Map.of(), id));
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getSqlCode());
        Assert.assertNotNull(actual.getMissingIds());
        MissingReferentialResult actualNull = service.computeMissingReferential(TripDto.class, MissingReferentialRequest.of(actual.getMissingIds(), id));
        Assert.assertNull(actualNull);
    }

    @Override
    public void getGroupByValue(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        for (Class<? extends RootOpenableDto> dtoType : ObserveBusinessProject.get().getRootOpenableDataTypes()) {
            getGroupByValue(this, service, dtoType);
        }
    }

    @Override
    public void getBrothers(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        for (Class<? extends RootOpenableDto> dtoType : ObserveBusinessProject.get().getRootOpenableDataTypes()) {
            getBrothers(this, service, dtoType);
        }
    }

    @Override
    public void getChildren(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        for (Class<? extends RootOpenableDto> dtoType : ObserveBusinessProject.get().getRootOpenableDataTypes()) {
            getChildren(this, service, dtoType);
        }
    }

    @Override
    public void getGroupByDtoValue(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        for (Class<? extends RootOpenableDto> dtoType : ObserveBusinessProject.get().getRootOpenableDataTypes()) {
            getGroupByDtoValue(this, service, dtoType);
        }
    }

    @Override
    public void getChildrenUpdate(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        for (Class<? extends RootOpenableDto> dtoType : ObserveBusinessProject.get().getRootOpenableDataTypes()) {
            getChildrenUpdate(this, service, DATE, dtoType);
        }
    }

    @Override
    public void loadForm(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        for (Class<? extends RootOpenableDto> dtoType : ObserveBusinessProject.get().getRootOpenableDataTypes()) {
            loadForm(this, service, dtoType);
        }
    }

    @Override
    public void loadDto(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        for (Class<? extends RootOpenableDto> dtoType : ObserveBusinessProject.get().getRootOpenableDataTypes()) {
            loadDto(this, service, dtoType);
        }
    }

    @Override
    public void preCreate(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        for (Class<? extends RootOpenableDto> dtoType : ObserveBusinessProject.get().getRootOpenableDataTypes()) {
            preCreate(this, service, dtoType);
        }
    }

    @Override
    public void exists(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        for (Class<? extends RootOpenableDto> dtoType : ObserveBusinessProject.get().getRootOpenableDataTypes()) {
            exists(this, service, dtoType);
        }
    }

    @Override
    public void delete(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        Set<Class<? extends RootOpenableDto>> types = ObserveBusinessProject.get().getRootOpenableDataTypes();
        Project editModel = new Project();
        ArrayListMultimap<Integer, Class<? extends RootOpenableDto>> orderedTypes = ArrayListMultimap.create();
        for (IdModel model : editModel.getModels()) {
            for (IdNode<?> node : model.getNodes()) {
                if (types.contains(node.getType())) {
                    @SuppressWarnings("unchecked") Class<? extends RootOpenableDto> dtoType = (Class<? extends RootOpenableDto>) node.getType();
                    orderedTypes.put(node.getLevel(), dtoType);
                }
            }
        }
        List<Integer> order = new LinkedList<>(orderedTypes.keySet());
        order.sort(Integer::compareTo);
        Collections.reverse(order);
        for (int thisOrder : order) {
            for (Class<? extends RootOpenableDto> dtoType : orderedTypes.get(thisOrder)) {
                delete(this, service, dtoType);
            }
        }
    }

    @Override
    public void save(ObserveServicesProvider servicesProvider, RootOpenableService service) {
        try {
            saveLlTrip(servicesProvider);
            savePsTrip(servicesProvider);
        } catch (ConcurrentModificationException e) {
            throw new RuntimeException(e);
        }
    }

    public void savePsTrip(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider, this,
                TripDto.class,
                d -> {
                    d.setOcean(ReferenceServiceFixtures.oceanReference());
                    d.setObservationsProgram(ReferenceServiceFixtures.psProgramReference());
                    d.setVessel(ReferenceServiceFixtures.vesselReference());
                    d.setDepartureHarbour(ReferenceServiceFixtures.harbourReference());
                    d.setLandingHarbour(ReferenceServiceFixtures.harbourReference());
                },
                d -> {
                    //FIXME:Test
                });
    }

    public void saveLlTrip(ObserveServicesProvider servicesProvider) throws ConcurrentModificationException {
        save(
                servicesProvider,
                this,
                fr.ird.observe.dto.data.ll.common.TripDto.class,
                d -> {
                    d.setOcean(ReferenceServiceFixtures.oceanReference());
                    d.setObservationsProgram(ReferenceServiceFixtures.llProgramReference());
                    d.setVessel(ReferenceServiceFixtures.vesselReference());
                    d.setDepartureHarbour(ReferenceServiceFixtures.harbourReference());
//                    d.setLandingHarbour(new HarbourReference(ReferentialLocale.FR, harbourDto));
                    d.setTripType(ReferenceServiceFixtures.tripTypeReference());
                },
                d -> {
                    //FIXME:Test
                });
    }

}
