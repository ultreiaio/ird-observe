package fr.ird.observe.services.service.data;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.ObserveServiceFixtures;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.junit.Assert;

import java.util.function.Consumer;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SimpleServiceFixtures extends GeneratedSimpleServiceFixtures {

    @SuppressWarnings("unused")
    public static
    class SaveResult<D extends SimpleDto> {
        private final Form<D> updateForm;
        private final SaveResultDto updateSave;

        public SaveResult(Form<D> updateForm, SaveResultDto updateSave) {
            this.updateForm = updateForm;
            this.updateSave = updateSave;
        }

        public Form<D> getUpdateForm() {
            return updateForm;
        }

        public SaveResultDto getUpdateSave() {
            return updateSave;
        }
    }

    public static <D extends SimpleDto> SaveResult<D> save(ObserveServiceFixtures<?> fixtures, SimpleService service, Class<D> dtoType, Consumer<D> beanUpdateFunction) throws ConcurrentModificationException {
        Class<BusinessDto> mainDtoType = ObserveBusinessProject.get().getMapping().getMainDtoType(dtoType);
        String variableName = fixtures.getVariableName(mainDtoType);
        String id = getVariable(variableName);
        Form<D> updateForm = service.loadForm(dtoType, id);
        D updateDto = updateForm.getObject();
        beanUpdateFunction.accept(updateDto);
        SaveResultDto updateSave = service.save(updateDto);
        return new SaveResult<>(updateForm, updateSave);
    }

    public static <D extends SimpleDto> Form<D> loadForm(ObserveServiceFixtures<?> fixtures, SimpleService service, Class<D> dtoType) {
        Class<BusinessDto> mainDtoType = ObserveBusinessProject.get().getMapping().getMainDtoType(dtoType);
        String variableName = fixtures.getVariableName(mainDtoType);
        String id = getVariable(variableName);
        Form<D> form = service.loadForm(dtoType, id);
        Assert.assertNotNull(form);
        D dto = form.getObject();
        Assert.assertEquals(id, dto.getId());
        return form;
    }

    @Override
    public void loadForm(ObserveServicesProvider servicesProvider, SimpleService service) {
        for (Class<? extends SimpleDto> dtoType : ObserveBusinessProject.get().getSimpleDataTypes()) {
            loadForm(this, service, dtoType);
        }
    }

    @Override
    public void save(ObserveServicesProvider servicesProvider, SimpleService service) {
//        super.save(servicesProvider, service);
        for (Class<? extends SimpleDto> dtoType : ObserveBusinessProject.get().getSimpleDataTypes()) {
            try {
                save(this, service, dtoType, d -> {
                    //FIXME:Test
                });
            } catch (ConcurrentModificationException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
