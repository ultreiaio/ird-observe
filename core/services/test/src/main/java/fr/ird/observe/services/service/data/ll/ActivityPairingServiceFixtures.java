package fr.ird.observe.services.service.data.ll;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.data.ll.pairing.ActivityPairingDto;
import fr.ird.observe.dto.data.ll.pairing.ActivityPairingResult;
import fr.ird.observe.dto.data.ll.pairing.TripActivitiesPairingDto;
import fr.ird.observe.dto.data.ll.pairing.TripActivitiesPairingResult;
import fr.ird.observe.dto.data.pairing.ApplyPairingRequest;
import fr.ird.observe.services.ObserveServicesProvider;
import org.junit.Assert;

import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

//TODO Add more data in tck to test more cases
public class ActivityPairingServiceFixtures extends GeneratedActivityPairingServiceFixtures {

    @Override
    public void applyPairing(ObserveServicesProvider servicesProvider, ActivityPairingService service) {
        TripActivitiesPairingResult actual = service.computePairing(getProperty("computePairing.tripId"));
        ActivityPairingResult firstResult = assertResult(actual);

        service.applyPairing(new ApplyPairingRequest(new TreeMap<>(Map.of(firstResult.getActivityLogbook().getId(), firstResult.getItems().get(0).getObservationActivity().getId()))));

        actual = service.computePairing(getProperty("computePairing.tripId"));
        assertResultAfterPairing(actual, firstResult);
    }

    @Override
    public void computePairing(ObserveServicesProvider servicesProvider, ActivityPairingService service) {
        TripActivitiesPairingResult actual = service.computePairing(getProperty("computePairing.tripId"));
        assertResult(actual);
    }

    @Override
    public void getActivityPairingDto(ObserveServicesProvider servicesProvider, ActivityPairingService service) {
        String tripId = getProperty("getActivityPairingDto.tripId");
        ActivityPairingDto actual = service.getActivityPairingDto(tripId);
        Assert.assertNotNull(actual);
    }

    @Override
    public void getTripActivitiesPairingDto(ObserveServicesProvider servicesProvider, ActivityPairingService service) {
        String tripId = getProperty("getTripActivitiesPairingDto.tripId");
        TripActivitiesPairingDto actual = service.getTripActivitiesPairingDto(tripId);
        Assert.assertNotNull(actual);
    }
    private ActivityPairingResult assertResult(TripActivitiesPairingResult actual) {
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getItems());
        Assert.assertEquals(3, actual.getItems().size());
        Optional<ActivityPairingResult> optionalFirstResult = actual.getItems().stream().filter(i -> i.getSelectedRelatedObservedActivity() == null && !i.getItems().isEmpty()).findFirst();
        Assert.assertTrue(optionalFirstResult.isPresent());
        ActivityPairingResult firstResult = optionalFirstResult.get();

        Assert.assertNotNull(firstResult);
        Assert.assertNotNull(firstResult.getItems());
        Assert.assertNull(firstResult.getSelectedRelatedObservedActivity());
        Assert.assertEquals(2, firstResult.getItems().size());
        return firstResult;
    }

    private void assertResultAfterPairing(TripActivitiesPairingResult actual, ActivityPairingResult firstResult) {
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getItems());
        Assert.assertEquals(3, actual.getItems().size());
        Optional<ActivityPairingResult> optionalSecondResult = actual.getItems().stream().filter(i -> i.getActivityLogbook().equals(firstResult.getActivityLogbook())).findFirst();
        Assert.assertTrue(optionalSecondResult.isPresent());
        ActivityPairingResult secondResult = optionalSecondResult.get();
        Assert.assertNotNull(secondResult.getSelectedRelatedObservedActivity());
    }
}
