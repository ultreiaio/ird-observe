package fr.ird.observe.services.service.data.ll.common;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.data.ll.common.TripDto;
import fr.ird.observe.dto.data.ll.common.TripReference;
import fr.ird.observe.dto.data.ll.logbook.ActivityDto;
import fr.ird.observe.dto.data.ll.logbook.SetDto;
import fr.ird.observe.dto.data.ll.observation.BranchlineDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.data.EditableService;
import fr.ird.observe.services.service.data.EditableServiceFixtures;
import fr.ird.observe.services.service.data.MoveRequest;
import fr.ird.observe.services.service.data.OpenableServiceFixtures;
import fr.ird.observe.services.service.data.RootOpenableServiceFixtures;
import org.junit.Assert;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class TripServiceFixtures extends GeneratedTripServiceFixtures {

    @Override
    public void getAllTripIds(ObserveServicesProvider servicesProvider, TripService service) {
        Set<String> actual = service.getAllTripIds();
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getAllTrip.count"), actual.size());
    }

    @Override
    public void getMatchingTripsVesselWithinDateRange(ObserveServicesProvider servicesProvider, TripService service) {
        TripReference reference = TripReference.of(ReferentialLocale.FR, RootOpenableServiceFixtures.loadDto(new RootOpenableServiceFixtures(), servicesProvider.getRootOpenableService(), TripDto.class));
        String tripId = getProperty("getMatchingTripsVesselWithinDateRange.tripId");
        String vesselId = getProperty("getMatchingTripsVesselWithinDateRange.vesselId");
        int actual = service.getMatchingTripsVesselWithinDateRange(tripId, vesselId, reference.getStartDate(), reference.getEndDate());
        Assert.assertEquals(getIntegerProperty("getMatchingTripsVesselWithinDateRange.count"), actual);
    }

    @Override
    public void getSpeciesByListAndTrip(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("getSpeciesByListAndTrip.tripId");
        String speciesListId = getProperty("getSpeciesByListAndTrip.speciesListId");
        ReferentialDtoReferenceSet<SpeciesReference> actual = service.getSpeciesByListAndTrip(tripId, speciesListId);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getSpeciesByListAndTrip.count"), actual.size());
    }

    @Override
    public void getTripMap(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("defaultId");
        TripMapConfigDto config = new TripMapConfigDto();
        config.setTripId(tripId);
        config.setAddLogbook(true);
        config.setAddObservations(true);
        TripMapDto actual = service.getTripMap(config);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getTripMap.count"), actual.sizePoints());
    }

    @Override
    public void getLogbookCatchSpeciesIds(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("getLogbookCatchSpeciesIds.tripId");
        TripMapConfigDto config = new TripMapConfigDto();
        config.setTripId(tripId);
        config.setAddLogbook(true);
        config.setAddObservations(true);
        List<String> actual = service.getLogbookCatchSpeciesIds(tripId);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getLogbookCatchSpeciesIds.count"), actual.size());
    }

    @Override
    public void saveAndCopyProperties(ObserveServicesProvider servicesProvider, TripService service) {
        String setToCopyId = getProperty("saveAndCopyProperties.setToCopyId");
        String activityId = getProperty("saveAndCopyProperties.activityId");

        ActivityDto activity = servicesProvider.getOpenableService().loadDto(ActivityDto.class, activityId);
        Assert.assertNotNull(activity);
        Assert.assertNull(activity.getSet());
        EditableService editableService = servicesProvider.getEditableService();
        Form<SetDto> form = EditableServiceFixtures.preCreate(new EditableServiceFixtures(), editableService, SetDto.class);
        Assert.assertNotNull(form);
        SaveResultDto actual;
        try {
            actual = service.saveAndCopyProperties(activityId, setToCopyId, form.getObject());
        } catch (ConcurrentModificationException e) {
            throw new RuntimeException(e);
        }
        Assert.assertNotNull(actual);

        SetDto newSet = editableService.loadDto(SetDto.class, actual.getId());
        Assert.assertNotNull(newSet);

        activity = servicesProvider.getOpenableService().loadDto(ActivityDto.class, activityId);
        Assert.assertNotNull(activity);
        Assert.assertNotNull(activity.getSet());
    }


    @Override
    public void loadBranchlineForm(fr.ird.observe.services.ObserveServicesProvider servicesProvider, TripService service) {
        Form<BranchlineDto> actual = service.loadBranchlineForm(getProperty("loadBranchlineForm.branchlineId"));
        Assert.assertNotNull(actual);
    }

    @Override
    public void saveBranchline(fr.ird.observe.services.ObserveServicesProvider servicesProvider, TripService service) {
        Form<BranchlineDto> loaded = service.loadBranchlineForm(getProperty("loadBranchlineForm.branchlineId"));
        Assert.assertNotNull(loaded);
        try {
            SaveResultDto actual = service.saveBranchline(loaded.getObject());
            Assert.assertNotNull(actual);
        } catch (ConcurrentModificationException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void getSampleActivityParentCandidate(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("getSampleActivityParentCandidate.tripId");
        String activityId = getProperty("getSampleActivityParentCandidate.activityId");
        List<ToolkitIdLabel> children = service.getSampleActivityParentCandidate(tripId, activityId);
        Assert.assertNotNull(children);
        int count = getIntegerProperty("getSampleActivityParentCandidate.count");
        Assert.assertEquals(count, children.size());
    }

    @Override
    public void moveActivitySample(ObserveServicesProvider servicesProvider, TripService service) {
        String id = getProperty("moveActivitySample.id");
        ToolkitIdDtoBean oldParentId = OpenableServiceFixtures.getIdReference(this, "moveActivitySample.oldParentId");
        ToolkitIdDtoBean newParentId = OpenableServiceFixtures.getIdReference(this, "moveActivitySample.newParentIdActivity");
        try {
            service.moveActivitySample(new MoveRequest(oldParentId, newParentId, Collections.singleton(id), true));
        } catch (ConcurrentModificationException e) {
            throw new RuntimeException(e);
        }
    }

    public void moveToTrip(TripService service) {
        String id = getProperty("moveActivitySample.id");
        ToolkitIdDtoBean oldParentId = OpenableServiceFixtures.getIdReference(this, "moveActivitySample.oldParentId");
        ToolkitIdDtoBean newParentId = OpenableServiceFixtures.getIdReference(this, "moveActivitySample.newParentIdTrip");
        try {
            service.moveActivitySample(new MoveRequest(oldParentId, newParentId, Collections.singleton(id), false));
        } catch (ConcurrentModificationException e) {
            throw new RuntimeException(e);
        }
    }
}
