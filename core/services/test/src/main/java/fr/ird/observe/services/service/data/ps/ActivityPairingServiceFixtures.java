package fr.ird.observe.services.service.data.ps;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.data.pairing.ApplyPairingRequest;
import fr.ird.observe.dto.data.ps.pairing.ActivityPairingResult;
import fr.ird.observe.dto.data.ps.pairing.RoutePairingDto;
import fr.ird.observe.dto.data.ps.pairing.RoutePairingResult;
import fr.ird.observe.dto.data.ps.pairing.TripActivitiesPairingDto;
import fr.ird.observe.dto.data.ps.pairing.TripActivitiesPairingResult;
import fr.ird.observe.services.ObserveServicesProvider;
import org.junit.Assert;

import java.util.Map;
import java.util.TreeMap;

//TODO Add more data in tck to test more cases
public class ActivityPairingServiceFixtures extends GeneratedActivityPairingServiceFixtures {

    @Override
    public void applyPairing(fr.ird.observe.services.ObserveServicesProvider servicesProvider, ActivityPairingService service) {
        TripActivitiesPairingResult actual = service.computePairing(getProperty("computePairing.tripId"));
        ActivityPairingResult firstResult = assertResult(actual);

        // apply but with no change
        service.applyPairing(new ApplyPairingRequest(new TreeMap<>(Map.of(firstResult.getActivityLogbook().getId(), firstResult.getSelectedRelatedObservedActivity().getObservationActivity().getId()))));

        actual = service.computePairing(getProperty("computePairing.tripId"));
        assertResult(actual);

        // apply but with change
        TreeMap<String, String> map = new TreeMap<>();
        map.put(firstResult.getActivityLogbook().getId(), null);
        service.applyPairing(new ApplyPairingRequest(map));

        actual = service.computePairing(getProperty("computePairing.tripId"));
        assertResultAfterPairing(actual);
    }

    @Override
    public void computePairing(fr.ird.observe.services.ObserveServicesProvider servicesProvider, ActivityPairingService service) {
        TripActivitiesPairingResult actual = service.computePairing(getProperty("computePairing.tripId"));
        assertResult(actual);
    }

    @Override
    public void getRoutePairingDto(ObserveServicesProvider servicesProvider, ActivityPairingService service) {
        String tripId = getProperty("getRoutePairingDto.tripId");
        String logbookRouteId = getProperty("getRoutePairingDto.logbookRouteId");
        RoutePairingDto actual = service.getRoutePairingDto(tripId, logbookRouteId);
        Assert.assertNotNull(actual);
    }

    @Override
    public void getTripActivitiesPairingDto(ObserveServicesProvider servicesProvider, ActivityPairingService service) {
        String tripId = getProperty("getTripActivitiesPairingDto.tripId");
        TripActivitiesPairingDto actual = service.getTripActivitiesPairingDto(tripId);
        Assert.assertNotNull(actual);
    }
    private ActivityPairingResult assertResult(TripActivitiesPairingResult actual) {
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getItems());
        Assert.assertEquals(2, actual.getItems().size());
        RoutePairingResult routePairingResult = actual.getItems().get(0);
        Assert.assertNotNull(routePairingResult);
        Assert.assertNotNull(routePairingResult.getItems());
        Assert.assertEquals(1, routePairingResult.getItems().size());
        ActivityPairingResult activityPairingResult = routePairingResult.getItems().get(0);
        Assert.assertNotNull(activityPairingResult);
        Assert.assertNotNull(activityPairingResult.getItems());
        Assert.assertEquals(2, activityPairingResult.getItems().size());
        Assert.assertNotNull(activityPairingResult.getSelectedRelatedObservedActivity());
        return activityPairingResult;
    }

    private void assertResultAfterPairing(TripActivitiesPairingResult actual) {
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getItems());
        Assert.assertEquals(2, actual.getItems().size());
    }
}
