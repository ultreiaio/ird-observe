package fr.ird.observe.services.service.data.ps;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.services.ObserveServicesProvider;

public class AvdthServiceFixtures extends GeneratedAvdthServiceFixtures {

    @Override
    public void importData(ObserveServicesProvider servicesProvider, AvdthService service) {
        // FIXME:Test Remove super method invocation and implements fixture
        // actual = service.importData(getProperty("importData.id"));
        // Assert.assertNotNull(actual);
        super.importData(servicesProvider, service);
    }

}
