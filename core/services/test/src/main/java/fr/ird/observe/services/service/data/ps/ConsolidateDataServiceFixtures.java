package fr.ird.observe.services.service.data.ps;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.consolidation.data.ps.common.TripConsolidateRequest;
import fr.ird.observe.consolidation.data.ps.common.TripConsolidateResult;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeManager;
import fr.ird.observe.consolidation.data.ps.dcp.SimplifiedObjectTypeSpecializedRules;
import fr.ird.observe.consolidation.data.ps.localmarket.BatchConsolidateRequest;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.dto.data.ps.localmarket.BatchDto;
import fr.ird.observe.dto.data.ps.localmarket.BatchWeightComputedValueSource;
import fr.ird.observe.dto.data.ps.localmarket.TripBatchDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.dto.referential.ps.localmarket.PackagingReference;
import fr.ird.observe.services.ObserveServicesProvider;
import io.ultreia.java4all.util.Dates;
import org.junit.Assert;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class ConsolidateDataServiceFixtures extends GeneratedConsolidateDataServiceFixtures {
    private final SimplifiedObjectTypeSpecializedRules simplifiedObjectTypeSpecializedRules;

    public ConsolidateDataServiceFixtures() {
        URL definition = getClass().getResource("/observe-specialized-fad-rules.properties");
        simplifiedObjectTypeSpecializedRules = SimplifiedObjectTypeSpecializedRules.of(definition);
    }

    @Override
    public void newSimplifiedObjectTypeManager(ObserveServicesProvider servicesProvider, ConsolidateDataService service) {
        SimplifiedObjectTypeManager actual = service.newSimplifiedObjectTypeManager(simplifiedObjectTypeSpecializedRules);
        Assert.assertNotNull(actual);
        DecoratorService decoratorService = new DecoratorService(ReferentialLocale.FR);
        {
            fr.ird.observe.consolidation.data.ps.logbook.FloatingObjectConsolidateEngine engine = new fr.ird.observe.consolidation.data.ps.logbook.FloatingObjectConsolidateEngine(actual, decoratorService);
            String id = getProperty("consolidateFloatingObjectLogbook.id");
            fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto dto = servicesProvider.getEditableService().loadDto(fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto.class, id);
            Assert.assertNotNull(dto.getComputedWhenArrivingSimplifiedObjectType());
            Assert.assertNotNull(dto.getComputedWhenArrivingBiodegradable());
            Assert.assertNotNull(dto.getComputedWhenArrivingNonEntangling());
            dto.setComputedWhenArrivingSimplifiedObjectType(null);
            dto.setComputedWhenArrivingBiodegradable(null);
            dto.setComputedWhenArrivingNonEntangling(null);
            fr.ird.observe.consolidation.data.ps.logbook.FloatingObjectConsolidateRequest request = new fr.ird.observe.consolidation.data.ps.logbook.FloatingObjectConsolidateRequest(dto, Set.copyOf(dto.getFloatingObjectPart()));
            Optional<ToolkitIdModifications> optionalResult = engine.consolidate(request);
            Assert.assertTrue(optionalResult.isPresent());
            ToolkitIdModifications result = optionalResult.get();
            Assert.assertNotNull(result.getModifications());
            Assert.assertEquals(getIntegerProperty("consolidateFloatingObjectLogbook.count"), result.getModifications().size());

            Assert.assertNull(dto.getComputedWhenArrivingSimplifiedObjectType());
            Assert.assertNull(dto.getComputedWhenArrivingBiodegradable());
            Assert.assertNull(dto.getComputedWhenArrivingNonEntangling());

            result.flushToBean(dto);

            Assert.assertNotNull(dto.getComputedWhenArrivingSimplifiedObjectType());
            Assert.assertNotNull(dto.getComputedWhenArrivingBiodegradable());
            Assert.assertNotNull(dto.getComputedWhenArrivingNonEntangling());

            optionalResult = engine.consolidate(request);
            Assert.assertTrue(optionalResult.isEmpty());
        }
        {
            fr.ird.observe.consolidation.data.ps.observation.FloatingObjectConsolidateEngine engine = new fr.ird.observe.consolidation.data.ps.observation.FloatingObjectConsolidateEngine(actual, decoratorService);
            String id = getProperty("consolidateFloatingObjectObservation.id");
            fr.ird.observe.dto.data.ps.observation.FloatingObjectDto dto = servicesProvider.getEditableService().loadDto(fr.ird.observe.dto.data.ps.observation.FloatingObjectDto.class, id);
            Assert.assertNotNull(dto.getComputedWhenArrivingSimplifiedObjectType());
            Assert.assertNotNull(dto.getComputedWhenArrivingBiodegradable());
            Assert.assertNotNull(dto.getComputedWhenArrivingNonEntangling());
            dto.setComputedWhenArrivingSimplifiedObjectType(null);
            dto.setComputedWhenArrivingBiodegradable(null);
            dto.setComputedWhenArrivingNonEntangling(null);
            fr.ird.observe.consolidation.data.ps.observation.FloatingObjectConsolidateRequest request = new fr.ird.observe.consolidation.data.ps.observation.FloatingObjectConsolidateRequest(dto, Set.copyOf(dto.getFloatingObjectPart()));
            Optional<ToolkitIdModifications> optionalResult = engine.consolidate(request);
            Assert.assertTrue(optionalResult.isPresent());
            ToolkitIdModifications result = optionalResult.get();
            Assert.assertNotNull(result.getModifications());
            Assert.assertEquals(getIntegerProperty("consolidateFloatingObjectObservation.count"), result.getModifications().size());

            Assert.assertNull(dto.getComputedWhenArrivingSimplifiedObjectType());
            Assert.assertNull(dto.getComputedWhenArrivingBiodegradable());
            Assert.assertNull(dto.getComputedWhenArrivingNonEntangling());

            result.flushToBean(dto);

            Assert.assertNotNull(dto.getComputedWhenArrivingSimplifiedObjectType());
            Assert.assertNotNull(dto.getComputedWhenArrivingBiodegradable());
            Assert.assertNotNull(dto.getComputedWhenArrivingNonEntangling());

            optionalResult = engine.consolidate(request);
            Assert.assertTrue(optionalResult.isEmpty());
        }
    }

    @Override
    public void consolidateLocalmarketBatch(ObserveServicesProvider servicesProvider, ConsolidateDataService service) {
        BatchConsolidateRequest request = new BatchConsolidateRequest();
        Form<TripBatchDto> form = servicesProvider.getContainerService().loadForm(TripBatchDto.class, getProperty("consolidateTrip.id"));
        Assert.assertNotNull(form);
        List<BatchDto> children = form.getObject().getChildren();
        Assert.assertNotNull(children);
        Assert.assertEquals(2, children.size());
        request.setOceanId("fr.ird.referential.common.Ocean#1239832686152#0.8325731048817705");
        request.setBatch(children.get(0));
        ToolkitIdModifications actual = service.consolidateLocalmarketBatch(request);

        Assert.assertNotNull(actual);
        Assert.assertEquals(2, actual.modificationsCount());

        {

            BatchDto createDto = new BatchDto();
            createDto.setId("fr.ird.data.ps.localmarket.Batch#1617103690104#0.1");
            createDto.setCount(5);
            createDto.setPackaging(servicesProvider.getReferenceService().loadReferential(PackagingReference.class, "fr.ird.referential.ps.localmarket.Packaging#1464000000000#001"));
            createDto.setSpecies(servicesProvider.getReferenceService().loadReferential(SpeciesReference.class, "fr.ird.referential.common.Species#1560863653582#0.22356459159799613"));
            createDto.setDate(Dates.createDate(30, 3, 2021));
            request.setBatch(createDto);
            request.setDate(createDto.getDate());

            actual = service.consolidateLocalmarketBatch(request);

            Assert.assertNotNull(actual);
        }
        {

            BatchDto createDto = new BatchDto();
            createDto.setId("fr.ird.data.ps.localmarket.Batch#1617103690104#0.2");
            createDto.setCount(5);
            createDto.setPackaging(servicesProvider.getReferenceService().loadReferential(PackagingReference.class, "fr.ird.referential.ps.localmarket.Packaging#1464000000000#039"));
            createDto.setSpecies(servicesProvider.getReferenceService().loadReferential(SpeciesReference.class, "fr.ird.referential.common.Species#1560863653582#0.22356459159799613"));
            createDto.setDate(Dates.createDate(30, 3, 2021));
            request.setBatch(createDto);
            request.setDate(createDto.getDate());

            actual = service.consolidateLocalmarketBatch(request);

            Assert.assertNotNull(actual);
        }
        {

            BatchDto createDto = new BatchDto();
            createDto.setId("fr.ird.data.ps.localmarket.Batch#1617103690104#0.3");
            createDto.setWeightComputedSource(BatchWeightComputedValueSource.fromPackagingMeanWeight);
            createDto.setPackaging(servicesProvider.getReferenceService().loadReferential(PackagingReference.class, "fr.ird.referential.ps.localmarket.Packaging#1464000000000#022"));
            createDto.setSpecies(servicesProvider.getReferenceService().loadReferential(SpeciesReference.class, "fr.ird.referential.common.Species#1560863653582#0.22356459159799613"));
            createDto.setDate(Dates.createDate(30, 3, 2021));
            request.setBatch(createDto);
            request.setDate(createDto.getDate());

            actual = service.consolidateLocalmarketBatch(request);

            Assert.assertNotNull(actual);
        }

        {

            BatchDto createDto = new BatchDto();
            createDto.setId("fr.ird.data.ps.localmarket.Batch#1617103690104#0.4");
            createDto.setWeightComputedSource(BatchWeightComputedValueSource.fromSpeciesMeanWeight);
            createDto.setPackaging(servicesProvider.getReferenceService().loadReferential(PackagingReference.class, "fr.ird.referential.ps.localmarket.Packaging#1464000000000#022"));
            createDto.setSpecies(servicesProvider.getReferenceService().loadReferential(SpeciesReference.class, "fr.ird.referential.common.Species#1560863653582#0.22356459159799613"));
            createDto.setDate(Dates.createDate(30, 3, 2021));
            createDto.setWeight(100.0f);
            request.setBatch(createDto);
            request.setDate(createDto.getDate());

            actual = service.consolidateLocalmarketBatch(request);

            Assert.assertNotNull(actual);
        }
    }

    @Override
    public void consolidateTrip(ObserveServicesProvider servicesProvider, ConsolidateDataService service) {
        TripConsolidateRequest request = new TripConsolidateRequest();
        request.setTripId(getProperty("consolidateTrip.id"));
        request.setFailIfLengthWeightParameterNotFound(false);
        request.setSpeciesListForLogbookSampleActivityWeightedWeight(getProperty("consolidateTrip.speciesListForLogbookSampleActivityWeightedWeight"));
        request.setSpeciesListForLogbookSampleWeights(getProperty("consolidateTrip.speciesListForLogbookSampleWeights"));

        TripConsolidateResult actual = service.consolidateTrip(simplifiedObjectTypeSpecializedRules, request);
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getActivityObservationResults());
        Assert.assertNotNull(actual.getActivityLogbookResults());
        Assert.assertNotNull(actual.getLogbookSampleResults());
        Assert.assertEquals(getIntegerProperty("consolidateTrip.logbookActivity.count"), actual.getActivityLogbookResults().size());
        Assert.assertEquals(getIntegerProperty("consolidateTrip.logbookSample.count"), actual.getLogbookSampleResults().stream().filter(r -> r.getModifications() != null && r.getModifications().withModifications()).count());
        Assert.assertEquals(getIntegerProperty("consolidateTrip.logbookSampleActivity.count"), actual.getLogbookSampleResults().stream().filter(r -> r.getSampleActivityModifications() != null).flatMap(r -> r.getSampleActivityModifications().stream()).filter(ToolkitIdModifications::withModifications).count());
        Assert.assertEquals(getIntegerProperty("consolidateTrip.observationsActivityResult.count"), actual.getActivityObservationResults().size());
        Assert.assertEquals(getIntegerProperty("consolidateTrip.observationsActivityResult.withModifications.count"), actual.getActivityObservationResults().stream().filter(fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult::withModifications).count());
        Assert.assertEquals(getIntegerProperty("consolidateTrip.observationsActivityResult.withWarnings.count"), actual.getActivityObservationResults().stream().filter(fr.ird.observe.consolidation.data.ps.observation.ActivityConsolidateResult::withWarnings).count());
    }

}
