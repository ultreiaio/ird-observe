package fr.ird.observe.services.service.data.ps.common;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.dto.data.TripMapConfigDto;
import fr.ird.observe.dto.data.TripMapDto;
import fr.ird.observe.dto.data.ps.common.TripDto;
import fr.ird.observe.dto.data.ps.common.TripLocalmarketDto;
import fr.ird.observe.dto.data.ps.common.TripLogbookDto;
import fr.ird.observe.dto.data.ps.common.TripReference;
import fr.ird.observe.dto.data.ps.dcp.FloatingObjectPreset;
import fr.ird.observe.dto.data.ps.logbook.ActivityStubDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.common.SpeciesReference;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.data.EditableServiceFixtures;
import fr.ird.observe.services.service.data.RootOpenableServiceFixtures;
import org.junit.Assert;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class TripServiceFixtures extends GeneratedTripServiceFixtures {

//    @Override
//    public void preCreate(ObserveServicesProvider servicesProvider, TripService service) {
//        Form<TripDto> form = OpenableDataServiceFixtures.preCreate(servicesProvider, this, service);
//
//        TripDto dto = form.getObject();
//
//        Assert.assertNull(dto.getCaptain());
//        Assert.assertNull(dto.getObserver());
//        Assert.assertNull(dto.getObservationsDataEntryOperator());
//        Assert.assertNull(dto.getVessel());
//        Assert.assertNull(dto.getOcean());
//        Assert.assertNull(dto.getDepartureHarbour());
//        Assert.assertNull(dto.getLandingHarbour());
//        Assert.assertNull(dto.getErsId());
//        Assert.assertEquals(Dates.getDay(ObserveServiceFixtures.DATE), dto.getStartDate());
//        Assert.assertEquals(Dates.getDay(ObserveServiceFixtures.DATE), dto.getEndDate());
//        Assert.assertNull(dto.getFormsUrl());
//        Assert.assertNull(dto.getReportsUrl());
//        Assert.assertNull(dto.getGeneralComment());
//        Assert.assertNull(dto.getLogbookComment());
//        Assert.assertNull(dto.getLastUpdateDate());
//    }

    @Override
    public void getAllTripIds(ObserveServicesProvider servicesProvider, TripService service) {
        Set<String> actual = service.getAllTripIds();
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getAllTrip.count"), actual.size());
    }

    @Override
    public void getMatchingTripsVesselWithinDateRange(ObserveServicesProvider servicesProvider, TripService service) {
        TripReference reference = TripReference.of(ReferentialLocale.FR, RootOpenableServiceFixtures.loadDto(new RootOpenableServiceFixtures(), servicesProvider.getRootOpenableService(), TripDto.class));
        String tripId = getProperty("getMatchingTripsVesselWithinDateRange.tripId");
        String vesselId = getProperty("getMatchingTripsVesselWithinDateRange.vesselId");
        int actual = service.getMatchingTripsVesselWithinDateRange(tripId, vesselId, reference.getStartDate(), reference.getEndDate());
        Assert.assertEquals(getIntegerProperty("getMatchingTripsVesselWithinDateRange.count"), actual);
    }

    @Override
    public void getSpeciesByListAndTrip(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("getSpeciesByListAndTrip.tripId");
        String speciesListId = getProperty("getSpeciesByListAndTrip.speciesListId");
        ReferentialDtoReferenceSet<SpeciesReference> actual = service.getSpeciesByListAndTrip(tripId, speciesListId);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getSpeciesByListAndTrip.count"), actual.size());
    }

    @Override
    public void getTripMap(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("defaultId");
        TripMapConfigDto config = new TripMapConfigDto();
        config.setTripId(tripId);
        config.setAddLogbook(true);
        config.setAddObservations(true);
        TripMapDto actual = service.getTripMap(config);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getTripMap.count"), actual.sizePoints());
    }

    @Override
    public void isActivityEndOfSearchFound(ObserveServicesProvider servicesProvider, TripService service) {
        String routeId = getProperty("isActivityEndOfSearchFound.routeId");
        boolean activityEndOfSearchFound = service.isActivityEndOfSearchFound(routeId);
        Assert.assertTrue(activityEndOfSearchFound);
    }

    @Override
    public void isActivitiesAcquisitionModeByTimeEnabled(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("isActivitiesAcquisitionModeByTimeEnabled.tripId");
        boolean actual = service.isActivitiesAcquisitionModeByTimeEnabled(tripId);
        Assert.assertTrue(actual);
    }

    @Override
    public void preCreateLogbookFloatingObject(ObserveServicesProvider servicesProvider, TripService service) {
        String activityId = getProperty("preCreateLogbookFloatingObject.activityId");
        fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto dto = EditableServiceFixtures.loadDto(this, servicesProvider.getEditableService(), fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto.class);
        FloatingObjectPreset preset = dto.toPreset();
        Form<fr.ird.observe.dto.data.ps.logbook.FloatingObjectDto> actual = service.preCreateLogbookFloatingObject(activityId, preset);
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getObject());
        Assert.assertNotNull(actual.getObject().getFirstBuoy());
        Assert.assertNull(actual.getObject().getSecondBuoy());
    }

    @Override
    public void preCreateObservationFloatingObject(ObserveServicesProvider servicesProvider, TripService service) {
        String activityId = getProperty("preCreateObservationFloatingObject.activityId");
        fr.ird.observe.dto.data.ps.observation.FloatingObjectDto dto = EditableServiceFixtures.loadDto(this, servicesProvider.getEditableService(), fr.ird.observe.dto.data.ps.observation.FloatingObjectDto.class);
        FloatingObjectPreset preset = dto.toPreset();
        Form<fr.ird.observe.dto.data.ps.observation.FloatingObjectDto> actual = service.preCreateObservationFloatingObject(activityId, preset);
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getObject());
        Assert.assertNotNull(actual.getObject().getFirstBuoy());
        Assert.assertNull(actual.getObject().getSecondBuoy());
    }

    @Override
    public void loadLocalmarketForm(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("loadLocalmarketForm.tripId");
        Form<TripLocalmarketDto> actual = service.loadLocalmarketForm(tripId);
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getObject());
        Assert.assertNotNull(actual.getObject().getId());
        Assert.assertEquals(tripId, actual.getObject().getId());
    }

    @Override
    public void getLogbookWellPlanActivities(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("getLogbookWellPlanActivities.tripId");
        List<ActivityStubDto> actual = service.getLogbookWellPlanActivities(tripId);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getLogbookWellPlanActivities.count"), actual.size());
    }

    @Override
    public void loadLogbookForm(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("loadLogbookForm.tripId");
        Form<TripLogbookDto> actual = service.loadLogbookForm(tripId);
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getObject());
        Assert.assertNotNull(actual.getObject().getId());
        Assert.assertEquals(tripId, actual.getObject().getId());
    }

    @Override
    public void getLogbookWellIdsFromSample(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("getLogbookWellIdsFromSample.tripId");
        Set<String> actual = service.getLogbookWellIdsFromSample(tripId);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getLogbookWellIdsFromSample.count"), actual.size());
    }

    @Override
    public void getLogbookWellIdsFromWellPlan(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("getLogbookWellIdsFromWellPlan.tripId");
        Set<String> actual = service.getLogbookWellIdsFromWellPlan(tripId);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getLogbookWellIdsFromWellPlan.count"), actual.size());
    }

    @Override
    public void createLogbookSampleActivityFromWellPlan(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("createLogbookSampleActivityFromWellPlan.tripId");
        String wellId = getProperty("createLogbookSampleActivityFromWellPlan.wellId");
        Set<String> actual = service.createLogbookSampleActivityFromWellPlan(tripId, wellId);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("createLogbookSampleActivityFromWellPlan.count"), actual.size());
    }

    @Override
    public void createLogbookWellActivityFromSample(ObserveServicesProvider servicesProvider, TripService service) {
        String tripId = getProperty("createLogbookWellActivityFromSample.tripId");
        String wellId = getProperty("createLogbookWellActivityFromSample.wellId");
        Set<String> actual = service.createLogbookWellActivityFromSample(tripId, wellId);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("createLogbookWellActivityFromSample.count"), actual.size());
    }
}
