package fr.ird.observe.services.service.referential;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferenceSetsRequest;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ps.common.ProgramDto;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.module.ObserveBusinessProject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ReferentialServiceFixtures extends GeneratedReferentialServiceFixtures {
    private static final Logger log = LogManager.getLogger(ReferentialServiceFixtures.class);

    public static List<Class<? extends ReferentialDto>> data() {
        return ObserveBusinessProject.get().getReferentialTypes().stream()
                                     .sorted(Comparator.comparing(Object::toString))
                                     .collect(Collectors.toList());
    }

    @Override
    public void changeId(ObserveServicesProvider servicesProvider, ReferentialService service) {
        for (Class<? extends ReferentialDto> type : data()) {
            String variableName = getVariableName(type).replace("referential.common", "referential.common.common");
            String id = getVariable(variableName + ".delete");
            if (id == null) {
                id = getVariable(variableName);
            }
            log.info(String.format("test duplicate for: %s", id));
            Assert.assertTrue(service.exists(type, id));
            String newId = id + "-Fake-" + System.nanoTime();
            service.changeId(type, id, newId);
            Assert.assertFalse(service.exists(type, id));
            Assert.assertTrue(service.exists(type, newId));
        }

    }

    @Override
    public void delete(ObserveServicesProvider servicesProvider, ReferentialService service) {
        String id = getProperty("delete.id");
        Assert.assertTrue(service.exists(ProgramDto.class, id));
        service.delete(ProgramDto.class, id);
        Assert.assertFalse(service.exists(ProgramDto.class, id));
    }

    @Override
    public void exists(ObserveServicesProvider servicesProvider, ReferentialService service) {
        boolean actual = service.exists(ProgramDto.class, getProperty("exists.id"));
        Assert.assertTrue(actual);
    }

    @Override
    public void getReferenceSet(ObserveServicesProvider servicesProvider, ReferentialService service) {
        ReferentialDtoReferenceSet<fr.ird.observe.dto.referential.ps.common.ProgramReference> actual = service.getReferenceSet(fr.ird.observe.dto.referential.ps.common.ProgramReference.class, null);
        ReferentialDtoReferenceSet<fr.ird.observe.dto.referential.ll.common.ProgramReference> actual2 = service.getReferenceSet(fr.ird.observe.dto.referential.ll.common.ProgramReference.class, null);
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual2);
        Assert.assertEquals(getIntegerProperty("getReferenceSet.count"), actual.size() + actual2.size());
    }

    @Override
    public void getReferentialIds(ObserveServicesProvider servicesProvider, ReferentialService service) {
        ReferentialIds actual = service.getReferentialIds();
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("getReferentialIds.count"), actual.keySet().size());
        Assert.assertEquals(REFERENTIAL_COUNT, actual.size());
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void getReferentialReferenceSets(ObserveServicesProvider servicesProvider, ReferentialService service) {
        for (Class<? extends ReferentialDto> type : data()) {

            ReferenceSetsRequest request = new ReferenceSetsRequest<>();
            request.setLastUpdateDates(Collections.emptyMap());
            request.setDtoType(type);
            Set<ReferentialDtoReferenceSet<?>> actual = service.getReferentialReferenceSets(request);
            Assert.assertNotNull(actual);
            Assert.assertFalse(actual.isEmpty());

        }
    }

    @Override
    public void insertMissingReferential(ObserveServicesProvider servicesProvider, ReferentialService service) {
        // FIXME:Test Remove super method invocation and implements fixture
        // actual = service.insertMissingReferential(getProperty("insertMissingReferential.id"));
        // Assert.assertNotNull(actual);
        super.insertMissingReferential(servicesProvider, service);
    }

    @Override
    public void loadDto(ObserveServicesProvider servicesProvider, ReferentialService service) {
        for (Class<? extends ReferentialDto> type : data()) {
            String variableName = getVariableName(type).replace("referential.common", "referential.common.common");
            String id = getVariable(variableName);
            log.info(String.format("test load form for: %s", id));
            ReferentialDto actual = service.loadDto(type, id);
            Assert.assertNotNull(actual);
            Assert.assertEquals(actual.getId(), id);
        }
    }

    @Override
    public void loadDtoList(ObserveServicesProvider servicesProvider, ReferentialService service) {
        for (Class<? extends ReferentialDto> type : data()) {
            String variableName = getVariableName(type).replace("referential.common", "referential.common.common");
            String id = getVariable(variableName);
            log.info(String.format("test load form for: %s", id));
            Set<? extends ReferentialDto> actual = service.loadDtoList(type);
            Assert.assertNotNull(actual);
            Assert.assertFalse(actual.isEmpty());
        }
    }

    @Override
    public void loadForm(ObserveServicesProvider servicesProvider, ReferentialService service) {
        for (Class<? extends ReferentialDto> type : data()) {
            String variableName = getVariableName(type).replace("referential.common", "referential.common.common");
            String id = getVariable(variableName);
            log.info(String.format("test load form for: %s", id));
            Form<? extends ReferentialDto> actual = service.loadForm(type, id);
            Assert.assertNotNull(actual);
            Assert.assertNotNull(actual.getObject());
            Assert.assertEquals(actual.getObject().getId(), id);
        }
    }

    @Override
    public void loadIds(ObserveServicesProvider servicesProvider, ReferentialService service) {
        Set<String> actual = service.loadIds(ProgramDto.class);
        Assert.assertNotNull(actual);
        Assert.assertEquals(getIntegerProperty("loadIds.count"), actual.size());
    }

    @Override
    public void preCreate(ObserveServicesProvider servicesProvider, ReferentialService service) {
        Form<fr.ird.observe.dto.referential.ps.common.ProgramDto> actual = service.preCreate(fr.ird.observe.dto.referential.ps.common.ProgramDto.class);
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getObject());
    }

    @Override
    public void replaceReference(ObserveServicesProvider servicesProvider, ReferentialService service) {
        service.replaceReference(fr.ird.observe.dto.referential.ps.common.ProgramDto.class, getProperty("replaceReference.idToReplace"), getProperty("replaceReference.replaceId"));
    }

    @Override
    public void replaceReferenceInData(ObserveServicesProvider servicesProvider, ReferentialService service) {
        service.replaceReference(fr.ird.observe.dto.referential.ps.common.ProgramDto.class, getProperty("replaceReferenceInData.idToReplace"), getProperty("replaceReferenceInData.replaceId"));
    }

    @Override
    public void save(ObserveServicesProvider servicesProvider, ReferentialService service) {
        fr.ird.observe.dto.referential.ps.common.ProgramDto actual = service.loadDto(fr.ird.observe.dto.referential.ps.common.ProgramDto.class, getProperty("loadDto.id"));
        Assert.assertNotNull(actual);
        try {
            SaveResultDto actualResult = service.save(actual);
            Assert.assertNotNull(actualResult);
        } catch (ConcurrentModificationException e) {
            throw new RuntimeException(e);
        }
    }

}
