/*
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.service.report.ps;


import com.google.auto.service.AutoService;
import fr.ird.observe.services.service.ReportFixture;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.definition.ReportRequestDefinition;
import fr.ird.observe.report.definition.RequestLayout;

import java.util.Iterator;

/**
 * Pour tester le report {@code dailySetAndCapture}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.9
 */
@AutoService(ReportFixture.class)
public class PsObservationDailySetAndCatchReportFixture extends ReportFixture {

    @Override
    public void assertSyntax(Report report) {
        super.assertSyntax(report);
        Iterator<ReportRequestDefinition> requests = getRequestIterator(report);
        assertReportRequestDimension(requests, RequestLayout.row, 0, 0);
    }
}
