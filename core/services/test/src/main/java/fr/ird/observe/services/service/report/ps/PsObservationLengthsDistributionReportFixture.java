package fr.ird.observe.services.service.report.ps;

/*-
 * #%L
 * ObServe Core :: Services :: Test
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.services.service.ReportFixture;
import fr.ird.observe.services.service.ReportService;
import fr.ird.observe.report.Report;

/**
 * Created on 13/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.17
 */
@AutoService(ReportFixture.class)
public class PsObservationLengthsDistributionReportFixture extends ReportFixture {

    public Report populateVariables(ReportService service, Report report) {
        Report result = super.populateVariables(service, report);
        result = service.populateVariables(result, getTripIds());
        setVariables2(result);
        return result;
    }

    @Override
    protected void setVariables(Report report) {
        setVariableValue(report, "species", "fr.ird.referential.common.Species#1239832684537#0.2397229787936519");
    }

    protected void setVariables2(Report report) {
        setVariableValue(report, "sizeMeasureType", "fr.ird.referential.common.SizeMeasureType#1433499465700#0.0902433863375336");
    }
}

