
# Introduction

* Ce document décrit notre réponse technique suite à votre demande de développement d'un pilote *ObServe* dans *Akoado*.
* Rédigé le 16/03/2023 à Saint Cybardeaux (16170)
* Auteur *Tony Chemit*, société *Ultreia.io*

\newpage

# Contexte

Le logiciel *AKaDo* permet à partir d'une base *AVDTH*, d'effectuer des contrôles et de produire ensuite des résultats
sous forme de fichier tableur de type *Excel*.

Le but de ce développement est de pouvoir utiliser une base *ObServe* pour effectuer des contrôles identiques (à noter 
cependant qu'il y en aura moins, puisque certains contrôles sur *AVDTH* sont des contrôles d'intégrité de base,
ce qui ne sera nécessaire pour *ObServe* vu que cette base est fortement contrainte) et de produire des résultats sous 
le même format adapté au modèle d'*ObServe*.

# Travail préliminaire

Avant de s'attaquer au développement de ce nouveau pilote *ObServe*, nous préconisons d'amélioration de l'utilisation
de *Maven* (qui est l'outil permettant de construite le project) et de *Git* (qui est l'outil pour conserver les sources du développement).

En effet, le projet en l'état ne respecte pas les bonnes pratiques de *Maven* et il nous semble important d'améliorer cela.

Cela consiste en :

  * pour *Git* : de partir sur une utilisation de deux branches (**develop** et **stable**) pour distinguer le code en cours de développement, et du code stable.
  * pour *Maven* :
    * de ne jamais être sur des versions stables des modules, mais toujours sur des *snapshot*, les version stables correspondants aux versions livrées
    * de mettre en place les bonnes pratiques qui permettent de produire automatiquement les nouvelles versions 
    * et **surtout** de les déployer sur *Maven Central*

Nous préconisons dans ce cadre d'introduire une nouveau projet *GitHub* pour avoir un **pom** parent des modules 
*AKaDo*, le module **ird.common** ne sera pas inclus ici.

Nous préconisons aussi de n'avoir qu'un seul projet dans *GitHub*, ce qui peut simplifier de manière significative 
le maintien du projet (cela remplacerait le projet **pom** décrit au-dessus).

Un dernier point nous semble important à améliorer, il s'agit des logs applicatifs, car actuellement les logs sont 
inexistants (nous n'en n'avons aucun lors du lancement de l'application) et cela est un frein très important à la bonne 
réussite de ce projet.
 
# Travail à effectuer pour le développement du pilote *ObServe*.

Décrivons dans un premier temps les différentes taches préssenties pour ce nouveau développement :

1. définir un nouveau module *Maven* **driver-common** pour extraire du module existant **driver-avdth** certains concepts réutilisables pour *ObServe*
2. définir un nouveau module *Maven* **driver-observe** qui est le pendant du module existant **driver-avdth**
3. renommer le module *Maven* **akado-avdth-common** en **akado-common** et d'y inclure les configurations nécessaires pour se conntecter à une base *ObServe*, on pourrait aussi supprimer ce module et l'intégrer dans le module *Maven* **akaodo-core**
4. définir un nouveau module *Maven* **akado-driver-observe** qui est le pendant du module existant **akado-driver-avdth**
5. renommer le module *Maven* **akado-avdth-ui** en **akado-ui** et d'y inclure une nouvelle vue pour les traitements d'une base *ObServe*
6. définir comment se connecter à une base *ObServe*

## Nouveau module *driver-common*

Il s'agit d'extraire depuis le module existant **driver-avdth**, quelques classes qui peuvent servir aussi pour le 
nouveau pilote *Observe*, à savoir :

 * le paquetage **fr.ird.driver.avdth.common**, à renommer en **fr.ird.driver.common**
   * la classe **AvdthUtils** à renommer en **DriverUtils**
   * la classe **AvdthDriverException** à renommer en **DriverException**

## Nouveau module *driver-observe*

Ce module comprend :

1. la définition des entités *ObServe*
2. la définition de la couche *DAO* qui permet de charger ces entités à partir d'une base *ObServe*
3. la définition de la couche *service* qui permet d'effectuer les opérations de haut niveau depuis les interfaces graphiques

À noter que pour la couche *DAO* et *service*, nous ne proposerons que de la lecture, car après analyse de l'existant, il
semble clair qu'aucune modification de la base *ObServe* ne soit nécéssaire pour ce projet.

Nous proposons pour les entités et DAO de les générer à partir du modèle décrit dans un module du projet *ObServe*, 
cela nous garantit que si le modèle d'*ObServe* change alors ces objets seront alors bien regénérés sans intervention humaine.

Il ne semble pas nécessaire de générer tout le modèle *ObServe* mais juste les entités utilisées dans *AKaDo*, à savoir :

 * le module **common** (qui ne contient que des référentiels)
 * le module **ps** (qui contient le modèle Senne)

Une autre solution serait d'écrire à la main tout cela, mais non évolutive, elle pourrait peut-être être plus bénéfique 
si la durée de vie du projet AKaDO en l'état n'est pas très longue. 

Nous préconisons de ne pas utiliser de technologie de type *ORM* pour gérer la persistence du type *Hibernate*, car ici 
nous n'effectuons que des lectures en base et ces technologies ne semblent pas nécessaire dans ce cadre. 

Nous ferons comme pour l'existant et écriront les requêtes sql de récupération des entités.

## Nouveau module *akado-driver-observe*

Ce module comprend :

1. La définition des contrôles à appliquer sur une base *ObServe*, il s'agira exactement des mêmes contrôles que ceux déjà implantés pour *AVDTH*, sauf pour les contrôles d'intégrité
2. La définition des résultats à produire, il s'agira exactement de la même structure de résultats, adaptée au modèle *ObServe*

Nous avons détecté 39 contrôles, dont seulement 3 sur des contrôles d'intégrité.

Nous avons détecté 7 résultats, tous à reprendre.

## Renommage du module *akado-avdth-common* en *akado-common* (ou intégration directement dans le module *akado-core*)

Le module **akado-avdth-common** contient des classes pour la configuration d'*AKaDo* et est très peu lié à *AVDTH*.

Il s'agit au final :

1. de rajouter dans la classe **AAProperties** de ce qu'il faut pour accéder à une base *ObServe*.
2. de renommer le paquetage **fr.ird.akado.avdth.common** en **fr.ird.akado.common**.

Nous serions vraiment très favorable à intégrer ce module dans le module **akado-core**, il ne nous semble pas nécessaire
d'avoir cette séparation dans deux modules.

## Renommage du module *akado-avdth-ui* en *akado-ui* et intégration du pilote *ObServe* dedans

Deux solutions sont possibles :

1. écrire un nouveau module *akado-observe-ui*
2. renommer le module *akado-avdth-ui* en *akado-ui* et y intégrer le pilote *ObServe*

La première solution serait plus rapide à mettre en place car nous n'aurions pas à effectuer de modification de l'existant 
et de ne pas à avoir les deux pilotes au même endroit.

La seconde solution aurait l'avantage de n'avoir qu'un seul exécutable, mais nous demandera un travail supplémentaire pour
effectuer les différents *switch* si on utilise une base *AVDTH* ou *ObServe*. Ce travail n'est pas non plus 
insurmontable vu la simplicité des interfaces graphiques.

Nous préconisons la seconde solution, cela semble plus facile à utiliser :

 * dans le menu *Fichier*, renommer *Ouvrir* en *Ouvrir une base AVDTH* 
 * dans le menu *Fichier*, ajouter un nouvel item *Ouvrir une base ObServe*
 * renommer le menu *contrôle AVDTH* en *contrôle AKaDo*

Si une base *AVDTH* est ouverte, alors on aura l'interface graphique actuelle, si une base *ObServe* est ouverte, 
l'interface sera légèrement différente (cela concerne juste la barre d'information en bas), le fonctionnement reste 
identique à celui du pilote sur tous les autres points.

## Comment se connecter à une base *ObServe* ?

*ObServe* est une application qui fonctionne selon trois modes :
 * un mode local où une base **H2** est utilisée sur le poste utilisateur (qui est initiée par l'ouverture d'une sauvegarde au format *sql.gz*)
 * un mode distant où une base *PostgreSQL* est utillisée sur le poste utiliseur (ou ailleurs)
 * un mode serveur où un serveur web est utilisé (qui lui utilise une base *PostgreSQL*)

Il ne semble pas réaliste (tout du moins pour ce contrat) de permettre d'utiliser ces trois modes de connexion.

Nous proposons alors de se baser sur l'utilisation des fichiers de sauvegarde :

 * l'utilisateur choisit une sauvegarde (tout comme il choisit une base *AVDTH*)
 * de créer ensuite à la volée la base *H2* issue de cette sauvegarde
 * d'utiliser ensuite cette base pour effectuer les traitements

Une autre solution serait d'offrir une interface graphique pour renseigner une configuration *Sql*, ce qui permettrait alors :

 * d'utiliser une base locale (*H2*) en mode serveur
 * d'utiliser une base *PostgreSQL*

Cette solution est plus élégeante, mais nécessite plus de développement d'interface graphique...

\newpage

# Conditions de réalisation

## Tarif 2022

Les tarifs sont les suivants :

* Prix journalier HT (sans remise licence Open-source) 600 €
* Remise Licence Open-source -10%
* Prix journalier HT net 540 €

## Délais et conditions de réalisation

**Nous sommes disponibles pour démarrer ce développement dès le début avril 2023.**

Celui-ci pourrait être terminé mi-mai 2023, si nous démmarons dès que possible et que vous nous obtenons les réponses 
aux questions thématiques  (il y en aura très certainement sur la partie contrôle et résultat).

# Chiffrage

**Tous les prix sont affichés HT.**

| Tâche                                                 | Temps (en J) |
|:------------------------------------------------------|-------------:|
| Travail préliminaire (Maven)                          |            2 |
| Travail préliminaire (Git)                            |            1 |
| Travail préliminaire (Log)                            |            1 |
| Nouveau module driver-common                          |            1 |
| Nouveau module driver-observe                         |            5 |
| Nouveau module akado-driver-observe (partie contrôle) |           20 |
| Nouveau module akado-driver-observe (partie résultat) |            5 |
| Renommage du module akado-avdth-common                |            1 |
| Intégration dans le module akado-ui                   |            5 |
| Divers - Gestion projet / recette                     |            5 |
| Total                                                 |           46 |

Le prix total est de **24 840 €**.
