# Introduction

* Ce document décrit un chiffrage suite à la demande de développements complémentaires par l'AZTI et l'IRD.
* Rédigé le 06/07/2023 à Saint Cybardeaux (16170)
* Auteur *Tony Chemit*, société *Ultreia.io*

\newpage

# Détail des développements demandés

## (2707) Faire évoluer l'UI équipement du bateau pour autoriser la gestion de listes de référence

* ajout d'un nouveau type **date** (1J)
* ajout du nouveau type **liste de choix** (1.5J)
* contraindre les caractéristiques d'équipement (common.gearcharacteristic) autorisées pour un équipement (common.gear) donné (3J)
* remplir les référentiels correctement via migration (3J)

## (2708) Ajout de deux nouveaux champs et listes déroulantes sur le formulaire bonnes pratiques de remise à l'eau

* ajout du nouveau référentiel Zone de manipulation (0.5J)
* ajout du champs Numéro de salabarde (0.5J)

## (2215) Améliorer le rapport de validation

* ajout méta-données et configuration de la validation (0.5J)
* catégoriser des nouvelles informations permettant d'identifier les objets (2J)

## (2449) Nouveaux flags allowSet et fpaZoneMode sur les types d'activités bateau

* ajout du flag **allowSet** et utilisation à tous les endroits propices (1J)
* ajout du flag **allowFpaZoneChange** (1J)

## (2497) Filtrage des devenirs pour les données observation et logbooks

* ajout des deux nouveaux flags **observation** et **logbook** (PS et LL) (0.5J)
* mise en place des valeurs associées via migration et utilisation sur les formulaires concernés (0.5J)

## (2498) Filtrage des types d'activité bateau pour les données observation et logbooks

* ajout du nouveau flag (PS et LL) (1J)
* remplissage du référentiel et utilisation sur les deux formulaires (1J)

## (2706) Améliorer l'identification des enregistrements dans l'UI de validation batch

* ajouter les clefs métiers qui remontent jusqu'à la marée (2J)

La modification du rapport est traité dans un autre ticket.

## (2725) Contrôler la syntaxe des id balises par une expression régulière propre à chaque modèle de balise

* ajout du nouveau champs **regex** (0.5J)
* mise à jour du référentiel via migration (1.5J)
* mise en place de la validation via ce champs (1J)

## (2729) En PS / logbook / Activité, voir comment mieux gérer l'ajout d'une activité dont l'horaire précéde celui de la dernière activité saisie (souci lié au numéro d'activité auto incrémenté)

* ajout du champs **activitiesAcquisitionMode** au niveau de la marée (0.5J)
* positionnement de ce nouveau champs via migration embarqué et AVDTH (0.5J)
* mise en place du mode horaire (0.5J)
* mise en place du mode indexé (0.5J)

## (2740) En PS / logbook, ajouter 2 champs previousFpaZone et nextFpaZone

* ajout des deux nouveaux champs *previousFpaZone* et *nextFpaZone* (0.25J)
* renommage du champs *fpaZone* en *currentFpaZone* (0.25J)
* pilotage de ces champs via le drapeau sur vesselActivity (0.5J)

## (2750) Effectuer un différentiel sur l'UI de synchro de marées

* calcul du différentiel entre les deux sources de données (1J)
* adaptation de l'arbre de navigation pour n'afficher que les marées différentes (2J)

# Chiffrage global

**Tous les prix sont affichés HT.** et le prix journalier est de **540€**.

| Tâche                                                                                                                                                                                           | Temps (en J) |
|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------:|
| (2707) Faire évoluer l'UI équipement du bateau pour autoriser la gestion de listes de référence                                                                                                 |          8.5 |
| (2708) Ajout de deux nouveaux champs et listes déroulantes sur le formulaire bonnes pratiques de remise à l'eau                                                                                 |            1 |
| (2215) Améliorer le rapport de validation                                                                                                                                                       |          2.5 |
| (2449) Nouveaux flags allowSet et fpaZoneMode sur les types d'activités bateau                                                                                                                  |            2 |
| (2497) Filtrage des devenirs pour les données observation et logbooks                                                                                                                           |            1 |
| (2498) Filtrage des types d'activité bateau pour les données observation et logbooks                                                                                                            |            2 |
| (2706) Améliorer l'identification des enregistrements dans l'UI de validation batch                                                                                                             |            2 |
| (2725) Contrôler la syntaxe des id balises par une expression régulière propre à chaque modèle de balise                                                                                        |            3 |
| (2729) En PS / logbook / Activité, voir comment mieux gérer l'ajout d'une activité dont l'horaire précéde celui de la dernière activité saisie (souci lié au numéro d'activité auto incrémenté) |            2 |
| (2740) En PS / logbook, ajouter 2 champs previousFpaZone et nextFpaZone                                                                                                                         |            1 |
| (2750) Effectuer un différentiel sur l'UI de synchro de marées                                                                                                                                  |            3 |
| Gestion de projet                                                                                                                                                                               |            5 |
| Total                                                                                                                                                                                           |           33 |
| *Remises offertes*                                                                                                                                                                              |              |
| Gestion de projet                                                                                                                                                                               |         -2.5 |
| (2750) Effectuer un différentiel sur l'UI de synchro de marées                                                                                                                                  |           -3 |
| Total                                                                                                                                                                                           |         27.5 |

* Prix total (sans remise) : **17 280€**
* Prix total (avec remise) : **14 850€**
