---
title:  Chiffrage IRD-2024 (v9.4)
author: Tony Chemit, société Ultreia.io
geometry: margin=1.2cm
---

# Introduction

* Ce document décrit un chiffrage suite à la demande de développements complémentaires et de documentation sur le logiciel *ObServe* (v9.4) par l'IRD.
* Rédigé le 22/05/2024 à Saint Cybardeaux (16170)

\newpage

# Détail des développements demandés

## [#2826](https://gitlab.com/ultreiaio/ird-observe/-/issues/2826) Permettre de renommer et de supprimer une présélection FOB

Nous proposons de pouvoir renommer, supprimer et réordonner, mais pas d'éditer le contenu (car cela requière trop de développement pour la partie matériaux et bouées et des formulaires existent déjà pour cela).

Cette fonctionnalité sera disponible depuis le menu Configuration > Gérer les présélections FOB.

Les présélections peuvent se retrouver à trois niveaux (commun, observation ou logbook), voir si il est pertinent de ré-impacter cela dans la nouvelle interface graphique.

**2J**

On peut ajouter une option de développement pour afficher le détail de chaque présélection (uniquement si une base est connectée car on doit aller chercher en base les label des différents référentiels utilisés).

**1J**

## [#2834](https://gitlab.com/ultreiaio/ird-observe/-/issues/2834) Permettre à l’utilisateur de sauvegarder ses choix quant au mode de classement des listes déroulantes

### Rappel

En préambule, rappelons qu'il existe déjà un mécanisme qui permet de modifier la configuration des listes déroulantes des formulaires dans l'application.

Il s'agit d'un fichier de propriétés nommé **observe-swing-preferences.properties** embarqué dans l'application, qui est recopié dans le répertoire des ressources de l'utilisateur.

Seul un utilisateur expert peut actuellement le modifier (connaissance requise du code des interfaces graphiques JAXX/Java).

### Amélioration du fichier de configuration

Nous proposons dans un premier temps de consolider ce fichier pour que toutes les valeurs possibles y soient (comme 
nous l'avons déjà fait pour les fichiers de configuration) et d'effectuer une sauvegarde qui conserve les valeurs non modifiées.

Le format du fichier va peut-être évoluer pour répondre à d'autres besoins préssentis sur les configurations de ces listes déroulantes.  

**2J**

### Adaptation de l'interface graphique

Dans un formulaire pour une liste déroulante dont la configuration est modifiée par l'utilisateur, il faudra lui indiquer 

 - avec un nouvel icône (à droite de celui de modification de la configuration) 
 - ou bien en changeant la couleur du contour de l'icône de modification de configuration (peut-être plus léger)

La popup de configuration d'une liste déroulante ainsi que son menu d'accès seront revus pour pouvoir prendre en compte les nouvelles actions :

 - **Mettre en préférence**
 - **Retirer des préférences**

**3J**

## [#2347](https://gitlab.com/ultreiaio/ird-observe/-/issues/2347) Permettre à l’utilisateur de pré régler ses propres valeurs par défaut de listes déroulantes

### Enrichissement du fichier de configuration

En reprenant le principe de la fonctionnalité précédente, on peut enrichir le fichier de configuration des listes déroulantes et associer un identifiant technique (*topiaId*) à chaque liste possible.

**1J**

### Adaptation de l'interface graphique

Les actions suivantes seront rajoutées au menu de configuration:

 - **Appliquer cette valeur par défaut**
 - **Supprimer la valeur par défaut**

À noter que ce nouveau mécanisme ne sera utilisable que pour des données en mode création. 

**2J**

### Utilisation de ces valeurs par défaut

Se pose la question de l'utilisation de ces valeurs par défaut. Il peut sembler pertinent d'envoyer ces valeurs par 
défaut au service pour que celui-ci nous retourne l'objet bien initialisé, plutôt que de le récupéré «vide» et que le formulaire remplisse ensuite les valeurs par défaut.

Il faudra aussi se poser la question de déporter toutes les valeurs par défaut qui sont fournis (en dure dans le code) par les services, elles pourrait ainsi devenir des valeurs par défaut.

Afin de palier au fait que certains référentiels utilisés dans cette nouvelle configuration puissent ne pas exister 
dans une base, à chaque ouverture d'une source de données, on reprendra la configuration pour en supprimer les valeurs
manquantes, sans pour autant modifier la configuration.

**3J**

## [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) Sur tous les widgets position, présélectionner le quadrant de la saisie précédente

### Mise en place de la nouvelle donnée à récupérer 
Deux choix techniques sont possibles :

 - Cette règle de gestion devrait-elle être réalisée au niveau service car faire cela au niveau de l'application cela ne semble pas pertinent (quid de l'API publique), sauf qu'à ce niveau là nous ne possédons par l'information quadrant (il est calculé depuis les coordonnées).
 - Faire cela au niveau de l'application, mais cela sous-entend aussi d'avoir cette information depuis la couche service...

Il semble alors indispensable de calculer cette information et de la rajouter dans les DTO du service (champ **defaultQuadrant** ou **xxxDefaultQuadrant**) qui ne sera utilisé dans le formulaire que si la coordonnée associée n'est pas renseignée.

**3J**

### Utilisation de cette nouvelle donnée dans le formulaire

À noter que ce nouveau mécanisme ne sera utilisable que pour des données en mode création.

**2J**

## [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) LL observations/logbook : mise à jour auto de la date de fin de marée en fonction des dates activités (comme en PS)

Le mécanisme proposé se fera sur toutes les dates possibles lorsqu'elles existent (activité et calée).

On renforcera donc aussi pour le module Seine.

Cela doit être réalisé au niveau service.

**1J**

On propose aussi en option d'en faire règle de consolidation.

**2J**

## [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) LL observations/logbook : filage: mettre par défaut date de dernière activité (ou dernier virage ?) + 1

Uniquement en mode création et remplit depuis le service.

**0.5J**

## [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) LL observations/logbook : virage : mettre date de virage à J+1 par rapport à celle du filage

Uniquement en mode création et remplit depuis le service.

**0.5J**

## [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) LL observations/composition détaillée : on nous signale une forte latence dans la V9 lors de la sélection de sections/paniers, inexistante en V7 (sûrement due à la normalisation du modèle). Possibilité de retrouver une meilleure réactivité ? (si techniquement possible)

Il est vrai que la validation à ce niveau n'est pas très performante... Elle valide tous les objects alors que l'on ne devrait valider que ceux affichés dans les tableaux...

Nous proposons dans ce cadre de rajouter le ticket suivant qui remplace la validation utilisant la technologie **XWorks** qui est la source de ces lenteurs.

**2J**

## [#2594](https://gitlab.com/ultreiaio/ird-observe/-/issues/2594) Suppression de la technologie XWorks (utilisé pour la validation)

La technologie de remplacement est déjà disponible et la migration est effective. 

La solution de remplaçement a été développée par nos soins et répond parfaitement à nos besoin et est performante.

**3J**

## [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) LL observations : amélioration du focus sur form captures (à voir plus précisément ensemble), et ajout de champs (actuellement existants mais masqués) dans le tableau récapitulatif

**1J**

## [#2669](https://gitlab.com/ultreiaio/ird-observe/-/issues/2669) PS logbook : Automatiser la calcul des sample.minus10Weight et sample.plus10Weight par sommation des lots du plan de cuves

Il faut ici reprendre le principe déjà en place pour les données calculées, à savoir :

 - ajouter un champs booléan **minus10WeightComputed** et **plus10WeightComputed** pour consigner le fait que la données a été observée ou calculée
 - utiliser le widget (icône œil ou roue) pour pouvoir supprimer le drapeau **computed** dans le formulaire

On propose aussi de rajouter une règle de validation pour n'autoriser au niveau des échantillons que les identifiants de cuves disponibles sur le plan de cuve (et ceci uniquement si le plan de cuve est saisi).

**3J**

## [#2799](https://gitlab.com/ultreiaio/ird-observe/-/issues/2799) PS logbook : Automatiser les associations Échantillon  Activités 

Cette évolution est incluse dans une autre que nous vous proposons de réaliser à la place, à savoir [#2734](https://gitlab.com/ultreiaio/ird-observe/-/issues/2734) La liste des activités associées à un échantillon pourrait être déduite du plan de cuves, et inversément.

 - Ajout du bouton sur le formulaire échantillons et récupération des activités depuis le plan de cuve (l'action ne sera accessible que si le plan de cuve existe et que la cuve décrite dans l'échantillonnage est bien présente) 1J 
 - Ajout du bouton sur le formulaire plan de cuve et récupération des activités depuis l'échantillonnage correspondant (l'action ne sera accessible que si un échantillonnage existe sur la cuve en question) 1J 
 - Mise en place via la consolidation 2J

Il faudra bien veiller à ne rien modifier si le plan de cuve n'existe pas ou bien que les échantillons n'existent pas afin de ne pas altérer les données existantes.

On devra aussi en cascade calculer au niveau des échantillons les pondérations des cuves-calées (**1J**).

## [#2726](https://gitlab.com/ultreiaio/ird-observe/-/issues/2726) Amélioration de la gestion des types date

Actuellement tous les types de dates dans le code *Java* utilise un simple **java.util.Date**, ce qui ne permet pas de connaître sa granularité temporelle (date, heure ou horodatage).

On se propose alors de les remplacer par des types java plus précis et qui ont été introduits en *Java 8* (sans time zone).

Pour ce qui est des horodatages techniques (topiaCreateDate et lastUpdateDate), on passera sur un type horodatage avec tome zone.

**5J**

## [#2875](https://gitlab.com/ultreiaio/ird-observe/-/issues/2875) Dates activités toujours affichées sur carte LL 

Ajout des dates sur toutes les activités, ajout de nouvelles options de configuration (mapLlStyleLogbookTextColor et mapLlStyleLogbookTextSize) comme fait pour le module Ps.

Ici on ne travaille que sur la partie Logbook.

**2J**

## [#2603](https://gitlab.com/ultreiaio/ird-observe/-/issues/2603) Améliorer la reconnexion à un serveur distant utilisé comme connexion d’un assistant 

Actuellement, seule la source de données principale propose une reconnexion, il faut étendre cela à toutes les sources de données ouvertes via les assistants.

**3J**

## [#2818](https://gitlab.com/ultreiaio/ird-observe/-/issues/2818) Ajouter quelques champs de référence dans common.Vessel

 - Ajout du référentiel EngineMake **0.5J**
 - Ajout du référentiel HullMaterial **0.5J**
 - Ajout des deux champs engineMake et hullMaterial dans le référentiel Vessel **0.5J**
 - Import via la migration des deux nouveaux référentiels (via le fichier Turbobat) **1J**
 - Ajout d'un champs **GT** dans le référentiel Vessel **0.5J**
 - 
## [#2816](https://gitlab.com/ultreiaio/ird-observe/-/issues/2816) Fonction de changement de programme en masse

Cela est tout à fait réalisable; Nous vous proposons de procéder de la sorte :

 - l'utilisateur sélectionne la liste de ces marées via le formulaire de liste de marées
 - une nouvelle action **Modification en masse** devient accessible, elle ouvre une une boite de dialogue composée de deux composants :
   - le premier pour choisir quelle propriété modifier via une liste déroulante (par défaut elle prendre la modalité sélectionnée dans la configuration de l'arbre de navigation)
   - le second pour éditer la valeur à utiliser, selon le type de la modalité sélectionnée (on reprend ici le principe utilisé dans l'écran de recherche de marées)

 **Note :** Il faudra penser à rester cohérent avec les status de collecte, par exemple il ne sera pas possible de modifier le capitaine si au moins une marée n'a pas de collecte des observations.

 - réalisation de la boite de dialogue **2J**
 - enregistrement via un nouveau service dédié **1J**
 - mise à jour de l'arbre de navigation **2J**

Dans cette première version, on se limitera aux modalités qualitatives.

## [#2846](https://gitlab.com/ultreiaio/ird-observe/-/issues/2846) Corriger un effet de bord indésirable lors de la désactivation ou la suppression d’une référence et la réattribution de ses données liées à une autre référence 

### Rappel des trois actions possibles
Pour rappel, le comportement d'une suppression est le suivant :

1. on calcule les utilisations du référentiel
2. si pas de résultat, on peut supprimer directement
3. sinon on affiche les utilisations et on demande un remplacement
4. si l'utilisateur renseigne le remplaçant et valide on procède 
   1. remplacement du référentiel partout
   2. suppression du référentiel

Le comportement d'une désactivation au moment de la sauvegarde :

1. on calcule les utilisations du référentiel
2. si pas de résultat, on peut désactiver directement
3. sinon on affiche les utilisations et on demande un remplacement
4. si l'utilisateur renseigne le remplaçant et valide on procède
   1. remplacement du référentiel partout
   2. désactivation du référentiel

Enfin le comportement d'un remplacement :

1. on calcule les utilisations du référentiel
2. si pas de résultat, il n'y a rien à faire
3. sinon on affiche les utilisations et on demande un remplacement
4. si l'utilisateur renseigne le remplaçant et valide on procède au remplacement du référentiel partout

### Nouveau besoin

Dans certains cas, on voit bien que le remplacement n'est pas pertinent et qu'il faudrait en fait effectuer 
les remplacements des référentiels impactés.

Ce besoin est induit par la logique métier et non par la structuration des données; il nous faut donc trouver un moyen 
élégant de le décrire.

Nous vous proposons de le décrire via une nouvelle tag-value sur les classes du modèle de type booléen :

 - la valeur **false** (celle par défaut) conservera le comportement actuel de remplacement
 - la valeur **true** permettra de mettre en place un remplacement en profondeur, i.e de demander le remplacement des 
   référentiels visés dans les autres données du modèle

Dans l'exemple des espèces, on cherchera alors les occurrences des RTP et RTT (il n'y en a aucune puisque ces données 
ne sont pas utilisées dans aucune autre donnée); pour les catégories de poids, on proposera alors un remplacement par 
une autre catégorie de poids dans le sous-ensemble de celles utilisant la nouvelle espèce de remplacement. 
   
**0.5J**

Il faudra bien distinguer une désactivation, d'un remplacement et d'une suppression :

 * dans le cas d'une désactivation, on devra alors désactiver les référentiels ciblés
 * dans le cas d'une suppression, les supprimer
 * dans le cas d'une remplacement, probablement ne rien faire

### Réorganisation de l'interface graphique

Pour que cela soit compréhensible pour l'utilisateur, il faut que nous distinguons plusieurs zones selon ce qu'il y a a faire :

1. celle déjà existant ou un remplacement  est possible
2. une nouvelle zone qui indique les objets qui ne doivent pas être remplacés et qui seront supprimés
3. une troisième zone qui liste les utilisations qui doivent être elles-même remplacées

Une organisation en onglet est peut-être envisageable ?

**4.5J**

### Adaptation du code de réalisation des actions

On doit alors effectuer toutes les suppressions et remplacements de second niveau, puis ensuite le code actuel peut-être appliqué tel quel.

**1J**

### Cas des doublons lors de remplacement sur une liste d'association

Effectivement comme décrit dans le ticket, on pourrait avoir des erreurs lors de l'exécution de l'action puisque les 
couples sont des clefs primaires.

Pour y remédier, nous pouvons améliorer le code sql généré dans ce but pour tenir compte de ce cas.

**0.5J**

## [#1203](https://gitlab.com/ultreiaio/ird-observe/-/issues/1203) Tableaux de saisie (captures, échantillons, équipements...) - Classement en cliquant sur les en-têtes de colonnes

On rajoute de plus une colonne **index** au début pour bien voir l'ordre.

Lors de l'ajout ou suppression, il faudra maintenir cohérent cet ordre induit.

Se pose la question des actions de changement de position ?

Avant d'enregistrer, on réordonnera la liste via cet index.

**3J**

# Détail de la documentation demandée


## [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) Écriture de tutoriels pratiques

Avant de pouvoir proposer des tutoriels de modification du projet, il faut en écrire quelques uns pour avoir une bonne compréhension de l'architecture de celui-ci :

 - un tutoriel pour mettre en place l'environnement de développement
 - un tutoriel pour expliquer l'organisation du projet et de ses modules Maven
 - un tutoriel pour expliquer le modèle et tous ce que l'on peut faire dessus (description détaillée de chaque tag-value)
 - un tutoriel pour expliquer comment fonctionne les migrations
 - un tutoriel pour expliquer comment fonctionne les décorations
 - un tutoriel pour expliquer comment fonctionne la validation
 - un tutoriel pour expliquer comment fonctionne les services
 - un tutoriel pour expliquer comment fonctionne l'API publique
 - un tutoriel pour expliquer comment fonctionne le serveur
 - un tutoriel pour expliquer comment fonctionne les rapports
 - un tutoriel pour expliquer comment fonctionne l'application lourde
 - un tutoriel pour expliquer comment fonctionne les interfaces graphiques
 - un tutoriel pour expliquer comment fonctionne l'éditeur de source de données
 - un tutoriel pour expliquer comment fonctionne les actions longues

Une fois cela réalisé, on pourra penser à écrire des tutoriels de modification du projet.

Nous vous proposons de travailler **5J** sur le sujet pour commencer ce travail.

## [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) Mise à jour de la documentation des librairies

Dans un premier temps, il nous faut lister toutes les technologies utilisées dans le projet et que nous supportons, à savoir :

  - java4all.java-util
  - java4all.java-lang
  - java4all.java-bean
  - java4all.jaxx
  - java4all.eugene
  - java4all.http
  - java4all.validation

Un certain nombres d'entre-elles ont directement été intégrée dans le projet (dans le module **toolkit**) car elles 
n'étaient plus maintenues, à savoir 
  - topia (gestion de la persistence)
  - webMotion (serveur web)

Nous vous proposons de travailler **5J** sur le sujet pour commencer ce travail.

## [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) Ajout d'une documentation dans la base de données

Pour réaliser cela nous allons utiliser une nouvelle tag-value sur le modèle de persistence, on pourra la renseigner à 
différents niveaux :

 - sur un schéma
 - sur une table
 - sur la colonne d'une table

Lors de la génération d'une nouvelle base ou d'une migration, cette documentation sera insérée dans la base juste après la création de la base.

Cela s'applique sur les bases H2 et Postgres.

**4J**

### Intégration de cette documentation dans l'API publique

Cette nouvelle documentation pourra être intégrée au niveau de la documentation de chaque entité.

**2J**

### Production d'un rapport de cette documentation dans le site de l'application

On peut aussi proposer un rapport généré dans le site de l'application et qui reprend cette fois via une vue physique de
la documentation déjà réalisé et indique le pourcentage de réalisation sur le sujet.

**3J**

### Production d'une documentation du schéma de base

Mettre en place la génération de la documentation du schéma de la base via probablement la librairie **SchemaSpy**.

La documentation produite sera alors placée dans la documentation du projet.

**4J**

\newpage

# Chiffrage global

**Tous les prix sont affichés HT.** et le prix journalier est de **540€**.

| Tâche                                                                                                                                                                                                                                                                                                         | Jours |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------:|
| [#2826](https://gitlab.com/ultreiaio/ird-observe/-/issues/2826) Permettre de renommer et de supprimer une présélection FOB                                                                                                                                                                                    |     3 |
| [#2834](https://gitlab.com/ultreiaio/ird-observe/-/issues/2834) Permettre à l’utilisateur de sauvegarder ses choix quant au mode de classement des listes déroulantes                                                                                                                                         |     5 |
| [#2347](https://gitlab.com/ultreiaio/ird-observe/-/issues/2347) Permettre à l’utilisateur de pré régler ses propres valeurs par défaut de listes déroulantes                                                                                                                                                  |     6 |
| [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) Sur tous les widgets position, présélectionner le quadrant de la saisie précédente                                                                                                                                                                |     5 |
| [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) LL observations/logbook : mise à jour auto de la date de fin de marée en fonction des dates activités (comme en PS)                                                                                                                               |     3 |
| [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) LL observations/logbook : filage: mettre par défaut date de dernière activité (ou dernier virage ?) + 1                                                                                                                                           |   0.5 |
| [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) LL observations/logbook : virage : mettre date de virage à J+1 par rapport à celle du filage                                                                                                                                                      |   0.5 |
| [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) LL observations/composition détaillée : on nous signale une forte latence dans la V9 lors de la sélection de sections/paniers, inexistante en V7 (sûrement due à la normalisation du modèle). Possibilité de retrouver une meilleure réactivité ? |     2 |
| [#2594](https://gitlab.com/ultreiaio/ird-observe/-/issues/2594) Suppression de la technologie XWorks (utilisé pour la validation)                                                                                                                                                                             |     3 |
| [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) LL observations : amélioration du focus sur form captures (à voir plus précisément ensemble), et ajout de champs (actuellement existants mais masqués) dans le tableau récapitulatif                                                              |     1 |
| [#2669](https://gitlab.com/ultreiaio/ird-observe/-/issues/2669) PS logbook : Automatiser la calcul des sample.minus10Weight et sample.plus10Weight par sommation des lots du plan de cuves                                                                                                                    |     3 |
| [#2799](https://gitlab.com/ultreiaio/ird-observe/-/issues/2799) PS logbook : Automatiser les associations Échantillon  Activités                                                                                                                                                                              |     5 |
| [#2726](https://gitlab.com/ultreiaio/ird-observe/-/issues/2726) Amélioration de la gestion des types date                                                                                                                                                                                                     |     5 |
| [#2875](https://gitlab.com/ultreiaio/ird-observe/-/issues/2875) Dates activités toujours affichées sur carte LL                                                                                                                                                                                               |     2 |
| [#2603](https://gitlab.com/ultreiaio/ird-observe/-/issues/2603) Améliorer la reconnexion à un serveur distant utilisé comme connexion d’un assistant                                                                                                                                                          |     3 |
| [#2818](https://gitlab.com/ultreiaio/ird-observe/-/issues/2818) Ajouter quelques champs de référence dans common.Vessel                                                                                                                                                                                       |   1.5 |
| [#2816](https://gitlab.com/ultreiaio/ird-observe/-/issues/2816) Fonction de changement de programme en masse                                                                                                                                                                                                  |     6 |
| [#2846](https://gitlab.com/ultreiaio/ird-observe/-/issues/2846) Corriger un effet de bord indésirable lors de la désactivation ou la suppression d’une référence et la réattribution de ses données liées à une autre référence                                                                               |     6 |
| [#1203](https://gitlab.com/ultreiaio/ird-observe/-/issues/1203) Tableaux de saisie (captures, échantillons, équipements...) - Classement en cliquant sur les en-têtes de colonnes                                                                                                                             |     3 |
| [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) Écriture de tutoriels pratiques                                                                                                                                                                                                                   |     5 |
| [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) Mise à jour de la documentation des librairies                                                                                                                                                                                                    |     5 |
| [#TODO](https://gitlab.com/ultreiaio/ird-observe/-/issues/) Ajout d'une documentation dans la base de données                                                                                                                                                                                                 |    13 |
| Total                                                                                                                                                                                                                                                                                                         |  86.5 |

* Prix total : **46 170** (**43250€** à **500€/J**)
