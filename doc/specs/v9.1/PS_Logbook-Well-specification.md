
# Introduction

 * Ce document décrit notre réponse technique suite à votre demande de modification du plan de cuve modélisé en v9.
 * La version ciblée est la **v9.1**.
 * Rédigé le 08/09/2022 à Saint Cybardeaux (16170)
 * Auteur *Tony Chemit*, société *Ultreia.io* 

\newpage

# Rappel de l'existant

La modélisation des plans de cuve (module **PS-Logbook**) a été introduite en version 9, elle est assez simpliste et
ne convient pas pour le développement des échantillonages avancés.

Pour rappel, une seule table est définie **ps_logbook.WellPlan** dont voici la description :

+----------------------------+---------------+-----------+-------------------------------------------------------+
| Nom du champ               | Type          | Optionnel | Signification                                         |
+============================+===============+===========+=======================================================+
| **activity**               | *FK*          | Non       | l'activité de la cuve-calée                           |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **well**                   | texte         | Non       | le nom de la cuve                                     |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **species**                | *FK*          | Non       | l'espèce dans la cuve-calée                           |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **weightCategory**         | *FK*          | Non       | la catégorie de poids de l'espèce                     |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **wellSamplingConformity** | *FK*          | Non       | conformité de la cuve vis à vis de l'échantillonnage  |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **wellSamplingStatus**     | *FK*          | Non       | statut de l'échantillonnage sur la cuve               |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **trip**                   | *FK*          | Non       | la marée de rattachement                              |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **trip_idx**               | entier        | Non       | pour gérer l'ordre des données au niveau de la marée  |
+----------------------------+---------------+-----------+-------------------------------------------------------+

On remarque bien que puisque tout est organisé dans une même table, il est impossible de distinguer via cette conception :

* la cuve
* la cuve-calée
* la composition de la cuve-calée

Or pour le développement des échantillonnages avancés, il est nécéssaire de pouvoir identifier la cuve-calée; en effet 
les échantillons seront connus à ce niveau.

Il faut donc revoir cette conception pour le permettre.

# Nouvelle modélisation proposée

Il s'agit de décomposer la table **ps_logbook.WellPlan** en trois tables distinctes, ce qui apportera une granularité 
plus fine au niveau du plan de cuve nécessaire ensuite pour le développement des échantillonnages avancés sur le module Senne.

Ceci est donc un pré-requis à l'intégration des échantillonnages avancés (du module Senne), en effet au niveau des 
échantillonages avancés il faut pouvoir les rattâcher à une cuve-calée décrite dans le plan de cuve.

Le nouveau découpage se fait en trois tables :

1. Table **ps_logbook.Well** : pour définir une cuve
2. Table **ps_logbook.WellActivity** : pour définir une cuve-calée
3. Table **ps_logbook.WellActivitySpecies** : pour définir le contenu de la cuve-calée

## Table **ps_logbook.Well**

Elle caractérise une cuve du plan de cuve.

### Définition

+----------------------------+---------------+-----------+-------------------------------------------------------+
| Nom du champ               | Type          | Optionnel | Signification                                         |
+============================+===============+===========+=======================================================+
| **well**                   | texte         | Non       | le nom de la cuve                                     |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **wellVessel**             | texte         | Oui       | numérotation de la cuve navire                        |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **wellFactory**            | texte         | Oui       | numérotation de la cuve usine                         |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **wellSamplingConformity** | *FK*          | Non       | conformité de la cuve vis à vis de l'échantillonnage  |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **wellSamplingStatus**     | *FK*          | Non       | statut de l'échantillonnage sur la cuve               |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **trip**                   | *FK*          | Non       | la marée de rattachement                              |
+----------------------------+---------------+-----------+-------------------------------------------------------+

## Table **ps_logbook.WellActivity**

Elle caractérise la **cuve-calée**.

### Définition

+----------------------------+---------------+-----------+-------------------------------------------------------+
| Nom du champ               | Type          | Optionnel | Signification                                         |
+============================+===============+===========+=======================================================+
| **activity**               | *FK*          | Non       | l'activité de la cuve-calée                           |
+----------------------------+---------------+-----------+-------------------------------------------------------+
| **well**                   | *FK*          | Non       | la calée de rattachement                              |
+----------------------------+---------------+-----------+-------------------------------------------------------+
 
## Table **ps_logbook.WellActivitySpecies**

Elle caractérise le contenu de la cuve-calée, à savoir les espèces (par catégorie de poids) de l'activité de type pêche 
dans la cuve ciblée.

### Définition

+----------------------------+---------------+-----------+------------------------------------------------------------+
| Nom du champ               | Type          | Optionnel | Signification                                              |
+============================+===============+===========+=======================================================+
| **species**                | *FK*          | Non       | l'espèce dans la cuve-calée                                |
+----------------------------+---------------+-----------+------------------------------------------------------------+
| **weightCategory**         | *FK*          | Non       | la catégorie de poids de l'espèce                          |
+----------------------------+---------------+-----------+------------------------------------------------------------+
| **weight**                 | nombre        | Non       | le poids                                                   |
+----------------------------+---------------+-----------+------------------------------------------------------------+
| **count**                  | entier        | Oui       | le nombre d'individus                                      |
+----------------------------+---------------+-----------+------------------------------------------------------------+
| **setSpeciesNumber**       | entier        | Oui       | vient d'AVDTH (CUVE_CALEE.N_CALESP) (pas dans l'ui)        |
+----------------------------+---------------+-----------+------------------------------------------------------------+
| **wellActivity**           | *FK*          | Non       | la cuve-calée de rattachement                              |
+----------------------------+---------------+-----------+------------------------------------------------------------+
| **wellActivity_idx**       | entier        | Non       | pour gérer l'ordre des données au niveau de la cuve-calée  |
+----------------------------+---------------+-----------+------------------------------------------------------------+

# Organisation de l'arbre de navigation

## Rappel

En v9, on utilise un seul nœud pour accéder au plan de cuve d'une marée, ce nœud est associé à un seul formulaire de 
type tableau, chaque ligne étant une ligne de la table **WellPlan**.

## Solution proposée

Cette organisation ne convient plus, car nous devons désormais renseigner trois niveaux pour définir l'ensemble du plan 
de cuve sur la marée.

On propose alors d'utiliser deux niveaux dans l'arbre de navigation :

 * un premier niveau pour définir la liste des cuves de la marée
 * un second niveau pour définir une cuve, ses cuves-calées et la composition de celles-ci

Pour reprendre les principes déjà mis en place dans l'application, on aura un nœud *«chapeau»* qui contiendra la 
liste des cuves de la marée (principe appliqué par exemple au niveau des routes de la senne (observation et livre de bord)).

En dessous, nous aurons un nœud pour chaque cuve.

En résumé, cela donnera cela

```
Trip
└── Livre de bord et données associées
    └── Plan de cuve (contient la liste des cuves ci-dessous)
        ├── Cuve-1
        ├── Cuve-2
        ├── ...
        └── Cuve-n
```

Nous ne préconisons pas de rajouter un niveau supplémentaire pour chaque cuve-calée pour éviter l'augmentation 
significative de la taille de l'arbre de navigation.

# Formulaire

Le nouveau formulaire associé à une cuve se présentera sous la forme de deux onglets :

  * le premier pour décrire la cuve (donc les informations de la table **Well**)
  * le second pour décrire ses cuves calées et leur composition (informations de la table **WellActivity** et **WellActivitySpecies**)

Ce second onglet est un formulaire de type tableau, chaque ligne correspondant à une entrée de la table **WellActivity**.

Dans l'éditeur d'une ligne du tableau (donc d'une ligne de **WellActivity**), on retrouve l'activité et un tableau **in-line** 
pour décrire la composition de cette cuve-calée (table **WellActivitySpecies**).

# Migration du modèle de persistence

Nous assurons cette migration via le mécanisme déjà mis en place dans l'application.

# Migration du code de la migration AVDTH

Il faut aussi revoir le code de la migration AVDTH.

Pour rappel, AVDTH utilise deux tables pour décrire le plan de cuve :

1. **CUVE** qui va exactement correspondre à la nouvelle table dans ObServe **Well**
2. **CUVE_CALEE** qui contient les informations des calées au sein de la cuve, que nous devrons alors migrer dans les
nouvelles tables **WellActivity** et **WellActivitySpecies**.
