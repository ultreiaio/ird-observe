package fr.ird.observe;

/*-
 * #%L
 * ObServe :: Model
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.ObserveFixtures;
import io.ultreia.java4all.classmapping.ImmutableClassMapping;
import org.junit.Assert;

import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 28/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObservePersistenceFixtures extends ObserveFixtures {

    protected static final Map<String, String> PERSISTENCE = loadFixturesMap("persistence");
    public static final int DECORATOR_DEFINITIONS_COUNT = Integer.parseInt(PERSISTENCE.get("DECORATOR_DEFINITIONS_COUNT"));

    public static Map<String, String> getProperties() {
        return PERSISTENCE;
    }

    public static void assertFixture(String fixtureName, Object expected, Object actual) {
        Assert.assertEquals("Could not find fixtures " + fixtureName, expected, actual);
    }

    public static String getProperty(String propertyName) {
        return getProperties().get(propertyName);
    }

    public static int getIntegerProperty(String propertyName) {
        String property = Objects.requireNonNull(getProperty(propertyName), String.format("%s Can not find fixture: %s", ObservePersistenceFixtures.class.getName(), propertyName));
        return Integer.parseInt(property);
    }

    public static void assertMapping(Supplier<ImmutableClassMapping<?, ?>> mappingSupplier, String fixtureKey) {
        ImmutableClassMapping<?, ?> mapping = mappingSupplier.get();
        Assert.assertNotNull(mapping);
        int expectedCount = getIntegerProperty(fixtureKey);
        assertFixture(fixtureKey, expectedCount, mapping.size());
    }

    public static void assertMap(Supplier<Map<?, ?>> mappingSupplier, String fixtureKey) {
        Map<?, ?> mapping = mappingSupplier.get();
        Assert.assertNotNull(mapping);
        int expectedCount = getIntegerProperty(fixtureKey);
        assertFixture(fixtureKey, expectedCount, mapping.size());
    }
}

