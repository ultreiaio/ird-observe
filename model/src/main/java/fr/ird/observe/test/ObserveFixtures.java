package fr.ird.observe.test;

/*-
 * #%L
 * ObServe :: Model
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Dates;
import org.junit.Assert;

import java.util.Date;

/**
 * Created on 29/12/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveFixtures extends ToolkitFixtures {

    //        public static final boolean WITH_ASSERT = false;
    public static final boolean WITH_ASSERT = !Boolean.parseBoolean(System.getProperty("test.skipAsserts"));
    public static final Date DATE = Dates.createDate(36, 15, 17, 21, 8, 2016);

    //FIXME Remove this
    public static String getLlCommonTripId() {
        return IDS.get("LL_COMMON_TRIP");
    }
    //FIXME Remove this
    public static String getPsCommonTripId() {
        return IDS.get("PS_COMMON_TRIP");
    }

    public static void assertFixture(String fixtureName, Object expected, Object actual) {
        Assert.assertEquals(String.format("Bad fixture value %s", fixtureName), expected, actual);
    }
}
