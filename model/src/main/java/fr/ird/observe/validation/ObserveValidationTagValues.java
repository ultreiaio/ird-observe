package fr.ird.observe.validation;

/*-
 * #%L
 * ObServe :: Model
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.provider.DefaultTagValueMetadatasProvider;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.tagvalue.ObjectModelTagValuesStore;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * Created at 09/10/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
@AutoService(TagValueMetadatasProvider.class)
public class ObserveValidationTagValues extends DefaultTagValueMetadatasProvider {

    public ObserveValidationTagValues() {
        super(Store.values());
    }

    @Override
    public String getDescription() {
        return "Tag values ObServe - validation";
    }

    /**
     * Obtain the value of the {@link Store#boundTemperature} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#boundTemperature
     */
    public String getBoundTemperature(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.boundTemperature, clazz, attribute);
    }
    /**
     * Obtain the value of the {@link Store#boundNauticalLength} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#boundNauticalLength
     */
    public String getBoundNauticalLength(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.boundNauticalLength, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#speciesWeight} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#speciesWeight
     */
    public String getSpeciesWeight(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.speciesWeight, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#speciesLength} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#speciesLength
     */
    public String getSpeciesLength(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.speciesLength, clazz, attribute);
    }

    public enum Store implements TagValueMetadata {

        boundTemperature("Pour qualifier un intervalle autorisé sur un attribut de type temperature, format: min:max:unit", String.class, null, ObjectModelAttribute.class),
        boundNauticalLength("Pour qualifier un intervalle autorisé sur un attribut de type mesure de distance marine, format: min:max:unit", String.class, null, ObjectModelAttribute.class),
        speciesLength("Pour vérifier la taille d'une espèce format: (:)condition (:) pour mettre en warning (condition la condition qui permet la validation)", String.class, null, ObjectModelAttribute.class),
        speciesWeight("Pour vérifier le poids d'une espèce format: (:)condition (:) pour mettre en warning (condition la condition qui permet la validation)", String.class, null, ObjectModelAttribute.class)
        ;

        private final Set<Class<?>> targets;
        private final Class<?> type;
        private final String i18nDescriptionKey;
        private final String defaultValue;

        Store(String i18nDescriptionKey, Class<?> type, String defaultValue, Class<?>... targets) {
            this.targets = new LinkedHashSet<>(Arrays.asList(targets));
            this.type = type;
            this.i18nDescriptionKey = i18nDescriptionKey;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Set<Class<?>> getTargets() {
            return targets;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public Class<EqualsTagValueNameMatcher> getMatcherClass() {
            return EqualsTagValueNameMatcher.class;
        }

        @Override
        public String getDescription() {
            return i18nDescriptionKey;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isDeprecated() {
            return false;
        }
    }
}


