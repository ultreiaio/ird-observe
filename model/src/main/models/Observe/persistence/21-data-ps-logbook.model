package fr.ird.observe.entities.data.ps.logbook

data.ps.logbook.Activity > data.DataEntity
comment String
time Date
latitude Float
longitude Float
latitudeOriginal Float
longitudeOriginal Float
originalDataModified boolean
vmsDivergent boolean
positionCorrected boolean
number int
setCount Integer
seaSurfaceTemperature Float
windDirection Integer
vesselActivity {*:1} referential.ps.common.VesselActivity
wind {*:0..1} referential.common.Wind
totalWeight Float
currentSpeed Float
currentDirection Integer
schoolType {*:1} referential.ps.common.SchoolType
relatedObservedActivity {0..1} data.ps.observation.Activity
catches + {*} data.ps.logbook.Catch
floatingObject + {*} data.ps.logbook.FloatingObject
observedSystem {*:*} referential.ps.common.ObservedSystem
currentFpaZone {*:0..1} referential.common.FpaZone
previousFpaZone {*:0..1} referential.common.FpaZone
nextFpaZone {*:0..1} referential.common.FpaZone
dataQuality {*:0..1} referential.common.DataQuality
informationSource {*:0..1} referential.ps.logbook.InformationSource
reasonForNoFishing {*:0..1} referential.ps.common.ReasonForNoFishing
setSuccessStatus   {*:0..1} referential.ps.logbook.SetSuccessStatus
reasonForNullSet {*:0..1} referential.ps.common.ReasonForNullSet
getDate() Date
isCoordinateRequiredForFloatingObjects() boolean

data.ps.logbook.Catch > data.DataEntity >> fr.ird.observe.dto.data.WellIdAware
comment String
species {*:1} referential.common.Species
weightCategory {*:0..1} referential.ps.common.WeightCategory
speciesFate {*:0..1} referential.ps.common.SpeciesFate
weight Float
count Integer
weightMeasureMethod {*:0..1} referential.common.WeightMeasureMethod
well String

data.ps.logbook.FloatingObject > data.DataEntity
comment String
objectOperation {*:1} referential.ps.common.ObjectOperation
supportVesselName String
transmittingBuoy + {*} data.ps.logbook.TransmittingBuoy
floatingObjectPart + {*} data.ps.logbook.FloatingObjectPart
computedWhenArrivingBiodegradable !fr.ird.observe.dto.data.ps.DcpComputedValue
computedWhenArrivingNonEntangling !fr.ird.observe.dto.data.ps.DcpComputedValue
computedWhenArrivingSimplifiedObjectType String
computedWhenLeavingBiodegradable !fr.ird.observe.dto.data.ps.DcpComputedValue
computedWhenLeavingNonEntangling !fr.ird.observe.dto.data.ps.DcpComputedValue
computedWhenLeavingSimplifiedObjectType String

data.ps.logbook.FloatingObjectPart > data.DataEntity
whenArriving String
whenLeaving String
objectMaterial {*:1} referential.ps.common.ObjectMaterial

data.ps.logbook.Route > data.DataEntity >> data.WithEndDateEntityAware
comment String
date Date
timeAtSea Integer
fishingTime Integer
activity + {*} data.ps.logbook.Activity

data.ps.logbook.TransmittingBuoy > data.DataEntity
comment String
code String
transmittingBuoyOwnership {*:1} referential.ps.common.TransmittingBuoyOwnership
transmittingBuoyType {*:1} referential.ps.common.TransmittingBuoyType
transmittingBuoyOperation {*:1} referential.ps.common.TransmittingBuoyOperation
country {*:1} referential.common.Country
vessel {*:1} referential.common.Vessel
latitude Float
longitude Float

data.ps.logbook.Sample > data.DataEntity >> fr.ird.observe.dto.data.WellIdAware
number Integer
comment String
well String
sampleType {*:0..1} referential.ps.common.SampleType
sampleQuality {*:0..1} referential.ps.logbook.SampleQuality
superSample boolean
smallsWeight Float
bigsWeight Float
totalWeight Float
weightsComputed boolean
person {*:*} referential.common.Person
sampleActivity + {*} data.ps.logbook.SampleActivity
sampleSpecies + {*} data.ps.logbook.SampleSpecies

data.ps.logbook.SampleActivity > data.DataEntity >> fr.ird.observe.dto.data.ps.logbook.ActivityLabelAware
activity {*:1} data.ps.logbook.Activity
weightedWeight Float
weightedWeightComputed boolean

data.ps.logbook.SampleSpecies > data.DataEntity
comment String
startTime Date
endTime Date
subSampleNumber Integer
measuredCount Integer
totalCount Integer
species {*:1} referential.common.Species
sizeMeasureType {*:0..1} referential.common.SizeMeasureType
sampleSpeciesMeasure + {*} data.ps.logbook.SampleSpeciesMeasure

data.ps.logbook.SampleSpeciesMeasure > data.DataEntity
sizeClass Float
count Integer

data.ps.logbook.Well > data.DataEntity >> fr.ird.observe.dto.data.WellIdAware
well String
wellVessel String
wellFactory String
wellSamplingConformity {*:1} referential.ps.logbook.WellSamplingConformity
wellSamplingStatus {*:1} referential.ps.logbook.WellSamplingStatus
wellActivity + {0:*} data.ps.logbook.WellActivity
getTotalWeight(activity  data.ps.logbook.Activity, speciesIds java.util.Collection<String>) double
getTotalWeight(activity  data.ps.logbook.Activity, speciesIds java.util.Collection<String>, wellIds java.util.Collection<String>) double

data.ps.logbook.WellActivity > data.DataEntity >> fr.ird.observe.dto.data.ps.logbook.ActivityLabelAware
activity {*:1} data.ps.logbook.Activity
wellActivitySpecies + {0:*} data.ps.logbook.WellActivitySpecies
getTotalWeight(speciesIds java.util.Collection<String>) double
getTotalWeight(activity  data.ps.logbook.Activity, speciesIds java.util.Collection<String>) double

data.ps.logbook.WellActivitySpecies > data.DataEntity
species {*:1} referential.common.Species
weightCategory {*:1} referential.ps.common.WeightCategory
weight Float
count Integer
setSpeciesNumber Integer