package fr.ird.observe.model;

/*-
 * #%L
 * ObServe :: Model
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.ToolkitFixtures;
import org.junit.Assert;

import java.util.Map;
import java.util.Objects;

/**
 * Created on 26/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveModelFixtures extends ToolkitFixtures {

    protected static final Map<String, String> STORE = loadFixturesMap("model");

    public static void assertFixture(String fixtureName, Object expected, Object actual) {
        Assert.assertEquals("Could not find fixtures " + fixtureName, expected, actual);
    }

    public static Map<String, String> getProperties() {
        return STORE;
    }

    public static String getProperty(String propertyName) {
        return getProperties().get(propertyName);
    }

    public static int getIntegerProperty(String propertyName) {
        String property = Objects.requireNonNull(getProperty(propertyName), String.format("%s Can not find fixture: %s", ObserveModelFixtures.class.getName(), propertyName));
        return Integer.parseInt(property);
    }
}

