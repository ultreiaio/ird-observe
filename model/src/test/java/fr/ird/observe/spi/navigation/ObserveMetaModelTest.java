package fr.ird.observe.spi.navigation;

/*-
 * #%L
 * ObServe :: Model
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.GsonBuilder;
import fr.ird.observe.model.ObserveModelFixtures;
import fr.ird.observe.spi.navigation.model.MetaModel;
import fr.ird.observe.spi.navigation.model.id.IdNodeModel;
import fr.ird.observe.spi.navigation.model.id.IdProjectModel;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeModel;
import fr.ird.observe.spi.navigation.model.tree.TreeProjectModel;
import fr.ird.observe.spi.navigation.parent.ParentLink;
import fr.ird.observe.spi.navigation.parent.ParentProjectModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created on 30/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveMetaModelTest {

    private static final Logger log = LogManager.getLogger(ObserveMetaModelTest.class);

    @Test
    public void getModel() {

        MetaModel metaModel = new ObserveMetaModel().getModel();
        Assert.assertNotNull(metaModel);

        int expectedCountNode = ObserveModelFixtures.getIntegerProperty("Metamodel.count.nodes");
        int expectedCountRoot = ObserveModelFixtures.getIntegerProperty("Metamodel.count.root");

        ObserveModelFixtures.assertFixture("Metamodel.count.nodes", expectedCountNode, metaModel.getNodes().size());
        ObserveModelFixtures.assertFixture("Metamodel.count.root", expectedCountRoot, metaModel.getRoot().size());
    }

    @Test
    public void getIdProjectModel() {

        IdProjectModel model = new ObserveMetaModel().getIdProjectModel();
        Assert.assertNotNull(model);

        List<IdNodeModel> nodes = model.getNodes();
        log.info(String.format("Found %d edit node(s).", nodes.size()));

        int expectedCountNode = ObserveModelFixtures.getIntegerProperty("Metamodel.id.count.nodes");
        ObserveModelFixtures.assertFixture("Metamodel.id.count.nodes", expectedCountNode, nodes.size());

        String content = new GsonBuilder().setPrettyPrinting().create().toJson(model);
        log.debug(String.format("Content:\n%s", content));
    }

    @Test
    public void getTreeSelectionProjectModel() {

        TreeProjectModel model = new ObserveMetaModel().getTreeSelectionProjectModel();
        Assert.assertNotNull(model);

        List<TreeNodeModel> nodes = model.getNodes();
        log.info(String.format("Found %d selection node(s).", nodes.size()));

        String content = new GsonBuilder().setPrettyPrinting().create().toJson(model);
        log.debug(String.format("Content:\n%s", content));

        int expectedCountNode = ObserveModelFixtures.getIntegerProperty("Metamodel.tree.selection.count.nodes");
        ObserveModelFixtures.assertFixture("Metamodel.tree.selection.count.nodes", expectedCountNode, nodes.size());
    }

    @Test
    public void getTreeNavigationProjectModel() {

        TreeProjectModel model = new ObserveMetaModel().getTreeNavigationProjectModel();
        Assert.assertNotNull(model);

        List<TreeNodeModel> nodes = model.getNodes();
        log.info(String.format("Found %d navigation node(s).", nodes.size()));

        String content = new GsonBuilder().setPrettyPrinting().create().toJson(model);
        log.debug(String.format("Content:\n%s", content));

        int expectedCountNode = ObserveModelFixtures.getIntegerProperty("Metamodel.tree.navigation.count.nodes");
        ObserveModelFixtures.assertFixture("Metamodel.tree.navigation.count.nodes", expectedCountNode, nodes.size());
    }

    @Test
    public void getParentProjectModel() {

        ParentProjectModel model = new ObserveMetaModel().getParentProjectModel();
        Assert.assertNotNull(model);

        List<ParentLink> nodes = model.getLinks();
        log.info(String.format("Found %d parent node(s).", nodes.size()));
        int expectedCountNode = ObserveModelFixtures.getIntegerProperty("Metamodel.parent.count");
        ObserveModelFixtures.assertFixture("Metamodel.parent.count", expectedCountNode, nodes.size());

        String content = new GsonBuilder().setPrettyPrinting().create().toJson(model);
        log.debug(String.format("Content:\n%s", content));
    }
}
