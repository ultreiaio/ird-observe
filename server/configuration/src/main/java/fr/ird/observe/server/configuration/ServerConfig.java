package fr.ird.observe.server.configuration;

/*
 * #%L
 * ObServe Server :: Configuration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.referential.ReferentialLocale;
import io.ultreia.java4all.application.context.spi.ApplicationComponentInstantiateStrategy;
import io.ultreia.java4all.application.context.spi.GenerateApplicationComponent;
import io.ultreia.java4all.application.template.TemplateGeneratorConfig;
import io.ultreia.java4all.config.ArgumentsParserException;
import io.ultreia.java4all.config.ConfigHelper;
import io.ultreia.java4all.config.clean.CleanTemporaryFilesTaskConfiguration;
import io.ultreia.java4all.config.spi.ApplicationConfigInit;
import io.ultreia.java4all.config.spi.ApplicationConfigScope;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import io.ultreia.java4all.validation.impl.java.NuitonValidatorProviderFactoryImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Web server configuration.
 * <p>
 * Created on 29/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateApplicationComponent(name = "ObServe Web configuration",
        hints = {TemplateGeneratorConfig.class, CleanTemporaryFilesTaskConfiguration.class},
        instantiateStrategy = ApplicationComponentInstantiateStrategy.SUPPLIER, instantiateSupplier = ServerConfigFinder.class)
public class ServerConfig extends GeneratedServerConfig implements CleanTemporaryFilesTaskConfiguration {

    private static final String DEFAULT_OBSERVE_WEB_CONFIGURATION_FILENAME = "observe-server.conf";
    private final static Map<String, String> V9_MIGRATION = Map.of(
            "observeweb.adminApiKey", "instance.security.key",
            "observeweb.temporaryFilesTimeout", "instance.timeout.temporary.files",
            "observeweb.httpTimeout", "instance.timeout.http",
            "observeweb.sessionExpirationDelay", "instance.timeout.session",
            "observeweb.sessionMaximumSize", "instance.session.maximum.size"
    );
    private static Logger log = LogManager.getLogger(ServerConfig.class);
    private ReferentialLocale referentialLocale;

    protected static ApplicationConfigInit setConfigFileName(ApplicationConfigInit init) {
        return init.setConfigFileName(DEFAULT_OBSERVE_WEB_CONFIGURATION_FILENAME);
    }

    protected static ApplicationConfigInit setInstanceExtraConfigDirectory(ApplicationConfigInit init) {
        return init.setExtraConfigDirectory("${instance.directory}");
    }

    protected static ApplicationConfigInit setCommonExtraConfigDirectory(ApplicationConfigInit init) {
        return init.setExtraConfigDirectory("${common.directory.config}");
    }

    public static ServerConfig fromContextPath(String contextPath) {

        // load a first configuration with /etc authorized (and only him)
        // using extra config directory as common configuration directory
        // if default configuration does not exist, generate it and migrates from /etc

        ApplicationConfigInit init = ApplicationConfigInit.forAllScopesWithout(ApplicationConfigScope.HOME, ApplicationConfigScope.ENV)
                .addDefaults(ServerConfigOption.CONTEXT_PATH.getKey(), contextPath);

        ServerConfig fakeConfig = new ServerConfig(setCommonExtraConfigDirectory(setConfigFileName(init)), true);
        fakeConfig.initFirst();

        // Get extra scope options (if any)
        Properties extraProperties = new Properties();
        fakeConfig.get().putAll(extraProperties, ApplicationConfigScope.EXTRA);
        extraProperties.put(ServerConfigOption.CONTEXT_PATH.getKey(), contextPath);
        // Now that common files are ready, starts a normal configuration without system file and without migration possible
        ApplicationConfigInit realInit = ApplicationConfigInit.forAllScopesWithout(ApplicationConfigScope.HOME, ApplicationConfigScope.ENV, ApplicationConfigScope.SYSTEM)
                .setDefaults(extraProperties);

        ServerConfig config = new ServerConfig(setInstanceExtraConfigDirectory(setConfigFileName(realInit)));
        config.init(true);
        return config;
    }

    public static ServerConfig forTest() throws IOException {
        Files.createDirectories(new File(System.getProperty("java.io.tmpdir")).toPath());
        Path directory = Files.createTempDirectory("taiste");
        ApplicationConfigInit init = ApplicationConfigInit
                .forAllScopesWithout(ApplicationConfigScope.CURRENT,
                        ApplicationConfigScope.HOME,
                        ApplicationConfigScope.ENV,
                        ApplicationConfigScope.CLASS_PATH,
                        ApplicationConfigScope.SYSTEM)
                .addDefaults(ServerConfigOption.CONTEXT_PATH.getKey(), "observe-${build.version}")
                .addDefaults(ServerConfigOption.COMMON_CONFIG_DIRECTORY.getKey(), String.format("%s/${build.version.major}", directory.resolve("conf")))
                .addDefaults(ServerConfigOption.COMMON_INSTANCES_DIRECTORY.getKey(), directory.resolve("instances").toString());
        ServerConfig config = new ServerConfig(setInstanceExtraConfigDirectory(setConfigFileName(init)));
        config.init(false);
        return config;
    }

    private ServerConfig(ApplicationConfigInit init) {
        this(init, false);
    }

    private ServerConfig(ApplicationConfigInit init, boolean acceptMigration) {
        super(init);
        if (acceptMigration) {
            get().setPropertiesMigration(V9_MIGRATION);
        }
        NuitonValidatorProviders.setDefaultFactoryName(getValidationProviderName());
    }

    @Override
    public Map<Path, Integer> getTemporaryDirectoriesAndTimeout() {
        return Collections.singletonMap(getTemporaryDirectory().toPath(), getTemporaryFilesTimeout());
    }

    public void initFirst() {
        log.info("Starts to init ObServe fake server configuration (to generate default directories and common files...)");

        parse();

        Path baseDirectory = getCommonConfigDirectory().toPath();
        createDirectories(baseDirectory, "Could not create application main directory (%s)");

        File extraConfigFile = get().getExtraConfigFile();
        if (Files.notExists(extraConfigFile.toPath())) {
            log.info(String.format("Save common configuration file to: %s", extraConfigFile));
            ConfigHelper.save(get(), extraConfigFile, Set.of(), ServerResources.APPLICATION_CONFIGURATION, options());
        }

        File log4jConfigurationFile = getCommonLog4jConfigurationFile();
        try {
            Files.deleteIfExists(log4jConfigurationFile.toPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ServerResources.LOG_CONFIGURATION.copyResource(log4jConfigurationFile);
        log.info(String.format("Generate empty log4j configuration file to: %s", log4jConfigurationFile));
    }

    public void init(boolean strict) {
        log.info("Starts to init ObServe server configuration...");

        parse();

        Path baseDirectory = getBaseDirectory().toPath();
        createDirectories(baseDirectory, "Could not create application instance directory (%s)");

        File extraConfigFile = get().getExtraConfigFile();

        // store the configuration on extra config file to have correct options linked to the current build
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2735

        log.info(String.format("Save instance configuration file to: %s", extraConfigFile));
        ConfigHelper.save(get(), extraConfigFile, Set.of(), ServerResources.APPLICATION_CONFIGURATION, options());

        if (log.isInfoEnabled()) {
            String message = getConfigurationDescription();
            log.info(message);
        }

        Path temporaryDirectory = getTemporaryDirectory().toPath();
        createDirectories(temporaryDirectory, "Could not create temporary directory (%s)");

        File securityConfigurationFile = getSecurityConfigurationFile();
        if (!securityConfigurationFile.exists()) {
            File commonSecurityConfigurationFile = getCommonSecurityConfigurationFile();
            if (strict && !commonSecurityConfigurationFile.exists()) {
                throw new IllegalStateException(String.format("Can not start application. Could not find security.yml file.\n\nPlease add it to one of this places:\n\t%s\n\t%s", commonSecurityConfigurationFile, securityConfigurationFile));
            }
            boolean generated = ServerResources.SECURITY_CONFIGURATION.setStrict(strict).setForWindows(get().isWindowsOs()).copyResource(commonSecurityConfigurationFile.toPath(), securityConfigurationFile);
            if (generated) {
                log.info("Generate default security.yml");
            } else {
                log.info("Create symbolic link to security.yml");
            }
        }
        initLog();
        log.info("ObServe server configuration init done.");
    }

    public ReferentialLocale getReferentialLocale() {
        if (referentialLocale == null) {
            referentialLocale = ReferentialLocale.valueOf(getDbLocale());
        }
        return referentialLocale;
    }

    public String getConfigurationContent() throws IOException {
        Path path = get().getExtraConfigFile().toPath();
        return Files.readString(path);
    }

    public String getSecurityContent() throws IOException {
        Path path = getSecurityConfigurationFile().toPath();
        return Files.readString(path);
    }

    private String getValidationProviderName() {
        return NuitonValidatorProviderFactoryImpl.PROVIDER_NAME;
    }

    private void parse() {
        try {
            get().parse();
        } catch (ArgumentsParserException e) {
            throw new ObserveWebApplicationConfigInitException("could not parse configuration", e);
        }
    }

    private void initLog() {
        File log4jConfigurationFile = getLog4jConfigurationFile();
        if (Files.exists(log4jConfigurationFile.toPath())) {
            // check if version is the same as the instance one, if not then delete configuration file, otherwise keep it
            // to be able to modify it before redeploying the instance
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2736
            try {
                String content = Files.readString(log4jConfigurationFile.toPath(), StandardCharsets.UTF_8);
                if (!content.contains("observe-server-" + getBuildVersion() + ".log\"")) {
                    // We need to delete the configuration file, build version mismatch
                    log.info("Delete previous log configuration file ({}) since the build version is not the same as the instance one ({})", log4jConfigurationFile, getBuildVersion());
                    Files.deleteIfExists(log4jConfigurationFile.toPath());
                } else {
                    log.info("Keep previous log configuration file ({}) since the build version is the same as the instance one ({})", log4jConfigurationFile, getBuildVersion());
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        log.info(String.format("Chargement du fichier de log : %s", log4jConfigurationFile));
        ObserveUtil.loadLogConfiguration(ServerResources.LOG_CONFIGURATION, getCommonLog4jConfigurationFile().toPath(), log4jConfigurationFile.toPath(), this);
        log = LogManager.getLogger(ServerConfig.class);
        log.info(String.format("Configuration des logs chargée depuis le fichier %s", log4jConfigurationFile));
    }

}
