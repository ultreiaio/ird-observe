package fr.ird.observe.server.configuration;

/*-
 * #%L
 * ObServe Server :: Configuration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.ConfigResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 07/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ServerResources {
    public static final ConfigResource2 APPLICATION_CONFIGURATION = new ConfigResource2("/META-INF/configuration/observe-server.conf");
    public static final ConfigResource2 SECURITY_CONFIGURATION = new ConfigResource2("/META-INF/configuration/security.yml");
    public static final ConfigResource2 LOG_CONFIGURATION = new ConfigResource2("/META-INF/configuration/log.xml");

    public static class ConfigResource2 extends ConfigResource {

        private boolean strict;
        private boolean forWindows;

        public ConfigResource2(String location) {
            super(location);
        }

        public boolean isStrict() {
            return strict;
        }

        public ConfigResource2 setStrict(boolean strict) {
            this.strict = strict;
            return this;
        }

        public boolean isForWindows() {
            return forWindows;
        }

        public ConfigResource2 setForWindows(boolean forWindows) {
            this.forWindows = forWindows;
            return this;
        }

        @Override
        public boolean copyResource(Path sharedFile, File file) {
            if (isStrict() && Files.notExists(sharedFile)) {
                throw new IllegalStateException(String.format("You are not allowed to copy this resource: %s", this));
            }
            if (isForWindows()) {
                if (Files.exists(sharedFile)) {
                    try {
                        Files.copy(sharedFile, file.toPath());
                        return false;
                    } catch (IOException e) {
                        throw new IllegalStateException(String.format("Could not copy from %s to %s", sharedFile, file), e);
                    }
                }
                copyResource(file);
                return true;
            }
            return super.copyResource(sharedFile, file);
        }
    }
}
