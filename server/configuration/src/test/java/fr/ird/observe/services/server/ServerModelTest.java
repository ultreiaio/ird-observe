package fr.ird.observe.services.server;

/*-
 * #%L
 * ObServe Server :: Configuration
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.server.configuration.ServerConfig;
import fr.ird.observe.server.configuration.ServerResources;
import fr.ird.observe.server.security.model.InvalidServerModelException;
import fr.ird.observe.server.security.model.ServerDatabase;
import fr.ird.observe.server.security.model.ServerModel;
import fr.ird.observe.server.security.model.ServerModelHelper;
import fr.ird.observe.server.security.model.ServerUser;
import fr.ird.observe.server.security.model.ServerUserPermission;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Created on 08/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ServerModelTest {

    protected ServerConfig configuration;

    @Before
    public void setUp() throws IOException {
        configuration = ServerConfig.forTest();
    }


    @Test
    public void testLoad_issue_2284() {

        ServerResources.ConfigResource2 securityConfiguration = new ServerResources.ConfigResource2("/META-INF/configuration/security-2284.yml");

        String content = String.join("\n", securityConfiguration.readLines());
        ServerModel model = ServerModelHelper.load(content);
        try {
            ServerModelHelper.validate(model);
            Assert.fail("Should have an error");
        } catch (InvalidServerModelException e) {
            Assert.assertEquals("Should have found this message error", "Found two default databases: production and test", e.getMessage());
        }

        securityConfiguration = new ServerResources.ConfigResource2("/META-INF/configuration/security-2284-2.yml");

        content = String.join("\n", securityConfiguration.readLines());
        model = ServerModelHelper.load(content);
        try {
            ServerModelHelper.validate(model);
        } catch (InvalidServerModelException e) {
            Assert.fail("Should not have an error");
        }

        securityConfiguration = new ServerResources.ConfigResource2("/META-INF/configuration/security-2284-3.yml");

        content = String.join("\n", securityConfiguration.readLines());
        model = ServerModelHelper.load(content);
        try {
            ServerModelHelper.validate(model);
        } catch (InvalidServerModelException e) {
            Assert.fail("Should not have an error");
        }
    }

    @Test
    public void testLoad() throws Exception {

        ServerModel model = ServerModelHelper.load(configuration.getSecurityConfigurationFile());
        ServerModelHelper.validate(model);
        Assert.assertNotNull(model);
        List<ServerDatabase> databasesSet = new LinkedList<>(model.getDatabases());
        Assert.assertNotNull(databasesSet);
        Assert.assertEquals(2, databasesSet.size());

        ServerDatabase defaultDatabase = model.getDefaultDatabase().orElse(null);
        Assert.assertNotNull(defaultDatabase);
        Assert.assertEquals("production", defaultDatabase.getName());
        {
            Optional<ServerDatabase> production = model.getDatabaseByName("production");
            Assert.assertTrue(production.isPresent());
            Optional<ServerDatabase> production2 = model.getDatabaseByName("production2");
            Assert.assertFalse(production2.isPresent());
            {
                ServerDatabase database = databasesSet.get(0);
                Assert.assertNotNull(database);
                Assert.assertEquals("production", database.getName());
                Assert.assertNotNull(database.getRoles());
                Assert.assertEquals(3, database.getRoles().size());
                Assert.assertTrue(database.getDatabaseRoleByLogin("admin").isPresent());
                Assert.assertFalse(database.getDatabaseRoleByLogin("administrator2").isPresent());
                Assert.assertFalse(database.getDatabaseRoleByLogin("test").isPresent());
            }
            {
                ServerDatabase database = databasesSet.get(1);
                Assert.assertNotNull(database);
                Assert.assertEquals("test", database.getName());
                Assert.assertNotNull(database.getRoles());
                Assert.assertEquals(4, database.getRoles().size());
                Assert.assertTrue(database.getDatabaseRoleByLogin("admin").isPresent());
                Assert.assertTrue(database.getDatabaseRoleByLogin("test").isPresent());
                Assert.assertFalse(database.getDatabaseRoleByLogin("administrator2").isPresent());
            }
        }
        {
            Assert.assertNotNull(model.getUsers());
            List<ServerUser> usersSet = new LinkedList<>(model.getUsers());
            Assert.assertNotNull(usersSet);
            Assert.assertEquals(2, usersSet.size());

            Optional<ServerUser> production = model.getUserByLogin("utilisateur-technicien");
            Assert.assertTrue(production.isPresent());

            Optional<ServerUser> production2 = model.getUserByLogin("user3");
            Assert.assertFalse(production2.isPresent());

            {
                ServerUser user = usersSet.get(0);
                Assert.assertNotNull(user);
                Assert.assertEquals("utilisateur-technicien", user.getLogin());
                Assert.assertNotNull(user.getPermissions());
                Assert.assertEquals(2, user.getPermissions().size());

                Optional<ServerUserPermission> administrator = user.getPermissionByDatabaseName("production");
                Assert.assertTrue(administrator.isPresent());

                Optional<ServerUserPermission> administrator2 = user.getPermissionByDatabaseName("administrator2");
                Assert.assertFalse(administrator2.isPresent());
            }
            {
                ServerUser user = usersSet.get(1);
                Assert.assertNotNull(user);
                Assert.assertEquals("utilisateur-referentiel", user.getLogin());
                Assert.assertNotNull(user.getPermissions());
                Assert.assertEquals(1, user.getPermissions().size());

                Optional<ServerUserPermission> administrator = user.getPermissionByDatabaseName("test");
                Assert.assertTrue(administrator.isEmpty());
            }
        }
    }

}

