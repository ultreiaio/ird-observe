package fr.ird.observe.server;

/*-
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import org.debux.webmotion.server.handler.injector.ClassInjector;
import org.debux.webmotion.server.handler.injector.CollectionInjector;
import org.debux.webmotion.server.handler.injector.DateInjector;
import fr.ird.observe.server.injector.JsonAwareDtoInjector;
import fr.ird.observe.server.injector.ObserveDataSourceConfigurationInjector;
import io.ultreia.java4all.http.HRequestBuilder;
import org.debux.webmotion.server.call.ServerContext;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.mapping.Mapping;

/**
 * Improve webMotion injectors (remove usage of paranamer and stop as soon as a injector has found value (in WebMotion it tries for each injector for each parameter!!!)
 * Created on 26/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveExecutorParametersInjectorHandler extends ExecutorParametersInjectorHandler {

    @Override
    public void handlerCreated(Mapping mapping, ServerContext context) {
        super.handlerCreated(mapping, context);
        ObserveWebApplicationContext applicationContext = ObserveWebApplicationContext.get(context.getServletContext());
        Gson gson = applicationContext.getGsonSupplier().get();
        context.addInjector(new ObserveDataSourceConfigurationInjector(gson));
        context.addInjector(new ClassInjector());
        context.addInjector(new DateInjector(HRequestBuilder.DATE_PATTERN));
        context.addInjector(new JsonAwareDtoInjector(gson));
        context.addInjector(new CollectionInjector(gson));
        //FIXME Add injectors for all possible types, so will then avoid to use parameter convertor
        //FIXME which is complex (commons converter + beanutils + reflections) Heu can do much lighter
    }
}
