package fr.ird.observe.server;

/*
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.server.configuration.ServerConfig;
import fr.ird.observe.server.configuration.ServerConfigApplicationComponent;
import fr.ird.observe.server.configuration.ServerConfigFinder;
import fr.ird.observe.server.security.ObserveWebSecurityApplicationContext;
import fr.ird.observe.server.security.model.InvalidServerModelException;
import fr.ird.observe.server.security.model.ServerModel;
import fr.ird.observe.server.security.model.ServerModelHelper;
import fr.ird.observe.services.ObserveServiceFactory;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.ObserveServiceMainFactory;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.config.clean.CleanTemporaryFilesTask;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.handler.converter.JsonAwareDtoConverter;

import javax.servlet.ServletContext;
import java.io.Closeable;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Timer;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebApplicationContext extends ApplicationContext {

    static final String APPLICATION_CONTEXT_PARAMETER = ObserveWebApplicationContext.class.getName();

    private static final String MISSING_APPLICATION_CONTEXT =
            String.format("%s not found. You probably forgot to register %s in your web.xml", ObserveWebApplicationContext.class.getSimpleName(), ObserveWebApplicationListener.class.getName());

    private static final Logger log = LogManager.getLogger(ObserveWebApplicationContext.class);
    private final ServerConfig applicationConfiguration;
    private DtoGsonSupplier gsonSupplier;
    private ObserveServiceFactory mainServiceFactory;
    private ObserveWebSecurityApplicationContext securityApplicationContext;
    private Timer deleteTemporaryFilesTimer;

    public static ObserveWebApplicationContext get(HttpContext context) {
        return get(context.getServletContext());
    }

    public static ObserveWebApplicationContext get(ServletContext servletContext) {
        return optional(servletContext).orElseThrow(() -> new IllegalStateException(MISSING_APPLICATION_CONTEXT));
    }

    public static Optional<ObserveWebApplicationContext> optional(ServletContext servletContext) {
        ObserveWebApplicationContext result = (ObserveWebApplicationContext) servletContext.getAttribute(APPLICATION_CONTEXT_PARAMETER);
        return Optional.of(result);
    }

    static ObserveWebApplicationContext init(String contextPath) throws InvalidServerModelException {
        if (contextPath.startsWith("/")) {
            contextPath = contextPath.substring(1);
        }
        // init configuration
        ServerConfigFinder.set(ServerConfig.fromContextPath(contextPath));
        return new ObserveWebApplicationContext();
    }

    private ObserveWebApplicationContext() throws InvalidServerModelException {
        this.applicationConfiguration = ServerConfigApplicationComponent.value();
        init();
    }

    public void reloadConfiguration() throws InvalidServerModelException {
        init();
    }

    private void init() throws InvalidServerModelException {
        // init security model
        ServerModel securityModel = ServerModelHelper.load(applicationConfiguration.getSecurityConfigurationFile());
        ServerModelHelper.validate(securityModel);

        // init security application context
        if (securityApplicationContext == null) {
            securityApplicationContext = new ObserveWebSecurityApplicationContext(applicationConfiguration);
        }

        Version modelVersion = applicationConfiguration.getModelVersion();

        securityApplicationContext.init(applicationConfiguration.getTemporaryDirectory().toPath(), securityModel, modelVersion);

        // init service factory
        if (mainServiceFactory == null) {
            mainServiceFactory = new ObserveServiceMainFactory();
        }

        // init gson supplier
        if (gsonSupplier == null) {
            gsonSupplier = new DtoGsonSupplier();
        }
        if (deleteTemporaryFilesTimer != null) {
            closeDeleteTemporaryFilesTimer();
        }
        deleteTemporaryFilesTimer = CleanTemporaryFilesTask.create(getApplicationConfiguration());

        JsonAwareDtoConverter<SqlScript> converter = new JsonAwareDtoConverter<>(getGsonSupplier().get(), SqlScript.class);
        ConvertUtils.register(converter, SqlScript.class);
    }

    @Override
    public void close() {
        List<Closeable> closeableList = List.of(mainServiceFactory, securityApplicationContext);
        for (Closeable closeable : closeableList) {
            try {
                closeable.close();
            } catch (IOException e) {
                log.error("Could not close " + closeable, e);
            }
        }
        closeDeleteTemporaryFilesTimer();
        super.close();
        deregisterJdbcDrivers();
    }

    void deregisterJdbcDrivers() {
        // Now deregister JDBC drivers in this context's ClassLoader:
        // Get the webapp's ClassLoader
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        // Loop through all drivers
        DriverManager.drivers().filter(driver -> driver.getClass().getClassLoader() == cl).forEach(driver -> {
            // This driver was registered by the webapp's ClassLoader, so deregister it:
            try {
                log.info("Deregistering JDBC driver {}", driver);
                DriverManager.deregisterDriver(driver);
            } catch (SQLException ex) {
                log.error("Error deregistering JDBC driver {}", driver, ex);
            }
        });
    }

    void closeDeleteTemporaryFilesTimer() {
        try {
            deleteTemporaryFilesTimer.cancel();
        } catch (Exception e) {
            log.error("Could not terminates delete temporary files timer...", e);
        }
    }

    public DtoGsonSupplier getGsonSupplier() {
        return gsonSupplier;
    }

    public ServerConfig getApplicationConfiguration() {
        return applicationConfiguration;
    }

    public ObserveWebSecurityApplicationContext getSecurityApplicationContext() {
        return securityApplicationContext;
    }

    public <S extends ObserveService> S newService(ObserveServiceInitializer observeServiceInitializer, Class<S> serviceType) {
        return mainServiceFactory.newService(observeServiceInitializer, serviceType);
    }

}
