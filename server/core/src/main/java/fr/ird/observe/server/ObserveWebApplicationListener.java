package fr.ird.observe.server;

/*
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.debux.webmotion.server.WebMotionServerListener;
import org.debux.webmotion.server.call.ServerContext;
import org.debux.webmotion.server.mapping.Mapping;

import javax.servlet.ServletContext;
import java.util.Date;
import java.util.Optional;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebApplicationListener implements WebMotionServerListener {

    private static final Logger log = LogManager.getLogger(ObserveWebApplicationListener.class);

    @Override
    public void onStart(Mapping mapping, ServerContext context) {
        if (mapping.getExtensionPath() != null) {
            return;
        }
        log.info(String.format("Initializing %s", this));
        ServletContext servletContext = context.getServletContext();
        String contextPath = servletContext.getContextPath();
        if (contextPath.isEmpty()) {
            contextPath = "ROOT";
        } else if (contextPath.startsWith("/")) {
            contextPath = contextPath.substring(1);
        }
        log.info(String.format("Application starting on [%s] at %s...", contextPath, new Date()));
        ObserveWebApplicationContext applicationContext;
        try {
            applicationContext = ObserveWebApplicationContext.init(contextPath);
            servletContext.setAttribute(ObserveWebApplicationContext.APPLICATION_CONTEXT_PARAMETER, applicationContext);
        } catch (Exception e) {
            throw new ObserveWebApplicationContextInitException("Can't init application context", e);
        }
        context.setTemporaryPath(applicationContext.getApplicationConfiguration().getTemporaryDirectory().toPath());

        log.info(String.format("Initializing %s done.", this));
    }

    @Override
    public void onStop(ServerContext context) {
        log.info(String.format("Destroying %s", this));
        if (context != null) {
            Optional<ObserveWebApplicationContext> applicationContext = ObserveWebApplicationContext.optional(context.getServletContext());
            if (applicationContext.isPresent()) {
                try {
                    applicationContext.get().close();
                } catch (Exception e) {
                    log.error("Error while closing application context", e);
                }
            }
        }
        log.info(String.format("Destroying %s done.", this));
    }
}
