package fr.ird.observe.server;

/*
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.mapping.Mapping;
import org.debux.webmotion.server.render.Render;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * To renderResource any entities.
 *
 * @author Tony chemit - dev@tchemit.fr
 */
public class ObserveWebMotionRender<T> extends Render {

    protected final T model;

    public ObserveWebMotionRender(T model) {
        this.model = model;
    }

    @Override
    public void create(Mapping mapping, Call call) throws IOException {

        HttpContext context = call.getContext();
        HttpServletResponse response = context.getResponse();
        if (call.getCurrent().getMethod().getName().equals("generateId")) {
            @SuppressWarnings("unchecked") List<String> ids = (List<String>) model;
            PrintWriter out = context.getOut();
            for (String id : ids) {
                out.println(id);
            }
            return;
        }
        response.setContentType("application/json");

        ObserveWebApplicationContext applicationContext = ObserveWebApplicationContext.get(context);
        Gson gson = applicationContext.getGsonSupplier().get();
        PrintWriter out = context.getOut();
        gson.toJson(model, out);
    }
}
