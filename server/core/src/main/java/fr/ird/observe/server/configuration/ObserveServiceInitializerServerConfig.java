package fr.ird.observe.server.configuration;

/*-
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.server.security.ObserveWebUserSession;
import fr.ird.observe.services.ObserveServiceInitializerConfig;
import io.ultreia.java4all.util.Version;

/**
 * Created on 06/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.7
 */
public class ObserveServiceInitializerServerConfig extends ObserveServiceInitializerConfig {
    private final String adminApiKey;

    public ObserveServiceInitializerServerConfig(String adminApiKey, ObserveWebUserSession session, Version persistenceModelVersion, Version applicationBuildVersion) {
        super(session.getApplicationLocale(), session.getReferentialLocale(), session.getTemporaryDirectoryRoot(), session.getHttpTimeout(), persistenceModelVersion, applicationBuildVersion);
        this.adminApiKey = adminApiKey;
    }

    public String getAdminApiKey() {
        return adminApiKey;
    }

}
