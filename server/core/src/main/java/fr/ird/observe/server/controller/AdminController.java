package fr.ird.observe.server.controller;

/*
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.server.security.ObserveWebSecurityApplicationContext;
import fr.ird.observe.server.security.ObserveWebUserSession;
import fr.ird.observe.server.security.model.InvalidServerModelException;
import fr.ird.observe.spi.RenderMarkdown;
import org.apache.commons.io.IOUtils;
import org.debux.webmotion.server.WebMotionContextable;
import org.debux.webmotion.server.render.RenderContent;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

/**
 * Created on 8/30/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("unused")
public class AdminController extends ObserveWebMotionController {

    @Override
    public void setContextable(WebMotionContextable contextable) {
        super.setContextable(contextable);
        getRequestContext().checkAdminApiKeyIsValid();
    }

    public RenderContent configuration() throws IOException {
        StringBuilder builder = new StringBuilder();
        builder.append("\n## Configuration\n");
        String content = getApplicationConfiguration().getConfigurationContent();
        builder.append(String.format("\n```properties\n%s\n```", content));
        return toHtml(RenderMarkdown.renderContent(builder.toString()));
    }

    public RenderContent authenticationTokens() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n## AuthenticationTokens\n");
        StringBuilder tokens = new StringBuilder();
        Collection<ObserveWebUserSession> cache = getSecurityApplicationContext().getSessions();
        tokens.append("Number of authentication tokens: ").append(cache.size());
        for (ObserveWebUserSession entry : cache) {
            tokens.append("\n").append(entry.getAuthenticationToken()).append(" - ").append(entry.getConfiguration());
        }
        builder.append(String.format("\n```\n%s\n```", tokens));
        return toHtml(RenderMarkdown.renderContent(builder.toString()));
    }

    public RenderContent security() throws IOException {
        StringBuilder builder = new StringBuilder();
        builder.append("\n## Fichier de définition du sécurité\n");
        File securityConfigurationFile = getApplicationConfiguration().getSecurityConfigurationFile();
        builder.append(String.format("\nEmplacement : %s\n", securityConfigurationFile));
        String content = getApplicationConfiguration().getSecurityContent();
        builder.append(String.format("\n```yaml\n%s\n```", content));
        return toHtml(RenderMarkdown.renderContent(builder.toString()));
    }

    public RenderContent mapping() throws IOException {
        return renderMapping("Mapping global", "/mapping");
    }

    public RenderContent clientApiMapping() throws IOException {
        return renderMapping("Mapping de l'API Client (/api/client)", "/META-INF/mapping-api-client.wm");
    }

    public RenderContent publicApiMapping() throws IOException {
        return renderMapping("Mapping de l'API Publique (/api/public)", "/META-INF/mapping-api-public.wm");
    }

    public RenderContent adminMapping() throws IOException {
        return renderMapping("Mapping de l'administration (/admin)", "/META-INF/mapping-admin.wm");
    }

    public RenderContent docMapping() throws IOException {
        return renderMapping("Mapping de la documentation (/doc)", "/META-INF/mapping-configuration.wm");
    }

    public RenderContent resetAuthenticationTokens() {
        ObserveWebSecurityApplicationContext securityApplicationContext = getSecurityApplicationContext();
        StringBuilder builder = new StringBuilder();
        Collection<ObserveWebUserSession> authenticationTokensCache = securityApplicationContext.getSessions();
        builder.append("Number of authentication tokens to reset: ").append(authenticationTokensCache.size());
        for (ObserveWebUserSession session : authenticationTokensCache) {
            String authenticationToken = session.getAuthenticationToken();
            builder.append("\n").append(authenticationToken).append(" - ").append(session.getConfiguration());
            securityApplicationContext.invalidateAuthenticationToken(authenticationToken);
        }
        return toTextPlain(builder.toString());
    }

    public RenderContent reloadConfiguration() throws IOException, InvalidServerModelException {
        // Reset all connexions
        ObserveWebSecurityApplicationContext securityApplicationContext = getSecurityApplicationContext();
        Collection<ObserveWebUserSession> authenticationTokensCache = securityApplicationContext.getSessions();
        for (ObserveWebUserSession session : authenticationTokensCache) {
            securityApplicationContext.invalidateAuthenticationToken(session.getAuthenticationToken());
        }
        getApplicationContext().reloadConfiguration();
        return configuration();
    }

    protected RenderContent renderMapping(String title, String resource) throws IOException {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("\n## %s\n", title));
        String mapping = IOUtils.resourceToString(resource, StandardCharsets.UTF_8);
        builder.append(String.format("\n```ini\n%s\n```", mapping));
        return toHtml(RenderMarkdown.renderContent(builder.toString()));
    }

}
