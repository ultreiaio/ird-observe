package fr.ird.observe.server.controller;

/*-
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.datasource.security.model.DataSourceUserDto;
import fr.ird.observe.server.request.ObserveWebRequestContext;
import fr.ird.observe.server.security.ObserveWebUserSession;
import fr.ird.observe.services.service.AnonymousService;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")
public class AnonymousServiceRestApi extends GeneratedAnonymousServiceRestApi {

    private static final Logger log = LogManager.getLogger(AnonymousServiceRestApi.class);

    @Override
    public ObserveDataSourceInformation checkCanConnectOrBeEmpty(ObserveDataSourceConfiguration config) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        ObserveWebUserSession session = getRequestContext().getConfigurationSession(config);
        return getService().checkCanConnectOrBeEmpty(session.getConfiguration());
    }

    @Override
    public ObserveDataSourceInformation checkCanConnect(ObserveDataSourceConfiguration config) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        ObserveWebUserSession session = getRequestContext().getConfigurationSession(config);
        return getService().checkCanConnect(session.getConfiguration());
    }

    @Override
    public ObserveDataSourceConnection createEmpty(ObserveDataSourceConfiguration config) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        ObserveWebUserSession configurationSession = getRequestContext().getConfigurationSession(config);
        ObserveDataSourceConnection connection = getService().createEmpty(configurationSession.getConfiguration());
        ObserveWebUserSession authenticatedSession = getSecurityApplicationContext().registerAuthenticatedSession(configurationSession, connection);
        return authenticatedSession.toRestConnection(config);
    }

    @Override
    public ObserveDataSourceConnection createFromDump(ObserveDataSourceConfiguration config, SqlScript dump) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        ObserveWebUserSession configurationSession = getRequestContext().getConfigurationSession(config);
        ObserveDataSourceConnection connection = getService().createFromDump(configurationSession.getConfiguration(), dump);
        ObserveWebUserSession authenticatedSession = getSecurityApplicationContext().registerAuthenticatedSession(configurationSession, connection);
        return authenticatedSession.toRestConnection(config);
    }

    @Override
    public ObserveDataSourceConnection createFromImport(ObserveDataSourceConfiguration config, SqlScript dump, SqlScript optionalDump) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        ObserveWebUserSession configurationSession = getRequestContext().getConfigurationSession(config);
        ObserveDataSourceConnection connection = getService().createFromImport(configurationSession.getConfiguration(), dump, optionalDump);
        ObserveWebUserSession authenticatedSession = getSecurityApplicationContext().registerAuthenticatedSession(configurationSession, connection);
        return authenticatedSession.toRestConnection(config);
    }

    @Override
    public ObserveDataSourceConnection open(ObserveDataSourceConfiguration config) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        ObserveWebUserSession configurationSession = getRequestContext().getConfigurationSession(config);
        ObserveDataSourceConnection connection = getService().open(configurationSession.getConfiguration());
        ObserveWebUserSession authenticatedSession = getSecurityApplicationContext().registerAuthenticatedSession(configurationSession, connection);
        return authenticatedSession.toRestConnection(config);
    }

    public Map<String, Object> ping() {
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("modelVersion", getApplicationConfiguration().getModelVersion());
        Version buildVersion = getApplicationConfiguration().getBuildVersion();
        if (buildVersion.isSnapshot()) {
            buildVersion = Version.removeSnapshot(buildVersion);
        }
        result.put("serverVersion", buildVersion);
        return result;
    }

    @Override
    public AnonymousService getService() {
        Method method = contextable.getCall().getCurrent().getMethod();
        ObserveWebRequestContext requestContext = getRequestContext();
        return requestContext.newService(serviceType, method);
    }

    @Override
    public Set<DataSourceUserDto> getUsers(ObserveDataSourceConfiguration config) {
        ObserveWebUserSession session = getRequestContext().getConfigurationSession(config);
        return getService().getUsers(session.getConfiguration());
    }

    @Override
    public void applySecurity(ObserveDataSourceConfiguration config, Set<DataSourceUserDto> users) {
        ObserveWebUserSession session = getRequestContext().getConfigurationSession(config);
        getService().applySecurity(session.getConfiguration(), users);
    }

    @Override
    public void migrateData(ObserveDataSourceConfiguration config) {
        ObserveWebUserSession session = getRequestContext().getConfigurationSession(config);
        getService().migrateData(session.getConfiguration());
    }

    @Override
    public Set<String> getAvailableDatabaseNames() {
        return getSecurityApplicationContext().getAvailableDatabaseNames();
    }

    @Override
    public Version getModelVersion() {
        return getApplicationConfiguration().getModelVersion();
    }

    @Override
    public Version getServerVersion() {
        Version buildVersion = getApplicationConfiguration().getBuildVersion();
        return buildVersion.isSnapshot() ? Version.removeSnapshot(buildVersion) : buildVersion;
    }
}
