package fr.ird.observe.server.controller;

/*-
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.RenderMarkdown;
import org.apache.commons.io.IOUtils;
import org.debux.webmotion.server.render.RenderContent;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Created on 30/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ConfigurationController extends ObserveWebMotionController {

    public RenderContent detail() throws IOException {
        String resource = IOUtils.resourceToString("/META-INF/configuration/observe-server.html", StandardCharsets.UTF_8);
        return toHtml(resource);
    }

    public RenderContent example() throws IOException {
        String resource = IOUtils.resourceToString("/META-INF/configuration/observe-server.conf", StandardCharsets.UTF_8);
        return toHtml(RenderMarkdown.renderContent(String.format("## Exemple de fichier de configuration\n\n```properties\n%s\n```", resource)));
    }

}

