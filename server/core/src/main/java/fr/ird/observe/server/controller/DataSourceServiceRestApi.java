package fr.ird.observe.server.controller;

/*-
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.server.injector.ObserveDataSourceConfigurationInjector;
import fr.ird.observe.server.request.ObserveWebRequestContext;
import fr.ird.observe.server.security.ObserveWebUserSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

public class DataSourceServiceRestApi extends GeneratedDataSourceServiceRestApi {

    private static final Logger log = LogManager.getLogger(DataSourceServiceRestApi.class);

    @Override
    public void close() {
        String authenticationToken = getRequestContext().getAuthenticationToken();
        log.info(String.format("Invalidate authenticationToken: %s", authenticationToken));
        getSecurityApplicationContext().invalidateAuthenticationToken(authenticationToken);
    }

    public Object information() {
        ObserveWebRequestContext requestContext = getRequestContext();
        ObserveWebUserSession session = requestContext.getSession();
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("url", ObserveDataSourceConfigurationInjector.getUrl(getContext()));
        result.put("login", session.getUser().getLogin());
        result.put("database", session.getUserPermission().getDatabase().getName());
        result.put("apiAccess", session.getUserPermission().getApiAccess());
        result.put("validationMode", session.getUserPermission().getValidationMode());
        result.put("authenticationToken", session.getConnection().getAuthenticationToken());
        result.put("modelVersion", session.getConnection().getDataSourceInformation().getVersion());
        result.put("credentials", session.getConnection().getDataSourceInformation().getCredentials());
        return result;
    }
}
