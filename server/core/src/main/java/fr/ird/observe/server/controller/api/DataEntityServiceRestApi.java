package fr.ird.observe.server.controller.api;

/*-
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.services.service.api.InvalidDataException;
import fr.ird.observe.spi.ObservePersistenceBusinessProject;
import fr.ird.observe.spi.context.DataDtoEntityContext;

import java.util.List;

/**
 * Created on 23/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class DataEntityServiceRestApi extends GeneratedDataEntityServiceRestApi {
    @Override
    public List<String> generateId(Class<? extends DataDto> dtoType, int number) {
        DataDtoEntityContext<?, ?, ?, ?> spi = ObservePersistenceBusinessProject.fromDataDto(dtoType);
        return STANDALONE_ID_FACTORY.generateId(spi.toEntityType(), number);
    }

    @Override
    public ToolkitId update(Class<? extends DataDto> dtoType, String id, String content) throws InvalidDataException {
        return getService(true).update(dtoType, id, content);
    }

    @Override
    public ToolkitId create(Class<? extends DataDto> dtoType, String content) throws InvalidDataException {
        return getService(true).create(dtoType, content);
    }
}
