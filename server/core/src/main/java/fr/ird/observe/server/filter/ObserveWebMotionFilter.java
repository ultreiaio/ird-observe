package fr.ird.observe.server.filter;

/*
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceApiAccess;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRestConstants;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.server.ObserveWebApplicationContext;
import fr.ird.observe.server.configuration.ServerConfig;
import fr.ird.observe.server.request.ObserveWebRequestContext;
import fr.ird.observe.server.security.InvalidAuthenticationTokenException;
import fr.ird.observe.server.security.ObserveWebSecurityApplicationContext;
import fr.ird.observe.server.security.ObserveWebUserSession;
import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.debux.webmotion.server.WebMotionFilter;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.mapping.Rule;
import org.nuiton.converter.ConverterUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ObserveWebMotionFilter extends WebMotionFilter implements ObserveDataSourceConfigurationRestConstants {

    private static final Logger log = LogManager.getLogger(ObserveWebMotionFilter.class);

    protected abstract DataSourceApiAccess getApiAccess();

    public void inject(HttpContext context) {

        ObserveWebApplicationContext applicationContext = ObserveWebApplicationContext.get(context);

        HttpServletRequest request = context.getRequest();

        ServerConfig configuration = applicationContext.getApplicationConfiguration();

        Locale applicationLocale = getApplicationLocale(request).orElse(configuration.getApplicationLocale());
        ReferentialLocale referentialLocale = getReferentialLocale(request).orElse(configuration.getReferentialLocale());

        String adminApiKey = getRequestHeaderOrParameterValueOrNull(request, REQUEST_ADMIN_API_KEY).filter(p -> !p.isEmpty()).orElse(null);

        String authenticationToken = getRequestHeaderOrParameterValueOrNull(request, REQUEST_AUTHENTICATION_TOKEN).filter(p -> !p.isEmpty()).orElse(null);

        DataSourceApiAccess requestApiAccess = getApiAccess();
        ObserveWebSecurityApplicationContext securityContext = applicationContext.getSecurityApplicationContext();
        ObserveWebUserSession session;
        if (authenticationToken != null) {
            try {
                session = securityContext.getSession(applicationLocale, authenticationToken, requestApiAccess);
            } catch (InvalidAuthenticationTokenException e) {
                Rule currentRule = contextable.getCall().getCurrentRule();
                if (Objects.equals(currentRule.getAction().getMethodName(), "close")) {
                    log.info(String.format("Remove expired authenticationToken: %s", authenticationToken));
                    securityContext.invalidateAuthenticationToken(authenticationToken);
                    return;
                }
                throw e;
            }
        } else {
            session = securityContext.newAnonymousSession(applicationLocale, referentialLocale, configuration.getTemporaryDirectory(), configuration.getHttpTimeout());
        }
        ObserveWebRequestContext requestContext = session.toRequest(applicationContext, adminApiKey, configuration.getModelVersion(), configuration.getBuildVersion());
        ObserveWebRequestContext.setRequestContext(context, requestContext);

        doProcess();
    }

    protected Optional<ReferentialLocale> getReferentialLocale(HttpServletRequest request) {
        return getRequestHeaderOrParameterValueOrNull(request, REQUEST_REFERENTIAL_LOCALE).map(p -> ConverterUtil.convert(Locale.class, p)).map(ReferentialLocale::valueOf);
    }

    protected Optional<Locale> getApplicationLocale(HttpServletRequest request) {
        return getRequestHeaderOrParameterValueOrNull(request, REQUEST_APPLICATION_LOCALE).map(p -> ConverterUtil.convert(Locale.class, p));
    }

    protected Optional<String> getRequestHeaderOrParameterValueOrNull(HttpServletRequest request, String parameterName) {
        String result = request.getHeader(parameterName);
        if (Strings.isEmpty(result)) {
            return getRequestParameterValueOrNull(request, parameterName);
        }
        return Optional.of(result);
    }

    protected Optional<String> getRequestParameterValueOrNull(HttpServletRequest request, String parameterName) {
        String parameterValue = request.getParameter(parameterName);
        if (Strings.isEmpty(parameterValue)) {
            parameterValue = null;
        }
        return Optional.ofNullable(parameterValue);
    }

}
