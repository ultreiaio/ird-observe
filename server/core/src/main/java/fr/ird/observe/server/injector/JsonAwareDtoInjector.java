package fr.ird.observe.server.injector;

/*
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import fr.ird.observe.navigation.tree.io.request.ToolkitRequestConfig;
import io.ultreia.java4all.http.HRestClientService;
import io.ultreia.java4all.util.json.JsonAware;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.handler.injector.JsonInjector;
import org.nuiton.topia.persistence.filter.ToolkitRequestFilter;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

/**
 * Created on 07/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JsonAwareDtoInjector extends JsonInjector<JsonAware> {
    private static final Logger log = LogManager.getLogger(JsonAwareDtoInjector.class);

    public JsonAwareDtoInjector(Gson gson) {
        super(JsonAware.class, gson);
    }

    @Override
    protected JsonAware buildValue(Call call, String name, Class<?> type, Type generic, Call.ParameterTree parameterTree) {
        Object value = parameterTree.getValue();
        if (value == null) {
            log.warn(String.format("No value found for %s (type: %s)", name, type.getName()));
            return null;
        }
        String gsonContent = ((String[]) value)[0];
        if (generic instanceof TypeVariable) {
            TypeVariable<?> typeVariable = (TypeVariable<?>) generic;
            String parameterizedTypeParam = HRestClientService.PARAMETERIZED_TYPE_PREFIX + typeVariable.getName();
            Call.ParameterTree parameterizedTypeParameterTree = call.getParameterTree().getObject().get(parameterizedTypeParam);
            String parameterizedTypeClassName = ((String[]) parameterizedTypeParameterTree.getValue())[0];
            try {
                generic = Class.forName(parameterizedTypeClassName);
            } catch (ClassNotFoundException e) {
                throw new JsonParseException(String.format("Class not found: %s", parameterizedTypeClassName), e);
            }
        }
        return gson.fromJson(gsonContent, generic);
    }

    @Override
    protected JsonAware buildNullValue(Call call, String name, Class<?> type, Type generic) {
        if (ToolkitRequestConfig.class.isAssignableFrom(type)) {
            return gson.fromJson("{}", generic);
        }
        if (ToolkitRequestFilter.class.isAssignableFrom(type)) {
            return gson.fromJson("{}", generic);
        }
        return null;
    }
}
