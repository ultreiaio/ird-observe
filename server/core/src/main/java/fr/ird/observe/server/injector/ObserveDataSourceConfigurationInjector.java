package fr.ird.observe.server.injector;

/*
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.handler.injector.JsonInjector;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Pour injecter une configuration de source de données.
 * <p>
 * Ici on ne peut injecter qu'une configuration de source de données de type REST. La correspondance avec une source
 * physique sera faite plus tard dans le service concerné.
 * <p>
 * Created on 07/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveDataSourceConfigurationInjector extends JsonInjector<ObserveDataSourceConfiguration> {

    public static String getUrl(HttpContext context) {
        return context.getRequest().getRequestURL().toString().replace(context.getRequest().getServletPath(), "");
    }

    public ObserveDataSourceConfigurationInjector(Gson gson) {
        super(ObserveDataSourceConfiguration.class, gson);
    }

    @Override
    protected ObserveDataSourceConfiguration buildValueFormParameters(Call call, String name, Class<?> type, Type generic, Call.ParameterTree parameterTree) {
        Map<String, Call.ParameterTree> object = parameterTree.getObject();
        Map<String, Object> map = new LinkedHashMap<>();
        for (Map.Entry<String, Call.ParameterTree> entry : object.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue().getValue();
            if (value instanceof String[]) {
                value = ((String[]) value)[0];
                if (key.equals("password")) {
                    value = ((String) value).toCharArray();
                }
                map.put(key, value);
            }
        }
        String gsonContent = gson.toJson(map);
        ObserveDataSourceConfiguration config = gson.fromJson(gsonContent, ObserveDataSourceConfigurationRest.class);
        String url = config.getUrl();
        if (url == null) {
            url = getUrl(call.getContext());
        }
        config.setUrl(url);
        return config;
    }

    @Override
    protected ObserveDataSourceConfigurationRest buildValue(Call call, String name, Class<?> type, Type generic, Call.ParameterTree parameterTree) {
        String gsonContent = ((String[]) parameterTree.getValue())[0];
        return gson.fromJson(gsonContent, ObserveDataSourceConfigurationRest.class);
    }
}
