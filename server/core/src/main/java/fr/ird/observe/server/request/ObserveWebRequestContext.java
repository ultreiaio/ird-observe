package fr.ird.observe.server.request;

/*
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.server.ObserveWebApplicationContext;
import fr.ird.observe.server.configuration.ObserveServiceInitializerServerConfig;
import fr.ird.observe.server.security.AdminApiKeyNotFoundException;
import fr.ird.observe.server.security.AuthenticationTokenNotFoundException;
import fr.ird.observe.server.security.InvalidAdminKeyApiException;
import fr.ird.observe.server.security.ObserveWebSecurityApplicationContext;
import fr.ird.observe.server.security.ObserveWebUserSession;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.service.ObserveService;
import io.ultreia.java4all.http.spi.SpiHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.debux.webmotion.server.call.HttpContext;

import java.lang.reflect.Method;
import java.util.Objects;

/**
 * Created on 4/25/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebRequestContext {

    private static final String REQUEST_OBSERVE_WEB_REQUEST_CONTEXT = ObserveWebRequestContext.class.getName();
    private static final Logger log = LogManager.getLogger(ObserveWebRequestContext.class);
    private final ObserveWebApplicationContext applicationContext;
    private final ObserveServiceInitializerServerConfig serviceInitializerConfig;
    private ObserveWebUserSession session;

    public static ObserveWebRequestContext getRequestContext(HttpContext httpContext) {
        return (ObserveWebRequestContext) httpContext.getRequest().getAttribute(REQUEST_OBSERVE_WEB_REQUEST_CONTEXT);
    }

    public static void setRequestContext(HttpContext httpContext, ObserveWebRequestContext serviceContext) {
        httpContext.getRequest().setAttribute(REQUEST_OBSERVE_WEB_REQUEST_CONTEXT, serviceContext);
    }

    public ObserveWebRequestContext(ObserveWebApplicationContext applicationContext,
                                    ObserveWebUserSession session,
                                    ObserveServiceInitializerServerConfig serviceInitializerConfig) {
        this.applicationContext = Objects.requireNonNull(applicationContext);
        this.session = Objects.requireNonNull(session);
        this.serviceInitializerConfig = Objects.requireNonNull(serviceInitializerConfig);
    }

    public ObserveWebApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public ObserveServiceInitializerServerConfig getServiceInitializerConfig() {
        return serviceInitializerConfig;
    }

    public <S extends ObserveService> S newService(Class<S> serviceType, Method method) {
        if (method.getName().startsWith("api")) {
            log.error(String.format("Infinite loop ??? %s::%s", serviceType, method));
            return newService(serviceType, method);
        }
        ObserveWebUserSession session = getSession();
        checkAuthentication(session, serviceType);
        ObserveServiceInitializer observeServiceInitializer = new ObserveServiceInitializer(serviceInitializerConfig, session.getConfiguration(), session.getConnection());
        return applicationContext.newService(observeServiceInitializer, serviceType);
    }

    public void checkAdminApiKeyIsValid() {
        checkAdminApiKeyIsPresent();
        String configurationAdminKey = applicationContext.getApplicationConfiguration().getAdminApiKey();
        if (!configurationAdminKey.equals(serviceInitializerConfig.getAdminApiKey())) {
            throw new InvalidAdminKeyApiException(serviceInitializerConfig.getApplicationLocale());
        }
    }

    public String getAuthenticationToken() {
        checkAuthenticationTokenIsPresent();
        return getSession().getAuthenticationToken();
    }

    public ObserveWebUserSession getSession() {
        return session;
    }

    public ObserveWebUserSession getConfigurationSession(ObserveDataSourceConfiguration dataSourceConfigurationFromRequest) {
        if (!(dataSourceConfigurationFromRequest.isServer())) {
            throw new IllegalStateException(String.format("request is not of good type, is: %s but should be of type: %s", dataSourceConfigurationFromRequest, ObserveDataSourceConfigurationRest.class.getName()));
        }
        ObserveDataSourceConfigurationRest dataSourceConfigurationRest = (ObserveDataSourceConfigurationRest) dataSourceConfigurationFromRequest;
        String login = dataSourceConfigurationRest.getLogin();
        String password = new String(dataSourceConfigurationRest.getPassword());
        String databaseName = dataSourceConfigurationRest.getDatabaseName();
        ObserveWebSecurityApplicationContext securityApplicationContext = getApplicationContext().getSecurityApplicationContext();
        ObserveWebUserSession configurationSession = securityApplicationContext.newConfigurationSession(getSession(), login, password, databaseName);
        setSession(configurationSession);
        return configurationSession;
    }

    private void setSession(ObserveWebUserSession session) {
        this.session = session;
    }

    private void checkAdminApiKeyIsPresent() {
        if (serviceInitializerConfig.getAdminApiKey() == null) {
            throw new AdminApiKeyNotFoundException(getServiceInitializerConfig().getApplicationLocale());
        }
    }

    private void checkAuthenticationTokenIsPresent() {
        if (getSession().getAuthenticationToken() == null) {
            throw new AuthenticationTokenNotFoundException(getServiceInitializerConfig().getApplicationLocale());
        }
    }

    public void checkAuthentication(ObserveWebUserSession session, Class<? extends ObserveService> serviceType) {
        boolean anonymous = SpiHelper.anonymous(serviceType);
        boolean withConnection = session.withConnection();
        if (anonymous) {
            if (withConnection) {
                throw new IllegalStateException("Vous avez demandé un service anonyme, mais une connection est enregistrée.");
            }
        } else {
            if (!withConnection) {
                throw new IllegalStateException("Vous avez demandé un service connecté, mais aucune connection n'est enregistrée.");
            }
        }
    }
}
