package fr.ird.observe.server.security;

/*
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceApiAccess;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.server.configuration.ServerConfig;
import fr.ird.observe.server.security.model.ServerModel;
import io.ultreia.java4all.util.Version;

import java.io.Closeable;
import java.io.File;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Locale;
import java.util.Set;

/**
 * Application security context.
 * <p>
 * Manages
 *
 * <ul>
 *     <li>a cache of configuration ({@link #configurationCache}, to init a new session from an available configuration)</li>
 *     <li>a cache of session ({@link #authenticateCache} to keep authenticated session)</li>
 * </ul>
 *
 * <p>
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebSecurityApplicationContext implements Closeable {

    /**
     * Cache of available configurations.
     */
    protected final ObserveWebSecurityConfigCache configurationCache;
    /**
     * Cache of session.
     */
    protected final ObserveWebSecuritySessionCache authenticateCache;

    public ObserveWebSecurityApplicationContext(ServerConfig configuration) {
        this.authenticateCache = new ObserveWebSecuritySessionCache(configuration.getSessionExpirationDelay());
        this.configurationCache = new ObserveWebSecurityConfigCache();
    }

    public synchronized void init(Path temporaryDirectory, ServerModel serverModel, Version modelVersion) {
        authenticateCache.close();
        configurationCache.load(serverModel, modelVersion, temporaryDirectory);
    }

    public ObserveWebUserSession newAnonymousSession(Locale applicationLocale, ReferentialLocale referentialLocale, File temporaryDirectory, int httpTimeout) {
        return new ObserveWebUserSession(applicationLocale, referentialLocale, temporaryDirectory, httpTimeout);
    }

    public ObserveWebUserSession newConfigurationSession(ObserveWebUserSession anonymousSession, String userLogin, String userPassword, String optionalDatabaseName) {
        Locale locale = anonymousSession.getApplicationLocale();
        ObserveWebSecurityConfigCacheValue cacheValue = configurationCache.getValue(locale, userLogin, optionalDatabaseName, userPassword);
        return anonymousSession.toConfigurationSession(cacheValue);
    }

    public ObserveWebUserSession registerAuthenticatedSession(ObserveWebUserSession configurationSession, ObserveDataSourceConnection connection) {
        ObserveWebUserSession session = configurationSession.toAuthenticatedSession(connection);
        authenticateCache.registerSession(session);
        return session;
    }

    public Collection<ObserveWebUserSession> getSessions() {
        return authenticateCache.getSessionCache().asMap().values();
    }

    public ObserveWebUserSession getSession(Locale locale, String authenticationToken, DataSourceApiAccess requestApiAccess) {
        return authenticateCache.getSession(locale, authenticationToken, requestApiAccess);
    }

    public void invalidateAuthenticationToken(String authenticationToken) {
        authenticateCache.removeSession(authenticationToken);
    }

    public Set<String> getAvailableDatabaseNames() {
        return configurationCache.getAvailableDatabaseNames();
    }

    @Override
    public void close() {
        authenticateCache.close();
    }

}
