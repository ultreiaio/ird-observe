package fr.ird.observe.server.security;

/*-
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.server.security.model.ServerDatabase;
import fr.ird.observe.server.security.model.ServerModel;
import fr.ird.observe.server.security.model.ServerRole;
import fr.ird.observe.server.security.model.ServerUser;
import fr.ird.observe.server.security.model.ServerUserPermission;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Cache of security config, build from the {@code security.yml} file.
 * <p>
 * The method {@link #load(ServerModel, Version, Path)} load once for all the security model in the {@link #cache}.
 * <p>
 * After that, to get a configuration, use the method {@link #getKey(String, String)}.
 * <p>
 * Created on 19/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveWebSecurityConfigCache {

    private static final Logger log = LogManager.getLogger(ObserveWebSecurityConfigCache.class);

    /**
     * Available configurations indexed by the key {@code login--databaseName}, values of configurations.
     *
     * @see #getKey(String, String) to get a cache key
     */
    protected final Map<String, ObserveWebSecurityConfigCacheValue> cache;
    /**
     * Available logins (used to check if login exists)
     */
    protected final Set<String> availableUserLogin;
    /**
     * Available logins (used to check if login exists)
     */
    protected final Set<String> availableDatabaseNames;
    /**
     * Default database name (defined in {@code security.yml}), used if non database is given in a request).
     *
     * @see ServerModel#getDefaultDatabase()
     */
    protected String defaultDatabaseName;

    protected static String getKey(String userLogin, String databaseName) {
        return userLogin + "--" + databaseName;
    }

    public ObserveWebSecurityConfigCache() {
        this.cache = new TreeMap<>();
        this.availableUserLogin = new TreeSet<>();
        this.availableDatabaseNames = new TreeSet<>();
    }

    protected void load(ServerModel serverModel, Version modelVersion, Path temporaryDirectory) {
        cache.clear();
        availableUserLogin.clear();

        Optional<ServerDatabase> defaultDatabase = serverModel.getDefaultDatabase();
        defaultDatabaseName = defaultDatabase.map(ServerDatabase::getName).orElse(null);

        for (ServerUser serverUser : serverModel.getUsers()) {
            availableUserLogin.add(serverUser.getLogin());
            for (ServerUserPermission serverUserPermission : serverUser.getPermissions()) {
                ServerDatabase database = serverUserPermission.getDatabase();

                ServerRole role = serverUserPermission.getRole();
                String jdbcUrl = database.getUrl();
                String login = role.getLogin();
                String password = role.getPassword();
                String userKey = getKey(serverUser.getLogin(), database.getName());

                // Create DataSourceConfiguration
                ObserveDataSourceConfiguration configuration = ObserveDataSourceConfigurationTopiaPG.create(
                        userKey,
                        jdbcUrl,
                        login,
                        password.toCharArray(),
                        true,
                        modelVersion);
                configuration.setTemporaryDirectory(temporaryDirectory);
                ObserveWebSecurityConfigCacheValue value = new ObserveWebSecurityConfigCacheValue(serverUser, serverUserPermission, configuration);
                log.info(String.format("Creates data source configuration for userKey %s : %s", userKey, configuration));
                cache.put(userKey, value);
            }
        }
        for (ServerDatabase database : serverModel.getDatabases()) {
            availableDatabaseNames.add(database.getName());
        }
    }

    public Set<String> getAvailableDatabaseNames() {
        return availableDatabaseNames;
    }

    protected ObserveWebSecurityConfigCacheValue getValue(Locale locale, String userLogin, String databaseName, String userPassword) {
        if (Strings.isEmpty(userLogin)) {
            throw new UserLoginNotFoundException(locale);
        }
        if (Strings.isEmpty(userPassword)) {
            throw new UserPasswordNotFoundException(locale);
        }
        if (!availableUserLogin.contains(userLogin)) {
            throw new UnknownObserveWebUserException(locale, userLogin);
        }
        if (databaseName == null) {
            if (defaultDatabaseName == null) {
                throw new NoDatabaseNameException(locale, getAvailableDatabaseNames());
            } else {
                databaseName = defaultDatabaseName;
            }
        }
        String cacheKey = getKey(userLogin, databaseName);
        log.info(String.format("Try to find data source configuration for: %s", cacheKey));
        ObserveWebSecurityConfigCacheValue cacheValue = cache.get(cacheKey);
        if (cacheValue == null) {
            throw new UnknownObserveWebUserForDatabaseException(locale, databaseName, userLogin);
        }
        ServerUser user = cacheValue.getServerUser();
        //FIXME In the cache do not keep real password but a hash
        if (!Objects.equals(user.getPassword(), userPassword)) {
            throw new BadObserveWebUserPasswordException(locale, userLogin);
        }
        log.info(String.format("Will use database configuration: %s", cacheValue.getConfiguration()));
        return cacheValue;
    }
}
