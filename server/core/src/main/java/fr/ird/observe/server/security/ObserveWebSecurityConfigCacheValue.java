package fr.ird.observe.server.security;

/*-
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.server.security.model.ServerUser;
import fr.ird.observe.server.security.model.ServerUserPermission;

import java.util.Objects;

/**
 * Represents a value in the {@link ObserveWebSecurityConfigCache}.
 * <p>
 * Created on 19/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveWebSecurityConfigCacheValue {
    /**
     * User.
     */
    private final ServerUser serverUser;
    /**
     * User permission.
     */
    private final ServerUserPermission serverUserPermission;
    /**
     * Data source configuration.
     */
    private final ObserveDataSourceConfiguration configuration;

    ObserveWebSecurityConfigCacheValue(ServerUser serverUser, ServerUserPermission serverUserPermission, ObserveDataSourceConfiguration configuration) {
        this.serverUser = Objects.requireNonNull(serverUser);
        this.serverUserPermission = Objects.requireNonNull(serverUserPermission);
        this.configuration = Objects.requireNonNull(configuration);
    }

    public ServerUser getServerUser() {
        return serverUser;
    }

    public ServerUserPermission getServerUserPermission() {
        return serverUserPermission;
    }

    public ObserveDataSourceConfiguration getConfiguration() {
        return configuration;
    }
}
