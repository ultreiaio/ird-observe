package fr.ird.observe.server.security;

/*
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.RemovalListener;
import fr.ird.observe.datasource.DataSourceApiAccess;
import fr.ird.observe.entities.ObserveTopiaApplicationContextFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Cache of user session.
 * <p>
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebSecuritySessionCache implements Closeable {

    private static final Logger log = LogManager.getLogger(ObserveWebSecuritySessionCache.class);

    /**
     * Cache of user session by their session id.
     */
    protected final Cache<String, ObserveWebUserSession> sessionCache;

    public ObserveWebSecuritySessionCache(int expireDelay) {
        this.sessionCache = Caffeine.newBuilder()
                .expireAfterAccess(expireDelay, TimeUnit.MINUTES)
                .evictionListener((RemovalListener<String, ObserveWebUserSession>) (key, value, cause) -> sessionRemoved(key, cause))
                .removalListener((key, value, cause) -> sessionRemoved(key, cause))
                .build();
    }

    public ObserveWebUserSession getSession(Locale locale, String authenticationToken, DataSourceApiAccess requestApiAccess) {
        ObserveWebUserSession session = sessionCache.getIfPresent(authenticationToken);
        if (session == null) {
            throw new InvalidAuthenticationTokenException(locale, authenticationToken);
        }
        if (session.rejectApiAccess(requestApiAccess)) {
            throw new InvalidApiAccessException(locale, requestApiAccess, session.getUserPermission().getApiAccess());
        }
        return session;
    }

    public void registerSession(ObserveWebUserSession session) {
        String authenticationToken = session.getAuthenticationToken();
        log.info(String.format("Add session: %s for data source configuration: %s", authenticationToken, session.getConfiguration()));
        sessionCache.put(authenticationToken, session);
    }

    public void removeSession(String authenticationToken) {
        log.info(String.format("Will remove session: %s ", authenticationToken));
        sessionCache.invalidate(authenticationToken);
        sessionRemoved(authenticationToken, RemovalCause.EXPLICIT);
    }

    public void sessionRemoved(String authenticationToken, RemovalCause cause) {
        log.info(String.format("Session removed: %s (cause: %s)", authenticationToken, cause));
        ObserveTopiaApplicationContextFactory.closeContext(authenticationToken);
    }

    public Cache<String, ObserveWebUserSession> getSessionCache() {
        return sessionCache;
    }

    @Override
    public void close() {
        log.info("Remove all session");
        sessionCache.invalidateAll();
    }

}
