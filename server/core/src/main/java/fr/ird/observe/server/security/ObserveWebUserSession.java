package fr.ird.observe.server.security;

/*-
 * #%L
 * ObServe Server :: Core
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceApiAccess;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfigurationAndConnection;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.server.ObserveWebApplicationContext;
import fr.ird.observe.server.configuration.ObserveServiceInitializerServerConfig;
import fr.ird.observe.server.request.ObserveWebRequestContext;
import fr.ird.observe.server.security.model.ServerUser;
import fr.ird.observe.server.security.model.ServerUserPermission;
import io.ultreia.java4all.util.Version;

import java.io.File;
import java.util.Locale;
import java.util.Objects;

/**
 * The session of a user once he is connected.
 * <p>
 * Created on 06/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class ObserveWebUserSession implements ObserveDataSourceConfigurationAndConnection {

    /**
     * Data source configuration.
     */
    private final ObserveDataSourceConfiguration configuration;

    /**
     * Data source connection.
     */
    private final ObserveDataSourceConnection connection;
    /**
     * User.
     */
    private final ServerUser user;
    /**
     * User permission.
     */
    private final ServerUserPermission userPermission;
    /**
     * Locale used for translation (good old I18n system).
     */
    private final Locale applicationLocale;
    /**
     * Referential local (to get label for referential).
     */
    private final ReferentialLocale referentialLocale;
    /**
     * Temporary directory.
     */
    private final File temporaryDirectoryRoot;
    /**
     * Http timeout (in milliseconds).
     *
     * @since 6.0
     */
    private final int httpTimeout;

    public ObserveWebUserSession(Locale applicationLocale, ReferentialLocale referentialLocale, File temporaryDirectoryRoot, int httpTimeout) {
        this.configuration = null;
        this.connection = null;
        this.user = null;
        this.userPermission = null;
        this.applicationLocale = applicationLocale;
        this.referentialLocale = referentialLocale;
        this.temporaryDirectoryRoot = temporaryDirectoryRoot;
        this.httpTimeout = httpTimeout;
    }

    protected ObserveWebUserSession(ObserveWebUserSession session, ObserveDataSourceConfiguration configuration, ServerUser user, ServerUserPermission userPermission) {
        this.configuration = Objects.requireNonNull(configuration);
        this.connection = null;
        this.user = Objects.requireNonNull(user);
        this.userPermission = Objects.requireNonNull(userPermission);
        this.applicationLocale = session.getApplicationLocale();
        this.referentialLocale = session.getReferentialLocale();
        this.temporaryDirectoryRoot = session.getTemporaryDirectoryRoot();
        this.httpTimeout = session.getHttpTimeout();
    }

    public ObserveWebUserSession(ObserveWebUserSession session, ObserveDataSourceConnection connection) {
        this.configuration = Objects.requireNonNull(session).getConfiguration();
        this.connection = Objects.requireNonNull(connection);
        this.user = session.getUser();
        this.userPermission = session.getUserPermission();
        this.applicationLocale = session.getApplicationLocale();
        this.referentialLocale = session.getReferentialLocale();
        this.temporaryDirectoryRoot = session.getTemporaryDirectoryRoot();
        this.httpTimeout = session.getHttpTimeout();
    }

    public ServerUser getUser() {
        return user;
    }

    public ServerUserPermission getUserPermission() {
        return userPermission;
    }

    public String getAuthenticationToken() {
        return Objects.requireNonNull(connection).getAuthenticationToken();
    }

    public Locale getApplicationLocale() {
        return applicationLocale;
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public File getTemporaryDirectoryRoot() {
        return temporaryDirectoryRoot;
    }

    public int getHttpTimeout() {
        return httpTimeout;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObserveWebUserSession)) return false;
        ObserveWebUserSession that = (ObserveWebUserSession) o;
        return getAuthenticationToken().equals(that.getAuthenticationToken());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuthenticationToken());
    }

    @Override
    public ObserveDataSourceConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public ObserveDataSourceConnection getConnection() {
        return connection;
    }

    @Override
    public void setConfiguration(ObserveDataSourceConfiguration configuration) {

    }

    @Override
    public void setConnection(ObserveDataSourceConnection connection) {
    }

    public boolean rejectApiAccess(DataSourceApiAccess requestApiAccess) {
        return getUserPermission().rejectApiAccess(requestApiAccess);
    }

    public ObserveWebRequestContext toRequest(ObserveWebApplicationContext applicationContext, String adminApiKey, Version persistenceModelVersion, Version applicationBuildVersion) {
        ObserveServiceInitializerServerConfig serviceInitializerConfig = new ObserveServiceInitializerServerConfig(adminApiKey, this, persistenceModelVersion, applicationBuildVersion);
        return new ObserveWebRequestContext(applicationContext, this, serviceInitializerConfig);
    }

    public ObserveWebUserSession toConfigurationSession(ObserveWebSecurityConfigCacheValue configCacheKey) {
        return new ObserveWebUserSession(this, configCacheKey.getConfiguration(), configCacheKey.getServerUser(), configCacheKey.getServerUserPermission());
    }

    public ObserveWebUserSession toAuthenticatedSession(ObserveDataSourceConnection connection) {
        return new ObserveWebUserSession(this, connection);
    }

    public ObserveDataSourceConnection toRestConnection(ObserveDataSourceConfiguration config) {
        return config.toConnection(getAuthenticationToken(), getConnection().getDataSourceInformation());
    }

}
