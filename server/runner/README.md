# Abstract

This module exposes the server application.

## Init root directory

By default, server use a special directory to store all his data: ```/var/local/observe-server```

The user that run the tomcat instance must be able to read and write in this directory.

Please apply the following command with root user, before starting the first instance of v9.

```sh
mkdir -p /var/local/observe-server
chown -R tomcat:staff /var/local/observe-server
```

TODO Write a nice document for any help with the server
