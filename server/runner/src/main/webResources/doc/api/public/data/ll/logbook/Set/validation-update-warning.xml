<!--
  #%L
  ObServe Server :: Runner
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC
    "-//Apache Struts//XWork Validator 1.0.3//EN"
    "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="basketsPerSectionCount">
    <!-- basketsPerSectionCount should be filled -->
    <field-validator type="notFilled">
      <message/>
    </field-validator>
  </field>

  <field name="branchlinesPerBasketCount">
    <!-- branchlinesPerBasketCount should be filled -->
    <field-validator type="notFilled">
      <message/>
    </field-validator>
  </field>

  <field name="haulingBreaks">
    <!-- haulingBreaks should be filled -->
    <field-validator type="notFilled">
      <message/>
    </field-validator>
  </field>

  <field name="haulingEndLatitude">
    <!-- is haulingEndQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ haulingEndQuadrant != null && haulingEndLatitude != null && haulingEndLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="haulingEndLongitude">
    <!-- is haulingEndQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ haulingEndQuadrant != null && haulingEndLatitude != null && haulingEndLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="haulingEndQuadrant">
    <!-- is haulingEndQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ haulingEndQuadrant != null && haulingEndLatitude != null && haulingEndLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="haulingStartLatitude">
    <!-- is haulingStartQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ haulingStartQuadrant != null && haulingStartLatitude != null && haulingStartLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="haulingStartLongitude">
    <!-- is haulingStartQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ haulingStartQuadrant != null && haulingStartLatitude != null && haulingStartLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="haulingStartQuadrant">
    <!-- is haulingStartQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ haulingStartQuadrant != null && haulingStartLatitude != null && haulingStartLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="homeId">
    <!-- homeId string should be filled -->
    <field-validator type="stringNotFilled">
      <message/>
    </field-validator>
  </field>

  <field name="lightsticksColor">
    <!-- check if referential lightsticksColor is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="lightsticksPerBasketCount">
    <!-- lightsticksPerBasketCount should be filled -->
    <field-validator type="notFilled">
      <message/>
    </field-validator>
  </field>

  <field name="lightsticksType">
    <!-- check if referential lightsticksType is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="lineType">
    <!-- check if referential lineType is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="settingEndLatitude">
    <!-- is settingEndQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ settingEndQuadrant != null && settingEndLatitude != null && settingEndLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="settingEndLongitude">
    <!-- is settingEndQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ settingEndQuadrant != null && settingEndLatitude != null && settingEndLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="settingEndQuadrant">
    <!-- is settingEndQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ settingEndQuadrant != null && settingEndLatitude != null && settingEndLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="settingShape">
    <!-- check if referential settingShape is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="settingStartLatitude">
    <!-- is settingStartQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ settingStartQuadrant != null && settingStartLatitude != null && settingStartLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="settingStartLongitude">
    <!-- is settingStartQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ settingStartQuadrant != null && settingStartLatitude != null && settingStartLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="settingStartQuadrant">
    <!-- is settingStartQuadrant filled ? -->
    <field-validator type="notFilled" short-circuit="true">
      <param name="skip"><![CDATA[ settingStartQuadrant != null && settingStartLatitude != null && settingStartLongitude != null ]]></param>
      <message>observe.Common.coordinate.validation.notFilled</message>
    </field-validator>
  </field>

</validators>
