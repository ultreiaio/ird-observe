<!--
  #%L
  ObServe Server :: Runner
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC
    "-//Apache Struts//XWork Validator 1.0.3//EN"
    "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="activitiesAcquisitionMode">
    <!-- activitiesAcquisitionMode is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="advancedSamplingAcquisitionStatus">
    <!-- advancedSamplingAcquisitionStatus is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential advancedSamplingAcquisitionStatus is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="captain">
    <!-- check if referential captain is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="departureHarbour">
    <!-- departureHarbour is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential departureHarbour is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="departureWellContentStatus">
    <!-- departureWellContentStatus is mandatory except if !logbookEnabled -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ !logbookEnabled ]]></param>
      <message>observe.data.ps.common.Trip.departureWellContentStatus.validation.required</message>
    </field-validator>

    <!-- check if referential departureWellContentStatus is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="endDate">
    <!-- endDate is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- endDate >= startDate -->
    <field-validator type="dayAfter">
      <message/>
    </field-validator>
  </field>

  <field name="ersId">
    <!-- ersId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="fishingTime">
    <!-- fishingTime is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- 0 <= fishingTime <= 1300 except if !logbookEnabled -->
    <field-validator type="boundNumber">
      <param name="skip"><![CDATA[ !logbookEnabled ]]></param>
      <param name="min">0</param>
      <param name="max">1300</param>
      <message/>
    </field-validator>
  </field>

  <field name="formsUrl">
    <!-- formsUrl (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check url syntax except if !observationsAvailability -->
    <field-validator type="url" short-circuit="true">
      <param name="skip">!observationsAvailability</param>
      <message>observe.data.Trip.validation.invalid.formsUrl</message>
    </field-validator>
  </field>

  <field name="generalComment">
    <!-- generalComment (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- generalComment length <= 8192 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">8192</param>
      <message/>
    </field-validator>

    <!-- generalComment is required if one of the selected referential requires it (captain,departureHarbour,landingHarbour,ocean,species,tripType,vessel) -->
    <field-validator type="commentNeeded">
      <param name="propertyNames">captain,departureHarbour,landingHarbour,ocean,species,tripType,vessel</param>
      <message/>
    </field-validator>
  </field>

  <field name="homeId">
    <!-- homeId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="landingAcquisitionStatus">
    <!-- landingAcquisitionStatus is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential landingAcquisitionStatus is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="landingHarbour">
    <!-- check if referential landingHarbour is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="landingTotalWeight">
    <!-- landingTotalWeight is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- 0.0 <= landingTotalWeight <= 2000.0 except if !targetWellsSamplingEnabled -->
    <field-validator type="boundNumber">
      <param name="skip"><![CDATA[ !targetWellsSamplingEnabled ]]></param>
      <param name="min">0.0</param>
      <param name="max">2000.0</param>
      <message/>
    </field-validator>
  </field>

  <field name="landingWellContentStatus">
    <!-- landingWellContentStatus is mandatory except if !logbookEnabled -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ !logbookEnabled ]]></param>
      <message>observe.data.ps.common.Trip.landingWellContentStatus.validation.required</message>
    </field-validator>

    <!-- check if referential landingWellContentStatus is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="localMarketAcquisitionStatus">
    <!-- localMarketAcquisitionStatus is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential localMarketAcquisitionStatus is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="localMarketSurveySamplingAcquisitionStatus">
    <!-- localMarketSurveySamplingAcquisitionStatus is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential localMarketSurveySamplingAcquisitionStatus is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="localMarketTotalWeight">
    <!-- localMarketTotalWeight is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- 0.0 <= localMarketTotalWeight <= 1500.0 except if !localmarketWellsSamplingEnabled && !localmarketSurveySamplingEnabled -->
    <field-validator type="boundNumber">
      <param name="skip"><![CDATA[ !localmarketWellsSamplingEnabled && !localmarketSurveySamplingEnabled ]]></param>
      <param name="min">0.0</param>
      <param name="max">1500.0</param>
      <message/>
    </field-validator>
  </field>

  <field name="localMarketWellsSamplingAcquisitionStatus">
    <!-- localMarketWellsSamplingAcquisitionStatus is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential localMarketWellsSamplingAcquisitionStatus is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="loch">
    <!-- loch is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>
  </field>

  <field name="logbookAcquisitionStatus">
    <!-- logbookAcquisitionStatus is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential logbookAcquisitionStatus is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="logbookComment">
    <!-- logbookComment (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- logbookComment length <= 8192 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">8192</param>
      <message/>
    </field-validator>

    <!-- logbookComment is required if one of the selected referential requires it (logbookDataEntryOperator,logbookDataQuality,logbookProgram) -->
    <field-validator type="commentNeeded">
      <param name="propertyNames">logbookDataEntryOperator,logbookDataQuality,logbookProgram</param>
      <message/>
    </field-validator>
  </field>

  <field name="logbookDataEntryOperator">
    <!-- logbookDataEntryOperator is mandatory except if !logbookCommonEnabled -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ !logbookCommonEnabled ]]></param>
      <message>observe.Common.logbookDataEntryOperator.validation.required</message>
    </field-validator>

    <!-- check if referential logbookDataEntryOperator is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="logbookDataQuality">
    <!-- logbookDataQuality is mandatory except if !logbookCommonEnabled -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ !logbookCommonEnabled ]]></param>
      <message>observe.Common.logbookDataQuality.validation.required</message>
    </field-validator>

    <!-- check if referential logbookDataQuality is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="logbookProgram">
    <!-- logbookProgram is mandatory except if !logbookEnabled -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ !logbookEnabled ]]></param>
      <message>observe.data.Trip.logbookProgram.validation.required</message>
    </field-validator>

    <!-- check if referential logbookProgram is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="observationsAcquisitionStatus">
    <!-- observationsAcquisitionStatus is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential observationsAcquisitionStatus is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="observationsComment">
    <!-- observationsComment (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- observationsComment length <= 8192 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">8192</param>
      <message/>
    </field-validator>

    <!-- observationsComment is required if one of the selected referential requires it (observationMethod,observationsDataEntryOperator,observationsProgram,observationsDataQuality,observer) -->
    <field-validator type="commentNeeded">
      <param name="propertyNames">observationMethod,observationsDataEntryOperator,observationsProgram,observationsDataQuality,observer</param>
      <message/>
    </field-validator>
  </field>

  <field name="observationsDataEntryOperator">
    <!-- observationsDataEntryOperator is mandatory except if !observationsEnabled -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ !observationsEnabled ]]></param>
      <message>observe.Common.observationsDataEntryOperator.validation.required</message>
    </field-validator>

    <!-- check if referential observationsDataEntryOperator is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="observationsDataQuality">
    <!-- check if referential observationsDataQuality is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="observationsProgram">
    <!-- observationsProgram is mandatory except if !observationsEnabled -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ !observationsEnabled ]]></param>
      <message>observe.data.Trip.observationsProgram.validation.required</message>
    </field-validator>

    <!-- check if referential observationsProgram is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="observer">
    <!-- observer is mandatory except if !observationsEnabled -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ !observationsEnabled ]]></param>
      <message>observe.Common.observer.validation.required</message>
    </field-validator>

    <!-- check if referential observer is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="ocean">
    <!-- ocean is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential ocean is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="reportsUrl">
    <!-- reportsUrl (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check url syntax except if !observationsAvailability -->
    <field-validator type="url" short-circuit="true">
      <param name="skip">!observationsAvailability</param>
      <message>observe.data.Trip.validation.invalid.reportsUrl</message>
    </field-validator>
  </field>

  <field name="startDate">
    <!-- startDate is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="targetWellsSamplingAcquisitionStatus">
    <!-- targetWellsSamplingAcquisitionStatus is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential targetWellsSamplingAcquisitionStatus is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="timeAtSea">
    <!-- timeAtSea is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- 10 <= timeAtSea <= 2400 except if !logbookEnabled -->
    <field-validator type="boundNumber">
      <param name="skip"><![CDATA[ !logbookEnabled ]]></param>
      <param name="min">10</param>
      <param name="max">2400</param>
      <message/>
    </field-validator>
  </field>

  <field name="vessel">
    <!-- vessel is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential vessel is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

</validators>
