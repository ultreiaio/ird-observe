<!--
  #%L
  ObServe Server :: Runner
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC
    "-//Apache Struts//XWork Validator 1.0.3//EN"
    "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="advancedSamplingAcquisitionStatus">
    <!-- check if referential advancedSamplingAcquisitionStatus is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="captain">
    <!-- captain should be filled -->
    <field-validator type="notFilled">
      <message/>
    </field-validator>

    <!-- check if referential captain is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="departureHarbour">
    <!-- check if referential departureHarbour is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="departureWellContentStatus">
    <!-- check if referential departureWellContentStatus is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>

    <!-- departureWellContentStatus is not filled (code = 3) -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression"><![CDATA[ departureWellContentStatus == null || departureWellContentStatus.code != '3']]></param>
      <message>observe.data.ps.common.Trip.departureWellContentStatus.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="landingAcquisitionStatus">
    <!-- check if referential landingAcquisitionStatus is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="landingHarbour">
    <!-- landingHarbour should be filled -->
    <field-validator type="notFilled">
      <message/>
    </field-validator>

    <!-- check if referential landingHarbour is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="landingWellContentStatus">
    <!-- check if referential landingWellContentStatus is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>

    <!-- landingWellContentStatus is not filled (code = 3) -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression"><![CDATA[ landingWellContentStatus == null || landingWellContentStatus.code != '3']]></param>
      <message>observe.data.ps.common.Trip.landingWellContentStatus.validation.notFilled</message>
    </field-validator>
  </field>

  <field name="localMarketAcquisitionStatus">
    <!-- check if referential localMarketAcquisitionStatus is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="localMarketSurveySamplingAcquisitionStatus">
    <!-- check if referential localMarketSurveySamplingAcquisitionStatus is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="localMarketWellsSamplingAcquisitionStatus">
    <!-- check if referential localMarketWellsSamplingAcquisitionStatus is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="logbookAcquisitionStatus">
    <!-- check if referential logbookAcquisitionStatus is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="logbookDataEntryOperator">
    <!-- check if referential logbookDataEntryOperator is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="logbookDataQuality">
    <!-- check if referential logbookDataQuality is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="logbookProgram">
    <!-- check if referential logbookProgram is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="observationsAcquisitionStatus">
    <!-- check if referential observationsAcquisitionStatus is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="observationsDataEntryOperator">
    <!-- check if referential observationsDataEntryOperator is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="observationsDataQuality">
    <!-- check if referential observationsDataQuality is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="observationsProgram">
    <!-- check if referential observationsProgram is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="observer">
    <!-- check if referential observer is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="ocean">
    <!-- check if referential ocean is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="routeObs">
    <!-- check routes loch -->
    <field-validator type="psCommonTripCheckRouteObsLogValues">
      <message/>
    </field-validator>
  </field>

  <field name="targetWellsSamplingAcquisitionStatus">
    <!-- check if referential targetWellsSamplingAcquisitionStatus is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>
  </field>

  <field name="vessel">
    <!-- check if referential vessel is disabled (only if validation is not strong) -->
    <field-validator type="checkDisabledReferentialOnWarningScope">
      <message/>
    </field-validator>

    <!-- check vessel availability on trip -->
    <field-validator type="tripVessel" short-circuit="true">
      <param name="serviceName">servicesProvider.psCommonTripService</param>
      <message/>
    </field-validator>
  </field>

</validators>
