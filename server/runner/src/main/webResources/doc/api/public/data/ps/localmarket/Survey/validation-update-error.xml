<!--
  #%L
  ObServe Server :: Runner
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC
    "-//Apache Struts//XWork Validator 1.0.3//EN"
    "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="comment">
    <!-- comment (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- comment length <= 8192 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">8192</param>
      <message/>
    </field-validator>
  </field>

  <field name="date">
    <!-- date is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- date >= currentPsCommonTrip.startDate -->
    <field-validator type="dayAfter">
      <param name="otherDateProperty">currentPsCommonTrip.startDate</param>
      <message>observe.data.ps.localmarket.Survey.validation.date.after.currentPsCommonTrip.startDate##${startDate}##${endDate}</message>
    </field-validator>
    <!-- date >= currentPsCommonTrip.endDate and max delay is 20 days -->
    <field-validator type="dayAfter">
      <param name="otherDateProperty">currentPsCommonTrip.endDate</param>
      <param name="maxDelayInDays">20</param>
      <param name="delayErrorMessage">observe.data.ps.localmarket.Survey.validation.date.delay.too.long##${delayInDays}##${maxDelayInDays}</param>
      <message>observe.data.ps.localmarket.Survey.validation.date.after.currentPsCommonTrip.endDate##${startDate}##${endDate}</message>
    </field-validator>

    <!-- (date,number) unique on trip -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression"><![CDATA[ currentPsCommonTrip.isLocalmarketSurveyDateAndNumberAvailable(topiaId, date, number)]]></param>
      <message>observe.data.ps.localmarket.Survey.validation.uniqueKey</message>
    </field-validator>
  </field>

  <field name="homeId">
    <!-- homeId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- homeId length <= 255 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">255</param>
      <message/>
    </field-validator>
  </field>

  <field name="number">
    <!-- number is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- number is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>

    <!-- (date,number) unique on trip -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression"><![CDATA[ currentPsCommonTrip.isLocalmarketSurveyDateAndNumberAvailable(topiaId, date, number)]]></param>
      <message>observe.data.ps.localmarket.Survey.validation.uniqueKey</message>
    </field-validator>
  </field>

  <field name="surveyPart">
    <!-- check unique value on surveyPart for tuple species -->
    <field-validator type="psLocalmarketSurveyDtoSurveyPartCollectionUniqueKey">
      <message/>
    </field-validator>
  </field>

  <field name="surveyPartProportionSum">
    <!-- is proportion sum equals 100 (on surveyPartProportionSum) ? -->
    <field-validator type="proportionTotal" short-circuit="true">
      <message/>
    </field-validator>
  </field>

</validators>
