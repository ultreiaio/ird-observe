<!--
  #%L
  ObServe Server :: Runner
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC
    "-//Apache Struts//XWork Validator 1.0.3//EN"
    "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="comment">
    <!-- comment (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- comment length <= 8192 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">8192</param>
      <message/>
    </field-validator>

    <!-- comment is required if one of the selected referential requires it (species,speciesFate,weightCategory,weightMeasureMethod) -->
    <field-validator type="commentNeeded">
      <param name="propertyNames">species,speciesFate,weightCategory,weightMeasureMethod</param>
      <message/>
    </field-validator>
  </field>

  <field name="count">
    <!-- count is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>
  </field>

  <field name="homeId">
    <!-- homeId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="species">
    <!-- species is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential species is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="speciesFate">
    <!-- speciesFate is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential speciesFate is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="weight">
    <!-- weight is mandatory except if count != null && count > 0 -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ count != null && count > 0 ]]></param>
      <message>observe.data.ps.logbook.Catch.weight.validation.required</message>
    </field-validator>

    <!-- weight is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>
  </field>

  <field name="weightCategory">
    <!-- check if referential weightCategory is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="weightMeasureMethod">
    <!-- weightMeasureMethod is mandatory except if weight == null -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ weight == null ]]></param>
      <message>observe.data.ps.logbook.Catch.weightMeasureMethod.validation.required</message>
    </field-validator>

    <!-- check if referential weightMeasureMethod is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="well">
    <!-- well id must be valid -->
    <field-validator type="wellId" short-circuit="true">
      <param name="patternProperty">psWellRegex</param>
      <message/>
    </field-validator>

    <!-- well (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

</validators>
