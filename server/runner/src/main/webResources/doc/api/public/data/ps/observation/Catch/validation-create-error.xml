<!--
  #%L
  ObServe Server :: Runner
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC
    "-//Apache Struts//XWork Validator 1.0.3//EN"
    "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="catchWeight">
    <!-- catchWeight is mandatory except if totalCount != null -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ totalCount != null ]]></param>
      <message>observe.data.ps.observation.Catch.catchWeight.validation.required</message>
    </field-validator>

    <!-- catchWeight is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>
  </field>

  <field name="comment">
    <!-- comment (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- comment length <= 8192 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">8192</param>
      <message/>
    </field-validator>

    <!-- comment is required if one of the selected referential requires it (informationSource,reasonForDiscard,sizeMeasureMethod,species,speciesFate,weightMeasureMethod) -->
    <field-validator type="commentNeeded">
      <param name="propertyNames">informationSource,reasonForDiscard,sizeMeasureMethod,species,speciesFate,weightMeasureMethod</param>
      <message/>
    </field-validator>
  </field>

  <field name="homeId">
    <!-- homeId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="informationSource">
    <!-- informationSource is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential informationSource is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="maxWeight">
    <!-- maxWeight is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>

    <!-- maxWeight >= minWeight -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression"><![CDATA[ maxWeight == null || maxWeight > minWeight ]]></param>
      <message>observe.Common.validation.maxWeight.lesser.than.minWeight##${minWeight}##${maxWeight}</message>
    </field-validator>
  </field>

  <field name="meanLength">
    <!-- meanLength is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- check species length bound on meanLength if meanLength != null -->
    <field-validator type="species_lengthDto">
      <param name="ratio">1.0</param>
      <param name="expression"><![CDATA[ meanLength != null ]]></param>
      <message>observe.referential.common.Species.validation.length.bound##${min}##${max}##${meanLength}</message>
    </field-validator>
  </field>

  <field name="meanWeight">
    <!-- meanWeight is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- check species weight bound on meanWeight if meanWeight != null -->
    <field-validator type="species_weightDto">
      <param name="ratio">1.0</param>
      <param name="expression"><![CDATA[ meanWeight != null ]]></param>
      <message>observe.referential.common.Species.validation.weight.bound##${min}##${max}##${meanWeight}</message>
    </field-validator>
  </field>

  <field name="minWeight">
    <!-- minWeight is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>
  </field>

  <field name="reasonForDiscard">
    <!-- check if referential reasonForDiscard is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="sizeMeasureMethod">
    <!-- check if referential sizeMeasureMethod is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="species">
    <!-- species is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential species is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="speciesFate">
    <!-- speciesFate is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential speciesFate is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="totalCount">
    <!-- totalCount is mandatory except if catchWeight != null -->
    <field-validator type="mandatory" short-circuit="true">
      <param name="skip"><![CDATA[ catchWeight != null ]]></param>
      <message>observe.data.ps.observation.Catch.totalCount.validation.required</message>
    </field-validator>

    <!-- totalCount is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>
  </field>

  <field name="weightMeasureMethod">
    <!-- check if referential weightMeasureMethod is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="well">
    <!-- well (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

</validators>
