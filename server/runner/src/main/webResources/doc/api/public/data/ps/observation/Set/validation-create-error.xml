<!--
  #%L
  ObServe Server :: Runner
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC
    "-//Apache Struts//XWork Validator 1.0.3//EN"
    "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="comment">
    <!-- comment (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- comment length <= 8192 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">8192</param>
      <message/>
    </field-validator>

    <!-- comment is required if one of the selected referential requires it (reasonForNullSet,schoolType) -->
    <field-validator type="commentNeeded">
      <param name="propertyNames">reasonForNullSet,schoolType</param>
      <message/>
    </field-validator>
  </field>

  <field name="currentDirection">
    <!-- currentDirection is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- 0 <= currentDirection <= 359 -->
    <field-validator type="boundNumber">
      <param name="min">0</param>
      <param name="max">359</param>
      <message/>
    </field-validator>
  </field>

  <field name="currentMeasureDepth">
    <!-- currentMeasureDepth is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>
  </field>

  <field name="currentSpeed">
    <!-- currentSpeed is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- 0.0 <= currentSpeed <= 40.0 -->
    <field-validator type="boundNumber">
      <param name="min">0.0</param>
      <param name="max">40.0</param>
      <message/>
    </field-validator>
  </field>

  <field name="endDate">
    <!-- endDate >= haulingEndDate -->
    <field-validator type="dayAfter">
      <param name="otherDateProperty">haulingEndDate</param>
      <message>observe.data.ps.observation.Set.validation.endDate.after.haulingEndDate##${startDate}##${endDate}</message>
    </field-validator>
  </field>

  <field name="endTimeStamp">
    <!-- endTimeStamp >= startTime -->
    <field-validator type="timestampAfter">
      <param name="otherDateProperty">startTime</param>
      <message>observe.data.ps.observation.Set.validation.endTimeStamp.after.startTime##${startDate}##${endDate}</message>
    </field-validator>
    <!-- endTimeStamp >= haulingEndTimeStamp and min delay is 45 minutes -->
    <field-validator type="timestampAfter">
      <param name="otherDateProperty">haulingEndTimeStamp</param>
      <param name="minDelayInMinutes">45</param>
      <param name="delayErrorMessage">observe.data.ps.observation.Set.validation.endTimeStamp.delay.too.short##${delayInMinutes}##${minDelayInMinutes}</param>
      <message>observe.data.ps.observation.Set.validation.endTimeStamp.after.haulingEndTimeStamp##${startDate}##${endDate}</message>
    </field-validator>
  </field>

  <field name="haulingEndDate">
    <!-- haulingEndDate >= currentPsObservationRoute.date -->
    <field-validator type="dayAfter">
      <param name="otherDateProperty">currentPsObservationRoute.date</param>
      <message>observe.data.ps.observation.Set.validation.haulingEndDate.after.currentPsObservationRoute.date##${startDate}##${endDate}</message>
    </field-validator>
  </field>

  <field name="haulingEndTimeStamp">
    <!-- haulingEndTimeStamp >= startTime -->
    <field-validator type="timestampAfter">
      <param name="otherDateProperty">startTime</param>
      <message>observe.data.ps.observation.Set.validation.haulingEndTimeStamp.after.startTime##${startDate}##${endDate}</message>
    </field-validator>
  </field>

  <field name="haulingStartTimeStamp">
    <!-- haulingStartTimeStamp >= startTime -->
    <field-validator type="timestampAfter">
      <param name="otherDateProperty">startTime</param>
      <message>observe.data.ps.observation.Set.validation.haulingStartTimeStamp.after.startTime##${startDate}##${endDate}</message>
    </field-validator>
  </field>

  <field name="homeId">
    <!-- homeId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="maxGearDepth">
    <!-- maxGearDepth is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- 0.0 <= maxGearDepth <= 1100.0 -->
    <field-validator type="boundNumber">
      <param name="min">0.0</param>
      <param name="max">1100.0</param>
      <message/>
    </field-validator>
  </field>

  <field name="reasonForNullSet">
    <!-- check if referential reasonForNullSet is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="schoolMeanDepth">
    <!-- schoolMeanDepth is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- 0.0 <= schoolMeanDepth <= 2000.0 -->
    <field-validator type="boundNumber">
      <param name="min">0.0</param>
      <param name="max">2000.0</param>
      <message/>
    </field-validator>
  </field>

  <field name="schoolThickness">
    <!-- schoolThickness is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- 1.0 <= schoolThickness <= 500.0 -->
    <field-validator type="boundNumber">
      <param name="min">1.0</param>
      <param name="max">500.0</param>
      <message/>
    </field-validator>
  </field>

  <field name="schoolTopDepth">
    <!-- schoolTopDepth is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>

    <!-- 0.0 <= schoolTopDepth <= 1000.0 -->
    <field-validator type="boundNumber">
      <param name="min">0.0</param>
      <param name="max">1000.0</param>
      <message/>
    </field-validator>
  </field>

  <field name="schoolType">
    <!-- check if referential schoolType is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="startTime">
    <!-- startTime >= currentPsObservationActivity.time -->
    <field-validator type="timeAfter">
      <param name="otherDateProperty">currentPsObservationActivity.time</param>
      <message>observe.data.ps.observation.Set.validation.startTime.after.currentPsObservationActivity.time##${startDate}##${endDate}</message>
    </field-validator>
  </field>

  <field name="supportVesselName">
    <!-- supportVesselName (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- supportVesselName length <= 32 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">32</param>
      <message/>
    </field-validator>
  </field>

</validators>
