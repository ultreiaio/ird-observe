/*-
 * #%L
 * ObServe Server :: Runner
 * %%
 * Copyright (C) 2008 - 2025 IRD, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

function loadContent(file) {
    let target = document.getElementById(file);
    if (target) {
        let oReq = new XMLHttpRequest();
        oReq.addEventListener("load",
            function responseListener() {

                let text = this.responseText;
                try {
                    let response = JSON.parse(text);
                    const formatter = new JSONFormatter(response, 100, {
                        hoverPreviewEnabled: false,
                        hoverPreviewArrayCount: 100,
                        hoverPreviewFieldCount: 5,
                        animateOpen: true,
                        animateClose: true,
                        theme: null, // or 'dark'
                        useToJSON: true // use the toJSON method to render an object as a string as available
                    });
                    let responseFormatter = formatter.render();
                    document.getElementById(file + "-view-source").appendChild(responseFormatter);
                } catch (e) {
                    document.getElementById(file + "-view-source").innerHTML = text
                }
                document.getElementById(file + "-raw-source").innerHTML = text;
            }
        );
        oReq.open("GET", file + ".json");
        oReq.send();
    }
}

function loadTypeDefinitionContent(file, create, update) {
    let target = document.getElementById(file);
    if (target) {
        let validation = {};
        load(create + ".json", function onLoad(text) {
            validation.create = JSON.parse(text);
            load(update + ".json", function onLoad(text) {
                validation.update = JSON.parse(text);
                load(file + ".json", function onLoad(text) {

                    let response = JSON.parse(text);
                    let html = '<table class="definition-table">';
                    html += '<thead><tr><th>Field name</th><th>Type</th><th>Create validation</th><th>Update validation</th></tr></thead>';
                    for (let propertyName in response) {
                        let propertyType = response[propertyName];
                        let createValidation = validation.create[propertyName];
                        let updateValidation = validation.update[propertyName];
                        html += '<tr><td><span>' + propertyName + '</span></td><td><span>' + propertyType + '</span></td>';
                        html += addValidationMessages(createValidation);
                        html += addValidationMessages(updateValidation);
                        html += '</tr>';
                    }
                    html += '</table>';
                    document.getElementById(file + "-source").innerHTML = html;
                });
            });
        });
    }
}


function loadRequestDefinitionContent(file, create) {
    let tag = file;
    if (tag.indexOf("../")===0) {
        tag = tag.substring(3);
    }
    console.info(tag);
    let target = document.getElementById(tag);
    if (target) {
        let validation = {};
        load(create + ".json", function onLoad(text) {
            validation.create = JSON.parse(text);
                load(file + ".json", function onLoad(text) {

                    let response = JSON.parse(text);
                    let html = '<table class="definition-table">';
                    html += '<thead><tr><th>Field name</th><th>Type</th><th>Validation</th></tr></thead>';
                    for (let propertyName in response) {
                        let propertyType = response[propertyName];
                        let createValidation = validation.create[propertyName];
                        html += '<tr><td><span>' + propertyName + '</span></td><td><span>' + propertyType + '</span></td>';
                        html += addValidationMessages(createValidation);
                        html += '</tr>';
                    }
                    html += '</table>';
                    document.getElementById(tag + "-source").innerHTML = html;
                });
        });
    }
}

function loadXml(tag, file) {
    let target = document.getElementById(tag + "-xml-source");
    if (target) {
        let oReq = new XMLHttpRequest();
        oReq.addEventListener("load",
            function responseListener() {
                let text = this.responseText;
                let indexOf = text.indexOf("<!DOCTYPE");
                target.innerHTML = text.substring(indexOf);
            }
        );
        oReq.open("GET", file + ".xml");
        oReq.send();
    }
}

function loadJson(tag, file) {
    let target = document.getElementById(tag + "-json-source");
    if (target) {
        load(file + ".json", function onLoad(text) {
            try {
                let response = JSON.parse(text);
                const formatter = new JSONFormatter(response, 100, {
                    hoverPreviewEnabled: false,
                    hoverPreviewArrayCount: 100,
                    hoverPreviewFieldCount: 5,
                    animateOpen: true,
                    animateClose: true,
                    theme: null, // or 'dark'
                    useToJSON: true // use the toJSON method to render an object as a string as available
                });
                let responseFormatter = formatter.render();
                // console.log("Youhou1 "+file+" : "+text);
                // console.log("Youhou2 "+file+" : "+responseFormatter);
                target.appendChild(responseFormatter);
            } catch (e) {
                target.innerHTML = text
            }
        });
    }
}

function addValidationMessages( messages) {
    let result = '<td><ul>';
    if (!!messages) {
        let errors = messages['errors'];
        let warnings = messages['warnings'];
        for (let message in errors) {
            result += '<li class="mandatory">' + errors[message] + '</li>';
        }
        for (let message in warnings) {
            result += '<li class="optional">' + warnings[message] + '</li>';
        }
    }
    result += '</ul></td>';
    return result;
}

function load(file, callback) {
    let validationRequest = new XMLHttpRequest();
    validationRequest.addEventListener("load",
        function responseListener() {
            let text = this.responseText;
            callback(text);
        }
    );
    validationRequest.open("GET", file);
    validationRequest.send();
}
