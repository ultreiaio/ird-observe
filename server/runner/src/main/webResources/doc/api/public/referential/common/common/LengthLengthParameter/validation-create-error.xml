<!--
  #%L
  ObServe Server :: Runner
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC
    "-//Apache Struts//XWork Validator 1.0.3//EN"
    "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="coefficients">
    <!-- coefficients is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- coefficients (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="endDate">
    <!-- endDate >= startDate -->
    <field-validator type="dayAfter">
      <message/>
    </field-validator>
  </field>

  <field name="homeId">
    <!-- homeId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- homeId should be unique -->
    <field-validator type="referentialUniqueField" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="inputOutputFormula">
    <!-- inputOutputFormula is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- inputOutputFormula (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check relation inputOutputFormula syntax -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ inputOutputFormulaValid ]]>
      </param>
      <message>observe.referential.common.LengthLengthParameter.validation.invalid.inputOutputFormula
      </message>
    </field-validator>
  </field>

  <field name="inputSizeMeasureType">
    <!-- inputSizeMeasureType is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential inputSizeMeasureType is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>

    <!-- clef unique species - gender - ocean - inputSizeMeasureTypeCode - outputSizeMeasureTypeCode -->
    <field-validator type="observeLengthFormulaCollectionUniqueKeyDto" short-circuit="true">
      <param name="collectionFieldName">editingReferentielList</param>
      <param name="keys">species,sexLabel,oceanLabel,inputSizeMeasureType,outputSizeMeasureType</param>
      <!--<param name="againstMe">true</param>-->
      <message>observe.referential.common.LengthLengthParameter.validation.invalid.uniqueKey</message>
    </field-validator>
  </field>

  <field name="ocean">
    <!-- ocean is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential ocean is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>

    <!-- clef unique species - gender - ocean - inputSizeMeasureTypeCode - outputSizeMeasureTypeCode -->
    <field-validator type="observeLengthFormulaCollectionUniqueKeyDto" short-circuit="true">
      <param name="collectionFieldName">editingReferentielList</param>
      <param name="keys">species,sexLabel,oceanLabel,inputSizeMeasureType,outputSizeMeasureType</param>
      <!--<param name="againstMe">true</param>-->
      <message>observe.referential.common.LengthLengthParameter.validation.invalid.uniqueKey</message>
    </field-validator>
  </field>

  <field name="outputInputFormula">
    <!-- outputInputFormula is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- outputInputFormula (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check relation outputInputFormula syntax -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ outputInputFormulaValid ]]>
      </param>
      <message>observe.referential.common.LengthLengthParameter.validation.invalid.outputInputFormula
      </message>
    </field-validator>
  </field>

  <field name="outputSizeMeasureType">
    <!-- outputSizeMeasureType is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential outputSizeMeasureType is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>

    <!-- clef unique species - gender - ocean - inputSizeMeasureTypeCode - outputSizeMeasureTypeCode -->
    <field-validator type="observeLengthFormulaCollectionUniqueKeyDto" short-circuit="true">
      <param name="collectionFieldName">editingReferentielList</param>
      <param name="keys">species,sexLabel,oceanLabel,inputSizeMeasureType,outputSizeMeasureType</param>
      <!--<param name="againstMe">true</param>-->
      <message>observe.referential.common.LengthLengthParameter.validation.invalid.uniqueKey</message>
    </field-validator>
  </field>

  <field name="sex">
    <!-- sex is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential sex is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>

    <!-- clef unique species - gender - ocean - inputSizeMeasureTypeCode - outputSizeMeasureTypeCode -->
    <field-validator type="observeLengthFormulaCollectionUniqueKeyDto" short-circuit="true">
      <param name="collectionFieldName">editingReferentielList</param>
      <param name="keys">species,sexLabel,oceanLabel,inputSizeMeasureType,outputSizeMeasureType</param>
      <!--<param name="againstMe">true</param>-->
      <message>observe.referential.common.LengthLengthParameter.validation.invalid.uniqueKey</message>
    </field-validator>
  </field>

  <field name="source">
    <!-- source (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- source length <= 8192 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">8192</param>
      <message/>
    </field-validator>
  </field>

  <field name="species">
    <!-- species is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential species is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>

    <!-- clef unique species - gender - ocean - inputSizeMeasureTypeCode - outputSizeMeasureTypeCode -->
    <field-validator type="observeLengthFormulaCollectionUniqueKeyDto" short-circuit="true">
      <param name="collectionFieldName">editingReferentielList</param>
      <param name="keys">species,sexLabel,oceanLabel,inputSizeMeasureType,outputSizeMeasureType</param>
      <!--<param name="againstMe">true</param>-->
      <message>observe.referential.common.LengthLengthParameter.validation.invalid.uniqueKey</message>
    </field-validator>
  </field>

  <field name="startDate">
    <!-- clef unique species - gender - ocean - inputSizeMeasureTypeCode - outputSizeMeasureTypeCode -->
    <field-validator type="observeLengthFormulaCollectionUniqueKeyDto" short-circuit="true">
      <param name="collectionFieldName">editingReferentielList</param>
      <param name="keys">species,sexLabel,oceanLabel,inputSizeMeasureType,outputSizeMeasureType</param>
      <!--<param name="againstMe">true</param>-->
      <message>observe.referential.common.LengthLengthParameter.validation.invalid.uniqueKey</message>
    </field-validator>
  </field>

  <field name="uri">
    <!-- uri (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- uri should be unique -->
    <field-validator type="referentialUniqueField" short-circuit="true">
      <message/>
    </field-validator>
  </field>

</validators>
