<!--
  #%L
  ObServe Server :: Runner
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC
    "-//Apache Struts//XWork Validator 1.0.3//EN"
    "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="capacity">
    <!-- capacity is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>
  </field>

  <field name="cfrId">
    <!-- cfrId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="code">
    <!-- code is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- code (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- code should be unique -->
    <field-validator type="referentialUniqueField" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="comId">
    <!-- comId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="comment">
    <!-- comment (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- comment length <= 8192 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">8192</param>
      <message/>
    </field-validator>

    <!-- comment is required if one of the selected referential requires it (flagCountry,fleetCountry,shipOwner,vesselSizeCategory,vesselType) -->
    <field-validator type="commentNeeded">
      <param name="propertyNames">flagCountry,fleetCountry,shipOwner,vesselSizeCategory,vesselType</param>
      <message/>
    </field-validator>
  </field>

  <field name="endDate">
    <!-- endDate >= startDate -->
    <field-validator type="dayAfter">
      <message/>
    </field-validator>
  </field>

  <field name="flagCountry">
    <!-- flagCountry is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential flagCountry is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="fleetCountry">
    <!-- fleetCountry is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential fleetCountry is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="homeId">
    <!-- homeId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- homeId should be unique -->
    <field-validator type="referentialUniqueField" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="iccatId">
    <!-- iccatId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="imoId">
    <!-- imoId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="iotcId">
    <!-- iotcId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="keelCode">
    <!-- keelCode is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>
  </field>

  <field name="label1">
    <!-- label1 is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- label1 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label2">
    <!-- label2 is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- label2 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label3">
    <!-- label3 is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- label3 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label4">
    <!-- label4 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label5">
    <!-- label5 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label6">
    <!-- label6 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label7">
    <!-- label7 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label8">
    <!-- label8 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="length">
    <!-- length is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>
  </field>

  <field name="lloydId">
    <!-- lloydId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="nationalId">
    <!-- nationalId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="powerCv">
    <!-- powerCv is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>
  </field>

  <field name="powerKW">
    <!-- powerKW is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>
  </field>

  <field name="radioCallSignId">
    <!-- radioCallSignId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="searchMaximum">
    <!-- searchMaximum is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>
  </field>

  <field name="shipOwner">
    <!-- check if referential shipOwner is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="source">
    <!-- source (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- source length <= 512 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">512</param>
      <message/>
    </field-validator>
  </field>

  <field name="tuviId">
    <!-- tuviId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="uri">
    <!-- uri (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- uri should be unique -->
    <field-validator type="referentialUniqueField" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="vesselSizeCategory">
    <!-- vesselSizeCategory is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential vesselSizeCategory is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="vesselType">
    <!-- vesselType is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>

    <!-- check if referential vesselType is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="wellRegex">
    <!-- wellRegex (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- wellRegex length <= 512 -->
    <field-validator type="stringMaxLength">
      <param name="maxLength">512</param>
      <message/>
    </field-validator>

    <!-- wellRegex must be a valid regex expression -->
    <field-validator type="regexField" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="yearService">
    <!-- yearService is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>
  </field>

</validators>
