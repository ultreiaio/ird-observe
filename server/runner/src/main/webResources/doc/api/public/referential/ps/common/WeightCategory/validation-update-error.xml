<!--
  #%L
  ObServe Server :: Runner
  %%
  Copyright (C) 2008 - 2025 IRD, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC
    "-//Apache Struts//XWork Validator 1.0.3//EN"
    "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="atLeastOneSelected">
    <!-- At least one of the following properties must be selected (landing,logbook,wellPlan) -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression"> <![CDATA[ atLeastOneSelected ]]> </param>
      <message>observe.referential.ps.common.WeightCategory.validation.atLeastOneSelected</message>
    </field-validator>
  </field>

  <field name="code">
    <!-- code is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- code (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- code should be unique -->
    <field-validator type="referentialUniqueField" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="homeId">
    <!-- homeId (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- homeId should be unique -->
    <field-validator type="referentialUniqueField" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label1">
    <!-- label1 is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- label1 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label2">
    <!-- label2 is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- label2 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label3">
    <!-- label3 is a mandatory string -->
    <field-validator type="mandatoryString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- label3 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label4">
    <!-- label4 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label5">
    <!-- label5 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label6">
    <!-- label6 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label7">
    <!-- label7 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="label8">
    <!-- label8 (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="landing">
    <!-- landing is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="logbook">
    <!-- logbook is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="maxWeight">
    <!-- maxWeight is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>

    <!-- maxWeight >= meanWeight -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression"><![CDATA[ meanWeight == null || maxWeight == null || maxWeight >= meanWeight ]]></param>
      <message>observe.Common.validation.maxWeight.lesser.than.meanWeight##${meanWeight}##${maxWeight}</message>
    </field-validator>

    <!-- maxWeight >= minWeight -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression"><![CDATA[ maxWeight == null || maxWeight > minWeight ]]></param>
      <message>observe.Common.validation.maxWeight.lesser.than.minWeight##${minWeight}##${maxWeight}</message>
    </field-validator>
  </field>

  <field name="meanWeight">
    <!-- meanWeight is a strictly positive number -->
    <field-validator type="positiveNumber">
      <param name="strict">true</param>
      <message/>
    </field-validator>

    <!-- meanWeight >= minWeight -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression"><![CDATA[ minWeight == null || meanWeight == null || meanWeight >= minWeight ]]></param>
      <message>observe.Common.validation.meanWeight.lesser.than.minWeight##${minWeight}##${meanWeight}</message>
    </field-validator>
  </field>

  <field name="minWeight">
    <!-- minWeight is a positive number -->
    <field-validator type="positiveNumber">
      <message/>
    </field-validator>
  </field>

  <field name="species">
    <!-- check if referential species is disabled (only if validation is strong) -->
    <field-validator type="checkDisabledReferentialOnErrorScope">
      <message/>
    </field-validator>
  </field>

  <field name="uri">
    <!-- uri (if not null) is not a blank string -->
    <field-validator type="notBlankString" short-circuit="true">
      <message/>
    </field-validator>

    <!-- uri should be unique -->
    <field-validator type="referentialUniqueField" short-circuit="true">
      <message/>
    </field-validator>
  </field>

  <field name="wellPlan">
    <!-- wellPlan is mandatory -->
    <field-validator type="mandatory" short-circuit="true">
      <message/>
    </field-validator>
  </field>

</validators>
