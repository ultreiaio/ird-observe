Bonjour à tous,

L'équipe ObServe est heureuse de vous annoncer la version 2.0.1 !

Cette version intègre un unique changement :

- Amélioration du temps de chargement des activités.

La documentation du projet est ici:
http://observe.labs.libre-entreprise.org/observe/index.html

La documentation d'administration des bases obstuna est ici :
http://observe.labs.libre-entreprise.org/observe/install-serverPG.html

Note de version :
http://observe.labs.libre-entreprise.org/observe/release-note.html

Fichiers téléchargeables :
https://labs.libre-entreprise.org/frs/?group_id=138

Pour toute anomalie rencontrée, merci de remplir un bugreport à cette adresse :
https://labs.libre-entreprise.org/tracker/?atid=618&group_id=138&func=browse

Cordialement,

L'équipe ObServe
