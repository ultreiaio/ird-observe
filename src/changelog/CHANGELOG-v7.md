# ObServe changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2022-04-20 17:38.

## Version [7.6.13](https://gitlab.com/ultreiaio/ird-observe/-/milestones/223)

**Closed at 2022-05-02.**

### Download
* [Application (observe-7.6.13.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.13/observe-7.6.13.zip)
* [Serveur (observe-7.6.13.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.13/observe-7.6.13.war)

### Issues
* [[Type::Evolution 2309]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2309) **Problème de quadrant** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.12](https://gitlab.com/ultreiaio/ird-observe/-/milestones/218)

**Closed at 2022-04-30.**

### Download
* [Application (observe-7.6.12.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.12/observe-7.6.12.zip)
* [Serveur (observe-7.6.12.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.12/observe-7.6.12.war)

### Issues
* [[Type::Anomalie 1635]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1635) **StomachFullness** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
* [[Type::Anomalie 2152]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2152) **Boutons de cadrage sur la carte 7.6.11** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
* [[Type::Anomalie 2177]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2177) **Référentiel / Systèmes Observés / &#39;Voir toutes les utilisations de ce référentiel&#39; ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
* [[Type::Anomalie 2200]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2200) **Petit défaut de mise en page sur 7.6.11** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
* [[Type::Anomalie 2214]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2214) **Impossible de créer une base centrale vierge, quelque soit la méthode appliquée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
* [[Type::Anomalie 2303]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2303) **Besoin d&#39;aide sur une base à intégrer rapidement** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
* [[Type::Anomalie 2307]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2307) **Impossible d&#39;exporter toutes les marées d&#39;une centrale en local en 7.6.11** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
* [[Type::Evolution 2209]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2209) **Mise à jour de dépendences** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
* [[Type::Evolution 2306]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2306) **Amélioration de la synchronisation simple de référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
* [[Type::Tâche 2305]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2305) **Souci d&#39;import de la base AZTI dans cette base centrale** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.11](https://gitlab.com/ultreiaio/ird-observe/-/milestones/214)

**Closed at 2022-01-06.**

### Download
* [Application (observe-7.6.11.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.11/observe-7.6.11.zip)
* [Serveur (observe-7.6.11.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.11/observe-7.6.11.war)

### Issues
  * [[Type::Evolution 2151]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2151) **Update to log4j 2.27.1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [7.6.10](https://gitlab.com/ultreiaio/ird-observe/-/milestones/213)

**Closed at 2021-12-24.**

### Download
* [Application (observe-7.6.10.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.10/observe-7.6.10.zip)
* [Serveur (observe-7.6.10.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.10/observe-7.6.10.war)

### Issues
  * [[Type::Evolution 2148]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2148) **Suppression des paramètres de configuration sur le serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [7.6.9](https://gitlab.com/ultreiaio/ird-observe/-/milestones/212)

**Closed at 2021-12-19.**

### Download
* [Application (observe-7.6.9.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.9/observe-7.6.9.zip)
* [Serveur (observe-7.6.9.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.9/observe-7.6.9.war)

### Issues
  * [[Type::Evolution 2146]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2146) **update log4j to 2.17.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [7.6.8](https://gitlab.com/ultreiaio/ird-observe/-/milestones/180)

**Closed at 2021-12-17.**


### Issues
  * [[Type::Anomalie 1356]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1356) **[CALCULS] Une marée fait planter la procédure de calcul en mode serveur uniquement** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1784]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1784) **Problème sur la gestion des référentiels manquant lors d&#39;un import** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 2012]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2012) **L&#39;écran de gestion du référentiel Port n&#39;enregistre pas les coordonnées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 2143]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2143) **Mise à jour de librairies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 2144]](https://gitlab.com/ultreiaio/ird-observe/-/issues/2144) **Amélioration configuration serveur minimale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [7.6.7](https://gitlab.com/ultreiaio/ird-observe/-/milestones/174)

**Closed at 2021-01-20.**

### Download
* [Application (observe-7.6.7.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.7/observe-7.6.7.zip)
* [Serveur (observe-7.6.7.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.7/observe-7.6.7.war)

### Issues
  * [[Type::Anomalie 1755]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1755) **Problème d&#39;enregistrement sur avançon depuis le formulaire capture** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1756]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1756) **Problème dans la gestion des onglets du formulaire &#39;Avançon&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1757]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1757) **Souci de gestion sur 12h du champ &#39;Temps depuis déclenchement&#39;** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1625]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1625) **[LL][Observations] Horodatage hook timers par défaut** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Evolution 1754]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1754) **Découplage case &#39;Horloge&#39; et champ &#39;Horodatage de montée à bord&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.6](https://gitlab.com/ultreiaio/ird-observe/-/milestones/170)

**Closed at 2020-11-01.**

### Download
* [Application (observe-7.6.6.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.6/observe-7.6.6.zip)
* [Serveur (observe-7.6.6.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.6/observe-7.6.6.war)

### Issues
  * [[Type::Anomalie 1624]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1624) **[LL] Perte des données saisies &quot;catchhealthstatus&quot; et &quot;discardhealthstatus&quot; dans table &quot;catch&quot; depuis le passage à 7.6.2** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1631]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1631) **[PS] Champ &quot;type de mensuration&quot; ne se met pas à jour** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)

## Version [7.6.5](https://gitlab.com/ultreiaio/ird-observe/-/milestones/169)

**Closed at 2020-08-10.**

### Download
* [Application (observe-7.6.5.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.5/observe-7.6.5.zip)
* [Serveur (observe-7.6.5.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.5/observe-7.6.5.war)

### Issues
  * [[Type::Anomalie 927]](https://gitlab.com/ultreiaio/ird-observe/-/issues/927) **Penser à tester le comportement des large objects sur la V7** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 942]](https://gitlab.com/ultreiaio/ird-observe/-/issues/942) **Plantage base en sortie de l&#39;outil de traduction** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1584]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1584) **[UI] Correction des libellés des tables de référence du schéma observe.longline dans Observe** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1585]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1585) **Listes à choix multiples et mode permissif sur référentiels désactivés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1587]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1587) **traductions manquantes (UI validation)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 887]](https://gitlab.com/ultreiaio/ird-observe/-/issues/887) **Ajouter une fonction de suppression en masse de marées/routes/activités** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1583]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1583) **[PS] Contrôle de la saisie des espèces dans faune accessoire par systèmes observés (cas requin baleine)** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)

## Version [7.6.4](https://gitlab.com/ultreiaio/ird-observe/-/milestones/166)

**Closed at 2020-07-08.**

### Download
* [Application (observe-7.6.4.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.4/observe-7.6.4.zip)
* [Serveur (observe-7.6.4.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.4/observe-7.6.4.war)

### Issues
  * [[Type::Anomalie 1533]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1533) **Deux comportements étranges sur l&#39;assistant tableaux de synthèse** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1552]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1552) **[PS][Observations] Activité : contrôle trop strict** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1558]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1558) **[LL] Onglet Composition détaillée / Détail avançon / Hameçon et appât non consultable** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1561]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1561) **[7.6.3] Rappel d&#39;espèce / catégorie** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1562]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1562) **[7.6.3][PS] Les thonidés sont disponibles dans la liste déroulante du form Espèces accessoires, alors qu&#39;ils ne sont pas dans la display liste correspondante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1563]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1563) **Correction des composants graphiques numériques et sélection** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1567]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1567) **les programmes ne sont pas triés dans le même ordre dans l&#39;arbre de navigation et dans la formulaire liste des marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1568]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1568) **le reset reste actif sur les éditeurs de nombres même si ceci ne sont pas actif** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1569]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1569) **le reset des combo box ne fonctionne pas bien (alors que l&#39;action associée Ctrl+D est ok)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1571]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1571) **Problème de traduction sur les validations taille/poids des espèces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1572]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1572) **Deux coquilles dans les validations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1573]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1573) **Revoir la gestion des validateurs taille-poids d&#39;espèces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1575]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1575) **[PS] Pratiques de remises à l&#39;eau : le mode de libération n&#39;est pas supprimé si on change d&#39;espèce** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1564]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1564) **[PS] Pratiques de remise à l&#39;eau, cétacés non trouvés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1565]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1565) **Pouvoir changer l&#39;espèce d&#39;un enregistrement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1529]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1529) **Requêtes de synthèse PS** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1574]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1574) **Amélioration des logs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [7.6.3](https://gitlab.com/ultreiaio/ird-observe/-/milestones/165)

**Closed at 2020-06-22.**

### Download
* [Application (observe-7.6.3.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.3/observe-7.6.3.zip)
* [Serveur (observe-7.6.3.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.3/observe-7.6.3.war)

### Issues
  * [[Type::Anomalie 1494]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1494) **LL / Opération de pêche / Composition globale / Avançons / Longueur bas de ligne : ce champ était facultatif et est devenu obligatoire** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1530]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1530) **[LL] Activité, Capture, Enregistreur de profondeur Le raccourci clavier MAJ+TAB (focus arrière) ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1528]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1528) **[PS] Rappel de la dernière espèce utilisée dans la saisie des échantillons non cible** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)

## Version [7.6.2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/164)

**Closed at 2020-05-26.**

### Download
* [Application (observe-7.6.2.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.2/observe-7.6.2.zip)
* [Serveur (observe-7.6.2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.2/observe-7.6.2.war)

### Issues
  * [[Type::Anomalie 1492]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1492) **Migration des trip.comment vers trip.homeid incorrecte** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/163)

**Closed at 2020-05-23.**

### Download
* [Application (observe-7.6.1.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.1/observe-7.6.1.zip)
* [Serveur (observe-7.6.1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.1/observe-7.6.1.war)

### Issues
  * [[Type::Evolution 1490]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1490) **Nom et version de l&#39;application Web** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.6.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/162)

**Closed at 2020-05-21.**

### Download
* [Application (observe-7.6.0.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.0/observe-7.6.0.zip)
* [Serveur (observe-7.6.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.6.0/observe-7.6.0.war)

### Issues
  * [[Type::Anomalie 1271]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1271) **[SYNCHRO SIMPLE] Le référentiel SPECIESGROUP_SPECIESGROUPRELEASEMODE n&#39;est pas mis à jour** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1432]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1432) **[SYNCHRO MAREES] L&#39;assistant demande un remplacement de code manquant, même si celui-ci n&#39;est pas utilisé par les données** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1485]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1485) **Version de base dans tms_version** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1487]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1487) **Ne pas sauvegarder automatiquement la base locale lors d&#39;une action longue** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1482]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1482) **La suppression d&#39;une marée pourrait être permise même si le formulaire n&#39;est pas parfaitement conforme** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1484]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1484) **Synchro simple des listes d&#39;espèce** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.5.2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/160)

**Closed at 2020-05-10.**

### Download
* [Application (observe-7.5.2.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.5.2/observe-7.5.2.zip)
* [Serveur (observe-7.5.2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.5.2/observe-7.5.2.war)

### Issues
  * [[Type::Anomalie 1473]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1473) **[PS] Affichage altéré/tronqué en 7.5.1 (régression)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1463]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1463) **Comportement sidérant d&#39;H2, a.k.a Amélioration de la lisibilité des erreurs rencontrées dans les logs lors de la consilidation de données ^^** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1472]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1472) **Report V8 → v7** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1474]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1474) **UI référentiel liste d&#39;affichage d&#39;espèces non classée** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)

## Version [7.5.1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/159)

**Closed at 2020-04-14.**

### Download
* [Application (observe-7.5.1.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.5.1/observe-7.5.1.zip)
* [Serveur (observe-7.5.1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.5.1/observe-7.5.1.war)

### Issues
  * [[Type::Anomalie 1461]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1461) **Mise à jour simple de référentiel échoue en v7.5.0** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1462]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1462) **Accumulation de fichiers** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.5.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/158)

**Closed at 2020-04-01.**

### Download
* [Application (observe-7.5.0.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.5.0/observe-7.5.0.zip)
* [Serveur (observe-7.5.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.5.0/observe-7.5.0.war)

### Issues
  * [[Type::Anomalie 1100]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1100) **Le calcul des vitesses a eu un raté après avoir fait usage de la fonction &quot;Voulez-vous créer l&#39;activité de fin de veille ?&quot;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1436]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1436) **Erreur lors de manipulations sur les caractéristiques d&#39;équipements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1437]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1437) **Erreur sur changement de caractéristique d&#39;équipement de type de donnée différent** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1457]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1457) **Mauvais affichage d&#39;une marée dans l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1458]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1458) **Correction de la migration 7.4.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1456]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1456) **Réusinage de la table transmittingbuoy** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1459]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1459) **Ajouter l&#39;icone de l&#39;application lors du lancement de l&#39;application en mode autre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1460]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1460) **[REFERENTIEL BALISES] Ajout d&#39;un champ &#39;technology&#39;** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)

## Version [7.4.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/156)

**Closed at 2020-03-20.**

### Download
* [Application (observe-7.4.0.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.4.0/observe-7.4.0.zip)
* [Serveur (observe-7.4.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.4.0/observe-7.4.0.war)

### Issues
  * [[Type::Anomalie 1430]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1430) **[PS][FOB] L&#39;algo indique &#39;aucune donnée modifiée&#39;, alors qu&#39;il y a eu modification** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1434]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1434) **[EXPORT CARTE] Le nom de fichier est généré sans l&#39;extension PNG** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1435]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1435) **Petit problème sur déplacement d&#39;activité** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1441]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1441) **[SYNCHRO AVANCEE] Double connexion à la même base** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1442]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1442) **[SYNCHRO AVANCEE] Sens droite vers gauche non disponible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1445]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1445) **[Synchro Avancée] Certaines actions ne sont plus présentes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1329]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1329) **Possibilité de décider, lors de la mise à jour d&#39;une référence, des champs que l&#39;on veut voir pris en compte dans la synchronisation** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1431]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1431) **Homogénéisation des arbres de sélection dans la syncrho de référentiel avancée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1433]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1433) **[LL] Contrôle sur nombre de baskets trop limité** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1438]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1438) **[V7-V8] Ajustement du modèle** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1455]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1455) **[PS OBSERVATIONS] Sur le formulaire Objet/Balises ajouter un champ &quot;bateau propriétaire&quot;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.3.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/153)

**Closed at 2020-01-27.**

### Download
* [Application (observe-7.3.0.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.3.0/observe-7.3.0.zip)
* [Serveur (observe-7.3.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.3.0/observe-7.3.0.war)

### Issues
  * [[Type::Anomalie 1390]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1390) **[PS][V7] Contrôle sur champ cuve** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1406]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1406) **[LL] Un contrôle sur le form observations/captures ne se déclenche pas bien** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1407]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1407) **[LL] Problème de rafraishissement de champs sur Observations LL/Captures/Avançon** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1409]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1409) **[LL] Bug enregistrement hook timer data** (Thanks to Pascal Cauquil) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1410]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1410) **Panneau de navigation / tout collapser -&gt; 1er programme toujours ouvert** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1411]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1411) **Consomme énormément de processeur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1414]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1414) **[PS] Après calcul, des FOBS dont l&#39;opération est &#39;retrait&#39; ont un type simplifié au départ non nul** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1429]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1429) **Il existe des ObjectMaterial avec des standardCode à vide** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1328]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1328) **Possibilité de forcer le topiaid sur tous les écrans de création des référentiels** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1334]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1334) **[PS FOB] Ajouter une action FOB suivant...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1344]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1344) **[REFERENTIEL][FOB] Présentation référentiel matériaux** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1345]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1345) **[PS] Sur calée, supprimer les champs booléens &quot;Tuna discard?&quot; et &quot;Fauna discard?&quot;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1408]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1408) **[LL] Renommage libellé &quot;Instrumentée&quot; sur formulaire opération de pêche LL** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Evolution 1412]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1412) **[PS] Amélioration de l&#39;algorithme de type d&#39;objet simplifié CECOFAD** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1424]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1424) **Amélioration des actions liées aux référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1408]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1408) **[LL] Renommage libellé &quot;Instrumentée&quot; sur formulaire opération de pêche LL** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Tâche 1410]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1410) **Panneau de navigation / tout collapser -&gt; 1er programme toujours ouvert** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)

## Version [7.2.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/148)

**Closed at 2019-09-12.**

### Download
* [Application (observe-7.2.0.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.2.0/observe-7.2.0.zip)
* [Serveur (observe-7.2.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.2.0/observe-7.2.0.war)

### Issues
  * [[Type::Anomalie 1373]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1373) **[LL] Une marée provoque un dépassement mémoire et/ou a un souci de commentaires trop longs** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1374]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1374) **[SYNCHRO AVANCEE] Une requête SQL échoue sur mise à jour d&#39;une espèce** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.1.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/147)

**Closed at 2019-08-03.**

### Download
* [Application (observe-7.1.0.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.1.0/observe-7.1.0.zip)
* [Serveur (observe-7.1.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.1.0/observe-7.1.0.war)

### Issues
  * [[Type::Anomalie 1350]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1350) **[REF] Consolidation du réferentiel common.LengthWeightParameter** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1352]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1352) **Problème synchro avancée référentiel** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1355]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1355) **[REF] Erreur sur suppression d&#39;espèce, appel de table inexistante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1357]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1357) **Impossible de modifier le champs Species.wormsId** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1367]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1367) **[PS] Une espèce sans catégorie apparaît à tord dans le formulaire thons capturés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1351]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1351) **[REF] Consolidation du réferentiel common.LengthLengthParameter** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1353]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1353) **Consolidation des champs techniques en base** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)

## Version [7.0.8](https://gitlab.com/ultreiaio/ird-observe/-/milestones/145)

**Closed at 2019-07-06.**

### Download
* [Application (observe-7.0.8.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.8/observe-7.0.8.zip)
* [Serveur (observe-7.0.8.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.8/observe-7.0.8.war)

### Issues
  * [[Type::Anomalie 1310]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1310) **On ne peut pas supprimer certains référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1325]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1325) **Une modification sur le référentiel n&#39;active pas le bouton &#39;Enregistrer&#39;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1331]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1331) **Assistant création de base PG échoue** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1336]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1336) **Clés de référentiel non traduite ou nom des clés invalide** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1338]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1338) **[REFERENTIEL] Impossible de modifier le parent d&#39;un matériau** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1339]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1339) **[REFERENTIEL] La validation sur les Objets flottant matériaux ne fonctionnent plus :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1340]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1340) **On ne peut pas sauvegarder une configuration d&#39;un serveur distant en mode admin** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1145]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1145) **L&#39;assistant migration via serveur pourrait refuser de continuer si l&#39;utilisateur n&#39;est pas assez acrédité** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1323]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1323) **[PS][FAD] Gestion visuelle de la hiérarchie des matériaux sélectionnée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1335]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1335) **[UI REFERENTIEL] Liste des relations taille-poids et taille-taille** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1342]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1342) **[FOB] Découpler la gestion de la hiérarchie FOB des codes métier** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.0.7](https://gitlab.com/ultreiaio/ird-observe/-/milestones/140)

**Closed at 2019-05-13.**

### Download
* [Application (observe-7.0.7.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.7/observe-7.0.7.zip)
* [Serveur (observe-7.0.7.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.7/observe-7.0.7.war)

### Issues
  * [[Type::Anomalie 1270]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1270) **[SYNCHRO SIMPLE] Une requête DELETE échoue pour cause de violation de contrainte FK** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.0.6](https://gitlab.com/ultreiaio/ird-observe/-/milestones/137)

**Closed at 2019-05-06.**

### Download
* [Application (observe-7.0.6.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.6/observe-7.0.6.zip)
* [Serveur (observe-7.0.6.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.6/observe-7.0.6.war)

### Issues
  * [[Type::Anomalie 1202]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1202) **[UI] Listes déroulantes : ergonomie** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1240]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1240) **On trip forms we should not be able to display map&#39;s tab in create mode** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1241]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1241) **Libellés manquants sur formulaire édition espèces** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1242]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1242) **Incohérence &quot;type de mensuration&quot; dans formulaire &quot;echantillons&quot;** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1243]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1243) **Sauvegarde hors de .observe/backup plus possible** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1244]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1244) **Bug requête tableau synthèse DCP** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1268]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1268) **[PS][FOB] Le calcul du type simplifié et des propriétés maillant/biodégradable ne se fait pas en passant l&#39;algorithme de calcul** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)

## Version [7.0.5](https://gitlab.com/ultreiaio/ird-observe/-/milestones/136)

**Closed at 2019-02-20.**

### Download
* [Application (observe-7.0.5.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.5/observe-7.0.5.zip)
* [Serveur (observe-7.0.5.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.5/observe-7.0.5.war)

### Issues
  * [[Type::Anomalie 1232]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1232) **Equipement bateau, bug sur suppression de caractéristique** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1234]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1234) **[PS] Formulaire FOB : les contrôles de saisie ne sont pas appliqués** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1236]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1236) **[LL] Composition globale : non contrôle de la somme des proportions** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.0.4](https://gitlab.com/ultreiaio/ird-observe/-/milestones/135)

**Closed at 2019-02-18.**

### Download
* [Application (observe-7.0.4.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.4/observe-7.0.4.zip)
* [Serveur (observe-7.0.4.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.4/observe-7.0.4.war)

### Issues
  * [[Type::Anomalie 1219]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1219) **[PS] Inversion de libellé** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1221]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1221) **[PS] Le formulaire bonnes pratiques de remise à l&#39;eau ne s&#39;ouvre pas** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1222]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1222) **[PS] La suppression d&#39;éléments dans Calée/Estimation du banc ne fonctionne pas** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1224]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1224) **A la ré ouverture de l&#39;UI, l&#39;affichage est partiel** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1228]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1228) **L&#39;asssitant sauvegarde a un comportement étonnant** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1230]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1230) **[UI] Des interlignes intempestifs s&#39;imiscent entre les éléments de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1233]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1233) **L&#39;enregistrement des caractéristiques ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1220]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1220) **[PS] Libellé échantillon faune associée** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1227]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1227) **Extension des sauvegardes** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1229]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1229) **Ergonomie assistant sauvegarde** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)

## Version [7.0.3](https://gitlab.com/ultreiaio/ird-observe/-/milestones/134)

**Closed at 2019-02-08.**

### Download
* [Application (observe-7.0.3.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.3/observe-7.0.3.zip)
* [Serveur (observe-7.0.3.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.3/observe-7.0.3.war)

### Issues
  * [[Type::Anomalie 1216]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1216) **Problème avec le calcul sur certaines marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1217]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1217) **Problème avec le calcul sur certaines marées 2** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1218]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1218) **Perte des tooltips sur les données calculées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1214]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1214) **[UI] Le slider pourrait défiler de façon automatique** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.0.2](https://gitlab.com/ultreiaio/ird-observe/-/milestones/132)

**Closed at 2019-02-06.**

### Download
* [Application (observe-7.0.2.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.2/observe-7.0.2.zip)
* [Serveur (observe-7.0.2.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.2/observe-7.0.2.war)

### Issues
  * [[Type::Anomalie 1184]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1184) **Problème d&#39;ouverture d&#39;une sauvegarde 5.4** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1193]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1193) **[UI REFERNETIEL] Personne : impossible d&#39;afficher les éléments liés à une personne** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1196]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1196) **Tentative d&#39;ouverture d&#39;une base avec des droits &quot;référentiel&quot; échoue** (Thanks to ) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1197]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1197) **Impossible de voir les utilisations de référentiel si connecté en mode referentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1201]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1201) **[PS] bug calcul des données** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1205]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1205) **Bases v5 qu&#39;on ne peut pas ouvrir avec la v7** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1206]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1206) **Bug observe connexion via webservice** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 1209]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1209) **La migration v3 vers v7 ne passe pas (fichier mal localisé)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1210]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1210) **[LL - Observation Capture] L&#39;onglet Brancheline est accessible même si aucune donnée n&#39;est sélectionée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1207]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1207) **Le mode plein écran est disponible (?) et pose des problèmes : le retirer** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.0.1](https://gitlab.com/ultreiaio/ird-observe/-/milestones/131)

**Closed at 2019-01-02.**

### Download
* [Application (observe-7.0.1.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.1/observe-7.0.1.zip)
* [Serveur (observe-7.0.1.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.1/observe-7.0.1.war)

### Issues
  * [[Type::Anomalie 1186]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1186) **Les informations sur la source de données ne peuvent pa être affichées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1187]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1187) **[PS] Marées non visibles dans l&#39;arbre après insertion** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1189]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1189) **Déplacement de marée intempestif dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1188]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1188) **Un type d&#39;objet flottant (28) n&#39;est pas pris en compte par la migration** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1190]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1190) **[PS][FOB] Champs whenarriving/whenleaving** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.0.0](https://gitlab.com/ultreiaio/ird-observe/-/milestones/130)

**Closed at 2018-11-26.**

### Download
* [Application (observe-7.0.0.zip)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.0/observe-7.0.0.zip)
* [Serveur (observe-7.0.0.war)](https://repo1.maven.org/maven2/fr/ird/observe/observe/7.0.0/observe-7.0.0.war)

### Issues
  * [[Type::Anomalie 1163]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1163) **Le script postgresql/update.bat ne démarre pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1168]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1168) **[FOB] Les valeurs des caractéristiques/matériaux désactivés ne sont pas affichées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1169]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1169) **[FOB] Affichage bizarre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1174]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1174) **[PS] Libération faune accessoire -&gt; Exception** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1179]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1179) **[SYNCHRO AVANCEE MAREE] La recopie de marées échoue** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1180]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1180) **Le démarrage du service web 7.0.0 provoque cette erreur sous Tomcat** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1170]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1170) **Mise à jour des objectmaterial** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1171]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1171) **Après insertion de #1170** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1173]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1173) **Spanish translations** (Thanks to Tony CHEMIT) (Reported by Loures Ramos Alonso)
  * [[Type::Evolution 1175]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1175) **[PS][FOB] Permettre NULL sur les propriétés biodegradable et nonentangling, ainsi que sur les déductions** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1176]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1176) **Propriétés biodegradable et nonEntangling non présentes sur l&#39;éditeur de ObjectMaterial** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1160]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1160) **Translate application** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Tâche 1161]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1161) **Versions à migrer** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [7.0-RC-9](https://gitlab.com/ultreiaio/ird-observe/-/milestones/106)

**Closed at *In progress*.**


### Issues
No issue.

## Version [7.0-RC-7](https://gitlab.com/ultreiaio/ird-observe/-/milestones/101)

**Closed at *In progress*.**


### Issues
No issue.

## Version [7.0-RC-6](https://gitlab.com/ultreiaio/ird-observe/-/milestones/100)

**Closed at *In progress*.**


### Issues
No issue.

## Version [7.0-RC](https://gitlab.com/ultreiaio/ird-observe/-/milestones/92)

**Closed at 2018-10-21.**


### Issues
  * [[Type::Anomalie 157]](https://gitlab.com/ultreiaio/ird-observe/-/issues/157) **Problèmes d&#39;affichage UNICODE des erreurs de tentative de connection sous Windows** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 407]](https://gitlab.com/ultreiaio/ird-observe/-/issues/407) **[SECURITE] Problème d&#39;initialisation des droits avec l&#39;assistant sécurité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 510]](https://gitlab.com/ultreiaio/ird-observe/-/issues/510) **[SECURITE] L&#39;assistant sécurité semble ne pas donner correctement l&#39;accès au schéma public** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 529]](https://gitlab.com/ultreiaio/ird-observe/-/issues/529) **[LL] Problème sur l&#39;ordre des sections/baskets dans le schéma de palangre selon le tri** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 547]](https://gitlab.com/ultreiaio/ird-observe/-/issues/547) **[PS] Les écrans captures enregistrent parfois des captures sans code espèce associé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 586]](https://gitlab.com/ultreiaio/ird-observe/-/issues/586) **[LL] Commentaire marée : contrôle d&#39;UI et taille du champ en base sont incohérents** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 636]](https://gitlab.com/ultreiaio/ird-observe/-/issues/636) **[Encodage] Corruption des caractères spéciaux en bases locales** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 646]](https://gitlab.com/ultreiaio/ird-observe/-/issues/646) **La fonctionnalité «Accéder à la marée» (ou route ou activité) ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 647]](https://gitlab.com/ultreiaio/ird-observe/-/issues/647) **La session ouverte n&#39;est pas sauvegardée dans la configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 648]](https://gitlab.com/ultreiaio/ird-observe/-/issues/648) **L&#39;action «Annuler» sur l&#39;édition d&#39;un référentiel affiche un formulaire vide et non pas le formulaire du réferentiel en cours d&#39;édition** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 649]](https://gitlab.com/ultreiaio/ird-observe/-/issues/649) **Mauvaise resitution de l&#39;information «observé» ou «calculé» sur l&#39;écran des échantillons** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 689]](https://gitlab.com/ultreiaio/ird-observe/-/issues/689) **Affichage des heures sur l&#39;écran de synchro avancée du référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 696]](https://gitlab.com/ultreiaio/ird-observe/-/issues/696) **La boîte Informations sur la source n&#39;affiche plus l&#39;URL de la base** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 705]](https://gitlab.com/ultreiaio/ird-observe/-/issues/705) **La bascule entre GUI ES et EN a planté** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 708]](https://gitlab.com/ultreiaio/ird-observe/-/issues/708) **le calendrier reste en anglais** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 710]](https://gitlab.com/ultreiaio/ird-observe/-/issues/710) **L&#39;application reste en espagnol après redémarrage** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 718]](https://gitlab.com/ultreiaio/ird-observe/-/issues/718) **Assistant sécurité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 749]](https://gitlab.com/ultreiaio/ird-observe/-/issues/749) **Problème de fonctionnement de la fenêtre d&#39;erreur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 752]](https://gitlab.com/ultreiaio/ird-observe/-/issues/752) **Problème à la première ouverture de la v6, lorsqu&#39;une base v5 est présente dans /db** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 754]](https://gitlab.com/ultreiaio/ird-observe/-/issues/754) **Le raccourci F6 semble ne pas fonctionner** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 759]](https://gitlab.com/ultreiaio/ird-observe/-/issues/759) **Perte des messages de validations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 761]](https://gitlab.com/ultreiaio/ird-observe/-/issues/761) **Sur les doubles listes les boutons du mileu n&#39;ont plus d&#39;icones** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 762]](https://gitlab.com/ultreiaio/ird-observe/-/issues/762) **Possibilité d&#39;insérer un capteur alors que son type n&#39;a pas été renseigné** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 763]](https://gitlab.com/ultreiaio/ird-observe/-/issues/763) **Dans certaisn référentiels, les doubles listes ne prennent pas toute la place disponible à cet effet** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 764]](https://gitlab.com/ultreiaio/ird-observe/-/issues/764) **Impossible de lancer l&#39;assistant migration** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 778]](https://gitlab.com/ultreiaio/ird-observe/-/issues/778) **Impossible d&#39;enregistrer une connexion dans les préférences depuis les interfaces d&#39;administration obtuna** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 779]](https://gitlab.com/ultreiaio/ird-observe/-/issues/779) **Problème de migration de base de v5 vers v6 sur observe_longline.sizemeasuretype** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 781]](https://gitlab.com/ultreiaio/ird-observe/-/issues/781) **Sur les opérations (synchro, ...) on ne récupère plus les informations de sécurité de la base locale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 789]](https://gitlab.com/ultreiaio/ird-observe/-/issues/789) **Régression dur l&#39;écran de configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 797]](https://gitlab.com/ultreiaio/ird-observe/-/issues/797) **lors de l&#39;ouverture d&#39;une base, l&#39;écran de la liste des marées n&#39;est pas bien décoré** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 798]](https://gitlab.com/ultreiaio/ird-observe/-/issues/798) **Les formulaires ne sont pas bien organisé lors de leur ouverture** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 807]](https://gitlab.com/ultreiaio/ird-observe/-/issues/807) **Problème de chargement de l&#39;écran de référentiel FloatingObjectMaterial** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 812]](https://gitlab.com/ultreiaio/ird-observe/-/issues/812) **L&#39;écran Senne Activité indique qu&#39;il n&#39;y a pas de dcp alors que j&#39;en ai saisi un** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 815]](https://gitlab.com/ultreiaio/ird-observe/-/issues/815) **Problème lors de la création d&#39;une base** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 816]](https://gitlab.com/ultreiaio/ird-observe/-/issues/816) **[WIDOWS] Il arrive que l&#39;application ne parvienne pas à supprimer le fichier i18n.zip** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 817]](https://gitlab.com/ultreiaio/ird-observe/-/issues/817) **Libellés manquants** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 819]](https://gitlab.com/ultreiaio/ird-observe/-/issues/819) **Lorsque l&#39;on crée un nouvel objet FOB, le formulaire devrait toujours se positionner sur le premier onglet** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 820]](https://gitlab.com/ultreiaio/ird-observe/-/issues/820) **Caractéristiques calculées des objets FOB** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 822]](https://gitlab.com/ultreiaio/ird-observe/-/issues/822) **Raccourcis sur assistant de connexion** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 823]](https://gitlab.com/ultreiaio/ird-observe/-/issues/823) **Impossible d&#39;enregistrer une DCP** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 826]](https://gitlab.com/ultreiaio/ird-observe/-/issues/826) **L&#39;enregistrement d&#39;un FOB plante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 827]](https://gitlab.com/ultreiaio/ird-observe/-/issues/827) **L&#39;accès aux écrans PS targetSample conservés et targetSample rejetés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 831]](https://gitlab.com/ultreiaio/ird-observe/-/issues/831) **Mauvais repositionnement de l&#39;écran lors du changement du type d&#39;opération FOB** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 834]](https://gitlab.com/ultreiaio/ird-observe/-/issues/834) **Impossible de valider le référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 835]](https://gitlab.com/ultreiaio/ird-observe/-/issues/835) **[PS][Echantillons][Faune accessoires] Report du type de mensuration standard** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 838]](https://gitlab.com/ultreiaio/ird-observe/-/issues/838) **Problème des caractères accentués, le retour** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 839]](https://gitlab.com/ultreiaio/ird-observe/-/issues/839) **Problème sur l&#39;assistant de sélection de marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 840]](https://gitlab.com/ultreiaio/ird-observe/-/issues/840) **L&#39;assistant sauver la base (central vers local) plante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 841]](https://gitlab.com/ultreiaio/ird-observe/-/issues/841) **La synchro avancée de référentiel ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 843]](https://gitlab.com/ultreiaio/ird-observe/-/issues/843) **[PS] Problème de validation sur captures cibles conservées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 846]](https://gitlab.com/ultreiaio/ird-observe/-/issues/846) **Curiosité d&#39;affichage dans l&#39;assistant de sélection des marées** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 851]](https://gitlab.com/ultreiaio/ird-observe/-/issues/851) **Certains raccourcis claviers peuvent être déclanché alors que cela ne devrait pas être possible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 856]](https://gitlab.com/ultreiaio/ird-observe/-/issues/856) **Impossible de recopier les valeurs des matérials via les actions en bas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 857]](https://gitlab.com/ultreiaio/ird-observe/-/issues/857) **Lors de la création d&#39;un dcp, les matériaux ne sont pas validés si on y touche pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 858]](https://gitlab.com/ultreiaio/ird-observe/-/issues/858) **Impossible d&#39;accéder au menu configuration lorsqu&#39;une base est chargée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 863]](https://gitlab.com/ultreiaio/ird-observe/-/issues/863) **Connexion distante impossible après changement de paramètre &quot;Version REST&quot; dans la configuration** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 864]](https://gitlab.com/ultreiaio/ird-observe/-/issues/864) **L&#39;assistant de migration de la RC6 ne démarre pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 867]](https://gitlab.com/ultreiaio/ird-observe/-/issues/867) **Problème affichage navire et observateur dans arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 869]](https://gitlab.com/ultreiaio/ird-observe/-/issues/869) **[LL][PS] Clic sur Equipement (du bateau) dans l&#39;arbre provoque une exception** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 870]](https://gitlab.com/ultreiaio/ird-observe/-/issues/870) **La connexion à la base centrale ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 871]](https://gitlab.com/ultreiaio/ird-observe/-/issues/871) **La connexion à un serveur distant ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 877]](https://gitlab.com/ultreiaio/ird-observe/-/issues/877) **La migration RC9 plante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 881]](https://gitlab.com/ultreiaio/ird-observe/-/issues/881) **[PS] Libellés des formulaires sous Calée à mettre à jour** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Anomalie 884]](https://gitlab.com/ultreiaio/ird-observe/-/issues/884) **[PS] Sasie d&#39;un échantillon - présélection du type de mesure par défaut** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 925]](https://gitlab.com/ultreiaio/ird-observe/-/issues/925) **Lorsque la migration de la base PG échoue, elle se retrouve corrompue** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 934]](https://gitlab.com/ultreiaio/ird-observe/-/issues/934) **Corriger le problème de validation d&#39;une chaine de caractère dans NonTargetCatchRelease** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 936]](https://gitlab.com/ultreiaio/ird-observe/-/issues/936) **Problème lors du changement de langue (les référentiels ne sont pas changé dans la bonne langue, mais ok après un redemarrage)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 945]](https://gitlab.com/ultreiaio/ird-observe/-/issues/945) **Migration RC12 PostgreSQL -&gt; OutOfMemoryError: Java heap space** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 946]](https://gitlab.com/ultreiaio/ird-observe/-/issues/946) **Migration RC12b PostgreSQL -&gt; NullPointerException: Can&#39;t find script /db/migration/6.0/00_fix_trigger-PG.sql** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 947]](https://gitlab.com/ultreiaio/ird-observe/-/issues/947) **Migration RC12c -&gt; OutOfMemory : Java Heap Space** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 953]](https://gitlab.com/ultreiaio/ird-observe/-/issues/953) **Des racourcis clavier interfèrent avec l&#39;autocomplétion des listes déroulantes** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 954]](https://gitlab.com/ultreiaio/ird-observe/-/issues/954) **[PS] Sur marée PS, les bateaux présentés sont les palangriers** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 961]](https://gitlab.com/ultreiaio/ird-observe/-/issues/961) **L&#39;assistant calcul de donnés demande abusivement (ou pas) de configurer une connexion** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 962]](https://gitlab.com/ultreiaio/ird-observe/-/issues/962) **[REFERENTIEL] Lien entre lengthlengthparameter et species ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 963]](https://gitlab.com/ultreiaio/ird-observe/-/issues/963) **Barre de progression sur calcul de données** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 964]](https://gitlab.com/ultreiaio/ird-observe/-/issues/964) **[REFERENTIEL] L&#39;écran du référentiel taille-poids plante à son ouverture** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 965]](https://gitlab.com/ultreiaio/ird-observe/-/issues/965) **[REFERENTIEL] L&#39;écran du référentiel taille-taille plante** (Thanks to ) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 966]](https://gitlab.com/ultreiaio/ird-observe/-/issues/966) **[REFERENTIEL] Une erreur s&#39;est glissée dans le référentiel SizeMeasureType** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 967]](https://gitlab.com/ultreiaio/ird-observe/-/issues/967) **[PS] Conversion taille-taille non fonctionnelle** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 968]](https://gitlab.com/ultreiaio/ird-observe/-/issues/968) **[PS] Formulaire DCP / matériaux / champs numériques** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 972]](https://gitlab.com/ultreiaio/ird-observe/-/issues/972) **L&#39;algo de calcul de données plante sur la base attachée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 976]](https://gitlab.com/ultreiaio/ird-observe/-/issues/976) **Ecran de configuration du référentiel taille-taille : libellés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 977]](https://gitlab.com/ultreiaio/ird-observe/-/issues/977) **Ecran de configuration du référentiel taille-taille : petit souci de validation** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 979]](https://gitlab.com/ultreiaio/ird-observe/-/issues/979) **Création de marée : focus forcé sur la carto** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 980]](https://gitlab.com/ultreiaio/ird-observe/-/issues/980) **Equipement bateau : première frappe inhibée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 981]](https://gitlab.com/ultreiaio/ird-observe/-/issues/981) **Validation écran activité : erreur pas visible sur l&#39;onglet caractéristiques** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 984]](https://gitlab.com/ultreiaio/ird-observe/-/issues/984) **Gestion référentiel taille-taille : message de validation douteux** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 986]](https://gitlab.com/ultreiaio/ird-observe/-/issues/986) **La conversion taille-taille ne fonctionne toujours pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 990]](https://gitlab.com/ultreiaio/ird-observe/-/issues/990) **Migration de sql.gz V5 HS** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 992]](https://gitlab.com/ultreiaio/ird-observe/-/issues/992) **Revoir la clé d&#39;unicité sur le référentiel taille-taille** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 996]](https://gitlab.com/ultreiaio/ird-observe/-/issues/996) **Création automatique de l&#39;activité de fin de veille non fonctionnelle** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 997]](https://gitlab.com/ultreiaio/ird-observe/-/issues/997) **[PS] Bycatch : la gomme ne réinitialise pas correctement le type de taille par défaut** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 998]](https://gitlab.com/ultreiaio/ird-observe/-/issues/998) **[PS] Forçage du focus lors de la création d&#39;une nouvelle actiité** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1014]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1014) **[LL] Opération de pêche -&gt; erreur de validation non remontée sur le libellé de l&#39;onglet** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1025]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1025) **[LL][SERVER] Sur section Equipement, le racourcis Nouveau (F4) ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1029]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1029) **[LL][SERVER] Sur section Capture, le racourcis Nouveau (F4) ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1039]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1039) **[PS] Un classement de liste déroulante est inversé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1042]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1042) **La synchro de marée LL et PS entre bases centrales en mode serveur ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1043]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1043) **Synchro avancée de marées : sélection de marées impossible à droite ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1044]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1044) **Ne peut-on plus choisir entre synchro uni et bidirectionnelle ?** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1045]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1045) **La synchro de marées PS en connexion directe n&#39;aboutit jamais (LL ok)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1046]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1046) **[PS][LL][DIRECT][SERVEUR] La suppression de marées ne fonctionne pas bien** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1047]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1047) **La synchro référentiel simple ne fonctionne pas, l&#39;ordre des requetes à executer n&#39;est pas bon** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1048]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1048) **La synchronisation des référentiels pourrait ne pas fonctionner sur la suppression de certains référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1049]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1049) **[LL] La suppression d&#39;une marée en mode serveur provoque une exception** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1050]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1050) **[PS][LL] La synchro avancée de marées plante** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1051]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1051) **Suppression de lignes de référentiel : liste de ceux qui plantent avec erreur de type Dto** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1052]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1052) **Suppression de lignes de référentiel : liste de ceux qui plantent avec erreurs spécifiques** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1053]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1053) **Problème de socket en RC18 en connexion serveur directe à Tomcat (pas de HTTPD)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1054]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1054) **La synchro avancée du référentiel listes d&#39;espèces provoque une exception** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1055]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1055) **Suppression d&#39;une référence &quot;Equipement bateau&quot; &quot;Radio BLU&quot; : erreur (RC20-SNAPSHOT)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1057]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1057) **En mode serveur, la boîte d&#39;information sur la connexion n&#39;indique pas la BD (alias) à laquelle on est connecté** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1058]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1058) **L&#39;UI de synchro de référentiels ne présente plus les éléments à synchroniser qu&#39;un par un** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1059]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1059) **L&#39;UI de remplacement de code de référentiel dépasse parfois les limites de l&#39;écran** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1060]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1060) **La suppression d&#39;une espèce avec remplacement de code échoue** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1061]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1061) **La suppression d&#39;océan avec remplacement de code pose problème** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1062]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1062) **La suppression d&#39;un sexe avec remplacement de code échoue** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1063]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1063) **[SYNCHRO] Problème avec la synchro avancée d&#39;un programme** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1068]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1068) **[LL] Après migration l&#39;UI v7 n&#39;affiche plus qu&#39;une seule des captures de chaque opération de pêche** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1070]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1070) **[LL] L&#39;UI RC20 ne parvient plus à ouvrir les captures LL depuis un base centrale en mode serveur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1072]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1072) **L&#39;action de sauvegarde de la base locale est lancée automatiquement si on sélectionne une source connue** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1073]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1073) **La mise à jour des référentiels ne fonctionne pas si il y a des référentiels obsolètes!** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1074]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1074) **Après avoir réussit à effectuer les remplacements de référentiels obsolètes, la base source est toujours sauvegardé!** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1075]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1075) **[PS] La création automatique d&#39;une calée ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1081]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1081) **Les rapports ne fonctionnent pas très bien** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Anomalie 1101]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1101) **[RAPPORTS] Les 4 requêtes sur espèces cibles ne fonctionnent pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1136]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1136) **[PS][FOB] Plusieurs mappings anciens types FAD / Nouveaux matériaux sont erronés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1141]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1141) **Connexion HTTP probablement périmée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1142]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1142) **Problème d&#39;affichage dans l&#39;UI référentiel relations taille-poids** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1143]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1143) **[PS] Calée / Captures accessoires : le numéro de cuve n&#39;est plus éditable après le premier enregistrement** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1148]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1148) **Filtres de l&#39;arbre de navigation : raccourcis inopérants** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1151]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1151) **Lacune de migration sur liste des systèmes observés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1157]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1157) **Le redémarrage imposé par l&#39;utilisation de l&#39;UI traduction empêche ensuite toute reconnexion** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Anomalie 1158]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1158) **[PS] Lorsque l&#39;on enregistre le formulaire FOB depuis le 2d onglet, le focus revient sur le 1er onglet** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 67]](https://gitlab.com/ultreiaio/ird-observe/-/issues/67) **Réglage par défaut des proportions des volets de la fenêtre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 81]](https://gitlab.com/ultreiaio/ird-observe/-/issues/81) **Action/calculer les données : sélection de la marée par défaut** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 299]](https://gitlab.com/ultreiaio/ird-observe/-/issues/299) **[LL] Formulaire Captures : Tableaux tailles et poids - ajouter des fonctions clic droit** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 306]](https://gitlab.com/ultreiaio/ird-observe/-/issues/306) **[LL] Ajout d&#39;un champs sur l&#39;écran Composition &gt; Onglet Avançons** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 352]](https://gitlab.com/ultreiaio/ird-observe/-/issues/352) **Permettre la MAJ du référentiel à partir d&#39;un sql.gz** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 353]](https://gitlab.com/ultreiaio/ird-observe/-/issues/353) **[PS] Supprimer le champ floattingobject.supportvesselname** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 354]](https://gitlab.com/ultreiaio/ird-observe/-/issues/354) **[PS] Ajouter un champ &quot;Numéro de cuve&quot;, sur le formulaire des captures accessoires** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 357]](https://gitlab.com/ultreiaio/ird-observe/-/issues/357) **[PS] Améliorer le contrôle des tailles/poids min/max d&#39;espèces (doubler le warning d&#39;une erreur)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 395]](https://gitlab.com/ultreiaio/ird-observe/-/issues/395) **TMA** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 412]](https://gitlab.com/ultreiaio/ird-observe/-/issues/412) **[ObServeLL] Classement des listes déroulantes section/panier/avançon du formulaire capture** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 442]](https://gitlab.com/ultreiaio/ird-observe/-/issues/442) **Dans les listes d&#39;espèces, ne plus montrer l&#39;ancien code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 443]](https://gitlab.com/ultreiaio/ird-observe/-/issues/443) **[PS] Unicité des enregistrements des rejets de thons à modifier** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 445]](https://gitlab.com/ultreiaio/ird-observe/-/issues/445) **[PS] Supprimer le champ calee/nom du supply** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 515]](https://gitlab.com/ultreiaio/ird-observe/-/issues/515) **[LL][PS] Possibilité de saisie directe au clavier des dates/heures sans passer par le calendrier** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 516]](https://gitlab.com/ultreiaio/ird-observe/-/issues/516) **[WIDGET POSITIONS] absence de message d&#39;erreur quand horodatage erronné (certains cas)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 520]](https://gitlab.com/ultreiaio/ird-observe/-/issues/520) **[LL] Nombre de coupures inconnu : cas problématique** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 523]](https://gitlab.com/ultreiaio/ird-observe/-/issues/523) **[LL] numérotation des captures** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 531]](https://gitlab.com/ultreiaio/ird-observe/-/issues/531) **Rajouter un champ Armateur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 535]](https://gitlab.com/ultreiaio/ird-observe/-/issues/535) **Lors d&#39;un import sql.gz vers la base centrale, possibilité d&#39;ajouter dans la base centrale les références manquantes** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 551]](https://gitlab.com/ultreiaio/ird-observe/-/issues/551) **[REFERENTIEL] Relations taille-poids - Ajout d&#39;un champ texte &#39;source&#39;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 557]](https://gitlab.com/ultreiaio/ird-observe/-/issues/557) **Retirer le champ relatif à l&#39;utilisation du sonar pendant la calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 558]](https://gitlab.com/ultreiaio/ird-observe/-/issues/558) **[PS] Ajout d&#39;un champ sur les captures** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 559]](https://gitlab.com/ultreiaio/ird-observe/-/issues/559) **[PS] Limiter l&#39;usage du formulaire DCP** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 560]](https://gitlab.com/ultreiaio/ird-observe/-/issues/560) **[PS] Gestion plus intelligente des champs Devenir et Raison du rejet** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 569]](https://gitlab.com/ultreiaio/ird-observe/-/issues/569) **[PS] Renommer 2 champs dans nontargetlength** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 570]](https://gitlab.com/ultreiaio/ird-observe/-/issues/570) **[PS] Saisie des échantillons de cible : modification de la gestion des types de taille** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 571]](https://gitlab.com/ultreiaio/ird-observe/-/issues/571) **[REFERENTIEL] Référentiel Espèces - gestion du type de mesure par défaut** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 575]](https://gitlab.com/ultreiaio/ird-observe/-/issues/575) **[TABLEAUX DE SYNTHESE] Nouvelle requête sur les équipements bateau** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 577]](https://gitlab.com/ultreiaio/ird-observe/-/issues/577) **[PS] Implanter les nouveaux formulaires DCP** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 581]](https://gitlab.com/ultreiaio/ird-observe/-/issues/581) **Gestion plus perfectionnée des calculs taille-poids** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 584]](https://gitlab.com/ultreiaio/ird-observe/-/issues/584) **Améliorer la gestion du focus et ajouter des racourcis clavier** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 587]](https://gitlab.com/ultreiaio/ird-observe/-/issues/587) **[REFERENTIEL] Ajouter des colonnes dans Vessel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 612]](https://gitlab.com/ultreiaio/ird-observe/-/issues/612) **[PS] Implanter le nouveau formulaire de remise à l&#39;eau des espèces sensibles** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 626]](https://gitlab.com/ultreiaio/ird-observe/-/issues/626) **Assistant de connexion** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 631]](https://gitlab.com/ultreiaio/ird-observe/-/issues/631) **Ajout de champs liés au marquage d&#39;individus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 632]](https://gitlab.com/ultreiaio/ird-observe/-/issues/632) **Ajout du champ Sexe sur les échantillons de cible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 637]](https://gitlab.com/ultreiaio/ird-observe/-/issues/637) **[PS] Sur les échantillons de non cible, ajouter un champ Devenir** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 642]](https://gitlab.com/ultreiaio/ird-observe/-/issues/642) **Limiter le Zoom- de la carte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 653]](https://gitlab.com/ultreiaio/ird-observe/-/issues/653) **Ajout d&#39;un clic droit/ajouter sur le tableau LL de définition des templates de section** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 695]](https://gitlab.com/ultreiaio/ird-observe/-/issues/695) **Ajout d&#39;une information visuelle sur les écrans de gestion avancée du référentiel et des données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 703]](https://gitlab.com/ultreiaio/ird-observe/-/issues/703) **Vérifier la parfaite adéquation version serveur / version client** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 742]](https://gitlab.com/ultreiaio/ird-observe/-/issues/742) **Report from version 5.x** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 750]](https://gitlab.com/ultreiaio/ird-observe/-/issues/750) **Filtres d&#39;affichage de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 751]](https://gitlab.com/ultreiaio/ird-observe/-/issues/751) **Filtrage de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 753]](https://gitlab.com/ultreiaio/ird-observe/-/issues/753) **[UI] Mise en page de la fenêtre d&#39;application** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 755]](https://gitlab.com/ultreiaio/ird-observe/-/issues/755) **[UI] Sortie au clavier d&#39;une zone de texte** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 756]](https://gitlab.com/ultreiaio/ird-observe/-/issues/756) **[UI] Code couleur** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 757]](https://gitlab.com/ultreiaio/ird-observe/-/issues/757) **Ne pas faire une sauvegarde de la base si aucune action de modification n&#39;a été effectuée sur la base locale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 760]](https://gitlab.com/ultreiaio/ird-observe/-/issues/760) **Suppression des autres raccourcis claiviers lorsqu&#39;un raccourci global existe pour la même action** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 765]](https://gitlab.com/ultreiaio/ird-observe/-/issues/765) **Mode de connexion par défaut** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 766]](https://gitlab.com/ultreiaio/ird-observe/-/issues/766) **Dans les UI proposant le mode de connexion, faire remonter le mode service web au dessus de connexion directe** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 771]](https://gitlab.com/ultreiaio/ird-observe/-/issues/771) **DCP Remove fields ‘Supply’ and ‘Time at sea’** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 773]](https://gitlab.com/ultreiaio/ird-observe/-/issues/773) **Synchro avancée référentiel/marées, écran inutile** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 785]](https://gitlab.com/ultreiaio/ird-observe/-/issues/785) **Ajout d&#39;un icone manquant pour la sauvegarde d&#39;un formulaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 786]](https://gitlab.com/ultreiaio/ird-observe/-/issues/786) **Utilisation d&#39;un modèle textuel pour le developpement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 787]](https://gitlab.com/ultreiaio/ird-observe/-/issues/787) **Gestion des objets flottants de référence** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 788]](https://gitlab.com/ultreiaio/ird-observe/-/issues/788) **Ajout d&#39;un traducteur dans l&#39;application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 791]](https://gitlab.com/ultreiaio/ird-observe/-/issues/791) **Utilisation des templates dans le répertoires des ressources** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 794]](https://gitlab.com/ultreiaio/ird-observe/-/issues/794) **Mieux filtrer les espèces sensibles** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 796]](https://gitlab.com/ultreiaio/ird-observe/-/issues/796) **optimiser la détection des objets flottants de référence** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 799]](https://gitlab.com/ultreiaio/ird-observe/-/issues/799) **Amélioration de l&#39;organisation lors des action Open/Close** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 800]](https://gitlab.com/ultreiaio/ird-observe/-/issues/800) **Ajouter une option pour ne pas demander une correspondance exacte entre la version du client et serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 801]](https://gitlab.com/ultreiaio/ird-observe/-/issues/801) **Revoir la gestion d&#39;erreur sur l&#39;api Rest** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 802]](https://gitlab.com/ultreiaio/ird-observe/-/issues/802) **Assainissement des API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 805]](https://gitlab.com/ultreiaio/ird-observe/-/issues/805) **Speed up connection opening** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 809]](https://gitlab.com/ultreiaio/ird-observe/-/issues/809) **Correction du nom d&#39;un champs sur FloatingObject + ne pas pouvoir éditer l&#39;opération de l&#39;objet est mode modification** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 810]](https://gitlab.com/ultreiaio/ird-observe/-/issues/810) **Ajout de la consolidation des dcp** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 811]](https://gitlab.com/ultreiaio/ird-observe/-/issues/811) **Changement du nom des fichiers de configuration du client et serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 814]](https://gitlab.com/ultreiaio/ird-observe/-/issues/814) **Migration sur les dcp du ObjectType en ObjectMaterialParts** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 818]](https://gitlab.com/ultreiaio/ird-observe/-/issues/818) **Ecran faune sensible rejetée, gestion de la date du rejet** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 821]](https://gitlab.com/ultreiaio/ird-observe/-/issues/821) **La barre de progression en % ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 825]](https://gitlab.com/ultreiaio/ird-observe/-/issues/825) **Le mode d&#39;acquisition sur les espèces sensibles est Par individu par défaut** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 828]](https://gitlab.com/ultreiaio/ird-observe/-/issues/828) **Amélioration de l&#39;onglet des ObjectMaterials** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 830]](https://gitlab.com/ultreiaio/ird-observe/-/issues/830) **[FOB] Précisions sur la validation** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 836]](https://gitlab.com/ultreiaio/ird-observe/-/issues/836) **[PS][Echantillons] Afficher le type de mensuration utilisé dans le tableau d&#39;empilement des échantillons** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 842]](https://gitlab.com/ultreiaio/ird-observe/-/issues/842) **Pouvoir ajouter une connexion directe ou serveur depuis l&#39;écran de gestion des connexions** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 847]](https://gitlab.com/ultreiaio/ird-observe/-/issues/847) **Dans les écrans de sélections de marées (export, backup, ...) il faudrait supprimer les programmes sans marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 848]](https://gitlab.com/ultreiaio/ird-observe/-/issues/848) **Amélioration de la disposition des racourcis claviers dans les menus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 849]](https://gitlab.com/ultreiaio/ird-observe/-/issues/849) **Déplacer le menu traduire dans le menu Aide** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 850]](https://gitlab.com/ultreiaio/ird-observe/-/issues/850) **Revoir le menu de configuration de la langue** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 852]](https://gitlab.com/ultreiaio/ird-observe/-/issues/852) **Trier les connexions sauvegardées + ajout raccourcis clavier** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 853]](https://gitlab.com/ultreiaio/ird-observe/-/issues/853) **Améliorer le tri des types** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 854]](https://gitlab.com/ultreiaio/ird-observe/-/issues/854) **Permettre l&#39;utilisation de la molette de la souris même sur des zones protégées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 859]](https://gitlab.com/ultreiaio/ird-observe/-/issues/859) **Ajout du zoom +/- sur les cartes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 860]](https://gitlab.com/ultreiaio/ird-observe/-/issues/860) **Ajout d&#39;un nouvel éditeur de température qui permet les conversions °C/°F** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 866]](https://gitlab.com/ultreiaio/ird-observe/-/issues/866) **[PS] Sur le formulaire Echantillon non cible, afficher dans le tableau la propriété &quot;devenir&quot;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 873]](https://gitlab.com/ultreiaio/ird-observe/-/issues/873) **Renommage de certaines tables du modèle palangre en les suffixant par Obs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 874]](https://gitlab.com/ultreiaio/ird-observe/-/issues/874) **Déplacer le référentiel Wind dans le schema commun** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 875]](https://gitlab.com/ultreiaio/ird-observe/-/issues/875) **Tranductions du référentiel Wind** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 883]](https://gitlab.com/ultreiaio/ird-observe/-/issues/883) **[PS] Tableaux de captures et échantillons** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 895]](https://gitlab.com/ultreiaio/ird-observe/-/issues/895) **Ajout du référentiel observe_common.DataQuality** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 926]](https://gitlab.com/ultreiaio/ird-observe/-/issues/926) **Améliorations sur le formulaire relâche d&#39;espèce sensible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 929]](https://gitlab.com/ultreiaio/ird-observe/-/issues/929) **Ajout d&#39;un référentiel PS NonTargetCatchReleaseConformity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 930]](https://gitlab.com/ultreiaio/ird-observe/-/issues/930) **Supprimer l&#39;entrée non conforme du SpeciesGroupReleaseMode et toutes les associations indiuites sur les Speciesgroup** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 931]](https://gitlab.com/ultreiaio/ird-observe/-/issues/931) **Mettre les mêmes modes de libération que Whale shark pour le groupe d&#39;espèce Cetaceans.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 932]](https://gitlab.com/ultreiaio/ird-observe/-/issues/932) **Remplacer detectionTime et release time par un référentiel NonTargetCatchReleasingTime** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 933]](https://gitlab.com/ultreiaio/ird-observe/-/issues/933) **Mettre à jour le référentiel NonTargetCatchReleaseStatus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 935]](https://gitlab.com/ultreiaio/ird-observe/-/issues/935) **Revue des FAD material** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 937]](https://gitlab.com/ultreiaio/ird-observe/-/issues/937) **Calculer en temps réel les données calculées sur un FAD lors de la modification de ses materials** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 938]](https://gitlab.com/ultreiaio/ird-observe/-/issues/938) **Ajouter une nouvelle règle si un dcp présent alors le type de banc est obligatoirement Objet (changement de l&#39;algorithme de consolidation)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 941]](https://gitlab.com/ultreiaio/ird-observe/-/issues/941) **Gestion des requins-baleine lors de la migration V7 et dans le fonctionnement régulier de la V7** (Thanks to Pascal Cauquil) (Reported by Pascal Cauquil)
  * [[Type::Evolution 948]](https://gitlab.com/ultreiaio/ird-observe/-/issues/948) **Improve FOB Form** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 955]](https://gitlab.com/ultreiaio/ird-observe/-/issues/955) **[GEAR] Page des caractéristiques d&#39;équipements -&gt; afficher l&#39;unité attendue** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 956]](https://gitlab.com/ultreiaio/ird-observe/-/issues/956) **[GEAR] Page des caractéristiques d&#39;équipements -&gt; ordonancement des caractéristiques** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 957]](https://gitlab.com/ultreiaio/ird-observe/-/issues/957) **Page des caractéristiques d&#39;équipements -&gt; champs texte tronqués à l&#39;affichage** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 960]](https://gitlab.com/ultreiaio/ird-observe/-/issues/960) **Widget température °C/°F : message de validation perfectible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 969]](https://gitlab.com/ultreiaio/ird-observe/-/issues/969) **[PS] Formulaire DCP / matériaux / Couleur de surbrillance** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 971]](https://gitlab.com/ultreiaio/ird-observe/-/issues/971) **[PS] Formulaire DCP / matériaux / recopie de l&#39;état départ &lt;-&gt; arrivée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 973]](https://gitlab.com/ultreiaio/ird-observe/-/issues/973) **Validation système observé / RHN** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 978]](https://gitlab.com/ultreiaio/ird-observe/-/issues/978) **Ecran de configuration du référentiel taille-taille : liste des relations existantes** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 983]](https://gitlab.com/ultreiaio/ird-observe/-/issues/983) **[PS] Placement des champs sur échantillon faune accessoire** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 985]](https://gitlab.com/ultreiaio/ird-observe/-/issues/985) **Gestion référentiel taille-taille : les enregistrements existants ne sont pas modifiables** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 993]](https://gitlab.com/ultreiaio/ird-observe/-/issues/993) **Présentation de la liste des relations taille-taille** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 994]](https://gitlab.com/ultreiaio/ird-observe/-/issues/994) **Présentation de la liste des références taille-poids** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1004]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1004) **[From MGr] FOB -&gt; Add 1.1.1.1.3 – non-visible** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1013]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1013) **Types de mesures par défaut manquants pour 94 espèces dans le référentiel** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1015]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1015) **[LL] Opération de pêche -&gt; validation poids émerillon et snap** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1028]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1028) **[LL] Plus de validation sur snap et émerillon** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1034]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1034) **Be able to use server configuration to perform database management** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1035]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1035) **Use a new improved combo box** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1038]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1038) **[PS] Ajout de racourcis clavier sur le formulaire des systèmes observés** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1041]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1041) **Affichage du nom de base dans les écrans de synchro avancée** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1064]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1064) **[SYNCHRO] La synchro avancée d&#39;une marée nécessitant une référence absente dans la base cible provoque une exception** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1065]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1065) **Ne pas remplacer les associations lors du remplacement d&#39;un référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1080]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1080) **Un contrôle insoupçonné vérifie les températures entre activités** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1082]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1082) **Revoir le rapport utilisation des dcp** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1099]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1099) **Petit renommage de libellé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1102]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1102) **Extension de fichier du rapport de validation** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1140]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1140) **Création de profils de connexions : cosmétique** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1144]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1144) **Report of nice features from v8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Evolution 1146]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1146) **Script update-obstuna.* pourrait être renommé...** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1149]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1149) **Migration des types d&#39;objets : consigner l&#39;ancien type dans le champ commentaire** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1153]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1153) **Les messages d&#39;erreur sur référentiels désactivés rendent difficile l&#39;édition des marées qui en usent** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1154]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1154) **[PS] Place du formulaire &quot;libération&quot; dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1156]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1156) **UI de traduction : recherche de libellé** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Evolution 1159]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1159) **[DROPDOWN LIST] TAB pourrait combiner les actions de ENTER+TAB** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 652]](https://gitlab.com/ultreiaio/ird-observe/-/issues/652) **Corriger le référentiel Espèce** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 767]](https://gitlab.com/ultreiaio/ird-observe/-/issues/767) **Mise à jour des dépendances** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 776]](https://gitlab.com/ultreiaio/ird-observe/-/issues/776) **Remplacer le client REST proxy par des services générés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 780]](https://gitlab.com/ultreiaio/ird-observe/-/issues/780) **Suppression de la table observe_longline.sizemeasuretype ?** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 790]](https://gitlab.com/ultreiaio/ird-observe/-/issues/790) **Mise à jour des librairies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 792]](https://gitlab.com/ultreiaio/ird-observe/-/issues/792) **Consolidation des traductions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 793]](https://gitlab.com/ultreiaio/ird-observe/-/issues/793) **Review design of ReferenceSetDefinition and FormRequestDefinition** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 803]](https://gitlab.com/ultreiaio/ird-observe/-/issues/803) **Re-add SupportVesselName on FloatingObject** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 804]](https://gitlab.com/ultreiaio/ird-observe/-/issues/804) **Remove application-swing-decoration module and move code to services module.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 806]](https://gitlab.com/ultreiaio/ird-observe/-/issues/806) **Review validation layouts** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 808]](https://gitlab.com/ultreiaio/ird-observe/-/issues/808) **Optimisation des traductions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 824]](https://gitlab.com/ultreiaio/ird-observe/-/issues/824) **optimize services usage in client** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 829]](https://gitlab.com/ultreiaio/ird-observe/-/issues/829) **Supprimer 2 champs dans objectmaterial** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 832]](https://gitlab.com/ultreiaio/ird-observe/-/issues/832) **Amélioration du format des logs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 833]](https://gitlab.com/ultreiaio/ird-observe/-/issues/833) **Create a sub project to put frameworks extensions used in ObServe** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 855]](https://gitlab.com/ultreiaio/ird-observe/-/issues/855) **Improve i18n keys** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Type::Tâche 881]](https://gitlab.com/ultreiaio/ird-observe/-/issues/881) **[PS] Libellés des formulaires sous Calée à mettre à jour** (Thanks to Tony CHEMIT) (Reported by Philippe Sabarros)
  * [[Type::Tâche 989]](https://gitlab.com/ultreiaio/ird-observe/-/issues/989) **Requêtes pour insertion préalable des systèmes observés 20, 21 et 22** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Type::Tâche 1033]](https://gitlab.com/ultreiaio/ird-observe/-/issues/1033) **Gestion de référentiel avancé - tests** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
