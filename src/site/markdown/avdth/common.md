# Import des données de marée

Toutes les données sont déversées dans le paquetage **ps_common**.

### Table MAREE

Cette table est intégralement déversée dans la table *Trip*.

|n°        | nom           | destination                      |note|
|:---      | :---          | :---                             |:---|
| MAREE_01 | C_BAT         | Trip.vessel                      |    |         
| MAREE_02 | D_DBQ         | Trip.endDate                     |    |     
| MAREE_03 | C_PORT_DBQ    | Trip.landingHarbour              |    |       
| MAREE_04 | V_TEMPS_M     | Trip.timeAtSea                   |    |       
| MAREE_05 | V_TEMPS_P     | Trip.fishingTime                 |    |       
| MAREE_06 | V_POIDS_DBQ   | Trip.landingTotalWeight          |    |
| MAREE_07 | V_POIDS_FP    | Trip.localMarketTotalWeight      |    |
| MAREE_08 | F_ENQ         | Trip.logbookAcquisitionStatus    | [1](#n_1)|        
| MAREE_09 | C_PORT_DEP    | Trip.departureHarbour            |    |       
| MAREE_10 | D_DEPART      | Trip.startDate                   |    |
| MAREE_11 | F_CAL_VID     | Trip.landingWellContentStatus    | [2](#n_2)|       
| MAREE_12 | V_LOCH        | Trip.loch                        |    |
| MAREE_13 | C_ZONE_GEO    | Not used                         |    |
| MAREE_14 | L_COM_M       | Trip.generalComment              |    |
| MAREE_15 | C_ID_ERS      | Trip.ersId                       |    |
| MAREE_16 | C_TOPIAID     | ???                              |    |
| MAREE_17 | F_CAL_VID_DPT | Trip.departureWellContentStatus  | [2](#n_2) |

* Note 1 <a name="n_1"></a>

```F_ENQ > 0 ? AcquisitionStatus(1) : AcquisitionStatus(99)```

* Note 2 <a name="n_2"></a>

```CODE == 0 ? WellContentStatus(2) : WellContentStatus(CODE)```

[Retour à l'introduction](./index.html)