# Import des données de débarquements

Toutes les données sont déversées dans le paquetage **ps_landing**.

## Table LOT_COM

Table déversée dans la table d'ObServe **Landing**.

| n°         | nom        | destination            | note                     |
|:-----------|:-----------|:-----------------------|:-------------------------|
| LOT_COM_01 | C_BAT      | Landing.trip           | [1](#n_0_1)              | 
| LOT_COM_02 | D_DBQ      | Landing.trip           | [1](#n_0_1)              | 
| LOT_COM_03 | N_LOT      | Landing.homeId         |                          |
| LOT_COM_04 | C_ESP      | Landing.species        | [2](#n_0_2), [5](#n_0_5) | 
| LOT_COM_05 | C_CAT_C    | Landing.weightCategory | [3](#n_0_3)              | 
| LOT_COM_06 | V_POIDS_LC | Landing.weight         |                          |
| LOT_COM_07 | C_DEST     | Landing.destination    | [4](#n_0_4), [5](#n_0_5) |

* Note 1 <a name="n_0_1"></a>

Identifie la marée

* Note 2 <a name="n_0_2"></a>

Voir mapping [REF_SPECIES](./referential.html#REF_SPECIES)

* Note 3 <a name="n_0_3"></a>

Voir mapping [REF_LANDING_WEIGHT_CATEGORY](./referential.html#REF_LANDING_WEIGHT_CATEGORY)

* Note 4 <a name="n_0_4"></a>

Mapping direct sur le code du référentiel **ps_landing.Destination**.

* Note 5 <a name="n_0_5"></a>

On positionne **ps_landing.Landing.fate** en suivant ce code 

**FIXME Voir https://gitlab.com/ultreiaio/ird-observe/-/issues/2959 **

On distingue quatre cas :

1. Si LOT_COM.C_DEST dans ```4 ou 5``` alors on utilise ps_landing.Fate de code 4 (transbordement)
2. Si LOT_COM.C_DEST dans ```18, 28, 40 à 61, 64 à 65)``` alors on utilise ps_landing.Fate de code 3 (vendu au marché local)
2. Sinon si LOT_COM.C_ESP est dans ```8, 800 à 899``` alors on utilise ps_landing.Fate de code 2 (rejeté)
3. Sinon on utilise ps_landing.Fate de code 1 (traité)

[Retour à l'introduction](./index.html)
