# Import des données du marché local

Toutes les données sont déversées dans le paquetage **ps_localmarket**.

### Table FP_LOT

Table déversée dans la table d'ObServe **Batch**

| n°        | nom          | destination     | note        |
|:----------|:-------------|:----------------|:------------|
| FP_LOT_01 | C_BAT        | Batch.trip      | [1](#n_0_1) | 
| FP_LOT_02 | D_DBQ        | Batch.trip      | [1](#n_0_1) | 
| FP_LOT_03 | N_LOT        | Batch.homeId    |             |
| FP_LOT_04 | C_ESP        | Batch.species   | [2](#n_0_2) | 
| FP_LOT_05 | C_COND       | Batch.packaging | [3](#n_0_3) | 
| FP_LOT_06 | N_UNIT       | Batch.count     |             |
| FP_LOT_07 | V_POIDS_PESE | Batch.weight    |             |
| FP_LOT_08 | L_ORIGINE    | Batch.origin    |             |
| FP_LOT_09 | L_COM        | Batch.comment   |             |

* Note 1
### <a name="n_0_1"></a>

Identifie la marée

* Note 2
### <a name="n_0_2"></a>

Voir mapping [REF_SPECIES](./referential.html#REF_SPECIES)

* Note 3
### <a name="n_0_3"></a>

Mapping direct sur le code du référentiel **ps_localmarket.Packaging**.

### Table FP_SONDAGE

Table déversée dans la table d'ObServe **Survey** et **SurveyPart**.

| n°            | nom    | destination           | note        |
|:--------------|:-------|:----------------------|:------------|
| FP_SONDAGE_01 | C_BAT  | Survey.trip           | [1](#n_1_1) | 
| FP_SONDAGE_02 | D_DBQ  | Survey.trip           | [1](#n_1_1) | 
| FP_SONDAGE_03 | N_SOND | Survey.homeId         |             |
| FP_SONDAGE_04 | C_ESP  | SurveyPart.species    | [2](#n_1_2) | 
| FP_SONDAGE_05 | V_PROP | SurveyPart.proportion |             |

* Note 1 <a name="n_1_1"></a>

Identifie la marée

* Note 2 <a name="n_1_2"></a>

Voir mapping [REF_SPECIES](./referential.html#REF_SPECIES)

### Table FP_ECHANTILLON

Table déversée dans la table d'ObServe **Sample**.

| n°                | nom        | destination   | note        |
|:------------------|:-----------|:--------------|:------------|
| FP_ECHANTILLON_01 | C_BAT      | Sample.trip   | [1](#n_2_1) | 
| FP_ECHANTILLON_02 | D_DBQ      | Sample.trip   | [1](#n_2_1) | 
| FP_ECHANTILLON_03 | N_ECH      | Sample.number |             |
| FP_ECHANTILLON_04 | D_ECH      | Sample.date   |             |
| FP_ECHANTILLON_05 | C_BAT_REEL | ???           |             |

* Note 1 <a name="n_2_1"></a>

Identifie la marée

### Table FP_ECH_ESP

Table déversée dans la table d'ObServe **SampleSpecies**.

| n°            | nom      | destination                   | note        |
|:--------------|:---------|:------------------------------|:------------|
| FP_ECH_ESP_01 | C_BAT    | SampleSpecies.sample          | [1](#n_3_1) | 
| FP_ECH_ESP_02 | D_DBQ    | SampleSpecies.sample          | [1](#n_3_1) | 
| FP_ECH_ESP_03 | N_ECH    | SampleSpecies.sample          | [1](#n_3_1) | 
| FP_ECH_ESP_04 | C_ESP    | SampleSpecies.species         | [2](#n_3_2) | 
| FP_ECH_ESP_05 | F_LDLF   | SampleSpecies.sizeMeasureType | [3](#n_3_3) | 
| FP_ECH_ESP_06 | V_NB_MES | SampleSpecies.measureCount    |             |

* Note 1 <a name="n_3_1"></a>

Identifie l'échantillon

* Note 2 <a name="n_3_2"></a>

Voir mapping [REF_SPECIES](./referential.html#REF_SPECIES)

* Note 3 <a name="n_3_3"></a>

Voir mapping [REF_SIZE_MEASURE_TYPE](./referential.html#REF_SIZE_MEASURE_TYPE)

### Table FP_ECH_FREQT

Table déversée dans la table d'ObServe **SampleSpeciesMeasure**.

| n°              | nom    | destination                        | note        |
|:----------------|:-------|:-----------------------------------|:------------|
| FP_ECH_FREQT_01 | C_BAT  | SampleSpeciesMeasure.sampleSpecies | [1](#n_4_1) | 
| FP_ECH_FREQT_02 | D_DBQ  | SampleSpeciesMeasure.sampleSpecies | [1](#n_4_1) | 
| FP_ECH_FREQT_03 | N_ECH  | SampleSpeciesMeasure.sampleSpecies | [1](#n_4_1) | 
| FP_ECH_FREQT_04 | C_ESP  | SampleSpeciesMeasure.sampleSpecies | [1](#n_4_1) | 
| FP_ECH_FREQT_05 | F_LDLF | SampleSpeciesMeasure.sampleSpecies | [1](#n_4_1) | 
| FP_ECH_FREQT_06 | V_LONG | SampleSpeciesMeasure.sizeClass     |             |
| FP_ECH_FREQT_07 | V_EFF  | SampleSpeciesMeasure.count         |             |

* Note 1 <a name="n_4_1"></a>

Identifie l'échantillon espèce

### Table FP_ECH_CUVE

Table déversée dans la table d'ObServe **Sample_Well**.

| n°             | nom        | destination        | note        |
|:---------------|:-----------|:-------------------|:------------|
| FP_ECH_CUVE_01 | C_BAT      | Sample_Well.sample | [1](#n_5_1) | 
| FP_ECH_CUVE_02 | D_DBQ      | Sample_Well.sample | [1](#n_5_1) | 
| FP_ECH_CUVE_03 | N_ECH      | Sample_Well.sample | [1](#n_5_1) | 
| FP_ECH_CUVE_04 | N_CUVE     | Sample_Well.well   | [2](#n_5_2) | 
| FP_ECH_CUVE_05 | C_POSITION | Sample_Well.well   | [2](#n_5_2) | 

* Note 1 <a name="n_5_1"></a>

Identifie l'échantillon

* Note 2 <a name="n_5_2"></a>

Identifiant de cuve

Voir mapping [REF_WELL_ID](./referential.html#REF_WELL_ID)

[Retour à l'introduction](./index.html)