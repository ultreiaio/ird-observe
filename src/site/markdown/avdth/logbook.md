# Import des données du livre de bord

Toutes les données sont déversées dans le paquetage **ps_logbook**.

### Table ACTIVITE

La notion de route n'existe pas dans *AVDTH*.

On groupe donc les activités d'AVDTH par date (*D_ACT*) et on les associe à une route portant sur cette date.

La table est déversée dans plusieurs tables :

* **Route**
* **Activity**
* **Activity_ObservedSystem**
* **FloatingObject**
* **TransmittingBuoy**

| n°          | nom               | destination                                | note          |
|:------------|:------------------|:-------------------------------------------|:--------------|
| ACTIVITE_01 | C_BAT             | Route.topiaId                              |               |
| ACTIVITE_02 | D_DBQ             | Route.topiaId                              |               |
| ACTIVITE_03 | D_ACT             | Route.date                                 |               |
| ACTIVITE_04 | N_ACT             | Activity.number                            |               |
| ACTIVITE_05 | H_ACT             | Activity.time                              | [1](#n_0_1)   | 
| ACTIVITE_06 | M_ACT             | Activity.time                              | [1](#n_0_1)   | 
| ACTIVITE_07 | C_OCEA            | Not used                                   |               |
| ACTIVITE_08 | Q_ACT             | Signe de latitude et longitude             | [2](#n_0_2)   |  
| ACTIVITE_09 | V_LAT             | Activity.latitude                          | [2](#n_0_2)   | 
| ACTIVITE_10 | V_LON             | Activity.longitude                         | [2](#n_0_2)   | 
| ACTIVITE_11 | V_TMER            | Route.timeAtSea                            | [3](#n_0_3)   | 
| ACTIVITE_12 | V_TPEC            | Route.fishingTime                          | [3](#n_0_3)   | 
| ACTIVITE_13 | C_OPERA           | Activity.vesselActivity                    | [4](#n_0_4)   | 
| ACTIVITE_14 | V_NB_OP           | Activity.setCount                          |               |
| ACTIVITE_15 | C_TBANC           | Activity.schoolType                        | [5](#n_0_5)   | 
| ACTIVITE_16 | F_DON_ORG         | Activity.originalDataModified              | [6](#n_0_6)   | 
| ACTIVITE_17 | F_POS_COR         | Activity.positionCorrected                 | [7](#n_0_7)   | 
| ACTIVITE_18 | F_POS_VMS_D       | Activity.vmsDivergent                      | [8](#n_0_8)   | 
| ACTIVITE_19 | F_CUVE_C          | Not used                                   |               |
| ACTIVITE_20 | F_OBS             | Not used                                   |               |
| ACTIVITE_21 | F_EXPERT          | Not used                                   |               |
| ACTIVITE_22 | V_POIDS_CAP       | Activity.totalWeight                       |               |
| ACTIVITE_23 | V_TEMP_S          | Activity.seaSurfaceTemperature             |               |
| ACTIVITE_24 | V_COUR_DIR        | Activity.currentDirection                  |               |
| ACTIVITE_25 | V_COUR_VIT        | Activity.currentSpeed                      |               |
| ACTIVITE_26 | V_VENT_DIR        | Activity.windDirection                     |               |
| ACTIVITE_27 | V_VENT_VIT        | Activity.wind                              | [9](#n_0_9)   | 
| ACTIVITE_28 | C_TYP_OBJET       |                                            | [10](#n_0_10) | 
| ACTIVITE_29 | F_DCP_ECO         | Dcp écologique                             | [10](#n_0_10) |
| ACTIVITE_30 | F_PROP_BALISE     | TransmittingBuoy.transmittingBuoyOwnerShip | [10](#n_0_10) |
| ACTIVITE_31 | C_TYP_BALISE      |                                            | [10](#n_0_10) |
| ACTIVITE_32 | V_ID_BALISE       | TransmittingBuoy.code                      |               |
| ACTIVITE_33 | V_POIDS_ESTIM_DCP | FloatingObject.comment                     | [11](#n_0_11) |
| ACTIVITE_34 | L_COM_A           | Activity.comment                           |               |
| ACTIVITE_35 | C_Z_FPA           | Activity.fpaZone                           | [12](#n_0_12) |


* Note 1 <a name="n_0_1"></a>

Permet de construire l'heure de l'activité

* Note 2 <a name="n_0_2"></a>

Dans ObServe les coordonnées sont signées, on déduit les signes depuis le champs *Q_ACT* quadrant d'AVDTH

* Note 3 <a name="n_0_3"></a>

On effecute la somme pour toutes les activités AVDTH du jour

* Note 4 <a name="n_0_4"></a>

Voir mapping [REF_VESSEL_ACTIVITY_TYPE](./referential.html#REF_VESSEL_ACTIVITY_TYPE)

* Note 5 <a name="n_0_5"></a>

Voir mapping  [REF_SCHOOL_TYPE](./referential.html#REF_SCHOOL_TYPE).

**À noter que cette donnée est renseignée uniquement si l'activité est une activité de pêche ```ACTIVITE_C_OPERA in (0, 1, 2, 14)```** 

* Note 6 <a name="n_0_6"></a>

F_DON_ORG == 0

* Note 7 <a name="n_0_7"></a>

F_POS_COR == 9

* Note 8 <a name="n_0_8"></a>

F_POS_VMS_D == 1

* Note 9 <a name="n_0_9"></a>

Utilisation des bornes minSpeed et maxSpeed sur common.Wind

* Note 10 <a name="n_0_10"></a>

Voir ajout d'un DCP

* Note 11 <a name="n_0_11"></a>

Donnée non persistée dans ObServe, (voir https://gitlab.com/ultreiaio/ird-observe/-/issues/2575#note_1247253501).

* Note 12 <a name="n_0_12"></a>

Mapping sur le code.

### Cas des activités de pêche à partir de ```ACTIVITE.C_OPERA```

Pour les activités de pêche, on rajoute des données sur l'activité pour décrire la calée.

| ACTIVITE.C_OPERA | SetSuccessStatus [1](#n_5_1) | ReasonForNullSet [1](#n_5_1) | SetCount    | SchoolType  |
|-----------------:|-----------------------------:|-----------------------------:|:------------|:------------|
|                0 |                            0 |                            0 | [2](#n_5_2) | [3](#n_5_3) |
|                1 |                            1 |                non renseigné | [2](#n_5_2) | [3](#n_5_3) |
|                2 |                            2 |                non renseigné | [2](#n_5_2) | [3](#n_5_3) |
|   14 [4](#n_5_4) |                            2 |                non renseigné | [2](#n_5_2) | [3](#n_5_3) |

* Note 1 <a name="n_5_1"></a>

On utilise le code du réferentiel ObServe

* Note 2 <a name="n_5_2"></a>

Calculé depuis ```ACTIVITE.V_NB_OP```

* Note 3 <a name="n_5_3"></a>

Calculé depuis ```ACTIVITE.C_TBANC```.

* Note 4 <a name="n_5_4"></a>

Dans ce cas on rajoute aussi le système observé **110**.

### Condition d'ajout d'un DCP

Dans **AVDTH**, il existe trois notions à considéder :

1. ```ACTIVITE.C_TYP_OBJET``` donne le type de DCP (la valeur ```NULL``` ou ```999``` indique pas de DCP déclaré dans **AVDTH**)
2. ```ACTIVITE.C_TYP_BALISE``` donne le type de balise (la valeur ```NULL``` ou ```999``` indique pas de bouée déclarée dans **AVDTH**)
3. Les valeurs ```20,21,22,23,24,25,81``` de la table ```ACT_ASSOC.C_ASSOC``` permettent de définir aussi un DCP (et de renseigner les matériaux associés)

On va alors interdire l'ajout d'un DCP, si aucune des trois notions précédentes n'est satisfaite, en clair on ne peut pas ajouté un DPC si :

1. Pas de DCP déclaré dans **AVDTH**
2. Pas de balise déclarée dans **AVDTH**
3. Pas de système observé décrivant un DCP 

De plus dans **AVDTH**, il existe des DCP déclarés sur des activités qui ne sont pas compatibles avec l'utilisation 
d'un DCP dans ObServe.

On propose tout de même une nouvelle correspondance sur les **vesselActivity** pour pallier à ce défaut.

| ACTIVITE.C_OPERA | ps_common.VesselActivity.code (si pas de DCP) | ps_common.VesselActivity.code (si DCP) |
|:-----------------|:----------------------------------------------|:---------------------------------------|
| 3                | 1                                             | 101                                    |
| 4                | 2                                             | 102                                    |

Les codes ```101 et 102``` sont des clones des VesselActivity de code ```1 et 2``` avec la possibilité d'utiliser 
des DCPs (```VesselActivity.allowFad = TRUE) (introduits via migration de la version **9.1.0**).

Enfin on ne tient pas compte des DCP qui seraient crées uniquement via la notion des systèmes observés si le type 
d'activité n'est pas compatible avec l'ajout de DCP et on **interdit** l'import dans tous les autres cas.

### Ajout d'un DCP

Pour ajouter un DCP on procède de la manière suivante : 

1. calcul de l'opération du DCP (**FloatingObject.objectOperation**)
2. calcul d'un éventuel système observé
3. ajout des matériaux sur le DCP
4. ajout d'une d'éventuelle balise

#### Calcul de FloatingObject.objectOperation

Cela est réalisé via la table de correspondace suivante :

| ACTIVITE.C_OPERA | ps_common.ObjectOperation.code |
|:-----------------|:-------------------------------|
| 0                | 99                             |
| 1                | 99                             |
| 2                | 99                             |
| 3                | 99                             |
| 4                | 99                             |
| 5                | 1                              |
| 6                | 4                              |
| 7                | 99                             |
| 8                | 99                             |
| 9                | 99                             |
| 10               | 99                             |
| 12               | 99                             |
| 13               | 99                             |
| 14               | 99                             |
| 15               | 99                             |
| 22               | 4                              |
| 23               | 1                              |
| 24               | 4                              |
| 25               | 2                              |
| 26               | 2                              |
| 29               | 2                              |
| 30               | 2                              |
| 31               | 8                              |
| 32               | 8                              |
| 33               | 11                             |
| 34               | 99                             |
| 40               | 11                             |
| 41               | 11                             |

#### Ajout d'un éventuel système observé

On distingue un cas particulier pour ajouter un système observé **20 (DCP)** :

1. le DCP n'est pas déclaré dans **AVDTH**
2. et la balise est déclarée dans **AVDTH**
3. et l'opération du DCP n'est pas une mise à l'eau

Dans tous les autres cas, on peut ajouter un système observé en utilisant la table de correspondance suivante : 

| ACTIVITE.C_TYP_OBJET | ps_common.ObservedSystem.code |
|:---------------------|:------------------------------|
| 1                    | 20                            |
| 2                    | 20                            |
| 3                    | 20                            |
| 4                    | 18                            |
| 5                    | 12                            |
| 6                    | 11                            |
| 7                    | 9                             |
| 9                    | 20                            |
| 10                   | 20                            |
| 11                   | 20                            |
| 12                   | 20                            |
| 13                   | 20                            |

#### Ajout des matériaux liés au DCP

La valeur des champs ```FloatingObject.floatingObjectPart.whenArriving``` et ```FloatingObject.floatingObjectPart.whenLeaving``` 
est déduite du référentiel  ```ps_common.ObjectOperation``` et n'est plus récupéré d'**AVDTH**.

##### Matériel Biodégradable

Si le DCP est déclaré dans **AVDTH** et que la valeur de la colonne ```ACTIVITE.F_DCP_ECO``` vaut **2**, on ajoute alors
le matériel **4-1 (Biodegradable materials)**.

##### Matériaux provenant des systèmes observés d'AVDTH

Les systèmes observés d'**AVDTH** permettent de caractériser les matériaux à ajouter au DCP comme indiqué dans le 
tableau suivant :

| ACT_ASSOC.C_ASSOC | ObjectMaterial |
|:------------------|:---------------|
| 20                | FOB            |
| 21                | VNLOG          |
| 22 [1](#n_6_1)    | VNLOG + DFAD   |
| 23                | ALOG           |
| 24 [1](#n_6_1)    | DFAD           |
| 25                | AFAD           |
| 81                | Carrion        |

* Note 1 <a name="n_6_1"></a>

Dans ce cas, on doit créer une balise de type **98** (Balise inconnue ou indéterminée) et ceci **uniquement dans le 
cas où la balise n'est pas déclarée dans AVDTH**.

##### Matériaux ajouté si le DCP est déclaré dans AVDTH

Dans ce cas, on peut ajouter un éventuel matériel en utilisant la table de correspondance suivante :

| ACTIVITE.C_TYP_OBJET | ObjectMaterial |
|:---------------------|:---------------|
| 1                    | AFAD           |
| 2                    | DFAD           |
| 3                    | LOG            |
| 9                    | FOB            |
| 10                   | FALOG          |
| 11                   | HALOG          |
| 12                   | ANLOG          |
| 13                   | VNLOG          |

##### Matériel Bouée émettrice seule

Lorsque **ACTIVITE.C_OPERA** vaut **34**, on ajoute toujours un tel matériel.

Dernier cas permettant d'ajouter un matériel de type **Bouée émettrice seule** au DCP :

1. Le DCP n'est pas déclaré dans **AVDTH**
2. et la balise est déclarée dans **AVDTH**
3. et aucun système observé d'**AVDTH** n'a été trouvé en lien avec les DCP

#### Ajout d'une éventuelle balise sur le DCP

On distingue deux cas :

1. La balise est déclarée dans **AVDTH**
2. La balise n'est pas déclarée dans **AVDTH** mais est ajoutée via les systèmes observés

##### Ajout d'une balise déclarée dans AVDTH

L'opération de la balise est déterminée depuis la table de correspondance suivante :

| ACTIVITE.C_OPERA | TransmittingBuoyOperation |
|:-----------------|:--------------------------|
| 0                | 99                        |
| 1                | 99                        |
| 2                | 99                        |
| 3                | 99                        |
| 4                | 99                        |
| 5                | 3                         |
| 6                | 2                         |
| 14               | 99                        |
| 22               | 99                        |
| 23               | 3                         |
| 24               | 2                         |
| 25               | 3                         |
| 26               | 2                         |
| 29               | 1                         |
| 30               | 99                        |
| 31               | 99                        |
| 32               | 3                         |
| 33               | 4                         |
| 34               | 2                         |
| 40               | 4                         |
| 41               | 5                         |

Si la correspondance n'est pas satisfaite, on bloque alors l'import avec une message explicite.

Pour obtenir le type de la balise, on utilise la table dfe correspondance suivante : 

| **ACTIVITE.C_TYP_BALISE** | **ps_common.TransmittingBuoyType.code** |
|:--------------------------|:----------------------------------------|
| 1                         | 1                                       |
| 2                         | 2                                       |
| 3                         | 3                                       |
| 4                         | 4                                       |
| 5                         | 5                                       |
| 6                         | 6                                       |
| 7                         | 7                                       |
| 8                         | 8                                       |
| 9                         | 9                                       |
| 10                        | 10                                      |
| 11                        | 90                                      |
| 12                        | 21                                      |
| 13                        | 22                                      |
| 14                        | 23                                      |
| 15                        | 24                                      |
| 16                        | 25                                      |
| 17                        | 26                                      |
| 18                        | 27                                      |
| 19                        | 28                                      |
| 20                        | 41                                      |
| 21                        | 42                                      |
| 22                        | 43                                      |
| 23                        | 44                                      |
| 24                        | 45                                      |
| 25                        | 46                                      |
| 26                        | 47                                      |
| 27                        | 100                                     |
| 28                        | 61                                      |
| 29                        | 62                                      |
| 30                        | 63                                      |
| 31                        | 64                                      |
| 32                        | 65                                      |
| 33                        | 66                                      |
| 34                        | 67                                      |
| 35                        | 68                                      |
| 36                        | 69                                      |
| 37                        | 70                                      |
| 38                        | 71                                      |
| 39                        | 3                                       |
| 40                        | 91                                      |
| 98                        | 99                                      |
| 99                        | 99                                      |

Pour renseigner le propriétaire de la balise  ```transmittingBuoyOwnerShip``` (translation de **ACTIVITE.F_PROP_BALISE**)

| **ACTIVITE.F_PROP_BALISE** | **ps_common.TransmittingBuoyOwnerShip.code** |
|:---------------------------|:---------------------------------------------|
| 1                          | 0                                            |
| 2                          | 3                                            |
| 3                          | null                                         |

Pour renseigner le code de la balise, on recopie la valeur de la colonne ```ACTIVITE.V_ID_BALISE```.

Si l'opération (dans **ObServe**) de la balise est **11**, on recopie alors les coordonnées de l'activité.

##### Ajout d'une balise non déclarée dans AVDTH, mais via les systèmes observés

On ajoute ici une balise avec les caractéristiques suivantes :

* **TransmittingBuoyType** 98 (Balise inconnue ou indéterminée)
* **TransmittingBuoyOperation** 1 (Visite)

**FIXME** Ne devrait pas plutôt mettre une opération à **99** (qui a été introduite en 9.1) ?

### Table ACT_ASSOC

| n°           | nom     | destination                            | note        |
|:-------------|:--------|:---------------------------------------|:------------|
| ACT_ASSOC_01 | C_BAT   | Activity_ObservedSystem.activity       | [1](#n_1_1) | 
| ACT_ASSOC_02 | D_DBQ   | Activity_ObservedSystem.activity       | [1](#n_1_1) | 
| ACT_ASSOC_03 | D_ACT   | Activity_ObservedSystem.activity       | [1](#n_1_1) | 
| ACT_ASSOC_04 | N_ACT   | Activity_ObservedSystem.activity       | [1](#n_1_1) | 
| ACT_ASSOC_05 | N_ASSOC | Not used                               |             |
| ACT_ASSOC_06 | C_ASSOC | Activity_ObservedSystem.ObservedSystem | [2](#n_1_2) | 

* Note 1 <a name="n_1_1"></a>

Identifie l'activité

* Note 2 <a name="n_1_2"></a>

Voir mapping [REF_OBSERVED_SYSTEM](./referential.html#REF_OBSERVED_SYSTEM)

### Table CAPT_ELEM

| n°           | nom          | destination          | note        |
|:-------------|:-------------|:---------------------|:------------|
| CAPT_ELEM_01 | C_BAT        | Catch.activity       | [1](#n_2_1) | 
| CAPT_ELEM_02 | D_DBQ        | Catch.activity       | [1](#n_2_1) | 
| CAPT_ELEM_03 | D_ACT        | Catch.activity       | [1](#n_2_1) | 
| CAPT_ELEM_04 | N_ACT        | Catch.activity       | [1](#n_2_1) | 
| CAPT_ELEM_05 | N_CAPT       | Catch.homeId         |             |
| CAPT_ELEM_06 | C_ESP        | Catch.species        | [2](#n_2_2) | 
| CAPT_ELEM_07 | C_CAT_T      | Catch.weightCategory | [3](#n_2_3) | 
| CAPT_ELEM_08 | V_POIDS_CAPT | Catch.weight         |             |

* Note 1 <a name="n_2_1"></a>

Identifie l'activité

* Note 2 <a name="n_2_2"></a>

Voir mapping [REF_SPECIES](./referential.html#REF_SPECIES)

* Note 3 <a name="n_2_3"></a>

Voir mapping [REF_CATCH_WEIGHT_CATEGORY](./referential.html#REF_CATCH_WEIGHT_CATEGORY)

### Table CUVE

En version **9.1**, on a revu la modélisation du plan de cuve.

Le plan de cuve est désormais représenté par 3 tables :

1. Well
2. WellActivity
3. WellActivitySpecies

La table **CUVE** est reversée dans la première table.

| n°      | nom        | destination                                       | note        |
|:--------|:-----------|:--------------------------------------------------|:------------|
| CUVE_01 | C_BAT      | Well.trip                                         | [1](#n_3_1) | 
| CUVE_02 | D_DBQ      | Well.trip                                         | [1](#n_3_1) | 
| CUVE_03 | N_CUVE     | Well.well                                         | [2](#n_3_2) | 
| CUVE_04 | F_POS_CUVE | Well.well                                         | [2](#n_3_2) | 
| CUVE_05 | C_DEST     | Well.wellSamplingConformity et wellSamplingStatus | [3](#n_3_3) | 

* Note 1 <a name="n_3_1"></a>

Identifie la marée

* Note 2 <a name="n_3_2"></a>

Identifiant de cuve

Voir mapping [REF_WELL_ID](./referential.html#REF_WELL_ID)

* Note 3 <a name="n_3_3"></a>

Voir mapping [REF_WELL_SAMPLING_CONFORMITY](./referential.html#REF_WELL_SAMPLING_CONFORMITY) et
[REF_WELL_SAMPLING_STATUS](./referential.html#REF_WELL_SAMPLING_STATUS)

### Table CUVE_CALEE

Cette table définit les données du plan de cuve et est déversée dans les tables **WellActivity** et **WellActivitySpecies**.

| n°            | nom         | destination                          | note        |
|:--------------|:------------|:-------------------------------------|:------------|
| CUVE_CALEE_01 | C_BAT       | WellActivity.well                    | [1](#n_4_1) | 
| CUVE_CALEE_02 | D_DBQ       | WellActivity.well                    | [1](#n_4_1) | 
| CUVE_CALEE_03 | N_CUVE      | WellActivity.well                    | [1](#n_4_1) | 
| CUVE_CALEE_04 | F_POS_CUVE  | WellActivity.well                    | [1](#n_4_1) | 
| CUVE_CALEE_05 | N_CALESP    | WellActivitySpecies.setSpeciesNumber |             |
| CUVE_CALEE_06 | D_ACT       | WellActivity.activity                | [2](#n_4_2) | 
| CUVE_CALEE_07 | N_ACT       | WellActivity.activity                | [2](#n_4_2) | 
| CUVE_CALEE_08 | C_ESP       | WellActivitySpecies.species          | [3](#n_4_3) | 
| CUVE_CALEE_09 | C_CAT_POIDS | WellActivitySpecies.weightCategory   | [4](#n_4_4) | 
| CUVE_CALEE_10 | V_POIDS     | WellActivitySpecies.weight           |             |
| CUVE_CALEE_11 | V_NB        | WellActivitySpecies.count            |             |

* Note 1 <a name="n_4_1"></a>

Identifie la cuve (donc l'identifiant de l'entrée dans **Well**).

* Note 2 <a name="n_4_2"></a>

Identifie l'activité associée (qui correspond exactement à une entrée dans **WellActivity**).

* Note 3 <a name="n_4_3"></a>

Voir mapping [REF_SPECIES](./referential.html#REF_SPECIES)

* Note 4 <a name="n_4_4"></a>

Voir mapping [REF_WELL_SAMPLE_WEIGHT_CATEGORY](./referential.html#REF_WELL_SAMPLE_WEIGHT_CATEGORY)

[Retour à l'introduction](./index.html)