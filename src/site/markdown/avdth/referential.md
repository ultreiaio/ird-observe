# Définition de toutes les correspondances de référentiel

### REF_WELL_ID

Mapping d'un identifiant de cuve.

Dans **AVDTH**, on utilise deux colonnes pour identifier une cuve.

Dans **ObServe**, cela est remplacé par un identifiant de type chaine de caractères de format :

```Numéro de cuve + Position de cuve (A, B ou T)```

Le mapping sur la position est réalisé ainsi :

| **Position dans AVDTH** | **Position dans ObServe** |
|:------------------------|:--------------------------|
| 1                       | B                         |
| 2                       | T                         |
| 3                       | A                         |

### REF_SPECIES

Mapping espèce

On transpose les codes espèce AVDTH (**ESPECE.C_ESP**) en **Species.faoCode**

| **ESPECE.C_ESP** | **Species.faoCode** |
|:-----------------|:--------------------|
| 8                | TUN                 |
| 9                | TUN                 |
| 34               | BUM                 |
| 40               | TUS                 |
| 43               | RAV*                |
| 99               | XXX*                |
| 701              | PIL                 |
| 702              | CLP                 |
| 703              | JAX                 |
| 704              | PAX                 |
| 705              | ANE                 |
| 706              | CLP                 |
| 707              | BOG                 |
| 708              | CLP                 |
| 709              | MZZ                 |
| 801              | YFT                 |
| 802              | SKJ                 |
| 803              | BET                 |
| 804              | ALB                 |
| 805              | LTA                 |
| 806              | FRI                 |
| 807              | SKH                 |
| 809              | TUN                 |
| 810              | KAW                 |
| 811              | LOT                 |
| 843              | RAV*                |
| 899              | XXX*                |

### REF_SPECIES_2 (pour les catégories de poids)

Pour calculer les différentes catégories de poids, on effectue une translation
sur les codes espèces AVDTH

| **ESPECE.C_ESP** | **ESPECE.C_ESP** | note                                                     |
|:-----------------|:-----------------|:---------------------------------------------------------|
| 8                | 41               |                                                          |
| 9                | 41               |                                                          |
| 16               | 310              | //FIXME add BIP Bonito oriental                          |
| 19               | 23               | //FIXME add BOP Palomette                                |
| 21               | 23               | //FIXME add SSM Thazard atlantique, Maquereau espagnol   |
| 22               | 23               | //FIXME add KGM Thazard serra                            |
| 24               | 23               | //FIXME add CER Thazard franc                            |
| 25               | 23               | //FIXME add COM Thazard rayé (Indo-Pacifique)            |
| 34               | 23               | //FIXME add BLZ Makaire bleu Indo-Pacifique              |
| 40               | 23               | //FIXME add BGT Grand thonidés                           |
| 43               | 5                | // FIXME add RAV                                         |
| 99               | 41               |                                                          |
| 701              | 41               | //FIXME Add Appat species ?                              |
| 702              | 41               | //FIXME Add Appat species ?                              |
| 703              | 41               | //FIXME Add Appat species ?                              |
| 704              | 41               | //FIXME Add Appat species ?                              |
| 705              | 41               | //FIXME Add Appat species ?                              |
| 706              | 5                | //FIXME Add Appat species ?                              |
| 707              | 41               | //FIXME Add Appat species ?                              |
| 708              | 41               | //FIXME Add Appat species ?                              |
| 709              | 41               | //FIXME Add Appat species ?                              |
| 801              | 1                |                                                          |
| 802              | 2                |                                                          |
| 803              | 3                |                                                          |
| 804              | 4                |                                                          |
| 805              | 5                |                                                          |
| 806              | 6                |                                                          |
| 809              | 41               |                                                          |
| 807              | 7                |                                                          |
| 810              | 10               |                                                          |
| 811              | 11               |                                                          |
| 817              | 17               |                                                          |
| 818              | 18               |                                                          |
| 820              | 20               |                                                          |
| 829              | 29               |                                                          |
| 830              | 30               |                                                          |
| 831              | 31               |                                                          |
| 832              | 32               |                                                          |
| 833              | 33               |                                                          |
| 834              | 34               |                                                          |
| 835              | 35               |                                                          |
| 836              | 36               |                                                          |
| 837              | 37               |                                                          |
| 838              | 38               |                                                          |
| 839              | 39               |                                                          |
| 843              | 5                |                                                          |
| 844              | 44               |                                                          |
| 845              | 45               |                                                          |
| 846              | 46               |                                                          |
| 847              | 47               |                                                          |
| 848              | 48               |                                                          |
| 849              | 49               |                                                          |
| 850              | 50               |                                                          |
| 851              | 51               |                                                          |
| 852              | 52               |                                                          |
| 853              | 53               |                                                          |
| 854              | 54               |                                                          |
| 855              | 55               |                                                          |
| 856              | 56               |                                                          |
| 857              | 57               |                                                          |
| 858              | 58               |                                                          |
| 899              | 41               |                                                          |
| 1042             | 41               | //FIXME Remove this ACC Grémille                         |
| 7066             | 312              |                                                          |

### REF_SIZE_MEASURE_TYPE

Mapping type de mesure de taille

À partir de la colonne AVDTH ```x=F_LDLF```, on obtient le code de **ps_common.SizeMeasureType** ainsi :

```code = x==2 ? FL : DL```

### REF_OBSERVED_SYSTEM

Mapping systèmes observés

Pour transposer tous les codes **ACT_ASSOC.C_ASSOC** en code **ps_common.ObservedSystem.code**, **aucun autre code n'est autorisé**.

| **ACT_ASSOC.C_ASSOC** | **ps_common.ObservedSystem.code** |
|:----------------------|:----------------------------------|
| 0                     | 0                                 |
| 10                    | 18                                |
| 11                    | 23                                |
| 12                    | 24                                |
| 13                    | 25                                |
| 14                    | 26                                |
| 20                    | 20                                |
| 21                    | 20                                |
| 22                    | 20                                |
| 23                    | 20                                |
| 24                    | 20                                |
| 25                    | 20                                |
| 26                    | 17                                |
| 27                    | 27                                |
| 28                    | 28                                |
| 29                    | 31                                |
| 30                    | 4                                 |
| 31                    | 4                                 |
| 32                    | 32                                |
| 33                    | 33                                |
| 34                    | 34                                |
| 35                    | 35                                |
| 36                    | 36                                |
| 37                    | 37                                |
| 38                    | 38                                |
| 40                    | 40                                |
| 41                    | 41                                |
| 42                    | 42                                |
| 43                    | 43                                |
| 50                    | 50                                |
| 51                    | 11                                |
| 52                    | 10                                |
| 53                    | 53                                |
| 60                    | 12                                |
| 61                    | 13                                |
| 62                    | 62                                |
| 70                    | 70                                |
| 71                    | 71                                |
| 72                    | 72                                |
| 73                    | 73                                |
| 74                    | 74                                |
| 80                    | 80                                |
| 81                    | 81 + 20                           |
| 90                    | 90                                |
| 91                    | 91                                |
| 92                    | 92                                |
| 93                    | 93                                |
| 94                    | 94                                |
| 95                    | 95                                |
| 96                    | 96                                |
| 97                    | 97                                |
| 98                    | 98                                |
| 99                    | 101                               |

### REF_VESSEL_ACTIVITY_TYPE

Mapping type d'activité

| **ACTIVITY.C_OPERA** | **ps_common.VesselActivity.code** |
|:---------------------|:----------------------------------|
| 0                    | 6   (1)                           |
| 1                    | 6   (1)                           |
| 2                    | 6   (1)                           |
| 14                   | 6   (1) (3)                       |
| 3                    | 2 (ou 102) (4)                    |
| 4                    | 1 (ou 101) (4)                    |
| 5                    | 13  (2)                           |
| 6                    | 13  (2)                           |
| 22                   | 13  (2)                           |
| 23                   | 13  (2)                           |
| 24                   | 13  (2)                           |
| 25                   | 13  (2)                           |
| 26                   | 13  (2)                           |
| 29                   | 13  (2)                           |
| 30                   | 13  (2)                           |
| 31                   | 13  (2)                           |
| 32                   | 13  (2)                           |
| 33                   | 13  (2)                           |
| 34                   | 13  (2)                           |
| 40                   | 13  (2)                           |
| 41                   | 13  (2)                           |
| 7                    | 11                                |
| 8                    | 10                                |
| 9                    | 23                                |
| 10                   | 24                                |
| 11                   | 25                                |
| 12                   | 26                                |
| 13                   | 27                                |
| 15                   | 0                                 |
| 19                   | 29                                |
| 20                   | 30                                |
| 35                   | 31                                |
| 36                   | 36 (5)                            |

1. **Seule une activité de code 6 permet la création de captures**.
2. **Seule une activité de code 13 permet la création de DCP**.
3. On ajoute en plus un système observé **110** et **setSuccessStatus** à **2**.
4. Si un dcp est trouvé alors on utilise un type d'activité introduit en version *9.1.0* qui permet l'ajout de dcp.
5. Le type d'activité a été ajouté lors de la migration de la version *9.1.0* (avant on utilisait *99*)
6. Si **C_OPERA** est supérieur ou égal à **52**, alors on fait correspondre le référentiel avec le même code (il doit alors être présent dans le référentiel).

### REF_SCHOOL_TYPE

Mapping type de banc

| **ACTIVITY.C_TBANC** | **ps_common.SchoolType.code** |
|:---------------------|:------------------------------|
| 1                    | 1                             |
| 2                    | 2                             |
| 3                    | 0                             |

### REF_CATCH_WEIGHT_CATEGORY

Mapping catégorie de poids capture

Soit ```x=CAPT.C_ESP``` et ```y=CAPT.C_CAT_T```

on déduit le code de **ps_logbook.WeightCategory** par

``` code = C-REF_SPECIES_2(x)-y```

Si **code** se termine par **-9** on n'utilise pas de catégorie.

On ne tient pas compte non plus des codes 
```
C-41-5
C-41-6
C-41-7
C-41-8
```

### REF_WELL_SAMPLING_CONFORMITY

Mapping conformité d'échantillonnage de cuve

À partir de **CUVE.C_DEST**, on déduit **ps_logbook.WellSamplingConformity.code**

| **CUVE.C_DEST**   | **WellSamplingConformity.code** |
|:------------------|:--------------------------------|
| 1                 | 9                               |
| 2                 | 9                               |
| 3                 | 9                               |
| 4                 | 9                               |
| 5                 | 9                               |
| 6                 | 9                               |
| 7                 | 9                               |
| 8                 | 9                               |
| 9                 | 9                               |
| 10                | 1                               |
| 11                | 1                               |
| 12                | 1                               |
| 13                | 1                               |
| 14                | 1                               |
| 15                | 1                               |
| 16                | 1                               |
| 17                | 1                               |
| 20                | 2                               |
| 21                | 2                               |
| 22                | 2                               |

### REF_WELL_SAMPLING_STATUS

Mapping status d'échantillonnage de cuve

À partir de **CUVE_DEST**, on déduit **ps_logbook.WellSamplingStatus.code**

| **CUVE.C_DEST** | **WellSamplingStatus.code**  |
|:----------------|:-----------------------------|
| 1               | 9                            |
| 2               | 9                            |
| 3               | 9                            |
| 4               | 9                            |
| 5               | 9                            |
| 6               | 9                            |
| 7               | 9                            |
| 8               | 9                            |
| 9               | 9                            |
| 10              | 1                            |
| 11              | 2                            |
| 12              | 3                            |
| 13              | 4                            |
| 14              | 5                            |
| 15              | 6                            |
| 16              | 7                            |
| 17              | 8                            |
| 20              | 10                           |
| 21              | 11                           |
| 22              | 12                           |

### REF_WELL_SAMPLE_WEIGHT_CATEGORY

Mapping catégorie de poids plan de cuve

Soit ```x=CUVE_CALEE.C_ESP``` et ```y=CUVE_CALEE.C_CAT```

on déduit le code de **ps_logbook.WeightCategory** par

``` code = W-REF_SPECIES_2(x)-y```

### REF_SAMPLE_QUALITY

Mapping qualité d'échantillon


Mapping direct à partir du code de **ps_logbook.SampleQuality**

### REF_SAMPLE_TYPE

Mapping type d'échantillon

Mapping direct à partir du code de **ps_logbook.SampleType**

### REF_LANDING_WEIGHT_CATEGORY

Mapping catégorie de poids débarquement

Soit ```x=LOT_COM.C_ESP``` et ```y=LOT_COM.C_CAT_C```

on déduit le code de **ps_landing.WeightCategory** par

``` code = REF_SPECIES_2(x)-y```

Si **code** se termine par **-9** on n'utilise pas de catégorie.

[Retour à l'introduction](./index.html)