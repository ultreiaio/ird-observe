# Import des données d'échantillonnage

Toutes les données sont déversées dans le paquetage **ps_logbook**.

### Table ECHANTILLON

Table déversée dans la table d'ObServe **Sample**.

|n°              | nom           | destination                   |note        |
|:---            | :---          | :---                          |:---        |
| ECHANTILLON_01 | C_BAT         | Sample.trip                   | [1](#n_6_1)| 
| ECHANTILLON_02 | D_DBQ         | Sample.trip                   | [1](#n_6_1)| 
| ECHANTILLON_03 | N_ECH         | Sample.number                 |            |
| ECHANTILLON_04 | C_QUAL_ECH    | Sample.sampleQuality          | [2](#n_6_2)| 
| ECHANTILLON_05 | C_TYP_ECH     | Sample.sampleType             | [3](#n_6_3)| 
| ECHANTILLON_06 | C_PORT_DBQ    | Not used                      |            |
| ECHANTILLON_07 | N_CUVE        | Sample.well                   | [4](#n_6_4)| 
| ECHANTILLON_08 | F_POS_CUVE    | Sample.well                   | [4](#n_6_4)| 
| ECHANTILLON_09 | F_S_ECH       | Sample.superSample            | [5](#n_6_5)| 
| ECHANTILLON_10 | V_POIDS_M10   | Sample.smallsWeight           |            |
| ECHANTILLON_11 | V_POIDS_P10   | Sample.bigsWeight             |            |
| ECHANTILLON_12 | V_POIDS_ECH   | Sample.totalWeight            |            |

* Note 1 <a name="n_6_1"></a>

Identifie la marée

* Note 2 <a name="n_6_2"></a>

Voir mapping [REF_SAMPLE_QUALITY](./referential.html#REF_SAMPLE_QUALITY)

* Note 3 <a name="n_6_3"></a>

Voir mapping [REF_SAMPLE_TYPE](./referential.html#REF_SAMPLE_TYPE)

* Note 4 <a name="n_6_4"></a>

Identifiant de cuve

Voir mapping [REF_WELL_ID](./referential.html#REF_WELL_ID)

* Note 5 <a name="n_6_5"></a>

F_S_ECH == 1

### Table ECH_CALEE

Table déversée dans la table d'ObServe **Sample_activity**.

|n°            | nom        | destination                        |note        |
|:---          | :---       | :---                               |:---        |
| ECH_CALEE_01 | C_BAT      | Sample_activity.sample             | [1](#n_5_1)| 
| ECH_CALEE_02 | D_DBQ      | Sample_activity.sample             | [1](#n_5_1)| 
| ECH_CALEE_03 | N_ECH      | Sample_activity.sample             | [1](#n_5_1)| 
| ECH_CALEE_04 | D_ACT      | Sample_activity.activity           | [2](#n_5_2)| 
| ECH_CALEE_05 | N_ACT      | Sample_activity.activity           | [2](#n_5_2)| 
| ECH_CALEE_06 | C_ZONE_GEO | Not used                           |            |
| ECH_CALEE_07 | Q_ACT      | Sample_activity.activity           | [2](#n_5_2)| 
| ECH_CALEE_08 | V_LAT      | Sample_activity.activity           | [2](#n_5_2)| 
| ECH_CALEE_09 | V_LON      | Sample_activity.activity           | [2](#n_5_2)| 
| ECH_CALEE_10 | C_TBANC    | ???                                |            |
| ECH_CALEE_11 | V_POND     | ???                                |            |

* Note 1 <a name="n_5_1"></a>

Identifie l'échantillon

* Note 2 <a name="n_5_2"></a>

Identifie l'activité

### Table ECH_ESP

Table déversée dans la table d'ObServe **SampleSpecies**.

|n°          | nom       | destination                           |note        |
|:---        | :---      | :---                                  |:---        |
| ECH_ESP_01 | C_BAT     | Sample.topiaId                        | [1](#n_7_1)| 
| ECH_ESP_02 | D_DBQ     | Sample.topiaId                        | [1](#n_7_1)| 
| ECH_ESP_03 | N_ECH     | Sample.topiaId                        | [1](#n_7_1)| 
| ECH_ESP_04 | N_S_ECH   | SampleSpecies.subSampleNumber         |            |
| ECH_ESP_05 | C_ESP     | SampleSpecies.species                 | [2](#n_7_2)| 
| ECH_ESP_06 | F_LDLF    | SampleSpecies.sizeMeasureType         | [3](#n_7_3)| 
| ECH_ESP_07 | V_NB_MES  | SampleSpecies.measureCount            |            |
| ECH_ESP_08 | V_NB_TOT  | SampleSpecies.totalCount              |            |

* Note 1 <a name="n_7_1"></a>

Identifie l'échantillon

* Note 2 <a name="n_7_2"></a>

Voir mapping [REF_SPECIES](./referential.html#REF_SPECIES)

* Note 3 <a name="n_7_3"></a>

Voir mapping [REF_SIZE_MEASURE_TYPE](./referential.html#REF_SIZE_MEASURE_TYPE)

### Table ECH_FREQT

Table déversée dans la table d'ObServe **SampleSpeciesMeasure**.

|n°            | nom      | destination                          |note        |
|:---          | :---     | :---                                 |:---        |
| ECH_FREQT_01 | C_BAT    | SampleSpeciesMeasure.sampleSpecies   | [1](#n_8_1)| 
| ECH_FREQT_02 | D_DBQ    | SampleSpeciesMeasure.sampleSpecies   | [1](#n_8_1)| 
| ECH_FREQT_03 | N_ECH    | SampleSpeciesMeasure.sampleSpecies   | [1](#n_8_1)| 
| ECH_FREQT_04 | N_S_ECH  | SampleSpeciesMeasure.sampleSpecies   | [1](#n_8_1)| 
| ECH_FREQT_05 | C_ESP    | SampleSpeciesMeasure.sampleSpecies   | [1](#n_8_1)| 
| ECH_FREQT_06 | F_LDLF   | SampleSpeciesMeasure.sampleSpecies   | [1](#n_8_1)| 
| ECH_FREQT_07 | V_LONG   | SampleSpeciesMeasure.sizeClass       |            |
| ECH_FREQT_08 | V_EFF    | SampleSpeciesMeasure.count           |            |

* Note 1 <a name="n_8_1"></a>

Identifie l'échantilon espèce.

[Retour à l'introduction](./index.html)