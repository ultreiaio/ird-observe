# Consolidation

Ce document décrit les règles de calcul utilisées lors de l'opération de consolidation.

## Modèle commun

Pour le moment aucune consolidation n'est effectuée sur ce modèle.

## Modèle Palangre

Pour le moment aucune consolidation n'est effectuée sur ce modèle.

## Modèle Senne

### Paquetage Commun

Pour le moment aucune consolidation n'est effectuée sur ce paquetage.

### Paquetage Livre de Bord

#### Entité Objet flottant (ps.logbook.FloatingObject)

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/logbook/FloatingObjectConsolidateActions.html#FloatingObjectConsolidateActions)

| Code                                                                        | Champs                                                                                                                |
|:----------------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------|
| ps.logbook.FloatingObjectConsolidateActions.CleanComputedValuesWhenArriving | computedWhenArrivingBiodegradable <br/>computedWhenArrivingNonEntangling<br/>computedWhenArrivingSimplifiedObjectType |         

Si l'objet flottant n'est pas actif sur l'arrivée, alors supprimer toutes les données calculées sur l'arrivée.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/CleanComputedValuesAction.html)

| Code                                                                       | Champs                                                                                                            |
|:---------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------|
| ps.logbook.FloatingObjectConsolidateActions.CleanComputedValuesWhenLeaving | computedWhenLeavingBiodegradable<br/>computedWhenLeavingNonEntangling<br/>computedWhenLeavingSimplifiedObjectType |         

Si l'objet flottant n'est pas actif sur le départ, alors supprimer toutes les données calculées sur le départ.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/CleanComputedValuesAction.html)

| Code                                                                         | Champs                            |
|:-----------------------------------------------------------------------------|:----------------------------------|
| ps.logbook.FloatingObjectConsolidateActions.ComputeBiodegradableWhenArriving | computedWhenArrivingBiodegradable |         

Calcul de la propriété **computedWhenArrivingBiodegradable** à partir des matériaux sur l'arrivée.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeBiodegradableConsolidateAction.html)

| Code                                                                        | Champs                           |
|:----------------------------------------------------------------------------|:---------------------------------|
| ps.logbook.FloatingObjectConsolidateActions.ComputeBiodegradableWhenLeaving | computedWhenLeavingBiodegradable |         

Calcul de la propriété **computedWhenLeavingBiodegradable** à partir des matériaux sur le départ.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeBiodegradableConsolidateAction.html)

| Code                                                                         | Champs                            |
|:-----------------------------------------------------------------------------|:----------------------------------|
| ps.logbook.FloatingObjectConsolidateActions.ComputeNonEntanglingWhenArriving | computedWhenArrivingNonEntangling |         

Calcul de la propriété **computedWhenArrivingNonEntangling** à partir des matériaux sur l'arrivée.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeNonEntanglingConsolidateAction.html)

| Code                                                                        | Champs                           |
|:----------------------------------------------------------------------------|:---------------------------------|
| ps.logbook.FloatingObjectConsolidateActions.ComputeNonEntanglingWhenLeaving | computedWhenLeavingNonEntangling |         

Calcul de la propriété **computedWhenLeavingNonEntangling** à partir des matériaux sur le départ.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeNonEntanglingConsolidateAction.html)

| Code                                                                                | Champs                                   |
|:------------------------------------------------------------------------------------|:-----------------------------------------|
| ps.logbook.FloatingObjectConsolidateActions.ComputeSimplifiedObjectTypeWhenArriving | computedWhenArrivingSimplifiedObjectType |         

Calcul de la propriété **computedWhenArrivingSimplifiedObjectType** à partir des matériaux sur l'arrivée.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeSimplifiedObjectTypeConsolidateAction.html)

| Code                                                                               | Champs                                  |
|:-----------------------------------------------------------------------------------|:----------------------------------------|
| ps.logbook.FloatingObjectConsolidateActions.ComputeSimplifiedObjectTypeWhenLeaving | computedWhenLeavingSimplifiedObjectType |

Calcul de la propriété **computedWhenLeavingSimplifiedObjectType** à partir des matériaux sur le départ.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeSimplifiedObjectTypeConsolidateAction.html)

#### Entité Sample (ps.logbook.Sample)

[Code source](./core/core-persistence/core-persistence-consolidation/xref/fr/ird/observe/consolidation/data/ps/logbook/SampleConsolidateActions.html#SampleConsolidateActions)

| Code                                               | Champs                                                 |
|:---------------------------------------------------|:-------------------------------------------------------|
| ps.logbook.SampleConsolidateActions.ComputeWeights | bigsWeight, smallsWeight, totalWeight, weightsComputed |         

Calcul des sample.minus10Weight, sample.plus10Weight et sample.totalWeight par sommation des lots du plan de cuves, 
à condition que ce dernier ait été saisi.

[Ticket 2669](https://gitlab.com/ultreiaio/ird-observe/-/issues/2669)

#### Entité SampleActivity (ps.logbook.SampleActivity)

[Code source](./core/core-persistence/core-persistence-consolidation/xref/fr/ird/observe/consolidation/data/ps/logbook/SampleActivityConsolidateActions.html#SampleActivityConsolidateActions)

| Code                                                              | Champs                                 |
|:------------------------------------------------------------------|:---------------------------------------|
| ps.logbook.SampleActivityConsolidateActions.ComputeWeightedWeight | weightedWeight, weightedWeightComputed |         

Calcul du poids pondéré à partir du plan de cuve s'il existe.

[Ticket 2053](https://gitlab.com/ultreiaio/ird-observe/-/issues/2053)

### Paquetage Observations

#### Entité Calée (ps.observation.Set)

[Code source](./core/core-persistence/core-persistence-consolidation/xref/fr/ird/observe/consolidation/data/ps/observation/SetConsolidateActions.html#SetConsolidateActions)

| Code                                                   | Champs     |
|:-------------------------------------------------------|:-----------|
| ps.observation.SetConsolidateActions.ComputeSchoolType | SchoolType |         

Calcul du type de banc à partir des systèmes observés de l'activité.

#### Entité Objet flottant (ps.observation.FloatingObject)

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/observation/FloatingObjectConsolidateActions.html#FloatingObjectConsolidateActions)

| Code                                                                            | Champs                                                                                                                |
|:--------------------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------|
| ps.observation.FloatingObjectConsolidateActions.CleanComputedValuesWhenArriving | computedWhenArrivingBiodegradable <br/>computedWhenArrivingNonEntangling<br/>computedWhenArrivingSimplifiedObjectType |         

Si l'objet flottant n'est pas actif sur l'arrivée, alors supprimer toutes les données calculées sur l'arrivée.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/CleanComputedValuesAction.html)

| Code                                                                           | Champs                                                                                                            |
|:-------------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------|
| ps.observation.FloatingObjectConsolidateActions.CleanComputedValuesWhenLeaving | computedWhenLeavingBiodegradable<br/>computedWhenLeavingNonEntangling<br/>computedWhenLeavingSimplifiedObjectType |         

Si l'objet flottant n'est pas actif sur le départ, alors supprimer toutes les données calculées sur le départ.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/CleanComputedValuesAction.html)

| Code                                                                             | Champs                            |
|:---------------------------------------------------------------------------------|:----------------------------------|
| ps.observation.FloatingObjectConsolidateActions.ComputeBiodegradableWhenArriving | computedWhenArrivingBiodegradable |         

Calcul de la propriété **computedWhenArrivingBiodegradable** à partir des matériaux sur l'arrivée.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeBiodegradableConsolidateAction.html)

| Code                                                                            | Champs                           |
|:--------------------------------------------------------------------------------|:---------------------------------|
| ps.observation.FloatingObjectConsolidateActions.ComputeBiodegradableWhenLeaving | computedWhenLeavingBiodegradable |         

Calcul de la propriété **computedWhenLeavingBiodegradable** à partir des matériaux sur le départ.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeBiodegradableConsolidateAction.html)

| Code                                                                             | Champs                            |
|:---------------------------------------------------------------------------------|:----------------------------------|
| ps.observation.FloatingObjectConsolidateActions.ComputeNonEntanglingWhenArriving | computedWhenArrivingNonEntangling |         

Calcul de la propriété **computedWhenArrivingNonEntangling** à partir des matériaux sur l'arrivée.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeNonEntanglingConsolidateAction.html)

| Code                                                                            | Champs                           |
|:--------------------------------------------------------------------------------|:---------------------------------|
| ps.observation.FloatingObjectConsolidateActions.ComputeNonEntanglingWhenLeaving | computedWhenLeavingNonEntangling |         

Calcul de la propriété **computedWhenLeavingNonEntangling** à partir des matériaux sur le départ.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeNonEntanglingConsolidateAction.html)

| Code                                                                                    | Champs                                   |
|:----------------------------------------------------------------------------------------|:-----------------------------------------|
| ps.observation.FloatingObjectConsolidateActions.ComputeSimplifiedObjectTypeWhenArriving | computedWhenArrivingSimplifiedObjectType |         

Calcul de la propriété **computedWhenArrivingSimplifiedObjectType** à partir des matériaux sur l'arrivée.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeSimplifiedObjectTypeConsolidateAction.html)

| Code                                                                                   | Champs                                  |
|:---------------------------------------------------------------------------------------|:----------------------------------------|
| ps.observation.FloatingObjectConsolidateActions.ComputeSimplifiedObjectTypeWhenLeaving | computedWhenLeavingSimplifiedObjectType |

Calcul de la propriété **computedWhenLeavingSimplifiedObjectType** à partir des matériaux sur le départ.

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/dcp/ComputeSimplifiedObjectTypeConsolidateAction.html)

#### Entité Capture (ps.observation.Catch)

[Code source des actions](./core/core-persistence/core-persistence-consolidation/xref/fr/ird/observe/consolidation/data/ps/observation/CatchConsolidateActions.html#CatchConsolidateActions)

[Code source de l'orchestrateur des calculs](./core/core-persistence/core-persistence-consolidation/xref/fr/ird/observe/consolidation/data/ps/observation/CatchConsolidateEngine.html#CatchConsolidateEngine)

| Code                                                                   | Champs                                  |
|:-----------------------------------------------------------------------|:----------------------------------------|
| ps.observation.CatchConsolidateActions.ComputeMeanLengthFromMeanWeight | meanLength<br/>meanLengthComputedSource |         

Pour calculer la taille moyenne de la capture à partir du poids moyen de la capture et du RTP adéquate.

La propriété calculée sera *CatchComputedValueSource.fromData*.

| Code                                                                   | Champs                                  |
|:-----------------------------------------------------------------------|:----------------------------------------|
| ps.observation.CatchConsolidateActions.ComputeMeanWeightFromMeanLength | meanWeight<br/>meanWeightComputedSource |         

Pour calculer le poids moyen de la capture à partir de la taille moyenne de la capture et du RTP adéquate.

La propriété calculée sera *CatchComputedValueSource.fromData*.

| Code                                                                                 | Champs                                    |
|:-------------------------------------------------------------------------------------|:------------------------------------------|
| ps.observation.CatchConsolidateActions.ComputeCatchWeightFromTotalCountAndMeanWeight | catchWeight<br/>catchWeightComputedSource |         

Pour calculer le poids total de la capture à partir du poids moyen et de l'effectif de la capture.

La propriété calculée sera *CatchComputedValueSource.fromData*.

| Code                                                                                 | Champs                                  |
|:-------------------------------------------------------------------------------------|:----------------------------------------|
| ps.observation.CatchConsolidateActions.ComputeTotalCountFromCatchWeightAndMeanWeight | totalCount<br/>totalCountComputedSource |         

Pour calculer l'effectif de la capture à partir du poids total et moyen de la capture.

La propriété calculée sera *CatchComputedValueSource.fromData*.

| Code                                                                                 | Champs                                  |
|:-------------------------------------------------------------------------------------|:----------------------------------------|
| ps.observation.CatchConsolidateActions.ComputeMeanWeightFromTotalCountAndCatchWeight | meanWeight<br/>meanWeightComputedSource |         

Pour calculer la taille moyenne à partir du poids total et de l'effectif de la capture.

La propriété calculée sera *CatchComputedValueSource.fromData*.

| Code                                                               | Champs                                  |
|:-------------------------------------------------------------------|:----------------------------------------|
| ps.observation.CatchConsolidateActions.CopyMeanLengthFromParameter | meanLength<br/>meanLengthComputedSource |         

Pour recopier la taille moyenne du RTP adéquate.

La propriété calculée sera *CatchComputedValueSource.fromReferentiel*.

| Code                                                               | Champs                                  |
|:-------------------------------------------------------------------|:----------------------------------------|
| ps.observation.CatchConsolidateActions.CopyMeanWeightFromParameter | meanWeight<br/>meanWeightComputedSource |         

Pour recopier le poids moyen du RTP adéquate.

La propriété calculée sera *CatchComputedValueSource.fromReferentiel*.

| Code                                                                | Champs                                  |
|:--------------------------------------------------------------------|:----------------------------------------|
| ps.observation.CatchConsolidateActions.ComputeMeanLengthFromSamples | meanLength<br/>meanLengthComputedSource |         

Pour calculer la taille moyenne à partir des tailles moyennes des échantillons associés.

La propriété calculée sera *CatchComputedValueSource.fromSample*.

| Code                                                                | Champs                                  |
|:--------------------------------------------------------------------|:----------------------------------------|
| ps.observation.CatchConsolidateActions.ComputeTotalCountFromSamples | totalCount<br/>totalCountComputedSource |         

Pour calculer l'effectif de la capture à partir des effectifs des échantillons associés.

La propriété calculée sera *CatchComputedValueSource.fromSample*.

| Code                                                                 | Champs              |
|:---------------------------------------------------------------------|:--------------------|
| ps.observation.CatchConsolidateActions.CleanCatchWeightMeasureMethod | weightMeasureMethod |         

Pour supprimer la méthode de mesure de poids si le poids a été calculé ou qu'il n'y a pas de poids.

#### Entité Échantillon (ps.observation.SampleMeasure)

[Code source](./core/core-persistence/core-persistence-consolidation/xref/fr/ird/observe/consolidation/data/ps/observation/SampleMeasureConsolidateActions.html#SampleMeasureConsolidateActions)

| Code                                                                   | Champs                      |
|:-----------------------------------------------------------------------|:----------------------------|
| ps.observation.SampleMeasureConsolidateActions.ComputeLengthFromWeight | length<br/>isLengthComputed |         

Calcul de la taille depuis le poids en utilisant la RTP adéquate.

| Code                                                                   | Champs                      |
|:-----------------------------------------------------------------------|:----------------------------|
| ps.observation.SampleMeasureConsolidateActions.ComputeWeightFromLength | weight<br/>isWeightComputed |         

Calcul du poids depuis la taille en utilisant la RTP adéquate.

### Paquetage Marché local

#### Entité Lot (ps.localmarket.Batch)

[Code source](./core/core-api/core-api-dto-consolidation/xref/fr/ird/observe/consolidation/data/ps/localmarket/BatchConsolidateActions.html#BatchConsolidateActions)

| Code                                                                      | Champs                          |
|:--------------------------------------------------------------------------|:--------------------------------|
| ps.localmarket.BatchConsolidateActions.ComputeWeightFromSpeciesMeanWeight | weight<br/>weightComputedSource |

Si le conditionnement est de type **Conditionnement non pesé**, on calcule alors le poids à partir du poids moyen de
l'espèce de la RTP adéquate (si celle-ci est définie) et de l'effectif du lot.

La propriété calculée sera *BatchWeightComputedValueSource.fromSpeciesMeanWeight*.

| Code                                                                        | Champs                          |
|:----------------------------------------------------------------------------|:--------------------------------|
| ps.localmarket.BatchConsolidateActions.ComputeWeightFromPackagingMeanWeight | weight<br/>weightComputedSource |         

Si le conditionnement est de type **Espèce unité**, on calcule alors le poids à partir du poids moyen du
conditionnement (si celui est défini) et de l'effectif du lot.

La propriété calculée sera *BatchWeightComputedValueSource.fromPackagingMeanWeight*.

| Code                                                     | Champs                          |
|:---------------------------------------------------------|:--------------------------------|
| ps.localmarket.BatchConsolidateActions.ResetWeightValues | weight<br/>weightComputedSource |         

Dans les autres cas, on supprime tout simplement le poids car on ne peut pas le calculer.