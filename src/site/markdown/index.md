# ObServe

## Présentation

Le logiciel *ObServe*... **TODO**

# Les documents

- [Architecture logicielle](./architecture-logicielle.html)
- [Administration obstuna](./server-configuration-db.html)
- [Administration web (v9)](./server-configuration-application.html)
- [Administration web sécurité (v9)](./server-configuration-security.html)
- [Administration web (v7)](./server-configuration-application-v7.html)
- [Configuration client](./observe-client.html)
- [Configuration server](./observe-server.html)
- [Documentation de l'outil de reporting](./report/index.html)
- [Documentation de l'outil de consolidation](./consolidation.html)




