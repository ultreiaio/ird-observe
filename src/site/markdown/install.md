# Installation d'ObServe

## Configuration des interfaces graphiques

Il est possible de modifier certains aspects de l'interfaces graphiques en modifiant le fichier
ui.properties qui se trouve à la racine du jar principal sans pour autant à avoir à recompiler
le projet :

- modifier les icones de la navigation

- modifier les icones des actions

- modifiers les couleurs

# Configuration des validateurs

Il est possible de modifier les règles de validations à partir du jar du module de validation.

# Configuration de l'utilisateur

L'application crée un fichier de configuration sur chaque poste où elle est installé.

Ce fichier se nomme **.observe/observe-client.conf** et se trouve dans le répertoire principal de l'utilisateur.

TODO définir ce qu'il faut y mettre.
