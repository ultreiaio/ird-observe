# List of specialized referential filters used in GUI

This document enumerates all specialized referential filters used in data source forms.

## Data PS

### Common Trip

* CAPTAIN: 
  
```Person filter on captain flag```

* OBSERVER: 

```Person filter on observer flag```

* DATA_ENTRY_OPERATOR: 

```Person filter on dataEntryOperator flag```

* VESSEL:
 
```Vessel filter on vessel type within config[vesselType.seine.common.trip]```

### Observation Activity

See how to make generic accessibility to Add Set (Add new flag ```allowSet```)

### Observation NonTargetCatchRelease

* SPECIES: 

```Species filter on all species in NonTargetCatch where SpeciesGroup within config[speciesGroup.seine.observation.nonTargetCatchRelease]```

FIXME add it

### Observation Catch

* SPECIES: 

```Species filter on trip ocean and within config[speciesList.seine.observation.catche]```

### Observation Sample

* SPECIES: 

```Species filter on NonTargetCatch species```

* SPECIES_FATE: 

```SpeciesFate filter on NonTargetCatch species fate```

* SIZE_MEASURE_TYPE:

```SizeMeasureType filter on config[sizeMeasureType.seine.observation.targetSample] (LD1 or LF)```

### Observation ObjectSchoolEstimate

* SPECIES: 

```Species filter on config[speciesList.seine.observation.objectSchoolEstimate]```
 
### Observation SchoolEstimate

* SPECIES: 

```Species filter on trip ocean and within config[speciesList.seine.observation.schoolEstimate]```

### Observation ObjectObservedSpecies

* SPECIES: 

```Species filter on config[speciesList.seine.observation.objectObservedSpecies]```

## Longline

### Common Trip

* CAPTAIN: 
  
```Person filter on captain flag```

* OBSERVER: 

```Person filter on observer flag```

* OBSERVATIONS_DATA_ENTRY_OPERATOR: 

```Person filter on dataEntryOperator flag```

* LOGBOOK_DATA_ENTRY_OPERATOR: 

```Person filter on dataEntryOperator flag```

* VESSEL:
 
```Vessel filter on vessel type within config[vesselType.longline.common.trip]```

* SPECIES: 

```Species filter on trip ocean and within config[speciesList.longline.common.trip]```

### Observation Activity

See how to make generic accessibility to Add Set (Add new flag ```allowSet```)

### Observation Catch

*  SPECIES_CATCH: 

```Species filter on trip ocean and within config[speciesList.longline.observation.catch]```

FIXME rename to LlObservationCatchSpeciesId

*  PREDATOR: 

```Species filter on trip ocean and within config[speciesList.longline.depredator]```

### Observation Encounter

* SPECIES: 

```Species filter on config[speciesList.longline.observation.encounter]```

### Observation Tdr

* SPECIES: 

```Species filter on trip ocean and within config[speciesList.longline.observation.catch]```

### Landing

* PERSON: 

```Person filter on dataSource flag```

* VESSEL: 

```Vessel filter on vessel type within config[vesselType.longline.landing]```

### LandingPart

* SPECIES: 

```Species filter on trip ocean and within config[speciesList.longline.landing]```

### Logbook Activity

See how to make generic accessibility to Add Set (Add new flag ```allowSet```)

### Logbook Sample

* SPECIES: 

```Species filter on trip ocean and within config[speciesList.longline.logbook.sample]```

### Logbook Catch

* SPECIES_CATCH: 

```Species filter on trip ocean and within config[sepeciesList.longline.logbook.catch]```

* PREDATOR: 

```Species filter on trip ocean and within config[sepeciesList.longline.depredator]```
