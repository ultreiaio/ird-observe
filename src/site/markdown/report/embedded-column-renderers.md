# Documentation des règles de rendu de colonnes disponibles

Ce document décrit toutes les règles de rendu de colonnes disponibles sur les rapports.

### HighlightIfAbsoluteDeltaIsPositive

Ce rendu permet de vérifier que les valeurs de chaque cellule de deux colonnes sont identiques.

Si deux valeurs ne sont pas égales, on colorise la cellule en orange (avertissement) pour un premier seuil et en rouge
(erreur)  selon un second seuil.

Le rendu nécessite quatre paramètres :

* la première colonne
* la seconde colonne
* le seuil pour afficher des avertissements
* le seuil pour afficher des erreurs

Exemple d'utilisation :

```properties
report.xxx.columnRenderers.1.type=HighlightIfAbsoluteDeltaIsPositive
report.ccc.columnRenderers.1.parameters=10|11|0.0001|0.5
```

### HighlightIfNumericalValueIsPositive

Ce rendu permet de vérifier que les valeurs de chaque celleule d'une colonne n'est pas zéro.

Si la valeur n'est pas zéro, on colorise la cellule en orange (avertissement) pour un premier seuil et en rouge
(erreur)  selon un second seuil.

Le rendu nécessite quatre paramètres :

* la colonne
* le seuil pour afficher des avertissements
* le seuil pour afficher des erreurs

Exemple d'utilisation

```properties
report.xxx.columnRenderers.1.type=HighlightIfNumericalValueIsPositive
report.xxx.columnRenderers.1.parameters=19|0.0001|0.5
```

### HighlightIfEquals18nReferentialValue

Ce rendu permet d'afficher de coloriser en rouge toute cellule des colonnes sélectionnées dont la valeur (de type
référentiel i18n) n'est pas celle spécifié (via son identifiant).

Le rendu nécessite deux paramètres :

* une liste de colonnes sépararées par des virgules
* l'identifiant du référentiel à mettre en valeur

Exemple d'utilisation :

```properties
report.xxx.columnRenderers.1.type=HighlightIfEquals18nReferentialValue
report.xxx.columnRenderers.1.parameters=14,15,16,17,18,19,20,21|fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#999|fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#000|fr.ird.referential.ps.common.AcquisitionStatus#1464000000000#001
```
