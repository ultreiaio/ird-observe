# Documentation des opérations disponibles

Ce document décrit toutes les opérations disponibles sur les rapports.

On en distingue deux types :

 * les opérations génériques : elles peuvent être utilisées pour écrire de nouveaux rapports, leur fonctionnement n'est pas lié au modèle de données d'ObServe
 * les opérations spécifiques : elles correspondent à des opérations utilisées sur des rapports embarqués et ne devraient pas être utilisées pour de nouveaux rapports

## Les opérations génériques

### ComputeDynamicHeader

Cette opération permet de construire des en-têtes du résultat à partir de deux choses :

 * une première partie qui est une liste de colonnes fixes
 * une seconde partie qui utilise une variable de répétition pour décrire le reste des en-têtes de colonnes de manière dynamique

**Cette opération requière un paramétrage**.

Exemple d'utilisation :

```properties
report.xxx.repeatVariable.1.name=reasonForDiscard
report.xxx.repeatVariable.1.type=fr.ird.observe.entities.referential.ps.observation.ReasonForDiscard
report.xxx.repeatVariable.1.request=Select sf \
From ReasonForDiscardImpl sf \
Order by sf.code
report.xxx.operations.1.type=ComputeDynamicHeader
report.xxx.operations.1.parameters=Espèce,Total BL,Total BO|reasonForDiscard
```

**À noter que pour décorer les valeurs dynamique, on utilise si c'est une entité de référentiel de type *I18n*, son *label2*, sinon un *toString*.**

Les décorations pourraient être améliorées pour utiliser les décorateurs associées au type d'entitée, mais nous n'avons pas eu ce besoin pour le moment.

### CopyRepeatVariableToFirstColumn

Cette opération permet de recopier dans la première colonne du résultat, l'univers d'une variable de répétition.

**Cette opération requière un paramétrage**.

Exemple d'utilisation :

```properties
report.xxx.repeatVariable.1.name=length
report.xxx.repeatVariable.1.type=java.lang.Float
report.xxx.repeatVariable.1.request=Select distinct ntl.length \
From TripImpl t \
Join t.routeObs r \
Join r.activity a \
Join a.set.sample nts \
Join nts.sampleMeasure ntl with ntl.species.id = :species And ntl.sizeMeasureType.id = :sizeMeasureType \
Where t.id In :tripId \
Order By ntl.length
report.xxx.operations.1.type=CopyRepeatVariableToFirstColumn
report.xxx.operations.1.parameters=length
```

Le meme mécanisme de décoration que pour l'opération précédente est utilisé.

### ExecuteRequestAndReorganizeDataByRepeatVariable

Cette opération permet d'exécuter une requête et ensuite de réorganiser le résultat selon les valeurs définies
via deux variables de répétition, une pour l'axe des abscisses et une par l'axe des ordonnées.

**Cette opération requière un paramétrage** avec trois valeurs (séparées via des *pipes*) :

 * le premier paramètre indique la variable de répétition pour ordonner sur l'axe des abscisses
 * le second paramètre indique la variable de répétition pour ordonner sur l'axe des ordonnées
 * le troisième paramètre est la requête à executer

Exemple d'utilisation

```properties
report.xxx.repeatVariable.1.name=length
report.xxx.repeatVariable.1.type=java.lang.Float
report.xxx.repeatVariable.1.request=Select distinct ntl.length \
From TripImpl t \
Join t.routeObs r \
Join r.activity a \
Join a.set.sample nts \
Join nts.sampleMeasure ntl with ntl.species.id = :species And ntl.sizeMeasureType.id = :sizeMeasureType \
Where t.id In :tripId \
Order By ntl.length
report.xxx.repeatVariable.2.name=speciesFate
report.xxx.repeatVariable.2.type=fr.ird.observe.entities.referential.ps.common.SpeciesFate
report.xxx.repeatVariable.2.request=Select sf \
From SpeciesFateImpl sf \
Order by sf.discard, sf.code
report.xxx.repeatVariable.2.addNull=true
report.xxx.operations.1.type=ExecuteRequestAndReorganizeDataByRepeatVariable
report.xxx.operations.1.parameters=length|speciesFate|Select ntl.length, sf.id, sum(ntl.count) \
From TripImpl t \
Join t.routeObs r \
Join r.activity a \
Join a.set.sample nts \
Join nts.sampleMeasure ntl With ntl.species.id = :species And ntl.sizeMeasureType.id = :sizeMeasureType \
Left Join ntl.speciesFate sf \
Where t.id In :tripId \
Group By ntl.length, sf \
Order By ntl.length
```

**À noter que la requête doit produire une résultat avec trois colonnes, la première pour l'axe des abscisses, 
la seconde pour l'axe des ordonnées et la troisième pour la valeur effective à replacer à la bonne position**.

**À noter aussi que les deux variables de répétition doivent être ordonnées afin de produire un résultat invariant.**
### SumColumn

Cette opération permet de réaliser la somme par colonne, i.e de rajouter une dernière ligne qui fait la somme des 
valeurs décimales de chaque colonne.

Le résultat sera un **entier avec décimales** (on arrondit à 4 décimales).

**À noter que si une colonne contient des valeurs non numériques, le total n'est pas effectué et la valeur de la 
celulle sera *-***.

**À noter aussi que si on se retrouve avec une seule colonne à sommer, alors la ligne *total* ne sera pas ajoutée**.

Exemple d'utilisation (sans paramétrage)

```properties
report.xxx.operations.1.type=SumColumn
```

L'opéréation peut être paramétrée pour indiquer sur quelle zone on doit effectuer les sommes.

Le paramétrage est une position sur le résultat final codifié sous la forme ```x|y```.

Exemple d'utilisation (avec paramétrage)

```properties
report.xxx.operations.1.type=SumColumn
report.xxx.operations.1.parameters=0|1
```

Ici on commence la somme à partir de la ligne **0** et la colonne **1**.

### SumIntColumn

Cette opération est identique à l'opération précédente, pour des entiers.

On effectue donc ici une somme d'entiers. Si une des valeurs de la colonne n'est pas un entier, on remplace alors
le résultat par **-**.

Le paramétrage optionnel est identique aussi.

Exemple d'utilisation (sans paramétrage)

```properties
report.xxx.operations.1.type=SumIntColumn
```
Exemple d'utilisation (avec paramétrage)

```properties
report.xxx.operations.1.type=SumIntColumn
report.xxx.operations.1.parameters=0|1
```

Ici on commence la somme à partir de la ligne **0** et la colonne **1**.

### SumRow

Cette opération permet de réaliser la somme par ligne, i.e de rajouter une dernière colonne qui fait la somme des
valeurs décimales de chaque ligne.

Le résultat sera un **entier avec décimales** (on arrondit à 4 décimales).

**À noter que si une ligne contient des valeurs non numériques, le total n'est pas effectué et la valeur de la
celulle sera *-***.

**À noter aussi que si on se retrouve avec une ligne colonne à sommer, alors la colonne *total* ne sera pas ajoutée**.

Exemple d'utilisation (sans paramétrage)

```properties
report.xxx.operations.1.type=SumRow
```

L'opéréation peut être paramétrée pour indiquer sur quelle zone on doit effectuer les sommes.

Le paramétrage est une position sur le résultat final codifié sous la forme ```x|y```.

Exemple d'utilisation (avec paramétrage)

```properties
report.xxx.operations.1.type=SumRow
report.xxx.operations.1.parameters=0|1
```

Ici on commence la somme à partir de la ligne **0** et la colonne **1**.

### SumIntRow

Cette opération est identique à l'opération précédente, pour des entiers.

On effectue donc ici une somme d'entiers. Si une des valeurs de la colonne n'est pas un entier, on remplace alors
le résultat par **-**.

Le paramétrage optionnel est identique aussi.

Exemple d'utilisation (sans paramétrage)

```properties
report.xxx.operations.1.type=SumIntRow
```

Exemple d'utilisation (avec paramétrage)

```properties
report.xxx.operations.1.type=SumIntRow
report.xxx.operations.1.parameters=0|1
```

Ici on commence la somme à partir de la ligne **0** et la colonne **1**.

### SubtractColum

Cette opération permet de réaliser la soustraction de deux colonnes, ligne par ligne et de consigner le résultat dans une nouvelle colonne.

Le résultat sera un **entier avec décimales** (on arrondit à 4 décimales).

**À noter que si une ligne contient des valeurs non numériques, la soustraction n'est pas effectuée et la valeur de la
celulle sera *-***.

L'opéréation doit être paramétrée pour indiquer les deux colonnes à traiter et où inscrire le résultat de la soustraction.

Le paramétrage est donc trois numéros de colonne codifié sous la forme ```column1|column2|targetColumn```.

Exemple d'utilisation

```properties
report.xxx.operations.1.type=SubtractColum
report.xxx.operations.1.parameters=0|1|2
```

Ici on soustrait la colonne 0 à la colonne 1 et on consigne le résultat dans la colonne 2.

## Opérations spécifiques à ObServe

Ces opérations étant utilisées uniquement dans les rapport embarqués, elles ne possèdent aucun paramétrage.

Nous ne donnerons pas d'exemple d'utilisation, mais le lien vers le rapport qui l'utilise.

## ComputeMeasurementsLongline

Cette opération permet pour un équipement déclaré d'une marée (du domaine Palangre), de regrouper les différentes 
composantes de cet équipement.

**Cette opération ne requière aucun paramétrage.**

Elle est utilisée dans le rapport [llCommonTripGearUseFeatures](./embedded-reports.html#llCommonTripGearUseFeatures).

## UnionAndSortLlLogbookSamples

Cette opération permet de regrouper les échantillons du modèle *Senne - Livre de bord* qui existent au niveau des 
activités et des marées, puis de les trier.

**Cette opération ne requière aucun paramétrage.**

Elle est utilisée dans le rapport [llLogbookSamplesOnBoth](./embedded-reports.html#llLogbookSamplesOnBoth).

## ComputeLocalmarketSampleWellAndSpeciesMeasure

Cette opération permet de calculer les distributions d'échantillons du modèle *Senne - Marché local* au niveau des cuves.

**Cette opération ne requière aucun paramétrage.**

Elle est utilisée dans le rapport [psLocalmarketSample](./embedded-reports.html#psLocalmarketSample).

## ComputeLocalmarketSurveyPart

Cette opération permet de calculer les données des sondages du modèle *Senne - Marché local*.

**Cette opération ne requière aucun paramétrage.**

Elle est utilisée dans le rapport [psLocalmarketSurvey](./embedded-reports.html#psLocalmarketSurvey).

## ComputeMeasurementsSeine

Cette opération permet pour un équipement déclaré d'une marée (du domaine Senne), de regrouper les différentes 
composantes de cet équipement.

**Cette opération ne requière aucun paramétrage.**

Elle est utilisée dans le rapport [psCommonTripGearUseFeatures](./embedded-reports.html#psCommonTripGearUseFeatures).

