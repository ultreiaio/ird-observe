# Documentation des rapports embarqués par l'application

Ce document décrit l'ensemble des rapports embarqués par l'application.

## Modèle Palangre

### llCommonTripGearUseFeatures

Liste des équipements

### llLanding

Livre de bord - Débarquements

### llLogbookActivities

Livre de bord - Liste des activités

### llLogbookCatches

Livre de bord - Liste des captures

### llLogbookSamplesOnActivity

Livre de bord - Liste des échantillons rattachés au niveau calée

### llLogbookSamplesOnBoth

Livre de bord - Liste des échantillons

### llLogbookSamplesOnTrip

Livre de bord - Liste des échantillons rattachés au niveau marée

## Modèle Senne

### psCommonTripGearUseFeatures

Liste des équipements

### psLocalmarketBatch

Marché local - Lots

### psLocalmarketSample

Marché local - Échantillons

### psLocalmarketSurvey

Marché local - Sondages

### psLogbookActivity

Livre de bord - Vérification croisée des captures

### psLogbookSampleMeasures

Livre de bord - Mesures par échantillons

### psLogbookSampleSet

Livre de bord - Calées des échantillons

### psLogbookSampleSpeciesMeasures

Livre de bord - Mesures par sous échantillons

### psLogbookSampleSpeciesMeasuresCount

Livre de bord - Nombre de mesures par sous échantillons

### psLogbookTrip

Livre de bord - Vérification croisée des données débarquements

### psLogbookWellPlan

Livre de bord - Plan de cuves

### psLogbookWellPlanCheck

Livre de bord - Vérification des plan de cuves

### psObservationActivityWithComment

Observations - Activités avec comment et leurs positions

## psObservationActivitiesByZone

Observations - Types d'activités par zones FPA

### psObservationAllActivities

Observations - Toutes les activités et leurs positions

### psObservationCatch

Observations - Liste des captures selon le type de banc, filtrées par groupe

### psObservationCatchTotalCountByGroupAndSpeciesFateDiscardPerAssociation

Observations - Dénombrement des captures selon le type d'association, filtrées par groupe et mode (rejeté/conservé)

### psObservationCatchTotalCountByGroupPerReasonForDiscard

Observations - Dénombrement des rejets par type de banc et raison de rejet, filtrés par groupe (en t)

### psObservationCatchTotalCountByGroupPerSpeciesFate

Observations - Dénombrement des captures par type de banc et devenir, filtrés par groupe

### psObservationCatchWeightByGroupAndSpeciesFateDiscardPerAssociation

Observations - Poids des captures selon le type d'association, filtrées par groupe et mode (rejeté/conservé) (en t)

### psObservationCatchWeightByGroupPerReasonForDiscard

Observations - Poids des rejets par type de banc et raison de rejet, filtrés par groupe (en t)

### psObservationCatchWeightByGroupPerSpeciesFate

Observations - Poids des captures par type de banc et devenir, filtrés par groupe (en t)

### psObservationDailySetAndCatch

Observations - Nombre de calées et captures journalières d'une marée

### psObservationFobUsageExtended

Observations - Utilisation des FOB, tableau détaillé

### psObservationFobUsageMinimal

Observations - Utilisation des FOB, tableau simplifiée

### psObservationLengthsDistribution

Observations - Distribution des tailles par espèces et type de mesure

### psObservationRepartionCaleeParCuve

Observations - Répartition des calées par cuves

### psObservationSetByAssociation

Observations - Nombre de calées selon le type d’association