# Documentation de la syntaxe d'un rapport

Ce document décrit comment écrire un rapport.

## Syntaxe d'un rapport

Comme écrit dans la page de présentation des rapports, ceux-ci sont décrits dans un fichier de type *properties*.

La syntaxe a été uniformisée en version 9 et depuis la 9.3.0 chaque rapport est décrit dans un fichier séparé.

### Identifiant d'un rapport

Chaque rapport possède un identifiant unique; le nom du fichier correspondant est **id.report**.

Voici un premier exemple de rapport pour mieux comprendre comment cela s'organise :

```properties
modelType=LL
name=Liste des équipements
name.en=Gears uses features
name.es=
description=Afficher les équipements
description.en=Display gear uses features
description.es=
columns=Equipement,Nombre,Utilisé dans la marée,Mesures
columns.en=Gear,Number,Used in trip?,Measures
columns.es=
repeatVariable.1.name=gearUseFeaturesId
repeatVariable.1.type=java.lang.String
repeatVariable.1.request=Select g.id \
From fr.ird.observe.entities.data.ll.common.TripImpl m \
Join m.gearUseFeatures g \
Where \
m.id In :tripId \
and g in elements (m.gearUseFeatures) \
Order By g.gear.@i18nColumnName@
request.1.location=0,0
request.1.layout=row
request.1.request=Select \
concat(CASE When g.gear.code IS NULL Then '@noCode@' Else  g.gear.code End, ' - ', g.gear.@i18nColumnName@), \
g.number, \
( CASE g.usedInTrip When true Then '@yes@' Else Case g.usedInTrip When false Then '@no@' Else '@Null@' End End )\
From fr.ird.observe.entities.data.ll.common.GearUseFeaturesImpl g \
Where g.id = :gearUseFeaturesId
request.1.repeat.name=gearUseFeaturesId
request.1.repeat.layout=column
operations.1.type=ComputeMeasurementsLongline
i18n.Null=Indéterminé
i18n.Null.en=Undefined
i18n.Null.es=Indeterminado
i18n.noCode=Aucun code
i18n.noCode.en=No code
i18n.noCode.es=
```

Ici l'identifiant du rapport est ```llCommonTripGearUseFeatures```.

Par convention, on utilise le domaine métier **ll**, suivi du sous-domaine métier **Common**, puis enfin un suffixe qui
exprime ce que le rapport doit produire **TripGearUseFeatures**.
  
### Méta-données d'un rapport

Il existe cinq méta-données pour un rapport (dont trois sont obligatoires) :

```properties
modelType=PS ou LL
name=Nom du rapport
name.en=Nom du rapport (Anglais)
name.es=Nom du rapport (Espagnol)
description=Description du rapport
description.en=Description du rapport (Anglais)
description.es=Description du rapport (Espagnol)
columns=En-têtes des colonnes du rapport (séparées par des virgules) [Optionnel]
columns.en=En-têtes des colonnes du rapport (séparées par des virgules) (Anglais) [Optionnel]
columns.es=En-têtes des colonnes du rapport (séparées par des virgules) (Espagnol) [Optionnel]
rows=En-têtes des lignes du rapport (séparées par des virgules) [Optionnel]
rows.en=En-têtes des lignes du rapport (séparées par des virgules) (Anglais) [Optionnel]
rows.es=En-têtes des lignes du rapport (séparées par des virgules) (Espagnol) [Optionnel]
```

```modelType``` permet de définir le domaine métier sur lequel porte le rapport, valeurs possibles :

* **PS** pour le domaine *Senne*
* **LL** pour le domaine *Palangre*.

En *V10*, on ajoutera certainement **COMMON** pour le domaine métier des marées agnostiques.

```name``` permet de définir le nom du rapport en français, ce texte est celui affiché dans la liste déroulante des
rapports à sélectionner dans l'assistant.

```name.en``` permet de définir le nom du rapport en anglais, si vide le texte français est utilisé.

```name.es``` permet de définir le nom du rapport en espagnol, si vide le texte français est utilisé.

```description``` permet de définir une description longue d'un rapport, ce texte est celui affiché
comme description lorsqu'un rapport est sélectionné dans l'assistant.

```description.en``` version anglaise, si vide le texte français est utilisé.

```description.es``` version espagnole, si vide le texte français est utilisé.

```colomns``` **[Optionnel]** permet de définir les en-têtes de colonnes de manière fixe.

```colomns.en``` version anglaise, si vide le texte français est utilisé.

```colomns.es``` version espagnole, si vide le texte français est utilisé.

```rows``` **[Optionnel]** permet de définir les en-têtes de lignes de manière fixe.

```rows.en``` version anglaise, si vide le texte français est utilisé.

```rows.es``` version espagnole, si vide le texte français est utilisé.

Il est possible de ne pas les indiquer si le rapport n'a pas d'en-têtes de colonnes ou de lignes fixes; Ces en-têtes
seront alors définies via les requêtes du rapport.

### Variables d'un rapport

Dans notre premier exemple, il n'y avait pas de variable (mais des variables de répétition). La syntaxe de ces deux
notions est exactement la même, au détail près qu'une variable est préfixée par ```variable``` alors qu'une variable de
répétition est préfixée par ```repeatVariable```.

Comme indiqué dans le préambule, l'ordre de description des variables importe, cet ordre sera utilisé pour :

* afficher les variables dans l'interface graphique,
* effectuer le calcul de l'univers des variables (dans le cas où des variables sont inter-dépendantes).

**À noter que l'outil de rapport utilise uniquement l'ordre induit dans la description du rapport, si par exemple la
première variable était dépendante de la seconde, alors le rapport ne pourra jamais être exécuté puisque la première
variable attend que la seconde soit remplie...**

Pour décrire une variable trois lignes sont nécessaires, comme on peut le voir dans l'exemple suivant :

```properties
variable.1.name=speciesGroup
variable.1.type=fr.ird.observe.dto.referential.common.SpeciesGroupReference
variable.1.request=Select distinct sg \
From TripImpl t \
Join t.routeObs r \
Join r.activity a \
Join a.set c \
Join c.catches ca with ca.totalCount Is Not Null Or ca.catchWeight Is Not Null \
Join ca.species e \
Join e.speciesGroup sg \
Where t.id In :tripId \
Order By sg.code
```

```variable.1.name``` définit l'*alias* de la variable que l'on pourra ensuite utiliser dans le
reste de la définition du rapport (par exemple dans d'autres variables, variables de répétition, requêtes ou
opérations).

```variable.1.type``` définit le type de la variable. Ce type doit être un type de *dto* ou une
*référence de dto*. On ne peut pas ici utiliser un type d'*entité*, puisque l'interface graphique (selon le principe de
séparation des couches) n'a pas connaissance du modèle des entités.

Techniquement, chaque type d'entité possède son type de *dto* et chaque type de *dto* possède un type de *référence*.

La notion de référence est utilisée dans l'application partout où l'on utilise des listes déroulantes, mais aussi dans
l'arbre de navigation; la notion de *dto* étant utilisée au niveau d'un formulaire.

À partir d'un type de référence, l'application est capable d'en déduire :

* le nom de la donnée à afficher
* comment décorer la donnée
* comment transformer l'entité

```variable.1.request``` définit la requête **hql** pour récupérer l'univers des valeurs de
la variable.

Il est possible de documenter cette variable en utilise une quatrième ligne optionnelle :

```properties
variable.1.comment=Un commentaire optionnel pour documenter la variable
```

**À noter que l'utilisation d'une variable dans le reste du rapport se fait toujours via son identifiant technique
(*topiaId*), ce qui ne sera pas le cas d'une variable de répétition qui elle utilisera toujours le type décrit.**

**À noter aussi que l'on peut utiliser pour décrire une variable, de la variable spéciale *tripId* qui reflète la
sélection des marées sur lequel on veut appliquer le rapport**.

Voici un second exemple de deux variables dont la seconde utilise la première :

```properties
variable.1.name=discardMode
variable.1.type=fr.ird.observe.dto.data.ps.observation.SpeciesFateDiscardModeDto
variable.1.request=Select distinct new fr.ird.observe.dto.data.ps.observation.SpeciesFateDiscardModeDto(sf.discard) \
From TripImpl t \
Join t.routeObs r \
Join r.activity a \
Join a.set c \
Join c.catches ca with ca.totalCount Is Not Null \
Join ca.speciesFate sf \
Where t.id In :tripId \
Order By sf.discard
variable.2.name=speciesGroup
variable.2.type=fr.ird.observe.dto.referential.common.SpeciesGroupReference
variable.2.request=Select distinct sg \
From TripImpl t \
Join t.routeObs r \
Join r.activity a \
Join a.set c \
Join c.catches ca with ca.totalCount Is Not Null \
Join ca.speciesFate sf with sf.discard = :discardMode \
Join ca.species e \
Join e.speciesGroup sg \
Where t.id In :tripId \
Order By sg.code
```

Cet exemple est intéressant car il nous permet de voir comment utiliser un type qui ne reflète pas exactement une
entité.

Ici **SpeciesFateDiscardModeDto** est un objet que l'on a ajouté dans le code de l'application pour refléter les
différents modes de captures :

* conservé (sf.discard = ```false```)
* rejeté (sf.discard = ```true```)
* non connu (sf.discard = ```NULL```)

### Variables de répétition d'un rapport

Comme indiqué plus haut, la syntaxe d'une variable de répétition est identique à celle d'une simple variable.

Pour décrire une variable de répétition, trois lignes sont nécessaires, comme on peut le voir dans l'exemple suivant :

```properties
repeatVariable.1.name=gearUseFeaturesId
repeatVariable.1.type=java.lang.String
repeatVariable.1.request=Select g.id \
From fr.ird.observe.entities.data.ll.common.TripImpl m \
Join m.gearUseFeatures g \
Where \
m.id In :tripId \
and g in elements (m.gearUseFeatures) \
Order By g.gear.label2
```

```repeatVariable.1.name``` définit l'*alias* de la variable de répétition à utiliser dans une requête, autre
variable de répétition ou opération

```repeatVariable.1.type``` définit le type de la variable de répétition. Contrairement aux variables, les
variables de répétition sont utilisées en interne pour exécuter le rapport, on peut donc utiliser directement des types
d'*entité*,
ou bien des types *simples* qui correspondent aux types d'une colonne en base (**java.lang.String**, **java.lang.Float
**, ...)

**À noter que contrairement aux variables, ici le type de la donnée sera toujours utilisé tel quel dans les autres
requêtes :
si on définit une variable de type *entité*, c'est bien l'entité qui sera injectée dans la requête *hql*, et non pas
juste son identifiant technique.**

```repeatVariable.1.request``` définit la requête **hql** pour récupérer l'univers des valeurs de la variable
de répétition

Il est possible d'utiliser dans une requête de variable de répétition, un alias sur toute variable du rapport (dont la
variable spéciale **tripId**), ainsi que tout autre alias de variable de répétition.

**À noter que l'outil de rapport utilise uniquement l'ordre induit dans la description du rapport, si par exemple la
première variable de répétition était dépendante de la seconde, alors le rapport ne pourra jamais être exécuté puisque
la
première variable attend que l'univers de la seconde soit calculée...**

On peut aussi ajouter une ligne supplémentaire pour indiquer que l'on veut rajouter la valeur **nulle** dans
l'univers de cette variable de répétion, ce qui peut etre utilise (et nous nous en servons), mais que nous ne pouvons
pas décrire via la requete *hql.

```properties
repeatVariable.1.addNull=true
```

Il est possible de documenter cette variable de répétition en utilisant une ligne optionnelle :

```properties
repeatVariable.1.comment=Un commentaire optionnel pour documenter la variable de répétition
```

### requêtes d'un rapport

On distingue deux types de requêtes :

* des requêtes *simples*
* des requêtes utilisant une variable de répétition (pour celles-ci on doit décrire en plus alors la variable de
  répétition à utiliser)

Les deux types de requêtes nécessitent les trois lignes, comme décrit dans l'exemple suivant :

```properties
request.1.location=0,0
request.1.layout=row
request.1.request=Select \
concat(CASE When g.gear.code IS NULL Then 'Aucun code' Else  g.gear.code End, ' - ', g.gear.label2), \
g.number, \
( CASE g.usedInTrip When true Then 'Oui' Else Case g.usedInTrip When false Then 'Non' Else 'Indéterminé' End End )\
From fr.ird.observe.entities.data.ll.common.GearUseFeaturesImpl g \
Where g.id = :gearUseFeaturesId
```

```request.1.location``` définit la position dans le résultat final où positionner le résultat de cette
requête.

```request.1.layout``` définit la disposition à utiliser pour placer le résultat de cette requête dans le
résultat final. Deux valeurs sont possibles :

* **row** pour signifier que le résultat de la requête sera positionné en ligne à partir de la position précedemment
  définie. Ce mode correspond exactement au resultat de la requête.
* **column** pour signifier que le résultat de la requête sera positionné en colonne à partir de la position
  précedemment définie. Ce mode est une transposition du résultat de la requête : une ligne du résultat de la requête
  sera une colonne dans le résultat final

```request.1.request``` définit le code **hql** qui permet de construire le résultat à placer ensuite dans le
résultat final du rapport

Pour une requête avec variable de répétition, il faut alors ajouter les deux lignes suivantes :

```properties
request.1.repeat.name=gearUseFeaturesId
request.1.repeat.layout=column
```

```request.1.repeat.name``` définit l'*alias* de la variable de répétition à utiliser. La requête sera
exécutée
autant de fois qu'il y a de valeurs dans l'univers calculé de la variable de répétition.

**À noter qu'il faut alors que le corps de cette requête doit utiliser l'alias de cette variable de répétition, même si
dans les faits, rien ne l'oblige, mais le résultat sera alors toujours le même...**

```request.1.repeat.layout``` définit la disposition à utiliser pour constuire le résultat final de la
requête appliqué à chaque valeur de la variable de répétition. Deux valeurs sont possibles :

* **row** pour signifier que pour chaque valeur de l'univers de la variable de répétition, le résultat de la requête
  sera positionné sur la même ligne;
* **column** pour signifier que pour chaque valeur de l'univers de la variable de répétition, le résultat de la requête
  sera positionné sur la même colonne.

**À noter qu'aucune vérification n'est effectuée sur la cohérence entre la disposition de la requête et la disposition
de la variable de répétition. On peut alors obtenir un résultat final de la requête incohérent si les dispositions ne
sont pas compatibles.**

Il est possible de documenter cette requête en utilisant une ligne optionnelle :

```properties
request.1.comment=Un commentaire optionnel pour documenter la requête
```

### Opérations d'un rapport

On distingue deux types d'opérations :

* celles sans paramétrage (une seule ligne est alors nécessaire)
* celles savec paramétrage (dans ce cas deux lignes sont nécessaires pour la décrire).

Les deux types d'opération nécessitent une première ligne pour déclarer le type d'opération à exécuter, comme décrit
dans l'exemple suivant :

```properties
operations.1.type=SumIntRow
```

```operations.1.type``` définit le type d'opération à réaliser.

Dans le cas où l'opération est paramétrable, on doit alors ajouter une autre ligne avec le paramétrage de l'opération :

```properties
operations.1.parameters=0|1
```

```operations.1.parameters``` définit le paramétrage à utiliser pour exécuter l'opération.

Il est possible de documenter cette opération en utilisant une ligne optionnelle :

```properties
operations.1.comment=Un commentaire optionnel pour documenter l'opération
```

Les opérations disponibles et leur documentation sont décrites dans le document [suivant](./embedded-operations.html).

### Rendu de colonnes

Depuis la version **9.3.0**, il est possible de définir des rendus de colonnes via le fichier de définition,
ce rendu sera valable dans le client swing ainsi que dans les rapports html.

Un rendu est défini par deux lignes :

1. Une pour définir le type de rendu
2. Une pour paramétrer ce rendu

```properties
columnRenderers.1.type=HighlightIfAbsoluteDeltaIsPositive
columnRenderers.1.parameters=10|11|0.0001|0.5
```

Il est possible d'ajouter plusieurs rendus sur un même rapport.

Les rendus disponibles et leur documentation sont décrits dans le document [suivant](./embedded-column-renderers.html).

### Traductions

**TODO**

## Pour aller plus loin

Vous pouvez aussi consulter la [documentation des rapports embarqués par l'application](./embedded-reports.html).
