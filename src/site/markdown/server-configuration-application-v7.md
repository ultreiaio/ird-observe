# Installation de l'application web

L'application web est une application web classique à installer dans un conteneur web (tomcat **< 10**).

Ce document explique comment installer et configurer l'application **en v7**.

Pour la **v9**, veuillez vous reporter à la cette [page](./server-configuration-application.html).

# Configuration de l'application web

La configuration de l'application web est regroupé dans un seul fichier à placer ici

```
  /etc/observe-server.conf
```

La configuration de l'application possède des valeurs par défaut pour toutes les options, si vous utiliser ces valeurs par défaut, un minimum de configuration est requise.

Une seule option doit être modifiée :

  - **observeweb.adminApiKey** : la clef à utiliser pour accéder aux services d'administration

Consulter la [page des configurations](./observe-server.html) pour connaitre l'ensemble des options de cette configuration.

Par défaut, l'application utilise le répertoire **/var/local/observeweb** pour y stoquer ses données.

Voici le contenu de ce dossier suite à un premier démarrage :

```
  /var/local/observe-server/{contextPath}
  |-- databases.yml  # configuration des bases de données
  |-- log
  |   `-- observeweb-XXX.log # logs de l'application
  |-- observeweb-log4j.conf # configuration des logs
  |-- temp # répertoire temporaire
  `-- users.yml # configuration des utilisateurs
```

Lors du premier démarrage, l'application génère des fichiers exemples de «*databases.yml*» et «*users.yml*».

Vous pouvez ensuite les modifier et recharger la configuration à chaud.

## Configuration sous Windows

Le fichier de configuration peut être placé dans le répertoire racine du tomcat.

Attention, à bien penser à échapper les <<\>> par des <<\\>> dans les options de répertoires.

Par exemple :

```
  observeweb.adminApiKey=a
  observeweb.baseDirectory=C:\\var\\local\\observeweb
  observeweb.sessionExpirationDelay=90
```

## Configuration des bases données

Le fichier databases.yml liste les bases de données connues par l'application.

Il s'agit donc ici bien uniquement de configuration de type postgresql.

Par exemple :

```
  databases:
  - name: production
    defaultDatabase: true
    roles:
    - login: technicien
      password: a
    - login: referentiel
      password: a
    url: jdbc:postgresql://localhost:5432/obstuna-production
  - name: test
    roles:
    - login: technicien
      password: a
    - login: referentiel
      password: a
    url: jdbc:postgresql://localhost:5432/obstuna-test
```

On décrit ici deux bases «*production*» (base par défaut) et «*test*» qui pointent sur les bases postgresql
«*obstuna-production*» et «*obstuna-test*».

Sur ces deux bases, on utilise deux utilisateurs (role postgres) «*technicien*» et «*referentiel*».

## Configuration des utilisateurs de l'application

Le fichier users.yml définit les utilisateurs de l'applications web ainsi que leur niveau de droit (mapping avec les droits des bases de données).

Par exemple :

```
  users:
  - login: utilisateur-technicien
    password: a
    permissions:
    - database: production
      role: technicien
      apiAccess: ALL
      validationMode: STRONG
    - database: test
      role: technicien
  - login: utilisateur-referentiel
    password: a
    permissions:
    - database: production
      role: referentiel
      apiAccess: CLIENT
      validationMode: STRONG

```

L'utilisateur «*utilisateur-technicien*» aura le droit d'accéder à la base de production et de test avec le rôle «*technicien*».

L'utilisateur «*utilisateur-referentiel*» aura le droit d'accéder à la base de production uniquement avec le rôle «*referentiel*».

### Droits sur les apis

Pour chaque permission d'un utilisateur, on peut définir son droit d'accès aux API.

Les différentes valeurs sont

  * **CLIENT** (pour avoir accès aux API du client lourd)
  * **PUBLIC** (pour avoir accès aux API public)
  * **ALL** (pour tous les accès)

La valeur par défaut est **CLIENT**.

### Configuration de la validation

Pour chaque permission d'un utilisateur, on peut définir le niveau de validation à utiliser dans les API publics

Les différentes valeurs sont

* **STRONG** (pour utiliser la validation forte)
* **PERMISSIVE** (pour utiliser la validation permissive)
* **NONE** (pour ne pas effectuer de validation)

La valeur par défaut est **NONE**.

## Tester l'application

Par défaut, l'application est déployée à l'adresse suivante :
```
  http://localhost:8080/observeweb/index.html?adminApiKey=change
```

Vous arrivez sur une page avec les différentes ressources disponibles de l'application.

Si vous arrivez sur cette page, l'application est fonctionnelle.

**Note :**

Le fait d'ajouter le paramètre **adminapiKey** va le transmettre aux urls qui en ont besoin.

## Configuration Apache

Par défaut le proxy_ajp a un timeout de 5 minutes, il faut augmenter cette valeur sinon les appels dépassant ce temps
seront rejetées.

Dans le fichier de configuration ``httpd.conf`` ajouter la ligne : (timeout de 3600 secondes = 60 minutes)
```
ProxyTimeout 3600
```

Une solution alternative meilleure (car non globale à l'instance de tomcat et surtout qui ne modifie pas les
configurations systèmes) est d'ajouter dans le host apache :

```
   ProxyPass / ajp://localhost:8009/ timeout=3600
   ProxyPassReverse / ajp://localhost:8009/ timeout=3600
```