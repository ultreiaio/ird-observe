# Installation de l'application web

L'application web est une application web classique à installer dans un conteneur web (tomcat **< 10**).

Ce document explique comment installer et configurer l'application.

Avant de déployer l'application il faut tout d'abord préparer sa configuration.

**Contrairement à la v7, le démarrage d'une instance de la v9 ne sera pas possible que si tout est bien configuré.**

## Configuration de l'application web

On distingue trois fichiers de configuration :

  1. le fichier de configuration de l'application
  2. le fichier de configuration de la sécurité d'une instance
  3. le fichier de configuration des logs d'une instance

## Configuration communes

La v9 introduit la notion de configurations communes.

Il s'agit de stoquer dans un répertoire adapté les différentes configurations utilisables pour plusieurs instances.

On y regroupe ici que les fichiers de configuration de la sécurité d'une instance.
```
  /var/local/observe-server/.conf/
```

## Fichier de configuration de l'application

Comme en version 7, la configuration de l'application web est regroupé dans un seul fichier dont l'emplacement est

```
  /etc/observe-server.conf
```

Ce fichier est commun à toutes les instances (que ce soit du v7 ou du v9).

La configuration de l'application possède des valeurs par défaut pour toutes les options modifiables, si vous utiliser ces valeurs par défaut, un minimum de configuration est requise.

Une seule option doit être modifiée :

  - **instance.security.key** : la clef à utiliser pour accéder aux services d'administration

Consulter la [page des configurations](./observe-server.html) pour connaitre l'ensemble des options de cette configuration.

### Migration v7 → v9

La configuration a été revue en v9.

Une migration automatique des options de la v7 est effectuée :

```
observeweb.adminApiKey            → instance.security.key
observeweb.temporaryFilesTimeout  → instance.timeout.temporary.files
observeweb.httpTimeout            → instance.timeout.http
observeweb.sessionExpirationDelay → instance.timeout.session
observeweb.sessionMaximumSize     → instance.session.maximum.size
```

## Fichiers de configuration de la sécurité d'une instance

En v7, on utilisait deux fichiers pour définir les accès à l'application (**databases.yml** et **users.yml**).

Ses fichiers doivent être placé dans

```
  /var/local/observe-server/.conf/7.x/databases.yml
  /var/local/observe-server/.conf/7.x/users.yml
```

En v9, on utilise un seul fichier qui regroupe les deux fichiers de la v7 (**security.yml**).

Son emplacement par défaut est
```
  /var/local/observe-server/.conf/9.x/security.yml
```

### Format du fichier

Voir [page de configuration de la sécurité](./server-security-configuration.html).

# Répertoire de travail des instances

Par défaut, l'application utilise le répertoire **/var/local/observe-server** pour y stoquer ses données.

**Attention en v7 avant la v7.6.8 le répertoire était** : **/var/local/observeweb**.

Pour chaque instance, on utilise le **contextPath** du *war* pour définir le répertoire de l'instance.

Par exemple, si on a déployé l'application web sur **demo**, on utilisera le répertoire
```
/var/local/observe-server/demo
```

Ce répertoire contient les fichiers de configuration de l'instance et un répertoire de **resources**.

Les fichiers de configuration sont
  1. le fichier de configuration de l'instance (**observe-server.conf**)
  2. le fichier de configuration des logs (**log.xml**)
  3. le fichier de configuration de la sécurité déjà évoqué plus haut (**security.yml**)

Le répertoire **resources** contient les *logs* et **fichiers temporaires* de l'instance.

Au démarrage de l'instance,

  1. on crée les répertoires nécessaires
  2. on génére si besoin le fichier de configuration de l'instance.
  2. on génére si besoin le fichier de configuration des logs.
  3. pour le fichier de la sécurité on procède ainsi
    * si le fichier existe, rien à faire
    * si le fichier commun existe, on crée un lien symbolique sur ce fichier
    * sinon l'application génère un fichier exemple

Voici le contenu de ce dossier suite à un premier démarrage :

```
/var/local/observe-server
├── .conf
│   ├── 7.x
│   │   ├── databases.yml
│   │   └── users.yml
│   └── 9.x
│       └── security.yml
└── demo
    ├── log.xml
    ├── observe-server.conf
    ├── resources
    │   ├── log
    │   │   └── observe-server-XXX.log
    │   └── temp
    └── security.yml -> /var/local/observe-server/.conf/9.x/security.yml
```

## Configuration sous Windows

Le fichier de configuration peut être placé dans le répertoire racine du tomcat.

Attention, à bien penser à échapper les <<\>> par des <<\\>> dans les options de répertoires.

Par exemple :

```
  observeweb.adminApiKey=a
  observeweb.baseDirectory=C:\\var\\local\\observe-server
  observeweb.sessionExpirationDelay=90
```

## Tester l'application

Par défaut, l'application est déployée à l'adresse suivante :
```
  http://localhost:8080/observeweb/index.html?adminApiKey=changeme
```

Vous arrivez sur une page avec les différentes ressources disponibles de l'application.

Si vous arrivez sur cette page, l'application est fonctionnelle.

**Note :**

Le fait d'ajouter le paramètre **adminApiKey** va le transmettre aux urls qui en ont besoin.

## Configuration Apache

Par défaut le proxy_ajp a un timeout de 5 minutes, il faut augmenter cette valeur sinon les appels dépassant ce temps
seront rejetées.

Dans le fichier de configuration ``httpd.conf`` ajouter la ligne : (timeout de 3600 secondes = 60 minutes)
```
ProxyTimeout 3600
```

Une solution alternative meilleure (car non globale à l'instance de tomcat et surtout qui ne modifie pas les
configurations systèmes) est d'ajouter dans le host apache :

```
   ProxyPass / ajp://localhost:8009/ timeout=3600
   ProxyPassReverse / ajp://localhost:8009/ timeout=3600
```