# Installation du serveur Obstuna

## Pré-requis

- Un certain nombre d'outils et scripts sont intégrés dans ObServe, il faut au
  préalable récupérer la dernière version de l'application.
  `downloads`_

- Un serveur Postgres doit être installé sur le serveur cible.

- Une machine possédant une jvm ayant les droits d'accès au serveur cible.

- A partir de la version 1.7, ObServe intègre des interfaces graphiques pour
  effectuer les opérations d'administration sur les bases obstuna.

  Pour utiliser ces nouveaux outils, il faut remplir ces conditions :

  - posséder une base postgres accessible par le programme
  - un utilisateur propriétaire de la base
  - tous les autres utilisateurs créés sur le serveur postgres

## Création d'une base obstuna

La mise en place d'une nouvelle base obstuna se fait en 2 étapes :

- création de la base vierge et des rôles sur le serveur postgres
- création du contenu de la base via ObServe

Note

  **Il faut de plus avoir à disposition une autre base qui contient le
  référentiel à importer dans la nouvelle base.**
  
### Phase 1

Pour créer une nouvelle instance d'obstuna, il faut au préalable avoir une
base vierge (sans schéma) et les utilisateurs sur le serveur postgres.

Utilisation de commandes classiques **pg**, rien à décrire de plus ici.

### Phase 2

Il suffit enfin de lancer l'application en mode *création de base obstuna* via
le raccourci suivant :

```
  (cd scripts/postgresl ; ./create.sh)
```
ou

```
  ./scripts/postgresl/create.bat
```

Cela va effectuer les opérations suivantes après configuration de la base cible,
de la base d'import de référentiel et des rôles de sécurité :

- supprimer toutes les tables de la base si elles existent
- créer le schéma à partir du mapping de l'ORM (hibernate)
- remplir le référentiel à partir de celui d'une autre base obstuna à jour (topia-service-migration)
- appliquer la sécurité sur les rôles de la base

Note : le script appelle l'application avec l'action **--admin create**

## Mise à jour d'une base obstuna

Pour mettre à jour une base obstuna via le service de migration intégré dans
ObServe, il suffit de lancer l'application en mode *mise à jour obstuna* via le
raccourci suivant :

```
  (cd scripts/postgresl ; ./update.sh)
```
ou

```
  ./scripts/postgresl/update.bat
```
Ce mode va effectuer les opérations suivantes après configuration de la base
cible et des rôles de sécurité :

- mettre à jour le schéma et les données via le service de migration intégré (topia)
- appliquer la sécurité sur les rôles de la base

Note : le script appelle l'application avec l'action **--admin update**

## Mise à jour de la sécurité

On différencie dans ObServe plusieurs types d'utilisateurs pour les connexions
distantes :

- ceux qui peuvent juste lire le référentiel sans rien modifier (les referentiels)
- ceux qui peuvent juste lire des données sans rien modifier (les lecteurs)
- ceux qui ont le droit de modifier les données et le référentiel (les techniciens et administrateurs).

Pour appliquer la sécurité sur les utilisateurs d'une base obstuna, il suffit
de lancer Observe en mode *mise à jour sécurité obstuna* via le raccourci
suivant :

```
  (cd scripts/postgresl ; ./update-security.sh)
```
ou

```
  ./scripts/postgresl/update-security.bat
```
Ce mode va effectuer les opérations suivantes après configuration de la base
cible et des rôles de sécurité :

- appliquer la sécurité sur les rôles de la base

Note : le script appelle l'application avec l'action **--admin security**

## Vider une base obstuna

Pour vider une base obstuna (ne vue de la recréer par exemple), il suffit de
lancer ObServe en mode *suppression obstuna* via le raccourci suivant :

```
  (cd scripts/postgresl ; ./drop.sh)
```
ou

```
  ./scripts/postgresl/drop.bat
```
Ce mode va effectuer les opérations suivantes après configuration de la base
cible :

- supprimer toutes les tables gérer de la base

Note : le script appelle l'application avec l'action **--admin drop**

## Appliquer des scripts sql supplémentaires

Depuis la version 3.0, il est possible de lancer sur la base de scripts sql supplémentaires placé dans le répertoire **extra**.

On lance ensuite la commande

```
  (cd scripts/postgresl ; ./apply-extra.sh)
```
Le script demande

- le nom de la base et l'utilisateur de connexion
- une confirmation d'exécution pour chaque script qu'il a trouvé dans le répertoire **extra**
