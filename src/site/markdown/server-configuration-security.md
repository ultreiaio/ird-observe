## Fichiers de configuration de la sécurité

En v7, on utilisait deux fichiers pour définir les accès à l'application (**databases.yml** et **users.yml**).

Ses fichiers doivent etre placé dans

```
  /var/local/observe-server/.conf/7.x/databases.yml
  /var/local/observe-server/.conf/7.x/users.yml
```

En v9, on utilise un seul fichier qui regroupe les deux fichiers de la v7 (**security.yml**).

Son emplacement par défaut est
```
  /var/local/observe-server/.conf/9.x/security.yml
```

Ce fichier utilise la notion de références ou *ancres* pour éviter de répéter des données, 
cela permet aussi une validation syntaxique accrue.

## Migration v7 → v9

Il existe un utilitaire fourni qui effectuer les migrations.

Consulter la [section des téléchargements (fichier observe-*-server-configuration-tools.zip)](./CHANGELOG.html).

À télécharger sur le serveur.

### Format du fichier

Le fichier **security.yml** est de cette forme :

```
roles:     -- pour définir les utilisateurs des bases de données
...
databases: -- pour définir les bases de données (se sert de roles)
...
users:     -- pour définir les utilisateurs de l'application (se sert de roles et databases)
```

### Définir un role

Un **role** représente un compte pour accéder à une base de données.

Sa définition est

  * un login
  * un mot de passe

Le login est représenté comme une *ancre* dans le format *yaml*.

Par convention, on préfixe toujours celui-ci par **role-**.

#### Exemple

Pour définir un utilisateur **admin** avec un mot de passe **a**, on écrit
```yaml
roles:
  - &role-admin
    password: a
```

### Définir une base de donnée

Un **database** représente une base de données.

Sa définition est

 * un nom
 * une url
 * (optionel) est-ce la base par défaut de l'application ? *(1)*
 * des roles associés à cette base

Le nom est représenté comme une *ancre* dans le format *yaml*.

Par convention, on préfixe toujours celui-ci par **db-**.

**Note *(1)*** à partir de version **9.0.7** il n'est plus obligatoire d'avoir une base par défaut.

#### Exemple

Pour définir une dabase de donnée **production**, on écrit
```yaml
roles:
  - &role-admin
    password: a
databases:
  - &db-production
    url: jdbc:postgresql://localhost:5432/obstuna-production
    defaultDatabase: true
    roles:
      - *role-admin
```

### Définir un utilisateur

Un **user** représente un utilisateur de l'application web.

En v9 les utilisateurs ont été enrichis de deux nouvelles propriétés :

  * **apiAccess** le type d'accès aux API dont les valeurs sont :
    * **CLIENT** pour avoir accès uniquement aux API du client lourd,
    * **PUBLIC** pour avoir accès uniquement aux API public,
    * **ALL** pour avoir accès aux deux
    * valeur par défaut **CLIENT**
  * **validationMode** le mode de validation dont les valeurs sont :
    * **STRONG** pour utiliser une validation forte
    * **PERMISSIVE** pour utiliser une validation souple
    * **NONE** pour ne pas utiliser de validation
    * valeur par défaut **STRONG** 

Toujours en v9, on ajoute au niveau de l'utilisateur des valeurs par défaut qui seront utilisées 
pour toute permission ne définissant pas cette valeur.

Un utilisateur possède :

  * un login
  * un mot de passe
  * (optionel) une référence sur le role par défaut
  * (optionel) un type d'accès aux API par défaut
  * (optionel) un mode de validation par défaut
  * des permissions

Une permission possède :

  * une référence sur une base de données
  * (optionel) une référence sur un role de base de données
  * (optionel) un type d'accès aux API
  * (optionel) un mode de validation

#### Exemple

```yaml
roles:
  - &role-admin
    password: a
databases:
  - &db-production
    url: jdbc:postgresql://localhost:5432/obstuna-production
    defaultDatabase: true
    roles:
      - *role-admin
users:
  - login: utilisateur-admin
    password: a
    permissions:
      - database: *db-production
        role: *role-admin
        apiAccess: ALL
        validationMode: STRONG
```

Voici un exemple plus complexe qui utilise les valeurs par défaut :

```yaml
roles:
  - &role-admin
    password: a
databases:
  - &db-production
    url: jdbc:postgresql://localhost:5432/obstuna-production
    defaultDatabase: true
    roles:
      - *role-admin
  - &db-test
    url: jdbc:postgresql://localhost:5432/obstuna-test
    roles:
      - *role-admin
users:
  - login: utilisateur-admin
    password: a
    defaultRole: *role-admin
    defaultApiAccess: ALL
    defaultValidationMode: STRONG
    permissions:
      - database: *db-production
      - database: *db-test
        validationMode: PERMISSIVE
```

Cette exemple équivant à cet autre :

```yaml
roles:
  - &role-admin
    password: a
databases:
  - &db-production
    url: jdbc:postgresql://localhost:5432/obstuna-production
    defaultDatabase: true
    roles:
      - *role-admin
  - &db-test
    url: jdbc:postgresql://localhost:5432/obstuna-test
    roles:
      - *role-admin
users:
  - login: utilisateur-admin
    password: a
    permissions:
      - database: *db-production
        role: *role-admin        
        apiAccess: ALL
        validationMode: STRONG
      - database: *db-test
        role: *role-admin
        apiAccess: ALL
        validationMode: PERMISSIVE
```
