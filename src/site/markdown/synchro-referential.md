# Synchronisation avancée du référentiel

Ce document décrit tous les cas possibles de la synchronisation avancée de référentiel.

## Notations

On parle ici d'une synchronisation d'une base gauche (``1``) vers une base droite (``2``) avec des droits de mise à 
jour dans les deux sens.

On décrit dans la suite les différentes cas de modification d'un référentiel ``A`` (``A1`` et ``A2``).

Les propriétés modifiées sont notées ``a, b, c, ...``.

Les dates de dernière mise à jour sera noté ``t1`` et ``t2``, on les prime lors d'une modification.

## Cas (1) d'une seule propriété à mettre à jour d'un seul côté

On suppose ici que le référentiel ``A`` est uniquement modifié sur une seule propriété ``a`` sur la source de droite.

On a donc ``t1 < t2``.

La mise à jour dans la base gauche rend alors les deux référentiels égaux :

 * toutes les propriétés métiers sont toutes identiques
 * ``t1' = t2``

## Cas (2) de plusieurs propriétés à mettre à jour d'un seul côté

On suppose ici que le référentiel ``A`` est uniquement modifié sur les propriétés ``a`` et ``b`` sur la source de droite.

On a donc ``t1 < t2``.

Si on met à jour les deux propriétés, on se retrouve dans le cas (1).

Supposons désormais que l'on ne met à jour que la propriété ``a``.

Afin de pouvoir ensuite lors d'une seconde mise à jour pouvoir encore mettre à jour ``b``, il faut donc que ``t1' != t2``.

On va donc positionner ``t1'`` arbitrairement entre son ancienne valeur et ``t2`` : ``t1 < t1' < t2``.

## Cas (3) de propriétés à mettre à jour des deux côtés

On suppose ici que le référentiel ``A`` est modifié sur les propriétés ``a1`` et ``b1`` sur la source de gauche et 
``a2`` et ``b2`` sur la source droite.

On doit appliquer le même principe que précdemment pour pouvoir toujours voir les modifications non encore appliquées 
lors d'une prochaine synchronisation.

On conserve donc l'ordre induit par ``t1`` et ``t2`` sur ``t1'`` et ``t2'``, juqu'à obtenir l'égalité lorsque toutes les
propriétés métiers sont égales.

## Cas de mise à jour avec cascade

Il s'agit de bien définir comment mettre à jour les associations multiples; normalement le cas est bien géré, mais il 
faudra s'en assurer.

```TODO```

## Cas de suppression avec cascade

```TODO```

## Cas d'une insertion

On report exactement le référentiel, donc pas de question à se poser.

## Cas de retour en arrière

Il s'agit du cas inverse d'une mise à jour, les mêmes principes sont donc à appliquer.

## Modification dans ToPIA ?

Il semblerait opportun de remplacer le ``topiaVersion`` actuellement encodé en entier par directement la date de 
dernière mise à jour; car au final le ``topiaVersion`` actuel n'est pas satisfaisant.

 






